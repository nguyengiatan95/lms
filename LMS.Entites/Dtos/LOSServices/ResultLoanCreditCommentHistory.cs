﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Entites.Dtos.LOSServices
{
    public class ResultLoanCreditCommentHistory
    {
        public long LoanBriefNoteId { get; set; }

        public long LoanBriefId { get; set; }

        public long Type { get; set; }

        public int UserId { get; set; }

        public string Note { get; set; }

        public int Status { get; set; }

        public string FullName { get; set; }

        public string ShopName { get; set; }

        public DateTime CreatedTime { get; set; }
    }
}
