﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Entites.Dtos.LOSServices
{
    public class LoanProduct
    {
        public int LoanProductId { get; set; }
        public string Name { get; set; }
        public object Status { get; set; }
        public object ConsultantRate { get; set; }
        public object ServiceRate { get; set; }
        public object LoanRate { get; set; }
        public object MaximumMoney { get; set; }
    }
}
