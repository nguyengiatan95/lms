﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Entites.Dtos.LOSServices
{
    public class Hub
    {
        public int shopId { get; set; }
        public string Name { get; set; }
        public object Address { get; set; }
        public object Phone { get; set; }
        public object Status { get; set; }
    }
}
