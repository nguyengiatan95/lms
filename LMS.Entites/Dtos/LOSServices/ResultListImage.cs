﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Entites.Dtos.LOSServices
{
    public class ResultListImage
    {
        public int? TypeId { get; set; }
        public string TypeName { get; set; }
        public List<ResultItemImage> LstFilePath { get; set; }
    }

    public class ResultItemImage
    {
        public string FilePath { get; set; }
        public string FileThumb { get; set; }
        public string TypeFile { get; set; }
    }
}
