﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Entites.Dtos.LOSServices
{
    public class BoundTelesale
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public object Password { get; set; }
        public string FullName { get; set; }
        public object Email { get; set; }
        public object Phone { get; set; }
        public object Status { get; set; }
        public object CreatedTime { get; set; }
        public object UpdatedTime { get; set; }
        public object CompanyId { get; set; }
        public object DepartmentId { get; set; }
        public object PositionId { get; set; }
        public object CityId { get; set; }
        public object GroupId { get; set; }
        public object DistrictId { get; set; }
        public object WardId { get; set; }
        public object CreatedBy { get; set; }
        public object Ipphone { get; set; }
        public object IsDeleted { get; set; }
        public object CountLoan { get; set; }
        public object ListGroup { get; set; }
        public List<object> ListUserModule { get; set; }
        public object HubIds { get; set; }
        public object UrlEdit { get; set; }
        public object UserIdSSO { get; set; }
        public object CiscoUsername { get; set; }
        public object CiscoPassword { get; set; }
        public object CiscoExtension { get; set; }
        public int TypeCallService { get; set; }
        public bool TelesaleRecievedLoan { get; set; }
        public int TypeRecievedProduct { get; set; }
    }
}
