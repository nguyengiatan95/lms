﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LOSServices
{
    /// <summary>
    /// doc tham chiếu: https://docs.google.com/document/d/19CROi7IPSGrCUbthIc9sv7bSvCO-Th4CQqEScAzKOsA/edit#
    /// </summary>
    public class LoanBriefDetail
    {
        public long loanBriefId { get; set; }
        public long customerId { get; set; }
        public LoanBriefDetail_CustomerInfomation customerInfomation { get; set; }
        public LoanBriefDetail_VaInfomation vaInfomation { get; set; }
        public LoanBriefDetail_CompanyInformation companyInformation { get; set; }
        public LoanBriefDetail_AddressHome addressHome { get; set; }
        public LoanBriefDetail_AddressHousehold addressHousehold { get; set; }
        public LoanBriefDetail_WorkInformation workInformation { get; set; }
        public List<LoanBriefDetail_ListRelativesAndColleagues_Item> listRelativesAndColleagues { get; set; }
        public LoanBriefDetail_LoanBriefForm loanBriefForm { get; set; }
        public LoanBriefDetail_AssetsInformation assetsInformation { get; set; }
        public LoanBriefDetail_LoanAmountInfomation loanAmountInfomation { get; set; }
        public LoanBriefDetail_CicInfomation cicInfomation { get; set; }
        public LoanBriefDetail_DisbursementInfomation disbursementInfomation { get; set; }
        public LoanBriefDetail_FraudCheckInfomation fraudCheckInfomation { get; set; }
        public LoanBriefDetail_EkycInfomation ekycInfomation { get; set; }
    }
    public class LoanBriefDetail_CustomerInfomation
    {
        public string platformType { get; set; }
        public string typeLoanBrief { get; set; }
        public string countCall { get; set; }
        public string fullName { get; set; }
        public string phone { get; set; }
        public string phoneOther { get; set; }
        public string nationalCard { get; set; }
        public string nationCardPlace { get; set; }
        public string dob { get; set; } // định dạng dd/MM/yyyy
        public string passport { get; set; }
        public string gender { get; set; }
        public string facebookAddress { get; set; }
        public string insurenceNumber { get; set; }
        public string insurenceNote { get; set; }
        public string merried { get; set; }
        public string numberBaby { get; set; }
        public string livingWith { get; set; }
        public string sim { get; set; }
        public string email { get; set; }
    }
    public class LoanBriefDetail_VaInfomation
    {
        public string vaAccountName { get; set; }
        public string vaAccountNumber { get; set; }
        public string vaBankName { get; set; }
        public string gpayAccountName { get; set; }
        public string gpayAccountNumber { get; set; }
    }
    public class LoanBriefDetail_CompanyInformation
    {
        public string companyName { get; set; }
        public string careerBusiness { get; set; }
        public string businessCertificationDate { get; set; }
        public string businessCertificationAddress { get; set; }
        public string headOffice { get; set; }
        public string address { get; set; }
        public string companyShareholder { get; set; }
        public string birthdayShareholder { get; set; }
        public string cardNumberShareholder { get; set; }
        public string cardNumberShareholderDate { get; set; }
        public string placeOfBirthShareholder { get; set; }
    }
    public class LoanBriefDetail_AddressHome
    {
        public string provinceName { get; set; }
        public string districtName { get; set; }
        public string wardName { get; set; }
        public string residentType { get; set; }
        public string livingTime { get; set; }
        public string customerShareLocation { get; set; }
        public string addressLatLng { get; set; }
        public string address { get; set; }
        public string addressGoogleMap { get; set; }

    }
    public class LoanBriefDetail_AddressHousehold
    {
        public string provinceName { get; set; }
        public string districtName { get; set; }
        public string wardName { get; set; }
        public string address { get; set; }
    }
    public class LoanBriefDetail_WorkInformation
    {
        public string jobName { get; set; }
        public int jobDescriptionId { get; set; }
        public string jobDescriptionName { get; set; }
        public string description { get; set; }
        public decimal totalIncome { get; set; }
        public string imcomeType { get; set; }
        public string companyInsurance { get; set; }
        public string workLocation { get; set; }
        public string businessPapers { get; set; }
        public string companyPhone { get; set; }
        public string companyTaxCode { get; set; }
        public string companyName { get; set; }
        public string provinceName { get; set; }
        public string districtName { get; set; }
        public string companyAddress { get; set; }
        public string companyAddressLatLng { get; set; }
        public string companyAddressGoogleMap { get; set; }
    }
    public class LoanBriefDetail_ListRelativesAndColleagues_Item
    {
        public string relationType { get; set; }
        public string fullName { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
    }
    public class LoanBriefDetail_LoanBriefForm
    {
        public string productName { get; set; }
        public string rateType { get; set; }
        public string productDetail { get; set; }
        public string ltv { get; set; }
        public string appraiser { get; set; }
        public string documentBusiness { get; set; }
    }
    public class LoanBriefDetail_AssetsInformation
    {
        public string brandName { get; set; }
        public string plateNumber { get; set; }
        public string loanAmountExpertise { get; set; }
        public string carName { get; set; }
        public string locate { get; set; }
        public decimal loanAmountExpertiseLast { get; set; }
        public string ownerFullName { get; set; }
        public string chassis { get; set; }
        public string engine { get; set; }
        public string motobikeCertificateNumber { get; set; }
        public string motobikeCertificateDate { get; set; }
        public string motobikeCertificateAddress { get; set; }
        public string postRegistration { get; set; }
        public string description { get; set; }
    }
    public class LoanBriefDetail_LoanAmountInfomation
    {
        public decimal loanAmount { get; set; }
        public int loanTime { get; set; }
        public string createdTime { get; set; }
        public string toDate { get; set; }
        public decimal loanAmountReceived { get; set; }
        public decimal totalFeeInsurence { get; set; }
        public decimal feeInsuranceOfCustomer { get; set; }
        public decimal rateMoney { get; set; }
        public bool buyInsurenceCustomer { get; set; }
        public bool buyInsuranceProperty { get; set; }
        public decimal lenderRate { get; set; }
        public decimal feeInsuranceOfProperty { get; set; }
    }
    
    public class LoanBriefDetail_CicInfomation
    {
        public string resultRuleCheck { get; set; }
        public string resultLocation { get; set; }
        public decimal creditScoring { get; set; }
    }
    public class LoanBriefDetail_DisbursementInfomation
    {
        public string receivingMoneyType { get; set; }
        public string bankName { get; set; }
        public string bankAccountName { get; set; }
        public string bankAccountNumber { get; set; }
        public string bankCardNumber { get; set; }
        public string bankBranch { get; set; }
    }
    public class LoanBriefDetail_FraudCheckInfomation
    {
        public string channel { get; set; }
        public List<string> message { get; set; }
        public string detailUrl { get; set; }
        public string hvChannel { get; set; }
        public List<string> hvMessage { get; set; }
    }
    public class LoanBriefDetail_EkycInfomation
    {
        public long id { get; set; }
        public long loanbriefId { get; set; }
        public DateTime createdAt { get; set; }
        public string addressValue { get; set; }
        public string addressCheck { get; set; }
        public string idValue { get; set; }
        public bool idCheck { get; set; }
        public string fullNameValue { get; set; }
        public bool fullNameCheck { get; set; }
        public string birthdayValue { get; set; } // định dạng dd-MM-yyyy
        public bool birthdayCheck { get; set; } 
        public string expiryValue { get; set; }
        public string expiryCheck { get; set; } 
        public string genderValue { get; set; }
        public string genderCheck { get; set; }
        public string ethnicityValue { get; set; }
        public string ethnicityCheck { get; set; }
        public string issueByValue { get; set; }
        public string issueByCheck { get; set; }
        public string issueDateValue { get; set; } // định dang dd/MM/yyyy
        public string issueDateCheck { get; set; } 
        public string religionValue { get; set; }
        public string religionCheck { get; set; }
        public string faceCompareCode { get; set; }
        public string faceCompareMessage { get; set; }
        public string faceQueryCode { get; set; }
        public string faceQueryMessage { get; set; }
    }
    
}
