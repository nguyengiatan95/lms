﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Entites.Dtos.LOSServices
{
    public class UserHub
    {
        public int userId { get; set; }
        public string username { get; set; }
        public object password { get; set; }
        public string fullName { get; set; }
        public object email { get; set; }
        public object phone { get; set; }
        public object status { get; set; }
        public object createdTime { get; set; }
        public object updatedTime { get; set; }
        public object companyId { get; set; }
        public object departmentId { get; set; }
        public object positionId { get; set; }
        public object cityId { get; set; }
        public object groupId { get; set; }
        public object districtId { get; set; }
        public object wardId { get; set; }
        public object createdBy { get; set; }
        public object ipphone { get; set; }
        public object isDeleted { get; set; }
        public object countLoan { get; set; }
        public object listGroup { get; set; }
        public List<object> listUserModule { get; set; }
        public object hubIds { get; set; }
        public object urlEdit { get; set; }
        public object userIdSSO { get; set; }
        public object ciscoUsername { get; set; }
        public object ciscoPassword { get; set; }
        public object ciscoExtension { get; set; }
        public int typeCallService { get; set; }
        public bool telesaleRecievedLoan { get; set; }
        public int typeRecievedProduct { get; set; }
    }
}
