﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LOSServices
{
    public class ResponseDataLOS
    {
        public ResponseMetaLOS Meta { get; set; }
        public object Data { get; set; }
    }

    public class ResponseMetaLOS
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int TotalRecords { get; set; }
    }
}
