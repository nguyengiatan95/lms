﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LOSServices
{
    public class DisbursementLoanReq
    {
        public long LoanBriefId { get; set; }
        public long LoanId { get; set; }
        public int Type { get; set; }
        public int CodeId { get; set; }
        public long FeeInsuranceOfCustomer { get; set; }
        public long FeeInsuranceOfProperty { get; set; }
        public string UserName { get; set; }
        public int IsHandleException { get; set; }
    }
}
