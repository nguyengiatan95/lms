﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Entites.Dtos.LOSServices
{
    public class ResultLoanDisbursmentWaitingReq
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int TypeID { get; set; }
        public string Search { get; set; }
        public int TotalRecord { get; set; }
    }
    public class ResultLoanDisbursmentWaiting
    {
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public long ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public long DistrictId { get; set; }
        public string DistrictName { get; set; }
        public long WardId { get; set; }
        public string WardName { get; set; }
        public long ShopId { get; set; }
        public string ShopName { get; set; }
        public long LoanBriefId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public decimal LoanAmountFinal { get; set; }
        public int LoanTime { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int Status { get; set; }
        public long LenderId { get; set; }
        public bool IsLocate { get; set; }
        public int ApproveId { get; set; }
        public int DisbursementBy { get; set; }
        public bool IsLock { get; set; }
        public DateTime NextDate { get; set; }
        public DateTime LenderReceivedDate { get; set; } // lender nhận đơn
        public DateTime LenderCareReceivedDate { get; set; } // phòng nguồn vốn nhận đơn
        public DateTime CreatedTime { get; set; } // thời gian tạo đơn 
        public int TypeInsurence { get; set; }
        public bool BackToLenderCare { get; set; } // đơn được trả lại
        public string LenderName { get; set; }
        public bool buyInsurenceCustomer { get; set; }// có mua bảo hiểm không
        public int StatusLMS { get; set; }

        public string StrStatus { get; set; }
        public string ContactCode
        {
            get
            {
                return "HĐ-" + LoanBriefId;
            }
        } // Mã HĐ

        public int StatusDisbursementAuto { get; set; }

        public string StrStatusDisbursementAuto { get; set; }
        public int TotalCount { get; set; }
        public bool EsignState { get; set; }//true đơn hợp đồng online
        public bool DebtRevolvingLoan { get; set; }//cấu trúc nợ
        public double LenderRate { get; set; } //lãi suất lender
    }
    public class WaitingMoneyDisbursementLenderLOS
    {
        public int LenderId { get; set; }
        public string LenderName { get; set; }
        public decimal TotalMoney { get; set; }
    }

    public class ResultLoanWaitingBlockDetailLOS
    {
        public bool IsLock { get; set; }
        public int LoanBriefId { get; set; }
        public string FullName { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int RateTypeId { get; set; }
        public string RateTypeName { get; set; }
        public string Phone { get; set; }
        public int Gender { get; set; }
        public string CardNumber { get; set; }
        public float LoanAmountFinal { get; set; }
        public int LoanTime { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int WardId { get; set; }
        public string WardName { get; set; }
        public string Address { get; set; }
        public string CompanyName { get; set; }
        public string JobTitle { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyCityName { get; set; }
        public string CompanyDistrictName { get; set; }
        public string CompanyWardName { get; set; }
        public string CompanyAddress { get; set; }
        public float TotalIncome { get; set; }
        public int BankId { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankCardNumber { get; set; }
        public string BankAccountName { get; set; }
        public string BankCode { get; set; }
        public int LenderId { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
    }
}
