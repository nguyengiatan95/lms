﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Entites.Dtos.LOSServices
{
    public class ReqLoanTopUp
    {
        public int LoanId { get; set; }

        public int LoanBriefId { get; set; }
    }
}
