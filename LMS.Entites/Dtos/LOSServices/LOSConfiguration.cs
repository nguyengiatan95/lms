﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LOSServices
{
    public class LOSConfiguration
    {
        public static string BaseUrl = "http://188.166.218.85:8877";// "http://178.128.86.175:8877";
        public static string AuthorizationLos = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiTE1TIiwiaWQiOjI0NTI2ODU0NjI1ODQ1ODUyfQ.GbJBHFdAHHorI9N5agH8zuuIMC3WG7jZVlkUuXoJgMs";
        public const string LOS_Action_GetLoanWaiting = "/api/v1.0/LMS/DisbursementWaiting";//"?page=1&pageSize=10";
        public const string LOS_Action_GetListComment = "/api/v1.0/LMS/ListComment";//"?page=1&pageSize=10";
        public const string LOS_Action_AddComment = "/api/v1.0/LMS/add_note";//"?page=1&pageSize=10";
        public const string LOS_Action_UpdateShop = "/api/v1.0/LMS/shop";
        public const string LOS_Action_GetLoanDetail = "/api/v1.0/LMS/LoanDetail";//?LoanId=10032"
        public const string LOS_Action_ReturnLoan = "/api/v1.0/LMS/ReturnLoan";
        public const string LOS_Action_LockLoan = "/api/v1.0/LMS/LoanLock";
        public const string LOS_Action_PushLoanLender = "/api/v1.0/LMS/PushLoanLender";
        public const string LOS_Action_Disbursement = "/api/v1.0/LMS/Disbursement";
        public const string LOS_Action_GetListImage = "/api/v1.0/LMS/ListImage";
        public const string LOS_Action_ChangeDisbursement = "/api/v1.0/LMS/change_disbursement";
        public const string LOS_Action_SearchLoanCredit = "/api/v1.0/LMS/SearchLoanAll/";
        public const string LOS_Action_CanTopUp = "/api/v1.0/LMS/can_topup";
        public const string LOS_Action_GetWaitingMoneyDisbursementLender = "/api/v1.0/lms/waiting_money_disbursement_lender";
        public const string LOS_Action_UpdateVaAccount = "/api/v1.0/LMS/update_va_account";
        public const string LOS_Action_LosReportTime = "/api/v1.0/lms/get_disbursement_loan_by_date";
        public const string LOS_Action_DisbursementWaitingBlock = "/api/v1.0/lms/disbursement_waiting_block"; // ?LenderId=0 // all
        public const string LOS_Update_Phone_Other = "/api/v1.0/lms/update_phoneother";
        public const string LOS_Update_LinkFacebook = "/api/v1.0/thn/update_link_facebook";
        public const string LOS_Relative_Family = "/api/v1.0/LMS/get_relative_family";
        public const string LOS_Relationship = "/api/v1.0/LMS/relationship";
        public const string LOS_Property_Info = "/api/v1.0/LMS/get_property_info";
        public const string LOS_Property_LenderEsign = "/api/v1.0/esign/LenderEsignTest";

        public static bool IsProduct = false;
        public const string LOS_Action_GetLoanBriefDetail = "api/v1.0/lms/get_loan_brief_detail?loanBriefId={0}";
    }
}
