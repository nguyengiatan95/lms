﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LOSServices
{
    public class GetListImagesReq
    {
        public int TypeFile { get; set; }
        public long LoanBriefId { get; set; }
    }
}
