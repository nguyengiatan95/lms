﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Entites.Dtos.LOSServices
{
    public class Ward
    {
        public int WardId { get; set; }

        public String Name { get; set; }
    }
}
