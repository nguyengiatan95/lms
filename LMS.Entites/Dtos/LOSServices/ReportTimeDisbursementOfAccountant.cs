﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Entites.Dtos.LOSServices
{
    public class ReportTimeDisbursementOfAccountant
    {

        public Int64 STT { get; set; }

        public long TotalCount { get; set; }
        public string CustomerName { get; set; }

        public string AgencyName { get; set; }

        public int LoanCreditID { get; set; }

        public int CodeID { get; set; }

        public DateTime FirstTimeAccountantReceived { get; set; }

        public DateTime FirstTimeAccountantDisbursement { get; set; }

        public int TimeDisbursement { get; set; }

        public int AvgTimeDisbursement { get; set; }

        public string TimeDisbursementView { get; set; }

        public string TheAverageTime { get; set; }
    }
    public class LosReportTime
    {
        public string FullName { get; set; }
        public int LoanBriefId { get; set; }
        public int CodeId { get; set; }
        public string LenderName { get; set; }
        public DateTime ReceiveTime { get; set; }
        public DateTime DisbursementTime { get; set; }
        public int Diff { get; set; }
    }
}
