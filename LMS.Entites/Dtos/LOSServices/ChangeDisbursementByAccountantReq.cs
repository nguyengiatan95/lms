﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LOSServices
{
    public class ChangeDisbursementByAccountantReq
    {
        public long LoanbriefId { get; set; }
        /// <summary>
        ///  enum LOS_Enum_DisbursementBy
        /// </summary>
        public int DisbursementBy { get; set; }
        public string Note { get; set; }
        public string UserName { get; set; }
    }
}
