﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LOSServices
{
    public class CreateRelationshipReq
    {
        public long LoanbriefId { get; set; }
        public string Phone { get; set; }
        public string FullName { get; set; }
        public int TypeId { get; set; }
        public string UserName { get; set; }
    }
}
