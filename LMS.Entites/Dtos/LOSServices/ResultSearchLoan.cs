﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Entites.Dtos.LOSServices
{
    public class ResultSearchLoan
    {
        public LoanProduct LoanProduct { get; set; }

        public Province Province { get; set; }
        public District District { get; set; }
        public Ward Ward { get; set; }
        public UserHub UserHub { get; set; }
        public CoordinatorUser CoordinatorUser { get; set; }
        public BoundTelesale BoundTelesale { get; set; }
        public Hub Hub { get; set; }
        public int LoanBriefId { get; set; }
        public int ProductId { get; set; }
        public object CustomerId { get; set; }
        public object RateTypeId { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public object Dob { get; set; }
        public int Gender { get; set; }
        public object NationalCard { get; set; }
        public object NationalCardDate { get; set; }
        public object NationCardPlace { get; set; }
        public double LoanAmount { get; set; }
        public object LoanAmountFirst { get; set; }
        public int LoanTime { get; set; }
        public object FromDate { get; set; }
        public object ToDate { get; set; }
        public object ConsultantRate { get; set; }
        public object ServiceRate { get; set; }
        public object LoanRate { get; set; }
        public int ProvinceId { get; set; }
        public int DistrictId { get; set; }
        public int WardId { get; set; }
        public string UtmSource { get; set; }
        public object UtmMedium { get; set; }
        public object UtmCampaign { get; set; }
        public object UtmTerm { get; set; }
        public object UtmContent { get; set; }
        public object ScoreCredit { get; set; }
        public DateTime CreatedTime { get; set; }
        public object UpdatedTime { get; set; }
        public int Status { get; set; }

        public string StrStatus { get; set; }

        public object IsNeeded { get; set; }
        public object AffCode { get; set; }
        public int BoundTelesaleId { get; set; }
        public object AffStatus { get; set; }
        public object MecashId { get; set; }
        public object ReceivingMoneyType { get; set; }
        public object BankId { get; set; }
        public object BankAccountNumber { get; set; }
        public object BankCardNumber { get; set; }
        public object BankAccountName { get; set; }
        public object OrderSourceId { get; set; }
        public object OrderSourceParentId { get; set; }
        public object IsHeadOffice { get; set; }
        public object PlatformType { get; set; }
        public object CurrentPipelineId { get; set; }
        public object CurrentSectionId { get; set; }
        public object CurrentSectionDetailId { get; set; }
        public object PipelineState { get; set; }
        public object SectionApprove { get; set; }
        public object TypeLoanBrief { get; set; }
        public object Frequency { get; set; }
        public object IsCheckCic { get; set; }
        public bool IsLocate { get; set; }
        public bool IsTrackingLocation { get; set; }
        public object RefCodeLocation { get; set; }
        public object RefCodeStatus { get; set; }
        public object BuyInsurenceCustomer { get; set; }
        public object NumberCall { get; set; }
        public object CreateBy { get; set; }
        public object LoanPurpose { get; set; }
        public object LoanBriefComment { get; set; }
        public object ReasonCancel { get; set; }
        public object ReMarketingLoanBriefId { get; set; }
        public object IsReMarketing { get; set; }
        public object LoanBriefCancelAt { get; set; }
        public object LoanBriefCancelBy { get; set; }
        public object TelesalesPushAt { get; set; }
        public object ScheduleTime { get; set; }
        public object HubId { get; set; }
        public int HubEmployeeId { get; set; }
        public object HubPushAt { get; set; }
        public object EvaluationHomeUserId { get; set; }
        public object EvaluationHomeState { get; set; }
        public object EvaluationCompanyUserId { get; set; }
        public object EvaluationCompanyState { get; set; }
        public int CoordinatorUserId { get; set; }
        public object ActionState { get; set; }
        public object AutomaticProcessed { get; set; }
        public object LenderId { get; set; }
        public object CoordinatorPushAt { get; set; }
        public object IsUploadState { get; set; }
        public object DeviceId { get; set; }
        public object DeviceStatus { get; set; }
        public object LastSendRequestActive { get; set; }
        public object ResultCic { get; set; }
        public object ResultLoaction { get; set; }
        public object CheckCic { get; set; }
        public object CheckLoaction { get; set; }
        public object ContractGinno { get; set; }
        public bool IsEdit { get; set; }
        public bool IsScript { get; set; }
        public bool IsPush { get; set; }
        public bool IsBack { get; set; }
        public bool IsCancel { get; set; }
        public bool IsUpload { get; set; }
        public bool IsConfirmCancel { get; set; }
        public bool IsConfirmAndPush { get; set; }
        public int InProcess { get; set; }
        public object ResultRuleCheck { get; set; }
        public object ResultLocation { get; set; }
        public object countCall { get; set; }
        public object firstProcessingTime { get; set; }
        public object IsReborrow { get; set; }
        public int TypeHotLead { get; set; }
        public object DisbursementAt { get; set; }
    }
}
