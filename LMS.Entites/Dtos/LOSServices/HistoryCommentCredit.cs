﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Entites.Dtos.LOSServices
{
    public class HistoryCommentCredit
    {
        public DateTime CreateDate { get; set; }

        public string FullName { get; set; }

        public string Comment { get; set; }

        public string ShopName { get; set; }
        public int Id { get; set; }

        public int LoanCreditId { get; set; }

        public Byte IsDisplay { get; set; }

        public Int16 ActionId { get; set; }
        public int GroupUserId { get; set; }

        public int UserId { get; set; }
        /// <summary>
        /// Đường dẫn file ghi âm
        /// </summary>

        public string UrlImg { get; set; }

        public int S3Status { get; set; }

        /// <summary>
        /// Loại File ghi âm
        /// </summary>

        public int TypeFile { get; set; }
    }

    public class ReqGetHistoryComment
    {
        public int Page { get; set; }

        public int PageSize { get; set; }

        public int LoanCreditId { get; set; }
    }
    public class ReqSaveCommentHistory
    {
        public string Note { get; set; }
        public long LoanBriefId { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
    }
}
