﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LOSServices
{
    public class RelativeFamily
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsEnable { get; set; }
    }
}
