﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Entites.Dtos.LOSServices
{
    public class Province
    {
        public int ProvinceId { get; set; }

        public String Name { get; set; }
    }
}
