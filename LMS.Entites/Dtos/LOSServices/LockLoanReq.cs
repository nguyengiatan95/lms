﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LOSServices
{
    public class LockLoanReq
    {
        public long LoanId { get; set; }
        public int Type { get; set; }
        public string Note { get; set; }
        public string UserName { get; set; }
    }
}
