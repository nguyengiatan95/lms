﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LOSServices
{
    public class ChangePhoneLOSReq
    {
        public long LoanbriefId { get; set; }
        public string Phone { get; set; }
        public string Application { get; set; }
    }
}
