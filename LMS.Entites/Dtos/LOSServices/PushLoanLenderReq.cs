﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LOSServices
{
    public class PushLoanLenderReq
    {
        public long LoanId { get; set; }
        public long LenderId { get; set; }
        public int DisbursementBy { get; set; }
        public string LenderName { get; set; }
        public string UserName { get; set; }
        public string LenderFullName { get; set; }
        public string LenderNationalCard { get; set; }
    }
}
