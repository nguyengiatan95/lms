﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LOSServices
{
    public class UpdateLinkFacebookReq
    {
        public long LoanBriefId { get; set; }
        public string FacebookAddress { get; set; }
    }
}
