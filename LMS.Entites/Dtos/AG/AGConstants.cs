﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.AG
{
    public class AGConstants
    {
        public static string BaseUrl ="";
        public static string APIAG = "";
        public static string ChangePhone = "/LMS/ChangePhone";
        public static string HistoryCommentChip = "/LMS/HistoryCommentChip";
        public const string CloseLoanByExemption = "/LMS/CloseLoanByExemption";
        public const string CloseLoanFromRequestThirdParty = "/LMS/CloseLoanFromRequestThirdParty";
        public const string LMSCreateUser = "/LMS/CreateUser";
    }
}
