﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.AG
{
    public class HistoryCommentChipReq
    {
        public long LoanCreditIDOfPartner { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class HistoryCommentChip
    {
        public long RowID { get; set; }
        public string LoanCreditCode { get; set; }
        public string ContractCode { get; set; }
        public string CustomerName { get; set; }
        public string ShopName { get; set; }
        public DateTime ForDate { get; set; }
        public DateTime FromDate { get; set; }
        public int ChipStatus { get; set; }
        public string Street { get; set; }
        public string AddressHouseHoldAI { get; set; }
        public double? Distance { get; set; }
        public List<CommentData> ListComment { get; set; }
        public DateTime? LastUpdate { get; set; }
        public long TotalCount { get; set; }
        public long CountRightAddress { get; set; }
        public long CountErrorNetWork { get; set; }
        public long CountWrongAddress { get; set; }
        public HistoryCommentChip()
        {
            ListComment = new List<CommentData>();
        }
    }



    public class CommentData
    {
        public string ReasonName { get; set; }

        public DateTime? CreateDate { get; set; }

        public string Username { get; set; }

        public long LoanCreditID { get; set; }
        public long LogAppraisalChiplD { get; set; }
        public string Note { get; set; }
        public string JsonImage { get; set; }
        public List<ResponseUpload> ImageLos { get; set; }

    }
    public class ResponseUpload
    {
        public string filePath { get; set; }
        public string fileThumb { get; set; }
    }
}
