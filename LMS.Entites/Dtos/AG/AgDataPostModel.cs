﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.AG
{
    public class AgDataPostModel
    {
        /// <summary>
        /// {path}
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// body json
        /// </summary>
        public string DataPost { get; set; }
    }
}
