﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.AG
{
    public class AGResponse
    {
        public object Data { get; set; }
        public string Message { get; set; }
        public int Status { get; set; }
    }
}
