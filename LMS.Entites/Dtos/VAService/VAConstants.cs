﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.VAService
{
    public class VAConstants
    {
        public static string BaseUrl = "http://42.113.207.131:10002/ApiResf_VirtualAccount/services/registerVA";
        public static string MerchantCode = "VAP001";
        public const string PcodeRegister = "9000";
        public const string PcodeUpdate = "9001";
        public const string PcodeCannel = "9002";
        public const string PcodeCheckStatus = "9099";
        public const string Condition = "01";
        public static string Key3DES = "31feae316de0a42520ef5ec4";
        public const string BankCode = "WOORIBANK";
        public const int AddYear = 10;
        public const string ConvertStartDate = "yyyyMMdd000000";
        public const string ConvertEndDate = "yyyyMMdd235959";
        public const int Repeat = 5;
        public const string ResponseCodeSuccess = "00";
        
    }

}
