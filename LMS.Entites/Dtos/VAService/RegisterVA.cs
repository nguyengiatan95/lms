﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.VAService
{
    public class RegisterVA
    {
        public string pcode { get; set; }
        public string merchant_code { get; set; }
        public string data { get; set; }
    }
    public class RegisterInformation
    {
        public string map_id { get; set; }
        public int amount { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public string condition { get; set; }
        public string customer_name { get; set; }
        public string request_id { get; set; }
        public string bank_code { get; set; }
        public CustomerRegisterVa extend { get; set; }
    }
    public class CustomerRegisterVa
    {
        public string phone { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public string id { get; set; }
    }
    public class VAResponse
    {
        [JsonProperty("response_code")]
        public string ResponseCode { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("account_no")]
        public string AccountNo { get; set; }
        [JsonProperty("account_name")]
        public string AccountName { get; set; }
        [JsonProperty("bank_code")]
        public string BankCode { get; set; }
        [JsonProperty("bank_name")]
        public string BankName { get; set; }
        [JsonProperty("map_id")]
        public string MapID { get; set; }
    }
}
