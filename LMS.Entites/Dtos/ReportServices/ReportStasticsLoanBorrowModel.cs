﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.ReportServices
{
    public class ReportStasticsLoanBorrowModel
    {
        public long ShopID { get; set; }

        public long DebtMoneyBorrow { get; set; }

        public int TotalLoanBorrow { get; set; }

        public long OverDebtMoneyBorrow { get; set; }

        public int TotalLoanBorrowOverDebt { get; set; }
    }
}
