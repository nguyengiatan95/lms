﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.Document
{
    public class NotifyVoluntarilyHandingOverPropertyeItem //thông báo tự nguyện bàn giao tài sản
    {
        public string EditCVTB { get; set; }
        public string Edit_CustomerName { get; set; }
        public string Edit_NumberCard { get; set; }
        public string Edit_Address { get; set; }
        public string Edit_FromDate { get; set; }
        public string Edit_TotalMoneyNeedCloseLoan { get; set; }
        public string Edit_DateCloseLoan { get; set; }
        public string Edit_TotalMoneyCurrent { get; set; }
        public string Edit_TotalMoneyIntersetFee { get; set; }
        public int Edit_DPD { get; set; }
        public string Edit_NameEmployees { get; set; }
        public string Edit_PhoneEmployees { get; set; }
        public string FileName { get; set; }
    }
}
