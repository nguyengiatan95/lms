﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.Document
{
    public class NotifyPropertySeizureItem // thông báo thu giữ tài sản
    {
        public string EditCVTB { get;set;}
        public string Edit_CustomerName { get; set; }
        public string Edit_NumberCard { get; set; }
        public string Edit_Address { get; set; }

        public string Edit_Product { get; set; }
        public string Edit_Engine { get; set; }
        public string Edit_Chassis { get; set; }
        public string Edit_FromDate { get; set; }
        public string Edit_NameEmployees { get; set; }
        public string Edit_PhoneEmployees { get; set; }
        public string FileName { get; set; }
    }
}
