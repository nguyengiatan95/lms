﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.Document
{
    public class DebtRestructuringItem //tái cấu trúc nợ
    {
        public string CustomerName { get; set; }
        public string NumberCard { get; set; }
        public string LoanCodeId { get; set; }
        public string TotalMoney { get; set; }
        public string TotalMoneyCurrent { get; set; }
        public string TotalMoneyInterest { get; set; }
        public string TotalMoneyCloseLoan { get; set; }

        public string BirthDay { get; set; }
        public string DebitGroup { get; set; }
        public int DayPastDue { get; set; }
        public string TotalMoneyConsultant { get; set; }
        public string TotalMoneyFineLate { get; set; }
        public string TotalMoneyFineOriginal { get; set; }
        public string FileName { get; set; }
    }
}
