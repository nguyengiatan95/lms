﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.Document
{
    public class PropertySeizureItem //Quyết định thu giữ taì sản
    {
        public string EditCVTB { get; set; }
        public string Edit_CustomerName { get; set; }
        public string Edit_FromDate { get; set; } //Căn cứ Hợp đồng gửi giữ tài sản ký  ngay
        public string Edit_Product { get; set; }
        public string Edit_PlateNumber { get; set; }
        public string Edit_NumberDayKeep { get; set; }//Thời gian thu giữ
        public string FileName { get;set; }
    }
}
