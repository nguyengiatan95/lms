﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.Document
{
    public  class UnsecuredFinalDebtNoticeItem//thông báo nợ lần cuối tín chấp
    {
        public string EditCVTB { get; set; }
        public string Edit_CustomerName { get; set; }
        public string Edit_NumberCard { get; set; }
        public string Edit_Address { get; set; }
        public string Edit_LoanCode { get; set; }
        public string Edit_TotalMoneyNeedCloseLoan { get; set; }
        public string Edit_DateCloseLoan { get; set; }
        public string Edit_TotalMoneyCurrent { get; set; }
        public string Edit_TotalMoneyIntersetFee { get; set; }
        public int Edit_DPD { get; set; }
        public string Edit_DateFinishCloseLoan { get; set; }
        public string Edit_NameEmployees { get; set; }
        public string Edit_PhoneEmployees { get; set; }
        public string FileName { get; set; }
    }
}
