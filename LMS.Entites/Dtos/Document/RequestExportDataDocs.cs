﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.Document
{
    public class RequestExportDataDocs
    {
        public long LoanID { get; set; }
        public int TypeDoc { get; set; }
        public string CVTB { get; set; }
        public string DateCloseLoan { get; set; }
        public string DateFinshCloseLoan { get; set; }
        public string NumberDayKeep { get; set; }
        public string NameEmployess { get; set; }
        public string PhoneEmployees { get; set; }
        public long UserID { get; set; }
        public string ContractCode{ get; set; }
    }
}
