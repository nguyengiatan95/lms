﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.Menu
{
    public class JSTreeModel
    {
        [JsonProperty("id")]
        public string ID { get; set; }
        [JsonProperty("text")]
        public string Text { get; set; }
        [JsonProperty("state")]
        public State State { get; set; }
        [JsonProperty("isMenu")]
        public string IsMenu { get; set; }
        [JsonProperty("controllerName")]
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        [JsonProperty("children")]
        public List<JSTreeModel> Children { get; set; }

        [JsonProperty("link")]
        public string Link
        {
            get
            {
                string link = "/";
                if (!string.IsNullOrEmpty(ControllerName))
                {
                    link += ControllerName.Replace(@"/", "");
                }
                if (!string.IsNullOrEmpty(ActionName))
                {
                    link += $"/{ActionName.Replace("/", "")}";
                }
                return link;
            }
        }

        [JsonProperty("parentId")]
        public string ParentID { get; set; }
        [JsonProperty("icon")]
        public string Icon { get; set; }
    }

    public class State
    {
        [JsonProperty("opened")]
        public bool Opened { get; set; }

        [JsonProperty("disabled")]
        public bool Disabled { get; set; }
        [JsonProperty("selected")]
        public bool Selected { get; set; }
    }
}
