﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.InvoiceService
{
    public class HistoryCommentInvoiceView
    {
        public long LogInvoiceID { get; set; }
        public string Note { get; set; }
        public string UserName { get; set; }
        public DateTime CreateDate { get; set; }
        public string ImageUrl { get; set; }
        public string StatusName { get; set; }
        public int Status { get; set; }
    }
}
