﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.InvoiceService
{
    public class TicketListView
    {
        public long TicketID { get; set; }
        public string Note { get; set; }

        public long? FromUserID { get; set; }

        public long? FromDepartmentID { get; set; }

        public long? ToDepartmentID { get; set; }

        public long? ToUserID { get; set; }

        public int? Type { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? Status { get; set; }

        public string JsonExtra { get; set; }

        public long? ModifyUserID { get; set; }

        public DateTime? ModifyDate { get; set; }
        public string TypeName { get; set; }
        public string FromDepartment { get; set; }
        public string ToDepartment { get; set; }
        public string FromUserName { get; set; }
        public string ToUserName { get; set; }

    }
}
