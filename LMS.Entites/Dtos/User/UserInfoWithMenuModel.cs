﻿using LMS.Entites.Dtos.Menu;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.User
{
    public class UserInfoWithMenuModel
    {
        public long UserID { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        /// <summary>
        /// tạm thời gán mặc định = 3703
        /// </summary>
        public long UsershopID { get; set; }
        public List<JSTreeModel> LstMenu { get; set; }
        public int UserType { get; set; }
    }
}
