﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.Common
{
    public class PropertiesChange
    {
        public string Name { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
    }
}
