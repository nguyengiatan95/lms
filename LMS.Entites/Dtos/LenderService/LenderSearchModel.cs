﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LenderService
{
    public class LenderSearchModel
    {
        public long ID { get; set; }

        public string FullName { get; set; }
    }
}
