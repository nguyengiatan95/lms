﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LenderService
{
    public class LenderDetailMoneyLoan
    {
        public long TotalMoney { get; set; }
        public long TotalMoneyInterest { get; set; }
        public long LoanID { get; set; }
        public long CodeID { get; set; }
        public int Status
        {
            get;set;
        }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
