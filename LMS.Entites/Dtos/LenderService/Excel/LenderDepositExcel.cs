﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace LMS.Entites.Dtos.LenderService.Excel
{
    [Description("danh sách đặt cọc")]
    public class LenderDepositExcel
    {
        [Description("Mã Nhà Đầu Tư")]
        public string LenderCode { get; set; }
        [Description("Tên khách hàng")]
        public string CustomerName { get; set; }
        [Description("LoanId")]
        public long LoanID { get; set; }
        [Description("Mã TC")]
        public string ContractCode { get; set; }
        [Description("Ngày giải ngân")]
        public DateTime FromDate { get; set; }
        [Description("Ngày Đáo Hạn")]
        public DateTime ToDate { get; set; }
        [Description("Số tiền giải Ngân")]
        public long TotalMoney { get; set; }
        [Description("Lãi cọc")]
        public long InterestMoney { get; set; }
        [Description("Gốc cọc")]
        public long TotalMoneyCurrent { get; set; }
        [Description("Tổng tiền cọc")]
        public long TotalMoneyDeposit { get; set; }
        [Description("Trạng thái")]
        public string StatusName { get; set; }
        [Description("Ngày phát sinh đặt cọc")]
        public DateTime CreateDate { get; set; }
        [Description("Ngày phải trả nợ")]
        public DateTime NextDate { get; set; }
        [Description("Tên nhà đầu tư")]
        public string LenderName { get; set; }
        [Description("Số TK nhận tiền")]
        public string AccountBanking { get; set; }
        [Description("Ngân hàng")]
        public string BankName { get; set; }
        [Description("Lãi Cọc sau thuế")]
        public long InterestAfterTax { get; set; }
        [Description("Số tiền trả cho lender")]
        public long TotalMoneyDepositAfterTax { get; set; }
        [Description("Thuế")]
        public long Tax { get; set; }
    }

    public class LenderDepositView
    {
        public long PaymentScheduleDepositMoneyID { get; set; }

        public long LoanID { get; set; }

        public long LenderID { get; set; }

        public long CustomerID { get; set; }

        public string CustomerName { get; set; }

        public string LenderCode { get; set; }

        public string ContractCode { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public long TotalMoney { get; set; }

        public long TotalMoneyCurrent { get; set; }

        public long InterestMoney { get; set; }

        public long TotalMoneyDeposit { get; set; }


        public int Status { get; set; }

        public DateTime CreateDate { get; set; }
        public string StatusName { get; set; }
        public long TotalCount { get; set; }
        public DateTime NextDate { get; set; }
        public string LenderName { get; set; }
        public string AccountBanking { get; set; }
        public string BankName { get; set; }
        public long InterestAfterTax { get; set; }
        public long TotalMoneyDepositAfterTax { get; set; }
        public long Tax { get; set; }
    }

    public class BM01Excel
    {
        public List<DepositData> LstDeposit { get; set; }
        public List<LenderInfo> LenderInfos { get; set; }
    }
    public class DepositData
    {
        [Description("Mã Nhà Đầu Tư")]
        public string LenderCode { get; set; }
        [Description("Tên khách hàng")]
        public string CustomerName { get; set; }
        [Description("LoanId")]
        public long LoanID { get; set; }
        [Description("Mã TC")]
        public string ContractCode { get; set; }
        [Description("Ngày giải ngân")]
        public DateTime FromDate { get; set; }
        [Description("Ngày Đáo Hạn")]
        public DateTime ToDate { get; set; }
        [Description("Số tiền giải Ngân")]
        public long TotalMoney { get; set; }
        [Description("Lãi cọc")]
        public long InterestMoney { get; set; }
        [Description("Gốc cọc")]
        public long TotalMoneyCurrent { get; set; }
        [Description("Tổng tiền cọc")]
        public long TotalMoneyDeposit { get; set; }
        [Description("Trạng thái")]
        public string StatusName { get; set; }
        //[Description("Ngày phải trả nợ")]
        //public DateTime NextDate { get; set; }
        [Description("Ngày phát sinh đặt cọc")]
        public DateTime CreateDate { get; set; }
        public DateTime LastPaySchedule { get; set; }
        public DateTime EndDaySchedule { get; set; }
        public int CountDay { get; set; }
        public long LenderID { get; set; }
        public long InterestAfterTax { get; set; }
        public long TotalMoneyDepositAfterTax { get; set; }
        public DateTime NextDate { get; set; }



    }

    public class LenderInfo
    {
        public string LenderCode { get; set; }
        public string FullName { get; set; }
        public long TotalMoney { get; set; }
        public long LenderID { get; set; }
        public string Represent { get; set; }
        public string NumberBankCard { get; set; }
        public string BankCode { get; set; }
    }

}
