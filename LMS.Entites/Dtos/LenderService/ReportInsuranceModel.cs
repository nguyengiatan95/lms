﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LenderService
{
    public class ReportInsuranceModel
    {
        public long LenderID { get; set; }
        public string FullName { get; set; }
        public string NumberCard { get; set; }
        public string Represent { get; set; }
        public string CustomerName { get; set; }
        public string InsuranceCode { get; set; }
        public long TotalMoneyDisbursement { get; set; }
        public long TotalMoneyCurrent { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public long TotalInterest { get; set; }
        public string SourceByInsurance { get; set; }
        public string LinkGNCLender { get; set; }
        public int StatusInsurance { get; set; }
        public string StrStatusInsurance { get; set; }
        public long LoanID { get; set; }
        public string ContractCode { get; set; }
        public DateTime BirthDay { get; set; }
        public string Address { get; set; }
        public long LoanTime { get; set; }
        public long TotalInsuranceMoney { get; set; }
        public string LinkKH { get; set; }
    }
}
