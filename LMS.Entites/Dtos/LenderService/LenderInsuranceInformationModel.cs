﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LenderService
{
    public class LenderInsuranceInformationModel
    {
        public string CustomerFullname { get; set; }
        public string CustomerIDCard { get; set; }
        public string CustomerAddress { get; set; }
        public string InsuranceNumber { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public string InvestorFullname { get; set; }
        public string InvestorDateOfBirth { get; set; }
        public string InvestorIDCard { get; set; }
        public string InvestorAddress { get; set; }
        public string InvestorPhone { get; set; }
        public string InvestorEmail { get; set; }
        public long TotalMoneyCurrent { get; set; } 
        public long TotalMoneyDisburement { get; set; } 
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public long InterestMoney { get; set; }
    }
}
