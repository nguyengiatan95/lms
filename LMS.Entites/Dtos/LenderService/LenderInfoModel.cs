﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LenderService
{
    public class LenderInfoModel
    {
        public long LenderID { get; set; }
        public string FullName { get; set; }
        public string NumberCard { get; set; }
        public DateTime? BirthDay { get; set; }
        public int? Gender { get; set; }
        public string Phone { get; set; }
        public string TaxCode { get; set; }
        public int? Status { get; set; }
        public string Address { get; set; }
        public decimal RateInterest { get; set; }
        public long TotalMoney { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public int IsVerified { get; set; }
        public long TotalCount { get; set; }

        public string InviteCode { get; set; }

        public decimal? RateLender { get; set; }

        public decimal? RateAff { get; set; }

        public string Represent { get; set; }

        public string AddressOfResidence { get; set; }

        public DateTime? CardDate { get; set; }

        public string CardPlace { get; set; }

        public string Email { get; set; }

        public string ContractCode { get; set; }

        public DateTime? ContractDate { get; set; }

        public string AccountBanking { get; set; }

        public long? BankID { get; set; }

        public long? LenderCareUserID { get; set; }

        public long? LenderTakeCareUserID { get; set; }

        public long? AffID { get; set; }

        public string RepresentPhone { get; set; }

        public string BranchOfBank { get; set; }
        public string InvitePhone { get; set; }
    }
}
