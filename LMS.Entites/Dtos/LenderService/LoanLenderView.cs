﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LenderService
{
    public class LoanLenderView
    {
        public long SumTotalMoneyDisbursement { get; set; }
        public long SumTotalMoney { get; set; }
        public long TotalLoan { get; set; }
        public long SumMoneyInterestExpected { get; set; }
        public long SumMoneyInterestEarned { get; set; }
        public List<LoanDetailView> LstLoan { get; set; }
        public LoanLenderView()
        {
            LstLoan = new List<LoanDetailView>();
        }
    }
    public class LoanDetailView
    {
        public long LoanCreditID { get; set; }
        public string LoanCreditCode { get; set; }
        public long LoanCode { get; set; }
        public string CustomerName { get; set; }
        public long LoanTotalMoneyCurrent { get; set; }
        public long MoneyInterestExpected { get; set; }
        public long MoneyInterestEarned { get; set; }
        public int LoanStatus { get; set; }
        public string LoanStatusName { get; set; }
        public string LoanDate { get; set; }
        public long LoanMoneyDisbursement { get; set; }

    }
}
