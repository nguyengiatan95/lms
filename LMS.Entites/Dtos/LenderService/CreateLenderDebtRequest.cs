﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LenderService
{
    public class CreateLenderDebtRequest
    {
        public long LenderID { get; set; }
        public int DebtType { get; set; }
        public long TotalMoney { get; set; }
        public long CreateBy { get; set; }
        public string Description { get; set; }
        public long ReferID { get; set; }
    }
}
