﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.CustomerService
{
    public class CustomerInfoModel
    {
        public long CustomerID { get; set; }

        public string FullName { get; set; }

        public string Phone { get; set; }

        public string NumberCard { get; set; }

        public DateTime? BirthDay { get; set; }

        public int Gender { get; set; }

        public DateTime CreateDate { get; set; }

        public int CityID { get; set; }

        public int DistrictID { get; set; }

        public int WardID { get; set; }

        public string Address { get; set; }

        public string AddressHouseHold { get; set; }

        public long TotalMoney { get; set; }

        public int IsBorrowing { get; set; }

        public DateTime ModifyDate { get; set; }

        public long TotalMoneyAccounting { get; set; }

        public string PermanentAddress { get; set; }
    }
}
