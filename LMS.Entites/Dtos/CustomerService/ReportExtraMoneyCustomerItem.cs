﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.CustomerService
{
    public class ReportExtraMoneyCustomerItem
    {
        public long CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string NumberCard { get; set; }
        public long TotalMoney { get; set; }
        public int TotalCount { get; set; }
    }

    public class ReportExtraMoneyCustomerReq
    {
        public string DateSearch { get; set; }
        public string QuerySearch { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
