﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.PaymentServices
{
    public class ListPaymentMoneyByLoan
    {
        public long LoanID { get; set; }
        public long TotalMoneyNeed { get; set; }
        public DateTime PayDate { get; set; }
        public long OriginalNeed { get; set; }
        public long InterestNeed { get; set; }
        public long ServiceNeed { get; set; }
        public long FineLateNeed { get; set; }
        public long FineInterestNeed { get; set; }
        public long ConsultantNeed { get; set; }
    }
}
