﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.PaymentServices
{
    public class ConstantsPayment
    {
        public const decimal RateInterest = 2700;
        public const decimal RateInterestLender = 493;
        public const int LoanTime = 12;
        public const int Frequency = 1;
    }
}
