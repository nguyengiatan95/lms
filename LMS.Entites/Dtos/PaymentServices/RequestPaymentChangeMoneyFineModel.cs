﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.PaymentServices
{
    public class RequestPaymentChangeMoneyFineModel
    {
        public long PaymentScheduleID { get; set; }
        public long MoneyAdd { get; set; }
        public bool IsFullPeriod { get; set; } // đã ghi nhận đủ tiền phạt kỳ
    }
}
