﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.Sms
{
    public class SmsItemViewModel
    {
        public long SmsAnalyticsID { get; set; }
        public string Sender { get; set; }
        public string SmsContent { get; set; }
        public string LoanCode { get; set; }
        public long LoanID { get; set; }
        public long ShopID { get; set; }
        public string ShopName { get; set; }
        public long CustomerID { get; set; }
        public string CustomerName { get; set; }
        public long IncreaseMoney { get; set; }
        public string SmsStatusName { get; set; }
        public DateTime SmsreceivedDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public long BankCardID { get; set; }
        public string BankCardAliasName { get; set; }

    }
}
