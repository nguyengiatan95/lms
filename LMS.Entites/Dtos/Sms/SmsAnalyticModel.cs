﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.Sms
{
    public class SmsContentModel
    {
        public string Sender { get; set; }
        public string SmsContent { get; set; }
        public int SmsType { get; set; }
    }
    public class SmsAnalyticModel
    {
        public string Sender { get; set; }
        public string Content { get; set; }
        public string AccountNumber { get; set; }
        public long IncreaseMoney { get; set; }
        public DateTime SmsreceivedDate { get; set; }
        public string Phone { get; set; }
        public string IdCard { get; set; }
        public int SmsType { get; set; }

        /// <summary>
        /// 1: +
        /// -1: -
        /// 0: ko xác định
        /// </summary>
        public int HasSign { get; set; }
        public long BankCardID { get; set; }
        // ví điện tử xác nhận dc CustomerID như MOMO, GPay...
        public long CustomerID { get; set; }
    }
}
