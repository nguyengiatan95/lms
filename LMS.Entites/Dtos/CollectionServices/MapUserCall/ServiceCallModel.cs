﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.CollectionServices.MapUserCall
{
    public class ServiceCallModel
    {
        public int TypeCall { get; set; }
        public string CareSoftLinkCall { get; set; }
        public string CiscoUserName { get; set; }
        public string CiscoPassword { get; set; }
        public string CiscoExtension { get; set; }
        public string IpPhone { get; set; }
    }
}
