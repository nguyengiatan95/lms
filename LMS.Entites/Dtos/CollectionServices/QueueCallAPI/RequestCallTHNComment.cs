﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.CollectionServices.QueueCallAPI
{
    public class RequestCallTHNComment
    {
        public long AGLoanID { get; set; }
        public string Comment { get; set; }
        public int ReasonID { get; set; }
        public long LMSCommentID { get; set; }
        public long UserID { get; set; }
        public string Username { get; set; }
        public string CreateDate { get; set; }
    }

    public class RequestSaveProvisionsCollectionTHN
    {
        public long AGLoanID { get; set; }

        public string Username { get; set; }

        public long UserID { get; set; }

        public string CreateDate { get; set; }

        public long Money { get; set; }

        public string TimeScheduled { get; set; }

        public int NumberAccruedPeriods { get; set; }

        public long LMSID { get; set; }
        public string TextAccruedPeriods { get; set; }
    }

    public class LMSRequestConfirmSuspendedMoneyTHN
    {
        public long InvoiceTimaID { get; set; }
        public long UserID { get; set; }
        public string CreateDate { get; set; }
        public string Note { get; set; }
        public long LmsConfirmID { get; set; }
        public List<string> PathImage { get; set; }
        public long LoanID { get; set; }
        public long CustomerID { get; set; }

    }
}
