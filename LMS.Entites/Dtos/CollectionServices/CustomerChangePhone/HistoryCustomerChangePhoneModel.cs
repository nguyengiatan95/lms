﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.CollectionServices.CustomerChangePhone
{
    public class HistoryCustomerChangePhoneModel
    {
        public string CreateByName { get; set; }
        public string OldPhone { get; set; }
        public string NewPhone { get; set; }
        public string Note { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
