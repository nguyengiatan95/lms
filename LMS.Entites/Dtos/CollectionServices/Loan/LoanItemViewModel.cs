﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.CollectionServices.Loan
{
    public class LoanItemViewModel
    {
        public long LoanID { get; set; }
        public string ContactCode { get; set; }
        public int LoanInsuranceType { get; set; }
        public string LoanInsuranceTypeName { get; set; }
        public int CountDownNextdate { get; set; }
        public List<EmployeeHandleLoan> Employees { get; set; }
        public int LoanPaymentStatus { get; set; }
        public string LoanPaymentStatusName { get; set; }
        public DateTime? LatestDateAction { get; set; }
        public int CustomerPayTypeID { get; set; }
        public string CustomerPayTypeName { get; set; }
        public string CustomerName { get; set; }
        public long LoanTotalMoneyCurrent { get; set; }
        public long CustomerTotalMoney { get; set; }
        public string ProductName { get; set; }
        public string CustomerPhone { get; set; }
        public DateTime NextDate { get; set; }
        public long CustomerTotalMoneyHold { get; set; }
        public string CommentReasCode { get; set; }

        // thêm cho app
        public string CustomerAddress { get; set; }
        public DateTime AlarmDate { get; set; }
        public long LoanBriefID { get; set; }

        public int NumberLoanCustomerHas { get; set; }
        public long TimaLoanID { get; set; }
        public string TypeName { get; set; } // giỏ nào
        public int TotalRows { get; set; }
    }
    public class EmployeeHandleLoan
    {
        public long UserID { get; set; }
        public string UserName { get; set; }
    }

    public class LoanViewNew
    {
        public long LoanID { get; set; }
        public string ContractCode { get; set; }
        public string CustomerName { get; set; }
        public long TotalMoneyCurrent { get; set; }
        public int OverdueDays { get; set; }
        public bool Location { get; set; }
        public string LoanBriefPropertyPlateNumber { get; set; }
        public string HouseholdAddress { get; set; }
        public string PresentAddress { get; set; }
        public bool Handling { get; set; }
        public int PlanLoanHandle { get; set; }
        public int AssignmentLoanType { get; set; }
        public string ReasonCode { get; set; }

        public List<int> LstAssignmentLoanType { get; set; }
        public string LoanStatusInsuranceName { get; set; }
        public long TotalMoneyNeedPay { get; set; }
    }
}
