﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.CollectionServices.Loan
{
    public class MotorcycleSeizureModel
    {
        public long LoanID { get; set; }
        public long CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string ContractCode { get; set; }
        public string Brand { get; set; }// hãng xe
        public string Product { get; set; }// tên xe
        public int YearMade { get; set; }//đời xe
        public string PlateNumber { get; set; }// biển kiểm soát xe máy
        public string PlateNumberCar { get; set; } // biển kiểm soát oto
        public string Chassis { get; set; }// số khung
        public string Engine { get; set; } // số máy 
        public string Address { get; set; }
    }
}
