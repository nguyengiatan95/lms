﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.CollectionServices.Loan
{
    public class PaymentScheduleModel
    {
        public long LoanID { get; set; }
        public DateTime PayDate { get; set; }
        public long MoneyOriginal { get; set; }
        public long MoneyInterest { get; set; }
        public long MoneyFee { get; set; }
        public long MoneyFineLate { get; set; }
        public long TotalMoneyNeedPay { get; set; }
        public long TotalMoneyPaid { get; set; }
        public int IsComplete { get; set; } // nhảy kỳ là hoàn thành tương đương với Isvisible bảng paymentschedule
    }
}
