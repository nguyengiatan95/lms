﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.CollectionServices.PlanHandleLoanDaily
{

    public class AppPlanDaily
    {
        public long DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public List<TeamleadPlan> TeamleadPlans { get; set; }
        public AppPlanDaily()
        {
            TeamleadPlans = new List<TeamleadPlan>();
        }

    }

    public class TeamleadPlan
    {
        public long LeadID { get; set; }
        public string LeadName { get; set; }
        public List<StaffPlan> StaffPlans { get; set; }
        public TeamleadPlan()
        {
            StaffPlans = new List<StaffPlan>();
        }
        public long DepartmentID { get; set; } // phòng ban tương ứng với trưởng nhóm
    }
    public class StaffPlan
    {
        public long StaffID { get; set; }
        public string StaffName { get; set; }
        public long TotalRegister { get; set; }
        public long TotalDone { get; set; }
    }
    public class TeamLeadAppTHN
    {
        public long ID { get; set; }
        public string Name { get; set; }
    }
}
