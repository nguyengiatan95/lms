﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.CollectionServices.ReportExcel
{
    public class ReportEmployeeHandleLoanDailyDetail
    {
        public long EmployeeID { get; set; } // userId
        public string EmployeeName { get; set; } // username
        public int TotalLoanHandle { get; set; } // tổng hd xử lý
        public int NumberLoanHandleDone { get; set; } // số hd đã xử lý
        //public int NumberCustomerTopUp { get; set; } // số kh đã nộp tiền
        //public int NumberLoanNeedCollection { get; set; }  // số hd cần thu
        public int NumberComment { get; set; } // số lần comment

        public int SumTotalLoanHandle { get; set; }
        public int SumNumberLoanHandleDone { get; set; }
        //public int SumNumberCustomerTopUp { get; set; }
        //public int SumNumberLoanNeedCollection { get; set; }
        public int SumNumberComment { get; set; }
    }

    public class ReportLoanEmployeeHandleDailyDetail
    {
        public long LoanID { get; set; } // id loan
        public string LoanContractCode { get; set; } // mã hd
        public int CountDPD { get; set; } // số ngày trễ
        public string LoanProductName { get; set; } // tên gói vay
        public long OldDebitMoney { get; set; } // nợ cũ
        public long MoneyOriginal { get; set; } // tiền gốc
        public long MoneyInterest { get; set; } // Tiền lãi
        public long MoneyService { get; set; } // tiền dịch vụ
        public long MoneyConsultant { get; set; } // tiền tư vấn
        public long MoneyFineLate { get; set; } // tiền phạt

        public long TotalMoneyNeedPay { get; set; } // tổng tiền phải trả
        public long TotalInterest { get { return this.MoneyInterest + this.MoneyService + this.MoneyConsultant; } } // tiền lãi + phí
        public long CustomerTotalMoney { get; set; } // tiền KH đang có
        public string CustomerName { get; set; }

        public long SumTotalInterest { get; set; } // tổng cộng lãi + phí
        public long SumMoneyOriginal { get; set; } // tổng cộng gốc
        public long SumMoneyFineLate { get; set; } // tổng cộng phí phạt
        public long SumTotalMoneyNeedPay { get; set; } // tổng cộng tiền phải trả
        public long SumCustomerTotalMoney { get; set; } // tổng cộng tiền kh đang có
        public long SumOldDebitMoney { get; set; } // tổng cộng nợ cũ

    }
}
