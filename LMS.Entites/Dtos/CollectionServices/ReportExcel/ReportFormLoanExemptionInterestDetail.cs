﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.CollectionServices.ReportExcel
{
    public class ReportFormLoanExemptionInterestDetail
    {
        public string CustomerName { get; set; }
        public string LoanContractCode { get; set; }
        public DateTime LoanFromDate { get; set; }
        public DateTime LoanToDate { get; set; }
        public int CountDPD { get; set; }
        public int BadDebtYear { get; set; }
        public string LoanProductName { get; set; }
        public long LoanTotalMoneyDisbrusement { get; set; } // số tiền giải ngân
        public long LoanTotalMoneyReceived { get; set; } // số tiền đã thu được
        public FormLoanExemptionInterestTypeMoney MoneyCloseLoan { get; set; } // số tiền được tính từ đơn vay
        public FormLoanExemptionInterestTypeMoney MoneyExpected { get; set; } // số tiền dự kiến thu
        public FormLoanExemptionInterestTypeMoney MoneySuggetExemption { get; set; } // số tiền đề xuất miễn giảm
        public FormLoanExemptionInterestPercent MoneyPercent { get; set; } // phần trăm miễn giảm

        public DateTime CutOffDate { get; set; }
        public DateTime CreateDate { get; set; }
        public string Note { get; set; } // lý do miễn giảm
        public string DepartmentName { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string ApprovedByName { get; set; }

        public long LoanID { get; set; }
        public long TimaLoanID { get; set; }
        public long LoanTotalMoneyCurrent { get; set; }
        public long CustomerID { get; set; }
    }

    public class FormLoanExemptionInterestTypeMoney
    {
        public long TotalMoneyNeedPay { get; set; }
        public long MoneyOriginal { get; set; }
        public long MoneyInterest { get; set; }
        public long MoneyConsultant { get; set; }
        public long MoneyService { get; set; }
        public long MoneyFineLate { get; set; }
        public long MoneyFineOriginal { get; set; }
        public long MoneyOldDebit { get; set; } // tiền nợ cũ
        public long TotalFee { get { return this.MoneyConsultant + this.MoneyService + this.MoneyFineLate + this.MoneyFineOriginal; } }
    }

    public class FormLoanExemptionInterestPercent
    {
        public decimal MoneyOriginal { get; set; }
        public decimal MoneyInterest { get; set; }
        public decimal MoneyConsultant { get; set; }
        public decimal MoneyService { get; set; }
        public decimal MoneyFineLate { get; set; }
        public decimal MoneyFineOriginal { get; set; }
        public decimal MoneyOldDebit { get; set; }
    }
}
