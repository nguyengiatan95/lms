﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.CollectionServices.ReportExcel
{
    public class AssignLoanEmployeeDetail
    {
        public long LoanID { get; set; } // loanID x
        public string LoanContactCode { get; set; } // mã TC x
        public string CustomerName { get; set; }// tên KH x
        public string CustomerDistrictName { get; set; } // quận huyện tạm trú  x
        public string CustomerCityName { get; set; } // x
        public long POSBOM { get; set; } // dư nợ gốc so với tháng trước   x
        public int DPDBOM { get; set; } // số ngày DPD so với tháng trước     x
        public string AssgineeNameFirst { get; set; } // tên nhân viên 1  x
        public string AssigneeNameSecond { get; set; } // tên nhân viên 2  x
        public string AssigneeNameLeader { get; set; } // trưởng bộ phận  x
        public string AssigneeNameDepartment { get; set; } // trưởng phòng   x

        public long LoanTotalMoneyReceipt { get; set; } // tổng tiền thu được       x

        public int DPDCurrent { get; set; } // dpd hiện tại   x
        public long POSCurrent { get; set; } // dư nợ gốc hiện tại   x
        public string BucketBOM { get; set; } // nhóm nợ của đơn vay tháng trước
        public string BucketCurrent { get; set; } // nhóm nợ hiện tại       x

        public string LoanProductName { get; set; }// mã sp     x
        public string LoanLatestComment { get; set; } //ngày tác nghiệp gần nhất        x
        public string AssigneeNameLatestComment { get; set; } // ng tác nghiệp      x
        public string CommentReasonCode { get; set; } // mã tác nghiệp      x
        public string CommentContent { get; set; } // nội dung      x

        public int LoanBadDebtYear { get; set; } // năm nợ xấu
        public string LoanInsuranceStatusName { get; set; } // trạng thái bảo hiểm      x
        public string LoanInsurancePartnerName { get; set; } // đối tượng đền bù bảo hiểm       x
        public string LoanDomainName { get; set; } // miền      x
        public long CustomerID { get; set; } // mã KH       x

        public string LoanFromDate { get; set; } // ngày giải ngân      x
        public string LoanToDate { get; set; } // kết thúc dự kiến      x
        public string LoanFinishedDate { get; set; } // kêt thúc thực tế        x
        public string CustomerBirthDate { get; set; } // ngày sinh kh       x
        public string CustomerAddress { get; set; } // địa chỉ kh       x

        public long LoanTotalMoneyCurrent { get; set; } // dư nợ giải ngân      x
        public string LoanRateTypeName { get; set; } // hình thức thanh toán        x
        public string LoanNextDate { get; set; } // ngày đến hạn        x
        public int LoanFequency { get; set; } // chu kỳ thanh toán      x
        public long LoanInterestClosed { get; set; } //  tiền lãi đóng hợp đồng     x
        public long LoanConsultantFeeClosed { get; set; } // tiền tư vấn đóng hợp đồng      x
        public long LoanMoneyFineLateClosed { get; set; } // phí phạt trả châm, nợ cũ, phí phạt treo        x
        public long LoanMoneyFineOriginalClosed { get; set; } // phí tất toán trước hạn     x
        public long CustomerTotalMoney { get; set; } // số dư KH        x

        public long LoanTotalMoneyOriginalReceipt { get; set; } // tiền gốc được hạch toán      x
        public long LoanTotalMoneyInterestReceipt { get; set; } // tiền lãi được hạch toán      x
        public long LoanTotalMoneyConsultantReceipt { get; set; } // tiền phí được hạch toán + tiền dịch vụ     x
        public string CustomerEpayAccountName { get; set; } // tài khoản Epay       x
        public bool LoanIsLocate { get; set; } // có thiết bị định vị ko, true, false       x
        public string LoanShopConsultantName { get; set; }// shop tư vấn        x
        public string LoanBKS { get; set; } // biển kiểm soát xe máy - LOS cung cấp     x
        public int LoanTopup { get; set; } // tái vay       x
        public string LoanPreparationDate { get; set; } // ngày dự thu      x
        public long LoanPreparationMoney { get; set; } // tiền dự thu       x
        public string CustomerPayTypeName { get; set; } // rủi ro       x

        public string LenderCode { get; set; } // mã lender     x
        public long LoanTotalMoneyReceivables { get; set; } // Nợ cũ = nợ cũ của các kỳ     x
    }
}
