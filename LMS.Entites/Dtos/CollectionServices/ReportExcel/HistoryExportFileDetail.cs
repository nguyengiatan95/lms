﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.CollectionServices.ReportExcel
{
    public class HistoryExportFileDetail
    {
        public string CreateByName { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string ReportName { get; set; }
        public string StatusName { get; set; }
        public long CreateBy { get; set; }
        public string TraceIDRequest { get; set; }
        public int TypeReport { get; set; }
        public bool ShowButtonCreateRequest { get; set; }
    }
}
