﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LoanServices
{
    public class LoanItemViewModel
    {
        public long LoanID { get; set; }
        public string ContactCode { get; set; }
        public string ProductName { get; set; }
        public long LoanTotalMoneyDisbursement { get; set; }
        public long LoanTotalMoneyCurrent { get; set; }
        public long LoanTotalMoneyPaid { get; set; }
        public DateTime LoanInterestStartDate { get; set; }
        public int LoanTime { get; set; }
        public string LoanStatusName { get; set; }
        public int LoanStatusID { get; set; }
        public DateTime LoanInterestPaymentNextDate { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public int CountDownNextdate { get; set; }
        public long OwnerShopID { get; set; }
        public string OwnerShopName { get; set; }
        public long LenderID { get; set; }
        public string LenderCode { get; set; }
        public long ConsultantShopID { get; set; }
        public DateTime ContactStartDate { get; set; }
        public DateTime ContactEndDate { get; set; }
        public long LoanCreditIDOfPartner { get; set; }
        public long CustomerID { get; set; }
        public long CustomerTotalMoney { get; set; }
        public string CustomerNumberCard { get; set; }
        public string CustomerName { get; set; }
        public string CustomerWithHeldReason { get; set; }
        public long CustomerWithHeldAmount { get; set; }
        public string UnitTimeName { get; set; }
        public long TimaLoanID { get; set; }
    }
}
