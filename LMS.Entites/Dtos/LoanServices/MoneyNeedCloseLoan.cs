﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LoanServices
{
    public class MoneyNeedCloseLoan
    {
        public long TotalMoneyCloseLoan { get; set; }
        public long MoneyFineOriginal { get; set; }

        public long MoneyOriginal { get; set; }
        public long MoneyInterest { get; set; }
        public long MoneyService { get; set; }
        public long MoneyConsultant { get; set; }
        public long MoneyFineLate { get; set; } // tiền nợ + nợ cũ nếu có
        public long TotalInterest { get; set; } //
        public int OverDate { get; set; } // số ngày quá hạn
        public bool OverDue { get; set; } // true: quá hạn
        /// 1: tất toán theo kỳ
        /// 2: tất toán sớm
        /// 3: HD kết thúc
        public int CloseType { get; set; }
        public int TotalDaysInterest { get; set; } // tổng só ngày tính lãi

        public long TotalMoneyReceivables { get; set; } // số tiền còn thiếu của các kỳ trước đó so với ngày tính tiền, mang tính chất tham chiếu

        public long DebitMoney { get; set; } // nợ cũ, một vài đơn mới có
    }
    public class MoneyAmountIncurred
    {
        public long MoneyInterest { get; set; }
        public long MoneyService { get; set; }
        public long MoneyConsultant { get; set; }
    }
}
