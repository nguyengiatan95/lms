﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LoanServices
{
    public class LoanDetailViewModel
    {
        public long LoanID { get; set; }
        public string ContactCode { get; set; }        
        public long TotalMoneyDisbursement { get; set; }
        public long TotalMoneyCurrent { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime NextDate { get; set; }
        public DateTime LastDateOfPay { get; set; }
        public int LoanTime { get; set; }
        public int Status { get; set; }
        public int RateType { get; set; } // loại hình vay
        public string RateTypeName { get; set; }
        public string ProductName { get; set; }
        public string LinkGCNChoVay { get; set; }
        public string LinkGCNVay { get; set; }
        public long LoanCreditIDOfPartner { get; set; }
        public long CountDay { get; set; }
        public long OtherMoney { get; set; }

        public long LenderID { get; set; }
        public string LenderName { get; set; }
        public long MoneyFeeInsuranceOfLender { get; set; }
        public long MoneyFineInterest { get; set; }

        public long OwnerShopID { get; set; }
        public string OwnerShopName { get; set; }
        public long ConsultantShopID { get; set; }
        public string ConsultantShopName { get; set; }

        public long CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerNumberCard { get; set; }
        public int CustomerGender { get; set; } // 1 là nữ, 0: nam
        public string CustomerBirthDay { get; set; } // format dd/MM/yyyy
        public long CustomerTotalMoney { get; set; }
        public long MoneyFeeInsuranceOfCustomer { get; set; }

        public long CityID { get; set; }
        public string CityName { get; set; }
        public long DistrictID { get; set; }
        public string DistrictName { get; set; }
        public long WardID { get; set; }
        public string WardName { get; set; }
        public int SourceMoneyDisbursement { get; set; }  // nguồn tiền giải ngân
        public string SourceMoneyDisbursementName { get; set; }

        public int Frequency { get; set; }
        public long RateInterest { get; set; }
        public long RateConsultant { get; set; }
        public long RateService { get; set; }
        public int SourcecBuyInsurance { get; set; } // nguồn mua bảo hiểm
        public string SourcecBuyInsuranceName { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string UnitTimeName { get; set; }

    }

    public class LoanJsonExtra
    {
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string ShopName { get; set; }
        public string ConsultantShopName { get; set; }
        public long TopUpOfLoanBriefID { get; set; }
        public bool IsLocate { get; set; }
        public string LoanBriefPropertyPlateNumber { get; set; }
    }
}
