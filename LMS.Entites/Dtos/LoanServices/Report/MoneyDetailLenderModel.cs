﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LoanServices.Report
{
    public class MoneyDetailLenderModel
    {
        public DateTime CreateDate { get; set; }
        public string CustomerName { get; set; }
        public string ContractCode { get; set; }
        public string ActionName { get; set; }
        public long TotalMoney { get; set; }
        public long MoneyOriginal { get; set; }
        public long MoneyFineOriginal { get; set; }
        public long MoneyInterest { get; set; }
        public long MoneyService { get; set; }
        public long MoneyConsultant { get; set; }
        public long MoneyFineLateLender { get; set; }
        public long MoneyFineLateTima { get; set; }
        public string LenderCode { get; set; }
        public int ActionID { get; set; }
        public long LenderID { get; set; }
        public long? LoanID { get; set; }
        public string ProductName { get; set; }

        public long SumTotalMoney { get; set; }
        public long SumMoneyOriginal { get; set; }
        public long SumMoneyFineOriginal { get; set; }
        public long SumMoneyInterest { get; set; }
        public long SumMoneyService { get; set; }
        public long SumMoneyConsultant { get; set; }
        public long SumMoneyFineLateLender { get; set; }
        public long SumMoneyFineLateTima { get; set; }


    }
}
