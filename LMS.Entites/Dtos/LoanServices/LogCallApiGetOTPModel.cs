﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LoanServices
{
    public class LogCallApiGetOTPModel
    {
        /// <summary>
        /// 0: lỗi, 1: thành công, 2: thử lại
        /// </summary>
        public int Status { get; set; }
    }
}
