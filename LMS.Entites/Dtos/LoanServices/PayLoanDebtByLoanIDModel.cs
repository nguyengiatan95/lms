﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LoanServices
{
    public class PayLoanDebtByLoanIDModel
    {
        public long LoanID { get; set; }
        public long UserID { get; set; }
        public long TotalMoney { get; set; }

    }
}
