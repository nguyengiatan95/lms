﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LoanServices
{
    public class LoanDebtModel
    {
        public long DebtID { get; set; }

        public long ReferID { get; set; }

        public long TotalMoney { get; set; }

        public int TypeID { get; set; }

        public long TargetID { get; set; }

        public string Description { get; set; }

        public int Status { get; set; }

        public DateTime CreateDate { get; set; }

        public long CreateBy { get; set; }
        public long SourceID { get; set; }
        public long ParentDebtID { get; set; }
        public string Username { get; set; }
        public bool ShowButtonDelete { get; set; }
    }
}
