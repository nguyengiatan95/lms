﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace LMS.Entites.Dtos.APIAutoDisbursement
{
    public enum VIB_AI_ResponseCodeConstant
    {
        [Description("Giao dịch thành công")]
        SUCCESS_CODE = 200,
        [Description("Lỗi tham số đầu vào khi thực hiện giao dịch")]
        FAIL_BAD_REQUEST = 204,
        [Description("Chờ ngân hàng đối soát lại")]
        FAIL_BANK_IS_PROCESSING = 205,
        [Description("Lỗi ngân hàng hủy giao dịch")]
        FAIL_BANK_CANCEL = 206,
        [Description("Lỗi tài khoản không đủ tiền thực hiện giao dịch")]
        FAIL_BANK_NOT_ENOUGH = 208,
        [Description("Không tìm thấy giao dịch khi nhập otp")]
        FAIL_TRANSACTION_NOT_EXITS = 300,
        [Description("Sai otp")]
        FAIL_OTP_NOT_VALID = 301,
        [Description("Otp hết hiệu lực")]
        FAIL_TRANSACTION_TIME_OUT = 302,
        [Description("Lỗi giao dịch")]
        FAIL_EXECUTE = 400,
        [Description("Tài khoản đã đăng nhập ở thiết bị khác")]
        FAIL_ACCOUNT_LOGIN_OTHER_DEVICE = 430,
        [Description("Đăng nhập bên bank không thành công")]
        FAIL_LOGIN_FAIL_BANK_SYS = 431,
        [Description("Có lỗi xảy ra bên bank, thử lại")]
        FAIL_INTERNAL_SERVER_ERROR_TRY_MANUALLY = 440,
        [Description("Lỗi tên tài khoản nhận không khớp")]
        FAIL_CONFLICT_NAME_REGISTER = 444,
    }
}
