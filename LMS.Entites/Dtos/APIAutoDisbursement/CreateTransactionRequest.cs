﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.APIAutoDisbursement
{
    public class Base
    {
        [JsonProperty("code")]
        public int Code { get; set; }
        [JsonProperty("messages")]
        public string Messages { get; set; }
    }
    public class CreateTransactionRequest
    {
        [JsonProperty("n")]
        public string BankAccountNumber { get; set; }
        [JsonProperty("bankId")]
        public string BankValue { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("amount")]
        public long Amount { get; set; }
        [JsonProperty("mess")]
        public string Mess { get; set; }
        public string MessageResponse { get; set; }
        public int Retry { get; set; }
        public string TraceIndentifier { get; set; }
        public long LoanCreditID { get; set; }

    }
    public class CreateTransactionResponse : Base
    {
        [JsonProperty("data")]
        public Transaction Data { get; set; }

        // biến dùng check retry tự thêm vào
        public bool IsRetry { get; set; }

        public string TraceIndentifier { get; set; }
    }
    public class Transaction
    {
        [JsonProperty("toacc")]
        public string Toacc { get; set; }

        [JsonProperty("conlai")]
        public string Conlai { get; set; }

        [JsonProperty("sodu")]
        public string Sodu { get; set; }

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("bankacc")]
        public string Bankacc { get; set; }

        [JsonProperty("fee")]
        public string Fee { get; set; }

        [JsonProperty("transid_request")]
        public string TransIDRequest { get; set; } // dùng để điền OTP cho phần giao dịch tiếp theo

        [JsonProperty("transid_verify")]
        public string TransIDVerify { get; set; } // dùng để điền OTP cho phần giao dịch tiếp theo(trường hợp verify otp có transid khác.)

        [JsonProperty("time")]
        public int Time { get; set; }

        [JsonProperty("warning")]
        public bool Warning { get; set; }

        [JsonProperty("transid")]
        public string TransID { get; set; }
    }
    public class ExecuteOtpRequest
    {
        [JsonProperty("TransId_Request")]
        public string TransIDRequest { get; set; }
        [JsonProperty("TransId_Verify")]
        public string TransIDVerify { get; set; }
        [JsonProperty("Otp")]
        public string Otp { get; set; }
        public string TraceIndentifier { get; set; }
        public long LoanCreditID { get; set; }
        /// <summary>
        /// 0: hệ thống, 1: kế toán: kế toán sẽ lấy kết quả ngay
        /// </summary>
        public int ExecuteBy { get; set; }

    }
    public class ExecuteOtpResponse : Base
    {
        [JsonProperty("data")]
        public ResultExecuteOtp Data { get; set; }
        public string TraceIndentifier { get; set; }
    }

    public class ResultExecuteOtp
    {
        [JsonProperty("mess")]
        public string Mess { get; set; }
        [JsonProperty("error")]
        public string Error { get; set; }
    }
    public class CancelTransactionRequest
    {
        public long LoanCreditID { get; set; }
        public string TransID { get; set; }
        public string TraceIndentifier { get; set; }
    }
    public class CancelTransactionResponse : Base
    {
        [JsonProperty("data")]
        public Transaction Data { get; set; }
        public string TraceIndentifier { get; set; }
    }

    public class ReconciliationInternalRequest
    {
        public string TraceIndentifier { get; set; }
    }
}
