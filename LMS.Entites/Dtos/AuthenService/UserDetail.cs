﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.AuthenService
{
    public class UserDetail
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int FullName { get; set; } 
    }
}
