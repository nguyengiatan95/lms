﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.InsuranceService
{
    public class LoanCertificateInformation
    {
        [JsonProperty("Customer_info")]
        public CustomerInfo CustomerInfo { get; set; }
        [JsonProperty("Insurance_info")]
        public InsuranceInfo InsuranceInfo { get; set; }
        [JsonProperty("Investor_info")]
        public InvestorInfo InvestorInfo { get; set; }
        public string Number { get; set; }
    }
    public class CustomerInfo
    {
        public string Address { get; set; }
        [JsonProperty("Contact_number")]
        public string ContactNumber { get; set; }
        [JsonProperty("date_of_birth")]
        public string DateOfBirth { get; set; }
        public string Email { get; set; }
        public string Fullname { get; set; }
        [JsonProperty("id_card")]
        public string IdCard { get; set; }
        public string Phone { get; set; }
    }

    public class InsuranceInfo
    {
        [JsonProperty("From_date")]
        public string FromDate { get; set; }

        [JsonProperty("From_hour")]
        public string FromHour { get; set; }

        [JsonProperty("From_month")]
        public string FromMonth { get; set; }

        [JsonProperty("From_year")]
        public string FromYear { get; set; }

        [JsonProperty("To_date")]
        public string ToDate { get; set; }

        [JsonProperty("To_hour")]
        public string ToHour { get; set; }

        [JsonProperty("To_month")]
        public string ToMonth { get; set; }

        [JsonProperty("To_year")]
        public string ToYear { get; set; }

        public string Money { get; set; }
    }

    public class InvestorInfo
    {
        public string Address { get; set; }

        [JsonProperty("Contact_number")]
        public string ContactNumber { get; set; }

        [JsonProperty("Date_of_birth")]
        public string DateOfBirth { get; set; }

        public string Email { get; set; }
        public string Fullname { get; set; }

        [JsonProperty("Id_card")]
        public string IdCard { get; set; }

        [JsonProperty("Invest_money")]
        public string InvestMoney { get; set; }
        public string Phone { get; set; }
        public string Product { get; set; }
        public string Rate { get; set; }

        [JsonProperty("Tax_code")]
        public string TaxCode { get; set; }
    }
    public class UrlRequest
    {
        public string url { get; set; }

        [JsonProperty("doc_type")]
        public int DocType { get; set; }
    }

}
