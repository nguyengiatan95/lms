﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.InsuranceService
{
    public class CreateLenderContract
    {
        public InputCreateLenderContract CreateContract { get; set; }
        public long LoanID { get; set; }
        public long LenderID { get; set; }
    }
}
