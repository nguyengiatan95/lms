﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.InsuranceService
{
    public class AuthenticationResult
    {
        [JsonProperty("App_id")]
        public string AppID { get; set; }
        public string Token { get; set; }

        [JsonProperty("Expired_date")]
        public DateTime ExpiredDate { get; set; }
    }

    public class AuthenticationInput
    {
        [JsonProperty("App_id")]
        public string AppID { get; set; }

        [JsonProperty("App_key")]
        public string AppKey { get; set; }
    }
}
