﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.InsuranceService
{
    public class OutputGetDoc
    {
        public Meta Meta { get; set; }
        public string Data { get; set; }
    }
}
