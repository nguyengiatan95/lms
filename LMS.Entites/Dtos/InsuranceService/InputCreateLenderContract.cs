﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.InsuranceService
{
    public class InputCreateLenderContract
    {
        public string ContractCode { get; set; }
        public string PartnerCode { get; set; }
        public LenderItem Lender { get; set; }
        public BorrowerItem Borrower { get; set; }
        public long TotalMoney { get; set; }
        public long InsuranceMoney { get; set; }
        public long LenderInsuranceFee { get; set; }
        public long CustomerInsuranceFee { get; set; }
        public int LoanTime { get; set; }
        public string ProductName { get; set; }
        public decimal Rate { get; set; }
        public string LenderContractCode { get; set; }
        public string CustomerCode { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string Signature { get; set; }
    }

    public class LenderItem
    {
        public int Type { get; set; }
        public string FullName { get; set; }
        public string IdNumber { get; set; }
        public string IdNumberPlace { get; set; }
        public string IdNumberDate { get; set; }
        public string Address { get; set; }
        public string Dob { get; set; }
        public int Gender { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string TaxCode { get; set; }
    }

    public class BorrowerItem
    {
        public int Type { get; set; }
        public string FullName { get; set; }
        public string IdNumber { get; set; }
        public string IdNumberPlace { get; set; }
        public string IdNumberDate { get; set; }
        public string Address { get; set; }
        public string Dob { get; set; }
        public int Gender { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string TaxCode { get; set; }
    }
    public class OutputCreateLenderContract
    {
        public Meta Meta { get; set; }
        public Data Data { get; set; }
    }
}
