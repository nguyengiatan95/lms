﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.InsuranceService
{
   public class CreateBorrowerContract
    {
        public InputCreateBorrowerContract CreateContract { get; set; }
        public long LoanID { get; set; }
        public long CustomerID { get; set; }
    }
}
