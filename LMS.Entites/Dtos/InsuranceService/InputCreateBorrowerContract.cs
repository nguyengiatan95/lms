﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.InsuranceService
{
    public class InputCreateBorrowerContract
    {
        public string ContractCode { get; set; }
        public string PartnerCode { get; set; }
        public BorrowerItem Borrower { get; set; }
        public long TotalMoney { get; set; }
        public long InsuranceMoney { get; set; }
        public long LenderInsuranceFee { get; set; }
        public long CustomerInsuranceFee { get; set; }
        public int LoanTime { get; set; }
        public string ProductName { get; set; }
        public decimal Rate { get; set; }
        public string BorrowerContractCode { get; set; }
        public string CustomerCode { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string Signature { get; set; }
    }

    public class OutputCreateBorrowerContract
    {
        public Meta Meta { get; set; }
        public Data Data { get; set; }
    }

    public class Meta
    {
        public int ErrCode { get; set; }
        public string ErrMessage { get; set; }
    }

    public class Data
    {
        public string Code { get; set; }
        public string Path { get; set; }
    }
}
