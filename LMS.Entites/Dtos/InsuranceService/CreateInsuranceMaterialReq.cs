﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.InsuranceService
{
    public class CreateInsuranceMaterial
    {
        public string CongTyBaoHiem { get; set; }

        public string DaiLyBaoHiem { get; set; }

        public string BenMuaBaoHiem { get; set; }

        public string SoDienThoai { get; set; }
        public string SoCMND { get; set; }

        public string DiaChiThuongTru { get; set; }
        public string BienKiemSoat { get; set; }

        public string SoKhung { get; set; }
        public string SoMay { get; set; }

        public string LoaiXe { get; set; }
        public string NhanHieu { get; set; }

        public int NamSanXuat { get; set; }
        public string SoGiayDangKy { get; set; }

        public string NgayCap { get; set; }
        public string CoQuanCap { get; set; }

        public string SoHDTinDung { get; set; }
        public string NgayKyHDTinDung { get; set; }

        public string SoHDBaoHiem { get; set; }
        public long GiaTriThamDinh { get; set; }

        public long SoTienBaoHiem { get; set; }

        public long PhiBaoHiemThanhToan { get; set; }

        public string Dob { get; set; }

        public string ThoiHanBaoHiemBatDau { get; set; }
        public string ThoiHanBaoHiemketThuc { get; set; }
        public string NgayCapDon { get; set; }
        public string NguoiDuocBaoHiem { get; set; }
        public int LoaiSanPham { get; set; }
        public string PartnerCode { get; set; }
        public string Signature { get; set; }
    }

    public class CreateInsuranceMaterialReq
    {
        public CreateInsuranceMaterial CreateInsuranceMaterial { get; set; }
        public long CustomerID { get; set; }
        public long LoanID { get; set; }
    }
    public class ResultBuyInsuranceMaterial
    {
        public Meta Meta { get; set; }

        public Data Data { get; set; }

    }
}
