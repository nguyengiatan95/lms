﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.TransactionServices
{
    public class RequestCreateListTransactionModel
    {
        public long LoanID { get; set; }
        public long TotalMoney { get; set; }
        public int ActionID { get; set; }
        public int MoneyType { get; set; }
        public long PaymentScheduleID { get; set; }
        public long UserID { get; set; }
    }
}
