﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.TransactionServices
{
    public class ListTransactionView
    {
        public long LoanID { get; set; }

        public long TotalMoney { get; set; }

        public long PaymentScheduleID { get; set; }

        public long CustomerID { get; set; }

        public long LenderID { get; set; }

        public DateTime CreateDate { get; set; }

        public long UserID { get; set; }
        public string ActionName
        {
            get;set;
        }
        public string MoneyTypeName { get; set; }
        public string UserName { get; set; }
    }
}
