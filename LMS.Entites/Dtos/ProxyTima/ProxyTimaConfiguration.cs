﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.ProxyTima
{
    public class ProxyTimaConfiguration
    {
        public static string BaseUrl = "https://proxy.tima.vn/";
        public const string LocationDiscovery = "api/Trandata/LocationDiscovery";
        public const string MostContactPhone = "api/Trandata/MostContactPhone";
        public const string KYCBriefInforById = "api/Trandata/KYCBriefInforById";
        public const string KYCBriefInforByPhone = "api/Trandata/KYCBriefInforByPhone";
        public const string SendSMSBrandNameFPT = "api/SMSBrandName/SendSMSBrandNameFPT";

        public const string AgencyAccount = "Job LMS";
        public const int CountRetries = 5;

        public const string SendEmail = "api/email/sendemail";

        public const string EmailDepartment = "IT";
        public const string EmailDomain = "LMS";
        public const string SmsDomainAppLender = "appLender";

    }
    public class TranDataItem
    {
        public string KeyCode { get; set; }
        public string Address { get; set; }
        public double Percent { get; set; }
        public double Long { get; set; }
        public double Lat { get; set; }
        public string Msisdn { get; set; }
        public int TotalCalls { get; set; }
        public int TotalDurations { get; set; }
        public string SPhone { get; set; }
        public string SName { get; set; }
        public string SDOB { get; set; }
        public string SNID { get; set; }
        public string NoteText { get; set; }
    }
}
