﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.ProxyTima
{
    public class ResultK21ProxyTima
    {
        [JsonProperty("s_PHONE")]
        public string SPHONE { get; set; }
        [JsonProperty("s_NAME")]
        public string S_NAME { get; set; }
        [JsonProperty("s_DOB")]
        public string SDOB { get; set; }
    }
    public class K21ProxyTima
    {
        public string ProviderCode { get; set; }
        public double ResponseTime { get; set; }
        public List<ResultK21ProxyTima> Result { get; set; }
    }
}
