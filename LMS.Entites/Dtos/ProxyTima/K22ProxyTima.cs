﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.ProxyTima
{
    public class ResultK22ProxyTima
    {
        [JsonProperty("s_NID")]
        public string SNID { get; set; }
        [JsonProperty("s_NAME")]
        public string SNAME { get; set; }
        [JsonProperty("s_DOB")]
        public string SDOB { get; set; }
    }

    public class K22ProxyTima
    {
        public string ProviderCode { get; set; }
        public double ResponseTime { get; set; }
        public List<ResultK22ProxyTima> Result { get; set; }
    }


}
