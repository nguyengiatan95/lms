﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.ProxyTima
{
    public class ResultK06ProxyTima
    {
        public string Msisdn { get; set; }
        [JsonProperty("total_calls")]
        public int TotalCalls { get; set; }
        [JsonProperty("total_durations")]
        public int TotalDurations { get; set; }
    }

    public class K06ProxyTima
    {
        public string ProviderCode { get; set; }
        public List<ResultK06ProxyTima> Result { get; set; }
    }
}
