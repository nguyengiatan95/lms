﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.ProxyTima
{
    public class K03ProxyTima
    {
        public string ProviderCode { get; set; }
        public double ResponseTime { get; set; }
        public Result Result { get; set; }
    }
    public class Result
    {
        public ResReport ResReport { get; set; }
    }
    public class ResReport
    {
        public Result2 Result { get; set; }
    }
    public class Result2
    {
        public List<Locationlist> Locationlist { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
    }
    public class Locationlist
    {
        public Location Location { get; set; }
    }
    public class Location
    {
        public string Address { get; set; }
        public double Percent { get; set; }
        public double Long { get; set; }
        public double Lat { get; set; }
    }
}
