﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.ProxyTima
{
    public class K21ProxyTimaRequest
    {
        public string NumberCard { get; set; }
        public int Getnew { get; set; }
        public string AgencyAccount { get; set; }
    }
}
