﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.ProxyTima
{
    public class K22ProxyTimaRequest
    {
        public string PhoneNumber { get; set; }
        public int Getnew { get; set; }
        public string AgencyAccount { get; set; }
    }
}
