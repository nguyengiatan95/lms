﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos
{
    public class PermissionListView
    {
        public long RowID { get; set; }
        public long PermissionID { get; set; }

        public string DisplayText { get; set; }

        public string LinkApi { get; set; }

        public int IsActive { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }

    }
}
