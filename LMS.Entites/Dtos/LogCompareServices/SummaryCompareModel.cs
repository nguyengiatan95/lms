﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.LogCompareServices
{
    public class SummaryCompareModel
    {
        public int MainType { get; set; }

        public int DetailType { get; set; }

        public string DetailNote { get; set; }

        public long TotalAG { get; set; }

        public long TotalLMS { get; set; }
    }
}
