﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.AI
{
    public class RequestGPS
    {
        public string contractid { get; set; }
        public string imei { get; set; }
        public long time_received1 { get; set; }
        public long time_received2 { get; set; }
    }
    public class ResponseGPS
    {
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public List<ResponseGPSData> Data { get; set; }
    }
    public class ResponseGPSData
    {

        public long Time { get; set; }
        public long Start { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
        public string Address { get; set; }
        public double TimeToMinute
        {
            get
            {
                double resul = 0;
                try
                {
                    if (Time > 0)
                    {
                        var t = TimeSpan.FromMilliseconds(Time);
                        resul = t.TotalMinutes;
                    }
                }
                catch { }
                return resul;
            }
        }
        public string timeToString
        {
            get
            {
                var resul = "";
                try
                {
                    if (Start > 0 && Start > 0)
                    {
                        var parts = new List<string>();
                        Action<int, string> add = (val, unit) => { if (val > 0) parts.Add(val + unit); };
                        var t = TimeSpan.FromMilliseconds(Time);
                        add(t.Days, " ngày");
                        add(t.Hours, " tiếng");
                        add(t.Minutes, " phút");
                        //add(t.Seconds, "s");
                        //add(t.Milliseconds, "ms");
                        resul = string.Join(" ", parts);
                    }
                    else if (Time > 0)
                    {
                        System.DateTime timeToString = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                        resul = timeToString.AddMilliseconds(Time).ToLocalTime().ToString("dd/MM/yyyy HH:mm");
                    }
                }
                catch { }
                return resul;
            }
        }
        public string startToString
        {
            get
            {
                var result = "";
                try
                {
                    if (Start > 0)
                    {
                        System.DateTime timeToString = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                        result = timeToString.AddMilliseconds(Start).ToLocalTime().ToString("dd/MM/yyyy HH:mm");
                    }
                }
                catch { }
                return result;//  
            }
        }

        public int startToStringOfDay
        {
            get
            {
                var result = 0;
                DateTime toDate = new DateTime();
                try
                {
                    if (Start > 0)
                    {
                        System.DateTime timeToString = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                        toDate = timeToString.AddMilliseconds(Start).ToLocalTime();
                        if (toDate.Hour >= 7 && toDate.Hour < 13)
                        {
                            result = 1; // Sáng : 7h - 12h59
                        }
                        else if (toDate.Hour >= 13 && toDate.Hour < 18)
                        {
                            result = 2; //Chiều : 13h - 17h59
                        }
                        else if (toDate.Hour >= 18 && toDate.Hour < 24)
                        {
                            result = 3; // Tối : 18h - 23h59
                        }
                        else if (toDate.Hour >= 0 && toDate.Hour < 7)
                        {
                            result = 4; // Đêm : 23h - 6h59
                        }
                    }
                }
                catch { }
                return result;//  
            }
        }
        public DateTime? startToDateTime
        {
            get
            {
                DateTime? result = null;
                try
                {
                    if (Start > 0)
                    {
                        System.DateTime timeToString = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                        result = timeToString.AddMilliseconds(Start).ToLocalTime();
                    }
                }
                catch { }
                return result;//  
            }
        }
        public DateTime? endToDateTime
        {
            get
            {
                DateTime? result = null;
                try
                {
                    if (Start > 0)
                    {
                        System.DateTime timeToString = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                        result = timeToString.AddMilliseconds((Start + Time)).ToLocalTime();
                    }
                }
                catch { }
                return result;//  
            }
        }

    }
}
