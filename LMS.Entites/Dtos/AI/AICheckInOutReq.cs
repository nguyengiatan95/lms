﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.AI
{
    public class AICheckInOutReq
    {
        [JsonProperty("contractid")]
        public int Contractid { get; set; }
        [JsonProperty("act")]
        public string Act { get; set; }
        [JsonProperty("lng")]
        public double Lng { get; set; }
        [JsonProperty("lat")]
        public double Lat { get; set; }
        [JsonProperty("location")]
        public string Location { get; set; }
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("uid")]
        public int Uid { get; set; }
        [JsonProperty("vc")]
        public string Vc { get; set; }
        [JsonProperty("appk")]
        public string Appk { get; set; }
        [JsonProperty("iid")]
        public string Iid { get; set; }
        [JsonProperty("t")]
        public long T { get; set; }
    }
    public class AICheckInOutResponse
    {
        public string Message { get; set; }
    }
}
