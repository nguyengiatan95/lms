﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.AI
{
    public class ListCheckInOut
    {
        public int Uid { get; set; }
        public long Time { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string Act { get; set; }
        public string Location { get; set; }
        public int Duration { get; set; }
        public double Distance { get; set; }
        public bool Pass { get; set; }
        public int Contractid { get; set; }
    }

    public class AiListCheckInOut
    {
        public List<ListCheckInOut> Data { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}
