﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.AI
{
    public class FriendFacebook
    {
        public int ResponseCode { get; set; }
        public string Mess { get; set; }
        public ResultFriendFacebook Result { get; set; }
    }
    public class ResultFriendFacebook
    {
        public string ReferenceCode { get; set; }
        public string Status { get; set; }
        public string StatusDes { get; set; }
        public List<string> TopFriends { get; set; }
        public string FbProfile { get; set; }
    }
}
