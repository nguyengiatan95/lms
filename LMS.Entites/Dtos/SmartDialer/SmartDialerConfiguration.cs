﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.SmartDialer
{
    public class SmartDialerConfiguration
    {
        public const string ApiGetCampain = "api/Cisco/GetCampaignThn";
        public const string ApiImportCampaign = "api/Cisco/ImportCampaign";
        public const string ApiGetPendingOfCampaign = "api/Cisco/GetPendingOfCampaign";
        public const string ApiUpdateResource = "api/Cisco/UpdateResource";
        public const string ApiUpdateCsq = "api/Cisco/UpdateCsq";
    }
}
