﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.SmartDialer
{
    public class UpdateResourceRequest
    {
        public string Resource { get; set; } // thông tin username cisco
        public List<UpdateResourceCampaignItem> Campaigns { get; set; }

    }
    public class UpdateResourceCampaignItem
    {
        public long CampaignId { get; set; }
    }
}
