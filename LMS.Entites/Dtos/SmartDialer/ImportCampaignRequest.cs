﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.SmartDialer
{
    public class ImportCampaignRequest
    {
        public long CampaignId { get; set; }
        public List<ImportCampaignPhoneItem> Data { get; set; }
    }
    public class ImportCampaignPhoneItem
    {
        public string Phone { get; set; }
    }
}
