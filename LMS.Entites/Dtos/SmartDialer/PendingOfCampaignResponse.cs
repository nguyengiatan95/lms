﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.SmartDialer
{
    public class PendingOfCampaignRequest
    {
        public long CampaignId { get; set; }
    }
    public class PendingOfCampaignResponse
    {
        public long CampaignId { get; set; }
        public string Phone1 { get; set; }
    }
}
