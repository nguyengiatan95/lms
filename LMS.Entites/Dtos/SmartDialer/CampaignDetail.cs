﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.SmartDialer
{

    public class SmartDialerBaseResponse
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public object UserMessage { get; set; }
        public int Error { get; set; }
        public int TotalPage { get; set; }
        public int TotalRow { get; set; }
        public int PageSize { get; set; }
        public int PageCurrent { get; set; }
        public object Data { get; set; }
    }

    public class CampaignDetail
    {
        public int Id { get; set; }
        public int CampaignId { get; set; }
        public string CampaignName { get; set; }
        public int CampaignType { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string Skillcompetencelevel { get; set; }
        public string SkillNameUriPairName { get; set; }
        public string SkillNameUriPairRefURL { get; set; }
    }
}
