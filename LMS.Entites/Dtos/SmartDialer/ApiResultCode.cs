﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace LMS.Entites.Dtos.SmartDialer
{
    public enum ApiResultCode
    {
        [Description("Thành công!")]
        Success = 0,
        [Description("Token không hợp lệ!")]
        ErrorToken = 1,
        [Description("Dữ liệu truyền vào không hợp lệ (token sai hoac so ngay tim kiem qua ̉̉60 ngay)")]
        ErrorParameter = 2,
        [Description("Exception")]
        Exception = 3,
        [Description("Không Thành công!")]
        Fail = 4,
        [Description("Tổng đài còn tồn chưa call hết!")]
        CiscoOver = 5,
        [Description("CampaignId không hợp lệ!")]
        WrongCampaignId = 6,
        [Description("Lỗi khác")]
        ErrorOther = 6,
        [Description("Không có sđt đang pending")]
        NodataPending = 7
    }

    public enum CallResult
    {
        [Description("Customer answered and was connected to agent.")]
        Voice = 1,
        [Description("Fax machine reached.")]
        Fax = 2,
        [Description("Answering machine reached.")]
        AnsweringMachine = 3,
        [Description("Number reported as invalid by the network or by the agent.")]
        Invalid = 4,
        [Description("Customer does not want to be called again.")]
        DoNotCall = 5,
        [Description("Number successfully contacted but wrong number.")]
        WrongNumber = 6,
        [Description("Numbersuccessfully contacted but reached the wrong person.")]
        WrongPerson = 7,
        [Description("Customer requested regular callback.")]
        Callback = 8,
        [Description("Agent skipped or rejected a preview call.")]
        SkipOrReject = 9,
        [Description("Agent skipped or rejected a preview call with the close option.")]
        SkipCloseOrRejectClose = 10,
        [Description("Busy signal detected or marked busy by agent.")]
        Busy = 11,
        [Description("Agent did not respond to the preview call within the timeout duration.")]
        AgentNotReady = 12,
        [Description("Callback Failed - this value is not written to the database; this is for internal use only.")]
        CallbackFailed = 13,
        [Description("Callback missed and marked for Retry.")]
        Retry = 14,
        [Description("Customer‘s phone timed out either due to Ring No Answer(RNA) or Gateway failure.")]
        RNA = 15,
        [Description("Call was abandoned because IVR port was unavailable or Unified CCX failed to transfer the call to the IVR port.")]
        Unavailable = 16,
        [Description("Call failed due any one of the reasons.")]
        CallFailed = 17,
        [Description("Customer abandoned as customer or agent disconnected the call within the time limit as configured in “Abandoned Call Wait Time” in Unified CCX Application Administration web interface.")]
        AgentDisconnect = 18,
    }
}
