﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Entites.Dtos.SmartDialer
{
    public class UpdateCsqRequest
    {
        public List<UpdateCsqItemRequest> Csqs { get; set; }
    }
    public class UpdateCsqItemRequest
    {
        public long CampaignId { get; set; }
        public int WrapupTime { get; set; }
    }


    public class UpdateCsqResponse
    {
        public long CsqId { get; set; }
        public long CampaignId { get; set; }
        public int WrapupTime { get; set; }
        public int StatusUpdate { get; set; } // = 1: success
        public string Message { get; set; } // = 1: success
    }
}
