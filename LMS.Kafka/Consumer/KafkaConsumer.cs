﻿using Confluent.Kafka;
using LMS.Kafka.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.Kafka.Consumer
{
    /// <summary>
    /// Base class for implementing Kafka Consumer.
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class KafkaConsumer<TKey, TValue> : IKafkaConsumer<TKey, TValue> where TValue : class
    {
        private readonly ConsumerConfig _config;
        private IKafkaHandler<TKey, TValue> _handler;
        private IConsumer<TKey, TValue> _consumer;
        private string _topic;
        ILogger<KafkaConsumer<TKey, TValue>> _logger;

        IServiceScopeFactory _serviceScopeFactory;
        LMS.Common.Helper.Utils _common = new Common.Helper.Utils();
        int MaxRetryConnect = 5;
        /// <summary>
        /// Indicates constructor to initialize the serviceScopeFactory and ConsumerConfig
        /// </summary>
        /// <param name="config">Indicates the consumer configuration</param>
        /// <param name="serviceScopeFactory">Indicates the instance for serviceScopeFactory</param>
        public KafkaConsumer(ConsumerConfig config, IServiceScopeFactory serviceScopeFactory, ILogger<KafkaConsumer<TKey, TValue>> logger)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _config = config;
            _logger = logger;
        }

        /// <summary>
        /// Triggered when the service is ready to consume the Kafka topic.
        /// </summary>
        /// <param name="topic">Indicates Kafka Topic</param>
        /// <param name="stoppingToken">Indicates stopping token</param>
        /// <returns></returns>
        public async Task<LMS.Common.Constants.ResponseActionResult> Consume(string topic, CancellationToken stoppingToken)
        {
            try
            {
                using var scope = _serviceScopeFactory.CreateScope();

                _handler = scope.ServiceProvider.GetRequiredService<IKafkaHandler<TKey, TValue>>(); ;
                _consumer = new ConsumerBuilder<TKey, TValue>(_config).SetValueDeserializer(new KafkaDeserializer<TValue>()).Build();
                _topic = topic;

                return await Task.Run(() => StartConsumerLoop(stoppingToken), stoppingToken);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Consume|Topic={topic}|TValue={typeof(TValue).Name}|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }

        /// <summary>
        /// This will close the consumer, commit offsets and leave the group cleanly.
        /// </summary>
        public void Close()
        {
            _consumer.Close();
        }

        /// <summary>
        /// Releases all resources used by the current instance of the consumer
        /// </summary>
        public void Dispose()
        {
            _consumer.Dispose();
        }

        private async Task<LMS.Common.Constants.ResponseActionResult> StartConsumerLoop(CancellationToken cancellationToken)
        {
            int count = 0;
            _consumer.Subscribe(_topic);

            while (!cancellationToken.IsCancellationRequested && count < MaxRetryConnect)
            {
                object value = null;
                try
                {
                    var result = _consumer.Consume(cancellationToken);
                    value = result?.Message?.Value;
                    if (result != null)
                    {
                        var actionResult = await _handler.HandleAsync(result.Message.Key, result.Message.Value);
                        //if (actionResult.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                        _consumer.Commit();
                        count = 0;// reset lại
                    }
                }
                catch (OperationCanceledException ex)
                {
                    _logger.LogError($"Consume_StartConsumerLoop_OperationCanceledException|Topic={_topic}|Handler={_handler}|value={_common.ConvertObjectToJSonV2(value)}|ex={ex.Message}-{ex.StackTrace}");
                    count++;
                }
                catch (ConsumeException ex)
                {
                    _logger.LogError($"Consume_StartConsumerLoop_ConsumeException|Topic={_topic}|Handler={_handler}|value={_common.ConvertObjectToJSonV2(value)}|Reason={ex.Error.Reason}-{ex.Message}");
                    count++;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Consume_StartConsumerLoop_Exception|Topic={_topic}|Handler={_handler}|value={_common.ConvertObjectToJSonV2(value)}|ex={ex.Message}-{ex.StackTrace}");
                    count++;
                }
                if(count != 0)
                {
                    await Task.Delay(1000);// chờ 1s
                }
            }
            _consumer.Close();
            return null;
        }
    }
}
