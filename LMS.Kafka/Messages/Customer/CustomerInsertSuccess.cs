﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Kafka.Messages.Customer
{
    public class CustomerInsertSuccess
    {
        public long CustomerID { get; set; }
        public long LoanID { get; set; }
    }
}
