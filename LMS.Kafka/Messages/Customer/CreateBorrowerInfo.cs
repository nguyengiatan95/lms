﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Kafka.Messages.Customer
{
    public class CreateBorrowerInfo
    {
        public string CustomerName { get; set; }
        public string NumberCard { get; set; }
        public string Phone { get; set; }
        public int Gender { get; set; }
        public int CityID { get; set; }
        public int DistrictID { get; set; }
        public int WardID { get; set; }
        public string Address { get; set; }
        public string AddressHouseHold { get; set; }
        public int IsBorrowing { get; set; }
        public string PermanentAddress { get; set; }
        public string BirthDay { get; set; } // dd/MM/yyyy
        public long TimaCustomerID { get; set; }
        
        public long LoanID { get; set; }
    }
}
