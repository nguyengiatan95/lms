﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Kafka.Messages.Customer
{
    public class CustomerBalanceTopupInfo
    {
        public long CustomerID { get; set; }
        public long TotalMoney { get; set; }
        public string Note { get; set; }
        /// <summary>
        /// định dạng TimaSettingConstant.DateTimeDayMonthYearFull
        /// </summary>
        public string TransactionDateTopUp { get; set; }
    }
    public class CustomerBalanceToupSuccessInfo
    {
        public long CustomerID { get; set; }
    }
}
