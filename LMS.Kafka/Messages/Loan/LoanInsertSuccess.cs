﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Kafka.Messages.Loan
{
    public class LoanInsertSuccess
    {
        public long LoanID { get; set; }
        public long CreateBy { get; set; }
        public string Note { get; set; }
    }
}
