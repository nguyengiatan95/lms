﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Kafka.Messages.Loan
{
    public class LoanCutPeriodSuccessInfo
    {
        public long LoanID { get; set; }
        public DateTime NextDate { get; set; }
        public DateTime LastDateOfPay { get; set; }
    }
}
