﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Kafka.Messages.Loan
{
    public class DebtRestructuringLoanSuccessInfo
    {
        public long LoanID { get; set; }
        public int DebtRestructuringType { get; set; }
        public long CreateBy { get; set; }
    }
}
