﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Kafka.Messages.Loan
{
    public class LoanPaymentSuccess
    {
        public long LoanID { get; set; }
        /// <summary>
        /// = 0 : tính lại từ đầu, ngược lại tính từ = start, end
        /// </summary>
        public long StartTransactionID { get; set; }
        public long EndTransactionID { get; set; }
    }
}
