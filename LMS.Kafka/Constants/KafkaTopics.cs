﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Kafka.Constants
{
    public static class KafkaTopics
    {
        public static string PrefixTopic = "local";
        //public static string LoanPayment => $"LMS_{PrefixTopic}_LoanPayment";

        //public static string CustomerCreateUpdateInfo => $"LMS_{PrefixTopic}_CustomerCreateUpdateInfo";
        //public static string CustomerBalanceChangeInfo => $"LMS_{PrefixTopic}_CustomerBalanceChangeInfo";
        public static string CustomerBalanceTopupInfo => $"LMS_{PrefixTopic}_CustomerBalanceTopupInfo";
        public static string CustomerBalanceTopupSuccessInfo => $"LMS_{PrefixTopic}_CustomerBalanceTopupSuccessInfo";
        //public static string LoanUpdateCustomerIDInfo => $"LMS_{PrefixTopic}_LoanUpdateCustomerIDInfo";
        public static string PaymentUpdateNextDateLoanInfo => $"LMS_{PrefixTopic}_PaymentUpdateNextDateLoanInfo";
        public static string LoanCreateSuccess => $"LMS_{PrefixTopic}_LoanCreateSuccess";
        public static string LoanPaymentSuccess => $"LMS_{PrefixTopic}_LoanPaymentSuccess";
        public static string LoanCutPeriodSuccess => $"LMS_{PrefixTopic}_LoanCutPeriodSuccess";
        public static string DebtRestructuringLoanSuccess => $"LMS_{PrefixTopic}_DebtRestructuringLoanSuccess";
    }
}
