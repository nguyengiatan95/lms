﻿using LMS.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.ReportServiceApi
{
    public class ReportServiceHelper
    {
        public static THN_DPD GetTypeBebtLoanByNextDate_v2(DateTime nextDate, DateTime? firstDay = null)
        {
            if (firstDay == null)
            {
                firstDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            }

            var dpdCurrent = firstDay.Value.Date.Subtract(nextDate).Days;
            // có 15 nhóm từ B0 - > B13+
            // bước nhảy 30 ngày
            if (dpdCurrent < 1)
            {
                return THN_DPD.B0;
            }
            int i = dpdCurrent / 30;
            if (i > 13)
            {
                return THN_DPD.B14;
            }
            int j = dpdCurrent % 30;
            if (j == 0)
            {
                return (THN_DPD)i;
            }
            if (i + 1 > 13)
            {
                return THN_DPD.B14;
            }
            return (THN_DPD)(i + 1);
        }
    }
}
