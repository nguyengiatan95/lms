﻿using LMS.Kafka.Interfaces;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.ReportServiceApi.KafkaEventHandlers
{
    public class CreateReportByLoanInsertSuccessHandler : IKafkaHandler<string, LMS.Kafka.Messages.Loan.LoanInsertSuccess>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        private readonly IMediator _bus;
        LMS.Common.Helper.Utils _common = new Common.Helper.Utils();
        ILogger<CreateReportByLoanInsertSuccessHandler> _logger;
        public CreateReportByLoanInsertSuccessHandler(IMediator bus,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            ILogger<CreateReportByLoanInsertSuccessHandler> logger)
        {
            _bus = bus;
            _loanTab = loanTab;
            _logger = logger;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> HandleAsync(string key, LMS.Kafka.Messages.Loan.LoanInsertSuccess value)
        {
            LMS.Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            try
            {
                var loanInfos = await _loanTab.WhereClause(x => x.LoanID == value.LoanID).QueryAsync();
                if (loanInfos == null || !loanInfos.Any())
                {
                    _logger.LogError($"CreateReportByLoanInsertSuccessHandler|value={_common.ConvertObjectToJSonV2(value)}|Not_found_loan");
                    return response;
                }
                var loanInfo = loanInfos.First();

            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateReportByLoanInsertSuccessHandler|value={_common.ConvertObjectToJSonV2(value)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
