﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.ReportServiceApi.Domain.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.ReportServiceApi.Commands.ReportLoanSummary
{
    public class ProcessReportLoanDebtDailyCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class ProcessReportLoanDebtDailyCommandHandler : IRequestHandler<ProcessReportLoanDebtDailyCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> _settingKeyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblProductCredit> _productionCreditTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblReportLoanDebtDaily> _reportLoanDebtDailyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanCloseLoan> _planCloseLoanTab;
        LMS.Common.Helper.Utils _common;
        ILogger<ProcessReportLoanDebtDailyCommandHandler> _logger;
        public ProcessReportLoanDebtDailyCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> settingKeyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblReportLoanDebtDaily> reportLoanDebtDailyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanCloseLoan> planCloseLoanTab,
            ILogger<ProcessReportLoanDebtDailyCommandHandler> logger)
        {
            _settingKeyTab = settingKeyTab;
            _common = new Utils();
            _logger = logger;
            _loanTab = loanTab;
            _paymentScheduleTab = paymentScheduleTab;
            _planCloseLoanTab = planCloseLoanTab;
            _reportLoanDebtDailyTab = reportLoanDebtDailyTab;
            _lenderTab = lenderTab;
        }

        public async Task<ResponseActionResult> Handle(ProcessReportLoanDebtDailyCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var keySettingInfo = (await _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.ReportLoanDebtDaily_ProcessDaily).QueryAsync()).FirstOrDefault();
                if (keySettingInfo == null)
                {
                    keySettingInfo = new Domain.Tables.TblSettingKey
                    {
                        CreateDate = DateTime.Now,
                        KeyName = LMS.Common.Constants.SettingKey_KeyValue.ReportLoanDebtDaily_ProcessDaily,
                        Status = (int)SettingKey_Status.Active,
                        ModifyDate = DateTime.Now,
                        Value = _common.ConvertObjectToJSonV2(new SettingKeyValueReportLoanDebtDaily
                        {
                            ReportDate = DateTime.Now.AddDays(-1),
                            LatestLoanID = 0
                        })
                    };
                    keySettingInfo.SettingKeyID = await _settingKeyTab.InsertAsync(keySettingInfo);
                }
                if (keySettingInfo.Status == (int)SettingKey_Status.InActice)
                {
                    response.Message = MessageConstant.SettingKey_ReportLoanDebtDaily_ProcessDailyStatus;
                    return response;
                }
                SettingKeyValueReportLoanDebtDaily settingKeyValue = new SettingKeyValueReportLoanDebtDaily() { ReportDate = DateTime.Now.AddDays(-1) };
                if (!string.IsNullOrEmpty(keySettingInfo.Value))
                {
                    try
                    {
                        settingKeyValue = _common.ConvertJSonToObjectV2<SettingKeyValueReportLoanDebtDaily>(keySettingInfo.Value);
                    }
                    catch (Exception)
                    {
                        settingKeyValue = new SettingKeyValueReportLoanDebtDaily
                        {
                            ReportDate = DateTime.Now.AddDays(-1)
                        };
                        keySettingInfo.Value = _common.ConvertObjectToJSonV2(settingKeyValue);
                    }
                }
                if (settingKeyValue.ReportDate.Date > DateTime.Now.Date && settingKeyValue.LatestLoanID == 0)
                {
                    response.Message = MessageConstant.SettingKey_ReportLoanDebtDaily_ProcessDailyFinished;
                    return response;
                }
                keySettingInfo.Status = (int)SettingKey_Status.InActice;
                settingKeyValue.CountHandle = 0;
                keySettingInfo.ModifyDate = DateTime.Now;
                _ = RunUntilFinished(keySettingInfo, settingKeyValue);
                _ = await _settingKeyTab.UpdateAsync(keySettingInfo);
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessCutOffLoanByDateDailyCommandHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        private async Task RunUntilFinished(Domain.Tables.TblSettingKey keySettingInfo, SettingKeyValueReportLoanDebtDaily settingvalue)
        {
            try
            {
                settingvalue = await ProcessStepMoveLoan(settingvalue);
                if (!settingvalue.IsContinous)
                {
                    settingvalue.CountTimeHandleFinished = Convert.ToInt64(DateTime.Now.Subtract(keySettingInfo.ModifyDate).TotalMilliseconds);
                    settingvalue.ReportDate = DateTime.Now.AddDays(1);
                    keySettingInfo.Status = (int)SettingKey_Status.Active;
                    keySettingInfo.ModifyDate = DateTime.Now;
                    keySettingInfo.Value = _common.ConvertObjectToJSonV2(settingvalue);
                    _ = await _settingKeyTab.UpdateAsync(keySettingInfo);
                    return;
                }
                await Task.Delay(TimeSpan.FromSeconds(2));
                settingvalue.CountHandle++;
                _ = RunUntilFinished(keySettingInfo, settingvalue);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessCutOffLoanByDateDailyCommandHandler_RunUntilFinished|ex={ex.Message}-{ex.StackTrace}");
            }
        }
        private async Task<SettingKeyValueReportLoanDebtDaily> ProcessStepMoveLoan(SettingKeyValueReportLoanDebtDaily settingvalue)
        {
            var currentDate = DateTime.Now;
            int top = 1000;
            try
            {
                List<int> lstLoanStatus = new List<int> { (int)Loan_Status.Lending, (int)Loan_Status.CloseIndemnifyLoanInsurance };
                // lấy danh sách đơn đang vay hoặc tất toán nhưng chưa bồi thường bảo hiểm
                var lstLoanDataTask = await _loanTab.SetGetTop(top)
                                          .SelectColumns(x => x.LoanID, x => x.TimaLoanID, x => x.ContactCode, x => x.LoanCreditIDOfPartner, x => x.FromDate, x => x.ToDate)
                                          .SelectColumns(x => x.TotalMoneyCurrent, x => x.TotalMoneyDisbursement, x => x.NextDate, x => x.LastDateOfPay, x => x.MoneyFineLate)
                                          .SelectColumns(x => x.LenderID, x => x.CityID, x => x.DistrictID, x => x.WardID, x => x.CustomerID, x => x.CustomerName)
                                          .SelectColumns(x => x.Status, x => x.StatusSendInsurance, x => x.ProductID, x => x.ProductName)
                                          .SelectColumns(x => x.OwnerShopID, x => x.ConsultantShopID, x => x.SourcecBuyInsurance)
                                          .WhereClause(x => lstLoanStatus.Contains((int)x.Status) && x.LoanID > settingvalue.LatestLoanID).OrderBy(x => x.LoanID).QueryAsync();
                if (lstLoanDataTask == null || !lstLoanDataTask.Any())
                {
                    settingvalue.LatestLoanID = 0;
                    settingvalue.IsContinous = false;
                    return settingvalue;
                }
                settingvalue.IsContinous = true;
                List<long> lstLoanID = new List<long>();
                List<long> lstLenderID = new List<long>();
                foreach (var item in lstLoanDataTask)
                {
                    lstLoanID.Add(item.LoanID);
                    lstLenderID.Add(item.LenderID);
                    lstLenderID.Add(item.OwnerShopID);
                    lstLenderID.Add(item.ConsultantShopID);
                    if (settingvalue.LatestLoanID < item.LoanID)
                        settingvalue.LatestLoanID = item.LoanID;
                }
                lstLenderID = lstLenderID.Distinct().ToList();
                var lstPlanCloseLoanTask = _planCloseLoanTab.WhereClause(x => x.CloseDate == currentDate.Date && lstLoanID.Contains(x.LoanID)).QueryAsync();

                var sqlQueryPayment = " WITH ctePaymentSchedule AS ( SELECT p.*,ROW_NUMBER() OVER (PARTITION BY p.LoanID ORDER BY p.PayDate) AS rn ";
                sqlQueryPayment += " FROM TblPaymentSchedule p (NOLOCK) join TblLoan l (nolock) on p.LoanID = l.LoanID ";
                sqlQueryPayment += " WHERE p.Status = @Status and p.PayDate >= l.NextDate and p.IsComplete = 0 and l.Status in @LstStatus  and p.LoanID in @LstLoanID ) ";
                sqlQueryPayment += " SELECT * from ctePaymentSchedule where rn = 1";
                var parameter = new Dapper.DynamicParameters();
                parameter.Add("@Status", (int)PaymentSchedule_Status.Use);
                parameter.Add("@LstStatus", lstLoanStatus);
                parameter.Add("@LstLoanID", lstLoanID);

                var lstPaymentScheudleTask = _paymentScheduleTab.QueryAsync(sqlQueryPayment, parameter);
                var lstLenderTask = _lenderTab.SelectColumns(x => x.LenderID, x => x.FullName).WhereClause(x => lstLenderID.Contains(x.LenderID)).QueryAsync();

                await Task.WhenAll(lstPlanCloseLoanTask, lstPaymentScheudleTask, lstLenderTask);


                var dictPlanCloseLoanInfos = lstPlanCloseLoanTask.Result.ToDictionary(x => x.LoanID, x => x);
                var dictPaymentInfos = lstPaymentScheudleTask.Result.ToDictionary(x => x.LoanID, x => x);
                var dictLenderInfos = lstLenderTask.Result.ToDictionary(x => x.LenderID, x => x);

                List<TblReportLoanDebtDaily> lstInsert = new List<TblReportLoanDebtDaily>();
                foreach (var item in lstLoanDataTask)
                {
                    var closeLoanInfo = dictPlanCloseLoanInfos.GetValueOrDefault(item.LoanID);
                    if (closeLoanInfo == null)
                    {
                        _logger.LogError($"ReportLoanDebtDaily_Not_Found_CloseLoan|LoanID={item.LoanID}");
                    }
                    var paymentInfo = dictPaymentInfos.GetValueOrDefault(item.LoanID);
                    if (paymentInfo == null)
                    {
                        _logger.LogError($"ReportLoanDebtDaily_Not_Found_PaymentSchedule|LoanID={item.LoanID}");
                    }
                    var report = new TblReportLoanDebtDaily
                    {
                        CityID = item.CityID,
                        DistrictID = item.DistrictID,
                        WardID = item.WardID,
                        LoanID = item.LoanID,
                        TimaLoanID = item.TimaLoanID,
                        LmsContractCode = item.ContactCode,
                        LosLoanID = item.LoanCreditIDOfPartner.GetValueOrDefault(),
                        LosLoanCode = $"{TimaSettingConstant.PatternContractCodeLOS}{item.LoanCreditIDOfPartner}",
                        CreateDate = currentDate,
                        ModifyDate = currentDate,
                        ReportDate = currentDate,
                        CustomerID = item.CustomerID.GetValueOrDefault(),
                        CustomerName = item.CustomerName,
                        NextDate = item.NextDate.GetValueOrDefault(),
                        LastDateOfPay = item.LastDateOfPay,
                        DPD = currentDate.Date.Subtract(item.NextDate.Value.Date).Days,
                        FromDate = item.FromDate,
                        ToDate = item.ToDate,
                        LenderID = item.LenderID,
                        LoanStatus = item.Status,
                        LoanStatusInsurance = item.StatusSendInsurance,
                        ProductID = item.ProductID.GetValueOrDefault(),
                        ProductName = item.ProductName,
                        TypeDebt = (int)ReportServiceHelper.GetTypeBebtLoanByNextDate_v2(item.NextDate.Value, currentDate),
                        LenderCode = dictLenderInfos.GetValueOrDefault(item.LenderID)?.FullName,
                        TotalMoneyDisbursement = item.TotalMoneyDisbursement,
                        TotalMoneyCurrent = item.TotalMoneyCurrent,
                        OwnerShopID = item.OwnerShopID,
                        ConsultantShopID = item.ConsultantShopID,
                        OwnerShopName = dictLenderInfos.GetValueOrDefault(item.OwnerShopID)?.FullName,
                        ConsultantShopName = dictLenderInfos.GetValueOrDefault(item.ConsultantShopID)?.FullName,
                        SourceBuyInsurance = item.SourcecBuyInsurance.GetValueOrDefault()
                    };
                    if (closeLoanInfo != null)
                    {
                        report.TotalMoneyCloseLoan = closeLoanInfo.TotalMoneyClose;
                        report.MoneyConsultantCloseLoan = closeLoanInfo.MoneyConsultant;
                        report.MoneyFineLateCloseLoan = closeLoanInfo.MoneyFineLate;
                        report.MoneyFineOriginalCloseLoan = closeLoanInfo.MoneyFineOriginal;
                        report.MoneyInterestCloseLoan = closeLoanInfo.MoneyInterest;
                        report.MoneyOriginalCloseLoan = closeLoanInfo.MoneyOriginal;
                        report.MoneyServiceCloseLoan = closeLoanInfo.MoneyService;
                    }
                    if (paymentInfo != null)
                    {
                        report.TotalMoneyNeedPay = paymentInfo.MoneyOriginal + paymentInfo.MoneyInterest + paymentInfo.MoneyService + paymentInfo.MoneyConsultant + paymentInfo.MoneyFineLate;
                        report.TotalMoneyNeedPay -= (paymentInfo.PayMoneyOriginal + paymentInfo.PayMoneyInterest + paymentInfo.PayMoneyService + paymentInfo.PayMoneyConsultant + paymentInfo.PayMoneyFineLate);
                        report.MoneyConsultantNeedPay = paymentInfo.MoneyConsultant;
                        report.MoneyFineLateNeedPay = paymentInfo.MoneyFineLate;
                        report.MoneyInteresetNeedPay = paymentInfo.MoneyInterest;
                        report.MoneyOriginalNeedPay = paymentInfo.MoneyOriginal;
                        report.MoneyServiceNeedPay = paymentInfo.MoneyService;
                    }
                    lstInsert.Add(report);
                }

                _reportLoanDebtDailyTab.InsertBulk(lstInsert);
            }
            catch (Exception ex)
            {
                settingvalue.IsContinous = false;
                _logger.LogError($"ReportLoanDebtDaily_Not_Found_CloseLoan|SettingValue={_common.ConvertObjectToJSonV2(settingvalue)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return settingvalue;

        }
    }
}
