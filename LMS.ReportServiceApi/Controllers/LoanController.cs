﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.ReportServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoanController : ControllerBase
    {
        IMediator _bus;
        public LoanController(IMediator bus)
        {
            this._bus = bus;
        }


        [HttpGet]
        [Route("ProcessReportLoanDebtDaily")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ProcessReportLoanDebtDaily()
        {
            return await _bus.Send(new Commands.ReportLoanSummary.ProcessReportLoanDebtDailyCommand { });
        }
    }
}
