﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.ReportServiceApi.Domain.Tables
{

    [Dapper.Contrib.Extensions.Table("TblLender")]
    public class TblLender
    {
        [Dapper.Contrib.Extensions.Key]
        public long LenderID { get; set; }

        public string FullName { get; set; }

        public string NumberCard { get; set; }

        public DateTime? BirthDay { get; set; }

        public int? Gender { get; set; }

        public string Phone { get; set; }

        public string TaxCode { get; set; }

        public int? Status { get; set; }

        public string Address { get; set; }
        // lãi suất % theo năm
        public decimal RateInterest { get; set; }
        [Dapper.Contrib.Extensions.Key]
        public byte[] Version { get; set; }
        public long TotalMoney { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public long TimaLenderID { get; set; }
        public int IsHub { get; set; }
        public int IsBuyInsurance { get; set; }
        public int UnitRate { get; set; }
        public int IsGcash { get; set; }
        public string Represent { get; set; }
        public int IsVerified { get; set; }
        public int RegFromApp { get; set; }
        public long MoneyHold { get; set; }
        public long MoneyAvailiable { get; set; }
        public int SelfEmployed { get; set; }
    }
}
