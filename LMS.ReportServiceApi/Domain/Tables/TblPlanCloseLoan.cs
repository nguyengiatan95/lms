﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.ReportServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblPlanCloseLoan")]
    public class TblPlanCloseLoan
    {
        [Dapper.Contrib.Extensions.Key]
        public long PlanCloseLoanID { get; set; }

        public long LoanID { get; set; }

        public DateTime NextDate { get; set; }

        public DateTime CloseDate { get; set; }

        public long TotalMoneyClose { get; set; }

        public long MoneyOriginal { get; set; }

        public long MoneyInterest { get; set; }

        public long MoneyService { get; set; }

        public long MoneyConsultant { get; set; }

        public long MoneyFineLate { get; set; }
        public long MoneyFineOriginal { get; set; }
        public long TotalMoneyReceivables { get; set; }

        public DateTime CreateDate { get; set; }
    }
}
