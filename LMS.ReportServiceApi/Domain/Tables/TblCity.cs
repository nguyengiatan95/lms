﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.ReportServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblCity")]
    public class TblCity
    {
        [Dapper.Contrib.Extensions.Key]
        public long CityID { get; set; }

        public string Name { get; set; }

        public string TypeCity { get; set; }

        public int? Position { get; set; }

        public int? IsApply { get; set; }

        public int? IsCity { get; set; }

        public int? DomainID { get; set; }

        public int? RegionID { get; set; }

    }
}
