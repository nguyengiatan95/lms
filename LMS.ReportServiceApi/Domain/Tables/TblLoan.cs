﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.ReportServiceApi.Domain.Tables
{

    [Dapper.Contrib.Extensions.Table("TblLoan")]
    public class TblLoan
    {
        [Dapper.Contrib.Extensions.Key]
        public long LoanID { get; set; }

        public long? CustomerID { get; set; }

        public string CustomerName { get; set; }

        public long? ProductID { get; set; }

        public string ProductName { get; set; }

        public long TotalMoneyDisbursement { get; set; }

        public long TotalMoneyCurrent { get; set; }

        public long CityID { get; set; }

        public long DistrictID { get; set; }

        public long WardID { get; set; }

        public int SourceMoneyDisbursement { get; set; }

        public int LoanTime { get; set; }

        public int RateType { get; set; }

        public int Frequency { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public DateTime? FinishDate { get; set; }

        public string ContactCode { get; set; }

        public long OwnerShopID { get; set; }

        public long LenderID { get; set; }

        public long ConsultantShopID { get; set; }

        public decimal RateInterest { get; set; }

        public decimal RateConsultant { get; set; }

        public decimal RateService { get; set; }

        public DateTime? NextDate { get; set; }

        public DateTime? LastDateOfPay { get; set; }

        public int? Status { get; set; }

        public int? SourcecBuyInsurance { get; set; }

        public string LinkGCNChoVay { get; set; }

        public string LinkGCNVay { get; set; }
        /// <summary>
        /// phí bảo hiểm sức khỏe
        /// </summary>

        public long MoneyFeeInsuranceOfCustomer { get; set; }
        /// <summary>
        /// phí bảo hiểm vật chất
        /// </summary>
        public long MoneyFeeInsuranceMaterialCovered { get; set; }

        public long MoneyFeeInsuranceOfLender { get; set; }

        public string JsonExtra { get; set; }

        public long MoneyFineLate { get; set; }

        public long? MoneyFineOriginal { get; set; }

        public long? MoneyFineInterest { get; set; }

        [Dapper.Contrib.Extensions.Key]
        public System.Byte[] Version { get; set; }

        public long? BankCardID { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }

        public long? LoanCreditIDOfPartner { get; set; }
        public int UnitRate { get; set; }
        public long TimaLoanID { get; set; }
        public long TimaCodeID { get; set; }
        public int StatusSendInsurance { get; set; }
        public string LinkInsuranceMaterial { get; set; }
        public decimal FeeFineOriginal { get; set; }
        public int UnitFeeFineOriginal { get; set; }
        // nợ cũ từ AG
        public long DebitMoney { get; set; }
    }

    public class LoanJsonExtra
    {
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string ShopName { get; set; }
        public string ConsultantShopName { get; set; }
        public long TopUpOfLoanBriefID { get; set; }
        public bool IsLocate { get; set; }
        public string LoanBriefPropertyPlateNumber { get; set; }
    }
}
