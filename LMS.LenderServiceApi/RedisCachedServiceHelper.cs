﻿using LMS.Common.Configuration;
using LMS.Common.Constants;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi
{
    public class RedisCachedServiceHelper
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        readonly Lazy<ConnectionMultiplexer> _lazyConnection;
        readonly ILogger<RedisCachedServiceHelper> _logger;
        public RedisCachedServiceHelper(IServiceScopeFactory serviceScopeFactory, RedisCacheSetting redisCacheSetting, ILogger<RedisCachedServiceHelper> logger)
        {
            _userTab = serviceScopeFactory.CreateScope().ServiceProvider.GetRequiredService<LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser>>();
            ConfigurationOptions configuration = new ConfigurationOptions
            {
                AbortOnConnectFail = redisCacheSetting.AbortConnect,
                ConnectTimeout = redisCacheSetting.ConnectTimeout,
                Password = redisCacheSetting.Password
            };
            configuration.EndPoints.Add(redisCacheSetting.Host, redisCacheSetting.Port);
            _lazyConnection = new Lazy<ConnectionMultiplexer>(() => ConnectionMultiplexer.Connect(configuration));
            _logger = logger;
        }
        private IConnectionMultiplexer RedisConnection => _lazyConnection.Value;
        #region test manual
        ///// <summary>
        ///// The lazy connection.
        ///// </summary>
        //private static Lazy<ConnectionMultiplexer> lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
        //{
        //    ConfigurationOptions configuration = new ConfigurationOptions
        //    {
        //        AbortOnConnectFail = false,
        //        ConnectTimeout = 5000,
        //        Password = "d71ee1e420eee79040c758ac603a62e79466ee7654c13b837fa2a7399f78f916"
        //    };

        //    configuration.EndPoints.Add("172.26.1.32", 6379);

        //    return ConnectionMultiplexer.Connect(configuration.ToString());
        //});
        //public static ConnectionMultiplexer Connection => lazyConnection.Value;
        //public List<string> TestRedis()
        //{
        //    ConcurrentBag<string> lstResult = new ConcurrentBag<string>();
        //    string lockKey = "lock:eat";
        //    TimeSpan expiration = TimeSpan.FromSeconds(5);
        //    Parallel.For(0, 5, x =>
        //    {
        //        string person = $"person:{x}";
        //        var val = 0;
        //        bool isLocked = AcquireLock(lockKey, person, expiration);
        //        while (!isLocked && val <= 5000)
        //        {
        //            val += 250;
        //            System.Threading.Thread.Sleep(250);
        //            isLocked = AcquireLock(lockKey, person, expiration);
        //        }

        //        if (isLocked)
        //        {
        //            lstResult.Add($"{person} begin eat food(with lock) at {DateTimeOffset.Now.ToUnixTimeMilliseconds()}.");
        //            if (new Random().NextDouble() < 0.6)
        //            {
        //                lstResult.Add($"{person} release lock {ReleaseLock(lockKey, person)}  {DateTimeOffset.Now.ToUnixTimeMilliseconds()}");
        //            }
        //            else
        //            {
        //                lstResult.Add($"{person} do not release lock ....");
        //            }
        //        }
        //        else
        //        {
        //            lstResult.Add($"{person} begin eat food(without lock) at {DateTimeOffset.Now.ToUnixTimeMilliseconds()}.");
        //        }
        //    });

        //    lstResult.Add("end");
        //    return lstResult.ToList();
        //}
        ///// <summary>
        ///// Releases the lock.
        ///// </summary>
        ///// <returns><c>true</c>, if lock was released, <c>false</c> otherwise.</returns>
        ///// <param name="key">Key.</param>
        ///// <param name="value">Value.</param>
        //static bool ReleaseLock(string key, string value)
        //{
        //    string lua_script = @"
        //    if (redis.call('GET', KEYS[1]) == ARGV[1]) then
        //        redis.call('DEL', KEYS[1])
        //        return true
        //    else
        //        return false
        //    end
        //    ";

        //    try
        //    {
        //        var res = Connection.GetDatabase().ScriptEvaluate(lua_script,
        //                                                   new RedisKey[] { key },
        //                                                   new RedisValue[] { value });
        //        return (bool)res;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine($"ReleaseLock lock fail...{ex.Message}");
        //        return false;
        //    }
        //}
        ///// <summary>
        ///// Acquires the lock.
        ///// </summary>
        ///// <returns><c>true</c>, if lock was acquired, <c>false</c> otherwise.</returns>
        ///// <param name="key">Key.</param>
        ///// <param name="value">Value.</param>
        ///// <param name="expiration">Expiration.</param>
        //static bool AcquireLock(string key, string value, TimeSpan expiration)
        //{
        //    bool flag = false;

        //    try
        //    {
        //        flag = Connection.GetDatabase().StringSet(key, value, expiration, When.NotExists);
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine($"Acquire lock fail...{ex.Message}");
        //        flag = true;
        //    }

        //    return flag;
        //}

        //public async Task<List<string>> TestRedisCount()
        //{
        //    ConcurrentBag<string> lstResult = new ConcurrentBag<string>();
        //    //string lockKey = "lock:eat";
        //    //string key_count = "increatment:codeid:lender";
        //    TimeSpan expiration = TimeSpan.FromSeconds(5);
        //    List<Task<(long, int)>> lstTask = new List<Task<(long, int)>>();
        //    for (int i = 0; i < 1000; i++)
        //    {
        //        var task = getBigID(i);
        //        lstTask.Add(task);
        //    }
        //    await Task.WhenAll(lstTask);
        //    foreach (var task in lstTask)
        //    {
        //        var (id, x) = task.Result;
        //        string person = $"person:{x}";
        //        lstResult.Add($"{person} ID =  {id}");
        //    }

        //    lstResult.Add("end");
        //    return lstResult.ToList();
        //}
        //private async Task<(long, int)> getBigID(int x)
        //{
        //    long id_c = 0;
        //    TimeSpan expiration = TimeSpan.FromSeconds(5);
        //    string lockKey = "lock:eat";
        //    string key_count = "increatment:codeid:lender";
        //    var db = Connection.GetDatabase();
        //    RedisValue token = Environment.MachineName;
        //    bool wait = true;
        //    while (wait)
        //    {
        //        if (db.LockTake(lockKey, token, expiration))
        //        {
        //            try
        //            {
        //                // you have the lock do work
        //                var values = await db.StringGetAsync(new RedisKey[] { key_count });

        //                if (values == null || !values.Any())
        //                {
        //                    id_c = 1;
        //                    await db.StringSetAsync(key_count, $"{id_c}");
        //                }
        //                else
        //                {
        //                    id_c = Convert.ToInt64(values.First());
        //                    id_c += 1;
        //                    await db.StringSetAsync(key_count, $"{id_c}");
        //                }
        //            }
        //            finally
        //            {
        //                db.LockRelease(lockKey, token);
        //                wait = false;
        //            }
        //        }
        //    }

        //    return (id_c, x);
        //}

        #endregion
        public async Task<long> GetLenderCodeIDNext()
        {
            long lenderCodeIDNext = 0;
            TimeSpan expiration = TimeSpan.FromSeconds(10);
            string lockKey = "lock:lendercodeid";
            var db = this.RedisConnection.GetDatabase();
            RedisValue token = Environment.MachineName;
            bool wait = true;
            while (wait)
            {
                if (db.LockTake(lockKey, token, expiration))
                {
                    try
                    {
                        // you have the lock do work
                        var values = await db.StringGetAsync(new RedisKey[] { RuleLenderServiceHelper.KeyRedistLenderCodeIDNext });
                        if (values == null || !values.Any())
                        {
                            lenderCodeIDNext = 0;
                        }
                        else
                        {
                            lenderCodeIDNext = Convert.ToInt64(values.First());
                        }
                        if (lenderCodeIDNext == 0)
                        {
                            var users = await _userTab.SetGetTop(1).SelectColumns(x => x.UserName).WhereClause(x => x.UserName.Contains(TimaSettingConstant.UserNameLenderConfig)).OrderByDescending(x => x.UserName).QueryAsync();
                            if (users != null && users.Any())
                            {
                                long lenderCode = long.Parse(users.First().UserName.Replace(TimaSettingConstant.UserNameLenderConfig, ""));
                                lenderCodeIDNext = lenderCode;
                            }
                            else
                            {
                                lenderCodeIDNext = 1;
                            }
                        }
                        lenderCodeIDNext += 1;
                        await db.StringSetAsync(RuleLenderServiceHelper.KeyRedistLenderCodeIDNext, $"{lenderCodeIDNext}");
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"GetLenderCodeIDNext|ex={ex.Message}-{ex.StackTrace}");
                    }
                    finally
                    {
                        db.LockRelease(lockKey, token);
                        wait = false;
                    }
                }
            }
            return lenderCodeIDNext;
        }
    }
}
