﻿namespace LMS.LenderServiceApi.Domain.Models
{
    public class PaymentScheduleDepositMoneyView : Tables.TblPaymentScheduleDepositMoney
    {
        public string StatusName { get; set; }
        public long TotalCount { get; set; }
        public string LenderName { get; set; }
        public string AccountBanking { get; set; }
        public string BankName { get; set; }
        public long Tax
        {
            get
            {
                return InterestMoney - InterestAfterTax;
            }
        }
    }
}
