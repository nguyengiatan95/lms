﻿using System;

namespace LMS.LenderServiceApi.Domain.Models
{
    public class AffView
    {
        public long AffID { get; set; }
        public string AffName { get; set; }
        public string Code { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public string NumberCard { get; set; }

        public string Address { get; set; }

        public DateTime? BirthDay { get; set; }

        public short? Source { get; set; }

        public DateTime? ContractDate { get; set; }
        public int? Status { get; set; }
        public string StrStatus { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string BankNumber { get; set; }

        public long? BankID { get; set; }

        public string BankPlace { get; set; }

        public string CodeNumber { get; set; }

        public DateTime? FinishContractDate { get; set; }

        public double? PercentComission { get; set; }
        public string TaxNumber { get; set; }
        public long TotalLender { get; set; }
        public string StaffName { get; set; }
        public long StaffID { get; set; }
    }
}
