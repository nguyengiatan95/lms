﻿namespace LMS.LenderServiceApi.Domain.Models
{
    public class LenderStaff
    {
        public long UserID { get; set; }
        public string Name { get; set; }
    }
}
