﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Domain.Models
{
    public class LenderUserView
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string NumberCard { get; set; }
        public string Phone { get; set; }
        public string CreateDate { get; set; }
        public int Status { get; set; }
        public int IsVerified { get; set; }
        public long UserID { get; set; }
        public int? Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Email { get; set; }
        public string PermanentResidenceAddress { get; set; }
        public string TemporaryResidenceAddress { get; set; }
        public long CityID { get; set; }
        public long DistrictID { get; set; }
        public long WardID { get; set; }
        public DateTime? CardDate { get; set; }
        public string CardPlace { get; set; }
    }
}
