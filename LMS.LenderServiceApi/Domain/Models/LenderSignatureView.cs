﻿namespace LMS.LenderServiceApi.Domain.Models
{
    public class LenderSignatureView
    {
        public long LenderID { get; set; }
        public string Signature { get; set; }
        public string Password { get; set; }
    }
}
