﻿using System;
using System.Collections.Generic;

namespace LMS.LenderServiceApi.Domain.Models
{
    public class PrintCommentContract
    {
        /// <summary>
        /// Tên KH
        /// </summary>
        public string CustomerName { get; set; }
        /// <summary>
        /// Ngày sinh
        /// </summary>
        public string BirthDay { get; set; }

        /// <summary>
        /// CMND
        /// </summary>
        public string NumberCard { get; set; }

        /// <summary>
        /// địa chỉ TT
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// địa chỉ HK
        /// </summary>
        public string AddressHouseHold { get; set; }

        /// <summary>
        /// Địa chỉ làm việc
        /// </summary>
        public string AddressJob { get; set; }

        /// <summary>
        /// Nghề nghiệp
        /// </summary>
        public string Job { get; set; }

        /// <summary>
        /// Điện thoại KH
        /// </summary>
        public string CustomerPhone { get; set; }

        /// <summary>
        /// Điện thoại công ty
        /// </summary>
        public string CompanyPhone { get; set; }
        /// <summary>
        /// Tên công ty
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Địa chỉ công ty
        /// </summary>
        public string CompanyAddress { get; set; }


        /// <summary>
        /// MÃ HĐ
        /// </summary>
        public string ContractCode { get; set; }



        /// <summary>
        /// Ngày giải ngân
        /// </summary>
        public string FromDate { get; set; }

        /// <summary>
        /// Số ngày vay
        /// </summary>
        public int LoanTime { get; set; }


        /// <summary>
        /// Khoản vay
        /// </summary>
        public long TotalMoney { get; set; }

        /// <summary>
        /// Gốc còn lại
        /// </summary>
        public long TotalMoneyCurrent { get; set; }


        /// <summary>
        /// Thanh toán gần nhất
        /// </summary>
        public DateTime? LastdateOfPay { get; set; }

        /// <summary>
        /// Số ngày quá hạn
        /// </summary>
        public long CountDay { get; set; }

        /// <summary>
        /// Số tiền quá hạn
        /// </summary>
        public long InterestMoney { get; set; }
        /// <summary>
        /// Mã đối tác
        /// </summary>
        public string LenderCode { get; set; }

        /// <summary>
        /// SĐT đối tác
        /// </summary>
        public string LenderPhone { get; set; }

        public List<CommentView> CommentViews { get; set; }

        /// <summary>
        /// SDT người thân
        /// </summary>
        public string FamilyPhone { get; set; }
        /// <summary>
        /// mối quan hệ ng thân
        /// </summary>
        public string RelativeName { get; set; }
        /// <summary>
        /// Tên ng thân
        /// </summary>
        public string FamilyName { get; set; }
        /// <summary>
        /// SĐT đồng nghiệp
        /// </summary>
        public string ColleaguePhone { get; set; }
        /// <summary>
        /// Tên đồng nghiệp
        /// </summary>
        public string ColleagueName { get; set; }


    }
    public class CommentView
    {
        /// <summary>
        /// Người thao tác
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// Thời gian
        /// </summary>
        public DateTime CreateDate { get; set; }
        /// <summary>
        /// Nội dung
        /// </summary>
        public string Comment { get; set; }
    }
}
