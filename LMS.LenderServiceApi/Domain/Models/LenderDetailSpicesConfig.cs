﻿using System.Collections.Generic;

namespace LMS.LenderServiceApi.Domain.Models
{
    public class LenderDetailSpicesConfig
    {
        public List<int> LoanTimes { get; set; }
        public int RateType { get; set; }
        public int RangeMoney { get; set; }
        public long UserID { get; set; }
        public long LenderSpicesConfigID { get; set; }
        public long LenderID { get; set; }
    }
}
