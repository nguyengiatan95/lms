﻿namespace LMS.LenderServiceApi.Domain.Models
{
    public class SendMail
    {
        public string Email { get; set; }
        public string Department { get; set; }
        public string Domain { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
