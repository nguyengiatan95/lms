﻿namespace LMS.LenderServiceApi.Domain.Models
{
    public class LenderStatusDeposit
    {
        public long LoanID { get; set; }
        public int Status { get; set; }
    }

    public class RequestExcelLenderDeposit
    {
        public string ContractCode { get; set; }
        public int Status { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string CustomerName { get; set; }
        public int LenderID { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }

    public class TimaLenderStatusDeposit
    {
        public long TimaLoanID { get; set; }
        public int Status { get; set; }
    }
    public class UpdateDepositLoanMoney
    {

        public string LoanCode { get; set; }

        public int LoanID { get; set; }
    }
}
