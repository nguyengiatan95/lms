﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Domain.App
{
    public enum Lender_LoanSearch_DisbursementType
    {
        [Description("Ngày giải ngân")]
        DisbursementDate = 1,
        [Description("Ngày tất toán")]
        FinishDate = 2,
    }

    public enum Lender_LoanSearch_Status
    {
        [Description("Đang vay")]
        Lending = 1,
        [Description("Chậm trả")]
        BadDebt = 2,
        [Description("Chờ bảo hiểm")]
        WaitInsurance = 3,
        [Description("Kết thúc")]
        Closed = 4,
    }
}
