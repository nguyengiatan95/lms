﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Domain.UserLender
{
    public class AffUserLenderContractItem
    {
        public string LenderFullName { get; set; }
        public string AffCode { get; set; }
        public string AffFullName { get; set; }
        public string LenderPhone { get; set; }
        public string StrStartDate { get; set; }
        public int NumberContract { get; set; }
        public int LenderVerify { get; set; }
        public string LenderVerifyName { get; set; }
        public string StrCreateDate { get; set; }
        public int TotalLenderRegisterByLink { get; set; }
        public int TotalLenderHasContract { get; set; }
        public DateTime StartDate { get; set; }
    }
}
