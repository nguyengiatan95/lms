﻿using System;

namespace LMS.LenderServiceApi.Domain.UserLender
{
    public class GetContractByUserLender
    {
        public long UserID { get; set; }
        public long LenderID { get; set; }
        public string ContractCode { get; set; }
        public DateTime ContractDate { get; set; }
        public DateTime ContractEndDate { get; set; }
        public int ContractType { get; set; }
        public string TypeLoan { get; set; }
        public string LenderCode { get; set; }
        public int ContractDuration { get; set; }
        public long TotalMoney { get; set; }
        public int IsDeposit { get; set; }
        public long AffID { get; set; }
    }
}
