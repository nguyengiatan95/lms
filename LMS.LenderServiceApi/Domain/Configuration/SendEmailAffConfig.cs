﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Domain.Configuration
{
    public class SendEmailAffConfig
    {
        public string Title { get; set; }
        public string Body { get; set; }
    }

    public class AffLink
    {
        public string Url { get; set; }

    }
}
