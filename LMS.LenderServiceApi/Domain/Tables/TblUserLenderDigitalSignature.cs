﻿using Dapper.Contrib.Extensions;
using System;

namespace LMS.LenderServiceApi.Domain.Tables
{
    [Table("TblUserLenderDigitalSignature")]
    public class TblUserLenderDigitalSignature
    {
        [Key]
        public long UserLenderDigitalSignatureID { get; set; }

        public long? UserID { get; set; }

        public string AgreementUUID { get; set; }

        public string PassCode { get; set; }

        public int Status { get; set; }

        public long CreateBy { get; set; }

        public DateTime CreateDate { get; set; }
        public long LenderID { get; set; }
    }
}
