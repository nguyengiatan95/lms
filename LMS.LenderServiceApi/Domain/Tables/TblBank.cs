﻿using Dapper.Contrib.Extensions;

namespace LMS.LenderServiceApi.Domain.Tables
{
    [Table("TblBank")]
    public class TblBank
    {
        [Key]
        public int Id { get; set; }

        public string NameBank { get; set; }

        public string Code { get; set; }

        public bool? Status { get; set; }

        public short? TypeBank { get; set; }

        public int? ValueOfBank { get; set; }
    }

}
