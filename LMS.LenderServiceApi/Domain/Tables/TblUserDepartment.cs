﻿using System;

namespace LMS.LenderServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblUserDepartment")]
    public class TblUserDepartment
    {
        [Dapper.Contrib.Extensions.Key]
        public long UserDepartmentID { get; set; }
        public long UserID { get; set; }
        public long DepartmentID { get; set; }
        public long PositionID { get; set; }
        public int Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public long ParentUserID { get; set; }
        public long ChildDepartmentID { get; set; }
    }
}