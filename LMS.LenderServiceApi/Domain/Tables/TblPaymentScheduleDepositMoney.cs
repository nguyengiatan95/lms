﻿using Dapper.Contrib.Extensions;
using System;

namespace LMS.LenderServiceApi.Domain.Tables
{
    [Table("TblPaymentScheduleDepositMoney")]
    public class TblPaymentScheduleDepositMoney
    {
        [Key]
        public long PaymentScheduleDepositMoneyID { get; set; }

        public long LoanID { get; set; }

        public long LenderID { get; set; }

        public long CustomerID { get; set; }

        public string CustomerName { get; set; }

        public string LenderCode { get; set; }

        public string ContractCode { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public long TotalMoney { get; set; }

        public long TotalMoneyCurrent { get; set; }

        public long InterestMoney { get; set; }

        public long TotalMoneyDeposit { get; set; }

        public int Status { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? LastPaySchedule { get; set; }
        public DateTime EndDaySchedule { get; set; }
        public long CountDay { get; set; }
        public long InterestAfterTax { get; set; }
        public long TotalMoneyDepositAfterTax { get; set; }
        public DateTime NextDate { get; set; }
        public DateTime? Modifydate { get; set; }



    }
    [Table("TblLogActionPaymentDepositMoney")]
    public class TblLogActionPaymentDepositMoney
    {
        [Key]
        public long LogActionPaymentDepositMoneyID { get; set; }

        public string Note { get; set; }

        public long? PaymentScheduleDepositMoneyID { get; set; }

        public long? LoanID { get; set; }

        public DateTime? CreateDate { get; set; }

        public long? UserID { get; set; }
    }

}
