﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblLender")]
    public class TblLender
    {
        [Key]
        public long LenderID { get; set; }

        //[Dapper.Contrib.Extensions.Key]
        //public System.Byte[] Version { get; set; }

        public string FullName { get; set; }

        public string LenderCode { get; set; }

        public string ContractCode { get; set; }

        public DateTime? ContractDate { get; set; }

        public DateTime? ContractEndDate { get; set; }
        public int? ContractDuration { get; set; }
        public int ContractType { get; set; }
        public long? TotalMoneyTopup { get; set; }

        public decimal? RateLender { get; set; }

        public decimal? RateInterest { get; set; }

        public long TotalMoney { get; set; }

        public long? MoneyAvailable { get; set; }

        public string NumberCard { get; set; }

        public DateTime? BirthDay { get; set; }

        public int? Gender { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public string TaxCode { get; set; }

        public int? Status { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? ModifyDate { get; set; }

        public long? TimaLenderID { get; set; }

        public int IsHub { get; set; }

        public short? IsBuyInsurance { get; set; }

        public int IsVerified { get; set; }

        public string AddressOfResidence { get; set; }

        public DateTime? CardDate { get; set; } = DateTime.Now;

        public string CardPlace { get; set; }

        public string Email { get; set; }

        public decimal? RateAff { get; set; }

        public string Represent { get; set; }

        public string AccountBanking { get; set; }

        public long? BankID { get; set; }

        public long? LenderCareUserID { get; set; }

        public long? LenderTakeCareUserID { get; set; }

        public long? AffID { get; set; }

        public string RepresentPhone { get; set; }

        public string BranchOfBank { get; set; }

        public string InvitePhone { get; set; }

        public int IsGcash { get; set; }
        public int RegFromApp { get; set; }
        public int UnitRate { get; set; }
        public long? MoneyHold { get; set; }
        public int SelfEmployed { get; set; }
        public long OwnerUserID { get; set; }
        public int IsDeposit { get; set; }
    }
}

