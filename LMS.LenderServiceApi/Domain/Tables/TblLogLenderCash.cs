﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblLogLenderCash")]
    public class TblLogLenderCash
    {
        [Dapper.Contrib.Extensions.Key]
        public long LogLenderCashID { get; set; }

        public long LenderID { get; set; }

        public long MoneyBefore { get; set; }

        public long MoneyAfter { get; set; }

        public long MoneyAdd { get; set; }

        public DateTime CreateDate { get; set; }

        public string Description { get; set; }
    }
}
