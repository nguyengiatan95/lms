﻿using Dapper.Contrib.Extensions;
using System;

namespace LMS.LenderServiceApi.Domain.Tables
{
    [Table("TblLogLenderChangeInfo")]
    public class TblLogLenderChangeInfo
    {
        [Key]
        public long LogLenderChangeInfoID { get; set; }

        public long? UserID { get; set; }

        public long? CreateBy { get; set; }

        public string DataOld { get; set; }

        public DateTime CreateDate { get; set; }

        public long LogTypeID { get; set; }
    }
}
