﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Domain.Tables
{
    [Table("TblCollaboratorLender")]
    public class TblCollaboratorLender
    {
        [Key]
        public long CollaboratorLenderID { get; set; }

        public string FullName { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime CreateDate { get; set; }

        public string CodeCollaborator { get; set; }

        public string Note { get; set; }

        public int Status { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int ModifyByUser { get; set; }
    }
}
