﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblDebt")]
    public class TblDebt
    {
        [Dapper.Contrib.Extensions.Key]
        public long DebtID { get; set; }
        /// <summary>
        /// loanID, invoiceID
        /// </summary>
        public long ReferID { get; set; }

        public int TypeID { get; set; }

        public long TargetID { get; set; }

        public long TotalMoney { get; set; }

        public string Description { get; set; }

        public int Status { get; set; }

        public DateTime CreateDate { get; set; }

        public long? CreateBy { get; set; }
        /// <summary>
        /// paymentID, transactionID
        /// </summary>
        public long SourceID { get; set; }
        /// <summary>
        /// Hủy: DebtID, DebtID child -> chuyển trạng thái = hủy
        /// </summary>
        public long ParentDebtID { get; set; }
    }
}
