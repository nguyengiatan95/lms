﻿using Dapper.Contrib.Extensions;
using System;

namespace LMS.LenderServiceApi.Domain.AG
{
    [Table("TblFptEContract")]
    public class TblFptEContract
    {
        [Key]
        public long FptEContractID { get; set; }

        public int LenderId { get; set; }

        public string AgreementId { get; set; }

        public string Passcode { get; set; }

        public DateTime? CreateDate { get; set; }

        public int? Status { get; set; }

        public DateTime? ModifyDate { get; set; }

        public int? UserId { get; set; }

    }
}
