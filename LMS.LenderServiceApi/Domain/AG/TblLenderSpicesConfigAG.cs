﻿using Dapper.Contrib.Extensions;

namespace LMS.LenderServiceApi.Domain.AG
{
    [Table("tblLenderSpicesConfig")]
    public class TblLenderSpicesConfigAG
    {
        public int ShopId { get; set; }

        public string LoanTimes { get; set; }

        public int? RateType { get; set; }

        public long MoneyMin { get; set; }

        public long MoneyMax { get; set; }

    }
}
