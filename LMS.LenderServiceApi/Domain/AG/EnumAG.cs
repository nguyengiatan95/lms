﻿using System.ComponentModel;

namespace LMS.LenderServiceApi.Domain.AG
{
    public enum Base_Status_AG
    {
        [Description("Chưa hoàn thành")]
        NotComplete = 0,
        [Description("Hoàn thành")]
        Complete = 1,
        [Description("Đã xóa")]
        Deleted = -1
    }
}
