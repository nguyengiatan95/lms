﻿namespace LMS.LenderServiceApi.Domain.CollaboratorLender
{
    public class CollaboratorLenderDetails : Domain.Tables.TblCollaboratorLender
    {
        public string AffName { get; set; }
    }
}
