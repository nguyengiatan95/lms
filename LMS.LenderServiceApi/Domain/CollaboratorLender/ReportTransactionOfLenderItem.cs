﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Domain.CollaboratorLender
{
    public class ReportTransactionOfLenderItem
    {
        public string AffCode { get; set; }
        public string AffFullName { get; set; }
        public string LenderFullName { get; set; }
        public string LenderPhone { get; set; }
        public int ContractType { get; set; }
        public string ContractTypeName { get; set; }
        public string StrTransactionDate { get; set; }
        public long MoneyDisbursement { get; set; }
        public long MoneyTopup { get; set; }
        public float CommissionRate { get; set; } = 1;
        public long MoneyCommission { get; set; } = 0;
        public long TotalMoneyDisbursement { get; set; } = 0;
        public long TotalMoneyTopup { get; set; } = 0;
        public long TotalCommissionMoney { get; set; } = 0;
        public DateTime TransactionDate { get; set; }
    }
}
