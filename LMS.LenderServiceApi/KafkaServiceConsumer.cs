﻿using LMS.Kafka.Interfaces;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi
{
    public class KafkaServiceConsumer : BackgroundService
    {
        readonly IKafkaConsumer<string, LMS.Kafka.Messages.Loan.LoanInsertSuccess> _loanCustomerConsumer;
        readonly IKafkaConsumer<string, LMS.Kafka.Messages.Loan.LoanCutPeriodSuccessInfo> _consumLoanCutPeriodHandler;
        readonly ILogger<KafkaServiceConsumer> _logger;
        public KafkaServiceConsumer(IKafkaConsumer<string, LMS.Kafka.Messages.Loan.LoanInsertSuccess> LoanCustomerConsumer,
            IKafkaConsumer<string, LMS.Kafka.Messages.Loan.LoanCutPeriodSuccessInfo> consumLoanCutPeriodHandler,
            ILogger<KafkaServiceConsumer> logger)
        {
            _loanCustomerConsumer = LoanCustomerConsumer;
            _consumLoanCutPeriodHandler = consumLoanCutPeriodHandler;
            _logger = logger;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                var t1 = _loanCustomerConsumer.Consume(Kafka.Constants.KafkaTopics.LoanCreateSuccess, stoppingToken);
                var t2 = _consumLoanCutPeriodHandler.Consume(Kafka.Constants.KafkaTopics.LoanCutPeriodSuccess, stoppingToken);
                await Task.WhenAll(t1, t2);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{(int)HttpStatusCode.InternalServerError} LMS.LenderServiceApi_KafkaServiceConsumer|ex={ex.Message}-{ex.StackTrace}");
            }
        }

        public override void Dispose()
        {
            _loanCustomerConsumer.Close();
            _loanCustomerConsumer.Dispose();
            _consumLoanCutPeriodHandler.Close();
            _consumLoanCutPeriodHandler.Dispose();

            base.Dispose();
        }
    }
}
