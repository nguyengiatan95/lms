﻿using LMS.Kafka.Interfaces;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.KafkaEventHandlers
{
    public class ConsumLoanCutPeriodSuccessHandler : IKafkaHandler<string, LMS.Kafka.Messages.Loan.LoanCutPeriodSuccessInfo>
    {
        private readonly IMediator _bus;
        LMS.Common.Helper.Utils _common = new LMS.Common.Helper.Utils();
        ILogger<ConsumLoanCutPeriodSuccessHandler> _logger;
        public ConsumLoanCutPeriodSuccessHandler(IMediator bus, ILogger<ConsumLoanCutPeriodSuccessHandler> logger)
        {
            _bus = bus;
            _logger = logger;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> HandleAsync(string key, LMS.Kafka.Messages.Loan.LoanCutPeriodSuccessInfo value)
        {
            _logger.LogInformation($"LMS.LenderServiceApi_ConsumLoanCutPeriodSuccessHandler_Waring|value={_common.ConvertObjectToJSonV2(value)}|topic={LMS.Kafka.Constants.KafkaTopics.LoanCutPeriodSuccess}");
            var request = new Commands.PaymentScheduleDepositMoney.DeleteByLoanCutPeriodsCommand
            {
                LoanID = value.LoanID
            };
            return await _bus.Send(request);
        }
    }
}
