﻿using LMS.Common.Constants;
using LMS.Kafka.Interfaces;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.KafkaEventHandlers
{
    public class CreateLenderDebtByLoanIDHandler : IKafkaHandler<string, LMS.Kafka.Messages.Loan.LoanInsertSuccess>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        private readonly IMediator _bus;
        LMS.Common.Helper.Utils _common = new Common.Helper.Utils();
        ILogger<CreateLenderDebtByLoanIDHandler> _logger;
        public CreateLenderDebtByLoanIDHandler(IMediator bus,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            ILogger<CreateLenderDebtByLoanIDHandler> logger)
        {
            _bus = bus;
            _loanTab = loanTab;
            _logger = logger;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> HandleAsync(string key, LMS.Kafka.Messages.Loan.LoanInsertSuccess value)
        {
            LMS.Common.Constants.ResponseActionResult response = new ResponseActionResult();
            try
            {
                var loanInfos = await _loanTab.WhereClause(x => x.LoanID == value.LoanID).QueryAsync();
                if (loanInfos == null || !loanInfos.Any())
                {
                    _logger.LogError($"GenPaymentScheduleByLoanIDHandler|value={_common.ConvertObjectToJSonV2(value)}|Not_found_loan");
                    return response;
                }
                var loanInfo = loanInfos.First();
                List<int> lstBankLenderDebt = new List<int> { (int)Loan_SourceMoneyDisbursement.BankNamAOfLender, (int)Loan_SourceMoneyDisbursement.OtherBank, (int)Loan_SourceMoneyDisbursement.VIBOfLender };
                if (!lstBankLenderDebt.Contains(loanInfo.SourceMoneyDisbursement))
                {
                    return response;
                }
                Commands.CreateLenderDebtCommand request = new Commands.CreateLenderDebtCommand
                {
                   Model = new Entites.Dtos.LenderService.CreateLenderDebtRequest
                   {
                       CreateBy = value.CreateBy,
                       DebtType = (int)Debt_TypeID.LenderKeepInsurance,
                       Description = $"Giữ tiền bảo hiểm đơn vay {loanInfo.ContactCode}",
                       LenderID = loanInfo.LenderID,
                       ReferID = loanInfo.LoanID,
                       TotalMoney = loanInfo.MoneyFeeInsuranceOfCustomer + loanInfo.MoneyFeeInsuranceMaterialCovered
                   }
                };
                response = await _bus.Send(request);
                
            }
            catch (Exception ex)
            {
                _logger.LogError($"GenPaymentScheduleByLoanIDHandler|value={_common.ConvertObjectToJSonV2(value)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
