﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class RunPaymentScheduleDepositMoneyCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {

    }
    public class RunPaymentScheduleDepositMoneyCommandHandler : IRequestHandler<RunPaymentScheduleDepositMoneyCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentScheduleDepositMoney> _paymentScheduleDepositMoneyTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        readonly ILogger<RunPaymentScheduleDepositMoneyCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        RedisCachedServiceHelper _redisCachedService;
        public RunPaymentScheduleDepositMoneyCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentScheduleDepositMoney> paymentScheduleDepositMoneyTab,
               LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
               LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
               LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            ILogger<RunPaymentScheduleDepositMoneyCommandHandler> logger)
        {
            _paymentScheduleDepositMoneyTab = paymentScheduleDepositMoneyTab;
            _paymentScheduleTab = paymentScheduleTab;
            _loanTab = loanTab;
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(RunPaymentScheduleDepositMoneyCommand request, CancellationToken cancellationToken)
        {
            //Được giải ngân từ ngày 1 / 1 / 2021
            //Phải là của các nhà đầu tư(Lender) có ký phụ lục tham gia chính sách đặt cọc
            //Đến hạn 90 ngày khách hàng chưa thanh toán kể từ ngày phát sinh nghĩa vụ phải trả
            ResponseActionResult response = new ResponseActionResult();
            DateTime dtNow = DateTime.Now;
            try
            {
                List<Domain.Tables.TblPaymentScheduleDepositMoney> lstDepositMoney = new List<Domain.Tables.TblPaymentScheduleDepositMoney>();
                var lenders = await _lenderTab.SelectColumns(x => x.LenderID, x => x.FullName).WhereClause(x => x.IsDeposit == (int)Lender_DepositType.Deposit).QueryAsync();
                if (lenders == null || !lenders.Any())
                {
                    response.SetSucces();
                    response.Data = null;
                }
                DateTime dtSearch = new DateTime(2021, 01, 01);
                DateTime dtNextDate = DateTime.Now.AddDays(-90);

                var lenderIDs = lenders.Select(x => x.LenderID).ToList();
                var dicLender = lenders.ToDictionary(x => x.LenderID, x => x.FullName);
                var loans = await _loanTab.WhereClause(x => x.FromDate >= dtSearch && x.NextDate <= dtNextDate && x.Status == (int)Loan_Status.Lending && lenderIDs.Contains(x.LenderID)).QueryAsync();

                int pageSize = 1000;
                int numberRow = loans.Count() / pageSize + 1;

                for (int i = 1; i <= numberRow; i++)
                {
                    var loanskip = loans.Skip((i - 1) * pageSize).Take(pageSize).ToList();
                    var loanIds = loanskip.Select(x => x.LoanID);
                    //check đã có trong dữ liệu
                    var loanPaymentScheduleDepositMoney = await _paymentScheduleDepositMoneyTab.SelectColumns(x => x.LoanID).WhereClause(x => loanIds.Contains(x.LoanID)
                    && x.Status != (int)PaymentScheduleDepositMoney_Status.Cancel).QueryAsync();
                    var loanIDPaymentScheduleDepositMoney = loanPaymentScheduleDepositMoney.Select(x => x.LoanID);
                    loanIds = loanIds.Where(x => !loanIDPaymentScheduleDepositMoney.Contains(x));
                    var lstPaymentScheduleLoan = await _paymentScheduleTab.WhereClause(m => m.Status == (int)PaymentSchedule_Status.Use && m.IsComplete <= (int)PaymentSchedule_IsComplete.Paid && loanIds.Contains(m.LoanID)).QueryAsync();

                    foreach (var item in loanskip)
                    {

                        var lstPaymentSchedule = lstPaymentScheduleLoan.Where(x => x.LoanID == item.LoanID).ToList();
                        var countPays = lstPaymentSchedule.Where(x => x.IsVisible == false).Count();
                        if (countPays <= 1)
                        {
                            continue;
                        }
                        try
                        {
                            var LastDateOfPayBeforeDeposit = lstPaymentSchedule.Where(m => m.PayDate == item.NextDate).FirstOrDefault();

                            DateTime BeginDateDeposit = LastDateOfPayBeforeDeposit.ToDate;

                            if (LastDateOfPayBeforeDeposit.PayDate < LastDateOfPayBeforeDeposit.ToDate)
                            {
                                BeginDateDeposit = LastDateOfPayBeforeDeposit.PayDate;
                            }
                            var dateMoneyInterestDeposit = BeginDateDeposit.AddDays(90);
                            long numberDayOver = (long)(dateMoneyInterestDeposit - (item.LastDateOfPay ?? item.FromDate)).TotalDays;
                            long interestMoney = this.GetMoneyInterestDeposit(dateMoneyInterestDeposit, item.TotalMoneyDisbursement, Convert.ToDecimal(item.RateInterest), lstPaymentSchedule);
                            lstDepositMoney.Add(new Domain.Tables.TblPaymentScheduleDepositMoney()
                            {
                                ContractCode = item.ContactCode,
                                LenderCode = dicLender.GetValueOrDefault(item.LenderID),
                                LenderID = item.LenderID,
                                CreateDate = dtNow,
                                CustomerID = (long)item.CustomerID,
                                CustomerName = item.CustomerName,
                                FromDate = item.FromDate,
                                ToDate = item.ToDate,
                                InterestMoney = interestMoney,
                                LoanID = item.LoanID,
                                TotalMoneyCurrent = item.TotalMoneyCurrent,
                                Status = (int)PaymentScheduleDepositMoney_Status.DefaultWaittingRecordDeposit,
                                TotalMoney = item.TotalMoneyDisbursement,
                                TotalMoneyDeposit = item.TotalMoneyCurrent + interestMoney,
                                CountDay = numberDayOver,
                                EndDaySchedule = dateMoneyInterestDeposit,
                                LastPaySchedule = item.LastDateOfPay,
                                InterestAfterTax = (long)(interestMoney * 0.98),
                                NextDate = item.NextDate.Value,
                                TotalMoneyDepositAfterTax = item.TotalMoneyCurrent + (long)(interestMoney * 0.98),
                            });
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError($"RunPaymentScheduleDepositMoneyCommandHandler|item={_common.ConvertObjectToJSonV2(item)}|ex={ex.Message}-{ex.StackTrace}");
                        }
                    }
                }
                if (lstDepositMoney != null && lstDepositMoney.Any()) _paymentScheduleDepositMoneyTab.InsertBulk(lstDepositMoney);
                response.SetSucces();
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"RunPaymentScheduleDepositMoneyCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private long GetMoneyInterestDeposit(DateTime dateMoneyInterestDeposit, long totalMoneyDisbursement, decimal rateInterest, List<Domain.Tables.TblPaymentSchedule> lstPaymentSchedules)
        {
            long totalMoneyInterest = 0;
            long totalMoneyCurrentPaid = 0;
            long rateLMS = 1000;
            long numberDayOver = 0;
            var lstPaymentSchedulesBeforeDeposit = lstPaymentSchedules.Where(m => m.PayDate <= dateMoneyInterestDeposit).ToList();
            // tính tiền còn thiếu của các kỳ trước còn thiếu
            foreach (var item in lstPaymentSchedulesBeforeDeposit)
            {
                totalMoneyInterest += item.MoneyInterest - item.PayMoneyInterest;
                totalMoneyCurrentPaid += item.MoneyOriginal;
            }

            var objPaymentScheduleNoDoneLast = lstPaymentSchedules.Where(m => m.Status == (int)PaymentSchedule_Status.Use && m.IsVisible == false && m.PayDate > dateMoneyInterestDeposit).OrderBy(m => m.PayDate).FirstOrDefault();
            //var objPaymentScheduleNoDoneLast = lstPaymentScheduleNoDone.FirstOrDefault();

            if (objPaymentScheduleNoDoneLast != null && objPaymentScheduleNoDoneLast.PaymentScheduleID > 0)
            {
                numberDayOver = dateMoneyInterestDeposit.Subtract(objPaymentScheduleNoDoneLast.FromDate).Days + 1;
                //if (dateMoneyInterestDeposit == objPaymentScheduleNoDoneLast.FromDate.Value)
                //{
                //    numberDayOver++;
                //}
            }
            // tính lãi phát sinh của các ngày còn thiếu

            if (numberDayOver > 0)
            {
                var totalMoneyCurrentLast = totalMoneyDisbursement - totalMoneyCurrentPaid;
                totalMoneyInterest += Convert.ToInt64(Math.Round(totalMoneyCurrentLast * numberDayOver * rateInterest / (1000 * rateLMS), 0)) - objPaymentScheduleNoDoneLast.PayMoneyInterest;
            }

            return totalMoneyInterest;

        }

    }
}
