﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.ProxyTima;
using LMS.Entites.Dtos.SendMail;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
namespace LMS.LenderServiceApi.Commands.CollaboratorLender
{
    public class CreateCollaboratorLenderCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required(ErrorMessage = "Chưa nhập tên cộng tác viên", AllowEmptyStrings = true)]
        public string FullName { get; set; }
        [Required(ErrorMessage = "Chưa nhập SĐT (Phone)", AllowEmptyStrings = true)]
        public string Phone { get; set; }
        public string CodeCollaborator { get; set; }
    }
    public class CreateCollaboratorLenderCommandHandler : IRequestHandler<CreateCollaboratorLenderCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCollaboratorLender> _collaboratorLenderTab;
        readonly ILogger<CreateCollaboratorLenderCommandHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public CreateCollaboratorLenderCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCollaboratorLender> collaboratorLenderTab,
            ILogger<CreateCollaboratorLenderCommandHandler> logger)
        {
            _collaboratorLenderTab = collaboratorLenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateCollaboratorLenderCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                string message = string.Empty;
                bool value = _common.CheckValidInputString<CreateCollaboratorLenderCommand>(request, ref message);
                if (!value)
                {
                    response.Message = message;
                    return response;
                }
                //Số điện thoại: chỉ cho phép nhập số, có validate trường này.
                if (!(request.Phone.Length == TimaSettingConstant.MaxLengthPhone && Regex.Match(request.Phone, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success))
                {
                    response.Message = MessageConstant.UserLender_PhoneInvalid;
                    return response;
                }
                var collaboratorLender = (await _collaboratorLenderTab.WhereClause(x => x.PhoneNumber == request.Phone).QueryAsync()).FirstOrDefault();
                if (collaboratorLender != null)
                {
                    response.Message = MessageConstant.ErrorExist;
                    return response;
                }
                var objCollaboratorLender = new Domain.Tables.TblCollaboratorLender()
                {
                    FullName = request.FullName,
                    PhoneNumber = request.Phone,
                    CreateDate = DateTime.Now,
                    CodeCollaborator = request.CodeCollaborator,
                    Status = (int)CollaboratorLender_Status.WaitApprove
                };
                var collaboratorLenderID = await _collaboratorLenderTab.InsertAsync(objCollaboratorLender);
                if (collaboratorLenderID > 0)
                {
                    response.SetSucces();
                    response.Data = collaboratorLenderID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateCollaboratorLenderCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
