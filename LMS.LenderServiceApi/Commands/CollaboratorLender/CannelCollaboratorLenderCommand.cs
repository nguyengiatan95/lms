﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.ProxyTima;
using LMS.Entites.Dtos.SendMail;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
namespace LMS.LenderServiceApi.Commands.CollaboratorLender
{
    public class CannelCollaboratorLenderCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long CollaboratorLenderID { get; set; }
        public string Note { get; set; }
        public int UserID { get; set; }
    }
    public class CannelCollaboratorLenderCommandHandler : IRequestHandler<CannelCollaboratorLenderCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCollaboratorLender> _collaboratorLenderTab;
        readonly ILogger<CannelCollaboratorLenderCommandHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public CannelCollaboratorLenderCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCollaboratorLender> collaboratorLenderTab,
            ILogger<CannelCollaboratorLenderCommandHandler> logger)
        {
            _collaboratorLenderTab = collaboratorLenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CannelCollaboratorLenderCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var collaboratorLender = (await _collaboratorLenderTab.WhereClause(x => x.CollaboratorLenderID == request.CollaboratorLenderID).QueryAsync()).FirstOrDefault();
                if (collaboratorLender == null)
                {
                    response.Message = MessageConstant.ErrorNotExist;
                    return response;
                }
                if (collaboratorLender.Status == (int)CollaboratorLender_Status.Approved)
                {
                    response.Message = MessageConstant.CollaboratorLenderApproved;
                    return response;
                }
                if (!string.IsNullOrEmpty(request.Note))
                    collaboratorLender.Note = request.Note;
                collaboratorLender.Status = (int)CollaboratorLender_Status.Cannel;
                collaboratorLender.ModifyDate = DateTime.Now;
                collaboratorLender.ModifyByUser = request.UserID;
                if (await _collaboratorLenderTab.UpdateAsync(collaboratorLender))
                {
                    response.SetSucces();
                    response.Data = request.CollaboratorLenderID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CannelCollaboratorLenderCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
