﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.ProxyTima;
using LMS.Entites.Dtos.SendMail;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
namespace LMS.LenderServiceApi.Commands.CollaboratorLender
{
    public class ApprovedCollaboratorLenderCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long CollaboratorLenderID { get; set; }
        public int UserID { get; set; }
    }
    public class ApprovedCollaboratorLenderCommandHandler : IRequestHandler<ApprovedCollaboratorLenderCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCollaboratorLender> _collaboratorLenderTab;
        readonly ILogger<ApprovedCollaboratorLenderCommandHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public ApprovedCollaboratorLenderCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCollaboratorLender> collaboratorLenderTab,
        ILogger<ApprovedCollaboratorLenderCommandHandler> logger)
        {
            _collaboratorLenderTab = collaboratorLenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(ApprovedCollaboratorLenderCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var collaboratorLender = (await _collaboratorLenderTab.WhereClause(x => x.CollaboratorLenderID == request.CollaboratorLenderID).QueryAsync()).FirstOrDefault();
                if (collaboratorLender == null)
                {
                    response.Message = MessageConstant.ErrorNotExist;
                    return response;
                }
                collaboratorLender.Status = (int)CollaboratorLender_Status.Approved;
                collaboratorLender.ModifyDate = DateTime.Now;
                collaboratorLender.ModifyByUser = request.UserID;
                _ = await _collaboratorLenderTab.UpdateAsync(collaboratorLender);
                response.SetSucces();
                return response;

            }
            catch (Exception ex)
            {
                _logger.LogError($"ApprovedCollaboratorLenderCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
