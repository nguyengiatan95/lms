﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.LenderService;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class CreateLenderDebtCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public CreateLenderDebtRequest Model;
    }
    public class CreateLenderDebtCommandHandler : IRequestHandler<CreateLenderDebtCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebt> _debtTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLenderCash> _logLenderCashTab;
        ILogger<CreateLenderDebtCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateLenderDebtCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebt> debtTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            ILogger<CreateLenderDebtCommandHandler> logger)
        {
            _debtTab = debtTab;
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(CreateLenderDebtCommand request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    var lenderInfo = _lenderTab.WhereClause(x => x.LenderID == request.Model.LenderID).Query().FirstOrDefault();
                    if (lenderInfo == null || lenderInfo.LenderID < 1)
                    {
                        response.Message = MessageConstant.LenderNotExist;
                        return response;
                    }
                    var idInsert = _debtTab.Insert(new Domain.Tables.TblDebt
                    {
                        CreateBy = request.Model.CreateBy,
                        CreateDate = DateTime.Now,
                        Description = request.Model.Description,
                        ReferID = request.Model.ReferID,
                        TypeID = request.Model.DebtType,
                        Status = (int)StatusCommon.Active,
                        TargetID = request.Model.LenderID,
                        TotalMoney = request.Model.TotalMoney
                    });
                    response.SetSucces();
                    response.Data = idInsert;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"CreateLenderDebtCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                }
                return response;
            });
        }
    }
}
