﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class UpdateStatusDepositCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public List<Domain.Models.LenderStatusDeposit> LstLoanStatus { get; set; }
        public long UserID { get; set; }
    }
    public class UpdateStatusDepositCommandHandler : IRequestHandler<UpdateStatusDepositCommand, LMS.Common.Constants.ResponseActionResult>
    {
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentScheduleDepositMoney> _paymentDepositTab;
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogActionPaymentDepositMoney> _logTab;
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        private readonly Services.IQueueCallAPIManager _queueCallAPIManager;
        private readonly ILogger<UpdateStatusDepositCommandHandler> _logger;
        private readonly LMS.Common.Helper.Utils _common;
        public UpdateStatusDepositCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentScheduleDepositMoney> paymentDepositTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogActionPaymentDepositMoney> logTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            Services.IQueueCallAPIManager queueCallAPIManager,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            ILogger<UpdateStatusDepositCommandHandler> logger)
        {
            _paymentDepositTab = paymentDepositTab;
            _queueCallAPIManager = queueCallAPIManager;
            _loanTab = loanTab;
            _userTab = userTab;
            _logTab = logTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(UpdateStatusDepositCommand request, CancellationToken cancellationToken)
        {
            DateTime dtNow = DateTime.Now;
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            try
            {
                var lstStatusAcceptLender = new List<int>()
                {
                    (int)PaymentScheduleDepositMoney_Status.DefaultWaittingRecordDeposit,
                    (int)PaymentScheduleDepositMoney_Status.WaittingDeposit
                };

                var lstLoanIDs = request.LstLoanStatus.Select(x => x.LoanID).ToList();
                var dicLoanStatus = request.LstLoanStatus.ToDictionary(x => x.LoanID, x => x.Status);
                var lstDeposit = await _paymentDepositTab.WhereClause(x => lstStatusAcceptLender.Contains(x.Status) && lstLoanIDs.Contains(x.LoanID)).QueryAsync();
                var dicLoan = (await _loanTab.SelectColumns(x => x.LoanID, x => x.TimaLoanID, x => x.TimaCodeID, x => x.ContactCode).WhereClause(x => lstLoanIDs.Contains(x.LoanID)).QueryAsync()).ToDictionary(x => x.LoanID, x => x);
                List<Domain.Tables.TblLogActionPaymentDepositMoney> logs = new List<Domain.Tables.TblLogActionPaymentDepositMoney>();
                List<Domain.Models.UpdateDepositLoanMoney> lstLoanAG = new List<Domain.Models.UpdateDepositLoanMoney>();
                foreach (var item in lstDeposit)
                {
                    if (dicLoanStatus[item.LoanID] != item.Status && lstStatusAcceptLender.Contains(dicLoanStatus[item.LoanID]))
                    {
                        logs.Add(new Domain.Tables.TblLogActionPaymentDepositMoney()
                        {
                            CreateDate = dtNow,
                            LoanID = item.LoanID,
                            PaymentScheduleDepositMoneyID = item.PaymentScheduleDepositMoneyID,
                            UserID = request.UserID,
                            Note = $"Chuyển trạng thái từ {((PaymentScheduleDepositMoney_Status)item.Status).GetDescription()} sang {((PaymentScheduleDepositMoney_Status)dicLoanStatus[item.LoanID]).GetDescription()}"
                        });
                        item.Status = dicLoanStatus[item.LoanID];
                        if (dicLoan.ContainsKey(item.LoanID))
                        {
                            var loanAG = dicLoan[item.LoanID];
                            lstLoanAG.Add(new Domain.Models.UpdateDepositLoanMoney()
                            {
                                LoanID = (int)loanAG.TimaLoanID,
                                LoanCode = loanAG.ContactCode
                            });
                        }
                    }

                }
                try
                {
                    _paymentDepositTab.UpdateBulk(lstDeposit);
                }
                catch (Exception ex)
                {


                }
                if (logs != null && logs.Any()) _logTab.InsertBulk(logs);
                // đồng bộ về AG
                try
                {
                    var user = _userTab.GetByID(request.UserID);
                    user ??= new Domain.Tables.TblUser();
                    _queueCallAPIManager.LenderDepositMoney((int)request.UserID, user.FullName, lstLoanAG);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"UpdateStatusDepositCommandHandler_DongBoAG_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");

                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateStatusDepositCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}

