﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class CreateInfoLenderCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string FullName { get; set; }
        public string NumberCard { get; set; }
        public string Phone { get; set; }
        public int Gender { get; set; }
        public string TaxCode { get; set; }
        public string Address { get; set; }
        public DateTime CreateDate { get; set; }
        public decimal RateInterest { get; set; }
        public int Status { get; set; }
        public int IsVerified { get; set; }
        public string StrBirthDay { get; set; }
        public long TotalMoney { get; set; }
        public string InviteCode { get; set; }

        public decimal? RateLender { get; set; }

        public decimal? RateAff { get; set; }

        public string Represent { get; set; }

        public string AddressOfResidence { get; set; }

        public string StrCardDate { get; set; }

        public string CardPlace { get; set; }

        public string Email { get; set; }

        public string ContractCode { get; set; }

        public string StrContractDate { get; set; }

        public string AccountBanking { get; set; }

        public long? BankID { get; set; }

        public long? LenderCareUserID { get; set; }

        public long? LenderTakeCareUserID { get; set; }

        public long? AffID { get; set; }

        public string RepresentPhone { get; set; }

        public string BranchOfBank { get; set; }
        public string InvitePhone { get; set; }
    }
    public class CreateInfoLenderCommandHandler : IRequestHandler<CreateInfoLenderCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLenderCash> _logLenderCashTab;
        ILogger<CreateInfoLenderCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateInfoLenderCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLenderCash> logLenderCashTab,
            ILogger<CreateInfoLenderCommandHandler> logger)
        {
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _logLenderCashTab = logLenderCashTab;
        }
        public async Task<ResponseActionResult> Handle(CreateInfoLenderCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstLenderInfos = await _lenderTab.SetGetTop(1).WhereClause(x => x.FullName == request.FullName).QueryAsync();
                if (lstLenderInfos != null && lstLenderInfos.Any())
                {
                    response.Message = MessageConstant.LenderExisted;
                    return response;
                }
                var currentDate = DateTime.Now;
                var tblLender = new Domain.Tables.TblLender
                {
                    FullName = request.FullName,
                    NumberCard = request.NumberCard,
                    Address = request.Address,
                    Phone = request.Phone,
                    CreateDate = currentDate,
                    Gender = request.Gender,
                    RateInterest = request.RateInterest,
                    Status = request.Status,
                    IsHub = 0,
                    TaxCode = request.TaxCode,
                    TotalMoney = request.TotalMoney,
                    IsVerified = request.IsVerified,
                    ModifyDate = currentDate,
                    //InviteCode = request.InviteCode,
                    RateLender = request.RateLender,
                    RateAff = request.RateAff,
                    Represent = request.Represent,
                    AddressOfResidence = request.AddressOfResidence,
                    CardPlace = request.CardPlace,
                    Email = request.Email,
                    ContractCode = request.ContractCode,
                    AccountBanking = request.AccountBanking,
                    BankID = request.BankID,
                    LenderCareUserID = request.LenderCareUserID,
                    LenderTakeCareUserID = request.LenderTakeCareUserID,
                    AffID = request.AffID,
                    RepresentPhone = request.RepresentPhone,
                    BranchOfBank = request.BranchOfBank,
                    InvitePhone = request.InvitePhone
                };
                if (!string.IsNullOrWhiteSpace(request.StrCardDate))
                {
                    var cardDate = DateTime.ParseExact(request.StrCardDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                    tblLender.CardDate = cardDate;
                }
                if (!string.IsNullOrWhiteSpace(request.StrContractDate))
                {
                    var contractDate = DateTime.ParseExact(request.StrContractDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                    tblLender.ContractDate = contractDate;
                }
                if (!string.IsNullOrWhiteSpace(request.StrBirthDay))
                {
                    var birthDay = DateTime.ParseExact(request.StrBirthDay, TimaSettingConstant.DateTimeDayMonthYear, null);
                    tblLender.BirthDay = birthDay;
                }
                var lenderID = await _lenderTab.InsertAsync(tblLender);
                if (lenderID > 0)
                {
                    _ = await _logLenderCashTab.InsertAsync(new Domain.Tables.TblLogLenderCash
                    {
                        LenderID = lenderID,
                        CreateDate = currentDate,
                        MoneyBefore = 0,
                        MoneyAdd = request.TotalMoney,
                        MoneyAfter = request.TotalMoney,
                        Description = "",
                    });
                    response.SetSucces();
                    response.Data = lenderID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateInfoLenderCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
