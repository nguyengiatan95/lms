﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.ProxyTima;
using LMS.Entites.Dtos.SendMail;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class AppCreateLenderUserCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required(ErrorMessage = "Chưa nhập tên đối tác (FullName)", AllowEmptyStrings = true)]
        public string FullName { get; set; }
        [Required(ErrorMessage = "Chưa nhập SĐT (Phone)", AllowEmptyStrings = true)]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Chưa nhập Mật khẩu (Password)", AllowEmptyStrings = true)]
        public string Password { get; set; }
    }
    public class AppCreateLenderUserCommandHandler : IRequestHandler<AppCreateLenderUserCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> _userlenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> _queueCallAPITab;
        ILogger<AppCreateLenderUserCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        RedisCachedServiceHelper _redisCachedService;
        public AppCreateLenderUserCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> userLenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> queueCallAPITab,
            RedisCachedServiceHelper redisCachedService,
            ILogger<AppCreateLenderUserCommandHandler> logger)
        {
            _userlenderTab = userLenderTab;
            _userTab = userTab;
            _queueCallAPITab = queueCallAPITab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _redisCachedService = redisCachedService;
        }
        public async Task<ResponseActionResult> Handle(AppCreateLenderUserCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            DateTime dtNow = DateTime.Now;
            int minPasswordLength = 8;
            try
            {
                // check giá trị ban đầu
                string message = string.Empty;
                bool value = _common.CheckValidInputString<AppCreateLenderUserCommand>(request, ref message);
                if (!value)
                {
                    response.Message = message;
                    return response;
                }
                //Số điện thoại: chỉ cho phép nhập số, có validate trường này.
                if (!(request.Phone.Length == TimaSettingConstant.MaxLengthPhone && Regex.Match(request.Phone, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success))
                {
                    response.Message = MessageConstant.UserLender_PhoneInvalid;
                    return response;
                }
                if (!request.Password.Any(char.IsUpper) || request.Password.Length < minPasswordLength)
                {
                    response.Message = MessageConstant.UserLender_PasswordInvalid;
                    return response;
                }
                // b1: kiểm tra số dt trong bảng tblUserLender nếu tồn tại và đã xác thực không cho tạo
                var userLenders = await _userlenderTab.SetGetTop(1).WhereClause(x => x.Phone == request.Phone && x.Status == (int)LenderUser_Status.Active).QueryAsync();
                if (userLenders != null && userLenders.Any())
                {
                    response.Message = MessageConstant.UserLender_Existed;
                    return response;
                }
                //b2: tạo thông tin user đăng nhập:
                //b2_1: userName = "LD_" + "Số thứ tự tăng dần gồm 6 chữ số" bắt đầu từ 000001
                long lenderCodeID = 1;

                var userNameLender = TimaSettingConstant.UserNameLenderConfig;
                lenderCodeID = await _redisCachedService.GetLenderCodeIDNext();
                string newCodeLender = $"{userNameLender}{lenderCodeID.ToString(TimaSettingConstant.FormatLengthLenderCode)}";
                var userInsert = new Domain.Tables.TblUser()
                {
                    UserName = newCodeLender,
                    CreateDate = dtNow,
                    FullName = newCodeLender,
                    Password = _common.MD5(request.Password),
                    Status = (int)User_Status.Waiting,
                    UserTypeID = (int)User_UserTypeID.Lender
                };
                long userIDInsert = await _userTab.InsertAsync(userInsert);
                //b2_2: UserTypeID = 2, Status = 1, IsVerified = đã xác thực hay không, createdate = ngày tạo
                //b3: thêm vào bảng tblUserLender: gồm UserID mới thêm thành công
                //b2_1: userName = "LD_" + "Số thứ tự tăng dần gồm 6 chữ số" bắt đầu từ 000001, mật khẩu md5 lại cho user
                //b2_2: UserTypeID = 2,
                //Status = 3: chờ duyệt
                //createdate = ngày tạo

                // gửi tin nhắn
                if (userIDInsert > 0)
                {
                    var userLender = new Domain.Tables.TblUserLender()
                    {
                        Status = (int)LenderUser_Status.Waiting,
                        Phone = request.Phone,
                        UserID = userIDInsert,
                        IsVerified = (int)LenderUser_Verified.UnActive,
                        CreateDate = dtNow,
                        FullName = request.FullName,
                        ModifyDate = dtNow,
                    };
                    var insertLender = await _userlenderTab.InsertAsync(userLender);

                    var objRequest = new
                    {
                        Department = Entites.Dtos.ProxyTima.ProxyTimaConfiguration.AgencyAccount,
                        Domain = ProxyTimaConfiguration.SmsDomainAppLender,
                        Phone = request.Phone,
                        MessageContent = string.Format(RuleLenderServiceHelper.SmsContentSendLenderRegisterSuccess, newCodeLender, request.Password)
                    };
                    var queueCallAPI = new Domain.Tables.TblQueueCallAPI
                    {
                        Domain = Entites.Dtos.ProxyTima.ProxyTimaConfiguration.BaseUrl,
                        Path = Entites.Dtos.ProxyTima.ProxyTimaConfiguration.SendSMSBrandNameFPT,
                        Method = "POST",
                        JsonRequest = _common.ConvertObjectToJSonV2(objRequest),
                        Status = 0,
                        Retry = 0,
                        CreateDate = DateTime.Now,
                        Description = QueueCallAPI_Description.SendSMS.GetDescription()
                    };
                    _ = await _queueCallAPITab.InsertAsync(queueCallAPI);

                    response.SetSucces();
                    response.Data = insertLender;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"AppCreateLenderUserCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
