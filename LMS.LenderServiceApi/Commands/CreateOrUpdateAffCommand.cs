﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.ProxyTima;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class CreateOrUpdateAffCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long AffID { get; set; }
        [Required(ErrorMessage = "Chưa nhập tên CTV (AffName)", AllowEmptyStrings = true)]
        public string AffName { get; set; }
        [Required(ErrorMessage = "Chưa nhập mã CTV (Code)", AllowEmptyStrings = true)]
        public string Code { get; set; }
        [Required(ErrorMessage = "Chưa nhập email (email)", AllowEmptyStrings = true)]
        public string Email { get; set; }
        [Required(ErrorMessage = "Chưa nhập sđt CTV (Phone)", AllowEmptyStrings = true)]
        public string Phone { get; set; }
        public string NumberCard { get; set; }
        public string Address { get; set; }
        public string BirthDay { get; set; }
        public short? Source { get; set; }
        public string ContractDate { get; set; }
        public string FinishContractDate { get; set; }
        public int CreateBy { get; set; }
        public string TaxNumber { get; set; }
        public double PercentComission { get; set; }
        public string BankNumber { get; set; }
        public int BankID { get; set; }
        public string BankCode { get; set; }
        public string BankPlace { get; set; }
        public long StaffID { get; set; }

    }
    public class CreateOrUpdateAffCommandHandler : IRequestHandler<CreateOrUpdateAffCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> _affTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        readonly Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> _queueCallAPITab;
        readonly ILogger<CreateOrUpdateAffCommandHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        readonly Domain.Configuration.SendEmailAffConfig _sendEmailAffConfig;
        readonly RestClients.IProxyTimaService _proxyTimaService;
        public CreateOrUpdateAffCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> affTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> queueCallAPITab,
            Domain.Configuration.SendEmailAffConfig sendEmailAffConfig,
            RestClients.IProxyTimaService proxyTimaService,
            ILogger<CreateOrUpdateAffCommandHandler> logger)
        {
            _affTab = affTab;
            _userTab = userTab;
            _queueCallAPITab = queueCallAPITab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _sendEmailAffConfig = sendEmailAffConfig;
            _proxyTimaService = proxyTimaService;
        }
        public async Task<ResponseActionResult> Handle(CreateOrUpdateAffCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            DateTime dtNow = DateTime.Now;
            int createID = 0;
            try
            {
                // check dữ liệu
                string message = string.Empty;
                bool value = _common.CheckValidInputString<CreateOrUpdateAffCommand>(request, ref message);
                if (!value)
                {
                    response.Message = message;
                    return response;
                }
                //Số điện thoại: chỉ cho phép nhập số, có validate trường này.
                if ((request.Phone.Length != TimaSettingConstant.MaxLengthPhone || !Regex.Match(request.Phone, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success))
                {
                    response.Message = MessageConstant.UserLender_PhoneInvalid;
                    return response;
                }
                // insert Aff
                if (request.AffID == createID)
                {
                    var checkUserExist = (await _userTab.WhereClause(x => x.UserName == request.Email).QueryAsync()).Count() > 0;
                    if (checkUserExist)
                    {
                        response.Message = MessageConstant.Aff_PhoneExisted;
                        return response;
                    }
                    //check xem tồn tại aff nào có cmnd đó chưa
                    if (!string.IsNullOrWhiteSpace(request.NumberCard))
                    {
                        var affLenders = await _affTab.SetGetTop(1).WhereClause(x => x.NumberCard == request.NumberCard).QueryAsync();
                        if (affLenders != null && affLenders.Count() > 0)
                        {
                            response.Message = MessageConstant.Aff_Existed;
                            return response;
                        }
                    }
                    var dataPost = new
                    {
                        UserName = request.Email,
                        Fullname = request.AffName,
                        Password = _common.HashMD5(request.Phone),
                        Email = request.Email,
                        Source = "CTV"
                    };
                    var actionResult = await _proxyTimaService.CallApiAg(new LMS.Entites.Dtos.AG.AgDataPostModel
                    {
                        Path = LMS.Entites.Dtos.AG.AGConstants.LMSCreateUser,
                        DataPost = _common.ConvertObjectToJSonV2(dataPost)
                    });

                    if (actionResult.Result != (int)ResponseAction.Success)
                    {
                        return actionResult;
                    }
                    // dữ liệu ag trả về phải prase 2 lần
                    var resposneAg = _common.ConvertJSonToObjectV2<ResponseActionResult>(actionResult.Data.ToString());
                    var timaUserID = (long)_common.ConvertJSonToObjectV2<ResponseActionResult>(resposneAg.Data.ToString()).Data;
                    var password = request.Phone;
                    string newCodeAff = request.Code;
                    var userInsert = new Domain.Tables.TblUser()
                    {
                        UserID = timaUserID,
                        TimaUserID = timaUserID,
                        UserName = request.Email,
                        CreateDate = dtNow,
                        Email = request.Email,
                        FullName = request.AffName,
                        Password = _common.HashMD5(password),
                        Status = (int)User_Status.Active,
                        UserTypeID = (int)User_UserTypeID.Aff
                    };

                    long userID = await _userTab.InsertAsync(userInsert, true);
                    if (userID <= 0)
                    {
                        response.Message = MessageConstant.Aff_ErrorCreateUser;
                        return response;
                    }
                    //insert dữ liệu
                    Domain.Tables.TblAff aff = new Domain.Tables.TblAff()
                    {
                        Address = request.Address,
                        Name = request.AffName,
                        Code = request.Code,
                        Email = request.Email,
                        PhoneNumber = request.Phone,
                        NumberCard = request.NumberCard,
                        Status = (int)Aff_Status.Active,
                        CreatedDate = dtNow,
                        CreateBy = request.CreateBy,
                        TaxNumber = request.TaxNumber,
                        PercentComission = request.PercentComission,
                        BankNumber = request.BankNumber,
                        BankID = request.BankID,
                        BankCode = request.BankCode,
                        BankPlace = request.BankPlace,
                        StaffID = request.StaffID,
                        UserID = timaUserID
                    };
                    if (!string.IsNullOrWhiteSpace(request.BirthDay))
                    {
                        aff.BirthDay = DateTime.ParseExact(request.BirthDay, TimaSettingConstant.DateTimeDayMonthYear, null);
                    }
                    if (!string.IsNullOrWhiteSpace(request.ContractDate))
                    {
                        aff.ContractDate = DateTime.ParseExact(request.ContractDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                    }
                    if (!string.IsNullOrWhiteSpace(request.FinishContractDate))
                    {
                        aff.FinishContractDate = DateTime.ParseExact(request.FinishContractDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                    }
                    long AffId = await _affTab.InsertAsync(aff);
                    if (AffId > 0)
                    {
                        response.SetSucces();
                        response.Data = AffId;
                    }
                    //b2_1: userName = "CTV" + "Số thứ tự tăng dần gồm 6 chữ số" bắt đầu từ 001
                    // gửi mail
                    if (!string.IsNullOrEmpty(request.Email))
                    {
                        var objSendMail = new Domain.Models.SendMail()
                        {
                            Email = request.Email,
                            Department = ProxyTimaConfiguration.EmailDepartment,
                            Domain = ProxyTimaConfiguration.EmailDomain,
                            Title = _sendEmailAffConfig.Title,
                            Content = string.Format(_sendEmailAffConfig.Body, request.Email, password, request.Code)
                            //$"Tên đăng nhập: {request.Phone} <br />" +
                            //     $"Mật khẩu {password} <br />" +
                            //     $"Link đăng nhập: https://lms.tima.vn/Login" +
                            //     $"Link đăng ký gửi NĐT: https://tima.vn/nha-dau-tu?utm_source=" + request.Code
                        };
                        var queueCallAPI = new Domain.Tables.TblQueueCallAPI
                        {
                            Domain = ProxyTimaConfiguration.BaseUrl,
                            Path = ProxyTimaConfiguration.SendEmail,
                            Method = "POST",
                            JsonRequest = _common.ConvertObjectToJSonV2(objSendMail),
                            Status = 0,
                            Retry = 0,
                            CreateDate = DateTime.Now,
                            Description = QueueCallAPI_Description.SendMail.GetDescription()
                        };
                        _ = await _queueCallAPITab.InsertAsync(queueCallAPI);
                    }
                    response.SetSucces();
                    response.Data = AffId;
                }
                else
                {
                    // update AFF
                    var affCurrent = _affTab.GetByID(request.AffID);
                    if (affCurrent == null || affCurrent.AffID == 0)
                    {
                        response.Message = MessageConstant.Aff_NotExisted;
                        return response;
                    }
                    affCurrent.Address = request.Address;
                    affCurrent.Name = request.AffName;
                    affCurrent.Code = request.Code;
                    affCurrent.Email = request.Email;
                    affCurrent.PhoneNumber = request.Phone;
                    affCurrent.NumberCard = request.NumberCard;
                    affCurrent.TaxNumber = request.TaxNumber;
                    affCurrent.PercentComission = request.PercentComission;
                    affCurrent.BankNumber = request.BankNumber;
                    affCurrent.BankID = request.BankID;
                    affCurrent.BankCode = request.BankCode;
                    affCurrent.BankPlace = request.BankPlace;
                    affCurrent.Status = (int)Aff_Status.Active;
                    affCurrent.StaffID = request.StaffID;
                    if (!string.IsNullOrWhiteSpace(request.BirthDay))
                    {
                        affCurrent.BirthDay = DateTime.ParseExact(request.BirthDay, TimaSettingConstant.DateTimeDayMonthYear, null);
                    }
                    if (!string.IsNullOrWhiteSpace(request.ContractDate))
                    {
                        affCurrent.ContractDate = DateTime.ParseExact(request.ContractDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                    }
                    if (!string.IsNullOrWhiteSpace(request.FinishContractDate))
                    {
                        affCurrent.FinishContractDate = DateTime.ParseExact(request.FinishContractDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                    }
                    bool update = await _affTab.UpdateAsync(affCurrent);
                    if (update)
                    {
                        response.SetSucces();
                        response.Data = request.AffID;
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateOrUpdateAffCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
