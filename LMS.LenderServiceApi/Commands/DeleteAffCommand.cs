﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class DeleteAffCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long AffID { get; set; }
    }
    public class DeleteAffCommandHandler : IRequestHandler<DeleteAffCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> _affTab;
        ILogger<DeleteAffCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public DeleteAffCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> affTab,
            ILogger<DeleteAffCommandHandler> logger)
        {
            _affTab = affTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(DeleteAffCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                // check dữ liệu
                var affCurrent = _affTab.GetByID(request.AffID);
                if (affCurrent == null || affCurrent.AffID == 0)
                {
                    response.Message = MessageConstant.Aff_NotExisted;
                    return response;
                }
                affCurrent.Status = (int)Aff_Status.Delete;
                bool update = await _affTab.UpdateAsync(affCurrent);
                if (update)
                {
                    response.SetSucces();
                    response.Data = request.AffID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"DeleteAffCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
