﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class CreateLenderDigitalSignatureCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LenderID { get; set; }
        [Required(ErrorMessage = "Chưa nhập chữ kí số (Signature)", AllowEmptyStrings = true)]
        public string Signature { get; set; }
        [Required(ErrorMessage = "Chưa nhập mật khẩu (Password)", AllowEmptyStrings = true)]
        public string Password { get; set; }
        public long UserID { get; set; }
    }
    public class CreateLenderDigitalSignatureCommandHandler : IRequestHandler<CreateLenderDigitalSignatureCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLenderDigitalSignature> _userLenderDigitalSignatureTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLenderChangeInfo> _loglenderChangeTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        RestClients.ILOSService _lOSService;
        ILogger<CreateLenderDigitalSignatureCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateLenderDigitalSignatureCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLenderDigitalSignature> userLenderDigitalSignatureTab,
           LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLenderChangeInfo> loglenderChangeTab,
           LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            RestClients.ILOSService lOSService,
            ILogger<CreateLenderDigitalSignatureCommandHandler> logger)
        {
            _userLenderDigitalSignatureTab = userLenderDigitalSignatureTab;
            _lOSService = lOSService;
            _lenderTab = lenderTab;
            _loglenderChangeTab = loglenderChangeTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(CreateLenderDigitalSignatureCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            DateTime dtNow = DateTime.Now;
            try
            {

                //input: UserID, chuỗi chữ ký số, mật khẩu
                string message = string.Empty;
                bool value = _common.CheckValidInputString<CreateLenderDigitalSignatureCommand>(request, ref message);
                if (!value)
                {
                    response.Message = message;
                    return response;
                }
                // CALL API A HÀ
                var responseLOS = await _lOSService.CreateLenderDigitalSignature(request.LenderID, request.Signature, request.Password);
                //var responseLOS = 200;
                if (responseLOS.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                {
                    var lenders = await _lenderTab.WhereClause(x => x.LenderID == request.LenderID).QueryAsync();
                    var lender = lenders.FirstOrDefault();
                    //ghi nhận thông tin vào bảng TblUserLenderDigitalSignature
                    var currentSignature = await _userLenderDigitalSignatureTab.SetGetTop(1).WhereClause(x => x.UserID == request.LenderID).QueryAsync();
                    if (currentSignature != null && currentSignature.Any())
                    {
                        var currentData = currentSignature.FirstOrDefault();

                        var oldData = new Domain.Tables.TblUserLenderDigitalSignature()
                        {
                            AgreementUUID = currentData.AgreementUUID,
                            UserID = lender.OwnerUserID,
                            CreateBy = currentData.CreateBy,
                            CreateDate = currentData.CreateDate,
                            PassCode = currentData.PassCode,
                            Status = currentData.Status,
                            UserLenderDigitalSignatureID = currentData.UserLenderDigitalSignatureID,
                            LenderID = request.LenderID
                        };
                        currentData.AgreementUUID = request.Signature;
                        currentData.PassCode = request.Password;

                        string strOldData = _common.GetJsonObjectChange<Domain.Tables.TblUserLenderDigitalSignature>(oldData, currentData);
                        bool update = await _userLenderDigitalSignatureTab.UpdateAsync(currentData);
                        if (update)
                        {
                            _ = await _loglenderChangeTab.InsertAsync(new Domain.Tables.TblLogLenderChangeInfo()
                            {
                                UserID = lender.OwnerUserID,
                                CreateBy = request.UserID,
                                CreateDate = dtNow,
                                DataOld = strOldData,
                                LogTypeID = (int)LogLenderChangeInfo_LogTypeID.LenderDigitalSignature
                            });
                            response.SetSucces();
                            response.Data = currentData.UserLenderDigitalSignatureID;
                        }
                    }
                    else
                    {
                        long iDinsert = await _userLenderDigitalSignatureTab.InsertAsync(new Domain.Tables.TblUserLenderDigitalSignature()
                        {
                            AgreementUUID = request.Signature,
                            CreateBy = request.UserID,
                            UserID = lender.OwnerUserID,
                            CreateDate = dtNow,
                            PassCode = request.Password,
                            Status = (int)StatusCommon.Active,
                            LenderID = request.LenderID,
                        });
                        if (iDinsert > 0)
                        {
                            response.Data = iDinsert;
                            response.SetSucces();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateLenderDigitalSignatureCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
