﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class AGUpdateStatusDepositCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public List<Domain.Models.TimaLenderStatusDeposit> LstLoanStatus { get; set; }
        public long UserID { get; set; }
    }
    public class AGUpdateStatusDepositCommandHandler : IRequestHandler<AGUpdateStatusDepositCommand, LMS.Common.Constants.ResponseActionResult>
    {
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentScheduleDepositMoney> _paymentDepositTab;
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogActionPaymentDepositMoney> _logTab;
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        private readonly ILogger<AGUpdateStatusDepositCommandHandler> _logger;
        private readonly LMS.Common.Helper.Utils _common;
        public AGUpdateStatusDepositCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentScheduleDepositMoney> paymentDepositTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogActionPaymentDepositMoney> logTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            ILogger<AGUpdateStatusDepositCommandHandler> logger)
        {
            _paymentDepositTab = paymentDepositTab;
            _loanTab = loanTab;
            _logTab = logTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(AGUpdateStatusDepositCommand request, CancellationToken cancellationToken)
        {
            DateTime dtNow = DateTime.Now;
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            try
            {
                var lstStatusAcceptLender = new List<int>()
                {
                    (int)PaymentScheduleDepositMoney_Status.DefaultWaittingRecordDeposit,
                    (int)PaymentScheduleDepositMoney_Status.WaittingDeposit
                };

                var lstLoanIDs = request.LstLoanStatus.Select(x => x.TimaLoanID).ToList();
                var loans = _loanTab.SelectColumns(x => x.LoanID, x => x.TimaLoanID).WhereClause(x => lstLoanIDs.Contains(x.TimaLoanID)).Query();
                var dicLoanTima = loans.ToDictionary(x => x.TimaLoanID, x => x.LoanID);

                //var dicLoanStatus = request.LstLoanStatus.ToDictionary(x => x.LoanID, x => x.Status);
                var dicLoanStatus = new Dictionary<long, int>();

                foreach (var item in request.LstLoanStatus)
                {
                    if (dicLoanTima.ContainsKey(item.TimaLoanID))
                    {
                        dicLoanStatus.Add(dicLoanTima[item.TimaLoanID], item.Status);
                    }
                }
                var lstLoanIDLMS = dicLoanStatus.Keys.ToList();
                var lstDeposit = await _paymentDepositTab.WhereClause(x => lstStatusAcceptLender.Contains(x.Status) && lstLoanIDLMS.Contains(x.LoanID)).QueryAsync();
                List<Domain.Tables.TblLogActionPaymentDepositMoney> logs = new List<Domain.Tables.TblLogActionPaymentDepositMoney>();
                foreach (var item in lstDeposit)
                {
                    if (dicLoanStatus[item.LoanID] != item.Status && lstStatusAcceptLender.Contains(dicLoanStatus[item.LoanID]))
                    {
                        item.Status = dicLoanStatus[item.LoanID];
                        logs.Add(new Domain.Tables.TblLogActionPaymentDepositMoney()
                        {
                            CreateDate = dtNow,
                            LoanID = item.LoanID,
                            PaymentScheduleDepositMoneyID = item.PaymentScheduleDepositMoneyID,
                            UserID = request.UserID,
                            Note = $"AG : Chuyển trạng thái từ {((PaymentScheduleDepositMoney_Status)item.Status).GetDescription()} sang {((PaymentScheduleDepositMoney_Status)dicLoanStatus[item.LoanID]).GetDescription()}"
                        });
                    }

                }
                _paymentDepositTab.UpdateBulk(lstDeposit);
                if (logs != null && logs.Any()) _logTab.InsertBulk(logs);
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"AGUpdateStatusDepositCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}

