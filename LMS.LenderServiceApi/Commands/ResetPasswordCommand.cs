﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.ProxyTima;
using LMS.Entites.Dtos.SendMail;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class ResetPasswordCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserID { get; set; }
        public long CreateBy { get; set; }
    }
    public class ResetPasswordCommandHandler : IRequestHandler<ResetPasswordCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> _queueCallAPITab;
        ILogger<ResetPasswordCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public ResetPasswordCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> queueCallAPITab,
            ILogger<ResetPasswordCommandHandler> logger)
        {
            _userTab = userTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _queueCallAPITab = queueCallAPITab;
        }

        public async Task<ResponseActionResult> Handle(ResetPasswordCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            DateTime dtNow = DateTime.Now;
            try
            {
                var lstUserData = await _userTab.WhereClause(x => (x.UserID == request.CreateBy) || (x.UserID == request.UserID && x.UserTypeID == (int)User_UserTypeID.Lender)).QueryAsync();
                if (lstUserData == null || lstUserData.Any())
                {
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                var objUser = lstUserData.FirstOrDefault(x => x.UserID == request.CreateBy);
                var userLender = lstUserData.FirstOrDefault(x => x.UserID == request.UserID && x.UserTypeID == (int)User_UserTypeID.Lender);
                if (userLender == null)
                {
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                if (objUser == null || string.IsNullOrEmpty(objUser.Email))
                {
                    response.Message = MessageConstant.NotIdentifiedUserID;
                    return response;
                }
                var password = _common.RandomPassword();
                userLender.Password = _common.MD5(password);
                if (await _userTab.UpdateAsync(userLender))
                {
                    #region gửi mail cho nhân viên tạo NĐT
                    var objSendMail = new Domain.Models.SendMail()
                    {
                        Email = objUser.Email,
                        Department = ProxyTimaConfiguration.EmailDepartment,
                        Domain = ProxyTimaConfiguration.EmailDomain,
                        Title = "Reset mật khẩu",
                        Content = $"Password: {password} <br />"
                    };
                    var queueCallAPI = new Domain.Tables.TblQueueCallAPI
                    {
                        Domain = ProxyTimaConfiguration.BaseUrl,
                        Path = ProxyTimaConfiguration.SendEmail,
                        Method = "POST",
                        JsonRequest = _common.ConvertObjectToJSonV2(objSendMail),
                        Status = 0,
                        Retry = 0,
                        CreateDate = DateTime.Now,
                        Description = QueueCallAPI_Description.SendMail.GetDescription()
                    };
                    _ = await _queueCallAPITab.InsertAsync(queueCallAPI);
                    #endregion
                }

                response.SetSucces();
                response.Data = request.UserID;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ResetPasswordCommand_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
