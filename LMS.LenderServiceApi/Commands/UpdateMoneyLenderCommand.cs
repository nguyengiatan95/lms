﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class UpdateMoneyLenderCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LenderID { get; set; }
        public long MoneyAdd { get; set; }
        public string Description { get; set; }
    }
    public class UpdateMoneyLenderCommandHandler : IRequestHandler<UpdateMoneyLenderCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLenderCash> _logLenderCashTab;
        ILogger<UpdateMoneyLenderCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdateMoneyLenderCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLenderCash> logLenderCashTab,
            ILogger<UpdateMoneyLenderCommandHandler> logger)
        {
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _logLenderCashTab = logLenderCashTab;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> Handle(UpdateMoneyLenderCommand request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
                try
                {
                    var lstLender = _lenderTab.WhereClause(x => x.LenderID == request.LenderID).Query();
                    if (lstLender == null || !lstLender.Any())
                    {
                        response.Message = MessageConstant.LenderNotExist;
                        return response;
                    }
                    var lenderInfo = lstLender.First();
                    var currentDate = DateTime.Now;
                    if (_logLenderCashTab.Insert(new Domain.Tables.TblLogLenderCash
                    {
                        LenderID = request.LenderID,
                        CreateDate = currentDate,
                        MoneyBefore = lenderInfo.TotalMoney,
                        MoneyAdd = request.MoneyAdd,
                        MoneyAfter = lenderInfo.TotalMoney + request.MoneyAdd,
                        Description = request.Description

                    }) > 0)
                    {
                        lenderInfo.TotalMoney += request.MoneyAdd;
                        lenderInfo.ModifyDate = currentDate;
                        _lenderTab.Update(lenderInfo);
                        response.SetSucces();
                        response.Data = lenderInfo.TotalMoney;
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"UpdateMoneyLenderCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                }
                return response;
            });
        }
    }
}
