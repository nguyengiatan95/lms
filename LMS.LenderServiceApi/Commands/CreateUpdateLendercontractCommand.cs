﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class CreateUpdateLenderContractCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LenderID { get; set; }
        public string ContractCode { get; set; }
        public string ContractDate { get; set; }
        public int ContractType { get; set; }
        public int ContractDuration { get; set; }
        public string ContractEndDate { get; set; }
        public long TotalMoneyTopup { get; set; }
        public long UserID { get; set; }
        public long CreateBy { get; set; }
        public int IsDeposit { get; set; }
        public long AffID { get; set; }
    }
    public class CreateUpdateLenderContractCommandCommandHandler : IRequestHandler<CreateUpdateLenderContractCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLenderChangeInfo> _loglenderChangeTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        ILogger<CreateUpdateLenderContractCommandCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateUpdateLenderContractCommandCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLenderChangeInfo> loglenderChangeTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
        ILogger<CreateUpdateLenderContractCommandCommandHandler> logger)
        {
            _lenderTab = lenderTab;
            _loglenderChangeTab = loglenderChangeTab;
            _loanTab = loanTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(CreateUpdateLenderContractCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            DateTime dtNow = DateTime.Now;
            try
            {
                if (request.UserID <= 0)
                {
                    response.Message = MessageConstant.LenderNotExist;
                    return response;
                }

                if (request.LenderID == 0)//create
                {
                    var maxNA = (await _lenderTab.SetGetTop(1).SelectColumns(x => x.FullName).WhereClause(x => x.FullName.Contains(RuleLenderServiceHelper.PrefixNA))
                                                                               .OrderByDescending(x => x.FullName)
                                                                               .QueryAsync()).FirstOrDefault().FullName.Replace(RuleLenderServiceHelper.PrefixNA, "");
                    var maxDT = (await _lenderTab.SelectColumns(x => x.FullName).WhereClause(x => x.FullName.Contains(RuleLenderServiceHelper.PrefixDT))
                                                                               .OrderByDescending(x => x.FullName)
                                                                               .QueryAsync()).FirstOrDefault().FullName.Replace(RuleLenderServiceHelper.PrefixDT, "");
                    int maxCode = 0;
                    maxCode = int.Parse(maxNA) > int.Parse(maxDT) ? int.Parse(maxNA) + 1 : int.Parse(maxDT) + 1;
                    string lenderCode = GetContractCode(request.ContractType, maxCode);
                    var lender = new Domain.Tables.TblLender()
                    {
                        FullName = lenderCode,
                        ContractCode = request.ContractCode,
                        SelfEmployed = (int)Lender_SeflEmployed.LenderOut,
                        OwnerUserID = request.UserID,
                        ContractType = request.ContractType,
                        ContractDuration = request.ContractDuration,
                        TotalMoneyTopup = request.TotalMoneyTopup,
                        CreateDate = dtNow,
                        ModifyDate = dtNow,
                        LenderCode = lenderCode,
                        IsDeposit = request.IsDeposit,
                        AffID = request.AffID,
                        Status = (int)Lender_Status.Active,
                        IsVerified = (int)Lender_IsVerified.Active,
                    };
                    if (!string.IsNullOrWhiteSpace(request.ContractEndDate))
                    {
                        var contractEndDate = DateTime.ParseExact(request.ContractEndDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                        lender.ContractEndDate = contractEndDate;
                    }
                    if (!string.IsNullOrWhiteSpace(request.ContractDate))
                    {
                        var contractDate = DateTime.ParseExact(request.ContractDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                        lender.ContractDate = contractDate;
                    }
                    long insertId = await _lenderTab.InsertAsync(lender);
                    if (insertId > 0)
                    {
                        response.SetSucces();
                        response.Data = insertId;
                        return response;
                    }
                }
                else // update
                {
                    //với Mã(FullName) đã có đơn giải ngân thì không cho cập nhật, ngược lại cho phép cập nhật các thông số:
                    var loans = await _loanTab.SetGetTop(1).SelectColumns(x => x.LoanID)
                        .WhereClause(x => x.LenderID == request.LenderID && x.Status != (int)Loan_Status.Delete).QueryAsync();
                    if (loans != null && loans.Any())
                    {
                        response.Message = MessageConstant.Lender_Error_Update_HaveLoan;
                        return response;
                    }
                    //Thời gian, loại hd, ngày hết hạn, số tiền đầu tư, số hợp đồng, ngày bắt đầu hd
                    var lenders = await _lenderTab.SetGetTop(1).WhereClause(x => x.LenderID == request.LenderID).QueryAsync();
                    var lender = lenders.FirstOrDefault();
                    var lenderOld = _common.ConvertOldToNewObject<Domain.Tables.TblLender>(lender);
                    lender.FullName = GetContractCode(request.ContractType, int.Parse(lender.FullName.Replace(RuleLenderServiceHelper.PrefixDT, "").Replace(RuleLenderServiceHelper.PrefixNA, "")));
                    lender.ContractCode = request.ContractCode;
                    lender.SelfEmployed = (int)Lender_SeflEmployed.LenderOut;
                    lender.OwnerUserID = request.UserID;
                    lender.ContractType = request.ContractType;
                    lender.ContractDuration = request.ContractDuration;
                    lender.TotalMoneyTopup = request.TotalMoneyTopup;
                    lender.LenderCode = lender.FullName;
                    lender.ModifyDate = dtNow;
                    lender.AffID = request.AffID;
                    lender.IsDeposit = request.IsDeposit;
                    lender.Status = (int)Lender_Status.Active;
                    lender.IsVerified = (int)Lender_IsVerified.Active;
                    var strOldData = _common.GetJsonObjectChange<Domain.Tables.TblLender>(lenderOld, lender);
                    if (!string.IsNullOrWhiteSpace(request.ContractEndDate))
                    {
                        var contractEndDate = DateTime.ParseExact(request.ContractEndDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                        lender.ContractEndDate = contractEndDate;
                    }
                    if (!string.IsNullOrWhiteSpace(request.ContractDate))
                    {
                        var contractDate = DateTime.ParseExact(request.ContractDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                        lender.ContractDate = contractDate;
                    }
                    bool update = await _lenderTab.UpdateAsync(lender);
                    if (update)
                    {
                        _ = await _loglenderChangeTab.InsertAsync(new Domain.Tables.TblLogLenderChangeInfo()
                        {
                            UserID = request.UserID,
                            CreateBy = request.CreateBy,
                            CreateDate = dtNow,
                            DataOld = strOldData,
                            LogTypeID = (int)LogLenderChangeInfo_LogTypeID.LenderContract
                        });
                        response.Data = request.LenderID;
                        response.SetSucces();
                        return response;
                    }
                    //ghi nhận log
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateUpdateLenderInfoCommandCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private string GetContractCode(int contractType, int maxCode)
        {
            switch ((Lender_ContractType)contractType)
            {
                case Lender_ContractType.DTNormal:
                    return $"{RuleLenderServiceHelper.PrefixDT}{maxCode.ToString(TimaSettingConstant.FormatLengthLenderCode)}";
                case Lender_ContractType.NANormal:
                    return $"{RuleLenderServiceHelper.PrefixNA}{maxCode.ToString(TimaSettingConstant.FormatLengthLenderCode)}";
                case Lender_ContractType.DTTima:
                    return $"{RuleLenderServiceHelper.PrefixDT}{maxCode.ToString(TimaSettingConstant.FormatLengthLenderCode)}";
                case Lender_ContractType.NATima:
                    return $"{RuleLenderServiceHelper.PrefixNA}{maxCode.ToString(TimaSettingConstant.FormatLengthLenderCode)}";
                case Lender_ContractType.DTTimaNew:
                    return $"{RuleLenderServiceHelper.PrefixDT}{maxCode.ToString(TimaSettingConstant.FormatLengthLenderCode)}";
                case Lender_ContractType.NAOfTimaNew:
                    return $"{RuleLenderServiceHelper.PrefixNA}{maxCode.ToString(TimaSettingConstant.FormatLengthLenderCode)}";
                case Lender_ContractType.GTimaNA:
                    return $"{RuleLenderServiceHelper.PrefixNA}{maxCode.ToString(TimaSettingConstant.FormatLengthLenderCode)}";
                case Lender_ContractType.GTimaDT:
                    return $"{RuleLenderServiceHelper.PrefixDT}{maxCode.ToString(TimaSettingConstant.FormatLengthLenderCode)}";

            }
            return "";
        }
    }
}
