﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class UpdateLenderSpicesConfigCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public List<int> LoanTimes { get; set; }
        public int RateType { get; set; }
        public int RangeMoney { get; set; }
        public long UserID { get; set; }
        public long LenderID { get; set; }
        public long LenderSpicesConfigID { get; set; }
        public long CreateBy { get; set; }
    }
    public class UpdateLenderSpicesConfigCommandHandler : IRequestHandler<UpdateLenderSpicesConfigCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLenderSpicesConfig> _lenderSpicesConfigTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLenderChangeInfo> _loglenderChangeTab;
        ILogger<UpdateLenderSpicesConfigCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdateLenderSpicesConfigCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLenderSpicesConfig> lenderSpicesConfigTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLenderChangeInfo> loglenderChangeTab,
            ILogger<UpdateLenderSpicesConfigCommandHandler> logger)
        {
            _lenderSpicesConfigTab = lenderSpicesConfigTab;
            _loglenderChangeTab = loglenderChangeTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(UpdateLenderSpicesConfigCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            DateTime dtNow = DateTime.Now;
            try
            {
                if (request.UserID <= 0)
                {
                    response.Message = MessageConstant.LenderNotExist;
                    return response;
                }
                if (!request.LoanTimes.Any())
                {
                    response.Message = MessageConstant.LenderSpicesConfig_LoanTimes;
                    return response;
                }
                var lstRateType = Enum.GetValues(typeof(LenderSpicesConfig_RateType))
                            .Cast<LenderSpicesConfig_RateType>()
                            .ToList();
                var lstConfig = Enum.GetValues(typeof(LenderSpicesConfig_TypeMoney))
                            .Cast<LenderSpicesConfig_TypeMoney>()
                            .ToList();

                if (!lstRateType.Contains((LenderSpicesConfig_RateType)request.RateType))
                {
                    response.Message = MessageConstant.LenderSpicesConfig_RateTypes;
                    return response;
                }
                if (!lstConfig.Contains((LenderSpicesConfig_TypeMoney)request.RangeMoney))
                {
                    response.Message = MessageConstant.LenderSpicesConfig_RateTypes;
                    return response;
                }
                //if (request.LenderSpicesConfigID == 0)
                //{
                //    response.Message = MessageConstant.LenderSpicesConfig_RateTypes;
                //    return response;
                //}
                long minMoney = 0;
                long maxMoney = 0;
                ConvertEnumToRangeMoney(request.RangeMoney, ref minMoney, ref maxMoney);
                var configLender = await _lenderSpicesConfigTab.WhereClause(x => x.UserID == request.UserID && x.LenderID == request.LenderID).QueryAsync();
                if (configLender == null || !configLender.Any())
                {
                    var newConfig = new Domain.Tables.TblLenderSpicesConfig()
                    {
                        LoanTimes = _common.ConvertObjectToJSonV2(request.LoanTimes),
                        RateType = request.RateType.ToString(),
                        MoneyMax = maxMoney,
                        MoneyMin = minMoney,
                        UserID = request.UserID,
                        LenderID = request.LenderID,
                    };
                    long IdInsert = await _lenderSpicesConfigTab.InsertAsync(newConfig);
                    if (IdInsert > 0)
                    {
                        response.SetSucces();
                        response.Data = IdInsert;
                    }
                    return response;
                }
                var currentConfigLender = configLender.FirstOrDefault();
                var oldData = new Domain.Tables.TblLenderSpicesConfig()
                {
                    LenderSpicesConfigID = currentConfigLender.LenderSpicesConfigID,
                    LoanTimes = currentConfigLender.LoanTimes,
                    RateType = currentConfigLender.RateType,
                    MoneyMax = currentConfigLender.MoneyMax,
                    MoneyMin = currentConfigLender.MoneyMin,
                    UserID = currentConfigLender.UserID,
                    LenderID = currentConfigLender.LenderID,
                };
                currentConfigLender.MoneyMin = minMoney;
                currentConfigLender.MoneyMax = maxMoney;
                currentConfigLender.RateType = request.RateType.ToString();
                currentConfigLender.LoanTimes = _common.ConvertObjectToJSonV2(request.LoanTimes);
                string strOldData = _common.GetJsonObjectChange<Domain.Tables.TblLenderSpicesConfig>(oldData, currentConfigLender);
                bool update = await _lenderSpicesConfigTab.UpdateAsync(currentConfigLender);
                if (update)
                {
                    _loglenderChangeTab.InsertAsync(new Domain.Tables.TblLogLenderChangeInfo()
                    {
                        UserID = request.UserID,
                        CreateBy = request.CreateBy,
                        CreateDate = dtNow,
                        DataOld = strOldData,
                        LogTypeID = (int)LogLenderChangeInfo_LogTypeID.LenderSpicesConfig
                    });
                    response.SetSucces();
                    response.Data = currentConfigLender.LenderSpicesConfigID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateLenderSpicesConfigCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private void ConvertEnumToRangeMoney(int rangeMoney, ref long minMoney, ref long maxMoney)
        {
            switch ((LenderSpicesConfig_TypeMoney)rangeMoney)
            {
                case LenderSpicesConfig_TypeMoney.Bellow5:
                    minMoney = 0;
                    maxMoney = 5000000;
                    break;
                case LenderSpicesConfig_TypeMoney.From5To15:
                    minMoney = 5000000;
                    maxMoney = 15000000;
                    break;
                case LenderSpicesConfig_TypeMoney.Over15:
                    minMoney = 15000000;
                    maxMoney = long.MaxValue;
                    break;
                case LenderSpicesConfig_TypeMoney.All:
                    minMoney = 0;
                    maxMoney = long.MaxValue;
                    break;
                default:
                    minMoney = 0;
                    maxMoney = long.MaxValue;
                    break;
            }
        }
    }
}
