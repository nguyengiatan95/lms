﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class UpdateInfoLenderCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LenderID { get; set; }
        public string FullName { get; set; }
        public string NumberCard { get; set; }
        public string Phone { get; set; }
        public int Gender { get; set; }
        public string TaxCode { get; set; }
        public string Address { get; set; }
        public DateTime CreateDate { get; set; }
        public decimal RateInterest { get; set; }
        public int Status { get; set; }
        public int IsVerified { get; set; }
        public string StrBirthDay { get; set; }
        public long TotalMoney { get; set; }
        public string InviteCode { get; set; }

        public decimal? RateLender { get; set; }

        public decimal? RateAff { get; set; }

        public string Represent { get; set; }

        public string AddressOfResidence { get; set; }

        public string StrCardDate { get; set; }

        public string CardPlace { get; set; }

        public string Email { get; set; }

        public string ContractCode { get; set; }

        public string StrContractDate { get; set; }

        public string AccountBanking { get; set; }

        public long? BankID { get; set; }

        public long? LenderCareUserID { get; set; }

        public long? LenderTakeCareUserID { get; set; }

        public long? AffID { get; set; }

        public string RepresentPhone { get; set; }

        public string BranchOfBank { get; set; }
        public string InvitePhone { get; set; }
    }
    public class UpdateInfoLenderCommandHandler : IRequestHandler<UpdateInfoLenderCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        ILogger<UpdateInfoLenderCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdateInfoLenderCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            ILogger<UpdateInfoLenderCommandHandler> logger)
        {
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(UpdateInfoLenderCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var currentDate = DateTime.Now;
                var lstLender = await _lenderTab.SetGetTop(1).WhereClause(x => x.LenderID == request.LenderID).QueryAsync();
                if (lstLender == null || !lstLender.Any())
                {
                    response.Message = MessageConstant.LenderNotExist;
                    return response;
                }
                var tblLender = lstLender.First();
                if (!string.IsNullOrWhiteSpace(request.StrBirthDay))
                {
                    var birthDay = DateTime.ParseExact(request.StrBirthDay, TimaSettingConstant.DateTimeDayMonthYear, null);
                    tblLender.BirthDay = birthDay;
                }
                tblLender.Address = request.Address;
                tblLender.FullName = request.FullName;
                tblLender.Gender = request.Gender;
                tblLender.IsHub = 0;
                tblLender.IsVerified = request.IsVerified;
                tblLender.Status = request.Status;
                tblLender.TaxCode = request.TaxCode;
                tblLender.RateInterest = request.RateInterest;
                tblLender.Phone = request.Phone;
                tblLender.NumberCard = request.NumberCard;
                tblLender.ModifyDate = currentDate;
                //tblLender.InviteCode = request.InviteCode;
                tblLender.RateLender = request.RateLender;
                tblLender.RateAff = request.RateAff;
                tblLender.Represent = request.Represent;
                tblLender.AddressOfResidence = request.AddressOfResidence;
                if (!string.IsNullOrWhiteSpace(request.StrCardDate))
                {
                    var cardDate = DateTime.ParseExact(request.StrCardDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                    tblLender.CardDate = cardDate;
                }
                if (!string.IsNullOrWhiteSpace(request.StrContractDate))
                {
                    var contractDate = DateTime.ParseExact(request.StrContractDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                    tblLender.ContractDate = contractDate;
                }
                tblLender.CardPlace = request.CardPlace;
                tblLender.Email = request.Email;
                tblLender.ContractCode = request.ContractCode;
                tblLender.AccountBanking = request.AccountBanking;
                tblLender.BankID = request.BankID;
                tblLender.LenderCareUserID = request.LenderCareUserID;
                tblLender.LenderTakeCareUserID = request.LenderTakeCareUserID;
                tblLender.AffID = request.AffID;
                tblLender.RepresentPhone = request.RepresentPhone;
                tblLender.BranchOfBank = request.BranchOfBank;
                //tblLender.InviteCode = request.InviteCode;
                tblLender.InvitePhone = request.InvitePhone;
                var lenderID = await _lenderTab.UpdateAsync(tblLender);

                response.SetSucces();
                response.Data = lenderID;
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateInfoLenderCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
