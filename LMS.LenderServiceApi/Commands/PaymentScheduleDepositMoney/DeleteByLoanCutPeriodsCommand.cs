﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands.PaymentScheduleDepositMoney
{
    public class DeleteByLoanCutPeriodsCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
    }
    public class DeleteByLoanCutPeriodsCommandHandler : IRequestHandler<DeleteByLoanCutPeriodsCommand, LMS.Common.Constants.ResponseActionResult>
    {
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentScheduleDepositMoney> _paymentDepositTab;
        private readonly ILogger<DeleteByLoanCutPeriodsCommandHandler> _logger;
        private readonly LMS.Common.Helper.Utils _common;
        public DeleteByLoanCutPeriodsCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentScheduleDepositMoney> paymentDepositTab,
            ILogger<DeleteByLoanCutPeriodsCommandHandler> logger)
        {
            _paymentDepositTab = paymentDepositTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(DeleteByLoanCutPeriodsCommand request, CancellationToken cancellationToken)
        {
            DateTime dtNow = DateTime.Now;
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            try
            {
                _ = await _paymentDepositTab.DeleteWhereAsync(x => x.LoanID == request.LoanID && x.Status == (int)PaymentScheduleDepositMoney_Status.DefaultWaittingRecordDeposit);
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"DeleteByLoanCutPeriodsCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
