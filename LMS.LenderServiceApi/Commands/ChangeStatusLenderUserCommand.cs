﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class ChangeStatusLenderUserCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserID { get; set; }
        public int Status { get; set; }
        public long CreateBy { get; set; }
    }
    public class ChangeStatusLenderUserCommandHandler : IRequestHandler<ChangeStatusLenderUserCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> _userlenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLenderChangeInfo> _loglenderChangeTab;
        ILogger<ChangeStatusLenderUserCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public ChangeStatusLenderUserCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> userLenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLenderChangeInfo> loglenderChangeTab,
            ILogger<ChangeStatusLenderUserCommandHandler> logger)
        {
            _userlenderTab = userLenderTab;
            _loglenderChangeTab = loglenderChangeTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(ChangeStatusLenderUserCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            DateTime dtNow = DateTime.Now;
            try
            {
                var lenders = await _userlenderTab.WhereClause(x => x.UserID == request.UserID).QueryAsync();
                // check dữ liệu
                if (lenders == null || !lenders.Any())
                {
                    response.Message = MessageConstant.LenderNotExist;
                    return response;
                }
                var lender = lenders.FirstOrDefault();
                // map dữ liệu cũ
                var lenderOld = _common.ConvertOldToNewObject<Domain.Tables.TblUserLender>(lender);
                // gán giá trị mới
                lender.Status = request.Status;
                lender.ModifyDate = dtNow;
                // update
                var strOldData = _common.GetJsonObjectChange<Domain.Tables.TblUserLender>(lenderOld, lender);
                bool update = await _userlenderTab.UpdateAsync(lender);
                if (update)
                {
                    await _loglenderChangeTab.InsertAsync(new Domain.Tables.TblLogLenderChangeInfo()
                    {
                        UserID = request.UserID,
                        CreateBy = request.CreateBy,
                        CreateDate = dtNow,
                        DataOld = strOldData,
                        LogTypeID = (int)LogLenderChangeInfo_LogTypeID.LenderInfomation
                    });
                    response.Data = request.UserID;
                    response.SetSucces();
                    return response;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ChangeStatusLenderUserCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
