﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.ProxyTima;
using LMS.Entites.Dtos.SendMail;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class CreateLenderUserCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required(ErrorMessage = "Chưa nhập tên đối tác (FullName)", AllowEmptyStrings = true)]
        public string FullName { get; set; }
        public string DateOfBirth { get; set; }
        [Required(ErrorMessage = "Chưa nhập SĐT (Phone)", AllowEmptyStrings = true)]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Chưa nhập Email", AllowEmptyStrings = true)]
        public string Email { get; set; }
        public int Gender { get; set; }
        public string PermanentAddress { get; set; } // địa chỉ thường trú
        public string TemporaryAddress { get; set; } //địa chỉ tạm trú,
        public int CityID { get; set; }
        public int DistrictID { get; set; }
        public int WardID { get; set; }
        public string CardNumber { get; set; }
        public string CardDate { get; set; }
        public string CardPlace { get; set; }
        public int IsVerified { get; set; }
        public long CreateBy { get; set; }
        
    }
    public class CreateLenderUserCommandHandler : IRequestHandler<CreateLenderUserCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> _userlenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> _queueCallAPITab;
        ILogger<CreateLenderUserCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        RedisCachedServiceHelper _redisCachedService;
        public CreateLenderUserCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> userLenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> queueCallAPITab,
            RedisCachedServiceHelper redisCachedService,
            ILogger<CreateLenderUserCommandHandler> logger)
        {
            _userlenderTab = userLenderTab;
            _userTab = userTab;
            _queueCallAPITab = queueCallAPITab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _redisCachedService = redisCachedService;
        }

        public async Task<ResponseActionResult> Handle(CreateLenderUserCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            DateTime dtNow = DateTime.Now;
            try
            {
                // check giá trị ban đầu
                string message = string.Empty;
                long lenderCodeID = 1;
                bool value = _common.CheckValidInputString<CreateLenderUserCommand>(request, ref message);
                if (!value)
                {
                    response.Message = message;
                    return response;
                }

                //b1: kiểm tra email, số dt trong bảng tblUserLender nếu tồn tại và đã xác thực thì không cho tạo
                var userLenderExist = await _userlenderTab.SetGetTop(1).WhereClause(x => x.Phone == request.Phone && x.Email == request.Email).QueryAsync();
                if (userLenderExist != null && userLenderExist.Any())
                {
                    response.Message = MessageConstant.UserLender_Existed;
                    return response;
                }
                //b2: tạo thông tin user đăng nhập:
                //b2_1: userName = "LD_" + "Số thứ tự tăng dần gồm 6 chữ số" bắt đầu từ 000001
                var userNameLender = TimaSettingConstant.UserNameLenderConfig;               
                lenderCodeID = await _redisCachedService.GetLenderCodeIDNext();
                string newCodeLender = $"{userNameLender}{lenderCodeID.ToString(TimaSettingConstant.FormatLengthLenderCode)}";
                var password = _common.RandomPassword();
                var userInsert = new Domain.Tables.TblUser()
                {
                    UserName = newCodeLender,
                    CreateDate = dtNow,
                    Email = request.Email,
                    FullName = newCodeLender,
                    Password = _common.MD5(password),
                    Status = (int)User_Status.Active,
                    UserTypeID = (int)User_UserTypeID.Lender
                };
                long userIDInsert = await _userTab.InsertAsync(userInsert);
                //b2_2: UserTypeID = 2, Status = 1, IsVerified = đã xác thực hay không, createdate = ngày tạo
                //b3: thêm vào bảng tblUserLender: gồm UserID mới thêm thành công
                if (userIDInsert > 0)
                {
                    var userLender = new Domain.Tables.TblUserLender()
                    {
                        Status = (int)LenderUser_Status.Active,
                        CityID = request.CityID,
                        DistrictID = request.DistrictID,
                        Email = request.Email,
                        FullName = request.FullName,
                        Gender = request.Gender,
                        NumberCard = request.CardNumber,
                        Phone = request.Phone,
                        PermanentResidenceAddress = request.PermanentAddress,
                        TemporaryResidenceAddress = request.TemporaryAddress,
                        UserID = userIDInsert,
                        WardID = request.WardID,
                        IsVerified = request.IsVerified,
                        CreateDate = dtNow,
                        CardPlace = request.CardPlace,
                        ModifyDate = dtNow,
                    };
                    if (!string.IsNullOrEmpty(request.CardDate))
                    {
                        userLender.CardDate = DateTime.ParseExact(request.CardDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                    }
                    if (!string.IsNullOrEmpty(request.DateOfBirth))
                    {
                        userLender.DateOfBirth = DateTime.ParseExact(request.DateOfBirth, TimaSettingConstant.DateTimeDayMonthYear, null);
                    }
                    var insertLender = await _userlenderTab.InsertAsync(userLender);
                    #region gửi mail cho nhân viên tạo NĐT
                    var objUser = (await _userTab.WhereClause(x => x.UserID == request.CreateBy).QueryAsync()).FirstOrDefault();
                    if (objUser != null && !string.IsNullOrEmpty(objUser.Email))
                    {
                        var objSendMail = new Domain.Models.SendMail()
                        {
                            Email = objUser.Email,
                            Department = ProxyTimaConfiguration.EmailDepartment,
                            Domain = ProxyTimaConfiguration.EmailDomain,
                            Title = "Thông tin nhà đầu tư",
                            Content = $"Họ tên NĐT: <b>{request.FullName}</b>  <br /> " +
                                      $"SĐT: {request.Phone} <br />" +
                                      $"CMT/CCCD: {request.CardNumber}<br />" +
                                      $"Ngày sinh: {request.DateOfBirth}<br />" +
                                      $"Giới tính: {((Lender_Gender)request.Gender).GetDescription()} <br /> " +
                                      $"Địa chỉ thường trú: {request.PermanentAddress} <br /> " +
                                      $"Địa chỉ tạm trú: {request.TemporaryAddress} <br /> " +
                                      $"User: {newCodeLender} <br />" +
                                      $"Password: {password} <br />"
                        };
                        var queueCallAPI = new Domain.Tables.TblQueueCallAPI
                        {
                            Domain = ProxyTimaConfiguration.BaseUrl,
                            Path = ProxyTimaConfiguration.SendEmail,
                            Method = "POST",
                            JsonRequest = _common.ConvertObjectToJSonV2(objSendMail),
                            Status = 0,
                            Retry = 0,
                            CreateDate = DateTime.Now,
                            Description = QueueCallAPI_Description.SendMail.GetDescription()
                        };
                        _ = await _queueCallAPITab.InsertAsync(queueCallAPI);
                    }
                    #endregion
                    response.SetSucces();
                    response.Data = insertLender;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateLenderDebtCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;

        }

       
    }
}
