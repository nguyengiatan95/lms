﻿using LMS.Common.Constants;
using LMS.LenderServiceApi.Domain.AG;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class CreateOrUpdateLenderFromAGCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long AGLenderID { get; set; }
    }
    public class CreateOrUpdateLenderFromAGCommandHandler : IRequestHandler<CreateOrUpdateLenderFromAGCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> _userLenderTab;
        LMS.Common.DAL.ITableHelper<Domain.AG.TblLenderAG> _lenderAGTab;
        LMS.Common.DAL.ITableHelper<Domain.AG.TblUserAG> _userAGTab;
        LMS.Common.DAL.ITableHelper<Domain.AG.TblLenderSpicesConfigAG> _lenderSpicesConfigAGTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLenderSpicesConfig> _lenderSpicesLMSTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userLMSTab;
        LMS.Common.DAL.ITableHelper<Domain.AG.TblFptEContract> _fptEcontractAGTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLenderDigitalSignature> _userLenderSignatureTab;

        ILogger<CreateOrUpdateLenderFromAGCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateOrUpdateLenderFromAGCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.AG.TblLenderAG> lenderAGTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> userLenderTab,
            LMS.Common.DAL.ITableHelper<Domain.AG.TblUserAG> userAGTab,
            LMS.Common.DAL.ITableHelper<Domain.AG.TblLenderSpicesConfigAG> lenderSpicesConfigAGTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLenderSpicesConfig> lenderSpicesLMSTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userLMSTab,
            LMS.Common.DAL.ITableHelper<Domain.AG.TblFptEContract> fptEcontractAGTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLenderDigitalSignature> userLenderSignatureTab,
        ILogger<CreateOrUpdateLenderFromAGCommandHandler> logger)
        {
            _lenderTab = lenderTab;
            _userLenderTab = userLenderTab;
            _lenderSpicesLMSTab = lenderSpicesLMSTab;
            _userLMSTab = userLMSTab;
            _userLenderSignatureTab = userLenderSignatureTab;
            _lenderAGTab = lenderAGTab.SetConnectString(LMS.Common.Configuration.ConnectStringSetting.AGConnectString);
            _userAGTab = userAGTab.SetConnectString(LMS.Common.Configuration.ConnectStringSetting.AGConnectString);
            _lenderSpicesConfigAGTab = lenderSpicesConfigAGTab.SetConnectString(LMS.Common.Configuration.ConnectStringSetting.AGConnectString);
            _fptEcontractAGTab = fptEcontractAGTab.SetConnectString(LMS.Common.Configuration.ConnectStringSetting.AGConnectString);
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(CreateOrUpdateLenderFromAGCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                List<Domain.Tables.TblLenderSpicesConfig> lenderConfigs = new List<Domain.Tables.TblLenderSpicesConfig>();
                List<Domain.Tables.TblUserLenderDigitalSignature> lenderSignatures = new List<Domain.Tables.TblUserLenderDigitalSignature>();

                // lender AG
                var taskLenderLMS = _lenderTab.SetGetTop(1).WhereClause(x => x.LenderID == request.AGLenderID).QueryAsync();
                var taslLenderAG = _lenderAGTab.SetGetTop(1).WhereClause(x => x.ShopID == request.AGLenderID).QueryAsync();
                var taskConfigSpicesAG = _lenderSpicesConfigAGTab.SetGetTop(1000).WhereClause(x => x.ShopId == request.AGLenderID).QueryAsync();
                await Task.WhenAll(taslLenderAG, taskLenderLMS, taskConfigSpicesAG);
                var lstLenderLMS = taskLenderLMS.Result;
                var lenderAG = taslLenderAG.Result.FirstOrDefault();
                var lstConfigSpicesLender = taskConfigSpicesAG.Result;
                int fixUserAG = 66111;
                int Status = 1;
                if (lenderAG.ShopID == 3703 || lenderAG.IsHub.Value)
                {
                    Status = lenderAG.Status ?? 0;
                }
                else
                {
                    Status = (lenderAG.IsAgent != -1) ? lenderAG.IsAgent ?? 0 : -11;
                }

                if (lstLenderLMS == null || !lstLenderLMS.Any())
                {
                    Domain.AG.TblUserAG userLenderAG = null;
                    int userCreate = 0;
                    if (lenderAG.SelfEmployed < 5)
                    {
                        userLenderAG = (await _userAGTab.WhereClause(x => x.Username == lenderAG.Name).QueryAsync()).FirstOrDefault();
                        userCreate = userLenderAG != null ? userLenderAG.Id : 0;
                    }
                    else if (lenderAG.TypeInvestment == 2)
                    {
                        userCreate = fixUserAG;
                    }
                    else if (lenderAG.SelfEmployed >= 5)
                    {
                        userLenderAG = (await _userAGTab.WhereClause(x => x.Username == lenderAG.Phone).QueryAsync()).FirstOrDefault();
                        userCreate = userLenderAG != null ? userLenderAG.Id : 0;
                    }
                    int contractType = (int)(lenderAG.TypeInvestment == 2 ? (lenderAG.Name.Contains("DT") ? Lender_ContractType.DTTima : Lender_ContractType.NAOfTima) : (lenderAG.Name.Contains("DT") ? Lender_ContractType.DTNormal : Lender_ContractType.NANormal));
                    // list lender update

                    Domain.Tables.TblLender lender = new Domain.Tables.TblLender()
                    {
                        Address = lenderAG.Address,
                        CreateDate = lenderAG.CreatedDate.Value,
                        FullName = lenderAG.Name,
                        Phone = lenderAG.Phone,
                        Status = Status,
                        RateInterest = lenderAG.RateInterest, // đơn vị tính trên 1 trịu
                        NumberCard = lenderAG.PersonCardNumber,
                        BirthDay = lenderAG.PersonBirthDay,
                        Gender = lenderAG.PersonGender,
                        TimaLenderID = lenderAG.ShopID,
                        ModifyDate = lenderAG.CreatedDate.Value,
                        TotalMoney = lenderAG.TotalMoney.Value,
                        TaxCode = lenderAG.TaxCode,
                        LenderID = lenderAG.ShopID,
                        IsHub = lenderAG.ShopID == 3703 || lenderAG.IsHub.Value ? 1 : 0,
                        IsVerified = lenderAG.Status == 1 ? 1 : 0,
                        UnitRate = (int)LMS.Common.Constants.Lender_UnitRate.Default,
                        AccountBanking = lenderAG.AccountBanking,
                        AddressOfResidence = lenderAG.AddressOfResidence,
                        BankID = lenderAG.BankId,
                        AffID = lenderAG.AffID,
                        BranchOfBank = lenderAG.BranchOfBank,
                        CardDate = lenderAG.CardNumberDate,
                        CardPlace = lenderAG.CardNumberPlace,
                        ContractCode = lenderAG.ContractEscrow,
                        ContractDate = lenderAG.DateOfContractEscrow,
                        Email = lenderAG.Email,
                        //InviteCode = lenderAG.InviteCode,
                        LenderCareUserID = lenderAG.LenderCareUserId,
                        LenderTakeCareUserID = lenderAG.LenderTakeCareUserId,
                        RateAff = lenderAG.RateAff,
                        RateLender = lenderAG.RateLender,
                        Represent = lenderAG.Represent,
                        RepresentPhone = lenderAG.PersonContactPhone,
                        IsGcash = lenderAG.IsGCash == true ? 1 : 0,
                        RegFromApp = lenderAG.RegFromApp == true ? 1 : 0,
                        InvitePhone = lenderAG.InvitePhone,
                        SelfEmployed = lenderAG.SelfEmployed ?? 0,
                        OwnerUserID = userCreate,
                        ContractType = contractType
                    };
                    // insert userlender
                    Domain.Tables.TblUserLender userLender = new Domain.Tables.TblUserLender()
                    {
                        CardDate = lenderAG.CardNumberDate,
                        CardPlace = lenderAG.CardNumberPlace,
                        CityID = (long)(lenderAG.CityId ?? 0),
                        CreateDate = lenderAG.CreatedDate.Value,
                        DistrictID = (long)(lenderAG.DistrictId ?? 0),
                        Email = lenderAG.Email,
                        FullName = lenderAG.Name,
                        Gender = lenderAG.PersonGender,
                        IsVerified = lenderAG.Status == 1 ? 1 : 0,
                        ModifyDate = lenderAG.CreatedDate.Value,
                        NumberCard = lenderAG.PersonCardNumber,
                        PermanentResidenceAddress = lenderAG.Address,
                        TemporaryResidenceAddress = lenderAG.AddressOfResidence,
                        Phone = lenderAG.Phone,
                        Status = (int)lenderAG.Status == (int)LenderUser_Status.Active || (int)lenderAG.Status == (int)LenderUser_Status.UnActive ? (int)lenderAG.Status : (int)LenderUser_Status.Waiting,
                        UserID = userCreate,
                        WardID = lenderAG.WardId ?? 0,
                    };
                    // cấu hình khẩu vị
                    foreach (var item in lstConfigSpicesLender)
                    {
                        lenderConfigs.Add(new Domain.Tables.TblLenderSpicesConfig()
                        {
                            LenderID = item.ShopId,
                            LoanTimes = "[" + item.LoanTimes + "]",
                            MoneyMax = item.MoneyMax,
                            MoneyMin = item.MoneyMin,
                            RateType = item.RateType.ToString(),
                            UserID = userCreate
                        });
                    }
                    // chữ kí số
                    var fptEContractAG = await _fptEcontractAGTab.WhereClause(x => x.LenderId == request.AGLenderID).QueryAsync();
                    if (fptEContractAG != null)
                    {
                        foreach (var item in fptEContractAG)
                        {
                            lenderSignatures.Add(new Domain.Tables.TblUserLenderDigitalSignature()
                            {
                                AgreementUUID = item.AgreementId,
                                CreateBy = item.UserId ?? 0,
                                CreateDate = item.CreateDate ?? DateTime.Now,
                                LenderID = item.LenderId,
                                PassCode = item.Passcode,
                                Status = item.Status == (int)Base_Status_AG.Complete ? (int)Base_Status_AG.Complete : (int)Base_Status_AG.NotComplete,
                                UserID = userCreate,
                            });
                        }
                        _userLenderSignatureTab.InsertBulk(lenderSignatures);
                    }
                    // insert User LMS
                    if (userCreate > 0 && userCreate != fixUserAG)
                    {
                        var userLMS = await _userLMSTab.WhereClause(x => x.TimaUserID == userCreate).QueryAsync();
                        if (userLMS == null || !userLMS.Any())
                        {
                            Domain.Tables.TblUser user = new Domain.Tables.TblUser()
                            {
                                UserID = userLenderAG.Id,
                                UserName = userLenderAG.Username,
                                TimaUserID = userLenderAG.Id,
                                CreateDate = DateTime.Now,
                                DepartmentID = 0,
                                Email = userLenderAG.Email,
                                FullName = userLenderAG.FullName,
                                Password = userLenderAG.Password,
                                Status = ConvertStatusToLMS(userLenderAG.Status)
                            };
                            _ = await _userLMSTab.InsertAsync(user, setOffIdentity: true);
                        }

                    }
                    _ = await _lenderTab.InsertAsync(lender, setOffIdentity: true);
                    _ = await _userLenderTab.InsertAsync(userLender, setOffIdentity: false);
                    _lenderSpicesLMSTab.InsertBulk(lenderConfigs);
                    response.SetSucces();
                    response.Data = lender.LenderID;
                }
                else
                {
                    //update
                    var objLender = lstLenderLMS.FirstOrDefault();
                    objLender.Status = Status;
                    objLender.IsVerified = lenderAG.Status == 1 ? 1 : 0;
                    objLender.TimaLenderID = lenderAG.ShopID;
                    _ = await _lenderTab.UpdateAsync(objLender);
                    response.SetSucces();
                    response.Data = objLender.LenderID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateOrUpdateLenderFromAGCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        private int ConvertStatusToLMS(int status)
        {
            switch ((Base_Status_AG)status)
            {
                case Base_Status_AG.Complete:
                    {
                        return (int)LMS.Common.Constants.User_Status.Active;
                    }
                case Base_Status_AG.Deleted:
                    {
                        return (int)LMS.Common.Constants.User_Status.Deleted;
                    }
                case Base_Status_AG.NotComplete:
                    {
                        return (int)LMS.Common.Constants.User_Status.Locked;
                    }
                default:
                    {
                        return -1;
                    }
            }
        }
    }

}
