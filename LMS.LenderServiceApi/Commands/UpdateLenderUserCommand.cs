﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class UpdateLenderUserCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required(ErrorMessage = "Chưa nhập tên đối tác (FullName)", AllowEmptyStrings = true)]
        public string FullName { get; set; }
        public string DateOfBirth { get; set; }
        [Required(ErrorMessage = "Chưa nhập SĐT (Phone)", AllowEmptyStrings = true)]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Chưa nhập Email", AllowEmptyStrings = true)]
        public string Email { get; set; }
        public int Gender { get; set; }
        public string PermanentAddress { get; set; } // địa chỉ thường trú
        public string TemporaryAddress { get; set; } //địa chỉ tạm trú,
        public int CityID { get; set; }
        public int DistrictID { get; set; }
        public int WardID { get; set; }
        public string CardNumber { get; set; }
        public string CardDate { get; set; }
        public string CardPlace { get; set; }
        public int IsVerified { get; set; }
        public long UserID { get; set; }
    }
    public class UpdateLenderUserCommandHandler : IRequestHandler<UpdateLenderUserCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> _userlenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<UpdateLenderUserCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdateLenderUserCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> userLenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            ILogger<UpdateLenderUserCommandHandler> logger)
        {
            _userlenderTab = userLenderTab;
            _userTab = userTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(UpdateLenderUserCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            DateTime dtNow = DateTime.Now;
            try
            {
                // check giá trị ban đầu
                string message = string.Empty;
                bool value = _common.CheckValidInputString<UpdateLenderUserCommand>(request, ref message);
                if (!value)
                {
                    response.Message = message;
                    return response;
                }
                var userExist = await _userTab.WhereClause(x => x.UserID == request.UserID && x.UserTypeID == (int)User_UserTypeID.Lender).QueryAsync();
                if (userExist == null || !userExist.Any())
                {
                    // lender k tồn tại
                    response.Message = MessageConstant.LenderNotExist;
                    return response;
                }
                // lấy lender ra update lại
                var lender = userExist.FirstOrDefault();
                lender.Email = request.Email;
                lender.FullName = request.FullName;
                var lenderUpdate = await _userTab.UpdateAsync(lender);
                if (lenderUpdate)
                {
                    //update lender user
                    var lenderUsers = await _userlenderTab.WhereClause(x => x.UserID == request.UserID).QueryAsync();
                    if (lenderUsers != null && lenderUsers.Any())
                    {
                        foreach (var item in lenderUsers)
                        {
                            item.Status = (int)LenderUser_Status.Active;
                            item.CityID = request.CityID;
                            item.DistrictID = request.DistrictID;
                            item.Email = request.Email;
                            item.FullName = request.FullName;
                            item.Gender = request.Gender;
                            item.NumberCard = request.CardNumber;
                            item.Phone = request.Phone;
                            item.PermanentResidenceAddress = request.PermanentAddress;
                            item.TemporaryResidenceAddress = request.TemporaryAddress;
                            item.WardID = request.WardID;
                            item.IsVerified = request.IsVerified;
                            item.CardPlace = request.CardPlace;
                            item.ModifyDate = dtNow;
                            if (!string.IsNullOrEmpty(request.CardDate))
                            {
                                item.CardDate = DateTime.ParseExact(request.CardDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                            }
                            if (!string.IsNullOrEmpty(request.DateOfBirth))
                            {
                                item.DateOfBirth = DateTime.ParseExact(request.DateOfBirth, TimaSettingConstant.DateTimeDayMonthYear, null);
                            }
                        }
                        _userlenderTab.UpdateBulk(lenderUsers);
                        response.Data = request.UserID;
                        response.SetSucces();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateLenderUserCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;

        }
    }
}
