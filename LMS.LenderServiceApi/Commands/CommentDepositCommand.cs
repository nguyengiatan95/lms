﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class CommentDepositCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long PaymentScheduleDepositMoneyID { get; set; }
        public string Note { get; set; }
        public long UserID { get; set; }
    }
    public class CommentDepositCommandHandler : IRequestHandler<CommentDepositCommand, LMS.Common.Constants.ResponseActionResult>
    {
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentScheduleDepositMoney> _paymentDepositTab;
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogActionPaymentDepositMoney> _logTab;
        private readonly ILogger<CommentDepositCommandHandler> _logger;
        private readonly LMS.Common.Helper.Utils _common;
        public CommentDepositCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentScheduleDepositMoney> paymentDepositTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogActionPaymentDepositMoney> logTab,
            ILogger<CommentDepositCommandHandler> logger)
        {
            _paymentDepositTab = paymentDepositTab;
            _logTab = logTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(CommentDepositCommand request, CancellationToken cancellationToken)
        {
            DateTime dtNow = DateTime.Now;
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            try
            {
                var paymentScheduleDeposit = _paymentDepositTab.GetByID(request.PaymentScheduleDepositMoneyID);
                long insert = await _logTab.InsertAsync(new Domain.Tables.TblLogActionPaymentDepositMoney()
                {
                    PaymentScheduleDepositMoneyID = request.PaymentScheduleDepositMoneyID,
                    CreateDate = dtNow,
                    LoanID = paymentScheduleDeposit.LoanID,
                    Note = request.Note,
                    UserID = request.UserID,
                });
                if (insert > 0) response.SetSucces();
                response.Data = insert;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CommentDepositCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}

