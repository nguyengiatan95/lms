﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Commands
{
    public class VerifyUserLenderCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserID { get; set; }
        public int IsVerified { get; set; }
        public long CreateBy { get; set; }
    }
    public class VerifyUserLenderCommandHandler : IRequestHandler<VerifyUserLenderCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> _userlenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLenderChangeInfo> _loglenderChangeTab;
        ILogger<VerifyUserLenderCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public VerifyUserLenderCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> userLenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLenderChangeInfo> loglenderChangeTab,
            ILogger<VerifyUserLenderCommandHandler> logger)
        {
            _userlenderTab = userLenderTab;
            _loglenderChangeTab = loglenderChangeTab;
            _userTab = userTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(VerifyUserLenderCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            DateTime dtNow = DateTime.Now;
            try
            {
                if (request.UserID <= 0)
                {
                    response.Message = MessageConstant.LenderNotExist;
                    return response;
                }

                var taskUserLender = _userlenderTab.WhereClause(x => x.UserID == request.UserID).QueryAsync();
                var taskUser = _userTab.WhereClause(x => x.UserID == request.UserID).QueryAsync();
                await Task.WhenAll(taskUserLender, taskUser);
                var userLender = taskUserLender.Result.FirstOrDefault();
                var lenderCurrent = new Domain.Tables.TblUserLender()
                {
                    CardDate = userLender.CardDate,
                    CardPlace = userLender.CardPlace,
                    CityID = userLender.CityID,
                    CreateDate = userLender.CreateDate,
                    DateOfBirth = userLender.DateOfBirth,
                    DistrictID = userLender.DistrictID,
                    Email = userLender.Email,
                    FullName = userLender.FullName,
                    Gender = userLender.Gender,
                    IsVerified = userLender.IsVerified,
                    NumberCard = userLender.NumberCard,
                    PermanentResidenceAddress = userLender.PermanentResidenceAddress,
                    Phone = userLender.Phone,
                    Status = userLender.Status,
                    TemporaryResidenceAddress = userLender.TemporaryResidenceAddress,
                    UserID = userLender.UserID,
                    UserLenderID = userLender.UserLenderID,
                    WardID = userLender.WardID,
                };
                var user = taskUser.Result.FirstOrDefault();
                if (string.IsNullOrWhiteSpace(userLender.FullName) || string.IsNullOrWhiteSpace(userLender.Phone) || string.IsNullOrWhiteSpace(userLender.Email) || string.IsNullOrWhiteSpace(userLender.NumberCard))
                {
                    response.Message = MessageConstant.UserLender_Verify_LostInfomation;
                    return response;
                }
                userLender.Status = (int)LenderUser_Status.Active;
                userLender.IsVerified = (int)LenderUser_Verified.Active;
                user.Status = (int)User_Status.Active;
                await Task.WhenAll(_userlenderTab.UpdateAsync(userLender), _userTab.UpdateAsync(user));
                string strOldData = _common.GetJsonObjectChange<Domain.Tables.TblUserLender>(lenderCurrent, userLender);
                _loglenderChangeTab.InsertAsync(new Domain.Tables.TblLogLenderChangeInfo()
                {
                    UserID = request.UserID,
                    CreateBy = request.CreateBy,
                    CreateDate = dtNow,
                    DataOld = strOldData,
                    LogTypeID = (int)LogLenderChangeInfo_LogTypeID.LenderInfomation
                });
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateMoneyLenderCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
