﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Constants
{

    public enum AG_Type_CloseLoan
    {
        [Description("Tất toán")]
        Finalization = 0,

        [Description("Thanh Lý")]
        Liquidation = 1,

        [Description("Thanh Lý bảo hiểm")]
        LiquidationLoanInsurance = 2,

        [Description("Bồi thường bảo hiểm")]
        IndemnifyLoanInsurance = 3,
        [Description("Đã thu đủ tiền sau khi bồi thường BH")]
        PaidInsurance = 4,
        [Description("Xin miễn giảm")]
        Exemption = 5,
        [Description("Xin miễn giảm bồi thường bảo hiểm")]
        PaidInsuranceExemption = 6
    }

    public enum AG_Status_Loan
    {
        [Description("Đang vay")]
        Lending = 11,
        [Description("Đến ngày đóng họ ")]
        ToDayPay = 12,
        [Description("Chậm Lãi")]
        DelayInterest = 13,
        [Description("Ngày cuối họ")] // Tra goc
        Delayed = 14,
        [Description("Quá hạn")]
        Delayed2 = 15,
        [Description("Nợ xấu")]
        WaitLiquidation = 100,
        //[Description("Đã Thanh Lý")]
        //Liquidated = -1,
        [Description("Đóng hợp đồng")]
        Acquision = 1,
        [Description("Đã Xóa")]
        Deleted = 0
    }
}
