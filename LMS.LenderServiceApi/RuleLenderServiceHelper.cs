﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi
{
    public class RuleLenderServiceHelper
    {
        public static string PrefixRedisEnvironmentName = "EnvironmentName";
        public const string PrefixDT = "DT";
        public const string PrefixNA = "NA1";
        public static string KeyRedistLenderCodeIDNext = $"{PrefixRedisEnvironmentName}:LenderServiceApi:LenderCodeIDNext";

        public const string SmsContentSendLenderRegisterSuccess = "Chao mung ban den voi san tai chinh Tima.Tai khoan: {0}. Mat khau: {1}";
    }
}
