﻿using LMS.Common.Helper;
using LMS.LenderServiceApi.Domain.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Services
{
    public interface IQueueCallAPIManager
    {
        Task<long> LenderDepositMoney(int userID, string fullName, List<UpdateDepositLoanMoney> LstLoanStatus);
    }
    public class QueueCallAPIManager : IQueueCallAPIManager
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> _queueCallAPITab;
        Utils _common;
        ILogger<QueueCallAPIManager> _logger;
        public QueueCallAPIManager(LMS.Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> queueCallAPITab,
          ILogger<QueueCallAPIManager> logger)
        {
            _queueCallAPITab = queueCallAPITab;
            _common = new Utils();
            _logger = logger;
        }

        public async Task<long> LenderDepositMoney(int userID, string fullName, List<UpdateDepositLoanMoney> LstLoanStatus)
        {
            DateTime dtNow = DateTime.Now;
            var request = new
            {
                UserID = userID,
                FullName = fullName,
                LstLoanStatus = LstLoanStatus,
            };
            try
            {
                Domain.Tables.TblQueueCallAPI tblQueueCallAPI = new Domain.Tables.TblQueueCallAPI()
                {
                    CreateDate = dtNow,
                    Description = "Thay đổi trạng thái đặt cọc LMS -> AG",
                    Domain = LMS.Entites.Dtos.AG.AGConstants.BaseUrl,
                    JsonRequest = _common.ConvertObjectToJSonV2(request),
                    Method = "POST",
                    Path = LMS.Common.Constants.ActionApiExternal.LENDER_LenderDepositMoney,
                    Status = (int)LMS.Common.Constants.QueueCallAPI_Status.Waiting,
                    Retry = 0
                };
                return await _queueCallAPITab.InsertAsync(tblQueueCallAPI);
            }
            catch (Exception ex)
            {
                _logger.LogError($"LenderDepositMoney|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                return 0;
            }
        }
    }
}
