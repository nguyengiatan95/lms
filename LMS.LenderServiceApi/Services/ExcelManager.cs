﻿using LMS.Common.Helper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Services
{
    public interface IExcelManager
    {
        int TypeReport { get; }
        Task RunDataAsync(long excelID, string request);
    }
    public class ExcelDeposit : IExcelManager
    {
        private readonly LMS.Common.Helper.Utils _common;
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> _excelReportTab;
        private readonly IMediator _bus;
        public ExcelDeposit(LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> excelReportTab, IMediator bus)
        {
            _common = new Common.Helper.Utils();
            _excelReportTab = excelReportTab;
            _bus = bus;
        }
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.Lender_Deposit;

        public async Task RunDataAsync(long excelID, string request)
        {
            Domain.Models.RequestExcelLenderDeposit data = _common.ConvertJSonToObjectV2<Domain.Models.RequestExcelLenderDeposit>(request);
            var excelDetail = (await _excelReportTab.WhereClause(x => x.ExcelReportID == excelID).QueryAsync()).FirstOrDefault();
            var responseData = await _bus.Send(new Queries.GetLoanDepositQuery()
            {
                ContractCode = data.ContractCode,
                CustomerName = data.CustomerName,
                FromDate = data.FromDate,
                LenderID = data.LenderID,
                PageIndex = 1,
                PageSize = int.MaxValue,
                Status = data.Status,
                ToDate = data.ToDate
            });
            excelDetail.Status = responseData.Result == (int)LMS.Common.Constants.ResponseAction.Success ? (int)Common.Constants.ExcelReport_Status.Success : (int)Common.Constants.ExcelReport_Status.Recall;
            excelDetail.ExtraData = responseData.Result == (int)LMS.Common.Constants.ResponseAction.Success ? _common.ConvertObjectToJSonV2(responseData.Data as List<Domain.Models.PaymentScheduleDepositMoneyView>) : "";
            excelDetail.ModifyDate = DateTime.Now;
            excelDetail.ReportName = LMS.Common.Constants.ExcelReport_TypeReport.Lender_Deposit.GetDescription();
            _ = await _excelReportTab.UpdateAsync(excelDetail);
        }
    }

    public class ExcelBM01 : IExcelManager
    {
        private readonly LMS.Common.Helper.Utils _common;
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> _excelReportTab;
        private readonly IMediator _bus;
        public ExcelBM01(LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> excelReportTab, IMediator bus)
        {
            _common = new Common.Helper.Utils();
            _excelReportTab = excelReportTab;
            _bus = bus;
        }
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.Lender_BM01;

        public async Task RunDataAsync(long excelID, string request)
        {
            Domain.Models.RequestExcelLenderDeposit data = _common.ConvertJSonToObjectV2<Domain.Models.RequestExcelLenderDeposit>(request);
            var excelDetail = (await _excelReportTab.WhereClause(x => x.ExcelReportID == excelID).QueryAsync()).FirstOrDefault();
            var responseData = await _bus.Send(new Queries.GetDataBM01Query()
            {
                ContractCode = data.ContractCode,
                CustomerName = data.CustomerName,
                FromDate = data.FromDate,
                LenderID = data.LenderID,
                ToDate = data.ToDate
            });
            excelDetail.Status = responseData.Result == (int)LMS.Common.Constants.ResponseAction.Success ? (int)Common.Constants.ExcelReport_Status.Success : (int)Common.Constants.ExcelReport_Status.Recall;
            excelDetail.ExtraData = responseData.Result == (int)LMS.Common.Constants.ResponseAction.Success ? _common.ConvertObjectToJSonV2(responseData.Data as Entites.Dtos.LenderService.Excel.BM01Excel) : "";
            excelDetail.ModifyDate = DateTime.Now;
            excelDetail.ReportName = LMS.Common.Constants.ExcelReport_TypeReport.Lender_BM01.GetDescription();
            _ = await _excelReportTab.UpdateAsync(excelDetail);
        }
    }
}


