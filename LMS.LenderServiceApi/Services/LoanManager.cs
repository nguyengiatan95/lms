﻿using LMS.Common.Helper;
using LMS.LenderServiceApi.Constants;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Services
{
    public interface ILoanManager
    {
        Task<int> ConvertStatusLMSToAG(int status, DateTime nextDate);
    }
    public class LoanManager : ILoanManager
    {
        ILogger<LoanManager> _logger;
        LMS.Common.Helper.Utils _common;
        public LoanManager(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            ILogger<LoanManager> logger)
        {
            _logger = logger;
            _common = new Utils();
        }

        public async Task<int> ConvertStatusLMSToAG(int status, DateTime nextDate)
        {
            return await Task.Run(() =>
            {
                DateTime dtNow = DateTime.Now.Date;
                int statusAG = 0;
                switch ((Common.Constants.Loan_Status)status)
                {
                    case Common.Constants.Loan_Status.Lending:
                        {
                            if (nextDate < dtNow) // chua đến ngày đóng tiền
                            {
                                statusAG = (int)AG_Status_Loan.Lending;
                            }
                            else if (nextDate == dtNow) // hm nay đóng tiền
                            {
                                statusAG = (int)AG_Status_Loan.ToDayPay;
                            }
                            else // chậm lãi
                            {
                                statusAG = (int)AG_Status_Loan.DelayInterest;
                            }
                            break;
                        }
                    case Common.Constants.Loan_Status.ClosePaidInsuranceExemption:
                    case Common.Constants.Loan_Status.CloseExemption:
                    case Common.Constants.Loan_Status.ClosePaidInsurance:
                    case Common.Constants.Loan_Status.CloseIndemnifyLoanInsurance:
                    case Common.Constants.Loan_Status.CloseLiquidationLoanInsurance:
                    case Common.Constants.Loan_Status.CloseLiquidation:
                    case Common.Constants.Loan_Status.Close:
                        statusAG = (int)AG_Status_Loan.Acquision;
                        break;
                    default:
                        {
                            break;
                        }
                }
                return statusAG;
            });
        }
    }
}
