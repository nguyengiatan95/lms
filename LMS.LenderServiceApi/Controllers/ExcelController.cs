﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExcelController : ControllerBase
    {
        private readonly IMediator _bus;
        RestClients.IUserService _userServices;
        public ExcelController(IMediator bus, RestClients.IUserService userServices)
        {
            this._bus = bus;
            _userServices = userServices;
        }
        [HttpPost]
        [Route("AddRequestExcel")]
        public async Task<LMS.Common.Constants.ResponseActionResult> AddRequestExcel(Commands.AddExcelCommand request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("GetByTraceID")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetByTraceID(Domain.Tables.RequestGetExcelByTraceID request)
        {

            return await _bus.Send(new Queries.GetExcelReportByTraceIDQuery
            {
                CreateBy = request.CreateBy,
                TraceIDRequest = request.TraceIDRequest,
                UserIDLogin = _userServices.GetHeaderUserID()
            });
        }

    }
}
