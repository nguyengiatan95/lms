﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CollaboratorLenderController : ControllerBase
    {
        private readonly RestClients.IUserService _userServices;
        private readonly IMediator bus;
        public CollaboratorLenderController(IMediator bus, RestClients.IUserService userServices)
        {
            this.bus = bus;
            _userServices = userServices;
        }
        [HttpPost]
        [Route("CreateCollaboratorLender")]
        public async Task<Common.Constants.ResponseActionResult> CreateCollaboratorLender(Commands.CollaboratorLender.CreateCollaboratorLenderCommand request)
        {
            return await bus.Send(request);
        }
        [HttpPost]
        [Route("ListCollaboratorLender")]
        public async Task<Common.Constants.ResponseActionResult> ListCollaboratorLender(Queries.CollaboratorLender.ListCollaboratorLenderQuery request)
        {
            return await bus.Send(request);
        }
        [HttpPost]
        [Route("CannelCollaboratorLender")]
        public async Task<Common.Constants.ResponseActionResult> CannelCollaboratorLender(Commands.CollaboratorLender.CannelCollaboratorLenderCommand request)
        {
            return await bus.Send(request);
        }
        [HttpPost]
        [Route("ApprovedCollaboratorLender")]
        public async Task<Common.Constants.ResponseActionResult> ApprovedCollaboratorLender(Commands.CollaboratorLender.ApprovedCollaboratorLenderCommand request)
        {
            return await bus.Send(request);
        }
        [HttpPost]
        [Route("GetReportTransactionOfLender")]
        public async Task<Common.Constants.ResponseActionResult> GetReportTransactionOfLender(Queries.CollaboratorLender.GetReportTransactionOfLenderQuery request)
        {
            request.UserIDLogin = _userServices.GetHeaderUserID();
            return await bus.Send(request);
        }
    }
}
