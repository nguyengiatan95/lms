﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LenderController : ControllerBase
    {
        RestClients.IUserService _userServices;
        private readonly IMediator bus;
        public LenderController(IMediator bus, RestClients.IUserService userServices)
        {
            this.bus = bus;
            _userServices = userServices;
        }

        [HttpPost]
        [Route("CreateInfoLender")]
        public async Task<Common.Constants.ResponseActionResult> CreateInfoLender(Commands.CreateInfoLenderCommand request)
        {
            return await bus.Send(request);
        }
        [HttpPost]
        [Route("GetLenderInfosByIDs")]
        public async Task<Common.Constants.ResponseActionResult> GetLenderInfosByIDs(Queries.GetLenderInfosByIDsQuery request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("UpdateMoneyLender")]
        public async Task<Common.Constants.ResponseActionResult> UpdateMoneyLender(Commands.UpdateMoneyLenderCommand request)
        {
            return await bus.Send(request);
        }

        [HttpGet]
        [Route("GetLenderSearch")]
        public async Task<Common.Constants.ResponseActionResult> GetLenderByType(int type, string generalSearch = "")
        {
            return await bus.Send(new Queries.GetLenderByTypeQuery
            {
                IsHub = type,
                GeneralSearch = generalSearch
            });
        }

        [HttpPost]
        [Route("GetLenderList")]
        public async Task<Common.Constants.ResponseActionResult> GetLenderList(Queries.GetLenderListQuery request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("UpdateInfoLender")]
        public async Task<Common.Constants.ResponseActionResult> UpdateInfoLender(Commands.UpdateInfoLenderCommand request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("GetReportInsurance")]
        public async Task<Common.Constants.ResponseActionResult> GetReportInsurance(Queries.GetReportInsuranceQuery request)
        {
            return await bus.Send(request);
        }
        [HttpPost]
        [Route("CreateLenderDebt")]
        public async Task<Common.Constants.ResponseActionResult> CreateLenderDebt(LMS.Entites.Dtos.LenderService.CreateLenderDebtRequest request)
        {
            return await bus.Send(new Commands.CreateLenderDebtCommand { Model = request });
        }

        [HttpPost]
        [Route("CreateOrUpdateLenderFromAG")]
        public async Task<Common.Constants.ResponseActionResult> CreateOrUpdateLenderFromAG(Commands.CreateOrUpdateLenderFromAGCommand request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("GetLoanForLender")]
        public async Task<Common.Constants.ResponseActionResult> GetLoanForLender(Queries.GetLoanForLenderQuery request)
        {
            return await bus.Send(request);
        }

        [HttpGet]
        [Route("GetDetailMoneyLoan/{LenderID}")]
        public async Task<Common.Constants.ResponseActionResult> GetDetailMoneyLoan(long LenderID)
        {
            return await bus.Send(new Queries.GetDetailMoneyLoanQuery
            {
                LenderID = LenderID
            });
        }

        [HttpPost]
        [Route("CreateLenderUser")]
        public async Task<Common.Constants.ResponseActionResult> CreateLenderUser(Commands.CreateLenderUserCommand request)
        {
            if (request.CreateBy == 0)
            {
                request.CreateBy = _userServices.GetHeaderUserID();
            }
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("UpdateLenderUser")]
        public async Task<Common.Constants.ResponseActionResult> CreateLenderUser(Commands.UpdateLenderUserCommand request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("GetLenderUserConditions")]
        public async Task<Common.Constants.ResponseActionResult> GetLenderUserConditions(Queries.GetLenderUserConditionsQuery request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("AppCreateLenderUser")]
        public async Task<Common.Constants.ResponseActionResult> AppCreateLenderUser(Commands.AppCreateLenderUserCommand request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("CreateLenderDigitalSignature")]
        public async Task<Common.Constants.ResponseActionResult> CreateLenderDigitalSignature(Commands.CreateLenderDigitalSignatureCommand request)
        {
            if (request.UserID == 0)
            {
                request.UserID = _userServices.GetHeaderUserID();
            }
            return await bus.Send(request);
        }


        [HttpPost]
        [Route("UpdateLenderSpicesConfig")]
        public async Task<Common.Constants.ResponseActionResult> UpdateLenderSpicesConfig(Commands.UpdateLenderSpicesConfigCommand request)
        {
            if (request.CreateBy == 0)
            {
                request.CreateBy = _userServices.GetHeaderUserID();
            }
            return await bus.Send(request);
        }

        [HttpGet]
        [Route("GetLenderSpicesConfig/{LenderID}")]
        public async Task<Common.Constants.ResponseActionResult> GetLenderSpicesConfig(long LenderID)
        {
            return await bus.Send(new Queries.GetLenderSpicesConfigQuery()
            {
                LenderID = LenderID
            });
        }

        [HttpPost]
        [Route("VerifyUserLender")]
        public async Task<Common.Constants.ResponseActionResult> VerifyUserLender(Commands.VerifyUserLenderCommand request)
        {
            if (request.CreateBy == 0)
            {
                request.CreateBy = _userServices.GetHeaderUserID();
            }
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("CreateUpdateLendercontract")]
        public async Task<Common.Constants.ResponseActionResult> CreateUpdateLendercontract(Commands.CreateUpdateLenderContractCommand request)
        {
            if (request.CreateBy == 0)
            {
                request.CreateBy = _userServices.GetHeaderUserID();
            }
            return await bus.Send(request);
        }

        [HttpGet]
        [Route("GetLenderSignature/{LenderID}")]
        public async Task<Common.Constants.ResponseActionResult> GetLenderSignature(long LenderID)
        {
            return await bus.Send(new Queries.GetLenderContractByIDQuery()
            {
                LenderID = LenderID
            });
        }

        [HttpPost]
        [Route("ChangeStatusLenderUser")]
        public async Task<Common.Constants.ResponseActionResult> ChangeStatusUserLender(Commands.ChangeStatusLenderUserCommand request)
        {
            if (request.CreateBy == 0)
            {
                request.CreateBy = _userServices.GetHeaderUserID();
            }
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("ResetPassword")]
        public async Task<Common.Constants.ResponseActionResult> ResetPassword(Commands.ResetPasswordCommand request)
        {
            if (request.CreateBy == 0)
            {
                request.CreateBy = _userServices.GetHeaderUserID();
            }
            return await bus.Send(request);
        }
        [HttpPost]
        [Route("GetLenderSearchByLenderCode")]
        public async Task<Common.Constants.ResponseActionResult> GetLenderSearchByLenderCode(Queries.Lender.GetLenderSearchByLenderCodeQuery request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("GetInsuranceClaimInformation")]
        public async Task<Common.Constants.ResponseActionResult> GetInsuranceClaimInformation(Queries.Lender.GetInsuranceClaimInformationQuery request)
        {
            return await bus.Send(request);
        }

        [HttpGet]
        [Route("LenderStaff")]
        public async Task<Common.Constants.ResponseActionResult> LenderStaff()
        {
            return await bus.Send(new Queries.Lender.GetLenderStaffQuery());
        }

        [HttpPost]
        [Route("RunPaymentScheduleDepositMoney")]
        public async Task<Common.Constants.ResponseActionResult> RunPaymentScheduleDepositMoney()
        {
            return await bus.Send(new Commands.RunPaymentScheduleDepositMoneyCommand());
        }

        [HttpPost]
        [Route("GetLoanDeposit")]
        public async Task<Common.Constants.ResponseActionResult> GetLoanDeposit(Queries.GetLoanDepositQuery request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("UpdateStatusDeposit")]
        public async Task<Common.Constants.ResponseActionResult> UpdateStatusDeposit(List<Domain.Models.LenderStatusDeposit> request)
        {
            return await bus.Send(new Commands.UpdateStatusDepositCommand()
            {
                LstLoanStatus = request,
                UserID = _userServices.GetHeaderUserID(),
            });
        }

        [HttpPost]
        [Route("AGUpdateStatusDeposit")]
        public async Task<Common.Constants.ResponseActionResult> AGUpdateStatusDeposit(List<Domain.Models.TimaLenderStatusDeposit> request)
        {
            return await bus.Send(new Commands.AGUpdateStatusDepositCommand()
            {
                LstLoanStatus = request,
                UserID = _userServices.GetHeaderUserID(),
            });
        }

        [HttpPost]
        [Route("CommentDeposit")]
        public async Task<Common.Constants.ResponseActionResult> CommentDeposit(Commands.CommentDepositCommand request)
        {
            request.UserID = _userServices.GetHeaderUserID();
            return await bus.Send(request);
        }
        [HttpGet]
        [Route("GetHistoryDepositLender/{paymentScheduleDepositMoneyID}")]
        public async Task<Common.Constants.ResponseActionResult> GetHistoryDepositLender(long paymentScheduleDepositMoneyID)
        {
            return await bus.Send(new Queries.GetHistoryDepositQuery
            {
                PaymentScheduleDepositMoneyID = paymentScheduleDepositMoneyID
            });
        }
        [HttpGet]
        [Route("GetPrintComment/{loanID}")]
        public async Task<Common.Constants.ResponseActionResult> GetPrintComment(long loanID)
        {
            return await bus.Send(new Queries.Lender.PrintCommentQuery
            {
                LoanID = loanID
            });
        }
    }
}
