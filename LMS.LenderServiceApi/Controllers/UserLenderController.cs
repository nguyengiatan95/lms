﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserLenderController : Controller
    {
        private readonly IMediator _bus;
        public UserLenderController(IMediator bus)
        {
            _bus = bus;
        }
        [HttpGet]
        [Route("GetContractByUserLender/{UserLenderID}")]
        public async Task<Common.Constants.ResponseActionResult> GetContractByUserLender(long UserLenderID)
        {
            return await _bus.Send(new Queries.UserLender.GetContractByUserLenderQuery()
            {
                UserLenderID = UserLenderID
            });
        }


    }
}
