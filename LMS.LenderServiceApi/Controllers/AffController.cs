﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AffController : ControllerBase
    {
        readonly RestClients.IUserService _userServices;
        private readonly IMediator bus;
        public AffController(IMediator bus
            , RestClients.IUserService userServices
            )
        {
            this.bus = bus;
            _userServices = userServices;
        }


        [HttpPost]
        [Route("CreateOrUpdateAff")]
        public async Task<Common.Constants.ResponseActionResult> CreateOrUpdateAff(Commands.CreateOrUpdateAffCommand request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("GetAffList")]
        public async Task<Common.Constants.ResponseActionResult> GetAffList(Queries.Aff.GetAffConditionsQuery request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("DeleteAff")]
        public async Task<Common.Constants.ResponseActionResult> DeleteAff(Commands.DeleteAffCommand request)
        {
            return await bus.Send(request);
        }

        [HttpGet]
        [Route("GetMaxCode")]
        public async Task<Common.Constants.ResponseActionResult> GetMaxCode()
        {
            return await bus.Send(new Queries.Aff.GetMaxCodeQuery());
        }



        [HttpPost]
        [Route("GetLstLenderInfoByConditions")]
        public async Task<Common.Constants.ResponseActionResult> GetLstLenderInfoByConditions(Queries.Aff.GetLstLenderInfoByConditionsQuery request)
        {
            request.UserIDLogin = _userServices.GetHeaderUserID();
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("GetLstSelectAff")]
        public async Task<Common.Constants.ResponseActionResult> GetLstSelectAff(Queries.Aff.GetLstSelectAffQuery request)
        {
            request.UserIDLogin = _userServices.GetHeaderUserID();
            return await bus.Send(request);
        }

        [HttpGet]
        [Route("GetLinkAff")]
        public async Task<Common.Constants.ResponseActionResult> GetLinkAff()
        {
            long userID = _userServices.GetHeaderUserID();
            return await bus.Send(new Queries.Aff.GetLinkAffQuery()
            {
                UserID = userID
            });
        }
    }
}
