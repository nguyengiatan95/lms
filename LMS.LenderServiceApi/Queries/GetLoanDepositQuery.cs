﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries
{
    public class GetLoanDepositQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string ContractCode { get; set; }
        public int Status { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string CustomerName { get; set; }
        public int LenderID { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class GetLoanDepositQueryHandler : IRequestHandler<GetLoanDepositQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentScheduleDepositMoney> _paymentScheduleDepositMoneyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBank> _bankTab;
        ILogger<GetLoanDepositQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetLoanDepositQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentScheduleDepositMoney> paymentScheduleDepositMoneyTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBank> bankTab,

        ILogger<GetLoanDepositQueryHandler> logger)
        {
            _paymentScheduleDepositMoneyTab = paymentScheduleDepositMoneyTab;
            _lenderTab = lenderTab;
            _bankTab = bankTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetLoanDepositQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            List<Domain.Models.PaymentScheduleDepositMoneyView> paymentScheduleDepositMoneyViews = new List<Domain.Models.PaymentScheduleDepositMoneyView>();
            try
            {
                if (!string.IsNullOrWhiteSpace(request.FromDate))
                {
                    var dtfromDate = DateTime.ParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, null).AddDays(-30);

                    _paymentScheduleDepositMoneyTab.WhereClause(x => x.EndDaySchedule >= dtfromDate);
                }
                if (!string.IsNullOrWhiteSpace(request.ToDate))
                {
                    var dtoDate = DateTime.ParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, null).AddDays(-29);
                    _paymentScheduleDepositMoneyTab.WhereClause(x => x.EndDaySchedule <= dtoDate);
                }
                if (!string.IsNullOrWhiteSpace(request.CustomerName))
                {
                    request.CustomerName = request.CustomerName.Trim();
                    _paymentScheduleDepositMoneyTab.WhereClause(x => x.CustomerName.Contains(request.CustomerName));
                }
                if (!string.IsNullOrWhiteSpace(request.ContractCode))
                {
                    request.ContractCode = request.ContractCode.Trim();
                    _paymentScheduleDepositMoneyTab.WhereClause(x => x.ContractCode.Contains(request.ContractCode));
                }
                if (request.LenderID != (int)StatusCommon.SearchAll)
                {
                    _paymentScheduleDepositMoneyTab.WhereClause(x => x.LenderID == request.LenderID);
                }
                if (request.Status != (int)StatusCommon.SearchAll)
                {
                    _paymentScheduleDepositMoneyTab.WhereClause(x => x.Status == request.Status);
                }
                var paymentSchedules = await _paymentScheduleDepositMoneyTab.QueryAsync();
                var totalCount = paymentSchedules.Count();
                paymentSchedules = paymentSchedules.Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                if (paymentSchedules.Any())
                {
                    var lenderIds = paymentSchedules.Select(x => x.LenderID).Distinct().ToList();
                    var dicLenders = (await _lenderTab.WhereClause(x => x.IsDeposit == (int)Lender_DepositType.Deposit && lenderIds.Contains(x.LenderID)).QueryAsync()).ToDictionary(x => x.LenderID, x => x);
                    var bankIds = dicLenders.Values.Select(x => x.BankID).Distinct().ToList();
                    var dicBank = (await _bankTab.WhereClause(x => bankIds.Contains(x.Id)).QueryAsync()).ToDictionary(x => x.Id, x => x);
                    foreach (var item in paymentSchedules)
                    {
                        var lender = dicLenders.ContainsKey(item.LenderID) ? dicLenders[item.LenderID] : new Domain.Tables.TblLender();
                        var data = _common.ConvertParentToChild<Domain.Models.PaymentScheduleDepositMoneyView, Domain.Tables.TblPaymentScheduleDepositMoney>(item);
                        data.StatusName = ((PaymentScheduleDepositMoney_Status)item.Status).GetDescription();
                        data.TotalCount = totalCount;
                        data.LenderName = lender.Represent;
                        data.AccountBanking = lender.AccountBanking;
                        data.BankName = dicBank.ContainsKey((int)lender.BankID) ? dicBank[(int)lender.BankID].Code + " - " + dicBank[(int)lender.BankID].NameBank : "";
                        paymentScheduleDepositMoneyViews.Add(data);
                    }
                }
                response.SetSucces();
                response.Total = totalCount;
                response.Data = paymentScheduleDepositMoneyViews;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLoanDepositQueryHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
