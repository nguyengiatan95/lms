﻿using LMS.Common.Constants;
using System.Collections.Generic;

namespace LMS.LenderServiceApi.Queries
{
    public interface IExcelExportDataHelper
    {
        int TypeReport { get; }
        ResponseActionResult GetLstDataExcelFromJson(string extraData);
    }
    public class ExcelExportBase
    {
        protected LMS.Common.Helper.Utils _common { get; set; }
        public ExcelExportBase()
        {
            _common = new LMS.Common.Helper.Utils();
        }
    }

    public class ExcelExportLenderDeposit : ExcelExportBase, IExcelExportDataHelper
    {
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.Lender_Deposit;

        public ResponseActionResult GetLstDataExcelFromJson(string extraData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.Data = _common.ConvertJSonToObjectV2<List<LMS.Entites.Dtos.LenderService.Excel.LenderDepositView>>(extraData);
                response.SetSucces();
            }
            catch
            {
            }
            return response;
        }
    }

    public class ExcelExportBM01 : ExcelExportBase, IExcelExportDataHelper
    {
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.Lender_BM01;

        public ResponseActionResult GetLstDataExcelFromJson(string extraData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.Data = _common.ConvertJSonToObjectV2<LMS.Entites.Dtos.LenderService.Excel.BM01Excel>(extraData);
                response.SetSucces();
            }
            catch
            {
            }
            return response;
        }
    }

}
