﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace LMS.LenderServiceApi.Queries.CollaboratorLender
{
    public class ListCollaboratorLenderQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string KeySearch { get; set; }
        public int Status { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class ListCollaboratorLenderQueryHandler : IRequestHandler<ListCollaboratorLenderQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCollaboratorLender> _collaboratorLenderTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> _affTab;
        readonly ILogger<ListCollaboratorLenderQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public ListCollaboratorLenderQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCollaboratorLender> collaboratorLenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> affTab,
        ILogger<ListCollaboratorLenderQueryHandler> logger)
        {
            _collaboratorLenderTab = collaboratorLenderTab;
            _affTab = affTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(ListCollaboratorLenderQuery request, CancellationToken cancellationToken)
        {
            Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            var resultData = new List<Domain.CollaboratorLender.CollaboratorLenderDetails>();
            try
            {
                if (!string.IsNullOrEmpty(request.KeySearch))
                    _collaboratorLenderTab.WhereClause(x => x.PhoneNumber == request.KeySearch);
                if (request.Status != (int)StatusCommon.SearchAll)
                    _collaboratorLenderTab.WhereClause(x => x.Status == request.Status);

                var lstData = (await _collaboratorLenderTab.QueryAsync());
                if (lstData.Any())
                {
                    response.Total = lstData.Count();
                    lstData = lstData.OrderByDescending(x => x.CreateDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                    var lstCodeCollaborator = new List<string>();
                    foreach (var item in lstData)
                    {
                        lstCodeCollaborator.Add(item.CodeCollaborator);
                    }
                    var dictAff = (await _affTab.SelectColumns(x => x.Name, x => x.Code).WhereClause(x => lstCodeCollaborator.Contains(x.Code)).QueryAsync()).ToDictionary(x => x.Code, x => x);
                    foreach (var item in lstData)
                    {
                        string affName = string.Empty;
                        if (!string.IsNullOrEmpty(item.CodeCollaborator))
                            affName = dictAff.GetValueOrDefault(item.CodeCollaborator)?.Name;

                        resultData.Add(new Domain.CollaboratorLender.CollaboratorLenderDetails
                        {
                            CollaboratorLenderID = item.CollaboratorLenderID,
                            FullName = item.FullName,
                            PhoneNumber = item.PhoneNumber,
                            CreateDate = item.CreateDate,
                            ModifyByUser = item.ModifyByUser,
                            CodeCollaborator = item.CodeCollaborator,
                            Note = item.Note,
                            Status = item.Status,
                            ModifyDate = item.ModifyDate,
                            AffName = affName
                        });
                    }
                    response.Data = resultData;
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ListCollaboratorLenderQueryHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
