﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries.CollaboratorLender
{
    public class GetReportTransactionOfLenderQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string GeneralSearch { get; set; }
        public int Status { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public long AffID { get; set; }
        public long IsCollaborator { get; set; }
        public long UserIDLogin { get; set; }
    }
    public class GetReportTransactionOfLenderQueryHandler : IRequestHandler<GetReportTransactionOfLenderQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> _affTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> _userLenderTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly ILogger<GetReportTransactionOfLenderQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public GetReportTransactionOfLenderQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> affTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> userLenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            ILogger<GetReportTransactionOfLenderQueryHandler> logger)
        {
            _affTab = affTab;
            _userLenderTab = userLenderTab;
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _invoiceTab = invoiceTab;
            _loanTab = loanTab;
        }

        public async Task<ResponseActionResult> Handle(GetReportTransactionOfLenderQuery request, CancellationToken cancellationToken)
        {
            Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            response.SetSucces();
            List<Domain.Models.AffView> affViews = new List<Domain.Models.AffView>();
            List<Domain.CollaboratorLender.ReportTransactionOfLenderItem> lstTransactionResult = new List<Domain.CollaboratorLender.ReportTransactionOfLenderItem>();
            Dictionary<long, Domain.Tables.TblUserLender> dictUserLenderInfos = new Dictionary<long, Domain.Tables.TblUserLender>();
            try
            {
                long affID = (long)LMS.Common.Constants.StatusCommon.SearchAll;
                var currentDate = DateTime.Now;
                var fromDate = new DateTime(currentDate.Year, currentDate.Month, 1);
                var toDate = fromDate.AddMonths(1);
                if (!string.IsNullOrWhiteSpace(request.FromDate))
                {
                    fromDate = DateTime.ParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, null).AddSeconds(-1);
                }
                if (!string.IsNullOrWhiteSpace(request.ToDate))
                {
                    toDate = DateTime.ParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, null).AddDays(1);
                }
                if (request.IsCollaborator > 0 && request.UserIDLogin != TimaSettingConstant.UserIDAdmin)
                {
                    // lấy thông tin affID theo user login
                    affID = (await _affTab.SetGetTop(1).WhereClause(x => x.UserID == request.UserIDLogin).QueryAsync()).FirstOrDefault()?.AffID ?? long.MaxValue;
                    _affTab.WhereClause(x => x.AffID == affID);
                    _lenderTab.WhereClause(x => x.AffID == affID);
                }
                else if (request.AffID > 0)
                {
                    affID = request.AffID;
                    _affTab.WhereClause(x => x.AffID == affID);
                    _lenderTab.WhereClause(x => x.AffID == affID);
                }
                else
                {
                    _lenderTab.WhereClause(x => x.AffID > 0);
                }
                _userLenderTab.WhereClause(x => x.CreateDate > fromDate && x.CreateDate < toDate);
                _loanTab.WhereClause(x => x.FromDate > fromDate && x.FromDate < toDate);
                _invoiceTab.WhereClause(x => x.TransactionDate > fromDate && x.TransactionDate < toDate);


                if (request.Status != (int)StatusCommon.SearchAll)
                {
                    switch ((LenderUser_Verified)request.Status)
                    {
                        case LenderUser_Verified.Active:
                            _lenderTab.WhereClause(x => x.ContractDate != null);
                            break;
                        case LenderUser_Verified.UnActive:
                            _lenderTab.WhereClause(x => x.ContractDate == null);
                            break;
                        default: // xóa
                            _lenderTab.WhereClause(x => x.Status == (int)LenderUser_Status.UnActive);
                            break;
                    }
                }
                if (!string.IsNullOrWhiteSpace(request.GeneralSearch))
                {
                    if (request.GeneralSearch.Length == TimaSettingConstant.MaxLengthPhone && Regex.Match(request.GeneralSearch, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success)
                    {
                        _userLenderTab.WhereClause(x => x.Phone == request.GeneralSearch);
                    }
                    else if (Regex.Match(request.GeneralSearch, TimaSettingConstant.PatternNumberCard, RegexOptions.IgnoreCase).Success)
                    {
                        _userLenderTab.WhereClause(x => x.NumberCard == request.GeneralSearch);
                    }
                    else
                    {
                        _userLenderTab.WhereClause(x => x.FullName.Contains(request.GeneralSearch));
                    }
                }
                var lstLenderContractDataTask = _lenderTab.SelectColumns(x => x.OwnerUserID, x => x.CreateDate, x => x.ContractType, x => x.ContractDate, x => x.AffID, x => x.LenderID)
                                                      .QueryAsync();

                var lstUserLenderDataTask = _userLenderTab.QueryAsync();
                var lstAffDataTask = _affTab.QueryAsync();
                await Task.WhenAll(lstUserLenderDataTask, lstLenderContractDataTask, lstAffDataTask);

                long TotalMoneyDisbursement = 0;
                long TotalMoneyTopup = 0;
                long TotalCommissionMoney = 0;
                var lstLenderContractInfos = lstLenderContractDataTask.Result.ToList();
                var lstLenderContractID = lstLenderContractInfos.Select(x => x.LenderID);

                // lấy danh sách đơn vay
                // lấy danh sách phiếu nạp tiền
                var lstLoanInfosTask = _loanTab.WhereClause(x => lstLenderContractID.Contains(x.LenderID)).QueryAsync();
                var lstInvoiceInfosTask = _invoiceTab.WhereClause(x => lstLenderContractID.Contains((long)x.SourceID) && x.InvoiceSubType == (int)GroupInvoiceType.ReceiptLenderCapital).QueryAsync();

                await Task.WhenAll(lstLoanInfosTask, lstInvoiceInfosTask);

                dictUserLenderInfos = lstUserLenderDataTask.Result.GroupBy(x => (long)x.UserID).ToDictionary(x => x.Key, x => x.FirstOrDefault());
                var dictLenderContractInfos = lstLenderContractInfos.ToDictionary(x => x.LenderID, x => x);
                var dictCodeAffInfos = new Dictionary<string, Domain.Tables.TblAff>();
                var dictUserIDAffInfos = new Dictionary<long, Domain.Tables.TblAff>();
                foreach (var item in lstAffDataTask.Result)
                {
                    if (!string.IsNullOrEmpty(item.Code) && !dictCodeAffInfos.ContainsKey(item.Code))
                    {
                        dictCodeAffInfos.Add(item.Code, item);
                    }
                    if (item.AffID > 0 && !dictUserIDAffInfos.ContainsKey(item.AffID))
                    {
                        dictUserIDAffInfos.Add(item.AffID, item);
                    }
                }
                var dictPhoneAffUserLenderContractItemInfos = new Dictionary<string, Domain.CollaboratorLender.ReportTransactionOfLenderItem>();
                var lstLoanInfos = lstLoanInfosTask.Result;
                var lstInvoiceInfos = lstInvoiceInfosTask.Result;
                if (lstLoanInfos != null && lstLoanInfos.Any())
                {
                    foreach (var item in lstLoanInfos)
                    {
                        var lenderContractInfo = dictLenderContractInfos.GetValueOrDefault(item.LenderID);
                        if (lenderContractInfo == null || lenderContractInfo.LenderID < 1)
                        {
                            continue;
                        }
                        var userLenderInfo = dictUserLenderInfos.GetValueOrDefault(lenderContractInfo.OwnerUserID);
                        if (userLenderInfo == null || userLenderInfo.UserID < 1)
                        {
                            continue;
                        }
                        var affInfo = dictUserIDAffInfos.GetValueOrDefault(lenderContractInfo.AffID.GetValueOrDefault());
                        // 09-04-2022: phải có cộng tác viên mới lấy
                        if (affInfo == null || affInfo.AffID < 1)
                        {
                            continue;
                        }
                        switch ((Lender_ContractType)lenderContractInfo.ContractType)
                        {
                            case Lender_ContractType.DTNormal:
                            case Lender_ContractType.NANormal:
                                // todo: xem lại ngày hết hiệu lực hoa hồng theo từng hợp đồng của NDT -> chưa chốt
                                TotalMoneyDisbursement += item.TotalMoneyDisbursement;
                                lstTransactionResult.Add(new Domain.CollaboratorLender.ReportTransactionOfLenderItem
                                {
                                    LenderFullName = userLenderInfo.FullName,
                                    LenderPhone = userLenderInfo.Phone,
                                    AffCode = affInfo?.Code,
                                    AffFullName = affInfo?.Name,
                                    ContractType = lenderContractInfo.ContractType,
                                    ContractTypeName = ((Lender_ContractType)lenderContractInfo.ContractType).GetDescription(),
                                    CommissionRate = 1,
                                    MoneyDisbursement = item.TotalMoneyDisbursement,
                                    StrTransactionDate = item.FromDate.ToString(TimaSettingConstant.DateTimeDayMonthYear),
                                    TransactionDate = item.FromDate
                                });
                                break;
                        }
                    }
                }
                if (lstInvoiceInfos != null && lstInvoiceInfos.Any())
                {
                    foreach (var item in lstInvoiceInfos)
                    {
                        var lenderContractInfo = dictLenderContractInfos.GetValueOrDefault(item.SourceID.GetValueOrDefault());
                        if (lenderContractInfo == null || lenderContractInfo.LenderID < 1)
                        {
                            continue;
                        }
                        var userLenderInfo = dictUserLenderInfos.GetValueOrDefault(lenderContractInfo.OwnerUserID);
                        if (userLenderInfo == null || userLenderInfo.UserID < 1)
                        {
                            continue;
                        }
                        var affInfo = dictUserIDAffInfos.GetValueOrDefault(lenderContractInfo.AffID.GetValueOrDefault());
                        // 09-04-2022: phải có cộng tác viên mới lấy
                        if (affInfo == null || affInfo.AffID < 1)
                        {
                            continue;
                        }
                        switch ((Lender_ContractType)lenderContractInfo.ContractType)
                        {
                            case Lender_ContractType.DTTimaNew:
                            case Lender_ContractType.NAOfTimaNew:
                            case Lender_ContractType.GTimaDT:                                
                            case Lender_ContractType.GTimaNA:                                
                                // dc tinh nhiều lần
                                TotalMoneyTopup += item.TotalMoney.GetValueOrDefault();
                                lstTransactionResult.Add(new Domain.CollaboratorLender.ReportTransactionOfLenderItem
                                {
                                    LenderFullName = userLenderInfo.FullName,
                                    LenderPhone = userLenderInfo.Phone,
                                    AffCode = affInfo?.Code,
                                    AffFullName = affInfo?.Name,
                                    ContractType = lenderContractInfo.ContractType,
                                    ContractTypeName = ((Lender_ContractType)lenderContractInfo.ContractType).GetDescription(),
                                    CommissionRate = 1,
                                    MoneyTopup = item.TotalMoney.GetValueOrDefault(),
                                    StrTransactionDate = (item.TransactionDate ?? item.CreateDate).Value.ToString(TimaSettingConstant.DateTimeDayMonthYear),
                                    TransactionDate = (item.TransactionDate ?? item.CreateDate).Value
                                });
                                break;
                        }
                    }

                }
                response.Total = lstTransactionResult.Count();
                lstTransactionResult = lstTransactionResult.OrderByDescending(x => x.TransactionDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();

                if (lstTransactionResult.Count > 0)
                {
                    lstTransactionResult[0].TotalCommissionMoney = TotalCommissionMoney;
                    lstTransactionResult[0].TotalMoneyDisbursement = TotalMoneyDisbursement;
                    lstTransactionResult[0].TotalMoneyTopup = TotalMoneyTopup;
                }
                response.Data = lstTransactionResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetReportTransactionOfLenderQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
