﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries
{
    public class GetDataBM01Query : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string ContractCode { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string CustomerName { get; set; }
        public int LenderID { get; set; }
    }
    public class GetDataBM01QueryHandler : IRequestHandler<GetDataBM01Query, LMS.Common.Constants.ResponseActionResult>
    {
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentScheduleDepositMoney> _paymentScheduleDepositMoneyTab;
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblBank> _bankTab;

        private readonly ILogger<GetLoanDepositQueryHandler> _logger;
        private readonly LMS.Common.Helper.Utils _common;
        public GetDataBM01QueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentScheduleDepositMoney> paymentScheduleDepositMoneyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBank> bankTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
        ILogger<GetLoanDepositQueryHandler> logger)
        {
            _paymentScheduleDepositMoneyTab = paymentScheduleDepositMoneyTab;
            _lenderTab = lenderTab;
            _bankTab = bankTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetDataBM01Query request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            Entites.Dtos.LenderService.Excel.BM01Excel bM01Excel = new Entites.Dtos.LenderService.Excel.BM01Excel();
            try
            {
                if (!string.IsNullOrWhiteSpace(request.FromDate))
                {
                    var dtfromDate = DateTime.ParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                    _paymentScheduleDepositMoneyTab.WhereClause(x => x.CreateDate >= dtfromDate);
                }
                if (!string.IsNullOrWhiteSpace(request.ToDate))
                {
                    var dtoDate = DateTime.ParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, null).AddDays(1);
                    _paymentScheduleDepositMoneyTab.WhereClause(x => x.CreateDate >= dtoDate);
                }
                if (!string.IsNullOrWhiteSpace(request.CustomerName))
                {
                    request.CustomerName = request.CustomerName.Trim();
                    _paymentScheduleDepositMoneyTab.WhereClause(x => x.CustomerName.Contains(request.CustomerName));
                }
                if (!string.IsNullOrWhiteSpace(request.ContractCode))
                {
                    request.ContractCode = request.ContractCode.Trim();
                    _paymentScheduleDepositMoneyTab.WhereClause(x => x.ContractCode.Contains(request.ContractCode));
                }
                if (request.LenderID != (int)StatusCommon.SearchAll)
                {
                    _paymentScheduleDepositMoneyTab.WhereClause(x => x.LenderID == request.LenderID);
                }
                _paymentScheduleDepositMoneyTab.WhereClause(x => x.Status == (int)PaymentScheduleDepositMoney_Status.WaittingDeposit);
                var paymentSchedules = await _paymentScheduleDepositMoneyTab.QueryAsync();
                var lenderIds = paymentSchedules.Select(x => x.LenderID);
                var lenders = await _lenderTab.WhereClause(x => lenderIds.Contains(x.LenderID)).QueryAsync();
                bM01Excel.LstDeposit = _common.ConvertParentToChild<List<Entites.Dtos.LenderService.Excel.DepositData>, List<Domain.Tables.TblPaymentScheduleDepositMoney>>(paymentSchedules.ToList());
                bM01Excel.LenderInfos = new List<Entites.Dtos.LenderService.Excel.LenderInfo>();
                var bankIds = lenders.Select(x => x.BankID).ToList();
                var dicBank = (await _bankTab.WhereClause(x => bankIds.Contains(x.Id)).QueryAsync()).ToDictionary(x => x.Id, x => x);
                foreach (var item in lenders)
                {
                    bM01Excel.LenderInfos.Add(new Entites.Dtos.LenderService.Excel.LenderInfo()
                    {
                        FullName = item.FullName,
                        LenderCode = item.LenderCode,
                        NumberBankCard = item.AccountBanking,
                        LenderID = item.LenderID,
                        Represent = item.Represent,
                        TotalMoney = bM01Excel.LstDeposit.Where(x => x.LenderID == item.LenderID).Sum(x => x.TotalMoneyDepositAfterTax),
                        BankCode = dicBank.ContainsKey((int)item.BankID) ? dicBank[(int)item.BankID].Code + " - " + dicBank[(int)item.BankID].NameBank : ""
                    });
                }
                response.SetSucces();
                response.Data = bM01Excel;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetDataBM01QueryHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
