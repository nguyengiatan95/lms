﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries
{
    public class GetLenderUserConditionsQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string GeneralSearch { get; set; }
        public int Status { get; set; }
        public int IsVerified { get; set; }
        public string CreateDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class GetLenderUserConditionsQueryHandler : IRequestHandler<GetLenderUserConditionsQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> _userlenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<GetLenderUserConditionsQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetLenderUserConditionsQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> userlenderTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            ILogger<GetLenderUserConditionsQueryHandler> logger)
        {
            _userlenderTab = userlenderTab;
            _userTab = userTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetLenderUserConditionsQuery request, CancellationToken cancellationToken)
        {
            Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            try
            {
                List<Domain.Models.LenderUserView> lenderUserViews = new List<Domain.Models.LenderUserView>();
                if (!string.IsNullOrWhiteSpace(request.GeneralSearch))
                {
                    //họ tên, số dt, số cmt của ndt
                    if (request.GeneralSearch.Length == TimaSettingConstant.MaxLengthPhone && Regex.Match(request.GeneralSearch, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success)
                    {
                        _userlenderTab.WhereClause(x => x.Phone == request.GeneralSearch);
                    }
                    else if (Regex.Match(request.GeneralSearch, TimaSettingConstant.PatternNumberCard, RegexOptions.IgnoreCase).Success)
                    {
                        _userlenderTab.WhereClause(x => x.NumberCard == request.GeneralSearch);
                    }
                    else
                    {
                        _userlenderTab.WhereClause(x => x.FullName.Contains(request.GeneralSearch) || x.Phone == request.GeneralSearch || x.NumberCard == request.GeneralSearch);
                    }
                }
                if (request.Status != (int)StatusCommon.SearchAll)
                {
                    _userlenderTab.WhereClause(x => x.Status == request.Status);
                }
                if (request.IsVerified != (int)StatusCommon.SearchAll)
                {
                    _userlenderTab.WhereClause(x => x.IsVerified == request.IsVerified);
                }
                if (!string.IsNullOrWhiteSpace(request.CreateDate))
                {
                    var dtCreateDate = DateTime.ParseExact(request.CreateDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                    _userlenderTab.WhereClause(x => x.CreateDate >= dtCreateDate);
                }
                var loanLenders = await _userlenderTab.QueryAsync();
                int totalRow = loanLenders.Count();
                var lstResult = loanLenders.OrderByDescending(x => x.CreateDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                if (lstResult.Any())
                {
                    var userIds = lstResult.Select(x => x.UserID).ToList();
                    var users = (await _userTab.SelectColumns(x => x.UserName, x => x.UserID).WhereClause(x => userIds.Contains(x.UserID)).QueryAsync()).ToDictionary(x => x.UserID, x => x);
                    foreach (var item in lstResult)
                    {
                        var lenderUserView = _common.ConvertParentToChild<Domain.Models.LenderUserView, Domain.Tables.TblUserLender>(item);
                        lenderUserView.UserName = users.GetValueOrDefault((int)item.UserID)?.UserName;
                        lenderUserViews.Add(lenderUserView);
                    }
                }
                response.SetSucces();
                response.Total = totalRow;
                response.Data = lenderUserViews;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLenderUserConditionsQueryHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
