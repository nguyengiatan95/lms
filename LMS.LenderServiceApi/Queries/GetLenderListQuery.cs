﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries
{
    public class GetLenderListQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string GeneralSearch { get; set; }
        public int Status { get; set; }
        public int IsVerified { get; set; }
        public int IsHub { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }

    public class GetLenderListQueryQueryHandler : IRequestHandler<GetLenderListQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        ILogger<GetLenderListQueryQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetLenderListQueryQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            ILogger<GetLenderListQueryQueryHandler> logger)
        {
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> Handle(GetLenderListQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
                try
                {
                    _lenderTab.WhereClause(x => x.IsHub == request.IsHub);
                    if (!string.IsNullOrWhiteSpace(request.GeneralSearch)) _lenderTab.WhereClause(x => x.FullName.Contains(request.GeneralSearch));
                    if (request.Status != (int)LMS.Common.Constants.StatusCommon.SearchAll) _lenderTab.WhereClause(x => x.Status == request.Status);
                    if (request.IsVerified != (int)LMS.Common.Constants.StatusCommon.SearchAll) _lenderTab.WhereClause(x => x.IsVerified == request.IsVerified);
                    var lstLender = _lenderTab.Query();
                    response.SetSucces();
                    //ResponseForDataTable dataTable = new ResponseForDataTable
                    //{
                    //    RecordsTotal = 0,
                    //    data = lstLender,
                    //    RecordsFiltered = 0,
                    //    Draw = 1
                    //};
                    //if (lstLender != null && lstLender.Any())
                    //{
                    lstLender = lstLender.OrderByDescending(x => x.CreateDate);
                    response.Total = lstLender.Count();

                    //dataTable.RecordsTotal = lstLender.Count();
                    lstLender = lstLender.Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                    //dataTable.data = lstLender;
                    response.Data = lstLender;
                    //}
                }
                catch (Exception ex)
                {
                    _logger.LogError($"GetLenderListQuery_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                }
                return response;
            });
        }
    }
}
