﻿using LMS.Entites.Dtos.LenderService;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries.Lender
{
    public class GetInsuranceClaimInformationQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
    }
    public class GetInsuranceClaimInformationQueryHandler : IRequestHandler<GetInsuranceClaimInformationQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLendingCertificateInformation> _lendingCertificateInformationTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        readonly ILogger<GetInsuranceClaimInformationQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public GetInsuranceClaimInformationQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLendingCertificateInformation> lendingCertificateInformationTab,
          LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
          LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
          ILogger<GetInsuranceClaimInformationQueryHandler> logger)
        {
            _lendingCertificateInformationTab = lendingCertificateInformationTab;
            _loanTab = loanTab;
            _paymentScheduleTab = paymentScheduleTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> Handle(GetInsuranceClaimInformationQuery request, CancellationToken cancellationToken)
        {
            Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            LenderInsuranceInformationModel lenderSearches;
            try
            {
                if (request.LoanID <= 0)
                {
                    response.Message = Common.Constants.MessageConstant.LoanNotExist;
                    return response;
                }
                // Lấy bảng insurance
                var taskLender = _lendingCertificateInformationTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync();
                // lấy bảng loan 
                // TotalMoneyDisbursement dư nợ giải ngân ban đầu, TotalMoneyCurrent  gốc còn lại, fromdate, todate
                var taskLoan = _loanTab.SelectColumns(x => x.TotalMoneyDisbursement, x => x.TotalMoneyCurrent, x => x.FromDate, x => x.ToDate).WhereClause(x => x.LoanID == request.LoanID).QueryAsync();
                // lấy bảng payment
                var taskPayment = _paymentScheduleTab.SelectColumns(x => x.MoneyInterest, x => x.PayMoneyInterest).WhereClause(x => x.LoanID == request.LoanID && x.Status == 1 && x.IsComplete != 1).QueryAsync();
                // map dữ liệu
                await Task.WhenAll(taskLender, taskLoan, taskPayment);
                var lender = taskLender.Result.FirstOrDefault();
                var loan = taskLoan.Result.FirstOrDefault();
                var paymentSchedules = taskPayment.Result;
                lenderSearches = new LenderInsuranceInformationModel()
                {
                    CustomerFullname = lender.CustomerFullname,
                    CustomerIDCard = lender.CustomerIdCard,
                    CustomerAddress = lender.CustomerAddress,
                    InsuranceNumber = lender.NumberInsurance,
                    CustomerPhone = lender.CustomerPhone,
                    CustomerEmail = lender.CustomerEmail,
                    InvestorFullname = lender.InvestorFullname,
                    InvestorDateOfBirth = lender.InvestorDateOfBirth,
                    InvestorIDCard = lender.InvestorIdCard,
                    InvestorAddress = lender.InvestorAddress,
                    InvestorPhone = lender.InvestorPhone,
                    InvestorEmail = lender.InvestorEmail,
                    TotalMoneyCurrent = loan.TotalMoneyCurrent,
                    TotalMoneyDisburement = loan.TotalMoneyDisbursement,
                    ToDate = loan.ToDate,
                    FromDate = loan.FromDate,
                    InterestMoney = paymentSchedules.Sum(x => x.MoneyInterest - x.PayMoneyInterest)
                };

                response.SetSucces();
                response.Data = lenderSearches;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetInsuranceClaimInformationQueryHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
