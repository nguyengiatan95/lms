﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries.Lender
{
    public class PrintCommentQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
    }
    public class PrintCommentQueryHandler : IRequestHandler<PrintCommentQuery, LMS.Common.Constants.ResponseActionResult>
    {
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLendingCertificateInformation> _lenderCertificateInformationTab;
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> _commentTab;
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentTab;




        private readonly RestClients.ILOSService _losService;
        ILogger<PrintCommentQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public PrintCommentQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
                                        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
                                        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLendingCertificateInformation> lenderCertificateInformationTab,
                                        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> commentTab,
                                        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentTab,
                                        RestClients.ILOSService lOSService,
        ILogger<PrintCommentQueryHandler> logger)
        {
            _lenderTab = lenderTab;
            _loanTab = loanTab;
            _lenderCertificateInformationTab = lenderCertificateInformationTab;
            _paymentTab = paymentTab;
            _logger = logger;
            _commentTab = commentTab;
            _losService = lOSService;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(PrintCommentQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                if (request.LoanID <= 0)
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                /// lấy loan
                var loan = (await _loanTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                if (loan.LoanID == 0)
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }

                var losData = await _losService.GetLoanCreditDetail((long)loan.LoanCreditIDOfPartner);

                var taskInsurance = _lenderCertificateInformationTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync();
                var taskComment = _commentTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync();
                var taskPayment = _paymentTab.WhereClause(x => x.LoanID == request.LoanID && x.Status == (int)PaymentSchedule_Status.Use && x.IsComplete == (int)PaymentSchedule_IsComplete.Waiting).QueryAsync();
                await Task.WhenAll(taskInsurance, taskComment, taskPayment);
                var insurances = taskInsurance.Result;
                var comments = taskComment.Result;
                long totalInterest = taskPayment.Result.Sum(x => x.MoneyInterest - x.PayMoneyInterest);
                var insurance = insurances.Any() ? insurances.First() : new Domain.Tables.TblLendingCertificateInformation();
                var relation = losData.Relationship != null && losData.Relationship.Any() ? losData.Relationship.FirstOrDefault() : new Entites.Dtos.LOSServices.CustomerRelationshipInfo();
                Domain.Models.PrintCommentContract print = new Domain.Models.PrintCommentContract()
                {
                    Address = losData.Address,
                    AddressHouseHold = losData.HouseOldAddress,
                    Job = losData.JobTitle,
                    AddressJob = losData.CompanyAddress,
                    CompanyPhone = losData.CompanyPhone,
                    ContractCode = losData.ContactCodeLMS,
                    CustomerName = losData.FullName,
                    CustomerPhone = insurance.CustomerPhone,
                    LenderCode = losData.LenderCode,
                    LoanTime = losData.LoanTime,
                    FromDate = loan.FromDate.ToString("dd/MM/yyyy"),
                    LastdateOfPay = loan.LastDateOfPay,
                    TotalMoney = loan.TotalMoneyDisbursement,
                    TotalMoneyCurrent = loan.TotalMoneyCurrent,
                    LenderPhone = insurance.InvestorPhone,
                    BirthDay = insurance.CustomerDateOfBirth,
                    NumberCard = losData.CardNumberShareholder,
                    CountDay = (long)(DateTime.Now - loan.NextDate).Value.TotalDays,
                    InterestMoney = totalInterest,
                    CommentViews = _common.ConvertParentToChild<List<Domain.Models.CommentView>, List<Domain.Tables.TblCommentDebtPrompted>>(comments.ToList()),
                    ColleagueName = losData.FullNameColleague,
                    ColleaguePhone = losData.PhoneColleague,
                    CompanyAddress = losData.CompanyAddress,
                    CompanyName = losData.CompanyName,
                    FamilyPhone = relation.Phone,
                    FamilyName = relation.FullName,
                    RelativeName = relation.RelationshipName,
                };
                response.Data = print;
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"PrintCommentQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
