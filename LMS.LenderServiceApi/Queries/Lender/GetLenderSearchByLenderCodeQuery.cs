﻿using LMS.Entites.Dtos.LenderService;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace LMS.LenderServiceApi.Queries.Lender
{
    public class GetLenderSearchByLenderCodeQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public List<int> LstContractType { get; set; }
        public string GeneralSearch { get; set; }
    }
    public class GetLenderSearchByLenderCodeQueryHandler : IRequestHandler<GetLenderSearchByLenderCodeQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        ILogger<GetLenderSearchByLenderCodeQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetLenderSearchByLenderCodeQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> loanTab,
            ILogger<GetLenderSearchByLenderCodeQueryHandler> logger)
        {
            _lenderTab = loanTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> Handle(GetLenderSearchByLenderCodeQuery request, CancellationToken cancellationToken)
        {
            Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            List<LenderSearchModel> lenderSearches = new List<LenderSearchModel>();
            try
            {
                if (!string.IsNullOrWhiteSpace(request.GeneralSearch))
                {
                    _lenderTab.WhereClause(x => x.LenderCode.Contains(request.GeneralSearch));
                    if (request.LstContractType.Any())
                        _lenderTab.WhereClause(x => request.LstContractType.Contains(x.ContractType));

                    var lstLender = await _lenderTab.SetGetTop(50).QueryAsync();
                    if (lstLender.Any())
                    {
                        foreach (var item in lstLender)
                        {
                            lenderSearches.Add(new LenderSearchModel()
                            {
                                ID = item.LenderID,
                                FullName = item.LenderCode
                            });
                        }
                    }
                }
                response.SetSucces();
                response.Data = lenderSearches;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLenderSearchByLenderCodeQueryHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
