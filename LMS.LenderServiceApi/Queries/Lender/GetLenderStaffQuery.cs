﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries.Lender
{
    public class GetLenderStaffQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {

    }
    public class GetLenderStaffQueryHandler : IRequestHandler<GetLenderStaffQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> _departmentTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> _userDepartmentTab;


        ILogger<GetLenderStaffQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetLenderStaffQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDepartmentTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> departmentTab,
            ILogger<GetLenderStaffQueryHandler> logger)
        {
            _userTab = userTab;
            _departmentTab = departmentTab;
            _userDepartmentTab = userDepartmentTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> Handle(GetLenderStaffQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            List<Domain.Models.LenderStaff> lenderStaffList = new List<Domain.Models.LenderStaff>();
            try
            {
                var departmentLender = await _departmentTab.SelectColumns(x => x.DepartmentID).WhereClause(x => x.AppID == (int)Menu_AppID.Lender
                                              && x.Status == (int)Department_Status.Active).QueryAsync();
                var deparmentIds = departmentLender.Select(x => x.DepartmentID).ToList();
                var userDepartment = await _userDepartmentTab.SelectColumns(x => x.UserID).WhereClause(x => deparmentIds.Contains(x.DepartmentID)).QueryAsync();
                var userIDs = userDepartment.Select(x => x.UserID).ToList();
                var users = await _userTab.WhereClause(x => userIDs.Contains(x.UserID) && x.Status == (int)User_Status.Active).QueryAsync();
                foreach (var item in users)
                {
                    lenderStaffList.Add(new Domain.Models.LenderStaff()
                    {
                        Name = item.FullName,
                        UserID = item.UserID,
                    });
                }
                response.Data = lenderStaffList;
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLenderStaffQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
