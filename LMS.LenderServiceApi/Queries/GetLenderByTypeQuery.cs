﻿using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries
{
    public class GetLenderByTypeQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int IsHub { get; set; }
        public string GeneralSearch { get; set; }
    }

    public class GetLenderByTypeQueryHandler : IRequestHandler<GetLenderByTypeQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        readonly ILogger<GetLenderByTypeQueryHandler> _logger;
        readonly Common.Helper.Utils _common;
        public GetLenderByTypeQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> loanTab,
            ILogger<GetLenderByTypeQueryHandler> logger)
        {
            _lenderTab = loanTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> Handle(GetLenderByTypeQuery request, CancellationToken cancellationToken)
        {
            Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            try
            {
                int verified = (int)Common.Constants.Lender_IsVerified.Active;
                if (!string.IsNullOrWhiteSpace(request.GeneralSearch)) _lenderTab.WhereClause(x => x.FullName.Contains(request.GeneralSearch));
                var lstLender = await _lenderTab.SetGetTop(50).WhereClause(x => x.IsHub == request.IsHub && x.IsVerified == verified).QueryAsync();
                List<Entites.Dtos.LenderService.LenderSearchModel> lenderSearches = new List<Entites.Dtos.LenderService.LenderSearchModel>();
                foreach (var item in lstLender)
                {
                    lenderSearches.Add(new Entites.Dtos.LenderService.LenderSearchModel()
                    {
                        ID = item.LenderID,
                        FullName = item.FullName
                    });
                }
                response.SetSucces();
                response.Data = lenderSearches;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{System.Reflection.MethodBase.GetCurrentMethod().Name}_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
