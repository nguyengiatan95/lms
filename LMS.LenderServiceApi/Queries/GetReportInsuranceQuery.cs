﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LMS.Common.Helper;

namespace LMS.LenderServiceApi.Queries
{
    public class GetReportInsuranceQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string CodeID { get; set; }
        public string StrFromDate { get; set; }
        public string StrToDate { get; set; }
        public long LenderID { get; set; }
        public int TypeInsurance { get; set; }
        public int StatusSendInsurance { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string CustomerName { get; set; }
    }

    public class GetReportInsuranceQueryHandler : IRequestHandler<GetReportInsuranceQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLendingCertificateInformation> _lendingInformationTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentTab;

        ILogger<GetReportInsuranceQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetReportInsuranceQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLendingCertificateInformation> lendingInformationTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentTab,
            ILogger<GetReportInsuranceQueryHandler> logger)
        {
            _loanTab = loanTab;
            _lenderTab = lenderTab;
            _lendingInformationTab = lendingInformationTab;
            _logger = logger;
            _paymentTab = paymentTab;
            _common = new Common.Helper.Utils();
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> Handle(GetReportInsuranceQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
                try
                {
                    List<Entites.Dtos.LenderService.ReportInsuranceModel> lstReport = new List<Entites.Dtos.LenderService.ReportInsuranceModel>();
                    if (!string.IsNullOrWhiteSpace(request.CodeID))
                    {
                        string codeID = "TC-" + request.CodeID;
                        _loanTab.WhereClause(x => x.ContactCode == codeID);
                    }
                    DateTime fromDate = DateTime.Now.Date;
                    DateTime toDate = DateTime.Now.Date;
                    if (!string.IsNullOrWhiteSpace(request.StrFromDate))
                    {
                        fromDate = DateTime.ParseExact(request.StrFromDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                    }
                    if (!string.IsNullOrWhiteSpace(request.StrToDate))
                    {
                        toDate = DateTime.ParseExact(request.StrToDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                    }
                    fromDate = fromDate.AddDays(-90);
                    toDate = toDate.AddDays(-90);
                    int status = (int)Loan_Status.Lending;
                    _loanTab.WhereClause(x => x.Status == status && x.ToDate >= fromDate && x.ToDate <= toDate && x.SourcecBuyInsurance > 0);
                    if (request.LenderID != (int)StatusCommon.SearchAll) _loanTab.WhereClause(x => x.LenderID == request.LenderID);
                    if (request.StatusSendInsurance != (int)StatusCommon.SearchAll) _loanTab.WhereClause(x => x.StatusSendInsurance == request.StatusSendInsurance);
                    if (request.TypeInsurance != (int)StatusCommon.SearchAll) _loanTab.WhereClause(x => x.SourcecBuyInsurance == request.TypeInsurance);
                    if (!string.IsNullOrWhiteSpace(request.CustomerName))
                    {
                        _loanTab.WhereClause(x => x.CustomerName == request.CustomerName);
                    }
                    var lstLoan = _loanTab.SelectColumns(x => x.LenderID, x => x.FromDate, x => x.ToDate, x => x.TotalMoneyCurrent, x => x.TotalMoneyDisbursement,
                                                        x => x.LoanID, x => x.LinkGCNVay, x => x.LinkGCNChoVay, x => x.CustomerID, x => x.CustomerName
                                                        , x => x.ContactCode, x => x.SourcecBuyInsurance, x => x.StatusSendInsurance, x => x.Status, x => x.LoanTime).Query();
                    response.SetSucces();
                    if (lstLoan != null && lstLoan.Any())
                    {
                        lstLoan = lstLoan.OrderByDescending(x => x.ToDate);
                        response.Total = lstLoan.Count();
                        lstLoan = lstLoan.Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                        // lấy thôhng tin lender;
                        var taskLender = Task.Run(() =>
                         {
                             var lstLenderIds = lstLoan.Select(x => x.LenderID).ToList();
                             return _lenderTab.WhereClause(x => lstLenderIds.Contains(x.LenderID)).Query().ToDictionary(x => x.LenderID, x => x);
                         });
                        // lấy bảng đã quét tiền
                        var taskCertificate = Task.Run(() =>
                        {
                            var lstLoanIds = lstLoan.Select(x => x.LoanID).ToList();
                            return _lendingInformationTab.SelectColumns(x => x.LoanID, x => x.NumberInsurance, x => x.MoneyIndemnification)
                            .WhereClause(x => lstLoanIds.Contains((long)x.LoanID)).Query().ToDictionary(x => x.LoanID, x => x);
                        });
                        //lấy tiền đã đóng tử payment
                        var taskPayment = Task.Run(() =>
                        {
                            var lstLoanIds = lstLoan.Select(x => x.LoanID).ToList();
                            return _paymentTab.WhereClause(x => x.PayMoneyInterest > 0 && lstLoanIds.Contains(x.LoanID)).Query();
                        });
                        Task.WaitAll(taskLender, taskCertificate, taskPayment);
                        var dicLender = taskLender.Result;
                        var dicCetificate = taskCertificate.Result;
                        var lstPayment = taskPayment.Result;

                        //gán giá trị
                        foreach (var item in lstLoan)
                        {
                            var lender = dicLender.ContainsKey(item.LenderID) ? dicLender[item.LenderID] : new Domain.Tables.TblLender();
                            lstReport.Add(new Entites.Dtos.LenderService.ReportInsuranceModel()
                            {
                                ContractCode = item.ContactCode,
                                CustomerName = item.CustomerName,
                                FromDate = item.FromDate,
                                FullName = lender.FullName,
                                LenderID = item.LenderID,
                                InsuranceCode = dicCetificate.ContainsKey((int)item.LoanID) ? dicCetificate[(int)item.LoanID].NumberInsurance : "",
                                LinkGNCLender = item.LinkGCNChoVay,
                                LoanID = item.LoanID,
                                NumberCard = lender.NumberCard,
                                Represent = lender.Represent,
                                ToDate = item.ToDate,
                                TotalMoneyCurrent = item.TotalMoneyCurrent,
                                SourceByInsurance = ((Insurance_PartnerCode)item.SourcecBuyInsurance).GetDescription(),
                                StatusInsurance = item.StatusSendInsurance,
                                StrStatusInsurance = ((Loan_StatusSendInsurance)item.StatusSendInsurance).GetDescription(),
                                TotalMoneyDisbursement = item.TotalMoneyDisbursement,
                                BirthDay = lender.BirthDay ?? DateTime.MinValue,
                                Address = lender.Address,
                                TotalInsuranceMoney = dicCetificate.ContainsKey((int)item.LoanID) && dicCetificate[(int)item.LoanID].MoneyIndemnification != null ? long.Parse(dicCetificate[(int)item.LoanID].MoneyIndemnification.Replace(",", "").Replace(".", "")) : 0,
                                LinkKH = item.LinkGCNVay,
                                LoanTime = item.LoanTime,
                                TotalInterest = lstPayment.Where(x => x.LoanID == item.LoanID).Sum(x => x.PayMoneyInterest)
                            });
                        }
                    }
                    response.Data = lstReport;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"GetLenderListQuery_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                }
                return response;
            });
        }
    }
}
