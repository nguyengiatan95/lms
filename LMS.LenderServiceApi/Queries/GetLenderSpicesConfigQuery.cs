﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries
{
    public class GetLenderSpicesConfigQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LenderID { get; set; }
    }
    public class GetLenderSpicesConfigQueryHandler : IRequestHandler<GetLenderSpicesConfigQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLenderSpicesConfig> _lenderSpicesConfigTab;
        ILogger<GetLenderSpicesConfigQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetLenderSpicesConfigQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLenderSpicesConfig> lenderSpicesConfigTab,
            ILogger<GetLenderSpicesConfigQueryHandler> logger)
        {
            _lenderSpicesConfigTab = lenderSpicesConfigTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }


        public async Task<ResponseActionResult> Handle(GetLenderSpicesConfigQuery request, CancellationToken cancellationToken)
        {
            Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            try
            {
                Domain.Models.LenderDetailSpicesConfig data = new Domain.Models.LenderDetailSpicesConfig();
                var lenderConfigs = await _lenderSpicesConfigTab.WhereClause(x => x.LenderID == request.LenderID).QueryAsync();
                if (lenderConfigs == null && !lenderConfigs.Any())
                {
                    response.Message = MessageConstant.LenderNotExist;
                    return response;
                }
                var lenderConfig = lenderConfigs.FirstOrDefault();
                data.UserID = lenderConfig.UserID.Value;
                data.LenderID = lenderConfig.LenderID;
                data.RateType = int.Parse(lenderConfig.RateType);
                data.LenderSpicesConfigID = lenderConfig.LenderSpicesConfigID;
                data.LoanTimes = _common.ConvertJSonToObjectV2<List<int>>(lenderConfig.LoanTimes);
                data.RangeMoney = ConvertRangeMoney(lenderConfig.MoneyMin ?? 0, lenderConfig.MoneyMax ?? 0);
                response.Data = data;
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLenderSpicesConfigQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private int ConvertRangeMoney(long minMoney, long maxMoney)
        {
            if (minMoney == 0 && maxMoney <= 5000000)
            {
                return (int)LenderSpicesConfig_TypeMoney.Bellow5;
            }
            else if (minMoney == 5000000 && maxMoney <= 15000000)
            {
                return (int)LenderSpicesConfig_TypeMoney.From5To15;
            }
            else if (minMoney == 15000000 && maxMoney > 15000000)
            {
                return (int)LenderSpicesConfig_TypeMoney.Over15;
            }
            else
            {
                return (int)LenderSpicesConfig_TypeMoney.All;
            }
        }
    }
}
