﻿using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries
{
    public class GetLenderContractByIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LenderID { get; set; }
    }

    public class GetLenderContractByIDQueryHandler : IRequestHandler<GetLenderContractByIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLenderDigitalSignature> _userLenderDigitalSignatureTab;
        ILogger<GetLenderContractByIDQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetLenderContractByIDQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLenderDigitalSignature> userLenderDigitalSignatureTab,
            ILogger<GetLenderContractByIDQueryHandler> logger)
        {
            _userLenderDigitalSignatureTab = userLenderDigitalSignatureTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> Handle(GetLenderContractByIDQuery request, CancellationToken cancellationToken)
        {
            Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            try
            {
                Domain.Models.LenderSignatureView lenderSignature = new Domain.Models.LenderSignatureView();
                var lenders = await _userLenderDigitalSignatureTab.WhereClause(x => x.LenderID == request.LenderID).QueryAsync();
                if (lenders != null && lenders.Any())
                {
                    var lender = lenders.First();
                    lenderSignature.LenderID = lender.LenderID;
                    lenderSignature.Signature = lender.AgreementUUID;
                    lenderSignature.Password = lender.PassCode;
                }
                response.SetSucces();
                response.Data = lenderSignature;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLenderContractByIDQueryHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
