﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries
{
    public class GetDetailMoneyLoanQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LenderID { get; set; }
    }
    public class GetDetailMoneyLoanQueryHandler : IRequestHandler<GetDetailMoneyLoanQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionTab;
        ILogger<GetDetailMoneyLoanQueryHandler> _logger;
        Services.ILoanManager _loanManager;
        LMS.Common.Helper.Utils _common;
        public GetDetailMoneyLoanQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionTab,
            Services.ILoanManager loanManager,
        ILogger<GetDetailMoneyLoanQueryHandler> logger)
        {
            _loanTab = loanTab;
            _transactionTab = transactionTab;
            _loanManager = loanManager;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetDetailMoneyLoanQuery request, CancellationToken cancellationToken)
        {
            Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            List<Entites.Dtos.LenderService.LenderDetailMoneyLoan> lenderDetailMoneyLoans = new List<Entites.Dtos.LenderService.LenderDetailMoneyLoan>();
            int statusLoanDelete = (int)Loan_Status.Delete;
            var lstMoneyLender = new List<int>()
            {
                (int)Transaction_TypeMoney.Interest,
                (int)Transaction_TypeMoney.Service,
                (int)Transaction_TypeMoney.Original,
            };

            var lstTranNotGet = new List<int>()
            {
                (int)Transaction_Action.ChoVay,
                (int)Transaction_Action.SuaHd,
                (int)Transaction_Action.VayThemGoc,
                (int)Transaction_Action.HuyVayThemGoc,
                (int)Transaction_Action.HuyVayThemGoc,
            };
            try
            {
                // Danh sách hđ của lender, các đơn khác hủy
                var loanTask = _loanTab.SelectColumns(x => x.LenderID, x => x.LoanID, x => x.ContactCode, x => x.FromDate, x => x.Status, x => x.ToDate, x => x.NextDate).WhereClause(x => x.LenderID == request.LenderID && x.Status != statusLoanDelete).QueryAsync();
                // Lấy transaction tiền gốc , tiền lãi = lãi + service
                var transTask = _transactionTab.SelectColumns(x => x.LoanID, x => x.TotalMoney,x=>x.MoneyType).WhereClause(x => x.LenderID == request.LenderID && lstMoneyLender.Contains(x.MoneyType) && !lstTranNotGet.Contains(x.ActionID)).QueryAsync();
                await Task.WhenAll(loanTask, transTask);
                var loans = loanTask.Result.OrderByDescending(x => x.FromDate);
                var tranactions = transTask.Result;
                foreach (var item in loans)
                {
                    lenderDetailMoneyLoans.Add(new Entites.Dtos.LenderService.LenderDetailMoneyLoan()
                    {
                        CodeID = int.Parse(item.ContactCode.Replace("TC-", "")),
                        FromDate = item.FromDate,
                        ToDate = item.ToDate,
                        LoanID = item.LoanID,
                        TotalMoney = tranactions.Where(x => x.MoneyType == (int)Transaction_TypeMoney.Original && x.LoanID == item.LoanID).Sum(x => x.TotalMoney),
                        TotalMoneyInterest = tranactions.Where(x => (x.MoneyType == (int)Transaction_TypeMoney.Interest || x.MoneyType == (int)Transaction_TypeMoney.Service) && x.LoanID == item.LoanID).Sum(x => x.TotalMoney),
                        Status = await _loanManager.ConvertStatusLMSToAG(item.Status ?? 0, item.NextDate.Value)
                    });
                }
                response.SetSucces();
                response.Data = lenderDetailMoneyLoans;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Lender_GetDetailMoneyLoanQuery_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
