﻿using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries
{
    public class GetLenderInfosByIDsQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public List<long> LenderIDs { get; set; }
    }
    public class GetLenderInfosByIDsQueryHandler : IRequestHandler<GetLenderInfosByIDsQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        ILogger<GetLenderInfosByIDsQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetLenderInfosByIDsQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> loanTab, 
            ILogger<GetLenderInfosByIDsQueryHandler> logger)
        {
            _lenderTab = loanTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> Handle(GetLenderInfosByIDsQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
                try
                {
                    if (request.LenderIDs != null && request.LenderIDs.Count > 0)
                    {
                        var lstInfos = _lenderTab.WhereClause(x => request.LenderIDs.Contains(x.LenderID)).Query().ToList();
                        response.SetSucces();
                        response.Data = _common.ConvertParentToChild<List<LMS.Entites.Dtos.LenderService.LenderInfoModel>, List<Domain.Tables.TblLender>>(lstInfos);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{System.Reflection.MethodBase.GetCurrentMethod().Name}_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                }
                return response;
            });
        }
    }
}
