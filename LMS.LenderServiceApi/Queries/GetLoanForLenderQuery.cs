﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.LenderService;
using LMS.LenderServiceApi.Domain.App;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries
{
    public class GetLoanForLenderQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int DisbursementType { get; set; }
        public int Status { get; set; }
        public long LenderID { get; set; }
    }
    public class GetLoanForLenderQueryHandler : IRequestHandler<GetLoanForLenderQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionTab;


        ILogger<GetLoanForLenderQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetLoanForLenderQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionTab,
            ILogger<GetLoanForLenderQueryHandler> logger)
        {
            _loanTab = loanTab;
            _paymentScheduleTab = paymentScheduleTab;
            _transactionTab = transactionTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetLoanForLenderQuery request, CancellationToken cancellationToken)
        {

            //list lender được truyền
            var lstDisbursementType = new List<int>()
                {
                    (int)Lender_LoanSearch_DisbursementType.DisbursementDate,
                    (int)Lender_LoanSearch_DisbursementType.FinishDate
                };
            // list status được truyền
            var lstStatus = new List<int>()
                {
                    (int)Lender_LoanSearch_Status.Lending,
                    (int)Lender_LoanSearch_Status.BadDebt,
                    (int)Lender_LoanSearch_Status.WaitInsurance,
                    (int)Lender_LoanSearch_Status.Closed,
                };

            LoanLenderView loanLender = new LoanLenderView();
            Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            try
            {

                // check input
                // lender
                if (request.LenderID <= 0)
                {
                    response.Message = MessageConstant.Lender_LenderIDError;
                    return response;
                }
                if (!lstDisbursementType.Contains(request.DisbursementType))
                {
                    response.Message = MessageConstant.Lender_DisbursementTypeError;
                    return response;
                }
                if (!lstStatus.Contains(request.Status))
                {
                    response.Message = MessageConstant.Lender_StatusTypeError;
                    return response;
                }

                // bắn theo ngày 
                if (!string.IsNullOrWhiteSpace(request.FromDate))
                {
                    DateTime fromDate = DateTime.ParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                    if (request.DisbursementType == (int)Lender_LoanSearch_DisbursementType.DisbursementDate)
                    {
                        _loanTab.WhereClause(x => x.FromDate >= fromDate);
                    }
                    else if (request.DisbursementType == (int)Lender_LoanSearch_DisbursementType.FinishDate)
                    {
                        _loanTab.WhereClause(x => x.FinishDate >= fromDate);
                    }
                }

                if (!string.IsNullOrWhiteSpace(request.ToDate))
                {
                    DateTime toDate = DateTime.ParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                    if (request.DisbursementType == (int)Lender_LoanSearch_DisbursementType.DisbursementDate)
                    {
                        _loanTab.WhereClause(x => x.FromDate <= toDate);
                    }
                    else if (request.DisbursementType == (int)Lender_LoanSearch_DisbursementType.FinishDate)
                    {
                        _loanTab.WhereClause(x => x.FinishDate >= toDate);
                    }
                }

                //bỏ đi hđ xóa
                int statusDelete = (int)Loan_Status.Delete;
                _loanTab.WhereClause(x => x.Status != statusDelete);
                // search theo trạng thái
                switch ((Lender_LoanSearch_Status)request.Status)
                {
                    case Lender_LoanSearch_Status.Lending:
                        {
                            int status = (int)Loan_Status.Lending;
                            _loanTab.WhereClause(x => x.Status == status);
                            break;
                        }
                    case Lender_LoanSearch_Status.BadDebt:
                        {
                            int status = (int)Loan_Status.Lending;
                            _loanTab.WhereClause(x => x.Status == status && x.NextDate < DateTime.Now);
                            break;
                        }
                    case Lender_LoanSearch_Status.WaitInsurance:
                        {
                            int statusSendInsurance = (int)Loan_StatusSendInsurance.Send;
                            _loanTab.WhereClause(x => x.StatusSendInsurance == statusSendInsurance);
                            break;
                        }
                    case Lender_LoanSearch_Status.Closed:
                        {
                            List<int> lstCloseStatus = new List<int>()
                                {
                                    (int)Loan_Status.Close,
                                    (int)Loan_Status.CloseLiquidation,
                                    (int)Loan_Status.CloseLiquidationLoanInsurance,
                                    (int)Loan_Status.CloseIndemnifyLoanInsurance,
                                    (int)Loan_Status.ClosePaidInsurance,
                                    (int)Loan_Status.CloseExemption,
                                    (int)Loan_Status.ClosePaidInsuranceExemption,
                                };
                            _loanTab.WhereClause(x => lstCloseStatus.Contains((int)x.Status));
                            break;
                        }
                    default:
                        break;
                }
                var loans = await _loanTab.SelectColumns(x => x.LoanID, x => x.CustomerID, x => x.CustomerName, x => x.TotalMoneyCurrent, x => x.ContactCode, x => x.LoanCreditIDOfPartner,
                                                         x => x.FromDate, x => x.Status, x => x.StatusSendInsurance, x => x.TotalMoneyDisbursement)
                    .WhereClause(x => x.LenderID == request.LenderID).QueryAsync();
                //lấy transaction theo loan
                var taskTransaction = Task.Run(() =>
                {
                    var lstMoneyLender = new List<int>()
                    {
                        (int)Transaction_TypeMoney.Interest,
                        (int)Transaction_TypeMoney.Service
                    };
                    var transactions = _transactionTab.SelectColumns(x => x.TotalMoney, x => x.LoanID).WhereClause(x => x.LenderID == request.LenderID && lstMoneyLender.Contains(x.MoneyType)).Query();
                    return transactions;
                });
                var taskPayment = Task.Run(() =>
                {

                    var loanIds = loans.Select(x => x.LoanID).ToList();
                    int pageSize = 2000;
                    int numberRow = loanIds.Count() / pageSize + 1;
                    int statusActive = (int)StatusCommon.Active;
                    List<Domain.Tables.TblPaymentSchedule> payments = new List<Domain.Tables.TblPaymentSchedule>();
                    for (int i = 1; i <= numberRow; i++)
                    {
                        payments.AddRange(_paymentScheduleTab.SelectColumns(x => x.MoneyInterest, x => x.PayMoneyInterest, x => x.MoneyService, x => x.PayMoneyService, x => x.LoanID)
                        .WhereClause(x => loanIds.Contains(x.LoanID) && x.Status == statusActive).Query());
                    }
                    return payments;
                });
                await Task.WhenAll(taskTransaction, taskPayment);
                // lấy payment theo loan
                var transLoan = taskTransaction.Result;
                var paymentLoan = taskPayment.Result;
                loans = loans.OrderByDescending(x => x.FromDate).ToList();
                foreach (var item in loans)
                {
                    long interestExpected = paymentLoan.Where(x => x.LoanID == item.LoanID).Sum(x => (x.MoneyInterest + x.MoneyService - x.PayMoneyInterest - x.PayMoneyService));
                    long interestEarn = transLoan.Where(x => x.LoanID == item.LoanID).Sum(x => x.TotalMoney);
                    loanLender.TotalLoan += 1;
                    loanLender.SumTotalMoneyDisbursement += item.TotalMoneyDisbursement;
                    loanLender.SumTotalMoney += item.TotalMoneyCurrent;
                    loanLender.SumMoneyInterestExpected += interestExpected;
                    loanLender.SumMoneyInterestEarned += interestEarn;
                    loanLender.LstLoan.Add(new LoanDetailView()
                    {
                        LoanTotalMoneyCurrent = item.TotalMoneyCurrent,
                        LoanCode = int.Parse(item.ContactCode.Replace("TC-", "")),
                        LoanCreditCode = (item.LoanCreditIDOfPartner ?? 0).ToString(),
                        LoanDate = item.FromDate.ToString(TimaSettingConstant.DateTimeDayMonthYear),
                        LoanCreditID = item.LoanCreditIDOfPartner ?? 0,
                        LoanStatus = item.Status ?? request.Status,
                        LoanStatusName = ((Loan_Status)item.Status).GetDescription(),
                        LoanMoneyDisbursement = item.TotalMoneyDisbursement,
                        CustomerName = item.CustomerName,
                        MoneyInterestEarned = interestEarn,
                        MoneyInterestExpected = interestExpected
                    });
                }
                response.SetSucces();
                response.Data = loanLender;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Lender_GetLoanForLenderQueryHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
