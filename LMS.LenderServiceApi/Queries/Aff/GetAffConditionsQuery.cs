﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries.Aff
{
    public class GetAffConditionsQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string GeneralSearch { get; set; }
        public int Status { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
    public class GetAffConditionsQueryHandler : IRequestHandler<GetAffConditionsQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> _affTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> _userLenderTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        readonly ILogger<GetAffConditionsQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public GetAffConditionsQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> affTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> userLenderTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            ILogger<GetAffConditionsQueryHandler> logger)
        {
            _affTab = affTab;
            _userTab = userTab;
            _userLenderTab = userLenderTab;
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetAffConditionsQuery request, CancellationToken cancellationToken)
        {
            Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            List<Domain.Models.AffView> affViews = new List<Domain.Models.AffView>();
            try
            {
                if (!string.IsNullOrWhiteSpace(request.GeneralSearch))
                {
                    _affTab.WhereClause(x => x.Name.Contains(request.GeneralSearch) || x.Code.Contains(request.GeneralSearch) || x.PhoneNumber == request.GeneralSearch);
                }
                if (request.Status != (int)StatusCommon.SearchAll)
                {
                    _affTab.WhereClause(x => x.Status == request.Status);
                }
                if (!string.IsNullOrWhiteSpace(request.FromDate))
                {
                    var dtFromdate = DateTime.ParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                    _affTab.WhereClause(x => x.CreatedDate >= dtFromdate);
                }
                if (!string.IsNullOrWhiteSpace(request.ToDate))
                {
                    var dtToDate = DateTime.ParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, null).AddDays(1);
                    _affTab.WhereClause(x => x.CreatedDate < dtToDate);
                }
                var lstAff = await _affTab.OrderByDescending(x => x.CreatedDate).QueryAsync();
                response.Total = lstAff.Count();
                lstAff = lstAff.Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                var lstStaffID = lstAff.Select(x => x.StaffID).ToList();
                var affIds = lstAff.Select(x => x.AffID).ToList();
                var userTask = _userTab.SelectColumns(x => x.UserID, x => x.FullName, x => x.UserName).WhereClause(x => lstStaffID.Contains(x.UserID)).QueryAsync();
                var lenderTask = _lenderTab.SelectColumns(x => x.AffID, x => x.OwnerUserID, x => x.LenderID, x => x.LenderCode).WhereClause(x => affIds.Contains((long)x.AffID)).QueryAsync();
                await Task.WhenAll(userTask, lenderTask);
                var dicUser = userTask.Result.ToDictionary(x => x.UserID, x => x);
                var lstLenders = lenderTask.Result.ToList();
                var ownerLenderIds = lstLenders.Select(x => x.OwnerUserID).Distinct().ToList();
                var lstLenderUser = await _userLenderTab.WhereClause(x => x.Status == (int)LenderUser_Status.Active
                                                                    && x.IsVerified == (int)LenderUser_Verified.Active
                                                                    && ownerLenderIds.Contains((long)x.UserID)).QueryAsync();
                var lstOwnerLendersID = lstLenderUser.Select(x => x.UserID).ToList();
                Dictionary<long, long> dicAffTotal = new Dictionary<long, long>();
                lstLenders = lstLenders.Where(x => lstOwnerLendersID.Contains(x.OwnerUserID)).ToList();


                foreach (var item in lstAff)
                {
                    affViews.Add(new Domain.Models.AffView()
                    {
                        Address = item.Address,
                        Status = item.Status,
                        StrStatus = ((Aff_Status)item.Status).GetDescription(),
                        AffID = item.AffID,
                        AffName = item.Name,
                        BirthDay = item.BirthDay,
                        Code = item.Code,
                        ContractDate = item.ContractDate,
                        CreatedDate = item.CreatedDate,
                        Email = item.Email,
                        NumberCard = item.NumberCard,
                        Phone = item.PhoneNumber,
                        Source = item.Source,
                        BankID = item.BankID,
                        BankNumber = item.BankNumber,
                        BankPlace = item.BankPlace,
                        CodeNumber = item.CodeNumber,
                        FinishContractDate = item.FinishContractDate,
                        PercentComission = item.PercentComission,
                        TaxNumber = item.TaxNumber,
                        StaffName = dicUser.ContainsKey((long)item.StaffID) ? dicUser[(long)item.StaffID].FullName : "",
                        TotalLender = lstLenders.Where(x => x.AffID == item.AffID).GroupBy(x => x.OwnerUserID).Count(),
                        StaffID = item.StaffID,
                    });
                }
                response.SetSucces();
                response.Data = affViews;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetAffConditionsQueryHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
