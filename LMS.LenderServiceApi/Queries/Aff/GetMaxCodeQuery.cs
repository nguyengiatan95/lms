﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries.Aff
{
    public class GetMaxCodeQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {

    }
    public class GetMaxCodeQueryHandler : IRequestHandler<GetMaxCodeQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> _affTab;
        readonly ILogger<GetMaxCodeQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public GetMaxCodeQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> affTab,
            ILogger<GetMaxCodeQueryHandler> logger)
        {
            _affTab = affTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetMaxCodeQuery request,
            CancellationToken cancellationToken)
        {
            Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            long code = 1;
            var userNameAff = TimaSettingConstant.UserNameCTVConfig;
            try
            {
                var aff = await _affTab.SetGetTop(1).SelectColumns(x => x.Code).OrderByDescending(x => x.CreatedDate).QueryAsync();
                if (aff != null && !string.IsNullOrWhiteSpace(aff.First().Code))
                {
                    try
                    {
                        code = long.Parse(aff.First().Code.Replace(userNameAff, "")) + 1;
                    }
                    catch
                    {
                        code = 1;
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetAffConditionsQueryHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            response.SetSucces();
            response.Data = $"{userNameAff}{code.ToString(TimaSettingConstant.FormatLengthCTVCode)}";
            return response;
        }
    }
}
