﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries.Aff
{
    public class GetLinkAffQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserID { get; set; }
    }
    public class GetLinkAffQueryHandler : IRequestHandler<GetLinkAffQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> _affTab;
        readonly ILogger<GetLinkAffQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        readonly Domain.Configuration.AffLink _affLink;
        public GetLinkAffQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> affTab,
            Domain.Configuration.AffLink affLink,
            ILogger<GetLinkAffQueryHandler> logger)
        {
            _affTab = affTab;
            _affLink = affLink;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetLinkAffQuery request, CancellationToken cancellationToken)
        {
            Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            try
            {
                var affs = await _affTab.WhereClause(x => x.UserID == request.UserID).QueryAsync();
                if (affs != null && affs.Any())
                {
                    string data = string.Format(_affLink.Url, affs.First().Code);
                    response.Data = data;
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLinkAffQueryHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
