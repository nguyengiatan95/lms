﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries.Aff
{
    public class GetLstSelectAffQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string GeneralSearch { get; set; }
        public long UserIDLogin { get; set; }
    }

    public class GetLstSelectAffQueryHandler : IRequestHandler<GetLstSelectAffQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> _lenderTab;
        readonly ILogger<GetLstSelectAffQueryHandler> _logger;
        readonly Common.Helper.Utils _common;
        public GetLstSelectAffQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> loanTab,
            ILogger<GetLstSelectAffQueryHandler> logger)
        {
            _lenderTab = loanTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> Handle(GetLstSelectAffQuery request, CancellationToken cancellationToken)
        {
            Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            try
            {
                if (!string.IsNullOrWhiteSpace(request.GeneralSearch)) _lenderTab.WhereClause(x => x.Code.Contains(request.GeneralSearch));
                if (request.UserIDLogin != TimaSettingConstant.UserIDAdmin)
                {
                    _lenderTab.WhereClause(x => x.StaffID == request.UserIDLogin);
                }
                var lstLender = await _lenderTab.SetGetTop(50).WhereClause(x => x.Status == (int)Aff_Status.Active).QueryAsync();
                List<Entites.Dtos.LenderService.LenderSearchModel> lenderSearches = new List<Entites.Dtos.LenderService.LenderSearchModel>();
                foreach (var item in lstLender)
                {
                    lenderSearches.Add(new Entites.Dtos.LenderService.LenderSearchModel()
                    {
                        ID = item.AffID,
                        FullName = $"{item.Code}-{item.Name}"
                    });
                }
                response.SetSucces();
                response.Data = lenderSearches;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{System.Reflection.MethodBase.GetCurrentMethod().Name}_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
