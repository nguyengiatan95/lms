﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries.Aff
{
    public class GetLstLenderInfoByConditionsQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string GeneralSearch { get; set; }
        public int Status { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public long AffID { get; set; }
        public long IsCollaborator { get; set; }
        public long UserIDLogin { get; set; }
    }
    public class GetLstLenderInfoByConditionsQueryHandler : IRequestHandler<GetLstLenderInfoByConditionsQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> _affTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> _userLenderTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCollaboratorLender> _collaboratorLenderTab;
        readonly ILogger<GetLstLenderInfoByConditionsQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public GetLstLenderInfoByConditionsQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> affTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> userLenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCollaboratorLender> collaboratorLenderTab,
            ILogger<GetLstLenderInfoByConditionsQueryHandler> logger)
        {
            _affTab = affTab;
            _userLenderTab = userLenderTab;
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _collaboratorLenderTab = collaboratorLenderTab;
        }

        public async Task<ResponseActionResult> Handle(GetLstLenderInfoByConditionsQuery request, CancellationToken cancellationToken)
        {
            Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            response.SetSucces();
            List<Domain.Models.AffView> affViews = new List<Domain.Models.AffView>();
            List<Domain.UserLender.AffUserLenderContractItem> lstUserLenderResult = new List<Domain.UserLender.AffUserLenderContractItem>();
            Dictionary<long, Domain.Tables.TblUserLender> dictUserLenderInfos = new Dictionary<long, Domain.Tables.TblUserLender>();
            try
            {
                long affID = (long)LMS.Common.Constants.StatusCommon.SearchAll;
                if (request.IsCollaborator > 0 && request.UserIDLogin != TimaSettingConstant.UserIDAdmin)
                {
                    // lấy thông tin affID theo user login
                    affID = (await _affTab.SetGetTop(1).WhereClause(x => x.UserID == request.UserIDLogin).QueryAsync()).FirstOrDefault()?.AffID ?? long.MaxValue;
                    _affTab.WhereClause(x => x.AffID == affID);
                    _lenderTab.WhereClause(x => x.AffID == affID);
                }
                else if (request.AffID > 0)
                {
                    affID = request.AffID;
                    _affTab.WhereClause(x => x.AffID == affID);
                    _lenderTab.WhereClause(x => x.AffID == affID);
                }
                else
                {
                    _lenderTab.WhereClause(x => x.AffID > 0);
                }
                if (!string.IsNullOrWhiteSpace(request.FromDate))
                {
                    var dtFromdate = DateTime.ParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                    dtFromdate = dtFromdate.AddSeconds(-1);
                    _userLenderTab.WhereClause(x => x.CreateDate > dtFromdate);
                    _collaboratorLenderTab.WhereClause(x => x.CreateDate > dtFromdate);
                }
                if (!string.IsNullOrWhiteSpace(request.ToDate))
                {
                    var dtToDate = DateTime.ParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, null).AddDays(1);
                    _userLenderTab.WhereClause(x => x.CreateDate < dtToDate);
                    _collaboratorLenderTab.WhereClause(x => x.CreateDate < dtToDate);
                }

                if (request.Status != (int)StatusCommon.SearchAll)
                {
                    //_userLenderTab.WhereClause(x => x.IsVerified == request.Status);
                    switch ((CollaboratorLender_Status)request.Status)
                    {
                        case CollaboratorLender_Status.WaitApprove:
                            _collaboratorLenderTab.WhereClause(x => x.Status == (int)CollaboratorLender_Status.WaitApprove);
                            break;
                        case CollaboratorLender_Status.Approved:
                            _collaboratorLenderTab.WhereClause(x => x.Status == (int)CollaboratorLender_Status.Approved);
                            break;
                        default:
                            _collaboratorLenderTab.WhereClause(x => x.Status == (int)CollaboratorLender_Status.Cannel);
                            break;

                    }
                    switch ((LenderUser_Verified)request.Status)
                    {
                        case LenderUser_Verified.Active:
                            _lenderTab.WhereClause(x => x.ContractDate != null);
                            break;
                        case LenderUser_Verified.UnActive:
                            _lenderTab.WhereClause(x => x.ContractDate == null);
                            break;
                        default: // xóa
                            _lenderTab.WhereClause(x => x.Status == (int)LenderUser_Status.UnActive);
                            break;
                    }
                }
                else
                {
                    _collaboratorLenderTab.WhereClause(x => x.Status != (int)CollaboratorLender_Status.Cannel);
                }
                if (!string.IsNullOrWhiteSpace(request.GeneralSearch))
                {
                    if (request.GeneralSearch.Length == TimaSettingConstant.MaxLengthPhone && Regex.Match(request.GeneralSearch, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success)
                    {
                        _userLenderTab.WhereClause(x => x.Phone == request.GeneralSearch);
                        _collaboratorLenderTab.WhereClause(x => x.PhoneNumber == request.GeneralSearch);
                    }
                    else if (Regex.Match(request.GeneralSearch, TimaSettingConstant.PatternNumberCard, RegexOptions.IgnoreCase).Success)
                    {
                        _userLenderTab.WhereClause(x => x.NumberCard == request.GeneralSearch);
                    }
                    else
                    {
                        _userLenderTab.WhereClause(x => x.FullName.Contains(request.GeneralSearch));
                        _collaboratorLenderTab.WhereClause(x => x.FullName.Contains(request.GeneralSearch));
                    }
                }
                var lstLenderContractDataTask = _lenderTab.SelectColumns(x => x.OwnerUserID, x => x.CreateDate, x => x.ContractType, x => x.ContractDate, x => x.AffID)
                                                      .QueryAsync();

                var lstUserLenderDataTask = _userLenderTab.QueryAsync();
                var lstcollaboratorLenderTask = _collaboratorLenderTab.QueryAsync();
                var lstAffDataTask = _affTab.QueryAsync();
                await Task.WhenAll(lstUserLenderDataTask, lstLenderContractDataTask, lstcollaboratorLenderTask, lstAffDataTask);

                int totalLenderRegisterByLink = 0;
                int totalLenderHasContract = 0;
                var lstLenderContractInfos = lstLenderContractDataTask.Result.ToList();
                dictUserLenderInfos = lstUserLenderDataTask.Result.GroupBy(x => (long)x.UserID).ToDictionary(x => x.Key, x => x.FirstOrDefault());

                var lstCollaboratorLenderData = lstcollaboratorLenderTask.Result.ToList();

                var dictCodeAffInfos = new Dictionary<string, Domain.Tables.TblAff>();
                var dictUserIDAffInfos = new Dictionary<long, Domain.Tables.TblAff>();
                foreach (var item in lstAffDataTask.Result)
                {
                    if (!string.IsNullOrEmpty(item.Code) && !dictCodeAffInfos.ContainsKey(item.Code))
                    {
                        dictCodeAffInfos.Add(item.Code, item);
                    }
                    if (item.AffID > 0 && !dictUserIDAffInfos.ContainsKey(item.AffID))
                    {
                        dictUserIDAffInfos.Add(item.AffID, item);
                    }
                }
                totalLenderRegisterByLink = lstCollaboratorLenderData.Count;
                var dictPhoneAffUserLenderContractItemInfos = new Dictionary<string, Domain.UserLender.AffUserLenderContractItem>();
                foreach (var item in lstLenderContractInfos)
                {
                    var userLenderInfo = dictUserLenderInfos.GetValueOrDefault(item.OwnerUserID);
                    if (userLenderInfo == null || userLenderInfo.UserID < 1)
                    {
                        continue;
                    }
                    var affInfo = dictUserIDAffInfos.GetValueOrDefault(item.AffID.GetValueOrDefault());
                    if (affInfo == null || affInfo.AffID < 1)
                    {
                        continue;
                    }
                    var keyPhoneInDict = $"{userLenderInfo.Phone}_{affInfo?.Code}";
                    if (!dictPhoneAffUserLenderContractItemInfos.ContainsKey(keyPhoneInDict))
                    {
                        dictPhoneAffUserLenderContractItemInfos.Add(keyPhoneInDict, new Domain.UserLender.AffUserLenderContractItem
                        {
                            LenderFullName = userLenderInfo.FullName,
                            LenderPhone = userLenderInfo.Phone,
                            LenderVerify = userLenderInfo.IsVerified,
                            LenderVerifyName = ((LenderUser_Verified)userLenderInfo.IsVerified).GetDescription(),
                            StrCreateDate = userLenderInfo.CreateDate.ToString(TimaSettingConstant.DateTimeDayMonthYear),
                            AffCode = affInfo?.Code,
                            AffFullName = affInfo?.Name,
                            StartDate = userLenderInfo.CreateDate
                        });

                        totalLenderHasContract += 1;
                    }

                    dictPhoneAffUserLenderContractItemInfos[keyPhoneInDict].NumberContract += 1;

                    if (string.IsNullOrEmpty(dictPhoneAffUserLenderContractItemInfos[keyPhoneInDict].StrStartDate) && item.ContractDate.HasValue)
                    {
                        dictPhoneAffUserLenderContractItemInfos[keyPhoneInDict].StrStartDate = item.ContractDate.Value.ToString(TimaSettingConstant.DateTimeDayMonthYear);
                    }
                }
                // bảng thông tin lender nháp dựa vào trạng thái duyệt -> xem như đã tạo bên ag
                foreach (var item in lstCollaboratorLenderData)
                {
                    var view = new Domain.UserLender.AffUserLenderContractItem
                    {
                        LenderFullName = item.FullName,
                        LenderPhone = item.PhoneNumber,
                        LenderVerify = (int)LenderUser_Verified.UnActive,
                        StrCreateDate = item.CreateDate.ToString(TimaSettingConstant.DateTimeDayMonthYear),
                        StartDate = item.CreateDate
                    };
                    var affDetail = dictCodeAffInfos.GetValueOrDefault(item.CodeCollaborator ?? "");

                    if (affDetail == null || affDetail.AffID < 1)
                    {
                        continue;
                    }
                    view.AffCode = affDetail?.Code ?? item.CodeCollaborator;
                    var keyPhoneInDict = $"{item.PhoneNumber}_{item.CodeCollaborator}";
                    if (!dictPhoneAffUserLenderContractItemInfos.ContainsKey(keyPhoneInDict))
                    {
                        dictPhoneAffUserLenderContractItemInfos.Add(keyPhoneInDict, new Domain.UserLender.AffUserLenderContractItem
                        {
                            LenderFullName = item.FullName,
                            LenderPhone = item.PhoneNumber,
                            StrCreateDate = item.CreateDate.ToString(TimaSettingConstant.DateTimeDayMonthYear),
                            AffCode = affDetail?.Code ?? item.CodeCollaborator,
                            AffFullName = affDetail?.Name,
                            StartDate = item.CreateDate
                        });
                    }
                    if (item.Status == (int)CollaboratorLender_Status.Approved)
                    {
                        dictPhoneAffUserLenderContractItemInfos[keyPhoneInDict].LenderVerify = (int)LenderUser_Verified.Active;
                    }
                    dictPhoneAffUserLenderContractItemInfos[keyPhoneInDict].LenderVerifyName = ((LenderUser_Verified)dictPhoneAffUserLenderContractItemInfos[keyPhoneInDict].LenderVerify).GetDescription();
                }
                response.Total = dictPhoneAffUserLenderContractItemInfos.Count();
                lstUserLenderResult = dictPhoneAffUserLenderContractItemInfos.Values.OrderByDescending(x => x.StartDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                if (affID > 0)
                {
                    var affDetail = dictUserIDAffInfos.GetValueOrDefault(affID);
                    if (affDetail != null && affDetail.UserID > 0)
                    {
                        totalLenderRegisterByLink = lstCollaboratorLenderData.Count(x => x.CodeCollaborator == affDetail.Code);
                    }
                }

                if (lstUserLenderResult.Count > 0)
                {
                    lstUserLenderResult[0].TotalLenderHasContract = totalLenderHasContract;
                    lstUserLenderResult[0].TotalLenderRegisterByLink = totalLenderRegisterByLink;
                }
                response.Data = lstUserLenderResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLstLenderInfoByConditionsQueryHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
