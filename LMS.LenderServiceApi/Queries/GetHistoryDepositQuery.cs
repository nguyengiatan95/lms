﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.Queries
{
    public class GetHistoryDepositQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long? PaymentScheduleDepositMoneyID { get; set; }
    }
    public class GetHistoryDepositQueryHandler : IRequestHandler<GetHistoryDepositQuery, LMS.Common.Constants.ResponseActionResult>
    {
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogActionPaymentDepositMoney> _logTab;
        private readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<GetHistoryDepositQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetHistoryDepositQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogActionPaymentDepositMoney> logTab,
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
        ILogger<GetHistoryDepositQueryHandler> logger)
        {
            _logTab = logTab;
            _userTab = userTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetHistoryDepositQuery request, CancellationToken cancellationToken)
        {
            Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            List<Domain.Models.LenderHistoryDepositDetail> lenderDetailMoneyLoans = new List<Domain.Models.LenderHistoryDepositDetail>();
            try
            {
                var logs = await _logTab.WhereClause(x => x.PaymentScheduleDepositMoneyID == request.PaymentScheduleDepositMoneyID).QueryAsync();
                var lstUserIds = logs.Select(x => x.UserID).Distinct().ToList();
                if (lstUserIds.Any())
                {
                    var dicUser = (await _userTab.SelectColumns(x => x.UserID, x => x.UserName, x => x.FullName).WhereClause(x => lstUserIds.Contains(x.UserID)).QueryAsync()).ToDictionary(x => x.UserID, x => x);
                    foreach (var item in logs)
                    {
                        lenderDetailMoneyLoans.Add(new Domain.Models.LenderHistoryDepositDetail()
                        {
                            UserID = item.UserID,
                            CreateDate = item.CreateDate,
                            LoanID = item.LoanID,
                            LogActionPaymentDepositMoneyID = item.LogActionPaymentDepositMoneyID,
                            Note = item.Note,
                            PaymentScheduleDepositMoneyID = item.PaymentScheduleDepositMoneyID,
                            UserName = dicUser.ContainsKey((long)item.UserID) ? dicUser[(long)item.UserID].UserName + " - " + dicUser[(long)item.UserID].FullName : "",
                        });

                    }
                }
                response.SetSucces();
                response.Data = lenderDetailMoneyLoans;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Lender_GetHistoryDepositQuery_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
