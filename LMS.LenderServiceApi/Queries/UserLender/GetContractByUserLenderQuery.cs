﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.LenderServiceApi.Domain.UserLender;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace LMS.LenderServiceApi.Queries.UserLender
{
    public class GetContractByUserLenderQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserLenderID { get; set; }
    }
    public class GetContractByUserLenderQueryHandler : IRequestHandler<GetContractByUserLenderQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> _userLenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        ILogger<GetContractByUserLenderQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetContractByUserLenderQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserLender> userLenderTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            ILogger<GetContractByUserLenderQueryHandler> logger)
        {
            _userLenderTab = userLenderTab;
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetContractByUserLenderQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var lstDataResult = new List<GetContractByUserLender>();
            try
            {
                var lstLender = await _lenderTab.WhereClause(x => x.OwnerUserID == request.UserLenderID).QueryAsync();
                if (lstLender.Any())
                {
                    foreach (var item in lstLender)
                    {
                        lstDataResult.Add(new GetContractByUserLender
                        {
                            UserID = item.OwnerUserID,
                            LenderID = item.LenderID,
                            ContractCode = item.ContractCode,
                            ContractDate = item.ContractDate.GetValueOrDefault(),
                            ContractEndDate = item.ContractEndDate.GetValueOrDefault(),
                            ContractType = item.ContractType,
                            TypeLoan = ((Lender_ContractType)item.ContractType).GetDescription(),
                            LenderCode = item.LenderCode == null ? item.FullName : item.LenderCode,
                            ContractDuration = item.ContractDuration.GetValueOrDefault(),
                            TotalMoney = item.TotalMoneyTopup.GetValueOrDefault(),
                            IsDeposit = item.IsDeposit,
                            AffID = item.AffID ?? 0
                        });
                    }
                }
                response.Data = lstDataResult;
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetContractByUserLenderQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
