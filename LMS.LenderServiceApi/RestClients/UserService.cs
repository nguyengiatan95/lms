﻿using LMS.Common.Constants;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace LMS.LenderServiceApi.RestClients
{
    public interface IUserService
    {
        long GetHeaderUserID();
    }
    public class UserService : IUserService
    {
        IHttpContextAccessor _httpContextAccessor;
        public UserService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public long GetHeaderUserID()
        {
            long userID = 0;
            try
            {
                var lstHeader = _httpContextAccessor.HttpContext?.Request?.Headers;
                if (lstHeader != null && lstHeader.Any())
                {
                    var headerCheck = TimaSettingConstant.HeaderLMSUserID.ToLowerInvariant();
                    foreach (var item in lstHeader)
                    {
                        if (item.Key.ToLowerInvariant() == headerCheck)
                        {
                            userID = long.Parse(item.Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            //// test user
            //// 1, 2
            //userID = DateTime.Now.Ticks % 2 == 0 ? 1 : 2;
            return userID;
        }
    }
}
