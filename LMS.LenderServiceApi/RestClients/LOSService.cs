﻿using LMS.Entites.Dtos.LOSServices;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.RestClients
{
    public interface ILOSService
    {
        Task<LMS.Common.Constants.ResponseActionResult> CreateLenderDigitalSignature(long lenderId, string agreementId, string passcode);
        Task<LMS.Entites.Dtos.LOSServices.MapResultLoanWaitingDisbursementDetailLOS> GetLoanCreditDetail(long loanID);
    }
    public class LOSService : ILOSService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        public LOSService(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }

        public async Task<LMS.Common.Constants.ResponseActionResult> CreateLenderDigitalSignature(long lenderId, string agreementId, string passcode)
        {
            var req = new
            {
                LenderId = lenderId,
                AgreementId = agreementId,
                Passcode = passcode
            };
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_LOS_CreateLenderDigitalSignature}";
            return await _apiHelper.ExecuteAsync(url: url.ToString(), RestSharp.Method.POST, req);
        }

        public async Task<MapResultLoanWaitingDisbursementDetailLOS> GetLoanCreditDetail(long loanID)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_LOS_GetLoanCreditDetail}/{loanID}";
            return await _apiHelper.ExecuteAsync<LMS.Entites.Dtos.LOSServices.MapResultLoanWaitingDisbursementDetailLOS>(url: url.ToString());
        }
    }
}