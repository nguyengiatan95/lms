﻿using Microsoft.Extensions.DependencyInjection;

namespace LMS.LenderServiceApi.RestClients
{
    public static class RestClientsInstaller
    {
        public static IServiceCollection AddRestClientsService(this IServiceCollection services)
        {
            services.AddTransient(typeof(ILOSService), typeof(LOSService));
            services.AddTransient(typeof(IUserService), typeof(UserService));
            services.AddTransient(typeof(IProxyTimaService), typeof(ProxyTimaService));
            return services;
        }
    }
}
