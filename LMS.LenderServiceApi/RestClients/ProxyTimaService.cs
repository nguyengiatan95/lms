﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.AG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi.RestClients
{
    public interface IProxyTimaService
    {
        Task<ResponseActionResult> CallApiAg(AgDataPostModel model);
    }
    public class ProxyTimaService : IProxyTimaService
    {
        readonly LMS.Common.Helper.IApiHelper _apiHelper;
        public ProxyTimaService(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }

        public async Task<ResponseActionResult> CallApiAg(AgDataPostModel request)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_CallApiAG}";
            return await _apiHelper.ExecuteAsync(url: url.ToString(), RestSharp.Method.POST, request);
        }
    }
}
