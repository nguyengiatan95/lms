using Confluent.Kafka;
using FluentValidation.AspNetCore;
using LMS.Common.Configuration;
using LMS.Common.Helper;
using LMS.Kafka.Consumer;
using LMS.Kafka.Interfaces;
using LMS.Kafka.Producer;
using LMS.LenderServiceApi.RestClients;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Steeltoe.Discovery.Client;
using Steeltoe.Discovery.Eureka;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace LMS.LenderServiceApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<KestrelServerOptions>(Configuration.GetSection("Kestrel"));
            services.AddDiscoveryClient(Configuration);
            services.AddServiceDiscovery(options => options.UseEureka());
            services.AddSingleton(Configuration);
            services.AddControllers().AddJsonOptions(opts => opts.JsonSerializerOptions.PropertyNamingPolicy = null);
            services.AddHttpContextAccessor();
            LMS.Common.Configuration.ConnectStringSetting.DefaultConnectString = Configuration["ConnectStringSetting:DefaultConnectString"];
            LMS.Common.Configuration.ConnectStringSetting.AGConnectString = Configuration["ConnectStringSetting:AGConnectString"];
            LMS.Entites.Dtos.AG.AGConstants.BaseUrl = Configuration["AGConfiguration:Url"];
            services.AddTransient(typeof(Common.DAL.ITableHelper<>), typeof(Common.DAL.TableHelper<>));
            services.AddTransient<Services.IQueueCallAPIManager, Services.QueueCallAPIManager>();
            services.AddTransient<Services.ILoanManager, Services.LoanManager>();
            services.AddTransient<Services.IExcelManager, Services.ExcelDeposit>();
            services.AddTransient<Services.IExcelManager, Services.ExcelBM01>();
            services.AddTransient<LMS.Common.Helper.IApiHelper, LMS.Common.Helper.ApiHelper>();
            //services.AddMvc().AddFluentValidation();
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddRestClientsService();
            AddServiceKafka(services);
            services.AddRedisCacheService(Configuration);

            var redisCacheSetting = new RedisCacheSetting();
            Configuration.GetSection(nameof(RedisCacheSetting)).Bind(redisCacheSetting);
            services.AddSingleton(redisCacheSetting);
            services.AddSingleton(typeof(RedisCachedServiceHelper), typeof(RedisCachedServiceHelper));


            var sendEmailAffConfig = new Domain.Configuration.SendEmailAffConfig();
            Configuration.GetSection(nameof(Domain.Configuration.SendEmailAffConfig)).Bind(sendEmailAffConfig);
            services.AddSingleton(sendEmailAffConfig);


            var affConfigLink = new Domain.Configuration.AffLink();
            Configuration.GetSection(nameof(Domain.Configuration.AffLink)).Bind(affConfigLink);
            services.AddSingleton(affConfigLink);
            services.AddTransient<Queries.IExcelExportDataHelper, Queries.ExcelExportLenderDeposit>();
            services.AddTransient<Queries.IExcelExportDataHelper, Queries.ExcelExportBM01>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseDiscoveryClient();
            //Add our new middleware to the pipeline
            app.UseMiddleware<LMS.Common.Helper.RequestResponseLoggingMiddleware>();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
        public void AddServiceKafka(IServiceCollection services)
        {
            var clientConfig = new ClientConfig()
            {
                BootstrapServers = Configuration["Kafka:ClientConfigs:BootstrapServers"]
            };

            var producerConfig = new ProducerConfig(clientConfig);
            var consumerConfig = new ConsumerConfig(clientConfig)
            {
                GroupId = Configuration["spring:application:name"],
                //EnableAutoCommit = true,
                AutoOffsetReset = AutoOffsetReset.Earliest,
                StatisticsIntervalMs = 5000,
                SessionTimeoutMs = 6000
            };

            services.AddSingleton(producerConfig);
            services.AddSingleton(consumerConfig);

            services.AddSingleton(typeof(IKafkaProducer<,>), typeof(KafkaProducer<,>));
            services.AddSingleton(typeof(IKafkaConsumer<,>), typeof(KafkaConsumer<,>));

            services.AddHostedService<KafkaServiceConsumer>();

            services.AddScoped<IKafkaHandler<string, Kafka.Messages.Loan.LoanInsertSuccess>, KafkaEventHandlers.CreateLenderDebtByLoanIDHandler>();
            services.AddScoped<IKafkaHandler<string, Kafka.Messages.Loan.LoanCutPeriodSuccessInfo>, KafkaEventHandlers.ConsumLoanCutPeriodSuccessHandler>();
        }
    }
}
