﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.APIAutoDisbursement;
using LMS.LoanServiceApi.Domain.Tables;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Services
{
    public interface IPaymentGatewayService
    {
        Task<CreateTransactionResponse> VibCreateTransaction(CreateTransactionRequest input, int loanCreditID, int retry = 0);
        Task<CancelTransactionResponse> VibCancelTransaction(CancelTransactionRequest dataPost);
        Task<ExecuteOtpResponse> VibExecuteOtp(ExecuteOtpRequest dataPost);
        // đối chiếu nội bộ
        CreateTransactionResponse ReconciliationInternalGetOTP(ReconciliationInternalRequest request);
        ExecuteOtpResponse ReconciliationInternalVerifyOTP(ReconciliationInternalRequest request);
        CancelTransactionResponse ReconciliationInternalCancelOTP(ReconciliationInternalRequest request);
        Task UpdateStatusLogCallApi(string traceIndentifierRequest, int status);
    }
    public class PaymentGatewayService : IPaymentGatewayService
    {
        ILogger<PaymentGatewayService> _logger;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCallApi> _logCallApiTab;
        RestClients.IOutGatewayService _outGatewayService;
        LMS.Common.Helper.Utils _common;
        public PaymentGatewayService(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCallApi> logCallApiTab,
            RestClients.IOutGatewayService outGatewayService,
            ILogger<PaymentGatewayService> logger
        )
        {
            _logCallApiTab = logCallApiTab;
            _outGatewayService = outGatewayService;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<CreateTransactionResponse> VibCreateTransaction(CreateTransactionRequest dataPost, int loanCreditID, int retry = 0)
        {
            CreateTransactionResponse objResultTransaction = null;
            try
            {
                // reset message response
                dataPost.MessageResponse = "";
                var logCallApiDetail = new TblLogCallApi
                {
                    ActionCallApi = (int)LogCallApi_ActionCallApi.PreCreateTransaction,
                    Status = (int)LogCallApi_StatusCallApi.CallApi,
                    NameActionCallApi = (LogCallApi_ActionCallApi.PreCreateTransaction).GetDescription(),
                    InputStringCallApi = _common.ConvertObjectToJSonV2(dataPost),
                    CreateDate = DateTime.Now,
                    ModifyDate = DateTime.Now,
                    LoanCreditID = loanCreditID,
                    TraceIndentifierRequest = dataPost.TraceIndentifier
                };
                logCallApiDetail.LogCallApiID = (int)_logCallApiTab.Insert(logCallApiDetail);
                var responseOutGateway = await _outGatewayService.VibCreateTransaction(dataPost);
                logCallApiDetail.ModifyDate = DateTime.Now;
                logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Error;
                if (responseOutGateway != null)
                {
                    logCallApiDetail.ResultStringCallApi = _common.ConvertObjectToJSonV2(responseOutGateway);
                    if (responseOutGateway.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                    {
                        objResultTransaction = _common.ConvertJSonToObjectV2<CreateTransactionResponse>($"{responseOutGateway.Data}");
                        logCallApiDetail.TraceIndentifierResponse = objResultTransaction.TraceIndentifier;
                        logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.WaitConfirm;
                        // nghiệp vụ cũ, xử lý sau
                        #region nghiệp vụ cũ, xử lý sau
                        //var messageResponse = objResultTransaction.Messages.ToLower();
                        //dataPost.MessageResponse = messageResponse;
                        //switch ((VIB_AI_ResponseCodeConstant)objResultTransaction.Code)
                        //{
                        //    case VIB_AI_ResponseCodeConstant.SUCCESS_CODE:
                        //        logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Success;
                        //        break;
                        //    case VIB_AI_ResponseCodeConstant.FAIL_EXECUTE:
                        //    case VIB_AI_ResponseCodeConstant.FAIL_BANK_CANCEL:
                        //        _logger.LogWarning($"VibCreateTransaction_Retry|retry={retry}|LogCallApiID={logCallApiDetail.LogCallApiID}|Parameters={logCallApiDetail.InputStringCallApi}|resultTransaction={_common.ConvertObjectToJSonV2(objResultTransaction)}");

                        //        if (messageResponse.Contains(MessagerNotConnect)
                        //            || messageResponse.Contains(MessageAccountNotValid)
                        //            || messageResponse.Contains(MessageBankDontSupport))
                        //        {
                        //            // chuyển luôn cho kế toán check
                        //            logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Error;
                        //        }
                        //        else
                        //        {
                        //            // sau lần chờ 10phut đầu tiên vẫn lỗi này
                        //            var timeSleep = TimeSleep;
                        //            if (messageResponse.Contains(MessagerTimeOut)
                        //                || messageResponse.Contains(MessageLogin))
                        //            {
                        //                timeSleep += TimeSleep;
                        //            }
                        //            if (retry < MaxRetry)
                        //            {
                        //                logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Retry;
                        //                _logCallApiTab.Update(logCallApiDetail);
                        //                objResultTransaction.IsRetry = true;
                        //                //System.Threading.Thread.Sleep(TimeSpan.FromMinutes(timeSleep));
                        //                //return VibCreateTransaction(dataPost, loanCreditID, ++retry);
                        //            }
                        //            logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Error;
                        //        }
                        //        break;
                        //    default:
                        //        logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Error;
                        //        _logger.LogError($"VibCreateTransaction_Response_Error|retry={retry}|LogCallApiID={logCallApiDetail.LogCallApiID}|Parameters={logCallApiDetail.InputStringCallApi}|resultTransaction={_common.ConvertObjectToJSonV2(objResultTransaction)}");
                        //        break;
                        //}
                        #endregion
                    }
                }
                _logCallApiTab.Update(logCallApiDetail);
            }
            catch (Exception ex)
            {
                _logger.LogError($"VibCreateTransaction|dataPost={_common.ConvertObjectToJSonV2(dataPost)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return objResultTransaction;
        }

        public async Task<CancelTransactionResponse> VibCancelTransaction(CancelTransactionRequest dataPost)
        {
            CancelTransactionResponse objResultCancleTransaction = null;
            try
            {
                var logCallApiDetail = new TblLogCallApi
                {
                    ActionCallApi = (int)LogCallApi_ActionCallApi.PreCancelTransaction,
                    Status = (int)LogCallApi_StatusCallApi.CallApi,
                    NameActionCallApi = (LogCallApi_ActionCallApi.PreCancelTransaction).GetDescription(),
                    InputStringCallApi = _common.ConvertObjectToJSonV2(dataPost),
                    CreateDate = DateTime.Now,
                    ModifyDate = DateTime.Now,
                    LoanCreditID = dataPost.LoanCreditID,
                    TraceIndentifierRequest = dataPost.TraceIndentifier
                };

                logCallApiDetail.LogCallApiID = (int)_logCallApiTab.Insert(logCallApiDetail);
                var responseOutGateway = await _outGatewayService.VibCancelTransaction(dataPost);
                logCallApiDetail.ModifyDate = DateTime.Now;
                logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Error;
                if (responseOutGateway != null)
                {
                    logCallApiDetail.ResultStringCallApi = _common.ConvertObjectToJSonV2(responseOutGateway);
                    if (responseOutGateway.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                    {
                        objResultCancleTransaction = _common.ConvertJSonToObjectV2<CancelTransactionResponse>($"{responseOutGateway.Data}");
                        logCallApiDetail.TraceIndentifierResponse = objResultCancleTransaction.TraceIndentifier;
                        logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.WaitConfirm;
                        #region nghiệp vụ cũ, review sau
                        //if (objResultCancleTransaction.Code == (int)VIB_AI_ResponseCodeConstant.SUCCESS_CODE)
                        //{
                        //    logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Success;
                        //}
                        //else
                        //{
                        //    _logger.LogWarning($"VibCancelTransaction_Response|LogCallApiID={logCallApiDetail.LogCallApiID}|transID={transID}|Response={_common.ConvertObjectToJSonV2(responseOutGateway)}");
                        //}
                        #endregion
                    }
                }
                _logCallApiTab.Update(logCallApiDetail);
            }
            catch (Exception ex)
            {
                _logger.LogError($"VibCancelTransaction|dataPost={_common.ConvertObjectToJSonV2(dataPost)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return objResultCancleTransaction;
        }

        public async Task<ExecuteOtpResponse> VibExecuteOtp(ExecuteOtpRequest dataPost)
        {
            ExecuteOtpResponse objResultExecuteOtp = null;
            try
            {
                var logCallApiDetail = new TblLogCallApi
                {
                    ActionCallApi = (int)LogCallApi_ActionCallApi.PreExecuteOtp,
                    Status = (int)LogCallApi_StatusCallApi.CallApi,
                    NameActionCallApi = (LogCallApi_ActionCallApi.PreExecuteOtp).GetDescription(),
                    InputStringCallApi = _common.ConvertObjectToJSonV2(dataPost),
                    CreateDate = DateTime.Now,
                    ModifyDate = DateTime.Now,
                    LoanCreditID = dataPost.LoanCreditID,
                    TraceIndentifierRequest = dataPost.TraceIndentifier
                };
                logCallApiDetail.LogCallApiID = (int)_logCallApiTab.Insert(logCallApiDetail);
                var responseOutGateway = await _outGatewayService.VibExecuteOtp(dataPost);
                logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Error;
                if (responseOutGateway != null)
                {
                    logCallApiDetail.ResultStringCallApi = _common.ConvertObjectToJSonV2(responseOutGateway);
                    if (responseOutGateway.Result == (int)ResponseAction.Success)
                    {
                        objResultExecuteOtp = _common.ConvertJSonToObjectV2<ExecuteOtpResponse>($"{responseOutGateway.Data}");
                        logCallApiDetail.TraceIndentifierResponse = objResultExecuteOtp.TraceIndentifier;
                        if (dataPost.ExecuteBy == 0)
                        {
                            logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.WaitConfirm;
                        }
                        else
                        {
                            // kế toán gọi
                            if (objResultExecuteOtp.Code == (int)VIB_AI_ResponseCodeConstant.SUCCESS_CODE)
                            {
                                logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Success;
                            }
                        }
                    }
                }
                else
                {
                    _logger.LogWarning($"VibExecuteOtp|LogCallApiID={logCallApiDetail.LogCallApiID}|Parameters={logCallApiDetail.InputStringCallApi}||call OutGateWayService error");
                }
                logCallApiDetail.ModifyDate = DateTime.Now;
                _logCallApiTab.Update(logCallApiDetail);
                return objResultExecuteOtp;
            }
            catch (Exception ex)
            {
                _logger.LogError($"VibExecuteOtp|parameters={_common.ConvertObjectToJSonV2(dataPost)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return objResultExecuteOtp;
        }

        public CreateTransactionResponse ReconciliationInternalGetOTP(ReconciliationInternalRequest request)
        {
            CreateTransactionResponse response = null;
            try
            {
                var detail = GetDetailLogCallApiByTraceIndentifierRequest(request.TraceIndentifier);
                if (detail != null)
                {
                    response = _common.ConvertJSonToObjectV2<CreateTransactionResponse>(detail.ResultStringCallApi);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ReconciliationInternalGetOTP|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public ExecuteOtpResponse ReconciliationInternalVerifyOTP(ReconciliationInternalRequest request)
        {
            ExecuteOtpResponse response = null;
            try
            {
                var detail = GetDetailLogCallApiByTraceIndentifierRequest(request.TraceIndentifier);
                if (detail != null)
                {
                    response = _common.ConvertJSonToObjectV2<ExecuteOtpResponse>(detail.ResultStringCallApi);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ReconciliationInternalVerifyOTP|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public CancelTransactionResponse ReconciliationInternalCancelOTP(ReconciliationInternalRequest request)
        {
            CancelTransactionResponse response = null;
            try
            {
                var detail = GetDetailLogCallApiByTraceIndentifierRequest(request.TraceIndentifier);
                if (detail != null && detail.Status == (int)LogCallApi_StatusCallApi.Success)
                {
                    response = _common.ConvertJSonToObjectV2<CancelTransactionResponse>(detail.ResultStringCallApi);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ReconciliationInternalCancelOTP|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private Domain.Tables.TblLogCallApi GetDetailLogCallApiByTraceIndentifierRequest(string traceIndentifer)
        {
            try
            {
                var lstLogCallApi = _logCallApiTab.WhereClause(x => x.TraceIndentifierRequest == traceIndentifer).Query();
                if (lstLogCallApi == null || !lstLogCallApi.Any())
                {
                    return null;
                }
                return lstLogCallApi.OrderByDescending(x => x.LogCallApiID).First();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetDetailLogCallApiByTraceIndentifierRequest|traceIndentifer={traceIndentifer}|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }

        public async Task UpdateStatusLogCallApi(string traceIndentifierRequest, int status)
        {
            await Task.Run(() =>
            {
                try
                {
                    var detail = GetDetailLogCallApiByTraceIndentifierRequest(traceIndentifierRequest);
                    if (detail != null)
                    {
                        detail.Status = status;
                        detail.ModifyDate = DateTime.Now;
                        _logCallApiTab.Update(detail);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"UpdateStatusLogCallApi|traceIndentifierRequest={traceIndentifierRequest}|status={status}|ex={ex.Message}-{ex.StackTrace}");
                }
            });
        }
    }
}
