﻿using LMS.Common.Constants;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Services
{
    public interface IBankCardManager
    {
        Task<int> ProcessSummaryBankCard(DateTime transactionDate, List<long> lstBankCardID = null, DateTime? endTransactionDate = null);

        Task<long> ProcessSummaryBankCardFromInvoiceID(long invoiceID = 0);
    }
    public class BankCardManager : IBankCardManager
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogBankCard> _logBankCardTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCashBankCardByDate> _cashBankCardByDateTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingDateBankCard> _settingDateBankCardTab;
        ILogger<BankCardManager> _logger;
        LMS.Common.Helper.Utils _common;
        const int _timeDelay = 100;
        public BankCardManager(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogBankCard> logBankCardTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCashBankCardByDate> cashBankCardByDateTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingDateBankCard> settingDateBankCardTab,
            ILogger<BankCardManager> logger
        )
        {
            _bankCardTab = bankCardTab;
            _logBankCardTab = logBankCardTab;
            _invoiceTab = invoiceTab;
            _cashBankCardByDateTab = cashBankCardByDateTab;
            _settingDateBankCardTab = settingDateBankCardTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<int> ProcessSummaryBankCard(DateTime transactionDate, List<long> lstBankCardID = null, DateTime? endTransactionDate = null)
        {
            try
            {
                DateTime startDate = transactionDate;
                var currentDate = DateTime.Now;
                if (!endTransactionDate.HasValue)
                {
                    endTransactionDate = currentDate;
                }
                // lấy danh sách thẻ
                if (lstBankCardID != null && lstBankCardID.Count > 0)
                {
                    _bankCardTab.WhereClause(x => lstBankCardID.Contains(x.BankCardID));
                }
                var dictBankCardInfos = (await _bankCardTab.QueryAsync()).ToDictionary(x => x.BankCardID, x => x);

                Dictionary<string, Domain.Tables.TblCashBankCardByDate> dictCashBankByDateInfos = new Dictionary<string, Domain.Tables.TblCashBankCardByDate>();

                // kiểm tra ngày cuối cùng trong bảng report có = với ngày startDate ko
                // xóa hết dữ liệu từ startDate cho đến endtransactionDate nếu có
                if (lstBankCardID != null && lstBankCardID.Count > 0)
                {
                    _ = await _cashBankCardByDateTab.DeleteWhereAsync(x => x.ForDate >= startDate.Date && x.ForDate <= endTransactionDate.Value && lstBankCardID.Contains(x.BankCardID));
                }
                else
                {
                    _ = await _cashBankCardByDateTab.DeleteWhereAsync(x => x.ForDate >= startDate.Date && x.ForDate <= endTransactionDate.Value);
                }

                // giữ lại ngày bắt đầu
                DateTime startDateBegin = startDate.Date;
                // đầu ngày hôm sau
                DateTime endDate = endTransactionDate.Value.Date.AddDays(1);
                while (startDate.Date < endDate.Date)
                {
                    // tạo dữ liệu ngày cho tất cả các thẻ
                    foreach (var item in dictBankCardInfos)
                    {
                        var keyDictCashBank = $"{startDate:yyyy-MM-dd}_{item.Key}";
                        if (!dictCashBankByDateInfos.ContainsKey(keyDictCashBank))
                        {
                            var bankCardInfo = dictBankCardInfos.GetValueOrDefault(item.Key);
                            dictCashBankByDateInfos.Add(keyDictCashBank, new Domain.Tables.TblCashBankCardByDate
                            {
                                ForDate = startDate.Date,
                                BankCardID = item.Key,
                                BankCardName = bankCardInfo?.AliasName,
                                CreateDate = currentDate
                            });
                        }
                    }

                    startDate = startDate.AddDays(1);
                }
                // lấy danh invoice theo ngày, transactionDate tính tới giây
                DateTime fromDate = startDateBegin.AddSeconds(-1);
                while (true)
                {
                    if (lstBankCardID != null && lstBankCardID.Count > 0)
                    {
                        _invoiceTab.WhereClause(x => lstBankCardID.Contains(x.BankCardID));
                    }
                    var lstInvoiceInfos = (await _invoiceTab.SetGetTop(TimaSettingConstant.MaxItemQuery).SelectColumns(x => x.BankCardID, x => x.TotalMoney, x => x.InvoiceType, x => x.TransactionDate)
                                                .WhereClause(x => x.TransactionDate > fromDate && x.TransactionDate < endDate && x.BankCardID > 0 && x.Status != (int)Invoice_Status.Deleted)
                                                .OrderBy(x => x.TransactionDate).QueryAsync()).ToList();
                    if (lstInvoiceInfos == null || lstInvoiceInfos.Count == 0)
                    {
                        break;
                    }
                    fromDate = lstInvoiceInfos.Last().TransactionDate;
                    foreach (var item in lstInvoiceInfos)
                    {
                        var keyDictCashBank = $"{item.TransactionDate:yyyy-MM-dd}_{item.BankCardID}";
                        if (dictCashBankByDateInfos.ContainsKey(keyDictCashBank))
                        {
                            var cashBankCardInfo = dictCashBankByDateInfos[keyDictCashBank];
                            switch ((Common.Constants.InvoiceType)item.InvoiceType)
                            {
                                case Common.Constants.InvoiceType.Receipt:
                                    cashBankCardInfo.MoneyInBound += item.TotalMoney;
                                    break;
                                case Common.Constants.InvoiceType.PaySlip:
                                    cashBankCardInfo.MoneyOutBound += item.TotalMoney;
                                    break;
                                default:
                                    if (item.TotalMoney > 0)
                                    {
                                        cashBankCardInfo.MoneyInBound += item.TotalMoney;
                                    }
                                    else
                                    {
                                        cashBankCardInfo.MoneyOutBound += item.TotalMoney;
                                    }
                                    break;
                            }
                        }
                    }
                }

                // gán lại ngày bắt đầu để update số dư đầu ngày và cuối ngày
                startDate = startDateBegin;
                // lấy dữ liệu đầu ngày trước đó
                var dateBefore = startDateBegin.AddDays(-1);
                if (lstBankCardID != null && lstBankCardID.Count > 0)
                {
                    _cashBankCardByDateTab.WhereClause(x => lstBankCardID.Contains(x.BankCardID));
                    _settingDateBankCardTab.WhereClause(x => lstBankCardID.Contains(x.BankCardID));
                }
                var lstCashBankByDateIDs = (await _cashBankCardByDateTab.SelectColumns(x => x.BankCardID)
                                                        .SetMax(x => x.ForDate)
                                                        .GroupBy(x => x.BankCardID)
                                                        .WhereClause(x => x.ForDate <= dateBefore)
                                                        .QueryAsync()).ToDictionary(x => $"{x.ForDate:yyyy-MM-dd}_{x.BankCardID}", x => x);

                foreach (var item in lstCashBankByDateIDs)
                {
                    _cashBankCardByDateTab.WhereClauseOR(x => x.ForDate == item.Value.ForDate && x.BankCardID == item.Value.BankCardID);
                }
                var dictCashBankByDateBeforeInfos = (await _cashBankCardByDateTab.SelectColumns(x => x.BankCardID, x => x.BankCardName, x => x.ForDate, x => x.MoneyEndDate)
                                                        .QueryAsync()).ToDictionary(x => $"{x.ForDate:yyyy-MM-dd}_{x.BankCardID}", x => x);

                var dictSettingDateBankCardInfos = (await _settingDateBankCardTab.QueryAsync()).ToDictionary(x => $"{x.DateApply:yyyy-MM-dd}_{x.BankCardID}", x => x);
                // lấy từ ngày gần nhất của thẻ đến ngày hiện tại
                foreach (var bc in dictBankCardInfos)
                {
                    startDate = startDateBegin;
                    var bankCardStartBefore = dictCashBankByDateBeforeInfos.Values.Where(x => x.BankCardID == bc.Key).FirstOrDefault();
                    if (bankCardStartBefore != null)
                    {
                        startDate = bankCardStartBefore.ForDate.AddDays(1);
                    }
                    while (startDate < endDate)
                    {
                        string keyDictCashBankYesterday = $"{startDate.AddDays(-1):yyyy-MM-dd}_{bc.Key}";
                        string keyDictCashBankCurrent = $"{startDate:yyyy-MM-dd}_{bc.Key}";
                        var bankCardDetailCurrent = dictCashBankByDateInfos.GetValueOrDefault(keyDictCashBankCurrent);
                        var bankCardDetailBefore = dictCashBankByDateInfos.GetValueOrDefault(keyDictCashBankYesterday);
                        var settingDateBankCardExist = dictSettingDateBankCardInfos.GetValueOrDefault(keyDictCashBankCurrent);
                        if (bankCardDetailBefore == null)
                        {
                            bankCardDetailBefore = bankCardStartBefore;
                        }
                        // ngày T + 1 không có dữ liệu lấy ngày T làm đầu ngày
                        if (bankCardDetailCurrent == null)
                        {
                            bankCardDetailCurrent = new Domain.Tables.TblCashBankCardByDate
                            {
                                BankCardID = bc.Value.BankCardID,
                                BankCardName = bc.Value.AliasName,
                                CreateDate = currentDate,
                                ForDate = startDate,
                                MoneyBeginDate = bankCardDetailBefore?.MoneyEndDate ?? 0
                            };
                            // giữ giá trị ngày T + 1 làm thành ngày T
                            dictCashBankByDateInfos.Add(keyDictCashBankCurrent, bankCardDetailCurrent);
                        }
                        else if (bankCardDetailBefore != null)
                        {
                            bankCardDetailCurrent.MoneyBeginDate = bankCardDetailBefore.MoneyEndDate;
                        }
                        // ưu tiên ngày setting tiền đầu ngày
                        if (settingDateBankCardExist != null && settingDateBankCardExist.BankCardID > 0)
                        {
                            bankCardDetailCurrent.MoneyBeginDate = settingDateBankCardExist.TotalMoney;
                        }
                        bankCardDetailCurrent.MoneyEndDate = bankCardDetailCurrent.MoneyBeginDate + bankCardDetailCurrent.MoneyInBound + bankCardDetailCurrent.MoneyOutBound;


                        startDate = startDate.AddDays(1);
                    }
                }
                if (dictCashBankByDateInfos.Values.Count > 0)
                {
                    _cashBankCardByDateTab.InsertBulk(dictCashBankByDateInfos.Values.OrderBy(x => x.BankCardID).ThenBy(x => x.ForDate).ToList());
                }
                return 1;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSummaryBankCard_Exception|transactiondate={transactionDate}|lstBankCardID={string.Join(",", lstBankCardID)}|endTransactionDate={endTransactionDate}|ex={ex.Message}-{ex.StackTrace}");
            }
            return 0;
        }

        public async Task<long> ProcessSummaryBankCardFromInvoiceID(long invoiceID = 0)
        {
            try
            {
                long startID = 0;
                Dictionary<long, long> dictBankCardID = new Dictionary<long, long>();
                DateTime? transactionDate = null;
                var currentDate = DateTime.Now;
                var lstInvoices = (await _invoiceTab.SetGetTop(TimaSettingConstant.MaxItemQuery)
                                              .WhereClause(x => x.BankCardID > 0 && x.JobStatus == (int)Invoice_JobStatus.Unprocessed)
                                              .OrderBy(x => x.TransactionDate).QueryAsync()).ToList();
                if (lstInvoices == null || lstInvoices.Count == 0)
                {
                    return 1;
                }
                foreach (var item in lstInvoices)
                {
                    if (transactionDate == null || transactionDate.Value > item.TransactionDate)
                    {
                        transactionDate = item.TransactionDate;
                    }
                    if (!dictBankCardID.ContainsKey(item.BankCardID))
                    {
                        dictBankCardID.Add(item.BankCardID, item.BankCardID);
                    }
                    item.JobStatus = (int)Invoice_JobStatus.Processed;
                    item.ModifyDate = currentDate;
                    if (item.InvoiceID > startID)
                    {
                        startID = item.InvoiceID;
                    }
                }
                _invoiceTab.UpdateBulk(lstInvoices);
                if (transactionDate.HasValue)
                {
                    await ProcessSummaryBankCard(transactionDate.Value, dictBankCardID.Values.ToList());
                }
                return startID;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSummaryBankCardFromInvoiceID|invoiceID={invoiceID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return 0;
        }
    }
}
