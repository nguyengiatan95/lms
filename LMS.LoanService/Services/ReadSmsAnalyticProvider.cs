﻿using LMS.Entites.Dtos.Sms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Services
{
    public interface IReadSmsAnalyticProvider
    {
        string GetSender { get; }
        SmsAnalyticModel GetSmsAnalyticModel(Domain.Tables.TblSmsAnalytics content);
    }
    public class AgriBankProvider : IReadSmsAnalyticProvider
    {
        private const string regexAgribank = "Agribank: (.*?) TK (.*?): \\+(.*?)VND \\((.*?)\\). SD: (.*?)VND.";// $1: thời gian ps, $2: stk, $3: tiền phát sinh, $4: nội dung, $5: số dư
        public string GetSender => LMS.Common.Constants.SmsSenderName.SmsSenderAgriBank;
        LMS.Common.Helper.Utils _common = new LMS.Common.Helper.Utils();
        public SmsAnalyticModel GetSmsAnalyticModel(Domain.Tables.TblSmsAnalytics content)
        {
            SmsAnalyticModel result = new SmsAnalyticModel();
            String[] splitString = Regex.Split(content.SmsContent, regexAgribank);
            if (content.SmsContent.Contains(@" +"))
            {
                result.HasSign = 1;
            }
            else if (content.SmsContent.Contains(@" -"))
            {
                result.HasSign = -1;
            }
            try
            {
                //bóc số tài khoản
                result.AccountNumber = splitString[2];
            }
            catch
            {
            }
            try
            {
                //Bóc số tiền phát sinh 
                long increaseMoney;
                if (long.TryParse(splitString[3].Replace(",", ""), out increaseMoney))
                {
                    result.IncreaseMoney = increaseMoney;
                }
            }
            catch
            {
            }
            try
            {
                //Bóc thời gian phát sinh  
                var smsreceivedDate = splitString[1].Replace("h", ":").Replace("p", "") + "/" + DateTime.Now.Year;
                DateTime oDate = DateTime.ParseExact(smsreceivedDate, "HH:mm dd/MM/yyyy", null);
                result.SmsreceivedDate = oDate;
            }
            catch
            {
            }
            try
            {
                // bóc số đt và số CMT
                if (!string.IsNullOrEmpty(splitString[4]))
                {
                    var phoneAndIdCard = _common.GetPhoneAndIdCard(splitString[4]);
                    result.Phone = phoneAndIdCard["Phone"].ToString();
                    result.IdCard = phoneAndIdCard["IdCard"].ToString();
                }
            }
            catch (Exception)
            {
            }
            return result;
        }
    }

    public class BidvBankProvider : IReadSmsAnalyticProvider
    {
        private const string regexBIDVtkxxx = "TK(.*?) tai BIDV \\+(.*?)VND vao (.*?)\\. So du:(.*?)VND\\. ND:(.*?)$";// $1: STK, $2: tiền phát sinh, $3: ngày giờ phát sinh, $4: số dư, $5: nội dung
        private const string regexBIDV = "TK(.*?) tai BIDV \\+(.*?)VND vao (.*?);So du:(.*?)VND;ND:(.*?)$";// $1: STK, $2: tiền phát sinh, $3: ngày giờ phát sinh, $4: số dư, $5: nội dung
        public string GetSender => LMS.Common.Constants.SmsSenderName.SmsSenderBIDV;
        LMS.Common.Helper.Utils _common = new LMS.Common.Helper.Utils();
        public SmsAnalyticModel GetSmsAnalyticModel(Domain.Tables.TblSmsAnalytics content)
        {
            SmsAnalyticModel result = new SmsAnalyticModel();
            String[] splitString = Regex.Split(content.SmsContent, regexBIDVtkxxx);
            if (content.SmsContent.Contains(@" +"))
            {
                result.HasSign = 1;
            }
            else if (content.SmsContent.Contains(@" -"))
            {
                result.HasSign = -1;
            }
            if (splitString.Length == 1)
            {
                splitString = Regex.Split(content.SmsContent, regexBIDV);
            }
            try
            {
                //bóc số tài khoản
                result.AccountNumber = splitString[1];
            }
            catch
            {
            }
            try
            {
                //Bóc số tiền phát sinh 
                long increaseMoney;
                if (long.TryParse(splitString[2].Replace(",", ""), out increaseMoney))
                {
                    result.IncreaseMoney = increaseMoney;
                }
            }
            catch
            {
            }
            try
            {
                //Bóc thời gian phát sinh  
                DateTime oDate = DateTime.ParseExact(splitString[3], "HH:mm dd/MM/yyyy", null);
                result.SmsreceivedDate = oDate;
            }
            catch
            {
            }
            try
            {
                // bóc số đt và số CMT
                if (!string.IsNullOrEmpty(splitString[5]))
                {
                    var phoneAndIdCard = _common.GetPhoneAndIdCard(splitString[5]);
                    result.Phone = phoneAndIdCard["Phone"].ToString();
                    result.IdCard = phoneAndIdCard["IdCard"].ToString();
                }
            }
            catch (Exception)
            {
            }
            return result;
        }
    }

    public class VietcomBankProvider : IReadSmsAnalyticProvider
    {
        private const string regexVietcombank = "SD TK (.*?) \\+(.*?)VND luc (.*?). SD (.*?)VND. Ref (.*?)$";// $1: STK, $2: tiền phát sinh, $3: ngày giờ phát sinh, $4: số dư, $5: nội dung
        public string GetSender => LMS.Common.Constants.SmsSenderName.SmsSenderVietComBank;
        LMS.Common.Helper.Utils _common = new LMS.Common.Helper.Utils();
        public SmsAnalyticModel GetSmsAnalyticModel(Domain.Tables.TblSmsAnalytics content)
        {
            SmsAnalyticModel result = new SmsAnalyticModel();
            String[] splitString = Regex.Split(content.SmsContent, regexVietcombank);
            if (content.SmsContent.Contains(@" +"))
            {
                result.HasSign = 1;
            }
            else if (content.SmsContent.Contains(@" -"))
            {
                result.HasSign = -1;
            }
            try
            {
                //bóc số tài khoản
                result.AccountNumber = splitString[1];
            }
            catch
            {
            }
            try
            {
                //Bóc số tiền phát sinh 
                long increaseMoney;
                if (long.TryParse(splitString[2].Replace(",", ""), out increaseMoney))
                {
                    result.IncreaseMoney = increaseMoney;
                }
            }
            catch
            {
            }
            try
            {
                //Bóc thời gian phát sinh  
                DateTime oDate = DateTime.ParseExact(splitString[3], "dd-MM-yyyy HH:mm:ss", null);
                result.SmsreceivedDate = oDate;
            }
            catch
            {
            }
            try
            {
                // bóc số đt và số CMT
                if (!string.IsNullOrEmpty(splitString[5]))
                {
                    var phoneAndIdCard = _common.GetPhoneAndIdCard(splitString[5]);
                    result.Phone = phoneAndIdCard["Phone"].ToString();
                    result.IdCard = phoneAndIdCard["IdCard"].ToString();
                }
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
    public class VibBankProvider : IReadSmsAnalyticProvider
    {
        private const string regexVIB = "(.*?) TK:(.*?)VND PS:\\+(.*?) ND:(.*?) SODU:\\+(.*?)$";// $1: thời gian ps, $2: stk, $3: tiền phát sinh, $4: nội dung, $5: số dư
        public string GetSender => LMS.Common.Constants.SmsSenderName.SmsSenderVIB;
        LMS.Common.Helper.Utils _common = new LMS.Common.Helper.Utils();
        public SmsAnalyticModel GetSmsAnalyticModel(Domain.Tables.TblSmsAnalytics content)
        {
            SmsAnalyticModel result = new SmsAnalyticModel();
            String[] splitString = Regex.Split(content.SmsContent, regexVIB);
            if (content.SmsContent.Contains(@" +"))
            {
                result.HasSign = 1;
            }
            else if (content.SmsContent.Contains(@" -"))
            {
                result.HasSign = -1;
            }
            try
            {
                //bóc số tài khoản
                result.AccountNumber = splitString[2];
            }
            catch
            {
            }
            try
            {
                //Bóc số tiền phát sinh 
                long increaseMoney;
                if (long.TryParse(splitString[3].Replace(",", ""), out increaseMoney))
                {
                    result.IncreaseMoney = increaseMoney;
                }
            }
            catch
            {
            }
            try
            {
                //Bóc thời gian phát sinh  
                var smsreceivedDate = splitString[1].Replace(";", " ");
                DateTime oDate = DateTime.ParseExact(smsreceivedDate, "HH:mm dd/MM/yyyy", null);
                result.SmsreceivedDate = oDate;
            }
            catch
            {
            }
            try
            {
                // bóc số đt và số CMT
                if (!string.IsNullOrEmpty(splitString[4]))
                {
                    var phoneAndIdCard = _common.GetPhoneAndIdCard(splitString[4]);
                    result.Phone = phoneAndIdCard["Phone"].ToString();
                    result.IdCard = phoneAndIdCard["IdCard"].ToString();
                }
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
    public class AcbBankProvider : IReadSmsAnalyticProvider
    {
        private const string regexACB = "ACB: TK (.*?)\\(VND\\) \\+ (.*?) luc (.*?). So du (.*?). GD: (.*?)$";// $1: STK, $2: tiền phát sinh, $3: ngày giờ phát sinh, $4: số dư, $5: nội dung 
        public string GetSender => LMS.Common.Constants.SmsSenderName.SmsSenderACB;
        LMS.Common.Helper.Utils _common = new LMS.Common.Helper.Utils();
        public SmsAnalyticModel GetSmsAnalyticModel(Domain.Tables.TblSmsAnalytics content)
        {
            SmsAnalyticModel result = new SmsAnalyticModel();
            // $1: STK, $2: tiền phát sinh, $3: ngày giờ phát sinh, $4: số dư, $5: nội dung 
            String[] splitString = Regex.Split(content.SmsContent, regexACB);
            if (content.SmsContent.Contains(@" +"))
            {
                result.HasSign = 1;
            }
            else if (content.SmsContent.Contains(@" -"))
            {
                result.HasSign = -1;
            }
            try
            {
                //bóc số tài khoản
                result.AccountNumber = splitString[1];
            }
            catch
            {
            }
            try
            {
                //Bóc số tiền phát sinh 
                long increaseMoney;
                if (long.TryParse(splitString[2].Replace(",", ""), out increaseMoney))
                {
                    result.IncreaseMoney = increaseMoney;
                }
            }
            catch
            {
            }
            try
            {
                //Bóc thời gian phát sinh  
                var smsreceivedDate = splitString[3];
                DateTime oDate = DateTime.ParseExact(smsreceivedDate, "HH:mm dd/MM/yyyy", null);
                result.SmsreceivedDate = oDate;
            }
            catch
            {
            }
            try
            {
                // bóc số đt và số CMT
                if (!string.IsNullOrEmpty(splitString[5]))
                {
                    var phoneAndIdCard = _common.GetPhoneAndIdCard(splitString[5]);
                    result.Phone = phoneAndIdCard["Phone"].ToString();
                    result.IdCard = phoneAndIdCard["IdCard"].ToString();
                }
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
    public class VpBankProvider : IReadSmsAnalyticProvider
    {
        private const string regexVPBank = "TK (.*?) tai VPB \\+(.*?)VND luc (.*?). So du (.*?)VND. ND: (.*?)$";// $1: STK, $2: tiền phát sinh, $3: nội dung, $4: số dư
        public string GetSender => LMS.Common.Constants.SmsSenderName.SmsSenderVPBank;
        LMS.Common.Helper.Utils _common = new LMS.Common.Helper.Utils();
        public SmsAnalyticModel GetSmsAnalyticModel(Domain.Tables.TblSmsAnalytics content)
        {
            SmsAnalyticModel result = new SmsAnalyticModel();
            // $1: STK, $2: tiền phát sinh, $3: nội dung, $4: số dư
            String[] splitString = Regex.Split(content.SmsContent, regexVPBank);
            if (content.SmsContent.Contains(@" +"))
            {
                result.HasSign = 1;
            }
            else if (content.SmsContent.Contains(@" -"))
            {
                result.HasSign = -1;
            }
            try
            { //bóc số tài khoản
                result.AccountNumber = splitString[1];
            }
            catch
            {
            }
            try
            {
                //Bóc số tiền phát sinh 
                long increaseMoney;
                if (long.TryParse(splitString[2].Replace(",", ""), out increaseMoney))
                {
                    result.IncreaseMoney = increaseMoney;
                }
            }
            catch
            {
            }
            try
            {
                // bóc số đt và số CMT
                if (!string.IsNullOrEmpty(splitString[3]))
                {
                    var phoneAndIdCard = _common.GetPhoneAndIdCard(splitString[3]);
                    result.Phone = phoneAndIdCard["Phone"].ToString();
                    result.IdCard = phoneAndIdCard["IdCard"].ToString();
                }
            }
            catch (Exception)
            {
            }
            return result;
        }
    }

    public class MomoSmsProvider : IReadSmsAnalyticProvider
    {
        public string GetSender => LMS.Common.Constants.SmsSenderName.SmsSenderMOMO;
        LMS.Common.Helper.Utils _common = new LMS.Common.Helper.Utils();
        public SmsAnalyticModel GetSmsAnalyticModel(Domain.Tables.TblSmsAnalytics content)
        {
            SmsAnalyticModel result = new SmsAnalyticModel
            {
                HasSign = 1,
                IncreaseMoney = content.IncreaseMoney ?? 0,
                IdCard = content.IdCard,
                Phone = content.Phone,
                SmsreceivedDate = content.SmsreceivedDate ?? content.CreateDate,
                BankCardID = content.BankCardID == 0 ? SmsAnalyticsManager.BankCardID_MOMO : content.BankCardID,
                CustomerID = content.CustomerID
            };
            return result;
        }
    }
    public class GPAYSmsProvider : IReadSmsAnalyticProvider
    {
        public string GetSender => LMS.Common.Constants.SmsSenderName.SmsSenderGPAY;
        LMS.Common.Helper.Utils _common = new LMS.Common.Helper.Utils();
        public SmsAnalyticModel GetSmsAnalyticModel(Domain.Tables.TblSmsAnalytics content)
        {
            SmsAnalyticModel result = new SmsAnalyticModel
            {
                HasSign = 1,
                IncreaseMoney = content.IncreaseMoney ?? 0,
                IdCard = content.IdCard,
                Phone = content.Phone,
                SmsreceivedDate = content.SmsreceivedDate ?? content.CreateDate,
                BankCardID = content.BankCardID == 0 ? SmsAnalyticsManager.BankCardID_GPAY : content.BankCardID,
                CustomerID = content.CustomerID
            };
            return result;
        }
    }
    public class GPayVASmsProvider : IReadSmsAnalyticProvider
    {
        public string GetSender => LMS.Common.Constants.SmsSenderName.SmsSenderGPay_VA;
        LMS.Common.Helper.Utils _common = new LMS.Common.Helper.Utils();
        public SmsAnalyticModel GetSmsAnalyticModel(Domain.Tables.TblSmsAnalytics content)
        {
            SmsAnalyticModel result = new SmsAnalyticModel
            {
                HasSign = 1,
                IncreaseMoney = content.IncreaseMoney ?? 0,
                IdCard = content.IdCard,
                Phone = content.Phone,
                SmsreceivedDate = content.SmsreceivedDate ?? content.CreateDate,
                BankCardID = content.BankCardID == 0 ? SmsAnalyticsManager.BankCardID_GPayVA : content.BankCardID,
                CustomerID = content.CustomerID
            };
            return result;
        }
    }
    public class GUTINASmsProvider : IReadSmsAnalyticProvider
    {
        public string GetSender => LMS.Common.Constants.SmsSenderName.SmsSenderGUTINA;
        LMS.Common.Helper.Utils _common = new LMS.Common.Helper.Utils();
        public SmsAnalyticModel GetSmsAnalyticModel(Domain.Tables.TblSmsAnalytics content)
        {
            SmsAnalyticModel result = new SmsAnalyticModel
            {
                HasSign = 1,
                IncreaseMoney = content.IncreaseMoney ?? 0,
                IdCard = content.IdCard,
                Phone = content.Phone,
                SmsreceivedDate = content.SmsreceivedDate ?? content.CreateDate,
                BankCardID = content.BankCardID == 0 ? SmsAnalyticsManager.BankCardID_GUTINA : content.BankCardID,
                CustomerID = content.CustomerID
            };
            return result;
        }
    }

    public class VaSmsProvider : IReadSmsAnalyticProvider
    {
        const int MatchRegexNumber = 4;
        const string regexVA = @"(\d)+";
        //VA mã tham chiếu:TIMAVN2021070915095800898210709151710 - Số tiền: 934000 - CMT: 024472127 - Phone: 0908108641
        public string GetSender => LMS.Common.Constants.SmsSenderName.SmsSenderVA;
        LMS.Common.Helper.Utils _common = new LMS.Common.Helper.Utils();
        public SmsAnalyticModel GetSmsAnalyticModel(Domain.Tables.TblSmsAnalytics content)
        {
            var matches = Regex.Matches(content.SmsContent, regexVA);
            SmsAnalyticModel result = new SmsAnalyticModel
            {
                HasSign = 1,
                IncreaseMoney = Convert.ToInt64(matches[1].Value),
                IdCard = matches[2].Value,
                Phone = matches[3].Value,
                SmsreceivedDate = content.SmsreceivedDate ?? content.CreateDate,
                BankCardID = content.BankCardID == 0 ? SmsAnalyticsManager.BankCardID_VA : content.BankCardID,
                CustomerID = content.CustomerID
            };
            return result;
        }
    }
}
