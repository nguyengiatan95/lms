﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.LoanServices;
using LMS.LoanServiceApi.Domain.Models.Loan;
using LMS.LoanServiceApi.Domain.Tables;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Services
{
    public interface ILoanManager
    {
        Task<IEnumerable<LMS.Entites.Dtos.LoanServices.LoanItemViewModel>> GetLoanItemViewModelsByCondition(RequestLoanItemViewModel request);
        Task<LoanDetailViewModel> GetDetailViewModelByID(long loanID);
        Task<LMS.Entites.Dtos.LoanServices.MoneyNeedCloseLoan> GetMoneyCloseLoan(long loanID, DateTime closeDate);
        Task<ProcessAutoFineLateModel> ProcessFineLate(DateTime? nextDateApplyFine = null, long latestLoanID = 0);
        Task<ProcessAutoPaymentModel> ProcessAutoPayment(long latestCustomerID);

        Task<ResponseActionResult> CreateLoanAsync(RequestCreateLoanModel model);
        Task<ResponseActionResult> UpdateLoanAfterPayment(long loanID, long creatBy, long totalMoneyFineCustomerPaid = 0);
        Task<ResponseActionResult> CreateLoanExtraOriginal(LoanExtraInsertDetailModel request);

        Task<ResponseActionResult> ProcessCutOffLoanByDateDaily(long latestLoanID);
        Task<ResponseActionResult> ProcessPlanCalculatorMoneyCloseLoanDaily(long latestLoanID, DateTime closeDate);

        Task<SettingKeyValueSplitTransaction> ProcessSplitTransactionLoan(SettingKeyValueSplitTransaction settingKeyValue);
        Task<MoneyNeedCloseLoan> GetMoneyNeedPayInPeriod(DateTime currentDate, Domain.Tables.TblLoan loanInfo, List<Domain.Tables.TblPaymentSchedule> lstPaymentScheduleAll, List<Domain.Tables.TblLoanExtra> lstLoanExtra = null, Domain.Tables.TblLender lenderInfo = null);
        Task<MoneyNeedCloseLoan> GetMoneyCloseLoanV3(DateTime closeDate, Domain.Tables.TblLoan loanInfo, List<Domain.Tables.TblPaymentSchedule> lstPaymentScheduleAll, List<Domain.Tables.TblLoanExtra> lstLoanExtra = null, Domain.Tables.TblLender lenderInfo = null);
    }
    public class LoanManager : ILoanManager
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblDistrict> _dictrictTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCity> _cityTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblHoldCustomerMoney> _holdCustomerMoneyTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblProductCredit> _productCreditTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebt> _loanDebtTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.LoanUpdateAfterPayment> _loanUpdateAfterPayment;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExtra> _loanExtraTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCutOffLoan> _cutOffLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanCloseLoan> _planCloseLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblSplitTransactionLoan> _splitTransactionLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblContractLoanWithLender> _contractLoanWithLenderTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblKafkaLogEvent> _kafkaLogEventTab;
        readonly ILogger<LoanManager> _logger;
        readonly LMS.Common.Helper.Utils _common;
        readonly RestClients.ITransactionService _transactionService;
        readonly RestClients.IPaymentScheduleService _paymentScheduleService;
        readonly RestClients.ICustomerService _customerService;
        readonly RestClients.IOutGatewayService _outGateway;
        readonly RestClients.IInsuranceService _insuranceService;
        readonly RestClients.IInvoiceService _invoiceService;
        readonly RestClients.ILOSService _losService;
        readonly RestClients.ILenderService _lenderService;
        public LoanManager(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblDistrict> dictrictTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblCity> cityTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblHoldCustomerMoney> holdCustomerMoneyTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblProductCredit> productCreditTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebt> loanDebtTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.LoanUpdateAfterPayment> loanUpdateAfterPayment,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExtra> loanExtraTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionLoanTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblCutOffLoan> cutOffLoanTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanCloseLoan> planCloseLoanTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblSplitTransactionLoan> splitTransactionLoanTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblContractLoanWithLender> contractLoanWithLenderTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblKafkaLogEvent> kafkaLogEventTab,
             RestClients.ITransactionService transactionService,
             RestClients.IPaymentScheduleService paymentScheduleService,
             RestClients.ICustomerService customerService,
             RestClients.IOutGatewayService outGateway,
             RestClients.IInsuranceService insuranceService,
             RestClients.IInvoiceService invoiceService,
             RestClients.ILOSService losService,
             RestClients.ILenderService lenderService,
             ILogger<LoanManager> logger)
        {
            _loanTab = loanTab;
            _customerTab = customerTab;
            _lenderTab = lenderTab;
            _dictrictTab = dictrictTab;
            _cityTab = cityTab;
            _logger = logger;
            _common = new Utils();
            _holdCustomerMoneyTab = holdCustomerMoneyTab;
            _paymentScheduleTab = paymentScheduleTab;
            _productCreditTab = productCreditTab;
            _loanDebtTab = loanDebtTab;
            _transactionLoanTab = transactionLoanTab;
            _transactionService = transactionService;
            _paymentScheduleService = paymentScheduleService;
            _customerService = customerService;
            _outGateway = outGateway;
            _insuranceService = insuranceService;
            _invoiceService = invoiceService;
            _losService = losService;
            _lenderService = lenderService;
            _loanUpdateAfterPayment = loanUpdateAfterPayment;
            _loanExtraTab = loanExtraTab;
            _cutOffLoanTab = cutOffLoanTab;
            _planCloseLoanTab = planCloseLoanTab;
            _splitTransactionLoanTab = splitTransactionLoanTab;
            _contractLoanWithLenderTab = contractLoanWithLenderTab;
            _kafkaLogEventTab = kafkaLogEventTab;
        }

        public async Task<IEnumerable<LoanItemViewModel>> GetLoanItemViewModelsByCondition(RequestLoanItemViewModel request)
        {
            List<LoanItemViewModel> lstLoanRespone = new List<LoanItemViewModel>();
            try
            {
                if (!string.IsNullOrEmpty(request.LoanCode) || request.LoanID > 0)
                {
                    List<long> lstCustomerIDs = new List<long>();
                    if (request.LoanID > 0)
                    {
                        _loanTab.WhereClause(x => x.LoanID == request.LoanID);
                    }
                    else if (long.TryParse(request.LoanCode, out long contracCodeID) && request.LoanCode.Length < TimaSettingConstant.MaxLengthPhone - 1)
                    {
                        request.LoanCode = $"{TimaSettingConstant.PrefixContractCode}{contracCodeID}";
                        _loanTab.WhereClause(x => x.ContactCode == request.LoanCode);
                    }
                    else if (Regex.Match(request.LoanCode, TimaSettingConstant.PatternContractCode, RegexOptions.IgnoreCase).Success)
                    {
                        _loanTab.WhereClause(x => x.ContactCode == request.LoanCode);
                    }
                    else
                    {
                        if (request.LoanCode.Length == TimaSettingConstant.MaxLengthPhone && Regex.Match(request.LoanCode, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success)
                        {
                            lstCustomerIDs = _customerTab.SelectColumns(x => x.CustomerID).WhereClause(x => x.Phone == request.LoanCode).Query().Select(x => x.CustomerID).ToList();
                        }
                        else if (Regex.Match(request.LoanCode, TimaSettingConstant.PatternNumberCard, RegexOptions.IgnoreCase).Success)
                        {
                            lstCustomerIDs = _customerTab.SelectColumns(x => x.CustomerID).WhereClause(x => x.NumberCard == request.LoanCode).Query().Select(x => x.CustomerID).ToList();
                        }
                        if (lstCustomerIDs != null && lstCustomerIDs.Count > 0)
                        {
                            _loanTab.WhereClause(x => lstCustomerIDs.Contains((long)x.CustomerID));
                        }
                        else
                        {
                            _loanTab.WhereClause(x => x.CustomerName == request.LoanCode);
                        }
                    }
                }
                else
                {
                    DateTime? fromDate = null;
                    DateTime? toDate = null;
                    if (DateTime.TryParseExact(request.FromDate, LMS.Common.Helper.Utils.DateTimeDayMonthYear, null, System.Globalization.DateTimeStyles.None, out DateTime dateRequest))
                    {
                        fromDate = dateRequest;
                    }
                    if (DateTime.TryParseExact(request.ToDate, Utils.DateTimeDayMonthYear, null, System.Globalization.DateTimeStyles.None, out dateRequest))
                    {
                        toDate = dateRequest;
                    }
                    if (fromDate.HasValue)
                    {
                        fromDate = fromDate.Value.AddDays(-1);
                        _loanTab.WhereClause(x => x.FromDate > fromDate.Value);
                    }
                    if (toDate.HasValue)
                    {
                        toDate = toDate.Value.AddDays(1);
                        _loanTab.WhereClause(x => x.FromDate < toDate.Value);
                    }
                    switch ((Loan_StatusFilterRequest)request.LoanStatus)
                    {
                        case Loan_StatusFilterRequest.Closed:
                            _loanTab.WhereClause(x => x.Status == (int)Loan_Status.Close);
                            break;
                        case Loan_StatusFilterRequest.InsuranceLiquidation:
                            _loanTab.WhereClause(x => x.Status == (int)Loan_Status.CloseLiquidationLoanInsurance);
                            break;
                        case Loan_StatusFilterRequest.InsurancePayment:
                            _loanTab.WhereClause(x => x.Status == (int)Loan_Status.ClosePaidInsurance);
                            break;
                        case Loan_StatusFilterRequest.Liquidation:
                            _loanTab.WhereClause(x => x.Status == (int)Loan_Status.CloseLiquidation);
                            break;
                        case Loan_StatusFilterRequest.OverdueLoanExpected30:
                            var nextDateOverdueLoanExpected30 = DateTime.Now.AddDays(-30);
                            _loanTab.WhereClause(x => x.NextDate < nextDateOverdueLoanExpected30 && x.Status == (int)Loan_Status.Lending);
                            break;
                        case Loan_StatusFilterRequest.OverdueLoanExpected90:
                            var nextDateOverdueLoanExpected90 = DateTime.Now.AddDays(-90);
                            _loanTab.WhereClause(x => x.NextDate < nextDateOverdueLoanExpected90 && x.Status == (int)Loan_Status.Lending);
                            break;
                        case Loan_StatusFilterRequest.Delete:
                            _loanTab.WhereClause(x => x.Status == (int)Loan_Status.Delete);
                            break;
                        default:
                            _loanTab.WhereClause(x => x.Status == (int)Loan_Status.Lending);
                            break;
                    }
                    if (request.ShopID != (int)StatusCommon.SearchAll)
                    {
                        _loanTab.WhereClause(x => x.OwnerShopID == request.ShopID);
                    }
                    if (request.LenderID != (int)StatusCommon.SearchAll)
                    {
                        _loanTab.WhereClause(x => x.LenderID == request.LenderID);
                    }
                    if (request.ConsultantShopID != (int)StatusCommon.SearchAll)
                    {
                        _loanTab.WhereClause(x => x.ConsultantShopID == request.ConsultantShopID);
                    }
                    if (request.ProductID != (int)StatusCommon.SearchAll)
                    {
                        _loanTab.WhereClause(x => x.ProductID == request.ProductID);
                    }
                    if (request.LstCustomerID != null && request.LstCustomerID.Count > 0)
                    {
                        _loanTab.WhereClause(x => request.LstCustomerID.Contains((long)x.CustomerID));
                    }
                }

                var lstLoanData = (await _loanTab.OrderByDescending(x => x.LoanID).QueryAsync()).ToList();
                request.TotalRow = lstLoanData.Count;
                var lstLoanInfo = lstLoanData.Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                List<long> lstShopID = new List<long>();
                List<long> cityIDs = new List<long>();
                List<long> districtIDs = new List<long>();
                List<long> customerIDs = new List<long>();
                List<int> lstRateTypeUnitTimeDate = new List<int> { (int)PaymentSchedule_ActionRateType.RateDay, (int)PaymentSchedule_ActionRateType.RateAmortization };
                foreach (var item in lstLoanInfo)
                {
                    lstShopID.Add(item.OwnerShopID);
                    lstShopID.Add(item.ConsultantShopID);
                    lstShopID.Add(item.LenderID);
                    cityIDs.Add(item.CityID);
                    districtIDs.Add(item.DistrictID);
                    customerIDs.Add(item.CustomerID ?? 0);
                }

                var dictShopInfosTask = _lenderTab.WhereClause(x => lstShopID.Contains(x.LenderID)).QueryAsync();
                var dictCityInfosTask = _cityTab.WhereClause(x => cityIDs.Contains(x.CityID)).QueryAsync();
                var dictDistrictInfosTask = _dictrictTab.WhereClause(x => districtIDs.Contains(x.DistrictID)).QueryAsync();
                var dictCustomerInfosTask = _customerTab.WhereClause(x => customerIDs.Contains(x.CustomerID)).QueryAsync();
                var dictHoldCustomerMoneyTask = _holdCustomerMoneyTab.WhereClause(x => customerIDs.Contains(x.CustomerID) && x.Status == 1).QueryAsync();

                await Task.WhenAll(dictShopInfosTask, dictCityInfosTask, dictDistrictInfosTask, dictCustomerInfosTask, dictHoldCustomerMoneyTask);

                var dictShopInfos = dictShopInfosTask.Result.ToDictionary(x => x.LenderID, x => x);
                var dictCityInfos = dictCityInfosTask.Result.ToDictionary(x => x.CityID, x => x);
                var dictDistrictInfos = dictDistrictInfosTask.Result.ToDictionary(x => x.DistrictID, x => x);
                var dictCustomerInfos = dictCustomerInfosTask.Result.ToDictionary(x => x.CustomerID, x => x);
                var dictHoldCustomerMoney = dictHoldCustomerMoneyTask.Result.ToDictionary(x => x.CustomerID, x => x);

                foreach (var item in lstLoanInfo)
                {
                    try
                    {
                        var consultantShop = dictShopInfos.GetValueOrDefault(item.ConsultantShopID);
                        var ownerShopInfo = dictShopInfos.GetValueOrDefault(item.OwnerShopID);
                        var lenderInfo = dictShopInfos.GetValueOrDefault(item.LenderID);
                        var cityInfo = dictCityInfos.GetValueOrDefault(item.CityID);
                        var districtInfo = dictDistrictInfos.GetValueOrDefault(item.DistrictID);
                        var customerInfo = dictCustomerInfos.GetValueOrDefault((long)item.CustomerID);
                        var customerWithHeldMoneyInfo = dictHoldCustomerMoney.GetValueOrDefault((long)item.CustomerID);
                        lstLoanRespone.Add(new LoanItemViewModel
                        {
                            CityName = cityInfo?.Name,
                            ContactCode = item.ContactCode,
                            CountDownNextdate = Convert.ToInt32(DateTime.Now.Date.Subtract(item.NextDate.Value.Date).TotalDays),
                            CustomerID = item.CustomerID ?? 0,
                            CustomerName = item.CustomerName,
                            CustomerNumberCard = customerInfo?.NumberCard,
                            CustomerTotalMoney = customerInfo?.TotalMoney ?? 0,
                            CustomerWithHeldAmount = customerWithHeldMoneyInfo?.Amount ?? 0,
                            CustomerWithHeldReason = customerWithHeldMoneyInfo?.Reason,
                            DistrictName = districtInfo?.Name,
                            LenderCode = lenderInfo?.FullName,
                            LoanCreditIDOfPartner = item.LoanCreditIDOfPartner ?? 0,
                            LenderID = item.LenderID,
                            ConsultantShopID = item.ConsultantShopID,
                            LoanID = item.LoanID,
                            LoanInterestPaymentNextDate = item.NextDate.Value,
                            LoanInterestStartDate = item.FromDate,
                            ContactEndDate = item.ToDate,
                            ContactStartDate = item.FromDate,
                            LoanStatusID = (int)item.Status,
                            LoanTime = item.LoanTime,
                            LoanTotalMoneyCurrent = item.TotalMoneyCurrent,
                            LoanTotalMoneyDisbursement = item.TotalMoneyDisbursement,
                            LoanTotalMoneyPaid = 0,
                            OwnerShopID = item.OwnerShopID,
                            OwnerShopName = ownerShopInfo?.FullName,
                            ProductName = item.ProductName,
                            LoanStatusName = ((Loan_Status)item.Status).GetDescription(),
                            UnitTimeName = _common.GetUnitTimeName(item.RateType),
                            TimaLoanID = item.TimaLoanID
                        });
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"GetLoanItemViewModelsByCondition_Item|Item={_common.ConvertObjectToJSonV2(item)}|ex={ex.Message}-{ex.StackTrace}");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLoanItemViewModelsByCondition|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return lstLoanRespone;
        }

        public async Task<LoanDetailViewModel> GetDetailViewModelByID(long loanID)
        {
            try
            {
                var loanInfo = (await _loanTab.WhereClause(x => x.LoanID == loanID).QueryAsync()).FirstOrDefault();
                if (loanInfo != null && loanInfo.LoanID > 0)
                {
                    LoanDetailViewModel response = new LoanDetailViewModel();
                    List<long> lstLenderID = new List<long> { loanInfo.OwnerShopID, loanInfo.ConsultantShopID, loanInfo.LenderID };
                    var dictShopInfosTask = _lenderTab.WhereClause(x => lstLenderID.Contains(x.LenderID)).QueryAsync();
                    var dictCityInfosTask = _cityTab.GetByIDAsync(loanInfo.CityID);
                    var dictDistrictInfosTask = _dictrictTab.GetByIDAsync(loanInfo.DistrictID);
                    var dictCustomerInfosTask = _customerTab.WhereClause(x => x.CustomerID == (long)loanInfo.CustomerID).QueryAsync();
                    await Task.WhenAll(dictShopInfosTask, dictCityInfosTask, dictDistrictInfosTask, dictCustomerInfosTask);
                    var dictShopInfos = dictShopInfosTask.Result.ToDictionary(x => x.LenderID, x => x);
                    var dictCityInfos = dictCityInfosTask.Result;
                    var dictDistrictInfos = dictDistrictInfosTask.Result;
                    var dictCustomerInfos = dictCustomerInfosTask.Result.FirstOrDefault();

                    long moneyFineLate = loanInfo.MoneyFineLate;
                    response.LoanID = loanInfo.LoanID;
                    response.ContactCode = loanInfo.ContactCode;
                    response.CountDay = Convert.ToInt32(DateTime.Now.Subtract(loanInfo.NextDate.Value).TotalDays);
                    response.CustomerBirthDay = dictCustomerInfos.BirthDay?.ToString(Utils.DateTimeDayMonthYear) ?? "";
                    response.CustomerGender = dictCustomerInfos.Gender ?? 0;
                    response.CustomerID = dictCustomerInfos.CustomerID;
                    response.CustomerName = dictCustomerInfos.FullName;
                    response.CustomerNumberCard = dictCustomerInfos.NumberCard;
                    response.CustomerTotalMoney = dictCustomerInfos.TotalMoney ?? 0;
                    response.FromDate = loanInfo.FromDate;
                    response.ToDate = loanInfo.ToDate;
                    response.LinkGCNChoVay = loanInfo.LinkGCNChoVay;
                    response.LinkGCNVay = loanInfo.LinkGCNVay;
                    response.LoanCreditIDOfPartner = loanInfo.LoanCreditIDOfPartner ?? 0;
                    response.OtherMoney = moneyFineLate;
                    response.ProductName = loanInfo.ProductName;
                    response.Status = (int)loanInfo.Status;
                    response.TotalMoneyCurrent = loanInfo.TotalMoneyCurrent;
                    response.TotalMoneyDisbursement = loanInfo.TotalMoneyDisbursement;
                    response.LenderID = loanInfo.LenderID;
                    response.LenderName = dictShopInfos.GetValueOrDefault(loanInfo.LenderID)?.FullName;
                    response.OwnerShopID = loanInfo.OwnerShopID;
                    response.OwnerShopName = dictShopInfos.GetValueOrDefault(loanInfo.OwnerShopID)?.FullName;
                    response.ConsultantShopID = loanInfo.ConsultantShopID;
                    response.ConsultantShopName = dictShopInfos.GetValueOrDefault(loanInfo.ConsultantShopID)?.FullName;
                    response.CityID = dictCityInfos.CityID;
                    response.CityName = dictCityInfos.Name;
                    response.CreateDate = loanInfo.CreateDate;
                    response.ModifyDate = loanInfo.ModifyDate;
                    response.MoneyFeeInsuranceOfCustomer = loanInfo.MoneyFeeInsuranceOfCustomer;
                    response.MoneyFeeInsuranceOfLender = loanInfo.MoneyFeeInsuranceOfLender;
                    response.MoneyFineInterest = loanInfo.MoneyFineLate;
                    response.NextDate = loanInfo.NextDate ?? loanInfo.FromDate;
                    response.LastDateOfPay = loanInfo.LastDateOfPay ?? loanInfo.FromDate;
                    response.LoanTime = loanInfo.LoanTime;
                    response.Frequency = loanInfo.Frequency;
                    response.RateConsultant = Convert.ToInt64(loanInfo.RateConsultant);
                    response.RateInterest = Convert.ToInt64(loanInfo.RateInterest);
                    response.RateService = Convert.ToInt64(loanInfo.RateService);
                    response.RateType = loanInfo.RateType;
                    response.RateTypeName = ((PaymentSchedule_ActionRateType)loanInfo.RateType).GetDescription();
                    response.SourcecBuyInsurance = loanInfo.SourcecBuyInsurance ?? (int)Loan_SourcecBuyInsurance.NO;
                    response.SourcecBuyInsuranceName = ((Loan_SourcecBuyInsurance)response.SourcecBuyInsurance).GetDescription();
                    response.SourceMoneyDisbursement = loanInfo.SourceMoneyDisbursement;
                    response.SourceMoneyDisbursementName = ((Loan_SourceMoneyDisbursement)loanInfo.SourceMoneyDisbursement).GetDescription();
                    response.UnitTimeName = _common.GetUnitTimeName(loanInfo.RateType);
                    return response;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetDetailViewModelByID|LoanID={loanID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }

        public async Task<MoneyNeedCloseLoan> GetMoneyCloseLoan(long loanID, DateTime closeDate)
        {
            return await GetMoneyCloseLoanV2(loanID, closeDate);
            #region oldcode
            //try
            //{
            //    var lstLoanInfos = await _loanTab.WhereClause(x => x.LoanID == loanID).QueryAsync();
            //    if (lstLoanInfos == null || !lstLoanInfos.Any())
            //    {
            //        return null;
            //    }
            //    var loanDetail = lstLoanInfos.First();
            //    LMS.Entites.Dtos.LoanServices.MoneyNeedCloseLoan moneyNeedCloseLoan = new Entites.Dtos.LoanServices.MoneyNeedCloseLoan
            //    {
            //        CloseType = (int)MoneyCloseLoanType.Schedule,
            //        MoneyOriginal = loanDetail.TotalMoneyCurrent,
            //        MoneyFineLate = loanDetail.MoneyFineLate
            //    };
            //    DateTime lastDateOfPay = loanDetail.FromDate;
            //    if (loanDetail.LastDateOfPay.HasValue)
            //    {
            //        // ngày lastdate đã được tính tiền, lấy ngày tiếp theo tính lãi
            //        lastDateOfPay = loanDetail.LastDateOfPay.Value.AddDays(1);
            //    }
            //    if (loanDetail.ToDate.Date > closeDate.Date)
            //    {
            //        moneyNeedCloseLoan.CloseType = (int)MoneyCloseLoanType.FineOriginal;
            //        moneyNeedCloseLoan.OverDue = false;
            //        moneyNeedCloseLoan.MoneyFineOriginal = Convert.ToInt64(loanDetail.TotalMoneyCurrent * (long)SettingFineForProduct.PercentPrePaymentPenaltySmall / 100);
            //        var productCreditInfos = _productCreditTab.WhereClause(x => x.ProductCreditID == loanDetail.ProductID).Query();
            //        if (productCreditInfos == null || !productCreditInfos.Any())
            //        {
            //            _logger.LogWarning($"GetMoneyNeedCloseLoanQuery_NotFoundProductCredit|ProductCreditID={loanDetail.ProductID}");
            //        }
            //        else
            //        {
            //            var productCreditDetail = productCreditInfos.First();
            //            if (productCreditDetail.TypeProduct == (int)Product_Type.Big)
            //            {
            //                moneyNeedCloseLoan.MoneyFineOriginal = Convert.ToInt64(loanDetail.TotalMoneyCurrent * (long)SettingFineForProduct.PercentPrePaymentPenaltyBig / 100);
            //            }
            //        }
            //    }
            //    if (loanDetail.ToDate.Date < closeDate.Date)
            //    {
            //        moneyNeedCloseLoan.OverDue = true;
            //    }
            //    moneyNeedCloseLoan.OverDate = Convert.ToInt32(closeDate.Date.Subtract(loanDetail.ToDate.Date).TotalDays);
            //    moneyNeedCloseLoan.TotalDaysInterest = Convert.ToInt32(closeDate.Date.Subtract(lastDateOfPay.Date).TotalDays + 1);
            //    decimal ratePerDay = loanDetail.RateConsultant + loanDetail.RateInterest + loanDetail.RateService;
            //    moneyNeedCloseLoan.TotalInterest = _calculatorInterestManager.Calculator(loanDetail.TotalMoneyCurrent, lastDateOfPay, closeDate, ratePerDay);
            //    moneyNeedCloseLoan.MoneyInterest = _calculatorInterestManager.Calculator(moneyNeedCloseLoan.TotalInterest, ratePerDay, loanDetail.RateInterest);
            //    moneyNeedCloseLoan.MoneyService = _calculatorInterestManager.Calculator(moneyNeedCloseLoan.TotalInterest, ratePerDay, loanDetail.RateService);
            //    moneyNeedCloseLoan.MoneyConsultant = _calculatorInterestManager.Calculator(moneyNeedCloseLoan.TotalInterest, ratePerDay, loanDetail.RateConsultant);

            //    // lấy các kỳ còn thiếu trước ngày đóng tiền gần nhất
            //    var lstPaymentSchedule = _paymentScheduleTab.WhereClause(x => x.LoanID == loanID && x.Status == (int)PaymentSchedule_Status.Use
            //                                                                && x.IsComplete == (int)PaymentSchedule_IsComplete.Waiting && x.PayDate <= lastDateOfPay).Query();
            //    if (lstPaymentSchedule != null)
            //    {
            //        foreach (var item in lstPaymentSchedule)
            //        {
            //            moneyNeedCloseLoan.MoneyInterest += item.MoneyInterest - item.PayMoneyInterest;
            //            moneyNeedCloseLoan.MoneyService += item.MoneyService - item.PayMoneyService;
            //            moneyNeedCloseLoan.MoneyConsultant += item.MoneyConsultant - item.PayMoneyConsultant;
            //        }
            //    }
            //    moneyNeedCloseLoan.TotalMoneyCloseLoan = moneyNeedCloseLoan.MoneyOriginal + moneyNeedCloseLoan.MoneyInterest + moneyNeedCloseLoan.MoneyFineLate + moneyNeedCloseLoan.MoneyService + moneyNeedCloseLoan.MoneyConsultant + moneyNeedCloseLoan.MoneyFineOriginal;

            //    return moneyNeedCloseLoan;
            //}
            //catch (Exception ex)
            //{
            //    _logger.LogError($"GetMoneyCloseLoan_Exception|loanID={loanID}|closeDate={closeDate.ToString(TimaSettingConstant.DateTimeDayMonthYear)}|ex={ex.Message}-{ex.StackTrace}");
            //    return null;
            //}
            #endregion
        }

        public async Task<ProcessAutoFineLateModel> ProcessFineLate(DateTime? nextDateApplyFine = null, long latestLoanID = 0)
        {
            ProcessAutoFineLateModel response = new ProcessAutoFineLateModel();
            int productSimID = 20;
            var currentDate = DateTime.Now;
            // ngày sp lớn giải ngân áp dụng tính trễ 1 ngày
            DateTime dateApplyeProductBigLateOneDay = new DateTime(2020, 01, 31);
            if (!nextDateApplyFine.HasValue)
            {
                nextDateApplyFine = currentDate;
            }
            try
            {
                Dictionary<long, long> dictProductCreditID = new Dictionary<long, long>();
                List<long> lstLoanIDFine = new List<long>();
                List<LMS.Entites.Dtos.TransactionServices.RequestCreateListTransactionModel> dataPostTransaction = new List<Entites.Dtos.TransactionServices.RequestCreateListTransactionModel>();
                List<LMS.Entites.Dtos.PaymentServices.RequestPaymentChangeMoneyFineModel> dataPostPayment = new List<Entites.Dtos.PaymentServices.RequestPaymentChangeMoneyFineModel>();
                List<Domain.Tables.TblDebt> lstLoanDebitForCustomerInsert = new List<Domain.Tables.TblDebt>();
                List<Domain.Tables.TblLoan> lstLoanUpdate = new List<Domain.Tables.TblLoan>();
                var dictProductCreditInfosTask = _productCreditTab.SelectColumns(x => x.ProductCreditID, x => x.TypeProduct, x => x.ProductCreditName).QueryAsync();
                var lstLoanInfosTask = _loanTab.SetGetTop(TimaSettingConstant.MaxItemQuery)
                                                 .WhereClause(x => x.Status == (int)Loan_Status.Lending && x.NextDate < nextDateApplyFine && x.LoanID > latestLoanID)
                                                 .WhereClause(x => x.ProductID != productSimID)
                                                 .OrderBy(x => x.LoanID).QueryAsync();

                await Task.WhenAll(dictProductCreditInfosTask, lstLoanInfosTask);

                var lstLoanInfos = lstLoanInfosTask.Result;
                if (lstLoanInfos == null || !lstLoanInfos.Any())
                {
                    return response;
                }
                var dictProductCreditInfos = dictProductCreditInfosTask.Result.ToDictionary(x => (long)x.ProductCreditID, x => x);
                var dictLoanFineInfos = lstLoanInfos.OrderBy(x => x.LoanID).ToDictionary(x => x.LoanID, x => x);
                latestLoanID = dictLoanFineInfos.Last().Key;
                foreach (var item in dictLoanFineInfos.Values)
                {
                    var productInfo = dictProductCreditInfos.GetValueOrDefault((long)item.ProductID);
                    if (productInfo == null || productInfo.ProductCreditID < 1)
                    {
                        _logger.LogError($"ProcessFineLate_warning|LoanID={item.LoanID}|Not found product info");
                        continue;
                    }
                    var nextDateOfSchedule = item.NextDate.Value.AddDays((int)SettingFineForProduct.MinDayFineLateSmall);
                    // sản phẩm nhỏ thì tính từ 5 ngày
                    // sản phẩm lớn trước 31/1/2020 thì tính từ 5 ngày
                    // sản phẩm lớn từ ngày 31/1/2020 thì tính 1 ngày.
                    if (productInfo.TypeProduct == (int)Product_Type.Big && item.FromDate.Date >= dateApplyeProductBigLateOneDay)
                    {
                        nextDateOfSchedule = item.NextDate.Value;
                    }

                    // Nghiệp vụ là quá 4 ngày thì mới ghi nợ phí phạt trả chậm
                    if (nextDateOfSchedule.Date >= nextDateApplyFine.Value.Date)
                    {
                        continue;
                    }
                    lstLoanIDFine.Add(item.LoanID);
                }
                // 27-03: bổ sung kiểm tra những lịch chưa có ghi nhận phí phạt
                // 31-03: Lamvt said: còn hủy mà nó vẫn trễ kỳ thì tính lại là bình thường.
                var dictPaymentScheduleFineLate = (await _paymentScheduleTab.SelectColumns(x => x.PaymentScheduleID, x => x.PayDate, x => x.MoneyFineLate, x => x.PayMoneyFineLate, x => x.LoanID)
                                                        .SelectColumns(x => x.Fined, x => x.DateApplyFineLate)
                                                        .WhereClause(x => lstLoanIDFine.Contains(x.LoanID) && x.PayDate < nextDateApplyFine)
                                                        .WhereClause(x => x.Status == (int)PaymentSchedule_Status.Use && x.IsVisible == (int)PaymentSchedule_IsVisible.Show && x.IsComplete == (int)PaymentSchedule_IsComplete.Waiting)
                                                        //.WhereClause(x => x.Fined == null || x.Fined == (int)PaymentSchedule_Fined.NotYet)
                                                        .QueryAsync()).GroupBy(x => x.LoanID).ToDictionary(x => x.Key, x => x.ToList());

                foreach (var itemLoan in dictPaymentScheduleFineLate)
                {
                    var loanInfo = dictLoanFineInfos.GetValueOrDefault(itemLoan.Key);
                    var productInfo = dictProductCreditInfos.GetValueOrDefault((long)loanInfo.ProductID);
                    bool loanHasChange = false;
                    foreach (var payment in itemLoan.Value)
                    {
                        var nextDateOfSchedule = payment.PayDate.AddDays((int)SettingFineForProduct.MinDayFineLateSmall);
                        // sản phẩm nhỏ thì tính từ 5 ngày
                        // sản phẩm lớn trước 31/1/2020 thì tính từ 5 ngày
                        // sản phẩm lớn từ ngày 31/1/2020 thì tính 1 ngày.
                        if (productInfo != null && productInfo.TypeProduct == (int)Product_Type.Big && loanInfo.FromDate.Date >= dateApplyeProductBigLateOneDay)
                        {
                            nextDateOfSchedule = payment.PayDate;
                        }

                        // Nghiệp vụ là quá 4 ngày thì mới ghi nợ phí phạt trả chậm
                        if (nextDateOfSchedule.Date >= nextDateApplyFine.Value.Date)
                        {
                            continue;
                        }
                        //28-09: Chốt là : Những kỳ có ngày phải thanh toán > ngày đáo hạn thì không tính phí phạt trả chậm
                        if (payment.PayDate.Date > loanInfo.ToDate.Date)
                        {
                            continue;
                        }
                        var numDateLate = nextDateApplyFine.Value.Subtract(nextDateOfSchedule).Days;
                        var loanDebit = new Domain.Tables.TblDebt
                        {
                            ReferID = payment.LoanID,
                            TypeID = (int)Debt_TypeID.CustomerFineLate,
                            CreateDate = currentDate,
                            TargetID = (long)loanInfo.CustomerID,
                            Status = (int)StatusCommon.Active,
                            CreateBy = 1,
                        };
                        var txnDetail = new Entites.Dtos.TransactionServices.RequestCreateListTransactionModel
                        {
                            ActionID = (int)Transaction_Action.GhiNoPhiPhatMuon,
                            LoanID = payment.LoanID,
                            MoneyType = (int)Transaction_TypeMoney.FineLate,
                            UserID = 1,
                            TotalMoney = 0
                        };
                        if (productInfo != null && productInfo.TypeProduct == (int)Product_Type.Big)
                        {
                            long moneyFine = numDateLate * (long)SettingFineForProduct.MoneyFineLateBig;
                            if (loanInfo.FromDate.Date < dateApplyeProductBigLateOneDay)
                            {
                                moneyFine = (long)SettingFineForProduct.MoneyFineLateSmall;
                                payment.Fined = (int)PaymentSchedule_Fined.Fined; // ghi nhận đã đủ
                            }
                            else
                            {
                                if (moneyFine > (long)SettingFineForProduct.MaxMoneyFineLateBig)
                                {
                                    moneyFine = (long)SettingFineForProduct.MaxMoneyFineLateBig;
                                }
                            }
                            if (payment.MoneyFineLate < moneyFine)
                            {
                                moneyFine -= payment.MoneyFineLate;
                                loanDebit.TotalMoney = moneyFine;
                            }
                            else if (payment.MoneyFineLate > moneyFine) // do cập nhật lại cách tính với sp lớn
                            {
                                // log check dữ liệu theo dõi 1 thời gian
                                _logger.LogError($"ProcessFineLate_Warning|moneyFine={moneyFine}|paymentID={payment.PaymentScheduleID}|loanID={loanInfo.LoanID}|productName={productInfo.ProductCreditName}");

                            }
                            else
                            {
                                payment.Fined = (int)PaymentSchedule_Fined.Fined; // ghi nhận đã đủ
                            }

                        }
                        else if (payment.MoneyFineLate == 0) // sản phẩm nhỏ chưa ghi nhận phạt
                        {
                            loanDebit.TotalMoney = (long)SettingFineForProduct.MoneyFineLateSmall;
                            payment.Fined = (int)PaymentSchedule_Fined.Fined; // ghi nhận đã đủ
                        }
                        payment.MoneyFineLate += loanDebit.TotalMoney;
                        if (loanDebit.TotalMoney > 0)
                        {
                            loanDebit.Description = string.Format(TimaSettingConstant.SystemFineLateCustomer, loanDebit.TotalMoney);
                            loanHasChange = true;
                            loanInfo.MoneyFineLate += loanDebit.TotalMoney;
                            loanInfo.ModifyDate = currentDate;
                            dataPostTransaction.Add(txnDetail);
                            lstLoanDebitForCustomerInsert.Add(loanDebit);
                            dataPostPayment.Add(new Entites.Dtos.PaymentServices.RequestPaymentChangeMoneyFineModel
                            {
                                PaymentScheduleID = payment.PaymentScheduleID,
                                MoneyAdd = loanDebit.TotalMoney,
                                IsFullPeriod = payment.Fined == (int)PaymentSchedule_Fined.Fined
                            });
                        }
                    }
                    if (loanHasChange)
                    {
                        lstLoanUpdate.Add(loanInfo);
                    }
                }
                if (lstLoanUpdate.Count > 0)
                {
                    _loanTab.UpdateBulk(lstLoanUpdate);
                    _loanDebtTab.InsertBulk(lstLoanDebitForCustomerInsert);
                    response.TotalProcessSuccess = lstLoanUpdate.Count;
                    // insert transaction
                    var t1 = _transactionService.CreateTransaction(dataPostTransaction);
                    var t2 = _paymentScheduleService.WriteLstMoneyFine(dataPostPayment);
                    await Task.WhenAll(t1, t2);
                }
                response.LatestLoanID = latestLoanID;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessFineLate|latestLoanID={latestLoanID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ProcessAutoPaymentModel> ProcessAutoPayment(long latestCustomerID)
        {
            ProcessAutoPaymentModel response = new ProcessAutoPaymentModel();
            try
            {
                var currentDate = DateTime.Now;
                List<long> lstLoanIDs = new List<long>();
                List<long> lstLoanIDsCloseLoan = new List<long>();
                List<long> lstLoanIDsShopMecach = new List<long>();
                List<long> lstCustomerIDs = new List<long>();
                List<long> lstCustomerIDCutOffSuccess = new List<long>();
                var dateCurrent = DateTime.Now.Date.AddDays(1); // lấy lớn hơn ngày hiện 1 ngày
                string messagePayment = "Gạch nợ kỳ {0}";
                string messageCloseLoanCustomer = "Tất toán hợp đồng {0}";
                int totalLoanProcess = 0;
                double totalTimeQuery = 0;
                //System.Diagnostics.Stopwatch stopWatch;
                Dictionary<long, LoanPaymentAutoModel> dictTaskAction = new Dictionary<long, LoanPaymentAutoModel>();
                Dictionary<long, Domain.Tables.TblDebt> dictLoanDebtInfos = new Dictionary<long, Domain.Tables.TblDebt>();
                int retry = 0; // mỗi lần xử lý tăng lên 1
                int maxRetry = 3; // số lần xử lý tối đa
                int maxItemQuery = 1000;
                var dNow = DateTime.Now;
                do
                {
                    lstLoanIDs.Clear();
                    lstLoanIDsShopMecach.Clear();
                    lstCustomerIDCutOffSuccess.Clear();
                    dictTaskAction.Clear();
                    //stopWatch = System.Diagnostics.Stopwatch.StartNew();
                    var dictCustomerHasMoney = (await _customerTab.SetGetTop(maxItemQuery).SelectColumns(x => x.CustomerID, x => x.TotalMoney)
                                                             .WhereClause(x => x.TotalMoney > TimaSettingConstant.MinCustomerTotalMoney)
                                                             .WhereClause(x => x.CustomerID > latestCustomerID)
                                                             .QueryAsync()).OrderBy(x => x.CustomerID).ToDictionary(x => (long)x.CustomerID, x => x);
                    //totalTimeQuery = stopWatch.Elapsed.TotalSeconds;
                    //_logger.LogInformation($"ProcessAutoPayment_TimeQueryCustomer={totalTimeQuery}|count={dictCustomerHasMoney.Count}");
                    // Khách hàng đã xử lý xong
                    if (dictCustomerHasMoney == null || dictCustomerHasMoney.Count == 0)
                    {
                        response.LatestCustomerID = 0;
                        response.TotalProcessSuccess = 0;
                        return response;
                    }
                    response.LatestCustomerID = dictCustomerHasMoney.Last().Key;
                    lstCustomerIDs = dictCustomerHasMoney.Keys.ToList();

                    //stopWatch = System.Diagnostics.Stopwatch.StartNew();
                    var dictCustomerHasLoans = (await _loanTab.SelectColumns(x => x.LoanID, x => x.CustomerID, x => x.StatusSendInsurance, x => x.NextDate)
                                                        .WhereClause(x => lstCustomerIDs.Contains((long)x.CustomerID) && x.Status == (int)Loan_Status.Lending) // khách đang vay
                                                        .WhereClause(x => x.StatusSendInsurance < (int)Loan_StatusSendInsurance.Send)
                                                        .WhereClause(x => x.NextDate < dateCurrent)
                                                        //.WhereClause(x => x.ToDate > dateCurrent) // ko lấy những đơn quá hạn
                                                        .QueryAsync()).GroupBy(x => x.CustomerID).ToDictionary(x => (long)x.Key, x => x.ToList());

                    //totalTimeQuery = stopWatch.Elapsed.TotalSeconds;
                    _logger.LogInformation($"ProcessAutoPayment_TimeQueryLoan={totalTimeQuery}|count={dictCustomerHasLoans.Count}");
                    if (dictCustomerHasLoans != null && dictCustomerHasLoans.Count > 0)
                    {
                        // ds khách hàng có đơn vay thực tế
                        lstCustomerIDs = dictCustomerHasLoans.Keys.ToList();

                        // khách ko thanh toán
                        //stopWatch = System.Diagnostics.Stopwatch.StartNew();
                        var dictCustomerHoldMoney = (await _holdCustomerMoneyTab.SelectColumns(x => x.CustomerID, x => x.Amount)
                                                                        .WhereClause(x => x.Status == (int)HoldCustomerMoney_Status.Hold)
                                                                        .WhereClause(x => lstCustomerIDs.Contains(x.CustomerID))
                                                                        .QueryAsync()).ToDictionary(x => x.CustomerID, x => x);

                        //totalTimeQuery = stopWatch.Elapsed.TotalSeconds;
                        //_logger.LogInformation($"ProcessAutoPayment_TimeQueryCustomerHoldMoney={totalTimeQuery}|count={dictCustomerHoldMoney.Count}");
                        //stopWatch = System.Diagnostics.Stopwatch.StartNew();
                        foreach (var item in dictCustomerHasLoans)
                        {
                            if (item.Value.Count > 1)
                            {
                                // khách có 2 khoản vay -> không xử lý
                                var lstCustomerLoanIDs = item.Value.Select(x => (long)x.LoanID).ToList();
                                _logger.LogWarning($"ProcessAutoPayment_Pending|customerID={item.Key}|total_loans={item.Value.Count}|loanIDs={string.Join(",", lstCustomerLoanIDs)}");
                                continue;
                            }

                            if (item.Value.Count == 0)
                            {
                                _logger.LogWarning($"ProcessQueuePaymentAuto_NotFoundLoan|customerID={item.Key}");
                                continue;
                            }

                            if (dictCustomerHasMoney.ContainsKey(item.Key))
                            {
                                var loanInfo = item.Value[0];
                                var customerDetail = dictCustomerHasMoney[item.Key];
                                long totalMoneyCustomerHas = (long)customerDetail.TotalMoney;
                                // kiểm tra khách hàng có nằm trong danh sách giữ tiền ko
                                if (dictCustomerHoldMoney.ContainsKey(item.Key))
                                {
                                    if (customerDetail.TotalMoney <= dictCustomerHoldMoney[item.Key].Amount)
                                    {
                                        _logger.LogWarning($"ProcessAutoPayment_Pending_Hold|customerID={item.Key}|loanIDs={string.Join(",", item.Value.Select(x => x.LoanID).ToList())}");
                                        continue;
                                    }
                                    totalMoneyCustomerHas -= dictCustomerHoldMoney[item.Key].Amount;
                                }
                                if (totalMoneyCustomerHas > 0)
                                {
                                    var moneyNeedCloseLoan = await GetMoneyCloseLoan(loanInfo.LoanID, DateTime.Now);
                                    ResponseActionResult actionResultPayment = new ResponseActionResult();
                                    if (moneyNeedCloseLoan.TotalMoneyCloseLoan <= totalMoneyCustomerHas)
                                    {
                                        //// đóng hợp đồng
                                        var taskActionCloseLoan = _paymentScheduleService.CloseLoanFromSchedule(loanInfo.LoanID, moneyNeedCloseLoan.MoneyOriginal, moneyNeedCloseLoan.MoneyService, moneyNeedCloseLoan.MoneyInterest, moneyNeedCloseLoan.MoneyConsultant, moneyNeedCloseLoan.MoneyFineOriginal, moneyNeedCloseLoan.MoneyFineLate, DateTime.Now, 1);

                                        if (!dictTaskAction.ContainsKey(loanInfo.LoanID))
                                        {
                                            dictTaskAction.Add(loanInfo.LoanID, new LoanPaymentAutoModel
                                            {
                                                LoanID = loanInfo.LoanID,
                                                ActionResult = taskActionCloseLoan,
                                                CustomerID = (long)loanInfo.CustomerID,
                                                TypeAction = 0,
                                                ContactCode = loanInfo.ContactCode
                                            });
                                            // ghi nhận kh đã trả nợ phí phạt
                                            if (moneyNeedCloseLoan.MoneyFineLate > 0)
                                            {
                                                if (!dictLoanDebtInfos.ContainsKey(loanInfo.LoanID))
                                                {
                                                    dictLoanDebtInfos.Add(loanInfo.LoanID, new Domain.Tables.TblDebt
                                                    {
                                                        CreateBy = TimaSettingConstant.UserIDAdmin,
                                                        ReferID = loanInfo.LoanID,
                                                        CreateDate = dNow,
                                                        Description = Debt_TypeID.CustomerPayFineLate.GetDescription(),
                                                        TypeID = (int)Debt_TypeID.CustomerPayFineLate,
                                                        TargetID = (long)loanInfo.CustomerID,
                                                        Status = (int)StatusCommon.Active
                                                    });
                                                }
                                                dictLoanDebtInfos[loanInfo.LoanID].TotalMoney += (moneyNeedCloseLoan.MoneyFineLate * -1);
                                            }
                                        }
                                        else
                                        {
                                            _logger.LogError($"key trùng đóng hợp đồng loanID={loanInfo.LoanID}|customerID={loanInfo.CustomerID}");
                                        }
                                    }
                                    else
                                    {
                                        //// xử lý thanh toán theo kỳ
                                        var taskActionPayPeriods = _paymentScheduleService.PayPaymentByLoanID(loanInfo.LoanID, loanInfo.NextDate.Value, totalMoneyCustomerHas, 1, checkLastPeriods: true);
                                        if (!dictTaskAction.ContainsKey(loanInfo.LoanID))
                                        {
                                            dictTaskAction.Add(loanInfo.LoanID, new LoanPaymentAutoModel
                                            {
                                                LoanID = loanInfo.LoanID,
                                                ActionResult = taskActionPayPeriods,
                                                CustomerID = (long)loanInfo.CustomerID,
                                                TypeAction = 1,
                                                NextDate = loanInfo.NextDate.Value
                                            }); ;
                                        }
                                        else
                                        {
                                            _logger.LogError($"key trùng thanh toán kỳ loanID={loanInfo.LoanID}|customerID={loanInfo.CustomerID}");
                                        }

                                    }
                                }
                            }
                        }
                        await Task.WhenAll(dictTaskAction.Values.Select(x => x.ActionResult).ToArray());
                        foreach (var item in dictTaskAction)
                        {
                            ResponseActionResult actionResultPayment = ((Task<ResponseActionResult>)item.Value.ActionResult).Result;
                            if (actionResultPayment.Result == (int)ResponseAction.Success)
                            {
                                totalLoanProcess++;
                                long totalMoneyCustomerPaid = Convert.ToInt64(actionResultPayment.Data);
                                switch (item.Value.TypeAction)
                                {
                                    case 0: // đóng HD
                                        lstLoanIDsCloseLoan.Add(item.Value.LoanID);
                                        _ = await _customerService.UpdateMoneyCustomer((long)item.Value.CustomerID, totalMoneyCustomerPaid * -1, string.Format(messageCloseLoanCustomer, item.Value.ContactCode));
                                        break;
                                    case 1: // gạch nợ kỳ
                                        _ = await _customerService.UpdateMoneyCustomer((long)item.Value.CustomerID, totalMoneyCustomerPaid * -1, string.Format(messagePayment, item.Value.NextDate));
                                        break;
                                }
                                // todo:
                                //    // ghi nhận KH đã gạch đc nợ
                                //    lstCustomerIDCutOffSuccess.Add(customerDetail.CustomerID);
                                //    // ghi nhận cho bên THN số tiền đã trừ
                            }

                        }
                        if (lstCustomerIDCutOffSuccess.Count > 0)
                        {
                            // cập nhật các sms có khách hàng gạch nợ thành công
                            //UpdateSmsAnalyticForCustomer(lstCustomerIDCutOffSuccess);
                        }
                        //totalTimeQuery = stopWatch.Elapsed.TotalSeconds;
                        //_logger.LogInformation($"ProcessAutoPayment_TimeForeachItem={totalTimeQuery}|count={dictTaskAction.Count}|success={totalLoanProcess}|retry={retry}");

                    }
                    retry++;
                } while (totalLoanProcess == 0 && retry < maxRetry);

                if (lstLoanIDsCloseLoan.Count > 0)
                {
                    var lstLoanInfos = await _loanTab.WhereClause(x => lstLoanIDsCloseLoan.Contains(x.LoanID)).QueryAsync();
                    List<Domain.Tables.TblDebt> lstLoanDebtInsert = new List<Domain.Tables.TblDebt>();
                    foreach (var item in lstLoanInfos)
                    {
                        item.Status = (int)Loan_Status.Close;
                        item.ModifyDate = dNow;
                        item.TotalMoneyCurrent = 0;
                        if (dictLoanDebtInfos.ContainsKey(item.LoanID))
                        {
                            var itemLoanDebt = dictLoanDebtInfos[item.LoanID];
                            lstLoanDebtInsert.Add(itemLoanDebt);
                            item.MoneyFineLate -= (itemLoanDebt.TotalMoney * -1);// số tiền khách đã trả nợ phí phạt trả chậm
                        }
                    }
                    _loanTab.UpdateBulk(lstLoanInfos);
                    if (lstLoanDebtInsert.Count > 0)
                        _loanDebtTab.InsertBulk(lstLoanDebtInsert);
                }

                response.TotalProcessSuccess = totalLoanProcess;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessAutoPayment|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> CreateLoanAsync(RequestCreateLoanModel request)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var loanLmsDetailExist = (await _loanTab.WhereClause(x => x.LoanCreditIDOfPartner == request.LoanCreditID).QueryAsync()).FirstOrDefault();
                if (loanLmsDetailExist != null && loanLmsDetailExist.LoanID > 0)
                {
                    response.SetSucces();
                    response.Message = MessageConstant.LoanExist;
                    response.Data = loanLmsDetailExist.LoanID;
                    return response;
                }
                LMS.Entites.Dtos.LOSServices.ResultLoanWaitingDisbursementDetailLOS loanDetailLos = await _outGateway.GetLoanCreditDetailAsync((int)request.LoanCreditID);
                if (loanDetailLos == null || loanDetailLos.LoanBriefId < 1)
                {
                    response.Message = LMS.Common.Constants.MessageConstant.LoanNotFoundLoanCreditPartner;
                    return response;
                }
                Domain.Tables.TblLender lenderDetail = null;
                long lenderID = loanDetailLos.LenderId;
                do
                {
                    var lenderInfos = await _lenderTab.WhereClause(x => x.LenderID == lenderID).QueryAsync();
                    if (lenderInfos == null || lenderInfos.Count() < 1)
                    {
                        // đồng bộ lender
                        var actionResultSyncLender = await _lenderService.CreateOrUpdateLenderFromAG(loanDetailLos.LenderId);
                        if (actionResultSyncLender == null || actionResultSyncLender.Result != (int)ResponseAction.Success)
                        {
                            response.Message = LMS.Common.Constants.MessageConstant.LoanNotFoundLender;
                            return response;
                        }
                        else
                        {
                            lenderID = Convert.ToInt64(actionResultSyncLender.Data);
                        }
                    }
                    else
                    {
                        lenderDetail = lenderInfos.First();
                    }

                } while (lenderDetail == null);


                var lstSourceByInsuranceIDs = ((Loan_SourcecBuyInsurance[])Enum.GetValues(typeof(Loan_SourcecBuyInsurance))).Select(c => (int)c).ToList();
                if (!lstSourceByInsuranceIDs.Contains(loanDetailLos.TypeInsurence))
                {
                    // log warning
                    _logger.LogWarning($"CreateLoanCommandHandler_Warning|TypeInsurence={loanDetailLos.TypeInsurence}|detaiLos={_common.ConvertObjectToJSonV2(loanDetailLos)}");
                }

                // tạo khách hàng
                var customerID = await _customerService.CreateInfoBorrower(new Domain.Models.CreateBorrowerCommandRequest
                {
                    Address = loanDetailLos.CustomerAddress,
                    CityID = loanDetailLos.HouseOldCityId,
                    DistrictID = loanDetailLos.HouseOldDistrictId,
                    NumberCard = loanDetailLos.NationalCard,
                    CustomerName = loanDetailLos.FullName,
                    Phone = loanDetailLos.Phone,
                    Gender = loanDetailLos.Gender,
                    PermanentAddress = loanDetailLos.AddressNationalCard,
                    AddressHouseHold = loanDetailLos.HouseOldAddress,
                    BirthDay = loanDetailLos.Dob == DateTime.MinValue ? "" : loanDetailLos.Dob.ToString(TimaSettingConstant.DateTimeDayMonthYear),
                    TimaCustomerID = request.TimaCustomerID
                });
                if (customerID < 1)
                {
                    response.Message = LMS.Common.Constants.MessageConstant.LoanCreateCustomerFail;
                    return response;
                }
                var currentDate = DateTime.Now;
                var totalMoneyInterest = _common.ConvertRateToVND(loanDetailLos.RatePercent);
                long moneyInterest = 0;
                // 03-11-2021 chuyển lấy lãi suất từ los
                //switch ((Lender_UnitRate)lenderDetail.UnitRate)
                //{
                //    case Lender_UnitRate.Percent:
                //        moneyInterest = _common.ConvertRateToVND(lenderDetail.RateInterest);
                //        break;
                //    default:
                //        moneyInterest = Convert.ToInt64(lenderDetail.RateInterest * TimaSettingConstant.UnitMillionMoney);
                //        break;
                //}
                moneyInterest = _common.ConvertRateToVND(loanDetailLos.LenderRate);
                var timaCodeID = ((await _loanTab.SetGetTop(1).SelectColumns(x => x.TimaCodeID)
                                            .WhereClause(x => x.OwnerShopID == TimaSettingConstant.ShopIDTima)
                                            .OrderByDescending(x => x.TimaCodeID)
                                            .QueryAsync()).FirstOrDefault()?.TimaCodeID ?? 0) + 1;
                if (request.AgCodeID > 0)
                {
                    // log 1 thời gian để check dữ liệu ag
                    if (timaCodeID != request.AgCodeID)
                    {
                        _logger.LogError($"CreateLoanAsync_Warning|AgCodeID={request.AgCodeID}|LmsCodeID={timaCodeID}");
                    }
                    timaCodeID = request.AgCodeID;
                }
                // tạo đơn vay
                Domain.Tables.LoanJsonExtra loanJsonExtra = new Domain.Tables.LoanJsonExtra
                {
                    DistrictName = loanDetailLos.DistrictName,
                    CityName = loanDetailLos.ProvinceName,
                    ShopName = lenderDetail.FullName,
                    ConsultantShopName = loanDetailLos.ShopName,
                    TopUpOfLoanBriefID = loanDetailLos.TopUpOfLoanBriefID,
                    IsLocate = loanDetailLos.IsLocate,
                    LoanBriefPropertyPlateNumber = loanDetailLos.LoanBriefProperty?.PlateNumber
                };
                var fromDate = request.FromDate ?? currentDate;
                var loanInfo = new Domain.Tables.TblLoan
                {
                    CustomerID = customerID,
                    FromDate = fromDate,
                    ToDate = fromDate.AddMonths(loanDetailLos.LoanTime).AddDays(-1),
                    CityID = loanDetailLos.ProvinceId,
                    DistrictID = loanDetailLos.DistrictId,
                    TotalMoneyDisbursement = (long)loanDetailLos.LoanAmountFinal,
                    LoanTime = loanDetailLos.LoanTime,
                    Frequency = loanDetailLos.Frequency,
                    RateType = loanDetailLos.RateTypeId,
                    CreateDate = currentDate,
                    ModifyDate = currentDate,
                    BankCardID = request.BankCardID,
                    ConsultantShopID = loanDetailLos.ShopId,
                    ContactCode = _common.GenLoanContactCode(timaCodeID, "0"),
                    CustomerName = loanDetailLos.FullName,
                    MoneyFeeInsuranceOfCustomer = request.MoneyfeeInsuranceOfCustomer,
                    MoneyFeeInsuranceMaterialCovered = request.MoneyFeeInsuranceMaterialCovered,
                    MoneyFeeInsuranceOfLender = 0,
                    LenderID = loanDetailLos.LenderId,
                    NextDate = currentDate.AddMonths(1),
                    OwnerShopID = TimaSettingConstant.ShopIDTima,
                    ProductID = loanDetailLos.ProductId,
                    RateConsultant = totalMoneyInterest - moneyInterest,
                    RateInterest = moneyInterest,
                    RateService = 0, // chưa có chính sách
                    SourcecBuyInsurance = loanDetailLos.TypeInsurence,
                    Status = (int)Loan_Status.Lending,
                    TotalMoneyCurrent = (long)loanDetailLos.LoanAmountFinal,
                    SourceMoneyDisbursement = request.SourceBankDisbursement,
                    WardID = loanDetailLos.HouseOldWardId,
                    LoanCreditIDOfPartner = loanDetailLos.LoanBriefId,
                    ProductName = loanDetailLos.ProductName,
                    UnitRate = (int)Loan_UnitRate.Default,
                    TimaLoanID = request.LoanAgID,
                    JsonExtra = _common.ConvertObjectToJSonV2(loanJsonExtra),
                    TimaCodeID = timaCodeID,
                    FeeFineOriginal = loanDetailLos.FeePaymentBeforeLoan * 100,
                    UnitFeeFineOriginal = (int)Loan_UnitRate.Percent
                };
                DateTime? nextDateTopUp = null;
                // 29-08-2021 remove check topup
                // 27-10-2021 mở lại topup
                if (loanDetailLos.TopUpOfLoanBriefID > 0 && loanDetailLos.RateTypeId == (int)PaymentSchedule_ActionRateType.RateAmortization30DaysTopUp) // kiểm tra điều kiện của đơn Topup
                {
                    // lấy đơn cũ
                    var loanInfoTopUp = _loanTab.SetGetTop(1).WhereClause(x => x.LoanCreditIDOfPartner == loanDetailLos.TopUpOfLoanBriefID).Query().FirstOrDefault();
                    if (loanInfoTopUp != null && loanInfoTopUp.LoanID > 0)
                    {
                        nextDateTopUp = loanInfoTopUp.NextDate;
                        //loanInfo.RateType = (int)PaymentSchedule_ActionRateType.RateAmortization30DaysTopUp;
                    }
                }

                loanInfo.LoanID = await _loanTab.InsertAsync(loanInfo);

                if (loanInfo.LoanID > 0)
                {
                    _ = _paymentScheduleService.CreatePaymentScheduleByLoanID(
                        loanInfo.TotalMoneyDisbursement,
                        loanInfo.FromDate,
                        loanInfo.LoanTime * TimaSettingConstant.NumberDayInMonth,
                        loanInfo.Frequency * TimaSettingConstant.NumberDayInMonth,
                        loanInfo.RateType,
                        loanInfo.RateConsultant,
                        loanInfo.RateInterest,
                        loanInfo.RateService,
                        loanInfo.LoanID, nextDateTopUp);

                    //tạo lịch sử giao dịch
                    _ = _transactionService.CreateTransaction(
                        new List<LMS.Entites.Dtos.TransactionServices.RequestCreateListTransactionModel>()
                        {
                            new LMS.Entites.Dtos.TransactionServices.RequestCreateListTransactionModel()
                            {
                                ActionID = (int)Transaction_Action.ChoVay,
                                LoanID = loanInfo.LoanID,
                                MoneyType = (int)Transaction_TypeMoney.Original,
                                PaymentScheduleID = 0,
                                TotalMoney =-loanInfo.TotalMoneyDisbursement,
                                UserID = request.CreateBy
                            }
                        }
                    );
                    List<int> lstBankLenderDebt = new List<int> { (int)Loan_SourceMoneyDisbursement.BankNamAOfLender, (int)Loan_SourceMoneyDisbursement.OtherBank, (int)Loan_SourceMoneyDisbursement.VIBOfLender };
                    // tạo phiếu giải ngân
                    if (request.SourceBankDisbursement == (int)Loan_SourceMoneyDisbursement.EscrowSource)
                    {
                        if (string.IsNullOrEmpty(request.Note))
                        {
                            request.Note = string.Format(TimaSettingConstant.SystemDisbursementToCustomer, loanDetailLos.FullName.ToUpper());
                        }
                        long moneyTranferToCustomer = loanInfo.TotalMoneyDisbursement - Convert.ToInt64(loanInfo.MoneyFeeInsuranceOfCustomer + loanInfo.MoneyFeeInsuranceMaterialCovered);
                        _ = _invoiceService.CreateInvoicePaySlipCustomer(moneyTranferToCustomer, loanInfo.LoanID, loanInfo.CustomerID.Value, loanInfo.OwnerShopID, fromDate, request.BankCardID, request.Note, request.CreateBy);
                    }
                    else if (lstBankLenderDebt.Contains(request.SourceBankDisbursement))
                    {
                        _ = _lenderService.CreateLenderDebt(new Entites.Dtos.LenderService.CreateLenderDebtRequest
                        {
                            CreateBy = request.CreateBy,
                            DebtType = (int)Debt_TypeID.LenderKeepInsurance,
                            Description = $"Giữ tiền bảo hiểm đơn vay {loanInfo.ContactCode}",
                            LenderID = loanInfo.LenderID,
                            ReferID = loanInfo.LoanID,
                            TotalMoney = loanInfo.MoneyFeeInsuranceOfCustomer + loanInfo.MoneyFeeInsuranceMaterialCovered
                        });
                    }

                    // mua bảo hiểm
                    if (loanDetailLos.BuyInsurenceCustomer)
                    {
                        _ = _insuranceService.CreateInsuranceOfCustomer(loanInfo.LoanID, loanInfo.CustomerID.Value);
                    }
                    if (loanDetailLos.BuyInsuranceProperty)
                    {
                        _ = _insuranceService.CreateInsuranceMaterial(loanInfo.LoanID, loanInfo.CustomerID.Value);
                    }
                    if (loanDetailLos.BuyInsuranceLender)
                    {
                        _ = _insuranceService.CreateInsuranceOfLender(loanInfo.LoanID, loanInfo.LenderID);
                    }
                    if (request.MoneyFeeDisbursement > 0)
                    {
                        // todo: ghi nhận phí trừ tiền đến thẻ
                        _ = _invoiceService.CreateInvoiceBankInterest(request.MoneyFeeDisbursement, request.Note, DateTime.Now, request.BankCardID, TimaSettingConstant.ShopIDTima, request.CreateBy, (int)GroupInvoiceType.PaySlipBankFee);
                    }
                    if (request.LoanAgID == 0)
                    {
                        // đơn tạo từ LMS chưa bắn qua cho LOS
                        _ = _losService.DisbursementLoan(new Entites.Dtos.LOSServices.DisbursementLoanReq
                        {
                            CodeId = (int)timaCodeID,
                            FeeInsuranceOfCustomer = (long)loanInfo.MoneyFeeInsuranceOfCustomer,
                            LoanBriefId = request.LoanCreditID,
                            LoanId = loanInfo.LoanID,
                            Type = (int)LOS_Enum_DisbursementType.CreateLoan,
                            IsHandleException = request.IsHandleException,
                            FeeInsuranceOfProperty = loanInfo.MoneyFeeInsuranceMaterialCovered
                        });
                    }

                    // ghi nhận hợp đồng của LenderID
                    _ = await _contractLoanWithLenderTab.InsertAsync(new TblContractLoanWithLender
                    {
                        LoanID = loanInfo.LoanID,
                        AccountingPriorityOrderID = 1,
                        TimaLoanID = loanInfo.TimaLoanID,
                        CreateDate = currentDate,
                        ModifyDate = currentDate,
                        FromDate = loanInfo.FromDate,
                        ToDate = loanInfo.ToDate,
                        LenderID = loanInfo.LenderID,
                        RateConsultant = loanInfo.RateConsultant,
                        RateInterest = loanInfo.RateInterest,
                        RateService = loanInfo.RateService,
                        UnitRate = loanInfo.UnitRate,
                        SelfEmployed = lenderDetail.SelfEmployed
                    });
                    response.SetSucces();
                    response.Data = loanInfo.LoanID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateLoanAsync|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> UpdateLoanAfterPayment(long loanID, long creatBy, long totalMoneyFineCustomerPaid = 0)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var loanInfo = (await _loanUpdateAfterPayment.WhereClause(x => x.LoanID == loanID).QueryAsync()).FirstOrDefault();
                if (loanInfo == null || loanInfo.LoanID < 1)
                {
                    response.Message = MessageConstant.LoanNotExist;
                    _logger.LogError($"UpdateLoanAfterPayment_WarningLoanNotFound|loanID={loanID}|message={response.Message}");
                    return response;
                }
                var currentDate = DateTime.Now;
                // cập nhật lại ngày thanh toán cho đơn vay
                var lstPaymentScheduleTask = _paymentScheduleTab.WhereClause(x => x.LoanID == loanInfo.LoanID && x.Status == (int)PaymentSchedule_Status.Use).QueryAsync();
                var lstTransactionLoanOriginal = _transactionLoanTab.SelectColumns(x => x.TotalMoney)
                                                                    .WhereClause(x => x.LoanID == loanInfo.LoanID && x.ActionID != (int)Transaction_Action.ChoVay && x.MoneyType == (int)Transaction_TypeMoney.Original)
                                                                    .QueryAsync();

                await Task.WhenAll(lstPaymentScheduleTask, lstTransactionLoanOriginal);

                var lstPaymentSchedule = lstPaymentScheduleTask.Result;
                long totalMoneyOriginalPaid = lstTransactionLoanOriginal.Result.Sum(x => x.TotalMoney);

                var paymentDetailNext = lstPaymentSchedule.Where(x => x.IsComplete == (int)PaymentSchedule_IsComplete.Waiting && x.IsVisible == (int)PaymentSchedule_IsVisible.Show).OrderBy(x => x.PaymentScheduleID).FirstOrDefault();
                if (paymentDetailNext == null)
                {
                    _logger.LogError($"UpdateLoanAfterPayment_WarningPaymentDetailNextNotFound|loanID={loanID}");
                    // hết kỳ đóng tiền
                    loanInfo.LastDateOfPay = loanInfo.NextDate;
                    loanInfo.ModifyDate = currentDate;
                }
                else
                {
                    // log theo dõi 1 thời gian
                    if (loanInfo.NextDate > paymentDetailNext.PayDate)
                    {
                        _logger.LogError($"UpdateLoanAfterPayment_Warning|LoanID={loanInfo.LoanID}|NextDate={loanInfo.NextDate}");
                    }
                    loanInfo.LastDateOfPay = paymentDetailNext.FromDate.AddDays(-1);
                    loanInfo.NextDate = paymentDetailNext.PayDate;
                    loanInfo.ModifyDate = currentDate;
                }

                loanInfo.TotalMoneyCurrent = loanInfo.TotalMoneyDisbursement - totalMoneyOriginalPaid;
                if (totalMoneyFineCustomerPaid > 0)
                {
                    loanInfo.MoneyFineLate -= totalMoneyFineCustomerPaid;
                }
                if (!(await _loanUpdateAfterPayment.UpdateAsync(loanInfo)))
                {
                    _logger.LogError($"UpdateLoanAfterPayment_UpdateLoanFail|loanID={loanID}");
                }
                else
                {
                    response.SetSucces();
                    _ = await _kafkaLogEventTab.InsertAsync(new TblKafkaLogEvent
                    {
                        CreateDate = DateTime.Now,
                        ModifyDate = DateTime.Now,
                        DataMessage = _common.ConvertObjectToJSonV2(new LMS.Kafka.Messages.Loan.LoanCutPeriodSuccessInfo
                        {
                            LoanID = loanID,
                            NextDate = loanInfo.NextDate.Value,
                            LastDateOfPay = loanInfo.LastDateOfPay.Value
                        }),
                        TopicName = Kafka.Constants.KafkaTopics.LoanCutPeriodSuccess,
                        ServiceName = ServiceHostInternal.LoanService,
                        Status = (int)KafkaLogEvent_Status.Waitting,
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateLoanAfterPayment|loanID={loanID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> CreateLoanExtraOriginal(LoanExtraInsertDetailModel request)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.Data = await _loanExtraTab.InsertAsync(new Domain.Tables.TblLoanExtra
                {
                    CreateBy = request.CreateBy,
                    CreateDate = DateTime.Now,
                    ModifyDate = DateTime.Now,
                    LoanID = request.LoanID,
                    PayDate = request.PayDate,
                    TypeMoney = request.TypeMoney,
                    TotalMoney = request.TotalMoney
                });
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateLoanExtraOriginal|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }


        public async Task<MoneyNeedCloseLoan> GetMoneyCloseLoanV2(long loanID, DateTime closeDate)
        {
            try
            {
                var lstLoanInfosTask = _loanTab.WhereClause(x => x.LoanID == loanID).QueryAsync();
                var lstPaymentScheduleTask = _paymentScheduleTab.WhereClause(m => m.LoanID == loanID &&
                                                                       m.Status == (int)PaymentSchedule_Status.Use &&
                                                                       m.IsComplete <= (int)PaymentSchedule_IsComplete.Paid)
                                                       .OrderBy(m => m.PayDate).QueryAsync();
                await Task.WhenAll(lstLoanInfosTask, lstPaymentScheduleTask);
                var lstLoanInfos = lstLoanInfosTask.Result;
                var lstPaymentScheduleAll = lstPaymentScheduleTask.Result;

                if (lstPaymentScheduleAll == null || !lstPaymentScheduleAll.Any())
                {
                    _logger.LogError($"GetMoneyCloseLoanV2|loanID={loanID}|closeDate={closeDate:dd/MM/yyyy}|message={MessageConstant.Payment_NotFound}");
                    return null;
                }
                if (lstLoanInfos == null || !lstLoanInfos.Any())
                {
                    _logger.LogError($"GetMoneyCloseLoanV2|loanID={loanID}|closeDate={closeDate:dd/MM/yyyy}|message={MessageConstant.LoanNotExist}");
                    return null;
                }
                var loanInfo = lstLoanInfos.First();
                List<Domain.Tables.TblLoanExtra> lstLoanExtra = null;
                // đơn đang vay mới tính tiền tất toán
                List<int> lstStatusLoanLending = new List<int> { (int)Loan_Status.Lending, (int)Loan_Status.CloseIndemnifyLoanInsurance };
                if (!lstStatusLoanLending.Contains((int)loanInfo.Status))
                {
                    return new MoneyNeedCloseLoan() { CloseType = (int)MoneyCloseLoanType.Finished };
                }
                var lenderInfo = (await _lenderTab.WhereClause(x => x.LenderID == loanInfo.LenderID).QueryAsync()).FirstOrDefault();
                if (loanInfo.ToDate < closeDate)
                {
                    lstLoanExtra = (await _loanExtraTab.WhereClause(m => m.LoanID == (int)loanID).QueryAsync()).ToList();
                }

                return await GetMoneyCloseLoanV3(closeDate, loanInfo, lstPaymentScheduleAll.ToList(), lstLoanExtra, lenderInfo);

                #region old code
                //// lịch trả nợ trong hạn
                //var lstPaymentSchedule = lstPaymentScheduleAll.Where(m => m.ToDate <= loanInfo.ToDate).OrderBy(m => m.PayDate).ToList();

                //// lịch trả nợ ngoài hạn
                //var lstPaymentScheduleOutOfDate = lstPaymentScheduleAll.Where(m => m.ToDate > loanInfo.ToDate).OrderBy(m => m.PayDate).ToList();

                //var objLender = (await _lenderTab.WhereClause(x => x.LenderID == loanInfo.LenderID).QueryAsync()).FirstOrDefault();
                //long moneyInterest = 0;
                //long moneyService = 0;
                //long moneyConsultant = 0;
                //long totalMoneyCloseLoan = 0;
                //LMS.Entites.Dtos.LoanServices.MoneyNeedCloseLoan moneyNeedCloseLoan = new Entites.Dtos.LoanServices.MoneyNeedCloseLoan
                //{
                //    CloseType = (int)MoneyCloseLoanType.Schedule,
                //    MoneyOriginal = loanInfo.TotalMoneyCurrent,
                //    MoneyFineLate = loanInfo.MoneyFineLate
                //};
                //DateTime lastDateOfPay = loanInfo.FromDate;
                //if (loanInfo.LastDateOfPay.HasValue)
                //{
                //    // ngày lastdate đã được tính tiền, lấy ngày tiếp theo tính lãi
                //    lastDateOfPay = loanInfo.LastDateOfPay.Value.AddDays(1);
                //}
                //moneyNeedCloseLoan.OverDate = closeDate.Date.Subtract(loanInfo.LastDateOfPay ?? loanInfo.FromDate.AddDays(-1)).Days;

                //foreach (var item in lstPaymentSchedule)
                //{
                //    moneyInterest += item.MoneyInterest - item.PayMoneyInterest;
                //    moneyService += item.MoneyService - item.PayMoneyService;
                //    moneyConsultant += item.MoneyConsultant - item.PayMoneyConsultant;

                //    // lấy số tiền còn thiếu của các kỳ trước đó so với ngày phải đóng hợp đồng
                //    if (item.PayDate <= closeDate)
                //    {
                //        moneyNeedCloseLoan.TotalMoneyReceivables += (item.MoneyInterest - item.PayMoneyInterest) + (item.MoneyService - item.PayMoneyService) + (item.MoneyConsultant - item.PayMoneyConsultant);
                //    }
                //}

                //totalMoneyCloseLoan = moneyNeedCloseLoan.MoneyOriginal + moneyInterest + moneyService + moneyConsultant + moneyNeedCloseLoan.MoneyFineLate;
                //if (closeDate == loanInfo.ToDate)
                //{
                //    moneyNeedCloseLoan.MoneyInterest = moneyInterest;
                //    moneyNeedCloseLoan.MoneyService = moneyService;
                //    moneyNeedCloseLoan.MoneyConsultant = moneyConsultant;
                //    moneyNeedCloseLoan.TotalMoneyCloseLoan = totalMoneyCloseLoan;
                //    moneyNeedCloseLoan.TotalInterest = moneyNeedCloseLoan.MoneyInterest + moneyNeedCloseLoan.MoneyService + moneyNeedCloseLoan.MoneyConsultant;
                //    return moneyNeedCloseLoan;
                //}

                //if (closeDate < loanInfo.ToDate) // nếu ngày tất toán nhỏ hơn hoặc bằng ngày đáo hạn
                //{
                //    long totalMoneyOriginalBeforeCloseLoan = 0; // tổng tiền gốc dự kiến trước lúc tất toán - tiền đầu kỳ.
                //    long totalMoneyOriginalPayBeforeCloseLoan = 0; // tổng tiền gốc phải đóng trước kỳ thanh toán

                //    decimal feeFineOriginal = loanInfo.FeeFineOriginal;
                //    if (loanInfo.UnitFeeFineOriginal == (int)Loan_UnitRate.Percent)
                //    {
                //        feeFineOriginal /= 100.0M;
                //    }

                //    var lstPaymentScheduleBeforeCloseLoan = lstPaymentSchedule.Where(m => m.ToDate < closeDate).ToList();
                //    var lstPaymentScheduleCloseLoan = lstPaymentSchedule.Where(m => m.FromDate <= closeDate && m.ToDate >= closeDate).ToList();

                //    // code AG đoạn này quăng lỗi
                //    if (lstPaymentScheduleCloseLoan.Count > 1)
                //    {
                //        _logger.LogError($"GetMoneyCloseLoanV2|loanID={loanID}|closeDate={closeDate:dd/MM/yyyy}|lstPaymentScheduleCloseLoan={lstPaymentScheduleCloseLoan.Count}|message=Lỗi vì có nhiều kỳ chưa thỏa AG");
                //        return null;
                //    }

                //    var paymentScheduleCloseLoan = lstPaymentScheduleCloseLoan.FirstOrDefault();
                //    // tính tổng tiền còn thiếu của các kỳ trước ngày closeloan
                //    foreach (var item in lstPaymentScheduleBeforeCloseLoan)
                //    {
                //        moneyNeedCloseLoan.MoneyInterest += item.MoneyInterest - item.PayMoneyInterest;
                //        moneyNeedCloseLoan.MoneyService += item.MoneyService - item.PayMoneyService;
                //        moneyNeedCloseLoan.MoneyConsultant += item.MoneyConsultant - item.PayMoneyConsultant;
                //        totalMoneyOriginalPayBeforeCloseLoan += item.MoneyOriginal;
                //    }
                //    var numberDayCloseLoan = closeDate.Subtract(paymentScheduleCloseLoan.FromDate).Days + 1;
                //    var numberDayOfpaymentScheduleCloseLoan = paymentScheduleCloseLoan.ToDate.Subtract(paymentScheduleCloseLoan.FromDate).Days + 1;

                //    totalMoneyOriginalBeforeCloseLoan = loanInfo.TotalMoneyDisbursement - totalMoneyOriginalPayBeforeCloseLoan;

                //    moneyNeedCloseLoan.MoneyInterest += Convert.ToInt64(Math.Round(paymentScheduleCloseLoan.MoneyInterest * 1.0 / numberDayOfpaymentScheduleCloseLoan * numberDayCloseLoan, 0));
                //    moneyNeedCloseLoan.MoneyService += Convert.ToInt64(Math.Round(paymentScheduleCloseLoan.MoneyService * 1.0 / numberDayOfpaymentScheduleCloseLoan * numberDayCloseLoan, 0));
                //    moneyNeedCloseLoan.MoneyConsultant += Convert.ToInt64(Math.Round(paymentScheduleCloseLoan.MoneyConsultant * 1.0 / numberDayOfpaymentScheduleCloseLoan * numberDayCloseLoan, 0));

                //    moneyNeedCloseLoan.MoneyInterest -= paymentScheduleCloseLoan.PayMoneyInterest;
                //    moneyNeedCloseLoan.MoneyService -= paymentScheduleCloseLoan.PayMoneyService;
                //    moneyNeedCloseLoan.MoneyConsultant -= paymentScheduleCloseLoan.PayMoneyConsultant;

                //    // Nếu là của Lender Level thì mới tính phí phạt trả trước gốc
                //    if (objLender.SelfEmployed >= (int)Lender_SeflEmployed.LenderOut)
                //    {
                //        moneyNeedCloseLoan.MoneyFineOriginal = Convert.ToInt64(Math.Round(totalMoneyOriginalBeforeCloseLoan / 100.0M * feeFineOriginal, 0));
                //    }

                //    moneyNeedCloseLoan.TotalMoneyCloseLoan = moneyNeedCloseLoan.MoneyOriginal + moneyNeedCloseLoan.MoneyFineOriginal + moneyNeedCloseLoan.MoneyService + moneyNeedCloseLoan.MoneyInterest + moneyNeedCloseLoan.MoneyConsultant + moneyNeedCloseLoan.MoneyFineLate;

                //    // Nếu tổng tiền trong hạn của khách hàng nhỏ hơn tổng tiền tất toán ở trên thì lấy tổng tiền trong hạn thôi
                //    if (totalMoneyCloseLoan < moneyNeedCloseLoan.TotalMoneyCloseLoan)
                //    {
                //        moneyNeedCloseLoan.MoneyInterest = moneyInterest;
                //        moneyNeedCloseLoan.MoneyService = moneyService;
                //        moneyNeedCloseLoan.MoneyConsultant = moneyConsultant;
                //        moneyNeedCloseLoan.TotalMoneyCloseLoan = totalMoneyCloseLoan;
                //        moneyNeedCloseLoan.MoneyFineOriginal = 0;
                //    }
                //    // tổng lãi + phí
                //    moneyNeedCloseLoan.TotalInterest = moneyNeedCloseLoan.MoneyInterest + moneyNeedCloseLoan.MoneyService + moneyNeedCloseLoan.MoneyConsultant;
                //}
                //else // ngày tất toán lớn hơn ngày đáo hạn
                //{
                //    var lstLoanExtra = (await _loanExtraTab.WhereClause(m => m.LoanID == (int)loanID).QueryAsync()).ToList();
                //    var totalMoneyPayOriginalAfterToDate = lstLoanExtra.Where(m => m.PayDate <= loanInfo.ToDate).Sum(m => m.TotalMoney);
                //    var totalMoneyOriginal = loanInfo.TotalMoneyDisbursement - (totalMoneyPayOriginalAfterToDate * -1);

                //    foreach (var item in lstPaymentSchedule)
                //    {
                //        moneyNeedCloseLoan.MoneyInterest += item.MoneyInterest - item.PayMoneyInterest;
                //        moneyNeedCloseLoan.MoneyService += item.MoneyService - item.PayMoneyService;
                //        moneyNeedCloseLoan.MoneyConsultant += item.MoneyConsultant - item.PayMoneyConsultant;
                //    }

                //    if (lstPaymentScheduleOutOfDate != null && lstPaymentScheduleOutOfDate.Count > 0)
                //    {
                //        foreach (var item in lstPaymentScheduleOutOfDate)
                //        {
                //            moneyNeedCloseLoan.MoneyInterest += item.MoneyInterest - item.PayMoneyInterest;
                //            moneyNeedCloseLoan.MoneyService += item.MoneyService - item.PayMoneyService;
                //            moneyNeedCloseLoan.MoneyConsultant += item.MoneyConsultant - item.PayMoneyConsultant;
                //        }
                //    }
                //    else
                //    {
                //        var lstLoanExtraAfterToDate = lstLoanExtra.Where(m => m.PayDate > loanInfo.ToDate).ToList();
                //        var objAmountIncurred = CalculateMoneyAmountIncurred(loanInfo.ToDate.AddDays(1), closeDate, totalMoneyOriginal, lstLoanExtraAfterToDate, loanInfo);

                //        moneyNeedCloseLoan.MoneyInterest += objAmountIncurred.MoneyInterest;
                //        moneyNeedCloseLoan.MoneyService += objAmountIncurred.MoneyService;
                //        moneyNeedCloseLoan.MoneyConsultant += objAmountIncurred.MoneyConsultant;
                //    }

                //    moneyNeedCloseLoan.TotalMoneyCloseLoan = moneyNeedCloseLoan.MoneyOriginal + moneyNeedCloseLoan.MoneyFineOriginal + moneyNeedCloseLoan.MoneyService + moneyNeedCloseLoan.MoneyInterest + moneyNeedCloseLoan.MoneyConsultant + moneyNeedCloseLoan.MoneyFineLate;
                //    // tổng lãi + phí
                //    moneyNeedCloseLoan.TotalInterest = moneyNeedCloseLoan.MoneyInterest + moneyNeedCloseLoan.MoneyService + moneyNeedCloseLoan.MoneyConsultant;
                //}


                //return moneyNeedCloseLoan;
                #endregion
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetMoneyCloseLoan_Exception|loanID={loanID}|closeDate={closeDate.ToString(TimaSettingConstant.DateTimeDayMonthYear)}|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }
        /// <summary>
        /// Tính tiền phát sinh thực tế theo lịch trả bớt gốc từ khoảng thời gian
        /// </summary>
        /// <param name="fromDate">ngày bắt đầu tính</param>
        /// <param name="toDate">ngày kết thúc tính</param>
        /// <param name="moneyOriginal">số tiền gốc tại thời điểm ngày bắt đầu tính</param>
        /// <param name="loanExtras">lịch trả bớt gốc</param>
        /// <param name="objLoan">thông tin đơn vay</param>
        /// 
        /// 
        /// <returns></returns>
        private MoneyAmountIncurred CalculateMoneyAmountIncurred(DateTime fromDate, DateTime toDate, long moneyOriginal, List<Domain.Tables.TblLoanExtra> loanExtras, Domain.Tables.TblLoan objLoan)
        {
            var result = new MoneyAmountIncurred();
            DateTime currentDate = fromDate;
            //return (TotalMoney / 1000) * Math.Round(Rate, 2) * iDatePayment;
            //double iDatePayment = (toDate - fromDate).TotalDays + 1;
            // AG chia cho 1000, lms chia cho 1M
            int iDatePayment = (toDate - fromDate).Days + 1;
            decimal temp = 1.0M;
            if (loanExtras != null && loanExtras.Count > 0)
            {
                for (int i = 0; i < iDatePayment; i++)
                {
                    long TotalMoneyCurrent = moneyOriginal;
                    long iTotalBackup = TotalMoneyCurrent;

                    for (int j = 0; j < loanExtras.Count; j++)
                    {
                        if (currentDate >= loanExtras[j].PayDate)
                        {
                            TotalMoneyCurrent = TotalMoneyCurrent + loanExtras[j].TotalMoney;
                            if (loanExtras[j].TotalMoney > 0 || currentDate > loanExtras[j].PayDate) iTotalBackup = TotalMoneyCurrent; // Nếu Trả gốc thì ngày hum sau mới tính lại tiền gốc, Nếu vay thêm gốc thì tính luôn gốc mới
                        }
                    }
                    result.MoneyConsultant += Convert.ToInt64(Math.Round(iTotalBackup * temp / TimaSettingConstant.UnitMillionMoney * objLoan.RateConsultant * 1, 0));
                    result.MoneyInterest += Convert.ToInt64(Math.Round(iTotalBackup * temp / TimaSettingConstant.UnitMillionMoney * objLoan.RateInterest * 1, 0));
                    result.MoneyService += Convert.ToInt64(Math.Round(iTotalBackup * temp / TimaSettingConstant.UnitMillionMoney * objLoan.RateService * 1, 0));

                    currentDate = currentDate.AddDays(1);
                }
                return result;
            }
            else
            {
                result.MoneyConsultant = Convert.ToInt64(Math.Round(moneyOriginal * temp / TimaSettingConstant.UnitMillionMoney * objLoan.RateConsultant * iDatePayment, 0));
                result.MoneyInterest = Convert.ToInt64(Math.Round(moneyOriginal * temp / TimaSettingConstant.UnitMillionMoney * objLoan.RateInterest * iDatePayment, 0));
                result.MoneyService = Convert.ToInt64(Math.Round(moneyOriginal * temp / TimaSettingConstant.UnitMillionMoney * objLoan.RateService * iDatePayment, 0));
            }
            return result;
        }
        public async Task<ResponseActionResult> ProcessCutOffLoanByDateDaily(long latestLoanID)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var currentDate = DateTime.Now;
                var cutOffDateToday = currentDate.Date;
                var cutOffLoanExists = await _cutOffLoanTab.SetGetTop(1).WhereClause(x => x.CutOffDate == cutOffDateToday).QueryAsync();
                if (cutOffLoanExists != null && cutOffLoanExists.Any() && latestLoanID == 0)
                {
                    response.Message = "Dữ liệu đã tổng hợp.";
                    return response;
                }
                List<Domain.Tables.TblCutOffLoan> lstCutOffLoanInsert = new List<Domain.Tables.TblCutOffLoan>();
                string keyDictCutOffLoan = "";
                Dictionary<string, Domain.Tables.TblCutOffLoan> dictCutOffLoanInfos = new Dictionary<string, Domain.Tables.TblCutOffLoan>();

                // lấy dữ liệu từ đầu tháng do AG bắn qua lần đầu tiên
                var firtDateOfMonth = (new DateTime(cutOffDateToday.Year, cutOffDateToday.Month, 1)).AddSeconds(-1);
                var yesterday = cutOffDateToday.AddDays(-1);

                List<int> lstTypeMoneyTxnLoan = new List<int>
                {
                    (int) Transaction_TypeMoney.Interest,
                    (int) Transaction_TypeMoney.Original,
                    (int) Transaction_TypeMoney.Consultant,
                    (int) Transaction_TypeMoney.Service
                };
                // lấy dữ liệu của ngày hôm qua của loan có phát sinh
                var beginDateOfCutDate = yesterday.Date.AddSeconds(-1);
                var endDateOfCutDate = yesterday.Date.AddDays(1);
                List<long> lstTxnLoanID = new List<long>();
                // ngày đầu tháng cập nhật full đơn đang vay
                if (currentDate.Date.Day != 1)
                {
                    // hiện tại 1 ngày ko đủ max loan trong txn
                    // improve sau nếu vượt ngưỡng max trong 1 ngày
                    var lstTxnLoanTask = _transactionLoanTab.SelectColumns(x => x.LoanID)
                                                    .WhereClause(x => x.CreateDate > beginDateOfCutDate && x.CreateDate < endDateOfCutDate && x.TotalMoney != 0)
                                                    .WhereClause(x => x.ActionID != (int)Transaction_Action.ChoVay)
                                                    .WhereClause(x => lstTypeMoneyTxnLoan.Contains(x.MoneyType))
                                                    .WhereClause(x => x.LoanID > latestLoanID)
                                                    .GetDistinct()
                                                    .QueryAsync();

                    await Task.WhenAll(lstTxnLoanTask);

                    lstTxnLoanID = lstTxnLoanTask.Result.Select(x => (long)x.LoanID).Distinct().ToList();
                    _loanTab.WhereClause(x => lstTxnLoanID.Contains(x.LoanID) || x.FromDate == yesterday);
                }
                else
                {
                    List<int> lstStatusLending = new List<int>() { (int)Loan_Status.Lending, (int)Loan_Status.CloseIndemnifyLoanInsurance };
                    _loanTab.WhereClause(x => lstStatusLending.Contains((int)x.Status));
                }
                var lstLoanInfoTask = _loanTab.SelectColumns(x => x.LoanID, x => x.NextDate, x => x.LastDateOfPay, x => x.FromDate, x => x.TotalMoneyCurrent, x => x.TotalMoneyDisbursement)
                                        .SelectColumns(x => x.CustomerID, x => x.LenderID, x => x.OwnerShopID, x => x.ConsultantShopID, x => x.Status)
                                        .QueryAsync();

                await Task.WhenAll(lstLoanInfoTask);
                var dictLoanInfos = lstLoanInfoTask.Result.ToDictionary(x => x.LoanID, x => x);
                if (currentDate.Day != 1)
                {
                    // dữ liệu mới 
                    var lstLoanNewInfos = dictLoanInfos.Values.Where(x => x.FromDate.Date == yesterday).ToList();
                    if (lstLoanNewInfos != null && lstLoanNewInfos.Any())
                    {
                        foreach (var item in lstLoanNewInfos)
                        {
                            keyDictCutOffLoan = $"{item.FromDate:yyyMMdd}_{item.LoanID}";
                            if (!dictCutOffLoanInfos.ContainsKey(keyDictCutOffLoan))
                            {
                                dictCutOffLoanInfos.Add(keyDictCutOffLoan, new Domain.Tables.TblCutOffLoan
                                {
                                    ConsultantShopID = item.ConsultantShopID,
                                    CreateDate = currentDate,
                                    CustomerID = item.CustomerID.Value,
                                    CutOffDate = yesterday,
                                    LastDateOfPay = item.LastDateOfPay ?? item.FromDate,
                                    LenderID = item.LenderID,
                                    LoanID = item.LoanID,
                                    NextDate = item.NextDate.Value,
                                    OwnerShopID = item.OwnerShopID,
                                    TotalMoneyCurrent = item.TotalMoneyCurrent,
                                    Status = (int)item.Status

                                });
                            }
                        }
                    }

                    if (lstTxnLoanID != null && lstTxnLoanID.Any())
                    {
                        foreach (var loanID in lstTxnLoanID)
                        {
                            if (dictCutOffLoanInfos.ContainsKey(keyDictCutOffLoan))
                            {
                                keyDictCutOffLoan = $"{cutOffDateToday:yyyMMdd}_{loanID}";
                                var loanInfo = dictLoanInfos.GetValueOrDefault((long)loanID);
                                if (!dictCutOffLoanInfos.ContainsKey(keyDictCutOffLoan))
                                {
                                    dictCutOffLoanInfos.Add(keyDictCutOffLoan, new Domain.Tables.TblCutOffLoan
                                    {
                                        ConsultantShopID = loanInfo.ConsultantShopID,
                                        CreateDate = currentDate,
                                        CustomerID = loanInfo.CustomerID.Value,
                                        CutOffDate = yesterday,
                                        LastDateOfPay = loanInfo.LastDateOfPay ?? loanInfo.FromDate,
                                        LenderID = loanInfo.LenderID,
                                        LoanID = loanInfo.LoanID,
                                        NextDate = loanInfo.NextDate.Value,
                                        OwnerShopID = loanInfo.OwnerShopID,
                                        TotalMoneyCurrent = loanInfo.TotalMoneyCurrent,
                                        Status = (int)loanInfo.Status
                                    });
                                }
                            }
                        }
                    }
                }
                else
                {
                    // dữ liệu mới đầu tháng ghi nhận = ngày cuối của tháng trước
                    foreach (var item in dictLoanInfos.Values)
                    {
                        keyDictCutOffLoan = $"{item.FromDate:yyyMMdd}_{item.LoanID}";
                        dictCutOffLoanInfos.Add(keyDictCutOffLoan, new Domain.Tables.TblCutOffLoan
                        {
                            ConsultantShopID = item.ConsultantShopID,
                            CreateDate = currentDate,
                            CustomerID = item.CustomerID.Value,
                            CutOffDate = currentDate.AddDays(-1),
                            LastDateOfPay = item.LastDateOfPay ?? item.FromDate,
                            LenderID = item.LenderID,
                            LoanID = item.LoanID,
                            NextDate = item.NextDate.Value,
                            OwnerShopID = item.OwnerShopID,
                            TotalMoneyCurrent = item.TotalMoneyCurrent,
                            Status = (int)item.Status
                        });
                    }
                }
                if (dictCutOffLoanInfos.Count > 0)
                {
                    _cutOffLoanTab.InsertBulk(dictCutOffLoanInfos.Values);
                }
                else
                {
                    // log lại check dữ liệu
                    _logger.LogError($"ProcessCutOffLoanByDateDaily_Waring_Empty|currentDate={currentDate:dd/MM/yyyy HH:mm:ss}");
                }

                response.SetSucces();
                response.Data = dictCutOffLoanInfos.Values.Count;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessCutOffLoanByDateDaily|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> ProcessPlanCalculatorMoneyCloseLoanDaily(long latestLoanID, DateTime closeDate)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.Data = latestLoanID; // giữ lại giá trị đầu tiên
                response.SetSucces();
                List<int> lstStatusLoanLending = new List<int>
                {
                    (int)Loan_Status.Lending,
                    (int)Loan_Status.CloseIndemnifyLoanInsurance
                };
                var lstLoanData = await _loanTab.SelectColumns(x => x.LoanID, x => x.NextDate, x => x.ToDate, x => x.LenderID, x => x.Status)
                                            .SelectColumns(x => x.TotalMoneyCurrent, x => x.MoneyFineLate, x => x.FromDate, x => x.LastDateOfPay)
                                            .SelectColumns(x => x.FeeFineOriginal, x => x.UnitFeeFineOriginal, x => x.TotalMoneyDisbursement)
                                            .SelectColumns(x => x.RateConsultant, x => x.RateInterest, x => x.RateService, x => x.RateType)
                                            .SetGetTop(TimaSettingConstant.MaxItemQuery)
                                            .WhereClause(x => lstStatusLoanLending.Contains((int)x.Status))
                                            .WhereClause(x => x.LoanID > latestLoanID).QueryAsync();

                if (lstLoanData == null || !lstLoanData.Any())
                {
                    response.Data = 0;
                    return response;
                }
                List<long> lstLoanIDGetLstExtra = new List<long>();
                List<long> lstLoanID = new List<long>();
                List<long> lstLenderID = new List<long>();
                foreach (var item in lstLoanData)
                {
                    if (latestLoanID < item.LoanID)
                    {
                        latestLoanID = item.LoanID;
                    }
                    if (item.ToDate < closeDate.Date)
                    {
                        lstLoanIDGetLstExtra.Add(item.LoanID);
                    }
                    lstLoanID.Add(item.LoanID);
                    lstLenderID.Add(item.LenderID);
                }
                var lstPaymentScheduleTask = _paymentScheduleTab.WhereClause(x => lstLoanID.Contains(x.LoanID) && x.Status == (int)PaymentSchedule_Status.Use).QueryAsync();
                var lstLoanExtraTask = _loanExtraTab.WhereClause(x => lstLoanIDGetLstExtra.Contains(x.LoanID)).QueryAsync();
                var lstLenderTask = _lenderTab.WhereClause(x => lstLenderID.Contains(x.LenderID)).QueryAsync();
                var planCloseLoanDataExistsTask = _planCloseLoanTab.WhereClause(x => x.CloseDate == closeDate.Date).QueryAsync();

                await Task.WhenAll(lstPaymentScheduleTask, lstLoanExtraTask, lstLenderTask, planCloseLoanDataExistsTask);

                var dictPaymentScheduleLoanInfos = lstPaymentScheduleTask.Result.GroupBy(x => x.LoanID).ToDictionary(x => x.Key, x => x.ToList());
                var dictLoanExtraInfos = lstLoanExtraTask.Result.GroupBy(x => x.LoanID).ToDictionary(x => x.Key, x => x.ToList());
                var dictLenderInfos = lstLenderTask.Result.ToDictionary(x => x.LenderID, x => x);
                var dictPlanCloseLoanExistsInfos = planCloseLoanDataExistsTask.Result.ToDictionary(x => x.LoanID, x => x);
                lstLoanData = lstLoanData.OrderBy(x => x.LoanID);
                latestLoanID = lstLoanData.Last().LoanID;
                Dictionary<long, Task> dictTaskCalculateMoneyCloseLoan = new Dictionary<long, Task>();
                Dictionary<long, Task> dictTaskCalculateMoneyNeedPay = new Dictionary<long, Task>();
                foreach (var item in lstLoanData)
                {
                    var lstPaymentSchedule = dictPaymentScheduleLoanInfos.GetValueOrDefault(item.LoanID);
                    var lstLoanExtra = dictLoanExtraInfos.GetValueOrDefault(item.LoanID);
                    var lenderInfo = dictLenderInfos.GetValueOrDefault(item.LenderID);
                    var actionTaskGetMoneyCloseLoan = GetMoneyCloseLoanV3(closeDate, item, lstPaymentSchedule, lstLoanExtra, lenderInfo);
                    var actionTaskGetMoneyNeedPay = GetMoneyNeedPayInPeriod(closeDate, item, lstPaymentSchedule, lstLoanExtra, lenderInfo);
                    dictTaskCalculateMoneyCloseLoan.Add(item.LoanID, actionTaskGetMoneyCloseLoan);
                    dictTaskCalculateMoneyNeedPay.Add(item.LoanID, actionTaskGetMoneyNeedPay);
                }
                await Task.WhenAll(dictTaskCalculateMoneyCloseLoan.Values);
                await Task.WhenAll(dictTaskCalculateMoneyNeedPay.Values);
                List<Domain.Tables.TblPlanCloseLoan> lstInsert = new List<Domain.Tables.TblPlanCloseLoan>();
                List<Domain.Tables.TblPlanCloseLoan> lstUpdate = new List<Domain.Tables.TblPlanCloseLoan>();
                var currentDate = DateTime.Now;
                foreach (var item in lstLoanData)
                {
                    try
                    {
                        var taskResult = dictTaskCalculateMoneyCloseLoan.GetValueOrDefault(item.LoanID);
                        var taskResultNeedPay = dictTaskCalculateMoneyNeedPay.GetValueOrDefault(item.LoanID);
                        if (taskResult == null)
                        {
                            continue;
                        }
                        MoneyNeedCloseLoan actionResult = ((Task<MoneyNeedCloseLoan>)taskResult).Result;
                        MoneyNeedCloseLoan actionResultNeedPay = null;
                        if (taskResultNeedPay != null)
                        {
                            actionResultNeedPay = ((Task<MoneyNeedCloseLoan>)taskResultNeedPay).Result;
                        }
                        if (actionResult != null && actionResult.TotalMoneyCloseLoan > 0)
                        {
                            var detailPlanCloseLoan = dictPlanCloseLoanExistsInfos.GetValueOrDefault(item.LoanID);
                            if (detailPlanCloseLoan == null || detailPlanCloseLoan.PlanCloseLoanID < 1)
                            {
                                detailPlanCloseLoan = new Domain.Tables.TblPlanCloseLoan
                                {
                                    CloseDate = closeDate,
                                    LoanID = item.LoanID,
                                    CreateDate = currentDate,
                                    MoneyConsultant = actionResult.MoneyConsultant,
                                    MoneyFineLate = actionResult.MoneyFineLate,
                                    MoneyFineOriginal = actionResult.MoneyFineOriginal,
                                    MoneyInterest = actionResult.MoneyInterest,
                                    MoneyOriginal = actionResult.MoneyOriginal,
                                    MoneyService = actionResult.MoneyService,
                                    NextDate = item.NextDate.Value,
                                    TotalMoneyClose = actionResult.TotalMoneyCloseLoan,
                                    TotalMoneyReceivables = actionResult.TotalMoneyReceivables,
                                    TotalMoneyNeedPay = -1, // không tính được tiền phải cắt kỳ
                                    ModifyDate = currentDate
                                };
                            }
                            else
                            {
                                detailPlanCloseLoan.CountChange += 1;
                                detailPlanCloseLoan.ModifyDate = currentDate;
                            }
                            if (actionResultNeedPay != null)
                            {
                                if (actionResult.OverDate > TimaSettingConstant.MaxDaySentInsurance)
                                {
                                    detailPlanCloseLoan.TotalMoneyNeedPay = actionResult.TotalMoneyCloseLoan;
                                    detailPlanCloseLoan.MoneyConsultantNeePay = actionResult.MoneyConsultant;
                                    detailPlanCloseLoan.MoneyFineLateNeedPay = actionResult.MoneyFineLate;
                                    detailPlanCloseLoan.MoneyInterestNeedPay = actionResult.MoneyInterest;
                                    detailPlanCloseLoan.MoneyOriginalNeedPay = actionResult.MoneyOriginal;
                                    detailPlanCloseLoan.MoneyServiceNeedPay = actionResult.MoneyService;
                                }
                                else if (actionResult.OverDate < 0)
                                {
                                    // các giá trị cần phải đóng trong kỳ sẽ trả về 0
                                    detailPlanCloseLoan.TotalMoneyNeedPay = 0;
                                }
                                else
                                {
                                    detailPlanCloseLoan.TotalMoneyNeedPay = actionResultNeedPay.TotalMoneyCloseLoan;
                                    detailPlanCloseLoan.MoneyConsultantNeePay = actionResultNeedPay.MoneyConsultant;
                                    detailPlanCloseLoan.MoneyFineLateNeedPay = actionResultNeedPay.MoneyFineLate;
                                    detailPlanCloseLoan.MoneyInterestNeedPay = actionResultNeedPay.MoneyInterest;
                                    detailPlanCloseLoan.MoneyOriginalNeedPay = actionResultNeedPay.MoneyOriginal;
                                    detailPlanCloseLoan.MoneyServiceNeedPay = actionResultNeedPay.MoneyService;
                                }
                            }
                            if (detailPlanCloseLoan.PlanCloseLoanID < 1)
                                lstInsert.Add(detailPlanCloseLoan);
                            else
                            {
                                lstUpdate.Add(detailPlanCloseLoan);
                            }

                        }
                        else
                        {
                            _logger.LogError($"ProcessPlanCalculatorMoneyCloseLoanDaily_DetailLoan_ActionResult|LoanID={item.LoanID}|closeDate={closeDate:dd/MM/yyyy}|actionResult={_common.ConvertObjectToJSonV2(actionResult)}");
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"ProcessPlanCalculatorMoneyCloseLoanDaily_DetailLoan|LoanID={item.LoanID}|closeDate={closeDate:dd/MM/yyyy}|{ex.Message}-{ex.StackTrace}");
                    }
                }
                if (lstInsert.Count > 0)
                {
                    _planCloseLoanTab.InsertBulk(lstInsert);
                }
                if (lstUpdate.Count > 0)
                {
                    _planCloseLoanTab.UpdateBulk(lstUpdate);
                }
                response.Data = latestLoanID; // xử lý xong ID gần nhất
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessPlanCalculatorMoneyCloseLoanDaily|latestLoanID={latestLoanID}|closeDate={closeDate:dd/MM/yyyy}|{ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<MoneyNeedCloseLoan> GetMoneyCloseLoanV3(DateTime closeDate, Domain.Tables.TblLoan loanInfo, List<Domain.Tables.TblPaymentSchedule> lstPaymentScheduleAll, List<Domain.Tables.TblLoanExtra> lstLoanExtra = null, Domain.Tables.TblLender lenderInfo = null)
        {
            try
            {
                if (lstPaymentScheduleAll == null || !lstPaymentScheduleAll.Any())
                {
                    _logger.LogError($"GetMoneyCloseLoanV3|loanInfo={loanInfo.LoanID}|closeDate={closeDate:dd/MM/yyyy}|message={MessageConstant.Payment_NotFound}");
                    return null;
                }
                closeDate = closeDate.Date; // chỉ lấy tới ngày
                //var loanInfo = lstLoanInfos.First();
                // đơn đang vay mới tính tiền tất toán
                List<int> lstStatusLoanLending = new List<int> { (int)Loan_Status.Lending, (int)Loan_Status.CloseIndemnifyLoanInsurance };
                if (!lstStatusLoanLending.Contains((int)loanInfo.Status))
                {
                    return new MoneyNeedCloseLoan() { CloseType = (int)MoneyCloseLoanType.Finished };
                }
                // lịch trả nợ trong hạn
                var lstPaymentSchedule = lstPaymentScheduleAll.Where(m => m.ToDate <= loanInfo.ToDate).OrderBy(m => m.PayDate).ToList();

                // lịch trả nợ ngoài hạn
                var lstPaymentScheduleOutOfDate = lstPaymentScheduleAll.Where(m => m.ToDate > loanInfo.ToDate).OrderBy(m => m.PayDate).ToList();
                if (lenderInfo == null)
                {
                    _logger.LogError($"GetMoneyCloseLoanV3_warning_lenderinfo|loanInfo={loanInfo.LoanID}|closeDate={closeDate.ToString(TimaSettingConstant.DateTimeDayMonthYear)}");
                    lenderInfo = (await _lenderTab.WhereClause(x => x.LenderID == loanInfo.LenderID).QueryAsync()).FirstOrDefault();
                }
                long moneyInterest = 0;
                long moneyService = 0;
                long moneyConsultant = 0;
                long totalMoneyCloseLoan = 0;
                LMS.Entites.Dtos.LoanServices.MoneyNeedCloseLoan moneyNeedCloseLoan = new Entites.Dtos.LoanServices.MoneyNeedCloseLoan
                {
                    CloseType = (int)MoneyCloseLoanType.Schedule,
                    MoneyOriginal = loanInfo.TotalMoneyCurrent,
                    MoneyFineLate = loanInfo.MoneyFineLate + loanInfo.DebitMoney,
                    DebitMoney = loanInfo.DebitMoney
                };
                DateTime lastDateOfPay = loanInfo.FromDate;
                if (loanInfo.LastDateOfPay.HasValue)
                {
                    // ngày lastdate đã được tính tiền, lấy ngày tiếp theo tính lãi
                    lastDateOfPay = loanInfo.LastDateOfPay.Value.AddDays(1);
                }
                moneyNeedCloseLoan.OverDate = closeDate.Date.Subtract(loanInfo.LastDateOfPay ?? loanInfo.FromDate.AddDays(-1)).Days;

                foreach (var item in lstPaymentSchedule)
                {
                    moneyInterest += item.MoneyInterest - item.PayMoneyInterest;
                    moneyService += item.MoneyService - item.PayMoneyService;
                    moneyConsultant += item.MoneyConsultant - item.PayMoneyConsultant;

                    // lấy số tiền còn thiếu của các kỳ trước đó so với ngày phải đóng hợp đồng
                    if (item.PayDate <= closeDate)
                    {
                        moneyNeedCloseLoan.TotalMoneyReceivables += (item.MoneyInterest - item.PayMoneyInterest) + (item.MoneyService - item.PayMoneyService) + (item.MoneyConsultant - item.PayMoneyConsultant);
                    }
                }

                totalMoneyCloseLoan = moneyNeedCloseLoan.MoneyOriginal + moneyInterest + moneyService + moneyConsultant + moneyNeedCloseLoan.MoneyFineLate;
                if (closeDate == loanInfo.ToDate)
                {
                    moneyNeedCloseLoan.MoneyInterest = moneyInterest;
                    moneyNeedCloseLoan.MoneyService = moneyService;
                    moneyNeedCloseLoan.MoneyConsultant = moneyConsultant;
                    moneyNeedCloseLoan.TotalMoneyCloseLoan = totalMoneyCloseLoan;
                    moneyNeedCloseLoan.TotalInterest = moneyNeedCloseLoan.MoneyInterest + moneyNeedCloseLoan.MoneyService + moneyNeedCloseLoan.MoneyConsultant;
                    return moneyNeedCloseLoan;
                }

                if (closeDate < loanInfo.ToDate) // nếu ngày tất toán nhỏ hơn hoặc bằng ngày đáo hạn
                {
                    long totalMoneyOriginalBeforeCloseLoan = 0; // tổng tiền gốc dự kiến trước lúc tất toán - tiền đầu kỳ.
                    long totalMoneyOriginalPayBeforeCloseLoan = 0; // tổng tiền gốc phải đóng trước kỳ thanh toán

                    decimal feeFineOriginal = loanInfo.FeeFineOriginal;
                    if (loanInfo.UnitFeeFineOriginal == (int)Loan_UnitRate.Percent)
                    {
                        feeFineOriginal /= 100.0M;
                    }

                    var lstPaymentScheduleBeforeCloseLoan = lstPaymentSchedule.Where(m => m.ToDate < closeDate).ToList();
                    var lstPaymentScheduleCloseLoan = lstPaymentSchedule.Where(m => m.FromDate <= closeDate && m.ToDate >= closeDate).ToList();

                    // code AG đoạn này quăng lỗi
                    if (lstPaymentScheduleCloseLoan.Count > 1)
                    {
                        _logger.LogError($"GetMoneyCloseLoanV3|loanInfo={loanInfo.LoanID}|closeDate={closeDate:dd/MM/yyyy}|lstPaymentScheduleCloseLoan={lstPaymentScheduleCloseLoan.Count}|message=Lỗi vì có nhiều kỳ chưa thỏa AG");
                        return null;
                    }

                    var paymentScheduleCloseLoan = lstPaymentScheduleCloseLoan.FirstOrDefault();
                    if (paymentScheduleCloseLoan == null)
                    {
                        _logger.LogError($"GetMoneyCloseLoanV3|loanInfo={loanInfo.LoanID}|closeDate={closeDate:dd/MM/yyyy}|paymentScheduleCloseLoan=NULL");
                        return null;
                    }
                    // tính tổng tiền còn thiếu của các kỳ trước ngày closeloan
                    foreach (var item in lstPaymentScheduleBeforeCloseLoan)
                    {
                        moneyNeedCloseLoan.MoneyInterest += item.MoneyInterest - item.PayMoneyInterest;
                        moneyNeedCloseLoan.MoneyService += item.MoneyService - item.PayMoneyService;
                        moneyNeedCloseLoan.MoneyConsultant += item.MoneyConsultant - item.PayMoneyConsultant;
                        totalMoneyOriginalPayBeforeCloseLoan += item.MoneyOriginal;
                    }
                    var numberDayCloseLoan = closeDate.Subtract(paymentScheduleCloseLoan.FromDate).Days + 1;
                    var numberDayOfpaymentScheduleCloseLoan = paymentScheduleCloseLoan.ToDate.Subtract(paymentScheduleCloseLoan.FromDate).Days + 1;

                    totalMoneyOriginalBeforeCloseLoan = loanInfo.TotalMoneyDisbursement - totalMoneyOriginalPayBeforeCloseLoan;

                    moneyNeedCloseLoan.MoneyInterest += Convert.ToInt64(Math.Round(paymentScheduleCloseLoan.MoneyInterest * 1.0 / numberDayOfpaymentScheduleCloseLoan * numberDayCloseLoan, 0));
                    moneyNeedCloseLoan.MoneyService += Convert.ToInt64(Math.Round(paymentScheduleCloseLoan.MoneyService * 1.0 / numberDayOfpaymentScheduleCloseLoan * numberDayCloseLoan, 0));
                    moneyNeedCloseLoan.MoneyConsultant += Convert.ToInt64(Math.Round(paymentScheduleCloseLoan.MoneyConsultant * 1.0 / numberDayOfpaymentScheduleCloseLoan * numberDayCloseLoan, 0));

                    moneyNeedCloseLoan.MoneyInterest -= paymentScheduleCloseLoan.PayMoneyInterest;
                    moneyNeedCloseLoan.MoneyService -= paymentScheduleCloseLoan.PayMoneyService;
                    moneyNeedCloseLoan.MoneyConsultant -= paymentScheduleCloseLoan.PayMoneyConsultant;

                    // Nếu là của Lender Level thì mới tính phí phạt trả trước gốc
                    if (lenderInfo.SelfEmployed >= (int)Lender_SeflEmployed.LenderOut)
                    {
                        moneyNeedCloseLoan.MoneyFineOriginal = Convert.ToInt64(Math.Round(totalMoneyOriginalBeforeCloseLoan / 1M * feeFineOriginal, 0));
                    }

                    moneyNeedCloseLoan.TotalMoneyCloseLoan = moneyNeedCloseLoan.MoneyOriginal + moneyNeedCloseLoan.MoneyFineOriginal + moneyNeedCloseLoan.MoneyService + moneyNeedCloseLoan.MoneyInterest + moneyNeedCloseLoan.MoneyConsultant + moneyNeedCloseLoan.MoneyFineLate;

                    // Nếu tổng tiền trong hạn của khách hàng nhỏ hơn tổng tiền tất toán ở trên thì lấy tổng tiền trong hạn thôi
                    if (totalMoneyCloseLoan < moneyNeedCloseLoan.TotalMoneyCloseLoan)
                    {
                        moneyNeedCloseLoan.MoneyInterest = moneyInterest;
                        moneyNeedCloseLoan.MoneyService = moneyService;
                        moneyNeedCloseLoan.MoneyConsultant = moneyConsultant;
                        moneyNeedCloseLoan.TotalMoneyCloseLoan = totalMoneyCloseLoan;
                        moneyNeedCloseLoan.MoneyFineOriginal = 0;
                    }
                    // tổng lãi + phí
                    moneyNeedCloseLoan.TotalInterest = moneyNeedCloseLoan.MoneyInterest + moneyNeedCloseLoan.MoneyService + moneyNeedCloseLoan.MoneyConsultant;
                }
                else // ngày tất toán lớn hơn ngày đáo hạn
                {
                    long totalMoneyPayOriginalAfterToDate = 0;
                    // sẽ dc truyền vào
                    if (lstLoanExtra == null)
                    {
                        _logger.LogError($"GetMoneyCloseLoanV3_warning_loanextra|loanInfo={loanInfo.LoanID}|closeDate={closeDate.ToString(TimaSettingConstant.DateTimeDayMonthYear)}");
                        lstLoanExtra = new List<Domain.Tables.TblLoanExtra>();
                        //var lstLoanExtraData = await _loanExtraTab.WhereClause(m => m.LoanID == (int)loanInfo.LoanID).QueryAsync();
                        //if (lstLoanExtraData != null)
                        //{
                        //    lstLoanExtra = lstLoanExtraData.ToList();
                        //}
                    }
                    totalMoneyPayOriginalAfterToDate = lstLoanExtra.Where(m => m.PayDate.Date <= loanInfo.ToDate.Date).Sum(m => m.TotalMoney);
                    var totalMoneyOriginal = loanInfo.TotalMoneyDisbursement - (totalMoneyPayOriginalAfterToDate * -1);

                    foreach (var item in lstPaymentSchedule)
                    {
                        moneyNeedCloseLoan.MoneyInterest += item.MoneyInterest - item.PayMoneyInterest;
                        moneyNeedCloseLoan.MoneyService += item.MoneyService - item.PayMoneyService;
                        moneyNeedCloseLoan.MoneyConsultant += item.MoneyConsultant - item.PayMoneyConsultant;
                    }

                    if (lstPaymentScheduleOutOfDate != null && lstPaymentScheduleOutOfDate.Count > 0)
                    {
                        foreach (var item in lstPaymentScheduleOutOfDate)
                        {
                            moneyNeedCloseLoan.MoneyInterest += item.MoneyInterest - item.PayMoneyInterest;
                            moneyNeedCloseLoan.MoneyService += item.MoneyService - item.PayMoneyService;
                            moneyNeedCloseLoan.MoneyConsultant += item.MoneyConsultant - item.PayMoneyConsultant;
                        }
                    }
                    else
                    {
                        var lstLoanExtraAfterToDate = lstLoanExtra.Where(m => m.PayDate.Date > loanInfo.ToDate.Date).ToList();
                        var objAmountIncurred = CalculateMoneyAmountIncurred(loanInfo.ToDate.AddDays(1), closeDate, totalMoneyOriginal, lstLoanExtraAfterToDate, loanInfo);

                        moneyNeedCloseLoan.MoneyInterest += objAmountIncurred.MoneyInterest;
                        moneyNeedCloseLoan.MoneyService += objAmountIncurred.MoneyService;
                        moneyNeedCloseLoan.MoneyConsultant += objAmountIncurred.MoneyConsultant;
                    }

                    moneyNeedCloseLoan.TotalMoneyCloseLoan = moneyNeedCloseLoan.MoneyOriginal + moneyNeedCloseLoan.MoneyFineOriginal + moneyNeedCloseLoan.MoneyService + moneyNeedCloseLoan.MoneyInterest + moneyNeedCloseLoan.MoneyConsultant + moneyNeedCloseLoan.MoneyFineLate;
                    // tổng lãi + phí
                    moneyNeedCloseLoan.TotalInterest = moneyNeedCloseLoan.MoneyInterest + moneyNeedCloseLoan.MoneyService + moneyNeedCloseLoan.MoneyConsultant;
                }

                return moneyNeedCloseLoan;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetMoneyCloseLoanV3|loanInfo={loanInfo.LoanID}|closeDate={closeDate.ToString(TimaSettingConstant.DateTimeDayMonthYear)}|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }

        public async Task<SettingKeyValueSplitTransaction> ProcessSplitTransactionLoan(Domain.Tables.SettingKeyValueSplitTransaction settingKeyValue)
        {
            try
            {
                List<int> lstActionIDLMS = new List<int>
                {
                    (int)LMS.Common.Constants.Transaction_Action.DongLai,
                    (int)LMS.Common.Constants.Transaction_Action.HuyDongLai,
                    (int)LMS.Common.Constants.Transaction_Action.DongHd,
                    (int)LMS.Common.Constants.Transaction_Action.HuyDongHd,
                    (int)LMS.Common.Constants.Transaction_Action.TraGoc,
                    (int)LMS.Common.Constants.Transaction_Action.HuyTraGoc,
                    (int)LMS.Common.Constants.Transaction_Action.TraNo,
                    (int)LMS.Common.Constants.Transaction_Action.HuyTraNo,
                    (int)LMS.Common.Constants.Transaction_Action.NoLai,
                    (int)LMS.Common.Constants.Transaction_Action.HuyNoLai,
                    (int)LMS.Common.Constants.Transaction_Action.TienTuVanThuTruoc,
                    (int)LMS.Common.Constants.Transaction_Action.TraNoPhiPhatMuon,
                    (int)LMS.Common.Constants.Transaction_Action.DeleteTransactionPayMoneyFeeLate,
                    (int)LMS.Common.Constants.Transaction_Action.PhiPhatTraTruocGoc,
                    (int)LMS.Common.Constants.Transaction_Action.HuyPhiPhatTraTruocGoc,
                    (int)LMS.Common.Constants.Transaction_Action.ThanhLy,
                    (int)LMS.Common.Constants.Transaction_Action.HuyThanhLy,
                    (int)LMS.Common.Constants.Transaction_Action.ThanhLyBaoHiem,
                    (int)LMS.Common.Constants.Transaction_Action.Exemption,
                    (int)LMS.Common.Constants.Transaction_Action.PayPartial_Interest,
                    (int)LMS.Common.Constants.Transaction_Action.PayPartial_Consultant,
                    (int)LMS.Common.Constants.Transaction_Action.PayPartial_Service,
                    (int)LMS.Common.Constants.Transaction_Action.DeletePayPartial_Interest,
                    (int)LMS.Common.Constants.Transaction_Action.DeletePayPartial_Consultant,
                    (int)LMS.Common.Constants.Transaction_Action.ReceiptInsurance,
                };
                List<long> lstTxnIDLms = new List<long>();
                List<TblSplitTransactionLoan> lstTxnLmsInsert = new List<TblSplitTransactionLoan>();
                List<long> lstLoanLmsID = new List<long>();
                List<long> lstLenderID = new List<long>();
                List<int> lstMoneyTypeLenderReceived = new List<int>
                {
                    (int)LMS.Common.Constants.Transaction_TypeMoney.Original,
                    (int)LMS.Common.Constants.Transaction_TypeMoney.Interest,
                    (int)LMS.Common.Constants.Transaction_TypeMoney.Service,
                    (int)LMS.Common.Constants.Transaction_TypeMoney.FineLate,
                };

                var lstTxnLMSData = await _transactionLoanTab.SetGetTop(TimaSettingConstant.MaxItemQuery)
                                                 .SelectColumns(x => x.TransactionID, x => x.CreateDate, x => x.LoanID, x => x.LenderID, x => x.ActionID, x => x.MoneyType, x => x.TotalMoney)
                                                 .WhereClause(x => x.TransactionID > settingKeyValue.LatestTransactionID && lstActionIDLMS.Contains(x.ActionID))
                                                 .OrderBy(x => x.TransactionID).QueryAsync();
                if (lstTxnLMSData == null || !lstTxnLMSData.Any())
                {
                    settingKeyValue.IsContinous = false;
                    return settingKeyValue;
                }
                settingKeyValue.IsContinous = true;

                foreach (var item in lstTxnLMSData)
                {
                    lstTxnIDLms.Add(item.TransactionID);
                    lstLenderID.Add((long)item.LenderID);
                    lstLoanLmsID.Add((long)item.LoanID);
                    if (settingKeyValue.LatestTransactionID < item.TransactionID)
                    {
                        settingKeyValue.LatestTransactionID = item.TransactionID;
                    }
                }
                lstLoanLmsID = lstLoanLmsID.Distinct().ToList();
                lstLenderID = lstLenderID.Distinct().ToList();

                var dictLoanInfosTask = _loanTab.SelectColumns(x => x.LoanID, x => x.LenderID, x => x.RateConsultant, x => x.RateInterest, x => x.RateService)
                                            .WhereClause(x => lstLoanLmsID.Contains(x.LoanID)).QueryAsync();

                var dictLenderInfosTask = _lenderTab.SelectColumns(x => x.LenderID, x => x.SelfEmployed)
                                            .WhereClause(x => lstLenderID.Contains(x.LenderID)).QueryAsync();

                var lstSplitDataExistsTask = _splitTransactionLoanTab.GetDistinct().SelectColumns(x => x.ReferTxnID, x => x.LenderID)
                                            .WhereClause(x => lstTxnIDLms.Contains(x.ReferTxnID))
                                            .QueryAsync();

                await Task.WhenAll(dictLoanInfosTask, dictLenderInfosTask, lstSplitDataExistsTask);

                var dictLoanInfos = dictLoanInfosTask.Result.ToDictionary(x => x.LoanID, x => x);
                var dictLenderInfos = dictLenderInfosTask.Result.ToDictionary(x => x.LenderID, x => x);
                var dictSplitTransacionExistsInfos = lstSplitDataExistsTask.Result.ToDictionary(x => $"{x.LenderID}_{x.ReferTxnID}", x => x);

                foreach (var item in lstTxnLMSData)
                {
                    var keyDict = $"{item.LenderID}_{item.TransactionID}";
                    if (dictSplitTransacionExistsInfos.ContainsKey(keyDict))
                    {
                        continue;
                    }
                    try
                    {
                        // tima hưởng mặc định
                        var txnDetail = new Domain.Tables.TblSplitTransactionLoan
                        {
                            ActionID = item.ActionID,
                            CreateDate = item.CreateDate,
                            LenderID = TimaSettingConstant.ShopIDTima,
                            MoneyType = item.MoneyType,
                            LoanID = item.LoanID.Value,
                            ReferTxnID = item.TransactionID,
                            TotalMoney = item.TotalMoney
                        };
                        // số tiền lender được hưởng
                        if (lstMoneyTypeLenderReceived.Contains(item.MoneyType))
                        {
                            if (item.MoneyType == (int)LMS.Common.Constants.Transaction_TypeMoney.FineLate)
                            {
                                // kiểm tra lender lever4 m
                                var lenderDetail = dictLenderInfos.GetValueOrDefault((long)item.LenderID);
                                if (lenderDetail != null && lenderDetail.SelfEmployed < (int)LMS.Common.Constants.Lender_SeflEmployed.LenderOut && lenderDetail.LenderID > 0)
                                {
                                    var loanDetail = dictLoanInfos.GetValueOrDefault((long)item.LoanID);
                                    decimal rateLender = (decimal)loanDetail.RateInterest + (decimal)loanDetail.RateService;
                                    decimal totalRate = rateLender + (decimal)loanDetail.RateConsultant;
                                    var txnDetailLender = new TblSplitTransactionLoan
                                    {
                                        ActionID = item.ActionID,
                                        CreateDate = item.CreateDate,
                                        LenderID = item.LenderID.Value,
                                        MoneyType = item.MoneyType,
                                        LoanID = item.LoanID.Value,
                                        ReferTxnID = item.TransactionID,
                                        TotalMoney = Convert.ToInt64(item.TotalMoney * (rateLender / totalRate))
                                    };
                                    lstTxnLmsInsert.Add(txnDetailLender);
                                    _ = await _splitTransactionLoanTab.InsertAsync(txnDetailLender);
                                    txnDetail.TotalMoney -= txnDetailLender.TotalMoney;
                                }
                            }
                            else
                            {
                                txnDetail.LenderID = (long)item.LenderID;
                            }
                        }
                        _ = await _splitTransactionLoanTab.InsertAsync(txnDetail);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"ProcessSplitTransactionLoan_Insert_Waring|keydict={keyDict}|ex={ex.Message}-{ex.Message}");
                    }
                }
            }
            catch (Exception ex)
            {
                settingKeyValue.IsContinous = false;
                _logger.LogError($"ProcessSplitTransactionLoan|settingKeyValue={_common.ConvertObjectToJSonV2(settingKeyValue)}|{ex.Message}-{ex.StackTrace}");
            }

            return settingKeyValue;
        }

        public async Task<MoneyNeedCloseLoan> GetMoneyNeedPayInPeriod(DateTime currentDate, Domain.Tables.TblLoan loanInfo, List<Domain.Tables.TblPaymentSchedule> lstPaymentScheduleAll, List<Domain.Tables.TblLoanExtra> lstLoanExtra = null, Domain.Tables.TblLender lenderInfo = null)
        {
            try
            {
                if (lstPaymentScheduleAll == null || !lstPaymentScheduleAll.Any())
                {
                    _logger.LogError($"GetMoneyNeedPayInPeriod|loanInfo={loanInfo.LoanID}|closeDate={currentDate:dd/MM/yyyy}|message={MessageConstant.Payment_NotFound}");
                    return null;
                }
                LMS.Entites.Dtos.LoanServices.MoneyNeedCloseLoan moneyNeedCloseLoan = new Entites.Dtos.LoanServices.MoneyNeedCloseLoan
                {
                    CloseType = (int)MoneyCloseLoanType.Schedule,
                    MoneyOriginal = loanInfo.TotalMoneyCurrent,
                    MoneyFineLate = loanInfo.MoneyFineLate + loanInfo.DebitMoney,
                    DebitMoney = loanInfo.DebitMoney
                };
                currentDate = currentDate.Date; // chỉ lấy tới ngày
                //var loanInfo = lstLoanInfos.First();
                // đơn đang vay mới tính tiền tất toán
                List<int> lstStatusLoanLending = new List<int> { (int)Loan_Status.Lending, (int)Loan_Status.CloseIndemnifyLoanInsurance };
                if (!lstStatusLoanLending.Contains((int)loanInfo.Status))
                {
                    return new MoneyNeedCloseLoan() { CloseType = (int)MoneyCloseLoanType.Finished };
                }
                // có 3 trường hợp cần tính
                // + dpd < 0 -> trả về số tiền kỳ hiện tại
                // + dpd >=0 && <= 90: tính tổng tiền cần cắt kỳ
                // + dpd >90: trả về số tiền cần tất toán
                int dpd = currentDate.Date.Subtract(loanInfo.NextDate.Value.Date).Days;

                moneyNeedCloseLoan.OverDate = dpd;
                if (dpd > TimaSettingConstant.MaxDaySentInsurance || dpd < 0)
                {
                    // trả về số tiền tất toán
                    return moneyNeedCloseLoan;
                }

                // lịch trả nợ trong hạn
                var lstPaymentSchedule = lstPaymentScheduleAll.Where(m => m.ToDate <= loanInfo.ToDate).OrderBy(m => m.PayDate).ToList();

                long moneyOriginal = 0;
                long moneyInterest = 0;
                long moneyService = 0;
                long moneyConsultant = 0;
                long totalMoneyCloseLoan = 0;

                DateTime lastDateOfPay = loanInfo.FromDate;
                if (loanInfo.LastDateOfPay.HasValue)
                {
                    // ngày lastdate đã được tính tiền, lấy ngày tiếp theo tính lãi
                    lastDateOfPay = loanInfo.LastDateOfPay.Value.AddDays(1);
                }
                moneyNeedCloseLoan.OverDate = currentDate.Date.Subtract(loanInfo.LastDateOfPay ?? loanInfo.FromDate.AddDays(-1)).Days;

                foreach (var item in lstPaymentSchedule)
                {
                    // lấy số tiền còn thiếu của các kỳ trước đó so với ngày phải đóng hợp đồng
                    if (item.PayDate <= currentDate)
                    {
                        moneyOriginal += item.MoneyOriginal - item.PayMoneyOriginal;
                        moneyInterest += item.MoneyInterest - item.PayMoneyInterest;
                        moneyService += item.MoneyService - item.PayMoneyService;
                        moneyConsultant += item.MoneyConsultant - item.PayMoneyConsultant;
                        moneyNeedCloseLoan.TotalMoneyReceivables += (item.MoneyInterest - item.PayMoneyInterest) + (item.MoneyService - item.PayMoneyService) + (item.MoneyConsultant - item.PayMoneyConsultant);
                    }
                }

                totalMoneyCloseLoan = moneyOriginal + moneyInterest + moneyService + moneyConsultant + moneyNeedCloseLoan.MoneyFineLate;
                moneyNeedCloseLoan.MoneyOriginal = moneyOriginal;
                moneyNeedCloseLoan.MoneyInterest = moneyInterest;
                moneyNeedCloseLoan.MoneyService = moneyService;
                moneyNeedCloseLoan.MoneyConsultant = moneyConsultant;
                moneyNeedCloseLoan.TotalMoneyCloseLoan = totalMoneyCloseLoan;
                moneyNeedCloseLoan.TotalInterest = moneyNeedCloseLoan.MoneyInterest + moneyNeedCloseLoan.MoneyService + moneyNeedCloseLoan.MoneyConsultant;
                await Task.Delay(1);
                return moneyNeedCloseLoan;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetMoneyCloseLoanV3|loanInfo={loanInfo.LoanID}|closeDate={currentDate.ToString(TimaSettingConstant.DateTimeDayMonthYear)}|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }
    }
}
