﻿using LMS.Common.Constants;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Services
{
    public interface IUserServices
    {
        long GetHeaderUserID();
    }
    public class UserServices : IUserServices
    {
        IHttpContextAccessor _httpContextAccessor;
        public UserServices(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public long GetHeaderUserID()
        {
            long userID = 0;
            try
            {
                var lstHeader = _httpContextAccessor.HttpContext?.Request?.Headers;
                if (lstHeader != null && lstHeader.Any())
                {
                    var headerCheck = TimaSettingConstant.HeaderLMSUserID.ToLowerInvariant();
                    foreach (var item in lstHeader)
                    {
                        if (item.Key.ToLowerInvariant() == headerCheck)
                        {
                            userID = long.Parse(item.Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return userID;
        }
    }
}
