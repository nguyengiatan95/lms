﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.LoanServiceApi.Domain.Models.Insurance;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Services
{
    public interface ILOSManager
    {
        Task<LMS.Entites.Dtos.LOSServices.MapResultLoanWaitingDisbursementDetailLOS> GetLoanCreditDetail(long loanBriefID, int calculateMoneyInsurance = (int)CalculateMoneyInsuranceLOS.No);
    }
    public class LOSManager : ILOSManager
    {
        RestClients.IInsuranceService _insuranceService;
        RestClients.ILOSService _lOSService;
        ILogger<LOSManager> _logger;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        Common.Helper.Utils _common;
        public LOSManager(RestClients.IInsuranceService insuranceService,
            RestClients.ILOSService lOSService,
            ILogger<LOSManager> logger,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab)
        {
            _insuranceService = insuranceService;
            _lOSService = lOSService;
            _logger = logger;
            _lenderTab = lenderTab;
            _common = new Common.Helper.Utils();
        }
        public async Task<LMS.Entites.Dtos.LOSServices.MapResultLoanWaitingDisbursementDetailLOS> GetLoanCreditDetail(long loanBriefID, int calculateMoneyInsurance = (int)CalculateMoneyInsuranceLOS.No)
        {
            var result = new LMS.Entites.Dtos.LOSServices.MapResultLoanWaitingDisbursementDetailLOS();
            try
            {
                result = await _lOSService.GetLoanCreditDetail(loanBriefID);
                if (result != null)
                {
                    result.StrImcomeType = ((LOS_ImcomeType)result.ImcomeType).GetDescription();
                    var objLender = _lenderTab.WhereClause(x => x.LenderID == result.LenderId).Query().FirstOrDefault();
                    if (objLender != null)
                    {
                        result.LenderCode = objLender.FullName;
                        result.InvestorBalance = objLender.TotalMoney;
                        if (calculateMoneyInsurance == (int)CalculateMoneyInsuranceLOS.No)// không tính bảo hiểm
                            return result;

                        var rateInterestLender = _common.ConvertRateInterestLenderToVND((float)objLender.RateInterest);
                        var calculateMoneyInsuranceLOS = new CalculateMoneyInsuranceLOSReq
                        {
                            TotalMoneyDisbursement = Convert.ToInt64(result.LoanAmountFinal),
                            LoanTime = _common.ConvertLoanTimeLOSToLMS(result.LoanTime),
                            LoanFrequency = _common.ConvertFrequencyLOSToLMS(result.Frequency),
                            RateType = result.RateTypeId,
                            RateConsultant = _common.ConvertRateToVND(result.RatePercent) - rateInterestLender,
                            RateInterest = rateInterestLender,
                            RateService = 0
                        };
                        var resultInsurance = await _insuranceService.CalculateMoneyInsuranceLOS(calculateMoneyInsuranceLOS);
                        if (resultInsurance != null)
                        {
                            if (result.BuyInsurenceCustomer == true)
                                result.FeesInsuranceCustomer = resultInsurance.FeesInsuranceCustomer;
                            if (result.BuyInsuranceProperty == true)
                                result.FeesInsuranceMaterial = resultInsurance.FeesInsuranceMaterial;
                            result.TotalFeesCustomer = result.FeesInsuranceCustomer + result.FeesInsuranceMaterial;
                            result.AmountActuallyReceived = result.LoanAmountFinal - result.TotalFeesCustomer;
                        }
                        else
                        {
                            _logger.LogError($"LOSManager_CalculateMoneyInsuranceLOS_Error|lenderInfo={_common.ConvertObjectToJSonV2(objLender)}|loanDetailLos={_common.ConvertObjectToJSonV2(result)}");
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                _logger.LogError($"LOSManager_GetLoanCreditDetail_Exception|LoanBriefID={loanBriefID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return result;
        }
    }
}
