﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Kafka.Interfaces;
using LMS.LoanServiceApi.Domain.Models.Loan;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Services
{
    public interface ILoanEvenManager
    {
        Task<ResponseActionResult> CreateLoanAsync(RequestCreateLoanModel model);
        Task<ResponseActionResult> UpdateLoanAfterPayment(long loanID, long totalMoneyFineCustomerPaid = 0);

    }
    public class LoanEvenManager : ILoanEvenManager
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.LoanUpdateAfterPayment> _loanUpdateAfterPayment;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionLoanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblKafkaLogEvent> _kakfaLogEventTab;
        ILogger<LoanEvenManager> _logger;
        LMS.Common.Helper.Utils _common;
        RestClients.ICustomerService _customerService;
        RestClients.IOutGatewayService _outGateway;
        RestClients.ILenderService _lenderService;
        public LoanEvenManager(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.LoanUpdateAfterPayment> loanUpdateAfterPayment,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionLoanTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblKafkaLogEvent> kakfaLogEventTab,
             RestClients.ICustomerService customerService,
             RestClients.IOutGatewayService outGateway,
             RestClients.ILenderService lenderService,
             IKafkaProducer<string, LMS.Kafka.Messages.Customer.CreateBorrowerInfo> kafkaCustomerProducer,
             ILogger<LoanEvenManager> logger)
        {
            _loanTab = loanTab;
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new Utils();
            _paymentScheduleTab = paymentScheduleTab;
            _transactionLoanTab = transactionLoanTab;
            _customerService = customerService;
            _outGateway = outGateway;
            _lenderService = lenderService;
            _loanUpdateAfterPayment = loanUpdateAfterPayment;
            _kakfaLogEventTab = kakfaLogEventTab;
        }
        public async Task<ResponseActionResult> CreateLoanAsync(RequestCreateLoanModel request)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var loanLmsDetailExist = (await _loanTab.WhereClause(x => x.LoanCreditIDOfPartner == request.LoanCreditID).QueryAsync()).FirstOrDefault();
                if (loanLmsDetailExist != null && loanLmsDetailExist.LoanID > 0)
                {
                    response.SetSucces();
                    response.Message = MessageConstant.LoanExist;
                    response.Data = loanLmsDetailExist.LoanID;
                    return response;
                }
                LMS.Entites.Dtos.LOSServices.ResultLoanWaitingDisbursementDetailLOS loanDetailLos = await _outGateway.GetLoanCreditDetailAsync((int)request.LoanCreditID);
                if (loanDetailLos == null || loanDetailLos.LoanBriefId < 1)
                {
                    response.Message = LMS.Common.Constants.MessageConstant.LoanNotFoundLoanCreditPartner;
                    return response;
                }
                Domain.Tables.TblLender lenderDetail = null;
                long lenderID = loanDetailLos.LenderId;
                do
                {
                    var lenderInfos = _lenderTab.WhereClause(x => x.LenderID == lenderID).Query();
                    if (lenderInfos == null || lenderInfos.Count() < 1)
                    {
                        // đồng bộ lender
                        var actionResultSyncLender = await _lenderService.CreateOrUpdateLenderFromAG(loanDetailLos.LenderId);
                        if (actionResultSyncLender == null || actionResultSyncLender.Result != (int)ResponseAction.Success)
                        {
                            response.Message = LMS.Common.Constants.MessageConstant.LoanNotFoundLender;
                            return response;
                        }
                        else
                        {
                            lenderID = Convert.ToInt64(actionResultSyncLender.Data);
                        }
                    }
                    else
                    {
                        lenderDetail = lenderInfos.First();
                    }

                } while (lenderDetail == null);


                var lstSourceByInsuranceIDs = ((Loan_SourcecBuyInsurance[])Enum.GetValues(typeof(Loan_SourcecBuyInsurance))).Select(c => (int)c).ToList();
                if (!lstSourceByInsuranceIDs.Contains(loanDetailLos.TypeInsurence))
                {
                    // log warning
                    _logger.LogWarning($"CreateLoanCommandHandler_Warning|TypeInsurence={loanDetailLos.TypeInsurence}|detaiLos={_common.ConvertObjectToJSonV2(loanDetailLos)}");
                }

                // tạo khách hàng
                // bảng customer tham chiếu theo loanID
                // loan lưu lại customerID từ LOS mới đúng
                var customerID = await _customerService.CreateInfoBorrower(new Domain.Models.CreateBorrowerCommandRequest
                {
                    Address = loanDetailLos.CustomerAddress,
                    CityID = loanDetailLos.HouseOldCityId,
                    DistrictID = loanDetailLos.HouseOldDistrictId,
                    NumberCard = loanDetailLos.NationalCard,
                    CustomerName = loanDetailLos.FullName,
                    Phone = loanDetailLos.Phone,
                    Gender = loanDetailLos.Gender,
                    PermanentAddress = loanDetailLos.AddressNationalCard,
                    AddressHouseHold = loanDetailLos.HouseOldAddress,
                    BirthDay = loanDetailLos.Dob == DateTime.MinValue ? "" : loanDetailLos.Dob.ToString(TimaSettingConstant.DateTimeDayMonthYear),
                    TimaCustomerID = request.TimaCustomerID
                });

                if (customerID < 1)
                {
                    response.Message = LMS.Common.Constants.MessageConstant.LoanCreateCustomerFail;
                    return response;
                }

                var currentDate = DateTime.Now;
                var totalMoneyInterest = _common.ConvertRateToVND(loanDetailLos.RatePercent);
                long moneyInterest = 0;
                switch ((Lender_UnitRate)lenderDetail.UnitRate)
                {
                    case Lender_UnitRate.Percent:
                        moneyInterest = _common.ConvertRateToVND(lenderDetail.RateInterest);
                        break;
                    default:
                        moneyInterest = Convert.ToInt64(lenderDetail.RateInterest * TimaSettingConstant.UnitMillionMoney);
                        break;
                }
                var timaCodeID = (_loanTab.SetGetTop(1).SelectColumns(x => x.TimaCodeID).WhereClause(x => x.OwnerShopID == TimaSettingConstant.ShopIDTima).OrderByDescending(x => x.TimaCodeID).Query().FirstOrDefault()?.TimaCodeID ?? 0) + 1;
                if (request.AgCodeID > 0)
                {
                    // log 1 thời gian để check dữ liệu ag
                    if (timaCodeID != request.AgCodeID)
                    {
                        _logger.LogError($"CreateLoanAsync_Warning|AgCodeID={request.AgCodeID}|LmsCodeID={timaCodeID}");
                    }
                    timaCodeID = request.AgCodeID;
                }
                // tạo đơn vay
                Domain.Tables.LoanJsonExtra loanJsonExtra = new Domain.Tables.LoanJsonExtra
                {
                    DistrictName = loanDetailLos.DistrictName,
                    CityName = loanDetailLos.ProvinceName,
                    ShopName = lenderDetail.FullName,
                    ConsultantShopName = loanDetailLos.ShopName,
                    TopUpOfLoanBriefID = loanDetailLos.TopUpOfLoanBriefID,
                    IsLocate = loanDetailLos.IsLocate,
                    LoanBriefPropertyPlateNumber = loanDetailLos.LoanBriefProperty?.PlateNumber
                };
                var fromDate = request.FromDate ?? currentDate;
                var loanInfo = new Domain.Tables.TblLoan
                {
                    CustomerID = customerID, // xử lý sau
                    FromDate = fromDate,
                    ToDate = fromDate.AddMonths(loanDetailLos.LoanTime).AddDays(-1),
                    CityID = loanDetailLos.ProvinceId,
                    DistrictID = loanDetailLos.DistrictId,
                    TotalMoneyDisbursement = (long)loanDetailLos.LoanAmountFinal,
                    LoanTime = loanDetailLos.LoanTime,
                    Frequency = loanDetailLos.Frequency,
                    RateType = loanDetailLos.RateTypeId,
                    CreateDate = currentDate,
                    ModifyDate = currentDate,
                    BankCardID = request.BankCardID,
                    ConsultantShopID = loanDetailLos.ShopId,
                    ContactCode = _common.GenLoanContactCode(timaCodeID, "0"),
                    CustomerName = loanDetailLos.FullName,
                    MoneyFeeInsuranceOfCustomer = request.MoneyfeeInsuranceOfCustomer,
                    MoneyFeeInsuranceMaterialCovered = request.MoneyFeeInsuranceMaterialCovered,
                    MoneyFeeInsuranceOfLender = 0,
                    LenderID = loanDetailLos.LenderId,
                    NextDate = currentDate.AddMonths(1),
                    OwnerShopID = TimaSettingConstant.ShopIDTima,
                    ProductID = loanDetailLos.ProductId,
                    RateConsultant = totalMoneyInterest - moneyInterest,
                    RateInterest = moneyInterest,
                    RateService = 0, // chưa có chính sách
                    SourcecBuyInsurance = loanDetailLos.TypeInsurence,
                    Status = (int)Loan_Status.Lending,
                    TotalMoneyCurrent = (long)loanDetailLos.LoanAmountFinal,
                    SourceMoneyDisbursement = request.SourceBankDisbursement,
                    WardID = loanDetailLos.HouseOldWardId,
                    LoanCreditIDOfPartner = loanDetailLos.LoanBriefId,
                    ProductName = loanDetailLos.ProductName,
                    UnitRate = (int)Loan_UnitRate.Default,
                    TimaLoanID = request.LoanAgID,
                    JsonExtra = _common.ConvertObjectToJSonV2(loanJsonExtra),
                    TimaCodeID = timaCodeID,
                    FeeFineOriginal = loanDetailLos.FeePaymentBeforeLoan * 100,
                    UnitFeeFineOriginal = (int)Loan_UnitRate.Percent
                };
                loanInfo.LoanID = await _loanTab.InsertAsync(loanInfo);

                if (loanInfo.LoanID > 0)
                {
                    if (string.IsNullOrEmpty(request.Note))
                    {
                        request.Note = string.Format(TimaSettingConstant.SystemDisbursementToCustomer, loanDetailLos.FullName.ToUpper());
                    }
                    _ = await _kakfaLogEventTab.InsertAsync(new Domain.Tables.TblKafkaLogEvent
                    {
                        CreateDate = DateTime.Now,
                        ModifyDate = DateTime.Now,
                        DataMessage = _common.ConvertObjectToJSonV2(new Kafka.Messages.Loan.LoanInsertSuccess
                        {
                            LoanID = loanInfo.LoanID,
                            CreateBy = request.CreateBy,
                            Note = request.Note
                        }),
                        ServiceName = ServiceHostInternal.LoanService,
                        Status = (int)KafkaLogEvent_Status.Waitting,
                        TopicName = LMS.Kafka.Constants.KafkaTopics.LoanCreateSuccess
                    });
                    if (request.LoanAgID == 0)
                    {
                        //// đơn tạo từ LMS chưa bắn qua cho LOS
                        //_ = _losService.DisbursementLoan(new Entites.Dtos.LOSServices.DisbursementLoanReq
                        //{
                        //    CodeId = (int)timaCodeID,
                        //    FeeInsuranceOfCustomer = (long)loanInfo.MoneyFeeInsuranceOfCustomer,
                        //    LoanBriefId = request.LoanCreditID,
                        //    LoanId = loanInfo.LoanID,
                        //    Type = (int)LOS_Enum_DisbursementType.CreateLoan,
                        //    IsHandleException = request.IsHandleException,
                        //    FeeInsuranceOfProperty = loanInfo.MoneyFeeInsuranceMaterialCovered
                        //});
                    }
                    response.SetSucces();
                    response.Data = loanInfo.LoanID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateLoanAsync|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> UpdateLoanAfterPayment(long loanID, long totalMoneyFineCustomerPaid = 0)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var loanInfo = (await _loanUpdateAfterPayment.WhereClause(x => x.LoanID == loanID).QueryAsync()).FirstOrDefault();
                if (loanInfo == null || loanInfo.LoanID < 1)
                {
                    response.Message = MessageConstant.LoanNotExist;
                    _logger.LogError($"UpdateLoanAfterPayment_WarningLoanNotFound|loanID={loanID}|message={response.Message}");
                    return response;
                }
                var currentDate = DateTime.Now;
                // cập nhật lại ngày thanh toán cho đơn vay
                var lstPaymentScheduleTask = _paymentScheduleTab.WhereClause(x => x.LoanID == loanInfo.LoanID && x.Status == (int)PaymentSchedule_Status.Use).QueryAsync();
                var lstTransactionLoanOriginal = _transactionLoanTab.SelectColumns(x => x.TotalMoney)
                                                                    .WhereClause(x => x.LoanID == loanInfo.LoanID && x.ActionID != (int)Transaction_Action.ChoVay && x.MoneyType == (int)Transaction_TypeMoney.Original)
                                                                    .QueryAsync();

                await Task.WhenAll(lstPaymentScheduleTask, lstTransactionLoanOriginal);

                var lstPaymentSchedule = lstPaymentScheduleTask.Result;
                long totalMoneyOriginalPaid = lstTransactionLoanOriginal.Result.Sum(x => x.TotalMoney);

                var paymentDetailNext = lstPaymentSchedule.Where(x => x.IsComplete == (int)PaymentSchedule_IsComplete.Waiting && x.IsVisible == (int)PaymentSchedule_IsVisible.Show).OrderBy(x => x.PaymentScheduleID).FirstOrDefault();
                if (paymentDetailNext == null)
                {
                    _logger.LogError($"UpdateLoanAfterPayment_WarningPaymentDetailNextNotFound|loanID={loanID}");
                    // hết kỳ đóng tiền
                    loanInfo.LastDateOfPay = loanInfo.NextDate;
                    loanInfo.ModifyDate = currentDate;
                }
                else if (paymentDetailNext.PayDate > loanInfo.NextDate)
                {
                    loanInfo.LastDateOfPay = paymentDetailNext.FromDate.AddDays(-1);
                    loanInfo.NextDate = paymentDetailNext.PayDate;
                    loanInfo.ModifyDate = currentDate;
                }
                loanInfo.TotalMoneyCurrent = loanInfo.TotalMoneyDisbursement - totalMoneyOriginalPaid;
                if (totalMoneyFineCustomerPaid > 0)
                {
                    loanInfo.MoneyFineLate -= totalMoneyFineCustomerPaid;
                }
                if (!(await _loanUpdateAfterPayment.UpdateAsync(loanInfo)))
                {
                    _logger.LogError($"UpdateLoanAfterPayment_UpdateLoanFail|loanID={loanID}");
                }
                else
                {
                    response.SetSucces();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateLoanAfterPayment|loanID={loanID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
