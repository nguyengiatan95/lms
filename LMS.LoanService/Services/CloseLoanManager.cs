﻿using LMS.Common.Constants;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Services
{
    public interface ICloseLoanManager
    {
        LMS.Common.Constants.Loan_Status TypeCloseLoan { get; }
        int GetStatusLoan();
        Task<ResponseActionResult> CloseLoan(long loanID, long moneyOriginal, long moneyService, long moneyInterest, long moneyConsultant, long moneyFineLate, long moneyFineOriginal, DateTime closeDate, long createBy, long insurancePartnerCode);
    }
    public class TypeCloseLoanFinalization : ICloseLoanManager
    {
        ILogger<TypeCloseLoanFinalization> _logger;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        RestClients.IPaymentScheduleService _paymentScheduleService;
        public TypeCloseLoanFinalization(ILogger<TypeCloseLoanFinalization> logger,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            RestClients.IPaymentScheduleService paymentScheduleService
            )
        {
            _logger = logger;
            _loanTab = loanTab;
            _paymentScheduleService = paymentScheduleService;
        }

        public Loan_Status TypeCloseLoan => Loan_Status.Close;

        public async Task<ResponseActionResult> CloseLoan(long loanID, long moneyOriginal, long moneyService, long moneyInterest, long moneyConsultant, long moneyFineLate, long moneyFineOriginal, DateTime closeDate, long createBy, long insurancePartnerCode)
        {
            ResponseActionResult responseAction = new ResponseActionResult();
            try
            {
                //  call service đóng hợp đồng
                responseAction = await _paymentScheduleService.ForceCloseLoan(loanID, moneyOriginal, moneyService, moneyInterest, moneyConsultant, moneyFineLate, moneyFineOriginal, closeDate, createBy, insurancePartnerCode, (int)Loan_Status.Close);
            }
            catch (Exception ex)
            {
                _logger.LogError($"TypeCloseLoanFinalization|loanID={loanID}|moneyOriginal={moneyOriginal}|moneyIntereset={moneyInterest}|moneyFineLate={moneyFineLate}|moneyFineOriginal={moneyFineOriginal}|closeDate={closeDate}|createBy={createBy}|insurancePartnerCode={insurancePartnerCode}|ex={ex.Message}-{ex.StackTrace}");
            }
            return responseAction;
        }

        public int GetStatusLoan()
        {
            return (int)Loan_Status.Close;
        }
    }

    public class TypeCloseLoanLiquidation : ICloseLoanManager
    {
        ILogger<TypeCloseLoanLiquidation> _logger;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        RestClients.IPaymentScheduleService _paymentScheduleService;
        public TypeCloseLoanLiquidation(ILogger<TypeCloseLoanLiquidation> logger,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            RestClients.IPaymentScheduleService paymentScheduleService
            )
        {
            _logger = logger;
            _loanTab = loanTab;
            _paymentScheduleService = paymentScheduleService;
        }

        public Loan_Status TypeCloseLoan => Loan_Status.CloseLiquidation;

        public async Task<ResponseActionResult> CloseLoan(long loanID, long moneyOriginal, long moneyService, long moneyInterest, long moneyConsultant, long moneyFineLate, long moneyFineOriginal, DateTime closeDate, long createBy, long insurancePartnerCode)
        {
            ResponseActionResult responseAction = new ResponseActionResult();
            try
            {
                //  call service đóng hợp đồng
                responseAction = await _paymentScheduleService.ForceCloseLoan(loanID, moneyOriginal, moneyService, moneyInterest, moneyConsultant, moneyFineLate, moneyFineOriginal, closeDate, createBy, insurancePartnerCode, (int)Loan_Status.CloseLiquidation);
            }
            catch (Exception ex)
            {
                _logger.LogError($"TypeCloseLoanLiquidation|loanID={loanID}|moneyOriginal={moneyOriginal}|moneyIntereset={moneyInterest}|moneyFineLate={moneyFineLate}|moneyFineOriginal={moneyFineOriginal}|closeDate={closeDate}|createBy={createBy}|insurancePartnerCode={insurancePartnerCode}|ex={ex.Message}-{ex.StackTrace}");
            }
            return responseAction;
        }

        public int GetStatusLoan()
        {
            return (int)Loan_Status.CloseLiquidation;
        }
    }

    public class TypeCloseLoanLiquidationLoanInsurance : ICloseLoanManager
    {
        ILogger<TypeCloseLoanLiquidationLoanInsurance> _logger;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        RestClients.IPaymentScheduleService _paymentScheduleService;
        public TypeCloseLoanLiquidationLoanInsurance(ILogger<TypeCloseLoanLiquidationLoanInsurance> logger,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            RestClients.IPaymentScheduleService paymentScheduleService
            )
        {
            _logger = logger;
            _loanTab = loanTab;
            _paymentScheduleService = paymentScheduleService;
        }

        public Loan_Status TypeCloseLoan => Loan_Status.CloseLiquidationLoanInsurance;

        public async Task<ResponseActionResult> CloseLoan(long loanID, long moneyOriginal, long moneyService, long moneyInterest, long moneyConsultant, long moneyFineLate, long moneyFineOriginal, DateTime closeDate, long createBy, long insurancePartnerCode)
        {
            ResponseActionResult responseAction = new ResponseActionResult();
            try
            {
                //  call service đóng hợp đồng
                responseAction = await _paymentScheduleService.ForceCloseLoan(loanID, moneyOriginal, moneyService, moneyInterest, moneyConsultant, moneyFineLate, moneyFineOriginal, closeDate, createBy, insurancePartnerCode, (int)Loan_Status.CloseLiquidationLoanInsurance);
            }
            catch (Exception ex)
            {
                _logger.LogError($"TypeCloseLoanLiquidationLoanInsurance|loanID={loanID}|moneyOriginal={moneyOriginal}|moneyIntereset={moneyInterest}|moneyFineLate={moneyFineLate}|moneyFineOriginal={moneyFineOriginal}|closeDate={closeDate}|createBy={createBy}|insurancePartnerCode={insurancePartnerCode}|ex={ex.Message}-{ex.StackTrace}");
            }
            return responseAction;
        }

        public int GetStatusLoan()
        {
            return (int)Loan_Status.CloseLiquidationLoanInsurance;
        }
    }

    public class TypeCloseLoanIndemnifyLoanInsurance : ICloseLoanManager
    {
        ILogger<TypeCloseLoanIndemnifyLoanInsurance> _logger;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanIndemnifyInsurance> _loanIndemnifyInsuranceTab;
        public TypeCloseLoanIndemnifyLoanInsurance(ILogger<TypeCloseLoanIndemnifyLoanInsurance> logger,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanIndemnifyInsurance> loanIndemnifyInsuranceTab
            )
        {
            _logger = logger;
            _loanTab = loanTab;
            _loanIndemnifyInsuranceTab = loanIndemnifyInsuranceTab;
        }
        public Loan_Status TypeCloseLoan => Loan_Status.CloseIndemnifyLoanInsurance;

        public async Task<ResponseActionResult> CloseLoan(long loanID, long moneyOriginal, long moneyService, long moneyInterest, long moneyConsultant, long moneyFineLate, long moneyFineOriginal, DateTime closeDate, long createBy, long insurancePartnerCode)
        {
            ResponseActionResult responseAction = new ResponseActionResult();
            try
            {
                DateTime dtNow = DateTime.Now;
                var objLoan = (await _loanTab.WhereClause(x => x.LoanID == loanID).QueryAsync()).FirstOrDefault();
                _ = _loanIndemnifyInsuranceTab.InsertAsync(new Domain.Tables.TblLoanIndemnifyInsurance()
                {
                    LoanId = objLoan.LoanID,
                    TotalMoneyCurrent = moneyOriginal,
                    CreateOn = dtNow,
                    ContactCode = objLoan.ContactCode,
                    InsuranceCompensatorID = (int)insurancePartnerCode,
                    MoneyIndemnification = moneyOriginal + moneyInterest + moneyConsultant + moneyFineLate + moneyFineOriginal,
                    MoneyInterest = moneyInterest + moneyConsultant + moneyFineLate + moneyFineOriginal,
                    UserID = (int)createBy,
                    Status = (int)StatusCommon.Active
                });
                //  call service đóng hợp đồng
                responseAction.SetSucces();
                long totalMoneyCustomerPaid = 0;
                responseAction.Data = totalMoneyCustomerPaid;
            }
            catch (Exception ex)
            {
                _logger.LogError($"TypeCloseLoanIndemnifyLoanInsurance|loanID={loanID}|moneyOriginal={moneyOriginal}|moneyIntereset={moneyInterest}|moneyFineLate={moneyFineLate}|moneyFineOriginal={moneyFineOriginal}|closeDate={closeDate}|createBy={createBy}|insurancePartnerCode={insurancePartnerCode}|ex={ex.Message}-{ex.StackTrace}");
            }
            return responseAction;
        }

        public int GetStatusLoan()
        {
            return (int)Loan_Status.CloseIndemnifyLoanInsurance;
        }
    }

    public class TypeCloseLoanPaidInsurance : ICloseLoanManager
    {
        ILogger<TypeCloseLoanPaidInsurance> _logger;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        public TypeCloseLoanPaidInsurance(ILogger<TypeCloseLoanPaidInsurance> logger,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab
            )
        {
            _logger = logger;
            _loanTab = loanTab;
        }
        public Loan_Status TypeCloseLoan => Loan_Status.ClosePaidInsurance;

        public async Task<ResponseActionResult> CloseLoan(long loanID, long moneyOriginal, long moneyService, long moneyInterest, long moneyConsultant, long moneyFineLate, long moneyFineOriginal, DateTime closeDate, long createBy, long insurancePartnerCode)
        {
            ResponseActionResult responseAction = new ResponseActionResult();
            try
            {
                responseAction.SetSucces();
                long totalMoneyCustomerPaid = 0;
                responseAction.Data = totalMoneyCustomerPaid;
                await Task.Delay(1);
            }
            catch (Exception ex)
            {
                _logger.LogError($"TypeCloseLoanPaidInsurance|loanID={loanID}|moneyOriginal={moneyOriginal}|moneyIntereset={moneyInterest}|moneyFineLate={moneyFineLate}|moneyFineOriginal={moneyFineOriginal}|closeDate={closeDate}|createBy={createBy}|insurancePartnerCode={insurancePartnerCode}|ex={ex.Message}-{ex.StackTrace}");
            }
            return responseAction;
        }

        public int GetStatusLoan()
        {
            return (int)Loan_Status.ClosePaidInsurance;
        }
    }

    public class TypeCloseLoanExemption : ICloseLoanManager
    {
        ILogger<TypeCloseLoanExemption> _logger;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        RestClients.IPaymentScheduleService _paymentScheduleService;
        public TypeCloseLoanExemption(ILogger<TypeCloseLoanExemption> logger,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            RestClients.IPaymentScheduleService paymentScheduleService
            )
        {
            _logger = logger;
            _loanTab = loanTab;
            _paymentScheduleService = paymentScheduleService;
        }

        public Loan_Status TypeCloseLoan => Loan_Status.CloseExemption;

        public async Task<ResponseActionResult> CloseLoan(long loanID, long moneyOriginal, long moneyService, long moneyInterest, long moneyConsultant, long moneyFineLate, long moneyFineOriginal, DateTime closeDate, long createBy, long insurancePartnerCode)
        {
            ResponseActionResult responseAction = new ResponseActionResult();
            try
            {
                //  call service đóng hợp đồng
                responseAction = await _paymentScheduleService.ForceCloseLoan(loanID, moneyOriginal, moneyService, moneyInterest, moneyConsultant, moneyFineLate, moneyFineOriginal, closeDate, createBy, insurancePartnerCode, (int)Loan_Status.CloseExemption);
            }
            catch (Exception ex)
            {
                _logger.LogError($"TypeCloseLoanExemption|loanID={loanID}|moneyOriginal={moneyOriginal}|moneyIntereset={moneyInterest}|moneyFineLate={moneyFineLate}|moneyFineOriginal={moneyFineOriginal}|closeDate={closeDate}|createBy={createBy}|insurancePartnerCode={insurancePartnerCode}|ex={ex.Message}-{ex.StackTrace}");
            }
            return responseAction;
        }

        public int GetStatusLoan()
        {
            return (int)Loan_Status.CloseExemption;
        }
    }
    public class TypeCloseLoanPaidInsuranceExemption : ICloseLoanManager
    {
        ILogger<TypeCloseLoanPaidInsuranceExemption> _logger;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        RestClients.IPaymentScheduleService _paymentScheduleService;
        public TypeCloseLoanPaidInsuranceExemption(ILogger<TypeCloseLoanPaidInsuranceExemption> logger,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            RestClients.IPaymentScheduleService paymentScheduleService
            )
        {
            _logger = logger;
            _loanTab = loanTab;
            _paymentScheduleService = paymentScheduleService;
        }
        public Loan_Status TypeCloseLoan => Loan_Status.ClosePaidInsuranceExemption;

        public async Task<ResponseActionResult> CloseLoan(long loanID, long moneyOriginal, long moneyService, long moneyInterest, long moneyConsultant, long moneyFineLate, long moneyFineOriginal, DateTime closeDate, long createBy, long insurancePartnerCode)
        {
            ResponseActionResult responseAction = new ResponseActionResult();
            try
            {               
                //  call service đóng hợp đồng
                responseAction = await _paymentScheduleService.ForceCloseLoan(loanID, moneyOriginal, moneyService, moneyInterest, moneyConsultant, moneyFineLate, moneyFineOriginal, closeDate, createBy, insurancePartnerCode, (int)Loan_Status.ClosePaidInsuranceExemption);
            }
            catch (Exception ex)
            {
                _logger.LogError($"TypeCloseLoanPaidInsuranceExemption|loanID={loanID}|moneyOriginal={moneyOriginal}|moneyIntereset={moneyInterest}|moneyFineLate={moneyFineLate}|moneyFineOriginal={moneyFineOriginal}|closeDate={closeDate}|createBy={createBy}|insurancePartnerCode={insurancePartnerCode}|ex={ex.Message}-{ex.StackTrace}");
            }
            return responseAction;
        }

        public int GetStatusLoan()
        {
            return (int)Loan_Status.ClosePaidInsuranceExemption;
        }
    }
}
