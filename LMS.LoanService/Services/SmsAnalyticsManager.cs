﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.Sms;
using LMS.LoanServiceApi.Domain.Models.SMS;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Services
{
    public interface ISmsAnalyticsManager
    {
        Task<LMS.Common.Constants.ResponseActionResult> CreateSmsTransactionViaWallet(SmsIncreatedMoneyCustomerRequest request);
        Task<LMS.Common.Constants.ResponseActionResult> CreateSmsTransactionBank(SmsTransactionBankRequest request);
        Task<LMS.Common.Constants.ResponseActionResult> CreateSMSOTPDisbursement(SmsTransactionBankRequest request);
        Task<LMS.Common.Constants.ResponseActionResult> UpdateStatusSmsTransactionBank(UpdateStatusSmsTransactionBankRequest request);

        Task<IEnumerable<Entites.Dtos.Sms.SmsItemViewModel>> GetLstSmsInfoByCondition(RequestSmsItemViewModel request);
        Task<LMS.Common.Constants.ResponseActionResult> ProcessSmsAnalytic();
    }
    public class SmsAnalyticsManager : ISmsAnalyticsManager
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSmsAnalytics> _smsAnalyticsTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSmsDisbursement> _smsDisbursementTab;
        LMS.Common.Helper.Utils _common;
        public const int BankCardID_MOMO = 69;
        public const int BankCardID_NCB = 82;
        public const int BankCardID_GPAY = 84;
        public const int BankCardID_GUTINA = 85;
        public const int BankCardID_GPayVA = 86;
        public const int BankCardID_VA = 83;
        public const string BankCardAliasName_NCB = "NCB1";
        public const string MomoName = "MOMO";
        public const string SenderNCBName = "NHQUOCDAN";
        public const string GPAYName = "GPAY";
        public const string GUTINAName = "GUTINA";
        public const string GPayVAName = "GPay_VA";
        public const string BankCardVAName = "EPAY";
        List<int> _lstBankCardID;
        List<string> _lstBankCardName;
        ILogger<SmsAnalyticsManager> _logger;
        IEnumerable<Services.IReadSmsAnalyticProvider> _readSmsAnalyticProviders;
        RestClients.IInvoiceService _invoiceService;
        public SmsAnalyticsManager(LMS.Common.DAL.ITableHelper<Domain.Tables.TblSmsAnalytics> smsAnalyticsTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSmsDisbursement> smsDisbursementTab,
            IEnumerable<Services.IReadSmsAnalyticProvider> readSmsAnalyticProviders,
            RestClients.IInvoiceService invoiceService,
            ILogger<SmsAnalyticsManager> logger
            )
        {
            _smsAnalyticsTab = smsAnalyticsTab;
            _customerTab = customerTab;
            _bankCardTab = bankCardTab;
            _loanTab = loanTab;
            _smsDisbursementTab = smsDisbursementTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _lstBankCardID = new List<int> { BankCardID_MOMO, BankCardID_GPAY, BankCardID_GUTINA, BankCardID_GPayVA, BankCardID_VA };
            _lstBankCardName = new List<string> { MomoName, GPAYName, GUTINAName, GPayVAName, BankCardVAName };
            _readSmsAnalyticProviders = readSmsAnalyticProviders;
            _invoiceService = invoiceService;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateSmsTransactionViaWallet(SmsIncreatedMoneyCustomerRequest request)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            try
            {
                if (!string.IsNullOrEmpty(request.CustomerNumberCard) && !string.IsNullOrEmpty(request.CustomerPhone))
                {
                    _customerTab.WhereClause(x => x.NumberCard == request.CustomerNumberCard);
                }
                else
                {
                    _customerTab.WhereClause(x => x.CustomerID == request.CustomerID);
                }
                var lstCustomerInfos = await _customerTab.QueryAsync();
                if (lstCustomerInfos == null || !lstCustomerInfos.Any())
                {
                    _logger.LogError($"CreateSmsTransactionViaWallet_NotFound|request={_common.ConvertObjectToJSonV2(request)}");
                    return response;
                }
                var customerInfo = lstCustomerInfos.First();
                var receivedDate = DateTime.ParseExact(request.ReceivedDate, TimaSettingConstant.DateTimeDayMonthYearFull, null);
                var smsContent = $"Khách {customerInfo.FullName}, CMT:{customerInfo.NumberCard} chuyển qua {_lstBankCardName[request.Source]} lúc {receivedDate:dd/MM/yyyy HH:mm} số tiền: {request.TotalMoney}, mã tham chiếu: {request.ReferenceID}";
                var smsCotentHash = _common.HashMD5(smsContent);
                var smsExist = (await _smsAnalyticsTab.WhereClause(x => x.SmsContentHashIndex == smsCotentHash).QueryAsync()).FirstOrDefault();
                if (smsExist != null && smsExist.SmsAnalyticsID > 0)
                {
                    response.Message = MessageConstant.SMS_ContentDouble;
                    return response;
                }
                var currentDate = DateTime.Now;
                var sms = new Domain.Tables.TblSmsAnalytics()
                {
                    CreateDate = DateTime.Now,
                    Sender = _lstBankCardName[request.Source],
                    SmsContent = smsContent,
                    IncreaseMoney = request.TotalMoney,
                    CustomerName = customerInfo.FullName,
                    IdCard = customerInfo.NumberCard,
                    Phone = customerInfo.Phone,
                    Status = (int)Common.Constants.SmsAnalytics_Status.Pending,//(int)Entity.Extend.SmsStatusAnalytics.ChuaPhanTich,
                    AcountNumber = null,
                    LoanID = 0,
                    LoanCodeID = 0,
                    CustomerID = customerInfo.CustomerID,
                    BankCardID = _lstBankCardID[request.Source],
                    BankCardAliasName = _lstBankCardName[request.Source],
                    TypeTransactionBank = 1,// (int)Entity.Extend.TypeTransactionBankCard.ChargingMoney,
                    NameTransactionBank = "Nạp tiền",//Entity.Extend.TypeTransactionBankCard.ChargingMoney.GetDescription(),
                    TransactionBankCardStatus = 0,
                    IsAnalytics = 0,// (int)Entity.Extend.SmsAnalytics_IsAnalytics.ChuaPhanTich,
                    SmsContentHashIndex = smsCotentHash,
                    SmsreceivedDate = receivedDate,
                    SmsType = (int)Constants.SmsType.Tima,
                    SmsTimaID = request.SmsTimaID
                };
                var id = await _smsAnalyticsTab.InsertAsync(sms);
                if (id > 0)
                {
                    response.SetSucces();
                    response.Data = id;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateSmsTransactionViaWallet|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<LMS.Common.Constants.ResponseActionResult> CreateSmsTransactionBank(SmsTransactionBankRequest request)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            try
            {
                var receivedDate = DateTime.ParseExact(request.ReceivedDate, "dd/MM/yyyy HH:mm:ss", null);
                var smsCotentHash = _common.HashMD5($"{request.SmsContent}-{receivedDate:dd/MM/yyyy HH:mm}");
                var smsExist = (await _smsAnalyticsTab.WhereClause(x => x.SmsContentHashIndex == smsCotentHash).QueryAsync()).FirstOrDefault();
                if (smsExist != null && smsExist.SmsAnalyticsID > 0)
                {
                    response.Message = MessageConstant.SMS_ContentDouble;
                    return response;
                }
                var currentDate = DateTime.Now;
                var sms = new Domain.Tables.TblSmsAnalytics()
                {
                    CreateDate = currentDate,
                    ModifyDate = currentDate,
                    Sender = request.Sender,
                    SmsContent = request.SmsContent,
                    BankCardID = request.BankCardID,
                    Status = (int)Common.Constants.SmsAnalytics_Status.Pending,
                    SmsContentHashIndex = smsCotentHash,
                    SmsreceivedDate = receivedDate,
                    SmsType = (int)Constants.SmsType.Tima,
                    SmsTimaID = request.SmsTimaID
                };
                var id = await _smsAnalyticsTab.InsertAsync(sms);
                if (id > 0)
                {
                    response.SetSucces();
                    response.Data = id;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateSmsTransactionBank|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<LMS.Common.Constants.ResponseActionResult> UpdateStatusSmsTransactionBank(UpdateStatusSmsTransactionBankRequest request)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            try
            {
                if (request.SmsAnalyticID > 0)
                {
                    _smsAnalyticsTab.WhereClause(x => x.SmsAnalyticsID == request.SmsAnalyticID);
                }
                else if (request.SmsTimaID > 0)
                {
                    _smsAnalyticsTab.WhereClause(x => x.SmsTimaID == request.SmsTimaID);
                }
                else
                {
                    return response;
                }
                var smsDetail = (await _smsAnalyticsTab.QueryAsync()).FirstOrDefault();
                if (smsDetail == null || smsDetail.SmsAnalyticsID < 1)
                {
                    return response;
                }
                if (request.TimaLoanID > 0)
                {
                    var loanInfoTask = _loanTab.WhereClause(x => x.TimaLoanID == request.TimaLoanID).QueryAsync();
                    var bankCardInfoTask = _bankCardTab.WhereClause(x => x.TimaBankCardID == request.TimaBankCardID).QueryAsync();
                    await Task.WhenAll(loanInfoTask, bankCardInfoTask);
                    smsDetail.BankCardID = bankCardInfoTask.Result.FirstOrDefault()?.BankCardID ?? request.TimaBankCardID;
                    smsDetail.BankCardAliasName = bankCardInfoTask.Result.FirstOrDefault()?.AliasName;
                    smsDetail.LoanCodeID = Convert.ToInt32(loanInfoTask.Result.FirstOrDefault()?.ContactCode.Replace(TimaSettingConstant.PrefixContractCode, ""));
                    smsDetail.LoanID = loanInfoTask.Result.FirstOrDefault()?.LoanID ?? request.TimaLoanID;
                    smsDetail.CustomerID = loanInfoTask.Result.FirstOrDefault()?.CustomerID ?? request.TimaCustomerID;
                    smsDetail.CustomerName = loanInfoTask.Result.FirstOrDefault()?.CustomerName;
                    smsDetail.IncreaseMoney = request.TimaIncreaseMoney;
                }
                smsDetail.Status = request.Status;
                smsDetail.ModifyDate = DateTime.Now;
                if ((await _smsAnalyticsTab.UpdateAsync(smsDetail)))
                {
                    response.SetSucces();
                    response.Data = smsDetail.SmsAnalyticsID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateStatusSmsTransactionBank|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        //todo: xử lý sau
        public async Task<ResponseActionResult> ProcessSmsAnalytic()
        {
            ResponseActionResult response = new ResponseActionResult();
            long lasteID = 0;
            DateTime currentDate = DateTime.Now;
            int totalSuccess = 0;
            int totalFail = 0;
            int totalItemHandle = 0;
            try
            {
                var lstBankCardInfos = (await _bankCardTab.QueryAsync()).ToList();
                while (true)
                {
                    var lstSmsInfos = await _smsAnalyticsTab.SetGetTop(TimaSettingConstant.MaxItemQuery)
                                                    .WhereClause(x => LMS.Common.Constants.SmsSenderName.ListSenderBankCustomer.Contains(x.Sender))
                                                    .WhereClause(x => x.Status == (int)Common.Constants.SmsAnalytics_Status.Pending && x.SmsAnalyticsID > lasteID)
                                                    .QueryAsync();
                    if (lstSmsInfos == null || !lstSmsInfos.Any()) break;
                    lstSmsInfos = lstSmsInfos.OrderBy(x => x.SmsAnalyticsID);
                    lasteID = lstSmsInfos.Last().SmsAnalyticsID;
                    foreach (var item in lstSmsInfos)
                    {
                        totalItemHandle++;
                        item.ModifyDate = currentDate;
                        if (IsCancelSmsAnalytic(item))
                        {
                            item.Status = (int)SmsAnalytics_Status.DaHuy;
                            totalSuccess++;
                            continue;
                        }
                        item.Status = (int)SmsAnalytics_Status.DaPhanTich;
                        item.TypeTransactionBank = (int)GroupInvoiceType.Waiting; // phiếu treo
                        item.NameTransactionBank = GroupInvoiceType.Waiting.GetDescription();

                        var smsProvider = _readSmsAnalyticProviders.Where(x => x.GetSender == item.Sender.ToLower()).FirstOrDefault();
                        if (smsProvider == null)
                        {
                            totalFail++;
                            _logger.LogError($"ProcessSmsAnalytic_UnKnowSender|item={_common.ConvertObjectToJSonV2(item)}");
                            continue;
                        }
                        var smsAnalyticDetail = smsProvider.GetSmsAnalyticModel(item);
                        if (smsAnalyticDetail.BankCardID == 0)
                        {
                            // nếu accountnumber có chứa xxx -> convert lại theo số tk vib2
                            if (!string.IsNullOrEmpty(smsAnalyticDetail.AccountNumber) && smsAnalyticDetail.AccountNumber.Contains("xxx"))
                            {
                                smsAnalyticDetail.AccountNumber = smsAnalyticDetail.AccountNumber.Replace("xxx", "");
                                int numberAccountLength = 10;
                                int firstNumber = 3;
                                int secondNumber = 4;
                                foreach (var itemBankCard in lstBankCardInfos)
                                {
                                    if (!string.IsNullOrEmpty(itemBankCard.NumberAccount) && itemBankCard.NumberAccount.Length >= numberAccountLength)
                                    {
                                        var numberAccounnt = $"{itemBankCard.NumberAccount.Substring(0, firstNumber)}{itemBankCard.NumberAccount.Substring(itemBankCard.NumberAccount.Length - secondNumber, secondNumber)}";
                                        if (smsAnalyticDetail.AccountNumber == numberAccounnt)
                                        {
                                            smsAnalyticDetail.BankCardID = itemBankCard.BankCardID;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // tìm bank card
                                var bankCardInfo = lstBankCardInfos.Where(x => x.NumberAccount == smsAnalyticDetail.AccountNumber).FirstOrDefault();
                                if (bankCardInfo != null)
                                {
                                    smsAnalyticDetail.BankCardID = bankCardInfo.BankCardID;
                                }
                            }
                        }
                        // không xác định dc bank ở bước này thì bỏ qua
                        if (smsAnalyticDetail.BankCardID == 0)
                        {
                            totalFail++;
                            _logger.LogError($"ProcessSmsAnalytic_UnKnowBankCard|item={_common.ConvertObjectToJSonV2(item)}|smsAnalyticDetail={_common.ConvertObjectToJSonV2(smsAnalyticDetail)}");
                            continue;
                        }
                        item.TypeTransactionBank = (int)GroupInvoiceType.ReceiptCustomerConsider; // phiếu treo
                        item.NameTransactionBank = GroupInvoiceType.ReceiptCustomerConsider.GetDescription();
                        //todo: sẽ xử lý lại các phân tích lấy thông tin khách hàng ngoài ở ngoài for
                        item.BankCardID = smsAnalyticDetail.BankCardID;
                        if (smsAnalyticDetail.CustomerID > 0)
                        {
                            var lstLoanInfos = await _loanTab.WhereClause(x => x.CustomerID == smsAnalyticDetail.CustomerID && x.Status == (int)Loan_Status.Lending).QueryAsync();
                            if (lstLoanInfos == null || !lstLoanInfos.Any())
                            {
                                totalFail++;
                                _logger.LogError($"ProcessSmsAnalytic_UnKnowLoan_CustomerID|item={_common.ConvertObjectToJSonV2(item)}|smsAnalyticDetail={_common.ConvertObjectToJSonV2(smsAnalyticDetail)}");
                                continue;
                            }
                            var loanDetail = lstLoanInfos.First();
                            Domain.Tables.LoanJsonExtra loanJsonExtra = new Domain.Tables.LoanJsonExtra();
                            if (!string.IsNullOrEmpty(loanDetail.JsonExtra))
                            {
                                loanJsonExtra = _common.ConvertJSonToObjectV2<Domain.Tables.LoanJsonExtra>(loanDetail.JsonExtra);
                            }
                            item.LoanID = loanDetail.LoanID;
                            item.ShopID = Convert.ToInt32(loanDetail.OwnerShopID);
                            item.ShopName = loanJsonExtra.ShopName;
                            item.LoanCodeID = int.Parse(loanDetail.ContactCode.Replace(TimaSettingConstant.PrefixContractCode, "", comparisonType: StringComparison.OrdinalIgnoreCase));
                            item.CustomerID = Convert.ToInt64(loanDetail.CustomerID);
                            item.ContractCode = loanDetail.ContactCode;
                            item.CustomerName = loanDetail.CustomerName;
                            item.IncreaseMoney = smsAnalyticDetail.IncreaseMoney;
                        }
                        else if (!string.IsNullOrEmpty(smsAnalyticDetail.Phone) || !string.IsNullOrEmpty(smsAnalyticDetail.IdCard))
                        {
                            var lstLoanInfos = await _loanTab.JoinOn<Domain.Tables.TblCustomer>(l => l.CustomerID, c => c.CustomerID)
                                                        .SelectColumns(x => x.LoanID, x => x.CustomerID, x => x.JsonExtra, x => x.CustomerName, x => x.ContactCode)
                                                        .WhereClauseJoinOn<Domain.Tables.TblCustomer>(c => c.NumberCard == smsAnalyticDetail.IdCard || c.Phone == smsAnalyticDetail.Phone)
                                                        .WhereClause(x => x.Status == (int)Loan_Status.Lending).QueryAsync();
                            if (lstLoanInfos == null || !lstLoanInfos.Any())
                            {
                                totalFail++;
                                _logger.LogError($"ProcessSmsAnalytic_UnKnowLoan_Phone_IdCard|item={_common.ConvertObjectToJSonV2(item)}|smsAnalyticDetail={_common.ConvertObjectToJSonV2(smsAnalyticDetail)}");
                                continue;
                            }
                            var loanDetail = lstLoanInfos.First();
                            Domain.Tables.LoanJsonExtra loanJsonExtra = new Domain.Tables.LoanJsonExtra();
                            if (!string.IsNullOrEmpty(loanDetail.JsonExtra))
                            {
                                loanJsonExtra = _common.ConvertJSonToObjectV2<Domain.Tables.LoanJsonExtra>(loanDetail.JsonExtra);
                            }
                            item.LoanID = loanDetail.LoanID;
                            item.ShopID = Convert.ToInt32(loanDetail.OwnerShopID);
                            item.ShopName = loanJsonExtra.ShopName;
                            item.LoanCodeID = int.Parse(loanDetail.ContactCode.Replace(TimaSettingConstant.PrefixContractCode, "", comparisonType: StringComparison.OrdinalIgnoreCase));
                            item.ContractCode = loanDetail.ContactCode;
                            item.CustomerID = Convert.ToInt64(loanDetail.CustomerID);
                            item.CustomerName = loanDetail.CustomerName;
                            item.IncreaseMoney = smsAnalyticDetail.IncreaseMoney;
                        }

                        if (item.CustomerID > 0)
                        {
                            switch ((Constants.SmsType)item.SmsType)
                            {
                                case Constants.SmsType.Hub:
                                    // tima
                                    if (item.ShopID == TimaSettingConstant.ShopIDTima)
                                    {
                                        // phiếu thu hộ
                                        item.TypeTransactionBank = (int)GroupInvoiceType.ReceiptOnBehalfShop;
                                        item.NameTransactionBank = GroupInvoiceType.ReceiptOnBehalfShop.GetDescription();
                                    }
                                    else
                                    {
                                        // phiếu thu KH
                                        item.TypeTransactionBank = (int)GroupInvoiceType.ReceiptCustomer;
                                        item.NameTransactionBank = GroupInvoiceType.ReceiptCustomer.GetDescription();
                                    }
                                    break;
                                default:
                                    // tima
                                    if (item.ShopID == TimaSettingConstant.ShopIDTima)
                                    {
                                        // phiếu thu KH
                                        item.TypeTransactionBank = (int)GroupInvoiceType.ReceiptCustomer;
                                        item.NameTransactionBank = GroupInvoiceType.ReceiptCustomer.GetDescription();
                                    }
                                    else
                                    {
                                        // phiếu thu hộ
                                        item.TypeTransactionBank = (int)GroupInvoiceType.ReceiptOnBehalfShop;
                                        item.NameTransactionBank = GroupInvoiceType.ReceiptOnBehalfShop.GetDescription();
                                    }
                                    break;
                            }
                        }
                        // call service tạo phiếu
                        ResponseActionResult actionResultInvoice = null;
                        switch ((GroupInvoiceType)item.TypeTransactionBank)
                        {
                            case GroupInvoiceType.ReceiptCustomer:
                                actionResultInvoice = await _invoiceService.CreateInvoiceCustomer(smsAnalyticDetail.IncreaseMoney, item.SmsContent, item.CreateDate, item.CustomerID, item.BankCardID, item.LoanID, 1);
                                break;
                            case GroupInvoiceType.ReceiptOnBehalfShop:
                                long shopCollectID = item.SmsType == (int)Constants.SmsType.Tima ? TimaSettingConstant.ShopIDTima : TimaSettingConstant.ShopIDHUB001;
                                long shopIDSms = item.ShopID == 0 ? TimaSettingConstant.ShopIDTima : (long)item.ShopID;
                                actionResultInvoice = await _invoiceService.CreateInvoiceOnBehalfShop(smsAnalyticDetail.IncreaseMoney, item.SmsContent, item.CreateDate, shopCollectID, shopIDSms, item.CustomerID, item.BankCardID, item.LoanID, 1);
                                break;
                            case GroupInvoiceType.ReceiptCustomerConsider:
                            case GroupInvoiceType.Waiting:
                                actionResultInvoice = await _invoiceService.CreateInvoiceConsiderCustomer(smsAnalyticDetail.IncreaseMoney, item.SmsContent, item.CreateDate, item.BankCardID, 1);
                                break;
                        }
                        if (actionResultInvoice != null)
                        {
                            if (actionResultInvoice.Result == (int)ResponseAction.Success)
                            {
                                totalSuccess++;
                                item.TransactionBankCardId = Convert.ToInt32(actionResultInvoice.Data);
                                switch ((GroupInvoiceType)item.TypeTransactionBank)
                                {
                                    case GroupInvoiceType.ReceiptCustomer:
                                        item.Status = (int)SmsAnalytics_Status.DaNapTuDong;
                                        break;
                                    case GroupInvoiceType.ReceiptOnBehalfShop:
                                        item.Status = (int)SmsAnalytics_Status.DaNapThuHoTuDong;
                                        break;
                                    case GroupInvoiceType.ReceiptCustomerConsider:
                                    case GroupInvoiceType.Waiting:
                                        item.Status = (int)SmsAnalytics_Status.DaNapPhieuTreoTuDong;
                                        break;
                                }
                            }
                            else
                            {
                                totalFail++;
                                _logger.LogError($"ProcessSmsAnalytic_create_invoice_fail|item={_common.ConvertObjectToJSonV2(item)}|smsAnalyticDetail={_common.ConvertObjectToJSonV2(smsAnalyticDetail)}|responeApi={_common.ConvertObjectToJSonV2(actionResultInvoice)}");
                            }

                        }
                    }
                    _smsAnalyticsTab.UpdateBulk(lstSmsInfos);
                    await Task.Delay(100);
                }
                response.SetSucces();
                response.Data = new { TotalItemHandle = totalItemHandle, TotalSuccess = totalSuccess, TotalFail = totalFail };
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSmsAnalytic|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<IEnumerable<Entites.Dtos.Sms.SmsItemViewModel>> GetLstSmsInfoByCondition(RequestSmsItemViewModel request)
        {
            List<Entites.Dtos.Sms.SmsItemViewModel> lstSmsRespone = new List<Entites.Dtos.Sms.SmsItemViewModel>();
            try
            {
                DateTime? fromDate = null;
                DateTime? toDate = null;
                if (DateTime.TryParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, null, System.Globalization.DateTimeStyles.None, out DateTime dateRequest))
                {
                    fromDate = dateRequest;
                }
                if (DateTime.TryParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, null, System.Globalization.DateTimeStyles.None, out dateRequest))
                {
                    toDate = dateRequest;
                }
                if (fromDate.HasValue)
                {
                    fromDate = fromDate.Value.AddSeconds(-1);
                    _smsAnalyticsTab.WhereClause(x => x.CreateDate > fromDate.Value);
                }
                if (toDate.HasValue)
                {
                    toDate = toDate.Value.AddDays(1);
                    _smsAnalyticsTab.WhereClause(x => x.CreateDate < toDate.Value);
                }
                if (request.Status != (int)Common.Constants.StatusCommon.SearchAll)
                {
                    _smsAnalyticsTab.WhereClause(x => x.Status == request.Status);
                }
                var lstSmsData = (await _smsAnalyticsTab.QueryAsync()).OrderByDescending(x => x.SmsAnalyticsID).ToList();
                request.TotalRow = lstSmsData.Count;
                lstSmsData = lstSmsData.Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                foreach (var item in lstSmsData)
                {
                    lstSmsRespone.Add(new Entites.Dtos.Sms.SmsItemViewModel
                    {
                        SmsAnalyticsID = item.SmsAnalyticsID,
                        Sender = item.Sender,
                        SmsContent = item.SmsContent,
                        LoanCode = item.ContractCode,
                        LoanID = item.LoanID,
                        ShopID = item.ShopID ?? 0,
                        ShopName = item.ShopName,
                        CustomerID = item.CustomerID,
                        CustomerName = item.CustomerName,
                        IncreaseMoney = item.IncreaseMoney ?? 0,
                        SmsStatusName = ((SmsAnalytics_Status)item.Status).GetDescription(),
                        SmsreceivedDate = item.SmsreceivedDate ?? item.CreateDate,
                        ModifyDate = item.ModifyDate ?? item.CreateDate,
                        BankCardID = item.BankCardID,
                        BankCardAliasName = item.BankCardAliasName
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLstSmsInfoByCondition|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return lstSmsRespone;
        }

        private bool IsCancelSmsAnalytic(Domain.Tables.TblSmsAnalytics sms)
        {
            var regexMatchNegativeDigit = @"-(\s)?\d+(((,\d+)?)+(.\d+)?)+";
            var matches = Regex.Matches(sms.SmsContent, regexMatchNegativeDigit, RegexOptions.Multiline);
            if (matches.Count > 0)
            {
                for (int i = 0; i < matches.Count; i++)
                {
                    if (matches[i].Success)
                    {
                        var value = matches[i].Value;
                        if (!string.IsNullOrEmpty(value) && value.Contains(","))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public async Task<ResponseActionResult> CreateSMSOTPDisbursement(SmsTransactionBankRequest request)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            try
            {
                var receivedDate = DateTime.ParseExact(request.ReceivedDate, "dd/MM/yyyy HH:mm:ss", null);
                var smsCotentHash = _common.HashMD5($"{request.SmsContent}-{receivedDate:dd/MM/yyyy HH:mm}");
                var smsExist = (await _smsDisbursementTab.WhereClause(x => x.SmsContentHash == smsCotentHash).QueryAsync()).FirstOrDefault();
                if (smsExist != null && smsExist.SmsDisbursementID > 0)
                {
                    response.Message = MessageConstant.SMS_ContentDouble;
                    return response;
                }
                var currentDate = DateTime.Now;
                var sms = new Domain.Tables.TblSmsDisbursement()
                {
                    Sender = request.Sender,
                    CreateDate = currentDate,
                    ModifyDate = currentDate,
                    SmsContent = request.SmsContent,
                    Status = (int)Common.Constants.SmsDisbursement_Status.Waiting,
                    SmsContentHash = smsCotentHash,
                    SmsTimaID = request.SmsTimaID
                };
                var id = await _smsDisbursementTab.InsertAsync(sms);
                if (id > 0)
                {
                    response.SetSucces();
                    response.Data = id;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateSmsTransactionBank|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
