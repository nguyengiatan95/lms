﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.ReportServices;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Services
{
    public interface IReportManager
    {
        LMS.Entites.Dtos.ReportServices.ReportStasticsLoanBorrowModel GetReportStasticsLoanBorrowModel(long shopID);
        Task<int> ProcessSummaryStasticLoanBorrow(long shopID);
    }
    public class ReportManager : IReportManager
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblReportStasticsLoanBorrow> _reportStasticsLoanBorrowTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        ILogger<ReportManager> _logger;
        LMS.Common.Helper.Utils _common;
        public ReportManager(LMS.Common.DAL.ITableHelper<Domain.Tables.TblReportStasticsLoanBorrow> reportStasticsLoanBorrowTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
             ILogger<ReportManager> logger)
        {
            _reportStasticsLoanBorrowTab = reportStasticsLoanBorrowTab;
            _loanTab = loanTab;
            _logger = logger;
            _common = new Utils();
        }

        public ReportStasticsLoanBorrowModel GetReportStasticsLoanBorrowModel(long shopID)
        {
            try
            {
                var reportDetail = _reportStasticsLoanBorrowTab.WhereClause(x => x.ShopID == shopID).Query().FirstOrDefault();
                if (reportDetail != null && reportDetail.ShopID > 0)
                {
                    return _common.ConvertParentToChild<ReportStasticsLoanBorrowModel, Domain.Tables.TblReportStasticsLoanBorrow>(reportDetail);
                }
                return new ReportStasticsLoanBorrowModel() { ShopID = shopID };
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetReportStasticsLoanBorrowModel|ex={ex.Message}|{ex.StackTrace}");
            }
            return null;
        }

        public async Task<int> ProcessSummaryStasticLoanBorrow(long shopID)
        {
            try
            {
                long latestLoanID = 0;
                Dictionary<long, Domain.Tables.TblReportStasticsLoanBorrow> dictStasticLoanBorrowInfos = new Dictionary<long, Domain.Tables.TblReportStasticsLoanBorrow>();
                DateTime dayPaidDueOver30 = DateTime.Now.AddDays(-29).Date;
                var currentDate = DateTime.Now;
                while (true)
                {
                    _loanTab.SetGetTop(1000).WhereClause(x => x.Status == (int)Loan_Status.Lending && x.LoanID > latestLoanID);
                    if (shopID > 0)
                    {
                        _loanTab.WhereClause(x => x.OwnerShopID == shopID);
                    }
                    var lstLoanInfos = (await _loanTab.SelectColumns(x => x.OwnerShopID, x => x.TotalMoneyCurrent, x => x.NextDate, x => x.LoanID)
                                               .OrderBy(x => x.LoanID).QueryAsync()).ToList();
                    if (lstLoanInfos == null || lstLoanInfos.Count == 0)
                    {
                        break;
                    }
                    lstLoanInfos = lstLoanInfos.OrderBy(x => x.LoanID).ToList();
                    latestLoanID = lstLoanInfos.Last().LoanID;
                    foreach (var item in lstLoanInfos)
                    {
                        if (!dictStasticLoanBorrowInfos.ContainsKey(item.OwnerShopID))
                        {
                            dictStasticLoanBorrowInfos.Add(item.OwnerShopID, new Domain.Tables.TblReportStasticsLoanBorrow()
                            {
                                ShopID = item.OwnerShopID,
                                CreateDate = currentDate,
                                ModifyDate = currentDate
                            });
                        }
                        var stasticLoanDetail = dictStasticLoanBorrowInfos[item.OwnerShopID];
                        stasticLoanDetail.DebtMoneyBorrow += item.TotalMoneyCurrent;
                        stasticLoanDetail.TotalLoanBorrow += 1;
                        if (item.NextDate.HasValue && item.NextDate.Value.Date < dayPaidDueOver30)
                        {
                            stasticLoanDetail.OverDebtMoneyBorrow += item.TotalMoneyCurrent;
                            stasticLoanDetail.TotalLoanBorrowOverDebt += 1;
                        }
                    }
                }
                if (dictStasticLoanBorrowInfos.Count > 0)
                {
                    if (shopID > 0)
                    {
                        _reportStasticsLoanBorrowTab.WhereClause(x => x.ShopID == shopID);
                    }
                    var lstReportStasticInfos = (await _reportStasticsLoanBorrowTab.QueryAsync()).ToList();
                    if (lstReportStasticInfos == null || lstReportStasticInfos.Count == 0)
                    {
                        _reportStasticsLoanBorrowTab.InsertBulk(dictStasticLoanBorrowInfos.Values.ToList());
                        return 1;
                    }
                    List<Domain.Tables.TblReportStasticsLoanBorrow> lstUpdate = new List<Domain.Tables.TblReportStasticsLoanBorrow>();
                    foreach (var item in lstReportStasticInfos)
                    {
                        if (dictStasticLoanBorrowInfos.ContainsKey(item.ShopID))
                        {
                            item.OverDebtMoneyBorrow = dictStasticLoanBorrowInfos[item.ShopID].OverDebtMoneyBorrow;
                            item.TotalLoanBorrow = dictStasticLoanBorrowInfos[item.ShopID].TotalLoanBorrow;
                            item.DebtMoneyBorrow = dictStasticLoanBorrowInfos[item.ShopID].DebtMoneyBorrow;
                            item.TotalLoanBorrowOverDebt = dictStasticLoanBorrowInfos[item.ShopID].TotalLoanBorrowOverDebt;
                            item.ModifyDate = currentDate;
                            lstUpdate.Add(item);
                            dictStasticLoanBorrowInfos.Remove(item.ShopID);
                        }
                    }
                    // cập nhật
                    if (lstUpdate.Count > 0)
                    {
                        _reportStasticsLoanBorrowTab.UpdateBulk(lstUpdate);
                    }
                    // thêm shop mới
                    if (dictStasticLoanBorrowInfos.Count > 0)
                    {
                        _reportStasticsLoanBorrowTab.InsertBulk(dictStasticLoanBorrowInfos.Values.ToList());
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSummaryStasticLoanBorrow|ex={ex.Message}|{ex.StackTrace}");
            }
            return 1;
        }
    }
}
