﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Validators
{
    public class ExtendLoanTimeValidator : AbstractValidator<Domain.Models.Loan.RequestExtendLoanTime>
    {
        public ExtendLoanTimeValidator()
        {
            RuleFor(x => x.LoanID).GreaterThan(0)
                .WithMessage("LoanID invalid");
            RuleFor(x => x.PeriodExtend).GreaterThan(0)
               .WithMessage("FrequencyExtend invalid");
        }
    }
}
