﻿using FluentValidation;
using LMS.LoanServiceApi.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Validators
{
    public class CreateLoanCommandModelValidator : AbstractValidator<CreateLoanCommandModel>
    {
        public CreateLoanCommandModelValidator()
        {
            RuleFor(x => x.LoanCreditId).GreaterThan(0)
                .WithMessage("LoanCreditId invalid");
            RuleFor(x => x.MoneyfeeInsuranceOfCustomer).GreaterThan(10000).WithMessage("MoneyfeeInsuranceOfCustomer invalid");

        }
    }
}
