﻿using FluentValidation;
using LMS.Common.Constants;
using LMS.LoanServiceApi.Commands.Loan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Validators
{
    public class UpdateInforPaymentLoanCommandValidator : AbstractValidator<UpdateInforPaymentLoanCommand>
    {
        public UpdateInforPaymentLoanCommandValidator()
        {
            var _common = new LMS.Common.Helper.Utils();
            RuleFor(x => x.LoanID).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.LoanIDNull);
            RuleFor(x => x.LoanTime).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.LoanTimeNull);
            RuleFor(x => x.RateType).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.RateTypeNull);
            RuleFor(x => x.RateInterest).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.RateInterestNull);
            RuleFor(x => x.CreateBy).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.RequireCreateBy);
            //RuleFor(x => x.RateService).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.RateServiceNull);
            RuleFor(x => x.RateConsultant).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.RateConsultantNull);
            RuleFor(x => x.StrFromDate).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.StrTransactionDateNull);
            RuleFor(x => x.StrFromDate).Must((strFromDate) => { return _common.BeAValidDate(strFromDate, _common.DateTimeDDMMYYYY); }).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
        }
    }
}
