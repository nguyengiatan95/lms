﻿using FluentValidation;
using LMS.LoanServiceApi.Commands.BankCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Validators
{
    public class CreateBankCardCommandValidator : AbstractValidator<CreateBankCardCommand>
    {
        public CreateBankCardCommandValidator()
        {
            var _common = new LMS.Common.Helper.Utils();
            RuleFor(x => x.BankID).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.NumberAccount).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.AccountHolderName).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.BranchName).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.AliasName).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.UserID).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
        }
    }
}
