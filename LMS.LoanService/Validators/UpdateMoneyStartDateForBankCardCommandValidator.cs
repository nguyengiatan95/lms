﻿using FluentValidation;
using LMS.Common.Constants;
using LMS.LoanServiceApi.Commands.BankCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Validators
{
    public class UpdateMoneyStartDateForBankCardCommandValidator : AbstractValidator<UpdateMoneyStartDateForBankCardCommand>
    {
        public UpdateMoneyStartDateForBankCardCommandValidator()
        {
            var _common = new LMS.Common.Helper.Utils();
            RuleFor(x => x.DateApply).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.DateApply).NotEmpty().WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.DateApply).Must((dateApply) => { return _common.BeAValidDate(dateApply, TimaSettingConstant.DateTimeDayMonthYear); }).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);

        }
    }
}
