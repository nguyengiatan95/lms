﻿using FluentValidation;
using LMS.Common.Constants;
using LMS.LoanServiceApi.Commands.BankCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Validators
{
    public class ProcessSummaryBankCardByBankCardIDCommandValidator : AbstractValidator<ProcessSummaryBankCardByBankCardIDCommand>
    {
        public ProcessSummaryBankCardByBankCardIDCommandValidator()
        {
            var _common = new LMS.Common.Helper.Utils();
            RuleFor(x => x.BankCardIDs).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.BankCardIDs).NotEmpty().WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.TransactionDate).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.TransactionDate).Must((transactionDate) => { return _common.BeAValidDate(transactionDate, TimaSettingConstant.DateTimeDayMonthYear); }).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
        }
    }
}
