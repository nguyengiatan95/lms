﻿using FluentValidation;
using LMS.Common.Constants;
using LMS.LoanServiceApi.Commands.Loan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Validators
{
    public class UpdateLinkInsuranceCommandValidator : AbstractValidator<UpdateLinkInsuranceCommand>
    {
        public UpdateLinkInsuranceCommandValidator()
        {
            var _common = new LMS.Common.Helper.Utils();
            List<int> lstTypeInsurance = new List<int>() {
              (int)Insurance_TypeInsurance.Customer,
              (int)Insurance_TypeInsurance.Lender,
               (int)Insurance_TypeInsurance.Material
            };
            RuleFor(x => x.TypeInsurance).Must(x => lstTypeInsurance.Contains(x)).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.Path).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.PathInsuranceNull);
            RuleFor(x => x.LoanID).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.LoanIDNull);
        }
    }
}
