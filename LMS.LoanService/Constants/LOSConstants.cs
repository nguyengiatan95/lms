﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Constants
{
    public class LOSConstants
    {
        public const string NA = "NA";
        public const string DT = "DT";
        public const int MoneyCustomerRecieve = 1;
        public const int IsHandleException = 0;

        public const decimal RateInterest = 2700;
        public const decimal RateInterestLender = 493;
        public const int LoanTime = 12;
        public const int Frequency = 1;

    }
}
