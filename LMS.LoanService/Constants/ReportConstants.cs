﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Constants
{
    public class ReportConstants
    {
        public const decimal PercentVat = 1.1M;
        public const string Denominator = "01GTKT0/001";
        public const string Notation = "TM/18E";
        public const int TaxPercentage = 10;
        public const string TypePayment = "TM/CK";
        public static  DateTime DateCommodityName = new DateTime(2019, 01, 01);
        public const string CommodityName = "Phí tư vấn quản lý";
        public const string CommodityNameOld = "Phí tư vấn dịch vụ tín dụng";
        public const string CommodityNameReportFineInterestLate = "Phí tư vấn khoản vay chậm trả";
        
        public const string OrderNumber = "000";
        public const string CommodityNameInterest = "Tiền lãi";
        public const string CommodityNameService = "Phí dịch vụ";
        public const string CommodityNameFines = "Tiền phạt";

        public const string CommodityNameReportVATForLender = "Thu lãi tiền vay";

        //report vat hub
        public const string DenominatorHub = "02/GTTT0/001";
        public const string NotationHub = "HU/21E";
        //reportVat Gcash
        public const string NotationVatGcash = "CD/18E";

        public const int MaxDayReport = 30;

        public  static DateTime FromDateReportVATForLender = new DateTime(2021, 08, 01);
    }
}
