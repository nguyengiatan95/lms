﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Constants
{
    public enum SmsType
    {
        Unknown = 0,
        Tima = 1,
        Hub = 2,
        Web = 3
    }
    public enum SmsAnaylyticSourceSendRequest
    {
        Momo = 0,
        Gpay = 1,
        Gutina = 2
    }
    public enum SmsStatusAnalytics
    {
        [Description("Chưa phân tích")]
        ChuaPhanTich = 0,
        [Description("Đã phân tích")]
        DaPhanTich = 1,
        [Description("Đã nạp")]
        DaNap = 2,
        [Description("Đã nạp phiếu treo")]
        DaNapPhieuTreo = 3,
        [Description("Đã nạp phiếu thu hộ")]
        DaNapThuHo = 4,
        [Description("Hệ thống nạp tự động")]
        DaNapTuDong = 5,
        [Description("Hệ thống nạp phiếu treo tự động")]
        DaNapPhieuTreoTuDong = 6,
        [Description("Hệ thống nạp phiếu thu hộ tự động")]
        DaNapThuHoTuDong = 7
    }
    public enum StateGroupAccountMeCash
    {
        [Description("Quản trị hệ thống")]
        Admin = 1,


        [Description("TeleSales")]
        Support = 9, // Hỗ trợ Mecash

        [Description("Direct Sales")]
        Counselor = 10,// Tư Vân Viên

        [Description("Kế Toán")]
        Accountant = 11, // kế toán

        [Description("Thẩm Định Hồ Sơ")]
        Coordinator = 12, // Người điều phối cho tư vấn viên

        [Description("Quản Lý DRS")]
        SupervisionArea = 13, // Quản lý khu vực

        SupervisionRegion = 14, // Quản lý vùng

        SuperAdmin = 3, // Supper Admin

        Agency = 99,// Đại lý
        CTV = 100,

        [Description("Thẩm định thực địa")]
        FieldSurvey = 17,// Thẩm định thực địa

        [Description("Ký hợp đồng")]
        AwardContract = 18, // Ký hợp đồng

        [Description("Quản Lý Ký hợp đồng")]
        FieldSaleManager = 19, // Quản lý ký hợp đồng 

        [Description("Quản Lý Thẩm Định Thực Địa")]
        FieldSurveyManager = 20, // Quản lý Thẩm định thực địa 

        [Description("Call")]
        CallReminders = 21, // Call nhắc nợ

        [Description("Field MB")]
        GroundReminders = 22, // Thực Địa Nợ

        [Description("Thẩm định viên của HUB")]
        FieldSurveyOfHub = 23,// Thẩm định viên của HUB

        [Description("Đối tác xử lý nợ xấu")]
        PartnerDealingBadDebts = 24, // Đối tác xử lý nợ xấu

        [Description("Legal")]
        ThnPhapLy = 25, // Lender Care

        [Description("Quản lý thu hồi nợ")]
        ManagerDebtRecovery = 26, // Lender Care

        [Description("Chuyên viên kinh doanh Nguồn Vốn")]
        LenderCare = 27, // Lender Care

        [Description("Trưởng cửa hàng")]
        HeadOfHub = 28, // Lender Care

        [Description("Skip Call")]
        SkipCall = 29,

        [Description("Quản lý TĐHS")]
        LeaderCoordinator = 30,

        [Description("Call Vay1h")]
        CallVay1h = 31,

        [Description("Dịch vụ khách hàng")]
        CustomerService = 32,

        [Description("Sales Admin SPL")]
        SalesAdminSPL = 33,

        [Description("Thu Hồi Nợ TTKD")]
        ThnTTKD = 34,

        [Description("Nhóm hỗ trợ THN")]
        NHTTHN = 35,

        [Description("P.Marketing")]
        Marketing = 36,

        [Description("Field MN")]
        Field_MN = 37,

        HUB = 4,

        [Description("Chuyên viên chăm sóc Lender")]
        LenderTakeCare = 38,
    }

   
}
