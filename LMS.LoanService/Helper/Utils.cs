﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace LMS.LoanService.Helper
{
    public class CommonLoanServiceApi
    {
        public string DateTimeDDMMYYYY = "dd-MM-yyyy";
        public string DateTimeDDMMYYYYHHMMSS = "dd-MM-yyyy HH:mm:ss";
        public string DateTimeYYYYMMDD = "yyyyMMdd";
        public string DateTimeHHDDMMYYYY = "HH:mm dd-MM-yyyy";
        public string DateTimeDDMMYYYYHHMM = "dd/MM/yyyy HH:mm";
        public string DateTimeDDMMYYYYNoTime = "dd/MM/yyyy";

        public const string UnitTimeNameDate = "Ngày";
        public const string UnitTimeNameMonth = "Tháng";
        public const string DateTimeDayMonthYear = "dd/MM/yyyy";

        public const int TimeWaitingReceivedOTP = 3;
    }

    public enum ReconciliationInternalGetOTPStatus
    {
        Fail = 0,
        Success = 1,
        Retry = 2
    }

}
