﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.LOSServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.RestClients
{
    public interface ILOSService
    {
        Task<LMS.Entites.Dtos.LOSServices.MapResultLoanWaitingDisbursementDetailLOS> GetLoanCreditDetail(long loanID);
        Task<List<HistoryCommentCredit>> GetHistoryCommentLOS(int page, int pageSize, long loanCreditId);
        Task<List<ResultListImage>> GetListImages(long loanID, int TypeFile = 0);
        Task<List<ResultLoanDisbursmentWaiting>> DisbursementWaitingLOS(int page, int pageSize, int type, string search);
        Task<ResponseActionResult> SaveCommentHistory(ReqSaveCommentHistory req);
        Task<ResponseActionResult> PushLoanLender(PushLoanLenderReq req);
        Task<ResponseActionResult> LockLoan(LockLoanReq req);
        Task<ResponseActionResult> DisbursementLoan(DisbursementLoanReq req);
        Task<ResponseActionResult> ReturnLoan(ReturnLoanReq req);
        int ConvertStatusLoanDisbursementFromLOSToAG(int statusLos);
        Task<ResponseActionResult> ChangeDisbursementByAccountant(ChangeDisbursementByAccountantReq dataRequest);
        Task<List<WaitingMoneyDisbursementLenderLOS>> GetWaitingMoneyDisbursementLenderLOS();
    }
    public class LOSService : ILOSService
    {
        Common.Helper.IApiHelper _apiHelper;
        public LOSService(Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }

        public async Task<LMS.Entites.Dtos.LOSServices.MapResultLoanWaitingDisbursementDetailLOS> GetLoanCreditDetail(long loanID)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{Common.Constants.ActionApiInternal.OutGatewayApi_LOS_GetLoanCreditDetail}/{loanID}";
            return await _apiHelper.ExecuteAsync<LMS.Entites.Dtos.LOSServices.MapResultLoanWaitingDisbursementDetailLOS>(url: url.ToString());
        }
        public async Task<List<HistoryCommentCredit>> GetHistoryCommentLOS(int page, int pageSize, long loanCreditId)
        {
            var req = new
            {
                Page = page,
                PageSize = pageSize,
                LoanCreditId = loanCreditId
            };
            var url = $"{Common.Constants.ServiceHostInternal.OutGateway}{Common.Constants.ActionApiInternal.OutGatewayApi_LOS_GetHistoryComment}";
            return await _apiHelper.ExecuteAsync<List<HistoryCommentCredit>>(url: url.ToString(), RestSharp.Method.POST, req);
        }
        public async Task<List<ResultListImage>> GetListImages(long loanBriefId, int typeFile = 0)
        {
            var req = new
            {
                LoanBriefId = loanBriefId,
                TypeFile = typeFile
            };
            var url = $"{Common.Constants.ServiceHostInternal.OutGateway}{Common.Constants.ActionApiInternal.OutGatewayApi_LOS_GetListImages}";
            return await _apiHelper.ExecuteAsync<List<ResultListImage>>(url: url.ToString(), RestSharp.Method.POST, req);
        }

        public async Task<List<ResultLoanDisbursmentWaiting>> DisbursementWaitingLOS(int page, int pageSize, int type, string search)
        {
            var req = new
            {
                Page = page,
                PageSize = pageSize,
                TypeID = type,
                Search = search
            };
            var url = $"{Common.Constants.ServiceHostInternal.OutGateway}{Common.Constants.ActionApiInternal.OutGatewayApi_LOS_DisbursementWaiting}";
            return await _apiHelper.ExecuteAsync<List<ResultLoanDisbursmentWaiting>>(url: url.ToString(), RestSharp.Method.POST, req);
        }

        public async Task<ResponseActionResult> SaveCommentHistory(ReqSaveCommentHistory req)
        {
            var url = $"{Common.Constants.ServiceHostInternal.OutGateway}{Common.Constants.ActionApiInternal.OutGatewayApi_LOS_SaveCommentHistory}";
            return await _apiHelper.ExecuteAsync(url: url.ToString(), RestSharp.Method.POST, req);
        }

        public async Task<ResponseActionResult> PushLoanLender(PushLoanLenderReq req)
        {
            var url = $"{Common.Constants.ServiceHostInternal.OutGateway}{Common.Constants.ActionApiInternal.OutGatewayApi_LOS_PushLoanLender}";
            return await _apiHelper.ExecuteAsync(url: url.ToString(), RestSharp.Method.POST, req);
        }
        public async Task<ResponseActionResult> LockLoan(LockLoanReq req)
        {
            var url = $"{Common.Constants.ServiceHostInternal.OutGateway}{Common.Constants.ActionApiInternal.OutGatewayApi_LOS_LockLoan}";
            return await _apiHelper.ExecuteAsync(url: url.ToString(), RestSharp.Method.POST, req);
        }
        public async Task<ResponseActionResult> DisbursementLoan(DisbursementLoanReq req)
        {
            var url = $"{Common.Constants.ServiceHostInternal.OutGateway}{Common.Constants.ActionApiInternal.OutGatewayApi_LOS_DisbursementLoan}";
            return await _apiHelper.ExecuteAsync(url: url.ToString(), RestSharp.Method.POST, req);
        }

        public async Task<ResponseActionResult> ReturnLoan(ReturnLoanReq req)
        {
            var url = $"{Common.Constants.ServiceHostInternal.OutGateway}{Common.Constants.ActionApiInternal.OutGatewayApi_LOS_ReturnLoan}";
            return await _apiHelper.ExecuteAsync(url: url.ToString(), RestSharp.Method.POST, req);
        }

        public int ConvertStatusLoanDisbursementFromLOSToAG(int statusLos)
        {
            return statusLos switch
            {
                // LOS: Chờ đẩy cho lender, 
                90 => (int)StateLoanCreditNew.WaittingPushToLender,
                // Chờ kế toán giải ngân
                102 => (int)StateLoanCreditNew.AccountantDisbursement,
                // Chờ lender giải ngân
                103 => (int)StateLoanCreditNew.WaittingAgency,
                // giải ngân tự động
                104 => (int)StateLoanCreditNew.AutoDisbursement,
                // không rõ status
                _ => -1,
            };
        }

        public async Task<ResponseActionResult> ChangeDisbursementByAccountant(ChangeDisbursementByAccountantReq dataRequest)
        {
            var url = $"{Common.Constants.ServiceHostInternal.OutGateway}{Common.Constants.ActionApiInternal.OutGatewayApi_LOS_ChangeDisbursementByAccountant}";
            return await _apiHelper.ExecuteAsync(url: url.ToString(), RestSharp.Method.POST, dataRequest);
        }

        public async Task<List<WaitingMoneyDisbursementLenderLOS>> GetWaitingMoneyDisbursementLenderLOS()
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{Common.Constants.ActionApiInternal.OutGatewayApi_LOS_GetWaitingMoneyDisbursementLenderLOS}";
            return await _apiHelper.ExecuteAsync<List<WaitingMoneyDisbursementLenderLOS>>(url: url.ToString());
        }
    }
}
