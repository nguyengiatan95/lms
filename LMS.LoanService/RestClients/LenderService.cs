﻿using LMS.Entites.Dtos.LenderService;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.RestClients
{
    public interface ILenderService
    {
        Task<IEnumerable<LMS.Entites.Dtos.LenderService.LenderInfoModel>> GetLenderInfosbyIDs(List<long> lenderIDs);
        Task<LMS.Common.Constants.ResponseActionResult> CreateLenderDebt(CreateLenderDebtRequest dataPost);
        Task<LMS.Common.Constants.ResponseActionResult> CreateOrUpdateLenderFromAG(long lenderID);
    }
    public class LenderService : ILenderService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        LMS.Common.Helper.Utils _common;
        ILogger<LenderService> _logger;
        public LenderService(LMS.Common.Helper.IApiHelper apiHelper, ILogger<LenderService> logger)
        {
            _apiHelper = apiHelper;
            _common = new Common.Helper.Utils();
            _logger = logger;
        }

        public async Task<Common.Constants.ResponseActionResult> CreateLenderDebt(CreateLenderDebtRequest dataPost)
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.LenderService}{LMS.Common.Constants.ActionApiInternal.Lender_CreateLenderDebt}";
                return await _apiHelper.ExecuteAsync(url, RestSharp.Method.POST, dataPost);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateLenderDebt_Exception|dataPost={_common.ConvertObjectToJSonV2(dataPost)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }

        public async Task<Common.Constants.ResponseActionResult> CreateOrUpdateLenderFromAG(long lenderID)
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.LenderService}{LMS.Common.Constants.ActionApiInternal.Lender_CreateOrUpdateLenderFromAG}";
                var dataPost = new { AGLenderID = lenderID };
                return await _apiHelper.ExecuteAsync(url, RestSharp.Method.POST, dataPost);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateOrUpdateLenderFromAG_Exception|lenderID={lenderID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }

        public async Task<IEnumerable<LenderInfoModel>> GetLenderInfosbyIDs(List<long> lenderIDs)
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.LenderService}{LMS.Common.Constants.ActionApiInternal.Lender_GetLenderInfosByIDs}";
                var dataPost = new { LenderIDs = lenderIDs };
                var responseLenderInfos = await _apiHelper.ExecuteAsync(url, RestSharp.Method.POST, dataPost);
                if (responseLenderInfos.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                {
                    return _common.ConvertJSonToObjectV2<List<LMS.Entites.Dtos.LenderService.LenderInfoModel>>(responseLenderInfos.Data.ToString());
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLenderInfosbyIDs_Exception|lenderIDs={_common.ConvertObjectToJSonV2(lenderIDs)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
    }
}
