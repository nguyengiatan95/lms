﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.CustomerService;
using LMS.LoanServiceApi.Domain.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.RestClients
{
    public interface ICustomerService
    {
        Task<long> CreateInfoBorrower(CreateBorrowerCommandRequest request);
        Task<IEnumerable<LMS.Entites.Dtos.CustomerService.CustomerInfoModel>> GetCustomerInfosByIDs(List<long> customerIDs);
        Task<LMS.Common.Constants.ResponseActionResult> UpdateMoneyCustomer(long customerID, long moneyAdd, string description);
        Task<List<ReportExtraMoneyCustomerItem>> ReportExtraMoneyCustomer(ReportExtraMoneyCustomerReq req);
    }
    public class CustomerService : ICustomerService
    {
        readonly LMS.Common.Helper.IApiHelper _apiHelper;
        readonly LMS.Common.Helper.Utils _common;
        readonly ILogger<CustomerService> _logger;
        public CustomerService(LMS.Common.Helper.IApiHelper apiHelper, ILogger<CustomerService> logger)
        {
            _apiHelper = apiHelper;
            _common = new Common.Helper.Utils();
            _logger = logger;
        }

        public async Task<long> CreateInfoBorrower(CreateBorrowerCommandRequest request)
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.CustomerService}{LMS.Common.Constants.ActionApiInternal.Customer_CreateInfoBorrower}";
                var resutlt = await _apiHelper.ExecuteAsync(url, body: request, method: RestSharp.Method.POST);
                if (resutlt.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                {
                    return Convert.ToInt64(resutlt.Data);
                }
                else if (resutlt.Message == LMS.Common.Constants.MessageConstant.CustomerExist)
                {
                    url = $"{LMS.Common.Constants.ServiceHostInternal.CustomerService}{LMS.Common.Constants.ActionApiInternal.Customer_GetInfosByNumberCard}";
                    var dataPost = new { NumberCard = request.NumberCard, FullName = request.CustomerName };
                    var responseCustomerInfos = await _apiHelper.ExecuteAsync(url, RestSharp.Method.POST, dataPost);
                    if (responseCustomerInfos.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                    {
                        var lstCustomerInfos = _common.ConvertJSonToObjectV2<List<LMS.Entites.Dtos.CustomerService.CustomerInfoModel>>(responseCustomerInfos.Data.ToString());
                        if (lstCustomerInfos.Count > 0)
                        {
                            return lstCustomerInfos.First().CustomerID;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{System.Reflection.MethodBase.GetCurrentMethod().Name}_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return 0;
        }

        public async Task<IEnumerable<LMS.Entites.Dtos.CustomerService.CustomerInfoModel>> GetCustomerInfosByIDs(List<long> customerIDs)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CustomerService}{LMS.Common.Constants.ActionApiInternal.Customer_GetCustomerInfosByIDs}";
            var dataPost = new
            {
                CustomerIDs = customerIDs
            };
            return await _apiHelper.ExecuteAsync<List<LMS.Entites.Dtos.CustomerService.CustomerInfoModel>>(url, body: dataPost, method: RestSharp.Method.POST);
        }

        public async Task<Common.Constants.ResponseActionResult> UpdateMoneyCustomer(long customerID, long moneyAdd, string description)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CustomerService}{LMS.Common.Constants.ActionApiInternal.Customer_UpdateMoneyCustomer}";
            var dataPost = new
            {
                CustomerID = customerID,
                MoneyAdd = moneyAdd,
                Description = description
            };
            return await _apiHelper.ExecuteAsync(url, body: dataPost, method: RestSharp.Method.POST);
        }
        public async Task<List<ReportExtraMoneyCustomerItem>> ReportExtraMoneyCustomer(ReportExtraMoneyCustomerReq req)
        {
          
            var url = $"{Common.Constants.ServiceHostInternal.CustomerService}{Common.Constants.ActionApiInternal.Customer_ReportExtraMoneyCustomer}";
            return await _apiHelper.ExecuteAsync<List<ReportExtraMoneyCustomerItem>>(url: url.ToString(), RestSharp.Method.POST, req);
        }
    }
}
