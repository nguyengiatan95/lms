﻿using LMS.Common.Constants;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.RestClients
{
    public interface IInvoiceService
    {
        Task<LMS.Common.Constants.ResponseActionResult> CreateInvoicePaySlipCustomer(long totalMoney, long loanID, long customerID, long shopID, DateTime transactionDate, long bankCardID, string note, long userIDCreate);
        Task<ResponseActionResult> CreateInvoiceConsiderCustomer(long totalMoney, string note, DateTime transactionDate, long bankCardID, long createBy);
        Task<ResponseActionResult> CreateInvoiceOnBehalfShop(long totalMoney, string note, DateTime transactionDate, long shopCollectID, long toShopID, long customerID, long bankCardID, long loanID, long createBy);
        Task<ResponseActionResult> CreateInvoiceCustomer(long totalMoney, string note, DateTime transactionDate, long customerID, long bankCardID, long loanID, long createBy);
        Task<ResponseActionResult> CreateInvoiceBankInterest(long totalMoney, string note, DateTime transactionDate, long bankCardID, long shopID, long createBy, int invoiceSubType);
    }
    public class InvoiceService : IInvoiceService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        LMS.Common.Helper.Utils _common;
        ILogger<InvoiceService> _logger;
        public InvoiceService(LMS.Common.Helper.IApiHelper apiHelper, ILogger<InvoiceService> logger)
        {
            _apiHelper = apiHelper;
            _common = new Common.Helper.Utils();
            _logger = logger;
        }

        public async Task<ResponseActionResult> CreateInvoiceConsiderCustomer(long totalMoney, string note, DateTime transactionDate, long bankCardID, long createBy)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.InvoiceService}{LMS.Common.Constants.ActionApiInternal.Invoice_CreateInvoiceConsiderCustomer}";
            var dataPost = new
            {
                TotalMoney = totalMoney,
                Note = note,
                StrTransactionDate = transactionDate.ToString("dd-MM-yyyy HH:mm:ss"),
                UserIDCreate = createBy,
                BankCardID = bankCardID
            };
            try
            {
                return await _apiHelper.ExecuteAsync(url, body: dataPost, method: RestSharp.Method.POST);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateInvoiceConsiderCustomer|url={url}|dataPost={_common.ConvertObjectToJSonV2(dataPost)}|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }

        public async Task<ResponseActionResult> CreateInvoiceCustomer(long totalMoney, string note, DateTime transactionDate, long customerID, long bankCardID, long loanID, long createBy)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.InvoiceService}{LMS.Common.Constants.ActionApiInternal.Invoice_CreateInvoiceCustomer}";
            var dataPost = new
            {
                TotalMoney = totalMoney,
                Note = note,
                StrTransactionDate = transactionDate.ToString("dd-MM-yyyy HH:mm:ss"),
                CustomerID = customerID,
                BankCardID = bankCardID,
                LoanID = loanID,
                UserIDCreate = createBy,
            };
            try
            {
                return await _apiHelper.ExecuteAsync(url, body: dataPost, method: RestSharp.Method.POST);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateInvoiceCustomer|url={url}|dataPost={_common.ConvertObjectToJSonV2(dataPost)}|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }

        public async Task<ResponseActionResult> CreateInvoiceOnBehalfShop(long totalMoney, string note, DateTime transactionDate, long shopCollectID, long toShopID, long customerID, long bankCardID, long loanID, long createBy)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.InvoiceService}{LMS.Common.Constants.ActionApiInternal.Invoice_CreateInvoiceOnBehalfShop}";
            var dataPost = new
            {
                ShopID = shopCollectID,
                ToShopID = toShopID,
                TotalMoney = totalMoney,
                Note = note,
                StrTransactionDate = transactionDate.ToString("dd-MM-yyyy HH:mm:ss"),
                CustomerID = customerID,
                BankCardID = bankCardID,
                LoanID = loanID,
                UserIDCreate = createBy,
                InvoiceSubType = (int)GroupInvoiceType.ReceiptOnBehalfShop
            };
            try
            {
                return await _apiHelper.ExecuteAsync(url, body: dataPost, method: RestSharp.Method.POST);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateInvoiceOnBehalfShop|url={url}|dataPost={_common.ConvertObjectToJSonV2(dataPost)}|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }

        public async Task<ResponseActionResult> CreateInvoicePaySlipCustomer(long totalMoney, long loanID, long customerID, long shopID, DateTime transactionDate, long bankCardID, string note, long userIDCreate)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.InvoiceService}{LMS.Common.Constants.ActionApiInternal.Invoice_CreateInvoicePaySlipCustomer}";
            var dataPost = new
            {
                TotalMoney = totalMoney,
                LoanID = loanID,
                CustomerID = customerID,
                ShopID = shopID,
                StrTransactionDate = transactionDate.ToString("dd-MM-yyyy HH:mm:ss"),
                BankCardID = bankCardID,
                Note = note,
                UserIDCreate = userIDCreate
            };
            try
            {
                return await _apiHelper.ExecuteAsync(url, body: dataPost, method: RestSharp.Method.POST);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateInvoicePaySlipCustomer|url={url}|dataPost={_common.ConvertObjectToJSonV2(dataPost)}|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }


        public async Task<ResponseActionResult> CreateInvoiceBankInterest(long totalMoney, string note, DateTime transactionDate, long bankCardID, long shopID, long createBy, int invoiceSubType)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.InvoiceService}{LMS.Common.Constants.ActionApiInternal.Invoice_CreateInvoiceBankInterest}";
            var dataPost = new
            {
                ShopID = shopID,
                TotalMoney = totalMoney,
                Note = note,
                StrTransactionDate = transactionDate.ToString(_common.DateTimeDDMMYYYYHHMMSS),
                BankCardID = bankCardID,
                UserIDCreate = createBy,
                InvoiceSubType = invoiceSubType
            };
            try
            {
                return await _apiHelper.ExecuteAsync(url, body: dataPost, method: RestSharp.Method.POST);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateInvoiceBankInterest|url={url}|dataPost={_common.ConvertObjectToJSonV2(dataPost)}|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }
    }
}
