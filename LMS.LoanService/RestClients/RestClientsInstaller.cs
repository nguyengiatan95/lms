﻿using LMS.LoanServiceApi.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.RestClients
{
    public static class RestClientsInstaller
    {
        public static IServiceCollection AddRestClientsService(this IServiceCollection services)
        {
            services.AddTransient(typeof(IPaymentScheduleService), typeof(PaymentScheduleService));
            services.AddTransient(typeof(IOutGatewayService), typeof(OutGatewayService));
            services.AddTransient(typeof(ICustomerService), typeof(CustomerService));
            services.AddTransient(typeof(ILenderService), typeof(LenderService));
            services.AddTransient(typeof(IInvoiceService), typeof(InvoiceService));
            services.AddTransient(typeof(IInsuranceService), typeof(InsuranceService));
            services.AddTransient(typeof(ITransactionService), typeof(TransactionService));
            services.AddTransient(typeof(ILOSService), typeof(LOSService));
            services.AddTransient(typeof(IUserServices), typeof(UserServices));
            return services;
        }
    }
}
