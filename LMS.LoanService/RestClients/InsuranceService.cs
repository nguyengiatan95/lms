﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Models.Insurance;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.RestClients
{
    public interface IInsuranceService
    {
        Task<long> CreateInsuranceOfCustomer(long loanID, long customerID);
        Task<long> CreateInsuranceOfLender(long loanID, long lenderID);
        Task<InsurancePremiumsCustomerResponse> InsurancePremiumsOfCustomers(InsurancePremiumsCustomerReq req);
        Task<CalculateMoneyInsuranceLOSResponse> CalculateMoneyInsuranceLOS(CalculateMoneyInsuranceLOSReq req);
        Task<LMS.Common.Constants.ResponseActionResult> CreateInsuranceMaterial(long loanID, long customerID);
    }
    public class InsuranceService : IInsuranceService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        LMS.Common.Helper.Utils _common;
        ILogger<InsuranceService> _logger;
        public InsuranceService(LMS.Common.Helper.IApiHelper apiHelper, ILogger<InsuranceService> logger)
        {
            _apiHelper = apiHelper;
            _common = new Common.Helper.Utils();
            _logger = logger;
        }
        public async Task<long> CreateInsuranceOfCustomer(long loanID, long customerID)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.InsuranceSerivce}{LMS.Common.Constants.ActionApiInternal.Insurance_CreateInsuranceOfCustomer}";
            var dataPost = new
            {
                LoanID = loanID,
                CustomerID = customerID
            };
            return await _apiHelper.ExecuteAsync<long>(url, RestSharp.Method.POST, dataPost);
        }

        public async Task<long> CreateInsuranceOfLender(long loanID, long lenderID)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.InsuranceSerivce}{LMS.Common.Constants.ActionApiInternal.Insurance_CreateInsuranceOfLender}";
            var dataPost = new
            {
                LoanID = loanID,
                LenderID = lenderID
            };
            return await _apiHelper.ExecuteAsync<long>(url, RestSharp.Method.POST, dataPost);
        }
        public async Task<InsurancePremiumsCustomerResponse> InsurancePremiumsOfCustomers(InsurancePremiumsCustomerReq req)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.InsuranceSerivce}{LMS.Common.Constants.ActionApiInternal.Insurance_InsurancePremiumsOfCustomers}";
            return await _apiHelper.ExecuteAsync<InsurancePremiumsCustomerResponse>(url, RestSharp.Method.POST, req);
        }
        public async Task<CalculateMoneyInsuranceLOSResponse> CalculateMoneyInsuranceLOS(CalculateMoneyInsuranceLOSReq req)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.InsuranceSerivce}{LMS.Common.Constants.ActionApiInternal.Insurance_CalculateMoneyInsuranceLOS}";
            return await _apiHelper.ExecuteAsync<CalculateMoneyInsuranceLOSResponse>(url, RestSharp.Method.POST, req);
        }

        public async Task<ResponseActionResult> CreateInsuranceMaterial(long loanID, long customerID)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.InsuranceSerivce}{LMS.Common.Constants.ActionApiInternal.Insurance_CreateInsuranceMaterial}";
            var dataPost = new
            {
                LoanID = loanID,
                CustomerID = customerID
            };
            return await _apiHelper.ExecuteAsync(url, RestSharp.Method.POST, dataPost);
        }
    }
}
