﻿using AutoMapper.Configuration;
using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Models;
using Polly;
using Polly.CircuitBreaker;
using Polly.Retry;
using Steeltoe.Common.Discovery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.RestClients
{

    public interface IPaymentScheduleService
    {
        Task<IEnumerable<LMS.Entites.Dtos.PaymentServices.PaymentScheduleModel>> GetLstPaymentByLoanID(long loanID);
        Task<long> CreatePaymentScheduleByLoanID(long totalMoneyDisbursement, DateTime interestStartDate, int loanTime, int loanFrequency, int rateType, decimal rateConsultant, decimal rateInterest, decimal rateService, long loanID, DateTime? dayMustPay = null);
        Task<LMS.Common.Constants.ResponseActionResult> PayPaymentByLoanID(long loanID, DateTime dayNeedPay, long totalMoneyHas, long createBy, List<int> lstTypeMoney = null, bool checkLastPeriods = false);
        Task<LMS.Common.Constants.ResponseActionResult> PayPartialScheduleInPeriodsByLoanID(long loanID, long totalMoneyPay, Transaction_TypeMoney typeMoney, long createBy);
        Task<ResponseActionResult> CloseLoanFromSchedule(long loanID, long moneyOriginal, long moneyService, long moneyInterest, long moneyConsultant, long moneyFineOriginal, long otherMoney, DateTime closeDate, long createBy);
        Task<ResponseActionResult> UpdateStatusPaymentScheduleByLoan(long loanID, int status);
        Task<ResponseActionResult> CuttingOffPaymentLoanInsuranceByLoanID(long loanID, long totalMoneyPay, Transaction_TypeMoney typeMoney, long createBy);
        Task<ResponseActionResult> ForceCloseLoan(long loanID, long moneyOriginal, long moneyService, long moneyIntereset, long moneyConsultant, long moneyFineLate, long moneyFineOriginal, DateTime closeDate, long createBy, long insurancePartnerCode, int loanStatus);
        Task<long> ExtendPaymentScheduleByLoanID(long totalMoneyDisbursement, int loanTime, int loanFrequency, int rateType, decimal rateConsultant, decimal rateInterest, decimal rateService, long loanID);
        Task<ResponseActionResult> WriteLstMoneyFine(List<LMS.Entites.Dtos.PaymentServices.RequestPaymentChangeMoneyFineModel> dataPost);

        Task<ResponseActionResult> DebtRestructuring(long loanID, int loanRateType, int numberOfRepaymentPeriod, string appointmentDate, int percentDiscount, int typeDebtRestructuring, long createBy, int minDayFineLate);
        Task<ResponseActionResult> CancelMoneyFineByLoanID(long loanID, long moneyFineLate);
        Task<ResponseActionResult> DeleteMoneyFineLateByLoanID(long loanID, long moneyFineLate);
        Task<ResponseActionResult> GenDebtRestructuring(long loanID, int loanRateType, int numberOfRepaymentPeriod, string appointmentDate, int percentDiscount, int typeDebtRestructuring, long createBy, int minDayFineLate);

    }
    public class PaymentScheduleService : IPaymentScheduleService
    {
        readonly LMS.Common.Helper.IApiHelper _apiHelper;
        public PaymentScheduleService(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }

        public async Task<IEnumerable<LMS.Entites.Dtos.PaymentServices.PaymentScheduleModel>> GetLstPaymentByLoanID(long loanID)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.PaymentService}{LMS.Common.Constants.ActionApiInternal.PaymentSchedule_GetLstPaymentByLoanID}/{loanID}";
            return await _apiHelper.ExecuteAsync<IEnumerable<LMS.Entites.Dtos.PaymentServices.PaymentScheduleModel>>(url: url.ToString());
        }
        public async Task<long> CreatePaymentScheduleByLoanID(long totalMoneyDisbursement, DateTime interestStartDate, int loanTime, int loanFrequency, int rateType, decimal rateConsultant, decimal rateInterest, decimal rateService, long loanID, DateTime? dayMustPay = null)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.PaymentService}{LMS.Common.Constants.ActionApiInternal.PaymentSchedule_CreatePaymentScheduleByLoanID}";
            var dataPost = new
            {
                TotalMoneyDisbursement = totalMoneyDisbursement,
                InterestStartDate = interestStartDate.ToString(TimaSettingConstant.DateTimeDayMonthYear),
                LoanTime = loanTime,
                LoanFrequency = loanFrequency,
                RateType = rateType,
                RateConsultant = rateConsultant,
                RateInterest = rateInterest,
                RateService = rateService,
                LoanID = loanID,
                DayMustPay = dayMustPay?.ToString(TimaSettingConstant.DateTimeDayMonthYear) ?? ""
            };
            return await _apiHelper.ExecuteAsync<long>(url, RestSharp.Method.POST, dataPost);
        }

        public async Task<ResponseActionResult> PayPaymentByLoanID(long loanID, DateTime dayNeedPay, long totalMoneyHas, long createBy, List<int> lstTypeMoney = null, bool checkLastPeriods = false)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.PaymentService}{LMS.Common.Constants.ActionApiInternal.PaymentSchedule_PayPaymentByLoanID}";
            var dataPost = new
            {
                LoanID = loanID,
                NextDate = dayNeedPay.ToString(TimaSettingConstant.DateTimeDayMonthYear),
                TotalMoney = totalMoneyHas,
                CreateBy = createBy,
                LstMoneyType = lstTypeMoney,
                CheckLastPeriods = checkLastPeriods
            };
            return await _apiHelper.ExecuteAsync(url, RestSharp.Method.POST, dataPost);
        }
        public async Task<ResponseActionResult> PayPartialScheduleInPeriodsByLoanID(long loanID, long totalMoneyPay, Transaction_TypeMoney typeMoney, long createBy)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.PaymentService}{LMS.Common.Constants.ActionApiInternal.PaymentSchedule_PayPartialScheduleInPeriodsByLoanID}";
            var dataPost = new
            {
                LoanID = loanID,
                TotalMoneyPay = totalMoneyPay,
                TypeMoney = typeMoney,
                CreateBy = createBy
            };
            return await _apiHelper.ExecuteAsync(url, RestSharp.Method.POST, dataPost);
        }

        public async Task<ResponseActionResult> CloseLoanFromSchedule(long loanID, long moneyOriginal, long moneyService, long moneyInterest, long moneyConsultant, long moneyFineOriginal, long otherMoney, DateTime closeDate, long createBy)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.PaymentService}{LMS.Common.Constants.ActionApiInternal.PaymentSchedule_CloseLoanFromSchedule}";
            var dataPost = new
            {
                LoanID = loanID,
                MoneyOriginal = moneyOriginal,
                MoneyService = moneyService,
                MoneyInterest = moneyInterest,
                MoneyConsultant = moneyConsultant,
                MoneyFineOriginal = moneyFineOriginal,
                OtherMoney = otherMoney,
                CloseDate = closeDate.ToString(TimaSettingConstant.DateTimeDayMonthYear),
                CreateBy = createBy
            };
            return await _apiHelper.ExecuteAsync(url, RestSharp.Method.POST, dataPost);
        }

        public async Task<ResponseActionResult> UpdateStatusPaymentScheduleByLoan(long loanID, int status)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.PaymentService}{LMS.Common.Constants.ActionApiInternal.PaymentSchedule_UpdateStatusPaymentScheduleByLoan}";
            var dataPost = new
            {
                LoanID = loanID,
                Status = status,
            };
            return await _apiHelper.ExecuteAsync(url, RestSharp.Method.POST, dataPost);
        }

        public async Task<ResponseActionResult> CuttingOffPaymentLoanInsuranceByLoanID(long loanID, long totalMoneyPay, Transaction_TypeMoney typeMoney, long createBy)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.PaymentService}{LMS.Common.Constants.ActionApiInternal.PaymentSchedule_CuttingOffPaymentLoanInsuranceByLoanID}";
            var dataPost = new
            {
                LoanID = loanID,
                TotalMoneyPay = totalMoneyPay,
                TypeMoney = typeMoney,
                CreateBy = createBy
            };
            return await _apiHelper.ExecuteAsync(url, RestSharp.Method.POST, dataPost);
        }

        public async Task<ResponseActionResult> ForceCloseLoan(long loanID, long moneyOriginal, long moneyService, long moneyInterest, long moneyConsultant
            , long moneyFineLate, long moneyFineOriginal, DateTime closeDate, long createBy, long insurancePartnerCode, int loanStatus)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.PaymentService}{LMS.Common.Constants.ActionApiInternal.PaymentSchedule_ForceCloseLoanByLoan}";
            var dataPost = new
            {
                LoanID = loanID,
                MoneyOriginal = moneyOriginal,
                MoneyService = moneyService,
                MoneyInterest = moneyInterest,
                MoneyConsultant = moneyConsultant,
                MoneyFineLate = moneyFineLate,
                MoneyFineOriginal = moneyFineOriginal,
                InsurancePartnerCode = insurancePartnerCode,
                CloseDate = closeDate.ToString(TimaSettingConstant.DateTimeDayMonthYear),
                CreateBy = createBy,
                LoanStatus = loanStatus,
            };
            return await _apiHelper.ExecuteAsync(url, RestSharp.Method.POST, dataPost);
        }

        public async Task<long> ExtendPaymentScheduleByLoanID(long totalMoneyDisbursement, int loanTime, int loanFrequency, int rateType, decimal rateConsultant, decimal rateInterest, decimal rateService, long loanID)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.PaymentService}{LMS.Common.Constants.ActionApiInternal.PaymentSchedule_ExtendPaymentScheduleByLoanID}";
            var dataPost = new
            {
                TotalMoneyDisbursement = totalMoneyDisbursement,
                LoanTime = loanTime,
                LoanFrequency = loanFrequency,
                RateType = rateType,
                RateConsultant = rateConsultant,
                RateInterest = rateInterest,
                RateService = rateService,
                LoanID = loanID
            };
            return await _apiHelper.ExecuteAsync<long>(url, RestSharp.Method.POST, dataPost);
        }
        public async Task<ResponseActionResult> WriteLstMoneyFine(List<LMS.Entites.Dtos.PaymentServices.RequestPaymentChangeMoneyFineModel> dataPost)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.PaymentService}{LMS.Common.Constants.ActionApiInternal.PaymentSchedule_WriteLstMoneyFine}";
            return await _apiHelper.ExecuteAsync(url, RestSharp.Method.POST, dataPost);
        }

        public async Task<ResponseActionResult> DebtRestructuring(long loanID, int loanRateType, int numberOfRepaymentPeriod, string appointmentDate, int percentDiscount,
            int typeDebtRestructuring, long createBy, int minDayFineLate)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.PaymentService}{LMS.Common.Constants.ActionApiInternal.PaymentSchedule_DebtRestructuring}";
            var dataPost = new
            {
                LoanID = loanID,
                LoanRateType = loanRateType,
                NumberOfRepaymentPeriod = numberOfRepaymentPeriod,
                AppointmentDate = appointmentDate,
                PercentDiscount = percentDiscount,
                TypeDebtRestructuring = typeDebtRestructuring,
                MinDayFineLate = minDayFineLate,
                CreateBy = createBy
            };
            return await _apiHelper.ExecuteAsync(url, RestSharp.Method.POST, dataPost);
        }
        public async Task<ResponseActionResult> CancelMoneyFineByLoanID(long loanID, long moneyFineLate)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.PaymentService}{LMS.Common.Constants.ActionApiInternal.PaymentSchedule_CancelMoneyFineByLoanID}";
            var dataPost = new
            {
                LoanID = loanID,
                MoneyFineLate = moneyFineLate
            };
            return await _apiHelper.ExecuteAsync(url, RestSharp.Method.POST, dataPost);
        }
        public async Task<ResponseActionResult> DeleteMoneyFineLateByLoanID(long loanID, long moneyFineLate)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.PaymentService}{LMS.Common.Constants.ActionApiInternal.PaymentSchedule_DeleteMoneyFineLateByLoanID}";
            var dataPost = new
            {
                LoanID = loanID,
                MoneyFineLate = moneyFineLate
            };
            return await _apiHelper.ExecuteAsync(url, RestSharp.Method.POST, dataPost);
        }
        public async Task<ResponseActionResult> GenDebtRestructuring(long loanID, int loanRateType, int numberOfRepaymentPeriod, string appointmentDate, int percentDiscount,
           int typeDebtRestructuring, long createBy, int minDayFineLate)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.PaymentService}{LMS.Common.Constants.ActionApiInternal.PaymentSchedule_GenDebtRestructuring}";
            var dataPost = new
            {
                LoanID = loanID,
                LoanRateType = loanRateType,
                NumberOfRepaymentPeriod = numberOfRepaymentPeriod,
                AppointmentDate = appointmentDate,
                PercentDiscount = percentDiscount,
                TypeDebtRestructuring = typeDebtRestructuring,
                MinDayFineLate = minDayFineLate,
                CreateBy = createBy
            };
            return await _apiHelper.ExecuteAsync(url, RestSharp.Method.POST, dataPost);
        }
    }
}
