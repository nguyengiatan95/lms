﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Common.Constants;

namespace LMS.LoanServiceApi.RestClients
{
    public interface ITransactionService
    {
        Task<long> CreateTransaction(List<LMS.Entites.Dtos.TransactionServices.RequestCreateListTransactionModel> request);
        Task<List<Entites.Dtos.TransactionServices.ListTransactionView>> GetByLoanID(long LoanID);
    }
    public class TransactionService : ITransactionService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        public TransactionService(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }
        public async Task<long> CreateTransaction(List<LMS.Entites.Dtos.TransactionServices.RequestCreateListTransactionModel> request)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.TransactionService}{LMS.Common.Constants.ActionApiInternal.Transaction_Action_CreateTransactionAfterPayment}";
            return await _apiHelper.ExecuteAsync<long>(url: url, body: request, method: RestSharp.Method.POST);
        }

        public async Task<List<Entites.Dtos.TransactionServices.ListTransactionView>> GetByLoanID(long LoanID)
        {
            string fullPath = string.Format(LMS.Common.Constants.ActionApiInternal.Transaction_Action_GetByLoanID, LoanID);
            var url = $"{LMS.Common.Constants.ServiceHostInternal.TransactionService}{fullPath}";
            var lstTransaction = await _apiHelper.ExecuteAsync<List<Entites.Dtos.TransactionServices.ListTransactionView>>(url: url, body: null, method: RestSharp.Method.GET);
            return lstTransaction;
        }
    }
}
