﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.AG;
using LMS.Entites.Dtos.APIAutoDisbursement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.RestClients
{
    public interface IOutGatewayService
    {
        Task<LMS.Entites.Dtos.LOSServices.ResultLoanWaitingDisbursementDetailLOS> GetLoanCreditDetailAsync(int loanID);
        Task<ResponseActionResult> VibCreateTransaction(LMS.Entites.Dtos.APIAutoDisbursement.CreateTransactionRequest dataPost);
        Task<ResponseActionResult> VibExecuteOtp(LMS.Entites.Dtos.APIAutoDisbursement.ExecuteOtpRequest dataPost);
        Task<ResponseActionResult> VibCancelTransaction(CancelTransactionRequest dataPost);
        Task<ResponseActionResult> CallApiAg(AgDataPostModel model);
    }
    public class OutGatewayService : IOutGatewayService
    {
        public const string Los_Action_GetLoanCreditDetail = "/api/Los/GetLoanCreditDetail";
        LMS.Common.Helper.IApiHelper _apiHelper;
        public OutGatewayService(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }

        public async Task<LMS.Entites.Dtos.LOSServices.ResultLoanWaitingDisbursementDetailLOS> GetLoanCreditDetailAsync(int loanID)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{Los_Action_GetLoanCreditDetail}/{loanID}";
            return await _apiHelper.ExecuteAsync<LMS.Entites.Dtos.LOSServices.ResultLoanWaitingDisbursementDetailLOS>(url: url.ToString());
        }

        public async Task<ResponseActionResult> VibCancelTransaction(CancelTransactionRequest dataPost)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_Payment_VIBCancelTransaction}";            
            return await _apiHelper.ExecuteAsync(url: url, method: RestSharp.Method.POST, dataPost);
        }

        public async Task<ResponseActionResult> VibCreateTransaction(CreateTransactionRequest dataPost)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_Payment_VIBCreateTransaction}";
            return await _apiHelper.ExecuteAsync(url: url, method: RestSharp.Method.POST, dataPost);
        }

        public async Task<ResponseActionResult> VibExecuteOtp(ExecuteOtpRequest dataPost)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_Payment_VIBExecuteOtp}";
            return await _apiHelper.ExecuteAsync(url: url, method: RestSharp.Method.POST, dataPost);
        }
        public async Task<ResponseActionResult> CallApiAg(AgDataPostModel request)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_CallApiAG}";
            return await _apiHelper.ExecuteAsync(url: url.ToString(), RestSharp.Method.POST, request);
        }
    }
}
