﻿using System;

namespace LMS.LoanServiceApi.Domain.Models
{
    public class RequestUpdateLoanAfterPaymentModel
    {
        public long LoanID { get; set; }
        public DateTime? NextDate { get; set; }
        public DateTime? LastDateOfPay { get; set; }
        public long OriginalMoney { get; set; }
    }
}
