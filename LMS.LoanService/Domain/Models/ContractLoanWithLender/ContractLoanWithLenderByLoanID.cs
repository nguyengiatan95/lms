﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Models.ContractLoanWithLender
{
    public class ContractLoanWithLenderByLoanID
    {
        public long LoanID { get; set; }
        public string ContactCode { get; set; }
        public string LenderCode { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime CreateDate { get; set; }
        public string AccountingMethod { get; set; }
    }
}
