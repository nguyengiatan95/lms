﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Models.SMS
{
    public class RequestSmsItemViewModel
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int Status { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public long ShopID { get; set; }
        public int TotalRow { get; set; }
        public RequestSmsItemViewModel()
        {
            Status = (int)Common.Constants.StatusCommon.SearchAll;
            ShopID = (int)Common.Constants.StatusCommon.SearchAll;
        }
    }
}
