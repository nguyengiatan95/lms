﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Models.SMS
{
    public class SmsIncreatedMoneyCustomerRequest
    {
        public int CustomerID { get; set; }
        public string ReferenceID { get; set; }
        public long TotalMoney { get; set; }
        public string ReceivedDate { get; set; }
        /// <summary>
        /// 0: momo, 1: Gpay, 2: Gutina
        /// enum SmsAnaylyticSourceSendRequest
        /// </summary>
        public int Source { get; set; }

        public long SmsTimaID { get; set; }

        public string CustomerPhone { get; set; }
        public string CustomerNumberCard { get; set; }
    }

    public class SmsTransactionBankRequest
    {
        public string Sender { get; set; }
        public string SmsContent { get; set; }
        public string ReceivedDate { get; set; } // dd/MM/yyyy HH:mm:ss
        public long SmsTimaID { get; set; }
        public long BankCardID { get; set; }
    }

    public class UpdateStatusSmsTransactionBankRequest
    {
        public long SmsAnalyticID { get; set; }
        public int Status { get; set; }
        public long SmsTimaID { get; set; }
        public long CreateBy { get; set; }
        public long TimaLoanID { get; set; }
        public long TimaCustomerID { get; set; }
        public long TimaBankCardID { get; set; }
        public long TimaIncreaseMoney { get; set; }
    }
}
