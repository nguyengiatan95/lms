﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Models.Insurance
{
    public class InsurancePremiumsCustomerReq
    {
        public long TotalMoneyDisbursement { get; set; }
        public int LoanTime { get; set; }
        public int LoanFrequency { get; set; }
        public int RateType { get; set; }
        public decimal RateConsultant { get; set; }
        public decimal RateInterest { get; set; }
        public decimal RateService { get; set; }
        public DateTime InterestStartDate { get; set; }
    }
    public class InsurancePremiumsCustomerResponse
    {
        public long InsuranceMoney { get; set; }
        public long InterestFees { get; set; }
    }
}
