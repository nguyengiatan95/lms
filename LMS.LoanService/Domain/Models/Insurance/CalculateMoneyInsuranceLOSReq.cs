﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Models.Insurance
{
    public class CalculateMoneyInsuranceLOSReq
    {
        public long TotalMoneyDisbursement { get; set; }
        public int LoanTime { get; set; }
        public int LoanFrequency { get; set; }
        public int RateType { get; set; }
        public decimal RateConsultant { get; set; }
        public decimal RateInterest { get; set; }
        public decimal RateService { get; set; }
        public DateTime InterestStartDate { get; set; }
    }
    public class CalculateMoneyInsuranceLOSResponse
    {
        public decimal FeesInsuranceCustomer { get; set; }
        public decimal FeesInsuranceMaterial { get; set; }
    }
}
