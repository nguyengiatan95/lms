﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Models.Loan
{
    public class RequestExtendLoanTime
    {
        [Required]
        public long LoanID { get; set; }
        [Required]
        public int PeriodExtend { get; set; }
        public string Note { get; set; }
        public long UserID { get; set; }
        public long TimaLoanID { get; set; }
    }
}
