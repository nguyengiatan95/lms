﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Models.Loan
{
    public class ProcessAutoPaymentModel
    {
        public long LatestCustomerID { get; set; }
        public int TotalProcessSuccess { get; set; }
    }

    public class LoanPaymentAutoModel
    {
        public long LoanID { get; set; }
        public long CustomerID { get; set; }
        public DateTime NextDate { get; set; }
        public string ContactCode { get; set; }
        /// <summary>
        /// 0: đóng hợp đồng, 1: gạch nợ kỳ
        /// </summary>
        public int TypeAction { get; set; }

        public Task ActionResult { get; set; }
    }
}
