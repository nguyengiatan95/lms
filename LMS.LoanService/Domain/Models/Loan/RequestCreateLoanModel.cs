﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Models.Loan
{
    public class RequestCreateLoanModel
    {
        public long LoanCreditID { get; set; }
        public long BankCardID { get; set; }
        public long LoanAgID { get; set; }
        public int SourceBankDisbursement { get; set; }
        /// <summary>
        /// phí bảo hiểm sức khỏe
        /// </summary>
        public long MoneyfeeInsuranceOfCustomer { get; set; }
        /// <summary>
        /// phí bảo hiểm vật chất
        /// </summary>
        public long MoneyFeeInsuranceMaterialCovered { get; set; }

        public long CreateBy { get; set; }
        public string Note { get; set; }
        public int IsHandleException { get; set; } // 0: tạo bình thường, 1: tạo thay cho NA

        public long AgCodeID { get; set; }

        public long MoneyFeeDisbursement { get; set; } // phí giải ngân, không gán giá trị âm\

        public long TimaCustomerID { get; set; }
        public DateTime? FromDate { get; set; }
    }
}
