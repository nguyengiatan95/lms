﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Models.Loan
{
    public class RequestSendStatusInsuranceModel
    {
        public long LoanID { get; set; }
        public long UserID { get; set; }
        public string Note { get; set; }
        public int StatusSendInsurance { get; set; }
    }
}
