﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Models.Loan
{
    public class ProcessAutoFineLateModel
    {
        public long LatestLoanID { get; set; }
        public int TotalProcessSuccess { get; set; }
    }
}
