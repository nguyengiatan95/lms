﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Models.Loan
{
    public class TranferListLoanLender
    {
        public long ContractCode { get; set; }
        public string LenderCode { get; set; }
        public string FromDate { get; set; }
        public string CurrentLenderCode { get; set; }

    }
}
