﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Models.Loan
{
    public class LoanExtraInsertDetailModel
    {
        public long LoanID { get; set; }
        public long CreateBy { get; set; }
        public int TypeMoney { get; set; }
        public DateTime PayDate { get; set; }
        public long TotalMoney { get; set; }
    }
}
