﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Models.Loan
{
    public class RequestLoanItemViewModel
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string LoanCode { get; set; }
        //enum: Loan_StatusFilterRequest
        public int LoanStatus { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public long ShopID { get; set; }
        public int TotalRow { get; set; }
        public int ProductID { get; set; }
        public long LenderID { get; set; }
        public long ConsultantShopID { get; set; }
        public long LoanID { get; set; }

        public List<long> LstCustomerID { get; set; }
        public RequestLoanItemViewModel()
        {
            LoanStatus = (int)Common.Constants.StatusCommon.SearchAll;
            ShopID = (int)Common.Constants.StatusCommon.SearchAll;
            ProductID = (int)Common.Constants.StatusCommon.SearchAll;
            LenderID = (int)Common.Constants.StatusCommon.SearchAll;
            ConsultantShopID = (int)Common.Constants.StatusCommon.SearchAll;
        }
    }
}
