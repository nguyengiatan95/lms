﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Models
{
    public class CreateLoanCommandModel
    {
        [Required]
        public int LoanCreditId { get; set; }
        [Required]
        public int ShopLenderId { get; set; }
        [Required]
        public long MoneyfeeInsuranceOfCustomer { get; set; }
        [Required]
        public int SourceBankDisbursement { get; set; }
        [Required]
        public int IsHandleException { get; set; }
    }
    public class CreateBorrowerCommandRequest
    {
        public string CustomerName { get; set; }
        public string NumberCard { get; set; }
        public string Phone { get; set; }
        public int Gender { get; set; }
        public int CityID { get; set; }
        public int DistrictID { get; set; }
        public int WardID { get; set; }
        public string Address { get; set; }
        public string AddressHouseHold { get; set; }
        public string PermanentAddress { get; set; }
        public string BirthDay { get; set; }
        public long TimaCustomerID { get; set; }
    }
}
