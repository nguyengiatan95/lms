﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Models.CashBankCardByDate
{
    public class ReportInOutMoneyBankDate
    {
        public DateTime ForDate { get; set; }

        public long BankCardID { get; set; }

        public long MoneyBeginDate { get; set; }

        public long MoneyEndDate { get; set; }
        public long MoneyInBound { get; set; }
        public long MoneyOutBound { get; set; }
        public string BankCardName { get; set; }
    }
}
