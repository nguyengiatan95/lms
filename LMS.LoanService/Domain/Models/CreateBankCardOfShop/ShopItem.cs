﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Models.CreateBankCardOfShop
{
    public class ShopItem
    {
        public long ShopID { get; set; }
        public string ShopName { get; set; }
    }
}
