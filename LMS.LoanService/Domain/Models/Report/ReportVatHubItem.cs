﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Models.Report
{
    public class ReportVatHubItem
    {
        public string Denominator { get; set; }// Mẫu số
        public string Notation { get; set; }// Ký hiệu
        public int TaxPercentage { get; set; }// % thuế
        public string TypePayment { get; set; }// hình thức thanh toán
        public string CommodityName { get; set; }// tên hàng
        public string OrderNumber { get; set; }// số đơn hàng
        public string HubName { get; set; }
        public string ContactCode { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public DateTime? TransactionDate { get; set; }
        public decimal FeeNotVat { get; set; }
        public decimal Vat { get; set; }
        public long TotalMoney { get; set; }
        public string TypeTransaction { get; set; }
    }
}
