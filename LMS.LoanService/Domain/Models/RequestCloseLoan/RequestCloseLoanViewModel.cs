﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Models.RequestCloseLoan
{
    public class RequestCloseLoanViewModel
    {
        public long RequestCloseLoanID { get; set; }
        public long LoanID { get; set; }
        public string LoanContractCode { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public string RequestByName { get; set; }
        public string ApprovedByName { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public DateTime CloseDate { get; set; }
        public string Reason { get; set; }
        public string Note { get; set; }
    }
}
