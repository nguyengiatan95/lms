﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblReportStasticsLoanBorrow")]
    public class TblReportStasticsLoanBorrow
    {
        [Dapper.Contrib.Extensions.Key]
        public long ReportStasticsLoanBorrowID { get; set; }

        public long ShopID { get; set; }

        public long DebtMoneyBorrow { get; set; }

        public int TotalLoanBorrow { get; set; }

        public long OverDebtMoneyBorrow { get; set; }

        public int TotalLoanBorrowOverDebt { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }
    }
}
