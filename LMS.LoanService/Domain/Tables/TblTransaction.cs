﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblTransaction")]
    public class TblTransaction
    {
        [Dapper.Contrib.Extensions.Key]
        public long TransactionID { get; set; }

        public long? LoanID { get; set; }

        public long TotalMoney { get; set; }

        public int ActionID { get; set; }

        public int MoneyType { get; set; }

        public long? PaymentScheduleID { get; set; }

        public long? CustomerID { get; set; }

        public long? LenderID { get; set; }

        public DateTime CreateDate { get; set; }

        public long? UserID { get; set; }

        public long? TimaTransactionID { get; set; }

        public long? TimaInsuranceTransactionID { get; set; }
        public long ShopID { get; set; }

    }
}
