﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblLogCallApi")]
    public class TblLogCallApi
    {
        [Dapper.Contrib.Extensions.Key]
        public long LogCallApiID { get; set; }

        public int ActionCallApi { get; set; }

        public string NameActionCallApi { get; set; }

        public string InputStringCallApi { get; set; }

        public string LinkCallApi { get; set; }

        public int Status { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }

        public long LoanID { get; set; }

        public string ResultStringCallApi { get; set; }

        public long LoanCreditID { get; set; }
        public string TraceIndentifierRequest { get; set; }
        public string TraceIndentifierResponse { get; set; }
        public int CountRetry { get; set; }

    }

}
