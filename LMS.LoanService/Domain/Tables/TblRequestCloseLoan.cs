﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblRequestCloseLoan")]
    public class TblRequestCloseLoan
    {
        [Dapper.Contrib.Extensions.Key]
        public long RequestCloseLoanID { get; set; }

        public long LoanID { get; set; }
        public DateTime CloseDate { get; set; }
        /// <summary>
        /// enum RequestCloseLoan_Status
        /// </summary>
        public int Status { get; set; }

        public string Note { get; set; }

        public string Reason { get; set; }

        public long RequestByUserID { get; set; }

        public long? ApprovedByUserID { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public string JsonExtra { get; set; }
    }

    public class RequestCloseLoanJsonExtra
    {
        public string RequestByName { get; set; }
        public string ApprovedByName { get; set; }
        public string LoanContractCode { get; set; }
        public List<RequestCloseLoanTimeHandle> TimeHandles { get; set; }
    }
    public class RequestCloseLoanTimeHandle
    {
        public DateTime TimeHandle { get; set; }
        public int Status { get; set; }
        public long CreateBy { get; set; }
    }

}
