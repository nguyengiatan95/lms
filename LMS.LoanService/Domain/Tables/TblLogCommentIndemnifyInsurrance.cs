﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Table("TblLogCommentIndemnifyInsurrance")]
    public class TblLogCommentIndemnifyInsurrance
    {
        [Key]
        public long LogCommentIndemnifyInsurranceID { get; set; }

        public long LoanID { get; set; }

        public string CodeID { get; set; }

        public string CustomerName { get; set; }

        public string Note { get; set; }

        public DateTime CreateDate { get; set; }

        public string UserName { get; set; }

        public long UserID { get; set; }

    }
}
