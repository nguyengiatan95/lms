﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Table("TblLogSendStatusInsurance")]
    public class TblLogSendStatusInsurance
    {
        [Key]
        public long LogSendStatusInsuranceID { get; set; }

        public long LoanID { get; set; }

        public int UserID { get; set; }

        public string Note { get; set; }

        public short? StatusNew { get; set; }

        public short? StatusOld { get; set; }

        public DateTime CreateDate { get; set; }
    }
}
