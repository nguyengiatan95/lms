﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Table("TblLoanIndemnifyInsurance")]
    public class TblLoanIndemnifyInsurance
    {
        [Key]
        public long LoanIndemnifyInsuranceID { get; set; }

        public long LoanId { get; set; }

        public long TotalMoneyCurrent { get; set; }

        public string ContactCode { get; set; }

        public long MoneyIndemnification { get; set; }

        public long MoneyInterest { get; set; }

        public int Status { get; set; }

        public DateTime CreateOn { get; set; }

        public int? InsuranceCompensatorID { get; set; }

        public int? UserID { get; set; }
    }
}
