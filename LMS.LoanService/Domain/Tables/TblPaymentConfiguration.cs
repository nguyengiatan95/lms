﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Table("TblPaymentConfiguration")]
    public class TblPaymentConfiguration
    {
        [Key]
        public long ConfigurationPaymentID { get; set; }

        public int AccountingPriorityID { get; set; }

        public string ColumnName { get; set; }

        public short Position { get; set; }

        public short? Status { get; set; }

        public short? IsLender { get; set; }
    }
}
