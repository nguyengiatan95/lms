﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblDistrict")]
    public class TblDistrict
    {
        [Dapper.Contrib.Extensions.Key]
        public long DistrictID { get; set; }

        public string Name { get; set; }

        public string TypeDistrict { get; set; }

        public string LongitudeLatitude { get; set; }

        public long? CityID { get; set; }

        public int? AreaID { get; set; }

        public int? IsApply { get; set; }

    }
}
