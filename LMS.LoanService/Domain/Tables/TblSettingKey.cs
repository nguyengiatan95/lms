﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("tblSettingKey")]
    public class TblSettingKey
    {
        [Dapper.Contrib.Extensions.Key]
        public long SettingKeyID { get; set; }
        public string KeyName { get; set; }
        public string Value { get; set; }
        public int Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public int? Sleep { get; set; }
        public string EndDate { get; set; }
    }

    public class SettingKeyValueAutoPayment
    {
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public long LatestCustomerID { get; set; }
    }

    public class SettingKeyValueAutoDisbursement
    {
        /// <summary>
        /// ID đang xử lý
        /// </summary>
        public long LoanCreditDisbursementAutoIDProcessing { get; set; }
        /// <summary>
        /// thời gian tắt gần nhất
        /// </summary>
        public DateTime LastTurnOff { get; set; }
        /// <summary>
        /// thời gian giải ngân thành công gần nhất
        /// </summary>
        public DateTime LastTimeSuccess { get; set; }
        /// <summary>
        /// số lần tìm sms có chứa otp
        /// </summary>
        public int RetryFindOtp { get; set; }
        /// <summary>
        /// số lần thử gọi lại lấy otp
        /// </summary>
        public int RetryCreateOtp { get; set; }
        /// <summary>
        /// đơn vị tính thông báo cho team kt
        /// </summary>
        public int CountPushNotiAccountant { get; set; }
    }

    public class SettingKeyValueAutoFineLate
    {
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public long LatestLoanID { get; set; }
        public DateTime DateApplyFine { get; set; }
    }

    public class SettingKeyValuePlanCloseLoan
    {
        public DateTime CloseDate { get; set; }
        public long CountTimeHandleFinished { get; set; }
        public long LatestLoanID { get; set; }
    }

    public class SettingKeyValueCutOffLoan
    {
        public DateTime CutOffDate { get; set; }
        public long CountTimeHandleFinished { get; set; }
        public long LatestLoanID { get; set; }
    }

    public class SettingKeyValueSplitTransaction
    {
        public long CountTimeHandleFinished { get; set; }
        public long LatestTransactionID { get; set; }
        public bool IsContinous { get; set; }
    }
}
