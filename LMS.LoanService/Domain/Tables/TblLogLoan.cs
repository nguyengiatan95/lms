﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblLogLoan")]
    public class TblLogLoan
    {
        [Dapper.Contrib.Extensions.Key]
        public long LogLoanID { get; set; }

        public long? LoanID { get; set; }

        public string ColumnName { get; set; }

        public string OldValue { get; set; }

        public string NewValue { get; set; }

        public string Request { get; set; }

        public int? CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
    }
    public class IgnoreLoggingAttribute : Attribute
    {
    }
    public class PrimaryKeyAttribute : Attribute
    {
    }
}
