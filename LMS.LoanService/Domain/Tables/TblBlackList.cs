﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblBlackList")]
    public class TblBlackList
    {
        [Dapper.Contrib.Extensions.Key]
        public long BlackListID { get; set; }

        public long CustomerCreditID { get; set; }

        public string FullName { get; set; }

        public string NumberPhone { get; set; }

        public string CardNumber { get; set; }

        public DateTime? BirthDay { get; set; }

        public long LoanID { get; set; }

        public long UserIDCreate { get; set; }

        public string UserNameCreate { get; set; }

        public string FullNameCreate { get; set; }

        public DateTime CreateOn { get; set; }

        public int Status { get; set; }

        public long UserIDModify { get; set; }

        public string UserNameModify { get; set; }

        public string FullNameModify { get; set; }

        public DateTime ModifyDate { get; set; }

        public string Note { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public int? NumberDay { get; set; }

        public long LoanCreditID { get; set; }

        public long UserIDApprove { get; set; }

        public string UserNameApprove { get; set; }

        public string FullNameApprove { get; set; }

        public DateTime? TimeApprove { get; set; }
    }

}
