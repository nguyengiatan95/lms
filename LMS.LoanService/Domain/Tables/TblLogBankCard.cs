﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblLogBankCard")]
    public class TblLogBankCard
    {
        [Dapper.Contrib.Extensions.Key]
        public long LogBankCardID { get; set; }

        public long BankCardID { get; set; }

        public long TotalMoney { get; set; }

        public DateTime CreateDate { get; set; }
        public long UserID { get; set; }

        public int JobStatus { get; set; }
    }
}
