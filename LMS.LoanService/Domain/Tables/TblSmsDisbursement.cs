﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("tblSmsDisbursement")]
    public class TblSmsDisbursement
    {
        [Dapper.Contrib.Extensions.Key]
        public long SmsDisbursementID { get; set; }
        public string Sender { get; set; }

        public string SmsContent { get; set; }

        public string OTP { get; set; }

        public long LoanCreditID { get; set; }

        public long LoanID { get; set; }

        public int Status { get; set; }

        public long Amount { get; set; }

        public string AccountNumberBanking { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string SmsContentHash { get; set; }
        
        public long SmsTimaID { get; set; }
    }
}
