﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Table("TblContractLoanWithLender")]
    public class TblContractLoanWithLender
    {
        [Key]
        public long ContractLoanWithLenderID { get; set; }

        public long LoanID { get; set; }

        public long TimaLoanID { get; set; }

        public long LenderID { get; set; }
        /// <summary>
        /// tính từ đầu ngày có hiệu lực
        /// </summary>
        public DateTime FromDate { get; set; }
        /// <summary>
        /// Tính đến hết ngày có hiệu lực
        /// </summary>
        public DateTime ToDate { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }

        public long AccountingPriorityOrderID { get; set; }

        public decimal RateInterest { get; set; }
        public decimal RateService { get; set; }
        public decimal RateConsultant { get; set; }
        public int SelfEmployed { get; set; }
        public int UnitRate { get; set; }
    }
}
