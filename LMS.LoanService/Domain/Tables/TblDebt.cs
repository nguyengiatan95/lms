﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblDebt")]
    public class TblDebt
    {
        [Dapper.Contrib.Extensions.Key]
        public long DebtID { get; set; }

        public long ReferID { get; set; }

        public long TotalMoney { get; set; }

        public int TypeID { get; set; }

        public long TargetID { get; set; }

        public string Description { get; set; }

        public int Status { get; set; }

        public DateTime CreateDate { get; set; }

        public long CreateBy { get; set; }
        public long SourceID { get; set; }
        public long ParentDebtID { get; set; }
    }
}
