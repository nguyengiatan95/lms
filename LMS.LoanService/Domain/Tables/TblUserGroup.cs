﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Table("TblUserGroup")]
    public class TblUserGroup
    {
        [Key]
        public long UserGroupID { get; set; }

        public long UserID { get; set; }

        public long GroupID { get; set; }
    }
}
