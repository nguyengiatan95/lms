﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblBankCardOfShop")]
    public class TblBankCardOfShop
    {
        [Dapper.Contrib.Extensions.Key]
        public long BankCardOfShopID { get; set; }

        public long ShopID { get; set; }

        public long BankCardID { get; set; }

        public DateTime CreateDate { get; set; }

        public string ShopName { get; set; }
        public string BankCardName { get; set; }
    }
}
