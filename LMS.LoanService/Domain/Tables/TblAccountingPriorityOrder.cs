﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Table("TblAccountingPriorityOrder")]
    public class TblAccountingPriorityOrder
    {
        [Key]
        public long AccountingPriorityID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public short? Status { get; set; }
    }
}
