﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Table("TblLoanCreditDisbursementAuto")]
    public class TblLoanCreditDisbursementAuto
    {
        [Key]
        public long LoanCreditDisbursementAutoID { get; set; }

        public long LoanCreditID { get; set; }

        public string AccountNumberBanking { get; set; }

        public string FullName { get; set; }

        public long MoneyDisbursement { get; set; }

        public int? DisbursementType { get; set; }

        public int? Status { get; set; }

        public int CountRetry { get; set; }

        public string JsonExtra { get; set; }

        public DateTime CreateDate { get; set; }

        public int? LenderID { get; set; }

        public long? TotalMoneyDisbursement { get; set; }

        public DateTime? ModifyDate { get; set; }
        public long MoneyFeeInsuranceOfCustomer { get; set; }
        public long MoneyFeeInsuranceMaterialCovered { get; set; }

    }
}
