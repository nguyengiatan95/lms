﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblPlanCloseLoan")]
    public class TblPlanCloseLoan
    {
        [Dapper.Contrib.Extensions.Key]
        public long PlanCloseLoanID { get; set; }

        public long LoanID { get; set; }

        public DateTime NextDate { get; set; }

        public DateTime CloseDate { get; set; }

        public long TotalMoneyClose { get; set; }

        public long MoneyOriginal { get; set; }

        public long MoneyInterest { get; set; }

        public long MoneyService { get; set; }

        public long MoneyConsultant { get; set; }

        public long MoneyFineLate { get; set; }
        public long MoneyFineOriginal { get; set; }
        public long TotalMoneyReceivables { get; set; }

        public DateTime CreateDate { get; set; }
        public long TotalMoneyNeedPay { get; set; }
        public long MoneyOriginalNeedPay { get; set; }
        public long MoneyInterestNeedPay { get; set; }
        public long MoneyServiceNeedPay { get; set; }
        public long MoneyConsultantNeePay { get; set; }
        public long MoneyFineLateNeedPay { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int CountChange { get; set; }
    }
}
