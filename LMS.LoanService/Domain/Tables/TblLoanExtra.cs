﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblLoanExtra")]
    public class TblLoanExtra
    {
        [Dapper.Contrib.Extensions.Key]
        public long LoanExtraID { get; set; }

        public long LoanID { get; set; }

        public long TimaLoanID { get; set; }

        public DateTime PayDate { get; set; }

        public int TypeMoney { get; set; }

        public long TotalMoney { get; set; }

        public long CreateBy { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime? ModifyDate { get; set; }

        public long TimaLoanExtraID { get; set; }
    }
}
