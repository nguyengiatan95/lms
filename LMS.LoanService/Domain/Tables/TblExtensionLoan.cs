﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{
    [Table("TblExtensionLoan")]
    public class TblExtensionLoan
    {
        [Key]
        public long ExtensionLoanID { get; set; }

        public long UserID { get; set; }

        public long LoanID { get; set; }

        public DateTime OldDate { get; set; }

        public DateTime NewDate { get; set; }

        public string Note { get; set; }

        public DateTime CreateDate { get; set; }
        public int PeriodExtend { get; set; }
    }
}
