﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Domain.Tables
{

    [Dapper.Contrib.Extensions.Table("tblSmsAnalytics")]
    public class TblSmsAnalytics
    {
        [Dapper.Contrib.Extensions.Key]
        public int SmsAnalyticsID { get; set; }

        public DateTime CreateDate { get; set; }

        public string Sender { get; set; }

        public string SmsContent { get; set; }

        public DateTime? IncreaseTime { get; set; }

        public long? IncreaseMoney { get; set; }

        public string CustomerName { get; set; }

        public string IdCard { get; set; }

        public string Phone { get; set; }

        public DateTime? SmsreceivedDate { get; set; }

        public int Status { get; set; }

        public string AcountNumber { get; set; }

        public long CurrentTotalMoney { get; set; }

        public long LoanID { get; set; }

        public long LoanCodeID { get; set; }

        public long CustomerID { get; set; }

        public long BankCardID { get; set; }

        public string BankCardAliasName { get; set; }

        public int? TypeTransactionBank { get; set; }

        public string NameTransactionBank { get; set; }

        public short? TransactionBankCardStatus { get; set; }

        public int? IsAnalytics { get; set; }

        public int? ShopID { get; set; }

        public DateTime? ModifyDate { get; set; }

        public string ShopName { get; set; }

        public int SmsType { get; set; }

        public string ContentCancel { get; set; }

        public int? TransactionBankCardId { get; set; }

        public int? ReasonIdCancel { get; set; }

        public string SmsContentHashIndex { get; set; }
        public int CuttingOffDebtType { get; set; }
        public long SmsTimaID { get; set; }
        public string ContractCode { get; set; }
    }
}
