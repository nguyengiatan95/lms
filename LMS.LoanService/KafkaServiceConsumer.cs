﻿using LMS.Kafka.Consumer;
using LMS.Kafka.Interfaces;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi
{
    public class KafkaServiceConsumer : BackgroundService
    {
        IKafkaConsumer<string, LMS.Kafka.Messages.Customer.CustomerInsertSuccess> _loanCustomerConsumer;
        IKafkaConsumer<string, LMS.Kafka.Messages.Payment.UpdateNextDateLoanInfo> _loanUpdateNextDateConsumer;
        IKafkaConsumer<string, LMS.Kafka.Messages.Loan.LoanPaymentSuccess> _loanPaymentSuccessConsumer;

        ILogger<KafkaServiceConsumer> _logger;
        public KafkaServiceConsumer(IKafkaConsumer<string, LMS.Kafka.Messages.Customer.CustomerInsertSuccess> LoanCustomerConsumer,
            IKafkaConsumer<string, LMS.Kafka.Messages.Payment.UpdateNextDateLoanInfo> loanUpdateNextDateConsume,
            IKafkaConsumer<string, LMS.Kafka.Messages.Loan.LoanPaymentSuccess> loanPaymentSuccessConsume,
            ILogger<KafkaServiceConsumer> logger)
        {
            _loanCustomerConsumer = LoanCustomerConsumer;
            _loanUpdateNextDateConsumer = loanUpdateNextDateConsume;
            _loanPaymentSuccessConsumer = loanPaymentSuccessConsume;
            _logger = logger;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                //var t1 = _loanCustomerConsumer.Consume(Kafka.Constants.KafkaTopics.LoanUpdateCustomerIDInfo, stoppingToken);
                var t2 = _loanUpdateNextDateConsumer.Consume(Kafka.Constants.KafkaTopics.PaymentUpdateNextDateLoanInfo, stoppingToken);
                var t3 = _loanPaymentSuccessConsumer.Consume(Kafka.Constants.KafkaTopics.LoanPaymentSuccess, stoppingToken);
                await Task.WhenAll(t2, t3);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{(int)HttpStatusCode.InternalServerError} LMS.LoanServiceApi_KafkaServiceConsumer|ex={ex.Message}-{ex.StackTrace}");
            }
        }

        public override void Dispose()
        {
            _loanCustomerConsumer.Close();
            _loanCustomerConsumer.Dispose();
            _loanUpdateNextDateConsumer.Close();
            _loanUpdateNextDateConsumer.Dispose();
            _loanPaymentSuccessConsumer.Close();
            _loanPaymentSuccessConsumer.Dispose();
            base.Dispose();
        }
    }
}
