﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Common.Constants;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace LMS.LoanServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InsuranceController : Controller
    {
        private readonly IMediator _bus;
      
        public InsuranceController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("CalculateMoneyInsuranceLOS")]
        public async Task<ResponseActionResult> CalculateMoneyInsuranceLOS(Queries.Insurance.CalculateMoneyInsuranceLOSQuery req)
        {
            return await _bus.Send(req);
        }
    }
}