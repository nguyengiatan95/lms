﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SmsController : ControllerBase
    {
        private readonly IMediator _bus;
        public SmsController(IMediator bus)
        {
            this._bus = bus;
        }
        // POST api/values
        [HttpPost]
        [Route("IncreaseMoneyFromGPay")]
        public async Task<Common.Constants.ResponseActionResult> IncreaseMoneyFromGPay(Domain.Models.SMS.SmsIncreatedMoneyCustomerRequest request)
        {
            return await _bus.Send(new Commands.Sms.IncreaseMoneyFromGPayCommand { Model = request });
        }
        // POST api/values
        [HttpPost]
        [Route("IncreaseMoneyFromGuTina")]
        public async Task<Common.Constants.ResponseActionResult> IncreaseMoneyFromGuTina(Domain.Models.SMS.SmsIncreatedMoneyCustomerRequest request)
        {
            return await _bus.Send(new Commands.Sms.IncreaseMoneyFromGuTinaCommand { Model = request });
        }
        // POST api/values
        [HttpPost]
        [Route("IncreaseMoneyFromMomo")]
        public async Task<Common.Constants.ResponseActionResult> IncreaseMoneyFromMomo(Domain.Models.SMS.SmsIncreatedMoneyCustomerRequest request)
        {
            return await _bus.Send(new Commands.Sms.IncreaseMoneyFromMomoCommand { Model = request });
        }
        [HttpPost]
        [Route("CreateSMSTransactionBank")]
        public async Task<Common.Constants.ResponseActionResult> CreateSMSTransactionBank(Domain.Models.SMS.SmsTransactionBankRequest request)
        {
            return await _bus.Send(new Commands.Sms.CreateSMSTransactionBankCommand { Model = request });
        }
        [HttpPost]
        [Route("UpdateStatusSmsTransactionBank")]
        public async Task<Common.Constants.ResponseActionResult> UpdateStatusSmsTransactionBank(Domain.Models.SMS.UpdateStatusSmsTransactionBankRequest request)
        {
            return await _bus.Send(new Commands.Sms.UpdateStatusSMSTransactionBankCommand { Model = request });
        }
        [HttpPost]
        [Route("IncreaseMoneyFromGPayVA")]
        public async Task<Common.Constants.ResponseActionResult> IncreaseMoneyFromGPayVA(Domain.Models.SMS.SmsIncreatedMoneyCustomerRequest request)
        {
            return await _bus.Send(new Commands.Sms.IncreaseMoneyFromGPayVACommand { Model = request });
        }
        [HttpPost]
        [Route("CreateSMSTransactionBankNCB")]
        public async Task<Common.Constants.ResponseActionResult> CreateSMSTransactionBankNCB(Domain.Models.SMS.SmsTransactionBankRequest request)
        {
            return await _bus.Send(new Commands.Sms.CreateSMSTransactionBankCommand { Model = request });
        }
        [HttpPost]
        [Route("GetLstSmsInfoByCondition")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstSmsInfoByCondition(Domain.Models.SMS.RequestSmsItemViewModel request)
        {
            return await _bus.Send(new Queries.Sms.GetLstSmsInfoByConditionQuery
            {
                SmsItemViewRequest = request
            });
        }
        [HttpGet]
        [Route("ProcessSmsAnalytics")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ProcessSmsAnalytics()
        {
            return await _bus.Send(new Commands.Sms.ProcessSmsAnalyticsCommand());
        }
        [HttpGet]
        [Route("ProcessSmsOtpDisbursement")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ProcessSmsOtpDisbursement()
        {
            return await _bus.Send(new Commands.Sms.ProcessSmsOtpDisbursementCommand());
        }

        [HttpPost]
        [Route("CreateSMSOTPDisbursement")]
        public async Task<Common.Constants.ResponseActionResult> CreateSMSOTPDisbursement(Domain.Models.SMS.SmsTransactionBankRequest request)
        {
            return await _bus.Send(new Commands.Sms.CreateSMSOTPDisbursementCommand { Model = request });
        }
    }
}
