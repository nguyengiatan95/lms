﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LMS.LoanServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentIndemnifyInsurranceController : ControllerBase
    {
        private readonly IMediator _bus;
        public CommentIndemnifyInsurranceController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("SaveComment")]
        public async Task<LMS.Common.Constants.ResponseActionResult> SaveComment(Commands.LogCommentIndemnifyInsurrance.SaveCommentIndemnifyInsurranceCommand cmd)
        {
            return await _bus.Send(cmd);
        }
        [HttpGet]
        [Route("GetHistoryComment/{LoanID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetHistoryComment(long LoanID)
        {
            return await _bus.Send(new Queries.LogCommentIndemnifyInsurrance.GetHistoryCommentQuery()
            {
                LoanID = LoanID
            });
        }
    }
}