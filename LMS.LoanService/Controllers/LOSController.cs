﻿using AutoMapper;
using LMS.Entites.Dtos.LOSServices;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Steeltoe.Common.Discovery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LOSController : ControllerBase
    {
        private readonly IMediator _bus;
        RestClients.ITransactionService _transactionService;
        public LOSController(IMediator bus, RestClients.ITransactionService transactionService)
        {
            this._bus = bus;
            _transactionService = transactionService;
        }
        [HttpPost]
        [Route("DisbursementWaitingLOS")]
        public async Task<LMS.Common.Constants.ResponseActionResult> DisbursementWaitingLOS(Queries.LOS.DisbursementWaitingLOSQuery request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("SaveCommentHistory")]
        public async Task<LMS.Common.Constants.ResponseActionResult> SaveCommentHistory(ReqSaveCommentHistory request)
        {
            return await _bus.Send(new Commands.LOS.SaveCommentHistoryCommand
            {
                Request = request
            });
        }
        [HttpPost]
        [Route("DisbursementLoan")]
        public async Task<LMS.Common.Constants.ResponseActionResult> DisbursementLoan(Commands.LOS.DisbursementLoanCommand request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("PushLoanLender")]
        public async Task<LMS.Common.Constants.ResponseActionResult> PushLoanLender(Commands.LOS.PushLoanLenderCommand request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("LockLoan")]
        public async Task<LMS.Common.Constants.ResponseActionResult> LockLoan(Commands.LOS.LockLoanCommand request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("GetHistoryCommentLos")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetHistoryCommentLos(Queries.LOS.GetHistoryCommentByLoanBriefIDQuery request)
        {
            return await _bus.Send(request);
        }

        [HttpGet]
        [Route("GetLoanCreditDetailLos/{LoanBriefID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLoanCreditDetailLos(long LoanBriefID)
        {
            var result = await _bus.Send(new Queries.LOS.GetLoanCreditDetailLosQuery
            {
                LoanBriefID = LoanBriefID
            });
            return result;
        }
        [HttpGet]
        [Route("GetListImagesLos/{LoanBriefID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetListImagesLos(long LoanBriefID)
        {
            var result = await _bus.Send(new Queries.LOS.GetListImagesLosByLoanBriefIDQuery
            {
                LoanBriefID = LoanBriefID
            });
            return result;
        }

        [HttpPost]
        [Route("ReturnLoan")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ReturnLoan(Commands.LOS.ReturnLoanCommand request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("ChangeDisbursementByAccountant")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ChangeDisbursementByAccountant(Commands.LOS.ChangeDisbursementByAccountantCommand request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("ChangeStatusAccountantToAutoDisbursement")]// chuyển trạng thái kế toán Gn-> auto GN
        public async Task<LMS.Common.Constants.ResponseActionResult> ChangeStatusAccountantToAutoDisbursement(Commands.LOS.ChangeStatusAccountantToAutoDisbursementCommad request)
        {
            return await _bus.Send(request);
        }
    }
}