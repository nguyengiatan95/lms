﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Steeltoe.Common.Discovery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BankCardController : ControllerBase
    {
        private readonly IMediator _bus;
        public BankCardController(IMediator bus)
        {
            this._bus = bus;
        }

        [HttpPost]
        [Route("ListBankCard")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ListBankCard(Queries.BankCard.GetListBankCardQuery cmd)
        {
            return await _bus.Send(cmd);
        }

        [HttpGet]
        [Route("GetBankCardByID/{BankCardID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetBankCardByID(int BankCardID)
        {
            var result = await _bus.Send(new Queries.BankCard.GetBankCardByIDQuery
            {
                BankCardID = BankCardID
            });
            return result;
        }
        [HttpPost]
        [Route("CreateBankCard")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateBankCard(Commands.BankCard.CreateBankCardCommand cmd)
        {
            return await _bus.Send(cmd);
        }
        [HttpPost]
        [Route("UpdateBankCard")]
        public async Task<LMS.Common.Constants.ResponseActionResult> UpdateBankCard(Commands.BankCard.UpdateBankCardCommand cmd)
        {
            return await _bus.Send(cmd);
        }
        [HttpGet]
        [Route("ProcessSummaryTransactionDate")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ProcessSummaryTransactionDate()
        {
            return await _bus.Send(new Commands.BankCard.ProcessSummaryTransactionDateCommand());
        }
        [HttpGet]
        [Route("ProcessSummaryBankCardFromInvoiceID")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ProcessSummaryBankCardFromInvoiceID()
        {
            return await _bus.Send(new Commands.BankCard.ProcessSummaryBankCardFromInvoiceIDCommand());
        }
        [HttpPost]
        [Route("ProcessSummaryBankCardByBankCardID")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ProcessSummaryBankCardByBankCardID(Commands.BankCard.ProcessSummaryBankCardByBankCardIDCommand req)
        {
            return await _bus.Send(req);
        }

        [HttpPost]
        [Route("UpdateMoneyStartDateForBankCard")]
        public async Task<LMS.Common.Constants.ResponseActionResult> UpdateMoneyStartDateForBankCard(Commands.BankCard.UpdateMoneyStartDateForBankCardCommand req)
        {
            return await _bus.Send(req);
        }
    }
}