﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Common.Constants;
using MediatR;
using Microsoft.AspNetCore.Mvc;


namespace LMS.LoanServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContractLoanWithLenderController : Controller
    {
        private readonly IMediator _bus;
        public ContractLoanWithLenderController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpGet]
        [Route("ContractLoanWithLenderByLoanID/{LoanID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ContractLoanWithLenderByLoanID(long LoanID)
        {
            var result = await _bus.Send(new Queries.ContractLoanWithLender.ContractLoanWithLenderByLoanIDQuery
            {
                LoanID = LoanID
            });
            return result;
        }
    }
}