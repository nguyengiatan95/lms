﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Steeltoe.Common.Discovery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace LMS.LoanServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BankCardOfShopController : ControllerBase
    {
        private readonly IMediator _bus;
        public BankCardOfShopController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("CreateBankCardOfShop")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateBankCardOfShop(Commands.BankCardOfShop.CreateBankCardOfShopCommand cmd)
        {
            return await _bus.Send(cmd);
        }
        [HttpPost]
        [Route("DeleteBankCardOfShop")]
        public async Task<LMS.Common.Constants.ResponseActionResult> DeleteBankCardOfShop(Commands.BankCardOfShop.DeleteBankCardOfShopCommand cmd)
        {
            return await _bus.Send(cmd);
        }
        [HttpGet]
        [Route("GetBankCardOfShopByShopID/{ShopID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetBankCardOfShopByShopID(int ShopID)
        {
            var result = await _bus.Send(new Queries.BankCardOfShop.GetBankCardOfShopByShopIDQuery
            {
                ShopID = ShopID
            });
            return result;
        }
        [HttpGet]
        [Route("GetShopOfBankCardID/{BankCardID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetShopOfBankCardID(int BankCardID)
        {
            var result = await _bus.Send(new Queries.BankCardOfShop.GetShopOfBankCardIDQuery
            {
                BankCardID = BankCardID
            });
            return result;
        }
    }
}