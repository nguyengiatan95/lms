﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Steeltoe.Common.Discovery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace LMS.LoanServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlanCloseLoanController : ControllerBase
    {
        private readonly IMediator _bus;
        public PlanCloseLoanController(IMediator bus)
        {
            this._bus = bus;
        }

        [HttpGet]
        [Route("NotifyTotalMoneyCloseCustomer/{LoanID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> NotifyTotalMoneyCloseCustomer(long LoanID)
        {
            return await _bus.Send(new Queries.PlanCloseLoan.NotifyTotalMoneyCloseCustomerQuery { LoanID = LoanID });
        }
    }
}
