﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly IMediator _bus;
        public ReportController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("GetStasticLoanStatusByShop")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetStasticLoanStatusByShop(Queries.Loan.ReportStasticLoanStatusQuery cmd)
        {
            return await _bus.Send(cmd);
        }

        [HttpPost]
        [Route("ProcessSummaryStasticsLoanBorrow")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ProcessSummaryStasticsLoanBorrow(Commands.Report.ProcessSummaryStasticsLoanBorrowCommand cmd)
        {
            return await _bus.Send(cmd);
        }

        [HttpPost]
        [Route("ReportFineInterestLate")]// báo cáo Báo cáo Phí phạt muộn
        public async Task<LMS.Common.Constants.ResponseActionResult> ReportFineInterestLate(Queries.Report.ReportFineInterestLateQuery cmd)
        {
            return await _bus.Send(cmd);
        }

        [HttpPost]
        [Route("ReportVat")] // báo cáo vat
        public async Task<LMS.Common.Constants.ResponseActionResult> ReportVat(Queries.Report.ReportVatQuery cmd)
        {
            return await _bus.Send(cmd);
        }
        [HttpPost]
        [Route("ReportVatHub")] // báo cáo vat hub
        public async Task<LMS.Common.Constants.ResponseActionResult> ReportVatHub(Queries.Report.ReportVatHubQuery cmd)
        {
            return await _bus.Send(cmd);
        }
        [HttpPost]
        [Route("ReportVatLender")] // báo cáo vat của lender
        public async Task<LMS.Common.Constants.ResponseActionResult> ReportVatLender(Queries.Report.ReportVatLenderQuery cmd)
        {
            return await _bus.Send(cmd);
        }
        [HttpPost]
        [Route("ReportPenaltyFeeVatHub")] // báo cáo phi phạt vat hub
        public async Task<LMS.Common.Constants.ResponseActionResult> ReportPenaltyFeeVatHub(Queries.Report.ReportPenaltyFeeVatHubQuery cmd)
        {
            return await _bus.Send(cmd);
        }
        [HttpPost]
        [Route("ReportVatGCash")] // báo cáo vat Gcash
        public async Task<LMS.Common.Constants.ResponseActionResult> ReportVatGCash(Queries.Report.ReportVatGCashQuery cmd)
        {
            return await _bus.Send(cmd);
        }
        [HttpPost]
        [Route("ReportVatHUbWithGCash")] // báo cáo vat hub với Gcash
        public async Task<LMS.Common.Constants.ResponseActionResult> ReportVatHUbWithGCashQuery(Queries.Report.ReportVatHUbWithGCashQuery cmd)
        {
            return await _bus.Send(cmd);
        }

        [HttpPost]
        [Route("ReportExtraMoneyCustomer")] // Báo cáo tiền thừa của khách hàng
        public async Task<LMS.Common.Constants.ResponseActionResult> ReportExtraMoneyCustomer(Entites.Dtos.CustomerService.ReportExtraMoneyCustomerReq request)
        {
            return await _bus.Send(new Queries.Report.ReportExtraMoneyCustomerQuery()
            {
                ReportExtraMoneyCustomerInforReq = request
            });
        }

        [HttpPost]
        [Route("ReportVATForLender")] // Báo cáo VAT dành cho Lãi Vay của nhà đầu tư từ tháng 08/2021
        public async Task<LMS.Common.Constants.ResponseActionResult> ReportVATForLender(Queries.Report.ReportVATForLenderQuery request)
        {
            return await _bus.Send(request);
        }
    }
}
