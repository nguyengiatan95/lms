﻿using AutoMapper;
using LMS.LoanServiceApi.Services;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Steeltoe.Common.Discovery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoanController : ControllerBase
    {
        private readonly IMediator _bus;
        readonly RestClients.ITransactionService _transactionService;
        readonly IUserServices _userServices;
        public LoanController(IMediator bus, RestClients.ITransactionService transactionService, IUserServices userServices)
        {
            this._bus = bus;
            _transactionService = transactionService;
            _userServices = userServices;
        }

        [HttpPost]
        [Route("CreateLoan")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateLoan(Commands.CreateLoanCommand request)
        {
            return await _bus.Send(request);
        }

        [HttpGet]
        [Route("GetLoanByID/{loanID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLoanByID(int loanID)
        {
            return await _bus.Send(new Queries.GetLoanByIDQuery
            {
                LoanID = loanID
            });
        }

        [HttpGet]
        [Route("GetLstPaymentScheduleByLoanID/{loanID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstPaymentScheduleByLoanID(int loanID)
        {
            return await _bus.Send(new Queries.Loan.GetLstPaymentScheduleByLoanIDQuery
            {
                LoanID = loanID
            });
        }

        [HttpPost]
        [Route("UpdateLoanAfterPayment")]
        public async Task<LMS.Common.Constants.ResponseActionResult> UpdateLoanAfterPayment(Domain.Models.RequestUpdateLoanAfterPaymentModel request)
        {
            return await _bus.Send(new Commands.UpdateLoanAfterPaymentCommand
            {
                Model = request
            });
        }
        [HttpPost]
        [Route("GetLstLoanInfoByCondition")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstLoanInfoByCondition(Domain.Models.Loan.RequestLoanItemViewModel request)
        {
            return await _bus.Send(new Queries.GetLstLoanInfoByConditionQuery
            {
                LoanItemViewRequest = request
            });
        }

        //gia hạn
        [HttpPost]
        [Route("ExtendLoanTime")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ExtendLoanTime(Domain.Models.Loan.RequestExtendLoanTime request)
        {
            request.UserID = _userServices.GetHeaderUserID();
            return await _bus.Send(new Commands.Loan.ExtendLoanTimeCommand
            {
                Model = request
            });
        }

        [HttpPost]
        [Route("UpdateLinkInsurance")]
        public async Task<LMS.Common.Constants.ResponseActionResult> UpdateLinkInsurance(Commands.Loan.UpdateLinkInsuranceCommand req)
        {
            return await _bus.Send(req);
        }


        [HttpPost]
        [Route("AGPayFullyScheduleInPeriods")]
        public async Task<LMS.Common.Constants.ResponseActionResult> AGPayFullyScheduleInPeriods(Commands.Loan.AGPayFullyScheduleInPeriodsCommand request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("AGPayPartialScheduleInPeriods")]
        public async Task<LMS.Common.Constants.ResponseActionResult> AGPayPartialScheduleInPeriods(Commands.Loan.AGPayPartialScheduleInPeriodsCommand request)
        {

            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("AGCloseLoanFromSchedule")]
        public async Task<LMS.Common.Constants.ResponseActionResult> AGCloseLoanFromSchedule(Commands.Loan.AGCloseLoanFromScheduleCommand request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("DeleteLoan")]
        public async Task<LMS.Common.Constants.ResponseActionResult> DeleteLoan(Commands.Loan.DeleteLoanCommand request)
        {
            request.CreateBy = _userServices.GetHeaderUserID();
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("UpdateInforPaymentLoan")] // update thời gian vay , thời gian GN , Loại hình thanh toán ,Lãi xuất
        public async Task<LMS.Common.Constants.ResponseActionResult> UpdateInforPaymentLoan(Commands.Loan.UpdateInforPaymentLoanCommand request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("GenPaymentScheduleByLoanID")] //gen lại lịch khi đồng bộ AG qua bị lỗi
        public async Task<LMS.Common.Constants.ResponseActionResult> GenPaymentScheduleByLoanID(Commands.Loan.GenPaymentScheduleByLoanIDCommand request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("AGCuttingOffPaymentLoanInsuranceByLoanID")]
        public async Task<LMS.Common.Constants.ResponseActionResult> AGCuttingOffPaymentLoanInsuranceByLoanID(Commands.Loan.AGCuttingOffPaymentLoanInsuranceByLoanIDCommand cmd)
        {
            return await _bus.Send(cmd);
        }

        [HttpPost]
        [Route("AGForceCloseLoanByLoanID")]
        public async Task<LMS.Common.Constants.ResponseActionResult> AGForceCloseLoanByLoanID(Commands.Loan.AGForceCloseLoanByLoanIDCommand request)
        {
            return await _bus.Send(request);
        }


        [HttpPost]
        [Route("GetLstLoanByFilterCustomer")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstLoanByFilterCustomer(Queries.Loan.GetLstLoanByFilterCustomerQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("GetHistoryCommentLos")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetHistoryCommentLos(Queries.LOS.GetHistoryCommentLosQuery request)
        {
            return await _bus.Send(request);
        }

        [HttpGet]
        [Route("GetListImagesLos/{loanID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetListImagesLos(long loanID)
        {
            return await _bus.Send(new Queries.LOS.GetListImagesLosQuery
            {
                LoanID = loanID
            });
        }
        /// <summary>
        /// Lịch sử giao dịch
        /// </summary>
        /// <param name="LoanID"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTransactionByLoanID/{LoanID}")]
        public async Task<Common.Constants.ResponseActionResult> GetTransactionByLoanID(long LoanID)
        {
            Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            var lstTransaction = await _transactionService.GetByLoanID(LoanID);
            if (lstTransaction != null)
            {
                response.Data = lstTransaction;
                response.SetSucces();
            }
            return response;
        }

        [HttpPost]
        [Route("PaymentMoneyFullSchedule")]
        public async Task<LMS.Common.Constants.ResponseActionResult> PaymentMoneyFullSchedule(Commands.Loan.PaymentMoneyFullScheduleCommand request)
        {
            request.UserID = _userServices.GetHeaderUserID();
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("PayPartialScheduleInPeriods")]
        public async Task<LMS.Common.Constants.ResponseActionResult> PayPartialScheduleInPeriods(Commands.Loan.PayPartialScheduleInPeriodsCommand request)
        {
            request.CreateBy = _userServices.GetHeaderUserID();
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("SendStatusInsurance")]
        public async Task<LMS.Common.Constants.ResponseActionResult> SendStatusInsurance(Commands.Loan.SendStatusInsuranceCommand request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("GetMoneyNeedCloseLoanByLoanID")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetMoneyNeedCloseLoanByLoanID(Queries.Loan.GetMoneyNeedCloseLoanByLoanIDQuery request)
        {
            return await _bus.Send(request);
        }

        [HttpGet]
        [Route("ProcessMoneyFineLate")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ProcessMoneyFineLate()
        {
            return await _bus.Send(new Commands.Loan.ProcessMoneyFineLateCommand());
        }

        [HttpGet]
        [Route("ProcessAutoPayment")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ProcessAutoPayment()
        {
            return await _bus.Send(new Commands.Loan.ProcessAutoPaymentCommand());
        }

        /// <summary>
        /// Đóng HĐ
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ForceCloseLoanByLoanID")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ForceCloseLoanByLoanID(Commands.Loan.ForceCloseLoanByLoanIDCommand request)
        {
            request.CreateBy = _userServices.GetHeaderUserID();
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("ReportMoneyDetail")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ReportMoneyDetail(Queries.Report.ReportMoneyDetailLenderQuery request)
        {
            return await _bus.Send(request);
        }


        [HttpGet]
        [Route("GetLoanDebtByLoanID")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLoanDebtByLoanID(long loanID)
        {
            return await _bus.Send(new Queries.LoanDebt.GetLoanDebtByLoanIDQuery()
            {
                LoanID = loanID
            });
        }

        [HttpPost]
        [Route("CreateLoanDebt")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateLoanDebt(Commands.LoanDebt.CreateLoanDebtCommand req)
        {
            req.UserID = _userServices.GetHeaderUserID();
            return await _bus.Send(req);
        }

        [HttpPost]
        [Route("CreateLoanForDisbursementLoanCreditOfAppLender")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateLoanForDisbursementLoanCreditOfAppLender(Commands.Loan.CreateLoanForDisbursementLoanCreditOfAppLenderCommand request)
        {
            return await _bus.Send(request);
        }


        [HttpGet]
        [Route("ProcessAutoDisbursementLoanCredit")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ProcessAutoDisbursementLoanCredit()
        {
            return await _bus.Send(new Commands.Loan.ProcessAutoDisbursementLoanCreditCommand());
        }

        [HttpPost]
        [Route("CreateLoanByAccountant")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateLoanByAccountant(Commands.Loan.CreateLoanByAccountantCommand request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("GetOtpDisburmentForLoanCredit")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetOtpDisburmentForLoanCredit(Commands.Loan.GetOtpDisburmentForLoanCreditCommand request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("ReconciliationStatusGetOTP")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ReconciliationStatusGetOTP(Queries.Loan.ReconciliationInternalGetOTPQuery request)
        {
            return await _bus.Send(request);
        }

        /// <summary>
        /// TRẢ NỢ PHÍ PHẠT CHẬM
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("PayLoanDebtByLoanID")]
        public async Task<LMS.Common.Constants.ResponseActionResult> PayLoanDebtByLoanID(Entites.Dtos.LoanServices.PayLoanDebtByLoanIDModel request)
        {
            Commands.Loan.PayPartialScheduleInPeriodsCommand req = new Commands.Loan.PayPartialScheduleInPeriodsCommand()
            {
                CreateBy = _userServices.GetHeaderUserID(),
                LoanID = request.LoanID,
                MoneyFineLate = request.TotalMoney,
            };
            return await _bus.Send(req);
        }

        /// <summary>
        /// Hủy ghi nợ phạt chậm
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteLoanDebt")]
        public async Task<LMS.Common.Constants.ResponseActionResult> DeleteLoanDebt(Commands.LoanDebt.DeleteLoanDebtCommand req)
        {
            return await _bus.Send(req);
        }

        [HttpGet]
        [Route("GetLoanCreditDetailLosByLoanID/{LoanID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLoanCreditDetailLos(long LoanID)
        {
            var result = await _bus.Send(new Queries.Loan.GetLoanCreditDetailLosByLoanIDQuery
            {
                LoanID = LoanID
            });
            return result;
        }


        [HttpPost]
        [Route("DebtRestructuring")]
        public async Task<LMS.Common.Constants.ResponseActionResult> DebtRestructuring(Commands.Loan.AGDebtRestructuringCommand request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("GenDebtRestructuring")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GenDebtRestructuring(Queries.Loan.GenDebtRestructuringQuery request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("AgCancelMoneyFineLate")]
        public async Task<LMS.Common.Constants.ResponseActionResult> AgCancelMoneyFineLate(Commands.LoanDebt.AgCancelMoneyFineLateCommand request)
        {
            return await _bus.Send(request);
        }


        [HttpGet]
        [Route("ProcessCutOffLoanDaily")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ProcessCutOffLoanDaily()
        {
            return await _bus.Send(new Commands.CutOffLoan.ProcessCutOffLoanByDateDailyCommand { });
        }
        [HttpGet]
        [Route("ProcessCalculatorMoneyCloseLoanDaily")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ProcessCalculatorMoneyCloseLoanDaily()
        {
            return await _bus.Send(new Commands.Loan.ProcessCalculatorMoneyCloseLoanDailyCommand { });
        }
        [HttpGet]
        [Route("ProcessSplitTransactionLoan")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ProcessSplitTransactionLoan()
        {
            return await _bus.Send(new Commands.SplitTransactionLoan.ProcessSplitTransactionLoanCommand { });
        }

        [HttpPost]
        [Route("CreateRequestCloseLoan")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateRequestCloseLoan(Commands.RequestCloseLoan.CreateRequestCloseLoanCommand request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("UpdateRequestCloseLoan")]
        public async Task<LMS.Common.Constants.ResponseActionResult> UpdateRequestCloseLoan(Commands.RequestCloseLoan.UpdateRequestCloseLoanCommand request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("GetLstRequestCloseLoan")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstRequestCloseLoan(Queries.RequestCloseLoan.GetLstRequestCloseLoanQuery request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("TranferLoan")]
        public async Task<LMS.Common.Constants.ResponseActionResult> TranferLoan(Commands.Loan.TranferLoanCommand request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("ReCalculateMoneyLenderByLoan")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ReCalculateMoneyLenderByLoan(Commands.Loan.ReCalculateMoneyLenderByLoanCommand request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("TranferListLoan")]
        public async Task<LMS.Common.Constants.ResponseActionResult> TranferListLoan(Commands.Loan.TransferListLoanCommand request)
        {
            return await _bus.Send(request);
        }
    }
}
