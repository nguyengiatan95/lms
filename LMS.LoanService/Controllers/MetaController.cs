﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Common.Constants;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace LMS.LoanServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MetaController : Controller
    {
        private readonly IMediator _bus;
        public MetaController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpGet]
        [Route("GetBankCard")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetBankCard(int Status = (int)StatusCommon.Default, long BankCardID = (int)StatusCommon.Default, int TypePurpose = (int)StatusCommon.Default)
        {
            var result = await _bus.Send(new Queries.BankCard.GetBankCardQuery
            {
                Status = Status,
                BankCardID = BankCardID,
                TypePurpose = TypePurpose
            });
            return result;
        }
        [HttpGet]
        [Route("GetListProductCredit")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetListProductCredit(long ProductCreditID = 0)
        {
            var result = await _bus.Send(new Queries.ProductCredit.GetListProductCreditCommand
            {
                ProductCreditID = ProductCreditID
            });
            return result;
        }
        [HttpGet]
        [Route("GetBank/{BankID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetBank(int BankID = 0)
        {
            var result = await _bus.Send(new Queries.Bank.GetBankQuery
            {
                BankID = BankID
            });
            return result;
        }

        [HttpGet]
        [Route("GetUserGroup/{GroupID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetUserGroup(int GroupID = 0)
        {
            var result = await _bus.Send(new Queries.User.GetUserByGroupQuery
            {
                GroupID = GroupID
            });
            return result;
        }

        [HttpGet]
        [Route("GetAff")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetAff()
        {
            var result = await _bus.Send(new Queries.Aff.GetAffQuery());
            return result;
        }

        [HttpGet]
        [Route("GetLender")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLender(string GeneralSearch)
        {
            var result = await _bus.Send(new Queries.Lender.GetLenderQuery
            {
                GeneralSearch = GeneralSearch
            });
            return result;
        }

        [HttpGet]
        [Route("GetGcash/{GCashID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetGcash(long GcashID)
        {
            var result = await _bus.Send(new Queries.Lender.GetGcashIDQuery
            {
                GcashID = GcashID
            });
            return result;
        }
    }
}