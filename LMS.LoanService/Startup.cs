using Confluent.Kafka;
using FluentValidation;
using FluentValidation.AspNetCore;
using LMS.Common.Helper;
using LMS.Kafka.Consumer;
using LMS.Kafka.Interfaces;
using LMS.Kafka.Producer;
using LMS.LoanServiceApi;
using LMS.LoanServiceApi.Commands.BankCard;
using LMS.LoanServiceApi.Commands.Loan;
using LMS.LoanServiceApi.Domain.Models;
using LMS.LoanServiceApi.KafkaEventHandlers;
using LMS.LoanServiceApi.RestClients;
using LMS.LoanServiceApi.Validators;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Steeltoe.Discovery.Client;
using Steeltoe.Discovery.Eureka;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace LMS.LoanService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<KestrelServerOptions>(Configuration.GetSection("Kestrel"));
            services.AddDiscoveryClient(Configuration);
            services.AddServiceDiscovery(options => options.UseEureka());
            services.AddSingleton(Configuration);
            services.AddRedisCacheService(Configuration);
            services.AddControllers().AddJsonOptions(opts => opts.JsonSerializerOptions.PropertyNamingPolicy = null);
            services.AddHttpContextAccessor();
            LMS.Common.Configuration.ConnectStringSetting.DefaultConnectString = Configuration["ConnectStringSetting:DefaultConnectString"];
            services.AddTransient(typeof(Common.DAL.ITableHelper<>), typeof(Common.DAL.TableHelper<>));
            services.AddMvc().AddFluentValidation();
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddRestClientsService();
            services.AddTransient<IValidator<CreateLoanCommandModel>, CreateLoanCommandModelValidator>();
            services.AddTransient<IValidator<LoanServiceApi.Domain.Models.Loan.RequestExtendLoanTime>, ExtendLoanTimeValidator>();
            services.AddTransient<IValidator<UpdateLinkInsuranceCommand>, UpdateLinkInsuranceCommandValidator>();
            services.AddTransient<IValidator<UpdateInforPaymentLoanCommand>, UpdateInforPaymentLoanCommandValidator>();
            services.AddTransient<IValidator<CreateBankCardCommand>, CreateBankCardCommandValidator>();
            services.AddTransient<IValidator<UpdateBankCardCommand>, UpdateBankCardCommandValidator>();
            services.AddTransient<IValidator<ProcessSummaryBankCardByBankCardIDCommand>, ProcessSummaryBankCardByBankCardIDCommandValidator>();
            services.AddTransient<IValidator<UpdateMoneyStartDateForBankCardCommand>, UpdateMoneyStartDateForBankCardCommandValidator>();

            services.AddTransient<LMS.Common.Helper.IApiHelper, LMS.Common.Helper.ApiHelper>();
            services.AddTransient<LMS.Common.Helper.ICalculatorInterestManager, LMS.Common.Helper.CalculatorInterestManager>();
            AddServiceManager(services);

            AddServiceKafka(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors(options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseAuthorization();
            app.UseDiscoveryClient();
            //Add our new middleware to the pipeline
            app.UseMiddleware<LMS.Common.Helper.RequestResponseLoggingMiddleware>();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
        public void AddServiceManager(IServiceCollection services)
        {
            services.AddTransient<LoanServiceApi.Services.ISmsAnalyticsManager, LoanServiceApi.Services.SmsAnalyticsManager>();
            services.AddTransient<LoanServiceApi.Services.ILoanManager, LoanServiceApi.Services.LoanManager>();
            services.AddTransient<LoanServiceApi.Services.IBankCardManager, LoanServiceApi.Services.BankCardManager>();
            services.AddTransient<LoanServiceApi.Services.IReportManager, LoanServiceApi.Services.ReportManager>();
            services.AddTransient<LoanServiceApi.Services.ICloseLoanManager, LoanServiceApi.Services.TypeCloseLoanFinalization>();
            services.AddTransient<LoanServiceApi.Services.ICloseLoanManager, LoanServiceApi.Services.TypeCloseLoanLiquidation>();
            services.AddTransient<LoanServiceApi.Services.ICloseLoanManager, LoanServiceApi.Services.TypeCloseLoanLiquidationLoanInsurance>();
            services.AddTransient<LoanServiceApi.Services.ICloseLoanManager, LoanServiceApi.Services.TypeCloseLoanIndemnifyLoanInsurance>();
            services.AddTransient<LoanServiceApi.Services.ICloseLoanManager, LoanServiceApi.Services.TypeCloseLoanPaidInsurance>();
            services.AddTransient<LoanServiceApi.Services.ICloseLoanManager, LoanServiceApi.Services.TypeCloseLoanPaidInsuranceExemption>();
            services.AddTransient<LoanServiceApi.Services.ICloseLoanManager, LoanServiceApi.Services.TypeCloseLoanExemption>();
            services.AddTransient<LoanServiceApi.Services.ILOSManager, LoanServiceApi.Services.LOSManager>();
            services.AddTransient<LoanServiceApi.Services.ILoanEvenManager, LoanServiceApi.Services.LoanEvenManager>();

            #region sms analytic
            services.AddTransient<LoanServiceApi.Services.IReadSmsAnalyticProvider, LoanServiceApi.Services.AcbBankProvider>();
            services.AddTransient<LoanServiceApi.Services.IReadSmsAnalyticProvider, LoanServiceApi.Services.AgriBankProvider>();
            services.AddTransient<LoanServiceApi.Services.IReadSmsAnalyticProvider, LoanServiceApi.Services.BidvBankProvider>();
            services.AddTransient<LoanServiceApi.Services.IReadSmsAnalyticProvider, LoanServiceApi.Services.VibBankProvider>();
            services.AddTransient<LoanServiceApi.Services.IReadSmsAnalyticProvider, LoanServiceApi.Services.VietcomBankProvider>();
            services.AddTransient<LoanServiceApi.Services.IReadSmsAnalyticProvider, LoanServiceApi.Services.VpBankProvider>();
            services.AddTransient<LoanServiceApi.Services.IReadSmsAnalyticProvider, LoanServiceApi.Services.MomoSmsProvider>();
            services.AddTransient<LoanServiceApi.Services.IReadSmsAnalyticProvider, LoanServiceApi.Services.GPAYSmsProvider>();
            services.AddTransient<LoanServiceApi.Services.IReadSmsAnalyticProvider, LoanServiceApi.Services.GPayVASmsProvider>();
            services.AddTransient<LoanServiceApi.Services.IReadSmsAnalyticProvider, LoanServiceApi.Services.GUTINASmsProvider>();
            services.AddTransient<LoanServiceApi.Services.IReadSmsAnalyticProvider, LoanServiceApi.Services.VaSmsProvider>();
            #endregion

            services.AddTransient<LoanServiceApi.Services.IPaymentGatewayService, LoanServiceApi.Services.PaymentGatewayService>();
        }

        public void AddServiceKafka(IServiceCollection services)
        {
            var clientConfig = new ClientConfig()
            {
                BootstrapServers = Configuration["Kafka:ClientConfigs:BootstrapServers"]
            };

            var producerConfig = new ProducerConfig(clientConfig);
            var consumerConfig = new ConsumerConfig(clientConfig)
            {
                GroupId = Configuration["spring:application:name"],
                EnableAutoCommit = false,
                AutoOffsetReset = AutoOffsetReset.Earliest,
                StatisticsIntervalMs = 5000,
                SessionTimeoutMs = 6000
            };

            services.AddSingleton(producerConfig);
            services.AddSingleton(consumerConfig);

            services.AddSingleton(typeof(IKafkaProducer<,>), typeof(KafkaProducer<,>));
            services.AddSingleton(typeof(IKafkaConsumer<,>), typeof(KafkaConsumer<,>));

            services.AddHostedService<KafkaServiceConsumer>();

            services.AddScoped<IKafkaHandler<string, Kafka.Messages.Customer.CustomerInsertSuccess>, UpdateCustomerForLoanHandler>();
            services.AddScoped<IKafkaHandler<string, Kafka.Messages.Payment.UpdateNextDateLoanInfo>, UpdateLoanInfoAfterChangePaymentHandler>();
            services.AddScoped<IKafkaHandler<string, Kafka.Messages.Loan.LoanPaymentSuccess>, UpdateLoanInfoAfterTransactionSuccessHandler>();
        }
    }
}
