﻿using LMS.Kafka.Interfaces;
using LMS.Kafka.Messages.Customer;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.KafkaEventHandlers
{
    public class UpdateCustomerForLoanHandler : IKafkaHandler<string, LMS.Kafka.Messages.Customer.CustomerInsertSuccess>
    {
        //private readonly IKafkaProducer<string, LMS.Kafka.Messages.CustomerDisbursement.CustomerInsertSuccess> _customerProducer;
        private readonly IMediator _bus;
        LMS.Common.Helper.Utils _common = new Common.Helper.Utils();
        public UpdateCustomerForLoanHandler(IMediator bus)
        {
            _bus = bus;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> HandleAsync(string key, CustomerInsertSuccess value)
        {
            Commands.Loan.UpdateCustomerForLoanCommand request = new Commands.Loan.UpdateCustomerForLoanCommand
            {
                LoanID = value.LoanID,
                CustomerID = value.CustomerID
            };
           return  await _bus.Send(request);
        }
    }
}
