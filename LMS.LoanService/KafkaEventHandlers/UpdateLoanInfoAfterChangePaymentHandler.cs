﻿using LMS.Kafka.Interfaces;
using LMS.Kafka.Messages.Payment;
using LMS.LoanServiceApi.Services;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.KafkaEventHandlers
{
    public class UpdateLoanInfoAfterChangePaymentHandler : IKafkaHandler<string, LMS.Kafka.Messages.Payment.UpdateNextDateLoanInfo>
    {
        private readonly IMediator _bus;
        LMS.Common.Helper.Utils _common = new Common.Helper.Utils();
        ILoanEvenManager _loanEventManager;
        public UpdateLoanInfoAfterChangePaymentHandler(IMediator bus, ILoanEvenManager loanEventManager)
        {
            _bus = bus;
            _loanEventManager = loanEventManager;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> HandleAsync(string key, UpdateNextDateLoanInfo value)
        {
            return await _loanEventManager.UpdateLoanAfterPayment(value.LoanID, 0);
        }
    }
}
