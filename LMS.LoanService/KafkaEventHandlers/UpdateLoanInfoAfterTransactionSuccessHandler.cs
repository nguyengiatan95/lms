﻿using LMS.Kafka.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.KafkaEventHandlers
{
    public class UpdateLoanInfoAfterTransactionSuccessHandler : IKafkaHandler<string, LMS.Kafka.Messages.Loan.LoanPaymentSuccess>
    {
        private readonly IMediator _bus;
        LMS.Common.Helper.Utils _common = new Common.Helper.Utils();
        public UpdateLoanInfoAfterTransactionSuccessHandler(IMediator bus)
        {
            _bus = bus;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> HandleAsync(string key, LMS.Kafka.Messages.Loan.LoanPaymentSuccess value)
        {
            var request = new Commands.Loan.CaculateMoneyForLoanAffterCutPeriodCommand
            {
                LoanID = value.LoanID
            };
            var t1 = _bus.Send(request);
            var calculateMoneyLender = new Commands.Loan.ReCalculateMoneyLenderByLoanCommand
            {
                LoanID = value.LoanID,
                StartTransactionID = value.StartTransactionID,
                EndTransactionID = value.EndTransactionID
            };
            var t2 = _bus.Send(calculateMoneyLender);
            await Task.WhenAll(t1, t2);
            return t1.Result;
        }
    }
}
