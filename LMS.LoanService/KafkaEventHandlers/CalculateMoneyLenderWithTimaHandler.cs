﻿using LMS.Kafka.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.KafkaEventHandlers
{
    public class CalculateMoneyLenderWithTimaHandler : IKafkaHandler<string, LMS.Kafka.Messages.Loan.LoanPaymentSuccess>
    {
        private readonly IMediator _bus;
        LMS.Common.Helper.Utils _common = new Common.Helper.Utils();
        public CalculateMoneyLenderWithTimaHandler(IMediator bus)
        {
            _bus = bus;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> HandleAsync(string key, LMS.Kafka.Messages.Loan.LoanPaymentSuccess value)
        {
            var request = new Commands.Loan.ReCalculateMoneyLenderByLoanCommand
            {
                LoanID = value.LoanID,
                StartTransactionID = value.StartTransactionID,
                EndTransactionID = value.EndTransactionID
            };
            return await _bus.Send(request);
        }
    }
}
