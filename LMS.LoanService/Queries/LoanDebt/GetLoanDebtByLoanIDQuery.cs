﻿using LMS.Common.Constants;
using LMS.Common.DAL;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.LoanDebt
{
    public class GetLoanDebtByLoanIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
    }
    public class GetLoanDebtByLoanIDQueryHandler : IRequestHandler<GetLoanDebtByLoanIDQuery, ResponseActionResult>
    {
        ILogger<GetLoanDebtByLoanIDQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        ITableHelper<Domain.Tables.TblDebt> _loanDebtTab;
        ITableHelper<Domain.Tables.TblUser> _userTab;
        public GetLoanDebtByLoanIDQueryHandler(ITableHelper<Domain.Tables.TblDebt> loanDebtTab,
            ITableHelper<Domain.Tables.TblUser> userTab,
            ILogger<GetLoanDebtByLoanIDQueryHandler> logger)
        {
            _logger = logger;
            _userTab = userTab;
            _loanDebtTab = loanDebtTab;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetLoanDebtByLoanIDQuery request, CancellationToken cancellationToken)
        {
            List<Entites.Dtos.LoanServices.LoanDebtModel> lstResult = new List<Entites.Dtos.LoanServices.LoanDebtModel>();
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstType = new List<int>()
                    {
                        (int)Debt_TypeID.CustomerFineLate,
                        (int)Debt_TypeID.CustomerPayFineLate,
                        (int)Debt_TypeID.CancelCustomerFineLate,
                        (int)Debt_TypeID.CancelCustomerPayFineLate
                    };
                var lstLoanDebt = await _loanDebtTab.WhereClause(x => x.ReferID == request.LoanID && lstType.Contains(x.TypeID)).QueryAsync();
                if (lstLoanDebt != null && lstLoanDebt.Any())
                {
                    var lstParentDebtID = new List<long>();
                    var lstUserID = new List<long>();
                    foreach (var item in lstLoanDebt)
                    {
                        if (item.ParentDebtID != 0)
                            lstParentDebtID.Add(item.ParentDebtID);

                        lstUserID.Add(item.CreateBy);
                    }
                    var dicUser = _userTab.WhereClause(x => lstUserID.Contains(x.UserID)).Query().ToDictionary(x => x.UserID, x => x.UserName);
                    foreach (var item in lstLoanDebt)
                    {
                        var data = _common.ConvertParentToChild<Entites.Dtos.LoanServices.LoanDebtModel, Domain.Tables.TblDebt>(item);
                        data.Username = dicUser.GetValueOrDefault(item.CreateBy);
                        data.ShowButtonDelete = true;
                        if (lstParentDebtID.Any(x => x == item.DebtID) || item.ParentDebtID != 0)
                            data.ShowButtonDelete = false;

                        lstResult.Add(data);
                    }
                }
                response.SetSucces();
                response.Data = lstResult.OrderByDescending(x => x.CreateDate);
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLoanDebtByLoanIDQuery_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;

        }
    }
}
