﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.User
{
    public class GetUserByGroupQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int GroupID { get; set; }
    }
    public class GetUserByGroupQueryHandler : IRequestHandler<GetUserByGroupQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserGroup> _userGroup;
        ILogger<GetUserByGroupQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetUserByGroupQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserGroup> userGroup,
            ILogger<GetUserByGroupQueryHandler> logger)
        {
            _userTab = userTab;
            _userGroup = userGroup;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetUserByGroupQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                int activeStaus = (int)StatusCommon.Active;
                var result = new List<Domain.SelectItem>();
                return await Task.Run(() =>
                {
                    var lstUserGroup = _userGroup.WhereClause(x => x.GroupID == request.GroupID).Query();
                    var lstUserIds = lstUserGroup.Select(x => x.UserID).ToList();
                    if (lstUserIds != null && lstUserIds.Any())
                    {
                        var lstUser = _userTab.SelectColumns(x => x.UserID, x => x.FullName).WhereClause(x => x.Status == activeStaus && lstUserIds.Contains(x.UserID)).Query();
                        foreach (var item in lstUser)
                        {
                            result.Add(new Domain.SelectItem
                            {
                                Text = item.FullName,
                                Value = item.UserID
                            });
                        }
                        response.SetSucces();
                        response.Data = result;
                    }
                    return response;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetUserByGroupQuery_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
