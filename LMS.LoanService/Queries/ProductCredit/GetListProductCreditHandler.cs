﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Models.Loan;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.ProductCredit
{
    public class GetListProductCreditCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long ProductCreditID { get; set; }
    }
    public class GetListProductCreditCommandHandler : IRequestHandler<GetListProductCreditCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblProductCredit> _productCreditTab;
        ILogger<GetListProductCreditCommandHandler> _logger;
        public GetListProductCreditCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblProductCredit> productCreditTab, ILogger<GetListProductCreditCommandHandler> logger)
        {
            _productCreditTab = productCreditTab;
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(GetListProductCreditCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                return await Task.Run(() =>
                {
                    if (request.ProductCreditID != 0)
                        _productCreditTab.WhereClause(x => x.ProductCreditID == request.ProductCreditID);
                    var lstProductCredit = _productCreditTab.Query();
                    if (lstProductCredit != null)
                    {
                        var result = new List<Domain.SelectItem>();
                        foreach (var item in lstProductCredit)
                        {
                            result.Add(new Domain.SelectItem
                            {
                                Text = item.ProductCreditName,
                                Value = item.ProductCreditID
                            });
                        }
                        response.SetSucces();
                        response.Data = result;
                    }
                    return response;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"{System.Reflection.MethodBase.GetCurrentMethod().Name}_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
