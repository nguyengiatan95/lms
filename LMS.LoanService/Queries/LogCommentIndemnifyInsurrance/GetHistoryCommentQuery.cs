﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.LogCommentIndemnifyInsurrance
{
    public class GetHistoryCommentQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
    }
    public class GetHistoryCommentQueryHandler : IRequestHandler<GetHistoryCommentQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCommentIndemnifyInsurrance> _logCommentTab;
        ILogger<GetHistoryCommentQueryHandler> _logger;
        public GetHistoryCommentQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCommentIndemnifyInsurrance> logCommentTab,
            Services.ILoanManager loanManager,
            ILogger<GetHistoryCommentQueryHandler> logger)
        {
            _logCommentTab = logCommentTab;
            _logger = logger;
        }

        public async Task<ResponseActionResult> Handle(GetHistoryCommentQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                var lstComment = _logCommentTab.WhereClause(x => x.LoanID == request.LoanID).Query();
                if (lstComment != null)
                {
                    response.SetSucces();
                    response.Data = lstComment;
                }
                return response;
            });
        }
    }
}
