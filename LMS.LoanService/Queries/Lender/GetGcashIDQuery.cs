﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.LOSServices;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace LMS.LoanServiceApi.Queries.Lender
{
    public class GetGcashIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long GcashID { get; set; }
    }
    public class GetGcashIDQueryHandler : IRequestHandler<GetGcashIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        ILogger<GetGcashIDQueryHandler> _logger;
        Common.Helper.Utils _common;
        public GetGcashIDQueryHandler(
            ILogger<GetGcashIDQueryHandler> logger,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab)
        {
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetGcashIDQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    if (request.GcashID == (int)StatusCommon.Default)
                        _lenderTab.WhereClause(x => x.IsGcash == (int)Lender_IsGcash.Gcash);
                    else
                        _lenderTab.WhereClause(x => x.LenderID == request.GcashID);

                    var lstLender = _lenderTab.Query();
                    var result = new List<Domain.SelectItem>();
                    if (lstLender != null)
                    {
                        foreach (var item in lstLender)
                        {
                            result.Add(new Domain.SelectItem
                            {
                                Text = item.FullName,
                                Value = item.LenderID
                            });
                        }
                    }
                    response.SetSucces();
                    response.Data = result;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"GetGcashIDQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                }
                return response;
            });
        }
    }
}
