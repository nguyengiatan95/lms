﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.LOSServices;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.Lender
{
    public class GetLenderQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string GeneralSearch { get; set; }
    }
    public class GetLenderQueryHandler : IRequestHandler<GetLenderQuery, LMS.Common.Constants.ResponseActionResult>
    {
        RestClients.ILOSService _lOSService;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        ILogger<GetLenderQueryHandler> _logger;
        Common.Helper.Utils _common;
        public GetLenderQueryHandler(
             RestClients.ILOSService lOSService,
            ILogger<GetLenderQueryHandler> logger,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab)
        {
            _lOSService = lOSService;
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetLenderQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstMoneyWaitingLenderDisbursement = await _lOSService.GetWaitingMoneyDisbursementLenderLOS();

                if (!string.IsNullOrWhiteSpace(request.GeneralSearch))
                {
                    _lenderTab.WhereClause(x => x.IsHub < (int)Lender_IsHub.Hub &&
                                           x.IsVerified == (int)Common.Constants.Lender_IsVerified.Active &&
                                           x.Status == (int)Lender_Status.Active &&
                                           x.FullName.Contains(request.GeneralSearch));
                }
                var lstLender = _lenderTab.SetGetTop(50).OrderByDescending(x => x.TotalMoney).Query();
                var result = new List<Domain.SelectItem>();
                if (lstLender != null)
                {
                    foreach (var item in lstLender)
                    {
                        var selectItem = new Domain.SelectItem
                        {
                            Value = item.LenderID,
                            Text = item.FullName
                        };
                        if (item.RegFromApp != (int)Lender_RegFromApp.NA)
                        {
                            if (lstMoneyWaitingLenderDisbursement != null && lstMoneyWaitingLenderDisbursement.Count > 0)
                            {
                                var moneyWaitingDetail = lstMoneyWaitingLenderDisbursement.FirstOrDefault(x => x.LenderId == item.LenderID);
                                if (moneyWaitingDetail != null && moneyWaitingDetail.TotalMoney > 0)
                                    item.MoneyAvailiable -= (long)moneyWaitingDetail.TotalMoney;
                            }
                            selectItem.Text = item.FullName + "-" + item.MoneyAvailiable.ToString("#,##0");
                        }
                        result.Add(selectItem);
                    }
                }
                response.SetSucces();
                response.Data = result;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLenderQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
