﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Models.ContractLoanWithLender;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.ContractLoanWithLender
{
    public class ContractLoanWithLenderByLoanIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
    }
    public class ContractLoanWithLenderByLoanIDQueryHandler : IRequestHandler<ContractLoanWithLenderByLoanIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblContractLoanWithLender> _contractLoanWithLenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblAccountingPriorityOrder> _accountingPriorityOrderTab;
        ILogger<ContractLoanWithLenderByLoanIDQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public ContractLoanWithLenderByLoanIDQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblContractLoanWithLender> contractLoanWithLenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblAccountingPriorityOrder> accountingPriorityOrderTab,
            ILogger<ContractLoanWithLenderByLoanIDQueryHandler> logger)
        {
            _contractLoanWithLenderTab = contractLoanWithLenderTab;
            _loanTab = loanTab;
            _lenderTab = lenderTab;
            _accountingPriorityOrderTab = accountingPriorityOrderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(ContractLoanWithLenderByLoanIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var lstResult = new List<ContractLoanWithLenderByLoanID>();
            try
            {
                var lstContractLoanWithLender = await _contractLoanWithLenderTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync();
                if (lstContractLoanWithLender.Any())
                {
                    var lstLenderID = new List<long>();
                    var lstAccountingPriorityOrderID = new List<long>();
                    foreach (var item in lstContractLoanWithLender)
                    {
                        lstLenderID.Add(item.LenderID);
                        lstAccountingPriorityOrderID.Add(item.AccountingPriorityOrderID);
                    }
                    var lenderTask = _lenderTab.WhereClause(x => lstLenderID.Contains(x.LenderID)).SelectColumns(x => x.LenderID, x => x.FullName).QueryAsync();
                    var AccountingPriorityOrderTask = _accountingPriorityOrderTab.WhereClause(x => lstAccountingPriorityOrderID.Contains(x.AccountingPriorityID)).QueryAsync();
                    var loanTask = _loanTab.WhereClause(x => x.LoanID == request.LoanID).SelectColumns(x => x.LoanID, x => x.ContactCode).QueryAsync();

                    await Task.WhenAll(lenderTask, AccountingPriorityOrderTask, loanTask);

                    var dictLender = lenderTask.Result.ToDictionary(x => x.LenderID, x => x);
                    var dictAccountingPriority = AccountingPriorityOrderTask.Result.ToDictionary(x => x.AccountingPriorityID, x => x);
                    var objLoan = loanTask.Result.First();

                    foreach (var item in lstContractLoanWithLender)
                    {
                        var objLender = dictLender.GetValueOrDefault(item.LenderID);
                        var objAccountingPriority = dictAccountingPriority.GetValueOrDefault(item.AccountingPriorityOrderID);
                        lstResult.Add(new ContractLoanWithLenderByLoanID
                        {
                            LoanID = objLoan.LoanID,
                            ContactCode = objLoan.ContactCode,
                            LenderCode = objLender.FullName,
                            FromDate = item.FromDate,
                            CreateDate =item.CreateDate,
                            ToDate = item.ToDate,
                            AccountingMethod = objAccountingPriority?.Name
                        });
                    }
                    response.Data = lstResult;
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ContractLoanWithLenderByLoanIDQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
