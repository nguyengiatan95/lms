﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.LoanServiceApi.RestClients;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.LOS
{
    public class DisbursementWaitingLOSQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
        public string Search { get; set; }
    }
    public class DisbursementWaitingLOSQueryHandler : IRequestHandler<DisbursementWaitingLOSQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanCreditDisbursementAuto> _loanCreditDisbursementAutoTab;
        ILOSService _lOSService;
        ILogger<DisbursementWaitingLOSQueryHandler> _logger;
        public DisbursementWaitingLOSQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanCreditDisbursementAuto> loanCreditDisbursementAutoTab,
            ILOSService lOSService,
            ILogger<DisbursementWaitingLOSQueryHandler> logger)
        {
            _loanCreditDisbursementAutoTab = loanCreditDisbursementAutoTab;
            _lOSService = lOSService;
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(DisbursementWaitingLOSQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                return await Task.Run(() =>
                {
                    var dataLos = _lOSService.DisbursementWaitingLOS(request.PageIndex, request.PageSize, request.Type, request.Search).Result;
                    var lstLoanBriefId = new List<long>();
                    if (dataLos != null && dataLos.Count > 0)
                    {
                        var lstStatusLOS = Enum.GetValues(typeof(DisbursementLOS_Status)).Cast<DisbursementLOS_Status>().Select(d => (long)d).ToList();
                        foreach (var item in dataLos)
                        {
                            item.StrStatus = string.Empty;
                            if (lstStatusLOS.Any(x => x == item.Status))
                                item.StrStatus = ((DisbursementLOS_Status)item.Status).GetDescription();
                            item.StatusLMS = ConverStatusLMS(item.Status);

                            if (item.Status == (int)DisbursementLOS_Status.AutoDisbursement)
                            {
                                lstLoanBriefId.Add(item.LoanBriefId);
                            }
                            response.Total = item.TotalCount;
                        }
                        // check trạng thái TblLoanCreditDisbursementAuto
                        if (lstLoanBriefId.Count() > 0)
                        {
                            var dictLoanCreditDisbursementAutoInfos = _loanCreditDisbursementAutoTab.WhereClause(x => lstLoanBriefId.Contains(x.LoanCreditID)).Query()
                                   .GroupBy(x => x.LoanCreditID).Select(x => x.OrderByDescending(x => x.LoanCreditDisbursementAutoID).FirstOrDefault()).ToDictionary(x => x.LoanCreditID, x => x);

                            foreach (var item in dataLos)
                            {
                                var objLoanCreditDisbursementAuto = dictLoanCreditDisbursementAutoInfos.GetValueOrDefault(item.LoanBriefId);
                                item.StatusDisbursementAuto = objLoanCreditDisbursementAuto == null ? (short)0 : (short)objLoanCreditDisbursementAuto.Status;
                                item.StrStatusDisbursementAuto = objLoanCreditDisbursementAuto == null ? "" : ((LoanCreditDisbursementAuto_Status)objLoanCreditDisbursementAuto.Status).GetDescription();
                            }
                        }
                    }
                    response.SetSucces();
                    response.Data = dataLos;
                    return response;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"DisbursementWaitingLOSQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private int ConverStatusLMS(int Status)
        {
            switch (Status)
            {
                // LOS: Chờ đẩy cho lender, 
                case 90: return (int)LMS.Common.Constants.StateLoanCreditNew.WaittingPushToLender;
                // Chờ kế toán giải ngân
                case 102: return (int)LMS.Common.Constants.StateLoanCreditNew.AccountantDisbursement;
                // Chờ lender giải ngân
                case 103: return (int)LMS.Common.Constants.StateLoanCreditNew.WaittingAgency;
                // giải ngân tự động
                case 104: return (int)LMS.Common.Constants.StateLoanCreditNew.AutoDisbursement;
                // không rõ status
                default: return -1;
            }
        }
    }
}
