﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.RestClients;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.LOS
{
    public class GetListImagesLosByLoanBriefIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanBriefID { get; set; }
    }
    public class GetListImagesLosByLoanBriefIDQueryHandler : IRequestHandler<GetListImagesLosByLoanBriefIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        ILOSService _lOSService;
        ILogger<GetListImagesLosByLoanBriefIDQueryHandler> _logger;
        public GetListImagesLosByLoanBriefIDQueryHandler(ILOSService lOSService, ILogger<GetListImagesLosByLoanBriefIDQueryHandler> logger)
        {
            _lOSService = lOSService;
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(GetListImagesLosByLoanBriefIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                return await Task.Run(() =>
                {
                    response.SetSucces();
                    response.Data = _lOSService.GetListImages(request.LoanBriefID).Result;
                    return response;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetListImagesLosByLoanBriefIDQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
