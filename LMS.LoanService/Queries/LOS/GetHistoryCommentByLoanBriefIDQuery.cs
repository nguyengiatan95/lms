﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.RestClients;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.LOS
{
    public class GetHistoryCommentByLoanBriefIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public long LoanBriefID { get; set; }
    }
    public class GetHistoryCommentByLoanBriefIDQueryHandler : IRequestHandler<GetHistoryCommentByLoanBriefIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        ILogger<DisbursementWaitingLOSQueryHandler> _logger;
        ILOSService _lOSService;
        public GetHistoryCommentByLoanBriefIDQueryHandler(ILogger<DisbursementWaitingLOSQueryHandler> logger, ILOSService lOSService)
        {
            _logger = logger;
            _lOSService = lOSService;
        }
        public async Task<ResponseActionResult> Handle(GetHistoryCommentByLoanBriefIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                return await Task.Run(() =>
                {
                    response.SetSucces();
                    response.Data = _lOSService.GetHistoryCommentLOS(request.PageIndex, request.PageSize, request.LoanBriefID).Result;
                    return response;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetHistoryCommentByLoanBriefIDQueryHandler|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;

        }
    }
}
