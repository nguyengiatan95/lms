﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.RestClients;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.LOS
{
    public class GetHistoryCommentLosQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public long LoanID { get; set; }
    }
    public class GetHistoryCommentLosQueryHandler : IRequestHandler<GetHistoryCommentLosQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        ILOSService _lOSService;
        public GetHistoryCommentLosQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab, ILOSService lOSService)
        {
            _loanTab = loanTab;
            _lOSService = lOSService;
        }
        public async Task<ResponseActionResult> Handle(GetHistoryCommentLosQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                var objLoan = _loanTab.WhereClause(x => x.LoanID == request.LoanID).Query().FirstOrDefault();
                if (objLoan != null && objLoan.LoanCreditIDOfPartner != null)
                {
                    response.SetSucces();
                    response.Data = _lOSService.GetHistoryCommentLOS(request.PageIndex, request.PageSize, (long)objLoan.LoanCreditIDOfPartner).Result;
                }
                return response;
            });
        }
    }
}
