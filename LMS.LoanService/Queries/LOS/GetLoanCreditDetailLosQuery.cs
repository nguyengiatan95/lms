﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Models.Insurance;
using LMS.LoanServiceApi.RestClients;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace LMS.LoanServiceApi.Queries.LOS
{
    public class GetLoanCreditDetailLosQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanBriefID { get; set; }
    }
    public class GetLoanCreditDetailLosQueryHandler : IRequestHandler<GetLoanCreditDetailLosQuery, LMS.Common.Constants.ResponseActionResult>
    {
        ILogger<GetLoanCreditDetailLosQueryHandler> _logger;
        Services.ILOSManager _lOSManager;
        Common.Helper.Utils _common;
        public GetLoanCreditDetailLosQueryHandler(ILogger<GetLoanCreditDetailLosQueryHandler> logger, Services.ILOSManager lOSManager)
        {
            _lOSManager = lOSManager;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetLoanCreditDetailLosQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.SetSucces();
                response.Data = await _lOSManager.GetLoanCreditDetail(request.LoanBriefID);
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLoanCreditDetailLosQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
