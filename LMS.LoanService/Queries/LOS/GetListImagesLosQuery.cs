﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.RestClients;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace LMS.LoanServiceApi.Queries.LOS
{
    public class GetListImagesLosQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
    }
    public class GetListImagesLosQueryHandler : IRequestHandler<GetListImagesLosQuery, LMS.Common.Constants.ResponseActionResult>
    {
        ILOSService _lOSService;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        public GetListImagesLosQueryHandler(ILOSService lOSService, LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab)
        {
            _lOSService = lOSService;
            _loanTab = loanTab;
        }
        public async Task<ResponseActionResult> Handle(GetListImagesLosQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                var objLoan = _loanTab.WhereClause(x => x.LoanID == request.LoanID).Query().FirstOrDefault();
                if (objLoan != null && objLoan.LoanCreditIDOfPartner != null)
                {
                    response.SetSucces();
                    response.Data = _lOSService.GetListImages((long)objLoan.LoanCreditIDOfPartner).Result;
                }
                return response;
            });
        }
    }
}
