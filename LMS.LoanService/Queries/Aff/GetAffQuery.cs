﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.Aff
{
    public class GetAffQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class GetAffQueryHandler : IRequestHandler<GetAffQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> _affTab;
        ILogger<GetAffQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetAffQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblAff> affTab,
            ILogger<GetAffQueryHandler> logger)
        {
            _affTab = affTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetAffQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                return await Task.Run(() =>
                {
                    var lstAff = _affTab.Query();
                    if (lstAff != null)
                    {
                        var result = new List<Domain.SelectItem>();
                        foreach (var item in lstAff)
                        {
                            result.Add(new Domain.SelectItem
                            {
                                Text = item.Code + " - " + item.Name,
                                Value = item.AffID
                            });
                        }
                        response.SetSucces();
                        response.Data = result;
                    }
                    return response;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"{System.Reflection.MethodBase.GetCurrentMethod().Name}_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
