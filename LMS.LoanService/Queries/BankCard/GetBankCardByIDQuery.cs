﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.BankCard
{
    public class GetBankCardByIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long BankCardID { get; set; }
    }
    public class GetBankCardByIDQueryHandler : IRequestHandler<GetBankCardByIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        ILogger<GetBankCardByIDQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetBankCardByIDQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab,
            ILogger<GetBankCardByIDQueryHandler> logger)
        {
            _bankCardTab = bankCardTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetBankCardByIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                return await Task.Run(() =>
                {
                    var objBankCard = _bankCardTab.GetByID(request.BankCardID);
                    if (objBankCard != null)
                    {
                        response.SetSucces();
                        response.Data = objBankCard;
                    }
                    return response;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"{System.Reflection.MethodBase.GetCurrentMethod().Name}_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
