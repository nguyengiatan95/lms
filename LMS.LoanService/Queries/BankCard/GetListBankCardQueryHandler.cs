﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Models.Loan;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.BankCard
{
    public class GetListBankCardQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string GeneralSearch { get; set; }
        public long BankCardID { get; set; }
        public int Status { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class GetListBankCardQueryHandler : IRequestHandler<GetListBankCardQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        ILogger<GetListBankCardQueryHandler> _logger;
        public GetListBankCardQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab, ILogger<GetListBankCardQueryHandler> logger)
        {
            _bankCardTab = bankCardTab;
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(GetListBankCardQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                return await Task.Run(() =>
                {
                    if (request.BankCardID  != (int)StatusCommon.Default)
                        _bankCardTab.WhereClause(x => x.BankCardID == request.BankCardID);

                    if (request.Status != (int)StatusCommon.Default)
                        _bankCardTab.WhereClause(x => x.Status == request.Status);

                    if (!string.IsNullOrEmpty(request.GeneralSearch))
                        _bankCardTab.WhereClause(x => x.AliasName.Contains(request.GeneralSearch) || x.BankCode.Contains(request.GeneralSearch) || x.NumberAccount.Contains(request.GeneralSearch) || x.AccountHolderName.Contains(request.GeneralSearch));

                    var lstBankCard = _bankCardTab.Query();
                    var totalCount = lstBankCard.Count();
                    lstBankCard = lstBankCard.OrderByDescending(x => x.CreateOn).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();

                    ResponseForDataTable dataTable = new ResponseForDataTable
                    {
                        RecordsTotal = totalCount,
                        Data = lstBankCard,
                        RecordsFiltered = totalCount,
                        Draw = 1
                    };
                    response.SetSucces();
                    response.Total = totalCount;
                    response.Data = dataTable;
                    return response;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"{System.Reflection.MethodBase.GetCurrentMethod().Name}_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
