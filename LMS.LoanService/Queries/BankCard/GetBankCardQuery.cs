﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace LMS.LoanServiceApi.Queries.BankCard
{
    public class GetBankCardQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long BankCardID { get; set; }
        public int Status { get; set; }
        public int TypePurpose { get; set; }
    }
    public class GetBankCardQueryHandler : IRequestHandler<GetBankCardQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        ILogger<GetBankCardQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetBankCardQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab,
            ILogger<GetBankCardQueryHandler> logger)
        {
            _bankCardTab = bankCardTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetBankCardQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                return await Task.Run(() =>
                {
                    if (request.BankCardID != (int)StatusCommon.Default)
                        _bankCardTab.WhereClause(x => x.BankCardID == request.BankCardID);
                    if (request.Status != (int)StatusCommon.Default)
                        _bankCardTab.WhereClause(x => x.Status == request.Status);
                    if (request.TypePurpose != (int)StatusCommon.Default)
                        _bankCardTab.WhereClause(x => x.TypePurpose == request.TypePurpose);
                    var lstBankCard = _bankCardTab.Query();
                    response.SetSucces();
                    if (lstBankCard != null)
                    {
                        var result = new List<Domain.SelectItem>();
                        foreach (var item in lstBankCard)
                        {
                            var itemSelect = new Domain.SelectItem
                            {
                                Text = item.AliasName + "-" + item.NumberAccount,
                                Value = item.BankCardID
                            };
                            if (request.TypePurpose == (int)BankCard_TypePurpose.Disbursement)
                            {
                                // fix cứng giá trị VIB2
                                if (item.AliasName == "VIB2")
                                {
                                    itemSelect.Option = new { Auto = 1 };
                                }
                            }
                            result.Add(itemSelect);
                        }
                        response.Data = result;
                    }
                    return response;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetBankCardQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
