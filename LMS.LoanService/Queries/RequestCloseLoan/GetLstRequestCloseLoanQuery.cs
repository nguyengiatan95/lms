﻿using LMS.Common.Constants;
using LMS.Common.DAL;
using LMS.Common.Helper;
using LMS.LoanServiceApi.Domain.Models.RequestCloseLoan;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.RequestCloseLoan
{
    public class GetLstRequestCloseLoanQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int Status { get; set; }
        public string KeySearch { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public GetLstRequestCloseLoanQuery()
        {
            Status = (int)StatusCommon.SearchAll;
            PageIndex = 1;
            PageSize = 20;
        }
    }
    public class GetLstRequestCloseLoanQueryHandler : IRequestHandler<GetLstRequestCloseLoanQuery, ResponseActionResult>
    {
        LMS.Common.Helper.Utils _common;
        ITableHelper<Domain.Tables.TblRequestCloseLoan> _requestCloseLoanTab;
        ITableHelper<Domain.Tables.TblUser> _userTab;
        ITableHelper<Domain.Tables.TblLoan> _loanTab;
        ILogger<GetLstRequestCloseLoanQueryHandler> _logger;
        public GetLstRequestCloseLoanQueryHandler(ITableHelper<Domain.Tables.TblRequestCloseLoan> requestCloseLoanTab,
            ITableHelper<Domain.Tables.TblUser> userTab,
            ITableHelper<Domain.Tables.TblLoan> loanTab,
            ILogger<GetLstRequestCloseLoanQueryHandler> logger)
        {
            _userTab = userTab;
            _requestCloseLoanTab = requestCloseLoanTab;
            _loanTab = loanTab;
            _common = new Common.Helper.Utils();
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(GetLstRequestCloseLoanQuery request, CancellationToken cancellationToken)
        {
            List<RequestCloseLoanViewModel> lstResult = new List<RequestCloseLoanViewModel>();
            ResponseActionResult response = new ResponseActionResult();
            ResponseForDataTable dataTable = new ResponseForDataTable
            {
                RecordsTotal = 0,
                Data = lstResult,
                RecordsFiltered = 0,
                Draw = 1
            };
            try
            {
                response.SetSucces();
                if (DateTime.TryParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, null, System.Globalization.DateTimeStyles.None, out var fromDate))
                {
                    _requestCloseLoanTab.WhereClause(x => x.CreateDate > fromDate);
                }
                if (DateTime.TryParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, null, System.Globalization.DateTimeStyles.None, out var toDate))
                {
                    toDate = toDate.AddDays(1);
                    _requestCloseLoanTab.WhereClause(x => x.CreateDate < toDate);
                }
                if (request.Status != (int)StatusCommon.SearchAll)
                {
                    _requestCloseLoanTab.WhereClause(x => x.Status == request.Status);
                }
                if (!string.IsNullOrEmpty(request.KeySearch))
                {
                    if (long.TryParse(request.KeySearch, out long contracCodeID) && request.KeySearch.Length < TimaSettingConstant.MaxLengthPhone - 1)
                    {
                        request.KeySearch = $"{TimaSettingConstant.PrefixContractCode}{contracCodeID}";
                        _loanTab.WhereClause(x => x.ContactCode == request.KeySearch);
                    }
                    else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternContractCode, RegexOptions.IgnoreCase).Success)
                    {
                        _loanTab.WhereClause(x => x.ContactCode == request.KeySearch);
                    }
                    else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternContractCodeLOSLoanCreditID, RegexOptions.IgnoreCase).Success)
                    {
                        var loanCreditID = long.Parse(request.KeySearch.Replace(TimaSettingConstant.PatternContractCodeLOS, "", StringComparison.OrdinalIgnoreCase));
                        _loanTab.WhereClause(x => x.LoanCreditIDOfPartner == loanCreditID);
                    }
                    else
                    {
                        _loanTab.WhereClause(x => x.CustomerName == request.KeySearch);
                    }
                    var lstLoanData = await _loanTab.SelectColumns(x => x.LoanID).QueryAsync();
                    if (lstLoanData == null || !lstLoanData.Any())
                    {
                        return response;
                    }
                    var lstLoanID = lstLoanData.Select(x => x.LoanID).ToList();
                    _requestCloseLoanTab.WhereClause(x => lstLoanID.Contains(x.LoanID));
                }

                response.Data = dataTable;
                var lstData = await _requestCloseLoanTab.QueryAsync();
                if (lstData == null || !lstData.Any())
                {
                    return response;
                }
                dataTable.RecordsTotal = lstData.Count();
                lstData = lstData.OrderByDescending(x => x.RequestCloseLoanID).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize);
                foreach (var item in lstData)
                {
                    var jsonExtra = _common.ConvertJSonToObjectV2<Domain.Tables.RequestCloseLoanJsonExtra>(item.JsonExtra);
                    lstResult.Add(new RequestCloseLoanViewModel
                    {
                        ApprovedByName = jsonExtra.ApprovedByName,
                        ApprovedDate = item.ApprovedDate,
                        CreateDate = item.CreateDate,
                        ModifyDate = item.ModifyDate,
                        LoanContractCode = jsonExtra.LoanContractCode,
                        LoanID = item.LoanID,
                        RequestByName = jsonExtra.RequestByName,
                        Status = item.Status,
                        StatusName = ((RequestCloseLoan_Status)item.Status).GetDescription(),
                        RequestCloseLoanID = item.RequestCloseLoanID,
                        CloseDate = item.CloseDate,
                        Reason = item.Reason,
                        Note = item.Note
                    });
                }
                dataTable.Data = lstResult.OrderByDescending(x => x.CreateDate);
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLoanDebtByLoanIDQuery_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;

        }
    }
}
