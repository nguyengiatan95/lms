﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.Bank
{
    public class GetBankQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long BankID { get; set; }
    }
    public class GetBankQueryHandler : IRequestHandler<GetBankQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBank> _bankTab;
        ILogger<GetBankQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetBankQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBank> bankTab,
            ILogger<GetBankQueryHandler> logger)
        {
            _bankTab = bankTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetBankQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                return await Task.Run(() =>
                {
                    if (request.BankID != 0)
                        _bankTab.WhereClause(x => x.Id == request.BankID);

                    var lstBankCard = _bankTab.Query();
                    if (lstBankCard != null)
                    {
                        var result = new List<Domain.SelectItem>();
                        foreach (var item in lstBankCard)
                        {
                            result.Add(new Domain.SelectItem
                            {
                                Text = $"{item.NameBank} ({item.Code})",
                                Value = item.Id
                            });
                        }
                        response.SetSucces();
                        response.Data = result;
                    }
                    return response;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"{System.Reflection.MethodBase.GetCurrentMethod().Name}_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
