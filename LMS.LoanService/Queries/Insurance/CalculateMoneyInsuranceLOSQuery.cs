﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Models.Insurance;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace LMS.LoanServiceApi.Queries.Insurance
{
    public class CalculateMoneyInsuranceLOSQuery : IRequest<Common.Constants.ResponseActionResult>
    {
        public long LoanBriefID { get; set; }
        public long TotalMoney { get; set; }
        public int LoanTime { get; set; }
        public int RateType { get; set; }
        public decimal RateInterest { get; set; }
        public decimal RateInterestLender { get; set; }
        public int Frequency { get; set; }
        public bool InsuranceCustomer { get; set; }
        public bool InsuranceMaterial { get; set; }

    }
    public class CalculateMoneyInsuranceLOSQueryHandler : IRequestHandler<CalculateMoneyInsuranceLOSQuery, Common.Constants.ResponseActionResult>
    {
        RestClients.IInsuranceService _insuranceService;
        ILogger<CalculateMoneyInsuranceLOSQueryHandler> _logger;
        RestClients.ILOSService _lOSService;
        Common.Helper.Utils _common;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        public CalculateMoneyInsuranceLOSQueryHandler(
            ILogger<CalculateMoneyInsuranceLOSQueryHandler> logger,
            RestClients.ILOSService lOSService,
            RestClients.IInsuranceService insuranceService,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab)
        {
            _logger = logger;
            _insuranceService = insuranceService;
            _lOSService = lOSService;
            _lenderTab = lenderTab;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CalculateMoneyInsuranceLOSQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    if (request.LoanBriefID != (int)StatusCommon.UnActive)
                    {
                        var objLoanLos = _lOSService.GetLoanCreditDetail(request.LoanBriefID).Result;
                        if (objLoanLos == null)
                        {
                            response.Message = MessageConstant.LoanNotExist;
                            return response;
                        }
                        var objLender = _lenderTab.WhereClause(x => x.LenderID == objLoanLos.LenderId).Query().FirstOrDefault();
                        if (objLender == null)
                        {
                            response.Message = MessageConstant.NotIdentifiedLenderID;
                            return response;
                        }
                        request.RateInterest = _common.ConvertRateToVND(objLoanLos.RatePercent);
                        request.RateInterestLender = _common.ConvertRateInterestLenderToVND((float)objLender.RateInterest);
                        request.TotalMoney = Convert.ToInt64(objLoanLos.LoanAmountFinal);
                        request.LoanTime = objLoanLos.LoanTime;
                        request.Frequency = objLoanLos.Frequency;
                        request.RateType = objLoanLos.RateTypeId;
                    }
                    else
                    {
                        if (request.TotalMoney == (int)StatusCommon.UnActive)
                        {
                            response.Message = MessageConstant.TotalMoneyNull;
                            return response;
                        }
                        if (request.RateInterest == (int)StatusCommon.UnActive)
                            request.RateInterest = Constants.LOSConstants.RateInterest;
                        if (request.RateInterestLender == (int)StatusCommon.UnActive)
                            request.RateInterestLender = Constants.LOSConstants.RateInterestLender;
                        if (request.LoanTime == (int)StatusCommon.UnActive)
                            request.LoanTime = Constants.LOSConstants.LoanTime;
                        if (request.Frequency == (int)StatusCommon.UnActive)
                            request.Frequency = Constants.LOSConstants.Frequency;
                    }

                    request.InsuranceCustomer = true;
                    request.InsuranceMaterial = true;
                    var calculateMoneyInsuranceLOS = new CalculateMoneyInsuranceLOSReq();
                    calculateMoneyInsuranceLOS.TotalMoneyDisbursement = request.TotalMoney;
                    calculateMoneyInsuranceLOS.LoanTime = _common.ConvertLoanTimeLOSToLMS(request.LoanTime);
                    calculateMoneyInsuranceLOS.LoanFrequency = _common.ConvertFrequencyLOSToLMS(request.Frequency);
                    calculateMoneyInsuranceLOS.RateType = request.RateType;
                    calculateMoneyInsuranceLOS.RateConsultant = request.RateInterest - request.RateInterestLender;
                    calculateMoneyInsuranceLOS.RateInterest = request.RateInterestLender;
                    calculateMoneyInsuranceLOS.RateService = 0;

                    var resultData = _insuranceService.CalculateMoneyInsuranceLOS(calculateMoneyInsuranceLOS).Result;
                    if (resultData != null)
                    {
                        response.SetSucces();
                        response.Data = resultData;
                    }
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"CalculateMoneyInsuranceLOSQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}");
                    return response;
                }
            });
        }
    }
}


