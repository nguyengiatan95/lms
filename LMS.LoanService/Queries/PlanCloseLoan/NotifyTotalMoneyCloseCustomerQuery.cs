﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

namespace LMS.LoanServiceApi.Queries.PlanCloseLoan
{
    public class NotifyTotalMoneyCloseCustomerQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
    }
    public class NotifyTotalMoneyCloseCustomerQueryHandler : IRequestHandler<NotifyTotalMoneyCloseCustomerQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanCloseLoan> _planCloseLoanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        ILogger<NotifyTotalMoneyCloseCustomerQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public NotifyTotalMoneyCloseCustomerQueryHandler(
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanCloseLoan> planCloseLoanTab,
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
        ILogger<NotifyTotalMoneyCloseCustomerQueryHandler> logger)
        {
            _planCloseLoanTab = planCloseLoanTab;
            _loanTab = loanTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(NotifyTotalMoneyCloseCustomerQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objLoan = (await _loanTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                if (objLoan != null)
                {
                    var dictLoanIDByCustomer = (await _loanTab.SelectColumns(x=>x.LoanID,x=>x.CustomerID).WhereClause(x => x.CustomerID == objLoan.CustomerID).QueryAsync()).ToDictionary(x => x.LoanID, x => x);
                    var closeDate = DateTime.Now.Date;
                    var totalMoneyClose = (await _planCloseLoanTab.SelectColumns(x => x.LoanID,x=>x.CloseDate,x=>x.TotalMoneyClose).WhereClause(x => dictLoanIDByCustomer.Keys.Contains(x.LoanID) && x.CloseDate == closeDate).QueryAsync()).Sum(x=>x.TotalMoneyClose);
                    response.Data = totalMoneyClose;
                    response.SetSucces();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"NotifyTotalMoneyCloseCustomerQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
