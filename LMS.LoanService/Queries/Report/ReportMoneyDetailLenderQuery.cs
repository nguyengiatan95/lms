﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.LoanServices.Report;
using LMS.LoanService.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.Report
{
    public class ReportMoneyDetailLenderQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int ProductID { get; set; }
        public string ContractCode { get; set; }
        public int LenderID { get; set; }
    }

    public class ReportMoneyDetailLenderQueryHandler : IRequestHandler<ReportMoneyDetailLenderQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        ILogger<ReportMoneyDetailLenderQueryHandler> _logger;
        CommonLoanServiceApi _commonLoanServiceApi;
        public ReportMoneyDetailLenderQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            ILogger<ReportMoneyDetailLenderQueryHandler> logger)
        {
            _transactionTab = transactionTab;
            _lenderTab = lenderTab;
            _loanTab = loanTab;
            _logger = logger;
            _commonLoanServiceApi = new CommonLoanServiceApi();
        }
        public async Task<ResponseActionResult> Handle(ReportMoneyDetailLenderQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var result = new List<MoneyDetailLenderModel>();
                var fromDate = DateTime.ParseExact(request.FromDate, _commonLoanServiceApi.DateTimeDDMMYYYYNoTime, CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(request.ToDate, _commonLoanServiceApi.DateTimeDDMMYYYYNoTime, CultureInfo.InvariantCulture);
                toDate = toDate.AddDays(1);
                var lstListLender = new List<long>();
                var lstLoanIds = new List<long>();
                var dicLoan = new Dictionary<long, Domain.Tables.TblLoan>();
                var dicLender = new Dictionary<long, Domain.Tables.TblLender>();
                long sumTotalMoney = 0;
                long sumMoneyOriginal = 0;
                long sumMoneyFineOriginal = 0;
                long sumMoneyInterest = 0;
                long sumMoneyService = 0;
                long sumMoneyConsultant = 0;
                long sumMoneyFineLateLender = 0;
                long sumMoneyFineLateTima = 0;
                IEnumerable<Domain.Tables.TblLoan> lstLoan = null;

                //search theo contractCode
                if (!string.IsNullOrWhiteSpace(request.ContractCode))
                {
                    //request.ContractCode = !request.ContractCode.ToUpper().Contains("TC-") ? "TC-" + request.ContractCode.Trim() : request.ContractCode;
                    if (long.TryParse(request.ContractCode, out long contracCodeID) && request.ContractCode.Length < TimaSettingConstant.MaxLengthPhone - 1)
                    {
                        request.ContractCode = $"{TimaSettingConstant.PrefixContractCode}{contracCodeID}";
                        _loanTab.WhereClause(x => x.ContactCode == request.ContractCode);
                    }
                    else if (Regex.Match(request.ContractCode, TimaSettingConstant.PatternContractCode, RegexOptions.IgnoreCase).Success)
                    {
                        _loanTab.WhereClause(x => x.ContactCode == request.ContractCode);
                    }
                    if (request.ProductID != (int)StatusCommon.SearchAll)
                        _loanTab.WhereClause(x => x.ProductID == request.ProductID);

                    lstLoan = await _loanTab.SelectColumns(x => x.LoanID, x => x.ProductName, x => x.CustomerName, x => x.TotalMoneyDisbursement, x => x.LenderID, x => x.ContactCode)
                                       .WhereClause(x => x.ContactCode == request.ContractCode)
                                       .QueryAsync();
                    if (lstLoan == null || !lstLoan.Any())
                    {
                        // k có dữ liệu , response luôn
                        response.SetSucces();
                        response.Data = result;
                        return response;
                    }
                    lstLoanIds = lstLoan.Select(x => x.LoanID).ToList();
                }
                if (lstLoanIds.Any())
                    _transactionTab.WhereClause(x => lstLoanIds.Contains((int)x.LoanID));
                if (request.LenderID != (int)StatusCommon.SearchAll)
                    _transactionTab.WhereClause(x => x.LenderID == request.LenderID);

                var lstTrans = await _transactionTab.WhereClause(x => x.CreateDate >= fromDate && x.CreateDate < toDate && x.TotalMoney != 0).QueryAsync();
                // với case contractCode k có và productID có => search trantrc => search loan
                if (string.IsNullOrWhiteSpace(request.ContractCode) && request.ProductID != (int)StatusCommon.SearchAll)
                {
                    var lstLoanProduct = new List<Domain.Tables.TblLoan>();
                    var lstLoanTranIds = lstTrans.OrderBy(x => x.LoanID).Distinct().Select(x => x.LoanID).ToList();
                    int pageSize = 2000;
                    int numberRow = lstLoanTranIds.Count() / pageSize + 1;
                    for (int i = 1; i <= numberRow; i++)
                    {
                        var lstLoanIdschild = lstLoanTranIds.Skip((i - 1) * pageSize).Take(pageSize).ToList();
                        var lstLoanchild = await _loanTab.SelectColumns(x => x.LoanID, x => x.ProductName, x => x.CustomerName, x => x.TotalMoneyDisbursement, x => x.LenderID, x => x.ContactCode)
                                                   .WhereClause(x => x.ProductID == request.ProductID && lstLoanIdschild.Contains(x.LoanID)).QueryAsync();
                        lstLoanProduct.AddRange(lstLoanchild.ToList());
                    }
                    lstLoan = lstLoanProduct;
                    var lstLoanIdsProduct = lstLoan.Select(x => x.LoanID).ToList();
                    lstTrans = lstTrans.Where(x => lstLoanIdsProduct.Contains(x.LoanID ?? 0));
                }
                var lstTranGroup = lstTrans
                            .GroupBy(l => new { l.LoanID, l.CreateDate, l.ActionID, l.LenderID })
                            .Select(cl => new MoneyDetailLenderModel
                            {
                                ActionID = cl.Key.ActionID,
                                LoanID = cl.Key.LoanID,
                                CreateDate = cl.Key.CreateDate,
                                LenderID = cl.Key.LenderID.Value,
                                MoneyConsultant = cl.Where(x => x.MoneyType == (int)Transaction_TypeMoney.Consultant).Sum(x => x.TotalMoney),
                                MoneyService = cl.Where(x => x.MoneyType == (int)Transaction_TypeMoney.Service).Sum(x => x.TotalMoney),
                                MoneyOriginal = cl.Where(x => x.MoneyType == (int)Transaction_TypeMoney.Original).Sum(x => x.TotalMoney),
                                MoneyInterest = cl.Where(x => x.MoneyType == (int)Transaction_TypeMoney.Interest).Sum(x => x.TotalMoney),
                                MoneyFineOriginal = cl.Where(x => x.MoneyType == (int)Transaction_TypeMoney.FineOriginal).Sum(x => x.TotalMoney),
                                MoneyFineLateTima = cl.Where(x => x.MoneyType == (int)Transaction_TypeMoney.FineLate).Sum(x => x.TotalMoney),
                            }).ToList();
                // ban ghi tong
                int totalCount = lstTranGroup.Count();
                if (lstTranGroup.Any())
                {
                    foreach (var item in lstTranGroup)
                    {
                        if (item.ActionID == (int)Transaction_Action.ChoVay)
                        {
                            sumTotalMoney += item.MoneyOriginal;
                        }
                        else
                        {
                            sumMoneyOriginal += item.MoneyOriginal;
                        }

                        sumMoneyFineOriginal += item.MoneyFineOriginal;
                        sumMoneyInterest += item.MoneyFineOriginal;
                        sumMoneyService += item.MoneyService;
                        sumMoneyConsultant += item.MoneyConsultant;
                        sumMoneyFineLateLender += item.MoneyFineLateLender;
                        sumMoneyFineLateTima += item.MoneyFineLateTima;
                    }
                }
                lstTranGroup = lstTranGroup.OrderByDescending(x => x.CreateDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                // case  truyền mặc định contract _ product lên
                if (string.IsNullOrWhiteSpace(request.ContractCode) && request.ProductID == (int)StatusCommon.SearchAll)
                {
                    var lstLoanIdschild = lstTranGroup.OrderBy(x => x.LoanID).Distinct().Select(x => x.LoanID).ToList();
                    lstLoan = _loanTab.SelectColumns(x => x.LoanID, x => x.ProductName, x => x.CustomerName, x => x.TotalMoneyDisbursement, x => x.LenderID, x => x.ContactCode)
                                                  .WhereClause(x => lstLoanIdschild.Contains(x.LoanID)).Query();
                }
                if (lstTrans.Any())
                {
                    // lấy mã lender
                    var lstLenderIds = lstTrans.Select(x => x.LenderID).Distinct();
                    var lstLender = _lenderTab.SelectColumns(x => x.LenderID, x => x.FullName)
                                              .WhereClause(x => lstLenderIds.Contains(x.LenderID)).Query();
                    dicLoan = lstLoan.ToDictionary(x => x.LoanID, x => x);
                    dicLender = lstLender.ToDictionary(x => x.LenderID, x => x);
                    foreach (var item in lstTranGroup)
                    {
                        var objLoan = dicLoan.GetValueOrDefault(item.LoanID ?? 0);
                        var objLender = dicLender.GetValueOrDefault(item.LenderID);
                        result.Add(new MoneyDetailLenderModel()
                        {
                            ActionName = ((Transaction_Action)item.ActionID).GetDescription(),
                            ContractCode = objLoan.ContactCode,
                            CreateDate = item.CreateDate,
                            CustomerName = objLoan.CustomerName,
                            LenderCode = objLender.FullName,
                            MoneyConsultant = item.MoneyConsultant,
                            MoneyFineLateTima = item.MoneyFineLateTima,
                            MoneyFineLateLender = item.MoneyFineLateLender,
                            MoneyInterest = item.MoneyInterest,
                            MoneyService = item.MoneyService,
                            MoneyOriginal = item.ActionID == (int)Transaction_Action.ChoVay ? 0 : item.MoneyOriginal,
                            MoneyFineOriginal = item.MoneyFineOriginal,
                            TotalMoney = item.ActionID == (int)Transaction_Action.ChoVay ? item.MoneyOriginal : 0,
                            ActionID = item.ActionID,
                            LenderID = item.LenderID,
                            LoanID = item.LoanID,
                            ProductName = objLoan.ProductName,
                            SumMoneyConsultant = sumMoneyConsultant,
                            SumMoneyFineLateLender = sumMoneyFineLateLender,
                            SumMoneyFineLateTima = sumMoneyFineLateTima,
                            SumMoneyFineOriginal = sumMoneyFineOriginal,
                            SumMoneyInterest = sumMoneyInterest,
                            SumMoneyOriginal = sumMoneyOriginal,
                            SumMoneyService = sumMoneyService,
                            SumTotalMoney = sumTotalMoney

                        });
                    }
                }
                response.SetSucces();
                response.Data = result;
                response.Total = totalCount;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ReportMoneyDetailLenderQuery|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
