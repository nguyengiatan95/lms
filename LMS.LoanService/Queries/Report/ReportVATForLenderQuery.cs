﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.LoanService.Helper;
using LMS.LoanServiceApi.Constants;
using LMS.LoanServiceApi.Domain.Models.Report;
using LMS.LoanServiceApi.Domain.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.Report
{
    public class ReportVATForLenderQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LenderID { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public List<int> LstMoneyType { get; set; }
    }
    public class ReportVATForLenderQueryHandler : IRequestHandler<ReportVATForLenderQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        ILogger<ReportVATForLenderQueryHandler> _logger;
        CommonLoanServiceApi _commonLoanServiceApi;
        LMS.Common.Helper.Utils _common;
        public ReportVATForLenderQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            ILogger<ReportVATForLenderQueryHandler> logger)
        {
            _transactionTab = transactionTab;
            _customerTab = customerTab;
            _loanTab = loanTab;
            _lenderTab = lenderTab;
            _logger = logger;
            _commonLoanServiceApi = new CommonLoanServiceApi();
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(ReportVATForLenderQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {

                var result = new List<ReportVatItem>();
                var fromDate = DateTime.ParseExact(request.FromDate, _commonLoanServiceApi.DateTimeDDMMYYYY, CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(request.ToDate, _commonLoanServiceApi.DateTimeDDMMYYYY, CultureInfo.InvariantCulture);
                toDate = toDate.AddDays(1);
  
                if (request.LstMoneyType.Count <= 0)
                {
                    response.Message = MessageConstant.NullTypeMoney;
                    return response;
                }
                if (fromDate < Constants.ReportConstants.FromDateReportVATForLender)
                {
                    response.Message = MessageConstant.TimeLimit01082021;
                    return response;
                }
                fromDate = fromDate.AddSeconds(-1);
                _transactionTab.WhereClause(x => request.LstMoneyType.Contains(x.MoneyType) && x.LenderID != null && x.TotalMoney != 0 && (x.CreateDate > fromDate && x.CreateDate < toDate));

                if (request.LenderID != (int)StatusCommon.Default)
                    _transactionTab.WhereClause(x => x.LenderID == request.LenderID);

                var lstTrans = await _transactionTab.QueryAsync();
                int totalCount = lstTrans.Count();
                if (totalCount > 0)
                {
                    lstTrans = lstTrans.OrderByDescending(x => x.CreateDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                    var lstCusID = new List<long>();
                    var lstLoanID = new List<long>();
                    foreach (var item in lstTrans)
                    {
                        lstCusID.Add((long)item.CustomerID);
                        lstLoanID.Add((long)item.LoanID);
                    }
                    var lstCustomer = _customerTab.WhereClause(x => lstCusID.Contains(x.CustomerID)).QueryAsync();
                    var lstLoan = _loanTab.WhereClause(x => lstLoanID.Contains(x.LoanID)).QueryAsync();

                    await Task.WhenAll(lstCustomer, lstLoan);

                    var dictCustomerInfos = lstCustomer.Result.ToDictionary(x => x.CustomerID, x => x);
                    var dictLoanInfos = lstLoan.Result.ToDictionary(x => x.LoanID, x => x);

                    int i = 0;
                    foreach (var item in lstTrans)
                    {
                        i++;
                        var objCustomer = dictCustomerInfos.GetValueOrDefault(item.CustomerID ?? 0);
                        var objLoan = dictLoanInfos.GetValueOrDefault(item.LoanID ?? 0);
                        result.Add(new ReportVatItem
                        {
                            Denominator = Constants.ReportConstants.Denominator,
                            Notation = Constants.ReportConstants.Notation,
                            OrderNumber = Constants.ReportConstants.OrderNumber + i,
                            TransactionDate = item.CreateDate,
                            CustomerName = objCustomer?.FullName,
                            Address = objCustomer?.Address,
                            ContactCode = objLoan.ContactCode,
                            TypePayment = Constants.ReportConstants.TypePayment,
                            CommodityName = CommodityName(item.MoneyType),
                            // TaxPercentage = Constants.ReportConstants.TaxPercentage,
                            //FeeNotVat = feeNotVat,
                            //Vat = item.TotalMoney - feeNotVat,
                            FeeNotVat = item.TotalMoney,
                            TotalMoney = item.TotalMoney,
                            TypeTransaction = ((Transaction_Action)item.ActionID).GetDescription()
                        });
                    }
                    response.Total = totalCount;
                    response.Data = result;
                }
                response.SetSucces();
                return response;

            }
            catch (Exception ex)
            {
                _logger.LogError($"ReportVATForLenderQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        private string CommodityName(int moneyType)
        {
            switch (moneyType)
            {
                case (int)Transaction_TypeMoney.Interest:
                    return Constants.ReportConstants.CommodityNameReportVATForLender;
                default: return string.Empty;
            }
        }
    }
}
