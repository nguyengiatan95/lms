﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.LoanService.Helper;
using LMS.LoanServiceApi.Domain.Models.Report;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.Report
{
    public class ReportVatLenderQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LenderID { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class ReportVatLenderQueryHandler : IRequestHandler<ReportVatLenderQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        CommonLoanServiceApi _commonLoanServiceApi;
        ILogger<ReportVatLenderQueryHandler> _logger;
        public ReportVatLenderQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            ILogger<ReportVatLenderQueryHandler> logger)
        {
            _transactionTab = transactionTab;
            _customerTab = customerTab;
            _loanTab = loanTab;
            _logger = logger;
            _commonLoanServiceApi = new CommonLoanServiceApi();
        }
        public async Task<ResponseActionResult> Handle(ReportVatLenderQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                return await Task.Run(() =>
                {
                    var result = new List<ReportVatItem>();
                    var fromDate = DateTime.ParseExact(request.FromDate, _commonLoanServiceApi.DateTimeDDMMYYYY, CultureInfo.InvariantCulture);
                    var toDate = DateTime.ParseExact(request.ToDate, _commonLoanServiceApi.DateTimeDDMMYYYY, CultureInfo.InvariantCulture);
                    toDate = toDate.AddDays(1);
                    var lstLenderReq = Enum.GetValues(typeof(ReportVatLender)).Cast<ReportVatLender>().Select(d => (long)d).ToList();
                    if (request.LenderID == (int)StatusCommon.Default)
                        _transactionTab.WhereClause(x => lstLenderReq.Contains((long)x.LenderID));
                    else
                        _transactionTab.WhereClause(x => x.ShopID == request.LenderID);
                    var lstTrans = _transactionTab.WhereClause(x => x.TotalMoney != 0 && (x.CreateDate >= fromDate && x.CreateDate < toDate)).Query();
                    int totalCount = lstTrans.Count();
                    if (totalCount > 0)
                    {
                        lstTrans = lstTrans.OrderByDescending(x => x.CreateDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                        var lstCusID = new List<long>();
                        var lstLoanID = new List<long>();
                        foreach (var item in lstTrans)
                        {
                            lstCusID.Add((long)item.CustomerID);
                            lstLoanID.Add((long)item.LoanID);
                        }
                        var lstCustomer = _customerTab.WhereClause(x => lstCusID.Contains(x.CustomerID)).Query();
                        var lstLoan = _loanTab.WhereClause(x => lstLoanID.Contains(x.LoanID)).Query();
                        var dictCustomerInfos = _customerTab.WhereClause(x => lstCusID.Contains(x.CustomerID)).Query().ToDictionary(x => x.CustomerID, x => x);
                        var dictLoanInfos = _loanTab.WhereClause(x => lstLoanID.Contains(x.LoanID)).Query().ToDictionary(x => x.LoanID, x => x);
                        int i = 0;
                        foreach (var item in lstTrans)
                        {
                            i++;
                            var objCustomer = dictCustomerInfos.GetValueOrDefault(item.CustomerID ?? 0);
                            var objLoan = dictLoanInfos.GetValueOrDefault(item.LoanID ?? 0);

                            var feeNotVat = Convert.ToInt64(Math.Round(item.TotalMoney / Constants.ReportConstants.PercentVat, 3));
                            result.Add(new ReportVatItem
                            {
                                Denominator = Constants.ReportConstants.Denominator,
                                Notation = Constants.ReportConstants.NotationVatGcash,
                                TaxPercentage = Constants.ReportConstants.TaxPercentage,
                                TypePayment = Constants.ReportConstants.TypePayment,
                                OrderNumber = Constants.ReportConstants.OrderNumber + i,
                                CommodityName = CommodityName((int)item.MoneyType),

                                ContactCode = objLoan == null ? "" : objLoan.ContactCode,
                                CustomerName = objCustomer == null ? "" : objCustomer.FullName,
                                Address = objCustomer == null ? "" : objCustomer.Address,
                                TransactionDate = item.CreateDate,
                                FeeNotVat = feeNotVat,
                                Vat = item.TotalMoney - feeNotVat,
                                TotalMoney = item.TotalMoney,
                                TypeTransaction = ((Transaction_Action)item.ActionID).GetDescription()
                            });
                        }
                        response.Total = totalCount;
                        response.Data = result;
                    }
                    response.SetSucces();
                    return response;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"ReportVatLenderQueryHandler|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        private string CommodityName(int moneyType)
        {
            switch (moneyType)
            {
                //  giao dịch thu / hủy tiền lãi, 
                case (int)Transaction_TypeMoney.Interest:
                    return Constants.ReportConstants.CommodityNameInterest;
                // giao dịch thu / hủy tiền dịch vụ
                case (int)Transaction_TypeMoney.Service: return Constants.ReportConstants.CommodityNameService;
                // giao dịch thu / hủy tiền phạt
                case (int)Transaction_TypeMoney.FineLate: return Constants.ReportConstants.CommodityNameInterest;
                // giao dịch thu / hủy tiền phạt
                case (int)Transaction_TypeMoney.FineInterestLate: return Constants.ReportConstants.CommodityNameInterest;
                //giao dịch thu / hủy tiền phạt
                case (int)Transaction_TypeMoney.FineOriginal: return Constants.ReportConstants.CommodityNameInterest;
                // không rõ moneyType
                default: return string.Empty;
            }
        }
    }
}
