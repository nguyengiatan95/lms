﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.LoanService.Helper;
using LMS.LoanServiceApi.Constants;
using LMS.LoanServiceApi.Domain.Models.Report;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.Report
{
    public class ReportVatGCashQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long GcashID { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class ReportVatGCashQueryHandler : IRequestHandler<ReportVatGCashQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        ILogger<ReportVatGCashQueryHandler> _logger;
        CommonLoanServiceApi _commonLoanServiceApi;
        public ReportVatGCashQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            ILogger<ReportVatGCashQueryHandler> logger)
        {
            _transactionTab = transactionTab;
            _customerTab = customerTab;
            _loanTab = loanTab;
            _lenderTab = lenderTab;
            _logger = logger;
            _commonLoanServiceApi = new CommonLoanServiceApi();
        }
        public async Task<ResponseActionResult> Handle(ReportVatGCashQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                return await Task.Run(() =>
                {
                    var result = new List<ReportVatHubItem>();
                    var fromDate = DateTime.ParseExact(request.FromDate, _commonLoanServiceApi.DateTimeDDMMYYYY, CultureInfo.InvariantCulture);
                    var toDate = DateTime.ParseExact(request.ToDate, _commonLoanServiceApi.DateTimeDDMMYYYY, CultureInfo.InvariantCulture);
                    toDate = toDate.AddDays(1);
                    if ((toDate.Date.Subtract(fromDate.Date)).Days > ReportConstants.MaxDayReport)
                    {
                        response.Message = MessageConstant.MaxDateReport + ReportConstants.MaxDayReport;
                        return response;
                    }
                    List<int> lstTypeMoney = new List<int>() {
                        (int)Transaction_TypeMoney.Interest,
                        (int)Transaction_TypeMoney.Service,
                        (int)Transaction_TypeMoney.FineLate,
                        (int)Transaction_TypeMoney.FineInterestLate,
                        (int)Transaction_TypeMoney.FineOriginal
                    };
                    var lstHubWithTima = _lenderTab.WhereClause(x => x.IsHub == (int)Lender_IsHub.Hub).SelectColumns(x => x.LenderID).Query().Select(x => x.LenderID).ToList();
                    _transactionTab.WhereClause(x => lstHubWithTima.Contains((long)x.ShopID));
                    if (request.GcashID != (int)StatusCommon.Default)
                    {
                        _transactionTab.WhereClause(x => x.LenderID == request.GcashID);
                    }
                    else
                    {
                        var lstGcash = _lenderTab.WhereClause(x => x.IsGcash == (int)Lender_IsGcash.Gcash).SelectColumns(x => x.LenderID).Query().Select(x => x.LenderID).ToList();
                        _transactionTab.WhereClause(x => lstGcash.Contains((long)x.LenderID));
                    }

                    _transactionTab.WhereClause(x => x.TotalMoney != 0 && lstTypeMoney.Contains((int)x.MoneyType) && (x.CreateDate >= fromDate && x.CreateDate < toDate));
                    var lstTrans = _transactionTab.Query();
                    int totalCount = lstTrans.Count();
                    if (totalCount > 0)
                    {
                        lstTrans = lstTrans.OrderByDescending(x => x.CreateDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                        var lstCusID = new List<long>();
                        var lstLoanID = new List<long>();
                        var lstShopID = new List<long>();
                        foreach (var item in lstTrans)
                        {
                            lstCusID.Add((long)item.CustomerID);
                            lstLoanID.Add((long)item.LoanID);
                            lstShopID.Add(item.ShopID);
                        }
                        var dictCustomerInfos = _customerTab.WhereClause(x => lstCusID.Contains(x.CustomerID)).Query().ToDictionary(x => x.CustomerID, x => x);
                        var dictLoanInfos = _loanTab.WhereClause(x => lstLoanID.Contains(x.LoanID)).Query().ToDictionary(x => x.LoanID, x => x);
                        var dictHubInfos = _lenderTab.WhereClause(x => lstShopID.Contains(x.LenderID)).Query().ToDictionary(x => x.LenderID, x => x);
                        int i = 0;
                        foreach (var item in lstTrans)
                        {
                            i++;
                            var objCustomer = dictCustomerInfos.GetValueOrDefault(item.CustomerID ?? 0);
                            var objLoan = dictLoanInfos.GetValueOrDefault(item.LoanID ?? 0);
                            var objHub = dictHubInfos.GetValueOrDefault(item.ShopID);
                            var feeNotVat = Math.Round(item.TotalMoney / Constants.ReportConstants.PercentVat, 3);
                            string commodityName = Constants.ReportConstants.CommodityName;
                            if (item.CreateDate < Constants.ReportConstants.DateCommodityName)
                                commodityName = Constants.ReportConstants.CommodityNameOld;

                            result.Add(new ReportVatHubItem
                            {
                                Denominator = Constants.ReportConstants.DenominatorHub,
                                Notation = Constants.ReportConstants.NotationVatGcash,
                                TaxPercentage = Constants.ReportConstants.TaxPercentage,
                                TypePayment = Constants.ReportConstants.TypePayment,
                                OrderNumber = Constants.ReportConstants.OrderNumber + i,
                                HubName = objHub == null ? "" : objHub.FullName,
                                CommodityName = commodityName,
                                ContactCode = objLoan == null ? "" : objLoan.ContactCode,
                                CustomerName = objCustomer == null ? "" : objCustomer.FullName,
                                Address = objCustomer == null ? "" : objCustomer.Address,
                                TransactionDate = item.CreateDate,
                                FeeNotVat = feeNotVat,
                                Vat = item.TotalMoney - feeNotVat,
                                TotalMoney = item.TotalMoney,
                                TypeTransaction = ((Transaction_Action)item.ActionID).GetDescription()
                            });

                        }
                        response.Total = totalCount;
                        response.Data = result;
                    }
                    response.SetSucces();
                    return response;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"ReportVatGCashQueryHandler|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
