﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.LoanService.Helper;
using LMS.LoanServiceApi.Domain.Models.Report;
using LMS.LoanServiceApi.Domain.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.Report
{
    public class ReportVatQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class ReportVatQueryHandler : IRequestHandler<ReportVatQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        ILogger<ReportVatQueryHandler> _logger;
        CommonLoanServiceApi _commonLoanServiceApi;
        public ReportVatQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            ILogger<ReportVatQueryHandler> logger)
        {
            _transactionTab = transactionTab;
            _customerTab = customerTab;
            _loanTab = loanTab;
            _logger = logger;
            _commonLoanServiceApi = new CommonLoanServiceApi();
        }
        public async Task<ResponseActionResult> Handle(ReportVatQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                return await Task.Run(() =>
                {
                    var result = new List<ReportVatItem>();
                    var fromDate = DateTime.ParseExact(request.FromDate, _commonLoanServiceApi.DateTimeDDMMYYYY, CultureInfo.InvariantCulture);
                    var toDate = DateTime.ParseExact(request.ToDate, _commonLoanServiceApi.DateTimeDDMMYYYY, CultureInfo.InvariantCulture);
                    toDate = toDate.AddDays(1);
                    var lstTrans = _transactionTab.WhereClause(x => x.ShopID == (int)ShopId.Tima &&
                                                               x.MoneyType == (int)Transaction_TypeMoney.Consultant &&
                                                              (x.CreateDate >= fromDate && x.CreateDate < toDate)).Query();
                    int totalCount = lstTrans.Count();
                    if (totalCount > 0)
                    {
                        lstTrans = lstTrans.OrderByDescending(x => x.CreateDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                        var lstCusID = new List<long>();
                        var lstLoanID = new List<long>();
                        foreach (var item in lstTrans)
                        {
                            lstCusID.Add((long)item.CustomerID);
                            lstLoanID.Add((long)item.LoanID);
                        }
                        var dictCustomerInfos = _customerTab.WhereClause(x => lstCusID.Contains(x.CustomerID)).Query().ToDictionary(x => x.CustomerID, x => x);
                        var dictLoanInfos = _loanTab.WhereClause(x => lstLoanID.Contains(x.LoanID)).Query().ToDictionary(x => x.LoanID, x => x);
                        int i = 0;
                        foreach (var item in lstTrans)
                        {
                            i++;
                            var objCustomer = dictCustomerInfos.GetValueOrDefault(item.CustomerID ?? 0);
                            var objLoan = dictLoanInfos.GetValueOrDefault(item.LoanID ?? 0);

                            var feeNotVat = Math.Round(item.TotalMoney / Constants.ReportConstants.PercentVat, 3);
                            string commodityName = Constants.ReportConstants.CommodityName;
                            if (item.CreateDate < Constants.ReportConstants.DateCommodityName)
                                commodityName = Constants.ReportConstants.CommodityNameOld;

                            result.Add(new ReportVatItem
                            {
                                Denominator = Constants.ReportConstants.Denominator,
                                Notation = Constants.ReportConstants.Notation,
                                TaxPercentage = Constants.ReportConstants.TaxPercentage,
                                TypePayment = Constants.ReportConstants.TypePayment,
                                OrderNumber = Constants.ReportConstants.OrderNumber + i,
                                CommodityName = commodityName,
                                ContactCode = objLoan == null ? "" : objLoan.ContactCode,
                                CustomerName = objCustomer == null ? "" : objCustomer.FullName,
                                Address = objCustomer == null ? "" : objCustomer.Address,
                                TransactionDate = item.CreateDate,
                                FeeNotVat = feeNotVat,
                                Vat = item.TotalMoney - feeNotVat,
                                TotalMoney = item.TotalMoney,
                                TypeTransaction = ((Transaction_Action)item.ActionID).GetDescription()
                            });
                        }
                        response.Total = totalCount;
                        response.Data = result;
                    }
                    response.SetSucces();
                    return response;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"ReportVatQueryHandler|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
