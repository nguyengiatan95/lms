﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.CustomerService;
using LMS.LoanService.Helper;
using LMS.LoanServiceApi.Domain.Models.Report;
using LMS.LoanServiceApi.Domain.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.Report
{
    public class ReportExtraMoneyCustomerQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public ReportExtraMoneyCustomerReq ReportExtraMoneyCustomerInforReq { get; set; }
    }
    public class ReportExtraMoneyCustomerQueryHandler : IRequestHandler<ReportExtraMoneyCustomerQuery, LMS.Common.Constants.ResponseActionResult>
    {
        RestClients.ICustomerService _customerService;
        ILogger<ReportExtraMoneyCustomerQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public ReportExtraMoneyCustomerQueryHandler(
            RestClients.ICustomerService customerService,
            ILogger<ReportExtraMoneyCustomerQueryHandler> logger)
        {
            _logger = logger;
            _customerService = customerService;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(ReportExtraMoneyCustomerQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                int totalCount = 0;
                var lstData = await _customerService.ReportExtraMoneyCustomer(request.ReportExtraMoneyCustomerInforReq);
                if (lstData != null && lstData.Count() > 0)
                {
                    totalCount = lstData[0].TotalCount;
                }
                response.SetSucces();
                response.Total = totalCount;
                response.Data = lstData;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ReportExtraMoneyCustomerQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
