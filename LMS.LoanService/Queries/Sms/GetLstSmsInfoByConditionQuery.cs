﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Models.SMS;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.Sms
{
    public class GetLstSmsInfoByConditionQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public RequestSmsItemViewModel SmsItemViewRequest { get; set; }
    }
    public class GetLstLoanInfoByConditionQueryHandler : IRequestHandler<GetLstSmsInfoByConditionQuery, LMS.Common.Constants.ResponseActionResult>
    {
        Services.ISmsAnalyticsManager _smsAnalyticsManager;
        public GetLstLoanInfoByConditionQueryHandler(Services.ISmsAnalyticsManager smsAnalyticsManager)
        {
            _smsAnalyticsManager = smsAnalyticsManager;
        }
        public async Task<ResponseActionResult> Handle(GetLstSmsInfoByConditionQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var lstSmsItemViewData = await _smsAnalyticsManager.GetLstSmsInfoByCondition(request.SmsItemViewRequest);
            ResponseForDataTable dataTable = new ResponseForDataTable
            {
                RecordsTotal = request.SmsItemViewRequest.TotalRow,
                Data = lstSmsItemViewData,
                RecordsFiltered = request.SmsItemViewRequest.TotalRow,
                Draw = 1
            };
            response.SetSucces();
            response.Data = dataTable;
            return response;
        }
    }
}
