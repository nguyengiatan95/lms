﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace LMS.LoanServiceApi.Queries.BankCardOfShop
{
    public class GetBankCardOfShopByShopIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long ShopID { get; set; }
    }
    public class GetBankCardOfShopByShopIDQueryHandler : IRequestHandler<GetBankCardOfShopByShopIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCardOfShop> _bankCardOfShopTab;
        ILogger<GetBankCardOfShopByShopIDQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetBankCardOfShopByShopIDQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCardOfShop> bankCardOfShopTab,
            ILogger<GetBankCardOfShopByShopIDQueryHandler> logger)
        {
            _bankCardOfShopTab = bankCardOfShopTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetBankCardOfShopByShopIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                return await Task.Run(() =>
                {
                    var lstBankCardOfShop = _bankCardOfShopTab.WhereClause(x => x.ShopID == request.ShopID).Query();
                    if (lstBankCardOfShop != null)
                    {
                        response.Data = lstBankCardOfShop;
                    }
                    response.SetSucces();
                    return response;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetBankCardOfShopByShopIDQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
