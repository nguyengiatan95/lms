﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.BankCardOfShop
{
    public class GetShopOfBankCardIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long BankCardID { get; set; }
    }
    public class GetShopOfBankCardIDQueryHandler : IRequestHandler<GetShopOfBankCardIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCardOfShop> _bankCardOfShopTab;
        ILogger<GetShopOfBankCardIDQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetShopOfBankCardIDQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCardOfShop> bankCardOfShopTab,
            ILogger<GetShopOfBankCardIDQueryHandler> logger)
        {
            _bankCardOfShopTab = bankCardOfShopTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetShopOfBankCardIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                return await Task.Run(() =>
                {
                    var lstShopOfBankCard = _bankCardOfShopTab.WhereClause(x => x.BankCardID == request.BankCardID).Query();
                    if (lstShopOfBankCard != null)
                    {
                        response.Data = lstShopOfBankCard;
                    }
                    response.SetSucces();
                    return response;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetShopOfBankCardIDQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
