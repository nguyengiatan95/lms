﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.Loan
{
    public class GetLstPaymentScheduleByLoanIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
    }
    public class GetLstPaymentScheduleByLoanIDQueryHandler : IRequestHandler<GetLstPaymentScheduleByLoanIDQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        RestClients.IPaymentScheduleService _paymentScheduleService;
        ILogger<GetLstPaymentScheduleByLoanIDQueryHandler> _logger;
        public GetLstPaymentScheduleByLoanIDQueryHandler(Services.ILoanManager loanManager,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            RestClients.IPaymentScheduleService paymentScheduleService,
            ILogger<GetLstPaymentScheduleByLoanIDQueryHandler> logger)
        {
            _loanTab = loanTab;
            _paymentScheduleService = paymentScheduleService;
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(GetLstPaymentScheduleByLoanIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstLoanInfos = _loanTab.WhereClause(x => x.LoanID == request.LoanID).Query();
                if (lstLoanInfos == null || !lstLoanInfos.Any())
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                var loanDetail = lstLoanInfos.First();
                List<LMS.Entites.Dtos.PaymentServices.PaymentScheduleModel> lstData = new List<Entites.Dtos.PaymentServices.PaymentScheduleModel>();
                response.Data = lstData;
                var lstPaymentSchedulInfos = await _paymentScheduleService.GetLstPaymentByLoanID(request.LoanID);
                if (lstPaymentSchedulInfos != null)
                {
                    lstPaymentSchedulInfos = lstPaymentSchedulInfos.OrderBy(x => x.PayDate);
                    if (loanDetail.Status != (int)Loan_Status.Lending)
                    {
                        lstPaymentSchedulInfos = lstPaymentSchedulInfos.Where(x => x.PayMoneyInterest > 0 || x.PayMoneyOriginal > 0 || x.PayMoneyConsultant > 0).ToList();
                    }
                    int flagButton = 0;

                    foreach (var item in lstPaymentSchedulInfos)
                    {

                        item.TotalCustomerPaid = item.PayMoneyOriginal + item.PayMoneyInterest + item.PayMoneyService + item.PayMoneyConsultant + item.PayMoneyFineLate + item.PayMoneyFineInterestLate;
                        item.TotalCustomerNeedPay = item.MoneyOriginal + item.MoneyInterest + item.MoneyService + item.MoneyConsultant + item.MoneyFineLate + item.MoneyFineInterestLate - item.TotalCustomerPaid;
                        long totalMoneyRemain = item.MoneyOriginal + item.MoneyInterest + item.MoneyService + item.MoneyConsultant - (item.PayMoneyOriginal + item.PayMoneyInterest + item.PayMoneyService + item.PayMoneyConsultant);
                        if (item.IsComplete == 0 && item.PayDate.AddDays(-3) <= DateTime.Now && flagButton == 0 && totalMoneyRemain > TimaSettingConstant.MoneyCanChangeDPD)
                        {
                            item.ShowButtonPayment = true;
                            flagButton = 1;
                        }
                        item.CountDay = (int)(item.ToDate - item.FromDate).TotalDays + 1;
                    }
                    response.Data = lstPaymentSchedulInfos;
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLstPaymentScheduleByLoanIDQueryHandler|loanID={request.LoanID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
