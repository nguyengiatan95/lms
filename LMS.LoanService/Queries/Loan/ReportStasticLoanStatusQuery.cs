﻿using LMS.Common.Constants;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.Loan
{
    public class ReportStasticLoanStatusQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long ShopID { get; set; }
    }
    public class ReportStasticLoanStatusQueryHandler : IRequestHandler<ReportStasticLoanStatusQuery, LMS.Common.Constants.ResponseActionResult>
    {
        Services.IReportManager _reportManager;
        public ReportStasticLoanStatusQueryHandler(Services.IReportManager reportManager)
        {
            _reportManager = reportManager;
        }
        public async Task<ResponseActionResult> Handle(ReportStasticLoanStatusQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                var reportDetail = _reportManager.GetReportStasticsLoanBorrowModel(request.ShopID);
                if (response != null)
                {
                    response.SetSucces();
                    response.Data = reportDetail;
                }
                return response;
            });
        }
    }
}
