﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Models.Loan;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries
{
    public class GetLstLoanInfoByConditionQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public RequestLoanItemViewModel LoanItemViewRequest { get; set; }
    }
    public class GetLstLoanInfoByConditionQueryHandler : IRequestHandler<GetLstLoanInfoByConditionQuery, LMS.Common.Constants.ResponseActionResult>
    {
        Services.ILoanManager _loanManager;
        ILogger<GetLstLoanInfoByConditionQueryHandler> _logger;
        public GetLstLoanInfoByConditionQueryHandler(Services.ILoanManager loanManager, ILogger<GetLstLoanInfoByConditionQueryHandler> logger)
        {
            _loanManager = loanManager;
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(GetLstLoanInfoByConditionQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var lstLoanItemView = await _loanManager.GetLoanItemViewModelsByCondition(request.LoanItemViewRequest);
            ResponseForDataTable dataTable = new ResponseForDataTable
            {
                RecordsTotal = request.LoanItemViewRequest.TotalRow,
                Data = lstLoanItemView,
                RecordsFiltered = request.LoanItemViewRequest.TotalRow,
                Draw = 1
            };
            response.SetSucces();
            response.Data = dataTable;
            return response;
        }
    }
}
