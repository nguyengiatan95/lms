﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.LoanServiceApi.Domain.Models.Insurance;
using LMS.LoanServiceApi.RestClients;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.Loan
{
    public class GetLoanCreditDetailLosByLoanIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
    }
    public class GetLoanCreditDetailLosByLoanIDQueryHandler : IRequestHandler<GetLoanCreditDetailLosByLoanIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomerBank> _customerBankTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionLoanTab;
        Services.ILOSManager _lOSManager;
        Services.ILoanManager _loanManager;

        ILogger<GetLoanCreditDetailLosByLoanIDQueryHandler> _logger;
        Common.Helper.Utils _common;
        public GetLoanCreditDetailLosByLoanIDQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomerBank> customerBankTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transaciontLoanTab,
            Services.ILOSManager lOSManager,
            Services.ILoanManager loanManager,
            ILogger<GetLoanCreditDetailLosByLoanIDQueryHandler> logger
            )
        {
            _loanTab = loanTab;
            _customerBankTab = customerBankTab;
            _customerTab = customerTab;
            _lOSManager = lOSManager;
            _paymentScheduleTab = paymentScheduleTab;
            _invoiceTab = invoiceTab;
            _transactionLoanTab = transaciontLoanTab;
            _loanManager = loanManager;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetLoanCreditDetailLosByLoanIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var currentDate = DateTime.Now;
                var firtDateOfMonth = new DateTime(currentDate.Year, currentDate.Month, 1);
                var firtDateOfNextMonth = firtDateOfMonth.AddMonths(1);
                var objLoan = _loanTab.WhereClause(x => x.LoanID == request.LoanID).Query().FirstOrDefault();
                if (objLoan == null || objLoan.LoanCreditIDOfPartner == 0)
                {
                    response.Message = MessageConstant.RequireLoanID;
                    return response;
                }
                var lstPaymentScheduleTask = _paymentScheduleTab.WhereClause(x => x.LoanID == request.LoanID)
                                                                          .WhereClause(x => x.Status == (int)PaymentSchedule_Status.Use)
                                                                          .QueryAsync();
                List<int> lstInvoiceSubTypeCustomer = new List<int>
                {
                    (int)GroupInvoiceType.ReceiptCustomer,
                    (int)GroupInvoiceType.ReceiptOnBehalfShop
                };
                var invoiceCustomerTask = _invoiceTab.SetGetTop(1).WhereClause(x => x.CustomerID == objLoan.CustomerID)
                                                     .WhereClause(x => lstInvoiceSubTypeCustomer.Contains(x.InvoiceSubType))
                                                     .WhereClause(x => x.Status == (int)Invoice_Status.Confirmed)
                                                     .OrderByDescending(x => x.InvoiceID)
                                                     .QueryAsync();
                var txnLoanTask = _transactionLoanTab.SelectColumns(x => x.TotalMoney)
                                                     .WhereClause(x => x.LoanID == request.LoanID && x.ActionID != (int)Transaction_Action.ChoVay)
                                                     .QueryAsync();
                var resultLoanLosTask = _lOSManager.GetLoanCreditDetail((long)objLoan.LoanCreditIDOfPartner, (int)CalculateMoneyInsuranceLOS.No);
                var customerInfosTask = _customerTab.WhereClause(x => x.CustomerID == objLoan.CustomerID).QueryAsync();
                var lstCustomerBankTask = _customerBankTab.WhereClause(x => x.CustomerID == objLoan.CustomerID && x.StatusVa == (int)CustomerBank_StatusVA.Success).QueryAsync();
                await Task.WhenAll(resultLoanLosTask);

                var loanLosDetail = resultLoanLosTask.Result;
                Entites.Dtos.LoanServices.MoneyNeedCloseLoan moneyNeedCloseLoan = null;
                if (loanLosDetail != null)
                {
                    await Task.WhenAll(customerInfosTask, lstCustomerBankTask, lstPaymentScheduleTask, invoiceCustomerTask, txnLoanTask);
                    if (objLoan.ToDate.Date < currentDate.Date)
                    {
                        moneyNeedCloseLoan = await _loanManager.GetMoneyCloseLoan(request.LoanID, currentDate);
                    }
                    // thông tin lấy từ LMS
                    loanLosDetail.ContactCodeLMS = objLoan.ContactCode;
                    loanLosDetail.StatusLMS = ((Loan_Status)objLoan.Status).GetDescription();
                    loanLosDetail.FromDateLMS = objLoan.FromDate;
                    loanLosDetail.ToDateLMS = objLoan.ToDate;
                    loanLosDetail.CustomerId = (long)objLoan.CustomerID;
                    loanLosDetail.ContactCode = $"{TimaSettingConstant.PatternContractCodeLOS}{loanLosDetail.LoanBriefId}".ToUpper();
                    loanLosDetail.MoneyFineLate = objLoan.MoneyFineLate;

                    var customerInfos = customerInfosTask.Result;
                    var customerDetail = customerInfos.FirstOrDefault();
                    var lstCustomerBank = lstCustomerBankTask.Result;
                    var paymentScheduleInfos = lstPaymentScheduleTask.Result;
                    var invoiceCustomer = invoiceCustomerTask.Result;
                    if (lstCustomerBank != null && lstCustomerBank.Count() > 0)
                    {
                        foreach (var item in lstCustomerBank)
                        {
                            loanLosDetail.LstBankCustomer.Add(new Entites.Dtos.LOSServices.BankCustomer
                            {
                                AccountNo = item.AccountNo,
                                AccountName = item.AccountName,
                                BankCode = item.BankCode,
                                BankName = item.BankName
                            });
                        }
                    }
                    loanLosDetail.Phone = customerDetail?.Phone;
                    loanLosDetail.FullName = customerDetail?.FullName;
                    loanLosDetail.CustomerTotalMoney = customerDetail?.TotalMoney ?? 0;
                    // lịch thanh toán
                    // đơn không có lịch lỗi data, sẽ check sau
                    if (paymentScheduleInfos != null)
                    {
                        foreach (var p in paymentScheduleInfos)
                        {
                            if (p.PayDate > firtDateOfNextMonth)
                            {
                                break;
                            }
                            if(p.IsComplete == (int)PaymentSchedule_IsComplete.Waiting)
                            {
                                loanLosDetail.MoneyOrginal += p.MoneyOriginal - p.PayMoneyOriginal;
                                loanLosDetail.MoneyInterest += p.MoneyInterest - p.PayMoneyInterest;
                                loanLosDetail.MoneyConsultant += p.MoneyConsultant - p.PayMoneyConsultant;
                                loanLosDetail.MoneyService += p.MoneyService - p.PayMoneyService;
                            }
                        }
                        loanLosDetail.TotalMoneyNeedPay = loanLosDetail.MoneyOrginal + loanLosDetail.MoneyInterest + loanLosDetail.MoneyConsultant + loanLosDetail.MoneyService;
                    }
                    loanLosDetail.TotalMoneyLoanPaid = txnLoanTask.Result.Sum(x => x.TotalMoney);
                    if (moneyNeedCloseLoan != null)
                    {
                        loanLosDetail.TotalMoneyNeedPay = moneyNeedCloseLoan.TotalMoneyCloseLoan;
                        loanLosDetail.MoneyOrginal = moneyNeedCloseLoan.MoneyOriginal;
                        loanLosDetail.MoneyInterest = moneyNeedCloseLoan.MoneyInterest;
                        loanLosDetail.MoneyConsultant = moneyNeedCloseLoan.MoneyConsultant;
                        loanLosDetail.MoneyService = moneyNeedCloseLoan.MoneyService;
                    }
                    if (invoiceCustomer != null && invoiceCustomer.Any())
                    {
                        loanLosDetail.LatestDateTopup = invoiceCustomer.FirstOrDefault().TransactionDate;
                    }

                }
                response.SetSucces();
                response.Data = loanLosDetail;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLoanCreditDetailLosByLoanIDQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
