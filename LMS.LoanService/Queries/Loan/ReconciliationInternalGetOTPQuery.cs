﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.APIAutoDisbursement;
using LMS.LoanServiceApi.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.Loan
{
    public class ReconciliationInternalGetOTPQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string TraceIndentifier { get; set; }
    }
    public class ReconciliationInternalGetOTPQueryHandler : IRequestHandler<ReconciliationInternalGetOTPQuery, LMS.Common.Constants.ResponseActionResult>
    {
        IPaymentGatewayService _paymentGatewayService;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCallApi> _logCallApiTab;
        ILogger<ReconciliationInternalGetOTPQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public ReconciliationInternalGetOTPQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCallApi> logCallApiTab,
            ILogger<ReconciliationInternalGetOTPQueryHandler> logger,
            IPaymentGatewayService paymentGatewayService)
        {
            _logCallApiTab = logCallApiTab;
            _paymentGatewayService = paymentGatewayService;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(ReconciliationInternalGetOTPQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    var lstLogCallApi = _logCallApiTab.WhereClause(x => x.TraceIndentifierRequest == request.TraceIndentifier || x.TraceIndentifierResponse == request.TraceIndentifier).Query();
                    response.SetSucces();
                    LMS.Entites.Dtos.LoanServices.LogCallApiGetOTPModel detailGetOtp = new LMS.Entites.Dtos.LoanServices.LogCallApiGetOTPModel
                    {
                        Status = (int)LoanService.Helper.ReconciliationInternalGetOTPStatus.Fail
                    };
                    response.Data = detailGetOtp;
                    if (lstLogCallApi == null || !lstLogCallApi.Any())
                    {
                        response.Message = LMS.Common.Constants.MessageConstant.LoanCreditNotGetTransionRequestOTPFromAI;
                        return response;
                    }
                    lstLogCallApi = lstLogCallApi.OrderByDescending(x => x.LogCallApiID);
                    if (lstLogCallApi.Count() == 1)
                    {
                        // kiểm tra thời gian gọi api có quá time chờ ko
                        detailGetOtp.Status = (int)LoanService.Helper.ReconciliationInternalGetOTPStatus.Retry;
                        if (Convert.ToInt32(DateTime.Now.Subtract(lstLogCallApi.First().ModifyDate).TotalMinutes) > LoanService.Helper.CommonLoanServiceApi.TimeWaitingReceivedOTP)
                        {
                            detailGetOtp.Status = (int)LoanService.Helper.ReconciliationInternalGetOTPStatus.Fail;
                            response.Message = LMS.Common.Constants.MessageConstant.LoanCreditNotGetTransionRequestOTPFromAI;
                        }
                        return response;
                    }
                    else if (lstLogCallApi.Count() == 2) // đã sort ở trên lấy thằng đầu tiền từ API AI trả về
                    {
                        var detailCallApiAI = lstLogCallApi.First();
                        switch ((LogCallApi_StatusCallApi)detailCallApiAI.Status)
                        {
                            case LogCallApi_StatusCallApi.CallApi:
                            case LogCallApi_StatusCallApi.Retry:
                                detailGetOtp.Status = (int)LoanService.Helper.ReconciliationInternalGetOTPStatus.Retry;
                                break;
                            case LogCallApi_StatusCallApi.Success:
                                detailGetOtp.Status = (int)LoanService.Helper.ReconciliationInternalGetOTPStatus.Success;
                                break;
                            default:
                                var detailCreateTranstion = _common.ConvertJSonToObjectV2<CreateTransactionResponse>(detailCallApiAI.ResultStringCallApi);
                                response.Message = detailCreateTranstion.Messages;
                                detailGetOtp.Status = (int)LoanService.Helper.ReconciliationInternalGetOTPStatus.Fail;
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"GetDetailLogCallApiByTraceIndentifierRequest|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                }
                return response;
            });
        }
    }
}
