﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.Loan
{
    public class GetLstLoanByFilterCustomerQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string CustomerKeySearch { get; set; }
    }
    public class GetLstLoanByFilterCustomerQueryHandler : IRequestHandler<GetLstLoanByFilterCustomerQuery, LMS.Common.Constants.ResponseActionResult>
    {
        Services.ILoanManager _loanManager;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        ILogger<GetLstLoanByFilterCustomerQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetLstLoanByFilterCustomerQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            Services.ILoanManager loanManager,
            ILogger<GetLstLoanByFilterCustomerQueryHandler> logger)
        {
            _customerTab = customerTab;
            _loanManager = loanManager;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetLstLoanByFilterCustomerQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            List<LMS.Entites.Dtos.LoanServices.LoanItemViewModel> lstResult = new List<Entites.Dtos.LoanServices.LoanItemViewModel>();
            ResponseForDataTable dataTable = new ResponseForDataTable
            {
                RecordsTotal = 0,
                Data = lstResult,
                RecordsFiltered = 0,
                Draw = 1
            };
            response.SetSucces();
            response.Data = dataTable;
            List<long> lstCustomerID = new List<long>();
            try
            {
                Domain.Models.Loan.RequestLoanItemViewModel requestSearchLoan = new Domain.Models.Loan.RequestLoanItemViewModel
                {
                    PageIndex = 0,
                    PageSize = TimaSettingConstant.MaxItemQuery
                };
                if (string.IsNullOrEmpty(request.CustomerKeySearch))
                {
                    return response;
                }
                if (long.TryParse(request.CustomerKeySearch, out long contracCodeID) && request.CustomerKeySearch.Length < TimaSettingConstant.MaxLengthPhone - 1)
                {
                    requestSearchLoan.LoanCode = $"{TimaSettingConstant.PrefixContractCode}{contracCodeID}";
                }
                else if (!Regex.Match(request.CustomerKeySearch, TimaSettingConstant.PatternContractCode, RegexOptions.IgnoreCase).Success)
                {
                    if (request.CustomerKeySearch.Length == TimaSettingConstant.MaxLengthPhone && Regex.Match(request.CustomerKeySearch, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success)
                    {
                        _customerTab.WhereClause(x => x.Phone == request.CustomerKeySearch);
                    }
                    else if (Regex.Match(request.CustomerKeySearch, TimaSettingConstant.PatternNumberCard, RegexOptions.IgnoreCase).Success)
                    {
                        _customerTab.WhereClause(x => x.NumberCard == request.CustomerKeySearch);
                    }
                    else
                    {
                        _customerTab.WhereClause(x => x.FullName == request.CustomerKeySearch);
                    }
                    lstCustomerID = (await _customerTab.SelectColumns(x => x.CustomerID).QueryAsync()).Select(x => x.CustomerID).ToList();
                    if (lstCustomerID.Count == 0)
                    {
                        return response;
                    }
                    requestSearchLoan.LstCustomerID = lstCustomerID;
                }
                else
                {
                    requestSearchLoan.LoanCode = request.CustomerKeySearch;
                }
                lstResult = (await _loanManager.GetLoanItemViewModelsByCondition(requestSearchLoan)).ToList();
                dataTable.Data = lstResult;
                dataTable.RecordsTotal = requestSearchLoan.TotalRow;
                dataTable.RecordsFiltered = requestSearchLoan.TotalRow;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLstLoanByFilterCustomerQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
