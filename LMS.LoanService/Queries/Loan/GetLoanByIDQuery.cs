﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries
{
    public class GetLoanByIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int LoanID { get; set; }
    }
    public class GetLoanByIDQueryHandler : IRequestHandler<GetLoanByIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        Services.ILoanManager _loanManager;
        public GetLoanByIDQueryHandler(Services.ILoanManager loanManager)
        {
            _loanManager = loanManager;
        }
        public async Task<ResponseActionResult> Handle(GetLoanByIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var loanDetail = await _loanManager.GetDetailViewModelByID(request.LoanID);
            if (loanDetail != null)
            {
                response.SetSucces();
                response.Data = loanDetail;
            }
            return response;
        }
    }
}
