﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.Loan
{
    public class GetMoneyNeedCloseLoanByLoanIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public string CloseDate { get; set; } // dd/MM/yyyy
        public long TimaLoanID { get; set; } = 0;
    }
    public class GetMoneyNeedCloseLoanByLoanIDHandler : IRequestHandler<GetMoneyNeedCloseLoanByLoanIDQuery, ResponseActionResult>
    {
        Services.ILoanManager _loanManager;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        ILogger<GetMoneyNeedCloseLoanByLoanIDHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetMoneyNeedCloseLoanByLoanIDHandler(Services.ILoanManager loanManager,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            ILogger<GetMoneyNeedCloseLoanByLoanIDHandler> logger)
        {
            _loanManager = loanManager;
            _logger = logger;
            _loanTab = loanTab;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetMoneyNeedCloseLoanByLoanIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                if (request.TimaLoanID != (int)StatusCommon.UnActive)
                {
                    var objLoan = (await _loanTab.WhereClause(x => x.TimaLoanID == request.TimaLoanID).QueryAsync()).FirstOrDefault();
                    if (objLoan != null)
                        request.LoanID = objLoan.LoanID;
                }
                DateTime closeDate = DateTime.Now;
                if (!string.IsNullOrEmpty(request.CloseDate))
                {
                    closeDate = DateTime.ParseExact(request.CloseDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                }
                response.SetSucces();
                var moneyCloseLoan = await _loanManager.GetMoneyCloseLoan(request.LoanID, closeDate);
                if (moneyCloseLoan == null)
                {
                    response.Message = MessageConstant.LoanNotExist;
                }
                response.Data = moneyCloseLoan;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetMoneyNeedCloseLoanByLoanIDQuery_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
