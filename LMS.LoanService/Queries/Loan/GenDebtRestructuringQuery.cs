﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.PaymentServices;
using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Queries.Loan
{
    public class GenDebtRestructuringQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long TimaLoanID { get; set; }
        public long LoanID { get; set; }

        /// <summary>
        /// Kiểu tái cơ cấu nợ
        /// </summary>
        public int TypeDebtRestructuring { get; set; }

        /// <summary>
        /// Số kỳ khoanh vùng trả nợ
        /// </summary>
        public int NumberOfRepaymentPeriod { get; set; }

        //public string AppointmentDate { get; set; }
        //public int PercentDiscount { get; set; }

    }
    public class GenDebtRestructuringQueryHandler : IRequestHandler<GenDebtRestructuringQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblProductCredit> _productCreditTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.LoanUpdateMoneyFineLate> _loanUpdateMoneyFineLateTab;
        RestClients.IPaymentScheduleService _paymentScheduleService;
        ILogger<GenDebtRestructuringQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        Services.ILoanManager _loanManager;
        public GenDebtRestructuringQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblProductCredit> productCreditTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.LoanUpdateMoneyFineLate> loanUpdateMoneyFineLateTab,
            RestClients.IPaymentScheduleService paymentScheduleService,
            Services.ILoanManager loanManager,
            ILogger<GenDebtRestructuringQueryHandler> logger)
        {
            _loanTab = loanTab;
            _productCreditTab = productCreditTab;
            _loanUpdateMoneyFineLateTab = loanUpdateMoneyFineLateTab;
            _paymentScheduleService = paymentScheduleService;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _loanManager = loanManager;
        }
        public async Task<ResponseActionResult> Handle(GenDebtRestructuringQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                if (request.TimaLoanID > 0)
                {
                    _loanTab.WhereClause(x => x.TimaLoanID == request.TimaLoanID);
                }
                else
                {
                    _loanTab.WhereClause(x => x.LoanID == request.LoanID);
                }

                var lstTypeDebtRestructuring = new List<int>
                {
                    (int)PaymentSchedule_TypeDebtRestructuring.AverageConsultingFees,
                    (int)PaymentSchedule_TypeDebtRestructuring.ConsultingFeesLastPeriods
                };
                if (!lstTypeDebtRestructuring.Contains(request.TypeDebtRestructuring))
                {
                    response.Message = MessageConstant.ParameterInvalid;
                    return response;
                }
                var loanInfos = await _loanTab.QueryAsync();
                if (loanInfos == null || !loanInfos.Any())
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                var loanInfo = loanInfos.First();

                var productCreditInfo = (await _productCreditTab.WhereClause(x => x.ProductCreditID == loanInfo.ProductID).QueryAsync()).FirstOrDefault();
                if (productCreditInfo == null || productCreditInfo.ProductCreditID < 1)
                {
                    response.Message = "Hệ thống không xác định được sản phẩm của thông tin giải ngân của HD-" + loanInfo.LoanID + " để tái cấu trúc nợ";
                    _logger.LogError($"GenDebtRestructuringCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                }
                DateTime appointmentDate = DateTime.Now;
                var minDayFineLate = (int)SettingFineForProduct.MinDayFineLateSmall;
                if (productCreditInfo != null && productCreditInfo.TypeProduct == (int)Product_Type.Big)
                {
                    if (loanInfo.FromDate >= new DateTime(2020, 01, 31))
                    {
                        minDayFineLate = 0;
                    }
                }
                minDayFineLate += 1; // + 1 tính từ ngày hôm sau       
                var percentDiscount = 0;
                var userID = 0;
                var actionResult = await _paymentScheduleService.GenDebtRestructuring(loanInfo.LoanID, loanInfo.RateType, request.NumberOfRepaymentPeriod,
                    appointmentDate.ToString(TimaSettingConstant.DateTimeDayMonthYear), percentDiscount, request.TypeDebtRestructuring, userID, minDayFineLate);
                response.Message = actionResult?.Message ?? response.Message;
                if (actionResult != null && actionResult.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                {
                    response.SetSucces();
                    response.Data = _common.ConvertJSonToObjectV2<List<GenDebtRestructuringModel>>(actionResult.Data.ToString());
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GenDebtRestructuringQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
