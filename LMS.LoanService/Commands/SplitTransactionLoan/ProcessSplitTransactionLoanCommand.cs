﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.LoanServiceApi.Domain.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.SplitTransactionLoan
{
    public class ProcessSplitTransactionLoanCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class ProcessSplitTransactionLoanCommandHandler : IRequestHandler<ProcessSplitTransactionLoanCommand, LMS.Common.Constants.ResponseActionResult>
    {
        Services.ILoanManager _loanManager;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> _settingKeyTab;
        LMS.Common.Helper.Utils _common;
        ILogger<ProcessSplitTransactionLoanCommandHandler> _logger;
        public ProcessSplitTransactionLoanCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> settingKeyTab,
            Services.ILoanManager loanManager,
             ILogger<ProcessSplitTransactionLoanCommandHandler> logger)
        {
            _loanManager = loanManager;
            _settingKeyTab = settingKeyTab;
            _common = new Utils();
            _logger = logger;
        }

        public async Task<ResponseActionResult> Handle(ProcessSplitTransactionLoanCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var keySettingInfo = (await _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.SplitTransactionLoan_ProcessEveryHour).QueryAsync()).FirstOrDefault();
                if (keySettingInfo == null)
                {
                    keySettingInfo = new Domain.Tables.TblSettingKey
                    {
                        CreateDate = DateTime.Now,
                        KeyName = LMS.Common.Constants.SettingKey_KeyValue.SplitTransactionLoan_ProcessEveryHour,
                        Status = (int)SettingKey_Status.Active,
                        ModifyDate = DateTime.Now,
                        Value = _common.ConvertObjectToJSonV2(new SettingKeyValueSplitTransaction
                        {
                            LatestTransactionID = 0
                        })
                    };
                    keySettingInfo.SettingKeyID = await _settingKeyTab.InsertAsync(keySettingInfo);
                }
                if (keySettingInfo.Status == (int)SettingKey_Status.InActice)
                {
                    response.Message = MessageConstant.SettingKey_SplitTransactionLoan_ProcessEveryHour;
                    return response;
                }
                SettingKeyValueSplitTransaction settingKeyValue = new SettingKeyValueSplitTransaction();
                if (!string.IsNullOrEmpty(keySettingInfo.Value))
                {
                    try
                    {
                        settingKeyValue = _common.ConvertJSonToObjectV2<SettingKeyValueSplitTransaction>(keySettingInfo.Value);
                    }
                    catch (Exception)
                    {
                        settingKeyValue = new SettingKeyValueSplitTransaction
                        {
                            LatestTransactionID = 0
                        };
                        keySettingInfo.Value = _common.ConvertObjectToJSonV2(settingKeyValue);
                    }
                }                
                keySettingInfo.Status = (int)SettingKey_Status.InActice;
                keySettingInfo.ModifyDate = DateTime.Now;
                _ = RunUntilFinished(keySettingInfo, settingKeyValue);
                _ = await _settingKeyTab.UpdateAsync(keySettingInfo);
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSplitTransactionLoanCommandHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        private async Task RunUntilFinished(Domain.Tables.TblSettingKey keySettingInfo, SettingKeyValueSplitTransaction detailValue)
        {
            try
            {
                var actionResult = await _loanManager.ProcessSplitTransactionLoan(detailValue);
                if (!actionResult.IsContinous)
                {
                    var settingKeyValue = _common.ConvertJSonToObjectV2<SettingKeyValueSplitTransaction>(keySettingInfo.Value);
                    settingKeyValue.CountTimeHandleFinished = Convert.ToInt64(DateTime.Now.Subtract(keySettingInfo.ModifyDate).TotalMilliseconds);
                    settingKeyValue.LatestTransactionID = actionResult.LatestTransactionID;
                    settingKeyValue.IsContinous = actionResult.IsContinous;
                    keySettingInfo.Status = (int)SettingKey_Status.Active;
                    keySettingInfo.ModifyDate = DateTime.Now;
                    keySettingInfo.Value = _common.ConvertObjectToJSonV2(settingKeyValue);
                    _ = await _settingKeyTab.UpdateAsync(keySettingInfo);
                    return;
                }
                await Task.Delay(TimeSpan.FromSeconds(2));
                _ = RunUntilFinished(keySettingInfo, actionResult);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessCutOffLoanByDateDailyCommandHandler_RunUntilFinished|ex={ex.Message}-{ex.StackTrace}");
            }
        }
    }
}
