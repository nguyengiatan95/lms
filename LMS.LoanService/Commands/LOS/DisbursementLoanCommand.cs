﻿using LMS.Entites.Dtos.LOSServices;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LMS.LoanServiceApi.Constants;

namespace LMS.LoanServiceApi.Commands.LOS
{
    public class DisbursementLoanCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanBriefID { get; set; }
        public long UserID { get; set; }
    }
    public class DisbursementLoanCommandHandler : IRequestHandler<DisbursementLoanCommand, LMS.Common.Constants.ResponseActionResult>
    {
        RestClients.ILOSService _lOSService;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<DisbursementLoanCommandHandler> _logger;
        Common.Helper.Utils _common;
        public DisbursementLoanCommandHandler(
            RestClients.ILOSService lOSService,
            ILogger<DisbursementLoanCommandHandler> logger,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab
        )
        {
            _lOSService = lOSService;
            _logger = logger;
            _loanTab = loanTab;
            _userTab = userTab;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(DisbursementLoanCommand req, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    var objLoanLos = _lOSService.GetLoanCreditDetail(req.LoanBriefID).Result;
                    if (objLoanLos == null)
                    {
                        response.Message = MessageConstant.LoanNotExist;
                        return response;
                    }
                    var objUser = _userTab.WhereClause(x => x.UserID == req.UserID).Query().FirstOrDefault();
                    if (objUser == null)
                    {
                        response.Message = MessageConstant.NotIdentifiedUserID;
                        return response;
                    }
                    var objLoan = _loanTab.WhereClause(x => x.LoanCreditIDOfPartner == req.LoanBriefID).Query().FirstOrDefault();
                    if (objLoan != null)
                    {
                        var objDisbursementLoan = new DisbursementLoanReq();

                        var codeID = objLoan.ContactCode.Split("TC-");
                        objDisbursementLoan.LoanBriefId = req.LoanBriefID;
                        objDisbursementLoan.LoanId = objLoan.LoanID;
                        objDisbursementLoan.Type = (int)LOS_Enum_DisbursementType.CreateLoan;
                        objDisbursementLoan.CodeId = Int32.Parse(codeID[0]);
                        objDisbursementLoan.FeeInsuranceOfCustomer = 0;
                        objDisbursementLoan.UserName = objUser.FullName;
                        objDisbursementLoan.IsHandleException = LOSConstants.IsHandleException;
                        return _lOSService.DisbursementLoan(objDisbursementLoan).Result;
                    }
                    else
                        response.Message = MessageConstant.NotIdentifiedLoanID;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"DisbursementLoanCommandHandler|request={_common.ConvertObjectToJSonV2(req)}|ex={ex.Message}-{ex.StackTrace}");
                }
                return response;
            });
        }
    }
}
