﻿using LMS.Entites.Dtos.LOSServices;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LMS.LoanServiceApi.Constants;
namespace LMS.LoanServiceApi.Commands.LOS
{
    public class ReturnLoanCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanBriefID { get; set; }
        public int TypeID { get; set; }
        public string Note { get; set; }
        public long UserID { get; set; }
    }
    public class ReturnLoanCommandHandler : IRequestHandler<ReturnLoanCommand, LMS.Common.Constants.ResponseActionResult>
    {
        RestClients.ILOSService _lOSService;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<ReturnLoanCommandHandler> _logger;
        Common.Helper.Utils _common;
        public ReturnLoanCommandHandler(
            RestClients.ILOSService lOSService,
            ILogger<ReturnLoanCommandHandler> logger,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab
        )
        {
            _lOSService = lOSService;
            _logger = logger;
            _loanTab = loanTab;
            _userTab = userTab;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(ReturnLoanCommand req, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    var objLoanLos = _lOSService.GetLoanCreditDetail(req.LoanBriefID).Result;
                    if (objLoanLos == null)
                    {
                        response.Message = MessageConstant.LoanNotExist;
                        return response;
                    }
                    var objUser = _userTab.WhereClause(x => x.UserID == req.UserID).Query().FirstOrDefault();
                    if (objUser == null)
                    {
                        response.Message = MessageConstant.NotIdentifiedUserID;
                        return response;
                    }
                    var objReturnLoan = new ReturnLoanReq
                    {
                        LoanId =req.LoanBriefID,
                        Type = req.TypeID,
                        Note =req.Note,
                        UserName = objUser.FullName
                    };
                    return _lOSService.ReturnLoan(objReturnLoan).Result;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"ReturnLoanCommandHandler|request={_common.ConvertObjectToJSonV2(req)}|ex={ex.Message}-{ex.StackTrace}");
                }
                return response;
            });
        }
    }
}
