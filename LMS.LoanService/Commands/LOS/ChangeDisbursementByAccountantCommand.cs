﻿using LMS.Entites.Dtos.LOSServices;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LMS.LoanServiceApi.Constants;

namespace LMS.LoanServiceApi.Commands.LOS
{
    public class ChangeDisbursementByAccountantCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanBriefID { get; set; }
        public long UserID { get; set; }
        public string Note { get; set; }
    }
    public class ChangeDisbursementByAccountantCommandHandler : IRequestHandler<ChangeDisbursementByAccountantCommand, LMS.Common.Constants.ResponseActionResult>
    {
        RestClients.ILOSService _lOSService;
        ILogger<ChangeDisbursementByAccountantCommandHandler> _logger;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanCreditDisbursementAuto> _loanCreditDisbursementAutoTab;
        Common.Helper.Utils _common;
        public ChangeDisbursementByAccountantCommandHandler(
            RestClients.ILOSService lOSService,
            ILogger<ChangeDisbursementByAccountantCommandHandler> logger,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanCreditDisbursementAuto> loanCreditDisbursementAutoTab
        )
        {
            _lOSService = lOSService;
            _logger = logger;
            _loanTab = loanTab;
            _userTab = userTab;
            _loanCreditDisbursementAutoTab = loanCreditDisbursementAutoTab;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(ChangeDisbursementByAccountantCommand req, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    var objLoanLos = _lOSService.GetLoanCreditDetail(req.LoanBriefID).Result;
                    if (objLoanLos == null)
                    {
                        response.Message = MessageConstant.LoanNotExist;
                        return response;
                    }
                    var objUser = _userTab.WhereClause(x => x.UserID == req.UserID).Query().FirstOrDefault();
                    if (objUser == null)
                    {
                        response.Message = MessageConstant.NotIdentifiedUserID;
                        return response;
                    }
                    var loanCreditDisbursementAuto = _loanCreditDisbursementAutoTab.WhereClause(x => x.LoanCreditID == req.LoanBriefID).Query().OrderByDescending(x => x.LoanCreditDisbursementAutoID).FirstOrDefault();
                    if (loanCreditDisbursementAuto.Status == (int)LoanCreditDisbursementAuto_Status.WaitingOTP)
                    {
                        response.Message = MessageConstant.LoanWaitingOTP;
                        return response;
                    }

                    var objComment = new ReqSaveCommentHistory
                    {
                        LoanBriefId = req.LoanBriefID,
                        UserId = req.UserID,
                        UserName = objUser.FullName,
                        Note = req.Note,
                    };
                    _ = _lOSService.SaveCommentHistory(objComment);

                    var changeDisbursementByAccountantReq = new ChangeDisbursementByAccountantReq
                    {
                        LoanbriefId = req.LoanBriefID,
                        UserName = objUser.FullName,
                        Note = req.Note,
                        DisbursementBy = (int)LOS_Enum_DisbursementBy.Accountant
                    };

                    var changeDisbursementByAccountant = _lOSService.ChangeDisbursementByAccountant(changeDisbursementByAccountantReq).Result;
                    if (changeDisbursementByAccountant.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                    {
                        loanCreditDisbursementAuto.Status = (int)LoanCreditDisbursementAuto_Status.FailByAccountant;
                        _loanCreditDisbursementAutoTab.Update(loanCreditDisbursementAuto);
                    }
                    return changeDisbursementByAccountant;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"ChangeDisbursementByAccountantCommandHandler|request={_common.ConvertObjectToJSonV2(req)}|ex={ex.Message}-{ex.StackTrace}");
                }
                return response;
            });
        }
    }
}
