﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.LOSServices;
using LMS.LoanServiceApi.Constants;
using LMS.LoanServiceApi.Domain.Models.Insurance;
using LMS.LoanServiceApi.Domain.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.LOS
{
    public class ChangeStatusAccountantToAutoDisbursementCommad : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanBriefID { get; set; }
        public long UserID { get; set; }
        public string Note { get; set; }
    }
    public class ChangeStatusAccountantToAutoDisbursementCommadHandler : IRequestHandler<ChangeStatusAccountantToAutoDisbursementCommad, LMS.Common.Constants.ResponseActionResult>
    {
        RestClients.IInsuranceService _insuranceService;
        RestClients.ILOSService _lOSService;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanCreditDisbursementAuto> _loanCreditDisbursementAutoTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        ILogger<ChangeStatusAccountantToAutoDisbursementCommadHandler> _logger;
        Common.Helper.Utils _common;
        public ChangeStatusAccountantToAutoDisbursementCommadHandler(
            RestClients.ILOSService lOSService,
            RestClients.IInsuranceService insuranceService,
            ILogger<ChangeStatusAccountantToAutoDisbursementCommadHandler> logger,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanCreditDisbursementAuto> loanCreditDisbursementAutoTab)
        {
            _lOSService = lOSService;
            _insuranceService = insuranceService;
            _lenderTab = lenderTab;
            _loanTab = loanTab;
            _userTab = userTab;
            _logger = logger;
            _loanCreditDisbursementAutoTab = loanCreditDisbursementAutoTab;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(ChangeStatusAccountantToAutoDisbursementCommad request, CancellationToken cancellationToken)
        {

            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objUser = _userTab.WhereClause(x => x.UserID == request.UserID).Query().FirstOrDefault();
                if (objUser == null)
                {
                    response.Message = MessageConstant.NotIdentifiedUserID;
                    return response;
                }
                var loanCreditDisbursementAuto = _loanCreditDisbursementAutoTab.WhereClause(x => x.LoanCreditID == request.LoanBriefID).Query().OrderByDescending(x => x.LoanCreditDisbursementAutoID).FirstOrDefault();
                if (loanCreditDisbursementAuto == null || loanCreditDisbursementAuto.CreateDate.Date != DateTime.Now.Date)// -> giải ngân tự động
                {
                    return await ChangeStatusAccountantToAutoDisbursement(request, objUser);
                }
                else
                {
                    loanCreditDisbursementAuto.CreateDate = DateTime.Now;
                    loanCreditDisbursementAuto.DisbursementType = (int)LoanCreditDisbursementAuto_DisbursementType.Accountant;
                    loanCreditDisbursementAuto.Status = (int)LoanCreditDisbursementAuto_Status.Waiting;
                    loanCreditDisbursementAuto.ModifyDate = null;
                    loanCreditDisbursementAuto.JsonExtra = null;
                    var loanCreditDisbursementAutoID = _loanCreditDisbursementAutoTab.Insert(loanCreditDisbursementAuto);
                    if (loanCreditDisbursementAutoID > 0)
                        return await ChangeStatusAccountantToAutoDisbursementLos(request, objUser);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ChangeStatusAccountantToAutoDisbursementCommadHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private async Task<ResponseActionResult> ChangeStatusAccountantToAutoDisbursement(ChangeStatusAccountantToAutoDisbursementCommad req, Domain.Tables.TblUser objUser)
        {
            ResponseActionResult response = new ResponseActionResult();
            var objLoanLos = await _lOSService.GetLoanCreditDetail(req.LoanBriefID);
            if (objLoanLos == null)
            {
                response.Message = MessageConstant.LoanNotExist;
                return response;
            }
            var objLender = _lenderTab.WhereClause(x => x.LenderID == objLoanLos.LenderId).Query().FirstOrDefault();
            if (objLender == null)
            {
                response.Message = MessageConstant.NotIdentifiedLenderID;
                return response;
            }
            if (objLender.FullName.Contains(Constants.LOSConstants.NA))
            {
                response.Message = MessageConstant.LenderInvalid;
                return response;
            }
            if (objLoanLos.ReceivingMoneyType != (int)StateTypeReceivingMoney.NumberAccount || string.IsNullOrEmpty(objLoanLos.BankAccountNumber))
            {
                response.Message = MessageConstant.NotBankAccountNumber;
                return response;
            }

            var rateVND = _common.ConvertRateToVND(objLoanLos.RatePercent);
            var rateInterest = _common.ConvertRateInterestLenderToVND((float)objLender.RateInterest);
            var rateConsultant = rateVND - rateInterest;
            var rateService = 0;
            var rateType = objLoanLos.RateTypeId;
            var calculateMoneyInsuranceLOS = new CalculateMoneyInsuranceLOSReq
            {
                TotalMoneyDisbursement = Convert.ToInt64(objLoanLos.LoanAmountFinal),
                LoanTime = _common.ConvertLoanTimeLOSToLMS(objLoanLos.LoanTime),
                LoanFrequency = _common.ConvertFrequencyLOSToLMS(objLoanLos.Frequency),
                RateType = rateType,
                RateConsultant = rateConsultant,
                RateInterest = rateInterest,
                RateService = rateService
            };

            var objInsurance = await _insuranceService.CalculateMoneyInsuranceLOS(calculateMoneyInsuranceLOS);
            if (objInsurance == null)
            {
                response.Message = MessageConstant.CalculateMoneyInsuranceError;
                return response;
            }

            if (objLoanLos.BuyInsurenceCustomer == true)
                objLoanLos.FeesInsuranceCustomer = objInsurance.FeesInsuranceCustomer;
            if (objLoanLos.BuyInsuranceProperty == true)
                objLoanLos.FeesInsuranceMaterial = objInsurance.FeesInsuranceMaterial;
            var moneyFreeInsurance = objLoanLos.FeesInsuranceCustomer + objLoanLos.FeesInsuranceMaterial;

            var moneyCustomerRecieve = objLoanLos.LoanAmountFinal - moneyFreeInsurance;
            if (moneyCustomerRecieve < LOSConstants.MoneyCustomerRecieve)
            {
                response.Message = MessageConstant.ErrormoneyCustomerRecieve;
                return response;
            }
            // chuyển về trạng thái tự động
            var changeStatusAccountantToAutoDisbursement = await ChangeStatusAccountantToAutoDisbursementLos(req, objUser);
            if (changeStatusAccountantToAutoDisbursement.Result == (int)LMS.Common.Constants.ResponseAction.Success)
            {
                var objLoanCreditDisbursementAuto = new TblLoanCreditDisbursementAuto
                {
                    AccountNumberBanking = objLoanLos.BankAccountNumber,
                    CreateDate = DateTime.Now,
                    DisbursementType = (int)LoanCreditDisbursementAuto_DisbursementType.Accountant,
                    LoanCreditID = req.LoanBriefID,
                    Status = (int)LoanCreditDisbursementAuto_Status.Waiting,
                    MoneyDisbursement = Convert.ToInt64(moneyCustomerRecieve),
                    JsonExtra = null,
                    FullName = objLoanLos.FullName,
                    LenderID = (int)objLender.LenderID,
                    TotalMoneyDisbursement = (int)objLoanLos.LoanAmountFinal,
                    MoneyFeeInsuranceOfCustomer = Convert.ToInt64(objInsurance.FeesInsuranceCustomer),
                    MoneyFeeInsuranceMaterialCovered = Convert.ToInt64(objInsurance.FeesInsuranceMaterial)
                };
                _loanCreditDisbursementAutoTab.Insert(objLoanCreditDisbursementAuto);
                return changeStatusAccountantToAutoDisbursement;
            }
            return response;
        }

        private async Task<ResponseActionResult> ChangeStatusAccountantToAutoDisbursementLos(ChangeStatusAccountantToAutoDisbursementCommad req, Domain.Tables.TblUser objUser)
        {
            var changeStatusAccountantToAutoDisbursementReq = new ChangeDisbursementByAccountantReq
            {
                LoanbriefId = req.LoanBriefID,
                UserName = objUser.FullName,
                Note = req.Note,
                DisbursementBy = (int)LOS_Enum_DisbursementBy.Auto
            };
            SaveComment(req, objUser);
            // chuyển về trạng thái tự động
            return await _lOSService.ChangeDisbursementByAccountant(changeStatusAccountantToAutoDisbursementReq);
        }

        private void SaveComment(ChangeStatusAccountantToAutoDisbursementCommad req, Domain.Tables.TblUser objUser)
        {
            var objComment = new ReqSaveCommentHistory
            {
                LoanBriefId = req.LoanBriefID,
                UserId = req.UserID,
                UserName = objUser.FullName,
                Note = req.Note,
            };
            _ = _lOSService.SaveCommentHistory(objComment);
        }
    }
}
