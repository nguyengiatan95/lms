﻿using LMS.Entites.Dtos.LOSServices;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.LOS
{
    public class SaveCommentHistoryCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public ReqSaveCommentHistory Request { get; set; }
    }
    public class SaveCommentHistoryCommandHandler : IRequestHandler<SaveCommentHistoryCommand, LMS.Common.Constants.ResponseActionResult>
    {
        RestClients.ILOSService _iLOSService;
        ILogger<SaveCommentHistoryCommandHandler> _logger;
        Common.Helper.Utils _common;
        public SaveCommentHistoryCommandHandler(RestClients.ILOSService iLOSService, ILogger<SaveCommentHistoryCommandHandler> logger)
        {
            _iLOSService = iLOSService;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(SaveCommentHistoryCommand request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    return _iLOSService.SaveCommentHistory(request.Request).Result;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"SaveCommentHistoryCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                }
                return response;
            });
        }
    }
}
