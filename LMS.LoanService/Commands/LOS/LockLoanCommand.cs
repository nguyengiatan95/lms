﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.LOSServices;
using LMS.LoanServiceApi.Constants;
using LMS.LoanServiceApi.Domain.Models.Insurance;
using LMS.LoanServiceApi.Domain.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.LOS
{
    public class LockLoanCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanBriefID { get; set; }
        public long UserID { get; set; }
        public int TypeLock { get; set; }
    }
    public class LockLoanCommandHandler : IRequestHandler<LockLoanCommand, LMS.Common.Constants.ResponseActionResult>
    {
        RestClients.ILOSService _lOSService;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<LockLoanCommandHandler> _logger;
        Common.Helper.Utils _common;
        public LockLoanCommandHandler(
            RestClients.ILOSService lOSService,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            ILogger<LockLoanCommandHandler> logger)
        {
            _lOSService = lOSService;
            _userTab = userTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(LockLoanCommand request, CancellationToken cancellationToken)
        {

            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objLoanLos = _lOSService.GetLoanCreditDetail(request.LoanBriefID).Result;
                if (objLoanLos == null)
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                var objUser = _userTab.WhereClause(x => x.UserID == request.UserID).Query().FirstOrDefault();
                if (objUser == null)
                {
                    response.Message = MessageConstant.NotIdentifiedUserID;
                    return response;
                }

                string message = string.Empty;
                if (request.TypeLock == (int)LOS_Enum_LockLoanType.Open)
                    message = MessageConstant.OpenLoan;
                else
                    message = MessageConstant.LockLoan;
                var saveCommentHistory = new ReqSaveCommentHistory
                {
                    Note = message,
                    LoanBriefId = request.LoanBriefID,
                    UserId = request.UserID,
                    UserName = objUser.FullName
                };
                _ = await _lOSService.SaveCommentHistory(saveCommentHistory);

                var objLockLoan = new LockLoanReq();
                objLockLoan.LoanId = request.LoanBriefID;
                objLockLoan.Type = request.TypeLock;
                objLockLoan.UserName = objUser.FullName;
                objLockLoan.Note = message;
                var LockLoan = await _lOSService.LockLoan(objLockLoan);
                if (LockLoan.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                    return LockLoan;
                else
                    response.Message = LockLoan.Message;
            }
            catch (Exception ex)
            {
                _logger.LogError($"LockLoanCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
