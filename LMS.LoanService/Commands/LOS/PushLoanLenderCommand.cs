﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.LOSServices;
using LMS.LoanServiceApi.Constants;
using LMS.LoanServiceApi.Domain.Models.Insurance;
using LMS.LoanServiceApi.Domain.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.LOS
{
    public class PushLoanLenderCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanBriefID { get; set; }
        public long LenderID { get; set; }
        public long UserID { get; set; }
    }
    public class PushLoanLenderCommandHandler : IRequestHandler<PushLoanLenderCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly RestClients.IInsuranceService _insuranceService;
        readonly RestClients.ILOSService _lOSService;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanCreditDisbursementAuto> _loanCreditDisbursementAutoTab;
        //LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        ILogger<PushLoanLenderCommandHandler> _logger;
        Common.Helper.Utils _common;
        public PushLoanLenderCommandHandler(
            RestClients.ILOSService lOSService,
            RestClients.IInsuranceService insuranceService,
            ILogger<PushLoanLenderCommandHandler> logger,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            //LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanCreditDisbursementAuto> loanCreditDisbursementAutoTab)
        {
            _lOSService = lOSService;
            _insuranceService = insuranceService;
            _lenderTab = lenderTab;
            //_loanTab = loanTab;
            _userTab = userTab;
            _logger = logger;
            _loanCreditDisbursementAutoTab = loanCreditDisbursementAutoTab;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(PushLoanLenderCommand request, CancellationToken cancellationToken)
        {

            ResponseActionResult response = new ResponseActionResult();
            try
            {

                var objLoanLosTask = _lOSService.GetLoanCreditDetail(request.LoanBriefID);
                var objLenderTask = _lenderTab.WhereClause(x => x.LenderID == request.LenderID).QueryAsync();
                var objUserTask = _userTab.WhereClause(x => x.UserID == request.UserID).QueryAsync();
                await Task.WhenAll(objLoanLosTask, objLenderTask, objUserTask);

                var objLoanLos = objLoanLosTask.Result;
                if (objLoanLos == null)
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                var objLender = objLenderTask.Result.FirstOrDefault();
                if (objLender == null)
                {
                    response.Message = MessageConstant.NotIdentifiedLenderID;
                    return response;
                }
                var objUser = objUserTask.Result.FirstOrDefault();
                if (objUser == null)
                {
                    response.Message = MessageConstant.NotIdentifiedUserID;
                    return response;
                }
                var rateVND = _common.ConvertRateToVND(objLoanLos.RatePercent);
                var rateInterest = _common.ConvertRateToVND(objLoanLos.LenderRate);
                var rateConsultant = rateVND - rateInterest;
                var rateService = 0;
                var rateType = objLoanLos.RateTypeId;
                long feesInsuranceCustomer = 0;
                long feesInsuranceMaterial = 0;
                if (objLoanLos.BuyInsurenceCustomer || objLoanLos.BuyInsuranceProperty)
                {
                    var calculateMoneyInsuranceLOS = new CalculateMoneyInsuranceLOSReq
                    {
                        TotalMoneyDisbursement = Convert.ToInt64(objLoanLos.LoanAmountFinal),
                        LoanTime = _common.ConvertLoanTimeLOSToLMS(objLoanLos.LoanTime),
                        LoanFrequency = _common.ConvertFrequencyLOSToLMS(objLoanLos.Frequency),
                        RateType = rateType,
                        RateConsultant = rateConsultant,
                        RateInterest = rateInterest,
                        RateService = rateService
                    };
                    var objInsurance = await _insuranceService.CalculateMoneyInsuranceLOS(calculateMoneyInsuranceLOS);
                    if (objInsurance == null)
                    {
                        response.Message = MessageConstant.CalculateMoneyInsuranceError;
                        return response;
                    }
                    if (objLoanLos.BuyInsurenceCustomer == true)
                        objLoanLos.FeesInsuranceCustomer = objInsurance.FeesInsuranceCustomer;
                    if (objLoanLos.BuyInsuranceProperty == true)
                        objLoanLos.FeesInsuranceMaterial = objInsurance.FeesInsuranceMaterial;

                    feesInsuranceCustomer = Convert.ToInt64(objInsurance.FeesInsuranceCustomer);
                    feesInsuranceMaterial = Convert.ToInt64(objInsurance.FeesInsuranceMaterial);
                }

                var moneyFreeInsurance = objLoanLos.FeesInsuranceCustomer + objLoanLos.FeesInsuranceMaterial;

                var moneyCustomerRecieve = objLoanLos.LoanAmountFinal - moneyFreeInsurance;
                if (moneyCustomerRecieve < LOSConstants.MoneyCustomerRecieve)
                {
                    response.Message = MessageConstant.ErrormoneyCustomerRecieve;
                    return response;
                }
                var PushLoanLenderReq = new PushLoanLenderReq
                {
                    LoanId = request.LoanBriefID,
                    LenderId = objLender.LenderID,
                    LenderName = objLender.FullName,
                    LenderFullName = objLender.Represent,
                    LenderNationalCard = objLender.NumberCard,
                    UserName = objUser.FullName
                };
                PushLoanLenderReq.LenderId = objLender.LenderID;
                if (objLender.RegFromApp == (int)Lender_RegFromApp.NA)
                {
                    PushLoanLenderReq.DisbursementBy = (int)LOS_Enum_DisbursementBy.Lender;
                }
                else
                {
                    if (objLoanLos.ReceivingMoneyType == (int)StateTypeReceivingMoney.NumberAccount && !string.IsNullOrEmpty(objLoanLos.BankAccountNumber))
                        PushLoanLenderReq.DisbursementBy = (int)LOS_Enum_DisbursementBy.Auto;
                    else
                        PushLoanLenderReq.DisbursementBy = (int)LOS_Enum_DisbursementBy.Accountant;
                }
                var PushLoanLender = await _lOSService.PushLoanLender(PushLoanLenderReq);
                if (PushLoanLender.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                {
                    if (PushLoanLenderReq.DisbursementBy == (int)LOS_Enum_DisbursementBy.Auto)
                    {
                        var objLoanCreditDisbursementAuto = new TblLoanCreditDisbursementAuto
                        {
                            AccountNumberBanking = objLoanLos.BankAccountNumber,
                            CreateDate = DateTime.Now,
                            DisbursementType = (int)LoanCreditDisbursementAuto_DisbursementType.Accountant,
                            LoanCreditID = request.LoanBriefID,
                            Status = (int)LoanCreditDisbursementAuto_Status.Waiting,
                            MoneyDisbursement = Convert.ToInt64(moneyCustomerRecieve),
                            JsonExtra = null,
                            FullName = objLoanLos.FullName,
                            LenderID = (int)objLender.LenderID,
                            TotalMoneyDisbursement = (int)objLoanLos.LoanAmountFinal,
                            MoneyFeeInsuranceOfCustomer = feesInsuranceCustomer,
                            MoneyFeeInsuranceMaterialCovered = feesInsuranceMaterial
                        };
                        _ = await _loanCreditDisbursementAutoTab.InsertAsync(objLoanCreditDisbursementAuto);
                    }
                    return PushLoanLender;
                }
                else
                    response.Message = PushLoanLender.Message;
            }
            catch (Exception ex)
            {
                _logger.LogError($"PushLoanLenderCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
