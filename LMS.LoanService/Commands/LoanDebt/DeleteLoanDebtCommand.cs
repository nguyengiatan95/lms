﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.LoanDebt
{
    public class DeleteLoanDebtCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public long UserID { get; set; }
        public long DebtID { get; set; }
        public long SourceID { get; set; }
    }
    public class DeleteLoanDebtCommandHandler : IRequestHandler<DeleteLoanDebtCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebt> _loanDebtTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.LoanUpdateMoneyFineLate> _loanTab;
        readonly ILogger<DeleteLoanDebtCommandHandler> _logger;
        readonly Common.Helper.Utils _common;
        readonly RestClients.IPaymentScheduleService _paymentService;
        readonly RestClients.ITransactionService _transactionService;
        readonly RestClients.ICustomerService _customerService;
        public DeleteLoanDebtCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.LoanUpdateMoneyFineLate> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebt> loanDebtTab,
            RestClients.IPaymentScheduleService paymentService,
            RestClients.ITransactionService transactionService,
            RestClients.ICustomerService customerService,
            ILogger<DeleteLoanDebtCommandHandler> logger)
        {
            _loanTab = loanTab;
            _loanDebtTab = loanDebtTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _paymentService = paymentService;
            _transactionService = transactionService;
            _customerService = customerService;
        }

        public async Task<ResponseActionResult> Handle(DeleteLoanDebtCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var currentDate = DateTime.Now;
                var loanInfo = (await _loanTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                if (loanInfo == null)
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                if (loanInfo.Status != (int)Loan_Status.Lending)
                {
                    response.Message = MessageConstant.LoanCantProcess;
                    return response;
                }
                var objDebt = await _loanDebtTab.GetByIDAsync(request.DebtID);
                if (objDebt.Status != (int)StatusCommon.Active || objDebt.ParentDebtID > 0)
                {
                    response.Message = MessageConstant.LoanDebtDeleteValid;
                    return response;
                }
                var debtInfoExits = await _loanDebtTab.WhereClause(x => x.ReferID == request.LoanID && x.ParentDebtID == objDebt.DebtID).QueryAsync();
                if (debtInfoExits != null && debtInfoExits.Any())
                {
                    response.Message = MessageConstant.LoanDebtDeleteValid;
                    return response;
                }
                var debtDelete = new Domain.Tables.TblDebt()
                {
                    CreateBy = request.UserID,
                    Status = (int)StatusCommon.Active,
                    CreateDate = currentDate,
                    Description = $"Hủy giao dịch {objDebt.CreateDate:dd/MM/yyyy HH:mm}",
                    ParentDebtID = objDebt.DebtID,
                    ReferID = objDebt.ReferID,
                    SourceID = objDebt.SourceID,
                    TargetID = objDebt.TargetID,
                    TotalMoney = objDebt.TotalMoney * -1,
                    TypeID = objDebt.TypeID == (int)Debt_TypeID.CustomerFineLate ? (int)Debt_TypeID.CancelCustomerFineLate : (int)Debt_TypeID.CancelCustomerPayFineLate
                };
                if (await _loanDebtTab.InsertAsync(debtDelete) < 1)
                {
                    _logger.LogError($"DeleteLoanDebtCommandHandler_InsertLoanDebtTab|request={_common.ConvertObjectToJSonV2(request)}|debtDelete={_common.ConvertObjectToJSonV2(debtDelete)}");
                    response.Message = MessageConstant.ErrorInternal;
                    return response;
                }

                Domain.Tables.LoanUpdateMoneyFineLate loanUpdate = new Domain.Tables.LoanUpdateMoneyFineLate
                {
                    LoanID = loanInfo.LoanID,
                    Version = loanInfo.Version,
                    MoneyFineLate = loanInfo.MoneyFineLate,
                    Status = loanInfo.Status
                };
                loanUpdate.MoneyFineLate += debtDelete.TotalMoney;// (objDebt.TotalMoney * -1);
                loanUpdate.ModifyDate = currentDate;
                if (!await _loanTab.UpdateAsync(loanUpdate))
                {
                    _logger.LogError($"DeleteLoanDebtCommandHandler_UpdateFail|request={_common.ConvertObjectToJSonV2(request)}|loanUpdate={_common.ConvertObjectToJSonV2(loanUpdate)}");
                }

                // kiểm tra lại nếu hủy giao dịch gạch nợ
                // hồi lại tiền khách hàng
                // thêm 1 giao dịc transaction
                // cộng tiền cho kh nếu có
                // cập nhật loan

                long totalMoneyFine = objDebt.TotalMoney;

                int actionIDTxn = (int)Transaction_Action.DeleteTransactionMoneyFeeLate;
                if (objDebt.TypeID == (int)Debt_TypeID.CustomerPayFineLate)
                {
                    actionIDTxn = (int)Transaction_Action.DeleteTransactionPayMoneyFeeLate;
                    var t1 = _customerService.UpdateMoneyCustomer(objDebt.TargetID, debtDelete.TotalMoney, Transaction_Action.DeleteTransactionPayMoneyFeeLate.GetDescription());
                    var t2 = _paymentService.DeleteMoneyFineLateByLoanID(loanInfo.LoanID, debtDelete.TotalMoney);
                    await Task.WhenAll(t1, t2);
                }
                else if (objDebt.TypeID == (int)Debt_TypeID.CustomerFineLate)
                {
                    _ = await _paymentService.CancelMoneyFineByLoanID(loanInfo.LoanID, objDebt.TotalMoney);
                }

                if (actionIDTxn == (int)Transaction_Action.DeleteTransactionMoneyFeeLate)
                {
                    totalMoneyFine = 0;
                }
                _ = await _transactionService.CreateTransaction(new List<Entites.Dtos.TransactionServices.RequestCreateListTransactionModel>
                {
                    new Entites.Dtos.TransactionServices.RequestCreateListTransactionModel
                    {
                        LoanID = loanInfo.LoanID,
                        MoneyType = (int)Transaction_TypeMoney.FineLate,
                        ActionID = actionIDTxn,
                        UserID = request.UserID,
                        TotalMoney = totalMoneyFine
                    }
                });
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogWarning($"DeleteLoanDebtCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}");
            }
            return response;
        }
    }

}
