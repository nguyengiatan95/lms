﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.LoanDebt
{
    public class AgCancelMoneyFineLateCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long TimaLoanID { get; set; }
        public long MoneyFineLate { get; set; }
        public long CreateBy { get; set; }
    }
    public class AgCancelMoneyFineLateCommandHandle : IRequestHandler<AgCancelMoneyFineLateCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebt> _loanDebtTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanQueryTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.LoanUpdateMoneyFineLate> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentTab;
        RestClients.IPaymentScheduleService _paymentService;
        RestClients.ITransactionService _transactionService;

        ILogger<AgCancelMoneyFineLateCommandHandle> _logger;
        Common.Helper.Utils _common;
        public AgCancelMoneyFineLateCommandHandle(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanQueryTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebt> loanDebtTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.LoanUpdateMoneyFineLate> loanTab,
            RestClients.IPaymentScheduleService paymentService,
            RestClients.ITransactionService transactionService,
            ILogger<AgCancelMoneyFineLateCommandHandle> logger)
        {
            _loanQueryTab = loanQueryTab;
            _loanTab = loanTab;
            _loanDebtTab = loanDebtTab;
            _paymentTab = paymentTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _paymentService = paymentService;
            _transactionService = transactionService;
        }
        public async Task<ResponseActionResult> Handle(AgCancelMoneyFineLateCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var loanInfo = (await _loanQueryTab.WhereClause(x => x.TimaLoanID == request.TimaLoanID).QueryAsync()).FirstOrDefault();
                if (loanInfo == null)
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                Domain.Tables.LoanUpdateMoneyFineLate loanUpdate = new Domain.Tables.LoanUpdateMoneyFineLate
                {
                    LoanID = loanInfo.LoanID,
                    Version = loanInfo.Version,
                    MoneyFineLate = loanInfo.MoneyFineLate,
                    Status = (int)loanInfo.Status
                };
                // log thông báo check số tiền phạt nếu lms < moneyfine
                if (loanInfo.MoneyFineLate < request.MoneyFineLate)
                {
                    _logger.LogError($"AgCancelMoneyFineLateCommandHandle_Warning|request={_common.ConvertObjectToJSonV2(request)}|MoneyFineLateLMS={loanInfo.MoneyFineLate}");
                }
                var currentDate = DateTime.Now;
                loanUpdate.MoneyFineLate -= request.MoneyFineLate;
                loanUpdate.ModifyDate = currentDate;

                var actionPaymentTask = _paymentService.CancelMoneyFineByLoanID(loanInfo.LoanID, request.MoneyFineLate);
                var actionTransactionTask = _transactionService.CreateTransaction(new List<Entites.Dtos.TransactionServices.RequestCreateListTransactionModel>
                {
                    new Entites.Dtos.TransactionServices.RequestCreateListTransactionModel
                    {
                        LoanID = loanInfo.LoanID,
                        MoneyType = (int)Transaction_TypeMoney.FineLate,
                        ActionID = (int)Transaction_Action.DeleteTransactionMoneyFeeLate,
                        UserID = request.CreateBy,
                        TotalMoney = 0
                    }
                });

                var debtDelete = new Domain.Tables.TblDebt()
                {
                    CreateBy = request.CreateBy,
                    Status = (int)StatusCommon.Active,
                    CreateDate = currentDate,
                    Description = Debt_TypeID.CancelCustomerFineLate.GetDescription(),
                    ParentDebtID = 0,
                    ReferID = loanInfo.LoanID,
                    SourceID = loanInfo.LoanID,
                    TargetID = loanInfo.CustomerID.Value,
                    TotalMoney = request.MoneyFineLate * -1,
                    TypeID = (int)Debt_TypeID.CancelCustomerFineLate
                };
                _ = _loanDebtTab.InsertAsync(debtDelete);
                if (!await _loanTab.UpdateAsync(loanUpdate))
                {
                    _logger.LogError($"AgCancelMoneyFineLateCommandHandle_UpdateFail|request={_common.ConvertObjectToJSonV2(request)}|loanUpdate={_common.ConvertObjectToJSonV2(loanUpdate)}");
                }
                response.SetSucces();
                response.Data = loanInfo.LoanID;
            }
            catch (Exception ex)
            {
                _logger.LogWarning($"CreateLoanDebtCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
