﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.LoanDebt
{
    public class CreateLoanDebtCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public long UserID { get; set; }
        public string Description { get; set; }
        public long TotalMoney { get; set; }
        public long PaymentID { get; set; }
    }
    public class CreateLoanDebtCommandHandler : IRequestHandler<CreateLoanDebtCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebt> _loanDebtTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentTab;

        ILogger<CreateLoanDebtCommandHandler> _logger;
        Common.Helper.Utils _common;
        RestClients.IPaymentScheduleService _paymentScheduleService;
        public CreateLoanDebtCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebt> loanDebtTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentTab,
            RestClients.IPaymentScheduleService paymentScheduleService,
            ILogger<CreateLoanDebtCommandHandler> logger)
        {
            _loanTab = loanTab;
            _loanDebtTab = loanDebtTab;
            _paymentTab = paymentTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _paymentScheduleService = paymentScheduleService;
        }

        public async Task<ResponseActionResult> Handle(CreateLoanDebtCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objLoan = (await _loanTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                if (objLoan == null)
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                if (objLoan.Status != (int)Loan_Status.Lending)
                {
                    response.Message = MessageConstant.LoanCantProcess;
                    return response;
                }
                if (request.PaymentID <= 0)
                {
                    response.Message = MessageConstant.LoanDebtPaymentIDError;
                    return response;
                }
                if (request.TotalMoney <= 0)
                {
                    response.Message = MessageConstant.LoanDebtTotalError;
                    return response;
                }
                var lstPayment = await _paymentTab.WhereClause(x => x.PaymentScheduleID == request.PaymentID && x.LoanID == request.LoanID).QueryAsync();
                if (lstPayment == null || !lstPayment.Any() || lstPayment.FirstOrDefault().IsComplete == 1)
                {
                    response.Message = MessageConstant.LoanDebtPaymentIDError;
                    return response;
                }
                var objPayment = lstPayment.FirstOrDefault();
                Domain.Tables.TblDebt loandebt = new Domain.Tables.TblDebt()
                {
                    ReferID = request.LoanID,
                    CreateDate = DateTime.Now,
                    TotalMoney = request.TotalMoney,
                    TypeID = (int)LMS.Common.Constants.Debt_TypeID.CustomerFineLate,
                    Description = $"Ghi nợ kì : {objPayment.PayDate} : {request.Description}",
                    CreateBy = request.UserID,
                    Status = (int)StatusCommon.Active,
                    TargetID = objLoan.CustomerID.Value,
                    SourceID = objPayment.PaymentScheduleID
                };
                long insert = _loanDebtTab.Insert(loandebt);
                if (insert > 0)
                {
                    objLoan.MoneyFineLate += request.TotalMoney;
                    _ = _loanTab.UpdateAsync(objLoan);

                    _ = _paymentScheduleService.WriteLstMoneyFine(new List<Entites.Dtos.PaymentServices.RequestPaymentChangeMoneyFineModel>
                    {
                        new Entites.Dtos.PaymentServices.RequestPaymentChangeMoneyFineModel
                        {
                            PaymentScheduleID = objPayment.PaymentScheduleID,
                            MoneyAdd = request.TotalMoney
                        }
                    });

                    response.SetSucces();
                    response.Data = insert;
                }
            }
            catch (Exception ex)
            {
                _logger.LogWarning($"CreateLoanDebtCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
