﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.RequestCloseLoan
{
    public class CreateRequestCloseLoanCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanAgID { get; set; }
        public long RequestByUserID { get; set; }
        public string RequestByName { get; set; }
        public string Reason { get; set; }
        public string CloseDate { get; set; } // dd/MM/yyyy
    }
    public class CreateRequestCloseLoanCommandHandler : IRequestHandler<CreateRequestCloseLoanCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblRequestCloseLoan> _requestCloseLoanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        ILogger<CreateRequestCloseLoanCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        ILoanManager _loanManager;
        const int MaxDateCloseLoan = 3;
        RestClients.IOutGatewayService _proxyTimaService;
        IMediator _mediator;
        public CreateRequestCloseLoanCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblRequestCloseLoan> requestCloseLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            ILogger<CreateRequestCloseLoanCommandHandler> logger,
            RestClients.IOutGatewayService proxyTimaService,
            IMediator mediator,
            ILoanManager loanManager
            )
        {
            _loanTab = loanTab;
            _requestCloseLoanTab = requestCloseLoanTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _loanManager = loanManager;
            _customerTab = customerTab;
            _proxyTimaService = proxyTimaService;
            _mediator = mediator;
        }
        public async Task<ResponseActionResult> Handle(CreateRequestCloseLoanCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var currentDate = DateTime.Now;
                // kiểm tra đơn đã tạo chưa
                var loanLmsDetailExist = (await _loanTab.SetGetTop(1).WhereClause(x => x.TimaLoanID == request.LoanAgID).QueryAsync()).FirstOrDefault();
                if (loanLmsDetailExist == null || loanLmsDetailExist.LoanID < 1)
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                if (loanLmsDetailExist.Status != (int)Loan_Status.Lending)
                {
                    response.Message = MessageConstant.LoanCreditDisbusementStatusByAccountantNotValid;
                    return response;
                }
                if (!DateTime.TryParseExact(request.CloseDate, TimaSettingConstant.DateTimeDayMonthYear, null, System.Globalization.DateTimeStyles.None, out DateTime closeDate))
                {
                    response.Message = MessageConstant.ErrorFormatDate;
                    return response;
                }
                // ngày đóng HD không nhỏ hơn quá 3 ngày so với hiện tại
                if (closeDate.Date.AddDays(MaxDateCloseLoan) < currentDate.Date)
                {
                    response.Message = MessageConstant.RequestCloseLoan_CloseDateWithMinDate;
                    return response;
                }
                if (closeDate.Date > currentDate.Date)
                {
                    response.Message = MessageConstant.RequestCloseLoan_CloseDateWithToDay;
                    return response;
                }
                // ngày đóng HD không lớn hơn ngày nạp tiền gần nhất của KH
                var customerInfo = (await _customerTab.SetGetTop(1).WhereClause(x => x.CustomerID == loanLmsDetailExist.CustomerID).QueryAsync()).FirstOrDefault();
                if (!customerInfo.LatestDateTopup.HasValue || customerInfo.LatestDateTopup.Value.Date > closeDate.Date)
                {
                    response.Message = MessageConstant.RequestCloseLoan_CloseDateWithTopUpDate;
                    return response;
                }
                List<int> lstStatusWaiting = new List<int>
                {
                    (int)RequestCloseLoan_Status.Waitting,
                    (int)RequestCloseLoan_Status.Approved,
                    (int)RequestCloseLoan_Status.Success
                };
                var requestExist = (await _requestCloseLoanTab.WhereClause(x => x.LoanID == loanLmsDetailExist.LoanID && lstStatusWaiting.Contains(x.Status)).QueryAsync()).FirstOrDefault();
                if (requestExist != null && requestExist.LoanID > 0)
                {
                    response.Message = MessageConstant.LoanExist;
                    return response;
                }
                Domain.Tables.RequestCloseLoanJsonExtra jsonExtra = new Domain.Tables.RequestCloseLoanJsonExtra
                {
                    RequestByName = request.RequestByName,
                    LoanContractCode = loanLmsDetailExist.ContactCode,
                    TimeHandles = new List<Domain.Tables.RequestCloseLoanTimeHandle>
                    {
                        new Domain.Tables.RequestCloseLoanTimeHandle
                        {
                            CreateBy = request.RequestByUserID,
                            Status = (int)RequestCloseLoan_Status.Waitting,
                            TimeHandle = currentDate
                        }
                    }
                };
                var idInsert = await _requestCloseLoanTab.InsertAsync(new Domain.Tables.TblRequestCloseLoan
                {
                    LoanID = loanLmsDetailExist.LoanID,
                    CreateDate = currentDate,
                    ModifyDate = currentDate,
                    Reason = request.Reason,
                    RequestByUserID = request.RequestByUserID,
                    CloseDate = closeDate,
                    JsonExtra = _common.ConvertObjectToJSonV2(jsonExtra)
                });
                response.SetSucces();
                response.Data = idInsert;
                await TranferAgHandle(request, idInsert);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestCloseLoanCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private async Task TranferAgHandle(CreateRequestCloseLoanCommand request, long requestCloseLoanID, int retry = 0)
        {
            var dataPost = new
            {
                LoanID = request.LoanAgID,
                CloseDate = request.CloseDate,
                CreateBy = request.RequestByUserID,
                UserName = request.RequestByName,
            };
            try
            {
                var actionResult = await _proxyTimaService.CallApiAg(new LMS.Entites.Dtos.AG.AgDataPostModel
                {
                    Path = LMS.Entites.Dtos.AG.AGConstants.CloseLoanFromRequestThirdParty,
                    DataPost = _common.ConvertObjectToJSonV2(dataPost)
                });

                if (actionResult != null && actionResult.Result != (int)ResponseAction.Success && retry < MaxDateCloseLoan)
                {
                    await Task.Delay(TimeSpan.FromSeconds(2));
                    await TranferAgHandle(request, requestCloseLoanID, ++retry);
                    return;
                }
                UpdateRequestCloseLoanCommand updateRequestCloseLoanDetail = new UpdateRequestCloseLoanCommand
                {
                    RequestID = requestCloseLoanID,
                    CreateBy = request.RequestByUserID,
                    CreateByName = request.RequestByName
                };
                if (actionResult == null)
                {
                    updateRequestCloseLoanDetail.Status = (int)RequestCloseLoan_Status.Fail;
                    updateRequestCloseLoanDetail.Note = MessageConstant.ErrorInternal;
                }
                else
                {
                    ResponseActionResult agResponseResult = _common.ConvertJSonToObjectV2<ResponseActionResult>(actionResult.Data.ToString());
                    // parse 2 lần mới lấy thông tin ag trả về
                    agResponseResult = _common.ConvertJSonToObjectV2<ResponseActionResult>(agResponseResult.Data.ToString());

                    if (agResponseResult.Result == (int)ResponseAction.Success)
                    {
                        updateRequestCloseLoanDetail.Status = (int)RequestCloseLoan_Status.Success;
                    }
                    else
                    {
                        updateRequestCloseLoanDetail.Status = (int)RequestCloseLoan_Status.Fail;
                        updateRequestCloseLoanDetail.Note = agResponseResult.Message;
                    }
                }               
                _ = await _mediator.Send(updateRequestCloseLoanDetail);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestCloseLoanCommandHandler|TranferAgHandle|dataPost={_common.ConvertObjectToJSonV2(dataPost)}|Retry={retry}-{ex.Message}-{ex.StackTrace}");
            }
        }
    }
}
