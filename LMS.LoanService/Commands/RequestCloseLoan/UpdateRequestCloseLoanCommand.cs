﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.RequestCloseLoan
{
    public class UpdateRequestCloseLoanCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long RequestID { get; set; }
        public long CreateBy { get; set; }
        public string CreateByName { get; set; }
        public string Note { get; set; }
        public int Status { get; set; }
    }
    public class UpdateRequestCloseLoanCommandHandler : IRequestHandler<UpdateRequestCloseLoanCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblRequestCloseLoan> _requestCloseLoanTab;
        ILogger<UpdateRequestCloseLoanCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdateRequestCloseLoanCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblRequestCloseLoan> requestCloseLoanTab,
            ILogger<UpdateRequestCloseLoanCommandHandler> logger
            )
        {
            _loanTab = loanTab;
            _requestCloseLoanTab = requestCloseLoanTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(UpdateRequestCloseLoanCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                List<int> lstStatusValid = new List<int>
                {
                    (int)RequestCloseLoan_Status.Approved,
                    (int)RequestCloseLoan_Status.Reject,
                    (int)RequestCloseLoan_Status.Success,
                    (int)RequestCloseLoan_Status.Fail,
                    (int)RequestCloseLoan_Status.Cancel
                };
                List<int> lstStatusApproved = new List<int>
                {
                    (int)RequestCloseLoan_Status.Approved,
                    (int)RequestCloseLoan_Status.Reject,
                    (int)RequestCloseLoan_Status.Cancel
                };
                if (!lstStatusValid.Contains(request.Status))
                {
                    response.Message = MessageConstant.RequireStatus;
                    return response;
                }
                var currentDate = DateTime.Now;
                var requestExist = (await _requestCloseLoanTab.WhereClause(x => x.RequestCloseLoanID == request.RequestID).QueryAsync()).FirstOrDefault();
                if (requestExist == null && requestExist.RequestCloseLoanID < 1)
                {
                    response.Message = MessageConstant.NoData;
                    return response;
                }
                switch ((RequestCloseLoan_Status)requestExist.Status)
                {
                    case RequestCloseLoan_Status.Success:
                    case RequestCloseLoan_Status.Fail:
                        response.Message = MessageConstant.RequireStatus;
                        return response;
                }

                Domain.Tables.RequestCloseLoanJsonExtra jsonExtra = _common.ConvertJSonToObjectV2<Domain.Tables.RequestCloseLoanJsonExtra>(requestExist.JsonExtra);
                jsonExtra.TimeHandles.Add(new Domain.Tables.RequestCloseLoanTimeHandle
                {
                    CreateBy = request.CreateBy,
                    Status = request.Status,
                    TimeHandle = currentDate
                });
                requestExist.Status = request.Status;
                requestExist.ModifyDate = currentDate;
                request.Note = request.Note;
                requestExist.ApprovedByUserID = request.CreateBy;
                jsonExtra.ApprovedByName = request.CreateByName;
                if (lstStatusApproved.Contains(requestExist.Status))
                {
                    requestExist.ApprovedDate = currentDate;
                }
                requestExist.JsonExtra = _common.ConvertObjectToJSonV2(jsonExtra);
                if (await _requestCloseLoanTab.UpdateAsync(requestExist))
                {
                    response.SetSucces();
                    response.Data = requestExist.RequestCloseLoanID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateRequestCloseLoanCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}

