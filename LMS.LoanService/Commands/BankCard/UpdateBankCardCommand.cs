﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.BankCard
{
    public class UpdateBankCardCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public long BankCardID { get; set; }
        [Required]
        public long BankID { get; set; }
        [Required]
        public string NumberAccount { get; set; } // số tài khoản
        [Required]
        public string AccountHolderName { get; set; } // chủ tài khoản
        [Required]
        public string BranchName { get; set; } // teen chi nhanh
        [Required]
        public string AliasName { get; set; } // tên tài khoản hiển thị

        public int ParentID { get; set; }

        public long TotalMoney { get; set; }
        [Required]
        public long UserID { get; set; }
        public int Status { get; set; }
        public int TypePurpose { get; set; }
    }
    public class UpdateBankCardCommandHandler : IRequestHandler<UpdateBankCardCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBank> _bankTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogBankCard> _logBankCardTab;
        ILogger<UpdateBankCardCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdateBankCardCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBank> bankTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogBankCard> logBankCardTab,
        ILogger<UpdateBankCardCommandHandler> logger
            )
        {
            _bankCardTab = bankCardTab;
            _bankTab = bankTab;
            _logBankCardTab = logBankCardTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(UpdateBankCardCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objbankCard = (await _bankCardTab.WhereClause(x => x.BankCardID == request.BankCardID).QueryAsync()).FirstOrDefault();
                if (objbankCard == null)
                {
                    response.Message = MessageConstant.BankCardNotExist;
                    return response;
                }
                var objBank = (await _bankTab.WhereClause(x => x.Id == request.BankID).QueryAsync()).FirstOrDefault();
                if (objBank == null)
                {
                    response.Message = MessageConstant.BankNotExist;
                    return response;
                }
                objbankCard.AccountHolderName = request.AccountHolderName;
                objbankCard.BankID = request.BankID;
                objbankCard.BankCode = objBank.Code;
                objbankCard.NumberAccount = request.NumberAccount;
                objbankCard.ModifyOn = DateTime.Now;
                objbankCard.BranchName = request.BranchName;
                objbankCard.AliasName = request.AliasName;
                objbankCard.ParentID = request.ParentID;
                // không cho cập nhật tiền
                //objbankCard.TotalMoney = request.TotalMoney;
                objbankCard.Status = (short)request.Status;
                objbankCard.TypePurpose = (short)request.TypePurpose;
                var update = await _bankCardTab.UpdateAsync(objbankCard);
                if (update)
                {
                    //var objLogBankCard = new Domain.Tables.TblLogBankCard
                    //{
                    //    BankCardID = request.BankCardID,
                    //    TotalMoney = request.TotalMoney,
                    //    CreateDate = DateTime.Now,
                    //    UserID = request.UserID,
                    //    JobStatus = (int)LogBankCard_JobStatus.NoProcess
                    //};
                    //_logBankCardTab.Insert(objLogBankCard);
                    response.SetSucces();
                    response.Data = request.BankCardID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateBankCardCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
