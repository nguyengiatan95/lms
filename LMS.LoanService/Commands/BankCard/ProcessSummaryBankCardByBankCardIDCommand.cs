﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.BankCard
{
    public class ProcessSummaryBankCardByBankCardIDCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string TransactionDate { get; set; }
        public List<long> BankCardIDs { get; set; }
    }
    public class ProcessSummaryBankCardByBankCardIDCommandHandler : IRequestHandler<ProcessSummaryBankCardByBankCardIDCommand, LMS.Common.Constants.ResponseActionResult>
    {
        Services.IBankCardManager _bankCardManager;
        ILogger<ProcessSummaryBankCardByBankCardIDCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public ProcessSummaryBankCardByBankCardIDCommandHandler(Services.IBankCardManager bankCardManager,
            ILogger<ProcessSummaryBankCardByBankCardIDCommandHandler> logger)
        {
            _bankCardManager = bankCardManager;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(ProcessSummaryBankCardByBankCardIDCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var startDate = DateTime.ParseExact(request.TransactionDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                long actionResult = await _bankCardManager.ProcessSummaryBankCard(startDate, request.BankCardIDs);
                response.SetSucces();
                if (actionResult > 0)
                {
                    response.Data = actionResult;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSummaryBankCardByBankCardIDCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
