﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.BankCard
{
    public class ProcessSummaryTransactionDateCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class ProcessSummaryTransactionDateCommandHandler : IRequestHandler<ProcessSummaryTransactionDateCommand, LMS.Common.Constants.ResponseActionResult>
    {
        Services.IBankCardManager _bankCardManager;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> _settingKeyTab;
        ILogger<ProcessSummaryTransactionDateCommandHandler> _logger;
        public ProcessSummaryTransactionDateCommandHandler(Services.IBankCardManager bankCardManager,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> settingKeyTab,
            ILogger<ProcessSummaryTransactionDateCommandHandler> logger)
        {
            _bankCardManager = bankCardManager;
            _settingKeyTab = settingKeyTab;
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(ProcessSummaryTransactionDateCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var keySettingInfo = (await _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.Invoice_SummaryTransactionDateBankCard).QueryAsync()).FirstOrDefault();
                if (keySettingInfo == null || keySettingInfo.Status == 0)
                {
                    response.Message = MessageConstant.SettingKey_Invoice_SummaryTransactionDateBankCardStatus;
                    return response;
                }
                var currentDate = DateTime.Now;
                DateTime startDate = currentDate;
                if (string.IsNullOrEmpty(keySettingInfo.Value))
                {
                    response.Message = MessageConstant.SettingKey_Invoice_SummaryTransactionDateBankCardEmpty;
                    return response;
                }
                if (!DateTime.TryParseExact(keySettingInfo.Value, TimaSettingConstant.DateTimeDayMonthYear, null, System.Globalization.DateTimeStyles.None, out var startDateValue))
                {
                    response.Message = MessageConstant.SettingKey_Invoice_SummaryTransactionDateBankCardFormat;
                    return response;
                }
                startDate = startDateValue;
                long actionResult = await _bankCardManager.ProcessSummaryBankCard(startDate);
                if (actionResult > 0)
                {
                    response.SetSucces();
                    response.Data = actionResult;
                    keySettingInfo.Value = currentDate.ToString(TimaSettingConstant.DateTimeDayMonthYear);
                    keySettingInfo.ModifyDate = currentDate;
                    _ = await _settingKeyTab.UpdateAsync(keySettingInfo);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSummaryTransactionDateCommand|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
