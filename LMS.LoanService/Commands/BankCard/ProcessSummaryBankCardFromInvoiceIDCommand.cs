﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.BankCard
{
    public class ProcessSummaryBankCardFromInvoiceIDCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class ProcessSummaryBankCardFromInvoiceIDCommandHandler : IRequestHandler<ProcessSummaryBankCardFromInvoiceIDCommand, LMS.Common.Constants.ResponseActionResult>
    {
        Services.IBankCardManager _bankCardManager;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> _settingKeyTab;
        ILogger<ProcessSummaryBankCardFromInvoiceIDCommandHandler> _logger;
        public ProcessSummaryBankCardFromInvoiceIDCommandHandler(Services.IBankCardManager bankCardManager,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> settingKeyTab,
            ILogger<ProcessSummaryBankCardFromInvoiceIDCommandHandler> logger)
        {
            _bankCardManager = bankCardManager;
            _settingKeyTab = settingKeyTab;
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(ProcessSummaryBankCardFromInvoiceIDCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var keySettingInfo = (await _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.Invoice_SummaryBankCardFromInvoiceID).QueryAsync()).FirstOrDefault();
                if (keySettingInfo == null || keySettingInfo.Status == (int)SettingKey_Status.InActice)
                {
                    response.Message = MessageConstant.SettingKey_Invoice_SummaryBankCardFromInvoiceIDStatus;
                    return response;
                }
                keySettingInfo.Status = (int)SettingKey_Status.InActice;
                keySettingInfo.ModifyDate = DateTime.Now;
                _ = await _settingKeyTab.UpdateAsync(keySettingInfo);
                _ = RunUntilFinished(keySettingInfo);
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSummaryBankCardFromInvoiceIDCommand|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        private async Task RunUntilFinished(Domain.Tables.TblSettingKey keySettingInfo)
        {
            try
            {
                long actionResult = await _bankCardManager.ProcessSummaryBankCardFromInvoiceID(0);
                if (actionResult <= 1)
                {
                    keySettingInfo.Status = (int)SettingKey_Status.Active;
                    keySettingInfo.Value = $"{Convert.ToInt64(DateTime.Now.Subtract(keySettingInfo.ModifyDate).TotalMilliseconds)}";
                    keySettingInfo.ModifyDate = DateTime.Now;
                    _ = _settingKeyTab.UpdateAsync(keySettingInfo);
                    return;
                }
                _ = RunUntilFinished(keySettingInfo);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSummaryBankCardFromInvoiceIDCommand_RunUntilFinished|ex={ex.Message}-{ex.StackTrace}");
            }
        }

    }
}
