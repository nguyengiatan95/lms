﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.BankCard
{
    public class CreateBankCardCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long BankCardID { get; set; }
        [Required]
        public long BankID { get; set; }
        [Required]
        public string NumberAccount { get; set; } // số tài khoản
        [Required]
        public string AccountHolderName { get; set; } // chủ tài khoản
        [Required]
        public string BranchName { get; set; } // teen chi nhanh
        [Required]
        public string AliasName { get; set; } // tên tài khoản hiển thị
        [Required]
        public int ParentID { get; set; }

        public long TotalMoney { get; set; }
        public long UserID { get; set; }
        public int TypePurpose { get; set; }
    }
    public class CreateBankCardCommandHandler : IRequestHandler<CreateBankCardCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBank> _bankTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogBankCard> _logBankCardTab;
        ILogger<CreateBankCardCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateBankCardCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBank> bankTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogBankCard> logBankCardTab,
        ILogger<CreateBankCardCommandHandler> logger
            )
        {
            _bankCardTab = bankCardTab;
            _bankTab = bankTab;
            _logBankCardTab = logBankCardTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateBankCardCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var checkBankCard = (await _bankCardTab.WhereClause(x => x.BankID == request.BankID && x.NumberAccount == request.NumberAccount).QueryAsync()).FirstOrDefault();
                if (checkBankCard != null)
                {
                    response.Message = MessageConstant.BankCardExist;
                    return response;
                }
                var objBank = (await _bankTab.WhereClause(x => x.Id == request.BankID).QueryAsync()).FirstOrDefault();
                if (objBank == null)
                {
                    response.Message = MessageConstant.BankNotExist;
                    return response;
                }
                var objbankCard = _common.ConvertParentToChild<Domain.Tables.TblBankCard, CreateBankCardCommand>(request);
                objbankCard.TypePurpose = (short)request.TypePurpose;
                objbankCard.BankCode = objBank.Code;
                objbankCard.Status = (int)BankCard_Status.Active;
                objbankCard.CreateOn = DateTime.Now;
                objbankCard.TotalMoney = 0;// không cho cập nhật tiền khi thêm mới
                var bankCardID = await _bankCardTab.InsertAsync(objbankCard);
                if (bankCardID > 0)
                {
                    //var objLogBankCard = new Domain.Tables.TblLogBankCard
                    //{
                    //    BankCardID = bankCardID,
                    //    TotalMoney = request.TotalMoney,
                    //    CreateDate = DateTime.Now,
                    //    UserID = request.UserID,
                    //    JobStatus = (int)LogBankCard_JobStatus.NoProcess
                    //};
                    //_logBankCardTab.Insert(objLogBankCard);
                    response.SetSucces();
                    response.Data = bankCardID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateBankCardCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
