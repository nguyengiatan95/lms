﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.BankCard
{
    public class UpdateMoneyStartDateForBankCardCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long BankCardID { get; set; }
        public long TotalMoney { get; set; }
        public string DateApply { get; set; }
        public long CreateBy { get; set; }
    }
    public class UpdateMoneyStartDateForBankCardCommandHandle : IRequestHandler<UpdateMoneyStartDateForBankCardCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingDateBankCard> _settingDateBankCardTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        Services.IBankCardManager _bankCardManager;
        ILogger<UpdateMoneyStartDateForBankCardCommandHandle> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdateMoneyStartDateForBankCardCommandHandle(LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingDateBankCard> settingDateBankCardTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab,
            Services.IBankCardManager bankCardManager,
            ILogger<UpdateMoneyStartDateForBankCardCommandHandle> logger)
        {
            _bankCardTab = bankCardTab;
            _settingDateBankCardTab = settingDateBankCardTab;
            _bankCardManager = bankCardManager;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(UpdateMoneyStartDateForBankCardCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var bankDetail = (await _bankCardTab.WhereClause(x => x.BankCardID == request.BankCardID).QueryAsync()).FirstOrDefault();
                if (bankDetail == null || bankDetail.BankCardID < 1)
                {
                    response.Message = MessageConstant.BankCardNotExist;
                    return response;
                }
                var currentDate = DateTime.Now;
                var dateApply = DateTime.ParseExact(request.DateApply, TimaSettingConstant.DateTimeDayMonthYear, null);
                var idResult = await _settingDateBankCardTab.InsertAsync(new Domain.Tables.TblSettingDateBankCard
                {
                    BankCardID = request.BankCardID,
                    TotalMoney = request.TotalMoney,
                    DateApply = dateApply,
                    CreateBy = request.CreateBy,
                    ModifyDate = currentDate,
                    CreateDate = currentDate
                });
                response.SetSucces();
                response.Data = idResult;
                _ = _bankCardManager.ProcessSummaryBankCard(dateApply, new List<long> { request.BankCardID });
                await Task.Delay(10);
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateMoneyStartDateForBankCardCommandHandle|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
