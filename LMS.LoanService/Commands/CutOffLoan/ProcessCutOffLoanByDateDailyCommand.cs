﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.LoanServiceApi.Domain.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.CutOffLoan
{
    public class ProcessCutOffLoanByDateDailyCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class ProcessCutOffLoanByDateDailyCommandHandler : IRequestHandler<ProcessCutOffLoanByDateDailyCommand, LMS.Common.Constants.ResponseActionResult>
    {
        Services.ILoanManager _loanManager;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> _settingKeyTab;
        LMS.Common.Helper.Utils _common;
        ILogger<ProcessCutOffLoanByDateDailyCommandHandler> _logger;
        public ProcessCutOffLoanByDateDailyCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> settingKeyTab,
            Services.ILoanManager loanManager,
             ILogger<ProcessCutOffLoanByDateDailyCommandHandler> logger)
        {
            _loanManager = loanManager;
            _settingKeyTab = settingKeyTab;
            _common = new Utils();
            _logger = logger;
        }

        public async Task<ResponseActionResult> Handle(ProcessCutOffLoanByDateDailyCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var keySettingInfo = (await _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.CutOffLoan_ProcessDaily).QueryAsync()).FirstOrDefault();
                if (keySettingInfo == null)
                {
                    keySettingInfo = new Domain.Tables.TblSettingKey
                    {
                        CreateDate = DateTime.Now,
                        KeyName = LMS.Common.Constants.SettingKey_KeyValue.CutOffLoan_ProcessDaily,
                        Status = (int)SettingKey_Status.Active,
                        ModifyDate = DateTime.Now,
                        Value = _common.ConvertObjectToJSonV2(new SettingKeyValueCutOffLoan
                        {
                            CutOffDate = DateTime.Now.AddDays(-1),
                            LatestLoanID = 0
                        })
                    };
                    keySettingInfo.SettingKeyID = await _settingKeyTab.InsertAsync(keySettingInfo);
                }
                if (keySettingInfo.Status == (int)SettingKey_Status.InActice)
                {
                    response.Message = MessageConstant.SettingKey_CutOffLoan_ProcessDailyStatus;
                    return response;
                }
                SettingKeyValueCutOffLoan settingKeyValue = new SettingKeyValueCutOffLoan() { CutOffDate = DateTime.Now.AddDays(-1) };
                if (!string.IsNullOrEmpty(keySettingInfo.Value))
                {
                    try
                    {
                        settingKeyValue = _common.ConvertJSonToObjectV2<SettingKeyValueCutOffLoan>(keySettingInfo.Value);
                    }
                    catch (Exception)
                    {
                        settingKeyValue = new SettingKeyValueCutOffLoan
                        {
                            CutOffDate = DateTime.Now.AddDays(-1)
                        };
                        keySettingInfo.Value = _common.ConvertObjectToJSonV2(settingKeyValue);
                    }
                }
                if (settingKeyValue.CutOffDate.Date == DateTime.Now.Date)
                {
                    response.Message = MessageConstant.SettingKey_PlanCloseLoan_ProcessDailyFinished;
                    return response;
                }
                keySettingInfo.Status = (int)SettingKey_Status.InActice;
                keySettingInfo.ModifyDate = DateTime.Now;
                _ = RunUntilFinished(keySettingInfo, settingKeyValue.LatestLoanID);
                _ = await _settingKeyTab.UpdateAsync(keySettingInfo);
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessCutOffLoanByDateDailyCommandHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        private async Task RunUntilFinished(Domain.Tables.TblSettingKey keySettingInfo, long latestLoanID)
        {
            try
            {
                // process hiện tại chỉ chạy 1 lần là xong
                var actionResult = await _loanManager.ProcessCutOffLoanByDateDaily(latestLoanID);
                if (actionResult.Result == (int)ResponseAction.Success)
                {
                    var settingKeyValue = _common.ConvertJSonToObjectV2<SettingKeyValueCutOffLoan>(keySettingInfo.Value);
                    settingKeyValue.CountTimeHandleFinished = Convert.ToInt64(DateTime.Now.Subtract(keySettingInfo.ModifyDate).TotalMilliseconds);
                    settingKeyValue.CutOffDate = DateTime.Now;
                    settingKeyValue.LatestLoanID = 0;
                    keySettingInfo.Status = (int)SettingKey_Status.Active;
                    settingKeyValue.CountTimeHandleFinished = Convert.ToInt64(DateTime.Now.Subtract(keySettingInfo.ModifyDate).TotalMilliseconds);
                    keySettingInfo.ModifyDate = DateTime.Now;
                    keySettingInfo.Value = _common.ConvertObjectToJSonV2(settingKeyValue);
                    _ = await _settingKeyTab.UpdateAsync(keySettingInfo);
                    return;
                }
                //_ = RunUntilFinished(keySettingInfo);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessCutOffLoanByDateDailyCommandHandler_RunUntilFinished|ex={ex.Message}-{ex.StackTrace}");
            }
        }
    }
}
