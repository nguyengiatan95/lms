﻿using LMS.Common.Constants;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Report
{
    public class ProcessSummaryStasticsLoanBorrowCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long ShopID { get; set; }
    }
    public class ProcessSummaryStasticsLoanBorrowCommandHandler : IRequestHandler<ProcessSummaryStasticsLoanBorrowCommand, LMS.Common.Constants.ResponseActionResult>
    {
        Services.IReportManager _reportManager;
        public ProcessSummaryStasticsLoanBorrowCommandHandler(Services.IReportManager reportManager)
        {
            _reportManager = reportManager;
        }
        public async Task<ResponseActionResult> Handle(ProcessSummaryStasticsLoanBorrowCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var reportDetail = await _reportManager.ProcessSummaryStasticLoanBorrow(request.ShopID);
            if (response != null)
            {
                response.SetSucces();
                response.Data = reportDetail;
            }
            return response;
        }
    }
}
