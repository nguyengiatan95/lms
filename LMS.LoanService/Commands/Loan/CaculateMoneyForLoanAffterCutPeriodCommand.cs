﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.LoanServices;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class CaculateMoneyForLoanAffterCutPeriodCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
    }
    public class CaculateMoneyForLoanAffterCutPeriodCommandHandler : IRequestHandler<CaculateMoneyForLoanAffterCutPeriodCommand, LMS.Common.Constants.ResponseActionResult>
    {
        Services.ILoanManager _loanManager;
        LMS.Common.Helper.Utils _common;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanCloseLoan> _planCloseLoanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExtra> _loanExtraTab;
        ILogger<CaculateMoneyForLoanAffterCutPeriodCommandHandler> _logger;
        public CaculateMoneyForLoanAffterCutPeriodCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanCloseLoan> planCloseLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExtra> loanExtraTab,
            Services.ILoanManager loanManager,
            ILogger<CaculateMoneyForLoanAffterCutPeriodCommandHandler> logger)
        {
            _loanManager = loanManager;
            _loanTab = loanTab;
            _paymentScheduleTab = paymentScheduleTab;
            _planCloseLoanTab = planCloseLoanTab;
            _common = new Utils();
            _logger = logger;
            _lenderTab = lenderTab;
            _loanExtraTab = loanExtraTab;
        }
        public async Task<ResponseActionResult> Handle(CaculateMoneyForLoanAffterCutPeriodCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult resut = new ResponseActionResult();
            try
            {
                var currentDate = DateTime.Now;
                var loanDetailTask = _loanTab.SetGetTop(1).WhereClause(x => x.LoanID == request.LoanID).QueryAsync();
                var lstPaymentScheduleTask = _paymentScheduleTab.WhereClause(x => x.LoanID == request.LoanID && x.Status == (int)PaymentSchedule_Status.Use).QueryAsync();
                var planDetailTask = _planCloseLoanTab.SetGetTop(1).WhereClause(x => x.LoanID == request.LoanID && x.CloseDate == currentDate.Date).QueryAsync();
                var lstLoanExtraTask = _loanExtraTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync();
                await Task.WhenAll(loanDetailTask, lstPaymentScheduleTask, planDetailTask, lstLoanExtraTask);
                if (loanDetailTask.Result == null)
                {
                    _logger.LogError($"CaculateMoneyForLoanAffterCutPeriodCommandHandle_Not_found_loan|request={_common.ConvertObjectToJSonV2(request)}");
                    return resut;
                }
                if (lstPaymentScheduleTask.Result == null)
                {
                    _logger.LogError($"CaculateMoneyForLoanAffterCutPeriodCommandHandle_Not_found_LstPaymentSchedule|request={_common.ConvertObjectToJSonV2(request)}");
                    return resut;
                }
                var lstLoanExtra = lstLoanExtraTask.Result.ToList();
                if (lstLoanExtra == null)
                {
                    _logger.LogError($"CaculateMoneyForLoanAffterCutPeriodCommandHandle_Not_found_LstLoanExtra_Warnig|request={_common.ConvertObjectToJSonV2(request)}");
                    lstLoanExtra = new List<Domain.Tables.TblLoanExtra>();
                }
                var loanDetail = loanDetailTask.Result.FirstOrDefault();
                var lstPaymentSchedule = lstPaymentScheduleTask.Result.ToList();
                var lenderInfo = (await _lenderTab.SetGetTop(1).WhereClause(x => x.LenderID == loanDetail.LenderID).QueryAsync()).FirstOrDefault();
                var actionTaskGetMoneyCloseLoan = _loanManager.GetMoneyCloseLoanV3(currentDate, loanDetail, lstPaymentSchedule, lstLoanExtra, lenderInfo);
                var actionTaskGetMoneyNeedPay = _loanManager.GetMoneyNeedPayInPeriod(currentDate, loanDetail, lstPaymentSchedule, lstLoanExtra, lenderInfo);
                await Task.WhenAll(actionTaskGetMoneyCloseLoan, actionTaskGetMoneyNeedPay);
                if (actionTaskGetMoneyCloseLoan.Result == null)
                {
                    return resut;
                }
                MoneyNeedCloseLoan actionResultCloseLoan = actionTaskGetMoneyCloseLoan.Result;
                MoneyNeedCloseLoan actionResultNeePay = null;
                if (actionTaskGetMoneyNeedPay != null)
                {
                    actionResultNeePay = actionTaskGetMoneyNeedPay.Result;
                }

                var detail = planDetailTask.Result.FirstOrDefault();
                if (detail == null)
                {
                    _logger.LogError($"CaculateMoneyForLoanAffterCutPeriodCommandHandler_plancloseloan_not_found_waring|request={_common.ConvertObjectToJSonV2(request)}|CloseDate={currentDate}");
                    // thêm mới
                    var insertDetail = new Domain.Tables.TblPlanCloseLoan
                    {
                        CloseDate = currentDate,
                        LoanID = loanDetail.LoanID,
                        CreateDate = currentDate,
                        MoneyConsultant = actionResultCloseLoan.MoneyConsultant,
                        MoneyFineLate = actionResultCloseLoan.MoneyFineLate,
                        MoneyFineOriginal = actionResultCloseLoan.MoneyFineOriginal,
                        MoneyInterest = actionResultCloseLoan.MoneyInterest,
                        MoneyOriginal = actionResultCloseLoan.MoneyOriginal,
                        MoneyService = actionResultCloseLoan.MoneyService,
                        NextDate = loanDetail.NextDate.Value,
                        TotalMoneyClose = actionResultCloseLoan.TotalMoneyCloseLoan,
                        TotalMoneyReceivables = actionResultCloseLoan.TotalMoneyReceivables,
                        TotalMoneyNeedPay = -1, // không tính được tiền phải cắt kỳ,
                        ModifyDate = DateTime.Now
                    };
                    if (actionResultNeePay != null)
                    {
                        if (actionResultCloseLoan.OverDate > TimaSettingConstant.MaxDaySentInsurance)
                        {
                            insertDetail.TotalMoneyNeedPay = actionResultCloseLoan.TotalMoneyCloseLoan;
                            insertDetail.MoneyConsultantNeePay = actionResultCloseLoan.MoneyConsultant;
                            insertDetail.MoneyFineLateNeedPay = actionResultCloseLoan.MoneyFineLate;
                            insertDetail.MoneyInterestNeedPay = actionResultCloseLoan.MoneyInterest;
                            insertDetail.MoneyOriginalNeedPay = actionResultCloseLoan.MoneyOriginal;
                            insertDetail.MoneyServiceNeedPay = actionResultCloseLoan.MoneyService;
                        }
                        else if (actionResultCloseLoan.OverDate < 0)
                        {
                            // các giá trị cần phải đóng trong kỳ sẽ trả về 0
                            insertDetail.TotalMoneyNeedPay = 0;
                        }
                        else
                        {
                            insertDetail.TotalMoneyNeedPay = actionResultNeePay.TotalMoneyCloseLoan;
                            insertDetail.MoneyConsultantNeePay = actionResultNeePay.MoneyConsultant;
                            insertDetail.MoneyFineLateNeedPay = actionResultNeePay.MoneyFineLate;
                            insertDetail.MoneyInterestNeedPay = actionResultNeePay.MoneyInterest;
                            insertDetail.MoneyOriginalNeedPay = actionResultNeePay.MoneyOriginal;
                            insertDetail.MoneyServiceNeedPay = actionResultNeePay.MoneyService;
                        }
                    }
                    _ = await _planCloseLoanTab.InsertAsync(insertDetail);
                }
                else
                {
                    // cập nhật giá trị mới tại ngày tính
                    detail.MoneyConsultant = actionResultCloseLoan.MoneyConsultant;
                    detail.MoneyFineLate = actionResultCloseLoan.MoneyFineLate;
                    detail.MoneyFineOriginal = actionResultCloseLoan.MoneyFineOriginal;
                    detail.MoneyInterest = actionResultCloseLoan.MoneyInterest;
                    detail.MoneyOriginal = actionResultCloseLoan.MoneyOriginal;
                    detail.MoneyService = actionResultCloseLoan.MoneyService;
                    detail.NextDate = loanDetail.NextDate.Value;
                    detail.TotalMoneyClose = actionResultCloseLoan.TotalMoneyCloseLoan;
                    detail.TotalMoneyReceivables = actionResultCloseLoan.TotalMoneyReceivables;
                    if (actionResultNeePay != null)
                    {
                        if (actionResultCloseLoan.OverDate > TimaSettingConstant.MaxDaySentInsurance)
                        {
                            detail.TotalMoneyNeedPay = actionResultCloseLoan.TotalMoneyCloseLoan;
                            detail.MoneyConsultantNeePay = actionResultCloseLoan.MoneyConsultant;
                            detail.MoneyFineLateNeedPay = actionResultCloseLoan.MoneyFineLate;
                            detail.MoneyInterestNeedPay = actionResultCloseLoan.MoneyInterest;
                            detail.MoneyOriginalNeedPay = actionResultCloseLoan.MoneyOriginal;
                            detail.MoneyServiceNeedPay = actionResultCloseLoan.MoneyService;
                        }
                        else if (actionResultCloseLoan.OverDate < 0)
                        {
                            // các giá trị cần phải đóng trong kỳ sẽ trả về 0
                            detail.TotalMoneyNeedPay = 0;
                        }
                        else
                        {
                            detail.TotalMoneyNeedPay = actionResultNeePay.TotalMoneyCloseLoan;
                            detail.MoneyConsultantNeePay = actionResultNeePay.MoneyConsultant;
                            detail.MoneyFineLateNeedPay = actionResultNeePay.MoneyFineLate;
                            detail.MoneyInterestNeedPay = actionResultNeePay.MoneyInterest;
                            detail.MoneyOriginalNeedPay = actionResultNeePay.MoneyOriginal;
                            detail.MoneyServiceNeedPay = actionResultNeePay.MoneyService;
                        }
                    }
                    detail.CountChange += 1;
                    detail.ModifyDate = DateTime.Now;
                    _ = await _planCloseLoanTab.UpdateAsync(detail);
                }
                resut.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"CaculateMoneyForLoanAffterCutPeriodCommandHandle|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return resut;
        }
    }
}
