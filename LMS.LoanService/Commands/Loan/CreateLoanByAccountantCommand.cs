﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.APIAutoDisbursement;
using LMS.LoanServiceApi.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class CreateLoanByAccountantCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int LoanCreditID { get; set; }
        public long MoneyfeeInsuranceOfCustomer { get; set; }
        public long MoneyfeeInsuranceMaterialCovered { get; set; }
        public long TotalMoneyDisbursment { get; set; }
        public string Otp { get; set; }
        public long BankFee { get; set; }
        public long BankCardID { get; set; }
        public long CreateBy { get; set; }
        public string Note { get; set; }
    }
    public class CreateLoanByAccountantCommandHandler : IRequestHandler<CreateLoanByAccountantCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCallApi> _logCallApi;
        ILoanManager _loanManager;
        RestClients.IOutGatewayService _outGateway;
        RestClients.IInvoiceService _invoiceService;
        IPaymentGatewayService _paymentGatewayService;
        ILogger<CreateLoanByAccountantCommandHandler> _logger;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> _settingKeyTab;
        LMS.Common.Helper.Utils _common;
        ILoanEvenManager _loanEventManager;
        public CreateLoanByAccountantCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCallApi> logCallApi,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> settingKeyTab,
            ILoanManager loanManager,
            RestClients.IOutGatewayService outGateway,
            RestClients.IInvoiceService invoiceService,
            IPaymentGatewayService paymentGatewayService,
            ILoanEvenManager loanEventManager,
            ILogger<CreateLoanByAccountantCommandHandler> logger
            )
        {
            _loanTab = loanTab;
            _logCallApi = logCallApi;
            _loanManager = loanManager;
            _outGateway = outGateway;
            _invoiceService = invoiceService;
            _paymentGatewayService = paymentGatewayService;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _settingKeyTab = settingKeyTab;
            _loanEventManager = loanEventManager;
        }
        public async Task<ResponseActionResult> Handle(CreateLoanByAccountantCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var keySettingInfoTask = _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.System_SwitchKafkaHandle).QueryAsync();
            // todo:
            // kiểm tra lại tiền phí bảo hiểm từ KT truyền lên
            // check lại thông tin bên LOS với phí bảo hiểm
            // check TotalMoneyDisbursment với los có khớp không

            // kiểm tra đơn đã tạo chưa
            var loanLmsDetailExistTask = _loanTab.WhereClause(x => x.LoanCreditIDOfPartner == request.LoanCreditID).QueryAsync();
            await Task.WhenAll(keySettingInfoTask, loanLmsDetailExistTask);

            var loanLmsDetailExist = loanLmsDetailExistTask.Result.FirstOrDefault();
            var keySettingInfo = keySettingInfoTask.Result.FirstOrDefault();

            if (loanLmsDetailExist != null && loanLmsDetailExist.LoanID > 0)
            {
                response.Message = MessageConstant.LoanExist;
                return response;
            }

            LMS.Entites.Dtos.LOSServices.ResultLoanWaitingDisbursementDetailLOS loanDetailLos = await _outGateway.GetLoanCreditDetailAsync(request.LoanCreditID);
            if (loanDetailLos == null && loanDetailLos.LoanBriefId < 1)
            {
                response.Message = LMS.Common.Constants.MessageConstant.LoanNotFoundLoanCreditPartner;
                return response;
            }
            long moneyfeeInsuranceOfCustomer = 0;
            long moneyfeeInsuranceMaterialCovered = 0;
            // mua bao hiểm cho khách hàng
            if (loanDetailLos.BuyInsurenceCustomer)
            {
                //moneyfeeInsuranceOfCustomer = objBase.GetMoneyFeeInsuranceOfCustomer(insuranceMoney);
                //if (moneyfeeInsuranceOfCustomer == -1)
                //{
                //    return Json(new { Error = 1, Message = "Chưa tính được phí bảo hiểm sức khỏe của đơn vay" });
                //}
            }

            if (loanDetailLos.BuyInsuranceProperty)
            {
                //moneyfeeInsuranceMaterialCovered = objBase.GetMoneyInsuranceOfMaterialCovered(insuranceMoney);
                //if (moneyfeeInsuranceMaterialCovered == -1)
                //{
                //    //return Json(new { Error = 1, Message = "Chưa tính được phí bảo hiểm vật chất của đơn vay" });
                //}
            }
            if (!string.IsNullOrEmpty(request.Otp))
            {
                #region xử lý otp
                var lstlogCallApi = _logCallApi.WhereClause(x => x.LoanCreditID == request.LoanCreditID && x.ActionCallApi == (int)LogCallApi_ActionCallApi.PreCreateTransaction && x.Status == (int)LogCallApi_StatusCallApi.WaitConfirm).Query();
                if (lstlogCallApi == null || !lstlogCallApi.Any())
                {
                    response.Message = LMS.Common.Constants.MessageConstant.LoanCreditNotYetSentOTP;
                    return response;
                }
                var detail = lstlogCallApi.OrderByDescending(x => x.LogCallApiID).First();
                var transactionGetOTP = _paymentGatewayService.ReconciliationInternalGetOTP(new Entites.Dtos.APIAutoDisbursement.ReconciliationInternalRequest
                {
                    TraceIndentifier = detail.TraceIndentifierResponse
                });
                if (transactionGetOTP == null)
                {
                    response.Message = LMS.Common.Constants.MessageConstant.LoanCreditNotGetTransionRequestOTPFromAI;
                    return response;
                }
                if (transactionGetOTP.Code != (int)VIB_AI_ResponseCodeConstant.SUCCESS_CODE)
                {
                    // clear log call
                    _ = Task.Run(() =>
                    {
                        foreach (var item in lstlogCallApi)
                        {
                            _ = _paymentGatewayService.UpdateStatusLogCallApi(item.TraceIndentifierRequest, (int)LogCallApi_StatusCallApi.Error);
                        }
                    });
                    response.Message = transactionGetOTP.Messages;
                    return response;
                }
                // clear log call
                _ = Task.Run(() =>
                {
                    _ = _paymentGatewayService.UpdateStatusLogCallApi(detail.TraceIndentifierRequest, (int)LogCallApi_StatusCallApi.Success);
                    foreach (var item in lstlogCallApi)
                    {
                        if (item.TraceIndentifierRequest != detail.TraceIndentifierRequest)
                        {
                            _ = _paymentGatewayService.UpdateStatusLogCallApi(item.TraceIndentifierRequest, (int)LogCallApi_StatusCallApi.Error);
                        }
                    }
                });
                string traceIndentiferRequestVerifyOtp = _common.GenTraceIndentifier();
                var resultSentOtp = await _paymentGatewayService.VibExecuteOtp(new Entites.Dtos.APIAutoDisbursement.ExecuteOtpRequest
                {
                    LoanCreditID = request.LoanCreditID,
                    Otp = request.Otp,
                    TraceIndentifier = traceIndentiferRequestVerifyOtp,
                    TransIDRequest = transactionGetOTP.Data.TransIDRequest ?? transactionGetOTP.Data.TransID,
                    TransIDVerify = transactionGetOTP.Data.TransIDVerify,
                    ExecuteBy = 1
                });
                if (resultSentOtp == null)
                {
                    response.Message = LMS.Common.Constants.MessageConstant.LoanCreditNotSureSentVerifyOTP;
                    return response;
                }
                switch ((VIB_AI_ResponseCodeConstant)resultSentOtp.Code)
                {
                    case VIB_AI_ResponseCodeConstant.SUCCESS_CODE:
                        break;
                    case VIB_AI_ResponseCodeConstant.FAIL_TRANSACTION_NOT_EXITS:
                    case VIB_AI_ResponseCodeConstant.FAIL_TRANSACTION_TIME_OUT:
                    case VIB_AI_ResponseCodeConstant.FAIL_OTP_NOT_VALID:
                    case VIB_AI_ResponseCodeConstant.FAIL_BANK_IS_PROCESSING:
                    case VIB_AI_ResponseCodeConstant.FAIL_BANK_NOT_ENOUGH:
                    case VIB_AI_ResponseCodeConstant.FAIL_EXECUTE:
                        response.Message = string.IsNullOrEmpty(resultSentOtp.Messages) ? ((VIB_AI_ResponseCodeConstant)resultSentOtp.Code).GetDescription() : resultSentOtp.Messages;
                        return response;
                    default:
                        response.Message = MessageConstant.LoanCreditNotSureSentVerifyOTP;
                        return response;
                }
                #endregion
            }
            else
            {
                // check đơn có khóa không, có khóa yêu cầu mở khóa
                if (loanDetailLos.IsLock)
                {
                    response.Message = MessageConstant.LoanCreditIsLock;
                    return response;
                }
            }
            if (request.MoneyfeeInsuranceMaterialCovered != moneyfeeInsuranceMaterialCovered)
            {
                // trả về cho client;
            }
            if (request.MoneyfeeInsuranceOfCustomer != moneyfeeInsuranceOfCustomer)
            {
                // trả về cho client;
            }
            if (keySettingInfo != null && keySettingInfo.Status == (int)StatusCommon.Active)
            {
                response = await _loanEventManager.CreateLoanAsync(new Domain.Models.Loan.RequestCreateLoanModel
                {
                    BankCardID = request.BankCardID,
                    CreateBy = request.CreateBy,
                    LoanCreditID = request.LoanCreditID,
                    LoanAgID = 0,
                    SourceBankDisbursement = (int)Loan_SourceMoneyDisbursement.EscrowSource,
                    MoneyfeeInsuranceOfCustomer = request.MoneyfeeInsuranceOfCustomer,
                    MoneyFeeInsuranceMaterialCovered = request.MoneyfeeInsuranceMaterialCovered,
                    IsHandleException = 0,
                    AgCodeID = 0,
                    MoneyFeeDisbursement = request.BankFee,
                    FromDate = DateTime.Now,
                    TimaCustomerID = 0
                });
            }
            else
            {
                response = await _loanManager.CreateLoanAsync(new Domain.Models.Loan.RequestCreateLoanModel
                {
                    BankCardID = request.BankCardID,
                    CreateBy = request.CreateBy,
                    LoanCreditID = request.LoanCreditID,
                    MoneyFeeInsuranceMaterialCovered = request.MoneyfeeInsuranceMaterialCovered,
                    MoneyfeeInsuranceOfCustomer = request.MoneyfeeInsuranceOfCustomer,
                    SourceBankDisbursement = (int)Loan_SourceMoneyDisbursement.EscrowSource,
                    Note = request.Note,
                    MoneyFeeDisbursement = request.BankFee
                });
            }

            if (response.Result == (int)ResponseAction.Success)
            {
                //long loanID = Convert.ToInt64(response.Data);
                //if (request.BankFee > 0)
                //{
                //    // todo: ghi nhận phí trừ tiền đến thẻ
                //    _ = _invoiceService.CreateInvoiceBankInterest(request.BankFee, request.Note, DateTime.Now, request.BankCardID, TimaSettingConstant.ShopIDTima, request.CreateBy, (int)GroupInvoiceType.PaySlipBankFee);
                //}
            }
            return response;
        }
    }
}
