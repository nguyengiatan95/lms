﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Models.Insurance;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class GetOtpDisburmentForLoanCreditCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanCreditID { get; set; }
    }
    public class GetOtpDisburmentForLoanCreditCommandHandler : IRequestHandler<GetOtpDisburmentForLoanCreditCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBank> _bankTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> _settingKeyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCallApi> _logCallApi;
        Services.IPaymentGatewayService _paymentGatewayService;
        RestClients.ILOSService _losManager;
        RestClients.IInsuranceService _insuranceService;
        LMS.Common.Helper.Utils _common;
        ILogger<GetOtpDisburmentForLoanCreditCommandHandler> _logger;
        public GetOtpDisburmentForLoanCreditCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblBank> bankTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> settingKeyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCallApi> logCallApi,
            Services.IPaymentGatewayService paymentGatewayService,
            RestClients.ILOSService losManager,
            RestClients.IInsuranceService insuranceService,
            ILogger<GetOtpDisburmentForLoanCreditCommandHandler> logger)
        {
            _bankTab = bankTab;
            _lenderTab = lenderTab;
            _settingKeyTab = settingKeyTab;
            _logCallApi = logCallApi;
            _paymentGatewayService = paymentGatewayService;
            _losManager = losManager;
            _insuranceService = insuranceService;
            _common = new Common.Helper.Utils();
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(GetOtpDisburmentForLoanCreditCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var settingKeyInfo = _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.Loan_ProcessAutoDisbursement).Query().FirstOrDefault();
                // tắt không chạy tự động, không ghi nhận sms
                if (settingKeyInfo != null && settingKeyInfo.Status == (int)SettingKey_Status.Active)
                {
                    response.Message = MessageConstant.SettingKey_Loan_ProcessAutoDisbursementRunning;
                    return response;
                }
                var loanDetailLos = await _losManager.GetLoanCreditDetail(request.LoanCreditID);
                if (loanDetailLos == null || loanDetailLos.LoanBriefId < 0)
                {
                    response.Message = MessageConstant.LoanNotFoundLoanCreditPartner;
                    return response;
                }
                var statusAg = _losManager.ConvertStatusLoanDisbursementFromLOSToAG(loanDetailLos.Status);
                if (statusAg != (int)StateLoanCreditNew.AccountantDisbursement)
                {
                    response.Message = MessageConstant.LoanCreditDisbusementStatusByAccountantNotValid;
                    return response;
                }
                // todo:
                // check backlist
                // check topup
                // check đơn có khóa trước đó hay ko

                if (loanDetailLos.IsLock)
                {
                    response.Message = MessageConstant.LoanCreditIsLock;
                    return response;
                }
                // check log call api lấy otp có làm liên tục ko
                var lstlogCallApi = _logCallApi.SetGetTop(1).WhereClause(x => x.LoanCreditID == request.LoanCreditID).OrderByDescending(x => x.LogCallApiID).Query();
                if (lstlogCallApi != null && lstlogCallApi.Any())
                {
                    var detailLogCallApi = lstlogCallApi.First();
                    switch ((LogCallApi_ActionCallApi)detailLogCallApi.ActionCallApi)
                    {
                        case LogCallApi_ActionCallApi.PreCreateTransaction:
                            response.Message = "Xin chờ trong giây lát, hệ thống đang chờ nhận otp.";
                            return response;
                        case LogCallApi_ActionCallApi.ExecuteCreateTransaction:
                            var lstStatusAllowGetOtp = new List<int> { (int)LogCallApi_StatusCallApi.Error, (int)LogCallApi_StatusCallApi.Success };
                            if (!lstStatusAllowGetOtp.Contains(detailLogCallApi.Status))
                            {
                                response.Message = "Xin chờ trong giây lát, hệ thống đang chờ nhận otp.";
                                return response;
                            }
                            else if (detailLogCallApi.Status == (int)LogCallApi_StatusCallApi.Success)
                            {
                                // check time gọi quá otp chưa - cho 5phut 
                                if (Convert.ToInt32(DateTime.Now.Subtract(detailLogCallApi.ModifyDate).TotalMinutes) < LoanService.Helper.CommonLoanServiceApi.TimeWaitingReceivedOTP)
                                {
                                    response.Message = "Otp đã được gửi và còn thời hạn hiệu lực. Chờ qua 3 phút hãy lấy lại otp.";
                                    return response;
                                }
                            }
                            break;
                    }
                }
                var objLender = _lenderTab.WhereClause(x => x.LenderID == loanDetailLos.LenderId).Query().FirstOrDefault();
                if (objLender == null)
                {
                    response.Message = MessageConstant.NotIdentifiedLenderID;
                    return response;
                }

                if (loanDetailLos.ReceivingMoneyType != (int)StateTypeReceivingMoney.NumberAccount || string.IsNullOrEmpty(loanDetailLos.BankAccountNumber))
                {
                    response.Message = MessageConstant.NotBankAccountNumber;
                    return response;
                }

                var rateVND = _common.ConvertRateToVND(loanDetailLos.RatePercent);
                var rateInterest = _common.ConvertRateInterestLenderToVND((float)objLender.RateInterest);
                var rateConsultant = rateVND - rateInterest;
                var rateService = 0;
                var rateType = loanDetailLos.RateTypeId;

                var calculateMoneyInsuranceLOS = new CalculateMoneyInsuranceLOSReq
                {
                    TotalMoneyDisbursement = Convert.ToInt64(loanDetailLos.LoanAmountFinal),
                    LoanTime = _common.ConvertLoanTimeLOSToLMS(loanDetailLos.LoanTime),
                    LoanFrequency = _common.ConvertFrequencyLOSToLMS(loanDetailLos.Frequency),
                    RateType = rateType,
                    RateConsultant = rateConsultant,
                    RateInterest = rateInterest,
                    RateService = rateService
                };
                long moneyfeeInsuranceOfCustomer = 0;
                long moneyfeeInsuranceMaterialCovered = 0;
                var insuranceFee = await _insuranceService.CalculateMoneyInsuranceLOS(calculateMoneyInsuranceLOS);
                if (insuranceFee == null)
                {
                    response.Message = MessageConstant.LoanCreditCaculatedInsurranceNotSuccess;
                    return response;
                }
                // mua bao hiểm cho khách hàng
                if (loanDetailLos.BuyInsurenceCustomer)
                {
                    moneyfeeInsuranceOfCustomer = Convert.ToInt64(insuranceFee.FeesInsuranceCustomer);
                }

                if (loanDetailLos.BuyInsuranceProperty)
                {
                    moneyfeeInsuranceMaterialCovered = Convert.ToInt64(insuranceFee.FeesInsuranceMaterial);
                }
                var moneyCustomerRecieve = loanDetailLos.LoanAmountFinal - (moneyfeeInsuranceOfCustomer + moneyfeeInsuranceMaterialCovered);
                if (moneyCustomerRecieve < Constants.LOSConstants.MoneyCustomerRecieve)
                {
                    response.Message = MessageConstant.ErrormoneyCustomerRecieve;
                    return response;
                }
                bool resultLockLoan = false;
                var mess = $"Đơn khóa do chuẩn bị giải ngân.";
                _ = _losManager.SaveCommentHistory(new Entites.Dtos.LOSServices.ReqSaveCommentHistory
                {
                    LoanBriefId = request.LoanCreditID,
                    Note = mess,
                    UserId = TimaSettingConstant.UserIDAdmin,
                    UserName = TimaSettingConstant.UserNameAutoDisbursement
                });
                var resultLockLoanRespone = _losManager.LockLoan(new Entites.Dtos.LOSServices.LockLoanReq
                {
                    LoanId = request.LoanCreditID,
                    Note = mess,
                    Type = (int)LOS_Enum_LockLoanType.Lock,
                    UserName = TimaSettingConstant.UserNameAutoDisbursement
                }).Result;
                if (resultLockLoanRespone != null && resultLockLoanRespone.Result == (int)ResponseAction.Success)
                {
                    resultLockLoan = true;
                }
                if (!resultLockLoan)
                {
                    response.Message = MessageConstant.LoanCreditLockNotSuccess;
                    return response;
                }
                var bankInfo = _bankTab.WhereClause(x => x.Id == loanDetailLos.BankId).Query().FirstOrDefault();
                var objCreateTransaction = new LMS.Entites.Dtos.APIAutoDisbursement.CreateTransactionRequest
                {
                    BankAccountNumber = loanDetailLos.BankAccountNumber,
                    BankValue = $"{bankInfo?.ValueOfBank}",
                    Name = _common.UnicodeToPlain(loanDetailLos.FullName.ToUpper()), // common.UnicodeToPlain(objCustomerCredit.FullName.ToUpper());
                    Amount = Convert.ToInt64(moneyCustomerRecieve), //objLoanCredit.TotalMoney;
                    Mess = _common.UnicodeToPlain(string.Format(TimaSettingConstant.SystemDisbursementToCustomer, loanDetailLos.FullName.ToUpper())),
                    TraceIndentifier = _common.GenTraceIndentifier(),
                    LoanCreditID = request.LoanCreditID
                };
                var objResult = await _paymentGatewayService.VibCreateTransaction(objCreateTransaction, (int)request.LoanCreditID);
                if (objResult != null)
                {
                    response.SetSucces();
                    response.Data = objResult.TraceIndentifier;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetOtpDisburmentForLoanCreditCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
