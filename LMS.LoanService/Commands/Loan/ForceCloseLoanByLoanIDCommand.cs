﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class ForceCloseLoanByLoanIDCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public string CloseDate { get; set; }
        public long CreateBy { get; set; }
    }
    public class ForceCloseLoanByLoanIDCommandHandler : IRequestHandler<ForceCloseLoanByLoanIDCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        Services.ILoanManager _loanManager;
        RestClients.IPaymentScheduleService _paymentService;
        RestClients.ICustomerService _customerService;
        LMS.Common.Helper.Utils _common;
        ILogger<ForceCloseLoanByLoanIDCommandHandler> _logger;
        public ForceCloseLoanByLoanIDCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            RestClients.ICustomerService customerService,
            RestClients.IPaymentScheduleService paymentService,
            Services.ILoanManager loanManager,
            ILogger<ForceCloseLoanByLoanIDCommandHandler> logger)
        {
            _loanTab = loanTab;
            _customerTab = customerTab;
            _customerService = customerService;
            _paymentService = paymentService;
            _logger = logger;
            _loanManager = loanManager;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(ForceCloseLoanByLoanIDCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var loanInfo = (await _loanTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                if (loanInfo == null || loanInfo.LoanID == 0)
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                if (loanInfo.Status != (int)Loan_Status.Lending)
                {
                    response.Message = MessageConstant.LoanCantProcess;
                    return response;
                }
                if (loanInfo.StatusSendInsurance == (int)Loan_StatusSendInsurance.Send)
                {
                    response.Message = MessageConstant.LoanSentInsurance;
                    return response;
                }
                if (loanInfo.StatusSendInsurance == (int)Loan_StatusSendInsurance.ProcessingComplete)
                {
                    response.Message = MessageConstant.LoanInsurancePaid;
                    return response;
                }
                var closeDate = DateTime.ParseExact(request.CloseDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                var customerInfo = (await _customerTab.WhereClause(x => x.CustomerID == loanInfo.CustomerID).QueryAsync()).FirstOrDefault();
                var moneyCloseLoan = await _loanManager.GetMoneyCloseLoan(request.LoanID, closeDate);
                long totalMoneyNeedPay = moneyCloseLoan.MoneyOriginal + moneyCloseLoan.MoneyInterest + moneyCloseLoan.MoneyConsultant
                    + moneyCloseLoan.MoneyFineLate + moneyCloseLoan.MoneyService + moneyCloseLoan.MoneyFineOriginal;

                if (customerInfo.TotalMoney < totalMoneyNeedPay)
                {
                    response.Message = MessageConstant.PaymentNotEnoughMoney;
                    return response;
                }
                var actionResultPayment = await _paymentService.CloseLoanFromSchedule(loanInfo.LoanID, moneyCloseLoan.MoneyOriginal, moneyCloseLoan.MoneyService, moneyCloseLoan.MoneyInterest, moneyCloseLoan.MoneyConsultant, moneyCloseLoan.MoneyFineOriginal, moneyCloseLoan.MoneyFineLate, closeDate, request.CreateBy);
                if (actionResultPayment.Result == (int)ResponseAction.Success)
                {
                    long totalMoneyCustomerPaid = Convert.ToInt64(actionResultPayment.Data);
                    if (totalMoneyCustomerPaid != totalMoneyNeedPay)
                    {
                        // ghi log lại để check lại
                        _logger.LogError($"ForceCloseLoanByLoanIDCommandHandler|moneyCloseLoan={_common.ConvertObjectToJSonV2(moneyCloseLoan)}|request={_common.ConvertObjectToJSonV2(request)}|moneyPaidLMS={totalMoneyCustomerPaid}");
                    }
                    _ = _customerService.UpdateMoneyCustomer((long)loanInfo.CustomerID, totalMoneyCustomerPaid * -1, "Tất toán hợp đồng");
                    if (moneyCloseLoan.MoneyOriginal > 0)
                    {
                        _ = _loanManager.CreateLoanExtraOriginal(new Domain.Models.Loan.LoanExtraInsertDetailModel
                        {
                            CreateBy = request.CreateBy,
                            LoanID = loanInfo.LoanID,
                            PayDate = closeDate,
                            TotalMoney = moneyCloseLoan.MoneyOriginal * -1,
                            TypeMoney = (int)Transaction_TypeMoney.Original
                        });
                    }
                    loanInfo.Status = (int)Loan_Status.Close;
                    loanInfo.ModifyDate = DateTime.Now;
                    loanInfo.TotalMoneyCurrent = 0;
                    loanInfo.FinishDate = DateTime.Now;
                    if (moneyCloseLoan.MoneyFineLate > 0)
                    {
                        loanInfo.MoneyFineLate -= moneyCloseLoan.MoneyFineLate;
                    }
                    var updateLoan = await _loanTab.UpdateAsync(loanInfo);
                    if (!updateLoan)
                    {
                        _logger.LogError($"ForceCloseLoanByLoanIDCommandHandler_UpdateLoanError|request={_common.ConvertObjectToJSonV2(request)}|actionResultPayment={_common.ConvertObjectToJSonV2(actionResultPayment)}");
                    }
                    response.SetSucces();
                    response.Data = loanInfo.LoanID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ForceCloseLoanByLoanIDCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
