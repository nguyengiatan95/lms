﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class AGPayFullyScheduleInPeriodsCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanAgID { get; set; }
        public string NextDate { get; set; }
        public long TotalMoneyPaid { get; set; } // số tiền đã trả
        public long TotalMoneyCustomerHas { get; set; }
        public string Description { get; set; }
        public long CreateBy { get; set; }
        /// <summary>
        /// dùng cho thanh toán lại lần nữa khi còn tiền từ AG
        /// </summary>
        public int CountPayment { get; set; }
    }
    public class AGPayFullyScheduleInPeriodsCommandHandler : IRequestHandler<AGPayFullyScheduleInPeriodsCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.LoanUpdateAfterPayment> _loanUpdateAfterPayment;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionLoanTab;
        RestClients.IPaymentScheduleService _paymentService;
        RestClients.ICustomerService _customerService;
        LMS.Common.Helper.Utils _common;
        ILogger<AGPayFullyScheduleInPeriodsCommandHandler> _logger;
        Services.ILoanManager _loanManager;
        int _MaxPaymentContinous = 5;
        public AGPayFullyScheduleInPeriodsCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.LoanUpdateAfterPayment> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionLoanTab,
            RestClients.ICustomerService customerService,
            RestClients.IPaymentScheduleService paymentService,
            Services.ILoanManager loanManager,
            ILogger<AGPayFullyScheduleInPeriodsCommandHandler> logger)
        {
            _loanUpdateAfterPayment = loanTab;
            _customerTab = customerTab;
            _paymentScheduleTab = paymentScheduleTab;
            _transactionLoanTab = transactionLoanTab;
            _customerService = customerService;
            _paymentService = paymentService;
            _loanManager = loanManager;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(AGPayFullyScheduleInPeriodsCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var currentDate = DateTime.Now;
                var nextDate = DateTime.ParseExact(request.NextDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                var loanInfo = (await _loanUpdateAfterPayment.WhereClause(x => x.TimaLoanID == request.LoanAgID).QueryAsync()).FirstOrDefault();
                var customerInfo = (await _customerTab.WhereClause(x => x.CustomerID == loanInfo.CustomerID).QueryAsync()).FirstOrDefault();

                // kiểm tra loan bên LMS có đồng bộ với AG chưa
                if (loanInfo.NextDate.Value.Date != nextDate.Date)
                {
                    response.Message = MessageConstant.LoanSyncNextDateDifferent;
                    _logger.LogError($"AGPayFullyScheduleInPeriodsCommand_Warning|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                }
                long totalMoneyPaidBefore = request.TotalMoneyPaid;
                do
                {
                    var actionResultPayment = await _paymentService.PayPaymentByLoanID(loanInfo.LoanID, nextDate, request.TotalMoneyPaid, request.CreateBy);
                    if (actionResultPayment.Result == (int)ResponseAction.Success)
                    {
                        long totalMoneyCustomerPaid = Convert.ToInt64(actionResultPayment.Data);
                        if (totalMoneyCustomerPaid != request.TotalMoneyPaid)
                        {
                            // ghi log lại để check lại
                            _logger.LogError($"AGPayFullyScheduleInPeriodsCommand_Warning_Payment|request={_common.ConvertObjectToJSonV2(request)}|moneyPaidLMS={totalMoneyCustomerPaid}");
                        }
                        _ = _customerService.UpdateMoneyCustomer((long)loanInfo.CustomerID, totalMoneyCustomerPaid * -1, request.Description);
                        request.TotalMoneyPaid -= totalMoneyCustomerPaid;
                        // trừ tiền tiếp cho đến hết
                        request.CountPayment += 1;
                    }
                    else
                    {
                        request.CountPayment += _MaxPaymentContinous;
                    }
                } while (request.TotalMoneyPaid > 0 && request.CountPayment < _MaxPaymentContinous);
                if (totalMoneyPaidBefore != request.TotalMoneyPaid)
                {
                    //19 - 03 - 2022: xử lý qua kafka
                    List<Domain.Tables.TblTransaction> lstTxnDetail = new List<Domain.Tables.TblTransaction>();
                    int countCheckTxn = 0;
                    do
                    {
                        await Task.Delay(100);
                        _logger.LogInformation($"AGPayFullyScheduleInPeriodsCommandHandler_NowBeforeQuery={DateTime.Now}|request={_common.ConvertObjectToJSonV2(request)}");
                        lstTxnDetail = (await _transactionLoanTab.SelectColumns(x => x.TotalMoney, x => x.TransactionID, x => x.MoneyType)
                                                                    .WhereClause(x => x.LoanID == loanInfo.LoanID && x.CreateDate > currentDate)
                                                                    .QueryAsync()).ToList();
                        _logger.LogInformation($"AGPayFullyScheduleInPeriodsCommandHandler_NowAfterQuery={DateTime.Now}");
                        countCheckTxn++;
                    } while (lstTxnDetail.Count == 0 && countCheckTxn < _MaxPaymentContinous);

                    long moneyOriginal = lstTxnDetail.Where(x => x.MoneyType == (int)Transaction_TypeMoney.Original).OrderByDescending(x => x.TransactionID).FirstOrDefault()?.TotalMoney ?? 0;
                    if (moneyOriginal > 0)
                    {
                        _ = _loanManager.CreateLoanExtraOriginal(new Domain.Models.Loan.LoanExtraInsertDetailModel
                        {
                            CreateBy = request.CreateBy,
                            LoanID = loanInfo.LoanID,
                            PayDate = currentDate,
                            TotalMoney = moneyOriginal * -1,
                            TypeMoney = (int)Transaction_TypeMoney.Original
                        });
                    }
                    response = await _loanManager.UpdateLoanAfterPayment(loanInfo.LoanID, request.CreateBy);
                    if (response.Result != (int)ResponseAction.Success)
                    {
                        _logger.LogError($"AGPayFullyScheduleInPeriodsCommand_UpdateLoanFail|request={_common.ConvertObjectToJSonV2(request)}|loanUpdateAfterPayment={_common.ConvertObjectToJSonV2(loanInfo)}");
                    }
                }
                response.SetSucces();
                response.Data = loanInfo.LoanID;
            }
            catch (Exception ex)
            {
                _logger.LogError($"AGPayFullyScheduleInPeriodsCommand|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
