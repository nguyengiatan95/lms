﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.LoanServiceApi.Commands.CutOffLoan;
using LMS.LoanServiceApi.Domain.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class ProcessCalculatorMoneyCloseLoanDailyCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class ProcessCalculatorMoneyCloseLoanDailyCommandHandler : IRequestHandler<ProcessCalculatorMoneyCloseLoanDailyCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly Services.ILoanManager _loanManager;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> _settingKeyTab;
        readonly LMS.Common.Helper.Utils _common;
        readonly ILogger<ProcessCalculatorMoneyCloseLoanDailyCommandHandler> _logger;
        public ProcessCalculatorMoneyCloseLoanDailyCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> settingKeyTab,
            Services.ILoanManager loanManager,
             ILogger<ProcessCalculatorMoneyCloseLoanDailyCommandHandler> logger)
        {
            _loanManager = loanManager;
            _settingKeyTab = settingKeyTab;
            _common = new Utils();
            _logger = logger;
        }

        public async Task<ResponseActionResult> Handle(ProcessCalculatorMoneyCloseLoanDailyCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var keySettingInfo = (await _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.PlanCloseLoan_ProcessDaily).QueryAsync()).FirstOrDefault();
                if (keySettingInfo == null)
                {
                    keySettingInfo = new TblSettingKey()
                    {
                        Status = (int)SettingKey_Status.Active,
                        CreateDate = DateTime.Now,
                        Value = _common.ConvertObjectToJSonV2(new SettingKeyValuePlanCloseLoan
                        {
                            CloseDate = DateTime.Now.AddDays(-1)
                        }),
                        KeyName = LMS.Common.Constants.SettingKey_KeyValue.PlanCloseLoan_ProcessDaily,
                        ModifyDate = DateTime.Now
                    };
                    keySettingInfo.SettingKeyID = await _settingKeyTab.InsertAsync(keySettingInfo);
                }
                if (keySettingInfo.Status == (int)SettingKey_Status.InActice)
                {
                    response.Message = MessageConstant.SettingKey_PlanCloseLoan_ProcessDailyStatus;
                    return response;
                }
                SettingKeyValuePlanCloseLoan settingKeyValue = new SettingKeyValuePlanCloseLoan() { CloseDate = DateTime.Now.AddDays(-1) };
                if (!string.IsNullOrEmpty(keySettingInfo.Value))
                {
                    try
                    {
                        settingKeyValue = _common.ConvertJSonToObjectV2<SettingKeyValuePlanCloseLoan>(keySettingInfo.Value);
                    }
                    catch (Exception)
                    {
                        settingKeyValue = new SettingKeyValuePlanCloseLoan
                        {
                            CloseDate = DateTime.Now.AddDays(-1)
                        };
                        keySettingInfo.Value = _common.ConvertObjectToJSonV2(settingKeyValue);
                    }
                }
                if (settingKeyValue.CloseDate.Date == DateTime.Now.Date)
                {
                    response.Message = MessageConstant.SettingKey_PlanCloseLoan_ProcessDailyFinished;
                    return response;
                }
                keySettingInfo.Status = (int)SettingKey_Status.InActice;
                keySettingInfo.ModifyDate = DateTime.Now;
                _ = RunUntilFinished(keySettingInfo, settingKeyValue.LatestLoanID);
                _ = await _settingKeyTab.UpdateAsync(keySettingInfo);
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessCalculatorMoneyCloseLoanDailyCommandHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        private async Task RunUntilFinished(Domain.Tables.TblSettingKey keySettingInfo, long latestLoanID)
        {
            try
            {
                var actionResult = await _loanManager.ProcessPlanCalculatorMoneyCloseLoanDaily(latestLoanID, DateTime.Now);
                if (actionResult.Result == (int)ResponseAction.Success)
                {
                    var settingKeyValue = _common.ConvertJSonToObjectV2<SettingKeyValuePlanCloseLoan>(keySettingInfo.Value);
                    latestLoanID = Convert.ToInt64(actionResult.Data);
                    settingKeyValue.CountTimeHandleFinished = Convert.ToInt64(DateTime.Now.Subtract(keySettingInfo.ModifyDate).TotalMilliseconds);
                    settingKeyValue.LatestLoanID = latestLoanID;
                    if (latestLoanID == 0)
                    {
                        keySettingInfo.Status = (int)SettingKey_Status.Active;
                        keySettingInfo.ModifyDate = DateTime.Now;
                        settingKeyValue.CloseDate = DateTime.Now.Date;
                        keySettingInfo.Value = _common.ConvertObjectToJSonV2(settingKeyValue);
                        _ = await _settingKeyTab.UpdateAsync(keySettingInfo);
                        return;
                    }
                    keySettingInfo.Value = _common.ConvertObjectToJSonV2(settingKeyValue);
                    _ = await _settingKeyTab.UpdateAsync(keySettingInfo);
                }
                _ = RunUntilFinished(keySettingInfo, latestLoanID);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessCalculatorMoneyCloseLoanDailyCommandHandler_RunUntilFinished|ex={ex.Message}-{ex.StackTrace}");
            }
        }
    }
}
