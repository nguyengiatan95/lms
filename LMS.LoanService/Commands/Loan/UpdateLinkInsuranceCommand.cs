﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Models;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class UpdateLinkInsuranceCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public int TypeInsurance { get; set; }
        [Required]
        public string Path { get; set; }
        [Required]
        public long LoanID { get; set; }
    }
    public class UpdateLinkInsuranceCommandHandler : IRequestHandler<UpdateLinkInsuranceCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        ILogger<UpdateLinkInsuranceCommandHandler> _logger;
        Common.Helper.Utils _common;
        public UpdateLinkInsuranceCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            ILogger<UpdateLinkInsuranceCommandHandler> logger)
        {
            _loanTab = loanTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(UpdateLinkInsuranceCommand request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                try
                {
                    ResponseActionResult response = new ResponseActionResult();
                    var objLoan = _loanTab.WhereClause(x => x.LoanID == request.LoanID).Query().FirstOrDefault();
                    if (objLoan == null)
                    {
                        response.Message = MessageConstant.LoanNotExist;
                        return response;
                    }
                    if (request.TypeInsurance == (int)LMS.Common.Constants.Insurance_TypeInsurance.Customer)
                        objLoan.LinkGCNVay = request.Path;
                    else if (request.TypeInsurance == (int)LMS.Common.Constants.Insurance_TypeInsurance.Material)
                        objLoan.LinkInsuranceMaterial = request.Path;
                    else if (request.TypeInsurance == (int)LMS.Common.Constants.Insurance_TypeInsurance.Lender)
                        objLoan.LinkGCNChoVay = request.Path;

                    var update = _loanTab.Update(objLoan);
                    if (update)
                    {
                        response.SetSucces();
                    }
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"UpdateLoanAfterPaymentCommandHandler|request={_common.ConvertObjectToJSonV2<UpdateLinkInsuranceCommand>(request)}|ex={ex.Message}");
                    return null;
                }
            });
        }
    }
}
