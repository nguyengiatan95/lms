﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.LoanServiceApi.Domain.Tables;
using LMS.LoanServiceApi.RestClients;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class ProcessMoneyFineLateCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class ProcessMoneyFineLateCommandHandler : IRequestHandler<ProcessMoneyFineLateCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> _settingKeyTab;
        readonly Services.ILoanManager _loanManager;
        readonly ILogger<ProcessMoneyFineLateCommandHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public ProcessMoneyFineLateCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> settingKeyTab,
            Services.ILoanManager loanManager,
            ILogger<ProcessMoneyFineLateCommandHandler> logger)
        {
            _settingKeyTab = settingKeyTab;
            _loanManager = loanManager;
            _logger = logger;
            _common = new Utils();
        }
        public async Task<ResponseActionResult> Handle(ProcessMoneyFineLateCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var keySettingInfo = (await _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.Loan_ProcessFineLate).QueryAsync()).FirstOrDefault();
                if (keySettingInfo == null || keySettingInfo.Status == 0)
                {
                    response.Message = MessageConstant.SettingKey_Loan_ProcessFineLateStatus;
                    return response;
                }

                SettingKeyValueAutoFineLate keyValueAutoFine = null;
                var currentDate = DateTime.Now;
                DateTime startDate = currentDate;
                TimeSpan timeStart = new TimeSpan(22, 00, 00);
                TimeSpan? endStart = null;
                if (string.IsNullOrEmpty(keySettingInfo.Value))
                {
                    response.Message = MessageConstant.SettingKey_Loan_ProcessFineLateEmpty;
                    return response;
                }
                if (!string.IsNullOrEmpty(keySettingInfo.Value))
                {
                    try
                    {
                        keyValueAutoFine = _common.ConvertJSonToObjectV2<SettingKeyValueAutoFineLate>(keySettingInfo.Value);
                    }
                    catch (Exception)
                    {
                        keyValueAutoFine = new SettingKeyValueAutoFineLate
                        {
                            StartTime = "21:00",
                            EndTime = "23:00",
                            LatestLoanID = 0,
                            DateApplyFine = DateTime.Now
                        };
                    }
                }
                if (!string.IsNullOrEmpty(keyValueAutoFine.StartTime))
                {
                    timeStart = TimeSpan.Parse(keyValueAutoFine.StartTime);
                }
                if (!string.IsNullOrEmpty(keyValueAutoFine.EndTime))
                {
                    endStart = TimeSpan.Parse(keyValueAutoFine.EndTime);
                }
                if ((!endStart.HasValue || endStart.Value <= timeStart || DateTime.Now.TimeOfDay < timeStart || DateTime.Now.TimeOfDay > endStart.Value) && keyValueAutoFine.LatestLoanID == 0)
                {
                    response.Message = MessageConstant.SettingKey_Loan_ProcessFineLateStartTime;
                    return response;
                }

                if (keyValueAutoFine.DateApplyFine.Date > DateTime.Now.Date)
                {
                    response.Message = MessageConstant.SettingKey_Loan_ProcessFineLateStartDate;
                    return response;
                }
                var actionResult = await _loanManager.ProcessFineLate(keyValueAutoFine.DateApplyFine, keyValueAutoFine.LatestLoanID);
                response.SetSucces();
                response.Data = actionResult;
                keyValueAutoFine.LatestLoanID = actionResult.LatestLoanID;
                if (actionResult.LatestLoanID == 0)
                {
                    keyValueAutoFine.DateApplyFine = DateTime.Now.AddDays(1);
                }
                keySettingInfo.Value = _common.ConvertObjectToJSonV2(keyValueAutoFine);
                keySettingInfo.ModifyDate = currentDate;
                _ = await _settingKeyTab.UpdateAsync(keySettingInfo);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessMoneyFineLate|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
