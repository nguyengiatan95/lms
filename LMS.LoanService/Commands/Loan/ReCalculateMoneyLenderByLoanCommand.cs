﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class ReCalculateMoneyLenderByLoanCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        /// <summary>
        /// = 0 : tính lại từ đầu, ngược lại tính từ =
        /// </summary>
        public long StartTransactionID { get; set; }
        public long EndTransactionID { get; set; }
    }
    public class ReCalculateMoneyLenderByLoanCommandHandler : IRequestHandler<ReCalculateMoneyLenderByLoanCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSplitTransactionLoan> _spilitTransactionLoanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblContractLoanWithLender> _contractLoanWithLenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentConfiguration> _paymentConfigurationTab;

        ILogger<ReCalculateMoneyLenderByLoanCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public ReCalculateMoneyLenderByLoanCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblSplitTransactionLoan> spilitTransactionLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblContractLoanWithLender> contractLoanWithLenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentConfiguration> paymentConfigurationTab,
            ILogger<ReCalculateMoneyLenderByLoanCommandHandler> logger
            )
        {
            _spilitTransactionLoanTab = spilitTransactionLoanTab;
            _contractLoanWithLenderTab = contractLoanWithLenderTab;
            _transactionTab = transactionTab;
            _paymentConfigurationTab = paymentConfigurationTab;
            _logger = logger;
            _common = new Utils();
        }

        public async Task<ResponseActionResult> Handle(ReCalculateMoneyLenderByLoanCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            DateTime dtNow = DateTime.Now;
            try
            {
                List<Task> lstTaskHandler = new List<Task>();
                List<Domain.Tables.TblSplitTransactionLoan> splitTransactionLoans = new List<Domain.Tables.TblSplitTransactionLoan>();
                //b1: xóa hết các giao dịch trong bảng TblSplitTransactionLoan theo đơn vay
                if (request.StartTransactionID == 0)
                {
                    lstTaskHandler.Add(_spilitTransactionLoanTab.DeleteWhereAsync(x => x.LoanID == request.LoanID));
                }
                //var taskDeleteTran = _spilitTransactionLoanTab.DeleteWhereAsync(x => x.LoanID == request.LoanID);
                //b2: dựa vào bảng TblContractLoanWithLender để tính tiền cho lender theo các nghiệp vụ nhất định
                var taskContractLoan = _contractLoanWithLenderTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync();
                lstTaskHandler.Add(taskContractLoan);
                if (request.StartTransactionID > 0)
                {
                    _transactionTab.WhereClause(x => x.TransactionID > request.StartTransactionID && x.TransactionID <= request.EndTransactionID);
                }
                var taskTranLoan = _transactionTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync();
                lstTaskHandler.Add(taskTranLoan);
                await Task.WhenAll(lstTaskHandler);

                var contractLoans = taskContractLoan.Result;
                var transLoan = taskTranLoan.Result;
                //b2_1: Dựa vào bảng TblPaymentConfiguration để tính tiền cho lender theo các loại tiền
                var accountingIds = contractLoans.Select(x => x.AccountingPriorityOrderID).ToList();
                var lstConfigMoney = (await _paymentConfigurationTab.SelectColumns(x => x.ColumnName, x => x.AccountingPriorityID, x => x.IsLender).WhereClause(x => accountingIds.Contains(x.AccountingPriorityID)).QueryAsync());
                //b2_2: thời gian transaction nằm trong khoảng thời gian FromDate và ToDate của bảng TblContractLoanWithLender tính cho lenderID đó
                foreach (var item in transLoan)
                {
                    // xem là loại tiền nào + của lender nào
                    var dateTxn = item.CreateDate.Date;
                    var lenderContract = contractLoans.Where(x => x.FromDate <= dateTxn && dateTxn <= x.ToDate).FirstOrDefault();
                    var moneyTypeName = ((PaymentConfiguration_MapMoney)item.MoneyType).GetDescription().ToLower().Trim();
                    var itemMoney = lstConfigMoney.Where(x => x.ColumnName.ToLower() == moneyTypeName).FirstOrDefault();
                    // set mặc định là tiền tima
                    var txnDetail = new Domain.Tables.TblSplitTransactionLoan()
                    {
                        ActionID = item.ActionID,
                        CreateDate = item.CreateDate,
                        LenderID = TimaSettingConstant.ShopIDTima,
                        LoanID = item.LoanID.GetValueOrDefault(),
                        MoneyType = item.MoneyType,
                        TotalMoney = item.TotalMoney,
                        ReferTxnID = item.TransactionID
                    };
                    // số tiền lender được hưởng
                    if (lenderContract != null && itemMoney.AccountingPriorityID == lenderContract.AccountingPriorityOrderID && itemMoney.IsLender == 1)
                    {
                        txnDetail.LenderID = lenderContract.LenderID;
                    }
                    // tạm thời check tiền phạt
                    // todo: sẽ có loại tiền phạt riêng của NDT kèm với tỉ lệ, sẽ review lại sau
                    if (item.MoneyType == (int)LMS.Common.Constants.Transaction_TypeMoney.FineLate)
                    {
                        // kiểm tra lender lever4
                        if (lenderContract != null && lenderContract.SelfEmployed < (int)LMS.Common.Constants.Lender_SeflEmployed.LenderOut && lenderContract.LenderID > 0)
                        {
                            decimal rateLender = lenderContract.RateInterest + lenderContract.RateService;
                            decimal totalRate = rateLender + lenderContract.RateConsultant;
                            var txnDetailLender = new Domain.Tables.TblSplitTransactionLoan
                            {
                                ActionID = item.ActionID,
                                CreateDate = item.CreateDate,
                                LenderID = lenderContract.LenderID,
                                MoneyType = item.MoneyType,
                                LoanID = item.LoanID.Value,
                                ReferTxnID = item.TransactionID,
                                TotalMoney = Convert.ToInt64(item.TotalMoney * (rateLender / totalRate))
                            };
                            splitTransactionLoans.Add(txnDetailLender);
                            txnDetail.TotalMoney -= txnDetailLender.TotalMoney;
                        }
                    }
                    splitTransactionLoans.Add(txnDetail);
                }
                try
                {
                    _spilitTransactionLoanTab.InsertBulk(splitTransactionLoans);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"ReCalculateMoneyLenderByLoanCommandHandler_Warning|request={_common.ConvertObjectToJSonV2(request)}|splitTransactionLoans={_common.ConvertObjectToJSonV2(splitTransactionLoans)}|ex={ex.Message}-{ex.StackTrace}");
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ReCalculateMoneyLenderByLoanCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;

        }


    }
}
