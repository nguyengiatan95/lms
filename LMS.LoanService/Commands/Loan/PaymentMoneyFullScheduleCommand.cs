﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class PaymentMoneyFullScheduleCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public long PaymentID { get; set; }
        public long UserID { get; set; }
    }
    public class PaymentMoneyFullScheduleCommandHandler : IRequestHandler<PaymentMoneyFullScheduleCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionLoanTab;
        RestClients.IPaymentScheduleService _paymentService;
        RestClients.ICustomerService _customerService;
        Services.ILoanManager _loanManager;
        LMS.Common.Helper.Utils _common;
        ILogger<PaymentMoneyFullScheduleCommandHandler> _logger;
        int _MaxPaymentContinous = 5;
        public PaymentMoneyFullScheduleCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            RestClients.ICustomerService customerService,
            RestClients.IPaymentScheduleService paymentService,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionLoanTab,
            Services.ILoanManager loanManager,
            ILogger<PaymentMoneyFullScheduleCommandHandler> logger)
        {
            _loanTab = loanTab;
            _customerTab = customerTab;
            _paymentService = paymentService;
            _logger = logger;
            _paymentTab = paymentTab;
            _transactionLoanTab = transactionLoanTab;
            _customerService = customerService;
            _loanManager = loanManager;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(PaymentMoneyFullScheduleCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var currentDate = DateTime.Now;
                long moneyCanChangeDPD = 10000; // tiền được phép chuyển DPD
                var loanInfo = (await _loanTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                if (loanInfo == null || loanInfo.LoanID == 0)
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                if (loanInfo.Status != (int)Loan_Status.Lending)
                {
                    response.Message = MessageConstant.LoanCantProcess;
                    return response;
                }
                if (loanInfo.StatusSendInsurance == (int)Loan_StatusSendInsurance.Send)
                {
                    response.Message = MessageConstant.LoanSentInsurance;
                    return response;
                }
                if (loanInfo.StatusSendInsurance == (int)Loan_StatusSendInsurance.ProcessingComplete)
                {
                    response.Message = MessageConstant.LoanInsurancePaid;
                    return response;
                }
                var paymentTask = _paymentTab.WhereClause(x => x.PaymentScheduleID == request.PaymentID && x.LoanID == request.LoanID).QueryAsync();
                var customerTask = _customerTab.WhereClause(x => x.CustomerID == loanInfo.CustomerID).QueryAsync();
                await Task.WhenAll(paymentTask, customerTask);
                var objPayment = paymentTask.Result != null && paymentTask.Result.Any() ? paymentTask.Result.FirstOrDefault() : null;
                var objCustomer = customerTask.Result != null && customerTask.Result.Any() ? customerTask.Result.FirstOrDefault() : null;
                if (objPayment == null)
                {
                    response.Message = MessageConstant.PaymentInvalid;
                    return response;
                }
                if (objPayment.IsComplete == 1)
                {
                    response.Message = MessageConstant.PaymentCompleted;
                    return response;
                }
                if (objCustomer == null)
                {
                    response.Message = MessageConstant.CustomerNotExist;
                    return response;
                }
                objCustomer.TotalMoney ??= 0;
                if (objCustomer.TotalMoney == 0)
                {
                    response.Message = MessageConstant.PaymentNotEnoughMoney;
                    return response;
                }
                long moneyNeedPay = objPayment.MoneyOriginal + objPayment.MoneyInterest + objPayment.MoneyService + objPayment.MoneyConsultant
                    - (objPayment.PayMoneyOriginal + objPayment.PayMoneyInterest + objPayment.PayMoneyService + objPayment.PayMoneyConsultant);
                if (objCustomer.TotalMoney < moneyNeedPay - moneyCanChangeDPD)
                {
                    response.Message = MessageConstant.PaymentNotEnoughMoney;
                    return response;
                }
                long moneyPay = objCustomer.TotalMoney > moneyNeedPay ? moneyNeedPay : objCustomer.TotalMoney.Value;
                //List<int> lstTypeMoney = Enum.GetValues(typeof(Transaction_TypeMoney)).Cast<Transaction_TypeMoney>().Select(v => (int)v)
                //    .ToList();
                long totalMoneyPaidBefore = moneyPay;
                do
                {
                    var actionResultPayment = await _paymentService.PayPaymentByLoanID(loanInfo.LoanID, objPayment.PayDate, moneyPay, request.UserID);
                    if (actionResultPayment.Result == (int)ResponseAction.Success)
                    {
                        long totalMoneyCustomerPaid = Convert.ToInt64(actionResultPayment.Data);
                        if (totalMoneyCustomerPaid != moneyPay)
                        {
                            // ghi log lại để check lại
                            _logger.LogError($"PaymentMoneyFullScheduleCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|moneyPaidLMS={totalMoneyCustomerPaid}");
                        }
                        _ = _customerService.UpdateMoneyCustomer((long)loanInfo.CustomerID, totalMoneyCustomerPaid * -1, $"Hạch toán tiền {objPayment.PayDate:dd/MM/yyyy}");
                        moneyPay -= totalMoneyCustomerPaid;
                    }
                    else
                    {
                        break;
                    }
                } while (moneyPay > 0);
                if (totalMoneyPaidBefore != moneyPay)
                {
                    //long moneyOriginal = (await _transactionLoanTab.SelectColumns(x => x.TotalMoney)
                    //                                                .WhereClause(x => x.LoanID == loanInfo.LoanID && x.CreateDate > currentDate && x.MoneyType == (int)Transaction_TypeMoney.Original)
                    //                                                .QueryAsync()).FirstOrDefault()?.TotalMoney ?? 0;
                    //if (moneyOriginal > 0)
                    //{
                    //    _ = _loanManager.CreateLoanExtraOriginal(new Domain.Models.Loan.LoanExtraInsertDetailModel
                    //    {
                    //        CreateBy = request.UserID,
                    //        LoanID = loanInfo.LoanID,
                    //        PayDate = currentDate,
                    //        TotalMoney = moneyOriginal,
                    //        TypeMoney = (int)Transaction_TypeMoney.Original
                    //    });
                    //}
                    List<Domain.Tables.TblTransaction> lstTxnDetail = new List<Domain.Tables.TblTransaction>();
                    int countCheckTxn = 0;
                    do
                    {
                        await Task.Delay(100);
                        lstTxnDetail = (await _transactionLoanTab.SelectColumns(x => x.TotalMoney, x => x.TransactionID, x => x.MoneyType)
                                                                    .WhereClause(x => x.LoanID == loanInfo.LoanID && x.CreateDate > currentDate)
                                                                    .QueryAsync()).ToList();
                        countCheckTxn++;
                    } while (lstTxnDetail.Count == 0 && countCheckTxn < _MaxPaymentContinous);

                    long moneyOriginal = lstTxnDetail.Where(x => x.MoneyType == (int)Transaction_TypeMoney.Original).OrderByDescending(x => x.TransactionID).FirstOrDefault()?.TotalMoney ?? 0;
                    if (moneyOriginal > 0)
                    {
                        _ = _loanManager.CreateLoanExtraOriginal(new Domain.Models.Loan.LoanExtraInsertDetailModel
                        {
                            CreateBy = request.UserID,
                            LoanID = loanInfo.LoanID,
                            PayDate = currentDate,
                            TotalMoney = moneyOriginal * -1,
                            TypeMoney = (int)Transaction_TypeMoney.Original
                        });
                    }
                    response = await _loanManager.UpdateLoanAfterPayment(loanInfo.LoanID, request.UserID);
                    if (response.Result != (int)ResponseAction.Success)
                    {
                        _logger.LogError($"PaymentMoneyFullScheduleCommandHandler_UpdateLoanFail|request={_common.ConvertObjectToJSonV2(request)}|loanUpdateAfterPayment={_common.ConvertObjectToJSonV2(loanInfo)}");
                    }
                }
                response.SetSucces();
                response.Data = loanInfo.LoanID;
            }
            catch (Exception ex)
            {
                _logger.LogError($"PaymentMoneyFullScheduleCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
