﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class AGCloseLoanFromScheduleCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanAgID { get; set; }
        public long MoneyOriginal { get; set; }
        public long MoneyService { get; set; }
        public long MoneyInterest { get; set; }
        public long MoneyConsultant { get; set; }
        public long MoneyFineOriginal { get; set; }
        public long OtherMoney { get; set; }
        public string CloseDate { get; set; }// dd/mm/yyyy
        public long CreateBy { get; set; }
        public long TotalMoneyCustomerHas { get; set; }
        public string Description { get; set; }
    }
    public class AGCloseLoanFromScheduleCommandHandler : IRequestHandler<AGCloseLoanFromScheduleCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebt> _loanDebtTab;
        RestClients.IPaymentScheduleService _paymentService;
        RestClients.ICustomerService _customerService;
        LMS.Common.Helper.Utils _common;
        ILogger<AGCloseLoanFromScheduleCommandHandler> _logger;
        Services.ILoanManager _loanManager;
        public AGCloseLoanFromScheduleCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebt> loanDebtTab,
            RestClients.ICustomerService customerService,
            RestClients.IPaymentScheduleService paymentService,
            Services.ILoanManager loanManager,
            ILogger<AGCloseLoanFromScheduleCommandHandler> logger)
        {
            _loanTab = loanTab;
            _customerTab = customerTab;
            _loanDebtTab = loanDebtTab;
            _customerService = customerService;
            _paymentService = paymentService;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _loanManager = loanManager;
        }
        public async Task<ResponseActionResult> Handle(AGCloseLoanFromScheduleCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var loanInfo = (await _loanTab.WhereClause(x => x.TimaLoanID == request.LoanAgID).QueryAsync()).FirstOrDefault();
                var customerInfo = (await _customerTab.WhereClause(x => x.CustomerID == loanInfo.CustomerID).QueryAsync()).FirstOrDefault();
                long totalMoneyNeedPay = request.MoneyOriginal + request.MoneyInterest + request.MoneyConsultant + request.OtherMoney + request.MoneyService + request.MoneyFineOriginal;
                var closeDate = DateTime.ParseExact(request.CloseDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                var actionResultPayment = await _paymentService.CloseLoanFromSchedule(loanInfo.LoanID, request.MoneyOriginal, request.MoneyService, request.MoneyInterest, request.MoneyConsultant, request.MoneyFineOriginal, request.OtherMoney, closeDate, request.CreateBy);
                if (actionResultPayment.Result == (int)ResponseAction.Success)
                {
                    long totalMoneyCustomerPaid = Convert.ToInt64(actionResultPayment.Data);
                    if (totalMoneyCustomerPaid != totalMoneyNeedPay)
                    {
                        // ghi log lại để check lại
                        _logger.LogError($"AGCloseLoanFromScheduleCommandHandler_Warning|request={_common.ConvertObjectToJSonV2(request)}|moneyPaidLMS={totalMoneyCustomerPaid}");
                    }
                    _ = await _customerService.UpdateMoneyCustomer((long)loanInfo.CustomerID, totalMoneyCustomerPaid * -1, request.Description);
                    if (request.OtherMoney > 0)
                    {
                        _ = _loanDebtTab.InsertAsync(new Domain.Tables.TblDebt
                        {
                            CreateBy = request.CreateBy,
                            ReferID = loanInfo.LoanID,
                            TargetID = (long)loanInfo.CustomerID,
                            CreateDate = loanInfo.ModifyDate,
                            Status = (int)StatusCommon.Active,
                            TotalMoney = (totalMoneyCustomerPaid * -1),
                            TypeID = (int)Debt_TypeID.CustomerPayFineLate,
                            Description = Debt_TypeID.CustomerPayFineLate.GetDescription()
                        });
                        loanInfo.MoneyFineLate -= request.OtherMoney;
                    }
                    if (request.MoneyOriginal > 0)
                    {
                        _ = _loanManager.CreateLoanExtraOriginal(new Domain.Models.Loan.LoanExtraInsertDetailModel
                        {
                            CreateBy = request.CreateBy,
                            LoanID = loanInfo.LoanID,
                            PayDate = closeDate,
                            TotalMoney = request.MoneyOriginal * -1,
                            TypeMoney = (int)Transaction_TypeMoney.Original
                        });
                    }
                    loanInfo.Status = (int)Loan_Status.Close;
                    loanInfo.ModifyDate = DateTime.Now;
                    loanInfo.TotalMoneyCurrent = 0;
                    loanInfo.FinishDate = DateTime.Now;
                    _ = _loanTab.UpdateAsync(loanInfo);
                }
                response.SetSucces();
                response.Data = loanInfo.LoanID;
            }
            catch (Exception ex)
            {
                _logger.LogError($"AGCloseLoanFromScheduleCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
