﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class AGDebtRestructuringCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [JsonProperty("loanBriefId")]
        public long LoanBriefID { get; set; }
        [JsonProperty("LoanId")]
        public long LoanID { get; set; }

        /// <summary>
        /// userId người thực hiện
        /// </summary>
        [JsonProperty("UserId")]
        public long UserID { get; set; }

        /// <summary>
        /// người thực hiện
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// tài khoản người thao tác
        /// </summary>
        public string UserName { get; set; }


        /// <summary>
        /// Kiểu tái cơ cấu nợ
        /// </summary>
        public int TypeDebtRestructuring { get; set; }

        /// <summary>
        /// Số kỳ khoanh vùng trả nợ
        /// </summary>
        public int NumberOfRepaymentPeriod { get; set; }

        public string AppointmentDate { get; set; }
        public int PercentDiscount { get; set; }

        public long TimaLoanID { get; set; }
    }
    public class AGDebtRestructuringCommandHandler : IRequestHandler<AGDebtRestructuringCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblProductCredit> _productCreditTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.LoanUpdateMoneyFineLate> _loanUpdateMoneyFineLateTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblKafkaLogEvent> _kafkaLogEventTab;
        readonly RestClients.IPaymentScheduleService _paymentScheduleService;
        readonly ILogger<AGDebtRestructuringCommandHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        readonly Services.ILoanManager _loanManager;
        public AGDebtRestructuringCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblProductCredit> productCreditTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.LoanUpdateMoneyFineLate> loanUpdateMoneyFineLateTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblKafkaLogEvent> kafkaLogEventTab,
            RestClients.IPaymentScheduleService paymentScheduleService,
            Services.ILoanManager loanManager,
            ILogger<AGDebtRestructuringCommandHandler> logger)
        {
            _loanTab = loanTab;
            _productCreditTab = productCreditTab;
            _loanUpdateMoneyFineLateTab = loanUpdateMoneyFineLateTab;
            _paymentScheduleService = paymentScheduleService;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _loanManager = loanManager;
            _kafkaLogEventTab = kafkaLogEventTab;
        }
        public async Task<ResponseActionResult> Handle(AGDebtRestructuringCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                if (request.TimaLoanID > 0)
                {
                    _loanTab.WhereClause(x => x.TimaLoanID == request.TimaLoanID);
                }
                else
                {
                    _loanTab.WhereClause(x => x.LoanID == request.LoanID);
                }
                var loanInfos = await _loanTab.QueryAsync();
                if (loanInfos == null || !loanInfos.Any())
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                var loanInfo = loanInfos.First();

                if (request.LoanBriefID != loanInfo.LoanCreditIDOfPartner)
                {
                    response.Message = $"Thông tin đơn vay HD-{request.LoanBriefID}  Không khớp với thông tin giải ngân. Vui lòng báo kỹ thuật check lại";
                    return response;
                }
                var productCreditInfo = (await _productCreditTab.WhereClause(x => x.ProductCreditID == loanInfo.ProductID).QueryAsync()).FirstOrDefault();
                if (productCreditInfo == null || productCreditInfo.ProductCreditID < 1)
                {
                    response.Message = "Hệ thống không xác định được sản phẩm của thông tin giải ngân của HD-" + request.LoanBriefID + " để tái cấu trúc nợ";
                    _logger.LogError($"AGDebtRestructuringCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                }
                DateTime appointmentDate = DateTime.Now;
                if (!string.IsNullOrEmpty(request.AppointmentDate))
                {
                    if (!DateTime.TryParseExact(request.AppointmentDate, "dd-MM-yyyy", null, System.Globalization.DateTimeStyles.None, out appointmentDate))
                    {
                        response.Message = "Ngày chọn phải là định dạng: dd-MM-yyyy";
                        return response;
                    }
                }

                var minDayFineLate = (int)SettingFineForProduct.MinDayFineLateSmall;
                if (productCreditInfo != null && productCreditInfo.TypeProduct == (int)Product_Type.Big)
                {
                    if (loanInfo.FromDate >= new DateTime(2020, 01, 31))
                    {
                        minDayFineLate = 0;
                    }
                }
                minDayFineLate += 1; // + 1 tính từ ngày hôm sau       
                var actionResult = await _paymentScheduleService.DebtRestructuring(loanInfo.LoanID, loanInfo.RateType, request.NumberOfRepaymentPeriod, appointmentDate.ToString(TimaSettingConstant.DateTimeDayMonthYear), request.PercentDiscount, request.TypeDebtRestructuring, request.UserID, minDayFineLate);
                response = actionResult;
                if (actionResult.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                {
                    long totalMoneyFineLateCashBack = Convert.ToInt64(actionResult.Data);
                    if (totalMoneyFineLateCashBack != 0)
                    {
                        var loanDetail = (await _loanUpdateMoneyFineLateTab.WhereClause(x => x.LoanID == loanInfo.LoanID).QueryAsync()).FirstOrDefault();
                        loanDetail.ModifyDate = DateTime.Now;
                        loanDetail.MoneyFineLate -= totalMoneyFineLateCashBack;
                        if (!await _loanUpdateMoneyFineLateTab.UpdateAsync(loanDetail))
                        {
                            _logger.LogError($"AGDebtRestructuringCommandHandler_Update_Error|request={_common.ConvertObjectToJSonV2(request)}|totalMoneyFineLateCashBack={totalMoneyFineLateCashBack}");
                        }
                    }
                    var t1 = _loanManager.UpdateLoanAfterPayment(loanInfo.LoanID, request.UserID);
                    var t2 = _kafkaLogEventTab.InsertAsync(new TblKafkaLogEvent
                    {
                        CreateDate = DateTime.Now,
                        ModifyDate = DateTime.Now,
                        DataMessage = _common.ConvertObjectToJSonV2(new LMS.Kafka.Messages.Loan.DebtRestructuringLoanSuccessInfo
                        {
                            LoanID = loanInfo.LoanID,
                            DebtRestructuringType = (int)DebtRestructuringLoan_DebtRestructuringType.DebtRelief,
                            CreateBy = request.UserID
                        }),
                        TopicName = Kafka.Constants.KafkaTopics.DebtRestructuringLoanSuccess,
                        ServiceName = ServiceHostInternal.LoanService,
                        Status = (int)KafkaLogEvent_Status.Waitting,
                    });
                    await Task.WhenAll(t1, t2);
                    response = t1.Result;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"AGDebtRestructuringCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
