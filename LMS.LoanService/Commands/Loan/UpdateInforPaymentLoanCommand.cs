﻿using Dapper.Contrib.Extensions;
using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Models;
using LMS.LoanServiceApi.Domain.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class UpdateInforPaymentLoanCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public long LoanID { get; set; }
        [Required]
        public int LoanTime { get; set; }
        [Required]
        public string StrFromDate { get; set; }
        [Required]
        public int RateType { get; set; }
        [Required]
        public decimal RateInterest { get; set; }
        [Required]
        public int CreateBy { get; set; }
        //[Required]
        public decimal RateService { get; set; }
        [Required]
        public decimal RateConsultant { get; set; }
        public DateTime FromDate { get; set; }
    }
    public class UpdateInforPaymentLoanCommandHandler : IRequestHandler<UpdateInforPaymentLoanCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLoan> _logLoanTab;
        RestClients.IPaymentScheduleService _paymentService;
        RestClients.IOutGatewayService _outGateway;
        ILogger<UpdateInforPaymentLoanCommandHandler> _logger;
        Common.Helper.Utils _common;
        public UpdateInforPaymentLoanCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLoan> logLoanTab,
            RestClients.IPaymentScheduleService paymentService,
            RestClients.IOutGatewayService outGateway,
            ILogger<UpdateInforPaymentLoanCommandHandler> logger)
        {
            _loanTab = loanTab;
            _transactionTab = transactionTab;
            _logLoanTab = logLoanTab;
            _logger = logger;
            _paymentService = paymentService;
            _outGateway = outGateway;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(UpdateInforPaymentLoanCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var fromDate = DateTime.ParseExact(request.StrFromDate, _common.DateTimeDDMMYYYY, CultureInfo.InvariantCulture);
                request.FromDate = fromDate;
                var loanInfo = (await _loanTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                if (loanInfo == null) // kiểm tra LoanID có tồn tại
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                var lstTransaction = await _transactionTab.SelectColumns(x => x.TotalMoney).WhereClause(x => x.LoanID == request.LoanID && x.ActionID != (int)Transaction_Action.ChoVay).QueryAsync();
                if (lstTransaction != null && lstTransaction.Count() > 0)
                {
                    if (lstTransaction.Sum(x => x.TotalMoney) != 0)
                    {
                        response.Message = MessageConstant.LoanHasIncurredMoney;
                        return response;
                    }
                }
                // lấy thong tin LoanCreditIDOfPartner
                LMS.Entites.Dtos.LOSServices.ResultLoanWaitingDisbursementDetailLOS loanDetailLos = await _outGateway.GetLoanCreditDetailAsync((int)loanInfo.LoanCreditIDOfPartner);
                if (loanDetailLos == null)
                {
                    response.Message = LMS.Common.Constants.MessageConstant.LoanNotFoundLoanCreditPartner;
                    return response;
                }
                //loan cũ
                var objLoanOld = _common.ConvertParentToChild<TblLoan, TblLoan>(loanInfo);

                loanInfo.FromDate = fromDate;
                loanInfo.RateType = request.RateType;
                loanInfo.LoanTime = request.LoanTime;
                loanInfo.ToDate = fromDate.AddMonths(request.LoanTime).AddDays(-1);
                loanInfo.RateInterest = request.RateInterest;
                loanInfo.RateConsultant = request.RateConsultant;
                loanInfo.RateService = request.RateService;
                if (await _loanTab.UpdateAsync(loanInfo)) //update lại thong tin loan
                {
                    _ = InsertLogLoan(request, objLoanOld);

                    DateTime? nextDateTopUp = null;
                    //if (loanDetailLos.TopUpOfLoanBriefID > 0) // kiểm tra điều kiện của đơn Topup
                    //{
                    //    // lấy đơn cũ
                    //    var loanInfoTopUp = _loanTab.WhereClause(x => x.LoanCreditIDOfPartner == loanDetailLos.TopUpOfLoanBriefID).Query().FirstOrDefault();
                    //    if (loanInfoTopUp != null && loanInfoTopUp.LoanID > 0)
                    //    {
                    //        nextDateTopUp = loanInfoTopUp.NextDate;
                    //        request.RateType = (int)PaymentSchedule_ActionRateType.RateAmortization30DaysTopUp;
                    //    }
                    //}
                    //update Status  PaymentSchedule  về ko dùng
                    var update = await _paymentService.UpdateStatusPaymentScheduleByLoan(request.LoanID, (int)PaymentSchedule_Status.NoUse);
                    if (update.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                    {
                        //tạo lại lịch
                        _ = _paymentService.CreatePaymentScheduleByLoanID(loanInfo.TotalMoneyDisbursement,
                              fromDate,
                              request.LoanTime * TimaSettingConstant.NumberDayInMonth,
                              loanInfo.Frequency * TimaSettingConstant.NumberDayInMonth,
                              request.RateType,
                              request.RateConsultant,
                              request.RateInterest,
                              request.RateService,
                              loanInfo.LoanID, nextDateTopUp).Result;

                        response.SetSucces();
                    }
                    else
                    {
                        response.Message = update.Message;
                    }
                }
                else
                {
                    response.Message = MessageConstant.LoanUpdateError;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateInforPaymentLoanCommandHandler|request={_common.ConvertObjectToJSonV2<UpdateInforPaymentLoanCommand>(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private bool InsertLogLoan(UpdateInforPaymentLoanCommand objNew, Domain.Tables.TblLoan objOld)
        {
            List<TblLogLoan> logs = new List<TblLogLoan>();
            var oldType = objOld.GetType();
            var newType = objNew.GetType();
            var oldProperties = oldType.GetProperties();
            var newProperties = newType.GetProperties();

            var dateChanged = DateTime.Now;
            foreach (var oldProperty in oldProperties)
            {
                var matchingProperty = newProperties.Where(x => !Attribute.IsDefined(x, typeof(IgnoreLoggingAttribute))
                                                                && x.Name == oldProperty.Name
                                                                && x.PropertyType == oldProperty.PropertyType)
                                                    .FirstOrDefault();


                if (matchingProperty == null)
                    continue;

                var oldPropertyValue = oldProperty.GetValue(objOld);
                var oldValue = "null";
                if (oldPropertyValue != null)
                {
                    oldValue = oldPropertyValue.ToString();
                }
                var matchingPropertyValue = matchingProperty.GetValue(objNew);
                var newValue = "null";
                if (matchingPropertyValue != null)
                {
                    newValue = matchingPropertyValue.ToString();
                }

                if (newValue != null && oldValue != newValue)
                {
                    logs.Add(new TblLogLoan()
                    {
                        NewValue = newValue,
                        OldValue = oldValue,
                        ColumnName = matchingProperty.Name,
                        CreateBy = objNew.CreateBy,
                        CreateDate = dateChanged,
                        LoanID = objNew.LoanID,
                        Request = _common.ConvertObjectToJSonV2(objNew)
                    });
                }
            }
            if (logs.Count() > 0)
            {
                _logLoanTab.InsertBulk(logs);
                return true;
            }
            return false;
        }
    }
}
