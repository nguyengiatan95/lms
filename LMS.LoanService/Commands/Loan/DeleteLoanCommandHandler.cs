﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.RestClients;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class DeleteLoanCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public long CreateBy { get; set; }
    }
    public class DeleteLoanCommandHandler : IRequestHandler<DeleteLoanCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _tranTab;
        ITransactionService _transactionService;
        ILogger<DeleteLoanCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public DeleteLoanCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab
            , LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> tranTab, ITransactionService transactionService)
        {
            _loanTab = loanTab;
            _tranTab = tranTab;
            _transactionService = transactionService;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(DeleteLoanCommand request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
             {
                 DateTime dtNow = DateTime.Now;
                 ResponseActionResult response = new ResponseActionResult();
                 try
                 {
                     var objLoan = _loanTab.WhereClause(x => x.LoanID == request.LoanID).Query().FirstOrDefault();
                     if (objLoan == null || objLoan.Status != (int)Loan_Status.Lending)
                     {
                         response.Message = MessageConstant.LoanCantProcess;
                         return response;
                     }
                     var lstListTranExclude = new List<int>()
                 {
                    (int)Transaction_Action.ChoVay,
                    (int)Transaction_Action.SuaHd,
                 };
                     var lstTran = _tranTab.SelectColumns(x => x.TotalMoney)
                                           .WhereClause(x => x.LoanID == request.LoanID && !lstListTranExclude.Contains(x.ActionID))
                                           .Query();
                     long totalPayment = lstTran.Sum(x => x.TotalMoney);
                     if (totalPayment > 0)
                     {
                         response.Message = MessageConstant.LoanHavePayment;
                         return response;
                     }

                     // update hợp đồng
                     objLoan.Status = (int)Loan_Status.Delete;
                     objLoan.ModifyDate = dtNow;
                     bool updateLoan = _loanTab.Update(objLoan);
                     if (updateLoan)
                     {
                         response.SetSucces();
                         List<LMS.Entites.Dtos.TransactionServices.RequestCreateListTransactionModel> requestTran = new List<LMS.Entites.Dtos.TransactionServices.RequestCreateListTransactionModel>()
                         {
                            new LMS.Entites.Dtos.TransactionServices.RequestCreateListTransactionModel()
                            {
                                ActionID = (int)Transaction_Action.HuyChoVay,
                                LoanID = objLoan.LoanID,
                                MoneyType = (int)Transaction_TypeMoney.Original,
                                TotalMoney = objLoan.TotalMoneyCurrent,
                                UserID = request.CreateBy
                            }
                         };
                         // insert transaction
                         _transactionService.CreateTransaction(requestTran);
                         // tạo phiếu hủy giải ngân
                         return response;
                     }
                 }
                 catch (Exception ex)
                 {
                     _logger.LogError($"{System.Reflection.MethodBase.GetCurrentMethod().Name}_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                 }
                 return response;
             });
        }
    }
}
