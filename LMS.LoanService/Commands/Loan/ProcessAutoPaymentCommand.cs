﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Models.Loan;
using LMS.LoanServiceApi.Domain.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class ProcessAutoPaymentCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class ProcessAutoPaymentCommandHandler : IRequestHandler<ProcessAutoPaymentCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> _settingKeyTab;
        Services.ILoanManager _loanManager;
        ILogger<ProcessAutoPaymentCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public ProcessAutoPaymentCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> settingKeyTab,
            Services.ILoanManager loanManager,
             ILogger<ProcessAutoPaymentCommandHandler> logger)
        {
            _settingKeyTab = settingKeyTab;
            _loanManager = loanManager;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(ProcessAutoPaymentCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var keySettingInfo = (await _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.Loan_ProcessAutoPayment).QueryAsync()).FirstOrDefault();
                if (keySettingInfo == null || keySettingInfo.Status == 0)
                {
                    response.Message = MessageConstant.SettingKey_Loan_ProcessAutoPaymentStatus;
                    return response;
                }
                SettingKeyValueAutoPayment keyValueAutoPaymentInfo = new SettingKeyValueAutoPayment
                {
                    StartTime = "13:00",
                    EndTime = "19:00",
                    LatestCustomerID = 0
                };

                //kiểm tra giờ chạy
                TimeSpan timeStart = new TimeSpan(22, 00, 00);
                if (!string.IsNullOrEmpty(keySettingInfo.Value))
                {
                    try
                    {
                        keyValueAutoPaymentInfo = _common.ConvertJSonToObjectV2<SettingKeyValueAutoPayment>(keySettingInfo.Value);
                    }
                    catch (Exception)
                    {
                        keyValueAutoPaymentInfo = new SettingKeyValueAutoPayment
                        {
                            StartTime = "13:00",
                            EndTime = "19:00",
                            LatestCustomerID = 0
                        };
                    }
                }
                if (!string.IsNullOrEmpty(keyValueAutoPaymentInfo.StartTime))
                {
                    timeStart = TimeSpan.Parse(keyValueAutoPaymentInfo.StartTime);
                }
                TimeSpan? endStart = null;
                if (!string.IsNullOrEmpty(keyValueAutoPaymentInfo.EndTime))
                {
                    endStart = TimeSpan.Parse(keyValueAutoPaymentInfo.EndTime);
                }
                if ((!endStart.HasValue || endStart.Value <= timeStart || DateTime.Now.TimeOfDay < timeStart || DateTime.Now.TimeOfDay > endStart.Value) && keyValueAutoPaymentInfo.LatestCustomerID == 0)
                {
                    response.Message = MessageConstant.SettingKey_Loan_ProcessAutoPaymentStartTime;
                    return response;
                }
                ProcessAutoPaymentModel responseAction = await _loanManager.ProcessAutoPayment(keyValueAutoPaymentInfo.LatestCustomerID);
                response.SetSucces();
                response.Data = responseAction;
                keyValueAutoPaymentInfo.LatestCustomerID = responseAction.LatestCustomerID;
                keySettingInfo.Value = _common.ConvertObjectToJSonV2(keyValueAutoPaymentInfo);
                keySettingInfo.ModifyDate = DateTime.Now;
                _ = await _settingKeyTab.UpdateAsync(keySettingInfo);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessAutoPaymentCommandHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
