﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Models;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands
{
    public class UpdateLoanAfterPaymentCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public RequestUpdateLoanAfterPaymentModel Model { get; set; }
    }
    public class UpdateLoanAfterPaymentCommandHandler : IRequestHandler<UpdateLoanAfterPaymentCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        ILogger<UpdateLoanAfterPaymentCommandHandler> _logger;
        Common.Helper.Utils _common;
        public UpdateLoanAfterPaymentCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            ILogger<UpdateLoanAfterPaymentCommandHandler> logger)
        {
            _loanTab = loanTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(UpdateLoanAfterPaymentCommand request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                try
                {
                    ResponseActionResult response = new ResponseActionResult();
                    var objLoan = _loanTab.WhereClause(x => x.LoanID == request.Model.LoanID).Query().FirstOrDefault();
                    if(objLoan.Status ==(int)Loan_Status.Close || objLoan.Status == (int)Loan_Status.Delete)
                    {
                        response.Message = MessageConstant.LoanCantProcess;
                        return response; 
                    }
                    if (request.Model.NextDate != null)
                    {
                        objLoan.NextDate = request.Model.NextDate.Value;
                    }
                    if (request.Model.LastDateOfPay != null)
                    {
                        objLoan.LastDateOfPay = request.Model.LastDateOfPay.Value;
                    }
                    objLoan.TotalMoneyCurrent -= request.Model.OriginalMoney;
                    var update = _loanTab.Update(objLoan);
                    if (update)
                    {
                        response.SetSucces();
                        response.Data = objLoan.LoanID;
                    }
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"UpdateLoanAfterPaymentCommandHandler|request={_common.ConvertObjectToJSonV2<UpdateLoanAfterPaymentCommand>(request)}|ex={ex.Message}");
                    return null;
                }
            });
        }
    }
}
