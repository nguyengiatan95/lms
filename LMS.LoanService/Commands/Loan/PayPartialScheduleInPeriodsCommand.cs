﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class PayPartialScheduleInPeriodsCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public long MoneyOriginal { get; set; }
        public long MoneyInterest { get; set; }
        public long MoneyConsultant { get; set; }
        public long MoneyService { get; set; }
        public long MoneyFineLate { get; set; }
        public long CreateBy { get; set; }
        public string Description { get; set; }
    }
    public class PayPartialScheduleInPeriodsCommandHandler : IRequestHandler<PayPartialScheduleInPeriodsCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebt> _debtTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _tranTab;
        RestClients.IPaymentScheduleService _paymentService;
        RestClients.ICustomerService _customerService;
        RestClients.ITransactionService _transactionService;
        Services.ILoanManager _loanManager;
        LMS.Common.Helper.Utils _common;
        ILogger<PayPartialScheduleInPeriodsCommandHandler> _logger;
        int _MaxPaymentContinous = 5;
        public PayPartialScheduleInPeriodsCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebt> debtTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> tranTab,
            RestClients.ICustomerService customerService,
            RestClients.IPaymentScheduleService paymentService,
            RestClients.ITransactionService transactionService,
            Services.ILoanManager loanManager,
            ILogger<PayPartialScheduleInPeriodsCommandHandler> logger)
        {
            _loanTab = loanTab;
            _customerTab = customerTab;
            _customerService = customerService;
            _paymentService = paymentService;
            _transactionService = transactionService;
            _loanManager = loanManager;
            _debtTab = debtTab;
            _tranTab = tranTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(PayPartialScheduleInPeriodsCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                DateTime dtnow = DateTime.Now;
                var loanInfo = (await _loanTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                var customerInfo = (await _customerTab.WhereClause(x => x.CustomerID == loanInfo.CustomerID).QueryAsync()).FirstOrDefault();
                long totalMoneyNeedPay = request.MoneyOriginal + request.MoneyInterest + request.MoneyConsultant + request.MoneyFineLate + request.MoneyService;
                long totalCustomerHas = customerInfo.TotalMoney ?? 0;
                if (loanInfo == null || loanInfo.LoanID == 0)
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                //// khách hàng LMS co it tien hon so tien nhap vao
                if (loanInfo.Status != (int)Loan_Status.Lending)
                {
                    response.Message = MessageConstant.LoanCantProcess;
                    return response;
                }
                if (loanInfo.StatusSendInsurance == (int)Loan_StatusSendInsurance.Send)
                {
                    response.Message = MessageConstant.LoanSentInsurance;
                    return response;
                }
                if (loanInfo.StatusSendInsurance == (int)Loan_StatusSendInsurance.ProcessingComplete)
                {
                    response.Message = MessageConstant.LoanInsurancePaid;
                    return response;
                }
                if (totalCustomerHas <= 0 || totalCustomerHas < totalMoneyNeedPay)
                {
                    response.Message = MessageConstant.PaymentNotEnoughMoney;
                    return response;
                }
                // gạch nợ trả từng phần đơn phải quá hạn
                // phí phạt trả chậm thì cho gạch tùy ý
                if (loanInfo.ToDate > DateTime.Now
                    && (request.MoneyOriginal > 0 || request.MoneyInterest > 0 || request.MoneyConsultant > 0))
                {
                    response.Message = MessageConstant.LoanInvalidDatePayment;
                    return response;
                }

                long totalMoneyNeedPayBefore = totalMoneyNeedPay;
                Dictionary<Transaction_TypeMoney, long> dictPaymentInfos = new Dictionary<Transaction_TypeMoney, long>();
                var currentDate = DateTime.Now;
                if (request.MoneyOriginal > 0)
                {
                    dictPaymentInfos.Add(Transaction_TypeMoney.Original, request.MoneyOriginal);
                }
                if (request.MoneyInterest > 0)
                {
                    dictPaymentInfos.Add(Transaction_TypeMoney.Interest, request.MoneyInterest);
                }
                if (request.MoneyConsultant > 0)
                {
                    dictPaymentInfos.Add(Transaction_TypeMoney.Consultant, request.MoneyConsultant);
                }
                if (request.MoneyService > 0)
                {
                    dictPaymentInfos.Add(Transaction_TypeMoney.Service, request.MoneyService);
                }
                if (request.MoneyFineLate > 0)
                {
                    dictPaymentInfos.Add(Transaction_TypeMoney.FineLate, request.MoneyFineLate);
                }
                long totalMoneyFineCustomerPaid = 0;
                long totalMoneyCustomerPaid = 0;

                string description = request.Description;
                if (string.IsNullOrEmpty(request.Description))
                {
                    request.Description = "Gạch nợ từng phần ";
                }
                foreach (var item in dictPaymentInfos)
                {
                    totalMoneyCustomerPaid = 0;
                    var actionResultPayment = await _paymentService.PayPartialScheduleInPeriodsByLoanID(loanInfo.LoanID, item.Value, item.Key, request.CreateBy);
                    if (actionResultPayment.Result != (int)ResponseAction.Success)
                    {
                        _logger.LogError($"PayPartialScheduleInPeriodsCommandHandler_CallFail|action={item.Key}|request={_common.ConvertObjectToJSonV2(request)}|actionResultPayment={_common.ConvertObjectToJSonV2(actionResultPayment)}");
                    }
                    else
                    {
                        description = $"{request.Description} {item.Key.GetDescription()}";
                        totalMoneyCustomerPaid = Convert.ToInt64(actionResultPayment.Data);
                        switch (item.Key)
                        {
                            case Transaction_TypeMoney.Original:
                                if (totalMoneyCustomerPaid != request.MoneyOriginal)
                                {
                                    // ghi log lại để check lại
                                    _logger.LogError($"PayPartialScheduleInPeriodsCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|MoneyOriginalPaidLMS={totalMoneyCustomerPaid}");
                                }
                                if (request.MoneyOriginal > 0)
                                {
                                    _ = _loanManager.CreateLoanExtraOriginal(new Domain.Models.Loan.LoanExtraInsertDetailModel
                                    {
                                        CreateBy = request.CreateBy,
                                        LoanID = loanInfo.LoanID,
                                        PayDate = DateTime.Now,
                                        TotalMoney = request.MoneyOriginal * -1,
                                        TypeMoney = (int)Transaction_TypeMoney.Original
                                    });
                                }
                                break;
                            case Transaction_TypeMoney.Interest:
                                if (totalMoneyCustomerPaid != request.MoneyInterest)
                                {
                                    // ghi log lại để check lại
                                    _logger.LogError($"PayPartialScheduleInPeriodsCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|MoneyInterestPaidLMS={totalMoneyCustomerPaid}");
                                }
                                break;
                            case Transaction_TypeMoney.Service:
                                if (totalMoneyCustomerPaid != request.MoneyService)
                                {
                                    // ghi log lại để check lại
                                    _logger.LogError($"PayPartialScheduleInPeriodsCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|MoneyServicePaidLMS={totalMoneyCustomerPaid}");
                                }
                                break;
                            case Transaction_TypeMoney.Consultant:
                                if (totalMoneyCustomerPaid != request.MoneyConsultant)
                                {
                                    // ghi log lại để check lại
                                    _logger.LogError($"PayPartialScheduleInPeriodsCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|MoneyConsultantPaidLMS={totalMoneyCustomerPaid}");
                                }
                                break;
                            case Transaction_TypeMoney.FineLate:
                                {
                                    if (totalMoneyCustomerPaid != request.MoneyFineLate)
                                    {
                                        // ghi log lại để check lại
                                        _logger.LogError($"PayPartialScheduleInPeriodsCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|MoneyFineLatePaidLMS={totalMoneyCustomerPaid}");
                                    }
                                    request.MoneyFineLate -= totalMoneyCustomerPaid;
                                    if (request.MoneyFineLate > 0)
                                    {
                                        // trừ tiếp từ bảng loan
                                        totalMoneyCustomerPaid += request.MoneyFineLate;
                                        // ghi nhận txn đã trả phí trả chậm
                                        _ = await _transactionService.CreateTransaction(new List<Entites.Dtos.TransactionServices.RequestCreateListTransactionModel>
                                            {
                                                new Entites.Dtos.TransactionServices.RequestCreateListTransactionModel
                                                {
                                                    ActionID = (int)Transaction_Action.TraNoPhiPhatMuon,
                                                    LoanID = loanInfo.LoanID,
                                                    MoneyType = (int)Transaction_TypeMoney.FineLate,
                                                    TotalMoney = request.MoneyFineLate,
                                                    UserID = request.CreateBy
                                                }
                                            });
                                    }
                                    totalMoneyFineCustomerPaid += totalMoneyCustomerPaid;
                                    int countCheckTxn = 0;
                                    List<Domain.Tables.TblTransaction> lstTxnDetail = new List<Domain.Tables.TblTransaction>();
                                    do
                                    {
                                        await Task.Delay(100);
                                        lstTxnDetail = (await _tranTab.SelectColumns(x => x.TotalMoney, x => x.TransactionID, x => x.MoneyType)
                                                                                    .WhereClause(x => x.LoanID == loanInfo.LoanID && x.MoneyType == (int)Transaction_TypeMoney.FineLate && x.CreateDate > currentDate)
                                                                                    .QueryAsync()).ToList();
                                        countCheckTxn++;
                                    } while (lstTxnDetail.Count == 0 && countCheckTxn < _MaxPaymentContinous);

                                    List<Domain.Tables.TblDebt> lstDebt = new List<Domain.Tables.TblDebt>();
                                    if (lstTxnDetail != null && lstTxnDetail.Any())
                                    {
                                        foreach (var txn in lstTxnDetail)
                                        {
                                            lstDebt.Add(new Domain.Tables.TblDebt()
                                            {
                                                CreateBy = request.CreateBy,
                                                TotalMoney = (txn.TotalMoney * -1),
                                                CreateDate = currentDate,
                                                Description = Transaction_Action.TraNoPhiPhatMuon.GetDescription(),
                                                ReferID = loanInfo.LoanID,
                                                Status = (int)StatusCommon.Active,
                                                TargetID = loanInfo.CustomerID ?? 0,
                                                TypeID = (int)Debt_TypeID.CustomerPayFineLate,
                                                SourceID = txn.PaymentScheduleID ?? 0
                                            });
                                        }
                                        _debtTab.InsertBulk(lstDebt);
                                    }
                                    else
                                    {
                                        // log lại theo dõi
                                        _logger.LogError($"PayPartialScheduleInPeriodsCommandHandler_Warning|Not found txn FineLate|loanID={loanInfo.LoanID}|totalMoneyCustomerPaid={totalMoneyCustomerPaid}");
                                    }
                                }
                                break;
                        }
                        totalMoneyNeedPay -= totalMoneyCustomerPaid;
                        _ = await _customerService.UpdateMoneyCustomer((long)loanInfo.CustomerID, totalMoneyCustomerPaid * -1, description);
                    }
                }

                if (totalMoneyNeedPayBefore != totalMoneyNeedPay)
                {
                    response = await _loanManager.UpdateLoanAfterPayment(loanInfo.LoanID, request.CreateBy, totalMoneyFineCustomerPaid);
                    if (response.Result != (int)ResponseAction.Success)
                    {
                        _logger.LogError($"PayPartialScheduleInPeriodsCommandHandler_UpdateLoanFail|request={_common.ConvertObjectToJSonV2(request)}|loanUpdateAfterPayment={_common.ConvertObjectToJSonV2(loanInfo)}");
                    }
                }
                response.SetSucces();
                response.Data = loanInfo.LoanID;
            }
            catch (Exception ex)
            {
                _logger.LogError($"PayPartialScheduleInPeriodsCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
