﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class TranferLoanCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public long LenderID { get; set; }
        public string FromDate { get; set; }
    }
    public class TranferLoanCommandHandler : IRequestHandler<TranferLoanCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblContractLoanWithLender> _contractLoanWithLenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblKafkaLogEvent> _kakfaLogEventTab;
        ILogger<TranferLoanCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public TranferLoanCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblContractLoanWithLender> contractLoanWithLenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblKafkaLogEvent> kakfaLogEventTab,
            ILogger<TranferLoanCommandHandler> logger)
        {
            _loanTab = loanTab;
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _contractLoanWithLenderTab = contractLoanWithLenderTab;
            _kakfaLogEventTab = kakfaLogEventTab;
        }
        public async Task<ResponseActionResult> Handle(TranferLoanCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            //Cập nhật bảng TblContractLoanWithLender theo thông tin sau:
            DateTime fromDate = DateTime.Now;
            DateTime dtNow = DateTime.Now;
            try
            {
                if (!string.IsNullOrEmpty(request.FromDate))
                {
                    if (!DateTime.TryParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, null, System.Globalization.DateTimeStyles.None, out fromDate))
                    {
                        response.Message = MessageConstant.TimeFormatError;
                        return response;
                    }
                }
                var lenderInfosTask = _lenderTab.SelectColumns(x => x.LenderID, x => x.SelfEmployed)
                                            .WhereClause(x => x.LenderID == request.LenderID).QueryAsync();
                var contractCurrentLoanTask = _contractLoanWithLenderTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync();
                await Task.WhenAll(lenderInfosTask, contractCurrentLoanTask);
                var lenderTakeCareInfo = lenderInfosTask.Result.FirstOrDefault();
                var contractCurrentLoan = contractCurrentLoanTask.Result;
                if (contractCurrentLoan == null || !contractCurrentLoan.Any())
                {
                    _logger.LogError($"TransferListLoanCommandHandler|TblContractLoanWithLender_not_found|request={_common.ConvertObjectToJSonV2(request)}");
                    response.Message = MessageConstant.NoData;
                    return response;
                }
                // lấy hợp đồng có todate > ngày hết hiệu lực
                var contractLast = contractCurrentLoan.Where(x => x.ToDate > fromDate).OrderByDescending(x => x.ContractLoanWithLenderID).FirstOrDefault();
                if (contractLast == null || contractLast.ContractLoanWithLenderID < 1)
                {
                    response.Message = MessageConstant.MoveLoanHigherTimeLoan;
                    return response;
                }
                if (contractLast.LenderID == request.LenderID)
                {
                    response.Message = MessageConstant.LenderSameCurrent_Error;
                    return response;
                }
                var toDate = contractLast.ToDate;
                contractLast.ToDate = fromDate.AddDays(-1);
                contractLast.ModifyDate = dtNow;
                var newContract = new Domain.Tables.TblContractLoanWithLender()
                {
                    LoanID = contractLast.LoanID,
                    ContractLoanWithLenderID = request.LenderID,
                    FromDate = fromDate,
                    ToDate = toDate,
                    CreateDate = dtNow,
                    LenderID = request.LenderID,
                    ModifyDate = dtNow,
                    TimaLoanID = contractLast.TimaLoanID,
                    AccountingPriorityOrderID = 1,
                    RateConsultant = contractLast.RateConsultant,
                    RateInterest = contractLast.RateInterest,
                    RateService = contractLast.RateService,
                    SelfEmployed = lenderTakeCareInfo.SelfEmployed,
                    UnitRate = contractLast.UnitRate
                };
                await _contractLoanWithLenderTab.UpdateAsync(contractLast);
                await _contractLoanWithLenderTab.InsertAsync(newContract);
                // 19-03-2022: xử lý lại
                //_ = await _kakfaLogEventTab.InsertAsync(new Domain.Tables.TblKafkaLogEvent
                //{
                //    CreateDate = DateTime.Now,
                //    ModifyDate = DateTime.Now,
                //    DataMessage = _common.ConvertObjectToJSonV2(new LMS.Kafka.Messages.Loan.LoanPaymentSuccess
                //    {
                //        LoanID = request.LoanID
                //    }),
                //    TopicName = Kafka.Constants.KafkaTopics.LoanPaymentSuccess,
                //    ServiceName = ServiceHostInternal.LoanService,
                //    Status = (int)KafkaLogEvent_Status.Waitting,
                //});
                response.SetSucces();
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"TranferLoanCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;

        }
    }
}
