﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class AGForceCloseLoanByLoanIDCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanAgID { get; set; }
        public long MoneyOriginal { get; set; }
        public long MoneyService { get; set; }
        public long MoneyInterest { get; set; }
        public long MoneyConsultant { get; set; }
        public long MoneyFineOriginal { get; set; }
        public long OtherMoney { get; set; }
        public string CloseDate { get; set; }// dd/mm/yyyy
        public long TotalMoneyPay { get; set; }
        public long CreateBy { get; set; }
        public int TypeCloseLoan { get; set; }
        public string Description { get; set; }
        public long InsurancePartnerCode { get; set; }
    }
    public class AGForceCloseLoanByLoanIDCommandHandler : IRequestHandler<AGForceCloseLoanByLoanIDCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebt> _loanDebtTab;
        RestClients.IPaymentScheduleService _paymentService;
        RestClients.ICustomerService _customerService;
        LMS.Common.Helper.Utils _common;
        ILogger<AGForceCloseLoanByLoanIDCommandHandler> _logger;
        IEnumerable<Services.ICloseLoanManager> _closeLoanManager;
        Services.ILoanManager _loanManager;
        public AGForceCloseLoanByLoanIDCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebt> loanDebtTab,
            RestClients.ICustomerService customerService,
            RestClients.IPaymentScheduleService paymentService,
            IEnumerable<Services.ICloseLoanManager> closeLoanManager,
            Services.ILoanManager loanManager,
            ILogger<AGForceCloseLoanByLoanIDCommandHandler> logger)
        {
            _loanTab = loanTab;
            _customerTab = customerTab;
            _loanDebtTab = loanDebtTab;
            _customerService = customerService;
            _paymentService = paymentService;
            _logger = logger;
            _closeLoanManager = closeLoanManager;
            _loanManager = loanManager;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(AGForceCloseLoanByLoanIDCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var dtNow = DateTime.Now;
                var typeCloseLoanAction = _closeLoanManager.SingleOrDefault(x => (int)x.TypeCloseLoan == request.TypeCloseLoan);
                if (typeCloseLoanAction == null)
                {
                    _logger.LogError($"AGForceCloseLoanByLoanIDCommandHandler_NotFoundTypeCloseLoanAction|request={_common.ConvertObjectToJSonV2(request)}");
                    return response;
                }
                var loanInfo = (await _loanTab.WhereClause(x => x.TimaLoanID == request.LoanAgID).QueryAsync()).FirstOrDefault();
                if (loanInfo == null || loanInfo.LoanID < 1)
                {
                    _logger.LogError($"AGForceCloseLoanByLoanIDCommandHandler_NotFoundLoanAgID|request={_common.ConvertObjectToJSonV2(request)}");
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                var closeDate = DateTime.ParseExact(request.CloseDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                response = await typeCloseLoanAction.CloseLoan(loanInfo.LoanID, request.MoneyOriginal, request.MoneyService, request.MoneyInterest, request.MoneyConsultant, request.OtherMoney, request.MoneyFineOriginal, closeDate, request.CreateBy, request.InsurancePartnerCode);
                if (response.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                {
                    long totalMoneyCustomerPaid = Convert.ToInt64(response.Data);
                    if (request.TypeCloseLoan != (int)LMS.Common.Constants.Loan_Status.CloseIndemnifyLoanInsurance)
                    {
                        loanInfo.FinishDate = closeDate;
                    }
                    //loanInfo.LastDateOfPay = closeDate;
                    loanInfo.Status = typeCloseLoanAction.GetStatusLoan();
                    //loanInfo.FinishDate = closeDate;
                    loanInfo.ModifyDate = dtNow;
                    if (totalMoneyCustomerPaid > 0)
                    {
                        // update loan
                        if (request.OtherMoney > 0)
                        {
                            loanInfo.MoneyFineLate -= request.OtherMoney;
                            _ = _loanDebtTab.InsertAsync(new Domain.Tables.TblDebt
                            {
                                CreateBy = request.CreateBy,
                                ReferID = loanInfo.LoanID,
                                TargetID = (long)loanInfo.CustomerID,
                                CreateDate = DateTime.Now,
                                Status = (int)StatusCommon.Active,
                                TotalMoney = (request.OtherMoney * -1),
                                TypeID = (int)Debt_TypeID.CustomerPayFineLate,
                                Description = Debt_TypeID.CustomerPayFineLate.GetDescription()
                            });
                        }

                        if (request.MoneyOriginal > 0)
                        {
                            _ = _loanManager.CreateLoanExtraOriginal(new Domain.Models.Loan.LoanExtraInsertDetailModel
                            {
                                CreateBy = request.CreateBy,
                                LoanID = loanInfo.LoanID,
                                PayDate = DateTime.Now,
                                TotalMoney = request.TotalMoneyPay * -1,
                                TypeMoney = (int)Transaction_TypeMoney.Original
                            });
                        }
                        loanInfo.TotalMoneyCurrent -= request.MoneyOriginal;
                        _ = _customerService.UpdateMoneyCustomer((long)loanInfo.CustomerID, totalMoneyCustomerPaid * -1, request.Description);
                    }

                    var updateLoan = await _loanTab.UpdateAsync(loanInfo);
                    if (!updateLoan)
                    {
                        _logger.LogError($"AGForceCloseLoanByLoanIDCommandHandler_UpdateLoanError|request={_common.ConvertObjectToJSonV2(request)}|paymentScheduleSuccess={_common.ConvertObjectToJSonV2(response)}");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"AGForceCloseLoanByLoanIDCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
