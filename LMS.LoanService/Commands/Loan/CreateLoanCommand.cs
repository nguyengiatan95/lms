﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Tables;
using LMS.LoanServiceApi.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands
{
    // giữ tạm thời như bên app đang gọi hoặc ag sẽ gọi
    public class CreateLoanCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanCreditID { get; set; }

        public long ShopLenderID { get; set; }

        public long MoneyfeeInsuranceOfCustomer { get; set; }

        public int SourceBankDisbursement { get; set; }

        public int IsHandleException { get; set; }

        // Ag gọi qua sẽ có bankcardid
        public long BankCardID { get; set; }
        public long LoanAgID { get; set; }
        public long CreateBy { get; set; }
        public long MoneyFeeInsuranceMaterialCovered { get; set; }
        public long AgCodeID { get; set; }
        public long MoneyFeeDisbursement { get; set; }
        public long TimaCustomerID { get; set; }
        public string FromDate { get; set; }
    }
    public class CreateLoanCommandHandler : IRequestHandler<CreateLoanCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        ILogger<CreateLoanCommandHandler> _logger;
        RestClients.IOutGatewayService _outGateway;
        LMS.Common.Helper.Utils _common;
        ILoanManager _loanManager;
        ILoanEvenManager _loanEventManager;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> _settingKeyTab;
        public CreateLoanCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            ILogger<CreateLoanCommandHandler> logger,
            RestClients.IOutGatewayService outGateway,
            ILoanManager loanManager,
            ILoanEvenManager loanEventManager,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> settingKeyTab
            )
        {
            _loanTab = loanTab;
            _lenderTab = lenderTab;
            _logger = logger;
            _outGateway = outGateway;
            _common = new Common.Helper.Utils();
            _loanManager = loanManager;
            _loanEventManager = loanEventManager;
            _settingKeyTab = settingKeyTab;
        }
        public async Task<ResponseActionResult> Handle(CreateLoanCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var keySettingInfoTask = _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.System_SwitchKafkaHandle).QueryAsync();

                // kiểm tra đơn đã tạo chưa
                var loanLmsDetailExistTask = _loanTab.WhereClause(x => x.LoanCreditIDOfPartner == request.LoanCreditID).QueryAsync();
                await Task.WhenAll(keySettingInfoTask, loanLmsDetailExistTask);

                var loanLmsDetailExist = loanLmsDetailExistTask.Result.FirstOrDefault();
                var keySettingInfo = keySettingInfoTask.Result.FirstOrDefault();

                if (loanLmsDetailExist != null && loanLmsDetailExist.LoanID > 0)
                {
                    response.Message = MessageConstant.LoanExist;
                    return response;
                }

                LMS.Entites.Dtos.LOSServices.ResultLoanWaitingDisbursementDetailLOS loanDetailLos = await _outGateway.GetLoanCreditDetailAsync((int)request.LoanCreditID);
                if (loanDetailLos == null || loanDetailLos.LoanBriefId < 1)
                {
                    response.Message = LMS.Common.Constants.MessageConstant.LoanNotFoundLoanCreditPartner;
                    return response;
                }
                var lstSourceByInsuranceIDs = ((Loan_SourcecBuyInsurance[])Enum.GetValues(typeof(Loan_SourcecBuyInsurance))).Select(c => (int)c).ToList();
                if (!lstSourceByInsuranceIDs.Contains(loanDetailLos.TypeInsurence))
                {
                    // log warning
                    _logger.LogError($"CreateLoanCommandHandler_Warning|TypeInsurence={loanDetailLos.TypeInsurence}|detaiLos={_common.ConvertObjectToJSonV2(loanDetailLos)}");
                }
                var fromDate = DateTime.Now;
                if (!string.IsNullOrEmpty(request.FromDate))
                {
                    if (!DateTime.TryParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, null, System.Globalization.DateTimeStyles.None, out fromDate))
                    {
                        _logger.LogError($"CreateLoanCommandHandler_Waring_FromDate|request={_common.ConvertObjectToJSonV2(request)}");
                        fromDate = DateTime.Now;
                    }
                }
                if (keySettingInfo != null && keySettingInfo.Status == (int)StatusCommon.Active)
                {
                    return await _loanEventManager.CreateLoanAsync(new Domain.Models.Loan.RequestCreateLoanModel
                    {
                        BankCardID = request.BankCardID,
                        CreateBy = request.CreateBy,
                        LoanCreditID = request.LoanCreditID,
                        LoanAgID = request.LoanAgID,
                        SourceBankDisbursement = request.SourceBankDisbursement,
                        MoneyfeeInsuranceOfCustomer = request.MoneyfeeInsuranceOfCustomer,
                        MoneyFeeInsuranceMaterialCovered = request.MoneyFeeInsuranceMaterialCovered,
                        IsHandleException = request.IsHandleException,
                        AgCodeID = request.AgCodeID,
                        MoneyFeeDisbursement = request.MoneyFeeDisbursement,
                        FromDate = fromDate,
                        TimaCustomerID = request.TimaCustomerID
                    });
                }
                else
                {
                    return await _loanManager.CreateLoanAsync(new Domain.Models.Loan.RequestCreateLoanModel
                    {
                        BankCardID = request.BankCardID,
                        CreateBy = request.CreateBy,
                        LoanCreditID = request.LoanCreditID,
                        LoanAgID = request.LoanAgID,
                        SourceBankDisbursement = request.SourceBankDisbursement,
                        MoneyfeeInsuranceOfCustomer = request.MoneyfeeInsuranceOfCustomer,
                        MoneyFeeInsuranceMaterialCovered = request.MoneyFeeInsuranceMaterialCovered,
                        IsHandleException = request.IsHandleException,
                        AgCodeID = request.AgCodeID,
                        MoneyFeeDisbursement = request.MoneyFeeDisbursement,
                        FromDate = fromDate,
                        TimaCustomerID = request.TimaCustomerID
                    });
                }
                
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateLoanCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
