﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class AGCuttingOffPaymentLoanInsuranceByLoanIDCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanAgID { get; set; }
        public long TotalMoneyPay { get; set; }
        public Transaction_TypeMoney TypeMoney { get; set; }

        public long CreateBy { get; set; }
        public string Description { get; set; }
    }
    public class AGCuttingOffPaymentLoanInsuranceByLoanIDCommandHandler : IRequestHandler<AGCuttingOffPaymentLoanInsuranceByLoanIDCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        RestClients.IPaymentScheduleService _paymentService;
        RestClients.ICustomerService _customerService;
        LMS.Common.Helper.Utils _common;
        Services.ILoanManager _loanManager;
        ILogger<AGCuttingOffPaymentLoanInsuranceByLoanIDCommandHandler> _logger;
        public AGCuttingOffPaymentLoanInsuranceByLoanIDCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            RestClients.ICustomerService customerService,
            RestClients.IPaymentScheduleService paymentService,
            Services.ILoanManager loanManager,
            ILogger<AGCuttingOffPaymentLoanInsuranceByLoanIDCommandHandler> logger)
        {
            _loanTab = loanTab;
            _customerTab = customerTab;
            _customerService = customerService;
            _paymentService = paymentService;
            _loanManager = loanManager;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(AGCuttingOffPaymentLoanInsuranceByLoanIDCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var loanInfo = (await _loanTab.WhereClause(x => x.TimaLoanID == request.LoanAgID).QueryAsync()).FirstOrDefault();
                var actionResultPayment = await _paymentService.CuttingOffPaymentLoanInsuranceByLoanID(loanInfo.LoanID, request.TotalMoneyPay, request.TypeMoney, request.CreateBy);
                if (actionResultPayment.Result == (int)ResponseAction.Success)
                {
                    long totalMoneyCustomerPaid = Convert.ToInt64(actionResultPayment.Data);
                    if (totalMoneyCustomerPaid != request.TotalMoneyPay)
                    {
                        // ghi log lại để check lại
                        _logger.LogError($"AGCuttingOffPaymentLoanInsuranceByLoanIDCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|moneyPaidLMS={totalMoneyCustomerPaid}");
                    }
                    if (request.TypeMoney == Transaction_TypeMoney.Original)
                    {
                        _ = await _loanManager.CreateLoanExtraOriginal(new Domain.Models.Loan.LoanExtraInsertDetailModel
                        {
                            CreateBy = request.CreateBy,
                            LoanID = loanInfo.LoanID,
                            PayDate = DateTime.Now,
                            TotalMoney = request.TotalMoneyPay * -1,
                            TypeMoney = (int)Transaction_TypeMoney.Original
                        });
                    }
                    response = await _loanManager.UpdateLoanAfterPayment(loanInfo.LoanID, request.CreateBy);
                    if (response.Result != (int)ResponseAction.Success)
                    {
                        _logger.LogError($"AGCuttingOffPaymentLoanInsuranceByLoanIDCommandHandler_UpdateLoanFail|request={_common.ConvertObjectToJSonV2(request)}|loanUpdateAfterPayment={_common.ConvertObjectToJSonV2(loanInfo)}");
                    }
                    _ = await _customerService.UpdateMoneyCustomer((long)loanInfo.CustomerID, totalMoneyCustomerPaid * -1, request.Description);
                }
                response.SetSucces();
                response.Data = loanInfo.LoanID;
            }
            catch (Exception ex)
            {
                _logger.LogError($"AGCuttingOffPaymentLoanInsuranceByLoanIDCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
