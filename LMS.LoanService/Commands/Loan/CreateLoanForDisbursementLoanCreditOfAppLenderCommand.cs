﻿using LMS.Entites.Dtos.LOSServices;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LMS.LoanServiceApi.Constants;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class CreateLoanForDisbursementLoanCreditOfAppLenderCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanBriefID { get; set; }
        public long UserID { get; set; }
    }
    public class CreateLoanForDisbursementLoanCreditOfAppLenderCommandHandler : IRequestHandler<CreateLoanForDisbursementLoanCreditOfAppLenderCommand, LMS.Common.Constants.ResponseActionResult>
    {
        private readonly IMediator _bus;
        RestClients.ILOSService _lOSService;
        ILogger<CreateLoanForDisbursementLoanCreditOfAppLenderCommandHandler> _logger;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanCreditDisbursementAuto> _loanCreditDisbursementAutoTab;
        Common.Helper.Utils _common;
        public CreateLoanForDisbursementLoanCreditOfAppLenderCommandHandler(
            IMediator bus,
            RestClients.ILOSService lOSService,
            ILogger<CreateLoanForDisbursementLoanCreditOfAppLenderCommandHandler> logger,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanCreditDisbursementAuto> loanCreditDisbursementAutoTab
        )
        {
            this._bus = bus;
            _lOSService = lOSService;
            _logger = logger;
            _loanTab = loanTab;
            _userTab = userTab;
            _loanCreditDisbursementAutoTab = loanCreditDisbursementAutoTab;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(CreateLoanForDisbursementLoanCreditOfAppLenderCommand req, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    var objLoanLos = _lOSService.GetLoanCreditDetail(req.LoanBriefID).Result;
                    if (objLoanLos == null)
                    {
                        response.Message = MessageConstant.LoanNotExist;
                        return response;
                    }
                    if (objLoanLos.IsLock == false)
                    {
                        response.Message = MessageConstant.IsLockOpen;
                        return response;
                    }
                    var objUser = _userTab.WhereClause(x => x.UserID == req.UserID).Query().FirstOrDefault();
                    if (objUser == null)
                    {
                        response.Message = MessageConstant.NotIdentifiedUserID;
                        return response;
                    }
                    var loanDetail = _loanTab.WhereClause(x => x.LoanCreditIDOfPartner == req.LoanBriefID && x.Status == (int)StateLoanCreditNew.AccountantDisbursed).Query().FirstOrDefault();
                    if (loanDetail != null && loanDetail.LoanID > 0)
                    {
                        response.Message = MessageConstant.LoanExist;
                        return response;
                    }
                    // check log đi tiền 

                    // tạo hđ
                    var sourceBankDisbursement = (int)SourceBankDisbursementOfApp.NamA;
                    var isHandleException = 1;
                    long moneyfeeInsuranceOfCustomer = 0;
                    long moneyFeeInsuranceMaterialCovered = 0;
                    var bankCardID = 0;
                    var result = _bus.Send(new Commands.CreateLoanCommand
                    {
                        LoanCreditID = (int)req.LoanBriefID,
                        ShopLenderID = (int)objLoanLos.LenderId,
                        MoneyfeeInsuranceOfCustomer = moneyfeeInsuranceOfCustomer,
                        SourceBankDisbursement = sourceBankDisbursement,
                        IsHandleException = isHandleException,
                        BankCardID = bankCardID,
                        CreateBy = req.UserID
                    }).Result;
                    return result;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"CreateLoanForDisbursementLoanCreditOfAppLenderCommand|request={_common.ConvertObjectToJSonV2(req)}|ex={ex.Message}-{ex.StackTrace}");
                }
                return response;
            });
        }
    }
}
