﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Models.Loan;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class ExtendLoanTimeCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public RequestExtendLoanTime Model { get; set; }
    }
    public class ExtendLoanTimeCommandHandler : IRequestHandler<ExtendLoanTimeCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblExtensionLoan> _extensionLoanTab;
        ILogger<ExtendLoanTimeCommandHandler> _logger;
        Common.Helper.Utils _common;
        RestClients.IPaymentScheduleService _paymentScheduleService;
        public ExtendLoanTimeCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblExtensionLoan> extensionLoanTab,
            RestClients.IPaymentScheduleService paymentScheduleService,
            ILogger<ExtendLoanTimeCommandHandler> logger)
        {
            _loanTab = loanTab;
            _logger = logger;
            _extensionLoanTab = extensionLoanTab;
            _common = new Common.Helper.Utils();
            _paymentScheduleService = paymentScheduleService;
        }

        public async Task<ResponseActionResult> Handle(ExtendLoanTimeCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                DateTime dtNow = DateTime.Now;
                // lấy thông tin đơn
                var lstRateType = new List<int>()
                {
                    (int)PaymentSchedule_ActionRateType.EndingBalance,
                    (int)PaymentSchedule_ActionRateType.RateDayMonthly,
                     (int)PaymentSchedule_ActionRateType.RateDay,
                     (int)PaymentSchedule_ActionRateType.RateTotalMoney,
                     (int)PaymentSchedule_ActionRateType.RateMonth,
                     (int)PaymentSchedule_ActionRateType.RateWeekPercent,
                     (int)PaymentSchedule_ActionRateType.RateWeekVND,
                };
                IEnumerable<Domain.Tables.TblLoan> lstLoan;
                if (request.Model.TimaLoanID > 0) // dong bo tu ben ag sang
                {
                    lstLoan = await _loanTab.WhereClause(x => x.TimaLoanID == request.Model.TimaLoanID && x.Status == (int)Loan_Status.Lending
                        && x.SourcecBuyInsurance < 1).QueryAsync();
                    request.Model.LoanID = lstLoan != null && lstLoan.Any() ? lstLoan.FirstOrDefault().LoanID : 0;
                }
                else
                {
                    lstLoan = await _loanTab.WhereClause(x => x.LoanID == request.Model.LoanID && x.Status == (int)Loan_Status.Lending
                                  && x.SourcecBuyInsurance < 1).QueryAsync();
                }
                if (lstLoan == null || !lstLoan.Any())
                {
                    response.Message = MessageConstant.LoanNotExtend;
                    return response;
                }

                if (!lstRateType.Contains(lstLoan.FirstOrDefault().RateType))
                {
                    response.Message = MessageConstant.LoanRateTypeNotExtend;
                    return response;
                }
                var lstPaymentSchedule = await _paymentScheduleService.GetLstPaymentByLoanID(request.Model.LoanID);
                if (lstPaymentSchedule == null || !lstPaymentSchedule.Any())
                {
                    response.Message = MessageConstant.Payment_NotFound;
                    return response;
                }
                // gán lại vào log
                var loanInfo = lstLoan.FirstOrDefault();
                // update lại loan

                loanInfo.LoanTime += request.Model.PeriodExtend * loanInfo.Frequency;
                var currentToDate = loanInfo.ToDate;
                loanInfo.ToDate = GetToDateByRateType(loanInfo.RateType, loanInfo.FromDate, loanInfo.LoanTime);
                Domain.Tables.TblExtensionLoan tblExtensionLoan = new Domain.Tables.TblExtensionLoan()
                {
                    CreateDate = dtNow,
                    LoanID = request.Model.LoanID,
                    NewDate = loanInfo.ToDate,
                    Note = request.Model.Note,
                    OldDate = currentToDate,
                    PeriodExtend = request.Model.PeriodExtend,
                    UserID = request.Model.UserID
                };
                bool updateLoan = _loanTab.Update(loanInfo);
                if (updateLoan)
                {
                    response.SetSucces();
                    var insertExtend = await _extensionLoanTab.InsertAsync(tblExtensionLoan);
                    if (insertExtend > 0)
                    {
                        response.Data = insertExtend;
                    }
                    int frequencyByDay = GetNumberDayFrequencyByRateType(loanInfo.RateType, loanInfo.Frequency);
                    var actionResponse = await _paymentScheduleService.ExtendPaymentScheduleByLoanID(
                        loanInfo.TotalMoneyCurrent,
                        request.Model.PeriodExtend * frequencyByDay,
                        frequencyByDay,
                        loanInfo.RateType,
                        loanInfo.RateConsultant,
                        loanInfo.RateInterest,
                        loanInfo.RateService,
                        loanInfo.LoanID
                    );
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ExtendLoanTimeCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        private DateTime GetToDateByRateType(int rateType, DateTime fromDate, int loantime)
        {
            switch (rateType)
            {
                case (int)PaymentSchedule_ActionRateType.RateDay:
                case (int)PaymentSchedule_ActionRateType.RateTotalMoney:
                case (int)PaymentSchedule_ActionRateType.RateAmortization:
                    return fromDate.AddDays(loantime - 1);
                case (int)PaymentSchedule_ActionRateType.RateMonth:
                    return fromDate.AddDays(loantime * 30).AddDays(-1);
                case (int)PaymentSchedule_ActionRateType.RateWeekPercent:
                case (int)PaymentSchedule_ActionRateType.RateWeekVND:
                    return fromDate.AddDays(loantime * 7).AddDays(-1);
                case (int)PaymentSchedule_ActionRateType.EndingBalance:
                    return fromDate.AddMonths(loantime).AddDays(-1);
                //case (int)PaymentSchedule_ActionRateType.RateAmortizationMonth:
                //    return fromDate.AddMonths(loantime).AddDays(-1);
                default:
                    return fromDate.AddMonths(loantime).AddDays(-1);
            }
        }

        private int GetNumberDayFrequencyByRateType(int ratetype, int loanFrequency)
        {
            switch ((PaymentSchedule_ActionRateType)ratetype)
            {
                case PaymentSchedule_ActionRateType.RateDay:
                case PaymentSchedule_ActionRateType.RateTotalMoney:
                case PaymentSchedule_ActionRateType.RateAmortization:
                    return loanFrequency;
                case PaymentSchedule_ActionRateType.RateMonth:
                case PaymentSchedule_ActionRateType.RateWeekPercent:
                case PaymentSchedule_ActionRateType.RateWeekVND:
                    return loanFrequency;
                default:
                    return TimaSettingConstant.NumberDayInMonth;
            }
        }

    }
}
