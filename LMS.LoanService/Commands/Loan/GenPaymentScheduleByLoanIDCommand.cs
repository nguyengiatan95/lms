﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class GenPaymentScheduleByLoanIDCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int LoanID { get; set; }
        public long CreateBy { get; set; }
    }
    public class GenPaymentScheduleByLoanIDCommandHandler : IRequestHandler<GenPaymentScheduleByLoanIDCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        ILogger<GenPaymentScheduleByLoanIDCommandHandler> _logger;
        RestClients.IOutGatewayService _outGateway;
        RestClients.ICustomerService _customerService;
        RestClients.IPaymentScheduleService _paymentService;
        RestClients.ILenderService _lenderService;
        RestClients.IInvoiceService _invoiceService;
        RestClients.IInsuranceService _insuranceService;
        LMS.Common.Helper.Utils _common;
        public GenPaymentScheduleByLoanIDCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            ILogger<GenPaymentScheduleByLoanIDCommandHandler> logger,
            RestClients.IOutGatewayService outGateway,
            RestClients.ICustomerService customerService,
            RestClients.IPaymentScheduleService paymentService,
            RestClients.IInvoiceService invoiceService,
            RestClients.IInsuranceService insuranceService,
            RestClients.ILenderService lenderService)
        {
            _loanTab = loanTab;
            _logger = logger;
            _outGateway = outGateway;
            _customerService = customerService;
            _paymentService = paymentService;
            _lenderService = lenderService;
            _invoiceService = invoiceService;
            _insuranceService = insuranceService;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GenPaymentScheduleByLoanIDCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var loanInfo = _loanTab.WhereClause(x => x.LoanID == request.LoanID).Query().FirstOrDefault();
                if (loanInfo == null || loanInfo.LoanID < 1)
                {
                    response.Message = LMS.Common.Constants.MessageConstant.LoanNotExist;
                    return response;
                }
                var lstPaymentExists = await _paymentService.GetLstPaymentByLoanID(request.LoanID);
                if (lstPaymentExists != null && lstPaymentExists.Any())
                {
                    response.Message = LMS.Common.Constants.MessageConstant.LoanExist;
                    return response;
                }
                LMS.Entites.Dtos.LOSServices.ResultLoanWaitingDisbursementDetailLOS loanDetailLos = await _outGateway.GetLoanCreditDetailAsync((int)loanInfo.LoanCreditIDOfPartner);
                if (loanDetailLos == null)
                {
                    response.Message = LMS.Common.Constants.MessageConstant.LoanNotFoundLoanCreditPartner;
                    return response;
                }
                DateTime? nextDateTopUp = null;
                if (loanDetailLos.TopUpOfLoanBriefID > 0) // kiểm tra điều kiện của đơn Topup
                {
                    // lấy đơn cũ
                    var loanInfoTopUp = _loanTab.WhereClause(x => x.LoanCreditIDOfPartner == loanDetailLos.TopUpOfLoanBriefID).Query().FirstOrDefault();
                    if (loanInfoTopUp != null && loanInfoTopUp.LoanID > 0)
                    {
                        nextDateTopUp = loanInfoTopUp.NextDate;
                        loanInfo.RateType = (int)PaymentSchedule_ActionRateType.RateAmortization30DaysTopUp;
                    }
                }
                var actionResult = await _paymentService.CreatePaymentScheduleByLoanID(loanInfo.TotalMoneyDisbursement,
                    loanInfo.FromDate,
                    loanInfo.LoanTime * TimaSettingConstant.NumberDayInMonth,
                    loanInfo.Frequency * TimaSettingConstant.NumberDayInMonth,
                    loanInfo.RateType,
                    loanInfo.RateConsultant,
                    loanInfo.RateInterest,
                    loanInfo.RateService,
                    loanInfo.LoanID, nextDateTopUp);
                if (actionResult > 0)
                {
                    response.SetSucces();
                    response.Data = request;
                }
            }
            catch (Exception ex)
            {
                _logger.LogWarning($"UpdateInforPaymentLoanCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
