﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{

    public class SendStatusInsuranceCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public List<Domain.Models.Loan.RequestSendStatusInsuranceModel> ListInsurance { get; set; }
    }
    public class SendStatusInsuranceCommandHandler : IRequestHandler<SendStatusInsuranceCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogSendStatusInsurance> _logSendStatusInsuranceTab;
        ILogger<SendStatusInsuranceCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public SendStatusInsuranceCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogSendStatusInsurance> logSendStatusInsuranceTab,
            ILogger<SendStatusInsuranceCommandHandler> logger)
        {
            _loanTab = loanTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _logSendStatusInsuranceTab = logSendStatusInsuranceTab;
        }
        public async Task<ResponseActionResult> Handle(SendStatusInsuranceCommand request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                DateTime dtNow = DateTime.Now;
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    var lstStatusSendInsurance = Enum.GetValues(typeof(Loan_StatusSendInsurance)).Cast<int>().ToList();
                    var listLoanIds = request.ListInsurance.Select(x => x.LoanID).ToList();
                    var dicLoan = _loanTab.WhereClause(x => listLoanIds.Contains(x.LoanID)).Query().ToDictionary(x => x.LoanID, x => x);
                    var lstLoan = new List<Domain.Tables.TblLoan>();
                    var listLogSendStatusInsurance = new List<Domain.Tables.TblLogSendStatusInsurance>();
                    foreach (var item in request.ListInsurance)
                    {
                        var objLoan = dicLoan.ContainsKey(item.LoanID) ? dicLoan[item.LoanID] : null;
                        if (objLoan == null 
                        || objLoan.Status != (int)Loan_Status.Lending
                        || objLoan.StatusSendInsurance == (int)Loan_StatusSendInsurance.ProcessingComplete 
                        || !lstStatusSendInsurance.Contains(item.StatusSendInsurance))
                        {
                            response.Message += $"HD không hợp lệ : {objLoan.ContactCode}";
                            continue;
                        }
                        else
                        {
                            int curentStatus = objLoan.StatusSendInsurance;
                            objLoan.StatusSendInsurance = item.StatusSendInsurance;
                            lstLoan.Add(objLoan);
                            listLogSendStatusInsurance.Add(
                                            new Domain.Tables.TblLogSendStatusInsurance()
                                            {
                                                LoanID = objLoan.LoanID,
                                                CreateDate = dtNow,
                                                Note = item.Note,
                                                StatusNew = (short)item.StatusSendInsurance,
                                                StatusOld = (short)curentStatus,
                                                UserID = (int)item.UserID
                                            });
                        }
                      
                    }
                    if (lstLoan.Count > 0)
                    {
                        _loanTab.UpdateBulk(lstLoan);
                        response.SetSucces();
                        _logSendStatusInsuranceTab.InsertBulk(listLogSendStatusInsurance);
                    }
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"SendStatusInsuranceCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                }
                return response;
            });
        }
    }
}
