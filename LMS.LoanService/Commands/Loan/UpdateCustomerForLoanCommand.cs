﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class UpdateCustomerForLoanCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public long LoanID { get; set; }
        [Required]
        public long CustomerID { get; set; }
    }
    public class UpdateCustomerForLoanCommandHandler : IRequestHandler<UpdateCustomerForLoanCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.LoanUpdateCustomerInfo> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLoan> _logLoanTab;
        RestClients.IPaymentScheduleService _paymentService;
        RestClients.IOutGatewayService _outGateway;
        ILogger<UpdateCustomerForLoanCommandHandler> _logger;
        Common.Helper.Utils _common;
        public UpdateCustomerForLoanCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.LoanUpdateCustomerInfo> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLoan> logLoanTab,
            RestClients.IPaymentScheduleService paymentService,
            RestClients.IOutGatewayService outGateway,
            ILogger<UpdateCustomerForLoanCommandHandler> logger)
        {
            _loanTab = loanTab;
            _transactionTab = transactionTab;
            _logLoanTab = logLoanTab;
            _logger = logger;
            _paymentService = paymentService;
            _outGateway = outGateway;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(UpdateCustomerForLoanCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var loanInfos = await _loanTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync();
                if (loanInfos == null || !loanInfos.Any())
                {
                    _logger.LogError($"UpdateCustomerForLoanCommandHandler_Warning|Not_found_loan_info|request={_common.ConvertObjectToJSonV2(request)}");
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                var loanDetail = loanInfos.FirstOrDefault();
                if (loanDetail.CustomerID > 0)
                {
                    _logger.LogError($"UpdateCustomerForLoanCommandHandler_Warning|Customer_Exists|request={_common.ConvertObjectToJSonV2(request)}");
                }
                loanDetail.CustomerID = request.CustomerID;
                _ = _loanTab.UpdateAsync(loanDetail);
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateCustomerForLoanCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;

        }
    }
}
