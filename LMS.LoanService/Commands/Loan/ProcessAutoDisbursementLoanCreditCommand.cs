﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.APIAutoDisbursement;
using LMS.LoanServiceApi.Domain.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class ProcessAutoDisbursementLoanCreditCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class ProcessAutoDisbursementLoanCreditCommandHandler : IRequestHandler<ProcessAutoDisbursementLoanCreditCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> _settingKeyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanCreditDisbursementAuto> _loanCreditDisbursementAutoTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBlackList> _blackListTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblProductCredit> _productCreditTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBank> _bankTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSmsDisbursement> _smsDisbursementTab;
        Services.ILoanManager _loanManager;
        Services.IPaymentGatewayService _paymentGatewayService;
        RestClients.ILOSService _losManager;
        ILogger<ProcessAutoDisbursementLoanCreditCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        IMediator _mediator;

        private const string _PrefixAuto = "[Auto]";
        private const int TimeStartWork = 9;
        /// <summary>
        /// timeout sms otp max = 180s, task lặp lại sau 2s -> 160s lần để tìm sms, 
        /// </summary>
        private const int MaxRetryWaitOTP = 80;
        private const int BankCardID_VIB2 = 6;
        private const int MaxTimeWaitingNext = 5;
        private const int MinCountPushNotiAccountant = 6;

        List<long> _lstBigProductID = new List<long>();
        DateTime _timeLatestSucces = DateTime.Now.AddHours(-1);

        public ProcessAutoDisbursementLoanCreditCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> settingKeyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanCreditDisbursementAuto> loanCreditDisbursementAutoTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBlackList> blackListTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblProductCredit> productCreditTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBank> bankTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSmsDisbursement> smsDisbursementTab,
            Services.ILoanManager loanManager,
            Services.IPaymentGatewayService paymentGatewayService,
            RestClients.ILOSService losManager,
            IMediator mediator,
            ILogger<ProcessAutoDisbursementLoanCreditCommandHandler> logger)
        {
            _settingKeyTab = settingKeyTab;
            _loanCreditDisbursementAutoTab = loanCreditDisbursementAutoTab;
            _lenderTab = lenderTab;
            _blackListTab = blackListTab;
            _loanTab = loanTab;
            _customerTab = customerTab;
            _productCreditTab = productCreditTab;
            _bankTab = bankTab;
            _smsDisbursementTab = smsDisbursementTab;
            _loanManager = loanManager;
            _losManager = losManager;
            _paymentGatewayService = paymentGatewayService;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _mediator = mediator;
        }

        public async Task<ResponseActionResult> Handle(ProcessAutoDisbursementLoanCreditCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var settingKeyInfo = _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.Loan_ProcessAutoDisbursement).Query().FirstOrDefault();
                // tắt không chạy tự động, không ghi nhận sms
                if (settingKeyInfo == null)
                {
                    response.Message = MessageConstant.SettingKey_Loan_ProcessAutoDisbursementStatus;
                    return response;
                }
                SettingKeyValueAutoDisbursement keyValueAutoDisbursement = new SettingKeyValueAutoDisbursement();
                if (string.IsNullOrEmpty(settingKeyInfo.Value))
                {
                    keyValueAutoDisbursement = new SettingKeyValueAutoDisbursement
                    {
                        LastTurnOff = settingKeyInfo.ModifyDate,
                        LoanCreditDisbursementAutoIDProcessing = 0,
                        LastTimeSuccess = DateTime.Now.AddHours(-1),
                        CountPushNotiAccountant = 0
                    };
                }
                else
                {
                    keyValueAutoDisbursement = _common.ConvertJSonToObjectV2<SettingKeyValueAutoDisbursement>(settingKeyInfo.Value);
                }
                if (settingKeyInfo.Status == (int)SettingKey_Status.InActice)
                {
                    response.Message = MessageConstant.SettingKey_Loan_ProcessAutoDisbursementStatus;

                    // chỉ thông báo 1 lần
                    if (keyValueAutoDisbursement.CountPushNotiAccountant == 0)
                    {
                        keyValueAutoDisbursement.CountPushNotiAccountant++;
                        //_slackClient.SendNotifyForDev($"{_PrefixAuto} HandleAuto_TurnOFF|key={Entity.SettingKeyConstant.LoanCreditDisbursementAuto_TurnOn}");
                        _logger.LogError($"{_PrefixAuto} key={LMS.Common.Constants.SettingKey_KeyValue.Loan_ProcessAutoDisbursement} Stop");
                    }
                    var dateStartWork = DateTime.Now.Date.AddHours(TimeStartWork);
                    var timeMaxWaitting = DateTime.Now.Subtract(keyValueAutoDisbursement.LastTurnOff);
                    if (DateTime.Now > dateStartWork && timeMaxWaitting.Minutes > MaxTimeWaitingNext)
                    {
                        keyValueAutoDisbursement.CountPushNotiAccountant++;
                        // chuyển các đơn chờ hơn 30p chưa giải ngân
                        if (keyValueAutoDisbursement.CountPushNotiAccountant > MinCountPushNotiAccountant)
                        {
                            _ = ChangeStatusToManually(endTime: DateTime.Now.AddMinutes((MaxTimeWaitingNext * MinCountPushNotiAccountant) * -1));
                            keyValueAutoDisbursement.LastTurnOff = DateTime.Now;
                        }
                        //_slackClient.SendNotifyToAccountGroup($"{_PrefixAuto} Tự động giải ngân chưa chạy trong giờ làm việc. Mời team KT kiểm tra lại.", title: $"{_PrefixAuto} Đơn vay cần giải ngân");

                    }
                }
                else if (settingKeyInfo.Status == (int)SettingKey_Status.PushToAccountant)
                {
                    response.Message = MessageConstant.SettingKey_Loan_ProcessAutoDisbursementStatusPushAccountant;
                    _ = ChangeStatusToManually();
                }
                else
                {
                    // setting key bật chạy lại
                    if (keyValueAutoDisbursement.CountPushNotiAccountant != 0 && settingKeyInfo.Status == (int)SettingKey_Status.Active)
                    {
                        keyValueAutoDisbursement.CountPushNotiAccountant = 0;
                        // mở key chạy lại
                        // remove các đơn đang chờ otp
                        RemoveLoanWaitingInvalid();
                    }
                    // xử lý gửi otp
                    if (keyValueAutoDisbursement.LoanCreditDisbursementAutoIDProcessing == 0)
                    {
                        _lstBigProductID = _productCreditTab.WhereClause(x => x.TypeProduct == (int)Product_Type.Big).Query().Select(x => (long)x.ProductCreditID).ToList();
                        response.Message = string.Format(MessageConstant.SettingKey_Loan_ProcessAutoDisbursementProcessing, keyValueAutoDisbursement.LoanCreditDisbursementAutoIDProcessing);
                        await HandleCreateOtp(keyValueAutoDisbursement);
                    }
                    else
                    {
                        // xử lý otp
                        await HandleVerifyOtp(keyValueAutoDisbursement);
                    }
                }
                response.SetSucces();
                settingKeyInfo.Value = _common.ConvertObjectToJSonV2(keyValueAutoDisbursement);
                _settingKeyTab.Update(settingKeyInfo);
                response.Data = keyValueAutoDisbursement;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessAutoDisbursementLoanCreditCommandHandler|ex={ex.Message}-{ex.StackTrace}");
            }

            return response;
        }

        private async Task HandleCreateOtp(SettingKeyValueAutoDisbursement keyValueDisbursementInfo)
        {
            bool openLock = true;
            var loanDisubermentInfo = this.GetLoanCreditWaitingDisbursement();
            if (loanDisubermentInfo == null)
            {
                // không có đơn chờ giải ngân
                keyValueDisbursementInfo.LastTimeSuccess = DateTime.Now;
                return;
            }
            else
            {
                // chờ xử gửi otp
                int actionResult = await SendRequestGetOTP(loanDisubermentInfo, keyValueDisbursementInfo);
                switch (actionResult)
                {
                    case 0: // lỗi
                        loanDisubermentInfo.Status = (int)LoanCreditDisbursementAuto_Status.Fail;
                        loanDisubermentInfo.ModifyDate = DateTime.Now;
                        _loanCreditDisbursementAutoTab.Update(loanDisubermentInfo);
                        _ = UpdateStatusLoanCreditToAccountantDisbursementAsync(loanDisubermentInfo.LoanCreditID, openLock);
                        return;
                    case 1: // tạo dc otp: đã cập nhật db
                        keyValueDisbursementInfo.LoanCreditDisbursementAutoIDProcessing = loanDisubermentInfo.LoanCreditDisbursementAutoID;
                        return;
                    case -1: // thử tạo lại otp, giữ nguyên id cần xử lý
                        keyValueDisbursementInfo.RetryCreateOtp = 1;
                        return;
                }
            }
        }

        private Domain.Tables.TblLoanCreditDisbursementAuto GetLoanCreditWaitingDisbursement()
        {
            try
            {
                // xử lý tuần tự cho đến khi xong otp hoặc lỗi
                TblLoanCreditDisbursementAuto loanDisubermentInfo = _loanCreditDisbursementAutoTab.WhereClause(x => x.Status == (int)LoanCreditDisbursementAuto_Status.Waiting).Query().OrderBy(x => x.LoanCreditDisbursementAutoID).FirstOrDefault();
                if (loanDisubermentInfo != null && loanDisubermentInfo.LoanCreditDisbursementAutoID > 0)
                {
                    var lenderInfo = _lenderTab.WhereClause(x => x.LenderID == loanDisubermentInfo.LenderID).Query().FirstOrDefault();
                    // kiểm tra tiền lender trước giải ngân
                    // todo:
                    //var flagCheckMoney = (long)((lenderInfo?.TotalMoneyInSafe ?? 0) - loanDisubermentInfo.TotalMoneyDisbursement);
                    long flagCheckMoney = 1;
                    if (flagCheckMoney < 0)
                    {
                        string mess = $"TIMA TB: Dai Ly {lenderInfo.FullName}  khong co du tien de giai ngan HD-{loanDisubermentInfo.LoanCreditID}";
                        //_smsTab.Insert(new Entity.TblSMS
                        //{
                        //    ShopId = 3703,
                        //    CustomerId = 12, // store local đang set mặc định
                        //    TypeId = 87,
                        //    NumberPhone = "0964000111",
                        //    HomeNetworkId = 1,
                        //    Content = mess,
                        //    CreateOn = DateTime.Now,
                        //    Status = 0,
                        //    NumberError = 0,
                        //    CustomerName = "Tima"
                        //});
                        loanDisubermentInfo.Status = (int)LoanCreditDisbursementAuto_Status.Fail;
                        _ = _losManager.SaveCommentHistory(new Entites.Dtos.LOSServices.ReqSaveCommentHistory
                        {
                            LoanBriefId = loanDisubermentInfo.LoanCreditID,
                            Note = mess,
                            UserId = TimaSettingConstant.UserIDAdmin,
                            UserName = TimaSettingConstant.UserNameAutoDisbursement
                        });
                        //_slackClient.SendNotifyToAccountGroup(mess, title: $"{_PrefixAuto} Đơn vay cần giải ngân");
                    }
                }
                return loanDisubermentInfo;
            }
            catch (Exception ex)
            {
                //_slackClient.SendNotifyForDev($"{_PrefixAuto} GetLoanCreditWaitingDisbursement_Exception|ex={ex.Message}-{ex.StackTrace}");
                _logger.LogError($"{_PrefixAuto} GetLoanCreditWaitingDisbursement|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }

        private async Task HandleVerifyOtp(SettingKeyValueAutoDisbursement keyValueDisbursementInfo)
        {
            int countWaitingOtp = 0;
            // trạng thái chờ otp
            TblLoanCreditDisbursementAuto loanDisubermentInfo = _loanCreditDisbursementAutoTab.WhereClause(x => x.LoanCreditDisbursementAutoID == keyValueDisbursementInfo.LoanCreditDisbursementAutoIDProcessing).Query().FirstOrDefault();

            var smsOTP = await AnalyticsSMSOTP();
            // chưa có sms mới về
            if (smsOTP == null)
            {
                countWaitingOtp++;
                keyValueDisbursementInfo.RetryFindOtp++;
                if (keyValueDisbursementInfo.RetryFindOtp < MaxRetryWaitOTP)
                {
                    return;
                }
            }
            else
            {
                switch ((SmsDisbursement_Status)smsOTP.Status)
                {
                    case SmsDisbursement_Status.Fail:
                        loanDisubermentInfo.Status = (int)LoanCreditDisbursementAuto_Status.Fail;
                        break;
                    case SmsDisbursement_Status.FailWaitingRecheck:
                        loanDisubermentInfo.Status = (int)LoanCreditDisbursementAuto_Status.Fail;
                        LockLoanCredit(loanDisubermentInfo.LoanCreditID, loanDisubermentInfo.FullName);
                        break;
                    case SmsDisbursement_Status.Success:
                        loanDisubermentInfo.Status = (int)LoanCreditDisbursementAuto_Status.Success;
                        var moneyFeeDisbursement = GetMoneyFeeDisbursement(loanDisubermentInfo.LoanCreditID, loanDisubermentInfo.JsonExtra);
                        LoanCreditDisbursementSuccess(loanDisubermentInfo.LoanCreditID, loanDisubermentInfo.FullName, loanDisubermentInfo.MoneyDisbursement, moneyFeeDisbursement, loanDisubermentInfo.MoneyFeeInsuranceOfCustomer, loanDisubermentInfo.MoneyFeeInsuranceMaterialCovered);
                        keyValueDisbursementInfo.LastTimeSuccess = DateTime.Now;
                        break;
                    case SmsDisbursement_Status.Spam:
                    case SmsDisbursement_Status.LoanNotFound:
                        // đọc tin sms tiếp theo
                        await HandleVerifyOtp(keyValueDisbursementInfo);
                        return;
                    case SmsDisbursement_Status.Retry:
                    case SmsDisbursement_Status.TimeOut:
                        loanDisubermentInfo.CountRetry++;
                        // case phía ngân hàng hay lỗi vào buổi tối
                        // lỗi verify otp ngay sau lần thành công trước đó dưới 10s, chờ 3.5p ròi gửi lại otp
                        if (DateTime.Now.Subtract(_timeLatestSucces).Duration().TotalSeconds < 10)
                        {
                            System.Threading.Thread.Sleep(TimeSpan.FromMilliseconds(3500));
                        }
                        int actionResult = await SendRequestGetOTP(loanDisubermentInfo, keyValueDisbursementInfo, true);
                        switch (actionResult)
                        {
                            case 0: // lỗi
                                loanDisubermentInfo.Status = (int)LoanCreditDisbursementAuto_Status.Fail;
                                return;
                            case 1: // tạo dc otp: đã cập nhật db
                                return;
                            case -1: // thử tạo lại otp, giữ nguyên id cần xử lý
                                keyValueDisbursementInfo.RetryCreateOtp += 1;
                                return;
                        }
                        return;
                }
            }
            if (loanDisubermentInfo.Status == (int)LoanCreditDisbursementAuto_Status.Success || loanDisubermentInfo.Status == (int)LoanCreditDisbursementAuto_Status.Fail)
            {
                keyValueDisbursementInfo.LoanCreditDisbursementAutoIDProcessing = 0;
                keyValueDisbursementInfo.RetryCreateOtp = 0;
                keyValueDisbursementInfo.RetryFindOtp = 0;
                loanDisubermentInfo.ModifyDate = DateTime.Now;
                _loanCreditDisbursementAutoTab.Update(loanDisubermentInfo);

                // trả đơn về cho kế toán
                if (loanDisubermentInfo.Status == (int)LoanCreditDisbursementAuto_Status.Fail)
                {
                    _ = UpdateStatusLoanCreditToAccountantDisbursementAsync(loanDisubermentInfo.LoanCreditID);
                }
            }

        }
        private void LoanCreditDisbursementSuccess(long loanCreditID, string fullName, long amountCustomerReceived, long moneyFeeDisbursement, long moneyfeeInsuranceOfCustomer, long moneyfeeInsuranceMaterialCovered)
        {
            try
            {
                var loanDetailLos = _losManager.GetLoanCreditDetail(loanCreditID).Result;
                var actionCreatLoan = _mediator.Send(new CreateLoanCommand
                {
                    BankCardID = BankCardID_VIB2,
                    CreateBy = TimaSettingConstant.UserIDAdmin,
                    LoanAgID = 0,
                    LoanCreditID = (int)loanCreditID,
                    MoneyfeeInsuranceOfCustomer = moneyfeeInsuranceOfCustomer,
                    SourceBankDisbursement = (int)Loan_SourceMoneyDisbursement.EscrowSource,
                    ShopLenderID = (int)loanDetailLos.LenderId
                }).Result;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{_PrefixAuto} LoanCreditDisbursementSuccess_Error|loanCreditID={loanCreditID}|ex={ex.Message}-{ex.StackTrace}");
            }

        }
        private void LockLoanCredit(long loanCreditID, string fullName)
        {
            try
            {
                // lock đơn trước khi giải ngân.
                // thông báo kế toán vào kiểm tra
                //_slackClient.SendNotifyToAccountGroup($"{_PrefixAuto} Hợp đồng HD-{loanCreditID} không xác định được giao dịch thành công hay thất bại. KT vào kiểm tra.");
            }
            catch (Exception ex)
            {
                _logger.LogError($"{_PrefixAuto} LockLoanCredit_Exception|LoanCreditID={loanCreditID}|ex={ex.Message}-{ex.StackTrace}");
            }
        }
        private async Task ChangeStatusToManually(DateTime? endTime = null)
        {
            try
            {
                var currentDate = DateTime.Now;
                if (endTime == null)
                {
                    endTime = currentDate;
                }
                // kiểm tra đã chạy hết hàng đợi
                var lstData = _loanCreditDisbursementAutoTab.WhereClause(x => x.Status == (int)LoanCreditDisbursementAuto_Status.Waiting && x.CreateDate < endTime).Query().ToList();
                if (lstData != null && lstData.Count > 0)
                {
                    var datecurrent = DateTime.Now;
                    _logger.LogError($"{_PrefixAuto} Số đơn còn chờ xử lý = {lstData.Count} chuyển về kế toán|endtime={endTime:dd-MM-yyyy HH:mm:ss}|now={currentDate:dd-MM-yyyy HH:mm:ss}");
                    foreach (var item in lstData)
                    {
                        await UpdateStatusLoanCreditToAccountantDisbursementAsync(item.LoanCreditID);
                        item.Status = (int)LoanCreditDisbursementAuto_Status.Fail;
                        item.ModifyDate = datecurrent;
                    }
                    _loanCreditDisbursementAutoTab.UpdateBulk(lstData);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{_PrefixAuto} ChangeStatusToManually_Error|ex={ex.Message}-{ex.StackTrace}");
            }
        }
        private async Task<bool> UpdateStatusLoanCreditToAccountantDisbursementAsync(long loanCreditID, bool openLock = false, bool isCheckResultCall = true)
        {
            try
            {
                string mess = "";
                var loanDetail = await _losManager.GetLoanCreditDetail(loanCreditID);
                var statusAg = _losManager.ConvertStatusLoanDisbursementFromLOSToAG(loanDetail.Status);
                if (statusAg == (int)StateLoanCreditNew.AutoDisbursement)
                {
                    if (openLock)
                    {
                        mess = $"{_PrefixAuto} Mở khóa.";
                        _ = _losManager.SaveCommentHistory(new Entites.Dtos.LOSServices.ReqSaveCommentHistory
                        {
                            LoanBriefId = loanCreditID,
                            Note = mess,
                            UserId = TimaSettingConstant.UserIDAdmin,
                            UserName = TimaSettingConstant.UserNameAutoDisbursement
                        });
                        _ = _losManager.LockLoan(new Entites.Dtos.LOSServices.LockLoanReq
                        {
                            LoanId = loanCreditID,
                            Type = (int)LOS_Enum_LockLoanType.Open,
                            Note = mess,
                            UserName = TimaSettingConstant.UserNameAutoDisbursement
                        });
                    }
                    _ = _losManager.SaveCommentHistory(new Entites.Dtos.LOSServices.ReqSaveCommentHistory
                    {
                        LoanBriefId = loanCreditID,
                        Note = $"{_PrefixAuto} Chuyển lại kế toán tự giải ngân.",
                        UserId = TimaSettingConstant.UserIDAdmin,
                        UserName = TimaSettingConstant.UserNameAutoDisbursement
                    });
                    //_losManager.ChangeDisbursementByAccountant(loanCreditID, $"{_PrefixAuto} Chuyển lại kế toán tự giải ngân.", _userName);
                    var actionChangeStatus = await _losManager.ChangeDisbursementByAccountant(new Entites.Dtos.LOSServices.ChangeDisbursementByAccountantReq
                    {
                        DisbursementBy = (int)LOS_Enum_DisbursementBy.Accountant,
                        LoanbriefId = loanCreditID,
                        Note = $"{_PrefixAuto} Chuyển lại kế toán tự giải ngân.",
                        UserName = TimaSettingConstant.UserNameAutoDisbursement
                    });
                    if (actionChangeStatus != null && Convert.ToBoolean(actionChangeStatus.Data) == true)
                    {
                        mess = $"{_PrefixAuto} Cần kế toán giải ngân HD-{loanCreditID}- Số tiền: {loanDetail?.LoanAmountFinal:#,##0} - {loanDetail?.LoanTime} Tháng.";
                        //_slackClient.SendNotifyToAccountGroup(mess, title: $"{_PrefixAuto} Đơn vay cần giải ngân.");
                        return true;
                    }
                    else if (isCheckResultCall)
                    {
                        _logger.LogError($"{_PrefixAuto} Chuyển trạng thái về kế toán tự giải ngân lỗi, LoanCreditID: {loanCreditID}");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{_PrefixAuto} Chuyển trạng thái về kế toán tự giải ngân lỗi, LoanCreditID: {loanCreditID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return false;
        }

        private void RemoveLoanWaitingInvalid()
        {
            try
            {
                var loanDisubermentInfos = _loanCreditDisbursementAutoTab.WhereClause(x => x.Status == (int)LoanCreditDisbursementAuto_Status.WaitingOTP).Query().ToList();
                if (loanDisubermentInfos != null && loanDisubermentInfos.Count > 0)
                {
                    var datecurrent = DateTime.Now;
                    foreach (var item in loanDisubermentInfos)
                    {
                        item.Status = (int)LoanCreditDisbursementAuto_Status.Fail;
                        item.ModifyDate = datecurrent;
                        // thử chuyển trạng thái cho bên los
                        // bước này không quan tâm kết quả trả về
                        _ = UpdateStatusLoanCreditToAccountantDisbursementAsync(item.LoanCreditID, isCheckResultCall: false);
                    }
                    _loanCreditDisbursementAutoTab.UpdateBulk(loanDisubermentInfos);
                    //_slackClient.SendNotifyForDev($"{_PrefixAuto} Applycation Start|key={Entity.SettingKeyConstant.LoanCreditDisbursementAuto_TurnOn}|Số đơn chờ OTP = {loanDisubermentInfos.Count}|lstID={string.Join(",", loanDisubermentInfos.Select(x => x.LoanCreditID))}");
                    _logger.LogError($"{_PrefixAuto} RemoveLoanWaitingInvalid|Số đơn chờ OTP = {loanDisubermentInfos.Count}|lstID={string.Join(",", loanDisubermentInfos.Select(x => x.LoanCreditID))}");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{_PrefixAuto} RemoveLoanWaitingInvalid|ex={ex.Message}-{ex.StackTrace}");
            }
        }

        /// <summary>
        /// 0: lỗi, 1: thành công, -1: retry
        /// </summary>
        /// <param name="loanDisubermentInfo"></param>
        /// <param name="retry"></param>
        /// <returns></returns>
        private async Task<int> SendRequestGetOTP(TblLoanCreditDisbursementAuto loanDisubermentInfo, SettingKeyValueAutoDisbursement keyValueDisbursementInfo, bool retry = false, int numberRerty = 0)
        {
            try
            {
                var loanCreditLosInfo = await _losManager.GetLoanCreditDetail(loanDisubermentInfo.LoanCreditID);
                if (loanCreditLosInfo != null)
                {
                    var statusAg = _losManager.ConvertStatusLoanDisbursementFromLOSToAG(loanCreditLosInfo.Status);
                    if (statusAg == (int)StateLoanCreditNew.AutoDisbursement)
                    {
                        string mess = "";
                        // check back list                        
                        var blackListInfos = _blackListTab.SetGetTop(1)
                                                          .WhereClause(x => x.Status == 1 && x.ExpirationDate > DateTime.Now && x.FullName == loanCreditLosInfo.FullName)
                                                          .WhereClause(x => (x.CardNumber == loanCreditLosInfo.NationalCard || x.NumberPhone == loanCreditLosInfo.Phone)).Query();
                        if (blackListInfos != null && blackListInfos.Any())
                        {
                            loanDisubermentInfo.Status = (int)LoanCreditDisbursementAuto_Status.Fail;
                            mess = $"{_PrefixAuto} Khách hàng này không hỗ trợ vì nằm trong blacklist.";
                            _ = _losManager.SaveCommentHistory(new Entites.Dtos.LOSServices.ReqSaveCommentHistory
                            {
                                LoanBriefId = loanDisubermentInfo.LoanCreditID,
                                Note = mess,
                                UserId = TimaSettingConstant.UserIDAdmin,
                                UserName = TimaSettingConstant.UserNameAutoDisbursement
                            });
                            _logger.LogWarning($"{_PrefixAuto} SendRequestGetOTP_BackList|LoanCreditID={loanDisubermentInfo.LoanCreditID}|LoanCreditDisbursementAutoID={loanDisubermentInfo.LoanCreditDisbursementAutoID}|loanCreditLosInfos={_common.ConvertObjectToJSonV2(loanCreditLosInfo)}.");

                            //_slackClient.SendNotifyToAccountGroup($"{mess} HĐ-{loanDisubermentInfo.LoanCreditID}.", color: "red");
                            return 0;
                        }
                        // không phải đơn top up, kiểm tra đơn vay khách hàng đã có chưa
                        if (loanCreditLosInfo.TopUpOfLoanBriefID < 1) // kiểm tra điều kiện của đơn Topup
                        {
                            // store: spt_CheckCustomerBorrowingByNumberPhoneOrCardNumber
                            var lstLoanExist = _loanTab.SetGetTop(1).JoinOn<TblCustomer>(l => l.CustomerID, c => c.CustomerID)
                                                       .SelectColumns(x => x.LoanID)
                                                       .WhereClauseJoinOn<TblCustomer>(c => c.NumberCard == loanCreditLosInfo.NationalCard || c.Phone == loanCreditLosInfo.Phone)
                                                       .WhereClause(l => l.Status == (int)Loan_Status.Lending)
                                                       .WhereClause(l => !_lstBigProductID.Contains((long)l.ProductID)) // không lấy sp lớn
                                                       .Query();
                            if (lstLoanExist != null && lstLoanExist.Any())
                            {
                                loanDisubermentInfo.Status = (int)LoanCreditDisbursementAuto_Status.Fail;
                                mess = $"{_PrefixAuto} Khách hàng {loanCreditLosInfo.FullName}: HD-{loanDisubermentInfo.LoanCreditID} chuẩn bị GN đã có đơn đang vay mã hợp đồng TC-{lstLoanExist.First().LoanID}.";
                                _ = _losManager.SaveCommentHistory(new Entites.Dtos.LOSServices.ReqSaveCommentHistory
                                {
                                    LoanBriefId = loanDisubermentInfo.LoanCreditID,
                                    Note = mess,
                                    UserId = TimaSettingConstant.UserIDAdmin,
                                    UserName = TimaSettingConstant.UserNameAutoDisbursement
                                });
                                _logger.LogError($"{_PrefixAuto} CheckCustomerBorrowed|LoanCreditID={loanDisubermentInfo.LoanCreditID}|loanCreditLosInfos={_common.ConvertObjectToJSonV2(loanCreditLosInfo)}.");
                                //_slackClient.SendNotifyToAccountGroup($"{mess}", color: "red");
                                return 0;
                            }
                        }
                        if (loanCreditLosInfo.IsLock == true && !retry)
                        {
                            _ = _losManager.SaveCommentHistory(new Entites.Dtos.LOSServices.ReqSaveCommentHistory
                            {
                                LoanBriefId = loanDisubermentInfo.LoanCreditID,
                                Note = $"{_PrefixAuto} Đơn bị khóa trước khi giải ngân.",
                                UserId = TimaSettingConstant.UserIDAdmin,
                                UserName = TimaSettingConstant.UserNameAutoDisbursement
                            });
                            loanDisubermentInfo.Status = (int)LoanCreditDisbursementAuto_Status.Fail;
                            _logger.LogError($"{_PrefixAuto} SendRequestGetOTP_IsLock|LoanCreditID={loanDisubermentInfo.LoanCreditID}|LoanCreditDisbursementAutoID={loanDisubermentInfo.LoanCreditDisbursementAutoID}|loanCreditLosInfos={_common.ConvertObjectToJSonV2(loanCreditLosInfo)}.");
                            return 0;
                        }
                        bool resultLockLoan = retry;
                        if (!retry)
                        {
                            mess = $"{_PrefixAuto} Đơn khóa do chuẩn bị giải ngân.";
                            _ = _losManager.SaveCommentHistory(new Entites.Dtos.LOSServices.ReqSaveCommentHistory
                            {
                                LoanBriefId = loanDisubermentInfo.LoanCreditID,
                                Note = mess,
                                UserId = TimaSettingConstant.UserIDAdmin,
                                UserName = TimaSettingConstant.UserNameAutoDisbursement
                            });
                            var resultLockLoanRespone = _losManager.LockLoan(new Entites.Dtos.LOSServices.LockLoanReq
                            {
                                LoanId = loanDisubermentInfo.LoanCreditID,
                                Note = mess,
                                Type = (int)LOS_Enum_LockLoanType.Lock,
                                UserName = TimaSettingConstant.UserNameAutoDisbursement
                            }).Result;
                            if (resultLockLoanRespone != null && resultLockLoanRespone.Result == (int)ResponseAction.Success)
                            {
                                resultLockLoan = true;
                            }
                        }
                        if (resultLockLoan)
                        {
                            var bankInfo = _bankTab.WhereClause(x => x.Id == loanCreditLosInfo.BankId).Query().FirstOrDefault();
                            var objCreateTransaction = new LMS.Entites.Dtos.APIAutoDisbursement.CreateTransactionRequest
                            {
                                BankAccountNumber = loanDisubermentInfo.AccountNumberBanking,
                                BankValue = $"{bankInfo?.ValueOfBank}",
                                Name = _common.UnicodeToPlain(loanDisubermentInfo.FullName.ToUpper()), // common.UnicodeToPlain(objCustomerCredit.FullName.ToUpper());
                                Amount = (long)loanDisubermentInfo.MoneyDisbursement, //objLoanCredit.TotalMoney;
                                Mess = _common.UnicodeToPlain($"TIMA CT CHO {loanDisubermentInfo.FullName.ToUpper()} THEO TT"),
                                TraceIndentifier = _common.GenTraceIndentifier(),
                                LoanCreditID = loanDisubermentInfo.LoanCreditID
                            };
                            var objResult = await _paymentGatewayService.VibCreateTransaction(objCreateTransaction, (int)loanDisubermentInfo.LoanCreditID, retry: numberRerty);
                            #region nghiệp vụ cũ, tối ưu lại
                            //if (objResult == null)
                            //{
                            //    //loanDisubermentInfo.Status = (int)LoanCreditDisbursementAuto_Status.Fail;
                            //    return 0;
                            //}
                            //else
                            //{
                            //    if (objResult.code == (int)VIB_AI_ResponseCodeConstant.SUCCESS_CODE)
                            //    {
                            //        if (!_common.UnicodeToPlain(objResult.Data.Bankacc.ToUpper()).Contains(_common.UnicodeToPlain(loanDisubermentInfo.FullName.Trim().ToUpper())))
                            //        {
                            //            _logger.LogError($"{_PrefixAuto} SendRequestGetOTP_CancelTransaction|LoanCreditID={loanDisubermentInfo.LoanCreditID}|LoanCreditDisbursementAutoID={loanDisubermentInfo.LoanCreditDisbursementAutoID}|objCreateTransaction={_common.ConvertObjectToJSonV2(objCreateTransaction)}|objResult={_common.ConvertObjectToJSonV2(objResult)}");
                            //            // gửi notify cho dev check lỗi qua slack
                            //            //_paymentGatewayService.VibCancelTransaction(objResult.data.transid_request, (int)loanDisubermentInfo.LoanCreditID);
                            //            _paymentGatewayService.VibCancelTransaction(new CancelTransactionRequest
                            //            {
                            //                LoanCreditID = (int)loanDisubermentInfo.LoanCreditID,
                            //                TransID = objResult.Data.TransIDRequest,
                            //                TraceIndentifier = _common.GenTraceIndentifier()
                            //            });
                            //            //loanDisubermentInfo.Status = (int)LoanCreditDisbursementAuto_Status.Fail;
                            //            return 0;
                            //        }
                            //        else
                            //        {
                            //            loanDisubermentInfo.ModifyDate = DateTime.Now;
                            //            loanDisubermentInfo.JsonExtra = _common.ConvertObjectToJSonV2(objResult.Data);
                            //            loanDisubermentInfo.Status = (int)LoanCreditDisbursementAuto_Status.WaitingOTP;
                            //            _loanCreditDisbursementAutoTab.Update(loanDisubermentInfo);
                            //            return 1;
                            //        }
                            //    }
                            //    else if (objResult.IsRetry) // call lại lần nữa
                            //    {
                            //        // vẫn ở tạng thái chờ xử lý và call lại api tạo giao dịch
                            //        return -1;
                            //    }
                            //    else
                            //    {
                            //        // gửi notify cho dev check lỗi qua slack
                            //        //loanDisubermentInfo.Status = (int)LoanCreditDisbursementAuto_Status.Fail;
                            //        //_slackClient.SendNotifyForDev($"{_PrefixAuto} SendRequestGetOTP_CreateRequest|LoanCreditID={loanDisubermentInfo.LoanCreditID}|LoanCreditDisbursementAutoID={loanDisubermentInfo.LoanCreditDisbursementAutoID}|objCreateTransaction={_common.ConvertObjectToJSonV2(objCreateTransaction)}|objResult={_common.ConvertObjectToJSonV2(objResult)}.");
                            //        if (!string.IsNullOrEmpty(objCreateTransaction.MessageResponse))
                            //        {
                            //            //_slackClient.SendNotifyToAccountGroup($"HD-{loanDisubermentInfo.LoanCreditID} - AccountNumberBanking: {loanDisubermentInfo.AccountNumberBanking} - {objCreateTransaction.Mess} - Lỗi: {objCreateTransaction.MessageResponse}");
                            //        }
                            //        // nếu sau khi quá retry -> push cho kế toán tạm dừng giải ngân tự động chờ bank ổn định rồi chạy
                            //        if (objCreateTransaction.Retry == Services.PaymentGatewayService.MaxRetry)
                            //        {
                            //            SetKeyPushToAccountant(keyValueDisbursementInfo);
                            //            //_slackClient.SendNotifyToAccountGroup("Hệ thống GN tự động tạm dừng do ngân hàng không ổn định. Mời team vào kiểm tra rồi bật lại GNTĐ");
                            //            _logger.LogError("Hệ thống GN tự động tạm dừng do ngân hàng không ổn định. Mời team vào kiểm tra rồi bật lại GNTĐ");
                            //        }
                            //        return 0;
                            //    }
                            //}

                            #endregion
                        }
                        else
                        {
                            // không khóa dc đơn
                            //loanDisubermentInfo.Status = (int)LoanCreditDisbursementAuto_Status.Fail;
                            _ = _losManager.SaveCommentHistory(new Entites.Dtos.LOSServices.ReqSaveCommentHistory
                            {
                                LoanBriefId = loanDisubermentInfo.LoanCreditID,
                                Note = $"{_PrefixAuto} Khóa đơn không thành công trước khi giải ngân.",
                                UserId = TimaSettingConstant.UserIDAdmin,
                                UserName = TimaSettingConstant.UserNameAutoDisbursement
                            });
                            return 0;
                        }

                    }
                    else
                    {
                        // không phải đơn tự động trả về cho kế toán
                        //loanDisubermentInfo.Status = (int)LoanCreditDisbursementAuto_Status.Fail;
                        _logger.LogWarning($"{_PrefixAuto} SendRequestGetOTP_StatusAG|LoanCreditID={loanDisubermentInfo.LoanCreditID}|LoanCreditDisbursementAutoID={loanDisubermentInfo.LoanCreditDisbursementAutoID}|loanCreditLosInfos={_common.ConvertObjectToJSonV2(loanCreditLosInfo)}.");
                        return 0;
                    }
                }
                else
                {
                    // không tìm thấy đơn
                    //loanDisubermentInfo.Status = (int)LoanCreditDisbursementAuto_Status.Fail;
                    _logger.LogWarning($"{_PrefixAuto} SendRequestGetOTP_NotFound_Detail|LoanCreditID={loanDisubermentInfo.LoanCreditID}|LoanCreditDisbursementAutoID={loanDisubermentInfo.LoanCreditDisbursementAutoID}|loanCreditLosInfos={_common.ConvertObjectToJSonV2(loanCreditLosInfo)}.");
                    return 0;
                }
            }
            catch (Exception ex)
            {
                //loanDisubermentInfo.Status = (int)LoanCreditDisbursementAuto_Status.Fail;
                _logger.LogError($"{_PrefixAuto} SendRequestGetOTP_Exception|LoanCreditID={loanDisubermentInfo.LoanCreditID}|LoanCreditDisbursementAutoID={loanDisubermentInfo.LoanCreditDisbursementAutoID}|ex={ex.Message}-{ex.StackTrace}.");

            }
            return 0;
        }

        private void SetKeyPushToAccountant(SettingKeyValueAutoDisbursement keyValueDisbursementInfo)
        {
            try
            {
                var settingKeyInfo = _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.Loan_ProcessAutoDisbursement).Query().FirstOrDefault();
                settingKeyInfo.Status = (int)SettingKey_Status.PushToAccountant;
                settingKeyInfo.ModifyDate = DateTime.Now;
                _settingKeyTab.Update(settingKeyInfo);

                keyValueDisbursementInfo.LoanCreditDisbursementAutoIDProcessing = 0;
                keyValueDisbursementInfo.RetryFindOtp = 0;
                keyValueDisbursementInfo.RetryCreateOtp = 0;
                keyValueDisbursementInfo.CountPushNotiAccountant = 1; // đã thông báo cho team kt
            }
            catch (Exception ex)
            {
                _logger.LogError($"{_PrefixAuto} SetKeyPushToAccountant_Exception|ex={ex.Message}-{ex.StackTrace}");
            }
        }

        private async Task<TblSmsDisbursement> AnalyticsSMSOTP()
        {
            try
            {
                var smsDisbursementInfos = _smsDisbursementTab.WhereClause(x => x.Status == (int)SmsDisbursement_Status.Analyzed).Query();
                if (smsDisbursementInfos != null && smsDisbursementInfos.Any())
                {
                    var smsDisbursementInfo = smsDisbursementInfos.FirstOrDefault();
                    try
                    {
                        var loanDisubermentInfos = _loanCreditDisbursementAutoTab.WhereClause(x => x.Status == (int)LoanCreditDisbursementAuto_Status.WaitingOTP
                                                                                                && x.AccountNumberBanking == smsDisbursementInfo.AccountNumberBanking
                                                                                                && x.MoneyDisbursement == smsDisbursementInfo.Amount
                                                                                             ).Query();

                        if (loanDisubermentInfos != null && loanDisubermentInfos.Any())
                        {
                            var loanDisubermentInfo = loanDisubermentInfos.FirstOrDefault();
                            long moneyFeeDisbursement = GetMoneyFeeDisbursement(loanDisubermentInfo.LoanCreditID, loanDisubermentInfo.JsonExtra);
                            smsDisbursementInfo.LoanCreditID = loanDisubermentInfo.LoanCreditID;
                            if (moneyFeeDisbursement != -1)
                            {
                                if (CheckLoanCreditExist(loanDisubermentInfo.LoanCreditID))
                                {
                                    smsDisbursementInfo.Status = (int)SmsDisbursement_Status.Fail;
                                }
                                else
                                {
                                    var objTransaction = _common.ConvertJSonToObjectV2<Entites.Dtos.APIAutoDisbursement.Transaction>(loanDisubermentInfo.JsonExtra);
                                    var objExecuteOtp = new Entites.Dtos.APIAutoDisbursement.ExecuteOtpRequest
                                    {
                                        TransIDRequest = objTransaction.TransIDRequest ?? objTransaction.TransID,
                                        TransIDVerify = objTransaction.TransIDVerify,
                                        Otp = smsDisbursementInfo.OTP,
                                        LoanCreditID = loanDisubermentInfo.LoanCreditID
                                    };
                                    var objResult = await _paymentGatewayService.VibExecuteOtp(objExecuteOtp);
                                    if (objResult == null)
                                    {
                                        smsDisbursementInfo.Status = (int)SmsDisbursement_Status.Retry;
                                        _logger.LogError($"{_PrefixAuto} VerifyOtp response null|LoanCreditID={loanDisubermentInfo.LoanCreditID}|objTransaction={loanDisubermentInfo.JsonExtra}");
                                    }
                                    else
                                    {
                                        switch ((VIB_AI_ResponseCodeConstant)objResult.Code)
                                        {
                                            case VIB_AI_ResponseCodeConstant.SUCCESS_CODE:
                                                smsDisbursementInfo.Status = (int)SmsDisbursement_Status.Success;
                                                _timeLatestSucces = DateTime.Now;
                                                break;
                                            case VIB_AI_ResponseCodeConstant.FAIL_TRANSACTION_NOT_EXITS:
                                            case VIB_AI_ResponseCodeConstant.FAIL_TRANSACTION_TIME_OUT:
                                            case VIB_AI_ResponseCodeConstant.FAIL_OTP_NOT_VALID:
                                                smsDisbursementInfo.Status = (int)SmsDisbursement_Status.TimeOut;
                                                break;
                                            case VIB_AI_ResponseCodeConstant.FAIL_BANK_IS_PROCESSING:
                                            case VIB_AI_ResponseCodeConstant.FAIL_BANK_NOT_ENOUGH:
                                                _logger.LogError($"{_PrefixAuto} VerifyOtp response|LoanCreditID={loanDisubermentInfo.LoanCreditID}|objTransaction={loanDisubermentInfo.JsonExtra}|response Code={objResult.Code}|response message={objResult.Messages}");
                                                smsDisbursementInfo.Status = (int)SmsDisbursement_Status.FailWaitingRecheck;
                                                _ = _losManager.SaveCommentHistory(new Entites.Dtos.LOSServices.ReqSaveCommentHistory
                                                {
                                                    LoanBriefId = loanDisubermentInfo.LoanCreditID,
                                                    Note = $"{_PrefixAuto} Vui lòng kiểm lại giao dịch qua ngân hàng. Lý do: {objResult.Messages}",
                                                    UserId = TimaSettingConstant.UserIDAdmin,
                                                    UserName = TimaSettingConstant.UserNameAutoDisbursement
                                                });
                                                break;
                                            case VIB_AI_ResponseCodeConstant.FAIL_EXECUTE:
                                                smsDisbursementInfo.Status = (int)SmsDisbursement_Status.Retry;
                                                break;
                                            default:
                                                // gửi notify cho dev check lỗi qua slack
                                                _logger.LogError($"{_PrefixAuto} VibExecuteOtp|LoanCreditID={loanDisubermentInfo.LoanCreditID}|LoanCreditDisbursementAutoID={loanDisubermentInfo.LoanCreditDisbursementAutoID}|objResult={_common.ConvertObjectToJSonV2(objResult)}.");
                                                break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                smsDisbursementInfo.Status = (int)SmsDisbursement_Status.Fail;
                            }
                        }
                        else
                        {
                            smsDisbursementInfo.Status = (int)SmsDisbursement_Status.LoanNotFound;
                        }
                    }
                    catch (Exception ex)
                    {
                        smsDisbursementInfo.Status = (int)SmsDisbursement_Status.Fail;
                        _logger.LogError($"{_PrefixAuto} AnalyticsSMSOTP|smsID={smsDisbursementInfo.SmsDisbursementID}|content={smsDisbursementInfo.SmsContent}|ex={ex.Message}-{ex.StackTrace}");
                    }
                    smsDisbursementInfo.ModifyDate = DateTime.Now;
                    _smsDisbursementTab.Update(smsDisbursementInfo);
                    return smsDisbursementInfo;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{_PrefixAuto} AnalyticsSMSOTP|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }

        private long GetMoneyFeeDisbursement(long loanCreditID, string jsonExtraTransaction)
        {
            long moneyFeeDisbursement = 0;
            try
            {
                var objTransaction = _common.ConvertJSonToObjectV2<Entites.Dtos.APIAutoDisbursement.Transaction>(jsonExtraTransaction);
                var lstmoneyFee = objTransaction.Fee.Split(' ');
                var moneyFee = lstmoneyFee[0].Trim().Replace(",", "");
                moneyFeeDisbursement = Convert.ToInt64(moneyFee);
            }
            catch (Exception ex)
            {
                moneyFeeDisbursement = -1;
                _logger.LogError($"{_PrefixAuto} GetMoneyFeeDisbursement|LoanCreditID: {loanCreditID} không lấy được phí chuyển tiền|objTransaction={jsonExtraTransaction}|ex={ex.Message}");
            }
            return moneyFeeDisbursement;
        }
        private bool CheckLoanCreditExist(long loanCreditID)
        {
            try
            {
                string idOfPartener = $"{loanCreditID}";
                var loanCreditInfos = _loanTab.SelectColumns(x => x.LoanID, x => x.LoanCreditIDOfPartner, x => x.ContactCode)
                                                    .WhereClause(x => x.LoanCreditIDOfPartner == loanCreditID)
                                                    .Query();
                if (loanCreditInfos != null && loanCreditInfos.Any())
                {
                    _logger.LogError($"{_PrefixAuto} CheckLoanCreditExist|loanCreditIDLOS={loanCreditID}|loanID={_common.ConvertObjectToJSonV2(loanCreditInfos.Select(x => new { x.LoanID, x.ContactCode, loanCreditID = x.LoanCreditIDOfPartner }))}");
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{_PrefixAuto} CheckLoanCreditExist|LoanCreditID={loanCreditID}|ex={ex.Message}-{ex.StackTrace}.");
                throw ex;
            }
        }
    }
}
