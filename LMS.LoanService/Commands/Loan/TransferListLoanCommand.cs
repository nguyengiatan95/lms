﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Models.Loan;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Loan
{
    public class TransferListLoanCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public List<TranferListLoanLender> LstLoanLender { get; set; }
    }
    public class TransferListLoanCommandHandler : IRequestHandler<TransferListLoanCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblContractLoanWithLender> _contractLoanWithLenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblKafkaLogEvent> _kakfaLogEventTab;
        ILogger<TransferListLoanCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public TransferListLoanCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblContractLoanWithLender> contractLoanWithLenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblKafkaLogEvent> kakfaLogEventTab,
            ILogger<TransferListLoanCommandHandler> logger)
        {
            _loanTab = loanTab;
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _contractLoanWithLenderTab = contractLoanWithLenderTab;
            _kakfaLogEventTab = kakfaLogEventTab;
        }
        public async Task<ResponseActionResult> Handle(TransferListLoanCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            DateTime dtNow = DateTime.Now;
            List<string> lenderCodes = new List<string>();
            List<string> contractCodes = new List<string>();
            List<Domain.Tables.TblContractLoanWithLender> contractLoanWithLenders = new List<Domain.Tables.TblContractLoanWithLender>();
            List<Domain.Tables.TblContractLoanWithLender> updateLoanWithLenders = new List<Domain.Tables.TblContractLoanWithLender>();

            try
            {
                foreach (var item in request.LstLoanLender)
                {
                    if (!lenderCodes.Contains(item.LenderCode)) lenderCodes.Add(item.LenderCode);
                    if (!lenderCodes.Contains(item.CurrentLenderCode)) lenderCodes.Add(item.CurrentLenderCode);

                    contractCodes.Add(TimaSettingConstant.PrefixContractCode + item.ContractCode);
                }
                // lấy dữ liệu loan + lender;
                var statusLoanLending = (int)Loan_Status.Lending;
                var taskLoan = _loanTab.SelectColumns(x => x.LoanID, x => x.LenderID, x => x.FromDate, x => x.ToDate, x => x.ContactCode)
                    .WhereClause(x => contractCodes.Contains(x.ContactCode) && x.Status == statusLoanLending && x.OwnerShopID == TimaSettingConstant.ShopIDTima).QueryAsync();
                var taskLenders = _lenderTab.SelectColumns(x => x.LenderID, x => x.FullName, x => x.SelfEmployed).WhereClause(x => lenderCodes.Contains(x.FullName)).QueryAsync();
                await Task.WhenAll(taskLoan, taskLenders);
                var loans = taskLoan.Result;
                var lenders = taskLenders.Result;
                var loanIds = loans.Select(x => x.LoanID).ToList();
                // lấy bảng loan hiện tại
                var contractCurrentLoan = await _contractLoanWithLenderTab.WhereClause(x => loanIds.Contains((int)x.LoanID)).QueryAsync();
                var dicContractLoan = new Dictionary<long, Domain.Tables.TblContractLoanWithLender>();

                // xử lý lấy những hd NDT mới nhất
                foreach (var item in contractCurrentLoan)
                {
                    if (!dicContractLoan.ContainsKey(item.LoanID))
                    {
                        dicContractLoan.Add(item.LoanID, item);
                    }
                    else if (item.FromDate > dicContractLoan[item.LoanID].FromDate)
                    {
                        dicContractLoan[item.LoanID] = item;
                    }
                }
                List<Domain.Tables.TblKafkaLogEvent> lstLogEventInsert = new List<Domain.Tables.TblKafkaLogEvent>();
                // map dữ liệu
                foreach (var item in request.LstLoanLender)
                {
                    try
                    {
                        var fromDate = DateTime.Now;
                        if (!DateTime.TryParseExact(item.FromDate, TimaSettingConstant.DateTimeDayMonthYear, null, System.Globalization.DateTimeStyles.None, out fromDate))
                        {
                            continue;
                        }
                        var lenderCurrentId = lenders.Where(x => x.FullName.Trim() == item.CurrentLenderCode.Trim()).First().LenderID;
                        var lenderNewInfo = lenders.Where(x => x.FullName.Trim() == item.LenderCode.Trim()).First();
                        string contractCode = TimaSettingConstant.PrefixContractCode + item.ContractCode;
                        var loan = loans.Where(x => x.ContactCode == contractCode).FirstOrDefault();
                        var loanId = loan.LoanID;
                        if (item.CurrentLenderCode != item.LenderCode && loanId > 0)
                        {
                            if (!dicContractLoan.ContainsKey(loanId))
                            {
                                _logger.LogError($"TransferListLoanCommandHandler|TblContractLoanWithLender_not_found|LoanId={loanId}");
                                continue;
                            }
                            var contractLast = dicContractLoan[loanId];
                            var toDate = contractLast.ToDate;
                            if (toDate < fromDate)
                            {
                                continue;
                            }
                            contractLast.ToDate = fromDate.AddDays(-1);
                            contractLast.ModifyDate = dtNow;

                            var newContract = new Domain.Tables.TblContractLoanWithLender()
                            {
                                LoanID = contractLast.LoanID,
                                //ContractLoanWithLenderID = lenderNewId,
                                FromDate = fromDate,
                                ToDate = toDate,
                                CreateDate = dtNow,
                                LenderID = lenderNewInfo.LenderID,
                                ModifyDate = dtNow,
                                TimaLoanID = contractLast.TimaLoanID,
                                AccountingPriorityOrderID = 1,
                                RateConsultant = loan.RateConsultant,
                                RateInterest = loan.RateInterest,
                                RateService = loan.RateService,
                                SelfEmployed = lenderNewInfo.SelfEmployed,
                                UnitRate = loan.UnitRate
                            };
                            updateLoanWithLenders.Add(contractLast);
                            contractLoanWithLenders.Add(newContract);
                            //// 19-03-2022: xử lý lại
                            //lstLogEventInsert.Add(new Domain.Tables.TblKafkaLogEvent
                            //{
                            //    CreateDate = DateTime.Now,
                            //    ModifyDate = DateTime.Now,
                            //    DataMessage = _common.ConvertObjectToJSonV2(new LMS.Kafka.Messages.Loan.LoanPaymentSuccess
                            //    {
                            //        LoanID = loanId
                            //    }),
                            //    TopicName = Kafka.Constants.KafkaTopics.LoanPaymentSuccess,
                            //    ServiceName = ServiceHostInternal.LoanService,
                            //    Status = (int)KafkaLogEvent_Status.Waitting,
                            //});
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"TransferListLoanCommandHandler_Exception|item={_common.ConvertObjectToJSonV2(item)}|ex={ex.Message}-{ex.StackTrace}");
                    }
                }
                if (updateLoanWithLenders.Any())
                {
                    _contractLoanWithLenderTab.UpdateBulk(updateLoanWithLenders);
                }
                if (contractLoanWithLenders.Any())
                {
                    _contractLoanWithLenderTab.InsertBulk(contractLoanWithLenders);
                }
                if (lstLogEventInsert.Count > 0)
                {
                    _kakfaLogEventTab.InsertBulk(lstLogEventInsert);
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"TransferListLoanCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;

        }
    }
}
