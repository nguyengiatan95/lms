﻿using LMS.LoanServiceApi.Domain.Models.SMS;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Sms
{
    public class IncreaseMoneyFromGuTinaCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public SmsIncreatedMoneyCustomerRequest Model;
    }
    public class IncreaseMoneyFromGuTinaCommandHandler : IRequestHandler<IncreaseMoneyFromGuTinaCommand, LMS.Common.Constants.ResponseActionResult>
    {
        Services.ISmsAnalyticsManager _smsAnalyticsManager;
        public IncreaseMoneyFromGuTinaCommandHandler(Services.ISmsAnalyticsManager smsAnalyticsManager)
        {
            _smsAnalyticsManager = smsAnalyticsManager;
        }

        public async Task<LMS.Common.Constants.ResponseActionResult> Handle(IncreaseMoneyFromGuTinaCommand request, CancellationToken cancellationToken)
        {
            request.Model.Source = (int)Constants.SmsAnaylyticSourceSendRequest.Gutina;
            return await _smsAnalyticsManager.CreateSmsTransactionViaWallet(request.Model);
        }
    }
}
