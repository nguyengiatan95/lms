﻿using LMS.Common.Constants;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Sms
{
    public class UpdateStatusSMSTransactionBankCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public Domain.Models.SMS.UpdateStatusSmsTransactionBankRequest Model;
    }
    public class UpdateStatusSMSTransactionBankCommandHandler : IRequestHandler<UpdateStatusSMSTransactionBankCommand, LMS.Common.Constants.ResponseActionResult>
    {
        Services.ISmsAnalyticsManager _smsAnalyticsManager;
        public UpdateStatusSMSTransactionBankCommandHandler(Services.ISmsAnalyticsManager smsAnalyticsManager)
        {
            _smsAnalyticsManager = smsAnalyticsManager;
        }
        public async Task<ResponseActionResult> Handle(UpdateStatusSMSTransactionBankCommand request, CancellationToken cancellationToken)
        {
            return await _smsAnalyticsManager.UpdateStatusSmsTransactionBank(request.Model);
        }
    }
}
