﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Sms
{
    public class ProcessSmsAnalyticsCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class ProcessSmsAnalyticsCommandHandler : IRequestHandler<ProcessSmsAnalyticsCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> _settingKeyTab;
        Services.ISmsAnalyticsManager _smsAnalyticsManager;
        ILogger<ProcessSmsAnalyticsCommandHandler> _logger;
        public ProcessSmsAnalyticsCommandHandler(Services.ISmsAnalyticsManager smsAnalyticsManager,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> settingKeyTab,
            ILogger<ProcessSmsAnalyticsCommandHandler> logger)
        {
            _settingKeyTab = settingKeyTab;
            _smsAnalyticsManager = smsAnalyticsManager;
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(ProcessSmsAnalyticsCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var keySettingInfo = (await _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.Sms_ProcessCreateInvoiceAuto).QueryAsync()).FirstOrDefault();
                if (keySettingInfo == null || keySettingInfo.Status == 0)
                {
                    response.Message = MessageConstant.SettingKey_Sms_ProcessCreateInvoiceAutoStatus;
                    return response;
                }
                var currentDate = DateTime.Now;
                DateTime startDate = currentDate;
                response = await _smsAnalyticsManager.ProcessSmsAnalytic();
                keySettingInfo.ModifyDate = currentDate;
                _ = await _settingKeyTab.UpdateAsync(keySettingInfo);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSmsAnalyticsCommandHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
