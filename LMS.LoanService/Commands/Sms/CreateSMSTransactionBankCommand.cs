﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Models.SMS;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Sms
{
    public class CreateSMSTransactionBankCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public SmsTransactionBankRequest Model;
    }
    public class CreateSMSTransactionBankCommandHandler : IRequestHandler<CreateSMSTransactionBankCommand, LMS.Common.Constants.ResponseActionResult>
    {
        Services.ISmsAnalyticsManager _smsAnalyticsManager;
        public CreateSMSTransactionBankCommandHandler(Services.ISmsAnalyticsManager smsAnalyticsManager)
        {
            _smsAnalyticsManager = smsAnalyticsManager;
        }
        public async Task<ResponseActionResult> Handle(CreateSMSTransactionBankCommand request, CancellationToken cancellationToken)
        {
            return await _smsAnalyticsManager.CreateSmsTransactionBank(request.Model);
        }
    }
}
