﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Models.SMS;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Sms
{
    public class CreateSMSOTPDisbursementCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public SmsTransactionBankRequest Model;
    }
    public class CreateSMSOTPDisbursementCommandHandler : IRequestHandler<CreateSMSOTPDisbursementCommand, LMS.Common.Constants.ResponseActionResult>
    {
        Services.ISmsAnalyticsManager _smsAnalyticsManager;
        public CreateSMSOTPDisbursementCommandHandler(Services.ISmsAnalyticsManager smsAnalyticsManager)
        {
            _smsAnalyticsManager = smsAnalyticsManager;
        }
        public async Task<ResponseActionResult> Handle(CreateSMSOTPDisbursementCommand request, CancellationToken cancellationToken)
        {
            return await _smsAnalyticsManager.CreateSMSOTPDisbursement(request.Model);
        }
    }
}
