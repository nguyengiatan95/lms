﻿using LMS.LoanServiceApi.Domain.Models.SMS;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Sms
{
    public class IncreaseMoneyFromGPayCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public SmsIncreatedMoneyCustomerRequest Model;
    }
    public class IncreaseMoneyFromGPayCommandHandler : IRequestHandler<IncreaseMoneyFromGPayCommand, LMS.Common.Constants.ResponseActionResult>
    {
        Services.ISmsAnalyticsManager _smsAnalyticsManager;
        public IncreaseMoneyFromGPayCommandHandler(Services.ISmsAnalyticsManager smsAnalyticsManager)
        {
            _smsAnalyticsManager = smsAnalyticsManager;
        }

        [Obsolete]
        public async Task<LMS.Common.Constants.ResponseActionResult> Handle(IncreaseMoneyFromGPayCommand request, CancellationToken cancellationToken)
        {
            request.Model.Source = (int)Constants.SmsAnaylyticSourceSendRequest.Gpay;
            return await _smsAnalyticsManager.CreateSmsTransactionViaWallet(request.Model);
        }
    }
}
