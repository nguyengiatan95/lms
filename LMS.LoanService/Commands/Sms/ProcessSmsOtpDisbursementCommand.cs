﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Sms
{
    public class ProcessSmsOtpDisbursementCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class ProcessSmsOtpDisbursementCommandHandler : IRequestHandler<ProcessSmsOtpDisbursementCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSmsDisbursement> _smsDisbursementTab;
        ILogger<ProcessSmsOtpDisbursementCommandHandler> _logger;
        public ProcessSmsOtpDisbursementCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblSmsDisbursement> smsDisbursementTab,
            ILogger<ProcessSmsOtpDisbursementCommandHandler> logger)
        {
            _smsDisbursementTab = smsDisbursementTab;
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(ProcessSmsOtpDisbursementCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var regexVIB = @"[(\d{1,3},\d{1,3},\d{1,3})]*[(\.\d+)]+";
            try
            {
                int totalSmsOtp = 0;
                var datecurrent = DateTime.Now;
                var smsDisbursementInfos = await _smsDisbursementTab.WhereClause(x => x.Status == (int)SmsDisbursement_Status.Waiting).QueryAsync();
                if (smsDisbursementInfos != null && smsDisbursementInfos.Any())
                {
                    foreach (var item in smsDisbursementInfos)
                    {
                        var matches = Regex.Matches(item.SmsContent, regexVIB);
                        item.ModifyDate = datecurrent;
                        if (matches.Count != 4)
                        {
                            item.Status = (int)SmsDisbursement_Status.Spam;
                        }
                        else
                        {
                            try
                            {
                                item.OTP = matches[0].Value;
                                item.AccountNumberBanking = matches[2].Value;
                                item.Amount = long.Parse(matches[3].Value.Replace(",", ""));
                                item.Status = (int)SmsDisbursement_Status.Analyzed;
                            }
                            catch
                            {
                                item.Status = (int)SmsDisbursement_Status.Spam;
                            }
                        }
                    }
                    _smsDisbursementTab.UpdateBulk(smsDisbursementInfos);
                    totalSmsOtp = smsDisbursementInfos.Count();
                }
                response.SetSucces();
                response.Data = totalSmsOtp;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSmsOtpDisbursementCommandHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
