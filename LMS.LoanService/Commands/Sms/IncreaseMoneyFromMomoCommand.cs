﻿using LMS.Common.Constants;
using LMS.LoanServiceApi.Domain.Models.SMS;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.Sms
{
    public class IncreaseMoneyFromMomoCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public SmsIncreatedMoneyCustomerRequest Model;
    }
    public class IncreaseMoneyFromMomoCommandHandler : IRequestHandler<IncreaseMoneyFromMomoCommand, LMS.Common.Constants.ResponseActionResult>
    {
        Services.ISmsAnalyticsManager _smsAnalyticsManager;
        public IncreaseMoneyFromMomoCommandHandler(Services.ISmsAnalyticsManager smsAnalyticsManager)
        {
            _smsAnalyticsManager = smsAnalyticsManager;
        }

        public async Task<ResponseActionResult> Handle(IncreaseMoneyFromMomoCommand request, CancellationToken cancellationToken)
        {
            request.Model.Source = (int)Constants.SmsAnaylyticSourceSendRequest.Momo;
            return await _smsAnalyticsManager.CreateSmsTransactionViaWallet(request.Model);
        }
    }
}
