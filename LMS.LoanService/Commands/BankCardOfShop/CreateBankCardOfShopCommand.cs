﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace LMS.LoanServiceApi.Commands.BankCardOfShop
{
    public class CreateBankCardOfShopCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public List<Domain.Models.CreateBankCardOfShop.ShopItem> LstShop { get; set; }
        public long BankCardID { get; set; }
        public string BankCardName { get; set; }
    }
    public class CreateBankCardOfShopCommandHandler : IRequestHandler<CreateBankCardOfShopCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCardOfShop> _bankCardOfShopTab;
        ILogger<CreateBankCardOfShopCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateBankCardOfShopCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCardOfShop> bankCardOfShopTab,
            ILogger<CreateBankCardOfShopCommandHandler> logger
        )
        {
            _bankCardOfShopTab = bankCardOfShopTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateBankCardOfShopCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstBankCardOfShop = new List<Domain.Tables.TblBankCardOfShop>();

                //xoa all các shop của thẻ
                _bankCardOfShopTab.DeleteWhere(x=>x.BankCardID==request.BankCardID);

                if (request.LstShop.Count > 0)
                {
                    // tạo mới
                    foreach (var item in request.LstShop)
                    {
                        lstBankCardOfShop.Add(new Domain.Tables.TblBankCardOfShop
                        {
                            ShopID = item.ShopID,
                            ShopName = item.ShopName,
                            BankCardID = request.BankCardID,
                            BankCardName = request.BankCardName,
                            CreateDate = DateTime.Now
                        });
                    }
                    _bankCardOfShopTab.InsertBulk(lstBankCardOfShop);
                }

                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateBankCardOfShopCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
