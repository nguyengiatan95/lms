﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.BankCardOfShop
{
    public class DeleteBankCardOfShopCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long ShopID { get; set; }
        public long BankCardID { get; set; }
    }
    public class DeleteBankCardOfShopCommandHandler : IRequestHandler<DeleteBankCardOfShopCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCardOfShop> _bankCardOfShopTab;
        ILogger<DeleteBankCardOfShopCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public DeleteBankCardOfShopCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCardOfShop> bankCardOfShopTab,
            ILogger<DeleteBankCardOfShopCommandHandler> logger
        )
        {
            _bankCardOfShopTab = bankCardOfShopTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(DeleteBankCardOfShopCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstBankCardOfShop = new List<Domain.Tables.TblBankCardOfShop>();
                var objBankCardOfShop = _bankCardOfShopTab.WhereClause(x => x.BankCardID == request.BankCardID && x.ShopID == request.ShopID).Query().FirstOrDefault();
                if (objBankCardOfShop != null)
                {
                    _bankCardOfShopTab.Delete(objBankCardOfShop);
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"DeleteBankCardOfShopCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
