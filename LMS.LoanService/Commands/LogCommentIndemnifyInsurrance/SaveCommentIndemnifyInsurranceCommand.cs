﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.LoanServiceApi.Commands.LogCommentIndemnifyInsurrance
{
    public class SaveCommentIndemnifyInsurranceCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public long UserID { get; set; }
        public string Note { get; set; }
    }
    public class SaveCommentIndemnifyInsurranceCommandHandler : IRequestHandler<SaveCommentIndemnifyInsurranceCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCommentIndemnifyInsurrance> _logCommentTab;
        ILogger<SaveCommentIndemnifyInsurranceCommandHandler> _logger;
        Common.Helper.Utils _common;
        public SaveCommentIndemnifyInsurranceCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
              LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCommentIndemnifyInsurrance> logCommentTab,
              LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            ILogger<SaveCommentIndemnifyInsurranceCommandHandler> logger)
        {
            _loanTab = loanTab;
            _logCommentTab = logCommentTab;
            _userTab = userTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(SaveCommentIndemnifyInsurranceCommand request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    var objLoan = _loanTab.WhereClause(x => x.LoanID == request.LoanID).Query().FirstOrDefault();
                    if (objLoan == null)
                    {
                        response.Message = MessageConstant.LoanNotExist;
                        return response;
                    }
                    var objUser = _userTab.WhereClause(x => x.UserID == request.UserID).SelectColumns(x => x.UserID, x => x.UserName).Query().FirstOrDefault();
                    Domain.Tables.TblLogCommentIndemnifyInsurrance logComment = new Domain.Tables.TblLogCommentIndemnifyInsurrance()
                    {
                        CodeID = objLoan.ContactCode,
                        CreateDate = DateTime.Now,
                        LoanID = objLoan.LoanID,
                        CustomerName = objLoan.CustomerName,
                        Note = request.Note,
                        UserID = request.UserID,
                        UserName = objUser.UserName
                    };
                    long insert = _logCommentTab.Insert(logComment);
                    if (insert > 0)
                    {
                        response.SetSucces();
                        response.Data = insert;
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"SaveCommentIndemnifyInsurranceCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}");
                }
                return response;
            });
        }
    }
}
