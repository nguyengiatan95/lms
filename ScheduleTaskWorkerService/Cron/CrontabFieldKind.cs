﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScheduleTaskWorkerService.Cron
{
    [Serializable]
    public enum CrontabFieldKind
    {
        Second,
        Minute,
        Hour,
        Day,
        Month,
        DayOfWeek
    }
}
