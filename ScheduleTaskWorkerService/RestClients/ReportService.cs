﻿using LMS.Common.Constants;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleTaskWorkerService.RestClients
{
    public interface IReportService
    {
        Task<ResponseActionResult> ProcessReportLoanDebtDaily();
    }
    public class ReportService : IReportService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        LMS.Common.Helper.Utils _common;
        ILogger<ReportService> _logger;
        public ReportService(LMS.Common.Helper.IApiHelper apiHelper, ILogger<ReportService> logger)
        {
            _apiHelper = apiHelper;
            _common = new LMS.Common.Helper.Utils();
            _logger = logger;
        }
        public async Task<ResponseActionResult> ProcessReportLoanDebtDaily()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.ReportService}{LMS.Common.Constants.ActionApiInternal.ReportService_Loan_ProcessReportLoanDebtDaily}";
                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.GET);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ScheduleTaskWorkerService_ProcessReportLoanDebtDaily|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
    }
}
