﻿using LMS.Common.Constants;
using Microsoft.Extensions.Logging;
using RestSharp;
using Steeltoe.Common.Discovery;
using Steeltoe.Discovery;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleTaskWorkerService.RestClients
{
    public interface ILoanService
    {
        Task<ResponseActionResult> ProcessSummaryBankCardFromInvoiceID();
        Task<ResponseActionResult> ProcessSummaryTransactionDate();
        Task<ResponseActionResult> ProcessSummaryStasticsLoanBorrow();
        Task<ResponseActionResult> ProcessMoneyFineLate();
        Task<ResponseActionResult> ProcessAutoPayment();
        Task<ResponseActionResult> ProcessSmsAnalytics();
        Task<ResponseActionResult> ProcessSmsOtpDisbursement();
        Task<ResponseActionResult> ProcessAutoDisbursementLoanCredit();
        Task<ResponseActionResult> ProcessCutOffLoanDaily();
        Task<ResponseActionResult> ProcessCalculatorMoneyCloseLoanDaily();
        Task<ResponseActionResult> ProcessSplitTransactionLoan();
    }
    public class LoanService : ILoanService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        LMS.Common.Helper.Utils _common;
        ILogger<LoanService> _logger;
        public LoanService(LMS.Common.Helper.IApiHelper apiHelper, ILogger<LoanService> logger)
        {
            _apiHelper = apiHelper;
            _common = new LMS.Common.Helper.Utils();
            _logger = logger;
        }
        public async Task<ResponseActionResult> ProcessSummaryBankCardFromInvoiceID()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{LMS.Common.Constants.ActionApiInternal.BankCard_ProcessSummaryBankCardFromInvoiceID}";
                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.GET);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSummaryBankCardFromInvoiceID|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
        public async Task<ResponseActionResult> ProcessSummaryTransactionDate()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{LMS.Common.Constants.ActionApiInternal.BankCard_ProcessSummaryTransactionDate}";
                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.GET);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSummaryTransactionDate|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }


        public async Task<ResponseActionResult> ProcessSummaryStasticsLoanBorrow()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{LMS.Common.Constants.ActionApiInternal.Report_ProcessSummaryStasticsLoanBorrow}";
                var dataPost = new
                {
                    ShopID = 0
                };
                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.POST, body: dataPost);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSummaryStasticsLoanBorrow|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }

        public async Task<ResponseActionResult> ProcessMoneyFineLate()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{LMS.Common.Constants.ActionApiInternal.Loan_ProcessMoneyFineLate}";
                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.GET);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessMoneyFineLate|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }

        public async Task<ResponseActionResult> ProcessAutoPayment()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{LMS.Common.Constants.ActionApiInternal.Loan_ProcessAutoPayment}";
                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.GET);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessAutoPayment|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
        public async Task<ResponseActionResult> ProcessSmsAnalytics()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{LMS.Common.Constants.ActionApiInternal.Sms_ProcessSmsAnalytics}";
                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.GET);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSmsAnalytics|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }

        public async Task<ResponseActionResult> ProcessSmsOtpDisbursement()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{LMS.Common.Constants.ActionApiInternal.Sms_ProcessSmsOtpDisbursement}";
                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.GET);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSmsOtpDisbursement|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }

        public async Task<ResponseActionResult> ProcessAutoDisbursementLoanCredit()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{LMS.Common.Constants.ActionApiInternal.Loan_ProcessAutoDisbursementLoanCredit}";
                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.GET);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessAutoDisbursementLoanCredit|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
        public async Task<ResponseActionResult> ProcessCutOffLoanDaily()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{LMS.Common.Constants.ActionApiInternal.Loan_ProcessCutOffLoanDaily}";
                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.GET);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessCutOffLoanDaily|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
        public async Task<ResponseActionResult> ProcessCalculatorMoneyCloseLoanDaily()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{LMS.Common.Constants.ActionApiInternal.Loan_ProcessCalculatorMoneyCloseLoanDaily}";
                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.GET);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessCalculatorMoneyCloseLoanDaily|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
        public async Task<ResponseActionResult> ProcessSplitTransactionLoan()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{LMS.Common.Constants.ActionApiInternal.Loan_ProcessSplitTransactionLoan}";
                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.GET);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSplitTransactionLoan|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
    }
}
