﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace ScheduleTaskWorkerService.RestClients
{
    public static class RestClientsInstaller
    {
        public static IServiceCollection AddRestClientsService(this IServiceCollection services)
        {
            //services.AddTransient(typeof(IPaymentScheduleService), typeof(PaymentScheduleService));
            //services.AddTransient(typeof(IOutGatewayService), typeof(OutGatewayService));
            services.AddTransient(typeof(ICustomerService), typeof(CustomerService));
            services.AddTransient(typeof(IInsuranceService), typeof(InsuranceService));
            services.AddTransient<ILoanService, LoanService>();
            services.AddTransient<ILenderService, LenderService>();
            services.AddTransient<IOutGatewayService, OutGatewayService>();
            services.AddTransient<ICollectionService, CollectionService>();
            services.AddTransient<IReportService, ReportService>();
            return services;
        }
    }
}
