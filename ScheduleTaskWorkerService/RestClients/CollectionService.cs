﻿using LMS.Common.Constants;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleTaskWorkerService.RestClients
{
    public interface ICollectionService
    {
        Task<ResponseActionResult> ProcessSummaryEmployeeHandleLoan();
        Task<ResponseActionResult> ProcessTranDataCallProxy();
        Task<ResponseActionResult> ProcessReportLoanDebtType();
        Task<ResponseActionResult> PushPhoneDailyToCallCenter();
        Task<ResponseActionResult> ProcessingThePhoneForDailyCall();
        Task<ResponseActionResult> CancelLoanExemptionByExpired();
    }
    public class CollectionService : ICollectionService
    {
        readonly LMS.Common.Helper.IApiHelper _apiHelper;
        readonly ILogger<CollectionService> _logger;
        public CollectionService(LMS.Common.Helper.IApiHelper apiHelper, ILogger<CollectionService> logger)
        {
            _apiHelper = apiHelper;
            _logger = logger;
        }
        public async Task<ResponseActionResult> ProcessSummaryEmployeeHandleLoan()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_Report_ProcessSummaryEmployeeHandleLoan}";
                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.GET);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSummaryBankCardFromInvoiceID|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
        public async Task<ResponseActionResult> ProcessTranDataCallProxy()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.TrandataCallProxy}";
                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.GET);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_ProcessTranDataCallProxy|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }

        public async Task<ResponseActionResult> ProcessReportLoanDebtType()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_Report_ProcessReportLoanDebtType}";
                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.POST);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_ProcessReportLoanDebtType|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
        public async Task<ResponseActionResult> PushPhoneDailyToCallCenter()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_SmartDialer_PushPhoneDailyToCallCenter}";
                var dataPost = new
                {
                    Status = (int)PlanHandleCallDaily_Status.Waiting,
                    PriorityOrder = 0 // job gọi mặc đinh = 0, != 0 admin xử lý = tay
                };
                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.POST, body: dataPost);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_PushPhoneDailyToCallCenter|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
        public async Task<ResponseActionResult> ProcessingThePhoneForDailyCall()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_SmartDialer_ProcessingThePhoneForDailyCall}";

                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.GET);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_ProcessingThePhoneForDailyCall|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
        public async Task<ResponseActionResult> CancelLoanExemptionByExpired()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_MGLP_CancelLoanExemptionByExpired}";

                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.GET);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_CancelLoanExemptionByExpired|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
    }
}
