﻿using LMS.Common.Constants;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleTaskWorkerService.RestClients
{
    public interface ILenderService
    {
        Task<ResponseActionResult> RunPaymentScheduleDepositMoney();
    }
    public class LenderService : ILenderService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        LMS.Common.Helper.Utils _common;
        ILogger<LenderService> _logger;
        public LenderService(LMS.Common.Helper.IApiHelper apiHelper, ILogger<LenderService> logger)
        {
            _apiHelper = apiHelper;
            _common = new LMS.Common.Helper.Utils();
            _logger = logger;

        }

        public async Task<ResponseActionResult> RunPaymentScheduleDepositMoney()
        {

            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.LenderService}{LMS.Common.Constants.ActionApiInternal.Lender_RunPaymentScheduleDepositMoney}";
                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.POST);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSummaryBankCardFromInvoiceID|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
    }
}
