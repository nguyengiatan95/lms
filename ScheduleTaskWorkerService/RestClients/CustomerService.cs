﻿using LMS.Common.Constants;
using Microsoft.Extensions.Logging;
using RestSharp;
using Steeltoe.Common.Discovery;
using Steeltoe.Discovery;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleTaskWorkerService.RestClients
{
    public interface ICustomerService
    {
        Task<IEnumerable<LMS.Entites.Dtos.CustomerService.CustomerInfoModel>> GetCustomerInfosByIDs(List<long> customerIDs);
        Task<ResponseActionResult> RegisterVa();
        Task<ResponseActionResult> UpdatePayType();
    }
    public class CustomerService : ICustomerService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        LMS.Common.Helper.Utils _common;
        ILogger<CustomerService> _logger;
        public CustomerService(LMS.Common.Helper.IApiHelper apiHelper, ILogger<CustomerService> logger)
        {
            _apiHelper = apiHelper;
            _common = new LMS.Common.Helper.Utils();
            _logger = logger;
        }
        public async Task<IEnumerable<LMS.Entites.Dtos.CustomerService.CustomerInfoModel>> GetCustomerInfosByIDs(List<long> customerIDs)
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.CustomerService}{LMS.Common.Constants.ActionApiInternal.Customer_GetCustomerInfosByIDs}";
                var dataPost = new
                {
                    CustomerIDs = customerIDs
                };
                return await _apiHelper.ExecuteAsync<List<LMS.Entites.Dtos.CustomerService.CustomerInfoModel>>(url, body: dataPost, method: RestSharp.Method.POST);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{System.Reflection.MethodBase.GetCurrentMethod().Name}_Exception|customerIDs={_common.ConvertObjectToJSonV2(customerIDs)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
        public async Task<ResponseActionResult> RegisterVa()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.CustomerService}{LMS.Common.Constants.ActionApiInternal.VA_RegisterVa}";
                return await _apiHelper.ExecuteAsync(url, method: RestSharp.Method.POST);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CustomerService_RegisterVa_Exception|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
        public async Task<ResponseActionResult> UpdatePayType()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.CustomerService}{LMS.Common.Constants.ActionApiInternal.Customer_UpdatePayType}";
                var dataPost = new
                {
                    LoanID = 0
                };
                return await _apiHelper.ExecuteAsync(url, body: dataPost, method: RestSharp.Method.POST);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CustomerService_UpdatePayType_Exception|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
    }
}
