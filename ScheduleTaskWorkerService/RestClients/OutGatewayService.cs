﻿using LMS.Common.Constants;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleTaskWorkerService.RestClients
{
    public interface IOutGatewayService
    {
        Task<ResponseActionResult> QueueCallAPIAG(); 
        Task<LMS.Entites.Dtos.LOSServices.MapResultLoanWaitingDisbursementDetailLOS> GetLoanCreditDetail(long loanID);
    }
    public class OutGatewayService : IOutGatewayService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        LMS.Common.Helper.Utils _common;
        ILogger<OutGatewayService> _logger;
        public OutGatewayService(LMS.Common.Helper.IApiHelper apiHelper, ILogger<OutGatewayService> logger)
        {
            _apiHelper = apiHelper;
            _common = new LMS.Common.Helper.Utils();
            _logger = logger;

        }
        public async Task<ResponseActionResult> QueueCallAPIAG()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_QueueCallAPIAG}";
                return await _apiHelper.ExecuteAsync(url: url.ToString());
            }
            catch (Exception ex)
            {
                _logger.LogError($"OutGatewayService_QueueCallAPIAG_Exception|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
        public async Task<LMS.Entites.Dtos.LOSServices.MapResultLoanWaitingDisbursementDetailLOS> GetLoanCreditDetail(long loanID)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_LOS_GetLoanCreditDetail}/{loanID}";
            return await _apiHelper.ExecuteAsync<LMS.Entites.Dtos.LOSServices.MapResultLoanWaitingDisbursementDetailLOS>(url: url.ToString());
        }

    }
}

