﻿using LMS.Common.Constants;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleTaskWorkerService.RestClients
{
    public interface IInsuranceService
    {
        Task<ResponseActionResult> ByInsurance();
        Task<ResponseActionResult> InformationInsuranceAI();
    }
    public class InsuranceService : IInsuranceService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        LMS.Common.Helper.Utils _common;
        ILogger<InsuranceService> _logger;
        public InsuranceService(LMS.Common.Helper.IApiHelper apiHelper, ILogger<InsuranceService> logger)
        {
            _apiHelper = apiHelper;
            _common = new LMS.Common.Helper.Utils();
            _logger = logger;
        }
        public async Task<ResponseActionResult> ByInsurance()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.InsuranceSerivce}{LMS.Common.Constants.ActionApiInternal.Insurance_ByInsurance}";
                return await _apiHelper.ExecuteAsync(url: url.ToString());
            }
            catch (Exception ex)
            {
                _logger.LogError($"{System.Reflection.MethodBase.GetCurrentMethod().Name}_Exception|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
        public async Task<ResponseActionResult> InformationInsuranceAI()
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.InsuranceSerivce}{LMS.Common.Constants.ActionApiInternal.Insurance_InformationInsuranceAI}";
                return await _apiHelper.ExecuteAsync(url: url.ToString());
            }
            catch (Exception ex)
            {
                _logger.LogError($"IInsuranceService_InformationInsuranceAI_Exception|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }

    }
}
  