﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScheduleTaskWorkerService.Tables
{
    [Dapper.Contrib.Extensions.Table("TblLogCustomerCash")]
    public class TblLogCustomerCash
    {
        [Dapper.Contrib.Extensions.Key]
        public long LogCustomerCashID { get; set; }

        public long? CustomerID { get; set; }

        public long? MoneyBefore { get; set; }

        public long? MoneyAfter { get; set; }

        public long? MoneyAdd { get; set; }

        public DateTime? CreateDate { get; set; }

        public string Description { get; set; }
    }
}
