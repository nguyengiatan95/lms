﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScheduleTaskWorkerService.Tables
{
    [Dapper.Contrib.Extensions.Table("TblInvoice")]
    public class TblInvoice
    {
        [Dapper.Contrib.Extensions.Key]
        public long InvoiceID { get; set; }

        public int InvoiceType { get; set; }

        public int InvoiceSubType { get; set; }

        public string InvoiceCode { get; set; }

        public long ShopID { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? ModifyDate { get; set; }

        public long? UserIDCreate { get; set; }

        public long? UserIDModify { get; set; }

        public DateTime? TransactionDate { get; set; }

        public long? TotalMoney { get; set; }

        public string Note { get; set; }

        public long? BankCardID { get; set; }

        public long? SourceID { get; set; }

        public long? DestinationID { get; set; }

        [Dapper.Contrib.Extensions.Key]
        public System.Byte[] Version { get; set; }

        public string JsonExtra { get; set; }

        public short? Status { get; set; }

        public long? LoanID { get; set; }

        public long? MoneyFee { get; set; }

        public long? MoneyVAT { get; set; }

        public long? TransactionMoney { get; set; }

        public long? CustomerID { get; set; }

        public long? LenderID { get; set; }

        public int JobStatus { get; set; }
        public int Payee { get; set; }
        public long PartnerID { get; set; }
        public string PathImg { get; set; }

    }
}
