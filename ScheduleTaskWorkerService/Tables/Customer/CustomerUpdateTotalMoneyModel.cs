﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScheduleTaskWorkerService.Tables.Customer
{
    [Dapper.Contrib.Extensions.Table("TblCustomer")]
    public class CustomerUpdateTotalMoneyModel
    {
        [Dapper.Contrib.Extensions.Key]
        public long CustomerID { get; set; }
        public long TotalMoney { get; set; }

        [Dapper.Contrib.Extensions.Key]
        public byte[] Version { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
