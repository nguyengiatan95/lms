﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScheduleTaskWorkerService.Tables.Loan
{

    [Dapper.Contrib.Extensions.Table("TblLoan")]
    public class LoanUpdateJsonExtra
    {

        [Dapper.Contrib.Extensions.Key]
        public long LoanID { get; set; }
        [Dapper.Contrib.Extensions.Key]
        public byte[] Version { get; set; }

        public string JsonExtra { get; set; }
        public long LoanCreditIDOfPartner { get; set; }
    }
}
