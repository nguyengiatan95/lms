﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScheduleTaskWorkerService.Tables.Loan
{

    [Dapper.Contrib.Extensions.Table("TblLoan")]
    public class LoanUpdateMoneyFineOriginalModel
    {
        [Dapper.Contrib.Extensions.Key]
        public long LoanID { get; set; }

        [Dapper.Contrib.Extensions.Key]
        public System.Byte[] Version { get; set; }

        public int Status { get; set; }
        public decimal? FeeFineOriginal { get; set; }
        public int UnitFeeFineOriginal { get; set; }
        public long ProductID { get; set; }
        public long LoanCreditIDOfPartner { get; set; }
    }
}
