﻿using LMS.Common.Constants;
using LMS.Common.DAL;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleTaskWorkerService.Services.AutoReviewSystem
{
    public interface IRunUpdateFeeFineOriginalService
    {
        Task RunUpdateDefault();
        Task RunUpdateFromLos();

        Task RunUpdateLoanExtraFromLos();
    }
    public class RunUpdateFeeFineOriginalService : IRunUpdateFeeFineOriginalService
    {
        ITableHelper<Tables.Loan.LoanUpdateMoneyFineOriginalModel> _loanUpdateMoneyFineOriginalTab;
        ITableHelper<Tables.Loan.LoanUpdateJsonExtra> _loanUpdateJsonExtraTab;
        ITableHelper<Tables.TblProductCredit> _productCreditTab;
        ILogger<RunUpdateFeeFineOriginalService> _logger;
        private const long MaxLoanIDBeforeChangePolicy = 150866;
        private static int MaxCountToStop = 10;
        private static int CountToStop = 0;
        private static long LoanIDLatestUpdateJsonExtra = 0;
        RestClients.IOutGatewayService _outGatewayService;
        LMS.Common.Helper.Utils _common;
        public RunUpdateFeeFineOriginalService(ITableHelper<Tables.Loan.LoanUpdateMoneyFineOriginalModel> loanUpdateMoneyFineOriginalTab,
            ITableHelper<Tables.Loan.LoanUpdateJsonExtra> loanUpdateJsonExtraTab,
            ITableHelper<Tables.TblProductCredit> productCreditTab,
            RestClients.IOutGatewayService outGatewayService,
            ILogger<RunUpdateFeeFineOriginalService> logger)
        {
            _loanUpdateMoneyFineOriginalTab = loanUpdateMoneyFineOriginalTab;
            _loanUpdateJsonExtraTab = loanUpdateJsonExtraTab;
            _productCreditTab = productCreditTab;
            _outGatewayService = outGatewayService;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task RunUpdateDefault()
        {
            try
            {
                if (CountToStop >= MaxCountToStop)
                {
                    _ = RunUpdateFromLos();
                    return;
                }
                var lstDataUpdate = await _loanUpdateMoneyFineOriginalTab.SetGetTop(LMS.Common.Constants.TimaSettingConstant.MaxItemQuery)
                                                                                 .WhereClause(x => x.Status == (int)LMS.Common.Constants.Loan_Status.Lending)
                                                                                 .WhereClause(x => (x.FeeFineOriginal == null || x.FeeFineOriginal < 1) && x.LoanID <= MaxLoanIDBeforeChangePolicy)
                                                                                 .OrderBy(x => x.LoanID).QueryAsync();
                if (lstDataUpdate == null || !lstDataUpdate.Any())
                {
                    CountToStop++;
                    if (CountToStop == MaxCountToStop)
                    {
                        _logger.LogError($"RunUpdateFeeFineOriginalService_Done|Stopped");
                    }
                    return;
                }
                CountToStop = 0;
                var dictProductCreditInfos = (await _productCreditTab.QueryAsync()).ToDictionary(x => (long)x.ProductCreditID, x => x);
                foreach (var item in lstDataUpdate)
                {
                    var productInfo = dictProductCreditInfos.GetValueOrDefault(item.ProductID);
                    item.FeeFineOriginal = Convert.ToDecimal(SettingFineForProduct.PercentPrePaymentPenaltySmall);
                    if (productInfo.TypeProduct == (int)Product_Type.Big)
                    {
                        item.FeeFineOriginal = Convert.ToDecimal(SettingFineForProduct.PercentPrePaymentPenaltyBig);
                    }
                    item.UnitFeeFineOriginal = (int)Loan_UnitRate.Percent;
                }
                _loanUpdateMoneyFineOriginalTab.UpdateBulk(lstDataUpdate);
            }
            catch (Exception ex)
            {
                _logger.LogError($"RunUpdateFeeFineOriginalService_RunUpdateDefault|ex={ex.Message}-{ex.StackTrace}");
            }
            return;
        }
        public async Task RunUpdateFromLos()
        {
            try
            {
                if (CountToStop == MaxCountToStop * 2)
                {
                    return;
                }
                var lstDataUpdate = await _loanUpdateMoneyFineOriginalTab.SetGetTop(LMS.Common.Constants.TimaSettingConstant.MaxItemQuery)
                                                                                    .WhereClause(x => x.Status == (int)LMS.Common.Constants.Loan_Status.Lending)
                                                                                    .WhereClause(x => (x.FeeFineOriginal == null || x.FeeFineOriginal < 1) && x.LoanID > MaxLoanIDBeforeChangePolicy)
                                                                                    .OrderBy(x => x.LoanID).QueryAsync();
                if (lstDataUpdate == null || !lstDataUpdate.Any())
                {
                    CountToStop++;
                    if (CountToStop == MaxCountToStop * 2)
                    {
                        _logger.LogError($"RunUpdateFeeFineOriginalService_Done_LOS|Stopped");
                    }
                    return;
                }
                Dictionary<long, Task> dictActionGetLoanLosInfos = new Dictionary<long, Task>();
                foreach (var item in lstDataUpdate)
                {
                    dictActionGetLoanLosInfos.Add(item.LoanID, _outGatewayService.GetLoanCreditDetail(item.LoanCreditIDOfPartner));
                }
                await Task.WhenAll(dictActionGetLoanLosInfos.Values);
                foreach (var item in lstDataUpdate)
                {
                    var detailLosInfo = ((Task<LMS.Entites.Dtos.LOSServices.MapResultLoanWaitingDisbursementDetailLOS>)dictActionGetLoanLosInfos[item.LoanID]).Result;
                    if (detailLosInfo != null && detailLosInfo.LoanBriefId > 0)
                    {
                        item.UnitFeeFineOriginal = (int)Loan_UnitRate.Percent;
                        item.FeeFineOriginal = detailLosInfo.FeePaymentBeforeLoan * 100;
                    }
                }
                _loanUpdateMoneyFineOriginalTab.UpdateBulk(lstDataUpdate);
            }
            catch (Exception ex)
            {
                _logger.LogError($"RunUpdateFeeFineOriginalService_RunUpdateFromLos|ex={ex.Message}-{ex.StackTrace}");
            }
            return;
        }

        public async Task RunUpdateLoanExtraFromLos()
        {
            try
            {
                var lstDataUpdate = await _loanUpdateJsonExtraTab.SetGetTop(LMS.Common.Constants.TimaSettingConstant.MaxItemQuery)
                                                                                            .WhereClause(x => x.LoanID > LoanIDLatestUpdateJsonExtra)
                                                                                            .OrderBy(x => x.LoanID).QueryAsync();
                if (lstDataUpdate == null || !lstDataUpdate.Any())
                {
                    CountToStop++;
                    if (CountToStop == MaxCountToStop)
                    {
                        _logger.LogError($"RunUpdateLoanExtraFromLos_Done_LOS|Stopped");
                    }
                    return;
                }
                Dictionary<long, Task> dictActionGetLoanLosInfos = new Dictionary<long, Task>();
                foreach (var item in lstDataUpdate)
                {
                    var jsonExtra = _common.ConvertJSonToObjectV2<Tables.LoanJsonExtra>(item.JsonExtra);
                    dictActionGetLoanLosInfos.Add(item.LoanID, _outGatewayService.GetLoanCreditDetail(item.LoanCreditIDOfPartner));
                    if (LoanIDLatestUpdateJsonExtra < item.LoanID)
                    {
                        LoanIDLatestUpdateJsonExtra = item.LoanID;
                    }
                }
                await Task.WhenAll(dictActionGetLoanLosInfos.Values);
                List<Tables.Loan.LoanUpdateJsonExtra> lstLoanUpdate = new List<Tables.Loan.LoanUpdateJsonExtra>();
                foreach (var item in lstDataUpdate)
                {
                    var jsonExtra = _common.ConvertJSonToObjectV2<Tables.LoanJsonExtra>(item.JsonExtra);
                    var detailLosInfo = ((Task<LMS.Entites.Dtos.LOSServices.MapResultLoanWaitingDisbursementDetailLOS>)dictActionGetLoanLosInfos[item.LoanID]).Result;
                    if (detailLosInfo != null && detailLosInfo.LoanBriefId > 0)
                    {
                        jsonExtra.IsLocate = detailLosInfo.IsLocate;
                        jsonExtra.TopUpOfLoanBriefID = detailLosInfo.TopUpOfLoanBriefID;
                        jsonExtra.LoanBriefPropertyPlateNumber = detailLosInfo.LoanBriefProperty?.PlateNumber;
                        item.JsonExtra = _common.ConvertObjectToJSonV2(jsonExtra);
                        lstLoanUpdate.Add(item);
                    }
                }
                if (lstLoanUpdate.Count > 0)
                {
                    _loanUpdateJsonExtraTab.UpdateBulk(lstLoanUpdate);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"RunUpdateLoanExtraFromLos|LoanIDLatestUpdateJsonExtra={LoanIDLatestUpdateJsonExtra}|{ex.Message}-{ex.StackTrace}");
            }
        }
    }
}
