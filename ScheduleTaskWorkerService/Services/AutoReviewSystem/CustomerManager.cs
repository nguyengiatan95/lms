﻿using LMS.Common.Constants;
using LMS.Common.DAL;
using LMS.Common.Helper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleTaskWorkerService.Services.AutoReviewSystem
{
    public interface ICustomerManager
    {
        Task<long> RunUpdateMoney();
    }
    public class CustomerManager : ICustomerManager
    {
        ITableHelper<Tables.TblLoan> _loanTab;
        ITableHelper<Tables.TblLogCustomerCash> _logCustomerCashTab;
        ITableHelper<Tables.TblInvoice> _invoiceTab;
        ITableHelper<Tables.TblTransaction> _transactionTab;
        ITableHelper<Tables.Customer.CustomerUpdateTotalMoneyModel> _customerUpdateTotalMoneyTab;

        ILogger<CustomerManager> _logger;
        public CustomerManager(ITableHelper<Tables.TblLoan> loanTab,
            ITableHelper<Tables.TblLogCustomerCash> logCustomerCashTab,
            ITableHelper<Tables.TblInvoice> invoiceTab,
            ITableHelper<Tables.TblTransaction> transactionTab,
            ITableHelper<Tables.Customer.CustomerUpdateTotalMoneyModel> customerUpdateTotalMoneyTab,
            ILogger<CustomerManager> logger)
        {
            _loanTab = loanTab;
            _logCustomerCashTab = logCustomerCashTab;
            _invoiceTab = invoiceTab;
            _transactionTab = transactionTab;
            _customerUpdateTotalMoneyTab = customerUpdateTotalMoneyTab;
            _logger = logger;
        }

        public async Task<long> RunUpdateMoney()
        {
            long latestID = 0;
            List<long> lstLoanIDs = new List<long>();
            List<long> lstCustomerIDs = new List<long>();
            List<int> lstInvoiceSubTypeCustomer = new List<int>
            {
                (int)GroupInvoiceType.ReceiptCustomer,
                (int)GroupInvoiceType.ReceiptCustomerConsider,
                (int)GroupInvoiceType.ReceiptOnBehalfShop,
                (int)GroupInvoiceType.PaySlipGiveBackExcessCustomer
            };
            Dictionary<long, Tables.TblInvoice> dictCustomerHasInvoice = new Dictionary<long, Tables.TblInvoice>();
            Dictionary<string, Tables.TblLogCustomerCash> dictLogCustomerCashInfos = new Dictionary<string, Tables.TblLogCustomerCash>();
            while (true)
            {
                var lstLoanInfos = (await _loanTab.SetGetTop(TimaSettingConstant.MaxItemQuery)
                                                 .SelectColumns(x => x.LoanID, x => x.CustomerID, x => x.ContactCode)
                                                 .WhereClause(x => x.LoanID > latestID)
                                                 .OrderBy(x => x.LoanID)
                                                 .QueryAsync()).ToDictionary(x => x.LoanID, x => x);
                if (lstLoanInfos == null || !lstLoanInfos.Any())
                {
                    break;
                }
                lstLoanIDs.Clear();
                lstCustomerIDs.Clear();
                foreach (var item in lstLoanInfos)
                {
                    lstCustomerIDs.Add(item.Value.CustomerID.Value);
                    lstLoanIDs.Add(item.Key);
                    // gán id lớn nhất trong ds lấy ra
                    if (latestID < item.Key)
                    {
                        latestID = item.Key;
                    }
                }
                // lấy phiếu nạp tiền khách hàng, phiếu trả tiền thừa
                // lấy lịch sử gạch nợ
                var lstInvoiceCustomersTask = _invoiceTab.SelectColumns(x => x.InvoiceID, x => x.TransactionDate, x => x.CustomerID, x => x.CreateDate, x => x.TotalMoney, x => x.InvoiceSubType, x => x.InvoiceType, x => x.Note)
                                                     .WhereClause(x => lstInvoiceSubTypeCustomer.Contains(x.InvoiceSubType))
                                                     .WhereClause(x => lstCustomerIDs.Contains((long)x.CustomerID) && x.TotalMoney != 0)
                                                     .WhereClause(x => x.Status == (int)Invoice_Status.Confirmed)
                                                     .QueryAsync();
                var lstTransactionTask = _transactionTab.WhereClause(x => lstLoanIDs.Contains(x.LoanID) && x.ActionID != (int)Transaction_Action.ChoVay && x.TotalMoney != 0).QueryAsync();
                await Task.WhenAll(lstInvoiceCustomersTask, lstTransactionTask);
                var lstInvoiceCustomer = lstInvoiceCustomersTask.Result;
                var lstTransaction = lstTransactionTask.Result;
                // pải lấy phát sinh khi có phiếu nạp tiền mới tính vào gạch nợ
                if (lstInvoiceCustomer != null)
                {
                    lstInvoiceCustomer = lstInvoiceCustomer.OrderBy(x => x.CustomerID).ThenBy(x => x.TransactionDate);
                    foreach (var item in lstInvoiceCustomer)
                    {
                        var keyDiction = $"{item.TransactionDate:yyyyMMddHHmmss}_{item.CustomerID}";
                        if (!dictLogCustomerCashInfos.ContainsKey(keyDiction))
                        {
                            dictLogCustomerCashInfos.Add(keyDiction, new Tables.TblLogCustomerCash
                            {
                                CustomerID = item.CustomerID,
                                CreateDate = item.CreateDate,
                                MoneyAdd = item.TotalMoney,
                                Description = $"{item.Note ?? ((GroupInvoiceType)item.InvoiceSubType).GetDescription()}"
                            });
                        }
                        // kh có phiếu nạp tiền đầu tiên
                        if (item.InvoiceType != (int)InvoiceType.PaySlip)
                        {
                            if (!dictCustomerHasInvoice.ContainsKey(item.CustomerID.Value))
                            {
                                dictCustomerHasInvoice.Add(item.CustomerID.Value, item);
                            }
                        }
                    }
                }
                if (lstTransaction != null)
                {
                    foreach (var item in lstTransaction)
                    {
                        var loanInfo = lstLoanInfos.GetValueOrDefault(item.LoanID);
                        // kh ko có phiếu nạp tiền trước đó
                        if (!dictCustomerHasInvoice.ContainsKey(loanInfo.CustomerID.Value))
                        {
                            continue;
                        }
                        var invoiceFirst = dictCustomerHasInvoice[loanInfo.CustomerID.Value];
                        if (item.CreateDate < invoiceFirst.TransactionDate.Value)
                        {
                            continue;
                        }
                        var keyDiction = $"{item.CreateDate:yyyyMMddHHmmss}_{loanInfo.CustomerID}";
                        if (!dictLogCustomerCashInfos.ContainsKey(keyDiction))
                        {
                            dictLogCustomerCashInfos.Add(keyDiction, new Tables.TblLogCustomerCash
                            {
                                CustomerID = loanInfo.CustomerID,
                                CreateDate = item.CreateDate,
                                Description = item.ActionID == (int)Transaction_Action.TraNoPhiPhatMuon ? "Thanh toán phí trả chậm" : $"Gạch nợ {loanInfo?.ContactCode} ngày {item.CreateDate:dd/MM/yyyy}",
                                MoneyAdd = 0
                            });
                        }
                        // transaction phải trừ
                        dictLogCustomerCashInfos[keyDiction].MoneyAdd -= item.TotalMoney;
                    }
                }
                // xóa khách hàng sẽ dc cập lại

                _ = await _logCustomerCashTab.DeleteWhereAsync(x => lstCustomerIDs.Contains((long)x.CustomerID));
            }
            dictLogCustomerCashInfos = dictLogCustomerCashInfos.OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value);
            var dictCustomerMoneyCurrent = new Dictionary<long, long>();
            foreach (var item in dictLogCustomerCashInfos)
            {
                if (!dictCustomerMoneyCurrent.ContainsKey(item.Value.CustomerID.Value))
                {
                    dictCustomerMoneyCurrent.Add(item.Value.CustomerID.Value, 0);
                }
                item.Value.MoneyBefore = dictCustomerMoneyCurrent[item.Value.CustomerID.Value];
                item.Value.MoneyAfter = item.Value.MoneyBefore + item.Value.MoneyAdd;
                dictCustomerMoneyCurrent[item.Value.CustomerID.Value] = item.Value.MoneyAfter.Value;
            }
            _logCustomerCashTab.InsertBulk(dictLogCustomerCashInfos.Values.ToList());
            long minCustomerID = int.MaxValue;
            long maxCustomerID = 0;
            foreach (var item in dictCustomerMoneyCurrent)
            {
                if (minCustomerID > item.Key)
                {
                    minCustomerID = item.Key;
                }
                if (maxCustomerID < item.Key)
                {
                    maxCustomerID = item.Key;
                }
            }
            minCustomerID -= 1;
            maxCustomerID += 1;
            var lstCustomerUpdateMoney = await _customerUpdateTotalMoneyTab.WhereClause(x => x.CustomerID > minCustomerID && x.CustomerID < maxCustomerID).QueryAsync();
            DateTime currentDate = DateTime.Now;
            if (lstCustomerUpdateMoney != null)
            {
                foreach (var item in lstCustomerUpdateMoney)
                {
                    if (dictCustomerMoneyCurrent.ContainsKey(item.CustomerID))
                    {
                        item.TotalMoney = dictCustomerMoneyCurrent[item.CustomerID];
                        item.ModifyDate = currentDate;
                    }
                }
                _customerUpdateTotalMoneyTab.UpdateBulk(lstCustomerUpdateMoney);
            }

            return dictLogCustomerCashInfos.Count();
        }
    }
}
