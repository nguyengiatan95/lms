﻿using Confluent.Kafka;
using LMS.Common.Constants;
using LMS.Common.DAL;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ScheduleTaskWorkerService.Services.KafkaEvent
{
    public class KakfaService : BackgroundService
    {
        ITableHelper<Tables.TblKafkaLogEvent> _kafkaLogEventTab;
        IProducer<Null, string> _producerBuilder;
        ILogger<KakfaService> _logger;
        public KakfaService(ITableHelper<Tables.TblKafkaLogEvent> kafkaLogEventTab,
            ProducerConfig config,
            ILogger<KakfaService> logger)
        {
            _kafkaLogEventTab = kafkaLogEventTab;
            _logger = logger;
            _producerBuilder = new ProducerBuilder<Null, string>(config).Build();
        }
        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                await ProcessProducer(cancellationToken);

                await Task.Delay(TimeSpan.FromSeconds(1), cancellationToken);
            }
        }

        private async Task ProcessProducer(CancellationToken cancellationToken)
        {
            try
            {
                var lstProducer = await _kafkaLogEventTab.SetGetTop(LMS.Common.Constants.TimaSettingConstant.MaxItemQuery)
                                        .WhereClause(x => x.Status == (int)KafkaLogEvent_Status.Waitting).OrderBy(x => x.KafkaLogEventID).QueryAsync();
                if (lstProducer == null || !lstProducer.Any())
                {
                    return;
                }
                lstProducer = lstProducer.OrderBy(x => x.KafkaLogEventID);
                foreach (var item in lstProducer)
                {
                    // xử lý tuần tự
                    var (pubResult, error) = await Publish(item.TopicName, item.DataMessage);
                    if (pubResult != null)
                    {
                        item.Status = (int)KafkaLogEvent_Status.Success;
                    }
                    else
                    {
                        item.Status = (int)KafkaLogEvent_Status.Fail;
                        item.ErrorReason = error;
                    }
                    item.ModifyDate = DateTime.Now;
                }
                _kafkaLogEventTab.UpdateBulk(lstProducer);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessProducer|ex={ex.Message}|{ex.StackTrace}");
            }

        }
        private async Task<(DeliveryResult<Null, string>, string)> Publish(string topicName, string data)
        {
            try
            {
                return (await _producerBuilder.ProduceAsync(topicName, new Message<Null, string> { Value = data }), string.Empty);
            }
            catch (ProduceException<Null, string> e)
            {
                _logger.LogError($"Publish|topic={topicName}|data={data}|e.Error.Reason={e.Error.Reason}");
                return (null, e.Error.Reason);
            }
        }
    }
}
