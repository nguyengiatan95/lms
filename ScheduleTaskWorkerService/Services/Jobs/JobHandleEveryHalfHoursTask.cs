﻿using Microsoft.Extensions.Logging;
using ScheduleTaskWorkerService.Scheduling;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ScheduleTaskWorkerService.Services.Jobs
{
    public class JobHandleEveryHalfHoursTask : IScheduledTask
    {
        ILogger<JobHandleEveryHalfHoursTask> _logger;
        RestClients.ICollectionService _collectionService;
        RestClients.ILoanService _loanService;
        public JobHandleEveryHalfHoursTask(
            RestClients.ICollectionService collectionService,
            RestClients.ILoanService loanService,
            ILogger<JobHandleEveryHalfHoursTask> logger)
        {
            _collectionService = collectionService;
            _loanService = loanService;
            _logger = logger;
        }
        public string Schedule => "0 0/15 * * * *";
        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("JobHandleEveryHalfHoursTask start");
            var t1 = _collectionService.ProcessSummaryEmployeeHandleLoan();
            var t2 = _loanService.ProcessSplitTransactionLoan();
            var t3 = _collectionService.ProcessReportLoanDebtType();
            await Task.WhenAll(t1, t2, t3);
            _logger.LogInformation("JobHandleEveryHalfHoursTask end");
        }
    }
}
