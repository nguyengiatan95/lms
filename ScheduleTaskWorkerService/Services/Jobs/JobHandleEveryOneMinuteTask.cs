﻿using Microsoft.Extensions.Logging;
using ScheduleTaskWorkerService.Scheduling;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ScheduleTaskWorkerService.Services
{
    public class JobHandleEveryOneMinuteTask : IScheduledTask
    {
        ILogger<JobHandleEveryOneMinuteTask> _logger;
        RestClients.ILoanService _loanService;
        RestClients.IInsuranceService _insuranceService;
        public JobHandleEveryOneMinuteTask(RestClients.ILoanService loanService,
            RestClients.IInsuranceService insuranceService,
            ILogger<JobHandleEveryOneMinuteTask> logger)
        {
            _loanService = loanService;
            _insuranceService = insuranceService;
            _logger = logger;
        }
        public string Schedule => "0 0/2 * * * *";

        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("JobHandleEveryOneMinuteTask start");
            var t2 = _loanService.ProcessSummaryStasticsLoanBorrow();
            var t3 = _insuranceService.ByInsurance();
            var t4 = _loanService.ProcessSmsAnalytics();
            var t5 = _loanService.ProcessAutoPayment();
            var t6 = _loanService.ProcessMoneyFineLate();
            await Task.WhenAll(t2, t3, t4, t5, t6);
            _logger.LogInformation("JobHandleEveryOneMinuteTask end");
        }
    }
}
