﻿using Microsoft.Extensions.Logging;
using ScheduleTaskWorkerService.Scheduling;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace ScheduleTaskWorkerService.Services
{

    public class JobHandleEveryFiveMinuteTask : IScheduledTask
    {
        ILogger<JobHandleEveryFiveMinuteTask> _logger;
        RestClients.ICustomerService _customerService;
        RestClients.IInsuranceService _insuranceService;
        RestClients.ILoanService _loanService;
        AutoReviewSystem.IRunUpdateFeeFineOriginalService _runUpdateFineOriginalService;
        AutoReviewSystem.ICustomerManager _customerManager;
        RestClients.ICollectionService _collectionService;
        public JobHandleEveryFiveMinuteTask(
            RestClients.ICustomerService customerService,
            RestClients.IInsuranceService insuranceService,
            RestClients.ILoanService loanService,
            AutoReviewSystem.IRunUpdateFeeFineOriginalService runUpdateFineOriginalService,
            AutoReviewSystem.ICustomerManager customerManager,
            RestClients.ICollectionService collectionService,
            ILogger<JobHandleEveryFiveMinuteTask> logger)
        {
            _customerService = customerService;
            _insuranceService = insuranceService;
            _loanService = loanService;
            _runUpdateFineOriginalService = runUpdateFineOriginalService;
            _customerManager = customerManager;
            _collectionService = collectionService;
            _logger = logger;
        }
        public string Schedule => "0 0/5 * * * *";

        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("JobHandleEveryFiveMinuteTask start");
            var t1 = _customerService.RegisterVa();
            var t2 = _insuranceService.InformationInsuranceAI();
            var t3 = _collectionService.ProcessTranDataCallProxy();
            //var t3 = _runUpdateFineOriginalService.RunUpdateLoanExtraFromLos();
            await Task.WhenAll(t1, t2, t3);
            //await _customerManager.RunUpdateMoney();
            _logger.LogInformation("JobHandleEveryFiveMinuteTask end");
        }
    }
}
