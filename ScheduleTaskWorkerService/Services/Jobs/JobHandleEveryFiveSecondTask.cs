﻿using Microsoft.Extensions.Logging;
using ScheduleTaskWorkerService.Scheduling;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ScheduleTaskWorkerService.Services
{
    public class JobHandleEveryFiveSecondTask : IScheduledTask
    {
        ILogger<JobHandleEveryFiveSecondTask> _logger;
        RestClients.IOutGatewayService _outGatewayService;
        public JobHandleEveryFiveSecondTask(
            RestClients.IOutGatewayService outGatewayService,
            ILogger<JobHandleEveryFiveSecondTask> logger)
        {
            _outGatewayService = outGatewayService;
            _logger = logger;
        }
        public string Schedule => "0/5 * * * * *";
        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("JobHandleEveryFiveSecondTask start");
            var t1 = _outGatewayService.QueueCallAPIAG();
            await Task.WhenAll(t1);
            _logger.LogInformation("JobHandleEveryFiveSecondTask end");
        }
    }
}
