﻿using Microsoft.Extensions.Logging;
using ScheduleTaskWorkerService.Scheduling;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ScheduleTaskWorkerService.Services
{
    public class JobHandleEveryOneSecondTask : IScheduledTask
    {
        ILogger<JobHandleEveryOneSecondTask> _logger;
        RestClients.ILoanService _loanService;
        RestClients.IInsuranceService _insuranceService;
        public JobHandleEveryOneSecondTask(RestClients.ILoanService loanService,
            RestClients.IInsuranceService insuranceService,
            ILogger<JobHandleEveryOneSecondTask> logger)
        {
            _loanService = loanService;
            _insuranceService = insuranceService;
            _logger = logger;
        }
        public string Schedule => "0/2 * * * * *";

        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("JobHandleEveryOneSecondTask start");
            var t1 = _loanService.ProcessSmsOtpDisbursement();
            var t2 = _loanService.ProcessAutoDisbursementLoanCredit();
            var t3 = _loanService.ProcessSummaryBankCardFromInvoiceID();
            await Task.WhenAll(t1, t2, t3);
            _logger.LogInformation("JobHandleEveryOneSecondTask end");
        }
    }
}
