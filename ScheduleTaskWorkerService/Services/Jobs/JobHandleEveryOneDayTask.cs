﻿using Microsoft.Extensions.Logging;
using ScheduleTaskWorkerService.Scheduling;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ScheduleTaskWorkerService.Services
{
    public class JobHandleEveryOneDayTask : IScheduledTask
    {
        ILogger<JobHandleEveryOneDayTask> _logger;
        RestClients.ILoanService _loanService;
        RestClients.IInsuranceService _insuranceService;
        RestClients.ICustomerService _customerService;
        RestClients.ILenderService _lenderService;

        public JobHandleEveryOneDayTask(RestClients.ILoanService loanService,
            RestClients.IInsuranceService insuranceService,
            RestClients.ICustomerService customerService,
             RestClients.ILenderService lenderService,
            ILogger<JobHandleEveryOneDayTask> logger)
        {
            _loanService = loanService;
            _insuranceService = insuranceService;
            _customerService = customerService;
            _lenderService = lenderService;
            _logger = logger;
        }
        public string Schedule => "0 0 2 * * *";

        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("JobHandleEveryOneDayTask start");
            var t1 = _loanService.ProcessSummaryTransactionDate();
            var t2 = _customerService.UpdatePayType();
            var t3 = _loanService.ProcessCutOffLoanDaily();
            var t4 = _loanService.ProcessCalculatorMoneyCloseLoanDaily();
            var t5 = _lenderService.RunPaymentScheduleDepositMoney();
            await Task.WhenAll(t1, t2, t3, t4, t5);
            _logger.LogInformation("JobHandleEveryOneDayTask end");
        }
    }
}
