﻿using Microsoft.Extensions.Logging;
using ScheduleTaskWorkerService.Scheduling;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ScheduleTaskWorkerService.Services.Jobs
{
    public class JobHandleAtThreeAMTask : IScheduledTask
    {
        ILogger<JobHandleAtThreeAMTask> _logger;
        RestClients.IReportService _reportService;
        public JobHandleAtThreeAMTask(RestClients.IReportService reportService,
            ILogger<JobHandleAtThreeAMTask> logger)
        {
            _reportService = reportService;
            _logger = logger;
        }
        public string Schedule => "0 0 3 * * *";

        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("JobHandleAtThreeAMTask start");
            var t1 = _reportService.ProcessReportLoanDebtDaily();
            await Task.WhenAll(t1);
            _logger.LogInformation("JobHandleAtThreeAMTask end");
        }
    }
}
