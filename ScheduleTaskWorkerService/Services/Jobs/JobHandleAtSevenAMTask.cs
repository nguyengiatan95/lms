﻿using Microsoft.Extensions.Logging;
using ScheduleTaskWorkerService.Scheduling;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ScheduleTaskWorkerService.Services.Jobs
{
    public class JobHandleAtSevenAMTask : IScheduledTask
    {
        readonly ILogger<JobHandleAtSevenAMTask> _logger;
        readonly RestClients.ICollectionService _collectionService;
        public JobHandleAtSevenAMTask(RestClients.ICollectionService collectionService,
            ILogger<JobHandleAtSevenAMTask> logger)
        {
            _collectionService = collectionService;
            _logger = logger;
        }
        public string Schedule => "0 0 7 * * *";

        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("JobHandleAtSevenAMTask start");
            var t1 = _collectionService.PushPhoneDailyToCallCenter();
            await Task.WhenAll(t1);
            _logger.LogInformation("JobHandleAtSevenAMTask end");
        }
    }
}
