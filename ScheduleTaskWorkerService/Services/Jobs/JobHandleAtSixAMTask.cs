﻿using Microsoft.Extensions.Logging;
using ScheduleTaskWorkerService.Scheduling;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ScheduleTaskWorkerService.Services.Jobs
{
    public class JobHandleAtSixAMTask : IScheduledTask
    {
        readonly ILogger<JobHandleAtSixAMTask> _logger;
        readonly RestClients.ICollectionService _collectionService;
        public JobHandleAtSixAMTask(RestClients.ICollectionService collectionService,
            ILogger<JobHandleAtSixAMTask> logger)
        {
            _collectionService = collectionService;
            _logger = logger;
        }
        public string Schedule => "0 0 6 * * *";

        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("JobHandleAtSixAMTask start");
            var t1 = _collectionService.ProcessingThePhoneForDailyCall();
            var t2 = _collectionService.CancelLoanExemptionByExpired();
            await Task.WhenAll(t1, t2);
            _logger.LogInformation("JobHandleAtSixAMTask end");
        }
    }
}
