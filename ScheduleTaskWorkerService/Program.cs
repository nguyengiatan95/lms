using Confluent.Kafka;
using LMS.Kafka.Consumer;
using LMS.Kafka.Interfaces;
using LMS.Kafka.Producer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ScheduleTaskWorkerService.RestClients;
using ScheduleTaskWorkerService.Scheduling;
using ScheduleTaskWorkerService.Services;
using ScheduleTaskWorkerService.Services.Jobs;
using ScheduleTaskWorkerService.Services.KafkaEvent;
using Serilog;
using Steeltoe.Discovery.Client;
using Steeltoe.Discovery.Eureka;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
   
namespace ScheduleTaskWorkerService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal($"Failed to start {Assembly.GetExecutingAssembly().GetName().Name}", ex);
                throw;
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostContext, builder) =>
                {
                    var appseting = "appsettings.json";
                    if (!string.IsNullOrEmpty(hostContext.HostingEnvironment.EnvironmentName))
                    {
                        appseting = $"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json";
                    }
                    Console.WriteLine($"appsetting: {appseting}");
                    builder.AddJsonFile(appseting);
                })
                .ConfigureServices((hostContext, services) =>
                {
                    ConfigureServices(services, hostContext.Configuration);
                })
                .AddServiceDiscovery(options => options.UseEureka())
                .AddDiscoveryClient()
                .UseSerilog((hostingContext, loggerConfig) => loggerConfig.ReadFrom.Configuration(hostingContext.Configuration))
                ;
        public static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            var cronSetting = new Cron.CronSetting();
            configuration.GetSection(nameof(Cron.CronSetting)).Bind(cronSetting);
            services.AddSingleton(cronSetting);
            services.AddHttpContextAccessor();
            services.AddTransient<LMS.Common.Helper.IApiHelper, LMS.Common.Helper.ApiHelper>();
            services.AddTransient<IScheduledTask, JobHandleEveryOneMinuteTask>();
            services.AddTransient<IScheduledTask, JobHandleEveryOneDayTask>();
            services.AddTransient<IScheduledTask, JobHandleEveryFiveMinuteTask>();
            services.AddTransient<IScheduledTask, JobHandleEveryOneSecondTask>();
            services.AddTransient<IScheduledTask, JobHandleEveryFiveSecondTask>();
            services.AddTransient<IScheduledTask, JobHandleEveryHalfHoursTask>();
            services.AddTransient<IScheduledTask, JobHandleAtThreeAMTask>();
            services.AddTransient<IScheduledTask, JobHandleAtSixAMTask>();
            services.AddTransient<IScheduledTask, JobHandleAtSevenAMTask>();
            services.AddRestClientsService();
            services.AddHostedService<SchedulerHostedService>();
            LMS.Common.Configuration.ConnectStringSetting.DefaultConnectString = configuration["ConnectStringSetting:DefaultConnectString"];

            services.AddTransient<Services.AutoReviewSystem.IRunUpdateFeeFineOriginalService, Services.AutoReviewSystem.RunUpdateFeeFineOriginalService>();
            services.AddTransient<Services.AutoReviewSystem.ICustomerManager, Services.AutoReviewSystem.CustomerManager>();
            services.AddTransient(typeof(LMS.Common.DAL.ITableHelper<>), typeof(LMS.Common.DAL.TableHelper<>));
            AddServiceKafka(services, configuration);
        }
        public static void AddServiceKafka(IServiceCollection services, IConfiguration configuration)
        {
            var clientConfig = new ClientConfig()
            {
                BootstrapServers = configuration["Kafka:ClientConfigs:BootstrapServers"]
            };

            var producerConfig = new ProducerConfig(clientConfig);
            services.AddSingleton(producerConfig);
            services.AddHostedService<KakfaService>();
        }
    }
}
