﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.CollectionServices.ReportExcel;
using LMS.Entites.Dtos.Document;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Spire.Doc;
using System;
using System.IO;
using System.Threading.Tasks;

namespace FrontEnd.RecoveryOfLoans.Services
{
    public interface IDocumentManager
    {
        Task<ResponseActionResult> GetDataFile(int Type, string jsonData);
    }
    public class DocumentManager : IDocumentManager
    {
        private readonly IHostingEnvironment _environment;
        readonly ILogger<DocumentManager> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public DocumentManager(
         IHostingEnvironment environment,
         ILogger<DocumentManager> logger)
        {
            _environment = environment;
            _logger = logger;
            _common = new Utils();
        }
        public async Task<ResponseActionResult> PropertySeizure(string JsonData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                string filename = "TemplateQuyetdinhThugiutaisan.docx";
                Document doc = new Document();
                var toDay = DateTime.Now;
                var path = Path.Combine(_environment.WebRootPath, "Files/") + filename;
                doc.LoadFromFile(path);
                var data = new PropertySeizureItem();
                data = _common.ConvertJSonToObjectV2<PropertySeizureItem>(JsonData);
                doc.Replace("edit_day", toDay.Day.ToString(), true, true);
                doc.Replace("edit_moth", toDay.Month.ToString(), true, true);
                doc.Replace("edit_year", toDay.Year.ToString(), true, true);
                doc.Replace("EditCVTB", data.EditCVTB == null ? "" : data.EditCVTB, true, true);
                doc.Replace("Edit_FromDate", data.Edit_FromDate == null ? "" : data.Edit_FromDate, true, true);
                doc.Replace("Edit_CustomerName", data.Edit_CustomerName == null ? "" : data.Edit_CustomerName, true, true);
                doc.Replace("Edit_Product", data.Edit_Product == null ? "" : data.Edit_Product, true, true);
                doc.Replace("Edit_plateNumber", data.Edit_PlateNumber == null ? "" : data.Edit_PlateNumber, true, true);
                doc.Replace("Edit_NumberDayKeep", data.Edit_NumberDayKeep == null ? "" : data.Edit_NumberDayKeep, true, true);
                string filenew = $"QuyetdinhThugiutaisan-{data.FileName}.docx";
                var url = "/DownloadDocs/" + filenew;
                var fullPath = Path.Combine(_environment.WebRootPath, "DownloadDocs/") + filenew;
                doc.SaveToFile(fullPath);
                doc.Close();
                response.SetSucces();
                response.Data = url;
            }
            catch (Exception ex)
            {
                _logger.LogError($"DocumentManager_PropertySeizure|request={_common.ConvertObjectToJSonV2(JsonData)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task<ResponseActionResult> DebtRestructuring(string JsonData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                string filename = "TemplateTaiCauTrucNo.docx";
                Document doc = new Document();
                var data = new DebtRestructuringItem();
                var toDay = DateTime.Now;
                data = _common.ConvertJSonToObjectV2<DebtRestructuringItem>(JsonData);
                var path = Path.Combine(_environment.WebRootPath, "Files/") + filename;
                doc.LoadFromFile(path);

                doc.Replace("edit_day", toDay.Day.ToString(), true, true);
                doc.Replace("edit_month", toDay.Month.ToString(), true, true);
                doc.Replace("edit_year", toDay.Year.ToString(), true, true);
                doc.Replace("CustomerName", data.CustomerName == null ? "" : data.CustomerName, true, true);
                doc.Replace("NumberCard", data.NumberCard == null ? "" : data.NumberCard, true, true);
                doc.Replace("LoanCodeId", data.LoanCodeId == null ? "" : data.LoanCodeId, true, true);
                doc.Replace("TotalMoney", data.TotalMoney == null ? "" : data.TotalMoney, true, true);
                doc.Replace("TotalMoneyCurrent", data.TotalMoneyCurrent == null ? "" : data.TotalMoneyCurrent, true, true);
                doc.Replace("TotalMoneyInterest", data.TotalMoneyInterest == null ? "" : data.TotalMoneyInterest, true, true);
                doc.Replace("TotalMoneyCloseLoan", data.TotalMoneyCloseLoan == null ? "" : data.TotalMoneyCloseLoan, true, true);
                doc.Replace("BirthDay", data.BirthDay == null ? "" : data.BirthDay, true, true);
                doc.Replace("DebitGroup", data.DebitGroup == null ? "" : data.DebitGroup, true, true);
                doc.Replace("DayPastDue", data.DayPastDue.ToString() == null ? "" : data.DayPastDue.ToString(), true, true);
                doc.Replace("TotalMoneyConsultant", data.TotalMoneyConsultant == null ? "" : data.TotalMoneyConsultant, true, true);
                doc.Replace("TotalMoneyFineLate", data.TotalMoneyFineLate == null ? "" : data.TotalMoneyFineLate, true, true);
                doc.Replace("TotalMoneyFineOriginal", data.TotalMoneyFineOriginal == null ? "" : data.TotalMoneyFineOriginal, true, true);
                string filenew = $"TaiCauTrucNo-{data.FileName}.docx";
                var fullPath = Path.Combine(_environment.WebRootPath, "DownloadDocs/") + filenew;
                var url = "/DownloadDocs/" + filenew;
                doc.SaveToFile(fullPath);
                doc.Close();
                response.SetSucces();
                response.Data = url;
            }
            catch (Exception ex)
            {
                _logger.LogError($"DocumentManager_DebtRestructuring|request={_common.ConvertObjectToJSonV2(JsonData)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> UnsecuredFinalDebtNotice(string JsonData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                string filename = "TemplateThongbaonolancuoitinchap.docx";
                Document doc = new Document();
                var data = new UnsecuredFinalDebtNoticeItem();
                var toDay = DateTime.Now;
                data = _common.ConvertJSonToObjectV2<UnsecuredFinalDebtNoticeItem>(JsonData);
                var path = Path.Combine(_environment.WebRootPath, "Files/") + filename;
                doc.LoadFromFile(path);

                doc.Replace("edit_day", toDay.Day.ToString(), true, true);
                doc.Replace("edit_month", toDay.Month.ToString(), true, true);
                doc.Replace("edit_year", toDay.Year.ToString(), true, true);
                doc.Replace("EditCVTB", data.EditCVTB == null ? "" : data.EditCVTB, true, true);
                doc.Replace("Edit_CustomerName", data.Edit_CustomerName, true, true);
                doc.Replace("Edit_NumberCard", data.Edit_NumberCard == null ? "" : data.Edit_NumberCard, true, true);
                doc.Replace("Edit_Address", data.Edit_Address == null ? "" : data.Edit_Address, true, true);
                doc.Replace("Edit_LoanCode", data.Edit_LoanCode == null ? "" : data.Edit_LoanCode, true, true);
                doc.Replace("Edit_TotalMoneyNeedCloseLoan", data.Edit_TotalMoneyNeedCloseLoan == null ? "" : data.Edit_TotalMoneyNeedCloseLoan, true, true);
                doc.Replace("Edit_DateCloseLoan", data.Edit_DateCloseLoan == null ? "" : data.Edit_DateCloseLoan, true, true);
                doc.Replace("Edit_TotalMoneyCurrent", data.Edit_TotalMoneyCurrent == null ? "" : data.Edit_TotalMoneyCurrent, true, true);
                doc.Replace("Edit_TotalMoneyIntersetFee", data.Edit_TotalMoneyIntersetFee == null ? "" : data.Edit_TotalMoneyIntersetFee, true, true);
                doc.Replace("Edit_DPD", data.Edit_DPD.ToString() == null ? "" : data.Edit_DPD.ToString(), true, true);
                doc.Replace("Edit_DateFinishCloseLoan", data.Edit_DateFinishCloseLoan == null ? "" : data.Edit_DateFinishCloseLoan, true, true);
                doc.Replace("Edit_NameEmployees", data.Edit_NameEmployees == null ? "" : data.Edit_NameEmployees, true, true);
                doc.Replace("Edit_PhoneEmployees", data.Edit_PhoneEmployees == null ? "" : data.Edit_PhoneEmployees, true, true);
                string filenew = $"Thongbaonolancuoitinchap-{data.FileName}.docx";
                var url = "/DownloadDocs/" + filenew;
                var fullPath = Path.Combine(_environment.WebRootPath, "DownloadDocs/") + filenew;
                doc.SaveToFile(fullPath);
                doc.Close();
                response.SetSucces();
                response.Data = url;
            }
            catch (Exception ex)
            {
                _logger.LogError($"DocumentManager_UnsecuredFinalDebtNotice|request={_common.ConvertObjectToJSonV2(JsonData)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task<ResponseActionResult> NotifyPropertySeizure(string JsonData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                string filename = "TemplateThongbaothugiutaisan.docx";
                Document doc = new Document();
                var data = new NotifyPropertySeizureItem();
                var toDay = DateTime.Now;
                data = _common.ConvertJSonToObjectV2<NotifyPropertySeizureItem>(JsonData);
                var path = Path.Combine(_environment.WebRootPath, "Files/") + filename;
                doc.LoadFromFile(path);

                doc.Replace("edit_day", toDay.Day.ToString(), true, true);
                doc.Replace("edit_month", toDay.Month.ToString(), true, true);
                doc.Replace("edit_year", toDay.Year.ToString(), true, true);
                doc.Replace("EditCVTB", data.EditCVTB, true, true);
                doc.Replace("Edit_CustomerName", data.Edit_CustomerName, true, true);
                doc.Replace("Edit_NumberCard", data.Edit_NumberCard == null ? "" : data.Edit_NumberCard, true, true);
                doc.Replace("Edit_Address", data.Edit_Address == null ? "" : data.Edit_Address, true, true);
                doc.Replace("Edit_Product", data.Edit_Product == null ? "" : data.Edit_Product, true, true);
                doc.Replace("Edit_Engine", data.Edit_Engine == null ? "" : data.Edit_Engine, true, true);
                doc.Replace("Edit_Chassis", data.Edit_Chassis == null ? "" : data.Edit_Chassis, true, true);
                doc.Replace("Edit_FromDate", data.Edit_FromDate == null ? "" : data.Edit_FromDate, true, true);
                doc.Replace("Edit_NameEmployees", data.Edit_NameEmployees == null ? "" : data.Edit_NameEmployees, true, true);
                doc.Replace("Edit_PhoneEmployees", data.Edit_PhoneEmployees == null ? "" : data.Edit_PhoneEmployees, true, true);
                string filenew = $"Thongbaothugiutaisan-{data.FileName}.docx";
                var url = "/DownloadDocs/" + filenew;
                var fullPath = Path.Combine(_environment.WebRootPath, "DownloadDocs/") + filenew;
                doc.SaveToFile(fullPath);
                doc.Close();
                response.SetSucces();
                response.Data = url;
            }
            catch (Exception ex)
            {
                _logger.LogError($"DocumentManager_NotifyPropertySeizure|request={_common.ConvertObjectToJSonV2(JsonData)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> NotifyVoluntarilyHandingOverPropertye(string JsonData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                string filename = "TemplateThongbaotunguyenbangiaotaisan.docx";
                Document doc = new Document();
                var data = new NotifyVoluntarilyHandingOverPropertyeItem();
                var toDay = DateTime.Now;
                data = _common.ConvertJSonToObjectV2<NotifyVoluntarilyHandingOverPropertyeItem>(JsonData);
                var path = Path.Combine(_environment.WebRootPath, "Files/") + filename;
                doc.LoadFromFile(path);

                doc.Replace("edit_day", toDay.Day.ToString(), true, true);
                doc.Replace("edit_month", toDay.Month.ToString(), true, true);
                doc.Replace("edit_year", toDay.Year.ToString(), true, true);
                doc.Replace("EditCVTB", data.EditCVTB, true, true);
                doc.Replace("Edit_CustomerName", data.Edit_CustomerName, true, true);
                doc.Replace("Edit_NumberCard", data.Edit_NumberCard == null ? "" : data.Edit_NumberCard, true, true);
                doc.Replace("Edit_Address", data.Edit_Address == null ? "" : data.Edit_Address, true, true);
                doc.Replace("Edit_FromDate", data.Edit_FromDate == null ? "" : data.Edit_FromDate, true, true);
                doc.Replace("Edit_NameEmployees", data.Edit_NameEmployees == null ? "" : data.Edit_NameEmployees, true, true);
                doc.Replace("Edit_PhoneEmployees", data.Edit_PhoneEmployees == null ? "" : data.Edit_PhoneEmployees, true, true);

                doc.Replace("Edit_TotalMoneyNeedCloseLoan", data.Edit_TotalMoneyNeedCloseLoan == null ? "" : data.Edit_TotalMoneyNeedCloseLoan, true, true);
                doc.Replace("Edit_DateCloseLoan", data.Edit_DateCloseLoan == null ? "" : data.Edit_DateCloseLoan, true, true);
                doc.Replace("Edit_TotalMoneyCurrent", data.Edit_TotalMoneyCurrent == null ? "" : data.Edit_TotalMoneyCurrent, true, true);

                doc.Replace("Edit_TotalMoneyIntersetFee ", data.Edit_TotalMoneyIntersetFee == null ? "" : data.Edit_TotalMoneyIntersetFee, true, true);
                doc.Replace("Edit_TotalMoneyNeedCloseLoan", data.Edit_TotalMoneyNeedCloseLoan == null ? "" : data.Edit_TotalMoneyNeedCloseLoan, true, true);
                doc.Replace("Edit_DPD", data.Edit_DPD.ToString(), true, true);

                string filenew = $"Thongbaotunguyenbangiaotaisan-{data.FileName}.docx";
                var url = "/DownloadDocs/" + filenew;
                var fullPath = Path.Combine(_environment.WebRootPath, "DownloadDocs/") + filenew;
                doc.SaveToFile(fullPath);
                doc.Close();
                response.SetSucces();
                response.Data = url;
            }
            catch (Exception ex)
            {
                _logger.LogError($"DocumentManager_NotifyVoluntarilyHandingOverPropertye|request={_common.ConvertObjectToJSonV2(JsonData)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> MGL(string JsonData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                string filename = "TeamPlaceMGL.docx";
                Document doc = new Document();
                var data = new ReportFormLoanExemptionInterestDetail();
                var toDay = DateTime.Now;
                data = _common.ConvertJSonToObjectV2<ReportFormLoanExemptionInterestDetail>(JsonData);
                var path = Path.Combine(_environment.WebRootPath, "Files/") + filename;
                doc.LoadFromFile(path);

                doc.Replace("Edit_day", toDay.Day.ToString(), true, true);
                doc.Replace("Edit_month", toDay.Month.ToString(), true, true);
                doc.Replace("Edit_year", toDay.Year.ToString(), true, true);


                doc.Replace("Edit_tinhdenngay", data.CutOffDate == DateTime.MinValue ? "" : data.CutOffDate.ToString(TimaSettingConstant.DateTimeDayMonthYear), true, true);
                doc.Replace("Edit_tenkhachhang", data.CustomerName == null ? "" : data.CustomerName, true, true);
                doc.Replace("Edit_maTC", data.LoanContractCode == null ? "" : data.LoanContractCode, true, true);
                doc.Replace("Edit_ngaygiaingan", data.LoanFromDate == DateTime.MinValue ? "" : data.LoanFromDate.ToString(TimaSettingConstant.DateTimeDayMonthYear), true, true);
                doc.Replace("Edit_bookno", data.BadDebtYear.ToString(), true, true);
                doc.Replace("Edit_ngayketthuc", data.LoanToDate == DateTime.MinValue ? "" : data.LoanToDate.ToString(TimaSettingConstant.DateTimeDayMonthYear), true, true);
                doc.Replace("Edit_DPD", data.CountDPD.ToString(), true, true);
                doc.Replace("Edit_nhomsanpham", data.LoanProductName == null ? "" : data.LoanProductName, true, true);
                doc.Replace("Edit_sotiengiaingan", data.LoanTotalMoneyDisbrusement.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_sotiendathu", data.LoanTotalMoneyReceived.ToString(TimaSettingConstant.FormatMoney), true, true);

                doc.Replace("Edit_tnvpt", data.MoneyCloseLoan ==null ? "" : data.MoneyCloseLoan.TotalMoneyNeedPay.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_sttdk", data.MoneyExpected ==null ? "" : data.MoneyExpected.TotalMoneyNeedPay.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_stdxmg", data.MoneySuggetExemption ==null ? "" : data.MoneySuggetExemption.TotalMoneyNeedPay.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_tlmg", "", true, true);

                doc.Replace("Edit_G_tnvpt", data.MoneyCloseLoan.MoneyOriginal == 0 ? "" : data.MoneyCloseLoan.MoneyOriginal.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_G_sttdk", data.MoneyExpected.MoneyOriginal ==0 ? "" : data.MoneyExpected.MoneyOriginal.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_G_stdxmg", data.MoneySuggetExemption.MoneyOriginal ==0 ? "" : data.MoneySuggetExemption.MoneyOriginal.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_P_tlmg", data.MoneyPercent.MoneyOriginal ==0 ? "" : $"{data.MoneyPercent.MoneyOriginal.ToString(TimaSettingConstant.FormatMoney)}%", true, true);

                doc.Replace("Edit_L_tnvpt", data.MoneyCloseLoan.MoneyInterest ==0 ? "" : data.MoneyCloseLoan.MoneyInterest.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_L_sttdk", data.MoneyExpected.MoneyInterest ==0 ? "" : data.MoneyExpected.MoneyInterest.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_L_stdxmg", data.MoneySuggetExemption.MoneyInterest ==0 ? "" : data.MoneySuggetExemption.MoneyInterest.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_L_tlmg", data.MoneyPercent.MoneyInterest ==0 ? "" : $"{data.MoneyPercent.MoneyInterest.ToString(TimaSettingConstant.FormatMoney)}%", true, true);

                doc.Replace("Edit_NC_tnvpt", data.MoneyCloseLoan.MoneyOldDebit ==0 ? "" : data.MoneyCloseLoan.MoneyOldDebit.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_NC_sttdk", data.MoneyExpected.MoneyOldDebit ==0 ? "" : data.MoneyExpected.MoneyOldDebit.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_NC_stdxmg", data.MoneySuggetExemption.MoneyOldDebit ==0 ? "" : data.MoneySuggetExemption.MoneyOldDebit.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_NC_tlmg", data.MoneyPercent.MoneyOldDebit ==0 ? "" : $"{data.MoneyPercent.MoneyOldDebit.ToString(TimaSettingConstant.FormatMoney)}%", true, true);

                doc.Replace("Edit_P_tnvpt", data.MoneyCloseLoan.MoneyOldDebit ==0 ? "" : data.MoneyCloseLoan.TotalFee.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_P_sttdk", data.MoneyExpected.MoneyOldDebit ==0 ? "" : data.MoneyExpected.TotalFee.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_P_stdxmg", data.MoneySuggetExemption.MoneyOldDebit ==0 ? "" : data.MoneySuggetExemption.TotalFee.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_P_tlmg", "", true, true);

                doc.Replace("Edit_ptv_tnvpt", data.MoneyCloseLoan.MoneyConsultant ==0 ? "" : data.MoneyCloseLoan.MoneyConsultant.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_ptv_sttdk", data.MoneyExpected.MoneyConsultant ==0 ? "" : data.MoneyExpected.MoneyConsultant.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_ptv_stdxmg", data.MoneySuggetExemption.MoneyConsultant ==0 ? "" : data.MoneySuggetExemption.MoneyConsultant.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_ptv_tlmg", data.MoneyPercent.MoneyConsultant ==0 ? "" : $"{data.MoneyPercent.MoneyConsultant.ToString(TimaSettingConstant.FormatMoney)}%", true, true);

                doc.Replace("Edit_pdv_tnvpt", data.MoneyCloseLoan.MoneyService ==0 ? "" : data.MoneyCloseLoan.MoneyService.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_pdv_sttdk", data.MoneyExpected.MoneyService ==0 ? "" : data.MoneyExpected.MoneyService.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_pdv_stdxmg", data.MoneySuggetExemption.MoneyService ==0 ? "" : data.MoneySuggetExemption.MoneyService.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_pdv_tlmg", data.MoneyPercent.MoneyService ==0 ? "" : $"{data.MoneyPercent.MoneyService.ToString(TimaSettingConstant.FormatMoney)}%", true, true);

                doc.Replace("Edit_ppt_tnvpt", data.MoneyCloseLoan.MoneyFineLate ==0 ? "" : data.MoneyCloseLoan.MoneyFineLate.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_ppt_sttdk", data.MoneyExpected.MoneyFineLate ==0 ? "" : data.MoneyExpected.MoneyFineLate.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_ppt_stdxmg", data.MoneySuggetExemption.MoneyFineLate ==0 ? "" : data.MoneySuggetExemption.MoneyFineLate.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_ppt_tlmg", data.MoneyPercent.MoneyFineLate ==0 ? "" : $"{data.MoneyPercent.MoneyFineLate.ToString(TimaSettingConstant.FormatMoney)}%", true, true);

                doc.Replace("Edit_pttth_tnvpt", data.MoneyCloseLoan.MoneyFineOriginal ==0 ? "" : data.MoneyCloseLoan.MoneyFineOriginal.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_pttth_sttdk", data.MoneyExpected.MoneyFineOriginal ==0 ? "" : data.MoneyExpected.MoneyFineOriginal.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_pttth_stdxmg", data.MoneySuggetExemption.MoneyFineOriginal ==0 ? "" : data.MoneySuggetExemption.MoneyFineOriginal.ToString(TimaSettingConstant.FormatMoney), true, true);
                doc.Replace("Edit_pttth_tlmg", data.MoneyPercent.MoneyFineOriginal ==0 ? "" : $"{data.MoneyPercent.MoneyFineOriginal.ToString(TimaSettingConstant.FormatMoney)}%", true, true);

                doc.Replace("Edit_team", data.DepartmentName ==null ? "" : data.DepartmentName, true, true);
                doc.Replace("Edit_ngayhieuluc", data.ExpirationDate == DateTime.MinValue ? "" : data.ExpirationDate.ToString(TimaSettingConstant.DateTimeDayMonthYear), true, true);
                doc.Replace("Edit_tencap", data.ApprovedByName ==null ? "" : data.ApprovedByName, true, true);
                doc.Replace("Edit_note", data.Note ==null ? "" : data.Note, true, true);

                string filenew = $"MGL-{data.LoanContractCode}.docx";
                var url = "/DownloadDocs/" + filenew;
                var fullPath = Path.Combine(_environment.WebRootPath, "DownloadDocs/") + filenew;
                doc.SaveToFile(fullPath);
                doc.Close();
                response.SetSucces();
                response.Data = url;
            }
            catch (Exception ex)
            {
                _logger.LogError($"DocumentManager_MGL|request={_common.ConvertObjectToJSonV2(JsonData)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> GetDataFile(int Type, string jsonData)
        {
            var response = new ResponseActionResult();
            string subPath = "DownloadDocs";

            var path = Path.Combine(_environment.WebRootPath, subPath);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string[] files = Directory.GetFiles(path);
            foreach (string file in files)
            {
                File.Delete(file);

            }
            switch ((ExcelReport_TypeReport)Type)
            {
                case ExcelReport_TypeReport.ExemptionInterest:
                    response = await MGL(jsonData);
                    break;
                case ExcelReport_TypeReport.PropertySeizure:
                    response = await PropertySeizure(jsonData);
                    break;
                case ExcelReport_TypeReport.DebtRestructuring:
                    response = await DebtRestructuring(jsonData);
                    break;
                case ExcelReport_TypeReport.UnsecuredFinalDebtNotice:
                    response = await UnsecuredFinalDebtNotice(jsonData);
                    break;
                case ExcelReport_TypeReport.NotifyPropertySeizure:
                    response = await NotifyPropertySeizure(jsonData);
                    break;
                case ExcelReport_TypeReport.NotifyVoluntarilyHandingOverPropertye:
                    response = await NotifyVoluntarilyHandingOverPropertye(jsonData);
                    break;
            }
            return response;
        }
    }
}
