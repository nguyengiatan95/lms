using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Services.Excel;
using FrontEnd.Common.Services.Insurance;
using FrontEnd.Common.Services.Login;
using FrontEnd.Common.Services.Upload;
using FrontEnd.Common.Services.User;
using FrontEnd.RecoveryOfLoans.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace FrontEnd.RecoveryOfLoans
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<KestrelServerOptions>(Configuration.GetSection("Kestrel"));
            services.Configure<FrontEnd.Common.Helpers.ServiceHost>(Configuration.GetSection("ServiceHost"));
            services.AddControllersWithViews();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromHours(12);
                options.Cookie.IsEssential = true;
                options.Cookie.Name = "LMS_Acounttant";
            });
            services.AddHttpClient();
            services.AddTransient<ILoginService, LoginService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IUploadServices, UploadServices>();
            services.AddTransient<FrontEnd.Common.Services.Excel.IExcelService, FrontEnd.Common.Services.Excel.ExcelService>();
            services.AddTransient<IInsuranceServices, InsuranceService>();
            services.AddTransient<IExcelExportData, ExcelExportDataLoan>();
            services.AddTransient<IExcelExportData, ExcelHistoryCustomerTopup>();
            services.AddTransient<IExcelExportData, ExcelHistoryTransactionLoanByCustomer>();
            services.AddTransient<IExcelExportData, ExcelReportLoanDebtType>();
            services.AddTransient<IExcelExportData, ExcelReportLoanDebtDaily>();
            services.AddTransient<IExcelExportData, ExcelReportDayPlan>();
            services.AddTransient<IExcelExportData, ExcelHistoryInteraction>();
            services.AddTransient<IExcelExportData, ExcelLenderDeposit>();
            services.AddTransient<IDocumentManager, DocumentManager>();
            Constants.STATIC_USERMODEL = Configuration["Configuration:CookieName"].ToString();
            Constants.STATIC_CALL_INFO_COOKIE = Configuration["Configuration:STATIC_CALL_INFO_COOKIE"].ToString();
            Constants.STATIC_VERSION = $"{DateTime.Now.Ticks}";// Configuration["Configuration:JsVersion"].ToString();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                // app.UseHsts();
            }
            // app.UseHttpsRedirection();
            app.UseSession();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
