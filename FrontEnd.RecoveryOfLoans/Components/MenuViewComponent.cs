﻿using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models.Management.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEnd.RecoveryOfLoans.Components
{
    public class MenuViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var userData = new UserInfoWithMenuModel();
            try
            {
                userData = await HttpContext.Session.GetObjectFromJson<UserInfoWithMenuModel>(Constants.STATIC_USERMODEL);
            }
            catch(Exception)
            { 
            } 
            return View(userData);
        }
    }
}
