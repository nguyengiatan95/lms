﻿using FrontEnd.Common.Helpers;
using FrontEnd.Common.Services.Excel;
using FrontEnd.Common.Services.Insurance;
using FrontEnd.RecoveryOfLoans.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEnd.RecoveryOfLoans.Controllers
{
    public class ExcelController : BaseController
    {
        private readonly ILogger<ExcelController> _logger;
        private IExcelService _excelService;
        private IEnumerable<IExcelExportData> _excelExportData;
        private Common.Helpers.Common _common;
        private IDocumentManager _documentManager;
        private IInsuranceServices _insuranceServices;
        public ExcelController(ILogger<ExcelController> logger,
            IConfiguration configution, IExcelService excelService,
            IInsuranceServices insuranceServices,
            IDocumentManager documentManager,
            IEnumerable<IExcelExportData> excelExportData) : base(configution)
        {
            _excelService = excelService;
            _logger = logger;
            _documentManager = documentManager;
            _insuranceServices = insuranceServices;
            _excelExportData = excelExportData;
            _common = new Common.Helpers.Common();
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<ActionResult> ExcelLoanTHN(string TraceID, int CreateBy)
        {
            Common.Models.ThnLoan.GetByTraceIDModel getByTraceIDModel = new Common.Models.ThnLoan.GetByTraceIDModel()
            {
                CreateBy = CreateBy,
                TraceIDRequest = TraceID,
            };
            var responseDataApi = await _insuranceServices.GetByTraceIDTHN(GetToken(), getByTraceIDModel);
            Common.Models.ResponseActionResult resultAction = new Common.Models.ResponseActionResult()
            {
                Result = -1
            };
            // -1 call lại api, 0: lỗi, 1: có data export
            // không check null vì khởi tạo đã set mặc định
            if (responseDataApi.Result == 1)
            {
                resultAction.Result = 0;
                resultAction.Message = responseDataApi.Message;
                if (responseDataApi.Data != null)
                {
                    resultAction.Result = 1;
                    HttpContext.Session.SetObjectAsJson($"{Constants.STATIC_EXCEL_THN}-{TraceID}-{CreateBy}", responseDataApi.Data);
                }
            }
            return Json(new { Data = resultAction });
        }
        public async Task<ActionResult> ExcelExportLoanTHN(string TraceID, int CreateBy, int reportType = 1)
        {
            List<Common.Entites.ThnLoan.ExcelGetByTraceID> lstData = new List<Common.Entites.ThnLoan.ExcelGetByTraceID>();
            byte[] fileContents = null;
            try
            {
                var excelExportDetail = _excelExportData.FirstOrDefault(x => x.TypeReport == reportType);
                if (excelExportDetail != null)
                {
                    var lstDataExcel = await HttpContext.Session.GetObjectFromJson<object>($"{Constants.STATIC_EXCEL_THN}-{TraceID}-{CreateBy}");
                    string jsonData = _common.ConvertObjectToJSon(lstDataExcel);
                    fileContents = excelExportDetail.GetDataExcelReport(jsonData);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ExcelExportLoanTHN|ex={ex.Message}-{ex.StackTrace}");
            }
            return File(fileContents: fileContents,
                      contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                      fileDownloadName: $"{((LMS.Common.Constants.ExcelReport_TypeReport)reportType).GetDescription()}-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx"
                  );
        }
        public async Task<ActionResult> ExcelInteraction(int UserID, string FromDate, string ToDate, string KeySearch)
        {
            Common.Models.ThnLoan.RequestInteractionModel interactionModel = new Common.Models.ThnLoan.RequestInteractionModel()
            {
                KeySearch = KeySearch == null ? "" : KeySearch,
                FromDate = FromDate,
                ToDate = ToDate,
                UserID = UserID,
                PageIndex = 1,
                PageSize = 1000000,
            };
            var lstReport = await _insuranceServices.GetByConditionsInteraction(GetToken(), interactionModel);
            List<Common.Models.ThnLoan.ExcelInteractionResponse> lstData = new List<Common.Models.ThnLoan.ExcelInteractionResponse>();
            foreach (var item in lstReport)
            {
                lstData.Add(new Common.Models.ThnLoan.ExcelInteractionResponse()
                {
                    LoanCreditID = item.LoanCreditID.ToString(),
                    LoanID = item.LoanID.ToString(),
                    AppointmentDate = item.AppointmentDate,
                    Comment = item.Comment,
                    ContacCode = item.ContacCode,
                    CreateDateComment = item.CreateDateComment,
                    CustomerName = item.CustomerName,
                    DebitReminderCode = item.DebitReminderCode,
                    UserFullName = item.UserFullName,
                    UserName = item.UserName
                });
            }
            var fileContents = _excelService.ExportExcelBase<Common.Models.ThnLoan.ExcelInteractionResponse>(lstData);
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: $"Báo cáo Lịch sử tương tác-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx"
            );
        }
        public IActionResult SendSMS()
        {
            return View();
        }
        public async Task<ActionResult> DownLoadFileDoc(string TraceID, int CreateBy, int TypeDoc)
        {
            Common.Models.ResponseActionResult resultAction = new Common.Models.ResponseActionResult();
            try
            {

                Common.Models.ThnLoan.GetByTraceIDModel getByTraceIDModel = new Common.Models.ThnLoan.GetByTraceIDModel()
                {
                    CreateBy = CreateBy,
                    TraceIDRequest = TraceID,
                };
                var responseDataApi = await _insuranceServices.GetByTraceIDTHN(GetToken(), getByTraceIDModel);
                if (responseDataApi.Result == 1)
                {
                    resultAction.Result = 0;
                    resultAction.Message = responseDataApi.Message;
                    if (responseDataApi.Data != null)
                    {
                        var data = await _documentManager.GetDataFile(TypeDoc, responseDataApi.Data.ToString());
                        return Json(new { Data = data });
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"DownLoadFileDoc|ex={ex.Message}-{ex.StackTrace}");
            }
            return Json(new { Data = resultAction });
        }
    }
}
