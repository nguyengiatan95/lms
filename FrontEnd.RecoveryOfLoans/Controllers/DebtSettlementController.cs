﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.RecoveryOfLoans.Controllers
{
    public class DebtSettlementController : BaseController
    {
        public DebtSettlementController(IConfiguration configution) : base(configution)
        {
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult DebtSettlementLeader()
        {
            return View();
        }
        public IActionResult DebtSettlementManager()
        {
            return View();
        }
    }
}