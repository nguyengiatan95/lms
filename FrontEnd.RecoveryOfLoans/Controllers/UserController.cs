﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models.Management.User;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.RecoveryOfLoans.Controllers
{
    public class UserController : BaseController
    {
        public UserController(IConfiguration configution) : base(configution)
        {
        }
        public async Task<IActionResult> ChangePassword(int t = 0)
        {
            var user = await HttpContext.Session.GetObjectFromJson<UserInfoWithMenuModel>(Constants.STATIC_USERMODEL);
            ViewBag.Active = t;
            return View(user);
        }
    }
}