﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace FrontEnd.RecoveryOfLoans.Controllers
{
    public class ReportController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult ReportByDebtGroup()
        {
            return View();
        }
        public IActionResult ReportDebtDay()
        {
            return View();
        }
        public IActionResult ReportDayPlan()
        {
            return View();
        }
    }
}