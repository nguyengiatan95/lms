﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using FrontEnd.RecoveryOfLoans.Models;
using Microsoft.Extensions.Configuration;
using FrontEnd.Common.Services.Login;
using FrontEnd.Common.Models.Management.User;
using FrontEnd.Common.Helpers;

namespace FrontEnd.RecoveryOfLoans.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;
        private ILoginService loginService;
        public HomeController(ILogger<HomeController> logger, IConfiguration configuration, ILoginService _loginService)
           : base(configuration)
        {
            _logger = logger;
            loginService = _loginService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult RefreshToken()
        {  
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> GetRefreshToken()
        {
            // check nếu chưa đến hạn 
            var dataToken = new Common.Models.ResponseActionResult<UserInfoWithMenuModel>();
            try
            {
                var value = await HttpContext.Session.GetObjectFromJson<UserInfoWithMenuModel>(Constants.STATIC_USERMODEL);
                if ((DateTime.Now - value.TimeExpired).TotalMinutes < 5)
                {
                    dataToken = await loginService.RefreshToken(GetToken());
                    value.Token = dataToken.Data.Token;
                    value.TimeExpired = dataToken.Data.TimeExpired;
                    value.TimeExpiredString = value.TimeExpired.ToString("dd/MM/yyyy HH:mm");
                    HttpContext.Session.SetObjectAsJson(Constants.STATIC_USERMODEL, value);
                    return Json(new { status = 1, data = value });
                }
                return Json(new { status = 0 });
            }
            catch (Exception)
            {
                return Json(new { status = 0 });
            }

        }
    }
}
