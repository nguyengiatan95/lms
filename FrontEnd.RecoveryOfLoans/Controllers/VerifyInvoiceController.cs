﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Entites.Upload;
using FrontEnd.Common.Services.Upload;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.RecoveryOfLoans.Controllers
{
    public class VerifyInvoiceController : BaseController
    {
        IUploadServices _uploadServices;
        public VerifyInvoiceController(IConfiguration configuration, IUploadServices uploadServices) : base(configuration)
        {
            _uploadServices = uploadServices;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> UploadImage(List<IFormFile> files)
        {
            var response = await _uploadServices.UploadImageToLos(files);
            return Json(response);
        }
        [HttpPost]
        public async Task<IActionResult> DeleteImage(string FifleName)
        {
            var response = await _uploadServices.DeleteImageToLos(FifleName);
            return Json(response);
        }
    }
}
