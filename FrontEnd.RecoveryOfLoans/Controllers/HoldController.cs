﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.RecoveryOfLoans.Controllers
{
    public class HoldController : BaseController
    {
        public HoldController(IConfiguration configution) : base(configution)
        {
        }
        public IActionResult Index()
        {
            return View();
        }
    }
}