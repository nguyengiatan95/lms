﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models.Management.Login;
using FrontEnd.Common.Models.Management.User;
using FrontEnd.Common.Services.Login;
using FrontEnd.Common.Services.User;
using LMS.Common.Constants;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace FrontEnd.RecoveryOfLoans.Controllers
{
    public class LoginController : BaseController
    {
        private ILoginService _loginService;
        private IUserService _user;
        public LoginController(IConfiguration configuration, ILoginService loginService, IUserService user) : base(configuration)
        {
            _loginService = loginService;
            _user = user;
        }
        public IActionResult Index()
        {
            return View();
        }
        [Route("logout.html")]
        public IActionResult Logout()
        {
            HttpContext.Session.SetObjectAsJson(Constants.STATIC_USERMODEL, null);
            Response.Cookies.Delete(Constants.STATIC_USERMODEL);
            Response.Cookies.Delete(Constants.STATIC_CALL_INFO_COOKIE);
            return Redirect(_baseConfig[Constants.STATIC_LogOut_Admin_URL]);
        }
        [Route("redirect")]
        [HttpGet]
        public async Task<IActionResult> RedirectIndex(string token, string referrer)
        {
            try
            {
                //Validate Token
                if (string.IsNullOrEmpty(token))
                    return Redirect(_baseConfig[Constants.STATIC_Login_URL]);
                var user = await _user.GetUserByUserID(token, 1);
                if (user == null || user.UserID == 0)
                    return Redirect(_baseConfig[Constants.STATIC_Login_URL]);
                var userLoginModel = new UserLoginModel { UserID = user.UserID, UserName = user.UserName, FullName = user.FullName, Token = token };
                userLoginModel.FullName = user.FullName;
                userLoginModel.UserName = user.UserName;
                userLoginModel.ApiUrl = _baseConfig[Constants.STATIC_ApiUrl_URL];
                userLoginModel.LoginUrl = _baseConfig[Constants.STATIC_Login_URL];
                userLoginModel.AdminUrl = _baseConfig[Constants.STATIC_HOME_URL];

                // gọi API lấy menu
                var userModel = new GetUserInfoWithMenuQuery { AppID = (int)Menu_AppID.THN };
                var responce = _loginService.GetUserInfoWithMenu(userModel, token).Result;
                if (responce != null && responce.Data != null && responce.Data.UserID > 0)
                {
                    // check điều hướng Referrer
                    responce.Data.Token = token;
                    HttpContext.Session.SetObjectAsJson(Constants.STATIC_USERMODEL, responce.Data);
                    SetCookie(Constants.STATIC_USERMODEL, JsonConvert.SerializeObject(userLoginModel), 24);
                    if (!string.IsNullOrEmpty(referrer))
                    {
                        return Redirect(referrer);
                    }
                    else
                    {
                        return Redirect(Constants.STATIC_HOME_ACC);
                    }
                }
                return Redirect(_baseConfig[Constants.STATIC_Login_URL]);
            }
            catch (Exception ex)
            {

            }
            return Redirect(_baseConfig[Constants.STATIC_Login_URL]);
        }
    }
}
