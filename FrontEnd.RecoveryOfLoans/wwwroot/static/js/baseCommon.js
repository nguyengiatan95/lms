﻿var isClearSession = false;
var access_token = "";
var USER_ID = 0;
var USER_NAME = "";
var AdminUrl = '';
var DomainCookie = '';
var DEPARMENT_ID = 0;
var POSTION = 0;
var PARENTID = 0;
var baseCommon = new function () {
    var StatusInDepartment = {
        THN: 0,
        Hub: 1
    };
    var Option = {
        All: -111,
        Pagesize: 10,
        PagesizeExcel: 100000
    };
    var VerifyInvoice = {
        DeleteInvoice: 1,
        ReturnInvoiceConsiderCustomer: 2,
        VerifyInvoiceConsiderCustomer: 3
    };
    var BaseUrl, Endpoint, UserLoginModel = {}, isCalledRefreshToken = "", LoginUrl_ = LoginUrl, _Token;
    var SetBase = function () {
        BaseUrl = {
            "Admin": ApiUrl + "admin/",
            "Loan": ApiUrl + "loan/",
            /* "Loan": " http://localhost:56587/",*/
            "Authen": ApiUrl + "authen/",
            "Invoice": ApiUrl + "invoice/",
            "Lender": ApiUrl + "lender/",
            "Customer": ApiUrl + "customer/",
            "Transaction": ApiUrl + "transaction/",
            "Insurance": ApiUrl + "insurance/",
            "Thn": ApiUrl + "thn/",
            /*  "Thn": " http://localhost:57585/",*/
            "Thn_Tima": "https://thn.tima.vn/",
            "Ag_Tima": "https://api.tima.com.vn/"
        };
        //BaseUrl = {
        //    "Admin": ApiUrl + "admin/",
        //    "Loan": ApiUrl + "loan/",
        //    "Authen": ApiUrl + "authen/",
        //    "Invoice": ApiUrl + "invoice/",
        //    "Lender": ApiUrl + "lender/",
        //    "Customer": ApiUrl + "customer/",
        //    "Transaction": ApiUrl + "transaction/",
        //    "Insurance": ApiUrl + "insurance/",
        //    "Thn": "http://localhost:57585/",
        //    "Thn_Tima": "https://thn.tima.vn/",
        //    "Ag_Tima": "https://api.tima.com.vn/"
        //};
        Endpoint = {
            "GetLoanByID": BaseUrl["Loan"] + "api/Loan/getLoanByID/",
            //"GetLstPaymentScheduleByLoanID": BaseUrl["Loan"] + "api/loan/GetLstPaymentScheduleByLoanID/",  
            "GetLstLoanInfoByCondition": BaseUrl["Thn"] + "api/loan/GetLstLoanInfoByCondition",
            //"CreatePreparationCollectionLoan": BaseUrl["Thn"] + "api/PreparationCollectionLoan/CreatePreparationCollectionLoan",
            "HistoryComment": BaseUrl["Thn"] + "api/THN/HistoryComment",
            "SuggestedAmountLoan": BaseUrl["Thn"] + "api/PreparationCollectionLoan/SuggestedAmountLoan",
            "CreatePreparationCollectionLoan": BaseUrl["Thn"] + "api/PreparationCollectionLoan/CreatePreparationCollectionLoan",
            "GetPreparationCollectionLoan": BaseUrl["Thn"] + "api/PreparationCollectionLoan/GetPreparationCollectionLoan",
            "GetBankCard": BaseUrl["Loan"] + "api/Meta/GetBankCard?BankCardID=",
            "GetInvoiceInfosByCondition": BaseUrl["Invoice"] + "api/Invoice/GetInvoiceInfosByCondition",
            "GetLstLoanByFilterCustomer": BaseUrl["Loan"] + "api/loan/GetLstLoanByFilterCustomer",
            "GetLenderSearch": BaseUrl["Lender"] + "api/Lender/GetLenderSearch",
            "VerifyInvoiceCustomer": BaseUrl["Invoice"] + "api/Invoice/VerifyInvoiceCustomer",
            "GetHistoryCommentInvoice": BaseUrl["Invoice"] + "api/Invoice/GetHistoryCommentInvoice",
            "GetListImgInvoiceConsider": BaseUrl["Invoice"] + "api/Invoice/GetListImgInvoiceConsider",
            "UpdatePathImg": BaseUrl["Invoice"] + "api/Invoice/UpdatePathImg",
            "VerifyInvoiceConsiderCustomer": BaseUrl["Invoice"] + "api/Invoice/VerifyInvoiceConsiderCustomer",
            "GetDataImagesLos": BaseUrl["Loan"] + "api/loan/GetListImagesLos/",
            "ListImages": BaseUrl["Thn"] + "api/THN/ListImages",
            "ListVideo": BaseUrl["Thn"] + "api/THN/ListVideo",
            "GetMoneyNeedCloseLoanByLoanID": BaseUrl["Loan"] + "api/loan/GetMoneyNeedCloseLoanByLoanID",
            "ForceCloseLoanByLoanID": BaseUrl["Loan"] + "api/Loan/ForceCloseLoanByLoanID",
            "GetLstCommentDebtPromptedByLoanID": BaseUrl["Thn"] + "api/CommentDebtPrompted/GetLstCommentDebtPromptedByLoanID",
            "GetReason": BaseUrl["Thn"] + "api/Reason/GetReason",
            "CreateCommentDebtPrompted": BaseUrl["Thn"] + "api/CommentDebtPrompted/CreateCommentDebtPrompted",
            "GetLstSmsInfoByCondition": BaseUrl["Thn"] + "api/sms/GetLstSmsInfoByCondition",
            "CreateHoldCustomerMoney": BaseUrl["Thn"] + "api/HoldCustomerMoney/CreateHoldCustomerMoney",
            "UpdateHoldCustomerMone": BaseUrl["Thn"] + "api/HoldCustomerMoney/UpdateHoldCustomerMoney",
            "GetLoanCreditDetailLos": BaseUrl["Loan"] + "api/LOS/GetLoanCreditDetailLos/",
            "ListHoldCustomerMoney": BaseUrl["Thn"] + "api/HoldCustomerMoney/ListHoldCustomerMoney",
            "GetTransactionByLoanID": BaseUrl["Loan"] + "api/loan/GetTransactionByLoanID/",
            "GetDetailBankCard": BaseUrl["Loan"] + "api/Meta/GetBank/",
            //"GetLoanCreditDetailLosByLoanID": BaseUrl["Loan"] + "api/loan/GetLoanCreditDetailLosByLoanID/",
            "GetLstPaymentScheduleByLoanID": BaseUrl["Thn"] + "api/loan/GetLstPaymentScheduleByLoanID/",
            "GetInvoiceInfosByConditionTHN": BaseUrl["Invoice"] + "api/Invoice/GetInvoiceInfosByConditionTHN",
            "GetTopFriendFacebook": BaseUrl["Thn"] + "api/THN/GetTopFriendFacebook",
            "GetServiceCallInfoByUserID": BaseUrl["Thn"] + "api/user/GetServiceCallInfoByUserID",
            "GetLinkCallForPhone": BaseUrl["Thn"] + "api/user/GetLinkCallForPhone",
            "GetLstLoanInfoByKeySearch": BaseUrl["Thn"] + "api/loan/GetLstLoanInfoByKeySearch",
            "Getlstemployees": BaseUrl["Thn"] + "api/user/getlstemployees",
            "ChangephonecustomerbyloanID": BaseUrl["Thn"] + "api/user/changephonecustomerbyloanID",
            "Gethistorycustomerchangephone": BaseUrl["Thn"] + "api/loan/gethistorycustomerchangephone/",
            "UploadImageLos": "/VerifyInvoice/UploadImage",
            "DeleteImage": "/VerifyInvoice/DeleteImage",
            "GetLoanCreditDetailLosByLoanID": BaseUrl["Thn"] + "api/loan/GetLoanCreditDetailLosByLoanID/",
            "GetGPSLoanID": BaseUrl["Thn"] + "api/thn/GetGPSLoanID",
            "ShowHistoryComment": BaseUrl["Thn"] + "api/thn/ShowHistoryComment",
            "GetRefreshToken": "/Home/GetRefreshToken",
            "AddRequestExcel": BaseUrl["Thn"] + "api/Excel/AddRequestExcel",
            "ExcelLoanTHN": "/Excel/ExcelLoanTHN",
            "GetHistoryExportFile": BaseUrl["Thn"] + "api/excel/GetHistoryExportFile",
            "GetListProductCredit": BaseUrl["Loan"] + "api/Meta/GetListProductCredit",
            "GetListProvince": BaseUrl["Thn"] + "api/meta/getlstcity",
            "GetLstLoanEmployeeHandleDaily": BaseUrl["Thn"] + "api/report/GetLstLoanEmployeeHandleDaily",
            "Getreportemployeewithloan": BaseUrl["Thn"] + "api/report/getreportemployeewithloan",
            "GetListAppraisalTHN": BaseUrl["Thn_Tima"] + "Appraisal/GetListAppraisalTHN",
            "GetTypeReasons": BaseUrl["Thn_Tima"] + "Appraisal/GetTypeReasons",
            "SaveComment": BaseUrl["Thn_Tima"] + "Appraisal/SaveComment",
            "ReloadAppraiChipByLoanID": BaseUrl["Thn_Tima"] + "Appraisal/ReloadAppraiChipByLoanID",
            "HistoryCommentLoanID": BaseUrl["Thn_Tima"] + "Appraisal/HistoryCommentLoanID",
            "SearchHistoryTransaction": BaseUrl["Thn"] + "api/thn/SearchHistoryTransaction",
            "ChangeStatusInDepartment": BaseUrl["Ag_Tima"] + "api/AppraisalChip/ChangeStatusInDepartment",
            "CreateCustomerExtensionThirdParty": BaseUrl["Thn"] + "api/CustomerExtensionThirdParty/CreateCustomerExtensionThirdParty",
            "SearchCustomerExtensionThirdParty": BaseUrl["Thn"] + "api/CustomerExtensionThirdParty/SearchCustomerExtensionThirdParty",
            "GetLstHistoryInteractDebtPrompted": BaseUrl["Thn"] + "api/CommentDebtPrompted/GetLstHistoryInteractDebtPrompted",
            "GetLstSendSms": BaseUrl["Thn"] + "api/SendSms/GetLstSendSms",
            "CreateSendSms": BaseUrl["Thn"] + "api/SendSms/CreateSendSms",
            "GetRelativeFamily": BaseUrl["Thn"] + "api/Meta/GetRelativeFamily",
            "CreateRelationship": BaseUrl["Thn"] + "api/thn/CreateRelationship",
            "CreateFormLoanExemptionInterest": BaseUrl["Thn"] + "api/excel/CreateFormLoanExemptionInterest",
            "GetLstTypeFileForm": BaseUrl["Thn"] + "api/meta/GetLstTypeFileForm",
            "GetFormLoanExemptionInterest": BaseUrl["Thn"] + "api/Excel/GetFormLoanExemptionInterest",
            "DivideLoanExcel": BaseUrl["Thn"] + "api/thn/DivideLoanExcel",
            "CreateUserDepartmentTHN": BaseUrl["Thn"] + "api/UserDepartment/CreateUserDepartmentTHN",
            "GetDepartmentTHN": BaseUrl["Thn"] + "api/meta/GetDepartmentTHN",
            "GetUserDepartmentTHNByDepartment": BaseUrl["Thn"] + "api/UserDepartment/GetUserDepartmentTHNByDepartment",
            "ExcelGetHistoryCustomerTopup": BaseUrl["Thn"] + "api/Excel/GetHistoryCustomerTopup",
            "ExportExcelPayment": BaseUrl["Thn"] + "api/Excel/GetHistoryTransactionLoanByCustomer",
            "GetLstCity": BaseUrl["Thn"] + "api/Meta/GetLstCity",
            "ReportByDebtGroup": BaseUrl["Thn"] + "api/Report/ReportByDebtGroup",
            "ExcelGetHistoryTrackingLoanInMonth": BaseUrl["Thn"] + "api/Excel/GetHistoryTrackingLoanInMonth",
            "ListBookDebt": BaseUrl["Thn"] + "api/BookDebt/ListBookDebt",
            "CreateOfUpdateBookDebt": BaseUrl["Thn"] + "api/BookDebt/CreateOfUpdateBookDebt",
            "CreateOfUpdateApprovalLevelBookDebt": BaseUrl["Thn"] + "api/ApprovalLevelBookDebt/CreateOfUpdateApprovalLevelBookDebt",
            //"GetListApprovalLevelBookDebt": BaseUrl["Thn"] + "api/ApprovalLevelBookDebt/GetListApprovalLevelBookDebt",
            "UpdateApprovalLevelBookDebt": BaseUrl["Thn"] + "api/ApprovalLevelBookDebt/UpdateApprovalLevelBookDebt",
            "GetListTreeViewApprovalLevelBookDebt": BaseUrl["Thn"] + "api/ApprovalLevelBookDebt/GetListTreeViewApprovalLevelBookDebt",
            "GetSettingByConditions": BaseUrl["Thn"] + "api/MGLP/GetSettingByConditions",
            "GetListApprovalLevelBookDebt": BaseUrl["Thn"] + "api/ApprovalLevelBookDebt/GetListApprovalLevelBookDebt",
            "CreateSettingInterestReduction": BaseUrl["Thn"] + "api/MGLP/CreateSettingInterestReduction",
            "GetSettingByBookDebtID": BaseUrl["Thn"] + "api/MGLP/GetSettingByBookDebtID",
            "CreateSettingTotalMoney": BaseUrl["Thn"] + "api/MGLP/CreateSettingTotalMoney",
            "GetSettingMoneyByApprovalLevelBookDebtID": BaseUrl["Thn"] + "api/MGLP/GetSettingMoneyByApprovalLevelBookDebtID",
            "GetUserApprovalLevelConditions": BaseUrl["Thn"] + "api/UserApprovalLevel/GetUserApprovalLevelConditions",
            "CreateOrUpdateUserAppovalLevel": BaseUrl["Thn"] + "api/UserApprovalLevel/CreateOrUpdateUserAppovalLevel",
            "CreateRequestLoanExemption": BaseUrl["Thn"] + "api/loan/CreateRequestLoanExemption",
            "Getlstloanexemption": BaseUrl["Thn"] + "api/loan/getlstloanexemption",
            "UpdateStatusLoanExemption": BaseUrl["Thn"] + "api/loan/UpdateStatusLoanExemption",
            "Uploadfileloanexemption": BaseUrl["Thn"] + "api/loan/uploadfileloanexemption",
            "GetDetailLoanExemptionByID": BaseUrl["Thn"] + "api/loan/GetDetailLoanExemptionByID/",
            "GetHistoryLoanExemptionByLoanExemptionID": BaseUrl["Thn"] + "api/loan/GetHistoryLoanExemptionByLoanExemptionID/",
            "SettingUserPhone": BaseUrl["Thn"] + "api/User/SettingUserPhone/",
            "GetLstRequestCloseLoan": BaseUrl["Loan"] + "api/loan/GetLstRequestCloseLoan/",
            "CreateRequestCloseLoan": BaseUrl["Loan"] + "api/loan/CreateRequestCloseLoan",
            "GetAppraisalByLoanID": BaseUrl["Thn_Tima"] + "Appraisal/GetAppraisalByLoanID",
            "LstLoanSplitSingle": BaseUrl["Thn"] + "api/AssignmentLoan/LstLoanSplitSingle",
            "LstDetailsLoanSplitSingle": BaseUrl["Thn"] + "api/AssignmentLoan/LstDetailsLoanSplitSingle",
            "GetReportLoanDebtDaily": BaseUrl["Thn"] + "api/excel/GetReportLoanDebtDaily",
            "NotifyTotalMoneyCloseCustomer": BaseUrl["Loan"] + "api/PlanCloseLoan/NotifyTotalMoneyCloseCustomer/",
            "createdebtrestructuring": BaseUrl["Thn"] + "api/loan/createdebtrestructuring",
            "getlstdebtrestructuringloan": BaseUrl["Thn"] + "api/loan/getlstdebtrestructuringloan",
            "updatestatusdebtrestructuring": BaseUrl["Thn"] + "api/loan/updatestatusdebtrestructuring",
            "getdetaildebtrestructuringloanbyid": BaseUrl["Thn"] + "api/loan/getdetaildebtrestructuringloanbyid",
            "updatefileimagedebtrestructuring": BaseUrl["Thn"] + "api/loan/updatefileimagedebtrestructuring",
            "GetPostionDeparmentUser": BaseUrl["Thn"] + "api/UserDepartment/GetPostionDeparmentUser/",
            "TeamOfManager": BaseUrl["Thn"] + "api/UserDepartment/TeamOfManager",
            "GetStaffByLeader": BaseUrl["Thn"] + "api/UserDepartment/GetStaffByLeader/",
            "ReportDayPlan": BaseUrl["Thn"] + "api/Report/ReportDayPlan",
            "CreateReportDay": BaseUrl["Thn"] + "api/Excel/CreateReportDay",
            "GetPositionUser": BaseUrl["Thn"] + "api/User/GetPositionUser",
            "TranferAgCloseLoanExemption": BaseUrl["Thn"] + "api/MGLP/TranferAgCloseLoanExemption",
            "DownLoadFileDoc": "/Excel/DownLoadFileDoc",
            "CreateExportDataDocs": BaseUrl["Thn"] + "api/Excel/CreateExportDataDocs",
            "ExcelHistoryInteraction": BaseUrl["Thn"] + "api/Excel/ExcelHistoryInteraction",
            "GetConfigUserMapCampaign": BaseUrl["Thn"] + "api/smartdialer/GetConfigUserMapCampaign",
            "UpdateResourceCampaign": BaseUrl["Thn"] + "api/smartdialer/UpdateResourceCampaign",
            "GetUserCiscoForCampaign": BaseUrl["Thn"] + "api/smartdialer/GetUserCiscoForCampaign",
            "ConfigHoldCodeForSmartDialer": BaseUrl["Thn"] + "api/Meta/ConfigHoldCodeForSmartDialer",
            "AddConfigHoldCodeForSmartDialer": BaseUrl["Thn"] + "api/Meta/AddConfigHoldCodeForSmartDialer",


            "GetLstLoanInfoByPhoneDaily": BaseUrl["Thn"] + "api/loan/GetLstLoanInfoByPhoneDaily",
            "GetPlanHandleCallDailyByConditions": BaseUrl["Thn"] + "api/smartdialer/GetPlanHandleCallDailyByConditions",

        };
        lmsToken = 'BAD21EA6C96B0B8D3E45A1E2E5C38F2B4CACC4357CD02073E6B6809869CC307F';
        _Token = {
            LmsToken: 'BAD21EA6C96B0B8D3E45A1E2E5C38F2B4CACC4357CD02073E6B6809869CC307F',
            AgToken: ''
        };
    };
    var LoanIndex = {
        loanID: "loanID",
        search: "search",
        shopID: "shopID",
        lenderID: "lenderID",
        productID: "productID",
        status: "status",
        fromDate: "fromDate",
        toDate: "toDate",
    };
    var InvoiceIndex = {
        InvoiceID: "InvoiceID",
        InvoiceType: "InvoiceType",
        InvoiceSubType: "InvoiceSubType",
        ShopID: "ShopID",
        BankCardID: "BankCardID",
        Status: "Status",
        FromDate: "FromDate",
        ToDate: "ToDate",
    };
    var LoginPage = function () {
        var referrer = window.location.href;
        referrer = referrer.replace(':', '>').replace(/\//gi, '<').replace('&', '*');
        $.removeCookie(STATIC_USERMODEL, { path: '/' });
        window.location = LoginUrl + referrer;
    };
    var HomePage = function () {
        window.open(AdminUrl, "_self");
    };

    var ChangePassWord = function () {
        var urlChangePass = "/User/ChangePassword";
        window.open(urlChangePass, "_self");
    };
    var GetPostionUser = function () {
        var data = {
            UserID: USER_ID
        }
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.GetPositionUser, JSON.stringify(data), function (respone) {
            if (respone.Result == 1) {
                DEPARMENT_ID = respone.Data.DepartmentID;
                POSTION = respone.Data.PositionID;
                PARENTID = respone.Data.ParentUserID;
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        }, "lỗi không thành công vui lòng thử lại!");
    }
    var InitUser = new Promise(function (myResolve, myReject) {
        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                if (userModel != null && userModel != "") {
                    var user = JSON.parse(userModel);
                    console.log(user);
                    access_token = user.Token;
                    AdminUrl = user.AdminUrl;
                    USER_ID = user.UserID;
                    USER_NAME = user.UserName;
                    SetBase();
                    myResolve(user);
                } else {
                    myReject("Error");
                    LoginPage();
                }
            } else {
                LoginPage();
            }
        } catch (err) {
            LoginPage();
        }
    });
    function diff_minutes(dt2, dt1) {

        var diff = (dt2.getTime() - dt1.getTime()) / 1000;
        diff /= 60;
        return Math.round(diff);

    }
    var RefreshToken = function () {
        //ngủ cho tới khi gần hết time Token
        //mở tab mới gọi chức năng check token
        // check token nếu còn dài hạn trả lại ngay = cách gọi lại tất cả các trang 
        // nếu hết hạn thì refresh token và gọi lại tất cả các trang
        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                var user = JSON.parse(userModel);
                DomainCookie = user.DomainCookie;
                var remainTime = diff_minutes(new Date(user.TimeExpired), new Date());
                var waittime = 2;
                if (remainTime < waittime) {
                    //gọi RefreshToken
                    setTimeout(() => { RefreshToken(); }, 10000);
                    if (isCalledRefreshToken == "") {
                        isCalledRefreshToken = "called";
                        baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetRefreshToken, "", function (respone) {
                            if (respone.status == 1) {
                                access_token = respone.data.token;
                                user.Token = respone.data.token;
                                user.TimeExpired = respone.data.timeExpired;
                                user.TimeExpiredString = respone.data.timeExpiredString;
                                $.cookie(STATIC_USERMODEL, JSON.stringify(user), { expires: 30, path: '/', domain: user.DomainCookie, secure: true });

                            } else {
                                $.removeCookie(STATIC_USERMODEL, { path: '/' });
                                ShowErrorNotLoad("Phiên làm việc sắp hết hạn, vui lòng F5 thử lại!");
                            }
                            isCalledRefreshToken = "";
                        }, "Phiên làm việc sắp hết hạn, vui lòng F5 thử lại!");
                    }
                } else {
                    var timeOut = ((remainTime - waittime) + 1) * 60 * 1000;
                    setTimeout(() => {
                        isCalledRefreshToken = "";
                        RefreshToken();
                    }, timeOut);
                }
            } else {
                setTimeout(() => { RefreshToken(); }, 2000);
            }
        } catch (err) {
            setTimeout(() => { RefreshToken(); }, 2000);
        }
    }
    var Init = function () {
        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                var user = JSON.parse(userModel);
                DomainCookie = user.DomainCookie;
                AdminUrl = user.AdminUrl;
                STATIC_USERMODEL = user.CookieName;
                SetBase();
                UserLoginModel = user.UserLoginModel;
                RefreshToken();
                var fullShortName = (user.FullName).split(" ");
                var shortName = fullShortName[(fullShortName.length - 1)];
                if (fullShortName.length >= 2) {
                    shortName = fullShortName[(fullShortName.length - 2)] + " " + shortName;
                }
                $('#_UserFullNameId').html(shortName);
                $('#_UserFullNameShortId').html(user.UserName.substring(0, 1).toUpperCase());
                $('#_UserFullNameShort2Id').html(user.UserName.substring(0, 1).toUpperCase());
                GetPostionUser();
            } else {
                LoginPage();
            }
            // nút gọi điện nhanh trên menu
            $("#_call_dial_701").attr('style', 'background-color: #36a3f7; color: white;');
            $("#btn_call_metech").click(function () {
                var phone = $('#txt_call_cisco_manual').val();
                finesseC2CModule.ClickToCall(phone);
            });
        } catch (err) {
            LoginPage();
        }
    };
    var click_callMetech = function (i) {
        $(".btn_call_metech").removeAttr('style', 'background-color: #36a3f7; color: white;');
        $("#_call_dial_" + i).attr('style', 'background-color: #36a3f7; color: white;');
        if ($('#txt_call_cisco_manual').val().length > 3) {
            var strphone = $('#txt_call_cisco_manual').val();
            var phone = strphone.slice(0, 3);
            $('#txt_call_cisco_manual').val(strphone.replace(phone, i));
        }
        else {
            $('#txt_call_cisco_manual').val(i);
        }
    }
    var Pagesize = function () {
        return 20;
    };
    var GetToken = function () {
        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                var user = JSON.parse(userModel);
                return user.Token;
            } else {
                LoginPage();
                return null;
            }
        } catch (err) {
            LoginPage();
        }
    };
    var GetUser = function () {
        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                var user = JSON.parse(userModel);
                return user;
            } else {
                LoginPage();
                return null;
            }
        } catch (err) {
            LoginPage();
        }
    };
    var AjaxDone = function (method, endpoint, data, done, error, errFuntion) {
        return $.ajax({
            type: method,
            headers: {
                'Authorization': access_token
            },
            url: endpoint,
            data: data,
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).done(done).fail(function (e) {
            if (e.status == 401) {
                LoginPage();
            } else if (e.status == 403) {
                var params = (new URL(endpoint)).pathname;
                baseCommon.ShowErrorNotLoad("Vui lòng liên hệ quản lý để được phân quyền chức năng!<br>" + params);
            } else if (errFuntion == null || errFuntion == "") {
                eval(errFuntion);
            } else {
                if (error == null || error == "") {
                    error = e;
                }
                baseCommon.ShowErrorNotLoad(error);
            }
        });
    };
    var AjaxSuccess = function (method, endpoint, data, success, error) {
        return $.ajax({
            type: method,
            headers: {
                'Authorization': access_token
            },
            url: endpoint,
            data: data,
            contentType: "application/json; charset=utf-8",
            success: success,
            error: function (e) {
                if (e.status == 401) {
                    LoginPage();
                } else if (e.status == 403) {
                    var params = (new URL(endpoint)).pathname;
                    baseCommon.ShowErrorNotLoad("Vui lòng liên hệ quản lý để được phân quyền chức năng!<br>" + params);
                } else {
                    if (error == null || typeof error == "undefined" || error == "") {
                        error = e.responseText;
                    }
                    baseCommon.ShowErrorNotLoad(error);
                }
            }
        });
    };
    var AjaxSuccessOther = function (method, endpoint, data, success, error) {
        return $.ajax({
            type: method,
            headers: {
                'Authorization': lmsToken
            },
            url: endpoint,
            data: data,
            contentType: "application/json; charset=utf-8",
            success: success,
            error: function (e) {
                if (e.status == 401) {
                    LoginPage();
                } else if (e.status == 403) {
                    var params = (new URL(endpoint)).pathname;
                    baseCommon.ShowErrorNotLoad("Vui lòng liên hệ quản lý để được phân quyền chức năng!<br>" + params);
                } else {
                    if (error == null || typeof error == "undefined" || error == "") {
                        error = e.responseText;
                    }
                    baseCommon.ShowErrorNotLoad(error);
                }
            }
        });
    };
    var AjaxOther = function (method, endpoint, data, done, error, errFuntion) {
        return $.ajax({
            type: method,
            headers: {
                'Authorization': _Token.LmsToken
            },
            url: endpoint,
            data: data,
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).done(done).fail(function (e) {
            if (e.status == 401) {
                LoginPage();
            } else if (e.status == 403) {
                var params = (new URL(endpoint)).pathname;
                baseCommon.ShowErrorNotLoad("Vui lòng liên hệ quản lý để được phân quyền chức năng!<br>" + params);
            } else if (errFuntion == null || errFuntion == "") {
                eval(errFuntion);
            } else {
                if (error == null || error == "") {
                    error = e;
                }
                baseCommon.ShowErrorNotLoad(error);
            }
        });
    };
    var AjaxAg = function (method, endpoint, data, done, error, errFuntion) {
        return $.ajax({
            type: method,
            headers: {
                'Authorization': _Token.AgToken,
                'Access-Control-Allow-Origin': '*',
                "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
            },
            url: endpoint,
            data: data,
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).done(done).fail(function (e) {
            if (e.status == 401) {
                LoginPage();
            } else if (e.status == 403) {
                var params = (new URL(endpoint)).pathname;
                baseCommon.ShowErrorNotLoad("Vui lòng liên hệ quản lý để được phân quyền chức năng!<br>" + params);
            } else if (errFuntion == null || errFuntion == "") {
                eval(errFuntion);
            } else {
                if (error == null || error == "") {
                    error = e;
                }
                baseCommon.ShowErrorNotLoad(error);
            }
        });
    };
    var SelectDataSource = function (url, element, placeholder, value, name, selectedId, data) {
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            processData: false,
            contentType: false,
        }).done(function (respone) {
            $(element).html('');
            $(element).append('<option value="">' + placeholder + '</option>');
            $.each(respone.data, function (key, item) {
                var selected = "";
                if (Array.isArray(selectedId)) {
                    if (selectedId.find((itemSelected) => itemSelected[value] === item[value])) {
                        selected = "selected";
                    }
                } else {
                    if (item[value] == selectedId) {
                        selected = "selected";
                    }
                }
                $(element).append('<option ' + selected + ' value="' + item[value] + '">' + item[name] + '</option>');
            });
        });
    };
    var GetDataSource = function (url, data, done) {
        return $.ajax({
            type: "POST",
            url: url,
            data: data,
            processData: false,
            contentType: false,
        }).done(done);
    };
    var DataSource = function (element, placeholder, value, name, selectedId, data, valueDefaul = -111, isReset = 1) {
        try {
            if (isReset == 1) {
                $(element).html('');
            }
            if (placeholder !== undefined && placeholder !== '') {
                $(element).append('<option value="' + valueDefaul + '">' + placeholder + ' </option>');
            }
            $.each(data, function (key, item) {
                var selected = "";
                if (Array.isArray(selectedId)) {
                    if (selectedId.find((itemSelected) => itemSelected[value] === item[value])) {
                        selected = "selected";
                    }
                } else {
                    if (item[value] == selectedId) {
                        selected = "selected";
                    }
                }
                $(element).append('<option ' + selected + ' value="' + item[value] + '">' + item[name] + '</option>');
            });
        } catch (err) {

        }
    };
    var FormatCurrency = function FormatCurrency(Num) {
        Num += '';
        Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
        Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
        x = Num.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1))
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        return x1 + x2;
    };
    var ShowErrorNotLoad = function (message) {
        toastr.error(message);
    };
    var ShowSuccess = function (message) {
        toastr.success(message);
    };
    function formatNumber(nStr, decSeperate, groupSeperate) {
        nStr += '';
        x = nStr.split(decSeperate);
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + groupSeperate + '$2');
        }
        return x1 + x2;
    };
    function FormatDate(dateFormat, format) {
        var date = moment(dateFormat);
        return date.format(format);
    };
    function FormatRepo(repo) {

        if (repo.loading) return repo.text;
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__title'>" + repo.text + "</div>";
        return markup;
    };

    function FormatRepoSelection(repo) {

        return repo.text;
    };
    function RepoConvert(data) {

        var newObject = [];
        if (data != null && data != "") {
            $.each(data, function (key, value) {
                var newRepo = { id: value.ID, text: value.FullName };
                newObject.push(newRepo);
            });
        }
        return newObject;
    };
    var ButtonSubmit = function (isVisible, id) {
        if (!isVisible) {
            $(id).addClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
            $(id).prop("disabled", true);
        } else {
            $(id).removeClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
            $(id).prop("disabled", false);
        }
    }
    var ButtonOnOff = function (isVisible, id) {
        if (!isVisible) {
            $(id).prop("disabled", true);
        } else {
            $(id).prop("disabled", false);
        }
    }
    var IconWaiting = function (isVisible, id) {
        if (!isVisible) {
            $(id).addClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
        } else {
            $(id).removeClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
        }
    }

    var html_money_2 = function (money) {
        try {
            if (Number(money) == "NaN" || Number(money) == 0) {
                return "<span class='kt-font-blue'> 0 </span>";
            }
            var numString = money.toString();
            var beforeDot = numString, afterDot = "";
            if (numString.includes(".")) {
                // chỉ lấy đến dấu "."
                var n = numString.indexOf(".");
                beforeDot = numString.substring(0, n);
                afterDot = numString.substring(n, numString.lenght);
            }
            if (beforeDot.length > 3) {
                // 000012,345,678 
                // 0.123
                for (var y = 0; beforeDot.length >= 0; y++) {
                    var zero = beforeDot.toString().substring(0, 1);
                    if (zero == "0" && beforeDot.length > 1) {
                        beforeDot = beforeDot.substring(1, beforeDot.lenght);
                    } else {
                        break;
                    }
                }
                var i, from, beforeDotTemp = "", tempNum = "", tempCount = 0;
                for (i = beforeDot.length; i > 0; i--) {
                    from = i - 1;
                    tempCount++;
                    var num = beforeDot.substring(from, i);
                    tempNum = num.concat(tempNum);
                    if (tempCount >= 3 || i <= 0) {
                        if (i > 1) {
                            tempNum = ",".concat(tempNum);
                        }
                        beforeDotTemp = tempNum.concat(beforeDotTemp);
                        tempCount = 0;
                        tempNum = "";
                    }
                }
                if (tempNum != "") {
                    beforeDotTemp = tempNum.concat(beforeDotTemp);
                }
                numString = beforeDotTemp.concat(afterDot);
            }
            if (money >= 0) {
                return "<span class='kt-font-blue'> " + numString + " </span>";
            } else {
                return "<span class='kt-font-danger'> " + numString + " </span>";
            }
        } catch (e) {
            return "<span class='kt-font-blue'> 0 </span>";
        }
    }
    var html_money = function (money) {
        try {
            if (Number(money) == "NaN" || Number(money) == 0) {
                return 0;
            }
            var numString = money.toString();
            var beforeDot = numString, afterDot = "";
            if (numString.includes(".")) {
                // chỉ lấy đến dấu "."
                var n = numString.indexOf(".");
                beforeDot = numString.substring(0, n);
                afterDot = numString.substring(n, numString.lenght);
            }
            if (beforeDot.length > 3) {
                // 000012,345,678 
                // 0.123
                for (var y = 0; beforeDot.length >= 0; y++) {
                    var zero = beforeDot.toString().substring(0, 1);
                    if (zero == "0" && beforeDot.length > 1) {
                        beforeDot = beforeDot.substring(1, beforeDot.lenght);
                    } else {
                        break;
                    }
                }
                var i, from, beforeDotTemp = "", tempNum = "", tempCount = 0;
                for (i = beforeDot.length; i > 0; i--) {
                    from = i - 1;
                    tempCount++;
                    var num = beforeDot.substring(from, i);
                    tempNum = num.concat(tempNum);
                    if (tempCount >= 3 || i <= 0) {
                        if (i > 1) {
                            tempNum = ",".concat(tempNum);
                        }
                        beforeDotTemp = tempNum.concat(beforeDotTemp);
                        tempCount = 0;
                        tempNum = "";
                    }
                }
                if (tempNum != "") {
                    beforeDotTemp = tempNum.concat(beforeDotTemp);
                }
                numString = beforeDotTemp.concat(afterDot);
            }
            return numString;
        } catch (e) {
            return 0;
        }

    }
    var StringFormat = function (stringOrg, placeholdersObject) {
        // how to use:  
        try {
            for (var propertyName in placeholdersObject) {
                var re = new RegExp('{' + propertyName + '}', 'gm');
                stringOrg = stringOrg.replace(re, placeholdersObject[propertyName]);
            }
            return stringOrg;
        } catch (e) {
            return null;
        }
    }
    function pad2(n) {
        return (n < 10 ? '0' : '') + n;
    }
    var FormatDateDDMMYYYY = function (date) {
        debugger
        var month = pad2(date.getMonth() + 1);
        var day = pad2(date.getDate());
        var year = date.getFullYear();
        return day + "/" + month + "/" + year;
    }
    return {
        FormatDateDDMMYYYY: FormatDateDDMMYYYY,
        InvoiceIndex: InvoiceIndex,
        LoanIndex: LoanIndex,
        StringFormat: StringFormat,
        ButtonSubmit: ButtonSubmit,
        Init: Init,
        SelectDataSource: SelectDataSource,
        DataSource: DataSource,
        GetDataSource: GetDataSource,
        AjaxDone: AjaxDone,
        AjaxSuccess: AjaxSuccess,
        HomePage: HomePage,
        FormatCurrency: FormatCurrency,
        Endpoint: Endpoint,
        GetToken: GetToken,
        ShowErrorNotLoad: ShowErrorNotLoad,
        ShowSuccess: ShowSuccess,
        formatNumber: formatNumber,
        Pagesize: Pagesize,
        FormatDate: FormatDate,
        LoginUrl: LoginUrl_,
        FormatRepo: FormatRepo,
        FormatRepoSelection: FormatRepoSelection,
        RepoConvert: RepoConvert,
        Option: Option,
        GetUser: GetUser,
        InitUser: InitUser,
        VerifyInvoice: VerifyInvoice,
        html_money_2: html_money_2,
        html_money: html_money,
        IconWaiting: IconWaiting,
        ChangePassWord: ChangePassWord,
        RefreshToken: RefreshToken,
        AjaxOther: AjaxOther,
        AjaxSuccessOther: AjaxSuccessOther,
        StatusInDepartment: StatusInDepartment,
        AjaxAg: AjaxAg,
        click_callMetech: click_callMetech,
        ButtonOnOff: ButtonOnOff,
        GetPostionUser: GetPostionUser
    };
}

$(document).ready(function () {
    baseCommon.Init();
});
$(document).on('click', '.dropdown-menu', function (e) {
    e.stopPropagation();
});
