﻿/**
* Show pictures in article content as a fullscreen slideshow
*/
function FullscreenSlideshow() {
    var BLANK_GIF = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==',
        firstTime = true;

    var $contentElm,
        $slideImages,
        $btnPrevious,
        $btnNext,
        $btnAutoPlay,
        $btnFullscreen,
        $btnTitle,
        $btnExit,
        $progressBar,
        $btnSaveImage,
        $btnRotateImage,
        $progressBarPercent,
        $slideshowScreen,
        $divContent,
        $currentImg,
        listImages,
        currentIndex,
        timer,
        optimizeWidth,
        slideshowTimer;


    var initImages = function (documentWidth) {
        var $ul = $slideshowScreen.find('.content ul');

        listImages = [];

        for (var i = 0, length = $slideImages.length; i < length; i++) {
            var image = {},
                $slideImage = $slideImages.eq(i),
                imageSrc = $slideImage.attr('src').replace(/\/w[0-9]+\//ig, '/w' + optimizeWidth + '/');
            $wrapper = $slideImage.parents('table');

            image.src = imageSrc;
            image.thumbSrc = $slideImage.attr('src');

            if ($wrapper.length > 0) {
                $caption = $wrapper.find('.Image');
            }
            pos = (i == 0) ? 'first' : (i == length - 1) ? 'last' : i;
            var slideHtml = '<li class="loading" style="width:' + documentWidth + 'px" slidePos="' + pos + '">' +
                    '<img data-src="' + imageSrc + '" />';

            //if ($caption && $caption.length > 0) {
            //    var captionText = $caption.text();
            //    captionText = (captionText != 'Nhập mô tả cho ảnh' ? captionText : '');
            //    slideHtml += '<p class="caption visible">' + captionText + '</p>';

            //    image.caption = captionText;
            //}
            slideHtml += '</li>';
            $ul.append(slideHtml);

            listImages.push(image);
        };
    };

    var getPhotoIndexBySrc = function (src) {
        var size = listImages.length;
        for (var i = 0; i < size; i++) {
            if (listImages[i].thumbSrc === src) {
                return i;
            }
        }

        return -1;
    };

    var showPhoto = function (index) {
        currentIndex = index;
        var documentWidth = $(document).width();

        if ($currentImg && $currentImg.length > 0) {
            $currentImg.removeClass('current');
            $currentImg.find('img').src = BLANK_GIF;
        }

        $currentImg = $slideshowScreen.find('li:eq(' + index + ')');
        $currentImg.addClass('current');

        if (!$currentImg.attr('data-loaded')) {
            var $img = $currentImg.find('img');
            if (!$img.attr('src')) {
                $img[0].onload = function (e) {
                    var src = this.getAttribute('src'),
                        $this = $(this),
                        imageWidth = $this.width(),
                        $parentLi = $this.parent('li');

                    if (src.indexOf("data:") == -1) {
                        $(this).parent().removeClass('loading').css('background-image', 'url(' + src + ')').attr('data-loaded', 1);
                        this.onload = null;
                    }

                    if ($this.height() < $(window).height()) {

                        if ((optimizeWidth == 1920 && imageWidth < 800) ||  // for screen > 1440, the minimum width to be display fullscreen is 800
                            (optimizeWidth == 1440 && imageWidth < 600) ||  // for screen > 1024, minimum fullscreen width is 600
                            (optimizeWidth == 1024 && imageWidth < 400)) {  // for screen > 1024, minimum fullscreen width is 400
                            $parentLi.addClass('noFullscreen');
                        }
                    }
                    /*
                    if (autoplay === true && $parentLi.hasClass('current')) {
                    autoPlay(4000);
                    }
                    */
                };
            }
            $img[0].src = $img.attr('data-src');
        }

        var slidePos = $currentImg.attr('slidePos');

        // decide which button to display
        if (slidePos == 'first') {
            $btnPrevious.hide();
            if ($slideshowScreen.find('li').length > 1) {
                $btnNext.show();
            } else {
                $btnNext.hide();
            }
        } else if (slidePos == 'last') {
            $btnNext.hide();
            if ($slideshowScreen.find('li').length > 1) {
                $btnPrevious.show();
            } else {
                $btnPrevious.hide();
            }
        }
        /*
        $slideshowScreen.find('ul').css({
        'width': $slideshowScreen.find('li').length * documentWidth,
        'left': '-' + $currentImg.prevAll().length * documentWidth + 'px'
        }).attr('slideWidth', documentWidth);
        */
        var $ul = $slideshowScreen.find('ul');
        if (!$ul.attr('slideWidth')) {
            $ul.css('width', $slideshowScreen.find('li').length * documentWidth).attr('slideWidth', documentWidth);
        }

        //var step = parseInt($ul.attr('slideWidth')) * relPos;
        var moveToLeft = -$currentImg.prevAll().length * documentWidth;

        if (firstTime) {
            $ul.css('left', moveToLeft + 'px');
            firstTime = false;
        } else {
            $ul.transition({
                'left': moveToLeft + 'px'
            }, 500, 'ease', function () {
                // decide which navigation button to show
                if ($currentImg.attr('slidePos') == 'first') {
                    $btnPrevious.hide();
                } else if ($currentImg.attr('slidePos') == 'last') {
                    $btnNext.hide();
                } else {
                    $btnNext.show();
                    $btnPrevious.show();
                }
                setTimeout(function () {
                    $currentImg.find('.caption').removeClass('visible');
                }, 3500);
                //ga('send', 'event', 'Slideshow', 'Slide Next', window.location.pathname);
            });
        }
    };

    var launchFullScreen = function (element) {
        //ga('send', 'event', 'Slideshow', 'Switch fullscreen', window.location.pathname);
        if (element.requestFullScreen) {
            element.requestFullScreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (element.webkitRequestFullScreen) {
            element.webkitRequestFullScreen();
        }
    };

    var cancelFullscreen = function () {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    };

    var launchSlideshow = function ($start, autoplay) {
        optimizeWidth = 1024;

        var $caption, slide, $wrapper,
            documentWidth = $(document).width();

        $('body').css('overflow', 'hidden');

        var width = $(document).width();

        $slideshowScreen = $('#slideshowScreen');

        if ($slideshowScreen.length == 0) {
            var html = '<div id="slideshowScreen">' +
                        '<div class="header"></div><div class="content"><ul></ul></div>' +
                        '<a class="btnPrevious button">Sau</a>' +
                        '<a class="btnNext button">Trước</a>' +
                        '<a href="javascript:page.article.slideshow_close();" class="btnClose button">Đóng</a>' +
                        '<div id="progressbar"><span class="percent"><span></span></span><label>Tự động chiếu hình tiếp theo. <span>Nhấn chuột vào đây để dừng</span>.</label></div>' +
                        '</div>';
            $('body').append($(html));

            $slideshowScreen = $('#slideshowScreen');
            $slideshowScreen.find('#progressbar').on('click', function () {
                stopAutoPlay();
            });

            $btnNext = $slideshowScreen.find('.btnNext');
            $btnPrevious = $slideshowScreen.find('.btnPrevious');

            $btnNext.click(function (e) {
                e.preventDefault();
                moveTo(1, true);
            });

            $btnPrevious.click(function (e) {
                e.preventDefault();
                moveTo(-1, true);
            });

            $slideshowScreen.bind('contextmenu', function (e) {
                return false;
            });
        }

        $divContent = $slideshowScreen.find('.content');
        $divContent.find('ul').empty();
        $divContent.on('click', function (e) {
            e.preventDefault();
            var x = e.hasOwnProperty('offsetX') ? e.offsetX : e.layerX;

            if (x > 200) {
                moveTo(1, true);
            } else {
                moveTo(-1, true);
            }
        });

        $divContent.on('click', '.caption', function (e) {
            e.stopPropagation();
        });

        $slideshowScreen.find('.header').html(
            '<a class="btnTitle">' + $('h1.title').text() + '</a>' +
          //  '<iframe src="//www.facebook.com/plugins/like.php?href=' + $("meta[property='og:url']").attr('content') + '&amp;width=170&amp;height=35&amp;colorscheme=light&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;&amp;locale=vi_VN&amp;send=true&amp;appId=1397711853803260" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:140px; height:20px; margin:7px 0 0 15px;" allowTransparency="true"></iframe>' +
            '<a class="btnExit ">Thoát</a>' +
            '<a class="btnFullscreen ">Xem toàn màn hình</a>' +
            '<a class="btnAutoPlay ">Tự động trình chiếu</a>'+
            '<a class="btnSave">Lưu</a>'+
            '<a class="btnRotate">Xoay ảnh</a>'
        );

        $btnNext = $slideshowScreen.find('.btnNext');
        $btnPrevious = $slideshowScreen.find('.btnPrevious');
        $btnAutoPlay = $slideshowScreen.find('.btnAutoPlay');
        $btnTitle = $slideshowScreen.find('.btnTitle');
        $btnExit = $slideshowScreen.find('.btnExit');
        $btnFullscreen = $slideshowScreen.find('.btnFullscreen');
        $progressBar = $slideshowScreen.find('#progressbar');
        $progressBarPercent = $progressBar.find('.percent span');
        $btnSaveImage = $slideshowScreen.find('.btnSave');
        $btnRotateImage = $slideshowScreen.find('.btnRotate');

        if (!autoplay) {
            $btnAutoPlay.show();
        }

        $btnAutoPlay.on('click', function (e) {
            e.preventDefault();
            autoPlay();
        });

        $btnSaveImage.on('click', function (e) {
            var currentsrc = $('#slideshowScreen li.current>img').attr("src");
            var btndownloadimage = $("<a>").attr("href", currentsrc).attr("download", currentsrc).appendTo("body");
            btndownloadimage[0].click();
            btndownloadimage.remove();
        });
        var numberRotate = 0;
        $btnRotateImage.on('click', function (e) {
            numberRotate = numberRotate + 90;
            $('#slideshowScreen li.current').rotate(numberRotate);
            $('#slideshowScreen li.current').css("margin-top", "120px");
        });
        //if (page.isFullscreenEnable()) {
        if (true) {
            $btnFullscreen.on('click', function (e) {
                e.preventDefault();
                launchFullScreen(document.getElementById('slideshowScreen'));
            });
        } else {
            $btnFullscreen.hide();
        }

        $btnExit.on('click', function (e) {
            e.preventDefault();
            closeSlideshow();
        });

        $btnTitle.on('click', function (e) {
            e.preventDefault();
            closeSlideshow();
        });

        if (documentWidth > 1440) {
            optimizeWidth = 1920;
        } else if (documentWidth > 1024) {
            optimizeWidth = 1440;
        }

        initImages(width);

        if ($start.length == 1) {
            $btnPrevious.hide();
            $btnNext.hide();
        } else {
            $btnPrevious.show();
            $btnNext.show();
        }

        var index = 0;
        if ($start.length > 0) {
            index = getPhotoIndexBySrc($start.attr('src'));
            index = index == -1 ? 0 : index;
        }

        showPhoto(index);

        $(document).keydown(onKeyDown);

        $('#slideshowScreen').show();

        //ga('send', 'event', 'Slideshow', 'Launch Slideshow', window.location.pathname);

    };

    var onKeyDown = function (e) {
        //console.log(e.which);
        if (e.which == 39) { // right + up
            moveTo(1, true);
        } else if (e.which == 37) { // left + down
            moveTo(-1, true);
        } else if (e.which == 27) { // esc
            closeSlideshow();
        } else if (e.which == 13) { // enter
            launchFullScreen(document.getElementById('slideshowScreen'));
        } else if (e.which == 32) { // space
            if (!slideshowTimer == null) {
                autoPlay(4000);
            } else {
                stopAutoPlay();
            }
        }
    };

    var showProgressBar = function (delay) {
        timer && clearTimeout(timer);
        timer = setTimeout(function () {
            $progressBar.show();
            $progressBarPercent.css('width', '0%').transition({
                'width': '100%'
            }, 2000, 'linear');
        }, delay - 2000);
    };

    var autoPlay = function (delay) {
        if (typeof delay === 'undefined') {
            delay = 4000;
        }

        $btnAutoPlay.hide();

        showProgressBar(delay);

        slideshowTimer && clearInterval(slideshowTimer);
        slideshowTimer = setInterval(function () {
            var current = $slideshowScreen.find('li.current');
            $progressBar.hide();
            if ($(current).attr('slidePos') == 'last') {
                showPhoto(0);
            } else {
                moveTo(1);
            }

            showProgressBar(delay);
        }, delay);
    };

    var stopAutoPlay = function () {
        $progressBar.hide();
        $progressBarPercent.css('width', '0%');

        slideshowTimer && clearInterval(slideshowTimer);
        timer && clearTimeout(timer);
        slideshowTimer = null;
        $btnAutoPlay.show();
    };

    var moveTo = function (relPos, stopAuto) {
        if (slideshowTimer != null && stopAuto) {
            stopAutoPlay();
        }

        var $current = $('#slideshowScreen li.current');
        if ($current.length == 0) {
            return;
        }
        var goNext = (relPos > 0);

        var $next = goNext ? $current.next('li') : $current.prev('li');

        if ($next.length > 0) {
            currentIndex += goNext ? 1 : -1;
            showPhoto(currentIndex);
        } else {
            //closeSlideshow();
            return;
        }
    };

    var closeSlideshow = function () {
        firstTime = true;
        stopAutoPlay();
        $("body").css("overflow", "auto");
        cancelFullscreen();
        $slideshowScreen.hide().remove();
        $(document).unbind('keydown', onKeyDown);
    };

    var init = function ($elm) {
        $contentElm = $elm;
        currentIndex = 0;

        $slideImages = $('div.photoHD img', $contentElm);

        if ($slideImages.length > 0) {
            $slideImages.unbind('click');
            $slideImages.on('mouseover', function () {
                var $img = $(this);
                var $btnSlideshow = $img.parents('div.photoHD').find('.btnSlideshow');
                var $btnFBShare = $img.parents('div.photoHD').find('.fbShareImage');

                if ($btnSlideshow.length == 0) {
                    $img.after('<a href="#slideshow" class="btnSlideshow">Phóng to</a>');
                    $img.parents('div.photoHD').find('.btnSlideshow').on('click', function () {
                        launchSlideshow($img);
                    });
                }
            }).on('click', function () {
                launchSlideshow($(this));
            });
        }
    };

    this.init = init;
};