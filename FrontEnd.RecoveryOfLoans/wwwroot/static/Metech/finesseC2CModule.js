﻿var
    //Private finesse object.
    _finesse,
    //Store agent information
    _username, _password, _extension, _domain, _login, _state, _callIdMake, _phoneCall,
    //Private reference to JabberWerx eventing object.
    _jwClient;
var finesseC2CModule = new function () {
    var _signInHandler = function (data, statusText, xhr) {
        //Ensure success.
        if (xhr.status === 202) {
            //Hide signin forms and show actions.
            console.log("signin success");
        }
    };

    var _makeCallHandler = function (data, statusText, xhr) {
        //Validate success.
        if (statusText === "success") {
            console.log("make call success");
        }
    };

    var _handler = function (data, statusText, xhr) {
        if (xhr) {
            console.log("RESPONSE " + xhr.status);
        } else {
            console.log("RESPONSE " + data);
        }
    };

    var LoginFinesse = function (username, password, extension, domain) {
        _username = username;
        _password = password;
        _extension = extension;
        _domain = domain;
        //Check non-empty fields
        if (!_username || !_password || !_extension || !_domain) {
            console.log("Please enter valid domain and credentials.");
        } else {
            _finesse = new Finesse(_username, _password);
            _finesse.signIn(_username, _extension, true, _signInHandler, _signInHandler);
        }
    };

    var ClickToCall = function (phoneNumber) {
        _finesse.makeCall(phoneNumber, _extension, _makeCallHandler, _handler);
    };

    return {
        LoginFinesse: LoginFinesse,
        ClickToCall: ClickToCall
    };
}