﻿var checkCall = 1;
var mess;
var lstCodePhone = [701, 702, 703, 704, 705, 706, 707, 708, 709, 710];
var lstCreateCodePhone = [];
var tongDai = new function () {
    var ConfigCallCenter = {
        Metech: 2,
        Caresoft: 1
    }
    var GetServiceCallInfoByUserID = function (btnClickToCallId) {
        try {
            $.cookie(STATIC_CALL_INFO_COOKIE, '', { expires: 30, path: '/', domain: DomainCookie, secure: true });
            baseCommon.ButtonOnOff(false, '#' + btnClickToCallId);//btnClickToCallCustomer
            baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetServiceCallInfoByUserID, "", function (respone) {
                if (respone.Result == 1) {
                    if (respone.Data.TypeCall == ConfigCallCenter.Metech) {
                        finesseC2CModule.LoginFinesse(respone.Data.CiscoUserName, respone.Data.CiscoPassword, parseInt(respone.Data.CiscoExtension), 'tima.vn');
                    }
                    baseCommon.ButtonOnOff(true, '#' + btnClickToCallId);
                    respone.Data.CiscoPassword = null;
                    respone.Data.CiscoExtension = null;
                    $.cookie(STATIC_CALL_INFO_COOKIE, JSON.stringify(respone), { expires: 30, path: '/', domain: DomainCookie, secure: true });
                } else {
                    checkCall = 0;
                    mess = respone.Message;
                }
            });
        } catch (e) {
            var s = e;
        }
    };
    $("#div_call").click(function () {
        if (checkCall == 0) {
            baseCommon.ShowErrorNotLoad(mess);
        } else {
        }
    })
    var CheckConfigCall = function (btnClickToCallId) {
        try {

            baseCommon.ButtonOnOff(false, '#' + btnClickToCallId);//btnClickToCallCustomer
            var static_call_info_cookie = $.cookie(STATIC_CALL_INFO_COOKIE);
            if (static_call_info_cookie != 'undefined' && static_call_info_cookie != null && static_call_info_cookie != '') {
                var static_call_info_cookie_json = JSON.parse(static_call_info_cookie);
                if (static_call_info_cookie_json.Result == 1) {
                    baseCommon.ButtonOnOff(true, '#' + btnClickToCallId);
                }
            } else {
                setTimeout(function () { CheckConfigCall(btnClickToCallId); }, 1000);
            }
        } catch (e) {
            setTimeout(function () { CheckConfigCall(btnClickToCallId); }, 1000);
        }
    };
    var CallCustomer = function (customerName, numberPhone) {
        var static_call_info_cookie = $.cookie(STATIC_CALL_INFO_COOKIE);
        var static_call_info_cookie_json = JSON.parse(static_call_info_cookie);
        var config = static_call_info_cookie_json.Data.TypeCall;
        if (ConfigCallCenter.Metech == config) {
            confirmCallCustomerMetech(customerName, numberPhone)
        } else {
            confirmCallCustomer(customerName, numberPhone)
        }
    };
    var confirmCallCustomer = function (customerName, numberPhone) {
        swal.fire({
            title: '<span style="color: red;">Tổng đài Caresoft</span>',
            html: '<h3>Bạn sẽ gọi ra cho khách hàng <b>' + customerName + '</b> với số điện thoại &nbsp; ' +
                '<select id="cbPhone" name="cbPhone" class="kt-select2">' +
                '<option style="cursor: pointer" title="Số mặc định" value="200532">Bộ định tuyến 0660000094</option>' +
                '</select> </h3>',
            position: 'top',
            customClass: 'swal-wide',
            showCancelButton: true,
            cancelButtonText: "<span><i class='la la-minus-circle'></i><span>Nghĩ lại</span></span>",
            cancelButtonClass: "btn btn-light btn-elevate btn-pill btn-sm",
            confirmButtonText: "<span><i class='la la-check-circle'></i><span>Đồng ý gọi!</span></span>",
            confirmButtonClass: "btn btn-success btn-elevate btn-pill btn-sm",
        }).then(function (result) {
            if (result.value) {
                var calloutId = $("#cbPhone").val();
                var data = { Phone: numberPhone, CallOutID: calloutId };
                baseCommon.AjaxDone("POST", baseCommon.Endpoint.GetLinkCallForPhone, JSON.stringify(data), function (respone) {
                    if (respone.Result == 1) {
                        swal.close();
                        setTimeout(function () {
                            window.open(respone.Data.CareSoftLinkCall, '_blank');
                        }, 500);
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                });
                swal.close();
            } else if (result.dismiss === 'cancel') {
                $('button').blur();
            };
        });
    };
    var confirmCallCustomerMetech = function (customerName, numberPhone) {
        if (lstCreateCodePhone.length <= 0) {
            lstCreateCodePhone.push.apply(lstCreateCodePhone, lstCodePhone);
        }
        var item = lstCreateCodePhone[Math.floor(Math.random() * lstCreateCodePhone.length)];// radom số trong list
        const index = lstCreateCodePhone.indexOf(item); // xóa số random đã quay trong list
        lstCreateCodePhone.splice(index, 1);

        swal.fire({
            title: '<span style="color: red;">Tổng đài Metech</span>',
            html: '<h4>Bạn sẽ gọi ra cho khách hàng <b>' + customerName + '</b> với số điện thoại <b>' + numberPhone + '</b> mã quay <b>' + item + '</b></h4>',
            position: 'top',
            customClass: 'swal-wide',
            allowOutsideClick: false,
            showCancelButton: true,
            cancelButtonText: "<span><i class='la la-minus-circle'></i><span>Nghĩ lại</span></span>",
            cancelButtonClass: "btn btn-light btn-elevate btn-pill btn-sm",
            confirmButtonText: "<span><i class='la la-check-circle'></i><span>Đồng ý gọi!</span></span>",
            confirmButtonClass: "btn btn-success btn-elevate btn-pill btn-sm",
        }).then(function (result) {
            if (result.value) {
                var firstNumber = item;
                phone = firstNumber + numberPhone;
                finesseC2CModule.ClickToCall(phone)
                swal.close();
            } else if (result.dismiss === 'cancel') {
                $('button').blur();
            };
        });;
    };
    return {
        CallCustomer: CallCustomer,
        GetServiceCallInfoByUserID: GetServiceCallInfoByUserID,
        CheckConfigCall: CheckConfigCall
    };
}