using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using FrontEnd.Common.Services.Bank;
using FrontEnd.Common.Services.Insurance;
using FrontEnd.Common.Services.Invoice;
using FrontEnd.Common.Services.Lender;
using FrontEnd.Common.Services.Loan;
using FrontEnd.Common.Services.Login;
using FrontEnd.Common.Services.Shop;
using FrontEnd.Common.Services.User;
using FrontEnd.Common.Services.Excel;
using FrontEnd.Common.Services.Ticket;
using FrontEnd.Common.Helpers;

namespace FrontEnd.Lender
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<KestrelServerOptions>(Configuration.GetSection("Kestrel"));
            services.Configure<FrontEnd.Common.Helpers.ServiceHost>(Configuration.GetSection("ServiceHost"));
            services.AddControllersWithViews();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromHours(12);
                options.Cookie.IsEssential = true;
                options.Cookie.Name = "LMS_Acounttant";
            });
            services.AddHttpClient();
            services.AddTransient<ILoanServices, LoanServices>();
            services.AddTransient<ILoginService, LoginService>();
            services.AddTransient<IShopService, ShopService>();
            services.AddTransient<ILenderService, LenderService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IInvoiceServices, InvoiceServices>();
            services.AddTransient<IBankServices, BankServices>();
            services.AddTransient<IMetaServices, MetaServices>();
            services.AddTransient<IInsuranceServices, InsuranceService>();
            services.AddTransient<IExcelService, ExcelService>();
            services.AddTransient<ITicketServices, TicketServices>();
            services.AddTransient<IExcelExportData, ExcelExportDataLoan>();
            services.AddTransient<IExcelExportData, ExcelHistoryCustomerTopup>();
            services.AddTransient<IExcelExportData, ExcelHistoryTransactionLoanByCustomer>();
            services.AddTransient<IExcelExportData, ExcelReportLoanDebtType>();
            services.AddTransient<IExcelExportData, ExcelReportLoanDebtDaily>();
            services.AddTransient<IExcelExportData, ExcelReportDayPlan>();
            services.AddTransient<IExcelExportData, ExcelHistoryInteraction>();
            services.AddTransient<IExcelExportData, ExcelLenderDeposit>();
            services.AddTransient<IExcelExportData, ExcelLenderBM01>();
            Constants.STATIC_USERMODEL = Configuration["Configuration:CookieName"].ToString();
            Constants.STATIC_VERSION = $"{DateTime.Now.Ticks}";// Configuration["Configuration:JsVersion"].ToString();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.   
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                // app.UseHsts();
            }
            // app.UseHttpsRedirection();
            app.UseSession();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
