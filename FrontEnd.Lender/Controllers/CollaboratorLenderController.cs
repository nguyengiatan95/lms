﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Lender.Controllers
{
    public class CollaboratorLenderController : BaseController
    {
        public CollaboratorLenderController(IConfiguration configuration)
            : base(configuration)
        {

        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ReportTransaction()
        {
            return View();
        }

    }
}
