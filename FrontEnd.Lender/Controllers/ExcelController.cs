﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Services.Excel;
using FrontEnd.Common.Services.Insurance;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace FrontEnd.Lender.Controllers
{
    public class ExcelController : BaseController
    {
        private readonly ILogger<ExcelController> _logger;
        private readonly IInsuranceServices _insuranceServices;
        private readonly IExcelService _excelService;
        private readonly Common.Helpers.Common _common;
        private readonly IEnumerable<IExcelExportData> _excelExportData;
        public ExcelController(ILogger<ExcelController> logger, IConfiguration configution, IInsuranceServices insuranceServices, IEnumerable<IExcelExportData> excelExportData, IExcelService excelService) : base(configution)
        {
            _insuranceServices = insuranceServices;
            _excelService = excelService;
            _logger = logger;
            _common = new Common.Helpers.Common();
            _excelExportData = excelExportData;
        }
        public async Task<ActionResult> ExcelLenderInsuranceReport(string StrFromDate, string StrToDate, int LenderID,
            int TypeInsurance, int StatusSendInsurance, string CodeID, string CustomerName)
        {
            Common.Models.Insurrance.IndemnifyModel indemnifyModel = new Common.Models.Insurrance.IndemnifyModel()
            {
                CodeID = CodeID,
                CustomerName = CustomerName,
                LenderID = LenderID,
                StatusSendInsurance = StatusSendInsurance,
                StrFromDate = StrFromDate,
                StrToDate = StrToDate,
                TypeInsurance = TypeInsurance,
            };
            var lstReport = await _insuranceServices.GetByConditionsIndemnify(GetToken(), indemnifyModel);
            List<Common.Entites.Insurance.ExcelInsuranceLender> lstData = new List<Common.Entites.Insurance.ExcelInsuranceLender>();
            foreach (var item in lstReport)
            {
                lstData.Add(new Common.Entites.Insurance.ExcelInsuranceLender()
                {
                    Address = item.Address,
                    ContractCode = item.ContractCode,
                    CustomerName = item.CustomerName,
                    FromDate = item.FromDate.ToString("dd/MM/yyyy"),
                    FullName = item.FullName,
                    InsuranceCode = item.InsuranceCode,
                    LinkGNCLender = item.LinkGNCLender,
                    LinkKH = item.LinkKH,
                    LoanID = item.LoanID.ToString(),
                    LoanTime = item.LoanTime.ToString(),
                    NumberCard = item.NumberCard.ToString(),
                    Represent = item.Represent,
                    SourceByInsurance = item.SourceByInsurance.ToString(),
                    StrBirthDay = item.BirthDay.ToString("dd/MM/yyyy"),
                    StrStatusInsurance = item.StrStatusInsurance,
                    ToDate = item.ToDate.ToString("dd/MM/yyyy"),
                    TotalInsuranceMoney = item.TotalInsuranceMoney.ToString("###,0"),
                    TotalInterest = item.TotalInterest.ToString("###,0"),
                    TotalMoneyCurrent = item.TotalMoneyCurrent.ToString("###,0"),
                    TotalMoneyDisbursement = item.TotalMoneyDisbursement.ToString("###,0"),
                });
            }
            var fileContents = _excelService.ExportExcelBase<Common.Entites.Insurance.ExcelInsuranceLender>(lstData);
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: $"_danhsachboithuongbaohiem-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx"
            );
        }

        public async Task<ActionResult> ExcelTemplateChangeStatus(string StrFromDate, string StrToDate, int LenderID,
           int TypeInsurance, int StatusSendInsurance, string CodeID, string CustomerName)
        {
            Common.Models.Insurrance.IndemnifyModel indemnifyModel = new Common.Models.Insurrance.IndemnifyModel()
            {
                CodeID = CodeID,
                CustomerName = CustomerName,
                LenderID = LenderID,
                StatusSendInsurance = StatusSendInsurance,
                StrFromDate = StrFromDate,
                StrToDate = StrToDate,
                TypeInsurance = TypeInsurance,
                page = "1",
                perpage = "1000000"
            };
            var lstReport = await _insuranceServices.GetByConditionsIndemnify(GetToken(), indemnifyModel);
            var fileContents = _excelService.ExcelTemplateChangeStatus(lstReport);
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: $"_templatebaohiem-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx"
            );
        }

        public async Task<ActionResult> ExcelExport(string TraceID, int CreateBy, int reportType = 1)
        {
            List<Common.Entites.ThnLoan.ExcelGetByTraceID> lstData = new List<Common.Entites.ThnLoan.ExcelGetByTraceID>();
            byte[] fileContents = null;
            try
            {
                var excelExportDetail = _excelExportData.FirstOrDefault(x => x.TypeReport == reportType);
                if (excelExportDetail != null)
                {
                    var lstDataExcel = await HttpContext.Session.GetObjectFromJson<object>($"{Constants.STATIC_EXCEL_LENDER}-{TraceID}-{CreateBy}");
                    string jsonData = _common.ConvertObjectToJSon(lstDataExcel);
                    fileContents = excelExportDetail.GetDataExcelReport(jsonData);
                }
            }
            catch (Exception ex)
            {
            }
            return File(fileContents: fileContents,
                      contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                      fileDownloadName: $"{((LMS.Common.Constants.ExcelReport_TypeReport)reportType).GetDescription()}-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx"
                  );
        }

        public async Task<ActionResult> ExcelByTraceID(string TraceID, int CreateBy)
        {
            Common.Models.ThnLoan.GetByTraceIDModel getByTraceIDModel = new Common.Models.ThnLoan.GetByTraceIDModel()
            {
                CreateBy = CreateBy,
                TraceIDRequest = TraceID,
            };
            var responseDataApi = await _insuranceServices.GetByTraceID(GetToken(), getByTraceIDModel);
            Common.Models.ResponseActionResult resultAction = new Common.Models.ResponseActionResult()
            {
                Result = -1
            };
            // -1 call lại api, 0: lỗi, 1: có data export
            // không check null vì khởi tạo đã set mặc định
            if (responseDataApi.Result == 1)
            {
                resultAction.Result = 0;
                resultAction.Message = responseDataApi.Message;
                if (responseDataApi.Data != null)
                {
                    resultAction.Result = 1;
                    HttpContext.Session.SetObjectAsJson($"{Constants.STATIC_EXCEL_LENDER}-{TraceID}-{CreateBy}", responseDataApi.Data);
                }
            }
            return Json(new { Data = resultAction });
        }
    }
}