﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Models.Invoice;
using FrontEnd.Common.Models.Ticket;
using FrontEnd.Common.Services.Invoice;
using FrontEnd.Common.Services.Lender;
using FrontEnd.Common.Services.Ticket;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Lender.Controllers
{
    public class AffController : BaseController
    {
        public AffController(IConfiguration configuration)
            : base(configuration)
        {

        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ReportRegisterLender()
        {
            return View();
        }
        public IActionResult ReportLender()
        {
            return View();
        }
        public IActionResult ReportTransaction()
        {
            return View();
        }
    }
}