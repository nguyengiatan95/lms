﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Models.Insurrance;
using FrontEnd.Common.Services.Insurance;
using FrontEnd.Common.Services.Lender;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Lender.Controllers
{
    public class IndemnifyController : BaseController
    {
        private IInsuranceServices _insuranceServices;
        public IndemnifyController(IConfiguration configuration, IInsuranceServices insuranceServices)
            : base(configuration)
        {
            _insuranceServices = insuranceServices;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<ActionResult> GetListDataIndemnify()
        {
            var modal = new IndemnifyModel(HttpContext.Request.Form);
            var lstData = await _insuranceServices.GetByConditionsIndemnify(GetToken(), modal);
            modal.total = lstData != null && lstData.Any() ? lstData[0].TotalCount.ToString() : "0";
            // Return
            return Json(new { meta = modal, data = lstData });
        }
    }
}