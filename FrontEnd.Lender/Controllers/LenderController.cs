﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Models.Invoice;
using FrontEnd.Common.Models.Ticket;
using FrontEnd.Common.Services.Invoice;
using FrontEnd.Common.Services.Lender;
using FrontEnd.Common.Services.Ticket;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Lender.Controllers
{
    public class LenderController : BaseController
    {
        private ILenderService _lenderService;
        private ITicketServices _ticketServices;
        private IInvoiceServices _invoiceServices;
        public LenderController(IConfiguration configuration, ILenderService lenderService, IInvoiceServices invoiceServices, ITicketServices ticketServices)
            : base(configuration)
        {
            _lenderService = lenderService;
            _invoiceServices = invoiceServices;
            _ticketServices = ticketServices;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<ActionResult> GetListData()
        {
            var dataRequest = Request.Form.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.ToString());
            var dicts = Request.Form.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.ToString());
            var request = dicts.ToNameValueCollection();
            TableAjaxBase tableAjax = new TableAjaxBase(request);

            string generalSearch = Request.Form["query[txt_search]"];
            int Status = string.IsNullOrEmpty(Request.Form["query[Status]"]) ? (int)LMS.Common.Constants.StatusCommon.SearchAll : int.Parse(Request.Form["query[Status]"]);
            int IsVerified = string.IsNullOrEmpty(Request.Form["query[IsVerified]"]) ? (int)LMS.Common.Constants.StatusCommon.SearchAll : int.Parse(Request.Form["query[IsVerified]"]);

            if (string.IsNullOrEmpty(tableAjax.field))
            {
                tableAjax.field = "LenderID";
                tableAjax.sort = "desc";
            }
            var lstData = await _lenderService.GetLenderList(GetToken(), generalSearch, Status, tableAjax.GetPageIndex, tableAjax.GetPageSize, isHub: 0, IsVerified);
            tableAjax.total = lstData != null && lstData.Any() ? lstData[0].TotalCount.ToString() : "0";
            return Json(new { meta = tableAjax, data = lstData });
        }
        public IActionResult FindInvoice()
        {
            return View();
        }

        public async Task<ActionResult> GetInvoiceLender()
        {
            var modal = new InvoiceDatatableModel(HttpContext.Request.Form);
            var lstInvoice = new List<LMS.Entites.Dtos.InvoiceService.InvoiceItemViewModel>();
            try
            {
                if (modal.MoneySearch == 0 && modal.InvoiceSubType == (int)LMS.Common.Constants.GroupInvoiceType.ReceiptCustomerConsider)
                {
                    return Json(new { meta = modal, data = lstInvoice });
                }
                var lstLoans = await _invoiceServices.GetInvoiceInfosByCondition(GetToken(), modal);
                if (lstLoans != null && lstLoans.Data != null)
                {
                    modal.total = lstLoans.Data.RecordsTotal.ToString();
                    lstInvoice = lstLoans.Data.Data;
                }
                return Json(new { meta = modal, data = lstInvoice });
            }
            catch (Exception)
            {
                return Json(new { meta = modal, data = new List<Common.Entites.Accountants.Invoice>() });
            }
        }

        [HttpGet]
        public IActionResult Ticket()
        {
            return View();
        }

        public async Task<ActionResult> GetTicket()
        {
            var modal = new TicketModel(HttpContext.Request.Form);
            var lstInvoice = new List<LMS.Entites.Dtos.InvoiceService.TicketListView>();
            try
            {
                var lstTicket = await _ticketServices.GetTicketConditions(GetToken(), modal);
                if (lstTicket != null && lstTicket.Data != null)
                {
                    modal.total = lstTicket.Data.RecordsTotal.ToString();
                    lstInvoice = lstTicket.Data.Data;
                }
                return Json(new { meta = modal, data = lstInvoice });
            }
            catch (Exception)
            {
                return Json(new { meta = modal, data = new List<Common.Entites.Accountants.Invoice>() });
            }
        }

        public IActionResult ListLender()
        {
            return View();
        }

        public IActionResult PaymentLender()
        {
            return View();
        }
    }
}