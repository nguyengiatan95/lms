﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Models.Invoice;
using FrontEnd.Common.Models.Management.User;
using FrontEnd.Common.Models.Ticket;
using FrontEnd.Common.Services.Invoice;
using FrontEnd.Common.Services.Lender;
using FrontEnd.Common.Services.Ticket;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Lender.Controllers
{
    public class UserController : BaseController
    {
        public UserController(IConfiguration configution) : base(configution)
        {
        }
        public async Task<IActionResult> ChangePassword(int t = 0)
        {
            var user = await HttpContext.Session.GetObjectFromJson<UserInfoWithMenuModel>(Constants.STATIC_USERMODEL);
            ViewBag.Active = t;
            return View(user);
        }
    }
}