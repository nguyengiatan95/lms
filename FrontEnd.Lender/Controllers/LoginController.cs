﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models.Management.User;
using FrontEnd.Common.Services.Login;
using FrontEnd.Common.Services.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace FrontEnd.Lender.Controllers
{
    public class LoginController : BaseController
    {
        // GET: LoginController 
        private readonly ILoginService _loginService;
        private readonly IUserService _user;
        public LoginController(IConfiguration configuration, ILoginService loginService, IUserService user):base(configuration)
        {
            _loginService = loginService;
            _user = user; 
        }
        public ActionResult Index()
        {
            return View();
        }
        [Route("logout.html")]
        public IActionResult Logout()
        {
            HttpContext.Session.SetObjectAsJson(Constants.STATIC_USERMODEL, null);
            Response.Cookies.Delete(Constants.STATIC_USERMODEL);
            return Redirect(_baseConfig[Constants.STATIC_LogOut_Admin_URL]);
        }
        [Route("redirect")]
        [HttpGet]
        public async Task<IActionResult> RedirectIndex(string token)
        {
            try
            {
                //Validate Token
                if (string.IsNullOrEmpty(token))
                    return Redirect(_baseConfig[Constants.STATIC_Login_URL]);
                var user = await _user.GetUserByUserID(token, 1);
                if (user == null || user.UserID == 0)
                    return Redirect(_baseConfig[Constants.STATIC_Login_URL]);
                var userLoginModel = new UserLoginModel { UserID = user.UserID, UserName = user.UserName, FullName = user.FullName, Token = token };
                userLoginModel.FullName = user.FullName;
                userLoginModel.UserName = user.UserName;
                userLoginModel.ApiUrl = _baseConfig[Constants.STATIC_ApiUrl_URL];
                userLoginModel.LoginUrl = _baseConfig[Constants.STATIC_Login_URL];
                userLoginModel.AdminUrl = _baseConfig[Constants.STATIC_HOME_URL];
                HttpContext.Session.SetObjectAsJson(Constants.STATIC_USERMODEL, userLoginModel);
                SetCookie(Constants.STATIC_USERMODEL, JsonConvert.SerializeObject(userLoginModel), 12); 
                return Redirect(Constants.STATIC_HOME_LENDER);
            }
            catch (Exception)
            {
                return Redirect(_baseConfig[Constants.STATIC_Login_URL]);
            }
        }
        //private void SetCookie(string key, string value, int? expireTime)
        //{
        //    CookieOptions option = new CookieOptions();
        //    if (expireTime.HasValue)
        //        option.Expires = DateTime.Now.AddHours(expireTime.Value);
        //    else
        //        option.Expires = DateTime.Now.AddHours(12);
        //    Response.Cookies.Append(key, value, option);
        //}
    }
}
