﻿var isClearSession = false;
var access_token = "";
var USER_ID = 0;
var USER_TYPE = 0;
var AdminUrl = '';
var baseCommon = new function () {
    var Option = {
        All: -111,
        Pagesize: 10
    }
    var BaseUrl, Endpoint, UserLoginModel = {}, isCalledRefreshToken = "", LoginUrl_ = LoginUrl;
    var SetBase = function () {
        BaseUrl = {
            "Admin": ApiUrl + "admin/",
            "Loan": ApiUrl + "loan/",
            "Authen": ApiUrl + "authen/",
            "Invoice": ApiUrl + "invoice/",
            "Lender": ApiUrl + "lender/",
            /*  "Lender":"http://localhost:63919/",*/
            "Customer": ApiUrl + "customer/",
            "Transaction": ApiUrl + "transaction/",
            "Thn": ApiUrl + "thn/"
        };
        Endpoint = {
            "GetLoanByID": BaseUrl["Loan"] + "api/Loan/getLoanByID/",
            "GetLstLoanInfoByCondition": BaseUrl["Loan"] + "api/Loan/GetLstLoanInfoByCondition",
            "GetLstPaymentScheduleByLoanID": BaseUrl["Loan"] + "api/loan/GetLstPaymentScheduleByLoanID/",
            "GetListProductCredit": BaseUrl["Loan"] + "api/Meta/GetListProductCredit",
            "GetLenderSearch": BaseUrl["Lender"] + "api/Lender/GetLenderSearch",
            "ExtendLoanTime": BaseUrl["Loan"] + "api/loan/ExtendLoanTime",
            "GenPaymentScheduleByLoanID": BaseUrl["Loan"] + "api/loan/GenPaymentScheduleByLoanID",
            "GetDetailBankCard": BaseUrl["Loan"] + "api/Meta/GetBank/",
            "GetInvoiceInfosByConditionTableAjax": BaseUrl["Invoice"] + "api/Invoice/GetInvoiceInfosByConditionTableAjax",
            "getstasticloanstatusbyshop": BaseUrl["Loan"] + "api/report/getstasticloanstatusbyshop",
            "GetLstLoanByFilterCustomer": BaseUrl["Loan"] + "api/loan/GetLstLoanByFilterCustomer",
            "PayPartialScheduleInPeriods": BaseUrl["Loan"] + "api/loan/PayPartialScheduleInPeriods",
            "GetHistoryCommentLos": BaseUrl["Loan"] + "api/loan/GetHistoryCommentLos/",
            "GetDataImagesLos": BaseUrl["Loan"] + "api/loan/GetListImagesLos/",
            "GetTransactionByLoanID": BaseUrl["Loan"] + "api/loan/GetTransactionByLoanID/",
            "PaymentMoneyFullSchedule": BaseUrl["Loan"] + "api/loan/PaymentMoneyFullSchedule/",
            "GetHistoryCommentLender": BaseUrl["Loan"] + "api/CommentIndemnifyInsurrance/GetHistoryComment/",
            "GetBankCard": BaseUrl["Loan"] + "api/Meta/GetBankCard?BankCardID=",
            "CreateInvoiceCustomer": BaseUrl["Invoice"] + "api/Invoice/CreateInvoiceCustomer",
            "CreateInvoicePaySlipCustomer": BaseUrl["Invoice"] + "api/Invoice/CreateInvoicePaySlipCustomer",
            "CreateInvoiceBackMoneyCustomer": BaseUrl["Invoice"] + "api/Invoice/CreateInvoiceBackMoneyCustomer",
            "CreateInvoiceConsiderCustomer": BaseUrl["Invoice"] + "api/Invoice/CreateInvoiceConsiderCustomer",
            "CreateInvoiceOther": BaseUrl["Invoice"] + "api/Invoice/CreateInvoiceOther",
            "CreateInvoiceOnBehalfShop": BaseUrl["Invoice"] + "api/Invoice/CreateInvoiceOnBehalfShop",
            "CreateInvoiceBankInterest": BaseUrl["Invoice"] + "api/Invoice/CreateInvoiceBankInterest",
            "CreateInvoiceInternal": BaseUrl["Invoice"] + "api/Invoice/CreateInvoiceInternal",
            "CreateInvoiceLender": BaseUrl["Invoice"] + "api/Invoice/CreateInvoiceLender",
            "SearchInvoiceAdvanceSlipLender": BaseUrl["Invoice"] + "api/Invoice/SearchInvoiceAdvanceSlipLender",
            "GetInvoiceInfosByCondition": BaseUrl["Invoice"] + "api/Invoice/GetInvoiceInfosByCondition",
            //lender
            "CreateInfoLender": BaseUrl["Lender"] + "api/Lender/CreateInfoLender",
            "UpdateInfoLender": BaseUrl["Lender"] + "api/Lender/UpdateInfoLender",
            "GetAllBank": BaseUrl["Loan"] + "api/Meta/GetBank/0",
            "UserByGroup": BaseUrl["Loan"] + "api/Meta/GetUserGroup/",
            "AffLender": BaseUrl["Loan"] + "api/Meta/GetAff/",
            "SendStatusInsurance": BaseUrl["Loan"] + "api/Loan/SendStatusInsurance/",
            "SaveCommentLender": BaseUrl["Loan"] + "api/CommentIndemnifyInsurrance/SaveComment",
            "DisbursementWaitingLOS": BaseUrl["Loan"] + "api/LOS/DisbursementWaitingLOS",
            "GetLender": BaseUrl["Loan"] + "api/Meta/GetLender",
            "GetHistoryCommentLos2": BaseUrl["Loan"] + "api/LOS/GetHistoryCommentLos",
            "SaveCommentHistoryLos": BaseUrl["Loan"] + "api/LOS/SaveCommentHistory",
            "LockLoan": BaseUrl["Loan"] + "api/LOS/LockLoan",
            "DisbursementLoan": BaseUrl["Loan"] + "api/LOS/DisbursementLoan",
            "PushLoanLender": BaseUrl["Loan"] + "api/LOS/PushLoanLender",
            "GetLoanCreditDetailLos": BaseUrl["Loan"] + "api/LOS/GetLoanCreditDetailLos/",
            "GetDataImagesLenderLos": BaseUrl["Loan"] + "api/LOS/GetListImagesLos/",
            "GetHistoryCommentLenderLos": BaseUrl["Loan"] + "api/LOS/GetHistoryCommentLos/",
            "GetHistoryCommentInvoice": BaseUrl["Invoice"] + "api/Invoice/GetHistoryCommentInvoice",
            "VerifyInvoiceCustomer": BaseUrl["Invoice"] + "api/Invoice/VerifyInvoiceCustomer",
            "VerifyInvoiceConsiderCustomer": BaseUrl["Invoice"] + "api/Invoice/VerifyInvoiceConsiderCustomer",
            "GetDepartment": BaseUrl["Admin"] + "api/Department/GetDepartment",
            "CreateTicket": BaseUrl["Invoice"] + "api/Ticket/CreateTicket",
            "UpdateTicket": BaseUrl["Invoice"] + "api/Ticket/UpdateTicket",
            "ChangeStatusTicket": BaseUrl["Invoice"] + "api/Ticket/ChangeStatusTicket",
            "ReturnLoanLender": BaseUrl["Loan"] + "api/LOS/ReturnLoan",
            "CreateLoanForDisbursementLoanCreditOfAppLender": BaseUrl["Loan"] + "api/Loan/CreateLoanForDisbursementLoanCreditOfAppLender",
            "GetLenderList": BaseUrl["Lender"] + "api/Lender/GetLenderList",
            "GetReportInsurance": BaseUrl["Lender"] + "api/Lender/GetReportInsurance",
            "GetTicketByConditions": BaseUrl["Invoice"] + "api/Ticket/GetTicketByConditions",
            "GetRefreshToken": "/Home/GetRefreshToken",
            "GetListProvince": BaseUrl["Thn"] + "api/meta/getlstcity",
            "GetDistrictByCityID": BaseUrl["Thn"] + "api/meta/GetDistrictByCityID/",
            "GetWardByDistrictID": BaseUrl["Thn"] + "api/meta/GetWardByDistrictID/",
            "CreateLenderUser": BaseUrl["Lender"] + "api/Lender/CreateLenderUser",
            "GetLenderUserConditions": BaseUrl["Lender"] + "api/Lender/GetLenderUserConditions",
            "UpdateLenderUser": BaseUrl["Lender"] + "api/Lender/UpdateLenderUser",
            "CreateLenderDigitalSignature": BaseUrl["Lender"] + "api/Lender/CreateLenderDigitalSignature",
            "UpdateLenderSpicesConfig": BaseUrl["Lender"] + "api/Lender/UpdateLenderSpicesConfig",
            "GetLenderSpicesConfig": BaseUrl["Lender"] + "api/Lender/GetLenderSpicesConfig/",
            "GetContractByUserLender": BaseUrl["Lender"] + "api/UserLender/GetContractByUserLender/",
            "CreateUpdateLendercontract": BaseUrl["Lender"] + "api/Lender/CreateUpdateLendercontract",
            "VerifyUserLender": BaseUrl["Lender"] + "api/Lender/VerifyUserLender",
            "GetLenderSignature": BaseUrl["Lender"] + "api/Lender/GetLenderSignature/",
            "ChangeStatusLenderUser": BaseUrl["Lender"] + "api/Lender/ChangeStatusLenderUser",
            "ResetPassword": BaseUrl["Lender"] + "api/Lender/ResetPassword",
            "PaymentLender": BaseUrl["Lender"] + "api/Lender/PaymentLender",
            "GetAffList": BaseUrl["Lender"] + "api/Aff/GetAffList",
            "CreateOrUpdateAff": BaseUrl["Lender"] + "api/Aff/CreateOrUpdateAff",
            "DeleteAff": BaseUrl["Lender"] + "api/Aff/DeleteAff",
            "GetMaxCode": BaseUrl["Lender"] + "api/Aff/GetMaxCode",
            "LenderStaff": BaseUrl["Lender"] + "api/Lender/LenderStaff/",
            "ListCollaboratorLender": BaseUrl["Lender"] + "api/CollaboratorLender/ListCollaboratorLender",
            "CannelCollaboratorLender": BaseUrl["Lender"] + "api/CollaboratorLender/CannelCollaboratorLender",
            "ApprovedCollaboratorLender": BaseUrl["Lender"] + "api/CollaboratorLender/ApprovedCollaboratorLender",
            "ReportRegisterLender": BaseUrl["Lender"] + "api/Aff/getlstLenderInfoByConditions",
            "GetLstSelectAff": BaseUrl["Lender"] + "api/aff/GetLstSelectAff",
            "GetLinkAff": BaseUrl["Lender"] + "api/Aff/GetLinkAff",

            "ChangePassword": BaseUrl["Admin"] + "api/User/ChangePassword",
            "GetReportTransactionOfLender": BaseUrl["Lender"] + "api/CollaboratorLender/GetReportTransactionOfLender",
            "GetLoanDeposit": BaseUrl["Lender"] + "api/Lender/GetLoanDeposit",
            "AddRequestExcel": BaseUrl["Lender"] + "api/Excel/AddRequestExcel",
            "ExcelByTraceID": "/Excel/ExcelByTraceID",
            "UpdateStatusDeposit": BaseUrl["Lender"] + "api/Lender/UpdateStatusDeposit",
            "GetHistoryDepositLender": BaseUrl["Lender"] + "api/Lender/GetHistoryDepositLender/",
            "CommentDeposit": BaseUrl["Lender"] + "api/Lender/CommentDeposit",
            "GetInsuranceClaimInformation": BaseUrl["Lender"] + "api/Lender/GetInsuranceClaimInformation",
            "GetPrintComment": BaseUrl["Lender"] + "api/Lender/GetPrintComment",
        };
    }
    var LoginPage = function () {
        var referrer = window.location.href;
        referrer = referrer.replace(':', '>').replace(/\//gi, '<').replace('&', '*');
        $.removeCookie(STATIC_USERMODEL, { path: '/' });
        window.location = LoginUrl + referrer;
    };
    var HomePage = function () {
        window.open(AdminUrl, "_self");
    };
    var ChangePassWord = function () {
        var urlChangePass = "/User/ChangePassword";
        window.open(urlChangePass, "_self");
    };
    var IconWaiting = function (isVisible, id) {
        if (!isVisible) {
            $(id).addClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
        } else {
            $(id).removeClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
        }
    }
    var InitUser = new Promise(function (myResolve, myReject) {

        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                if (userModel != null && userModel != "") {
                    var user = JSON.parse(userModel);
                    access_token = user.Token;
                    AdminUrl = user.AdminUrl;
                    USER_ID = user.UserID;
                    USER_TYPE = user.UserType;
                    SetBase();
                    myResolve(user);
                } else {
                    myReject("Error");
                    LoginPage();
                }
            } else {
                LoginPage();
            }
        } catch (err) {
            LoginPage();
        }
    });
    var AjaxDone = function (method, endpoint, data, done, error, errFuntion) {
        return $.ajax({
            type: method,
            headers: {
                'Authorization': access_token
            },
            url: endpoint,
            data: data,
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).done(done).fail(function (e) {
            if (e.status == 401) {
                LoginPage();
            } else if (e.status == 403) {
                var params = (new URL(endpoint)).pathname;
                baseCommon.ShowErrorNotLoad("Vui lòng liên hệ quản lý để được phân quyền chức năng!<br>" + params);
            } else if (errFuntion == null || errFuntion == "") {
                eval(errFuntion);
            } else {
                if (error == null || error == "") {
                    error = e;
                }
                baseCommon.ShowErrorNotLoad(error);
            }
        });
    };
    var AjaxSuccess = function (method, endpoint, data, success, error) {
        return $.ajax({
            type: method,
            headers: {
                'Authorization': access_token
            },
            url: endpoint,
            data: data,
            contentType: "application/json; charset=utf-8",
            success: success,
            error: function (e) {
                debugger
                if (e.status == 401) {
                    LoginPage();
                } else if (e.status == 403) {
                    var params = (new URL(endpoint)).pathname;
                    baseCommon.ShowErrorNotLoad("Vui lòng liên hệ quản lý để được phân quyền chức năng!<br>" + params);
                } else {
                    if (error == null || typeof error == "undefined" || error == "") {
                        error = e.responseText;
                    }
                    baseCommon.ShowErrorNotLoad(error);
                }
            }
        });
    };
    var SelectDataSource = function (url, element, placeholder, value, name, selectedId, data) {
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            processData: false,
            contentType: false,
        }).done(function (respone) {
            $(element).html('');
            $(element).append('<option value="">' + placeholder + '</option>');
            $.each(respone.data, function (key, item) {
                var selected = "";
                if (Array.isArray(selectedId)) {
                    if (selectedId.find((itemSelected) => itemSelected[value] === item[value])) {
                        selected = "selected";
                    }
                } else {
                    if (item[value] == selectedId) {
                        selected = "selected";
                    }
                }
                $(element).append('<option ' + selected + ' value="' + item[value] + '">' + item[name] + '</option>');
            });
        });
    };
    function diff_minutes(dt2, dt1) {

        var diff = (dt2.getTime() - dt1.getTime()) / 1000;
        diff /= 60;
        return Math.round(diff);

    }
    var RefreshToken = function () {
        //ngủ cho tới khi gần hết time Token
        //mở tab mới gọi chức năng check token
        // check token nếu còn dài hạn trả lại ngay = cách gọi lại tất cả các trang 
        // nếu hết hạn thì refresh token và gọi lại tất cả các trang
        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                var user = JSON.parse(userModel);
                var remainTime = diff_minutes(new Date(user.TimeExpired), new Date());
                var waittime = 3;
                if (remainTime < waittime) {
                    //gọi RefreshToken
                    setTimeout(() => { RefreshToken(); }, 10000);
                    if (isCalledRefreshToken == "") {
                        isCalledRefreshToken = "called";
                        baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetRefreshToken, "", function (respone) {
                            if (respone.status == 1) {
                                access_token = respone.data.token;
                                user.Token = respone.data.token;
                                user.TimeExpired = respone.data.timeExpired;
                                user.TimeExpiredString = respone.data.timeExpiredString;
                                $.cookie(STATIC_USERMODEL, JSON.stringify(user), { expires: 30, path: '/', domain: user.DomainCookie, secure: true });
                            } else {
                                ShowErrorNotLoad("Phiên làm việc sắp hết hạn, vui lòng F5 thử lại!");
                            }
                            isCalledRefreshToken = "";
                        }, "Phiên làm việc sắp hết hạn, vui lòng F5 thử lại!");
                    }
                } else {
                    var timeOut = ((remainTime - waittime) + 1) * 60 * 1000;
                    setTimeout(() => {
                        isCalledRefreshToken = "";
                        RefreshToken();
                    }, timeOut);
                }
            } else {
                setTimeout(() => { RefreshToken(); }, 2000);
            }
        } catch (err) {
            setTimeout(() => { RefreshToken(); }, 2000);
        }
    }
    var Init = function () {
        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                var userModel = $.cookie(STATIC_USERMODEL);
                if (userModel != null && userModel != "") {
                    var user = JSON.parse(userModel);
                    console.log(user);
                    AdminUrl = user.AdminUrl;
                    STATIC_USERMODEL = user.CookieName;
                    UserLoginModel = user.UserLoginModel;
                    RefreshToken();
                    SetBase();
                    var fullShortName = "";
                    if (user != null && user.FullName != null) {
                        fullShortName = user.FullName.split(" ");
                    }
                    var shortName = fullShortName[(fullShortName.length - 1)];
                    if (fullShortName.length >= 2) {
                        shortName = fullShortName[(fullShortName.length - 2)] + " " + shortName;
                    }
                    $('#_UserFullNameId').html(shortName);
                    $('#_UserFullNameShortId').html(user.UserName.substring(0, 1).toUpperCase());
                    $('#_UserFullNameShort2Id').html(user.UserName.substring(0, 1).toUpperCase());
                    $('#lbl_header_fullname').html(user.UserName);
                    USER_TYPE = user.UserType;
                } else {
                    // LoginPage();
                    return null;
                }
            } else {
                LoginPage();
            }
        } catch (e) {
            LoginPage();
        }

    }
    var Pagesize = function () {
        return 20;
    };
    var GetToken = function () {
        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                var user = JSON.parse(userModel);
                return user.Token;
            } else {
                LoginPage();
                return null;
            }
        } catch (e) {
            LoginPage();
        }
    };
    var GetUser = function () {
        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                var user = JSON.parse(userModel);
                return user;
            } else {
                LoginPage();
                return null;
            }
        } catch (e) {
            LoginPage();
        }
    };
    var GetDataSource = function (url, data, done) {
        return $.ajax({
            type: "POST",
            url: url,
            data: data,
            processData: false,
            contentType: false,
        }).done(done);
    };
    var DataSource = function (element, placeholder, value, name, selectedId, data) {
        try {
            $(element).html('');
            $(element).append('<option value="-111">' + placeholder + ' </option>');
            $.each(data, function (key, item) {
                var selected = "";
                if (Array.isArray(selectedId)) {
                    if (selectedId.find((itemSelected) => itemSelected[value] === item[value])) {
                        selected = "selected";
                    }
                } else {
                    if (item[value] == selectedId) {
                        selected = "selected";
                    }
                }
                $(element).append('<option ' + selected + ' value="' + item[value] + '">' + item[name] + '</option>');
            });
        } catch (e) {

        }
    };

    var FormatCurrency = function FormatCurrency(Num) { //function to add commas to textboxes
        Num += '';
        Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
        Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
        x = Num.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1))
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        return x1 + x2;
    }
    var ShowErrorNotLoad = function (message) {
        toastr.error(message);
    }
    var ShowSuccess = function (message) {
        toastr.success(message);
    }
    function formatNumber(nStr, decSeperate, groupSeperate) {
        nStr += '';
        x = nStr.split(decSeperate);
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + groupSeperate + '$2');
        }
        return x1 + x2;
    }
    function FormatDate(dateFormat, format) {
        var date = moment(dateFormat);
        return date.format(format);// DD/MM/YYYY HH:mm
    }
    function FormatRepo(repo) {

        if (repo.loading) return repo.text;
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__title'>" + repo.text + "</div>";
        return markup;
    }

    function FormatRepoSelection(repo) {

        return repo.text;
    }
    function RepoConvert(data) {

        var newObject = [];
        if (data != null && data != "") {
            $.each(data, function (key, value) {
                var newRepo = { id: value.ID, text: value.FullName };
                newObject.push(newRepo);
            });
        }
        return newObject;
    }
    var ButtonSubmit = function (isVisible, id) {
        if (!isVisible) {
            $(id).addClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
            $(id).prop("disabled", true);
        } else {
            $(id).removeClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
            $(id).prop("disabled", false);
        }
    }
    return {
        Init: Init,
        SelectDataSource: SelectDataSource,
        DataSource: DataSource,
        GetDataSource: GetDataSource,
        HomePage: HomePage,
        FormatCurrency: FormatCurrency,
        //SetToken: SetToken,
        Endpoint: Endpoint,
        GetToken: GetToken,
        ShowErrorNotLoad: ShowErrorNotLoad,
        ShowSuccess: ShowSuccess,
        formatNumber: formatNumber,
        Pagesize: Pagesize,
        //AjaxDoneFail: AjaxDoneFail,
        //AjaxPost: AjaxPost,
        //AjaxGet: AjaxGet,
        //AjaxGetPromise: AjaxGetPromise,
        //ServicesAjaxDone: ServicesAjaxDone,
        AjaxDone: AjaxDone,
        AjaxSuccess: AjaxSuccess,
        FormatDate: FormatDate,
        LoginUrl: LoginUrl_,
        FormatRepo: FormatRepo,
        FormatRepoSelection: FormatRepoSelection,
        RepoConvert: RepoConvert,
        Option: Option,
        GetUser: GetUser,
        InitUser: InitUser,
        ButtonSubmit: ButtonSubmit,
        IconWaiting: IconWaiting,
        ChangePassWord: ChangePassWord,
        RefreshToken: RefreshToken
    }
}
$(document).ready(function () {
    baseCommon.Init();
});