﻿
var affList = function () {
    var Option = {
        loadInfoAff: 1,
        Delete: 2
    };
    var rowClicked = Option.LoadInfoLender;
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var GeneralSearch = $('#txt_search').val();
                var Status = $('#sl_search_status').val();
                var FromDate = $('#txt_search_fromDate').val();
                var ToDate = $('#txt_search_toDate').val();
                var data = {
                    GeneralSearch: GeneralSearch,
                    Status: parseInt(Status),
                    FromDate: FromDate,
                    ToDate: ToDate,
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetAffList, JSON.stringify(data), function (res) {
                    if (res.Result == 1) {
                        options.success(res);
                    } else {
                        options.success(null);
                    }
                }, "Thao tác thất bại, vui lòng thử lại sau!");
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = 0;
                if (response != null && response.Total != null && response.Total > 0) {
                    total = response.Total;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    function Grid_change(e) {
        var selectedRows = this.select();
        var selectedDataItems = [];
        for (var i = 0; i < selectedRows.length; i++) {
            var dataItem = this.dataItem(selectedRows[i]);
            selectedDataItems.push(dataItem);
        }
        // selectedDataItems contains all selected data items
        gridSelectedRowData = selectedDataItems[0];
        setTimeout(function () {
            var jsonData = JSON.stringify(gridSelectedRowData);
            if (rowClicked == Option.loadInfoAff) {
                affList.loadInfoAff(jsonData);
            }
            rowClicked = Option.loadInfoAff;
        }, 5);
    }
    var InitGrid = function (objQuery) {
        $("#grid").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [10, 20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            change: Grid_change,
            selectable: "row",
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #",
                    width: 90
                },
                {
                    field: 'AffName',
                    title: 'Họ tên',
                    textAlign: 'left',
                },
                {
                    field: 'Code',
                    title: 'Mã',
                    textAlign: 'left',
                    template: function (row) {
                        // callback function support for column rendering
                        return '<a class="kt-font-primary  kt-link"  title="Sửa" data-toggle="modal" data-target="#modal_add_edit_aff" onclick="affList.loadInfoAff(\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">' + row.Code + '</a>';
                    },
                },
                {
                    field: 'Phone',
                    title: 'Số điện thoại',
                    textAlign: 'left',
                },
                {
                    field: 'StaffName',
                    title: 'Chuyên viên',
                    textAlign: 'left',
                },
                {
                    field: 'ContractDate',
                    title: 'Ngày kí HĐ',
                    textAlign: 'left',
                    template: function (row) {
                        return row.ContractDate == null ? "" : formattedDateHourMinutes(row.ContractDate);
                    },
                },
                {
                    field: 'FinishContractDate',
                    title: 'Ngày hết hạn',
                    textAlign: 'left',
                    template: function (row) {
                        return row.FinishContractDate == null ? "" : formattedDateHourMinutes(row.FinishContractDate);
                    },
                },
                {
                    field: 'TotalLender',
                    title: 'Số lượng NDT',
                    textAlign: 'right',
                },
                {
                    field: 'Status',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    template: function (row) {
                        if (row.Status == 1) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">' + row.StrStatus + '</span>';
                        }
                        else if (row.Status == 0) {
                            return '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">' + row.StrStatus + '</span>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + row.StrStatus + '</span>';
                        }
                    },
                },
                {
                    title: 'Chức năng',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += '<a class="btn btn-primary btn-icon btn-sm" title="Sửa" data-toggle="modal" data-target="#modal_add_edit_aff" onclick="affList.RowClick(' + affList.Option.loadInfoAff + ',\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                                    <i class="fa fa-edit"></i>\
                                 </a>\
                                <a class="btn btn-danger btn-icon btn-sm" title="Xóa" onclick="affList.RowClick(' + affList.Option.Delete + ',\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                                    <i class="fa fa-trash"></i>\
                                 </a>';
                        return html;
                    }
                }
            ]
        });
    };
    var RowClick = function (rowClick, row) {
        rowClicked = rowClick;
        var data = JSON.parse(row);
        console.log(data);
        if (rowClicked == Option.loadInfoAff) {
            affList.loadInfoAff(row);
        } else if (rowClicked == Option.Delete) {
            affList.deleteAff(data.AffID, data.AffName);
        }
    };
    var GridRefresh = function () {
        var grid = $("#grid").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };
    var loadInfoAff = function (jsonData = '') {
        if (jsonData == '') {
            $('#hdd_AffID').val(0);
            loadMaxCode();
            $('#headerId').html('Thêm mới CTV');
            $('#txt_create_Address').val('');
            $('#txt_create_AffName').val('');
            $('#txt_create_StrBirthDay').val('');
            $('#txt_create_Phone').val('');
            $('#txt_create_Email').val('');
            $('#txt_create_NumberCard').val('');
            $('#txt_create_TaxNumber').val('');
            $('#txt_create_PercentCommision').val(0);
            $('#txt_create_ContractDate').val('');
            $('#txt_create_FinishContractDate').val('');
            $('#txt_create_BankNumber').val('');
            $('#txt_create_BankPlace').val('');
            $('#txt_create_ContractDate').val('');
            $('#sl_create_Bank').val(-111).trigger('change');
            $('#sl_create_StaffID').val(-111).trigger('change');
            $('#btn_save').html('<i class="fa fa-save"></i>Thêm mới');
        } else {
            var data = JSON.parse(jsonData);
            $('#headerId').html('Cập nhật CTV');
            $('#hdd_AffID').val(data.AffID);
            $('#txt_create_FullName').val(data.Code);
            $('#txt_create_Address').val(data.Address);
            $('#txt_create_AffName').val(data.AffName);
            $('#txt_create_StrBirthDay').val(data.BirthDay == null ? "" : formattedDate(data.BirthDay));
            $('#txt_create_Phone').val(data.Phone);
            $('#txt_create_Email').val(data.Email);
            $('#txt_create_NumberCard').val(data.NumberCard);
            $('#txt_create_TaxNumber').val(data.TaxNumber);
            $('#txt_create_PercentCommision').val(data.PercentComission);
            $('#txt_create_ContractDate').val(data.ContractDate == null ? "" : formattedDate(data.ContractDate));
            $('#txt_create_FinishContractDate').val(data.FinishContractDate == null ? "" : formattedDate(data.FinishContractDate));
            $('#txt_create_BankNumber').val(data.BankNumber);
            $('#txt_create_BankPlace').val(data.BankPlace);
            $('#txt_create_ContractDate').val(data.ContractDate == null ? "" : formattedDate(data.ContractDate));
            $('#sl_create_Bank').val(data.BankID == -111 ? 0 : data.BankID).trigger('change');
            $('#sl_create_StaffID').val(data.StaffID == -111 ? 0 : data.StaffID).trigger('change');
            $('#btn_save').html('<i class="fa fa-save"></i>Cập nhật');
        }
    };
    var deleteAff = function (affId, affName) {
        swal.fire({
            html: "Bạn có chắc chắn muốn xóa CTV <b>" + affName + "<b>?",
            type: "warning",
            showCancelButton: !0,
            cancelButtonText: "Hủy",
            confirmButtonText: "Đồng ý"
        }).then(function (result) {
            if (result.value) {
                var request = {
                    "AffID": parseInt(affId),
                };
                baseCommon.AjaxDone("POST", baseCommon.Endpoint.DeleteAff, JSON.stringify(request), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        GridRefresh()
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                }, "Xóa CTV không thành công vui lòng thử lại!");
            }
        });
    }
    var SumitFormSave = function () {
        $('#btn_save').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    Code: {
                        required: true,
                    },
                    AffName: {
                        required: true,
                    },
                    Phone: {
                        required: true,
                    },
                    Email: {
                        required: true,
                    },
                    StaffID: {
                        required: true,
                    },
                    StaffID: {
                        required: true,
                        min: 1
                    },
                    //ContractDate: {
                    //    required: true,
                    //},
                    //FinishContractDate: {
                    //    required: true,
                    //},
                },
                messages: {
                    Code: {
                        required: "Vui lòng nhập mã CTV",
                    },
                    AffName: {
                        required: "Vui lòng nhập tên CTV",
                    },
                    Phone: {
                        required: "Vui lòng nhập SĐT",
                    },
                    Email: {
                        required: "Vui lòng nhập Email",
                    },
                    StaffID: {
                        required: "Vui lòng chọn nhân viên chăm sóc",
                        min: "Vui lòng chọn nhân viên chăm sóc"
                    },
                    //ContractDate: {
                    //    required: "Vui lòng chọn ngày kí HĐ",
                    //},
                    //FinishContractDate: {
                    //    required: "Vui lòng chọn ngày kết thúc HĐ",
                    //},
                }
            });
            if (!form.valid()) {
                return;
            }
            var request = {
                Code: $("#txt_create_FullName").val(),
                Address: $("#txt_create_Address").val(),
                Phone: $("#txt_create_Phone").val(),
                Email: $("#txt_create_Email").val(),
                AffName: $("#txt_create_AffName").val(),
                BirthDay: $("#txt_create_StrBirthDay").val(),
                CreateBy: USER_ID,
                ContractDate: $("#txt_create_ContractDate").val(),
                NumberCard: $("#txt_create_NumberCard").val(),
                AffID: parseInt($("#hdd_AffID").val()),
                FinishContractDate: $("#txt_create_FinishContractDate").val(),
                TaxNumber: $("#txt_create_TaxNumber").val(),
                PercentComission: parseFloat($("#txt_create_PercentCommision").val()),
                BankNumber: $("#txt_create_BankNumber").val(),
                BankID: parseInt($("#sl_create_Bank").val() == null ? "0" : $("#sl_create_Bank").val()),
                BankCode: $("#txt_create_FinishContractDate").val(),
                BankPlace: $("#txt_create_BankPlace").val(),
                StaffID: parseInt($("#sl_create_StaffID").val() == null ? "0" : $("#sl_create_StaffID").val()),
            }
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            baseCommon.AjaxDone("POST", baseCommon.Endpoint.CreateOrUpdateAff, JSON.stringify(request), function (respone) {
                if (respone.Result == 1) {
                    baseCommon.ShowSuccess(respone.Message);
                    $('#btnGetData').trigger('click');
                    $('#modal_add_edit_aff').modal('hide');
                } else {
                    baseCommon.ShowErrorNotLoad(respone.Message);
                }
            });
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
        });
    };
    var loadMaxCode = function () {
        baseCommon.AjaxDone('GET', baseCommon.Endpoint.GetMaxCode, null, function (res) {
            if (res.Result == 1) {
                $('#txt_create_FullName').val(res.Data);
            }
        }, "Thao tác thất bại, vui lòng thử lại sau!");
    }
    var Init = function () {
        GridRefresh();
        $('#sl_search_status').on('change', function () {
            GridRefresh();
        });
        $('#btnGetData').on('click', function () {
            GridRefresh();
        });
        $("#txt_search").on('keypress', function (e) {
            if (e.which === 13) {
                GridRefresh();
            }
        });
        SumitFormSave();
    };
    return {
        Init: Init,
        Option: Option,
        RowClick: RowClick,
        loadInfoAff: loadInfoAff,
        deleteAff: deleteAff,
        loadMaxCode: loadMaxCode
    };
}();
