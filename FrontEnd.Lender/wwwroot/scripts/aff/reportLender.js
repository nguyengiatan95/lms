﻿var reportLender = function () {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var GeneralSearch = $('#txt_search').val();
                var Status = parseInt($('#sl_search_status').val());
                var FromDate = $('#txt_search_fromDate').val();
                var ToDate = $('#txt_search_toDate').val();
                var AffID = $("#sl_search_ctv").val();
                if (typeof AffID == undefined || AffID == null || AffID == "") {
                    AffID = baseCommon.Option.All;
                }
                var data = {
                    GeneralSearch: GeneralSearch,
                    Status: parseInt(Status),
                    FromDate: FromDate,
                    ToDate: ToDate,
                    AffID: parseInt(AffID),
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.ReportRegisterLender, JSON.stringify(data), function (res) {
                    if (res.Result == 1) {
                        options.success(res);
                        TotalLenderRegisterByLink = 0;
                        TotalLenderHasContract = 0;
                        if (res.Data.length > 0) {
                            TotalLenderRegisterByLink = res.Data[0].TotalLenderRegisterByLink;
                            TotalLenderHasContract = res.Data[0].TotalLenderHasContract;
                        }
                        $('#lbl_TotalLenderRegisterByLink').text(TotalLenderRegisterByLink);
                        $('#lbl_TotalLenderHasContract').text(TotalLenderHasContract);
                    } else {
                        options.success(null);
                    }
                }, "Thao tác thất bại, vui lòng thử lại sau!");
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = 0;
                if (response != null && response.Total != null && response.Total > 0) {
                    total = response.Total;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response == null || response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    function Grid_change(e) {
        var selectedRows = this.select();
        var selectedDataItems = [];
        for (var i = 0; i < selectedRows.length; i++) {
            var dataItem = this.dataItem(selectedRows[i]);
            selectedDataItems.push(dataItem);
        }
        // selectedDataItems contains all selected data items
    }
    var InitGrid = function () {
        $("#grid").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [10, 20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            change: Grid_change,
            selectable: "row",
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #",
                    width: 90
                },
                {
                    field: 'AffFullName',
                    title: 'Tên CTV',
                    textAlign: 'left',
                },
                {
                    field: 'AffCode',
                    title: 'Mã CTV',
                    textAlign: 'left',
                },
                {
                    field: 'LenderFullName',
                    title: 'Tên NDT',
                    textAlign: 'left',
                },
                {
                    field: 'LenderPhone',
                    title: 'SĐT',
                    textAlign: 'center',
                },
                {
                    field: 'StrCreateDate',
                    title: 'Ngày đăng kí',
                    textAlign: 'center',
                },
                {
                    field: 'StrStartDate',
                    title: 'Ngày ký HĐ (đầu tiên)',
                    textAlign: 'center',
                },
                {
                    field: 'NumberContract',
                    title: 'Số lượng HĐ	',
                    textAlign: 'left',
                    template: function (row) {
                        return formatNumber(row.NumberContract);
                    },
                },
                {
                    field: 'LenderVerifyName',
                    title: 'Trạng thái',
                    textAlign: 'center',
                },
            ]
        });
    };

    var GridRefresh = function () {
        var grid = $("#grid").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };

    var init = function () {
        GridRefresh();
        $('#btnGetData').click(function () {
            GridRefresh();
        });
        $("#sl_search_ctv").on("change", function () {
            GridRefresh();
        });
    };
    return {
        init: init
    }
}();
