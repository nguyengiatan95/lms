﻿var deposit = function () {

    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var ContractCode = $('#txt_search_code').val();
                var Status = $('#sl_search_status').val();
                var FromDate = $('#txt_search_fromDate').val();
                var ToDate = $('#txt_search_toDate').val();
                var CustomerName = $("#txt_search_customerName").val();
                var LenderID = $('#sl_search_lenderID').val() == null ? -111 : parseInt($('#sl_search_lenderID').val());
                var data = {
                    ContractCode: ContractCode,
                    Status: parseInt(Status),
                    FromDate: FromDate,
                    ToDate: ToDate,
                    CustomerName: CustomerName,
                    LenderID: LenderID,
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetLoanDeposit, JSON.stringify(data), function (res) {
                    if (res.Result == 1) {
                        options.success(res);
                    } else {
                        options.success(null);
                    }
                }, "Thao tác thất bại, vui lòng thử lại sau!");
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = 0;
                if (response != null && response.Total != null && response.Total > 0) {
                    total = response.Total;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response == null || response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    function Grid_change(e) {
        var selectedRows = this.select();
        var selectedDataItems = [];
        for (var i = 0; i < selectedRows.length; i++) {
            var dataItem = this.dataItem(selectedRows[i]);
            selectedDataItems.push(dataItem);
        }
        // selectedDataItems contains all selected data items
    }
    var InitGrid = function () {
        $("#grid").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [10, 20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            change: Grid_change,
            selectable: "row",
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #",
                    width: 90
                },
                {
                    field: 'LenderCode',
                    title: 'Mã NĐT',
                    textAlign: 'left',
                },
                {
                    field: 'CustomerName',
                    title: 'Tên KH',
                    textAlign: 'left',
                    template: function (row) {
                        return `<span>${row.CustomerName}</span><p style="color:#837a7a;font-size:12px">${row.ContractCode}</p>`;
                    },
                },
                {
                    field: 'FromDate',
                    title: 'Ngày giải ngân',
                    textAlign: 'center',
                    template: function (row) {
                        return `${formattedDate(row.FromDate)}`;
                    },
                },
                {
                    field: 'ToDate',
                    title: 'Ngày đáo hạn',
                    textAlign: 'center',
                    template: function (row) {
                        return `${formattedDate(row.ToDate)}`;
                    },
                },
                {
                    field: 'TotalMoney',
                    title: 'Tiền giải ngân',
                    textAlign: 'center',
                    template: function (row) {
                        return `<p>${formatCurrency(row.TotalMoney)}</p><p style="color:#837a7a;font-size:12px">${formatCurrency(row.TotalMoneyCurrent)}</p>`;
                    },
                },
                {
                    field: 'InterestMoney',
                    title: 'Lãi cọc',
                    textAlign: 'center',
                    template: function (row) {
                        return `<p>${formatCurrency(row.InterestMoney)}</p>`;
                    },
                },
                {
                    field: 'TotalDepositMoney',
                    title: 'Tổng tiền cọc',
                    textAlign: 'center',
                    template: function (row) {
                        return `<p>${formatCurrency(row.TotalMoneyDeposit)}</p>`;
                    },
                },
                {
                    field: 'StatusName',
                    title: 'Trạng thái',
                    textAlign: 'center',
                },
                {
                    field: 'Action',
                    title: 'Action',
                    textAlign: 'center',
                    template: function (row) {
                        var btn = '';
                        if (row.Status == 0) {
                            btn += `<a class="btn btn-success btn-icon btn-sm" title="Xác nhận chờ đặt cọc" onclick="deposit.ChangeStatus(${row.LoanID}, 2,'${row.ContractCode}')"> <i class="fa fa-check"></i></a>`

                        }// else if (row.Status == 2) {
                        //    btn += `<a class="btn btn-danger btn-icon btn-sm" title="Chuyển về chờ làm hồ sơ" onclick="deposit.ChangeStatus(${row.LoanID}, 0,'${row.ContractCode}')">  <i class="fa fa-backward"></i></a>`
                        //} 
                        else {
                            btn += '';
                        }
                        btn += ` <a class="btn btn-primary btn-icon btn-sm" title="Ghi chú" data-toggle="modal" data-target="#modal_edit_historyComment" onclick="deposit.GetDataHistoryComment(${row.PaymentScheduleDepositMoneyID})"><i class="fa fa-comment-dots"></i></a>`;
                        return btn;
                    },
                },
            ]
        });
    };
    var GridRefresh = function () {
        var grid = $("#grid").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
        $("#sl_search_lenderID").select2({
            placeholder: "Tất cả",
            allowClear: true,
            ajax: {
                url: baseCommon.Endpoint.GetLenderSearch,
                type: 'get',
                headers: {
                    'Authorization': baseCommon.GetToken()
                },
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        generalSearch: params.term, // search term
                        type: 0
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    if (data != null && typeof data != "undefined" && data.Result == 1) {
                        return {
                            results: repoConvert(data.Data),
                            pagination: {
                                more: (params.page * 30) < data.Total
                            }
                        };
                    }
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 3,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
        function formatRepo(repo) {

            if (repo.loading) return repo.text;
            var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__title'>" + repo.text + "</div>";
            return markup;
        }
        function formatRepoSelection(repo) {

            return repo.text;
        }
        function repoConvert(data) {

            var newObject = [];
            if (data != null && data != "") {
                $.each(data, function (key, value) {
                    var newRepo = { id: value.ID, text: value.FullName };
                    newObject.push(newRepo);
                });
            }
            return newObject;
        }
    };
    var ExportExcel = function (type = 1001) {
        KTApp.blockPage();
        var ContractCode = $('#txt_search_code').val();
        var Status = $('#sl_search_status').val();
        var FromDate = $('#txt_search_fromDate').val();
        var ToDate = $('#txt_search_toDate').val();
        var CustomerName = $("#txt_search_customerName").val();
        var LenderID = $('#sl_search_lenderID').val() == null ? -111 : parseInt($('#sl_search_lenderID').val());
        var dataQuery = {
            ContractCode: ContractCode,
            Status: parseInt(Status),
            FromDate: FromDate,
            ToDate: ToDate,
            CustomerName: CustomerName,
            LenderID: LenderID,
            PageIndex: 1,
            PageSize: 1000000
        };
        var data = {
            "CreateBy": baseCommon.GetUser().UserID,
            "TypeReport": type,
            "RequestQuery": JSON.stringify(dataQuery)
        };
        baseCommon.AjaxDone('POST', baseCommon.Endpoint.AddRequestExcel, JSON.stringify(data), function (respone) {
            if (respone.Result == 1) {
                KTApp.unblockPage();
                baseCommon.ShowSuccess(respone.Message);
                setTimeout(function () {
                    ExcelThn(respone.Data, type);
                }, 2000);
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        });
    };
    var ExcelThn = function (TraceID, type) {
        var dataEx = {
            "TraceID": TraceID,
            "CreateBy": baseCommon.GetUser().UserID,
            "reportType": type,
        };
        baseCommon.AjaxDone('GET', baseCommon.Endpoint.ExcelByTraceID, dataEx, function (respone) {
            switch (respone.data.result) {
                case -1: // call lại
                    setTimeout(function () {
                        ExcelThn(TraceID, type);
                    }, 1000);
                    return;
                case 1: // export excel
                    window.location.href = "/Excel/ExcelExport?TraceID=" + TraceID + "&CreateBy=" + baseCommon.GetUser().UserID + "&reportType=" + type;
                    return;
                case 0: // lỗi
                default:
                    baseCommon.ShowErrorNotLoad(respone.data.message);
                    return;
            }
        });
    };
    var uploadFileChangeStatus = function () {
        $("#tem_upload").change(function (evt) {
            var selectedFile = evt.target.files[0];
            if (selectedFile == undefined) {
                return;
            }
            KTApp.blockPage();
            var reader = new FileReader();
            var ListDeposit = [];
            reader.onload = function (event) {
                var data = event.target.result;
                var workbook = XLSX.read(data, {
                    type: 'binary'
                });
                workbook.SheetNames.forEach(function (sheetName) {
                    var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                    var json_object = JSON.stringify(XL_row_object);
                    var objData = JSON.parse(json_object);
                    for (var i = 0; i < objData.length; i++) {
                        var column_LoanID = objData[i]["LoanID"];
                        var column_Status = objData[i]["Trạng thái"];
                        if (parseInt(column_LoanID) > 0 && column_Status != undefined) {
                            var objSend = {
                                "LoanID": parseInt(column_LoanID),
                                "Status": parseInt(column_Status),
                            };
                            ListDeposit.push(objSend);
                        }
                    }
                });
                if (ListDeposit.length == 0) {
                    baseCommon.ShowErrorNotLoad('File sai định dạng');
                    return;
                }
                baseCommon.AjaxDone("POST", baseCommon.Endpoint.UpdateStatusDeposit, JSON.stringify(ListDeposit), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        $('#btnGetData').click();
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                });
            };
            reader.onerror = function (event) {
                console.error("File could not be read! Code " + event.target.error.code);
            };
            reader.readAsBinaryString(selectedFile);
            KTApp.unblockPage();
        });
    };
    var DownLoadFileExcel = function () {
        var link = document.createElement("a");
        link.download = 'file_mau_import_excel';
        link.href = '/static/Upload/template_excel_datcoc.xlsx';
        link.click();
    };
    var ChangeStatus = function (loanID, status, codeID) {
        var text = status == 2 ? "Bạn có chắc chắn đẩy hồ sơ <b>" + codeID + " lên chờ đặt cọc không <b>?" : "Bạn có chắc chắn chuyển hồ sơ <b>" + codeID + " về chờ làm hồ sơ không <b>?"
        swal.fire({
            html: text,
            type: "warning",
            showCancelButton: !0,
            cancelButtonText: "Hủy",
            confirmButtonText: "Đồng ý"
        }).then(function (result) {
            if (result.value) {
                var ListDeposit = [];
                var objSend = {
                    "LoanID": loanID,
                    "Status": status,
                };
                ListDeposit.push(objSend);
                baseCommon.AjaxDone("POST", baseCommon.Endpoint.UpdateStatusDeposit, JSON.stringify(ListDeposit), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        $('#btnGetData').click();
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                });
            }
        });
    }

    var TableHistoryComment = function () {
        var recordHistoryCommentTable = 0;
        $("#HistoryCommentTable_Deposit").kendoGrid({
            dataSource: {
                pageSize: 1000,
            },
            scrollable: true,
            resizable: true,
            pageable: true,
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordHistoryCommentTable #",
                    width: 60
                },
                {
                    field: "CreateDate",
                    title: "Ngày tạo",
                    width: 100,
                    template: '#= baseCommon.FormatDate(CreateDate,\'DD/MM/YYYY HH:mm\')#',
                },
                {
                    field: "Note",
                    title: "Ghi chú",
                    width: 100,
                    template: function (row) {
                        var html = "";
                        if (row == 0 || row == null || row.Note == "undefined" || row.Note == null) {
                            return html;
                        }
                        html = `<span>${row.Note}</span>`;
                        return html;
                    }
                },
                {
                    field: "UserName",
                    title: "Người thao tác",
                    width: 120
                },
            ],
            dataBinding: function () {
                recordHistoryCommentTable = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
        });
    }
    var GetDataHistoryComment = function (paymentScheduleDepositMoneyID) {
        recordHistoryCommentTable = 0;
        $("#hdd_PaymentScheduleDepositMoneyID").val(paymentScheduleDepositMoneyID)
        if (paymentScheduleDepositMoneyID == null || paymentScheduleDepositMoneyID == undefined) {
            baseCommon.ShowErrorNotLoad("paymentScheduleDepositMoneyID chưa có vui lòng thử lại");
        }
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetHistoryDepositLender + paymentScheduleDepositMoneyID, '', function (respone) {
            if (respone.Result == 1) {
                if (respone.Data != null && respone.Data != null) {
                    var dataSource = new kendo.data.DataSource({
                        data: respone.Data
                    });
                    var grid = $("#HistoryCommentTable_Deposit").data("kendoGrid");
                    grid.setDataSource(dataSource);
                }
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        });
    }
    var Save_CommentDeposit = function () {
        var btn = $('#btn_save_comment_deposit');
        var paymentScheduleDepositMoneyID = $("#hdd_PaymentScheduleDepositMoneyID").val()

        var Note = CKEDITOR.instances['txtNote_deposit'].getData();
        if (Note.length > 500) {
            baseCommon.ShowErrorNotLoad("Ghi chú quá dài .Vui lòng thử lại!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }
        if (Note == null || Note == "") {
            baseCommon.ShowErrorNotLoad("Ghi chú không được bỏ trống!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }
        var modelComment = {
            "PaymentScheduleDepositMoneyID": parseInt(paymentScheduleDepositMoneyID),
            "Note": Note,
            "userID": baseCommon.GetUser().UserID
        };
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.CommentDeposit, JSON.stringify(modelComment), function (respone) {
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                GetDataHistoryComment(paymentScheduleDepositMoneyID);
                CKEDITOR.instances.txtNote.setData('');
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        });
    }
    var init = function () {
        GridRefresh();
        $('#btnGetData').click(function () {
            GridRefresh();
        });
        $('#txt_search_fromDate').on('change', function () {
            GridRefresh();
        });
        $('#txt_search_toDate').on('change', function () {
            GridRefresh();
        });
        $('#sl_search_lenderID').on('change', function () {
            GridRefresh();
        });
        $('#sl_search_status').on('change', function () {
            GridRefresh();
        });
        $("#txt_search_code").on('keypress', function (e) {
            if (e.which === 13) {
                GridRefresh();
            }
        });
        $("#txt_search_customerName").on('keypress', function (e) {
            if (e.which === 13) {
                GridRefresh();
            }
        });
        uploadFileChangeStatus();
        TableHistoryComment();
    };
    return {
        init: init,
        ExportExcel: ExportExcel,
        DownLoadFileExcel: DownLoadFileExcel,
        ChangeStatus: ChangeStatus,
        GetDataHistoryComment: GetDataHistoryComment,
        Save_CommentDeposit: Save_CommentDeposit
    }
}();