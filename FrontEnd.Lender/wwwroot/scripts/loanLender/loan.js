﻿var datatable;
var _loanId, gridSelectedRowData, rowClicked = 0;
var loanInfo = {};
var recordGrid = 0;
var recordHistoryCommentTable = 0;
var recordXetDuyet = 0;
var loan = new function () {
    var Option = {
        Detail: 0,
        GetDataHistoryCommentLOS: 1,
        UnlockLoanLender: 2,
        InitPushLoanForLender: 3,
        CreateLoanDisbursement: 4,
        InitRutHS: 5,
        InitSaveReturnLoan: 6,
    };

    var RowClick = function (rowClick, loanBriefId, fullName) {
        rowClicked = rowClick;
        if (rowClicked == Option.Detail) {
            DetailModal(loanBriefId)
        }
        else if (rowClicked == Option.GetDataHistoryCommentLOS) {
            loan.GetDataHistoryCommentLOS(loanBriefId);
        }
        else if (rowClicked == Option.UnlockLoanLender) {
            loan.UnlockLoanLender(loanBriefId);
        }
        else if (rowClicked == Option.InitPushLoanForLender) {
            loan.InitPushLoanForLender(loanBriefId);
        }
        else if (rowClicked == Option.CreateLoanDisbursement) {
            loan.CreateLoanDisbursement(loanBriefId, fullName);
        }
        else if (rowClicked == Option.InitRutHS) {
            loan.InitRutHS(loanBriefId, fullName);
        }
        else if (rowClicked == Option.InitSaveReturnLoan) {
            loan.InitSaveReturnLoan(loanBriefId, fullName);
        }
        else {
            DetailModal(loanBriefId);
        }
    };
    function Grid_change(e) {
        var selectedRows = this.select();
        var selectedDataItems = [];
        for (var i = 0; i < selectedRows.length; i++) {
            var dataItem = this.dataItem(selectedRows[i]);
            selectedDataItems.push(dataItem);
        }
        // selectedDataItems contains all selected data items
        gridSelectedRowData = selectedDataItems[0];
        setTimeout(function () {
            if (rowClicked == Option.Detail) {
                DetailModal(gridSelectedRowData.LoanBriefId)
            }
            rowClicked = 0;
        }, 50);
    }
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                //var customerKeySearch = $('#txt_search').val();
                var type = $('#sl_search_statusLender').val();
                var data = {
                    Search: '',
                    Type: parseInt(type),
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.DisbursementWaitingLOS, JSON.stringify(data), function (res) {
                    if (res.Result == 1) {
                        //DetailModal(res.Data[0]);
                        options.success(res);
                    }
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function (objQuery) {
        $("#grid").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                input: true,
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            change: Grid_change,
            selectable: "row",
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 2) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'LoanBriefId',
                    title: 'Mã HĐ',
                    attributes: { style: "text-align:center;" },
                    template: function (row) {
                        var html = "";
                        if (row == 0 || row == null || typeof row.LoanBriefId == "undefined" || row.LoanBriefId == null) {
                            return html;
                        }
                        html = `<span style="color:#5867dd">${row.ContactCode}</span>`;
                        return html;
                    }
                },
                {
                    field: 'FullName',
                    title: 'Tên khách hàng',
                    textAlign: 'left',
                    width: 250
                },
                {
                    title: 'Quận/Huyện',
                    attributes: { style: "text-align:center;" },
                    headerAttributes: { style: "text-align: center" },
                    width: 150,
                    template: function (row) {
                        var html = `<span>${row.DistrictName}\
                                    <br/> <span class="item-desciption"> ${row.ProvinceName} </span>\
                                    </span>`;
                        return html;
                    }
                },
                {
                    field: 'LenderCareReceivedDate',
                    title: 'Thời gian nhận đơn',
                    textAlign: 'left',
                    template: function (row) {
                        if (row == null || row.LenderCareReceivedDate == null || row.LenderCareReceivedDate == "0001-01-01T00:00:00") {
                            return "";
                        }
                        var date = moment(row.LenderCareReceivedDate);
                        var html = date.format("DD/MM/YYYY HH:mm");
                        return html;
                    }
                },
                {
                    field: 'CreatedTime',
                    title: 'Ngày tạo',
                    textAlign: 'left',
                    template: function (row) {
                        if (row == null || row.CreatedTime == null) {
                            return "";
                        }
                        var date = moment(row.CreatedTime);
                        var html = date.format("DD/MM/YYYY HH:mm");
                        return html;
                    }
                },
                {
                    title: 'Tiền vay (VNĐ)',
                    attributes: { style: "text-align:right;" },
                    headerAttributes: { style: "text-align: right" },
                    template: function (row) {
                        return `${html_money(row.LoanAmountFinal)}
                                    <br/> <span class="item-desciption"> ${row.ProductName} </span >`;
                    }
                },
                {
                    title: 'Thời gian vay',
                    attributes: { style: "text-align:center;" },
                    template: function (row) {
                        var html = "";
                        if (row.buyInsurenceCustomer == false) {
                            html = `<span><span style="color:blue">${row.LoanTime} </span>tháng
                                    <br/> <span class="item-desciption" style="color:red">Không mua bảo hiểm</span>\
                                    </span>`
                        } else {
                            html = `<span><span style="color:blue">${row.LoanTime} </span>tháng
                                    <br/> <span class="item-desciption" style="color:blue">Có mua bảo hiểm</span>\
                                    </span>`;
                        }
                        if (row.DebtRevolvingLoan) {
                            html += `<br/> <span style="color:red">Cấu trúc nợ</span>`
                        }
                        if (row.EsignState) {
                            html += `<br/> <span style="color:red">Hợp đồng online</span>`
                        }
                        if (row.LenderRate > 0) {
                            html += `<br/> <span style="color:red">Lãi xuất ${row.LenderRate}%</span>`
                        }
                        return html;

                    }
                },
                {
                    field: 'StrStatus',
                    title: 'Trạng thái',
                    attributes: { style: "text-align:center;" },
                    headerAttributes: { style: "text-align: center" },
                    template: function (row) {
                        var strIslock = '';
                        if (row == null || row.StrStatus == null) {
                            return "";
                        }
                        if (row.IsLock == true) {
                            strIslock = 'Đơn bị khóa'
                        }
                        if (row.Status == 102) {
                            return '<span class="kt-badge kt-badge--primary kt-badge--inline kt-badge--pill">' + row.StrStatus + '</span>\
                                        <br/> <span class="item-desciption" style="color:red">' + strIslock + '</span>';
                        }
                        if (row.Status == 103) {
                            return '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">' + row.StrStatus + '</span>\
                                        <br/> <span class="item-desciption" style="color:red">' + strIslock + '</span>';
                        }
                        if (row.Status == 104) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + row.StrStatus + '</span>\
                                        <br/> <span class="item-desciption" style="color:red">' + strIslock + '</span>';
                        }
                        return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">' + row.StrStatus + '</span>\
                                        <br/> <span class="item-desciption" style="color:red">' + strIslock + '</span>';
                    }
                },
                {
                    title: "Hành động",
                    template: function (row) {
                        var strAction = '';
                        strAction += '<button class="btn btn-outline-brand btn-icon" title="Ghi chú" data-toggle="modal" data-target="#modal_edit_loanLender" onclick="loan.RowClick(loan.Option.GetDataHistoryCommentLOS, ' + row.LoanBriefId + ')">\
                                            <i class="fa fa-comment-dots"></i></button> ';
                        if (row.IsLock == true) {
                            strAction += '<button class="btn btn-outline-danger btn-icon" title ="Mở khóa" onclick="loan.RowClick(loan.Option.UnlockLoanLender,' + row.LoanBriefId + ')">\
                                            <i class="fa fa-unlock"></i></button> ';
                        }
                        if (row.IsLock == true && row.Status == 103) {
                            strAction += '<button class="btn btn-outline-success btn-icon" title="Tạo HĐ giải ngân" onclick="loan.RowClick(loan.Option.CreateLoanDisbursement,' + row.LoanBriefId + ',\'' + row.FullName + '\')">\
                                            <i class="fa fa-plus"></i></button>';
                        } else if (row.IsLock == false && row.Status == 103) {
                            strAction += '<button class="btn btn-outline-brand btn-icon" title="Rút hồ sơ" data-toggle="modal" data-target="#modal_RutHS" onclick="loan.RowClick(loan.Option.InitRutHS,' + row.LoanBriefId + ',\'' + row.FullName + '\')">\
                                                  <i class="fa fa-arrow-down"></i></button>';
                        }
                        if (row.IsLock == false && row.Status == 90) {
                            strAction += '<button class="btn btn-outline-success btn-icon" title="Đẩy cho đối tác" data-toggle="modal" data-target="#modal_pushLoanForLender" onclick="loan.RowClick(loan.Option.InitPushLoanForLender, ' + row.LoanBriefId + ')">\
                                            <i class="fa fa-handshake"></i></button>\
                                         <button class="btn btn-outline-brand btn-icon" title="Trả lại" data-toggle="modal" data-target="#modal_returnLoan" onclick="loan.RowClick(loan.Option.InitSaveReturnLoan,' + row.LoanBriefId + ',\'' + row.FullName + '\')">\
                                             <i class="fa fa-reply"></i></button>';
                        }
                        //cho day lender
                        //if (row.Status == 90) {
                        //    strAction += '<button class="btn btn-outline-success btn-icon" title="Đẩy cho đối tác" data-toggle="modal" data-target="#modal_pushLoanForLender" onclick="loan.RowClick(loan.Option.InitPushLoanForLender, ' + row.LoanBriefId + ')">\
                        //                    <i class="fa fa-handshake"></i></button>\
                        //                 <button class="btn btn-outline-brand btn-icon" title="Trả lại" data-toggle="modal" data-target="#modal_returnLoan" onclick="loan.RowClick(loan.Option.InitSaveReturnLoan,' + row.LoanBriefId + ',\'' + row.FullName + '\')">\
                        //                     <i class="fa fa-reply"></i></button>';
                        //}
                        //// cho KT GN
                        //if (row.Status == 102) {
                        //    //strAction += '<button class="btn btn-outline-brand btn-icon" title="Rút hồ sơ" data-toggle="modal" data-target="#modal_RutHS" onclick="loan.RowClick(loan.Option.InitRutHS)">\
                        //    //                <i class="fa fa-arrow-down"></i></button> ';
                        //}
                        ////cho lender gn
                        //if (row.Status == 103) {
                        //    strAction += '<button class="btn btn-outline-success btn-icon" title="Tạo HĐ giải ngân" onclick="loan.RowClick(loan.Option.CreateLoanDisbursement,' + row.LoanBriefId + ',\'' + row.FullName + '\')">\
                        //                    <i class="fa fa-plus"></i></button>\
                        //                 <button class="btn btn-outline-brand btn-icon" title="Rút hồ sơ" data-toggle="modal" data-target="#modal_RutHS" onclick="loan.RowClick(loan.Option.InitRutHS,' + row.LoanBriefId + ',\'' + row.FullName + '\')">\
                        //                    <i class="fa fa-arrow-down"></i></button>';
                        //}
                        return strAction;
                    }
                }
            ]
        });
    };
    var DetailModal = function (loanId) {
        _loanId = loanId;
        baseCommon.IconWaiting(false, '#modalLoanDetailId');
        $('#modalFadeBody').html('');
        $.ajax({
            url: '/Loan/Detail?id=' + _loanId,
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                if (res.isSuccess == 1) {
                    $('#modalFadeBody').html(res.html);
                    $('#modalFadeId').modal('show');
                    loan.InitDetailLoan(loanId);
                    //setTimeout(loanDetail.InitTabLichDongLaiPhi(), 2000);
                } else {
                    baseCommon.ShowErrorNotLoad(res.message);
                }
            }
        });
    }
    var Init = function () {
        InitGrid();
        $("#sl_search_statusLender").on('change', function (e) {
            $('#btnGetData').click();
        });
        TableHistoryComment();
        //loan.GetLenderInPushLoan();

        $("#sl_partial_pushLoanForLender").select2({
            placeholder: "Tất cả",
            allowClear: true,
            ajax: {
                url: baseCommon.Endpoint.GetLender,
                type: 'get',
                headers: {
                    'Authorization': baseCommon.GetToken()
                },
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        GeneralSearch: params.term, // search term
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    if (data != null && typeof data != "undefined" && data.Result == 1) {
                        return {
                            results: repoConvert(data.Data),
                            pagination: {
                                more: (params.page * 30) < data.Total
                            }
                        };
                    }
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 3,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
        function formatRepo(repo) {

            if (repo.loading) return repo.text;
            var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__title'>" + repo.text + "</div>";
            return markup;
        }
        function formatRepoSelection(repo) {

            return repo.text;
        }
        function repoConvert(data) {

            var newObject = [];
            if (data != null && data != "") {
                $.each(data, function (key, value) {
                    var newRepo = { id: value.Value, text: value.Text };
                    newObject.push(newRepo);
                });
            }
            return newObject;
        }
    };
    var GridRefresh = function () {
        var grid = $("#grid").data("kendoGrid")
        grid.dataSource.page(1);
    };
    var GetDataHistoryCommentLOS = function (loanID) {
        recordHistoryCommentTable = 0;
        $("#hdd_loanlenderID").val(loanID)
        var modelGetComment = {
            "LoanBriefID": parseInt(loanID),
            "PageIndex": 1,
            "PageSize": 10000
        };
        baseCommon.AjaxDone('POST', baseCommon.Endpoint.GetHistoryCommentLos2, JSON.stringify(modelGetComment), function (respone) {
            if (respone.Result == 1) {
                if (respone.Data != null && respone.Data != null) {
                    var dataSource = new kendo.data.DataSource({
                        data: respone.Data
                    });
                    var grid = $("#HistoryCommentTable").data("kendoGrid");
                    grid.setDataSource(dataSource);
                }
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        });
    }
    var TableHistoryComment = function () {
        var recordHistoryCommentTable = 0;
        $("#HistoryCommentTable").kendoGrid({
            dataSource: {
                pageSize: 1000,
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 3 && i != 4) {
                        this.autoFitColumn(i);
                    }
                }
            },
            scrollable: true,
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordHistoryCommentTable #"
                },

                {
                    field: "CreateDate",
                    title: "Ngày tạo",
                    template: '#= baseCommon.FormatDate(CreateDate,\'DD/MM/YYYY HH:mm\')#',
                },
                {
                    field: "ShopName",
                    title: "Tên Shop"
                },
                {
                    field: "FullName",
                    title: "Người thao tác",
                    width: 200
                },
                {
                    field: "Comment",
                    title: "Ghi chú",
                    width: 600,
                    template: function (row) {
                        var html = "";
                        if (row == 0 || row == null || row.Comment == "undefined" || row.Comment == null) {
                            return html;
                        }
                        html = `<span>${row.Comment}</span>`;
                        return html;
                    }
                },

            ],
            dataBinding: function () {
                recordHistoryCommentTable = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
        });
    }
    var Save_CommentLenderLOS = function () {
        var btn = $('#btn_save_comment');
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        var loanId = $("#hdd_loanlenderID").val()

        var Note = $('#txtNote').val();
        if (Note == null || Note == "") {
            baseCommon.ShowErrorNotLoad("Ghi chú không được bỏ trống!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }
        var modelComment = {
            "LoanBriefId": parseInt(loanId),
            "Note": Note,
            "UserId": baseCommon.GetUser().UserID,
            "UserName": baseCommon.GetUser().UserName,
        };
        baseCommon.AjaxDone('POST', baseCommon.Endpoint.SaveCommentHistoryLos, JSON.stringify(modelComment), function (respone) {
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                GetDataHistoryCommentLOS(loanId);
                //CKEDITOR.instances.txtNote.setData('');
                $('#txtNote').val('');
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            }
        });
    }
    var UnlockLoanLender = function (loanID) {
        swal.fire({
            html: "Bạn có chắc chắn muốn mở khóa cho hồ sơ <b>HĐ-" + loanID + "</b> không?",
            type: "warning",
            showCancelButton: !0,
            cancelButtonText: "Hủy",
            confirmButtonText: "Đồng ý"
        }).then(function (result) {
            if (result.value) {
                var modelUnlock = {
                    "LoanBriefID": parseInt(loanID),
                    "UserID": baseCommon.GetUser().UserID,
                    "TypeLock": 1,
                };
                baseCommon.AjaxDone('POST', baseCommon.Endpoint.LockLoan, JSON.stringify(modelUnlock), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        GridRefresh()
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                });
            }
        });
    };
    var CreateLoanDisbursement = function (loanID, FullName) {
        swal.fire({
            html: "Bạn có chắc chắn tạo hợp đồng Giải Ngân cho mã <b>HĐ-" + loanID + "</b> khách hàng <b>" + FullName + "</b> không?",
            type: "warning",
            showCancelButton: !0,
            cancelButtonText: "Hủy",
            confirmButtonText: "Đồng ý"
        }).then(function (result) {
            if (result.value) {
                var modelCreateLoanDisbursement = {
                    "LoanBriefID": parseInt(loanID),
                    "UserID": baseCommon.GetUser().UserID,
                };
                baseCommon.AjaxDone('POST', baseCommon.Endpoint.CreateLoanForDisbursementLoanCreditOfAppLender, JSON.stringify(modelCreateLoanDisbursement), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        GridRefresh()
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                });
            }
        });
    };
    var InitSaveReturnLoan = function (loanId, fullName) {
        $("#hdd_partial_returnLoanID").val(loanId)
        $("#hdd_partial_returnLoanFullName").val(fullName)
        $("#title_partial_returnLoan").html("<b>Hồ sơ vay <span style='color:red'>HĐ-" + loanId + " : " + fullName + "</span> => Không Duyệt</b>")
        $("#partial_note").val('')
    }
    var Save_ReturnLoan = function () {
        var btn = $('#btn_save_returnLoan');
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

        var loanId = $("#hdd_partial_returnLoanID").val()
        //var typeID = $("#sl_partial_reason").val()

        var Note = $("#partial_note").val();
        if (Note == null || Note == "") {
            baseCommon.ShowErrorNotLoad("Ghi chú không được bỏ trống!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }
        var modelReturnLoan = {
            "LoanBriefID": parseInt(loanId),
            "Note": Note,
            "UserID": baseCommon.GetUser().UserID,
            "TypeID": 2,
        };
        baseCommon.AjaxDone('POST', baseCommon.Endpoint.ReturnLoanLender, JSON.stringify(modelReturnLoan), function (respone) {
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                $("#btnExit").click();
                GridRefresh()
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            }
        });
    }

    var InitRutHS = function (loanId, fullName) {
        $("#hdd_partial_RutHSID").val(loanId)
        $("#hdd_partial_RutHSFullName").val(fullName)
        $("#title_partial_RutHS").html("<b>Bạn có chắc chắn Rút hồ sơ <span style='color:red'>HĐ-" + loanId + " : " + fullName + "</span></b>")
        $("#partialRutHS_note").val('')
    }
    var Save_RutHS = function () {
        var btn = $('#btn_save_RutHS');
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

        var loanId = $("#hdd_partial_RutHSID").val()
        //var typeID = $("#sl_partial_reason").val()

        var Note = $("#partialRutHS_note").val();
        if (Note == null || Note == "") {
            baseCommon.ShowErrorNotLoad("Ghi chú không được bỏ trống!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }
        var modelReturnLoan = {
            "LoanBriefID": parseInt(loanId),
            "Note": Note,
            "UserID": baseCommon.GetUser().UserID,
            "TypeID": 3,
        };
        baseCommon.AjaxDone('POST', baseCommon.Endpoint.ReturnLoanLender, JSON.stringify(modelReturnLoan), function (respone) {
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                $("#btnExitRutHS").click();
                GridRefresh()
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            }
        });
    }

    var InitPushLoanForLender = function (loanId) {
        $("#hdd_partial_pushLoanForLenderID").val(loanId)
        $("#title_partial_pushLoanForLender").html("Chuyền hợp đồng <b>HĐ-" + loanId + "</b> cho Đối tác")
        $("#sl_partial_pushLoanForLender").val(-111).change()
    }
    //var GetLenderInPushLoan = function () {
    //    baseCommon.AjaxGet(baseCommon.Endpoint.GetLender + -1, function (respone) {
    //        if (respone.Result == 1) {
    //            baseCommon.DataSource("#sl_partial_pushLoanForLender", "Tất cả", "Value", "Text", 0, respone.Data);
    //        }
    //    }, "Chưa lấy được danh sách Lý do chi tiết!");
    //}

    var Save_PushLoanForLender = function () {
        var loanId = $("#hdd_partial_pushLoanForLenderID").val()
        var lenderId = $("#sl_partial_pushLoanForLender").val()
        var btn = $('#btn_save_returnLoan');
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

        if (lenderId == -111) {
            baseCommon.ShowErrorNotLoad("Mã Đối tác không được chọn Tất cả!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }
        var modelPushLoanLender = {
            "LoanBriefID": parseInt(loanId),
            "LenderID": parseInt(lenderId),
            "UserID": baseCommon.GetUser().UserID
        };
        baseCommon.AjaxDone('POST', baseCommon.Endpoint.PushLoanLender, JSON.stringify(modelPushLoanLender), function (respone) {
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                $("#btnExitPushLoan").click();
                GridRefresh()
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            }
        });
    }

    ////detail
    var InitDetailLoan = function () {
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetLoanCreditDetailLos + _loanId, '', function (respone) {
            if (respone.Result == 1) {
                if (respone.Data == null || respone.Data == "") {
                    baseCommon.ShowErrorNotLoad("Không có dữ liệu");
                }
                loanInfo = respone.Data
                if (loanInfo.TypeLoanBrief == 1) {
                    $('#hdd_typeCustomer').hide();
                }
                $('#modalLoanDetailId').html("Chi tiết <b>" + loanInfo.ContactCode + "</b> - Khách hàng: <b>" + loanInfo.FullName + "</b>");
                //tab tổng hợp - thông tin cá nhân
                $('#detail_fullName').html(loanInfo.FullName)
                if (loanInfo.Phone !== null && loanInfo.Phone !== '') {
                    var phone = loanInfo.Phone,
                        formattedPhone = phone.substr(0, 3) + '****' + phone.substr(7, 4)
                    $('#detail_phone').html(formattedPhone)
                } else {
                    formattedPhone = ''
                    $('#detail_phone').html(formattedPhone)
                }
                $('#detail_cmt').html(loanInfo.NationalCard)
                $('#detail_facebook').html(loanInfo.FacebookAddress)
                $('#detail_sbh').html(loanInfo.InsurenceNumber)
                $('#detail_email').html(loanInfo.Email)
                $('#detail_callMany').html(loanInfo.CountCall)
                $('#detail_gender').html(loanInfo.Gender == 0 ? "Nam" : "Nữ")
                var Dob = moment(loanInfo.Dob);
                $('#detail_dob').html(Dob.format("DD/MM/YYYY"))
                $('#detail_note_sbh').html(loanInfo.InsurenceNote)
                $('#detail_TypeLoanBriefName').html(loanInfo.TypeLoanBriefName)
                //tab tổng hợp - thông tin doanh nghiệp
                var BusinessCertificationDate = moment(loanInfo.BusinessCertificationDate);
                $('#detail_BusinessCertificationDate').html(BusinessCertificationDate.format("DD/MM/YYYY"))
                $('#detail_CareerBusiness').html(loanInfo.CareerBusiness)
                $('#detail_BusinessCompanyName').html(loanInfo.BusinessCompanyName)
                $('#detail_BusinessCertificationAddress').html(loanInfo.BusinessCertificationAddress)
                $('#detail_HeadOffice').html(loanInfo.HeadOffice)
                $('#detail_BusinessAddress').html(loanInfo.BusinessAddress)
                $('#detail_CompanyShareholder').html(loanInfo.CompanyShareholder)
                var dobShareHolder = moment(loanInfo.BirthdayShareholder)
                $('#detail_BirthdayShareholder').html(dobShareHolder.format("DD/MM/YYYY"))
                $('#detail_CardNumberShareholder').html(loanInfo.CardNumberShareholder)
                var carNumberShare = moment(loanInfo.CardNumberShareholderDate)
                $('#detail_CardNumberShareholderDate').html(carNumberShare.format("DD/MM/YYYY"))
                $('#detail_PlaceOfBirthShareholder').html(loanInfo.PlaceOfBirthShareholder)
                //tab tổng hợp - hình thức vay
                $('#detail_ProductName').html(loanInfo.ProductName)
                $('#detail_ProductName2').html(loanInfo.ProductName)
                $('#detail_RateTypeName').html(loanInfo.RateTypeName)
                $('#detail_ScoreCredit').html(loanInfo.ScoreCredit)
                $('#detail_IsTrackingLocation').html(loanInfo.IsTrackingLocation == true ? "Có" : "Không")
                //tab tổng hợp - khoản vay
                $('#detail_LoanPurposeName').html(loanInfo.LoanPurposeName)
                $('#detail_LoanAmountFinal').html(html_money(loanInfo.LoanAmountFinal))
                $('#detail_BuyInsurenceCustomer').html(loanInfo.BuyInsurenceCustomer == true ? "Có" : "Không")
                $('#detail_LoanAmount').html(html_money(loanInfo.LoanAmount))
                $('#detail_LoanTime').html(loanInfo.LoanTime + " tháng")
                $('#detail_Frequency').html(loanInfo.Frequency + " tháng")
                var Todate = moment(loanInfo.ToDate)
                $('#detail_ToDate').html(Todate.format("DD/MM/YYYY"))
                //tab tổng hợp - địa chỉ đang ở
                $('#detail_ProvinceName').html(loanInfo.ProvinceName)
                $('#detail_DistrictName').html(loanInfo.DistrictName)
                $('#detail_WardName').html(loanInfo.WardName)
                $('#detail_Address').html(loanInfo.CustomerAddress)
                $('#detail_LivingTimeName').html(loanInfo.LivingTimeName)
                $('#detail_ResidentTypeName').html(loanInfo.ResidentTypeName)
                //tab tổng hợp - địa chỉ hộ khẩu
                $('#detail_HouseOldCityName').html(loanInfo.HouseOldCityName)
                $('#detail_HouseOldDistrictName').html(loanInfo.HouseOldDistrictName)
                $('#detail_HouseOldWardName').html(loanInfo.HouseOldWardName)
                $('#detail_HouseOldAddress').html(loanInfo.HouseOldAddress)
                //tab tổng hợp- việc làm
                $('#detail_JobTitle').html(loanInfo.JobTitle)
                $('#detail_CompanyName').html(loanInfo.CompanyName)
                $('#detail_CompanyPhone').html(loanInfo.CompanyPhone)
                $('#detail_CompanyTaxCode').html(loanInfo.CompanyTaxCode)
                $('#detail_CompanyAddress').html(loanInfo.CompanyAddress)
                $('#detail_CompanyDistrictName').html(loanInfo.CompanyDistrictName)
                $('#detail_TotalIncome').html(html_money(loanInfo.TotalIncome))
                $('#detail_DescriptionJob').html(loanInfo.DescriptionJob)
                //tab tổng hợp - người thân, đồng nghiệp
                $('#rendeRelationShip').html("")
                for (var i = 0; i < respone.Data.Relationship.length; i++) {
                    var fullName = loanInfo.Relationship[i].FullName == null ? '' : loanInfo.Relationship[i].FullName
                    var phone = loanInfo.Relationship[i].Phone == null ? '' : loanInfo.Relationship[i].Phone
                    var address = loanInfo.Relationship[i].Address == null ? '' : loanInfo.Relationship[i].Address
                    $('#rendeRelationShip').append('<div class="form-group row form-group-marginless">\
                        <label class= "col-lg-3 col-form-label" >Tên:</label>\
                        <div class="col-lg-3">\
                        <label type="text" class="padding_top_lable text_align" id="detail_RlfullName">'+ fullName + '</label>\
                             </div>\
                        <label class="col-lg-3 col-form-label">Số ĐT:</label>\
                        <div class="col-lg-3">\
                        <label type="text" class="padding_top_lable text_align" id="detail_Rlphone">'+ phone + '</label>\
                        </div>\
                     </div >\
                     <div class="form-group row form-group-marginless">\
                        <label class="col-lg-3 col-form-label">Địa chỉ:</label>\
                        <div class="col-lg-3">\
                        <label type="text" class="padding_top_lable text_align" id="detail_Rladdress">'+ address + '</label>\
                        </div>\
                     </div>');
                }
                $("input[name=radio2]").val([loanInfo.LivingWith]);
                $("input[name=radio1]").val([loanInfo.NumberBaby]);
                $("input[name=radio0]").val([loanInfo.IsMerried]);
                //tab tổng hợp - CIC
                $('#detail_IsCheckCic').html(loanInfo.IsCheckCic == true ? "Đã check CIC" : "Chưa Check CIC")
                //tab tổng hợp - tài sản
                $('#detail_BrandName').html(loanInfo.BrandName)
                $('#detail_loanAmountExpertiseLast').html(html_money(loanInfo.LoanAmountExpertiseLast))
                $('#detail_Product').html(loanInfo.LoanBriefProperty.Product)
                $('#detail_YearMade').html(loanInfo.LoanBriefProperty.YearMade)
                $('#detail_loanAmountExpertise').html(html_money(loanInfo.LoanAmountExpertise))
                $('#detail_IsLocate').html(loanInfo.IsLocate == true ? "Có" : "Không")
                //tab tổng hợp - Location
                $('#detail_IsTrackingLocation2').html(loanInfo.IsTrackingLocation == true ? "Đã gửi yêu cầu" : "Chưa gửi yêu cầu")
                //tab tổng hợp - giải ngân
                $('#detail_BankName').html(loanInfo.BankName)
                $('#detail_BankAccountName').html(loanInfo.BankAccountName)
                $('#detail_BankAccountNumber').html(loanInfo.BankAccountNumber)
                $('#detail_ReceivingMoneyTypeName').html(loanInfo.ReceivingMoneyTypeName)


            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        }, "Chưa lấy được danh sách Chi tiết đơn vay!");
    }
    var GetDataImagesLos = function () {
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetDataImagesLenderLos + _loanId, '', function (respone) {
            if (respone.Result == 1) {
                ShowFileInModal(respone.Data);
                new FullscreenSlideshow().init($('#div-lst-img'));
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        });
    }
    function ShowFileInModal(data) {
        $("#m_accordion_5").html("");
        $("#div-lst-img").html("");
        if (data != null && data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                $("#m_accordion_5").append('<div class="m-accordion__item"> \
                    <div class="m-accordion__item-head collapsed check-click" role="tab" id="m_accordion_5_item_1_head_'+ data[i].TypeId + '" onclick="ShowImg(' + data[i].TypeId + ')" data-toggle="collapse" href="#m_accordion_5_item_1_body_' + data[i].TypeId + '" aria-expanded="false">\
                    <div class="div_style">\
                     <span class="m-accordion__item-title">'+ data[i].TypeName + ' <b style="color:red;">(' + data[i].LstFilePath.length + ')</b></span >\
                     </div>\
                    </div>');
                $("#div-lst-img").append('<div id="detail_content_' + data[i].TypeId + '" style="display:none" class="row div-lst-img"></div>');
                var lstImg = data[i].LstFilePath;
                if (lstImg != null && lstImg.length > 0) {
                    for (var k = 0; k < lstImg.length; k++) {
                        $("#detail_content_" + data[i].TypeId).append('<div class="wapperImage col-md-4">\
                            <div class="photoHD">\
                                <img src="'+ lstImg[k].FilePath.replaceAll('\\', '/') + '" />\
                             </div>\
                            <div class="div-remove">\
                                </div>\
                     </div >');
                    }
                }
                if (i == 0) {
                    $("#m_accordion_5_item_1_head_" + data[i].TypeId).css("background", "#c5b0b0");
                    $("#detail_content_" + data[i].TypeId).show();
                    //new FullscreenSlideshow().init($('#detail_content_' + data[i].TypeId));
                }
            }
        }

    }

    var dataSourceHistoryComment = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var LoanId = _loanId;
                var pageIndex = options.data.page;
                var gridPagesize = baseCommon.Option.Pagesize;
                var data = {
                    "LoanBriefID": LoanId,
                    "PageSize": 1000,
                    "PageIndex": pageIndex
                };

                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetHistoryCommentLenderLos, JSON.stringify(data), function (res) {
                    if (res.Result != 1) {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                    options.success(res);
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null || response.Data.length == 0) {
                    baseCommon.ShowErrorNotLoad("Không có dữ liệu!");
                    var data = [];
                    return data;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        }
        ,
        batch: true,
        pageSize: 1000
    });
    var InitTabLichSuXetDuyet = function () {
        $("#XetDuyet").kendoGrid({
            dataSource: dataSourceHistoryComment,
            scrollable: true,
            pageable: true,
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordXetDuyet #",
                    width: 20
                },
                {
                    field: "ShopName",
                    title: "Tên Shop",
                    width: 40
                },
                {
                    field: "FullName",
                    title: "Người thao tác",
                    width: 50
                },
                {
                    field: "CreateDate",
                    title: "Ngày",
                    width: 60,
                    template: function (row) {
                        var html = "";
                        if (row == 0 || row == null || row.CreateDate == "undefined" || row.CreateDate == null) {
                            return html;
                        }
                        html = baseCommon.FormatDate(row.CreateDate, 'DD/MM/YYYY HH:mm');
                        return html;
                    }
                },
                {
                    field: "Comment",
                    title: "Nội dung",
                    width: 150,
                    template: function (row) {
                        var html = "";
                        if (row == 0 || row == null || row.Comment == "undefined" || row.Comment == null) {
                            return html;
                        }
                        html = `<span>${row.Comment}</span>`;
                        return html;
                    }
                },
            ],
            dataBinding: function () {
                recordXetDuyet = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
        });
    }
    var GetHistoryComment = function () {
        var gridXetDuyet = $("#XetDuyet").data("kendoDropDownList");
        if (typeof gridXetDuyet == "undefined" || gridXetDuyet == null) {
            recordXetDuyet = 0;
            InitTabLichSuXetDuyet();
        } else {
            $("#XetDuyet").data("kendoDropDownList").read();
        }
    }

    return {
        Init: Init,
        GridRefresh: GridRefresh,
        GetDataHistoryCommentLOS: GetDataHistoryCommentLOS,
        Save_CommentLenderLOS: Save_CommentLenderLOS,
        UnlockLoanLender: UnlockLoanLender,
        CreateLoanDisbursement: CreateLoanDisbursement,
        Save_ReturnLoan: Save_ReturnLoan,
        InitSaveReturnLoan: InitSaveReturnLoan,
        InitPushLoanForLender: InitPushLoanForLender,
        //GetLenderInPushLoan: GetLenderInPushLoan,
        Save_PushLoanForLender: Save_PushLoanForLender,
        DetailModal: DetailModal,
        InitDetailLoan: InitDetailLoan,
        GetDataImagesLos: GetDataImagesLos,
        ShowFileInModal: ShowFileInModal,
        InitTabLichSuXetDuyet: InitTabLichSuXetDuyet,
        GetHistoryComment: GetHistoryComment,
        InitRutHS: InitRutHS,
        Save_RutHS: Save_RutHS,
        Option: Option,
        RowClick: RowClick
    };
}
function ShowImg(TypeId) {
    $(".check-click").css("background", "#f7f8fa");
    $("#m_accordion_5_item_1_head_" + TypeId).css("background", "#c5b0b0");
    $(".div-lst-img").hide();
    $("#detail_content_" + TypeId).show();
    new FullscreenSlideshow().init($('#detail_content_' + TypeId));
}
$(document).ready(function () {
    loan.Init();
});