﻿var reportTransaction = function () {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var GeneralSearch = $('#txt_search').val();
                var Status = -111;
                var FromDate = $('#txt_search_fromDate').val();
                var ToDate = $('#txt_search_toDate').val();
                var AffID = USER_ID; //USER_ID,
                var data = {
                    GeneralSearch: GeneralSearch,
                    Status: parseInt(Status),
                    FromDate: FromDate,
                    ToDate: ToDate,
                    AffID: AffID,
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize,
                    IsCollaborator: 1
                };
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetReportTransactionOfLender, JSON.stringify(data), function (res) {
                    if (res.Result === 1) {
                        options.success(res);
                        TotalMoneyDisbursement = 0;
                        TotalMoneyTopup = 0;
                        SumMoneyOfLender = 0;
                        TotalCommissionMoney = 0;
                        if (res.Data.length > 0) {
                            TotalMoneyDisbursement = res.Data[0].TotalMoneyDisbursement;
                            TotalMoneyTopup = res.Data[0].TotalMoneyTopup;
                            TotalCommissionMoney = res.Data[0].TotalCommissionMoney;
                        }
                        SumMoneyOfLender = TotalMoneyDisbursement + TotalMoneyTopup;
                        $('#lbl_TotalMoneyDisbursement').text(formatCurrency(TotalMoneyDisbursement));
                        $('#lbl_TotalMoneyTopup').text(formatCurrency(TotalMoneyTopup));
                        $('#lbl_SumMoneyOfLender').text(formatCurrency(SumMoneyOfLender));
                        $('#lbl_TotalCommissionMoney').text(formatCurrency(TotalCommissionMoney));
                    } else {
                        options.success(null);
                    }
                }, "Thao tác thất bại, vui lòng thử lại sau!");
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = 0;
                if (response != null && response.Total != null && response.Total > 0) {
                    total = response.Total;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response == null || response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    function Grid_change(e) {
        var selectedRows = this.select();
        var selectedDataItems = [];
        for (var i = 0; i < selectedRows.length; i++) {
            var dataItem = this.dataItem(selectedRows[i]);
            selectedDataItems.push(dataItem);
        }
        // selectedDataItems contains all selected data items
    }
    var InitGrid = function () {
        $("#grid").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [10, 20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            change: Grid_change,
            selectable: "row",
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #",
                    width: 90
                },
                {
                    field: 'LenderFullName',
                    title: 'Tên NDT',
                    textAlign: 'left',
                },
                {
                    field: 'LenderPhone',
                    title: 'SĐT',
                    textAlign: 'center',
                },
                {
                    field: 'ContractTypeName',
                    title: 'Loại HĐ',
                    textAlign: 'center',
                },
                {
                    field: 'StrTransactionDate',
                    title: 'Thời gian giao dịch',
                    textAlign: 'center',
                },
                {
                    field: 'MoneyDisbursement',
                    title: 'Số tiền NDT giải ngân',
                    textAlign: 'left',
                    template: function (row) {
                        return formatCurrency(row.MoneyDisbursement);
                    },
                },
                {
                    field: 'MoneyTopup',
                    title: 'Số tiền NDT vào tiền',
                    textAlign: 'left',
                    template: function (row) {
                        return formatCurrency(row.MoneyTopup);
                    },
                },
                {
                    field: 'CommissionRate',
                    title: 'Hệ số hoa hồng',
                    textAlign: 'left',
                    template: function (row) {
                        return formatCurrency(row.CommissionRate);
                    },
                },
                {
                    field: 'MoneyCommission',
                    title: 'Tiền hoa hồng',
                    textAlign: 'left',
                    template: function (row) {
                        return formatCurrency(row.MoneyCommission);
                    },
                },
            ]
        });
    };

    var GridRefresh = function () {
        var grid = $("#grid").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };
    var SetFromDateToDate = function () {
        var date = new Date();
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        $("#txt_search_fromDate").datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            language: "vi",
        }).datepicker("setDate", getFormattedDate(firstDay));
        $("#txt_search_toDate").datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            language: "vi",
        }).datepicker("setDate", getFormattedDate(date));
    }
    var init = function () {
        SetFromDateToDate();
        GridRefresh();
        $('#btnGetData').click(function () {
            GridRefresh();
        });
    };
    return {
        init: init
    }
}();