﻿
var list = function () {
    var Option = {
        Approved: 1,
        Delete: 2
    };
    var rowClicked;
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var GeneralSearch = $('#txt_search').val();
                var Status = $('#sl_search_status').val();
                var data = {
                    KeySearch: GeneralSearch,
                    Status: parseInt(Status),
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.ListCollaboratorLender, JSON.stringify(data), function (res) {
                    if (res.Result == 1) {
                        options.success(res);
                    } else {
                        options.success(null);
                    }
                }, "Thao tác thất bại, vui lòng thử lại sau!");
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = 0;
                if (response != null && response.Total != null && response.Total > 0) {
                    total = response.Total;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    function Grid_change(e) {
        var selectedRows = this.select();
        var selectedDataItems = [];
        for (var i = 0; i < selectedRows.length; i++) {
            var dataItem = this.dataItem(selectedRows[i]);
            selectedDataItems.push(dataItem);
        }
        gridSelectedRowData = selectedDataItems[0];
        setTimeout(function () {
            var jsonData = JSON.stringify(gridSelectedRowData);
            //if (rowClicked == Option.loadInfoAff) {
            //    affList.loadInfoAff(jsonData);
            //}
            //rowClicked = Option.loadInfoAff;
        }, 5);
    }
    var InitGrid = function (objQuery) {
        $("#grid").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [10, 20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            change: Grid_change,
            selectable: "row",
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #",
                    width: 90
                },
                {
                    field: 'FullName',
                    title: 'Họ tên',
                    textAlign: 'left',
                },
                {
                    field: 'PhoneNumber',
                    title: 'Số điện thoại',
                    textAlign: 'left',
                },
                {
                    field: 'CreateDate',
                    title: 'Thời gian tạo',
                    textAlign: 'center',
                    template: function (row) {
                        var html = "";
                        if (row.CreateDate != null || row.CreateDate != "0001-01-01T00:00:00") {
                            html = moment(row.CreateDate).format("DD/MM/YYYY HH:mm:ss");
                        }
                        return html;
                    }
                },
                {
                    field: 'CodeCollaborator',
                    title: 'Mã CTV',
                    textAlign: 'left',
                },
                {
                    field: 'AffName',
                    title: 'Tên CTV',
                    textAlign: 'left',
                },
                {
                    field: 'Status',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    template: function (row) {
                        if (row.Status == 1) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill"> Đã duyệt </span>';
                        }
                        else if (row.Status == 0) {
                            return '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">Chờ duyệt</span>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Hủy</span>';
                        }
                    },
                },
                {
                    field: 'Note',
                    title: 'Ghi chú',
                    textAlign: 'left',
                },

                {
                    title: 'Chức năng',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        if (row.Status == 0) {
                            html += '<a class="btn btn-primary btn-icon btn-sm"  onclick="list.RowClick(' + list.Option.Approved + ',\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                                    <i class="fa fa-edit"></i>\
                                 </a>\
                                <a class="btn btn-danger btn-icon btn-sm" title="Sửa" data-toggle="modal" data-target="#modal_cannel_CollaboratorLender" title="Xóa" onclick="list.RowClick(' + list.Option.Delete + ',\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                                    <i class="fa fa-trash"></i>\
                                 </a>';
                        }
                        return html;
                    }
                }
            ]
        });
    };
    var RowClick = function (rowClick, row) {
        rowClicked = rowClick;
        var data = JSON.parse(row);
        if (rowClicked == Option.Approved) {
            list.ApprovedCollaboratorLender(row);
        } else if (rowClicked == Option.Delete) {
            list.ShowInforDelete(row);
        }
    };
    var GridRefresh = function () {
        var grid = $("#grid").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };
    var ShowInforDelete = function (jsondata) {
        var data = JSON.parse(jsondata);
        $("#hdd_CollaboratorLenderID").val(data.CollaboratorLenderID);
        $("#title_cannel").text(data.FullName);
    }
    var ApprovedCollaboratorLender = function (jsondata) {
        var data = JSON.parse(jsondata);
        swal.fire({
            html: "Bạn có chắc chắn duyệt NĐT <b>" + data.FullName + "<b>?",
            type: "warning",
            showCancelButton: !0,
            cancelButtonText: "Hủy",
            confirmButtonText: "Đồng ý"
        }).then(function (result) {
            if (result.value) {
                var request = {
                    "CollaboratorLenderID": parseInt(data.CollaboratorLenderID),
                    "UserID": USER_ID
                };
                baseCommon.AjaxDone("POST", baseCommon.Endpoint.ApprovedCollaboratorLender, JSON.stringify(request), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        GridRefresh()
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                }, "Duyệt CTV lender không thành công vui lòng thử lại!");
            }
        });
    }
    var CannnelCollaboratorLender = function () {
        $('#btn_cannel').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    txt_reasonCannnel: {
                        required: true,
                    }
                },
                messages: {
                    txt_reasonCannnel: {
                        required: "Vui lòng nhập lý do hủy",
                    }
                }
            });
            if (!form.valid()) {
                return;
            }
            var request = {
                Note: $("#txt_reasonCannnel").val(),
                CollaboratorLenderID: parseInt($("#hdd_CollaboratorLenderID").val()),
                UserID: USER_ID
            }
            console.log(request);
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            baseCommon.AjaxDone("POST", baseCommon.Endpoint.CannelCollaboratorLender, JSON.stringify(request), function (respone) {
                if (respone.Result == 1) {
                    baseCommon.ShowSuccess(respone.Message);
                    // $('#btnGetData').trigger('click');
                    GridRefresh()
                    $('#modal_cannel_CollaboratorLender').modal('hide');
                } else {
                    baseCommon.ShowErrorNotLoad(respone.Message);
                }
            });
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
        });
    };
    var Init = function () {
        GridRefresh();
        $('#sl_search_status').on('change', function () {
            GridRefresh();
        });
        $('#btnGetData').on('click', function () {
            GridRefresh();
        });
        $("#txt_search").on('keypress', function (e) {
            if (e.which === 13) {
                GridRefresh();
            }
        });
        CannnelCollaboratorLender();
    };
    return {
        Init: Init,
        Option: Option,
        RowClick: RowClick,
        ShowInforDelete: ShowInforDelete,
        ApprovedCollaboratorLender: ApprovedCollaboratorLender,
        CannnelCollaboratorLender: CannnelCollaboratorLender
    };
}();
