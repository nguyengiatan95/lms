﻿var home = function () {
    var init = function () {
        if (USER_TYPE == 4) {
            baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetLinkAff, null, function (respone) {
                $('#link_aff').html(`<a href="${respone.Data}" style="font-size:20px" target="_blank"> Link giới thiệu NĐT : ${respone.Data}</a>`);
            });
        }
    };
    return {
        init: init
    }
}();
