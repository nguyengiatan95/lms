﻿var recordGrid = 0;
var ticketLender = function () {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var tickKetID = $('#type_ticketID').val();
                if (tickKetID == null || tickKetID == "" || tickKetID.trim() == "") {
                    tickKetID = -111
                }
                var FromDate = $('#fromDate').val();
                var ToDate = $('#toDate').val();
                var status = $('#status_ticketID').val();
                if (status == null || status == "" || status.trim() == "") {
                    status = -111
                }
                var data = {
                    Status: parseInt(status),
                    Type: parseInt(tickKetID),
                    FromDate: FromDate,
                    ToDate: ToDate,
                    PageIndex: options.data.page,
                    PageSize: baseCommon.Option.Pagesize
                };
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetTicketByConditions, JSON.stringify(data), function (res) {
                    if (res.Result == 1) {
                        options.success(res);
                    }
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data.Data != null && response.Data.Data.length > 0) {
                    total = response.Data.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data.Data == null) {
                    baseCommon.ShowErrorNotLoad("Không có dữ liệu!");
                    var fruits = [];
                    return fruits;
                }
                return response.Data.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function (objQuery) {
        $("#dv_result").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                input: true,
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 4) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #",
                    attributes: { style: "text-align:center;" },
                },
                {
                    field: 'TypeName',
                    title: 'Loại',
                    textAlign: 'center',
                },
                {
                    field: 'FromDepartment',
                    title: 'Từ',
                    textAlign: 'left',
                },
                {
                    field: 'ToDepartment',
                    title: 'Đến',
                    textAlign: 'left',
                },
                {
                    field: 'Note',
                    title: 'Nội dung',
                    textAlign: 'left',
                    width: 300
                },
                {
                    field: 'CreateDate',
                    title: 'Ngày tạo',
                    textAlign: 'center',
                    template: function (row) {
                        return `${row.CreateDate == null ? "" : formattedDateHourMinutes(row.CreateDate)} <div>${row.FromUserName}</div>`;
                    },
                },
                {
                    field: 'Status',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    template: function (row) {
                        if (row.Status == 1) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Đã xử lý</span>';
                        } else if (row.Status == 0) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Chờ xử lý</span>';
                        }
                        else if (row.Status == -11) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Hủy</span>';
                        }
                    },
                },
                {
                    field: 'Action',
                    title: 'Chức năng',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var htmlAction = "";
                        if (row.Status == 0) {
                            htmlAction += '\
                          <a class="btn btn-primary btn-icon btn-sm" title="Sửa" data-toggle="modal" data-target="#modal_add_edit_ticket" onclick="ticketLender.loadDetailTicket(\''+ JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                            <i class="fa fa-edit"></i>\
                          </a>';
                            htmlAction += '\
                          <a class="btn btn-danger btn-icon btn-sm" title="Hủy" onclick="ticketLender.deleteTicket('+ row.TicketID + ')">\
                            <i class="fa fa-trash"></i>\
                          </a>';
                        }
                        return htmlAction;
                    }
                }
            ],
        });
    };
    var GridRefresh = function () {
        $("#dv_result").data("kendoGrid").dataSource.read();
    };
    var sumitFormSave = function () {
        $('#btn_save_ticket').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    fromDepartment: {
                        required: true,
                    },
                    toDepartment: {
                        required: true,
                    },
                    type: {
                        required: true,
                    },
                    note: {
                        required: true,
                    },
                },
                messages: {
                    fromDepartment: {
                        required: "Vui lòng chọn phòng ban gửi",
                    },
                    toDepartment: {
                        required: "Vui lòng chọn phòng ban nhận",
                    },
                    type: {
                        required: "Vui lòng chọn loại",
                    },
                    note: {
                        required: "Vui lòng nhập nội dung",
                    },
                }
            });
            if (!form.valid()) {
                return;
            }
            var fromDepartment = $('#sl_ticket_create_fromDepartment').val();
            var toDepartment = $('#sl_ticket_create_toDepartment').val();
            var type = $('#sl_ticket_create_type').val();
            var note = $('#txt_ticket_create_note').val();
            var ticketID = $('#hdd_create_ticketID').val();
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            var request = {
                "FromDepartmentID": parseInt(fromDepartment),
                "ToDepartmentID": parseInt(toDepartment),
                "FromUserID": parseInt(userLogin_UserID),
                "Type": parseInt(type),
                "Note": note,
                "TicketID": parseInt(ticketID),
            };
            if (ticketID == 0) {
                baseCommon.AjaxDone("POST", baseCommon.Endpoint.CreateTicket, JSON.stringify(request), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        $('#btnGetData').trigger('click');
                        $('#modal_add_edit_ticket').modal('hide');
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                });
            } else {
                baseCommon.AjaxDone("POST", baseCommon.Endpoint.UpdateTicket, JSON.stringify(request), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        $('#btnGetData').trigger('click');
                        $('#modal_add_edit_ticket').modal('hide');
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                });
            }
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
        });
    };
    var loadDetailTicket = function (jsonData = '') {
        if (jsonData == '') {
            $('#headerId').text('Thêm mới');
            $('#sl_ticket_create_fromDepartment').val('').change();
            $('#sl_ticket_create_toDepartment').val('').change();
            $('#sl_ticket_create_type').val('').change();
            $('#txt_ticket_create_note').val('');
            $('#hdd_create_ticketID').val(0);
            $('#btn_save_ticket').html('<i class="fa fa-save"></i> Thêm mới');
        } else {
            var data = JSON.parse(jsonData);
            $('#headerId').text('Cập nhật');
            $('#sl_ticket_create_fromDepartment').val(data.FromDepartmentID).change();
            $('#sl_ticket_create_toDepartment').val(data.ToDepartmentID).change();
            $('#sl_ticket_create_type').val(data.Type).change();
            $('#txt_ticket_create_note').val(data.Note);
            $('#hdd_create_ticketID').val(data.TicketID);
            $('#btn_save_ticket').html('<i class="fa fa-save"></i> Cập nhật');
        }
    }
    var deleteTicket = function (ticketID) {
        swal.fire({
            html: "Bạn có chắc chắn muốn xóa yêu cầu này không?",
            type: "warning",
            showCancelButton: !0,
            cancelButtonText: "Hủy",
            confirmButtonText: "Đồng ý"
        }).then(function (result) {
            if (result.value) {
                var request = {
                    "UserID": parseInt(userLogin_UserID),
                    "TicketID": parseInt(ticketID),
                };
                baseCommon.AjaxDone("POST", baseCommon.Endpoint.ChangeStatusTicket, JSON.stringify(request), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        $('#btnGetData').trigger('click');
                        $('#modal_add_edit_ticket').modal('hide');
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                });
            }
        });
    }
    var Init = function () {
        InitGrid()
        $("#fromDate").on('change', function (e) {
            $('#btnGetData').click();
        });
        $("#toDate").on('change', function (e) {
            $('#btnGetData').click();
        });
        $("#type_ticketID").on('change', function (e) {
            $('#btnGetData').click();
        });
        $("#status_ticketID").on('change', function (e) {
            $('#btnGetData').click();
        });
        $("#btnGetData").on('click', function (e) {
            GridRefresh();
        });
    }
    return {
        Init: Init,
        GridRefresh: GridRefresh,
        sumitFormSave: sumitFormSave,
        loadDetailTicket: loadDetailTicket,
        deleteTicket: deleteTicket
    };
}();