﻿var paymentLender = function () {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var generalSearch = $('#txt_search').val();
                var paymentTime = $('#sl_search_payment').val();
                var data = {
                    generalSearch: generalSearch,
                    paymentTime: paymentTime,
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                }
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.PaymentLender, JSON.stringify(data), function (res) {
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    options.success(res);
                    if (res.Result == 1) {
                    } else {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                if (response != null && response.Total != null) {
                    return response.Total;
                }
                return 0; // total is returned in the "total" field of the response 
            },
            data: function (response) {
                var data = [];
                if (response != null && response.Data != null) {
                    data = response.Data;
                }
                return data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var initGrid = function () {
        $("#dv_data_paymentMoney").kendoGrid({
            dataSource: dataSource,
            selectable: "row",
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    this.autoFitColumn(i);
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'LenderCode',
                    title: 'Tên đại lý',
                    textAlign: 'left',
                    template: function (row) {
                        var html = "";
                        html = `<span>${row.LenderCode}</span>`;
                        return html;
                    }
                },
                {
                    field: 'FullName',
                    title: 'Người đại diện',
                    textAlign: 'left',
                },
                {
                    field: 'BankNumber',
                    title: 'Số tài khoản',
                    textAlign: 'center',
                },
                {
                    field: 'BeginMoney',
                    title: 'Dư đầu kì',
                    textAlign: 'right',
                },
                {
                    field: 'TimaThuHo',
                    title: 'Tima thu hộ gốc, lãi phí',
                    textAlign: 'right',
                },
                {
                    field: 'Goc',
                    title: 'Gốc',
                    textAlign: 'right',
                },
                {
                    field: 'Lai',
                    title: 'Lãi',
                    textAlign: 'right',
                },
                {
                    field: 'tax',
                    title: 'Thuê thu nhập cá nhân',
                    textAlign: 'right',
                },
                {
                    field: 'Phi',
                    title: 'Phí',
                    textAlign: 'right',
                },
                {
                    field: 'baohiemnhadautu',
                    title: 'Bảo hiểm NĐT đã hoàn lại Tima',
                    textAlign: 'right',
                },
                {
                    field: 'thuhotima',
                    title: 'Bảo hiểm NĐT thu hộ Tima',
                    textAlign: 'right',
                },
                {
                    field: 'timathanhtoan',
                    title: 'Tima thanh toán gốc lãi phí',
                    textAlign: 'right',
                },
                {
                    field: 'ducuoiky',
                    title: 'Dư cuối kì',
                    textAlign: 'right',
                },
                {
                    title: "Hành động",
                    selectable: false,
                    template: function (row) {
                        var strAction = '';
                        strAction += '<button class="btn btn-outline-danger btn-icon" title="Xác nhận chuyển tiền">\
                                            <i onclick="javascript:;" class="fa fa-times"></i></button> ';
                        return strAction;
                    }
                }
            ]
        });
    };
    var init = function () {
        initGrid();
    }
    return {
        init: init
    }
}();