﻿
var datatable, INVOICETYPE_SELECTED;
var recordGrid = 0;
var invoice = new function () {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var BankCardID = $('#BankCardID').val();
                if (BankCardID == null || BankCardID == "" || BankCardID.trim() == "") {
                    BankCardID = -111
                }
                var InvoiceSubType = $('#InvoiceSubType').val();
                if (InvoiceSubType == null || InvoiceSubType == "" || InvoiceSubType.trim() == "") {
                    InvoiceSubType = groupInvoiceType_ReceiptCustomerConsider
                }
                var FromDate = $('#fromDate').val();
                var ToDate = $('#toDate').val();
                var numberSearch = $('#txt_search').val();
                if (numberSearch == null || numberSearch == "" || numberSearch.trim() == "") {
                    numberSearch = "0"
                }
                numberSearch = formatTextMoneyToNumber(numberSearch);
                var data = {
                    InvoiceType: -111,
                    InvoiceSubType: parseInt(InvoiceSubType),
                    ShopID: -111,
                    BankCardID: parseInt(BankCardID),
                    Status: -111,
                    FromDate: FromDate,
                    ToDate: ToDate,
                    MoneySearch: parseInt(numberSearch),
                    PageIndex: options.data.page,
                    PageSize: baseCommon.Option.Pagesize
                };
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetInvoiceInfosByCondition, JSON.stringify(data), function (res) {
                    if (res.Result == 1) {
                        options.success(res);
                    }
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data.Data != null && response.Data.Data.length > 0) {
                    total = response.Data.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data.Data == null) {
                    baseCommon.ShowErrorNotLoad("Không có dữ liệu!");
                    var fruits = [];
                    return fruits;
                }
                return response.Data.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function (objQuery) {
        $("#dv_result").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                input: true
            },
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 4) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #",
                    attributes: { style: "text-align:center;" },
                },
                {
                    field: 'InvoiceSubTypeName',
                    title: 'Loại phiếu',
                    template: function (row) {
                        var html = `<a href="javascript:;"  data-toggle="modal" data-target="#ModalInvoice" title="Thông tin chi tiết" onclick="invoice.loadInfoInvoice(\'${JSON.stringify(row).replace(/"/g, '&quot;')}\')">\
                                    ${row.InvoiceSubTypeName}\
                                    <br/> <span class="item-desciption"> ${row.InvoiceTypeName} </span>\
                                    </a>`;
                        return html;
                    }
                },
                {
                    field: 'TransactionMoney',
                    title: 'Số tiền',
                    attributes: { style: "text-align:right;" },
                    headerAttributes: { style: "text-align: right" },
                    template: function (row) {
                        if (row.InvoiceSubType == groupInvoiceType_PaySlipLenderWithdraw) {
                            return `${html_money(row.TransactionMoney)}\
                                    <br/> <span class="item-desciption"> ${row.SourceName} </span>`;
                        } else if (row.InvoiceSubType == groupInvoiceType_ReceiptLenderCapital || row.InvoiceSubType == groupInvoiceType_ReceiptCustomerConsider) {
                            return `${html_money(row.TransactionMoney)}\
                                    <br/> <span class="item-desciption"> ${row.DestinationName} </span>`;
                        }
                        return `${html_money(row.TransactionMoney)}`;
                    }
                },
                {
                    field: 'SourceName',
                    title: 'Lender',
                    template: function (row) {
                        if (row.InvoiceSubType == groupInvoiceType_PaySlipLenderWithdraw) {
                            return `${row.DestinationName} `;
                        } else if (row.InvoiceSubType == groupInvoiceType_ReceiptLenderCapital || row.InvoiceSubType == groupInvoiceType_ReceiptCustomerConsider) {
                            return `${row.SourceName} `;
                        }
                        return '';
                    }
                },

                {
                    field: 'Description',
                    title: 'Diễn giải',
                    width: 450
                },
                {
                    field: 'Status',
                    title: 'Trạng thái',
                    attributes: { style: "text-align:center;" },
                    headerAttributes: { style: "text-align: center" },
                    template: function (row) {
                        if (row.Status >= 3) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">' + row.StatusName + '</span>';
                        }
                        else if (row.Status >= 1) {
                            return '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">' + row.StatusName + '</span>';
                        } else {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + row.StatusName + '</span>';
                        }
                    }
                },
                {
                    field: 'TransactionDate',
                    title: 'Ngày giao dịch',
                    template: function (row) {
                        var date = moment(row.TransactionDate);
                        var html = date.format("DD/MM/YYYY HH:mm");
                        return html;
                    }
                },
                {
                    field: 'UserNameCreate',
                    title: 'Người/ngày tạo',
                    template: function (row) {
                        var date = moment(row.CreateDate);
                        var html = row.UserNameCreate + ' <br/> <span class="item-desciption">' + date.format("DD/MM/YYYY") + '</span >';
                        return html;
                    }
                },
                {
                    field: 'Action',
                    title: 'Hành động',
                    template: function (row, index, datatable) {
                        if (row.Status == 1) { // xác minh phiếu treo
                            return '\
                          <a class="btn btn-success btn-icon btn-sm" title="Xác minh" data-toggle="modal" data-target="#ModalInvoice" onclick="invoice.loadInfoInvoice(\''+ JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                            <i class="fa fa-stamp"></i>\
                          </a>';
                        } else {
                            return '\
                          <a class="btn btn-success btn-icon btn-sm" title="Lịch sử" data-toggle="modal" data-target="#ModalInvoice" onclick="invoice.loadInfoInvoice(\''+ JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                            <i class="fa fa-book"></i>\
                          </a>';
                        }
                    }
                }
            ]
        });
    };
    var GridRefresh = function () {
        $("#dv_result").data("kendoGrid").dataSource.read();
    };
    var Init = function () {
        InitGrid();
        $("#fromDate").on('change', function (e) {
            $('#btnGetData').click();
        });
        $("#toDate").on('change', function (e) {
            $('#btnGetData').click();
        });
        $("#BankCardID").on('change', function (e) {
            $('#btnGetData').click();
        });
        $("#InvoiceSubType").on('change', function (e) {
            $('#btnGetData').click();
        });
        $('#txt_search').keyup(function () {
            doSearch();
        });
        $("#btnGetData").on('click', function (e) {
            GridRefresh();
        });
    };
    var delayTimer = 1000;
    var doSearch = function () {
        clearTimeout(delayTimer);
        delayTimer = setTimeout(function () {
            GridRefresh();
        }, 500); // Will do the ajax stuff after 1000 ms, or 1 s
    };

    var loadInfoInvoice = function (jsonData = '') {
        var data = JSON.parse(jsonData);
        $("#VerifyInvoiceHeaderId").html("Xác minh phiếu treo");
        $('#TransactionDate').text(formattedDateHourMinutes(data.TransactionDate));
        $('#TransactionMoney').html(html_money(data.TransactionMoney));

        var htmlStatus = "";
        if (data.Status >= 3) {
            htmlStatus = '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">' + data.StatusName + '</span>';
        }
        else if (data.Status >= 1) {
            htmlStatus = '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">' + data.StatusName + '</span>';
        } else {
            htmlStatus = '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + data.StatusName + '</span>';
        }
        $('#StatusName').html(htmlStatus);
        $('#DestinationName').text(data.DestinationName);
        $('#Description').html(data.Description);
        $('#Note_Verify').val('');
        $('#LenderID').val(0).change();
        $('#InvoiceID').val(data.InvoiceID);
        $('#btn_VerifyInvoiceConsiderCustomer_invoice').hide();
        if (data.Status == 1) {
            $('#btn_VerifyInvoiceConsiderCustomer_invoice').show();
        }
    };
    var GridLichSu = function () {
        $("#GridLichSu").kendoGrid({
            dataSource: dataSourceGridLichSu,
            pageable: true,
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 3) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'CreateDate',
                    title: 'Ngày thao tác',
                    textAlign: 'left',
                    template: function (row) {
                        if (row == null || row.CreateDate == null) {
                            return "";
                        }
                        var date = moment(row.CreateDate);
                        var html = date.format("DD/MM/YYYY HH:mm");
                        return html;
                    }
                },
                {
                    field: 'UserName',
                    title: 'Người thao tác'
                },
                {
                    field: 'Note',
                    title: 'Nội dung',
                }
                ,
                {
                    field: 'StatusName',
                    title: 'Trạng thái',
                    template: function (row) {
                        if (row.Status >= 3) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">' + row.StatusName + '</span>';
                        }
                        else if (row.Status >= 1) {
                            return '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">' + row.StatusName + '</span>';
                        } else {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + row.StatusName + '</span>';
                        }
                    }
                }
            ]
        });
    };
    var dataSourceGridLichSu = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var _InvoiceID = $('#InvoiceID').val();
                if (typeof _InvoiceID == "undefined" || _InvoiceID == null || _InvoiceID == "" || _InvoiceID.trim() == "") {
                    baseCommon.ShowErrorNotLoad("Thao tác thất bại, vui lòng thử lại sau!");
                    return;
                }
                var pageIndex = options.data.page;
                var data = {
                    InvoiceID: parseFloat(_InvoiceID),
                    InvoceSubType: 0,
                    PageIndex: pageIndex,
                    PageSize: baseCommon.Option.Pagesize
                };
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetHistoryCommentInvoice, JSON.stringify(data), function (res) {
                    if (res.Result != 1) {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                    options.success(res);
                    $("#LichSuLoading").hide();
                });
            }
        },
        serverPaging: true,
        serverAggregates: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    baseCommon.ShowErrorNotLoad("Không có dữ liệu!");
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
            aggregate: function (response) {
                if (response.Data == null) {
                    baseCommon.ShowErrorNotLoad("Không có dữ liệu!");
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGridLichSu = function (isVisible) {
        $("#LichSuLoading").show();
        setTimeout(function () { GridLichSu(); }, 500);
    }
    var InvoiceAction = function () {
        baseCommon.ButtonSubmit(false, '#btn_Verify_invoice');
        var LenderID = $('#LenderID').val();
        if (typeof LenderID == "undefined" || LenderID == null || LenderID == "" || LenderID == 0) {
            baseCommon.ShowErrorNotLoad("Bạn chưa chọn Lender!");
            baseCommon.ButtonSubmit(true, '#btn_Verify_invoice');
            return;
        }
        var InvoiceID = $('#InvoiceID').val();
        var CreateBy = baseCommon.GetUser().UserID;
        var Note = $('#Note_Verify').val();
        if (typeof Note == "undefined" || Note == null || Note == '') {
            baseCommon.ShowErrorNotLoad("Bạn chưa nhập nội dung!");
            baseCommon.ButtonSubmit(true, '#btn_Verify_invoice');
            return;
        }
        if (typeof CreateBy == "undefined" || CreateBy == null || CreateBy == 0 || typeof InvoiceID == "undefined" || InvoiceID == null || InvoiceID == 0) {
            baseCommon.ShowErrorNotLoad("Cập nhật không thành công vui lòng thử lại sau!");
            baseCommon.ButtonSubmit(true, '#btn_Verify_invoice');
            return;
        }
        var model = {
            InvoiceID: parseFloat(InvoiceID),
            UserIDCreate: CreateBy,
            Note: Note,
            LenderID: parseFloat(LenderID),
            TypeVerifyInvoiceConsider: 1 // xác minh lender
        };
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.VerifyInvoiceConsiderCustomer, JSON.stringify(model), function (respone) {
            baseCommon.ButtonSubmit(true, '#btn_Verify_invoice');
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                $('#ModalInvoice').modal('hide');
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        });
    }
    return {
        Init: Init,
        GridRefresh: GridRefresh,
        loadInfoInvoice: loadInfoInvoice,
        InitGridLichSu: InitGridLichSu,
        InvoiceAction: InvoiceAction,
    };
}();
