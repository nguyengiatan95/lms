﻿
var lenderListDetail = function () {
    var Danhsachhopdong = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var userLenderID = parseInt($("#hdd_UserID_Value").val());
                baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetContractByUserLender + userLenderID, "", function (res) {
                    if (res.Result == 1) {
                        options.success(res);
                    } else {
                        options.success(null);
                    }
                }, "Thao tác thất bại, vui lòng thử lại sau!");
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = 0;
                if (response != null && response.Total != null && response.Total > 0) {
                    total = response.Total;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGridListLoanLender = function () {
        $("#Danhsachhopdong").kendoGrid({
            dataSource: Danhsachhopdong,
            pageable: {
                pageSizes: [10, 20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            selectable: "row",
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 2) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'TypeLoan',
                    title: 'Loại hợp đồng',
                    textAlign: 'left'
                },
                {
                    field: 'LenderCode',
                    title: 'Mã hợp đồng',
                    textAlign: 'left',
                },
                {
                    field: 'ContractCode',
                    title: 'Số hợp đồng',
                    textAlign: 'left',
                },
                {
                    field: 'ContractDuration',
                    title: 'Thời gian hợp đồng',
                    textAlign: 'left',
                    template: function (row) {
                        return row.ContractDuration + " Tháng";
                    },
                },
                {
                    field: 'TotalMoney',
                    title: 'Số tiền đầu tư',
                    textAlign: 'left',
                    template: function (row) {
                        var html = "";
                        html = `<span>${html_money(row.TotalMoney)}</span>`;
                        return html;
                    }
                },
                {
                    title: 'Chức năng',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        return '\
                          <a class="btn btn-primary btn-icon btn-sm" title="Sửa" data-toggle="modal" data-target="#modal_create_loan_lender" onclick="lenderListDetail.loadInfoLoanOfLender(\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                            <i class="fa fa-edit"></i>\
                          </a>\
                          <a class="btn btn-primary btn-icon btn-sm" title="Cấu hình khẩu vị hợp đồng lender" data-toggle="modal" data-target="#modal_configuration_lender" onclick="lenderListDetail.ConfigurationLender(\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                             <i class="fa fa-cog"></i>\
                          </a>';
                    }
                }
            ]
        });
    };
    var Resetmodal = function myfunction() {
        $("#hdd_create_loan_lender_LenderID").val(0)
        $("#txt_create_loan_lender_contactCode").val("")
        $('#txt_create_loan_lender_ContractDate').val("");
        $("#sl_create_loan_lender_ContractType").val(-111).change();
        $("#txt_create_loan_lender_ContractDuration").val("")
        $('#txt_create_loan_lender_ContractEndDate').val("");
        $("#txt_create_loan_lender_TotalMoneyTopup").val("");
        $("#IsDeposit_false").prop("checked", true);
        $("#sl_create_contract_affID").val(0).change();
    }
    var JsonData = "";
    var loadInfoLoanOfLender = function (jsonData = '') {

        $('#modal_details_lender').modal('hide');
        if (jsonData == '') {

        } else {
            debugger;
            JsonData = jsonData;
            var data = JSON.parse(jsonData);
            $("#hdd_create_loan_lender_LenderID").val(data.LenderID)
            $("#txt_create_loan_lender_contactCode").val(data.ContractCode)
            $('#txt_create_loan_lender_ContractDate').val(data.ContractDate == null ? "" : formattedDate(data.ContractDate));
            $("#sl_create_loan_lender_ContractType").val(data.ContractType).change();
            $("#txt_create_loan_lender_ContractDuration").val(data.ContractDuration)
            $('#txt_create_loan_lender_ContractEndDate').val(data.ContractEndDate == null ? "" : formattedDate(data.ContractEndDate));
            var money = formatCurrency(data.TotalMoney)
            $("#txt_create_loan_lender_TotalMoneyTopup").val(money)
            $("input[name=txt_create_loan_lender_IsDeposit][value=" + data.IsDeposit + "]").attr('checked', 'checked');
            $("#sl_create_contract_affID").val(data.AffID).change();
        }
    };
    var GetDanhsachhopdong = function () {
        var grid = $("#Danhsachhopdong").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGridListLoanLender();
        }
    };

    var UpdateInfoLender = function () {
        $('#btn_edit_save').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    txt_edit_fullname: {
                        required: true,
                    },
                    txt_edit_phone: {
                        required: true,
                    },
                    txt_edit_birthDay: {
                        required: true,
                    }
                },
                messages: {
                    txt_edit_fullname: {
                        required: "Vui lòng nhập tên Lender",
                    },
                    txt_edit_phone: {
                        required: "Vui lòng nhập sđt",
                    },
                    txt_edit_birthDay: {
                        required: "Vui lòng chọn ngày sinh",
                    }
                }
            });
            if (!form.valid()) {
                return;
            }
            var ctity = $("#sl_edit_city").val();
            var districtID = $("#sl_edit_district").val();
            var wardID = $("#sl_edit_ward").val();
            if (ctity == null || ctity == "") {
                ctity = 0;
            }
            if (districtID == null || districtID == "") {
                districtID = 0;
            }
            if (wardID == null || wardID == "" || wardID == -111) {
                wardID = 0;
            }
            var request = {
                UserID: parseInt($("#hdd_edit_User").val()),
                FullName: $("#txt_edit_fullname").val(),
                DateOfBirth: $("#txt_edit_birthDay").val(),
                Phone: $("#txt_edit_phone").val(),
                Email: $("#txt_edit_email").val(),
                Gender: parseInt($("input[name='edit_Gender']:checked").val()),
                PermanentAddress: $("#txt_edit_AddressOfResidence").val(),
                TemporaryAddress: $("#txt_edit_address").val(),
                CityID: parseInt(ctity),
                DistrictID: parseInt(districtID),
                WardID: parseInt(wardID),
                CardNumber: $("#txt_edit_numberCard").val(),
                CardDate: $("#txt_edit_cardDate").val(),
                CardPlace: $("#txt_edit_cardPlace").val(),
                IsVerified: $('#edit_IsVerified').prop('checked') ? 1 : 0
            }
            console.log(request);
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            baseCommon.AjaxDone("POST", baseCommon.Endpoint.UpdateLenderUser, JSON.stringify(request), function (respone) {
                if (respone.Result == 1) {
                    baseCommon.ShowSuccess(respone.Message);
                    setTimeout(function () { location.reload(); }, 500);
                } else {
                    baseCommon.ShowErrorNotLoad(respone.Message);
                }
            });
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
        });
    };
    var ShowModalCreateLoanLender = function () {
        Resetmodal();
        $('#modal_details_lender').modal('hide');
    }
    var HiddenModal = function myfunction() {
        $('#hiddenModal_create_loan_lender').click(function (e) {
            $('#modal_details_lender').modal('show');
            $('#modal_create_loan_lender').modal('hide');
        })
    }

    var CreateUpdateLendercontract = function () {
        $('#btn_create_loan_lender').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    txt_create_loan_lender_contactCode: {
                        required: true,
                    },
                    txt_create_loan_lender_ContractDate: {
                        required: true,
                    }
                },
                messages: {
                    txt_create_loan_lender_contactCode: {
                        required: "Vui lòng nhập mã hợp đồng",
                    },
                    txt_create_loan_lender_ContractDate: {
                        required: "Vui lòng nhập ngày ký hợp đồng",
                    }
                }
            });
            if (!form.valid()) {
                return;
            }
            var lenderID = $("#hdd_create_loan_lender_LenderID").val();
            var contractType = $("#sl_create_loan_lender_ContractType").val();
            var userID = $("#hdd_UserID_Value").val();
            var contractDuration = $("#txt_create_loan_lender_ContractDuration").val();
            var totalMoneyTopup = $("#txt_create_loan_lender_TotalMoneyTopup").val().replace(/[^\d\.]/g, '');
            if (totalMoneyTopup == null || totalMoneyTopup == "") {
                totalMoneyTopup = 0;
            }
            var isDeposit = parseInt($("input[name='txt_create_loan_lender_IsDeposit']:checked").val());
            var request = {
                LenderID: parseInt(lenderID),
                ContractCode: $("#txt_create_loan_lender_contactCode").val(),
                ContractDate: $("#txt_create_loan_lender_ContractDate").val(),
                ContractType: parseInt(contractType),
                ContractDuration: parseInt(contractDuration),
                ContractEndDate: $("#txt_create_loan_lender_ContractEndDate").val(),
                UserID: parseInt(userID),
                TotalMoneyTopup: parseInt(totalMoneyTopup),
                IsDeposit: isDeposit,
                AffID: parseInt($("#sl_create_contract_affID").val() == null ? "0" : $("#sl_create_contract_affID").val()),
            }
            console.log(request);
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            baseCommon.AjaxDone("POST", baseCommon.Endpoint.CreateUpdateLendercontract, JSON.stringify(request), function (respone) {
                if (respone.Result == 1) {
                    baseCommon.ShowSuccess(respone.Message);
                    lenderListDetail.GetDanhsachhopdong();
                    $('#modal_details_lender').modal('show');
                    $('#modal_create_loan_lender').modal('hide');
                } else {
                    baseCommon.ShowErrorNotLoad(respone.Message);
                }
            });
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
        });
    };

    //câu hình hđ lender
    var ConfigurationLender = function (jsonData = '') {
        $("#sl_MoneyLike").val("").change();
        $("#sl_Rate").val("").change();
        $('#modal_details_lender').modal('hide');
        $("input.type_checkbox").prop('checked', false);
        var data = JSON.parse(jsonData);
        $("#txt_title_configuration").text(data.LenderCode)
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetLenderSpicesConfig + data.LenderID, "", function (respone) {
            if (respone.Result == 1) {
                $("#sl_MoneyLike").val(respone.Data.RangeMoney).change();
                $("#sl_Rate").val(respone.Data.RateType).change();
                $("#hdd_LenderSpicesConfigID").val(respone.Data.LenderSpicesConfigID);
                for (var i = 0; i < respone.Data.LoanTimes.length; i++) {
                    $('input.type_checkbox[value="' + respone.Data.LoanTimes[i] + '"]').prop('checked', true);
                }
            }
        });

        $("#hdd_configuration_User").val(data.UserID);
        $("#hdd_configuration_LenderID").val(data.LenderID);
    }
    var UpdateLenderSpicesConfig = function () {
        $('#btn_configuration_lender').click(function (e) {
            var arrayUpdateLenderSpicesConfig = [];
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    sl_MoneyLike: {
                        required: true
                    },
                    sl_Rate: {
                        required: true
                    }
                },
                messages: {
                    sl_MoneyLike: {
                        required: "Chọn số tiền cho vay yêu thích"
                    },
                    sl_Rate: {
                        required: "Chọn hình thức trả lãi yêu thích"
                    }
                }
            });
            if (!form.valid()) {
                return;
            }
            $("input:checkbox[name=ck_time]:checked").each(function () {
                arrayUpdateLenderSpicesConfig.push(parseInt($(this).val()));
            });
            var rate = $("#sl_Rate").val();
            var lenderID = $("#hdd_configuration_LenderID").val();
            var userID = $("#hdd_configuration_User").val();
            var LenderSpicesConfigID = $("#hdd_LenderSpicesConfigID").val();
            var moneyLike = $("#sl_MoneyLike").val();

            var request = {
                LoanTimes: arrayUpdateLenderSpicesConfig,
                UserID: parseInt(userID),
                LenderID: parseInt(lenderID),
                LenderSpicesConfigID: parseInt(userID),
                RangeMoney: parseInt(moneyLike),
                RateType: parseInt(rate),
                CreateBy: USER_ID
            }
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            baseCommon.AjaxDone("POST", baseCommon.Endpoint.UpdateLenderSpicesConfig, JSON.stringify(request), function (respone) {
                if (respone.Result == 1) {
                    baseCommon.ShowSuccess(respone.Message);
                    $('#modal_details_lender').modal('show');
                    $('#modal_configuration_lender').modal('hide');
                } else {
                    baseCommon.ShowErrorNotLoad(respone.Message);
                }
            });
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
        });
    };

    var HiddenModalLenderSpicesConfig = function myfunction() {
        $('#btn_configuration_lender_exit').click(function (e) {
            $('#modal_details_lender').modal('show');
            $('#modal_configuration_lender').modal('hide');
        })
    }

    //end cau hinh

    $('#sl_create_loan_lender_ContractType').on('change', function () {
        if ($(this).val() > 1) {
            $(".c-hidden").attr("hidden", false);
            ChangeDate();
        } else {
            $(".c-hidden").attr("hidden", true);
        }
    });

    function pad2(n) {
        return (n < 10 ? '0' : '') + n;
    }
    var FormatDateDDMMYYYY = function (date) {
        var month = pad2(date.getMonth() + 1);
        var day = pad2(date.getDate());
        var year = date.getFullYear();
        return day + "/" + month + "/" + year;
    }

    $('#txt_create_loan_lender_ContractDate').on('change', function () {
        ChangeDate();
    });
    $('#txt_create_loan_lender_ContractDuration').on('change', function () {
        ChangeDate();
    });

    var ChangeDate = function () {
        //if ($("#sl_create_loan_lender_ContractType").val() > 1) {
        //    return false;
        //}
        var ngaykyhd = $("#txt_create_loan_lender_ContractDate").val();
        var thoihanhd = $("#txt_create_loan_lender_ContractDuration").val();
        if (ngaykyhd == "") {
            return false;
        }
        if (thoihanhd == "") {
            thoihanhd = 0;
        }
        thoihanhd = parseInt(thoihanhd)

        var from = ngaykyhd.split("/")
        var monthnew = new Date(from[2], from[1] - 1, from[0])
        monthnew.setMonth(monthnew.getMonth() + thoihanhd)
        var daynew = monthnew;
        daynew.setDate(daynew.getDate() - 1);
        $("#txt_create_loan_lender_ContractEndDate").val(FormatDateDDMMYYYY(daynew))
    }

    function formatCurrency(num) {
        num = num.toString().replace(/\$|\,/g, '');
        if (isNaN(num))
            num = "0";
        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        cents = num % 100;
        num = Math.floor(num / 100).toString();
        if (cents < 10)
            cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
            num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                num.substring(num.length - (4 * i + 3));
        return (((sign) ? '' : '-') + num);
    }
    $(document).on('keyup', '#txt_create_loan_lender_TotalMoneyTopup', function (e) {
        var loValue = formatCurrency($(this).val().replace(/,/g, '').replace(/\./g, ''));
        $("#txt_create_loan_lender_TotalMoneyTopup").val(loValue);
    });

    return {
        GetDanhsachhopdong: GetDanhsachhopdong,
        ShowModalCreateLoanLender: ShowModalCreateLoanLender,
        CreateUpdateLendercontract: CreateUpdateLendercontract,
        UpdateInfoLender: UpdateInfoLender,
        loadInfoLoanOfLender: loadInfoLoanOfLender,
        HiddenModal: HiddenModal,
        UpdateLenderSpicesConfig: UpdateLenderSpicesConfig,
        ConfigurationLender: ConfigurationLender,
        HiddenModalLenderSpicesConfig: HiddenModalLenderSpicesConfig
    };
}();
