﻿
var lenderList = function () {
    var Option = {
        LoadInfoLender: 1,
        DigitalSignature: 2,
        Accuracy: 3,
        ConfigurationLender: 4,
        UserLenderUpdateStatus: 5
    };
    var rowClicked = Option.LoadInfoLender;
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {

                var GeneralSearch = $('#txt_search').val();
                var Status = $('#sl_search_status').val();
                var IsVerified = $('#sl_search_isverify').val();
                var CreateDate = $('#txt_create').val();
                var data = {
                    GeneralSearch: GeneralSearch,
                    CreateDate: CreateDate,
                    IsVerified: parseInt(IsVerified),
                    Status: parseInt(Status),
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetLenderUserConditions, JSON.stringify(data), function (res) {
                    if (res.Result == 1) {
                        options.success(res);
                    } else {
                        options.success(null);
                    }
                }, "Thao tác thất bại, vui lòng thử lại sau!");
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = 0;
                if (response != null && response.Total != null && response.Total > 0) {
                    total = response.Total;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    function Grid_change(e) {
        var selectedRows = this.select();
        var selectedDataItems = [];
        for (var i = 0; i < selectedRows.length; i++) {
            var dataItem = this.dataItem(selectedRows[i]);
            selectedDataItems.push(dataItem);
        }
        // selectedDataItems contains all selected data items
        gridSelectedRowData = selectedDataItems[0];
        setTimeout(function () {
            var jsonData = JSON.stringify(gridSelectedRowData);
            if (rowClicked == Option.LoadInfoLender) {
                lenderList.loadInfoLender(jsonData);
                /* $('#modal_create_lender').modal('toggle');*/
            }
            rowClicked = Option.LoadInfoLender;
        }, 5);
    }
    var InitGrid = function (objQuery) {
        $("#grid").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [10, 20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            change: Grid_change,
            selectable: "row",
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 2) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'UserName',
                    title: 'Tên đăng nhập',
                    textAlign: 'left',
                    template: function (row) {
                        // callback function support for column rendering
                        return '<a class="kt-font-primary  kt-link"  title="Sửa" data-toggle="modal" data-target="#modal_add_edit_lender" onclick="lenderAddOrEdit.loadInfoLender(\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">' + row.UserName + '</a>';
                    },
                },
                {
                    field: 'FullName',
                    title: 'Họ tên',
                    textAlign: 'left',
                },
                {
                    field: 'NumberCard',
                    title: 'CMT',
                    textAlign: 'left',
                },
                {
                    field: 'Phone',
                    title: 'Số điện thoại',
                    textAlign: 'left',
                },
                {
                    field: 'CreateDate',
                    title: 'Ngày tạo',
                    textAlign: 'left',
                    template: function (row) {
                        return row.CreateDate == null ? "" : formattedDateHourMinutes(row.CreateDate);
                    },
                },
                {
                    field: 'Status',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    template: function (row) {
                        if (row.Status == 1) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Hoạt Động</span>';
                        } else if (row.Status == 0) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Khóa</span>';
                        }
                        else if (row.Status == 3) {
                            return '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">Đang chờ</span>';
                        }
                        else if (row.Status == -11) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Xóa</span>';
                        }
                    },
                },
                {
                    field: 'IsVerified',
                    title: 'Xác minh',
                    textAlign: 'center',
                    template: function (row) {
                        if (row.IsVerified == 1) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Đã xác minh</span>';
                        } else if (row.IsVerified == 0) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Chưa xác minh</span>';
                        }
                    },
                },
                {
                    title: 'Chức năng',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += '<a class="btn btn-primary btn-icon btn-sm" title="Sửa" data-toggle="modal" data-target="#modal_details_lender" onclick="lenderList.RowClick(' + lenderList.Option.LoadInfoLender + ',\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                                    <i class="fa fa-edit"></i>\
                                 </a>\
                                 <a class="btn btn-primary btn-icon btn-sm" title="Chữ ký số" data-toggle="modal" data-target="#modal_create_digital_signarture" onclick="lenderList.RowClick('+ lenderList.Option.DigitalSignature + ',\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                                   <i class="fa fa-signature"></i>\
                                 </a>\
                                 <a class="btn btn-primary btn-icon btn-sm" title="Cập nhật trang thái" onclick="lenderList.RowClick('+ lenderList.Option.UserLenderUpdateStatus + ',\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                                   <i class="fa fa-user-lock"></i>\
                                 </a> ';
                        if (row.IsVerified == 0) {
                            html += ' <a class="btn btn-primary btn-icon btn-sm" title="xác thực"  onclick="lenderList.RowClick(' + lenderList.Option.Accuracy + ',\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                                      <i class="fa fa-check"></i>\
                                     </a> ';
                        }
                        html += ' <a class="btn btn-warning btn-icon btn-sm" title="Reset mật khẩu"  onclick="lenderList.RowClick(' + lenderList.Option.ResetPassword + ',\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                                      <i class="flaticon2-reload"></i>\
                                     </a> ';
                        return html;
                    }
                }
            ]
        });
    };
    var RowClick = function (rowClick, row) {
        rowClicked = rowClick;
        var data = JSON.parse(row);
        if (rowClicked == Option.LoadInfoLender) {
            lenderList.loadInfoLender(row);
        } else if (rowClicked == Option.DigitalSignature) {
            lenderList.ModalCreateLenderDigitalSignature(row);
        }
        else if (rowClicked == Option.Accuracy) {
            lenderList.FastAuthentication(data.UserID, data.FullName)
        }
        else if (rowClicked == Option.ConfigurationLender) {
            lenderList.ConfigurationLender(row);
        } else if (rowClicked == Option.UserLenderUpdateStatus) {
            lenderList.UpdateStatusUserLender(data.UserID, data.FullName, data.Status)
        }
        else if (rowClicked == Option.ResetPassword) {
            lenderList.ResetPassword(data.UserID, data.FullName, data.Phone);
        }
    };
    var GridRefresh = function () {
        var grid = $("#grid").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };
    var loadInfoLender = function (jsonData = '') {
        $(".nav-link").removeClass("active");
        $(".tab-pane").removeClass("active");
        $(".tab-active").addClass("active");
        if (jsonData == '') {
        } else {
            var data = JSON.parse(jsonData);
            $("#title_infor_lender").text(data.FullName)
            $("#hdd_UserID_Value").val(data.UserID);
            $("#hdd_edit_User").val(data.UserID);
            $("#txt_edit_fullname").val(data.FullName);
            $('#txt_edit_birthDay').val(data.DateOfBirth == null ? "" : formattedDate(data.DateOfBirth));
            $("#txt_edit_phone").val(data.Phone);
            $("#txt_edit_email").val(data.Email);
            data.Gender = data.Gender == null ? 0 : data.Gender;
            $("input[name=edit_Gender][value=" + data.Gender + "]").attr('checked', 'checked');
            $("#txt_edit_AddressOfResidence").val(data.PermanentResidenceAddress);
            $("#txt_edit_address").val(data.TemporaryResidenceAddress);
            $("#sl_edit_city").val(data.CityID).change();

            $("#txt_edit_numberCard").val(data.NumberCard);
            $('#txt_edit_cardDate').val(data.CardDate == null ? "" : formattedDate(data.CardDate));
            $("#txt_edit_cardPlace").val(data.CardPlace);
            if (data.IsVerified > 0) {
                $('#edit_IsVerified').prop('checked', true).trigger('change');

            } else {
                $('#edit_IsVerified').prop('checked', false).trigger('change');
            }
            setTimeout(function () {
                $("#sl_edit_district").val(data.DistrictID).change();
                setTimeout(function () {
                    $("#sl_edit_ward").val(data.WardID).change();
                }, 500);
            }, 500);
        }
    };
    var ShowCreateModal = function () {
        resetModal();
    }
    var resetModal = function () {
        UserID: $("#hdd_User").val(0)
        FullName: $("#txt_fullname").val("")
        // DateOfBirth: $("#txt_birthDay").val()
        Phone: $("#txt_phone").val("")
        Email: $("#txt_email").val("")
        // Gender: $("input[name='Gender']:checked").val()
        PermanentAddress: $("#txt_AddressOfResidence").val("")
        TemporaryAddress: $("#txt_address").val("")
        CityID: $("#sl_city").val(0)
        DistrictID: $("#sl_district").val(0)
        WardID: $("#sl_ward").val(0)
        CardNumber: $("#txt_numberCard").val("")
        // CardDate: $("#txt_cardDate").val()
        CardPlace: $("#txt_cardPlace").val("")
        // IsVerified: $('#IsVerified').prop('checked') ? 1 : 0
    }
    var SumitFormSave = function () {
        $('#btn_save').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    fullname: {
                        required: true,
                    },
                    phone: {
                        required: true,
                    },
                    birthDay: {
                        required: true,
                    }
                },
                messages: {
                    fullname: {
                        required: "Vui lòng nhập tên Lender",
                    },
                    phone: {
                        required: "Vui lòng nhập sđt",
                    },
                    birthDay: {
                        required: "Vui lòng chọn ngày sinh",
                    }
                }
            });
            if (!form.valid()) {
                return;
            }
            var ctity = $("#sl_city").val();
            var districtID = $("#sl_district").val();
            var wardID = $("#sl_ward").val();
            if (ctity == null || ctity == "") {
                ctity = 0;
            }
            if (districtID == null || districtID == "") {
                districtID = 0;
            }
            if (wardID == null || wardID == "" || wardID == -111) {
                wardID = 0;
            }
            var request = {
                UserID: parseInt($("#hdd_User").val()),
                FullName: $("#txt_fullname").val(),
                DateOfBirth: $("#txt_birthDay").val(),
                Phone: $("#txt_phone").val(),
                Email: $("#txt_email").val(),
                Gender: parseInt($("input[name='Gender']:checked").val()),
                PermanentAddress: $("#txt_AddressOfResidence").val(),
                TemporaryAddress: $("#txt_address").val(),
                CityID: parseInt(ctity),
                DistrictID: parseInt(districtID),
                WardID: parseInt(wardID),
                CardNumber: $("#txt_numberCard").val(),
                CardDate: $("#txt_cardDate").val(),
                CardPlace: $("#txt_cardPlace").val(),
                IsVerified: $('#IsVerified').prop('checked') ? 1 : 0,
                CreateBy: USER_ID
            }
            console.log(request);
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            if ($("#hdd_User").val() == 0) {
                baseCommon.AjaxDone("POST", baseCommon.Endpoint.CreateLenderUser, JSON.stringify(request), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        $('#btnGetData').trigger('click');
                        $('#modal_create_lender').modal('hide');
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                });
            } else {
                baseCommon.AjaxDone("POST", baseCommon.Endpoint.UpdateLenderUser, JSON.stringify(request), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        $('#btnGetData').trigger('click');
                        $('#modal_create_lender').modal('hide');
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                });
            }
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
        });
    };

    var FastAuthentication = function (userLenderID, lenderName) {
        swal.fire({
            html: "Bạn có chắc chắn muốn xác thực nhà đầu tư <b>" + lenderName + "<b>?",
            type: "warning",
            showCancelButton: !0,
            cancelButtonText: "Hủy",
            confirmButtonText: "Đồng ý"
        }).then(function (result) {
            if (result.value) {
                var request = {
                    "UserID": parseInt(userLenderID),
                    "IsVerified": 1,
                    "CreateBy": USER_ID,
                };
                baseCommon.AjaxDone("POST", baseCommon.Endpoint.VerifyUserLender, JSON.stringify(request), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        GridRefresh()
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                }, "Xác thực NĐT không thành công vui lòng thử lại!");
            }
        });
    };

    var UpdateStatusUserLender = function (userLenderID, lenderName, status) {
        var mes = "Bạn có chắc chẵn muốn chuyển trạng thái nhà đầu tư <b>" + lenderName + "</b> thành hoạt động ?";
        var newStatus = 1;
        if (status == 1) {
            mes = "Bạn có chắc chẵn muốn chuyển trạng thái nhà đầu tư <b>" + lenderName + "</b> thành khóa ?";
            newStatus = 0;
        }
        swal.fire({
            html: mes,
            type: "warning",
            showCancelButton: !0,
            cancelButtonText: "Hủy",
            confirmButtonText: "Đồng ý"
        }).then(function (result) {
            if (result.value) {
                var request = {
                    "UserID": parseInt(userLenderID),
                    "Status": newStatus,
                    "CreateBy": USER_ID,
                };
                baseCommon.AjaxDone("POST", baseCommon.Endpoint.ChangeStatusLenderUser, JSON.stringify(request), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        GridRefresh()
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                }, "Chuyển trạng thái NĐT không thành công vui lòng thử lại!");
            }
        });
    };

    var ModalCreateLenderDigitalSignature = function (jsonData = '') {
        var data = JSON.parse(jsonData);
        $("#hdd_signature_User").val(data.UserID);
        $("#txt_Signature_Account").val("");
        $("#txt_Signature_PassCode").val("");

        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetContractByUserLender + data.UserID, "", function (respone) {
            if (respone.Result == 1) {
                baseCommon.DataSource("#sl_Signature_Loan", "Chọn HĐ", "LenderID", "ContractCode", 0, respone.Data);
            }
        });
    }
    var CreateLenderDigitalSignature = function () {
        $('#btn_save_digitalSignature').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    txt_Signature_Account: {
                        required: true,
                    },
                    txt_Signature_PassCode: {
                        required: true,
                    }
                },
                messages: {
                    Account: {
                        txt_Signature_Account: "Vui lòng nhập tài khoản (AgreementUUID)",
                    },
                    txt_Signature_PassCode: {
                        required: "Vui lòng nhập PassCode",
                    }
                }
            });
            if (!form.valid()) {
                return;
            }
            var signature = $("#txt_Signature_Account").val();
            var password = $("#txt_Signature_PassCode").val();
            var lenderID = $("#sl_Signature_Loan").val();
            var userID = $("#hdd_signature_User").val();

            var request = {
                LenderID: parseInt(lenderID),
                Signature: signature,
                Password: password,
                UserID: parseInt(userID)
            }
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            baseCommon.AjaxDone("POST", baseCommon.Endpoint.CreateLenderDigitalSignature, JSON.stringify(request), function (respone) {
                if (respone.Result == 1) {
                    baseCommon.ShowSuccess(respone.Message);
                    $('#btnGetData').trigger('click');
                    $('#modal_create_digital_signarture').modal('hide');
                } else {
                    baseCommon.ShowErrorNotLoad(respone.Message);
                }
            });
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
        });
    };

   
    var ResetPassword = function (UserID, FullName, Phone) {
        swal.fire({
            html: `Bạn muốn reset mật khẩu nhà đầu tư :<b> ${FullName} </b> - số điện thoại : <b>${Phone} ?</b> `,
            type: "warning",
            showCancelButton: !0,
            cancelButtonText: "Hủy",
            confirmButtonText: "Đồng ý"
        }).then(function (result) {
            if (result.value) {
                var model = {
                    "UserID": UserID,
                };
                baseCommon.AjaxDone("POST", baseCommon.Endpoint.ResetPassword, JSON.stringify(model), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        GridRefresh()
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                }, "Hủy Hold tiền không thành công vui lòng thử lại!");
            }
        });
    }
    var GetDataSelectProvince = function () {
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetListProvince, "", function (respone) {
            if (respone.Result == 1) {
                baseCommon.DataSource("#sl_city", "Chọn tỉnh/thành", "Value", "Text", 0, respone.Data);
                baseCommon.DataSource("#sl_edit_city", "Chọn tỉnh/thành", "Value", "Text", 0, respone.Data);
            }
        });
    }
    var GetDataSelectDistrictByCityID = function (cityID) {
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetDistrictByCityID + cityID, "", function (respone) {
            if (respone.Result == 1) {
                baseCommon.DataSource("#sl_district", "Chọn quận/huyện", "Value", "Text", 0, respone.Data);
                baseCommon.DataSource("#sl_edit_district", "Chọn quận/huyện", "Value", "Text", 0, respone.Data);
            }
        });
    }
    var GetDataSelectWardByDistrictID = function (districtID) {
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetWardByDistrictID + districtID, "", function (respone) {
            if (respone.Result == 1) {
                baseCommon.DataSource("#sl_ward", "Chọn phường/xã", "Value", "Text", 0, respone.Data);
                baseCommon.DataSource("#sl_edit_ward", "Chọn phường/xã", "Value", "Text", 0, respone.Data);
            }
        });
    }
    $('#sl_city').on('change', function () {
        GetDataSelectDistrictByCityID($(this).val());
        $("#sl_ward").val(0).change();
    });
    $('#sl_district').on('change', function () {
        GetDataSelectWardByDistrictID($(this).val());
    });
    $('#sl_edit_city').on('change', function () {
        GetDataSelectDistrictByCityID($(this).val());
        $("#sl_edit_ward").val(0).change();
    });
    $('#sl_edit_district').on('change', function () {
        GetDataSelectWardByDistrictID($(this).val());
    });
    $('#sl_Signature_Loan').on('change', function () {
        var lenderID = parseInt($(this).val())
        if (lenderID != 0) {
            baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetLenderSignature + lenderID, "", function (respone) {
                if (respone.Result == 1) {
                    console.log(respone)
                    $("#txt_Signature_Account").val(respone.Data.Signature);
                    $("#txt_Signature_PassCode").val(respone.Data.Password);
                } else {
                    $("#txt_Signature_Account").val("");
                    $("#txt_Signature_PassCode").val("");
                }
            });
        }
    });

    var Init = function () {

        $('#sl_edit_city').select2({
            placeholder: "Chọn tỉnh thành",
            tags: true
        });
        $('#sl_edit_district').select2({
            placeholder: "Chọn quận /huyện",
            tags: true
        });
        $('#sl_edit_ward').select2({
            placeholder: "Chọn phường /xã ",
            tags: true
        });

        GetDataSelectProvince();
        GridRefresh();
        $('#sl_search_status').on('change', function () {
            GridRefresh();
        });
        $('#sl_search_isverify').on('change', function () {
            GridRefresh();
        });
        $('#btnGetData').on('click', function () {
            GridRefresh();
        });
        $("#txt_search").on('keypress', function (e) {
            if (e.which === 13) {
                GridRefresh();
            }
        });


    };
    return {
        Init: Init,
        RowClick: RowClick,
        Option: Option,
        GetDataSelectProvince: GetDataSelectProvince,
        SumitFormSave: SumitFormSave,
        ShowCreateModal: ShowCreateModal,
        loadInfoLender: loadInfoLender,
        FastAuthentication: FastAuthentication,
        CreateLenderDigitalSignature: CreateLenderDigitalSignature,
        ModalCreateLenderDigitalSignature: ModalCreateLenderDigitalSignature,
        UpdateStatusUserLender: UpdateStatusUserLender,
        ResetPassword: ResetPassword
    };
}();
