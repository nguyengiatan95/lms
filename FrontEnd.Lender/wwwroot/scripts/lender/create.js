﻿var lenderAddOrEdit = function () {
    var sumitFormSave = function () {
        $('#btn_save').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    FullName: {
                        required: true,
                    },
                    Represent: {
                        required: true,
                    },
                    Phone: {
                        required: true,
                    },
                    ContractDate: {
                        required: true,
                    },
                    ContractCode: {
                        required: true,
                    },
                    PercentLender: {
                        number: true,
                    },
                    PercentAffLender: {
                        number: true,
                    },
                },
                messages: {
                    FullName: {
                        required: "Vui lòng nhập mã Lender",
                    },
                    Represent: {
                        required: "Vui lòng nhập tên Lender",
                    },
                    Phone: {
                        required: "Vui lòng nhập sđt",
                    },
                    ContractDate: {
                        required: "Vui lòng chọn ngày kí hđ",
                    },
                    ContractCode: {
                        required: "Vui lòng nhập số HĐ hợp tác",
                    },
                    PercentLender: {
                        number: "Vui lòng nhập số",
                    },
                    PercentAffLender: {
                        number: "Vui lòng nhập số",
                    },
                }
            });
            if (!form.valid()) {
                return;
            }
            var Status = $('#kt_switch_2').prop('checked') ? 1 : 0;
            var Isverified = $('#kt_switch_3').prop('checked') ? 1 : 0;
            var LenderID = $('#hdd_LenderID').val();
            var FullName = $('#txt_create_FullName').val();
            var NumberCard = $('#txt_create_NumberCard').val();
            var StrBirthDay = $('#txt_create_StrBirthDay').val();
            var Phone = $('#txt_create_Phone').val();
            var TaxCode = $('#txt_create_TaxCode').val();
            var Address = $('#txt_create_Address').val();
            var Gender = $("input[name='Gender']:checked").val();
            var Represent = $('#txt_create_Represent').val();
            var AddressOfResidence = $('#txt_create_AddressOfResidence').val();
            var Email = $('#txt_create_Email').val();
            var RepresentPhone = $('#txt_create_RepresentPhone').val();
            var CardDate = $('#txt_create_CardDate').val();

            var CardPlace = $('#txt_create_CardPlace').val();
            var StrContractDate = $('#txt_create_ContractDate').val();
            var ContractCode = $('#txt_create_ContractCode').val();
            var AccountBanking = $('#txt_create_AccountBanking').val();
            var BankID = $('#sl_create_bankID').val();

            var BranchOfBank = $('#txt_create_BranchOfBank').val();
            var LenderCareUserID = $('#sl_create_LenderCare').val();
            var LenderTakeCareUserID = $('#sl_create_LenderTakeCare').val();
            var PercentLender = $('#txt_create_PercentLender').val();
            var AffID = $('#sl_create_InvitePerson').val();
            var PercentAffLender = $('#txt_create_PercentAffLender').val();
            var InvitePhone = $('#txt_create_InvitePhone').val();

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            var request = {
                "LenderID": parseInt(LenderID),
                "Status": parseInt(Status),
                "IsVerified": parseInt(Isverified),
                "FullName": FullName,
                "NumberCard": NumberCard,
                "StrBirthDay": StrBirthDay,
                "Phone": Phone,
                "TaxCode": TaxCode,
                "Address": Address,
                "Gender": parseInt(Gender),
                "Represent": Represent,
                "AddressOfResidence": AddressOfResidence,
                "Email": Email,
                "RepresentPhone": RepresentPhone,
                "StrCardDate": CardDate,
                "CardPlace": CardPlace,
                "StrContractDate": StrContractDate,
                "ContractCode": ContractCode,
                "AccountBanking": AccountBanking,
                "BankID": parseInt(BankID),
                "BranchOfBank": BranchOfBank,
                "LenderCareUserID": parseInt(LenderCareUserID),
                "RateLender": parseFloat(PercentLender),
                "LenderTakeCareUserID": parseInt(LenderTakeCareUserID),
                "AffID": parseInt(AffID),
                "RateAff": parseFloat(PercentAffLender),
                "InvitePhone": InvitePhone
            };
            if (LenderID == 0) {
                baseCommon.AjaxDone("POST",baseCommon.Endpoint.CreateInfoLender, JSON.stringify(request), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        $('#btnGetData').trigger('click');
                        $('#modal_add_edit_lender').modal('hide');
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                });
            } else {
                baseCommon.AjaxDone("POST",baseCommon.Endpoint.UpdateInfoLender, JSON.stringify(request), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        $('#btnGetData').trigger('click');
                        $('#modal_add_edit_lender').modal('hide');
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                } );
            }
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
        });
    };
    var loadInfoLender = function (jsonData = '') {
        if (jsonData == '') {
            $('#headerId').text('Thêm mới nhà đầu tư');
            $('#btn_save').html('<i class="fa fa-save"></i>Thêm mới');
            $('#hdd_LenderID').val(0);
            $('#txt_create_FullName').val('');
            $('#txt_create_NumberCard').val('');
            $('#txt_create_Phone').val('');
            $('#txt_create_TaxCode').val('');
            $('#txt_create_Address').val('');
            $('#txt_create_StrBirthDay').val('');
            $("input[name=Gender][value=" + 0 + "]").attr('checked', 'checked');
            $('#kt_switch_2').removeAttr('checked');
            $('#kt_switch_3').removeAttr('checked');


            $('#txt_create_Represent').val('');
            $('#txt_create_AddressOfResidence').val('');
            $('#txt_create_Email').val('');
            $('#txt_create_RepresentPhone').val('');
            $('#txt_create_CardDate').val('');
            $('#txt_create_CardPlace').val('');
            $('#txt_create_ContractDate').val('');
            $('#txt_create_ContractCode').val('');
            $('#txt_create_AccountBanking').val('');
            $('#sl_create_bankID').val(0).change();
            $('#txt_create_BranchOfBank').val('');
            $('#sl_create_LenderCare').val(0).change();
            $('#sl_create_LenderTakeCare').val(0).change();
            $('#txt_create_PercentLender').val('');

            $('#sl_create_InvitePerson').val(0).change();
            $('#txt_create_PercentAffLender').val('');
             $('#txt_create_InvitePhone').val('');

        } else {
            var data = JSON.parse(jsonData);
            $('#hdd_LenderID').val(data.LenderID);
            $('#txt_create_FullName').val(data.FullName);
            $('#txt_create_NumberCard').val(data.NumberCard);
            $('#txt_create_Phone').val(data.Phone);
            $('#txt_create_TaxCode').val(data.TaxCode);
            $('#txt_create_Address').val(data.Address);
            $('#txt_create_StrBirthDay').val(data.BirthDay == null ? "" : formattedDate(data.BirthDay));
            if (data.BirthDay != null) {
                $('#txt_create_StrBirthDay').datepicker('setDate', $('#txt_create_StrBirthDay').val());
            }
            data.Gender = data.Gender == null ? 0 : data.Gender;
            $("input[name=Gender][value=" + data.Gender + "]").attr('checked', 'checked');
            if (data.Status > 0) {
                $('#kt_switch_2').prop('checked', true).trigger('change');
            } else {
                $('#kt_switch_2').prop('checked', false).trigger('change');
            }
            if (data.IsVerified > 0) {
                $('#kt_switch_3').prop('checked', true).trigger('change');

            } else {
                $('#kt_switch_3').prop('checked', false).trigger('change');
            }
            $('#txt_create_Represent').val(data.Represent);
            $('#txt_create_AddressOfResidence').val(data.AddressOfResidence);
            $('#txt_create_Email').val(data.Email);
            $('#txt_create_RepresentPhone').val(data.RepresentPhone);

            $('#txt_create_CardDate').val(data.CardDate == null ? "" : formattedDate(data.CardDate));
            if (data.CardDate != null) {
                $('#txt_create_CardDate').datepicker('setDate', $('#txt_create_CardDate').val());
            }

            $('#txt_create_CardPlace').val(data.CardPlace);

            $('#txt_create_ContractDate').val(data.ContractDate == null ? "" : formattedDate(data.ContractDate));
            if (data.ContractDate != null) {
                $('#txt_create_ContractDate').datepicker('setDate', $('#txt_create_ContractDate').val());
            }
            $('#txt_create_ContractCode').val(data.ContractCode);
            $('#txt_create_AccountBanking').val(data.AccountBanking);
            $('#sl_create_bankID').val(data.BankID).change();
            $('#txt_create_BranchOfBank').val(data.BranchOfBank);
            $('#sl_create_LenderCare').val(data.LenderCareUserID).change();
            $('#sl_create_LenderTakeCare').val(data.LenderTakeCareUserID).change();
            $('#txt_create_PercentLender').val(data.RateLender);
            $('#sl_create_InvitePerson').val(data.AffID).change();
            $('#txt_create_PercentAffLender').val(data.RateAff);
            $('#txt_create_InvitePhone').val(data.InvitePhone);

            $('#headerId').text('Cập nhật nhà đầu tư');
            $('#btn_save').html('<i class="fa fa-save"></i>Cập nhật');
        }
    };
    return {
        sumitFormSave: sumitFormSave,
        loadInfoLender: loadInfoLender
    };
}();