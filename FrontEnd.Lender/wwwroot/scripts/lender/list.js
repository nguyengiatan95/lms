﻿
var lender = function () {
    var Option = {
        LoadInfoLender: 1 
    };
    var rowClicked = Option.LoadInfoLender;
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) { 
                var GeneralSearch = $('#txt_search').val();
                var Status = $('#sl_search_status').val();
                var IsVerified = $('#sl_search_isverify').val();
                var data = {
                    GeneralSearch: GeneralSearch,
                    IsHub: 0,
                    IsVerified: parseInt(IsVerified),
                    Status: parseInt(Status),
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetLenderList, JSON.stringify(data), function (res) {
                    if (res.Result == 1) {
                        options.success(res);
                    } else { 
                        options.success(null);
                    }
                }, "Thao tác thất bại, vui lòng thử lại sau!");
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total =0;
                if (response != null && response.Total != null && response.Total > 0) {
                    total = response.Total;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    function Grid_change(e) {
        var selectedRows = this.select();
        var selectedDataItems = [];
        for (var i = 0; i < selectedRows.length; i++) {
            var dataItem = this.dataItem(selectedRows[i]);
            selectedDataItems.push(dataItem);
        }
        // selectedDataItems contains all selected data items
        gridSelectedRowData = selectedDataItems[0];
        setTimeout(function () {
            var jsonData = JSON.stringify(gridSelectedRowData);
            if (rowClicked == Option.LoadInfoLender) { 
                lenderAddOrEdit.loadInfoLender(jsonData); 
                $('#modal_add_edit_lender').modal('toggle');
            } 
            rowClicked = Option.LoadInfoLender;
        }, 5);
    }
    var InitGrid = function (objQuery) {
        $("#grid").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            change: Grid_change,
            selectable: "row",
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 2) {
                        this.autoFitColumn(i);
                    }
                }
            }, 
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'FullName',
                    title: 'Mã Lender',
                    textAlign: 'left',
                    template: function (row) {
                        // callback function support for column rendering
                        return '<a class="kt-font-primary  kt-link"  title="Sửa" data-toggle="modal" data-target="#modal_add_edit_lender" onclick="lenderAddOrEdit.loadInfoLender(\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">' + row.FullName + '</a>';
                    },
                },
                {
                    field: 'NumberCard',
                    title: 'CMND',
                    textAlign: 'left',
                },
                {
                    field: 'Phone',
                    title: 'SĐT',
                    textAlign: 'left',
                },
                {
                    field: 'BirthDay',
                    title: 'Ngày sinh',
                    textAlign: 'left',
                    template: function (row) {
                        return row.BirthDay == null ? "" : formattedDate(row.BirthDay);
                    },
                },
                {
                    field: 'TaxCode',
                    title: 'Mã số thuế',
                    textAlign: 'left',
                },
                {
                    field: 'CreateDate',
                    title: 'Ngày tạo',
                    textAlign: 'left',
                    template: function (row) {
                        return row.CreateDate == null ? "" : formattedDateHourMinutes(row.CreateDate);
                    },
                },
                {
                    field: 'Status',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    template: function (row) {
                        if (row.Status == 1) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Hoạt Động</span>';
                        } else if (row.Status == 0) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Khóa</span>';
                        }
                        else if (row.Status == -11) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Xóa</span>';
                        }
                    },
                },
                {
                    field: 'IsVerified',
                    title: 'Xác minh',
                    textAlign: 'center',
                    template: function (row) {
                        if (row.IsVerified == 1) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Đã xác minh</span>';
                        } else if (row.IsVerified == 0) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Chưa xác minh</span>';
                        }
                    },
                },
                { 
                    title: 'Chức năng',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        return '\
                          <a class="btn btn-primary btn-icon btn-sm" title="Sửa" data-toggle="modal" data-target="#modal_add_edit_lender" onclick="lender.RowClick('+ lender.Option.LoadInfoLender+',\''+ JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                            <i class="fa fa-edit"></i>\
                          </a>';
                    }
                }
            ]
        });
    };
    var RowClick = function (rowClick, row) {
        rowClicked = rowClick;
        if (rowClicked == Option.LoadInfoLender) {
            lenderAddOrEdit.loadInfoLender(row);
        }  
    };
    var GridRefresh = function () {
        var grid = $("#grid").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };
    var Init = function () { 
        GridRefresh();
        $('#sl_search_status').on('change', function () {
            GridRefresh();
        });
        $('#sl_search_isverify').on('change', function () {
            GridRefresh();
        });
        $('#btnGetData').on('click', function () {
            GridRefresh();
        }); 
        $("#txt_search").on('keypress', function (e) {
            if (e.which === 13) {
                GridRefresh();
            }
        }); 
    };
    return {
        Init: Init,
        RowClick: RowClick,
        Option: Option
    };
}();