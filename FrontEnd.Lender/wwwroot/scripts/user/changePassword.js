﻿var user = new function () {
    var ConfigCallCenter = {
        Cisco: 2,
        Caresoft: 1
    }
    var showAndHide = function (number) {
        if (number == 1) {
            $('#userInfo').hide();
            $('#ipPhone_user_info').hide();
            $('#changePass').show();
            $('#info_activeClass').removeClass('kt-widget__item kt-widget__item--active');
            $('#info_activeClass').addClass('kt-widget__item');
            $('#ipPhone_activeClass').removeClass('kt-widget__item kt-widget__item--active');
            $('#ipPhone_activeClass').addClass('kt-widget__item');
            $('#changePass_activeClass').addClass('kt-widget__item kt-widget__item--active');
        } else if (number == 0) {
            $('#userInfo').show();
            $('#ipPhone_user_info').hide();
            $('#changePass').hide();
            $('#changePass_activeClass').removeClass('kt-widget__item kt-widget__item--active');
            $('#changePass_activeClass').addClass('kt-widget__item');
            $('#ipPhone_activeClass').removeClass('kt-widget__item kt-widget__item--active');
            $('#ipPhone_activeClass').addClass('kt-widget__item');
            $('#info_activeClass').addClass('kt-widget__item kt-widget__item--active');
        } else if (number == 2) {
            loadDataPhone();
            $('#userInfo').hide();
            $('#ipPhone_user_info').show();
            $('#changePass').hide();
            $('#changePass_activeClass').removeClass('kt-widget__item kt-widget__item--active');
            $('#changePass_activeClass').addClass('kt-widget__item');
            $('#info_activeClass').removeClass('kt-widget__item kt-widget__item--active');
            $('#info_activeClass').addClass('kt-widget__item');
            $('#ipPhone_activeClass').addClass('kt-widget__item kt-widget__item--active');
        }
    }
    var Init = function () { onChangeDataPhone(); $("#sl_type_phone").val(1).change(); loadDataPhone(); }
    var loadDataPhone = function () {
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetServiceCallInfoByUserID, "", function (response) {
            if (response.Result == 1) {
                var data = response.Data;
                $("#sl_type_phone").val(data.TypeCall).change();
                $('#txt_create_info_UserName').val(data.CiscoUserName);
                $('#txt_create_info_Password').val(data.CiscoPassword);
                $('#txt_create_info_Extension').val(data.CiscoExtension);
                $('#txt_create_info_IpPhone').val(data.IpPhone);
            } else {
                baseCommon.ShowErrorNotLoad(response.Message);
            }
        });
    }
    var onChangeDataPhone = function () {
        $("#sl_type_phone").on('change', function (e) {
            var typePhone = $("#sl_type_phone").val();
            if (typePhone == 1) {
                $('.type_caresoft').show();
                $('.type_cisco').hide();
            } else {
                $('.type_caresoft').hide();
                $('.type_cisco').show();
            }
        });
    }
    var saveConfigPhone = function () {
        var typePhone = $("#sl_type_phone").val();
        var IpPhone = $('#txt_create_info_IpPhone').val();
        var UserName = $('#txt_create_info_UserName').val();
        var Password = $('#txt_create_info_Password').val();
        var Extension = $('#txt_create_info_Extension').val();

        if (typePhone == ConfigCallCenter.Cisco) {
            if (UserName == "" || UserName == null) {
                baseCommon.ShowErrorNotLoad("Vui lòng nhập Tài khoản Cisco");
                return;
            } else if (Password == "" || Password == null) {
                baseCommon.ShowErrorNotLoad("Vui lòng nhập mật khẩu Cisco");
                return;
            }
            else if (Extension == "" || Extension == null) {
                baseCommon.ShowErrorNotLoad("Vui lòng nhập số máy lẻ Cisco");
                return;
            }
        } else {
            if (IpPhone == "" || IpPhone == null) {
                baseCommon.ShowErrorNotLoad("Vui lòng nhập Vui lòng nhập số máy lẻ CareSoft");
                return;
            }
        }
        var dataRequest = {
            "TypeCall": parseInt(typePhone),
            "IpPhone": IpPhone,
            "CiscoUserName": UserName,
            "CiscoPassword": Password,
            "CiscoExtension": Extension,
        };
        var btn = $('#btn_save_typePhone');
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.SettingUserPhone, JSON.stringify(dataRequest), function (respone) {
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                loadDataPhone();
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            }
        });
    }
    var changePassword = function () {
        var PasswordOld = $("#txt_changePass_oldPass").val();
        var PasswordNew = $("#txt_changePass_newPass").val();
        if (PasswordOld == '') {
            baseCommon.ShowErrorNotLoad("Vui lòng điền mật khẩu cũ");
            return;
        }

        if (PasswordNew == '') {
            baseCommon.ShowErrorNotLoad("Vui lòng điền mật khẩu mới");
            return;
        }

        var dataRequest = {
            "UserID": USER_ID,
            "PasswordNew": PasswordNew,
            "PasswordOld": PasswordOld,
        };
        var btn = $('#btn_save_change_password');
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.ChangePassword, JSON.stringify(dataRequest), function (respone) {
            if (respone.Result == 1) {
                $("#txt_changePass_oldPass").val('');
                $("#txt_changePass_newPass").val('');
                baseCommon.ShowSuccess(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            }
        });
    }
    return {
        Init: Init,
        showAndHide: showAndHide,
        saveConfigPhone: saveConfigPhone,
        changePassword: changePassword
    }
}();
