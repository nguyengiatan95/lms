﻿var datatable;
var _loanId;
//var record = 0;
var recordGrid = 0;
var recordHistoryCommentTable = 0;
var Indemnify = new function () {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var txt_search = $('#txt_search').val();
                if (typeof txt_search != "undefined" && txt_search != null && txt_search == "") {
                    txt_search = txt_search.trim();
                }
                var txt_search_customerName = $('#txt_search_customerName').val();
                if (typeof txt_search_customerName != "undefined" && txt_search_customerName != null && txt_search_customerName == "") {
                    txt_search_customerName = txt_search_customerName.trim();
                }
                var fromDate = $('#fromDate').val();
                if (typeof fromDate != "undefined" && fromDate != null && fromDate == "") {
                    fromDate = fromDate.trim();
                }
                var toDate = $('#toDate').val();
                if (typeof toDate != "undefined" && toDate != null && toDate == "") {
                    toDate = toDate.trim();
                }
                var lenderID = $('#sl_search_lenderID').val();
                if (typeof lenderID == undefined || lenderID == null || lenderID == "") {
                    lenderID = -111;
                }
                var sl_search_status = $('#sl_search_status').val();
                if (typeof sl_search_status != "undefined" && sl_search_status == "") {
                    sl_search_status = -111;
                }
                var TypeInsurance = $('#sl_search_insurance').val();
                if (typeof TypeInsurance != "undefined" && TypeInsurance == "") {
                    TypeInsurance = -111;
                }
                var data = {
                    CodeID: txt_search,
                    StatusSendInsurance: parseInt(sl_search_status),
                    LenderID: lenderID,
                    StrFromDate: fromDate,
                    StrToDate: toDate,
                    CustomerName: txt_search_customerName,
                    TypeInsurance: parseInt(TypeInsurance),
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetReportInsurance, JSON.stringify(data), function (res) {
                    if (res.Result == 1) {
                        options.success(res);
                    }
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function (objQuery) {
        $("#dv_result").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                input: true,
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 3) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #",
                    attributes: { style: "text-align:center;" },
                },
                {
                    title: 'Tên nhà đầu tư',
                    template: function (row) {
                        var html = "";
                        if (row == 0 || row == null) {
                            return '';
                        }
                        html = `<span>${row.FullName}</span><br/>`
                        return html;
                    }
                },
                {
                    field: 'CustomerName',
                    title: 'Tên khách hàng',
                    template: function (row) {
                        var html = "";
                        if (row == 0 || row == null) {
                            return '';
                        }
                        html = `<span>${row.CustomerName}</span><br/>
                                    <span class="item-desciption">${row.ContractCode}</span>`;
                        return html;
                    }
                },
                {
                    field: 'InsuranceCode',
                    title: 'Mã BH',
                    width: 200,
                    template: function (row) {
                        var html = "";
                        if (row == 0 || row == null) {
                            return html;
                        }
                        html = `<span title="${row.InsuranceCode}">${row.InsuranceCode}</span><br/>`
                        return html;
                    }
                },
                {
                    field: 'TotalMoneyDisbursement',
                    title: 'Số tiền giải ngân',
                    attributes: { style: "text-align:right;" },
                    headerAttributes: { style: "text-align: right" },
                    template: function (row) {
                        var html = "";
                        if (row == 0 || row == null || row.TotalMoneyDisbursement == null || row.TotalMoneyDisbursement == null) {
                            return html;
                        }
                        html = `<span>${html_money(row.TotalMoneyDisbursement)}</span><br/>
                                    <span class="item-desciption">Tiền gốc còn lại: ${formatCurrency(row.TotalMoneyCurrent)}</span>`;
                        return html;
                    }
                },
                {
                    field: 'FromDate',
                    title: 'Ngày giải ngân',
                    template: function (row) {
                        if (row == 0 || row == null || row.FromDate == null) {
                            return "";
                        }
                        var date = moment(row.FromDate);
                        return date.format("DD/MM/YYYY");
                    }
                },
                {
                    field: 'ToDate',
                    title: 'Ngày kết thúc',
                    template: function (row) {
                        if (row == 0 || row == null || row.ToDate == null) {
                            return "";
                        }
                        var date = moment(row.ToDate);
                        return date.format("DD/MM/YYYY");
                    }
                },
                {
                    field: 'TotalInterest',
                    title: 'Tổng lãi',
                    attributes: { style: "text-align:right;" },
                    template: function (row) {
                        if (row == 0 || row == null || row.TotalInterest == null) {
                            return "";
                        }
                        return html_money(row.TotalInterest);//baseCommon.FormatCurrency(row.loanTotalMoneyCurrent);
                    }
                },
                {
                    field: 'SourceByInsurance',
                    title: 'Nguồn bảo hiểm',
                    textAlign: 'center'
                },
                {
                    field: 'LinkGNCLender',
                    title: 'Link GNC Lender',
                    textAlign: 'center',
                    template: function (row) {
                        if (row == 0 || row == null || row.LinkGNCLender == null || row.LinkGNCLender == "") {
                            return "";
                        }
                        var html = `<a href="${row.LinkGNCLender}" target="_blank" style="color:blue">Click vào đây</a>`;
                        return html;
                    }
                },
                {
                    field: 'StrStatusInsurance',
                    title: 'Trạng thái',
                    headerAttributes: { style: "text-align: center" },
                },
                {
                    title: 'Hành động',
                    template: function (row, index, datatable) {
                        var strAction = '';
                        if (row.StatusInsurance == loan_statusInsurance_NoSend || row.StatusInsurance == loan_statusInsurance_Back) {
                            strAction += '<a class="btn btn-success btn-icon btn-sm" title="Xác nhận gửi bảo hiểm"  data-toggle="modal" data-target="#modal_edit_InsuranceStatus" onclick="Indemnify.moveLoanInsurace(' + row.LoanID + ',' + loan_statusInsurance_Send + ',\'' + row.ContractCode + '\')">\
                                                    <i class="fa fa-check"></i></a> ';
                        } else if (row.StatusInsurance == loan_statusInsurance_Send) {
                            strAction += '<a class="btn btn-danger btn-icon btn-sm" title="Rút lại hồ sơ gửi bảo hiểm" data-toggle="modal" data-target="#modal_edit_InsuranceStatus" onclick="Indemnify.moveLoanInsurace(' + row.LoanID + ',' + loan_statusInsurance_Back + ',\'' + row.ContractCode + '\')">\
                                                    <i class="fa fa-backspace"></i></a> ';
                        }
                        strAction += '<a class="btn btn-success btn-icon btn-sm" title="In file YCBH"  onclick="Indemnify.printGrid(' + row.LoanID + ')">\
                            <i class="fa fa-print" ></i>\</a> ';
                        strAction += '<a class="btn btn-success btn-icon btn-sm" title="In file nhắc nợ"  onclick="Indemnify.printComment(' + row.LoanID + ')">\
                            <i class="fa fa-print" ></i>\</a> ';
                        strAction += '<a class="btn btn-primary btn-icon btn-sm" title="Ghi chú" data-toggle="modal" data-target="#modal_edit_indemnify" onclick="Indemnify.GetDataHistoryComment(' + row.LoanID + ')">\
                            <i class="fa fa-comment-dots" ></i>\ </a>';
                        return strAction;
                    }
                },
            ],
        });
    };
    var GridRefresh = function () {

        //var grid = $("#dv_result").data("kendoGrid").dataSource.read();
        //grid.dataSource.page(1);
        var grid = $("#dv_result").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };
    var TableHistoryComment = function () {
        var recordHistoryCommentTable = 0;
        $("#HistoryCommentTable").kendoGrid({
            dataSource: {
                pageSize: 1000,
            },

            scrollable: true,
            resizable: true,
            pageable: true,
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordHistoryCommentTable #",
                    width: 60
                },
                {
                    field: "CodeID",
                    title: "Mã HĐ",
                    width: 100
                },
                {
                    field: "CustomerName",
                    title: "Khách hàng",
                    width: 100
                },
                {
                    field: "CreateDate",
                    title: "Ngày tạo",
                    width: 100,
                    template: '#= baseCommon.FormatDate(CreateDate,\'DD/MM/YYYY HH:mm\')#',
                },
                {
                    field: "Note",
                    title: "Ghi chú",
                    width: 100,
                    template: function (row) {
                        var html = "";
                        if (row == 0 || row == null || row.Note == "undefined" || row.Note == null) {
                            return html;
                        }
                        html = `<span>${row.Note}</span>`;
                        return html;
                    }
                },
                {
                    field: "UserName",
                    title: "Người thao tác",
                    width: 120
                },
            ],
            dataBinding: function () {
                recordHistoryCommentTable = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
        });
    }
    var GetDataHistoryComment = function (loanID) {
        recordHistoryCommentTable = 0;
        $("#hdd_indemnifyID").val(loanID)
        if (loanID == null || loanID == undefined) {
            baseCommon.ShowErrorNotLoad("LoanID chưa có vui lòng thử lại");
        }
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetHistoryCommentLender + loanID, '', function (respone) {
            if (respone.Result == 1) {
                if (respone.Data != null && respone.Data != null) {
                    var dataSource = new kendo.data.DataSource({
                        data: respone.Data
                    });
                    var grid = $("#HistoryCommentTable").data("kendoGrid");
                    grid.setDataSource(dataSource);
                }
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        });
    }
    var Save_CommentLender = function () {
        var btn = $('#btn_save_comment');
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        var loanId = $("#hdd_indemnifyID").val()

        var Note = CKEDITOR.instances['txtNote'].getData();
        if (Note.length > 500) {
            baseCommon.ShowErrorNotLoad("Ghi chú quá dài .Vui lòng thử lại!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }
        if (Note == null || Note == "") {
            baseCommon.ShowErrorNotLoad("Ghi chú không được bỏ trống!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }
        var modelComment = {
            "LoanID": parseInt(loanId),
            "Note": Note,
            "userID": baseCommon.GetUser().UserID
        };
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.SaveCommentLender, JSON.stringify(modelComment), function (respone) {
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                GetDataHistoryComment(loanId);
                CKEDITOR.instances.txtNote.setData('');
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            }
        });
    }
    var Init = function () {
        InitGrid();
        TableHistoryComment();
        $("#fromDate").on('change', function (e) {
            $('#btnGetData').click();
        });
        $("#toDate").on('change', function (e) {
            $('#btnGetData').click();
        });
        $("#sl_search_lenderID").on('change', function (e) {
            $('#btnGetData').click();
        });
        $("#sl_search_insurance").on('change', function (e) {
            $('#btnGetData').click();
        });
        $("#sl_search_status").on('change', function (e) {
            $('#btnGetData').click();
        });
        $("#txt_search").on('keypress', function (e) {
            $('#btnGetData').click();
        });
        $("#txt_search_customerName").on('keypress', function (e) {
            $('#btnGetData').click();
        });
        $("#btnGetData").on('click', function (e) {
            //LoadData();
        });
        $("#sl_search_lenderID").select2({
            placeholder: "Tất cả",
            allowClear: true,
            ajax: {
                url: baseCommon.Endpoint.GetLenderSearch,
                type: 'get',
                headers: {
                    'Authorization': baseCommon.GetToken()
                },
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        generalSearch: params.term, // search term
                        type: 0
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    if (data != null && typeof data != "undefined" && data.Result == 1) {
                        return {
                            results: repoConvert(data.Data),
                            pagination: {
                                more: (params.page * 30) < data.Total
                            }
                        };
                    }
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 3,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
        submitMoveLoanInsurance();
        uploadFileChangeStatus();
    };

    function formatRepo(repo) {

        if (repo.loading) return repo.text;
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__title'>" + repo.text + "</div>";
        return markup;
    }
    function formatRepoSelection(repo) {

        return repo.text;
    }
    function repoConvert(data) {

        var newObject = [];
        if (data != null && data != "") {
            $.each(data, function (key, value) {
                var newRepo = { id: value.ID, text: value.FullName };
                newObject.push(newRepo);
            });
        }
        return newObject;
    }

    var moveLoanInsurace = function (loanID, statusInsurance, codeID) {
        if (statusInsurance == loan_statusInsurance_Send) {
            $('#header_changeInsuranceStatus').html('Thay đổi trạng thái <b>' + codeID + '</b> gửi bảo hiểm thành <span style="color: red;">Đã gửi sang Bảo Hiểm</span>');
        } else {
            $('#header_changeInsuranceStatus').html('Thay đổi trạng thái <b>' + codeID + '</b> gửi bảo hiểm thành <span style="color: red;">Đã rút hồ sơ Bảo Hiểm</span>');
        }
        $('#txtInsuranceStatus_loanID').val(loanID);
        $('#txtInsuranceStatus_status').val(statusInsurance);
        $('#txtInsuranceStatus_Note').val('');
    };
    var submitMoveLoanInsurance = function () {
        $('#btn_save_InsuranceStatus').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    txtInsuranceStatus_Note: {
                        required: true,
                    },
                },
                messages: {
                    txtInsuranceStatus_Note: {
                        required: "Vui lòng nhập ghi chú",
                    },
                }
            });
            if (!form.valid()) {
                return;
            }
            var statusInsurance = $('#txtInsuranceStatus_status').val();
            var loanID = $('#txtInsuranceStatus_loanID').val();
            var note = $('#txtInsuranceStatus_Note').val();
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            var ListInsurance = [];
            var objSend = {
                "LoanID": parseInt(loanID),
                "StatusSendInsurance": parseInt(statusInsurance),
                "userID": parseInt(userLogin_UserID),
                "Note": note,
            };
            ListInsurance.push(objSend);
            var request = { "ListInsurance": ListInsurance };
            baseCommon.AjaxDone('POST', baseCommon.Endpoint.SendStatusInsurance, JSON.stringify(request), function (respone) {
                if (respone.Result == 1) {
                    baseCommon.ShowSuccess(respone.Message);
                    $('#btn_exit_InsuranceStatus').click();
                    $('#btnGetData').click();
                } else {
                    baseCommon.ShowErrorNotLoad(respone.Message);
                }
            });
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
        });
    };
    var excelData = function () {
        var txt_search = $('#txt_search').val();
        if (typeof txt_search != "undefined" && txt_search != null && txt_search == "") {
            txt_search = txt_search.trim();
        }
        var txt_search_customerName = $('#txt_search_customerName').val();
        if (typeof txt_search_customerName != "undefined" && txt_search_customerName != null && txt_search_customerName == "") {
            txt_search_customerName = txt_search_customerName.trim();
        }
        var fromDate = $('#fromDate').val();
        if (typeof fromDate != "undefined" && fromDate != null && fromDate == "") {
            fromDate = fromDate.trim();
        }
        var toDate = $('#toDate').val();
        if (typeof toDate != "undefined" && toDate != null && toDate == "") {
            toDate = toDate.trim();
        }
        var lenderID = $('#sl_search_lenderID').val();
        if (typeof lenderID == undefined || lenderID == null || lenderID == "") {
            lenderID = -111;
        }
        var sl_search_status = $('#sl_search_status').val();
        if (typeof sl_search_status != "undefined" && sl_search_status == "") {
            sl_search_status = -111;
        }
        var TypeInsurance = $('#sl_search_insurance').val();
        if (typeof TypeInsurance != "undefined" && TypeInsurance == "") {
            TypeInsurance = -111;
        }
        window.location.href = "/Excel/ExcelLenderInsuranceReport?CodeID=" + txt_search + "&StatusSendInsurance=" + sl_search_status + "&LenderID=" + lenderID + "&StrFromDate=" + fromDate + "&StrToDate=" + toDate
            + '&CustomerName=' + txt_search_customerName + '&TypeInsurance=' + TypeInsurance;
    };

    var excelDataTemplate = function () {
        var txt_search = $('#txt_search').val();
        if (typeof txt_search != "undefined" && txt_search != null && txt_search == "") {
            txt_search = txt_search.trim();
        }
        var txt_search_customerName = $('#txt_search_customerName').val();
        if (typeof txt_search_customerName != "undefined" && txt_search_customerName != null && txt_search_customerName == "") {
            txt_search_customerName = txt_search_customerName.trim();
        }
        var fromDate = $('#fromDate').val();
        if (typeof fromDate != "undefined" && fromDate != null && fromDate == "") {
            fromDate = fromDate.trim();
        }
        var toDate = $('#toDate').val();
        if (typeof toDate != "undefined" && toDate != null && toDate == "") {
            toDate = toDate.trim();
        }
        var lenderID = $('#sl_search_lenderID').val();
        if (typeof lenderID == undefined || lenderID == null || lenderID == "") {
            lenderID = -111;
        }
        var sl_search_status = $('#sl_search_status').val();
        if (typeof sl_search_status != "undefined" && sl_search_status == "") {
            sl_search_status = -111;
        }
        var TypeInsurance = $('#sl_search_insurance').val();
        if (typeof TypeInsurance != "undefined" && TypeInsurance == "") {
            TypeInsurance = -111;
        }
        window.location.href = "/Excel/ExcelTemplateChangeStatus?CodeID=" + txt_search + "&StatusSendInsurance=" + sl_search_status + "&LenderID=" + lenderID + "&StrFromDate=" + fromDate + "&StrToDate=" + toDate
            + '&CustomerName=' + txt_search_customerName + '&TypeInsurance=' + TypeInsurance;
    };

    var uploadFileChangeStatus = function () {
        $("#tem_upload").change(function (evt) {
            var selectedFile = evt.target.files[0];
            if (selectedFile == undefined) {
                return;
            }
            KTApp.blockPage();
            var reader = new FileReader();
            var ListInsurance = [];
            reader.onload = function (event) {
                var data = event.target.result;
                var workbook = XLSX.read(data, {
                    type: 'binary'
                });
                workbook.SheetNames.forEach(function (sheetName) {
                    var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                    var json_object = JSON.stringify(XL_row_object);
                    var objData = JSON.parse(json_object);
                    for (var i = 0; i < objData.length; i++) {
                        var column_LoanID = objData[i]["LoanID"];
                        var column_Status = objData[i]["Trạng thái"];
                        var column_Note = objData[i]["Ghi chú"] == undefined ? "" : objData[i]["Ghi chú"];
                        if (parseInt(column_LoanID) > 0 && column_Status != undefined && column_Note != undefined) {
                            var objSend = {
                                "LoanID": parseInt(column_LoanID),
                                "StatusSendInsurance": parseInt(column_Status),
                                "userID": parseInt(userLogin_UserID),
                                "Note": column_Note.toString(),
                            };
                            ListInsurance.push(objSend);
                        }
                    }

                });
                if (ListInsurance.length == 0) {
                    baseCommon.ShowErrorNotLoad('File sai định dạng');
                    return;
                }
                var request = { "ListInsurance": ListInsurance };
                baseCommon.AjaxDone("POST", baseCommon.Endpoint.SendStatusInsurance, JSON.stringify(request), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        $('#btnGetData').click();
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                });
            };
            reader.onerror = function (event) {
                console.error("File could not be read! Code " + event.target.error.code);
            };
            reader.readAsBinaryString(selectedFile);
            KTApp.unblockPage();
        });
    };
    var checkValidFileExcelImport = function () {
        return true;
    }

    var printGrid = function (loanID) {
        var data = {
            "LoanID": loanID
        }
        baseCommon.AjaxDone('POST', baseCommon.Endpoint.GetInsuranceClaimInformation, JSON.stringify(data), function (res) {
            if (res.Result == 1 && res.Data !== null) {
                var doc = window.open('', 'YCBH', 'height=' + $(window).height() + ',width=' + $(window).width());
                $.get("/Files/ycbh.html", function (data) {
                    //data = data.replace('[ten_khach_hang]', moment(res.Data.ExpirationDate).format("DD/MM/YYYY") ?? '');
                    data = replaceAllData(data, '[ten_khach_hang]', res.Data.CustomerFullname ?? '');
                    data = replaceAllData(data, '[ma_hop_dong]', res.Data.ContractCode ?? '');
                    data = replaceAllData(data, '[hop_dong_ndt]', res.Data.ContractCode ?? '');

                    data = replaceAllData(data, '[ngay_giai_ngan]', moment(res.Data.FromDate).format("DD/MM/YYYY") ?? '');
                    data = replaceAllData(data, '[ngay_ket_thuc]', moment(res.Data.ToDate).format("DD/MM/YYYY") ?? '');
                    data = replaceAllData(data, '[goc_ban_dau]', formatCurrency(res.Data.TotalMoneyDisburement) ?? '0');
                    data = replaceAllData(data, '[Frequency]', formatCurrency(res.Data.LoanTime) ?? '0');
                    data = replaceAllData(data, '[goc_con_lai]', formatCurrency(res.Data.TotalMoneyCurrent) ?? '0');
                    data = replaceAllData(data, '[lai_con_lai] ', formatCurrency(res.Data.InterestMoney) ?? '0');
                    data = replaceAllData(data, '[tong_tien_con_lai]', formatCurrency(res.Data.TotalMoneyCurrent + res.Data.InterestMoney) ?? '0');

                    data = replaceAllData(data, '[ten_ndt]', res.Data.InvestorFullname ?? '');
                    data = replaceAllData(data, '[cmnd_ndt]', res.Data.InvestorIDCard ?? '');
                    data = replaceAllData(data, '[gcnbh]', res.Data.InsuranceNumber);
                    data = replaceAllData(data, '[dia_chi_ndt]', res.Data.InvestorAddress ?? '');
                    data = replaceAllData(data, '[sdt_ndt]', res.Data.InvestorPhone ?? '');
                    data = replaceAllData(data, '[email_ndt]', res.Data.InvestorEmail ?? '');


                    data = replaceAllData(data, '[ngay_sinh_khach_hang]', res.Data.CustomerDateOfBirth ?? '');
                    data = replaceAllData(data, '[dia_chi_kh]', res.Data.CustomerAddress ?? '');
                    data = replaceAllData(data, '[sdt_kh]', res.Data.CustomerPhone ?? '');
                    data = replaceAllData(data, '[email_kh]', res.Data.CustomerEmail ?? '');
                    data = replaceAllData(data, '[ma_hd_bao_hiem]', res.Data.InvestorContactNumber ?? '');
                    data = replaceAllData(data, '[cmnd_kh]', res.Data.CustomerIDCard ?? '');
                    var dateNow = new Date();
                    data = replaceAllData(data, '[ngay_thang_nam_hom_nay]', `Hà Nội, ngày ${dateNow.getDate()} tháng ${dateNow.getMonth() + 1} năm ${dateNow.getFullYear()}`);
                    data = replaceAllData(data, '[ngay_hom_nay]', `Hà Nội, ngày ${dateNow.getDate()}/${dateNow.getMonth() + 1}/${dateNow.getFullYear()}`);
                    doc.document.write(data);
                    setTimeout(function () {
                        doc.print();
                        doc.close();
                    }, 500);
                    return true;
                });
            }
            else {
                baseCommon.ShowErrorNotLoad(res.Message);
            }
        });
    }

    var printComment = function (loanID) {
        var data = {
            "LoanID": loanID
        }
        baseCommon.AjaxDone('POST', baseCommon.Endpoint.GetPrintComment, JSON.stringify(data), function (res) {
            if (res.Result == 1 && res.Data !== null) {
                var doc = window.open('', 'lịch sử nhắc nợ', 'height=' + $(window).height() + ',width=' + $(window).width());
                $.get("/Files/lichsunhacno.html", function (data) {
                    //data = data.replace('[ten_khach_hang]', moment(res.Data.ExpirationDate).format("DD/MM/YYYY") ?? '');
                    data = replaceAllData(data, '[ten_khach_hang]', res.Data.CustomerName ?? '');
                    data = replaceAllData(data, '[dia_chi_kh]', res.Data.Address ?? '');
                    data = replaceAllData(data, '[cmnd_kh]', res.Data.NumberCard ?? '');
                    data = replaceAllData(data, '[ngay_sinh_kh]', res.Data.BirthDay ?? '');

                    data = replaceAllData(data, '[dia_chi_hk]', res.Data.AddressHouseHold ?? '');
                    data = replaceAllData(data, '[cong_ty]', res.Data.CompanyName ?? '');

                    data = replaceAllData(data, '[cong_ty]', res.Data.CompanyName ?? '');
                    data = replaceAllData(data, '[dia_chi_cong_ty]', res.Data.CompanyAddress ?? '');
                    data = replaceAllData(data, '[nghe_nghiep]', res.Data.Job ?? '');
                    data = replaceAllData(data, '[dt_kh]', res.Data.CustomerPhone ?? '');
                    data = replaceAllData(data, '[dt_cong_ty]', res.Data.CompanyPhone ?? '');
                    data = replaceAllData(data, '[dt_nguoi_than]', res.Data.FamilyPhone ?? '');

                    data = replaceAllData(data, '[ten_nguoi_than]', res.Data.FamilyName ?? '');
                    data = replaceAllData(data, '[dt_nguoi_than]', res.Data.FamilyPhone ?? '');
                    data = replaceAllData(data, '[ten_quan_he]', res.Data.RelativeName ?? '');

                    data = replaceAllData(data, '[dt_dong_nghiep]', res.Data.ColleaguePhone ?? '');
                    data = replaceAllData(data, '[ten_dong_nghiep]', res.Data.ColleagueName ?? '');
                    data = replaceAllData(data, '[so_ngay_vay]', res.Data.LoanTime ?? '');
                    data = replaceAllData(data, '[ngay_giai_ngan]', moment(res.Data.FromDate).format("DD/MM/YYYY") ?? '');
                    data = replaceAllData(data, '[san_pham]', res.Data.ProductName ?? '');
                    data = replaceAllData(data, '[tien_vay]', formatCurrency(res.Data.TotalMoney) ?? '0');
                    data = replaceAllData(data, '[tien_vay]', formatCurrency(res.Data.TotalMoney) ?? '0');

                    data = replaceAllData(data, '[so_ngay_vay]', moment(res.Data.LoanTime).format("DD/MM/YYYY") ?? '');
                    data = replaceAllData(data, '[goc_ban_dau]', formatCurrency(res.Data.TotalMoneyDisburement) ?? '0');
                    data = replaceAllData(data, '[goc_con_lai]', formatCurrency(res.Data.TotalMoneyCurrent) ?? '0');

                    data = replaceAllData(data, '[ngay_dong_tien]', moment(res.Data.LastDateOfPay).format("DD/MM/YYYY") ?? '');
                    data = replaceAllData(data, '[tien_qua_han]', formatCurrency(res.Data.InterestMoney) ?? '0');
                    data = replaceAllData(data, '[so_ngay_qua_han]', formatCurrency(res.Data.CountDay) ?? '0');
                    data = replaceAllData(data, '[ten_doi_tac]', res.Data.LenderCode ?? '');
                    data = replaceAllData(data, '[dt_doitac]', res.Data.LenderPhone ?? '');
                    if (res.Data.CommentViews != null && res.Data.CommentViews.length > 0) {
                        var html = '';
                        $('')
                        $.each(res.Data.CommentViews, function (key, value) {
                            html += ` <tr>
                                <td align="center">${key + 1}</td>
                                <td align="center" style="padding-left:10px;">${formattedDateHourMinutes(value.CreateDate)} </td>
                                <td align="center" style="padding-left:10px;">${value.FullName}</td>
                                <td align="left" style="padding-left:10px;">
                                   ${value.Comment}
                                </td>
                            </tr>`
                        });
                    }

                    data = replaceAllData(data, '[tr_comment]', html ?? '');
                    doc.document.write(data);
                    setTimeout(function () {
                        doc.print();
                        doc.close();
                    }, 500);
                    return true;
                });
            }
            else {
                baseCommon.ShowErrorNotLoad(res.Message);
            }
        });
    }



    return {
        Init: Init,
        GridRefresh: GridRefresh,
        GetDataHistoryComment: GetDataHistoryComment,
        TableHistoryComment: TableHistoryComment,
        Save_CommentLender: Save_CommentLender,
        moveLoanInsurace: moveLoanInsurace,
        excelData: excelData,
        excelDataTemplate: excelDataTemplate,
        printGrid: printGrid,
        printComment: printComment
    };
}
$(document).ready(function () {
    Indemnify.Init();
});

function replaceAllData(string, search, replace) {
    return string.split(search).join(replace);
}