﻿"use strict";

// Class Definition
var KTLoginGeneral = function () {

    var login = $('#kt_login');

    var showErrorMsg = function (form, type, msg) {
        var alert = $('<div class="alert alert-' + type + ' alert-dismissible" role="alert">\
			<div class="alert-text">'+ msg + '</div>\
			<div class="alert-close">\
                <i class="flaticon2-cross kt-icon-sm" data-dismiss="alert"></i>\
            </div>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        KTUtil.animateClass(alert[0], 'fadeIn animated');
        alert.find('span').html(msg);
    }

    // Private Functions
    var displaySignUpForm = function () {
        login.removeClass('kt-login--forgot');
        login.removeClass('kt-login--signin');

        login.addClass('kt-login--signup');
        KTUtil.animateClass(login.find('.kt-login__signup')[0], 'flipInX animated');
    }

    var displaySignInForm = function () {
        login.removeClass('kt-login--forgot');
        login.removeClass('kt-login--signup');

        login.addClass('kt-login--signin');
        KTUtil.animateClass(login.find('.kt-login__signin')[0], 'flipInX animated');
        //login.find('.kt-login__signin').animateClass('flipInX animated');
    }

    var displayForgotForm = function () {
        login.removeClass('kt-login--signin');
        login.removeClass('kt-login--signup');

        login.addClass('kt-login--forgot');
        //login.find('.kt-login--forgot').animateClass('flipInX animated');
        KTUtil.animateClass(login.find('.kt-login__forgot')[0], 'flipInX animated');

    }

    var handleFormSwitch = function () {
        $('#kt_login_forgot').click(function (e) {
            e.preventDefault();
            displayForgotForm();
        });

        $('#kt_login_forgot_cancel').click(function (e) {
            e.preventDefault();
            displaySignInForm();
        });

        $('#kt_login_signup').click(function (e) {
            e.preventDefault();
            displaySignUpForm();
        });

        $('#kt_login_signup_cancel').click(function (e) {
            e.preventDefault();
            displaySignInForm();
        });
    }

    var handleSignInFormSubmit = function () {
        $('#kt_login_signin_submit').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    username: {
                        required: true,
                    },
                    password: {
                        required: true
                    }
                },
                messages: {
                    username: {
                        required: "Vui lòng nhập tên đăng nhập",
                    },
                    password: {
                        required: "Vui lòng nhập mật khẩu ",
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: '/User/ProcessLogin',
                method: 'POST',
                success: function (response, status, xhr, $form) {
                    if (response.Result == 1) {
                        var username = $('#username').val();
                        var password = $('#password').val();
                        var remember = document.getElementById("remember").checked;
                        if (remember == true) {
                            createCookie("cm_username", username, 30);
                            createCookie("cm_password", password, 30);
                        }
                        else {
                            createCookie("cm_username", username, -1);
                            createCookie("cm_password", password, -1);
                        }
                        window.location = '/Home/Index';
                    } else {
                        // similate 2s delay
                        setTimeout(function () {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            showErrorMsg(form, 'danger', response.Message);
                        }, 1000);
                    }
                }
            });
        });
    }

    var handleSignUpFormSubmit = function () {
        $('#kt_login_signup_submit').click(function (e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');
            var register_username = $('#register_username').val();
            jQuery.validator.addMethod('checkSpecialCharacter', function (value, element) {
                return /^[a-zA-Z0-9-]*$/.test(value);
            }, "Vui lòng nhập tên tài khoản không có khoảng trắng và kí tự đặc biệt");
            form.validate({
                rules: {
                    register_username: {
                        required: true,
                        checkSpecialCharacter: true
                    },
                    register_password: {
                        required: true,
                        minlength: 6,
                        maxlength: 20,
                    },
                    register_fullName: {
                        required: true
                    },
                    register_phone: {
                        required: true,
                        minlength: 10,
                        maxlength: 11,
                    },
                    register_shopName: {
                        required: true
                    },
                },
                messages: {
                    register_username: {
                        required: "Vui lòng nhập tên đăng nhập",
                        checkSpecialCharacter: "Vui lòng nhập tên tài khoản không có khoảng trắng và kí tự đặc biệt"
                    },
                    register_password: {
                        required: "Vui lòng nhập mật khẩu ",
                        minlength: "Vui lòng nhập  mật khẩu phải lớn hơn hoặc bằng 6 kí tự",
                        maxlength: "Vui lòng nhập  mật khẩu nhỏ hơn hoặc bằng 20 kí tự"
                    },
                    register_fullName: {
                        required: "Vui lòng nhập tên của bạn ",
                    },
                    register_phone: {
                        required: "Vui lòng nhập số điện thoại",
                        minlength: "Vui lòng nhập số điện thoại 10 hoặc 11 số",
                        maxlength: "Vui lòng nhập số điện thoại 10 hoặc 11 số",
                    },
                    register_shopName: {
                        required: "Vui lòng nhập tên cửa hàng/công ty của bạn",
                    },
                }
            });
            var register_password = $('#register_password').val();

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: '/User/Register',
                method: "Post",
                success: function (response, status, xhr, $form) {
                    // similate 2s delay
                    if (response.Result == 1) {
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        form.clearForm();
                        form.validate().resetForm();
                        // display signup form
                        showErrorMsg(form, 'success', 'Đăng kí tài khoản thành công');
                        // similate 2s delay
                        setTimeout(function () {
                            $('#username').val(register_username);
                            $('#password').val(register_password);
                            $('#kt_login_signin_submit').trigger('click');
                        }, 500);
                    } else {
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        showErrorMsg(form, 'danger', response.Message);
                    }

                }
            });
        });
    }

    var handleForgotFormSubmit = function () {
        $('#kt_login_forgot_submit').click(function (e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: '',
                success: function (response, status, xhr, $form) {
                    // similate 2s delay
                    setTimeout(function () {
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false); // remove
                        form.clearForm(); // clear form
                        form.validate().resetForm(); // reset validation states

                        // display signup form
                        displaySignInForm();
                        var signInForm = login.find('.kt-login__signin form');
                        signInForm.clearForm();
                        signInForm.validate().resetForm();

                        showMesage(signInForm, 'success', 'Mật khẩu mới đã được gửi về số điện thoại của bạn .');
                    }, 2000);
                }
            });
        });
    }

    var checkRemmember = function () {

        var username = readCookie("cm_username");
        if (username != '') {
            var password = readCookie("cm_password");
            $("#kt_form_login").find("#username").val(username);
            $("#kt_form_login").find("#password").val(password);
            $("#kt_form_login").find("#remember").prop('checked', true);
            $("#kt_form_login").find("#remember").closest("span").addClass("checked");
        }
        else {
            $("#kt_form_login").find("#username").val('');
            $("#kt_form_login").find("#password").val('');
        }
    };
    var createCookie = function (name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        }
        else var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    };

    var readCookie = function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return '';
    };

    // Public Functions
    return {
        // public functions
        init: function () {
            handleFormSwitch();
            handleSignInFormSubmit();
            handleSignUpFormSubmit();
            handleForgotFormSubmit();
            checkRemmember();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function () {
    KTLoginGeneral.init();
});
