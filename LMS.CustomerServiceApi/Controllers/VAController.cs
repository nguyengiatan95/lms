﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace LMS.CustomerServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VAController : ControllerBase
    {
        private readonly IMediator _bus;
        public VAController(IMediator bus)
        {
            _bus = bus;
        }
        [HttpPost]
        [Route("RegisterVa")]
        public async Task<LMS.Common.Constants.ResponseActionResult> RegisterVa()
        {
            var result = await _bus.Send(new Commands.RegisterVaCommand());
            return result;
        }
    }
}