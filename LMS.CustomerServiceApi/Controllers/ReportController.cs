﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace LMS.CustomerServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : Controller
    {
        private readonly IMediator _bus;
        public ReportController(IMediator bus)
        {
            _bus = bus;
        }
        [HttpPost]
        [Route("ReportExtraMoneyCustomer")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ReportExtraMoneyCustomer(Queries.Report.ReportExtraMoneyCustomerQuery request)
        {
            return await _bus.Send(request);
        }
    }
}