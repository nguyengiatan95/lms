﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly IMediator bus;
        public CustomerController(IMediator bus)
        {
            this.bus = bus;
        }

        [HttpPost]
        [Route("CreateInfoBorrower")]
        public async Task<Common.Constants.ResponseActionResult> CreateInfoBorrower(Commands.CreateInfoBorrowerCommand request)
        {
            return await bus.Send(request);
        }
        [HttpPost]
        [Route("GetCustomerInfosByNumberCard")]
        public async Task<Common.Constants.ResponseActionResult> GetCustomerInfosByNumberCard(Queries.GetCustomerInfosByNumberCardQuery request)
        {
            return await bus.Send(request);
        }
        [HttpPost]
        [Route("GetCustomerInfosByIDs")]
        public async Task<Common.Constants.ResponseActionResult> GetCustomerInfosByIDs(Queries.GetCustomerInfosByIDsQuery request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("UpdateMoneyCustomer")]
        public async Task<Common.Constants.ResponseActionResult> UpdateMoneyCustomer(Commands.UpdateMoneyCustomerCommand request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("UpdatePayType")]
        public async Task<Common.Constants.ResponseActionResult> UpdatePayType(Commands.UpdatePayTypeCommand request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("ChangePhone")]
        public async Task<Common.Constants.ResponseActionResult> ChangePhone(Commands.ChangePhoneCommand request)
        {
            return await bus.Send(request);
        }
    }
}
