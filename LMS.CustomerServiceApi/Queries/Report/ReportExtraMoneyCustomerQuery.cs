﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.CustomerService;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.Queries.Report
{
    public class ReportExtraMoneyCustomerQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string DateSearch { get; set; }
        public string QuerySearch { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class ReportExtraMoneyCustomerQueryHandler : IRequestHandler<ReportExtraMoneyCustomerQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        ILogger<ReportExtraMoneyCustomerQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public ReportExtraMoneyCustomerQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> loanTab, ILogger<ReportExtraMoneyCustomerQueryHandler> logger)
        {
            _customerTab = loanTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> Handle(ReportExtraMoneyCustomerQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
                try
                {
                    var resultData = new List<ReportExtraMoneyCustomerItem>();
                    DateTime date = DateTime.Now;
                    if (!string.IsNullOrEmpty(request.QuerySearch))
                    {
                        if (request.QuerySearch.Length == TimaSettingConstant.MaxLengthPhone && Regex.Match(request.QuerySearch, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success)
                            _customerTab.WhereClause(x => x.Phone == request.QuerySearch);
                        else if (Regex.Match(request.QuerySearch, TimaSettingConstant.PatternNumberCard, RegexOptions.IgnoreCase).Success)
                            _customerTab.WhereClause(x => x.NumberCard == request.QuerySearch);
                        else
                            _customerTab.WhereClause(x => x.FullName == request.QuerySearch);
                    }
                    if (!string.IsNullOrEmpty(request.DateSearch))
                    {
                        date = DateTime.ParseExact(request.DateSearch, _common.DateTimeDDMMYYYY, CultureInfo.InvariantCulture);
                    }
                    var lstCustomer = _customerTab.WhereClause(x => x.TotalMoney != 0).Query();
                    int totalCount = lstCustomer.Count();
                    if (totalCount > 0)
                    {
                        lstCustomer = lstCustomer.OrderByDescending(x => x.TotalMoney).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                        foreach (var item in lstCustomer)
                        {
                            var reportExtraMoneyCustomerItem = new ReportExtraMoneyCustomerItem
                            {
                                CustomerID = item.CustomerID,
                                CustomerName = item.FullName,
                                Address = item.Address,
                                TotalMoney = item.TotalMoney,
                                TotalCount = totalCount,
                                Phone = _common.HideNumberPhoneOrCardNumber(item.Phone),
                                NumberCard = _common.HideNumberPhoneOrCardNumber(item.NumberCard)
                            };
                            resultData.Add(reportExtraMoneyCustomerItem);
                        }
                    }
                    response.SetSucces();
                    response.Data = resultData;
                    response.Total = totalCount;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"ReportExtraMoneyCustomerQueryHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                }
                return response;
            });
        }
    }
}
