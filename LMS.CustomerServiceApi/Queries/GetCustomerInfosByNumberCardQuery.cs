﻿using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.Queries
{
    public class GetCustomerInfosByNumberCardQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string NumberCard { get; set; }
        public string FullName { get; set; }
    }

    public class GetCustomerInfosByNumberCardQueryHandler : IRequestHandler<GetCustomerInfosByNumberCardQuery, LMS.Common.Constants.ResponseActionResult>
    {

        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        ILogger<GetCustomerInfosByNumberCardQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetCustomerInfosByNumberCardQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> loanTab, ILogger<GetCustomerInfosByNumberCardQueryHandler> logger)
        {
            _customerTab = loanTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> Handle(GetCustomerInfosByNumberCardQuery request, CancellationToken cancellationToken)
        {
            Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            try
            {
                _customerTab.WhereClause(x => x.NumberCard == request.NumberCard);
                if (!string.IsNullOrEmpty(request.FullName))
                {
                    _customerTab.WhereClause(x => x.FullName == request.FullName);
                }
                var data = (await _customerTab.QueryAsync()).ToList();
                response.SetSucces();
                response.Data = _common.ConvertParentToChild<List<LMS.Entites.Dtos.CustomerService.CustomerInfoModel>, List<Domain.Tables.TblCustomer>>(data);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{System.Reflection.MethodBase.GetCurrentMethod().Name}_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
