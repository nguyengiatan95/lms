﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.Queries
{
    public class GetCustomerInfosByIDsQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public List<long> CustomerIDs { get; set; }
    }
    public class GetCustomerInfosByIDsQueryHandler : IRequestHandler<GetCustomerInfosByIDsQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        ILogger<GetCustomerInfosByIDsQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetCustomerInfosByIDsQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> loanTab, ILogger<GetCustomerInfosByIDsQueryHandler> logger)
        {
            _customerTab = loanTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> Handle(GetCustomerInfosByIDsQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
                try
                {
                    if (request.CustomerIDs != null && request.CustomerIDs.Count > 0)
                    {
                        var lstInfos = _customerTab.WhereClause(x => request.CustomerIDs.Contains(x.CustomerID)).Query().ToList();
                        response.SetSucces();
                        response.Data = _common.ConvertParentToChild<List<LMS.Entites.Dtos.CustomerService.CustomerInfoModel>, List<Domain.Tables.TblCustomer>>(lstInfos);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{System.Reflection.MethodBase.GetCurrentMethod().Name}_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                }
                return response;
            });
        }
    }
}
