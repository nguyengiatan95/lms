﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblLogVa")]
    public class TblLogVa
    {
        [Dapper.Contrib.Extensions.Key]
        public int LogVAID { get; set; }

        public string Pcode { get; set; }

        public DateTime? CreateDate { get; set; }

        public string Request { get; set; }

        public string Response { get; set; }

        public int? LoanID { get; set; }

        public int? CustomerID { get; set; }

        public int? Status { get; set; }

        public string ReferenceId { get; set; }

    }
}
