﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblKafkaLogEvent")]
    public class TblKafkaLogEvent
    {
        [Dapper.Contrib.Extensions.Key]
        public long KafkaLogEventID { get; set; }

        public string TopicName { get; set; }

        public string DataMessage { get; set; }

        public string ServiceName { get; set; }

        public int Status { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }
        public string ErrorReason { get; set; }
    }
}
