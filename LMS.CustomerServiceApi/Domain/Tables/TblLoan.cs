﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblLoan")]
    public class TblLoan
    {
        [Dapper.Contrib.Extensions.Key]
        public long LoanID { get; set; }

        public long? CustomerID { get; set; }

        public string CustomerName { get; set; }

        public long? ProductID { get; set; }

        public long TotalMoneyDisbursement { get; set; }

        public long? TotalMoneyCurrent { get; set; }

        public long? CityID { get; set; }

        public long? DistrictID { get; set; }

        public long? WardID { get; set; }

        public short? SourceMoneyDisbursement { get; set; }

        public int LoanTime { get; set; }

        public short? RateType { get; set; }

        public int? Frequency { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public DateTime? FinishDate { get; set; }

        public string ContactCode { get; set; }

        public long? OwnerShopID { get; set; }

        public long? LenderID { get; set; }

        public long? ConsultantShopID { get; set; }

        public long? RateInterest { get; set; }

        public long? RateConsultant { get; set; }

        public long? RateService { get; set; }

        public DateTime? NextDate { get; set; }

        public DateTime? LastDateOfPay { get; set; }

        public int? Status { get; set; }

        public int? SourcecBuyInsurance { get; set; }

        public string LinkGCNChoVay { get; set; }

        public string LinkGCNVay { get; set; }

        public long? MoneyFeeInsuranceOfCustomer { get; set; }

        public long? MoneyFeeInsuranceOfLender { get; set; }

        public string JsonExtra { get; set; }

        public long? MoneyFineLate { get; set; }

        public long? MoneyFineOriginal { get; set; }

        public long? MoneyFineInterest { get; set; }

        [Dapper.Contrib.Extensions.Key]
        public byte[] Version { get; set; }

        public long? BankCardID { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }

        public long? LoanCreditIDOfPartner { get; set; }

        public string ProductName { get; set; }
    }
}
