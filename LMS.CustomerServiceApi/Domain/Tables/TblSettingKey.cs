﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("tblSettingKey")]
    public class TblSettingKey
    {
        [Dapper.Contrib.Extensions.Key]
        public long SettingKeyID { get; set; }
        public string KeyName { get; set; }
        public string Value { get; set; }
        public int Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public int? Sleep { get; set; }
        public string EndDate { get; set; }
    }
}
