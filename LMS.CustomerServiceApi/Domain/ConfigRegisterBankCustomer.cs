﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.Domain
{
    public class ConfigRegisterBankCustomer
    {
        public long LoanID { get; set; }
        public long CustomerID { get; set; }
    }
}
