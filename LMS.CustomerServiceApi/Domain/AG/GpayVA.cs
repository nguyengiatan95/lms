﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.Domain.AG
{
    [Dapper.Contrib.Extensions.Table("GpayVA")]
    public class GpayVA
    {
        [Dapper.Contrib.Extensions.Key]
        public int GpayVAId { get; set; }

        public int? CustomerId { get; set; }

        public string MapId { get; set; }

        public string MapType { get; set; }

        public string CustomerName { get; set; }

        public string AccountName { get; set; }

        public string AccountNumber { get; set; }

        public int? Status { get; set; }

        public int? Balance { get; set; }

        public DateTime? StartAt { get; set; }

        public DateTime? ExpireAt { get; set; }

        public string AccountType { get; set; }

        public int? LimitAmount { get; set; }

        public string BankCode { get; set; }
    }
}
