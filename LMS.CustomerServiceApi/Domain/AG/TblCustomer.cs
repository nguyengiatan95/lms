﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.Domain.AG
{
    [Dapper.Contrib.Extensions.Table("TblCustomer")]
    public class TblCustomer
    {
        [Dapper.Contrib.Extensions.Key]
        public int CustomerID { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        public string NumberCard { get; set; }

        public short? Status { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int? ShopID { get; set; }

        public string CardDate { get; set; }

        public string Place { get; set; }

        public int? DocumentId { get; set; }

        public string DocumentValue { get; set; }

        public int? CityId { get; set; }

        public int? DistrictId { get; set; }

        public string PhoneRelationShip { get; set; }

        public long? TotalMoney { get; set; }

        public string RegistrationCustomerId { get; set; }

        public string PermanentAddress { get; set; }

        public string AddressHouseHold { get; set; }

        public string ListPhone { get; set; }

        public short? StatusReadImgCMT { get; set; }

        public string Account_no { get; set; }

        public string Account_name { get; set; }

        public string BankCode_Va { get; set; }

        public string BankName_Va { get; set; }

        public string Map_id { get; set; }

        public int? Status_Va { get; set; }

        public DateTime? EndDate { get; set; }

        public short? LabelTHN { get; set; }

        public DateTime? LastDateLabelTHN { get; set; }

        public string UserNamePutLabel { get; set; }

        public long? UserIDPutLabel { get; set; }

        public string LastNoteLabel { get; set; }

        public int? StatusHoldMoney { get; set; }

        public DateTime? ModifyDate { get; set; }
    }
}
