﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.Commands
{
    public class UpdateMoneyCustomerCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long CustomerID { get; set; }
        public long MoneyAdd { get; set; }
        public string Description { get; set; }
        public string TransactionDateTopUp { get; set; }
    }
    public class UpdateMoneyCustomerCommandHandler : IRequestHandler<UpdateMoneyCustomerCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCustomerCash> _logCustomerCashTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblKafkaLogEvent> _kakfaLogEventTab;
        readonly ILogger<UpdateMoneyCustomerCommandHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public UpdateMoneyCustomerCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCustomerCash> logCustomerCashTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblKafkaLogEvent> kakfaLogEventTab,
            ILogger<UpdateMoneyCustomerCommandHandler> logger)
        {
            _customerTab = customerTab;
            _logCustomerCashTab = logCustomerCashTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _kakfaLogEventTab = kakfaLogEventTab;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> Handle(UpdateMoneyCustomerCommand request, CancellationToken cancellationToken)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            try
            {
                DateTime? transactionDateTopUp = null;
                if (!string.IsNullOrEmpty(request.TransactionDateTopUp))
                {
                    transactionDateTopUp = DateTime.ParseExact(request.TransactionDateTopUp, TimaSettingConstant.DateTimeDayMonthYearFull, null);
                }
                var customerInfo = (await _customerTab.WhereClause(x => x.CustomerID == request.CustomerID).QueryAsync()).FirstOrDefault();
                if (customerInfo == null && customerInfo.CustomerID < 1)
                {
                    response.Message = MessageConstant.ErrorNotExist;
                    return response;
                }
                var currentDate = DateTime.Now;
                if (await _logCustomerCashTab.InsertAsync(new Domain.Tables.TblLogCustomerCash
                {
                    CustomerID = request.CustomerID,
                    CreateDate = currentDate,
                    MoneyBefore = customerInfo.TotalMoney,
                    MoneyAdd = request.MoneyAdd,
                    MoneyAfter = customerInfo.TotalMoney + request.MoneyAdd,
                    Description = request.Description

                }) < 1)
                {
                    _logger.LogError($"UpdateMoneyCustomerCommand|request={_common.ConvertObjectToJSonV2(request)}|insert TblLogCustomerCash not success ");
                    return response;
                }
                customerInfo.TotalMoney += request.MoneyAdd;
                //customerInfos.TotalMoney += request.MoneyAdd;
                customerInfo.ModifyDate = currentDate;
                if (transactionDateTopUp.HasValue)
                {
                    customerInfo.LatestDateTopup = transactionDateTopUp;
                }
                if (!await _customerTab.UpdateAsync(customerInfo))
                {
                    _logger.LogError($"UpdateMoneyCustomerCommand|request={_common.ConvertObjectToJSonV2(request)}|Update not success");
                    return response;
                }
                response.SetSucces();
                response.Data = customerInfo.TotalMoney;
                if (transactionDateTopUp.HasValue)
                {
                    var t1 = await _kakfaLogEventTab.InsertAsync(new Domain.Tables.TblKafkaLogEvent
                    {
                        CreateDate = DateTime.Now,
                        ModifyDate = DateTime.Now,
                        DataMessage = _common.ConvertObjectToJSonV2(new Kafka.Messages.Customer.CustomerBalanceToupSuccessInfo
                        {
                            CustomerID = customerInfo.CustomerID
                        }),
                        ServiceName = ServiceHostInternal.CustomerService,
                        Status = (int)KafkaLogEvent_Status.Waitting,
                        TopicName = LMS.Kafka.Constants.KafkaTopics.CustomerBalanceTopupSuccessInfo
                    });
                    //// dùng tạm cách này khi kafka chưa ổn định
                    //var t2 = _collectionService.TranferAgCloseLoanExemption(customerInfo.CustomerID, LMS.Common.Constants.TimaSettingConstant.UserIDAdmin, LMS.Common.Constants.TimaSettingConstant.UserNameAdmin);
                    //await Task.WhenAll(t1, t2);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateMoneyCustomerCommand_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }

}
