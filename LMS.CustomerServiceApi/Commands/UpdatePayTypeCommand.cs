﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.Commands
{
    public class UpdatePayTypeCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; } = 0;
    }

    public class UpdatePayTypeCommandHandler : IRequestHandler<UpdatePayTypeCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        ILogger<UpdatePayTypeCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdatePayTypeCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            ILogger<UpdatePayTypeCommandHandler> logger)
        {
            _customerTab = customerTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(UpdatePayTypeCommand request, CancellationToken cancellationToken)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            string query = $@" 
	            -- update M
	            UPDATE TblCustomer set PayTypeID = 0 where CustomerID in
	            (
		            select c.CustomerID FROM TblLoan(NOLOCK) L 
				              INNER JOIN TblCustomer(NOLOCK) C
				              ON L.CustomerID = C.CustomerID
				              WHERE L.Status =  1 AND LastDateOfPay is null 
                              and NextDate >= cast(GETDATE()  as date)
                              and (LoanID = {request.LoanID} or {request.LoanID} = 0) 
		                      group by c.CustomerID having count(c.CustomerID)  < 2
	             )
            
             UPDATE C SET C.PayTypeID = 2 
			 FROM TblLoan(NOLOCK) L 
			  INNER JOIN TblCustomer(NOLOCK) C
			  ON L.CustomerID = C.CustomerID
			  WHERE L.Status =  1 AND L.NextDate < CAST(GETDATE() AS DATE) and (LoanID = {request.LoanID} or {request.LoanID} = 0) 
	             -- update L / H
	                  update c1 set c1.PayTypeID = t2.PayTypeID FROM TblCustomer C1 INNER JOIN
	                (
                         SELECT CustomerID,PayTypeID = CASE WHEN (SUM(RankData) <= 3 AND SUM(RankData - PayTypeID) <= 0) OR (SUM(RankData) = 6 AND SUM(PayTypeID) > 3) THEN 2 ELSE 1 END FROM
		                    (
		                    select c.CustomerID , Row_Number() 
                  over (Partition BY c.CustomerID
                        ORDER BY paydate DESC ) AS RankData, CASE WHEN DATEDIFF(DAY,PayDate,cast(FirstPaymentDate as date)) > 1  or FirstPaymentDate is null THEN 2 ELSE 1 END as PayTypeID
				                      FROM TblLoan(NOLOCK) L 
				                      INNER JOIN (  select c.CustomerID FROM TblLoan(NOLOCK) L 
				                                  INNER JOIN TblCustomer(NOLOCK) C
				                                  ON L.CustomerID = C.CustomerID
				                                  WHERE L.Status =  1 AND NextDate > GETDATE() and LastDateOfPay is not null and (LoanID = {request.LoanID} or {request.LoanID} = 0)) C
				                      ON L.CustomerID = C.CustomerID
				                      INNER JOIN TblPaymentSchedule(NOLOCK) P 
				                      ON L.LoanID = P.LoanID  AND P.PAYDATE  <= GETDATE()
		                    ) T where RankData <= 3
		                    GROUP BY CustomerID  
	                    ) T2 on C1.CustomerID = T2.CustomerId";
            await _customerTab.QueryAsync(query, null);
            response.SetSucces();
            return response;
        }
    }
}
