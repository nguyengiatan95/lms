﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.Commands
{
    public class CreateInfoBorrowerCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string CustomerName { get; set; }
        public string NumberCard { get; set; }
        public string Phone { get; set; }
        public int Gender { get; set; }
        public int CityID { get; set; }
        public int DistrictID { get; set; }
        public int WardID { get; set; }
        public string Address { get; set; }
        public string AddressHouseHold { get; set; }
        public int IsBorrowing { get; set; }
        public string PermanentAddress { get; set; }
        public string BirthDay { get; set; } // dd/MM/yyyy
        public long TimaCustomerID { get; set; }
    }
    public class CreateInfoBorrowerCommandHandler : IRequestHandler<CreateInfoBorrowerCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCustomerCash> _logCustomerCashTab;
        ILogger<CreateInfoBorrowerCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateInfoBorrowerCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCustomerCash> logCustomerCashTab,
            ILogger<CreateInfoBorrowerCommandHandler> logger)
        {
            _customerTab = customerTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _logCustomerCashTab = logCustomerCashTab;
        }
        public async Task<ResponseActionResult> Handle(CreateInfoBorrowerCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var currentDate = DateTime.Now;
                DateTime? birthDay = null;
                if (!string.IsNullOrEmpty(request.BirthDay))
                {
                    birthDay = DateTime.ParseExact(request.BirthDay, TimaSettingConstant.DateTimeDayMonthYear, null);
                }
                request.CustomerName = request.CustomerName.Trim();
                request.Phone = request.Phone.Trim();
                request.NumberCard = request.NumberCard.Trim();
                // check giống AG ko check riêng mỗi cmt
                if (request.TimaCustomerID > 0)
                {
                    _customerTab.WhereClause(x => x.TimaCustomerID == request.TimaCustomerID);
                }
                else
                {
                    _customerTab.WhereClause(x => (x.NumberCard == request.NumberCard && x.FullName == request.CustomerName)
                                                                || (x.Phone == request.Phone && x.FullName == request.CustomerName));
                }
                var customerInfos = await _customerTab.QueryAsync();
                if (customerInfos != null && customerInfos.Count() > 0)
                {
                    var customerInfo = customerInfos.First();
                    response.Message = MessageConstant.CustomerExist;
                    //_logger.LogError($"CreateInfoBorrowerCommandHandler_Warning|CustomerExists={_common.ConvertObjectToJSonV2(customerInfos)}|message={response.Message}|request={_common.ConvertObjectToJSonV2(request)}");
                    // đã tồn thì cập nhật lại thông tin KH cũ
                    customerInfo.NumberCard = request.NumberCard;
                    customerInfo.FullName = request.CustomerName;
                    customerInfo.Address = request.Address;
                    customerInfo.AddressHouseHold = request.AddressHouseHold;
                    customerInfo.PermanentAddress = request.PermanentAddress;
                    customerInfo.Phone = request.Phone;
                    customerInfo.CityID = request.CityID;
                    customerInfo.DistrictID = request.DistrictID;
                    customerInfo.WardID = request.WardID;
                    customerInfo.IsBorrowing = 1;
                    customerInfo.CreateDate = currentDate;
                    customerInfo.Gender = request.Gender;
                    customerInfo.NumberCard = request.NumberCard;
                    customerInfo.BirthDay = birthDay;
                    if (await _customerTab.UpdateAsync(customerInfo))
                    {
                        response.SetSucces();
                        response.Data = customerInfo.CustomerID;
                    }
                    return response;
                }

                var customerID = _customerTab.Insert(new Domain.Tables.TblCustomer
                {
                    FullName = request.CustomerName,
                    Address = request.Address,
                    AddressHouseHold = request.AddressHouseHold,
                    PermanentAddress = request.PermanentAddress,
                    Phone = request.Phone,
                    CityID = request.CityID,
                    DistrictID = request.DistrictID,
                    WardID = request.WardID,
                    IsBorrowing = 1,
                    CreateDate = currentDate,
                    Gender = request.Gender,
                    NumberCard = request.NumberCard,
                    BirthDay = birthDay,
                    PayTypeID = (int)Customer_PayTypeID.Medium,
                    TimaCustomerID = request.TimaCustomerID
                });
                if (customerID > 0)
                {
                    _logCustomerCashTab.Insert(new Domain.Tables.TblLogCustomerCash
                    {
                        CreateDate = currentDate,
                        CustomerID = customerID,
                        MoneyBefore = 0,
                        MoneyAdd = 0,
                        MoneyAfter = 0,
                        Description = "Khởi tạo thông tin KH"
                    });
                    response.SetSucces();
                    response.Data = customerID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateInfoBorrowerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

    }
}
