﻿using LMS.Common.Constants;
using LMS.CustomerServiceApi.Domain.Tables;
using LMS.Entites.Dtos.VAService;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.Commands
{
    public class RegisterVaCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class RegisterVaCommandHandler : IRequestHandler<RegisterVaCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> _settingKeyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogVa> _vALogTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomerBank> _customerBankTab;
        LMS.Common.DAL.ITableHelper<Domain.AG.TblCustomer> _customerAGTab;
        LMS.Common.DAL.ITableHelper<Domain.AG.GpayVA> _gpayVATab;
        RestClients.IVAServices _vAServices;
        ILogger<RegisterVaCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public RegisterVaCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> settingKeyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogVa> vALogTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomerBank> customerBankTab,
            LMS.Common.DAL.ITableHelper<Domain.AG.TblCustomer> customerAGTab,
            LMS.Common.DAL.ITableHelper<Domain.AG.GpayVA> gpayVATab,
            RestClients.IVAServices vAServices,
            ILogger<RegisterVaCommandHandler> logger)
        {
            _settingKeyTab = settingKeyTab;
            _loanTab = loanTab;
            _customerTab = customerTab;
            _vAServices = vAServices;
            _vALogTab = vALogTab;
            _customerBankTab = customerBankTab;
            _customerAGTab = customerAGTab.SetConnectString(LMS.Common.Configuration.ConnectStringSetting.AGConnectString);
            _gpayVATab = gpayVATab.SetConnectString(LMS.Common.Configuration.ConnectStringSetting.AGConnectString);
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(RegisterVaCommand request, CancellationToken cancellationToken)
        {

            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.SetSucces();
                var keySettingInfo = (await _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.VA_Register).QueryAsync()).FirstOrDefault();

                if (keySettingInfo == null || keySettingInfo.Status == (int)VaSettingKey_Status.Stop || keySettingInfo.Value == null)
                {
                    response.Message = MessageConstant.SettingKey_VA_Register;
                    return response;
                }
                #region lấy thông tin AG
                if (keySettingInfo.Status == (int)VaSettingKey_Status.InforAG)
                {
                    _ = UpdateInforVaInAG(keySettingInfo);
                    await UpdateSettingKey(keySettingInfo, (int)VaSettingKey_Status.Stop);
                    return response;
                }
                #endregion
                if (keySettingInfo.Status == (int)VaSettingKey_Status.CallBy)
                {
                    _ = ProcessCallBy(keySettingInfo);
                    await UpdateSettingKey(keySettingInfo, (int)VaSettingKey_Status.Stop);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"RegisterVaCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;

        }

        private async Task ProcessCallBy(Domain.Tables.TblSettingKey keySettingInfo)
        {
            try
            {
                int maxItemQuery = 50;
                List<TblLogVa> lstLogVaInsert = new List<TblLogVa>();
                List<TblCustomerBank> lstCustomerBankInsert = new List<TblCustomerBank>();
                var objConfigRegisterBankCustomer = _common.ConvertJSonToObjectV2<Domain.ConfigRegisterBankCustomer>(keySettingInfo.Value);
                long lastLoanIDProcessed = objConfigRegisterBankCustomer.LoanID;
                var lstLoan = await _loanTab.SetGetTop(maxItemQuery)
                                .SelectColumns(x => x.CustomerID, x => x.LoanID, x => x.TotalMoneyDisbursement, x => x.FromDate, x => x.ToDate)
                                .WhereClause(x => x.LoanID > lastLoanIDProcessed).QueryAsync();
                #region tạo tk KH mới
                if (lstLoan != null)
                {
                    var lstCustomerID = new List<long>();
                    foreach (var item in lstLoan)
                    {
                        lstCustomerID.Add((long)item.CustomerID);
                    }

                    var lstCustomerTask = _customerTab.WhereClause(x => lstCustomerID.Contains(x.CustomerID)).QueryAsync();
                    var lstCustomerBankTask = _customerBankTab.WhereClause(x => lstCustomerID.Contains((long)x.CustomerID)).QueryAsync();
                    await Task.WhenAll(lstCustomerTask, lstCustomerBankTask);

                    var dictCustomerInfos = lstCustomerTask.Result.ToDictionary(x => x.CustomerID, x => x);
                    var lstCustomerBank = lstCustomerBankTask.Result;
                    if (lstCustomerBank == null)
                    {
                        lstCustomerBank = new List<Domain.Tables.TblCustomerBank>();
                    }

                    if (dictCustomerInfos != null && dictCustomerInfos.Count() > 0)
                    {
                        foreach (var item in lstLoan)
                        {
                            var objCustomerBank = lstCustomerBank.FirstOrDefault(x => x.CustomerID == item.CustomerID);
                            if (objCustomerBank != null && objCustomerBank.EndDate.HasValue
                                && (objCustomerBank.EndDate.Value == DateTime.MinValue || objCustomerBank.EndDate.Value.Date > DateTime.Now.Date))// đã có tài khoản và  tài khoản còn hạn sử dụng
                                continue;

                            var objCustomer = dictCustomerInfos.GetValueOrDefault((long)item.CustomerID);
                            if (objCustomer == null)
                            {
                                _logger.LogError($"ProcessCallBy_Warning|Not_Found_Customer_Info|loanID={item.LoanID}|CustomerID={item.CustomerID}");
                                continue;
                            }

                            string registerVA_json = string.Empty;
                            var registerVA = MapRegisterVA(item, objCustomer, ref registerVA_json);
                            if (registerVA == null)
                                continue;

                            var mapid = objCustomer.CustomerID.ToString();
                            if (objCustomer.TimaCustomerID > 0)
                            {
                                mapid = objCustomer.TimaCustomerID.ToString();
                            }
                            var customerBank = new TblCustomerBank
                            {
                                MapID = mapid,
                                CustomerID = item.CustomerID,
                                CreateDate = DateTime.Now,
                                PartnerCode = (int)CustomerBank_PartnerCode.VA,
                                StatusVa = (int)CustomerBank_StatusVA.Error,
                                LoanID = item.LoanID,
                                Repeat = 1
                            };
                            // improve sau nếu dữ liệu nhiều
                            //tao tk
                            var createVAResponse = await _vAServices.Register(registerVA);
                            //lu log
                            var objLogVa = new TblLogVa
                            {
                                Pcode = VAConstants.PcodeRegister,
                                Request = registerVA_json,
                                LoanID = (int)item.LoanID,
                                CustomerID = (int)item.CustomerID,
                                CreateDate = DateTime.Now,
                                Response = _common.ConvertObjectToJSonV2(createVAResponse),
                                Status = (int)LogVA_Status.Error
                            };
                            if (createVAResponse.ResponseCode == VAConstants.ResponseCodeSuccess)
                            {
                                //UpdateStatusSuccess(customerBank, createVAResponse, item.ToDate.AddYears(VAConstants.AddYear));
                                customerBank.MapID = createVAResponse.MapID;
                                customerBank.AccountNo = createVAResponse.AccountNo;
                                customerBank.AccountName = createVAResponse.AccountName;
                                customerBank.EndDate = item.ToDate.AddYears(VAConstants.AddYear);
                                customerBank.BankCode = createVAResponse.BankCode;
                                customerBank.BankName = createVAResponse.BankName;
                                customerBank.StatusVa = (int)CustomerBank_StatusVA.Success;
                                objLogVa.Status = (int)LogVA_Status.Success;

                                // ghi nhận loanID thành công
                                lastLoanIDProcessed = item.LoanID;
                            }
                            lstLogVaInsert.Add(objLogVa);
                            lstCustomerBankInsert.Add(customerBank);
                        }
                    }
                }
                #endregion
                if (lstLogVaInsert.Count > 0)
                {
                    _vALogTab.InsertBulk(lstLogVaInsert);
                }
                if (lstCustomerBankInsert.Count > 0)
                {
                    _customerBankTab.InsertBulk(lstCustomerBankInsert);
                }

                await ProcessRetry();

                objConfigRegisterBankCustomer.LoanID = lastLoanIDProcessed;
                keySettingInfo.Value = _common.ConvertObjectToJSonV2(objConfigRegisterBankCustomer);
                keySettingInfo.ModifyDate = DateTime.Now;
                keySettingInfo.Status = (int)VaSettingKey_Status.CallBy;

                await UpdateSettingKey(keySettingInfo, (int)VaSettingKey_Status.CallBy);
                //_ = await _settingKeyTab.UpdateAsync(keySettingInfo);
            }
            catch (Exception ex)
            {
                _logger.LogError($"RegisterVaCommandHandler_ProcessCallBy|request={_common.ConvertObjectToJSonV2(keySettingInfo)}|ex={ex.Message}-{ex.StackTrace}");
            }
        }

        private async Task ProcessRetry()
        {
            try
            {
                #region tạo lại KH bị lỗi
                var lstCustomerBankError = await _customerBankTab.WhereClause(x => x.StatusVa == (int)CustomerBank_StatusVA.Error && x.Repeat < VAConstants.Repeat).QueryAsync();
                List<TblLogVa> lstLogVaInsert = new List<TblLogVa>();
                if (lstCustomerBankError == null || !lstCustomerBankError.Any())
                {
                    return;
                }
                var lstCustomeID = new List<long>();
                var lstLoanID = new List<long>();
                var currentDate = DateTime.Now;
                foreach (var item in lstCustomerBankError)
                {
                    lstCustomeID.Add((long)item.CustomerID);
                    lstLoanID.Add((long)item.LoanID);
                }
                var lstLoanErrorTask = _loanTab.SelectColumns(x => x.LoanID, x => x.FromDate, x => x.ToDate, x => x.TotalMoneyDisbursement, x => x.CustomerID)
                                                .WhereClause(x => lstLoanID.Contains(x.LoanID)).QueryAsync();
                var lstCustomerErrorTask = _customerTab.WhereClause(x => lstCustomeID.Contains(x.CustomerID)).QueryAsync();
                await Task.WhenAll(lstLoanErrorTask, lstCustomerErrorTask);

                var dictLoanErrorInfos = lstLoanErrorTask.Result.ToDictionary(x => x.LoanID, x => x);
                var dictCustomerErrorInfos = lstCustomerErrorTask.Result.ToDictionary(x => x.CustomerID, x => x);
                foreach (var item in lstCustomerBankError)
                {
                    item.Repeat += 1;
                    var objLoan = dictLoanErrorInfos.GetValueOrDefault(item.LoanID);
                    var objCustomer = dictCustomerErrorInfos.GetValueOrDefault((long)item.CustomerID);
                    if (objLoan == null || objCustomer == null)
                    {
                        continue;
                    }
                    string registerVA_json = string.Empty;
                    var registerVA = MapRegisterVA(objLoan, objCustomer, ref registerVA_json);
                    if (registerVA == null)
                    {
                        continue;
                    }
                    // improve sau nếu dữ liệu nhiều
                    var createVAResponse = await _vAServices.Register(registerVA);
                    //lu log
                    var objLogVa = new TblLogVa
                    {
                        Pcode = VAConstants.PcodeRegister,
                        Request = registerVA_json,
                        LoanID = (int)item.LoanID,
                        CustomerID = (int)item.CustomerID,
                        CreateDate = DateTime.Now,
                        Response = _common.ConvertObjectToJSonV2(createVAResponse),
                        Status = (int)LogVA_Status.Error
                    };
                    if (createVAResponse.ResponseCode == VAConstants.ResponseCodeSuccess)
                    {
                        // Cập nhật thông tin
                        //UpdateStatusSuccess(item, createVAResponse, objLoan.ToDate.AddYears(VAConstants.AddYear));
                        item.MapID = createVAResponse.MapID;
                        item.AccountNo = createVAResponse.AccountNo;
                        item.AccountName = createVAResponse.AccountName;
                        item.EndDate = objLoan.ToDate.AddYears(VAConstants.AddYear);
                        item.BankCode = createVAResponse.BankCode;
                        item.BankName = createVAResponse.BankName;
                        item.StatusVa = (int)CustomerBank_StatusVA.Success;
                        objLogVa.Status = (int)LogVA_Status.Success;
                    }
                    lstLogVaInsert.Add(objLogVa);
                }
                if (lstLogVaInsert.Count > 0)
                {
                    _vALogTab.InsertBulk(lstLogVaInsert);
                }
                _customerBankTab.UpdateBulk(lstCustomerBankError);
                #endregion
            }
            catch (Exception ex)
            {
                _logger.LogError($"RegisterVaCommandHandler_ProcessRetry|ex={ex.Message}-{ex.StackTrace}");
            }
        }

        public RegisterVA MapRegisterVA(TblLoan objLoan, TblCustomer objCustomer, ref string request)
        {
            try
            {
                var registerVA = new RegisterVA
                {
                    pcode = VAConstants.PcodeRegister,
                    merchant_code = VAConstants.MerchantCode
                };
                var customerRegister = new CustomerRegisterVa
                {
                    phone = objCustomer.Phone,
                    email = string.Empty,
                    address = objCustomer.Address,
                    id = objCustomer.NumberCard,
                };
                var mapid = objCustomer.CustomerID.ToString();
                if (objCustomer.TimaCustomerID > 0)
                {
                    mapid = objCustomer.TimaCustomerID.ToString();
                }
                var registerInformation = new RegisterInformation
                {
                    map_id = mapid,
                    amount = (int)objLoan.TotalMoneyDisbursement,
                    start_date = objLoan.FromDate.ToString(VAConstants.ConvertStartDate),
                    end_date = objLoan.ToDate.AddYears(VAConstants.AddYear).ToString(VAConstants.ConvertEndDate),
                    condition = VAConstants.Condition,
                    customer_name = _common.UnicodeToPlain(objCustomer.FullName),
                    request_id = VAConstants.MerchantCode + "_" + DateTime.Now.Ticks,
                    bank_code = VAConstants.BankCode,
                    extend = customerRegister,
                };
                var registerVA_json = _common.ConvertObjectToJSonV2(registerInformation);
                var dataEncrypt3DES = new Encrypt().Encrypt3DES(registerVA_json, VAConstants.Key3DES);
                registerVA.data = dataEncrypt3DES;

                request = registerVA_json;
                return registerVA;
            }
            catch (Exception ex)
            {
                _logger.LogError($"MapRegisterVA_Exception|LoanID={objLoan.LoanID}|CustomerID={objCustomer.CustomerID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }

        public Task UpdateRepeat(TblCustomerBank objCustomerBank)
        {
            objCustomerBank.Repeat = objCustomerBank.Repeat + 1;
            if (objCustomerBank.Repeat == VAConstants.Repeat)
            {
                _logger.LogError($"Error Register VA .Repeat = {VAConstants.Repeat} |CustomerBank={_common.ConvertObjectToJSonV2(objCustomerBank)}");
            }
            _customerBankTab.UpdateAsync(objCustomerBank);
            return Task.CompletedTask;
        }

        public void UpdateStatusSuccess(TblCustomerBank item, VAResponse entity, DateTime endDate)
        {
            item.MapID = entity.MapID;
            item.AccountNo = entity.AccountNo;
            item.AccountName = entity.AccountName;
            item.EndDate = endDate;
            item.BankCode = entity.BankCode;
            item.BankName = entity.BankName;
            item.StatusVa = (int)CustomerBank_StatusVA.Success;
            _customerBankTab.Update(item);//Update Lại thông tin
        }

        public async Task<ResponseActionResult> UpdateInforVaInAG(Domain.Tables.TblSettingKey keySettingInfo)
        {
            var response = new ResponseActionResult();
            try
            {
                int maxItemQuery = 1000;
                var objConfigRegisterBankCustomer = _common.ConvertJSonToObjectV2<Domain.ConfigRegisterBankCustomer>(keySettingInfo.Value);

                long customerIDSetting = objConfigRegisterBankCustomer.CustomerID;
                var dictCustomerInfor = (await _customerTab.SelectColumns(x => x.CustomerID, x => x.TimaCustomerID).SetGetTop(maxItemQuery)
                                                    .WhereClause(x => x.CustomerID > customerIDSetting).QueryAsync())
                                                    .ToDictionary(x => x.CustomerID, x => x);

                if (dictCustomerInfor == null || dictCustomerInfor.Count < 1)
                {
                    // update lại Status  =2  khi ko có CustomerID mới
                    ////keySettingInfo.Status = (int)VaSettingKey_Status.InforAG;
                    ////keySettingInfo.ModifyDate = DateTime.Now;
                    //_ = await _settingKeyTab.UpdateAsync(keySettingInfo);
                    await UpdateSettingKey(keySettingInfo, (int)VaSettingKey_Status.InforAG);

                    response.SetSucces();
                    return response;
                }

                var lstCustomerIDLMS = new List<long>();
                var lstCustomerIDAG = new List<long>();
                foreach (var item in dictCustomerInfor)
                {
                    lstCustomerIDLMS.Add((long)item.Key);
                    lstCustomerIDAG.Add((long)item.Value.TimaCustomerID);
                }

                var lstCustomerBankTask = _customerBankTab.WhereClause(x => lstCustomerIDLMS.Contains((long)x.CustomerID)).QueryAsync();
                var lstCustomerAGTask = _customerAGTab.WhereClause(x => lstCustomerIDAG.Contains(x.CustomerID)).QueryAsync();
                var lstCustomerGpayVATask = _gpayVATab.WhereClause(x => lstCustomerIDAG.Contains((long)x.CustomerId)).QueryAsync();

                await Task.WhenAll(lstCustomerBankTask, lstCustomerAGTask, lstCustomerGpayVATask);

                var lstCustomerBank = lstCustomerBankTask.Result;
                var dictCustomerAGInfos = lstCustomerAGTask.Result.ToDictionary(x => x.CustomerID, x => x);
                var lstCustomerGpayVA = lstCustomerGpayVATask.Result;
                var dictCustomerGpayVAInfos = new Dictionary<int, List<Domain.AG.GpayVA>>();
                foreach (var item in lstCustomerGpayVA)
                {
                    if (!dictCustomerGpayVAInfos.ContainsKey((int)item.CustomerId))
                    {
                        dictCustomerGpayVAInfos[(int)item.CustomerId] = new List<Domain.AG.GpayVA>();
                    }
                    dictCustomerGpayVAInfos[(int)item.CustomerId].Add(item);
                }

                var lstCustomerBankInfor = new List<Domain.Tables.TblCustomerBank>();
                foreach (var item in dictCustomerInfor)
                {
                    var objcustomerLMS = dictCustomerInfor.GetValueOrDefault(item.Key);
                    if (objcustomerLMS.TimaCustomerID < 1)
                        continue;

                    var objCustomerBankVA = lstCustomerBank.FirstOrDefault(x => x.CustomerID == objcustomerLMS.CustomerID && x.PartnerCode == (int)CustomerBank_PartnerCode.VA);
                    if (objCustomerBankVA == null || (objCustomerBankVA.EndDate == DateTime.MinValue || objCustomerBankVA.EndDate < DateTime.Now))//chưa có  tài khoản va epay hoặc  tài khoản hết hạn sử dụng
                    {
                        var objCustomerAG = dictCustomerAGInfos.GetValueOrDefault((int)objcustomerLMS.TimaCustomerID);
                        if (objCustomerAG != null && !string.IsNullOrEmpty(objCustomerAG.Map_id))
                        {
                            var customerBank = new TblCustomerBank
                            {
                                AccountNo = objCustomerAG.Account_no,
                                AccountName = objCustomerAG.Account_name,
                                BankCode = objCustomerAG.BankCode_Va,
                                BankName = objCustomerAG.BankName_Va,
                                EndDate = objCustomerAG.EndDate,
                                MapID = objCustomerAG.Map_id,
                                CustomerID = item.Key,
                                CreateDate = DateTime.Now,
                                PartnerCode = (int)CustomerBank_PartnerCode.VA,
                                StatusVa = objCustomerAG.Status_Va,
                                Repeat = 0
                            };
                            lstCustomerBankInfor.Add(customerBank);
                        }
                    }

                    //số tk nh customer đang có LMS
                    var lstObjBankCustomer = lstCustomerBank.Where(x => x.CustomerID == objcustomerLMS.CustomerID
                                                                            && x.StatusVa == 1);

                    //các số tk nh customer đang có bên AG
                    var lstAccountCustomerAG = dictCustomerGpayVAInfos.GetValueOrDefault((int)objcustomerLMS.TimaCustomerID);
                    if (lstAccountCustomerAG != null)
                    {
                        foreach (var k in lstAccountCustomerAG)
                        {
                            if (lstObjBankCustomer.Any(x => x.CustomerID == k.CustomerId && x.StatusVa == 1 && x.BankCode == k.BankCode))// đã được đông bộ về
                                continue;

                            int partnerCode = 0;
                            try
                            {
                                partnerCode = (int)Enum.Parse(typeof(CustomerBank_PartnerCode), k.BankCode);
                            }
                            catch (Exception ex)
                            {
                                _logger.LogError($"NotFindPartnerCode_Exception|request={_common.ConvertObjectToJSonV2(k)}|ex={ex.Message}-{ex.StackTrace}");
                                continue;
                            }
                            var customerBank = new TblCustomerBank
                            {
                                AccountNo = k.AccountNumber,
                                AccountName = k.AccountName,
                                BankCode = k.BankCode,
                                BankName = k.BankCode,
                                MapID = k.MapId,
                                CustomerID = item.Key,
                                CreateDate = k.StartAt,
                                PartnerCode = partnerCode,
                                StatusVa = k.Status,
                                Repeat = 0
                            };
                            lstCustomerBankInfor.Add(customerBank);
                        }
                    }
                }
                _customerBankTab.InsertBulk(lstCustomerBankInfor);
                objConfigRegisterBankCustomer.CustomerID = dictCustomerInfor.Keys.OrderByDescending(x => x).First();
                keySettingInfo.Value = _common.ConvertObjectToJSonV2(objConfigRegisterBankCustomer);
                keySettingInfo.Status = (int)VaSettingKey_Status.InforAG;
                keySettingInfo.ModifyDate = DateTime.Now;

                await UpdateSettingKey(keySettingInfo, (int)VaSettingKey_Status.InforAG);
                //_ = await _settingKeyTab.UpdateAsync(keySettingInfo);
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"RegisterVaCommandHandler_UpdateInforVaInAG_Exception|request={_common.ConvertObjectToJSonV2(keySettingInfo)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private async Task UpdateSettingKey(Domain.Tables.TblSettingKey objSettingKey, int status, int retryUpdateKey = 0)
        {
            bool updateResult = false;
            do
            {
                try
                {
                    objSettingKey.ModifyDate = DateTime.Now;
                    objSettingKey.Status = status;
                    updateResult = await _settingKeyTab.UpdateAsync(objSettingKey);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"RegisterVa_UpdateSettingKey|ex={ex.Message}-{ex.StackTrace}");
                    // retry lại 
                    if (retryUpdateKey > 5)
                    {
                        return;
                    }
                    await Task.Delay(TimeSpan.FromSeconds(2));
                }
            } while (!updateResult);

        }
    }
}
