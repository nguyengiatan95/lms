﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.Commands
{
    public class ChangePhoneCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string Phone { get; set; }
        public long CustomerID { get; set; }
        public long CreateBy { get; set; }
        public long LoanID { get; set; }
        public string Note { get; set; }
    }
    public class ChangePhoneCommandHandler : IRequestHandler<ChangePhoneCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.CustomerChangePhone> _customerChangePhoneTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogChangePhone> _logChangePhoneTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<ChangePhoneCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public ChangePhoneCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.CustomerChangePhone> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogChangePhone> logChangePhoneTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            ILogger<ChangePhoneCommandHandler> logger)
        {
            _customerChangePhoneTab = customerTab;
            _logChangePhoneTab = logChangePhoneTab;
            _userTab = userTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(ChangePhoneCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var customInfosTask = _customerChangePhoneTab.WhereClause(x => x.CustomerID == request.CustomerID).QueryAsync();
                var userInfosTask = _userTab.WhereClause(x => x.UserID == request.CreateBy).QueryAsync();
                await Task.WhenAll(new Task[] { customInfosTask, userInfosTask });
                var customInfos = customInfosTask.Result;
                var userInfos = userInfosTask.Result;
                if (customInfos == null || !customInfos.Any())
                {
                    response.Message = MessageConstant.CustomerNotExist;
                    return response;
                }
                if (userInfos == null || !userInfos.Any())
                {
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                var customerDetail = customInfos.First();
                var createByInfo = userInfos.First();
                var currentDate = DateTime.Now;
                Domain.Tables.TblLogChangePhone logChangePhone = new Domain.Tables.TblLogChangePhone
                {
                    CustomerID = request.CustomerID,
                    NewPhone = request.Phone,
                    CreateBy = createByInfo.UserID,
                    CreateByName = createByInfo.FullName,
                    OldPhone = customerDetail.Phone,
                    CreateDate = currentDate,
                    LoanID = request.LoanID,
                    Note = request.Note
                };
                customerDetail.ModifyDate = currentDate;
                customerDetail.Phone = request.Phone;
                if (_customerChangePhoneTab.Update(customerDetail))
                {
                    var logChangePhoneID = _logChangePhoneTab.Insert(logChangePhone);
                    response.SetSucces();
                    response.Data = logChangePhoneID;
                }

            }
            catch (Exception ex)
            {
                _logger.LogError($"ChangePhoneCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
