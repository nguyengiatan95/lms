﻿using LMS.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.RestClients
{
    public interface ICollectionService
    {
        Task<ResponseActionResult> TranferAgCloseLoanExemption(long customerID, long createBy, string username);
    }
    public class CollectionService : ICollectionService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        public CollectionService(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }

        public async Task<ResponseActionResult> TranferAgCloseLoanExemption(long customerID, long createBy, string username)
        {
            var req = new
            {
                CustomerID = customerID,
                UserID = createBy,
                Username = username
            };
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{ActionApiInternal.CollectionService_MGLP_TranferAgCloseLoanExemption}";
            return await _apiHelper.ExecuteAsync(url.ToString(), RestSharp.Method.POST, req);
        }
    }
}
