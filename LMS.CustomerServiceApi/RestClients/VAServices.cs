﻿using LMS.Entites.Dtos.VAService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.RestClients
{
    public interface IVAServices
    {
        Task<VAResponse> Register(RegisterVA entity);
    }
    public class VAServices : IVAServices
    {
        Common.Helper.IApiHelper _apiHelper;
        public VAServices(Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }
        public async Task<VAResponse> Register(RegisterVA req)
        {
            var url = $"{Common.Constants.ServiceHostInternal.OutGateway}{Common.Constants.ActionApiInternal.OutGatewayApi_VA_Register}";
            return await _apiHelper.ExecuteAsync<VAResponse>(url: url.ToString(), RestSharp.Method.POST, req);
        }

    }
}
