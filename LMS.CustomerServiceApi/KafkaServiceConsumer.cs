﻿using LMS.Kafka.Interfaces;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi
{
    public class KafkaServiceConsumer : BackgroundService
    {
        IKafkaConsumer<string, LMS.Kafka.Messages.Customer.CreateBorrowerInfo> _createBorrowerInfoConsumer;
        IKafkaConsumer<string, LMS.Kafka.Messages.Customer.CustomerBalanceChangeInfo> _customerBalanceChangeConsumer;
        IKafkaConsumer<string, LMS.Kafka.Messages.Customer.CustomerBalanceTopupInfo> _customerBalanceTopupConsumer;

        ILogger<KafkaServiceConsumer> _logger;
        public KafkaServiceConsumer(IKafkaConsumer<string, LMS.Kafka.Messages.Customer.CreateBorrowerInfo> kafkaConsumer,
            IKafkaConsumer<string, LMS.Kafka.Messages.Customer.CustomerBalanceChangeInfo> customerBalanceChangeConsumer,
            IKafkaConsumer<string, LMS.Kafka.Messages.Customer.CustomerBalanceTopupInfo> customerBalanceTopupConsumer,
            ILogger<KafkaServiceConsumer> logger)
        {
            _createBorrowerInfoConsumer = kafkaConsumer;
            _customerBalanceChangeConsumer = customerBalanceChangeConsumer;
            _customerBalanceTopupConsumer = customerBalanceTopupConsumer;
            _logger = logger;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                //var t1 = _createBorrowerInfoConsumer.Consume(Kafka.Constants.KafkaTopics.CustomerCreateUpdateInfo, stoppingToken);
                //var t2 = _customerBalanceChangeConsumer.Consume(Kafka.Constants.KafkaTopics.CustomerBalanceChangeInfo, stoppingToken);
                //var t3 = _customerBalanceChangeConsumer.Consume(Kafka.Constants.KafkaTopics.CustomerBalanceTopupInfo, stoppingToken);
                //await Task.WhenAll(t1, t2, t3);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"{(int)HttpStatusCode.InternalServerError} ConsumeFailedOnTopic - {Kafka.Constants.KafkaTopics.CustomerCreateUpdateInfo}, {ex}");
            }
        }

        public override void Dispose()
        {
            _logger.LogError($"KafkaServiceConsumer_CustomerServiceApi_Dispose");
            _createBorrowerInfoConsumer.Close();
            _createBorrowerInfoConsumer.Dispose();
            _customerBalanceChangeConsumer.Close();
            _customerBalanceChangeConsumer.Dispose();
            _customerBalanceTopupConsumer.Close();
            _customerBalanceTopupConsumer.Dispose();

            base.Dispose();
        }
    }
}
