﻿using LMS.Kafka.Interfaces;
using LMS.Kafka.Messages.Customer;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.KafkaEventHandlers
{
    public class CustomerBalanceChangeInfoHandler : IKafkaHandler<string, LMS.Kafka.Messages.Customer.CustomerBalanceChangeInfo>
    {
        private readonly IMediator _bus;
        LMS.Common.Helper.Utils _common = new Common.Helper.Utils();
        public CustomerBalanceChangeInfoHandler(IMediator bus)
        {
            _bus = bus;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> HandleAsync(string key, CustomerBalanceChangeInfo value)
        {
            LMS.Common.Constants.ResponseActionResult response = new Common.Constants.ResponseActionResult();
            try
            {
                Commands.UpdateMoneyCustomerCommand request = new Commands.UpdateMoneyCustomerCommand
                {
                    CustomerID = value.CustomerID,
                    Description = value.Note,
                    MoneyAdd = value.TotalMoney,
                    TransactionDateTopUp = value.TransactionDateTopUp
                };
                response = await _bus.Send(request);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return response;
        }
    }
}
