﻿using LMS.Kafka.Interfaces;
using LMS.Kafka.Messages.Customer;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.KafkaEventHandlers
{
    public class CustomerBalanceTopupInfoHandler : IKafkaHandler<string, LMS.Kafka.Messages.Customer.CustomerBalanceTopupInfo>
    {
        private readonly IKafkaProducer<string, LMS.Kafka.Messages.Customer.CustomerBalanceTopupInfo> _customerProducer;
        private readonly IMediator _bus;
        LMS.Common.Helper.Utils _common = new Common.Helper.Utils();
        public CustomerBalanceTopupInfoHandler(IMediator bus,
            IKafkaProducer<string, LMS.Kafka.Messages.Customer.CustomerBalanceTopupInfo> customerProducer)
        {
            _bus = bus;
            _customerProducer = customerProducer;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> HandleAsync(string key, CustomerBalanceTopupInfo value)
        {
            Commands.UpdateMoneyCustomerCommand request = new Commands.UpdateMoneyCustomerCommand
            {
                CustomerID = value.CustomerID,
                Description = value.Note,
                MoneyAdd = value.TotalMoney,
                TransactionDateTopUp = value.TransactionDateTopUp
            };
            return await _bus.Send(request);
        }
    }
}
