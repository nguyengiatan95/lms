﻿using LMS.Kafka.Interfaces;
using LMS.Kafka.Messages.Customer;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.CustomerServiceApi.KafkaEventHandlers
{
    public class CustomerInsertUpdateInfoHandler : IKafkaHandler<string, LMS.Kafka.Messages.Customer.CreateBorrowerInfo>
    {
        private readonly IKafkaProducer<string, LMS.Kafka.Messages.Customer.CustomerInsertSuccess> _customerProducer;
        private readonly IMediator _bus;
        LMS.Common.Helper.Utils _common = new Common.Helper.Utils();
        public CustomerInsertUpdateInfoHandler(IMediator bus,
            IKafkaProducer<string, LMS.Kafka.Messages.Customer.CustomerInsertSuccess> customerProducer)
        {
            _bus = bus;
            _customerProducer = customerProducer;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> HandleAsync(string key, CreateBorrowerInfo value)
        {
            Commands.CreateInfoBorrowerCommand request = _common.ConvertParentToChild<Commands.CreateInfoBorrowerCommand, CreateBorrowerInfo>(value);
            var response = await _bus.Send(request);
            //if (response.Result == (int)LMS.Common.Constants.ResponseAction.Success)
            //{
            //    _ = _customerProducer.ProduceAsync(LMS.Kafka.Constants.KafkaTopics.LoanUpdateCustomerIDInfo, "", new CustomerInsertSuccess
            //    {
            //        CustomerID = Convert.ToInt64(response.Data),
            //        LoanID = value.LoanID
            //    });
            //}
            return response;
        }
    }
}
