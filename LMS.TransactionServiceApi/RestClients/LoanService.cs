﻿using LMS.Entites.Dtos.LoanServices;
using System.Threading.Tasks;

namespace LMS.TransactionServiceApi.RestClients
{
    public interface ILoanService
    {
        Task<LoanDetailViewModel> GetLoanByID(long LoanID);
    }
    public class LoanService : ILoanService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        public LoanService(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }

        public async Task<LoanDetailViewModel> GetLoanByID(long LoanID)
        {
            string fullPath = string.Format(LMS.Common.Constants.ActionApiInternal.Loan_Action_GetLoanByID, LoanID);
            var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{fullPath}";
            return await _apiHelper.ExecuteAsync<LoanDetailViewModel>(url: url.ToString(), body: null, method: RestSharp.Method.GET);
        }
    }
}
