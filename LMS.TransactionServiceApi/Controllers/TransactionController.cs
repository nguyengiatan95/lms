﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LMS.TransactionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly IMediator bus;
        public TransactionController(IMediator bus)
        {
            this.bus = bus;
        }

        [HttpPost]
        [Route("CreateTransactionAfterPayment")]
        public async Task<Common.Constants.ResponseActionResult> CreateTransactionAfterPayment(List<LMS.Entites.Dtos.TransactionServices.RequestCreateListTransactionModel> request)
        {
            var result = await bus.Send(new Commands.CreateAfterPaymentCommand
            {
                LstRequest = request
            });
            return result;
        }

        [HttpGet]
        [Route("GetByLoanID/{LoanID}")]
        public async Task<Common.Constants.ResponseActionResult> GetByLoanID(long LoanID)
        {
            var result = await bus.Send(new Queries.GetByLoanIDQuery
            {
                LoanID = LoanID
            });
            return result;
        }
    }
}
