﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LMS.Common.Helper;
namespace LMS.TransactionServiceApi.Queries
{
    public class GetByLoanIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }

    }

    public class GetLoanByIDQueryHandler : IRequestHandler<GetByLoanIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<GetLoanByIDQueryHandler> _logger;
        public GetLoanByIDQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            RestClients.ILoanService loanService, ILogger<GetLoanByIDQueryHandler> logger)
        {
            _logger = logger;
            _transactionTab = transactionTab;
            _userTab = userTab;
        }
        public async Task<ResponseActionResult> Handle(GetByLoanIDQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                List<Entites.Dtos.TransactionServices.ListTransactionView> listTransactionViews = new List<Entites.Dtos.TransactionServices.ListTransactionView>();
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    int insuranceAction = (int)Transaction_Action.ReceiptInsurance;
                    var lstTran = _transactionTab.OrderByDescending(x => x.CreateDate).WhereClause(x => x.LoanID == request.LoanID && x.ActionID != insuranceAction).Query();
                    if (lstTran != null && lstTran.Any())
                    {
                        var lstUserIds = lstTran.Select(x => x.UserID).ToList();
                        var dicUser = _userTab.WhereClause(x => lstUserIds.Contains(x.UserID)).Query().ToDictionary(x => x.UserID, x => x.UserName);
                        foreach (var item in lstTran)
                        {
                            listTransactionViews.Add(new Entites.Dtos.TransactionServices.ListTransactionView()
                            {
                                CreateDate = item.CreateDate,
                                CustomerID = item.CustomerID,
                                LenderID = item.LenderID,
                                LoanID = item.LoanID,
                                PaymentScheduleID = item.PaymentScheduleID ?? 0,
                                UserID = item.UserID,
                                TotalMoney = item.TotalMoney,
                                ActionName = ((Transaction_Action)item.ActionID).GetDescription(),
                                MoneyTypeName = ((Transaction_TypeMoney)item.MoneyType).GetDescription(),
                                UserName = dicUser.ContainsKey(item.UserID) ? dicUser[item.UserID] : ""
                            }) ;
                        }
                    }
                    response.SetSucces();
                    response.Data = listTransactionViews;
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{System.Reflection.MethodBase.GetCurrentMethod().Name}_Exception|LoanID={request.LoanID}|ex={ex.Message}-{ex.StackTrace}");
                    return response;
                }
            });
        }
    }
}
