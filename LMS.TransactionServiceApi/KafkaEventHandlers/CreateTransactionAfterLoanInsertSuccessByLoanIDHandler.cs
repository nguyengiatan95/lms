﻿using LMS.Common.Constants;
using LMS.Kafka.Interfaces;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.TransactionServiceApi.KafkaEventHandlers
{
    public class CreateTransactionAfterLoanInsertSuccessByLoanIDHandler : IKafkaHandler<string, LMS.Kafka.Messages.Loan.LoanInsertSuccess>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionTab;
        private readonly IMediator _bus;
        LMS.Common.Helper.Utils _common = new Common.Helper.Utils();
        ILogger<CreateTransactionAfterLoanInsertSuccessByLoanIDHandler> _logger;
        public CreateTransactionAfterLoanInsertSuccessByLoanIDHandler(IMediator bus,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionTab,
            ILogger<CreateTransactionAfterLoanInsertSuccessByLoanIDHandler> logger)
        {
            _bus = bus;
            _loanTab = loanTab;
            _transactionTab = transactionTab;
            _logger = logger;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> HandleAsync(string key, LMS.Kafka.Messages.Loan.LoanInsertSuccess value)
        {
            LMS.Common.Constants.ResponseActionResult response = new ResponseActionResult();
            try
            {
                var loanInfosTask = _loanTab.WhereClause(x => x.LoanID == value.LoanID).QueryAsync();
                var txnExistsTask = _transactionTab.SetGetTop(1).WhereClause(x => x.LoanID == value.LoanID && x.ActionID == (int)Transaction_Action.ChoVay).QueryAsync();
                await Task.WhenAll(loanInfosTask, txnExistsTask);
                var loanInfos = loanInfosTask.Result;
                if (loanInfos == null || !loanInfos.Any())
                {
                    _logger.LogError($"CreateTransactionAfterLoanInsertSuccessByLoanIDHandler|value={_common.ConvertObjectToJSonV2(value)}|Not_found_loan");
                    return response;
                }
                if(txnExistsTask.Result != null && txnExistsTask.Result.Any())
                {
                    _logger.LogError($"CreateTransactionAfterLoanInsertSuccessByLoanIDHandler|value={_common.ConvertObjectToJSonV2(value)}|Transaction_Exists");
                    return response;
                }
                var loanInfo = loanInfos.First();
                Commands.CreateAfterPaymentCommand request = new Commands.CreateAfterPaymentCommand
                {
                    LstRequest = new List<Entites.Dtos.TransactionServices.RequestCreateListTransactionModel>
                    {
                        new Entites.Dtos.TransactionServices.RequestCreateListTransactionModel
                        {
                            ActionID = (int)Transaction_Action.ChoVay,
                            LoanID = loanInfo.LoanID,
                            MoneyType = (int)Transaction_TypeMoney.Original,
                            PaymentScheduleID = 0,
                            TotalMoney =-loanInfo.TotalMoneyDisbursement,
                            UserID = value.CreateBy
                        }
                    }
                };
                response = await _bus.Send(request);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateTransactionAfterLoanInsertSuccessByLoanIDHandler|value={_common.ConvertObjectToJSonV2(value)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
