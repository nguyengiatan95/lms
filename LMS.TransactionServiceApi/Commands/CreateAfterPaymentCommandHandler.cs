﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.TransactionServiceApi.Commands
{
    public class CreateAfterPaymentCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public List<LMS.Entites.Dtos.TransactionServices.RequestCreateListTransactionModel> LstRequest { get; set; }
    }

    public class CreateAfterPaymentCommandHandler : IRequestHandler<CreateAfterPaymentCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblKafkaLogEvent> _kakfaLogEventTab;
        ILogger<CreateAfterPaymentCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateAfterPaymentCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblKafkaLogEvent> kakfaLogEventTab,
            ILogger<CreateAfterPaymentCommandHandler> logger)
        {
            _logger = logger;
            _common = new Common.Helper.Utils();
            _transactionTab = transactionTab;
            _loanTab = loanTab;
            _kakfaLogEventTab = kakfaLogEventTab;
        }

        public async Task<ResponseActionResult> Handle(CreateAfterPaymentCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var currentDate = DateTime.Now;
            try
            {
                var lstLoanID = request.LstRequest.Select(x => x.LoanID).Distinct().ToList();
                var maxTxnIDBeforeTask = _transactionTab.SetGetTop(1).SelectColumns(x => x.TransactionID)
                                                     .OrderByDescending(x => x.TransactionID)
                                                     .QueryAsync();

                var dictLoanInfosTask = _loanTab.SelectColumns(x => x.LoanID, x => x.CustomerID, x => x.LenderID, x => x.OwnerShopID)
                                                   .WhereClause(x => lstLoanID.Contains(x.LoanID))
                                                   .QueryAsync();
                await Task.WhenAll(maxTxnIDBeforeTask, dictLoanInfosTask);

                var dictLoanInfos = dictLoanInfosTask.Result.ToDictionary(x => x.LoanID, x => x);
                List<Domain.Tables.TblTransaction> lstTran = new List<Domain.Tables.TblTransaction>();
                foreach (var item in request.LstRequest)
                {
                    var objLoan = dictLoanInfos.GetValueOrDefault(item.LoanID);
                    Domain.Tables.TblTransaction transaction = new Domain.Tables.TblTransaction()
                    {
                        ActionID = item.ActionID,
                        CreateDate = currentDate,
                        CustomerID = objLoan.CustomerID.Value,
                        LenderID = objLoan.LenderID,
                        LoanID = item.LoanID,
                        MoneyType = item.MoneyType,
                        PaymentScheduleID = item.PaymentScheduleID,
                        TotalMoney = item.TotalMoney,
                        UserID = item.UserID,
                        ShopID = objLoan.OwnerShopID
                    };
                    lstTran.Add(transaction);
                }
                _transactionTab.InsertBulk(lstTran);
                List<Domain.Tables.TblKafkaLogEvent> lstLogEventInsert = new List<Domain.Tables.TblKafkaLogEvent>();
                var maxTxnIDAfter = (await _transactionTab.SetGetTop(1).SelectColumns(x => x.TransactionID)
                                                    .OrderByDescending(x => x.TransactionID)
                                                    .QueryAsync()).First().TransactionID;
                foreach (var item in dictLoanInfos)
                {
                    lstLogEventInsert.Add(new Domain.Tables.TblKafkaLogEvent
                    {
                        CreateDate = DateTime.Now,
                        ModifyDate = DateTime.Now,
                        DataMessage = _common.ConvertObjectToJSonV2(new LMS.Kafka.Messages.Loan.LoanPaymentSuccess
                        {
                            LoanID = item.Key,
                            StartTransactionID = maxTxnIDBeforeTask.Result.FirstOrDefault()?.TransactionID ?? 0,
                            EndTransactionID = maxTxnIDAfter
                        }),
                        TopicName = Kafka.Constants.KafkaTopics.LoanPaymentSuccess,
                        ServiceName = ServiceHostInternal.TransactionService,
                        Status = (int)KafkaLogEvent_Status.Waitting,
                    });
                }
                _kakfaLogEventTab.InsertBulk(lstLogEventInsert);
                response.SetSucces();
                response.Data = 1;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateAfterPaymentCommand_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
