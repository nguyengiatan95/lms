﻿using FrontEnd.Common.Entites.Management;
using FrontEnd.Common.Helpers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEnd.Accountant.Components
{
	public class BreadcrumbViewComponent : ViewComponent
	{
		public async Task<IViewComponentResult> InvokeAsync(string ModuleName)
		{
			ViewBag.ModuleName = ModuleName;
			var model = await HttpContext.Session.GetObjectFromJson<UserDetail>(Constants.STATIC_USERMODEL);
			return View(model.Groups);
		}
	}
}
