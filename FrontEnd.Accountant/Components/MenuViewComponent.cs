﻿using FrontEnd.Common.Entites.Management;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models.Management.User;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEnd.Accountant.Components
{
	public class MenuViewComponent : ViewComponent
	{
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var userData = await HttpContext.Session.GetObjectFromJson<UserInfoWithMenuModel>(Constants.STATIC_USERMODEL);
            return View(userData);
        }
    }
}
