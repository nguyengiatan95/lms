﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Entites.Bank;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models.Bank;
using FrontEnd.Common.Services.Bank;
using LMS.Entites.Dtos.InvoiceService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Accountant.Controllers
{
    public class BankController : BaseController
    {
        private IBankServices _bankServices;
        private IMetaServices _metaServices;
        public BankController(IBankServices bankServices, IMetaServices metaServices, IConfiguration configuration) : base(configuration)
        {
            _bankServices = bankServices;
            _metaServices = metaServices;
        }
        public IActionResult Index()
        {
            var lstBankCard = _metaServices.GetBankCard(GetToken()).Result;
            ViewBag.LstBankCard = lstBankCard;
            return View();
        }
        public async Task<ActionResult> ReportInOutMoneyBankDate()
        {
            var modal = new BankModel(HttpContext.Request.Form);
            var lstReportMoney = new List<ReportInOutMoneyBankDate>();
            try
            {
                var lstLoans = await _bankServices.ReportInOutMoneyBankDate(GetToken(), modal);
                if (lstLoans != null && lstLoans.Data != null)
                {
                    lstReportMoney = lstLoans.Data;
                }
                return Json(new { meta = modal, data = lstReportMoney });
            }
            catch (Exception)
            {
                return Json(new { meta = modal, data = new List<Common.Entites.Accountants.Invoice>() });
            }
        }

        public async Task<ActionResult> GetInvoiceInfosByBankCard()
        {
            var modal = new BankModel(HttpContext.Request.Form);
            var lstInvoiceInfosByBankCard = new List<InvoiceItemViewModel>();
            try
            {
                var lstLoans = await _bankServices.GetInvoiceInfosByBankCard(GetToken(), modal);
                if (lstLoans != null && lstLoans.Data != null)
                {
                    modal.total = lstLoans.Data.RecordsTotal.ToString();
                    lstInvoiceInfosByBankCard = lstLoans.Data.Data;
                }
                return Json(new { meta = modal, data = lstInvoiceInfosByBankCard });
            }
            catch (Exception)
            {
                return Json(new { meta = modal, data = new List<Common.Entites.Accountants.Invoice>() });
            }
        }
        public IActionResult ManagerBankCard()
        {
            var lstBankCard = _metaServices.GetBankCard(GetToken()).Result;
            ViewBag.LstBankCard = lstBankCard;
            return View();
        }

        public async Task<ActionResult> GetListDataBankCard()
        {
            var modal = new ManageBankCardModel(HttpContext.Request.Form);
            var lstData = await _bankServices.GetByConditions(GetToken(), modal);
            modal.total = lstData.RecordsTotal.ToString() ?? "0";
            // Return
            return Json(new { meta = modal, data = lstData.Data });
        }

        [HttpPost]
        public ActionResult AddOrUpDate(FrontEnd.Common.Models.Bank.BankCardReq request)
        { 
            request.UserID = 1;
            if (request.BankID == 0)
            {
                return Json(new { Result = 0, Message = "Ngân hàng bắt buộc phải chọn!" });
            }
            else
            {
                if (request.BankCardID == 0)
                {
                    var response = _bankServices.AddBankCard(request, GetToken());
                    return Json(response);
                }
                else
                {
                    var response = _bankServices.UpdateBankCard(request, GetToken());
                    return Json(response);
                }
            }

            // Return
        }
        public IActionResult SmsManage()
        { 
            return View();
        }
    }
}
