﻿using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models.Management.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.IO;

namespace FrontEnd.Accountant.Controllers
{
    [AuthorizeFilter]
    public class BaseController : Controller
    {
        protected IConfiguration _baseConfig; 
        public BaseController( IConfiguration configuration)
        {
            _baseConfig = configuration; 
        }
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            try
            {
                string cookieValueFromReq = Request.Cookies[Constants.STATIC_USERMODEL];
                var cookie = JsonConvert.DeserializeObject<UserInfoWithMenuModel>(cookieValueFromReq);
                var value = HttpContext.Session.GetObjectFromJson<UserInfoWithMenuModel>(Constants.STATIC_USERMODEL).Result;
                if (cookie != null && value != null && cookie.Token != value.Token && (cookie.TimeExpired - DateTime.Now).TotalMinutes > 0)
                {
                    value.Token = cookie.Token;
                    value.TimeExpired = cookie.TimeExpired;
                    value.CookieName = Constants.STATIC_USERMODEL;
                    HttpContext.Session.SetObjectAsJson(Constants.STATIC_USERMODEL, value);
                }
                else if (cookie == null || value == null || (value.TimeExpired - DateTime.Now).TotalMinutes <= 0 || (cookie.TimeExpired - DateTime.Now).TotalMinutes <= 0)
                {
                    CookieOptions option = new CookieOptions();
                    option.Secure = true;
                    option.Expires = DateTime.Now.AddDays(30);
                    option.Domain = _baseConfig[Constants.STATIC_Cookie];
                    option.Secure = true; 
                    Response.Cookies.Append(Constants.STATIC_USERMODEL, "", option);
                }
            }
            catch (Exception)
            {

            }
        }
        protected string GetToken()
        {
            var token = "";
            try
            {
                var sec = HttpContext.Session.GetObjectFromJson<UserInfoWithMenuModel>(Constants.STATIC_USERMODEL);
                if (sec != null && sec.Result != null && !string.IsNullOrEmpty(sec.Result.Token))
                {
                    token = sec.Result.Token;
                }
            }
            catch (Exception)
            {
                token = "";
            }
            return token;
        }
        protected UserInfoWithMenuModel GetUserGlobal()
        {
            var userLoginModel = new UserInfoWithMenuModel();
            try
            {
                userLoginModel = HttpContext.Session.GetObjectFromJson<UserInfoWithMenuModel>(Constants.STATIC_USERMODEL).Result; 
            }
            catch (Exception)
            {
                userLoginModel = new UserInfoWithMenuModel();
            }
            return userLoginModel;
        }
        public virtual object BaseResult(bool isSuccess = true, string msg = "", object data = null, string html = "")
        {
            return new
            {
                IsSuccess = isSuccess,
                Message = msg,
                Data = data,
                Html = html
            };
        }
        public void SetCookie(string key, string value, int? expireTime)
        {
            CookieOptions option = new CookieOptions();
            if (expireTime.HasValue)
                option.Expires = DateTime.Now.AddMinutes(expireTime.Value);
            else
                option.Expires = DateTime.Now.AddMilliseconds(10);
            Response.Cookies.Append(key, value, option);
        }
    }
}
