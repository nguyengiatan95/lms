﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Entites.Report;
using FrontEnd.Common.Models.Report;
using FrontEnd.Common.Services.Bank;
using FrontEnd.Common.Services.Excel;
using FrontEnd.Common.Services.Insurance;
using FrontEnd.Common.Services.Report;
using LMS.Common.Constants;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace FrontEnd.Accountant.Controllers
{
    public class ExcelController : BaseController
    {
        private readonly ILogger<ExcelController> _logger;
        private IExcelService _excelService;
        private IInsuranceServices _insuranceServices;
        private IReportServices _reportServices;
        private IBankServices _bankServices;
        public ExcelController(IReportServices reportServices, ILogger<ExcelController> logger, IConfiguration configution, IExcelService excelService, IInsuranceServices insuranceServices, IBankServices bankServices) : base(configution)
        {
            _excelService = excelService;
            _logger = logger;
            _insuranceServices = insuranceServices;
            _reportServices = reportServices;
            _bankServices = bankServices;
        }
        public IActionResult Index()
        {
            return View();
        }
        //báo cáo mua bảo hiểm
        public async Task<ActionResult> ExcelInsuranceReport(int LenderID, string FromDate, string ToDate)
        {
            Common.Models.Insurance.InsuranceModel insuranceModel = new Common.Models.Insurance.InsuranceModel()
            {
                LenderID = LenderID,
                Fromdate = FromDate,
                Todate = ToDate,
            };
            var lstReport = await _insuranceServices.GetByConditions(GetToken(), insuranceModel);
            List<Common.Entites.Insurance.ExcelReportInsurance> lstData = new List<Common.Entites.Insurance.ExcelReportInsurance>();
            foreach (var item in lstReport)
            {
                lstData.Add(new Common.Entites.Insurance.ExcelReportInsurance()
                {
                    loanCredit = item.LoanCredit,
                    code = item.Code,
                    customerName = item.CustomerName,
                    disbursementDate = item.DisbursementDate?.ToString("dd/MM/yyyy"),
                    totalMoneyDisbursement = item.TotalMoneyDisbursement.ToString("###,0"),
                    partnerCode = item.PartnerCode,
                    //status = item.Status,
                    lenderName = item.LenderName,
                    moneyInsuranceCustomer = item.MoneyInsuranceCustomer.ToString("###,0"),
                    moneyInsuranceLender = item.MoneyInsuranceLender.ToString("###,0"),
                    moneyInsuranceMaterial = item.MoneyInsuranceMaterial.ToString("###,0"),
                    linkCustomer = item.LinkCustomer,
                    linkLender = item.LinkLender,
                    linkInsuranceMaterial = item.LinkInsuranceMaterial,
                });
            }
            var fileContents = _excelService.ExportExcelBase<Common.Entites.Insurance.ExcelReportInsurance>(lstData);
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: $"Báo cáo mua bảo hiểm-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx"
            );
        }
        //báo cáo vat
        public async Task<ActionResult> ExcelReportVat(string FromDate, string ToDate)
        {
            Common.Models.Report.ReportVatModel reportVatModel = new Common.Models.Report.ReportVatModel()
            {
                FromDate = FromDate,
                ToDate = ToDate,
                PageIndex = 1,
                PageSize = 10000
            };
            var lstReport = await _insuranceServices.GetByConditionsReportVat(GetToken(), reportVatModel);
            List<Common.Entites.Insurance.ExcelReportVat> lstData = new List<Common.Entites.Insurance.ExcelReportVat>();
            foreach (var item in lstReport)
            {
                lstData.Add(new Common.Entites.Insurance.ExcelReportVat()
                {
                    Account = item.Account,
                    BankName = item.BankName,
                    CK = item.CK,
                    CommodityName = item.CommodityName,
                    ContactCode = item.ContactCode,
                    Denominator = item.Denominator,
                    Notation = item.Notation,
                    OrderNumber = item.OrderNumber,
                    TransactionDate = item.TransactionDate?.ToString("dd/MM/yyyy"),
                    CustomerName = item.CustomerName,
                    CustomerID = item.CustomerID,
                    UnitName = item.UnitName,
                    Taxcode = item.Taxcode,
                    Address = item.Address,
                    TypePayment = item.TypePayment,
                    ItemCode = item.ItemCode,
                    DVT = item.DVT,
                    Quantily = item.Quantily,
                    UnitPrice = item.UnitPrice,
                    FeeNotVat = item.FeeNotVat.ToString("###,0"),
                    Discount = item.Discount,
                    TaxPercentage = item.TaxPercentage,
                    Vat = item.Vat.ToString("###,0"),
                    TotalMoney = item.TotalMoney.ToString("###,0")
                });
            }
            var fileContents = _excelService.ExportExcelBase<Common.Entites.Insurance.ExcelReportVat>(lstData);
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: $"Báo cáo VAT-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx"
            );
        }
        //báo cáo phí phạt muộn
        public async Task<ActionResult> ExcelReportFineInterestLate(string FromDate, string ToDate)
        {
            Common.Models.Report.ReportFineInterestLate reportVatModel = new Common.Models.Report.ReportFineInterestLate()
            {
                //LenderID = LenderID,
                FromDate = FromDate,
                ToDate = ToDate,
                PageIndex = 1,
                PageSize = 10000
            };
            var lstReport = await _insuranceServices.GetByConditionsReportFineInterestLate(GetToken(), reportVatModel);
            List<Common.Entites.Insurance.ExcelReportVat> lstData = new List<Common.Entites.Insurance.ExcelReportVat>();
            foreach (var item in lstReport)
            {
                lstData.Add(new Common.Entites.Insurance.ExcelReportVat()
                {
                    Account = item.Account,
                    BankName = item.BankName,
                    CK = item.CK,
                    CommodityName = item.CommodityName,
                    ContactCode = item.ContactCode,
                    Denominator = item.Denominator,
                    Notation = item.Notation,
                    OrderNumber = item.OrderNumber,
                    TransactionDate = item.TransactionDate?.ToString("dd/MM/yyyy"),
                    CustomerName = item.CustomerName,
                    CustomerID = item.CustomerID,
                    UnitName = item.UnitName,
                    Taxcode = item.Taxcode,
                    Address = item.Address,
                    TypePayment = item.TypePayment,
                    ItemCode = item.ItemCode,
                    DVT = item.DVT,
                    Quantily = item.Quantily,
                    UnitPrice = item.UnitPrice,
                    FeeNotVat = item.FeeNotVat.ToString("###,0"),
                    Discount = item.Discount,
                    TaxPercentage = item.TaxPercentage,
                    Vat = item.Vat.ToString("###,0"),
                    TotalMoney = item.TotalMoney.ToString("###,0")
                });
            }
            var fileContents = _excelService.ExportExcelBase<Common.Entites.Insurance.ExcelReportVat>(lstData);
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: $"Báo cáo phí phạt muộn-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx"
            );
        }
        //báo cáo vat hub
        public async Task<ActionResult> ExcelReportVatHub(int HubID, string FromDate, string ToDate)
        {
            Common.Models.Report.ReportVatHub reportVatModel = new Common.Models.Report.ReportVatHub()
            {
                HubID = HubID,
                FromDate = FromDate,
                ToDate = ToDate,
                PageIndex = 1,
                PageSize = 10000
            };
            var lstReport = await _insuranceServices.GetByConditionsReportVatHub(GetToken(), reportVatModel);
            List<Common.Entites.Insurance.ExcelReportVatHub> lstData = new List<Common.Entites.Insurance.ExcelReportVatHub>();
            foreach (var item in lstReport)
            {
                lstData.Add(new Common.Entites.Insurance.ExcelReportVatHub()
                {
                    CommodityName = item.CommodityName,
                    ContactCode = item.ContactCode,
                    Denominator = item.Denominator,
                    Notation = item.Notation,
                    OrderNumber = item.OrderNumber,
                    TransactionDate = item.TransactionDate?.ToString("dd/MM/yyyy"),
                    CustomerName = item.CustomerName,
                    Address = item.Address,
                    TypePayment = item.TypePayment,
                    FeeNotVat = item.FeeNotVat.ToString("###,0"),
                    TaxPercentage = item.TaxPercentage,
                    Vat = item.Vat.ToString("###,0"),
                    TotalMoney = item.TotalMoney.ToString("###,0"),
                    HubName = item.HubName
                });
            }
            var fileContents = _excelService.ExportExcelBase<Common.Entites.Insurance.ExcelReportVatHub>(lstData);
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: $"Báo cáo VAT HUB-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx"
            );
        }
        //báo cáo phí phạt vat hub
        public async Task<ActionResult> ExcelReportPenaltyFeeVatHub(int HubID, string FromDate, string ToDate)
        {
            Common.Models.Report.ReportPenaltyFeeVatHub reportVatModel = new Common.Models.Report.ReportPenaltyFeeVatHub()
            {
                HubID = HubID,
                FromDate = FromDate,
                ToDate = ToDate,
                PageIndex = 1,
                PageSize = 10000
            };
            var lstReport = await _insuranceServices.GetByConditionsReportPenaltyFeeVatHub(GetToken(), reportVatModel);
            List<Common.Entites.Insurance.ExcelReportVatHub> lstData = new List<Common.Entites.Insurance.ExcelReportVatHub>();
            foreach (var item in lstReport)
            {
                lstData.Add(new Common.Entites.Insurance.ExcelReportVatHub()
                {
                    CommodityName = item.CommodityName,
                    ContactCode = item.ContactCode,
                    Denominator = item.Denominator,
                    Notation = item.Notation,
                    OrderNumber = item.OrderNumber,
                    TransactionDate = item.TransactionDate?.ToString("dd/MM/yyyy"),
                    CustomerName = item.CustomerName,
                    Address = item.Address,
                    TypePayment = item.TypePayment,
                    FeeNotVat = item.FeeNotVat.ToString("###,0"),
                    TaxPercentage = item.TaxPercentage,
                    Vat = item.Vat.ToString("###,0"),
                    TotalMoney = item.TotalMoney.ToString("###,0"),
                    HubName = item.HubName
                });
            }
            var fileContents = _excelService.ExportExcelBase<Common.Entites.Insurance.ExcelReportVatHub>(lstData);
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: $"Báo cáo phí phạt VAT HUB-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx"
            );
        }
        //báo cáo vat gcash
        public async Task<ActionResult> ExcelReportVatGCash(int HubID, string FromDate, string ToDate)
        {
            Common.Models.Report.ReportVatGCash reportVatModel = new Common.Models.Report.ReportVatGCash()
            {
                GcashID = HubID,
                FromDate = FromDate,
                ToDate = ToDate,
                PageIndex = 1,
                PageSize = 10000
            };
            var lstReport = await _insuranceServices.GetByConditionsReportVatGCash(GetToken(), reportVatModel);
            List<Common.Entites.Insurance.ExcelReportVatHub> lstData = new List<Common.Entites.Insurance.ExcelReportVatHub>();
            foreach (var item in lstReport)
            {
                lstData.Add(new Common.Entites.Insurance.ExcelReportVatHub()
                {
                    CommodityName = item.CommodityName,
                    ContactCode = item.ContactCode,
                    Denominator = item.Denominator,
                    Notation = item.Notation,
                    OrderNumber = item.OrderNumber,
                    TransactionDate = item.TransactionDate?.ToString("dd/MM/yyyy"),
                    CustomerName = item.CustomerName,
                    Address = item.Address,
                    TypePayment = item.TypePayment,
                    FeeNotVat = item.FeeNotVat.ToString("###,0"),
                    TaxPercentage = item.TaxPercentage,
                    Vat = item.Vat.ToString("###,0"),
                    TotalMoney = item.TotalMoney.ToString("###,0"),
                    HubName = item.HubName
                });
            }
            var fileContents = _excelService.ExportExcelBase<Common.Entites.Insurance.ExcelReportVatHub>(lstData);
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: $"Báo cáo VAT GCash-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx"
            );
        }
        //báo cáo vat lender
        public async Task<ActionResult> ExcelReportLender(int LenderID, string FromDate, string ToDate)
        {
            Common.Models.Report.ReportLender reportVatModel = new Common.Models.Report.ReportLender()
            {
                LenderID = LenderID,
                FromDate = FromDate,
                ToDate = ToDate,
                PageIndex = 1,
                PageSize = 1000000
            };
            var lstReport = await _insuranceServices.GetByConditionsReportLender(GetToken(), reportVatModel);
            List<Common.Entites.Insurance.ExcelReportVatHub> lstData = new List<Common.Entites.Insurance.ExcelReportVatHub>();
            foreach (var item in lstReport)
            {
                lstData.Add(new Common.Entites.Insurance.ExcelReportVatHub()
                {
                    Account = item.Account,
                    BankName = item.BankName,
                    CK = item.CK,
                    CommodityName = item.CommodityName,
                    ContactCode = item.ContactCode,
                    Denominator = item.Denominator,
                    Notation = item.Notation,
                    OrderNumber = item.OrderNumber,
                    TransactionDate = item.TransactionDate?.ToString("dd/MM/yyyy"),
                    CustomerName = item.CustomerName,
                    CustomerID = item.CustomerID,
                    UnitName = item.UnitName,
                    Taxcode = item.Taxcode,
                    Address = item.Address,
                    TypePayment = item.TypePayment,
                    ItemCode = item.ItemCode,
                    DVT = item.DVT,
                    Quantily = item.Quantily,
                    UnitPrice = item.UnitPrice,
                    FeeNotVat = item.FeeNotVat.ToString("###,0"),
                    Discount = item.Discount,
                    TaxPercentage = item.TaxPercentage,
                    Vat = item.Vat.ToString("###,0"),
                    TotalMoney = item.TotalMoney.ToString("###,0")
                });
            }
            var fileContents = _excelService.ExportExcelBase<Common.Entites.Insurance.ExcelReportVatHub>(lstData);
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: $"Báo cáo lENDER-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx"
            );

        }
        //báo cáo vat hub với gcash
        public async Task<ActionResult> ExcelReportVatHUbWithGCash(int HubID, string FromDate, string ToDate)
        {
            var lstData = new List<ReportVatHubItemExcel>();
            try
            {
                var hubname = "All";
                var model = new ReportVatHUbWithGCashQuery()
                {
                    HubID = HubID,
                    FromDate = FromDate,
                    ToDate = ToDate,
                    PageIndex = 1,
                    PageSize = 1000000
                };
                var lstReport = await _reportServices.GetByConditions(GetToken(), model);

                if (lstReport != null && lstReport.Any())
                {
                    if (HubID > 0)
                    {
                        hubname = lstReport.FirstOrDefault().HubName;
                    }
                    foreach (var item in lstReport)
                    {
                        lstData.Add(new ReportVatHubItemExcel()
                        {
                            //HubName = item.HubName,
                            //TypeTransaction = item.TypeTransaction,
                            Denominator = item.Denominator,
                            Notation = item.Notation,
                            OrderNumber = item.OrderNumber,
                            TransactionDate = item.TransactionDate?.ToString("dd/MM/yyyy"),
                            CustomerName = item.CustomerName,
                            CustomerID = item.CustomerID,
                            UnitName = item.UnitName,
                            Taxcode = item.Taxcode,
                            Address = item.Address,
                            Account = item.Account,
                            BankName = item.BankName,
                            ContactCode = item.ContactCode,
                            TypePayment = item.TypePayment,
                            ItemCode = item.ItemCode,
                            CommodityName = item.CommodityName,
                            DVT = item.DVT,
                            Quantily = item.Quantily,
                            UnitPrice = item.UnitPrice,
                            FeeNotVat = item.FeeNotVat.ToString("###,0"),
                            CK = item.CK,
                            Discount = item.Discount,
                            TaxPercentage = item.TaxPercentage,
                            Vat = item.Vat.ToString("###,0"),
                            TotalMoney = item.TotalMoney.ToString("###,0"),

                        });
                    }
                }
                var fileContents = _excelService.ExportExcelBase<ReportVatHubItemExcel>(lstData);
                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"Báo cáo VAT HUB với GCASH -{hubname}-{FromDate}-{ToDate}.xlsx"
                );
            }
            catch (Exception ex)
            {
                return File(
                    fileContents: null,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"Báo cáo VAT HUB với GCASH-{FromDate}-{ToDate}.xlsx"
                );
            }
        }
        public async Task<ActionResult> ExcelBankCard(string FromDate, string ToDate, int BankCardID, int InvoiceType)
        {
            var excelBankCard = new Common.Models.Bank.BankModel()
            {
                FromDate = FromDate,
                ToDate = ToDate,
                BankCardID = BankCardID,
                InvoiceType = InvoiceType,
            };
            var lstLoans = await _bankServices.GetInvoiceInfosByBankCard(GetToken(), excelBankCard);
            var lstData = new List<Common.Entites.Insurance.ExcelBankCard>();

            if (lstLoans.Data != null)
            {
                foreach (var item in lstLoans.Data.Data)
                {
                    var excelDetail = new Common.Entites.Insurance.ExcelBankCard()
                    {
                        InvoiceSubTypeName = item.InvoiceSubTypeName,
                        StatusName = item.StatusName,
                        UserNameCreate = item.UserNameCreate,
                        Note = item.Note,
                        TransactionDate = item.TransactionDate?.ToString("dd-MM-yyyy HH:mm"),
                        BankName = item.BankName,
                        CustomerName = item.SourceName,
                        CreateDate = item.CreateDate?.ToString("dd-MM-yyyy HH:mm")
                    };
                    if (item.TransactionMoney > 0)
                    {
                        excelDetail.MoneyReceipt = item.TransactionMoney.ToString("###,0");
                    }
                    else
                    {
                        excelDetail.MoneyPaySlip = item.TransactionMoney.ToString("###,0");
                    }
                    lstData.Add(excelDetail);
                }
            }

            var fileContents = _excelService.ExportExcelBase<Common.Entites.Insurance.ExcelBankCard>(lstData);
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: $"Báo cáo Tiền vào ra của thẻ-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx"
            );
        }
        //SỔ QUỸ TIỀN MẶT
        public async Task<ActionResult> ExcelReportMoneyDetail(string ContractCode, int ProductID, string ProductName, int LenderID, string FromDate, string ToDate, int PageSize)
        {
            var lstData = new List<MoneyDetailLenderModel>();
            try
            {
                ContractCode = string.IsNullOrEmpty(ContractCode) ? "" : ContractCode.Trim();
                ProductName = ProductID > 0 ? ProductName : "All";
                var LenderName = "All";
                var model = new ReportMoneyDetailLenderQuery()
                {
                    ContractCode = ContractCode,
                    ProductID = ProductID,
                    LenderID = LenderID,
                    FromDate = FromDate,
                    ToDate = ToDate,
                    PageIndex = 1,
                    PageSize = PageSize
                };
                lstData = await _reportServices.ReportMoneyDetail(GetToken(), model);

                if (lstData != null && lstData.Any())
                {
                    if (LenderID > 0)
                    {
                        LenderName = lstData.FirstOrDefault().LenderCode;
                    }
                }
                var fileContents = _excelService.ExportExcelBase<MoneyDetailLenderModel>(lstData);
                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"Sổ quỹ tiền mặt -{ContractCode}-{ProductName}-{LenderName}-{FromDate}-{ToDate}.xlsx"
                );
            }
            catch (Exception)
            {
                return File(
                    fileContents: null,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"Sổ quỹ tiền mặt-{FromDate}-{ToDate}.xlsx"
                );
            }
        }
        //báo cáo vat lender
        public async Task<ActionResult> ExcelMoneyInterestLender(int LenderID, string FromDate, string ToDate)
        {
            Common.Models.Report.ReportMoneyInterestLender reportVatModel = new Common.Models.Report.ReportMoneyInterestLender()
            {
                LenderID = LenderID,
                FromDate = FromDate,
                ToDate = ToDate,
                LstMoneyType = new List<int> { (int)Transaction_TypeMoney.Interest }, 
                PageIndex = 1,
                PageSize = 1000000
            };
            var lstReport = await _insuranceServices.GetByConditionsReportMoneyInterestLender(GetToken(), reportVatModel);
            List<Common.Entites.Insurance.ExcelReportVatHub> lstData = new List<Common.Entites.Insurance.ExcelReportVatHub>();
            foreach (var item in lstReport)
            {
                lstData.Add(new Common.Entites.Insurance.ExcelReportVatHub()
                {
                    Account = item.Account,
                    BankName = item.BankName,
                    CK = item.CK,
                    CommodityName = "Thu lãi tiền vay",
                    ContactCode = item.ContactCode,
                    Denominator = item.Denominator,
                    Notation = item.Notation,
                    OrderNumber = item.OrderNumber,
                    TransactionDate = item.TransactionDate?.ToString("dd/MM/yyyy"),
                    CustomerName = item.CustomerName,
                    CustomerID = item.CustomerID,
                    UnitName = item.UnitName,
                    Taxcode = item.Taxcode,
                    Address = item.Address,
                    TypePayment = item.TypePayment,
                    ItemCode = item.ItemCode,
                    DVT = item.DVT,
                    Quantily = item.Quantily,
                    UnitPrice = item.UnitPrice,
                    FeeNotVat = item.FeeNotVat.ToString("###,0"),
                    Discount = item.Discount,
                    TaxPercentage = 0,
                    Vat = "",
                    TotalMoney = item.TotalMoney.ToString("###,0")
                });
            }
            var fileContents = _excelService.ExportExcelBase<Common.Entites.Insurance.ExcelReportVatHub>(lstData);
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: $"Báo cáo Vat tiền lãi LENDER-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx"
            );

        }
    }
}