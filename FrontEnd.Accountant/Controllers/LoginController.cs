﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FrontEnd.Common.Core.Security.Crypt;
using FrontEnd.Common.Entites.Management;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models.Management.Login;
using FrontEnd.Common.Models.Management.User;
using FrontEnd.Common.Services.Login;
using FrontEnd.Common.Services.User;
using LMS.Common.Constants;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace FrontEnd.Accountant.Controllers
{ 
    public class LoginController : BaseController
    {
        // GET: LoginController 
        private ILoginService _loginService;
        private IUserService _user;
        public LoginController(IConfiguration configuration, ILoginService loginService, IUserService user) : base(configuration)
        {
            _loginService = loginService;
            _user = user; 
        }
        public ActionResult Index()
        {
            return View();
        }
        [Route("logout.html")]
        public IActionResult Logout()
        {
            HttpContext.Session.SetObjectAsJson(Constants.STATIC_USERMODEL, null); 
            Response.Cookies.Delete(Constants.STATIC_USERMODEL); 
            return Redirect(_baseConfig[Constants.STATIC_LogOut_Admin_URL]);
        }
        [Route("redirect")]
        [HttpGet]
        public async Task<IActionResult> RedirectIndex(string token, string referrer)
        {
            try
            {
                //Validate Token
                if (string.IsNullOrEmpty(token))
                    return Redirect(_baseConfig[Constants.STATIC_Login_URL]);
                var user = await _user.GetUserByUserID(token, 1);
                if (user == null || user.UserID == 0)
                    return Redirect(_baseConfig[Constants.STATIC_Login_URL]);
                var userLoginModel = new UserLoginModel { UserID = user.UserID, UserName = user.UserName, FullName = user.FullName, Token = token };
                userLoginModel.FullName = user.FullName;
                userLoginModel.UserName = user.UserName;
                userLoginModel.ApiUrl = _baseConfig[Constants.STATIC_ApiUrl_URL];
                userLoginModel.LoginUrl = _baseConfig[Constants.STATIC_Login_URL];
                userLoginModel.AdminUrl = _baseConfig[Constants.STATIC_HOME_URL];

                // gọi API lấy menu
                var userModel = new GetUserInfoWithMenuQuery { AppID = (int)Menu_AppID.Accountant };
                var responce = _loginService.GetUserInfoWithMenu(userModel, token).Result;
                if (responce != null && responce.Data != null && responce.Data.UserID > 0)
                {
                    // check điều hướng Referrer
                    responce.Data.Token = token;
                    HttpContext.Session.SetObjectAsJson(Constants.STATIC_USERMODEL, responce.Data);
                    SetCookie(Constants.STATIC_USERMODEL, JsonConvert.SerializeObject(userLoginModel), 24);
                    if (!string.IsNullOrEmpty(referrer))
                    {
                        return Redirect(referrer);
                    }
                    else
                    {
                        return Redirect(Constants.STATIC_HOME_ACC); 
                    }
                }
                return Redirect(_baseConfig[Constants.STATIC_Login_URL]);
            }
            catch (Exception ex)
            {
                
            }
            return Redirect(_baseConfig[Constants.STATIC_Login_URL]);
        } 
        //public async Task<IActionResult> RedirectIndex( string token)
        //{
        //    try
        //    {
        //        //Validate Token 
        //        ThongBaoViaTelegram(-461400009, "LMS  Accountant redirect Cookies" + Request.Cookies[Constants.STATIC_USERMODEL]);
        //        if (Request.Cookies[Constants.STATIC_USERMODEL] == null)
        //                return Redirect(_baseConfig[Constants.STATIC_Login_URL]);
        //        var value = Request.Cookies[Constants.STATIC_USERMODEL];
        //        var userLoginModel = JsonConvert.DeserializeObject<UserLoginModel>(value);
        //        if (userLoginModel == null || String.IsNullOrEmpty(userLoginModel.Token))
        //            return Redirect(_baseConfig[Constants.STATIC_Login_URL]);
        //        var user = await _user.GetUserByUserID(userLoginModel.Token, 1);
        //        if (user == null || user.UserID == 0)
        //            return Redirect(_baseConfig[Constants.STATIC_Login_URL]);
        //        userLoginModel.FullName = user.FullName;
        //        userLoginModel.UserName = user.UserName;
        //        userLoginModel.ApiUrl = _baseConfig[Constants.STATIC_ApiUrl_URL];
        //        userLoginModel.LoginUrl = _baseConfig[Constants.STATIC_Login_URL];
        //        HttpContext.Session.SetObjectAsJson(Constants.STATIC_USERMODEL, userLoginModel); 
        //        SetCookie(Constants.STATIC_USERMODEL, JsonConvert.SerializeObject(userLoginModel), 12);
        //        return Redirect("/Invoice/Index"); 
        //    }
        //    catch (Exception ex)
        //    {
        //        var thongBao = ThongBaoViaTelegram(-461400009, "LMS  Accountant redirect Exception" + ex.Message);
        //        return Redirect(_baseConfig[Constants.STATIC_Login_URL]);
        //    }
        //}
        public void SetCookie(string key, string value, int? expireTime)
        {
            CookieOptions option = new CookieOptions();
            if (expireTime.HasValue)
                option.Expires = DateTime.Now.AddHours(expireTime.Value);
            else
                option.Expires = DateTime.Now.AddHours(24);
            Response.Cookies.Append(key, value, option);
        }
        public string ThongBaoViaTelegram(int groupId, string text)
        {
            try
            {
                if (!string.IsNullOrEmpty(text))
                {
                    using (var client = new HttpClient())
                    {
                        var response = client.GetAsync("https://api.telegram.org/bot1289672837:AAFjY383npbd-3_a8ERsH13QFS8MBaw9LZs/sendMessage?chat_id=" + groupId + "&text=" + text).Result;
                        return response.Content.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return text;
        }
    }
}
