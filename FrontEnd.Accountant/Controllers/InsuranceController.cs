﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models.Insurance;
using FrontEnd.Common.Services.Insurance;
using FrontEnd.Common.Services.Lender;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Accountant.Controllers
{
    public class InsuranceController : BaseController
    {
        private IInsuranceServices _insuranceServices;
        private ILenderService _lenderService;

        public InsuranceController(IInsuranceServices insuranceServices, ILenderService lenderService, IConfiguration configuration) : base(configuration)
        {
            _insuranceServices = insuranceServices;
            _lenderService = lenderService;
        }
        public async Task<IActionResult> Index()
        { 
            return View();
        }
        public async Task<ActionResult> GetListDataInsurance()
        {
            var modal = new InsuranceModel(HttpContext.Request.Form);
            var lstData = await _insuranceServices.GetByConditions(GetToken(), modal);
            modal.total = lstData != null && lstData.Any() ? lstData[0].TotalCount.ToString() : "0";
            // Return
            return Json(new { meta = modal, data = lstData });
        }
    }
}