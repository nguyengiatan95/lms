﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Services.Lender;
using FrontEnd.Common.Services.Shop;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Accountant.Controllers
{
    public class CommonController : BaseController
    {
        private IShopService _shopService;
        private ILenderService _lenderService;
        public CommonController(IConfiguration configuration, IShopService shopService, ILenderService lenderService)
            : base(configuration)
        {
            _shopService = shopService;
            _lenderService = lenderService;
        }
        
        public async Task<ActionResult> GetShops()
        { 
            var listShop = await _shopService.GetShops(GetToken());  
            return Json(new {data = listShop });
        }
        public async Task<ActionResult> GetLendersAndShop(int type)
        {
            var listLender = await _lenderService.GetLendersAndShop(GetToken(),type);
            return Json(new { data = listLender });
        }
    }
}
