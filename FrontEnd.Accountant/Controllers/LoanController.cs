﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Models.Loan;
using FrontEnd.Common.Models.Management.User;
using FrontEnd.Common.Services.Lender;
using FrontEnd.Common.Services.Loan;
using LMS.Entites.Dtos.LoanServices;
using LMS.Entites.Dtos.ReportServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Accountant.Controllers
{
    public class LoanController : BaseController
    {
        private ILoanServices _iloanServices;
        private ICompositeViewEngine _viewEngine;
        private ILenderService _lenderService;
        public LoanController(ILoanServices iloanServices, IConfiguration configuration, ICompositeViewEngine viewEngine, ILenderService lenderService) : base(configuration)
        {
            _iloanServices = iloanServices;
            _viewEngine = viewEngine;
            _lenderService = lenderService;
        }
        public async Task<IActionResult> Index()
        {
            //var lstShop = await _lenderService.GetLendersAndShop(GetToken(), (int)LenderAndShop.Shop);
            //var listLender = await _lenderService.GetLendersAndShop(GetToken(), (int)LenderAndShop.Lender);
            //ViewBag.ListShop = lstShop ?? new List<LMS.Entites.Dtos.LenderService.LenderSearchModel>(); ;
            //ViewBag.ListLender = listLender ?? new List<LMS.Entites.Dtos.LenderService.LenderSearchModel>(); ;
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> GetLstLoanInfoByCondition()//RequestLoanItemViewModel modal
        {
            var modal = new LoanDatatableModel(HttpContext.Request.Form);
            var res = new ResponseActionResult<KTDatatableResult>();
            var listLoan = new List<LoanItemViewModel>();
            try
            {
                var lstLoans = await _iloanServices.GetLstLoanInfoByCondition(GetToken(), modal);
                if (lstLoans.Result == 1)
                {
                    modal.total = lstLoans.Data.RecordsTotal.ToString();
                    listLoan = lstLoans.Data.Data;
                }
                return Json(new { meta = modal, data = listLoan });
            }
            catch (Exception)
            {
                return Json(new { meta = modal, data = res });
            }
        }
        [HttpPost]
        public async Task<ActionResult> Getstasticloanstatusbyshop()
        {
            var modal = new RequestReport { ShopID = 3703 };
            var res = new ReportStasticsLoanBorrowModel();
            var listLoan = new List<LoanItemViewModel>();
            try
            {
                var lstLoans = await _iloanServices.Getstasticloanstatusbyshop(GetToken(), modal);
                if (lstLoans.Result == 1)
                {
                    res = lstLoans.Data;
                }
                return Json(new { meta = modal, data = res });
            }
            catch (Exception)
            {
                return Json(new { meta = modal, data = res });
            }
        }
        public async Task<ActionResult> GetLstPaymentScheduleByLoanID(int loanID)
        {
            var metaData = new DatatableBase();
            try
            {
                var lstLoans = await _iloanServices.GetLstPaymentScheduleByLoanID(GetToken(), loanID);
                if (lstLoans.Data != null && lstLoans.Data.Count > 0)
                {
                    int record = 1;
                    foreach (var i in lstLoans.Data.OrderBy(x => x.PayDate))
                    {
                        i.RecordID = record;
                        record++;
                    }
                }
                return Json(new { meta = metaData, data = lstLoans.Data });
            }
            catch (Exception)
            {
                return Json(new { meta = "" });
            }
        }
        public async Task<ActionResult> GetLoanByID(int loanID)
        {
            try
            {
                var lstLoans = await _iloanServices.GetLoanByID(GetToken(), loanID);
                return Json(lstLoans);
            }
            catch (Exception)
            {
                return Json(new { meta = loanID });
            }
        }

        #region Detail
        public IActionResult Detail(int id = 0)
        {
            var user = GetUserGlobal();
            ViewBag.User = user;
            return Json(BaseResult(true, "Success", null, RenderViewAsString(user, "Detail")));
        }
        #endregion
        public string RenderViewAsString(object model, string viewName = null)
        {
            viewName = viewName ?? ControllerContext.ActionDescriptor.ActionName;
            ViewData.Model = model;
            using (StringWriter sw = new StringWriter())
            {
                IView view = _viewEngine.FindView(ControllerContext, viewName, true).View;
                ViewContext viewContext = new ViewContext(ControllerContext, view, ViewData, TempData, sw, new HtmlHelperOptions());
                view.RenderAsync(viewContext).Wait();
                return sw.GetStringBuilder().ToString();
            }
        }

        [HttpPost]
        public async Task<ActionResult> DeleteLoan(long loanID)
        {
            var response = new ResponseActionResult();
            try
            {
                var objUser = GetUserGlobal();
                response = await _iloanServices.Delete(GetToken(), loanID, objUser.UserID);
                return Json(new { response = response });
            }
            catch (Exception ex)
            {
                response.Result = (int)LMS.Common.Constants.ResponseAction.Error;
                response.Message = LMS.Common.Constants.MessageConstant.ErrorInternal;
                return Json(new { response = response });
            }
        }
    }
}
