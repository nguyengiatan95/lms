﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Models.Loan;
using FrontEnd.Common.Services.Lender;
using FrontEnd.Common.Services.Loan;
using LMS.Entites.Dtos.LoanServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Accountant.Controllers
{
    public class ReportController : BaseController
    {
        private ILoanServices _iloanServices;
        private ICompositeViewEngine _viewEngine;
        private ILenderService _lenderService;
        public ReportController(ILoanServices iloanServices, IConfiguration configuration, ICompositeViewEngine viewEngine, ILenderService lenderService) : base(configuration)
        {
            _iloanServices = iloanServices;
            _viewEngine = viewEngine;
            _lenderService = lenderService;
        }
        public async Task<IActionResult> Index()
        {
            var lstShop = await _lenderService.GetLendersAndShop(GetToken(), (int)LenderAndShop.Shop);
            var listLender = await _lenderService.GetLendersAndShop(GetToken(), (int)LenderAndShop.Lender);
            ViewBag.ListShop = lstShop ?? new List<LMS.Entites.Dtos.LenderService.LenderSearchModel>();  
            ViewBag.ListLender = listLender ?? new List<LMS.Entites.Dtos.LenderService.LenderSearchModel>();  
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> GetLstLoanInfoByCondition()//RequestLoanItemViewModel modal
        {
            var modal = new LoanDatatableModel(HttpContext.Request.Form);
            var res = new ResponseActionResult<KTDatatableResult>();
            var listLoan = new List<LoanItemViewModel>();
            try
            {
                var lstLoans = await _iloanServices.GetLstLoanInfoByCondition(GetToken(), modal);
                if (lstLoans.Result == 1)
                {
                    modal.total = lstLoans.Data.RecordsTotal.ToString();
                    listLoan = lstLoans.Data.Data;
                }
                return Json(new { meta = modal, data = listLoan });
            }
            catch (Exception)
            {
                return Json(new { meta = modal, data = res });
            }
        }
        public IActionResult ReportVatHub()
        { 
            return View();
        } 
        public IActionResult ReportVat()
        {
            return View();
        } 
        public IActionResult ReportFineInterestLate()
        {
            return View();
        } 
        public IActionResult ReportVatLender()
        {
            return View();
        }
        public IActionResult ReportPenaltyFeeVatHub()
        {
            return View();
        }
        public IActionResult ReportVatGCash()
        {
            return View();
        }
        public IActionResult ReportMoneyDetail()
        {
            return View();
        }
        public IActionResult ReportVatHUbWithGCash()
        {
            return View();
        }
        public IActionResult ReportExtraMoneyCustomer()
        {
            return View();
        }
        public IActionResult ReportMoneyInterest()
        {
            return View();
        }
    }
}