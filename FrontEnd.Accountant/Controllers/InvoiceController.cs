﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models.Invoice;
using FrontEnd.Common.Services.Bank;
using FrontEnd.Common.Services.Invoice;
using FrontEnd.Common.Services.Lender; 
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Accountant.Controllers
{
    public class InvoiceController : BaseController
    {
        private IInvoiceServices _invoiceServices;
        private ILenderService _lenderService;
        private IMetaServices _metaServices;
        public InvoiceController(IMetaServices metaServices,IInvoiceServices invoiceServices, IConfiguration configuration, ILenderService lenderService) : base(configuration)
        {
            _invoiceServices = invoiceServices;
            _lenderService = lenderService;
            _metaServices = metaServices;
        }
        public IActionResult Index()
        { 
            var listBankCard = _metaServices.GetBankCard(GetToken()).Result;
            ViewBag.ListBankCard = listBankCard ?? new List<Common.Entites.SelectItem>();
            return View();
        } 
        public async Task<ActionResult> GetLstInvoiceInfoByCondition()
        {
            var modal = new InvoiceDatatableModel(HttpContext.Request.Form);
            var lstInvoice = new List<LMS.Entites.Dtos.InvoiceService.InvoiceItemViewModel>();
            try
            {
                var lstLoans = await _invoiceServices.GetInvoiceInfosByCondition(GetToken(), modal);
                if(lstLoans!=null && lstLoans.Data != null)
                {
                    modal.total = lstLoans.Data.RecordsTotal.ToString();
                    lstInvoice = lstLoans.Data.Data;
                }     
                return Json(new { meta = modal, data = lstInvoice });
            }
            catch (Exception)
            {
                return Json(new { meta = modal, data = new List<Common.Entites.Accountants.Invoice>() });
            }
        }
    }
}
