﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Accountant.Controllers
{
    public class DisbursementController : BaseController
    {
        private ICompositeViewEngine _viewEngine;

        public DisbursementController(IConfiguration configuration ,ICompositeViewEngine viewEngine) : base(configuration)
        {
            _viewEngine = viewEngine;
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }
        public IActionResult Detail(int id = 0)
        {
            var user = GetUserGlobal();
            ViewBag.User = user;
            return Json(BaseResult(true, "Success", null, RenderViewAsString(user, "Detail")));
        }
        public string RenderViewAsString(object model, string viewName = null)
        {
            viewName = viewName ?? ControllerContext.ActionDescriptor.ActionName;
            ViewData.Model = model;
            using (StringWriter sw = new StringWriter())
            {
                IView view = _viewEngine.FindView(ControllerContext, viewName, true).View;
                ViewContext viewContext = new ViewContext(ControllerContext, view, ViewData, TempData, sw, new HtmlHelperOptions());
                view.RenderAsync(viewContext).Wait();
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}