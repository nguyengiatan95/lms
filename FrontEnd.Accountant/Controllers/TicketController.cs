﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Models.Ticket;
using FrontEnd.Common.Services.Ticket;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Accountant.Controllers
{
    public class TicketController : BaseController
    {
        private ITicketServices _ticketServices;
        public TicketController(IConfiguration configuration, ITicketServices ticketServices) : base(configuration)
        {
            _ticketServices = ticketServices;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> GetTicket()
        {
            var modal = new TicketModel(HttpContext.Request.Form);
            var lstInvoice = new List<LMS.Entites.Dtos.InvoiceService.TicketListView>();
            try
            {
                modal.DepartmentID = (int)LMS.Common.Constants.Department_ID.AccountTant;
                var lstTicket = await _ticketServices.GetTicketConditions(GetToken(), modal);
                if (lstTicket != null && lstTicket.Data != null)
                {
                    modal.total = lstTicket.Data.RecordsTotal.ToString();
                    lstInvoice = lstTicket.Data.Data;
                }
                return Json(new { meta = modal, data = lstInvoice });
            }
            catch (Exception)
            {
                return Json(new { meta = modal, data = new List<Common.Entites.Accountants.Invoice>() });
            }
        }
    }
}