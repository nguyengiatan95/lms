﻿var notify = function () {
    var loadCountNotify = function (departmentID, className, idShowToggle, strType) {
        $('.' + className).html('');
        baseCommon.AjaxDone("GET",`${baseCommon.Endpoint.CountNotifyTicket}?DepartmentID=${departmentID}`,"", function (respone) {
            if (respone.Result == 1) {
                $('.' + className).text(respone.Data);
                $('#' + idShowToggle).attr('data-original-title', 'Có <code><b>' + respone.Data + '</b> ' + strType + '</code> cần xử lý');
            }
        }, "Chưa lấy được dữ liệu!");

    };
    return {
        loadCountNotify: loadCountNotify
    }
}();