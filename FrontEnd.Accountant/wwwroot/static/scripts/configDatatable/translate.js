﻿var translate = {
    records: {
        processing: 'Đang tải dữ liệu...',
        noRecords: 'Không có dữ liệu'
    },
    toolbar: {
        pagination: {
            items: {
                default: {
                    first: 'Về trang đầu',
                    prev: 'Trang trước',
                    next: 'Trang sau',
                    last: 'Đến trang cuối',
                },
                info: 'Tống số {{total}} bản ghi'
            }
        }
    }
}