﻿var datatable;
var insurranceList = new function () {

    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var lenderID = $('#sl_search_lenderID').val();
                if (typeof lenderID == undefined || lenderID == null || lenderID == "") {
                    lenderID = -1;
                }
                var FromDate = $('#fromDate').val();
                var formatFromdate = moment(FromDate, "DD/MM/YYYY").format('DD-MM-YYYY');
                if (FromDate != null && FromDate != "" && FromDate.trim() != "") {
                    FromDate = formatFromdate;
                }
                var ToDate = $('#toDate').val();
                var formatToDate = moment(ToDate, "DD/MM/YYYY").format('DD-MM-YYYY');
                if (ToDate != null && ToDate != "" && ToDate.trim() != "") {
                    ToDate = formatToDate;
                }
                var data = {
                    LenderID: parseInt(lenderID),
                    Fromdate: FromDate,
                    Todate: ToDate,
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                }
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.ReportInsurance, JSON.stringify(data), function (res) {
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    options.success(res);
                    if (res.Result == 1) {
                    } else {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                if (response != null && response.Total != null) {
                    return response.Total;
                }
                return 0; // total is returned in the "total" field of the response 
            },
            data: function (response) {
                var data = [];
                if (response != null && response.Data != null) {
                    data = response.Data;
                }
                return data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function () {
        $("#dv_result_insurrance").kendoGrid({
            dataSource: dataSource,
            selectable: "row",
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 5) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'LoanCredit',
                    title: 'Mã HĐ đăng ký',
                    textAlign: 'left',
                    template: function (row) {
                        var html = "";
                        html = `<span style="color:blue">${row.LoanCredit}</span>`;
                        return html;
                    }
                },
                {
                    field: 'Code',
                    title: 'Mã HĐ giải ngân',
                    textAlign: 'left',
                    template: function (row) {
                        var html = "", param = {}; 
                        param[baseCommon.LoanIndex.search] = row.Code;
                        html = `<a  target="_blank" class="kt-link kt-link--state kt-link--primary" href="${baseCommon.StringFormat(baseCommon.Endpoint.LoanIndex, param)}">${row.Code}</a>`;
                        return html;
                    }
                },
                {
                    field: 'CustomerName',
                    title: 'Tên KH',
                    textAlign: 'left',
                    template: function (row) {
                        var html = "";
                        if (row.CustomerName == null) {
                            return html;
                        }
                        html = `<span>${row.CustomerName}</span>`;
                        return html;
                    }
                },
                {
                    field: 'DisbursementDate',
                    title: 'Ngày giải ngân',
                    textAlign: 'center',
                    template: function (row) {
                        if (row == 0 || row == null || row.DisbursementDate == null || row.DisbursementDate == "0001-01-01T00:00:00") {
                            return "";
                        }
                        var date = moment(row.DisbursementDate);
                        return date.format("DD/MM/YYYY");
                    }
                },
                {
                    field: 'TotalMoneyDisbursement',
                    title: 'Số tiền giải ngân',
                    textAlign: 'right',
                    template: function (row) {
                        if (row == 0 || row == null || row.TotalMoneyDisbursement == null) {
                            return "";
                        }
                        return html_money(row.TotalMoneyDisbursement);
                    }
                },
                {
                    field: 'LenderName',
                    title: 'Đối tác cho vay',
                    textAlign: 'center',
                    template: function (row) {
                        var html = "";
                        if (row.LenderName == null) {
                            return html;
                        }
                        html = `<span>${row.LenderName}</span>`;
                        return html;
                    }
                },
                {
                    field: 'PartnerCode',
                    title: 'Loại BH',
                    textAlign: 'center',
                    width: 100,
                },
                {
                    field: 'MoneyInsuranceCustomer',
                    title: 'Phí BH của KH',
                    width: 100,
                    headerAttributes: { style: "text-align: right" },
                    attributes: { style: "text-align:right;" },
                    template: function (row) {
                        if (row == 0 || row == null || row.MoneyInsuranceCustomer == null) {
                            return "";
                        }
                        return html_money(row.MoneyInsuranceCustomer);
                    }
                },
                {
                    field: 'MoneyInsuranceLender',
                    title: 'Phí BH của Lender',
                    width: 120,
                    headerAttributes: { style: "text-align: right" },
                    attributes: { style: "text-align:right;" },
                    template: function (row) {
                        if (row == 0 || row == null || row.MoneyInsuranceLender == null) {
                            return "";
                        }
                        return html_money(row.MoneyInsuranceLender);
                    }
                },
                {
                    field: 'MoneyInsuranceMaterial',
                    title: 'Phí BH vật chất',
                    width: 120,
                    headerAttributes: { style: "text-align: right" },
                    attributes: { style: "text-align:right;" },
                    template: function (row) {
                        if (row == 0 || row == null || row.MoneyInsuranceMaterial == null) {
                            return "";
                        }
                        return html_money(row.MoneyInsuranceMaterial);
                    }
                },
               
                {
                    field: 'LinkCustomer',
                    title: 'Link KH',
                    attributes: { style: "text-align:center;" },
                    headerAttributes: { style: "text-align: center" },
                    template: function (row) {
                        var date = moment(row.DateOfPurchaseCustomer).format("DD/MM/YYYY");
                        if (date == '01/01/0001' && row.LinkCustomer == '') {
                            return ''
                        }
                        var html = `<a style="color:blue" target="_blank" href="${row.LinkCustomer}">Link\
                                    <br/> <span class="item-desciption">Ngày mua:${date} </span>\
                                    </a>`;
                        return html;
                    }
                },
                {
                    field: 'LinkLender',
                    title: 'Link Lender',
                    attributes: { style: "text-align:center;" },
                    headerAttributes: { style: "text-align: center" },
                    template: function (row) {
                        var date = moment(row.DateOfPurchaseLender).format("DD/MM/YYYY");
                        if (date == '01/01/0001' && row.LinkLender == '') {
                            return ''
                        }
                        var html = `<a style="color:blue" target="_blank" href="${row.LinkLender}">Link\
                                    <br/> <span class="item-desciption">Ngày mua:${date} </span>\
                                    </a>`;
                        return html;
                    }
                },
                {
                    field: 'LinkInsuranceMaterial',
                    title: 'Link BH vật chất',
                    attributes: { style: "text-align:center;" },
                    template: function (row) {
                        var date = moment(row.DateOfPurchaseMaterial).format("DD/MM/YYYY");
                        if (date == '01/01/0001' && row.LinkInsuranceMaterial == '') {
                            return ''
                        }
                        var html = `<a style="color:blue" target="_blank" href="${row.LinkInsuranceMaterial}">Link\
                                    <br/> <span class="item-desciption">Ngày mua:${date} </span>\
                                    </a>`;
                        return html;
                    }
                },
            ]
        });
    };
    var GridRefresh = function () {
        baseCommon.ButtonSubmit(false, '#btnGetData');
        var grid = $("#grid").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };
    var Init = function () {
        GridRefresh();
        $("#btnGetData").on('click', function (e) {
            GridRefresh();
        });
        $("#fromDate,#toDate,#sl_search_lenderID").on('change', function (e) {
            GridRefresh();
        });
        $("#sl_search_lenderID").select2({
            placeholder: "Tất cả",
            allowClear: true,
            ajax: {
                url: baseCommon.Endpoint.GetLenderSearch,
                type: 'get',
                headers: {
                    'Authorization': baseCommon.GetToken()
                },
                dataType: 'json',
                delay: 250,
                data: function (params) {

                    return {
                        generalSearch: params.term, // search term
                        type: 0
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    if (data != null && typeof data != "undefined" && data.Result == 1) {
                        return {
                            results: repoConvert(data.Data),
                            pagination: {
                                more: (params.page * 30) < data.Total
                            }
                        };
                    }
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 3,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
    };
    var ExcelData = function () {
        var FromDate = $('#fromDate').val();
        var formatFromdate = moment(FromDate, "DD/MM/YYYY").format('DD-MM-YYYY');
        if (FromDate != null && FromDate != "" && FromDate.trim() != "") {
            FromDate = formatFromdate;
        }

        var ToDate = $('#toDate').val();
        var formatToDate = moment(ToDate, "DD/MM/YYYY").format('DD-MM-YYYY');
        if (ToDate != null && ToDate != "" && ToDate.trim() != "") {
            ToDate = formatToDate;
        }
        var lenderID = parseInt($('#sl_search_lenderID').val());
        if (typeof lenderID == undefined || lenderID == null || lenderID == "") {
            lenderID = -1;
        }
        window.location.href = "/Excel/ExcelInsuranceReport?LenderID=" + lenderID + "&FromDate=" + FromDate + "&ToDate=" + ToDate;
    };
    function formatRepo(repo) {

        if (repo.loading) return repo.text;
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__title'>" + repo.text + "</div>";
        return markup;
    }
    function formatRepoSelection(repo) {

        return repo.text;
    }
    function repoConvert(data) {
        var newObject = [];
        if (data != null && data != "") {
            $.each(data, function (key, value) {
                var newRepo = { id: value.ID, text: value.FullName };
                newObject.push(newRepo);
            });
        }
        return newObject;
    }
    return {
        Init: Init,
        GridRefresh: GridRefresh,
        ExcelData: ExcelData,
    };
}
$(document).ready(function () {
    insurranceList.Init();
});

