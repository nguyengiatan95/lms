﻿var recordPreviewInvoice = 0;
var createInvoice = new function () {

    var customerID, loanID, shopID, toShopID, invoiceTimaID;
    var SearchInvoiceAdvanceSlipLenderGrid = function () {
        $("#SearchInvoiceAdvanceSlipLender").kendoGrid({
            dataSource: {
                data: [],
                aggregate: [
                    { field: "MoneyAdvance", aggregate: "sum" },
                    { field: "MoneyUnrefunded", aggregate: "sum" },
                    { field: "MoneyRefunded", aggregate: "sum" },
                    { field: "MoneyControl", aggregate: "sum" }
                ]
            },
            selectable: "row",
            columns: [
                {
                    field: "LenderName",
                    title: "Họ và tên"
                },
                {
                    field: "TransactionDate",
                    title: "Ngày giao dịch",
                    width: 140,
                    template: '#= baseCommon.FormatDate(TransactionDate,\'DD/MM/YYYY\')#',
                },
                {
                    field: "MoneyAdvance",
                    title: "Số tiền tạm ứng",
                    template: '#= html_money(MoneyAdvance)#',
                    attributes: { style: "text-align:right;" },
                    footerTemplate: "<div style='text-align:right'>Tổng: #= html_money(sum) #</div> "
                },
                {
                    field: "MoneyRefunded",
                    title: "Số tiền đã hoàn ứng",
                    template: '#= html_money(MoneyRefunded)#',
                    attributes: { style: "text-align:right;" },
                    footerTemplate: "<div style='text-align:right'>#= html_money(sum) #</div> "
                },
                {
                    field: "MoneyUnrefunded",
                    title: "Số tiền chưa hoàn ứng",
                    template: '#= html_money(MoneyUnrefunded)#',
                    attributes: { style: "text-align:right;" },
                    footerTemplate: "<div style='text-align:right'>#= html_money(sum) #</div> "
                },
                {
                    field: "MoneyControl",
                    title: "Phân bổ tiền",
                    template: '#= html_money(MoneyControl)#',
                    attributes: { style: "text-align:right;" },
                    footerTemplate: "<div style='text-align:right'>#= html_money(sum) #</div> "
                }
            ]
        });
    }
    var SearchInvoiceAdvanceSlipLender = function () {
        try {
            var dataSource = new kendo.data.DataSource({
                data: [],
                aggregate: [
                    { field: "MoneyAdvance", aggregate: "sum" },
                    { field: "MoneyUnrefunded", aggregate: "sum" },
                    { field: "MoneyRefunded", aggregate: "sum" },
                    { field: "MoneyControl", aggregate: "sum" }
                ]
            });
            var grid = $("#SearchInvoiceAdvanceSlipLender").data("kendoGrid");
            grid.setDataSource(dataSource);
            var lenderID = $('#Invoice_LenderID').val();
            if (lenderID == null || lenderID == "" || lenderID.trim() == "") {
                baseCommon.ShowErrorNotLoad("Bạn chưa chọn lender!");
                return;
            }
            var model = {
                "LenderID": parseFloat(lenderID)
            };
            baseCommon.AjaxDone("POST", baseCommon.Endpoint.SearchInvoiceAdvanceSlipLender, JSON.stringify(model), function (respone) {
                if (respone.Result == 1) {
                    if (respone.Data != null && respone.Data != null && respone.Data.length > 0) {
                        var dataSource = new kendo.data.DataSource({
                            data: respone.Data,
                            aggregate: [
                                { field: "MoneyAdvance", aggregate: "sum" },
                                { field: "MoneyUnrefunded", aggregate: "sum" },
                                { field: "MoneyRefunded", aggregate: "sum" },
                                { field: "MoneyControl", aggregate: "sum" }
                            ]
                        });
                        var grid = $("#SearchInvoiceAdvanceSlipLender").data("kendoGrid");
                        grid.setDataSource(dataSource);
                    } else {
                        baseCommon.ShowErrorNotLoad("Không tìm thấy thông tin phù hợp!");
                    }
                } else {
                    baseCommon.ShowErrorNotLoad(respone.Message);
                }
            });
        } catch (err) {
            baseCommon.ShowErrorNotLoad("Lấy dữ liệu không thành công vui lòng thử lại!");
        }
    }
    var LenderMoney = function (money) {
        try {
            var grid = $("#SearchInvoiceAdvanceSlipLender").data("kendoGrid");
            var data = grid.dataSource;
            if (typeof data != "undefined" && data != null && typeof data._data != "undefined" && data._data.length > 0) {
                var i;
                var moneyOrg = money;
                for (i = 0; i < data._data.length; i++) {
                    var dataAt = data.at(i);
                    dataAt.set("MoneyControl", 0);
                    var moneyUnrefunded = dataAt.MoneyUnrefunded;
                    if (money >= 0) {
                        money -= moneyUnrefunded;
                        if (money >= 0) {
                            dataAt.set("MoneyControl", moneyUnrefunded);
                            moneyOrg -= moneyUnrefunded;
                        } else if (moneyOrg >= 0) {
                            dataAt.set("MoneyControl", moneyOrg);
                        }
                    }
                }
                if (money > 0) {
                    baseCommon.ShowErrorNotLoad("Số tiền hoàn ứng đã đủ!");
                }
            }
        } catch (err) {
            baseCommon.ShowErrorNotLoad("Lấy dữ liệu không thành công vui lòng thử lại!");
        }
    }
    var CustomerList = function () {
        $("#lstCustomer").kendoGrid({
            dataSource: [],
            selectable: "row",
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    this.autoFitColumn(i);
                }
            },
            change: function (e) {
                var selectedRows = this.select();
                var selectedDataItems = [];
                for (var i = 0; i < selectedRows.length; i++) {
                    var dataItem = this.dataItem(selectedRows[i]);
                    selectedDataItems.push(dataItem);
                }
                // selectedDataItems contains all selected data items
                SetCustomerInfo(selectedDataItems[0]);
            },
            columns: [
                {
                    field: "CustomerName",
                    title: "Tên KH"
                },
                {
                    field: "CustomerNumberCard",
                    title: "CMT"
                },
                {
                    field: "ContactCode",
                    title: "Mã TC"
                },
                {
                    field: "ContactStartDate",
                    title: "Ngày giải ngân",
                    template: '#= baseCommon.FormatDate(ContactStartDate,\'DD/MM/YYYY\')#',
                },
                {
                    field: "LoanInterestPaymentNextDate",
                    title: "Ngày phải đóng lãi",
                    template: '#= baseCommon.FormatDate(LoanInterestPaymentNextDate,\'DD/MM/YYYY\')#',
                },
                {
                    field: "LoanTotalMoneyCurrent",
                    title: "Số tiền gốc còn lại",
                    template: '#= html_money(LoanTotalMoneyCurrent)#',
                    attributes: { style: "text-align:right;" }
                },
                {
                    field: "OwnerShopName",
                    title: "Cửa hàng"
                },
                {
                    field: "LoanTotalMoneyDisbursement",
                    title: "Tiền GN",
                    template: '#= html_money(LoanTotalMoneyDisbursement)#',
                    attributes: { style: "text-align:right;" }
                },
                {
                    field: "LoanCreditIDOfPartner",
                    title: "Mã HĐ",
                    template: '#= "HĐ-"+LoanCreditIDOfPartner#',
                },
                {
                    field: "ProductName",
                    title: "Sản phẩm",
                },
                //{
                //    field: "LoanTotalMoneyPaid",
                //    title: "Phải thanh toán",
                //    template: '#= html_money(LoanTotalMoneyPaid)#',
                //    attributes: { style: "text-align:right;" }
                //},
                {
                    field: "LoanStatusName",
                    title: "Trạng thái"
                },
                {
                    field: "CityName",
                    title: "Tỉnh/TP"
                }
            ]
        });
    }
    var PreviewInvoice = function () {
        recordPreviewInvoice = 0;
        $("#PreviewInvoice").kendoGrid({
            dataSource: [],
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordPreviewInvoice #",
                    width: 100
                },
                {
                    field: "invoiceTypeName",
                    title: "Loại phiếu",
                    template: '#= invoiceSubTypeName#' + '<br/> <span class="item-desciption" > #= invoiceTypeName# </span >'
                },
                {
                    field: "transactionMoney",
                    title: "Số tiền",
                    template: '#= html_money(transactionMoney)#' + '<br/> <span class="item-desciption" > #= sourceName# </span >'
                },
                {
                    field: "destinationName",
                    title: "Đối tượng thu/chi",
                    template: '#= destinationName#'
                },
                {
                    field: "description",
                    title: "Diễn giải"
                }
            ],
            dataBinding: function () {
                recordPreviewInvoice = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
        });
        var listInvoice = [];
        var model = {
            invoiceTypeName: "",
            invoiceSubTypeName: "",
            sourceName: "",
            destinationName: "",
            transactionMoney: 0,
            description: ""
        }
        listInvoice.push(model);
        var dataSource = new kendo.data.DataSource({
            data: [],
            pageSize: 5
        });
        var grid = $("#PreviewInvoice").data("kendoGrid");
        grid.setDataSource(dataSource);
    }
    var SetPreviewInvoice = function () {

        try {
            record = 0;
            var listInvoice = [];
            //chi
            var invoice_BankCardID = parseInt($("#Invoice_BankCardID").val());
            var invoice_BankCardID_text = $("#Invoice_BankCardID").data("kendoDropDownList").text();
            var invoice_TotalMoney = parseFloat($("#Invoice_TotalMoney_hdd").val());
            // thu
            var invoice_ToBankCardID = parseInt($("#Invoice_ToBankCardID").val());
            var invoice_ToBankCardID_text = $("#Invoice_ToBankCardID").data("kendoDropDownList").text();
            // phí
            var invoice_Payee = parseInt($("#Invoice_Payee").val());
            var invoice_MoneyFee_hdd = parseInt($("#Invoice_MoneyFee_hdd").val());
            //
            var note = $('#Note').val();
            if (invoice_BankCardID != null && invoice_BankCardID > 0) {
                var model = {
                    invoiceTypeName: "Phiếu chi",
                    invoiceSubTypeName: "Chuyển nội bộ",
                    sourceName: invoice_BankCardID_text,
                    destinationName: invoice_ToBankCardID_text,
                    transactionMoney: invoice_TotalMoney * -1,
                    description: note
                }
                listInvoice.push(model);
            }
            if (invoice_ToBankCardID != null && invoice_ToBankCardID > 0) {
                var model = {
                    invoiceTypeName: "Phiếu thu",
                    invoiceSubTypeName: "Thu nội bộ",
                    sourceName: invoice_ToBankCardID_text,
                    destinationName: invoice_BankCardID_text,
                    transactionMoney: invoice_TotalMoney,
                    description: note
                }
                listInvoice.push(model);
            }
            if (invoice_Payee != null && invoice_Payee >= 0 && invoice_MoneyFee_hdd > 0) {
                var sourceName = invoice_BankCardID_text;
                var destinationName = invoice_ToBankCardID_text;
                if (invoice_Payee == 1) {// bên nhân chịu phí
                    sourceName = invoice_ToBankCardID_text;
                    destinationName = invoice_BankCardID_text;
                }
                var model = {
                    invoiceTypeName: "Phiếu chi",
                    invoiceSubTypeName: "Phí ngân hàng",
                    sourceName: sourceName,
                    destinationName: destinationName,
                    transactionMoney: invoice_MoneyFee_hdd * -1,
                    description: note
                }
                listInvoice.push(model);
            }
            var dataSource = new kendo.data.DataSource({
                data: listInvoice,
                pageSize: 5
            });
            var grid = $("#PreviewInvoice").data("kendoGrid");
            grid.setDataSource(dataSource);
        } catch (e) {
            baseCommon.ShowErrorNotLoad("Thao tác không thành công vui lòng thử lại! " + e);
        }
    }
    var GetCustomer = function () {
        try {
            var dataSource = new kendo.data.DataSource({
                data: []
            });
            var grid = $("#lstCustomer").data("kendoGrid");
            grid.setDataSource(dataSource);
            var customerKeySearch = $('#CustomerKeySearch').val();
            if (customerKeySearch == null || customerKeySearch == "" || customerKeySearch.trim() == "") {
                baseCommon.ShowErrorNotLoad("Bạn chưa nhập tên KH hoặc CMT!");
                return;
            }
            var modelCustomer = {
                "CustomerKeySearch": customerKeySearch
            };
            baseCommon.AjaxDone("POST", baseCommon.Endpoint.GetLstLoanByFilterCustomer, JSON.stringify(modelCustomer), function (respone) {
                if (respone.Result == 1) {
                    if (respone.Data != null && respone.Data.Data != null && respone.Data.Data.length > 0) {
                        var dataSource = new kendo.data.DataSource({
                            data: respone.Data.Data
                        });
                        var grid = $("#lstCustomer").data("kendoGrid");
                        grid.setDataSource(dataSource);
                        SetCustomerInfo(respone.Data.Data[0]);
                    } else {
                        baseCommon.ShowErrorNotLoad("Không tìm thấy khách hàng phù hợp!");
                    }
                } else {
                    baseCommon.ShowErrorNotLoad(respone.Message);
                }
            });
        } catch (err) {
            baseCommon.ShowErrorNotLoad("Lấy dữ liệu không thành công vui lòng thử lại!");
        }
    }
    var SetCustomerInfo = function (customerInfo) {
        $('#Invoice_ContactCode').val(customerInfo.ContactCode);
        $('#Invoice_CustomerName').val(customerInfo.CustomerName);
        $('#Invoice_OwnerShopName').val(customerInfo.OwnerShopName);
        customerID = customerInfo.CustomerID;
        loanID = customerInfo.LoanID;
        shopID = customerInfo.OwnerShopID;
        invoiceTimaID = customerInfo.LoanCreditIDOfPartner;
    };
    var CustomerInfo_Clear = function () {
        $('#Invoice_ContactCode').val("");
        $('#Invoice_CustomerName').val("");
        $('#Invoice_OwnerShopName').val("");
        customerID = 0;
        loanID = 0;
        shopID = 0;
        toShopID = 0;
        invoiceTimaID = 0;
        $('#Invoice_TotalMoney').val(0);
        $('#Invoice_TotalMoney_hdd').val(0);
        $('#Invoice_MoneyFee').val(0);
        $('#Invoice_MoneyFee_hdd').val(0);
        $('#Note').val("");
        var dataSource = new kendo.data.DataSource({
            data: []
        });
        var grid = $("#lstCustomer").data("kendoGrid");
        if (typeof grid != "undefined" && grid != null) {
            grid.setDataSource(dataSource);
        }
        //
        var invoice_BankCardID = $("#Invoice_BankCardID").data("kendoDropDownList");
        if (typeof invoice_BankCardID != "undefined" && invoice_BankCardID != null) {
            invoice_BankCardID.value(0);
            invoice_BankCardID.trigger("change");
        }
        //
        var invoice_ToBankCardID = $("#Invoice_ToBankCardID").data("kendoDropDownList");
        if (typeof invoice_ToBankCardID != "undefined" && invoice_ToBankCardID != null) {
            invoice_ToBankCardID.value(0);
            invoice_ToBankCardID.trigger("change");
        }
        //
        var invoice_Payee = $("#Invoice_Payee").data("kendoDropDownList");
        if (typeof invoice_Payee != "undefined" && invoice_Payee != null) {
            invoice_Payee.value(-111);
            invoice_Payee.trigger("change");
        }
        //clear thông tin lender
        $("#Invoice_LenderID").val(0).change();

        var gridSearchInvoiceAdvanceSlipLender = $("#SearchInvoiceAdvanceSlipLender").data("kendoGrid");
        if (typeof gridSearchInvoiceAdvanceSlipLender != "undefined" && gridSearchInvoiceAdvanceSlipLender != null) {
            var dataSourceSearchInvoiceAdvanceSlipLender = new kendo.data.DataSource({
                data: [],
                aggregate: [
                    { field: "MoneyAdvance", aggregate: "sum" },
                    { field: "MoneyUnrefunded", aggregate: "sum" },
                    { field: "MoneyRefunded", aggregate: "sum" },
                    { field: "MoneyControl", aggregate: "sum" }
                ]
            });
            gridSearchInvoiceAdvanceSlipLender.setDataSource(dataSourceSearchInvoiceAdvanceSlipLender);
        }
    };
    var Init = function () {
        SearchInvoiceAdvanceSlipLenderGrid();
        PreviewInvoice();
        CustomerList();
        $("#customer_search").on('keypress', function (e) {
            if (e.which === 13) {
                GetCustomer();
            }
        });
        $("#Invoice_TotalMoney").on('keyup', function (e) {
            var loValue = baseCommon.html_money($(this).val().replace(/,/g, '').replace(/\./g, ''));
            if (loValue.length > 15) {
                baseCommon.ShowErrorNotLoad("Số tiền quá lớn!");
            }
            $("#Invoice_TotalMoney").val(loValue);
            var money = $("#Invoice_TotalMoney").val().replace(/,/g, '').replace(/\./g, '');
            $("#Invoice_TotalMoney_hdd").val(money);
            switch (INVOICETYPE_SELECTED) {
                case enumGroupInvoiceType.ReceiptInternal:// thu/chi nội bộ
                    SetPreviewInvoice();
                    break;
                case enumGroupInvoiceType.ReceiptLenderTopUp:// Thu tiền lender hoàn ứng
                    LenderMoney(money);
                    break;
                default:
            }
        });
        $("#Invoice_MoneyFee").on('keyup', function (e) {
            var loValue = formatCurrency($(this).val().replace(/,/g, '').replace(/\./g, ''));
            if (loValue.length > 15) {
                baseCommon.ShowErrorNotLoad("Số tiền quá lớn!");
            }
            $("#Invoice_MoneyFee").val(loValue);
            $("#Invoice_MoneyFee_hdd").val($("#Invoice_MoneyFee").val().replace(/,/g, '').replace(/\./g, ''));
            switch (INVOICETYPE_SELECTED) {
                case enumGroupInvoiceType.ReceiptInternal:// thu/chi nội bộ
                    SetPreviewInvoice();
                    break;
                default:
            }
        });
        $("#Note").on('keyup', function (e) {
            switch (INVOICETYPE_SELECTED) {
                case enumGroupInvoiceType.ReceiptInternal:// thu/chi nội bộ
                    SetPreviewInvoice();
                    break;
                default:
            }
        });
        $("#Invoice_BankCardID").kendoDropDownList({
            filter: "startswith",
            dataTextField: "Text",
            dataValueField: "Value",
            dataSource: [],
            optionLabel: {
                Text: "Chọn thẻ ngân hàng",
                Value: 0
            },
            change: function (e) {
                switch (INVOICETYPE_SELECTED) {
                    case enumGroupInvoiceType.ReceiptInternal:// thu/chi nội bộ
                        SetPreviewInvoice();
                        break;
                    default:
                    // code block
                }
            }
        });
        $("#Invoice_ToBankCardID").kendoDropDownList({
            filter: "startswith",
            dataTextField: "Text",
            dataValueField: "Value",
            dataSource: [],
            optionLabel: {
                Text: "Chọn thẻ ngân hàng",
                Value: 0
            },
            change: function (e) {
                switch (INVOICETYPE_SELECTED) {
                    case enumGroupInvoiceType.ReceiptInternal:// thu/chi nội bộ
                        SetPreviewInvoice();
                        break;
                    default:
                    // code block
                }
            }
        });
        $("#Invoice_Payee").kendoDropDownList({
            filter: "startswith",
            dataTextField: "Value",
            dataValueField: "Key",
            dataSource: enumInvoice_Payee,
            optionLabel: {
                Value: "Chọn bên chịu phí",
                Key: -111
            },
            change: function (e) {
                switch (INVOICETYPE_SELECTED) {
                    case enumGroupInvoiceType.ReceiptInternal:// thu/chi nội bộ
                        SetPreviewInvoice();
                        break;
                    default:
                    // code block
                }
            }
        });
        $("#Invoice_ShopID").kendoDropDownList({
            filter: "startswith",
            dataTextField: "FullName",
            dataValueField: "ID",
            dataSource: enumInvoice_Payee,
            optionLabel: {
                FullName: "Chọn cửa hàng thu",
                ID: 0
            }
        });
        $("#Invoice_LenderID").select2({
            placeholder: "Chọn mã hợp đồng",
            allowClear: true,
            ajax: {
                url: baseCommon.Endpoint.GetLenderSearchByLenderCode,
                type: 'post',
                headers: {
                    'Authorization': baseCommon.GetToken(),
                    "Content-Type": "application/json"
                },
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return JSON.stringify({ "GeneralSearch": params.term, "LstContractType": [0, 2, 5, 6] })
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    if (data != null && typeof data != "undefined" && data.Result == 1) {
                        return {
                            results: baseCommon.RepoConvert(data.Data),
                            pagination: {
                                more: (params.page * 30) < data.Total
                            }
                        };
                    }
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 3,
            templateResult: baseCommon.FormatRepo, // omitted for brevity, see the source of this page
            templateSelection: baseCommon.FormatRepoSelection // omitted for brevity, see the source of this page
        });
        $("#Invoice_LenderID").on('change', function (e) {
            var lenderId = $("#Invoice_LenderID").val();
            $('#Invoice_LenderCode').val($("#Invoice_LenderID option:selected").text());
            switch (INVOICETYPE_SELECTED) {
                case enumGroupInvoiceType.ReceiptLenderTopUp:// Lender hoàn ứng 
                    var dataSource = new kendo.data.DataSource({
                        data: [],
                        aggregate: [
                            { field: "MoneyAdvance", aggregate: "sum" },
                            { field: "MoneyUnrefunded", aggregate: "sum" },
                            { field: "MoneyRefunded", aggregate: "sum" },
                            { field: "MoneyControl", aggregate: "sum" }
                        ]
                    });
                    var grid = $("#SearchInvoiceAdvanceSlipLender").data("kendoGrid");
                    grid.setDataSource(dataSource);
                    if (lenderId != null && lenderId != "" && parseFloat(lenderId) > 0) {
                        SearchInvoiceAdvanceSlipLender();
                    }
                    break;
                default:
                // code block
            }
        });
        GetBankCard();
        GetShop();
    };

    var InitForVerifyInvoice = function () {
        $("#Invoice_BankCardID").kendoDropDownList({
            filter: "startswith",
            dataTextField: "Text",
            dataValueField: "Value",
            dataSource: [],
            optionLabel: {
                Text: "Chọn thẻ ngân hàng",
                Value: 0
            },
            change: function (e) {
                switch (INVOICETYPE_SELECTED) {
                    case enumGroupInvoiceType.ReceiptInternal:// thu/chi nội bộ
                        SetPreviewInvoice();
                        break;
                    default:
                    // code block
                }
            }
        });
        $("#Invoice_ToBankCardID").kendoDropDownList({
            filter: "startswith",
            dataTextField: "Text",
            dataValueField: "Value",
            dataSource: [],
            optionLabel: {
                Text: "Chọn thẻ ngân hàng",
                Value: 0
            },
            change: function (e) {
                switch (INVOICETYPE_SELECTED) {
                    case enumGroupInvoiceType.ReceiptInternal:// thu/chi nội bộ
                        SetPreviewInvoice();
                        break;
                    default:
                    // code block
                }
            }
        });
        $("#Invoice_TotalMoney").on('keyup', function (e) {
            var loValue = formatCurrency($(this).val().replace(/,/g, '').replace(/\./g, ''));
            if (loValue.length > 15) {
                baseCommon.ShowErrorNotLoad("Số tiền quá lớn!");
            }
            $("#Invoice_TotalMoney").val(loValue);
            var money = $("#Invoice_TotalMoney").val().replace(/,/g, '').replace(/\./g, '');
            $("#Invoice_TotalMoney_hdd").val(money);
            switch (INVOICETYPE_SELECTED) {
                case enumGroupInvoiceType.ReceiptInternal:// thu/chi nội bộ
                    SetPreviewInvoice();
                    break;
                case enumGroupInvoiceType.ReceiptLenderTopUp:// Thu tiền lender hoàn ứng
                    LenderMoney(money);
                    break;
                default:
            }
        });
        $("#Invoice_MoneyFee").on('keyup', function (e) {
            var loValue = formatCurrency($(this).val().replace(/,/g, '').replace(/\./g, ''));
            if (loValue.length > 15) {
                baseCommon.ShowErrorNotLoad("Số tiền quá lớn!");
            }
            $("#Invoice_MoneyFee").val(loValue);
            $("#Invoice_MoneyFee_hdd").val($("#Invoice_MoneyFee").val().replace(/,/g, '').replace(/\./g, ''));
            switch (INVOICETYPE_SELECTED) {
                case enumGroupInvoiceType.ReceiptInternal:// thu/chi nội bộ
                    SetPreviewInvoice();
                    break;
                default:
            }
        });
        $("#Note").on('keyup', function (e) {
            switch (INVOICETYPE_SELECTED) {
                case enumGroupInvoiceType.ReceiptInternal:// thu/chi nội bộ
                    SetPreviewInvoice();
                    break;
                default:
            }
        });
        $("#Invoice_BankCardID").kendoDropDownList({
            filter: "startswith",
            dataTextField: "Text",
            dataValueField: "Value",
            dataSource: [],
            optionLabel: {
                Text: "Chọn thẻ ngân hàng",
                Value: 0
            },
            change: function (e) {
                switch (INVOICETYPE_SELECTED) {
                    case enumGroupInvoiceType.ReceiptInternal:// thu/chi nội bộ
                        SetPreviewInvoice();
                        break;
                    default:
                    // code block
                }
            }
        });
        GetBankCard();
    };
    var GetShop = function () {
        var dataSource = new kendo.data.DataSource({
            data: []
        });
        var dropdownlist = $("#Invoice_ShopID").data("kendoDropDownList");
        dropdownlist.setDataSource(dataSource);
        var data = { "type": 1, "generalSearch": "" };
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetLenderSearch, data, function (respone) {
            if (respone.Result == 1) {
                var dataSources = new kendo.data.DataSource({
                    data: respone.Data
                });
                var dropdownlist = $("#Invoice_ShopID").data("kendoDropDownList");
                dropdownlist.setDataSource(dataSources);
            }
        }, "Chưa lấy được danh sách cửa hàng!");
    }
    var GetBankCard = function () {
        try {
            var dataSource = new kendo.data.DataSource({
                data: []
            });
            var dropdownlist = $("#Invoice_BankCardID").data("kendoDropDownList");
            dropdownlist.setDataSource(dataSource);
            var invoice_ToBankCardID = $("#Invoice_ToBankCardID").data("kendoDropDownList");
            invoice_ToBankCardID.setDataSource(dataSource);
            baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetBankCard + statusCommon.Default + "&Status=" + statusCommon.Active, "", function (respone) {
                if (respone.Result == 1) {
                    var dataSources = new kendo.data.DataSource({
                        data: respone.Data
                    });
                    var dropdownlist = $("#Invoice_BankCardID").data("kendoDropDownList");
                    dropdownlist.setDataSource(dataSources);
                    var dataSources_ToBankCardID = new kendo.data.DataSource({
                        data: respone.Data
                    });
                    var invoice_ToBankCardID = $("#Invoice_ToBankCardID").data("kendoDropDownList");
                    invoice_ToBankCardID.setDataSource(dataSources_ToBankCardID);
                }
            }, "Chưa lấy được danh sách ngân hàng!");
        } catch (err) {
            baseCommon.ShowErrorNotLoad("Có lỗi xảy ra vui lòng thử lại!");
        }
    }
    var SaveInvoice = function () {
        Save_Visible(false);// double click
        try {

            switch (INVOICETYPE_SELECTED) {
                case enumGroupInvoiceType.ReceiptCustomer:
                    SaveInvoice_Customer();
                    break;
                case enumGroupInvoiceType.Disbursement:
                    SaveInvoice_Customer();
                    break;
                case enumGroupInvoiceType.PaySlipGiveBackExcessCustomer:
                    SaveInvoice_Customer();
                    break;
                case enumGroupInvoiceType.ReceiptCustomerConsider:
                    SaveInvoice_Other();
                    break;
                case enumGroupInvoiceType.ReceiptOther:// thu tiền khác
                    SaveInvoice_Other();
                    break;
                case enumGroupInvoiceType.PaySlipOther:// chi tiền khác
                    SaveInvoice_Other();
                    break;
                case enumGroupInvoiceType.ReceiptOnBehalfShop:// thu hộ cửa hàng
                    SaveInvoice_Customer();
                    break;
                case enumGroupInvoiceType.ReceiptInterestBank:// phiếu thu tiền lãi ngân hàng
                    SaveInvoice_Other();
                    break;
                case enumGroupInvoiceType.ReceiptInternal:// Thu nội bộ
                    SaveInvoice_Other();
                    break;
                case enumGroupInvoiceType.PaySlipTransferInternal:// chi nội bộ
                    SaveInvoice_Other();
                    break;
                case enumGroupInvoiceType.ReceiptLenderCapital:// Lender thêm vốn
                    SaveInvoice_Other();
                    break;
                case enumGroupInvoiceType.ReceiptLenderInsurance:// Thu tiền bảo hiểm lender
                    SaveInvoice_Other();
                    break;
                case enumGroupInvoiceType.ReceiptLenderTopUp:// Thu tiền lender hoàn ứng
                    SaveInvoice_Other();
                    break;
                case enumGroupInvoiceType.PaySlipLenderWithdraw:// Lender rút vốn
                    SaveInvoice_Other();
                    break;
                case enumGroupInvoiceType.PaySlipFeeForBank:// Trả phí cho ngân hàng
                    SaveInvoice_Other();
                    break;
                default:
                // code block
            }
        } catch (err) {
            baseCommon.ShowErrorNotLoad("Có lỗi xảy ra vui lòng thử lại!");
        }
    }
    var SaveInvoice_Customer = function () {
        try {
            var totalMoney = parseFloat($('#Invoice_TotalMoney_hdd').val());
            var strTransactionDate = $('#Invoice_StrTransactionDate').val();
            var bankCardID = parseInt($('#Invoice_BankCardID').val());
            invoice_ShopID = parseInt($('#Invoice_ShopID').val());
            var note = $('#Note').val();
            var userIDCreate = USER_ID;
            if (typeof INVOICETYPE_SELECTED == "undefined" || INVOICETYPE_SELECTED == null || INVOICETYPE_SELECTED == 0) {
                baseCommon.ShowErrorNotLoad("Thông tin tạo phiếu không hợp lệ!");
                Save_Visible(true);
                return;
            }
            if (typeof totalMoney == "undefined" || totalMoney == null || totalMoney == 0) {
                baseCommon.ShowErrorNotLoad("Bạn chưa nhập số tiền!");
                Save_Visible(true);
                return;
            }
            if (typeof customerID == "undefined" || customerID == null || customerID == 0) {
                baseCommon.ShowErrorNotLoad("Bạn chưa chọn khách hàng!");
                Save_Visible(true);
                return;
            }
            if (typeof strTransactionDate == "undefined" || strTransactionDate == null || strTransactionDate == "") {
                baseCommon.ShowErrorNotLoad("Bạn chưa chọn ngày giao dịch!");
                Save_Visible(true);
                return;
            }
            if (typeof bankCardID == "undefined" || bankCardID == null || bankCardID == 0) {
                baseCommon.ShowErrorNotLoad("Bạn chưa chọn thẻ ngân hàng!");
                Save_Visible(true);
                return;
            }
            if (typeof note == "undefined" || note == null || note == "" || note.trim() == "") {
                baseCommon.ShowErrorNotLoad("Bạn chưa nhập nội dung!");
                Save_Visible(true);
                return;
            }
            if (note.length > 500) {
                baseCommon.ShowErrorNotLoad("Nội dung vượt quá 500 ký tự!");
                Save_Visible(true);
                return;
            }
            if (typeof userIDCreate == "undefined" || userIDCreate == null || userIDCreate == 0) {
                baseCommon.ShowErrorNotLoad("thông tin người tạo không hợp lệ vui lòng thử lại!");
                Save_Visible(true);
                return;
            }
            strTransactionDate = strTransactionDate.replace("/", "-").replace("/", "-") + ":00";
            var invoiceModel = {
                TotalMoney: totalMoney,
                CustomerID: customerID,
                StrTransactionDate: strTransactionDate,
                BankCardID: bankCardID,
                Note: note,
                UserIDCreate: userIDCreate,
                LoanID: loanID,
                //InvoiceTimaID: invoiceTimaID,
                ShopID: shopID,
                ToShopID: toShopID,
                InvoiceSubType: INVOICETYPE_SELECTED
            };
            switch (INVOICETYPE_SELECTED) {
                case enumGroupInvoiceType.ReceiptOnBehalfShop:// thu hộ cửa hàng 
                    if (typeof invoice_ShopID == "undefined" || invoice_ShopID == null || invoice_ShopID == 0) {
                        baseCommon.ShowErrorNotLoad("Bạn chưa chọn cửa hàng thu!");
                        Save_Visible(true);
                        return;
                    }
                    invoiceModel.ShopID = invoice_ShopID;// shop thu tiền
                    invoiceModel.ToShopID = shopID; // shop theo đơn vay
                    break;
                default:
                // code block
            }
            var endpoint = Invoice_Endpoint();
            baseCommon.AjaxDone("POST", endpoint, JSON.stringify(invoiceModel), function (respone) {
                Save_Visible(true);
                if (respone.Result > 0) {
                    baseCommon.ShowSuccess(respone.Message);
                    CustomerInfo_Clear();
                    $('#btnGetData').click();
                    $('#ModalCreateInvoice').modal('hide');
                } else {
                    baseCommon.ShowErrorNotLoad(respone.Message);
                }
            });
        } catch (err) {
            Save_Visible(true);
            baseCommon.ShowErrorNotLoad("Có lỗi xảy ra vui lòng thử lại!");
        }
    }
    var SaveInvoice_Other = function () {
        try {
            var totalMoney = parseFloat($('#Invoice_TotalMoney_hdd').val());
            var moneyFee = parseFloat($('#Invoice_MoneyFee_hdd').val());
            var strTransactionDate = $('#Invoice_StrTransactionDate').val();
            var bankCardID = parseInt($('#Invoice_BankCardID').val());
            var toBankCardID = parseInt($('#Invoice_ToBankCardID').val());
            var payee = parseInt($('#Invoice_Payee').val());
            var lenderID = parseInt($('#Invoice_LenderID').val());
            var note = $('#Note').val();
            var userIDCreate = USER_ID;
            if (typeof INVOICETYPE_SELECTED == "undefined" || INVOICETYPE_SELECTED == null || INVOICETYPE_SELECTED == 0) {
                baseCommon.ShowErrorNotLoad("Loại phiếu tạo không hợp lệ, vui lòng thử lại!");
                Save_Visible(true);
                return;
            }
            if (typeof totalMoney == "undefined" || totalMoney == null || totalMoney == 0) {
                baseCommon.ShowErrorNotLoad("Bạn chưa nhập số tiền!");
                Save_Visible(true);
                return;
            }
            if (typeof strTransactionDate == "undefined" || strTransactionDate == null || strTransactionDate == "") {
                baseCommon.ShowErrorNotLoad("Bạn chưa chọn ngày giao dịch!");
                Save_Visible(true);
                return;
            }
            if (typeof bankCardID == "undefined" || bankCardID == null || bankCardID == 0) {
                baseCommon.ShowErrorNotLoad("Bạn chưa chọn thẻ ngân hàng!");
                Save_Visible(true);
                return;
            }
            if (typeof note == "undefined" || note == null || note == "" || note.trim() == "") {
                baseCommon.ShowErrorNotLoad("Bạn chưa nhập nội dung!");
                Save_Visible(true);
                return;
            }
            if (note.length > 500) {
                baseCommon.ShowErrorNotLoad("Nội dung vượt quá 500 ký tự!");
                Save_Visible(true);
                return;
            }
            if (typeof userIDCreate == "undefined" || userIDCreate == null || userIDCreate == 0) {
                baseCommon.ShowErrorNotLoad("thông tin người tạo không hợp lệ vui lòng thử lại!");
                Save_Visible(true);
                return;
            }
            switch (INVOICETYPE_SELECTED) {
                case enumGroupInvoiceType.ReceiptInternal:// thu/chi nội bộ
                    if (typeof toBankCardID == "undefined" || toBankCardID == null || toBankCardID == 0) {
                        baseCommon.ShowErrorNotLoad("Bạn chưa chọn thẻ ngân hàng thu!");
                        Save_Visible(true);
                        return;
                    }
                    if ((typeof payee == "undefined" || payee == null || payee == -111) && moneyFee > 0) {
                        baseCommon.ShowErrorNotLoad("Bạn chưa chọn bên chịu phí!");
                        Save_Visible(true);
                        return;
                    } else if (payee < 0 && moneyFee == 0) {
                        payee = 0;
                    }
                    if (bankCardID == toBankCardID) {
                        baseCommon.ShowErrorNotLoad("Thẻ ngân hàng thu,chi trùng nhau!");
                        Save_Visible(true);
                        return;
                    }
                    break;
                case enumGroupInvoiceType.ReceiptLenderCapital:// Lender thêm vốn
                    if (typeof lenderID == "undefined" || lenderID == null || lenderID == 0) {
                        baseCommon.ShowErrorNotLoad("Bạn chưa chọn lender!");
                        Save_Visible(true);
                        return;
                    }
                    break;
                default:
                // code block
            }
            strTransactionDate = strTransactionDate.replace("/", "-").replace("/", "-") + ":00";
            var invoiceModel = {
                InvoiceSubType: INVOICETYPE_SELECTED,
                TotalMoney: totalMoney,
                StrTransactionDate: strTransactionDate,
                BankCardID: bankCardID,
                Note: note,
                UserIDCreate: userIDCreate,
                FromBankCardID: bankCardID,
                ToBankCardID: toBankCardID,
                Payee: payee,
                MoneyFee: moneyFee,
                LenderID: lenderID
            };
            var endpoint = Invoice_Endpoint();
            baseCommon.AjaxDone("POST", endpoint, JSON.stringify(invoiceModel), function (respone) {
                Save_Visible(true);
                if (respone.Result > 0) {
                    baseCommon.ShowSuccess(respone.Message);
                    CustomerInfo_Clear();
                    $('#btnGetData').click();
                    $('#ModalCreateInvoice').modal('hide');
                    if ($('#hdd_confirm_ticketID').val() > 0) {
                        ticketLender.confirmDone($('#hdd_confirm_ticketID').val());
                    }
                } else {
                    baseCommon.ShowErrorNotLoad(respone.Message);
                }
            });
        } catch (err) {
            Save_Visible(true);
            baseCommon.ShowErrorNotLoad("Có lỗi xảy ra vui lòng thử lại!");
        }
    }
    var Invoice_Endpoint = function () {
        try {
            var endpoint = "";
            switch (INVOICETYPE_SELECTED) {
                case enumGroupInvoiceType.ReceiptCustomer:
                    endpoint = baseCommon.Endpoint.CreateInvoiceCustomer;
                    break;
                case enumGroupInvoiceType.Disbursement:
                    endpoint = baseCommon.Endpoint.CreateInvoicePaySlipCustomer;
                    break;
                case enumGroupInvoiceType.PaySlipGiveBackExcessCustomer:
                    endpoint = baseCommon.Endpoint.CreateInvoiceBackMoneyCustomer;
                    break;
                case enumGroupInvoiceType.ReceiptCustomerConsider:
                    endpoint = baseCommon.Endpoint.CreateInvoiceConsiderCustomer;
                    break;
                case enumGroupInvoiceType.ReceiptOther:// thu tiền khác
                    endpoint = baseCommon.Endpoint.CreateInvoiceOther;
                    break;
                case enumGroupInvoiceType.PaySlipOther:// chi tiền khác
                    endpoint = baseCommon.Endpoint.CreateInvoiceOther;
                    break;
                case enumGroupInvoiceType.ReceiptOnBehalfShop:// thu hộ
                    endpoint = baseCommon.Endpoint.CreateInvoiceOnBehalfShop;
                    break;
                case enumGroupInvoiceType.ReceiptInterestBank:// thu tiền lãi ngân hàng 
                    endpoint = baseCommon.Endpoint.CreateInvoiceBankInterest;
                    break;
                case enumGroupInvoiceType.ReceiptInternal:// Thu/chi nội bộ
                    endpoint = baseCommon.Endpoint.CreateInvoiceInternal;
                    break;
                case enumGroupInvoiceType.PaySlipTransferInternal:// Thu/chi nội bộ
                    endpoint = baseCommon.Endpoint.CreateInvoiceInternal;
                    break;
                case enumGroupInvoiceType.ReceiptLenderCapital:// Lender thêm vốn
                    endpoint = baseCommon.Endpoint.CreateInvoiceLender;
                    break;
                case enumGroupInvoiceType.ReceiptLenderInsurance:// Thu tiền bảo hiểm lender
                    endpoint = baseCommon.Endpoint.CreateInvoiceLender;
                    break;
                case enumGroupInvoiceType.ReceiptLenderTopUp:// Thu tiền lender hoàn ứng
                    endpoint = baseCommon.Endpoint.CreateInvoiceLender;
                    break;
                case enumGroupInvoiceType.PaySlipLenderWithdraw:// Lender rút vốn
                    endpoint = baseCommon.Endpoint.CreateInvoiceLender;
                    break;
                case enumGroupInvoiceType.PaySlipFeeForBank:// Trả phí cho ngân hàng
                    endpoint = baseCommon.Endpoint.CreateInvoiceBankInterest;
                    break;
                default:
                // code block
            }
            return endpoint;
        } catch (err) {
            return null;
        }
    }
    var Save_Visible = function (isVisible) {
        if (!isVisible) {
            $("#btn_save_invoice").addClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
            $("#btn_save_invoice").prop("disabled", true);
        } else {
            $("#btn_save_invoice").removeClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
            $("#btn_save_invoice").prop("disabled", false);
        }
    }
    return {
        Init: Init,
        GetCustomer: GetCustomer,
        SaveInvoice: SaveInvoice,
        CustomerInfo_Clear: CustomerInfo_Clear,
        Save_Visible: Save_Visible,
        InitForVerifyInvoice: InitForVerifyInvoice
    };
}