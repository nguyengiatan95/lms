﻿
var datatable, INVOICETYPE_SELECTED, recordGrid, isSearchLoan = true;
var invoice = new function () {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var InvoiceType = $('#InvoiceType').val();
                if (InvoiceType == null || InvoiceType == "" || InvoiceType.trim() == "") {
                    InvoiceType = 0
                }
                var InvoiceSubType = $('#InvoiceSubType').val();
                if (InvoiceSubType == null || InvoiceSubType == "" || InvoiceSubType.trim() == "") {
                    InvoiceSubType = 0
                }
                var ShopID = $('#ShopID').val();
                if (ShopID == null || ShopID == "" || ShopID.trim() == "") {
                    ShopID = 0
                }
                var BankCardID = $('#BankCardID').val();
                if (BankCardID == null || BankCardID == "" || BankCardID.trim() == "") {
                    BankCardID = 0
                }
                var Status = $('#Status').val();
                if (Status == null || Status == "" || Status.trim() == "") {
                    Status = 0
                }
                var FromDate = $('#fromDate').val();
                var ToDate = $('#toDate').val();
                var pageIndex = options.data.page;
                var data = {
                    InvoiceType: parseFloat(InvoiceType),
                    InvoiceSubType: parseFloat(InvoiceSubType),
                    ShopID: parseFloat(ShopID),
                    BankCardID: parseFloat(BankCardID),
                    Status: parseFloat(Status),
                    FromDate: FromDate,
                    ToDate: ToDate,
                    PageIndex: pageIndex,
                    PageSize: options.data.pageSize
                }
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetInvoiceInfosByCondition, JSON.stringify(data), function (res) {
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    if (res.Result == 1) {
                        options.success(res.Data);
                    }
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                }, "Thao tác thất bại, vui lòng thử lại sau!"); 
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                return response.RecordsTotal; // total is returned in the "total" field of the response
            },
            data: function (response) {
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function () {
        $("#grid").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [10, 20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 5) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'InvoiceSubTypeName',
                    title: 'Loại phiếu',
                    textAlign: 'left',
                    template: function (row) {
                        var html = `<a href="javascript:;" title="Thông tin chi tiết" onclick="invoice.ShowModal(${row.InvoiceID},${row.InvoiceType},${row.InvoiceSubType},'${row.StatusName}','${row.ShopName}','${row.SourceName}','${row.CreateDate}','${row.UserNameCreate}',${row.TransactionMoney},'${row.Description}','${row.InvoiceTypeName}','${row.InvoiceSubTypeName}')">\
                                    ${row.InvoiceSubTypeName}\
                                    <br/> <span class="item-desciption" > ${row.InvoiceTypeName} </span >\
                                    </a>`;
                        return html;
                    }
                },
                {
                    field: 'TransactionMoney',
                    title: 'Số tiền',
                    textAlign: 'right',
                    template: function (row) {
                        if (row == null || row.TransactionMoney == null) {
                            return "";
                        }
                        return `${html_money(row.TransactionMoney)}\
                                    <br/> <span class="item-desciption" > ${row.DestinationName} </span >`;
                    }
                },
                {

                    field: 'SourceName',
                    title: 'Đối tượng thu/chi',
                    textAlign: 'left'
                },
                {
                    field: 'ShopName',
                    title: 'Cửa hàng',
                    textAlign: 'left'
                },
                {

                    field: 'Description',
                    title: 'Diễn giải',
                    textAlign: 'left',
                    width: 400
                },
                {
                    field: 'Status',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    template: function (row) {
                        if (row == null || row.Status == null) {
                            return "";
                        }
                        if (row.Status >= 3) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">' + row.StatusName + '</span>';
                        }
                        else if (row.Status > 1 && row.Status < 3) {
                            return '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">' + row.StatusName + '</span>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + row.StatusName + '</span>';
                        }
                    }
                },
                {
                    field: 'TransactionDate',
                    title: 'Ngày giao dịch',
                    textAlign: 'left',
                    template: function (row) {
                        if (row == null || row.TransactionDate == null) {
                            return "";
                        }
                        var date = moment(row.TransactionDate);
                        var html = date.format("DD/MM/YYYY HH:mm");
                        return html;
                    }
                },
                {
                    field: 'UserNameCreate',
                    title: 'Người/ngày tạo',
                    textAlign: 'left',
                    template: function (row) {
                        if (row == null || row.CreateDate == null) {
                            return "";
                        }
                        var date = moment(row.CreateDate);
                        var html = row.UserNameCreate + ' <br/> <span class="item-desciption" >' + date.format("DD/MM/YYYY") + '</span >';
                        return html;
                    }
                },
                {
                    field: 'Action',
                    title: 'Hành động',
                    sortable: false,
                    hide: false,
                    template: function (row, index, datatable) {
                        if (row == null || row.InvoiceID == null) {
                            return "";
                        }
                        var html = '';
                        var func = `invoice.ShowModal(${row.InvoiceID},${row.InvoiceType},${row.InvoiceSubType},'${row.StatusName}','${row.ShopName}','${row.SourceName}','${row.CreateDate}','${row.UserNameCreate}',${row.TransactionMoney},'${row.Description}','${row.InvoiceTypeName}','${row.InvoiceSubTypeName}')`;
                        return '<a class="btn btn-primary btn-icon btn-sm" title="Sửa" data-toggle="modal" data-target="#modal_add_edit" onclick="' + func + '"> <i class="fa fa-edit" ></i></a>';
                        return html;
                    }
                }
            ]
        });
    };

    var LoadData = function () { 
        if (isSearchLoan) {
            baseCommon.ButtonSubmit(false, '#btnGetData');
            var grid = $("#grid").data("kendoGrid");
            if (grid != null) {
                grid.dataSource.page(1);
            } else {
                InitGrid();
            }
        }  
    }
    var Init = function () { 
        $("#CreateInvoiceInternal").on('click', function (e) {
            invoice.CreateInvoice(enumGroupInvoiceType.ReceiptInternal, 'Tạo phiếu', 'Chuyển tiền nội bộ');
        });
        $("#CreateInvoiceReceipt").kendoDropDownList({
            filter: "startswith",
            dataTextField: "Value",
            dataValueField: "Key",
            dataSource: createInvoiceReceipt,
            optionLabel: {
                Value: "Tạo phiếu thu",
                Key: 0,
            },
            optionLabelTemplate: '<span style="color:\\#0d460d; font-weight: bold; font-size: 16px;"><i class="fa fa-file-invoice-dollar"> Tạo phiếu thu </span>',
            // footerTemplate: 'Total #: instance.dataSource.total() # items found', 
            valueTemplate: '<i class="fa fa-file-invoice-dollar"></i><span class="selected-value" > <span>#:data.Value#</span></span>',
            template: '<i class="fa fa-file-invoice-dollar"></i> <span class="dropdown-item"> #:data.Value#</span>',
            height: 400,
            change: function (e) {
                try {
                    var key = parseInt(this.value());
                    if (key == 0) {
                        return;
                    }
                    var value = this.dataItem().Value;
                    invoice.CreateInvoice(key, 'Tạo phiếu thu', value);
                    var createInvoiceReceipt = $("#CreateInvoiceReceipt").data("kendoDropDownList");
                    createInvoiceReceipt.value(0);
                    createInvoiceReceipt.trigger("change");
                } catch (err) {

                }
            }
        });
        $("#CreateInvoicePay").kendoDropDownList({
            filter: "startswith",
            dataTextField: "Value",
            dataValueField: "Key",
            dataSource: createInvoicePay,
            optionLabel: {
                Value: "Tạo phiếu chi",
                Key: 0,
            },
            optionLabelTemplate: '<span style="color:\\#a00000; font-weight: bold; font-size: 16px;"><i class="fa fa-file-invoice"> Tạo phiếu chi </span>',
            // footerTemplate: 'Total #: instance.dataSource.total() # items found',
            valueTemplate: '<i class="fa fa-file-invoice"></i><span class="selected-value" > <span>#:data.Value#</span></span>',
            template: '<i class="fa fa-file-invoice"></i> <span class="dropdown-item"> #:data.Value#</span>',
            height: 400,
            change: function (e) {
                try {
                    var key = parseInt(this.value());
                    if (key == 0) {
                        return;
                    }
                    var value = this.dataItem().Value;
                    invoice.CreateInvoice(key, 'Tạo phiếu chi', value);
                    var createInvoicePay = $("#CreateInvoicePay").data("kendoDropDownList");
                    createInvoicePay.value(0);
                    createInvoicePay.trigger("change");
                } catch (err) {

                }
            }
        });
        $("#fromDate,#toDate,#InvoiceType,#InvoiceSubType,#ShopID,#BankCardID,#Status").on('change', function (e) {
            LoadData();
        });
        $("#btnGetData").on('click', function (e) {
            LoadData();
        });
        $('#InvoiceType').select2({
            placeholder: "Chọn loại phiếu",
            tags: true
        });
        $('#InvoiceSubType').select2({
            placeholder: "Chọn loại phiếu chi tiết",
            tags: true
        });
        $('#ShopID').select2({
            placeholder: "Chọn cửa hàng",
            tags: true
        });
        $('#BankCardID').select2({
            placeholder: "Chọn cửa hàng",
            tags: true
        });
        $('#Status').select2({
            placeholder: "Chọn trạng thái",
            tags: true
        });
        submitFormSave();
        baseCommon.DataSource("#BankCardID", "Chọn thẻ ngân hàng", "value", "text", 0, listBankCard);
        invoice.GetShop();
        setTimeout(function () { Searching(); }, 500);
    };
    var GetShop = function () {
        var data = { "type": 1, "generalSearch":"" };
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetLenderSearch, data, function (respone) {
            if (respone.Result == 1) {
                baseCommon.DataSource("#ShopID", "Tất cả", "ID", "FullName", 0, respone.Data);
            }
        }, "Chưa lấy được danh sách cửa hàng!");
    }
    var ShowModal = function (invoiceId = 0, InvoiceType = 0, InvoiceSubType = 0, Status = 0, ShopName = '', SourceName = 1, CreateDate = "", UserNameCreate = "", TransactionMoney = 0, Description = "", InvoiceTypeName = "", InvoiceSubTypeName = "", ) {
        var form = $('#btn_save').closest('form');
        form.validate().destroy();
        $("#headerId").html(InvoiceTypeName + " - " + InvoiceSubTypeName);
        $("#hdd_invoiceId").val(invoiceId);
        $('#Status_').val(Status);
        $('#Description').val(Description);
        $('#TransactionMoney_').val(TransactionMoney);
        $('#SourceName_').val(SourceName);
        $('#ShopName_').val(ShopName);
        $('#UserNameCreate_').val(UserNameCreate);
        var date = moment(CreateDate);
        var createDate = date.format("DD/MM/YYYY HH:mm");
        $('#CreateDate_').val(createDate);
        $('#modal_add_edit').modal('toggle');
    }
    var submitFormSave = function () {

    }
    var CreateInvoice = function (invoiceType, title, subTitle, ticketID = 0, note = '') {
        $('#hdd_confirm_ticketID').val(ticketID);
        setTimeout(function () {
            $('#Note').val(note);
        }, 300);
        INVOICETYPE_SELECTED = invoiceType;
        createInvoice.Save_Visible(true);
        $("#Invoice_StrTransactionDate").val(baseCommon.FormatDate(new Date(), 'DD/MM/YYYY HH:mm'));
        $("#CreateInvoiceHeaderId").html(title + " - " + subTitle);
        $("#Customer_Invoice_Form").html("Điền thông tin " + subTitle.toLowerCase());
        Invoice_Modal();
    }
    var Invoice_Modal = function () {
        try {
            var endpoint = "";
            $(".Class_Invoice_ReceiptOnBehalfShop").css("display", "none");
            $(".class_Invoice_ToBankCardID").css("display", "none");
            $('#lable_Invoice_BankCardID').html("Thẻ ngân hàng");
            $(".Class_Invoice_Customer").css("display", "none");// ẩn tìm khách hàng 
            $(".class_Invoice_Payee").css("display", "none");// ẩn chịu phí  
            $(".class_Invoice_MoneyFee").css("display", "none");// ẩn tiền phí chuyển tiền
            $(".class_Invoice_PreviewInvoice").css("display", "none");// ẩn xem trước phiếu chuyển tiền nội bộ
            $(".Class_Invoice_Lender").css("display", "none");// ẩn Class_Invoice_Lender  
            $(".Class_Invoice_SearchInvoiceAdvanceSlipLender").css("display", "none");// ẩn Class_Invoice_Lender 

            createInvoice.CustomerInfo_Clear();
            switch (INVOICETYPE_SELECTED) {
                case enumGroupInvoiceType.ReceiptCustomer:
                    $(".Class_Invoice_Customer").removeAttr("style");// show tìm khách hàng
                    $('#ModalCreateInvoice').modal('toggle');
                    break;
                case enumGroupInvoiceType.Disbursement:
                    $(".Class_Invoice_Customer").removeAttr("style");
                    $('#ModalCreateInvoice').modal('toggle');
                    break;
                case enumGroupInvoiceType.PaySlipGiveBackExcessCustomer:
                    $(".Class_Invoice_Customer").removeAttr("style");
                    $('#ModalCreateInvoice').modal('toggle');
                    break;
                case enumGroupInvoiceType.ReceiptCustomerConsider:
                    $('#ModalCreateInvoice').modal('toggle');
                    break;
                case enumGroupInvoiceType.ReceiptOther:// thu tiền khác 
                    $('#ModalCreateInvoice').modal('toggle');
                    break;
                case enumGroupInvoiceType.PaySlipOther:// chi tiền khác 
                    $('#ModalCreateInvoice').modal('toggle');
                    break;
                case enumGroupInvoiceType.ReceiptOnBehalfShop:// thu hộ cửa hàng
                    $('#lable_Invoice_ShopID').html("Cửa hàng thu hộ");
                    $(".Class_Invoice_ReceiptOnBehalfShop").removeAttr("style"); // show chọn cửa hàng 
                    $(".Class_Invoice_Customer").removeAttr("style"); // show tìm khách hàng
                    $('#ModalCreateInvoice').modal('toggle');
                    break;
                case enumGroupInvoiceType.ReceiptInterestBank:// thu tiền lãi ngân hàng 
                    $('#ModalCreateInvoice').modal('toggle');
                    break;
                case enumGroupInvoiceType.ReceiptInternal:// Thu nội bộ  
                    $(".class_Invoice_ToBankCardID").removeAttr("style"); // show Thẻ ngân hàng thu
                    $(".class_Invoice_Payee").removeAttr("style"); // show chịu phí
                    $(".class_Invoice_MoneyFee").removeAttr("style"); // show phí
                    $(".class_Invoice_PreviewInvoice").removeAttr("style"); // xem trước phiếu chuyển tiền nội bộ
                    $('#lable_Invoice_BankCardID').html("Thẻ ngân hàng chi");
                    $('#ModalCreateInvoice').modal('toggle');
                    break;
                case enumGroupInvoiceType.ReceiptLenderCapital:// Lender thêm vốn
                    $(".Class_Invoice_Lender").removeAttr("style"); // show thông tin lender   
                    $('#ModalCreateInvoice').modal('toggle');
                    break;
                case enumGroupInvoiceType.ReceiptLenderInsurance:// Thu tiền bảo hiểm lender
                    $(".Class_Invoice_Lender").removeAttr("style"); // show thông tin lender    
                    $('#ModalCreateInvoice').modal('toggle');
                    break;
                case enumGroupInvoiceType.ReceiptLenderTopUp:// Thu tiền lender hoàn ứng 
                    $(".Class_Invoice_Lender").removeAttr("style"); // show thông tin lender   
                    $(".Class_Invoice_SearchInvoiceAdvanceSlipLender").removeAttr("style"); // show Class_Invoice_SearchInvoiceAdvanceSlipLender  
                    $('#ModalCreateInvoice').modal('toggle');
                    break;
                case enumGroupInvoiceType.PaySlipLenderWithdraw:// Lender rút vốn
                    $(".Class_Invoice_Lender").removeAttr("style"); // show thông tin lender    
                    $('#ModalCreateInvoice').modal('toggle');
                    break;
                case enumGroupInvoiceType.PaySlipFeeForBank:// Trả phí cho ngân hàng 
                    $('#ModalCreateInvoice').modal('toggle');
                    break;
                default:
                // code block
            }
            return endpoint;
        } catch (err) {
            return null;
        }
    } 
    var Searching = function () {
        isSearchLoan = false;
        var paramValue;
        let searchParams = new URLSearchParams(window.location.search);
        if (searchParams.has(baseCommon.InvoiceIndex.InvoiceID)) {
            paramValue = searchParams.get(baseCommon.InvoiceIndex.InvoiceID);
            if (paramValue != null && paramValue != "" && paramValue.trim() != "" && !paramValue.includes("{")) {
                $('#InvoiceID').val(parseFloat(paramValue));
            }
        }
        if (searchParams.has(baseCommon.InvoiceIndex.InvoiceType)) {
            paramValue = searchParams.get(baseCommon.InvoiceIndex.InvoiceType);
            if (paramValue != null && paramValue != "" && paramValue.trim() != "" && !paramValue.includes("{")) {
                $('#InvoiceType').val(paramValue).change();
            }
        }
        if (searchParams.has(baseCommon.InvoiceIndex.InvoiceSubType)) {
            paramValue = searchParams.get(baseCommon.InvoiceIndex.InvoiceSubType);
            if (paramValue != null && paramValue != "" && paramValue.trim() != "" && !paramValue.includes("{")) {
                paramValue = parseFloat(paramValue);
                $('#InvoiceSubType').val(parseFloat(paramValue)).change();
            }
        }
        if (searchParams.has(baseCommon.InvoiceIndex.ShopID)) {
            paramValue = searchParams.get(baseCommon.InvoiceIndex.ShopID);
            if (paramValue != null && paramValue != "" && paramValue.trim() != "" && !paramValue.includes("{")) {
                $('#ShopID').val(parseFloat(paramValue)).change();
            }
        }
        if (searchParams.has(baseCommon.InvoiceIndex.BankCardID)) {
            paramValue = searchParams.get(baseCommon.InvoiceIndex.BankCardID);
            if (paramValue != null && paramValue != "" && paramValue.trim() != "" && !paramValue.includes("{")) {
                $('#BankCardID').val(parseFloat(paramValue)).change();
            }
        }
        if (searchParams.has(baseCommon.InvoiceIndex.Status)) {
            paramValue = searchParams.get(baseCommon.InvoiceIndex.Status);
            if (paramValue != null && paramValue != "" && paramValue.trim() != "" && !paramValue.includes("{")) {
                $('#Status').val(paramValue).change();
            }
        }
        if (searchParams.has(baseCommon.InvoiceIndex.FromDate)) {
            paramValue = searchParams.get(baseCommon.InvoiceIndex.FromDate);
            if (paramValue != null && paramValue != "" && paramValue.trim() != "" && !paramValue.includes("{")) {
                $('#fromDate').val(paramValue);
            }
        }
        if (searchParams.has(baseCommon.InvoiceIndex.ToDate)) {
            paramValue = searchParams.get(baseCommon.InvoiceIndex.ToDate);
            if (paramValue != null && paramValue != "" && paramValue.trim() != "" && !paramValue.includes("{")) {
                $('#toDate').val(paramValue);
            }
        }
        isSearchLoan = true;
        LoadData();
    }
    return {
        Init: Init,
        LoadData: LoadData,
        ShowModal: ShowModal,
        submitFormSave: submitFormSave,
        GetShop: GetShop,
        CreateInvoice: CreateInvoice
    };
}