﻿var managerBankCardList = new function () {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var bankCardID = $('#BankCardID').val().trim();
                if (typeof bankCardID == undefined || bankCardID == "") {
                    bankCardID = -1;
                }
                var data = {
                    BankCardID: parseInt(bankCardID),
                    GeneralSearch: $('#txt_search_bank').val().trim(),
                    Status: parseInt($('#sl_search_status').val().trim()),
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxDone('POST', baseCommon.Endpoint.ListBankCard, JSON.stringify(data), function (res) {
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    if (res.Result == 1) {
                        options.success(res.Data);
                    }
                }, "Thao tác thất bại, vui lòng thử lại sau!");
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = response.RecordsTotal;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function (objQuery) {
        $("#dv_result_bank").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            selectable: "row",
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 1) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'BankCode',
                    width: 150,
                    title: 'Thẻ Ngân hàng',
                    template: function (row) {
                        var html = `<a class="kt-font-primary kt-font-boldest kt-link" style="cursor:pointer" href="javascript:;"  title="Sửa" data-toggle="modal" data-target="#modal_edit_bankCard" onclick="managerBankCardList.ShowModal(${row.BankCardID},'${row.BranchName}','${row.NumberAccount}','${row.AccountName}','${row.AccountHolderName}','${row.AliasName}',${row.Status},${row.BankID},${row.TotalMoney})">\
                                    ${row.AliasName} - ${row.NumberAccount}\
                                    <br/> <span class="item-desciption" > ${row.BankCode} </span >\
                                    </a>`;
                        return html;
                    }
                },
                {
                    field: 'BranchName',
                    width: 100,
                    title: 'Chi nhánh',
                    textAlign: 'left'
                },
                {
                    field: 'AccountHolderName',
                    width: 150,
                    title: 'Chủ tài khoản',
                },
                {
                    field: 'AliasName',
                    width: 150,
                    title: 'Tên định danh tài khoản',
                },
                {
                    field: 'NumberAccount',
                    width: 120,
                    title: 'Số tài khoản',
                },
                {
                    field: 'TotalMoney',
                    width: 150,
                    title: 'Số tiền (VNĐ)',
                    attributes: { style: "text-align:right;" },
                    headerAttributes: { style: "text-align: right" },
                    template: function (row) {
                        if (row.TotalMoney !== 0 || row.TotalMoney !== null) {
                            return '<span>' + formatCurrency(row.TotalMoney) + '</span>';
                        } else {
                            return '<span>0</span>';
                        }
                    },
                },
                {
                    field: 'CreateOn',
                    title: 'Ngày tạo',
                    template: function (row) {
                        // callback function support for column rendering
                        return formattedDate(row.CreateOn);
                    },
                },
                {
                    field: 'TypePurpose',
                    title: 'Mục đích',
                    textAlign: 'center',
                    width: 160,
                    // callback function support for column rendering
                    template: function (row) {
                        if (row.TypePurpose == '' || row.TypePurpose == null) {
                            return ''
                        }
                        if (row.TypePurpose == 1) {
                            return '<span>Dùng giải ngân</span>';
                        } else if (row.TypePurpose == 2) {
                            return '<span>Thanh toán gốc và lãi</span>';
                        } else if (row.TypePurpose == 3) {
                            return '<span>Cả 2 việc trên</span>';
                        } else if (row.TypePurpose == 4) {
                            return '<span>Giải ngân tự động</span>';
                        }
                    },
                },
                {
                    field: 'Status',
                    title: 'Trạng thái',
                    attributes: { style: "text-align:center;" },
                    headerAttributes: { style: "text-align: center" },
                    // callback function support for column rendering
                    template: function (row) {
                        if (row.Status == 1) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Hoạt Động</span>';
                        } else if (row.Status == 0) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Khóa</span>';
                        }
                    },
                },
                {
                    field: 'Action',
                    title: 'Hành động',
                    sortable: false,
                    width: 160,
                    template: function (row, index, datatable) {
                        var html = '';
                        return '\
                          <button class="btn btn-primary btn-icon btn-sm" title="Sửa" data-toggle="modal" data-target="#modal_edit_bankCard" onclick="managerBankCardList.ShowModal('+ row.BankCardID + ',\'' + row.BranchName + '\',\'' + row.NumberAccount + '\',\'' + row.AccountName + '\',\'' + row.AccountHolderName + '\',\'' + row.AliasName + '\',' + row.Status + ',' + row.BankID + ',' + row.TotalMoney + ',' + row.TypePurpose + ')">\
                            <i class="fa fa-edit" ></i>\
                            </button>\
                            <button class="btn btn-primary btn-icon btn-sm" title="Chọn cửa hàng" data-toggle="modal" data-target="#bankCard_shop" onclick="managerBankCardList.ShowModalBankCardOfShop('+ row.BankCardID + ',\'' + row.BranchName + '\')">\
                            <i class="fa fa-store" ></i>\
                            </button>';
                        return html;
                    }
                }
            ]
        });
    };

    var GetDataPopupBankCard = function () {
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetDetailBankCard + 0, "", function (respone) {
            if (respone.Result == 1) {
                loanInfo = respone.Data;
                baseCommon.DataSource("#txt_bankID", "Tất cả", "Value", "Text", 0, respone.Data);
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        }, "Lấy danh sách ngân hàng không thành công vui lòng thử lại!");
    }
    var LoadData = function () {
        baseCommon.ButtonSubmit(false, '#btnGetData');
        var grid = $("#dv_result_bank").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    }
    var Init = function () {
        LoadData();
        $("#txt_search_bank").on('change', function (e) {
            LoadData();
        });
        $("#sl_search_status").on('change', function (e) {
            LoadData();
        });
        $("#BankCardID").on('change', function (e) {
            LoadData();
        });
        $("#btnGetData").on('click', function (e) {
            LoadData();
        });
        submitFormSave();
        GetDataPopupBankCard();
        $("#sl_search_shopID").on('change', function (e) {
            Getstasticloanstatusbyshop(this.value);
            $('#btnGetData').click();
        });
        GetShop();
    };
    var ShowModal = function (id = 0, branch_name = '', account_number = '', account_name = '', account_holder = '', alias_name = '', status = 1, bankID = 0, totalMoney = 0, typePurpose = 1) {
        var form = $('#btn_save_bank').closest('form');
        form.validate().destroy();
        $("#hdd_edit_BankID").val(id);

        if (bankID == 'null' || bankID == undefined) {
            $('#txt_bankID').val(0).change();
        } else {
            $('#txt_bankID').val(bankID).change();
        }
        if (typePurpose == 'null' || typePurpose == undefined) {
            $('#txt_typePurpose').val(1).change();
        } else {
            $('#txt_typePurpose').val(typePurpose).change();
        }

        if (branch_name == 'null' || branch_name == undefined) {
            $('#txt_branchName').val('');
        } else {
            $('#txt_branchName').val(branch_name);
        }

        if (account_number == 'null' || account_number == undefined) {
            $('#txt_account_number').val('');
        } else {
            $('#txt_account_number').val(account_number);
        }

        if (account_name == 'null' || account_name == undefined) {
            $('#txt_account_name').val('');
        } else {
            $('#txt_account_name').val(account_name);
        }

        if (account_holder == 'null' || account_holder == undefined) {
            $('#txt_account_holder_name').val('');
        } else {
            $('#txt_account_holder_name').val(account_holder);
        }

        if (alias_name == 'null' || alias_name == undefined) {
            $('#txt_aliasName').val('');
        } else {
            $('#txt_aliasName').val(alias_name);
        }
        if (totalMoney == null || totalMoney == undefined) {
            $('#txt_total_money').val(0);
        } else {
            $('#txt_total_money').val(formatCurrency(totalMoney));
        }

        if (status == 1) {
            $('#kt_switch_2').attr('checked', 'checked');
        } else {
            $('#kt_switch_2').removeAttr('checked');
        }
        if (id == 0)//form tạo mới
        {
            $('#header_edit_bankCard').text('Thêm mới');
            $('#btn_save_bank').html('Thêm mới');
        }
        else {
            $('#header_edit_bankCard').text('Cập nhật');
            $('#btn_save_bank').html('Cập nhật');
        }
        $('#modal_edit_bankCard').modal('toggle');
    }
    var submitFormSave = function () {
        $('#btn_save_bank').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            $.validator.addMethod('minNumberMoney', function (value, el, param) {
                var numTotalMoney = parseInt(value.replace(/[^\d\.]/g, ''));
                return numTotalMoney > param;
            });
            form.validate({
                rules: {
                    NumberAccount: {
                        required: true,
                    },
                    AccountName: {
                        required: true,
                    },
                    AccountHolderName: {
                        required: true,
                    },
                    AliasName: {
                        required: true,
                    },
                    BranchName: {
                        required: true,
                    },
                    TotalMoney: {
                        required: true,
                        maxlength: 15
                    }
                },
                messages: {
                    NumberAccount: {
                        required: "Vui lòng nhập Số tài khoản",
                    },
                    AccountName: {
                        required: "Vui lòng nhập Tên tài khoản",
                    },
                    AccountHolderName: {
                        required: "Vui lòng nhập Chủ tài khoản",
                    },
                    AliasName: {
                        required: "Vui lòng nhập Tên ngân hàng",
                    },
                    BranchName: {
                        required: "Vui lòng nhập Tên chi nhánh",
                    },
                    TotalMoney: {
                        required: "Vui lòng nhập Số tiền",
                        maxlength: "Số tiền quá lớn",
                    }
                }
            });

            if (!form.valid()) {
                return;
            }
            //var totalMoney = parseFloat($("#txt_total_money_hdd").val()??"0");
            var regex = new RegExp("^[a-zA-Z0-9 ]+$");
            var aliasName = $("#txt_aliasName").val();
            var account_holder = $("#txt_account_holder_name").val();
            if (!aliasName.match(regex)) {
                baseCommon.ShowErrorNotLoad("Tên tài khoản không được nhập ký tự đặc biệt,dấu!");
                return;
            } else if (!account_holder.match(regex)) {
                baseCommon.ShowErrorNotLoad("Chủ tài khoản không được nhập ký tự đặc biệt,dấu!");
                return;
            }

            var isActive = $('#kt_switch_2').prop('checked') ? 1 : 0;
            var bankId = $("#txt_bankID").val();
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            var model = {
                Status: isActive,
                // TotalMoney: totalMoney,
                BankID: bankId
            };
            form.ajaxSubmit({
                url: '/Bank/AddOrUpDate',
                method: 'POST',
                data: model,
                success: function (response, status, xhr, $form) {
                    if (response.result == 1) {
                        showMesage("success", response.message);
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        $('#btn_exit').click()
                        LoadData()
                    } else {
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        showMesage("danger", response.message);
                    }
                }
            });
        });
    }
    var GetShop = function () {
        var data = { "type": 1, "generalSearch": "" };
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetLenderSearch, data, function (respone) {
            if (respone.Result == 1) {
                baseCommon.DataSource("#actionBank_shop", "Tất cả", "ID", "FullName", 0, respone.Data);
            }
        }, "Chưa lấy được danh sách cửa hàng!");
    }
    var Save_BankCardOfShop = function () {
        if ($('#actionBank_shop').val() == -111) {
            baseCommon.ShowErrorNotLoad("Không thể nhận trường hợp Tất cả");
            return;
        }
        var bankID = $("#hdd_BankCardIDOfShop_BankCardID").val();
        var bankName = $("#hdd_BankCardnameOfShop_BankCardName").val();

        var sl_shop = $('#actionBank_shop').val();
        var arrLstShop = [];
        for (var i = 0; i < sl_shop.length; i++) {
            if (sl_shop[i] == -111) {
                baseCommon.ShowErrorNotLoad("Không thể nhận trường hợp Tất cả");
                return;
            }
            arrLstShop.push({
                "ShopID": parseInt(sl_shop[i]),
                "ShopName": $("#actionBank_shop option:selected").eq(i).text()
            })
        }
        var modelBankCardOfShop = {
            "BankCardID": parseInt(bankID),
            "BankCardName": bankName,
            "LstShop": arrLstShop,
        };
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.CreateBankCardOfShop, JSON.stringify(modelBankCardOfShop), function (respone) {
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                setTimeout(function () {
                    $('#exit_modal').click();
                    LoadData()
                }, 1000);

            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        }, "Gán cửa hàng không thành công vui lòng thử lại!");
    }
    var GetBankCardOfShopID = function (bankCardID) {
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetShopOfBankCardID + bankCardID, "", function (respone) {
            if (respone.Result == 1) {
                var arrShopID = [];
                for (var i = 0; i < respone.Data.length; i++) {
                    arrShopID.push([
                        (respone.Data[i].ShopID),
                    ])
                }
                $("#actionBank_shop").val(arrShopID).change()
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        }, "Lấy dữ liệu không thành công vui lòng thử lại!");
    }
    var ShowModalBankCardOfShop = function (BankCardID = 0, BankCardName = '') {
        var form = $('#button_bankcardOfShop').closest('form');
        form.validate().destroy();
        GetBankCardOfShopID(BankCardID);
        $("#hdd_BankCardIDOfShop_BankCardID").val(BankCardID);

        $("#hdd_BankCardnameOfShop_BankCardName").val(BankCardName);
        if (BankCardID == 0)//form tạo mới
        {
            $('#actionBank_shop').val([-111]).change();
        }
        else {
            $('#button_bankcardOfShop').html('Cập nhật');
        }
    }
    $(document).on('keyup', '#txt_total_money', function (e) {
        var loValue = formatCurrency($(this).val().replace(/,/g, '').replace(/\./g, ''));
        if (loValue.length > 15) {
            $('#btn_save_bank').click();
        }
        $("#txt_total_money").val(loValue);
        $("#txt_total_money_hdd").val($("#txt_total_money").val().replace(/,/g, '').replace(/\./g, ''));

    });
    function formatCurrency(num) {
        num = num.toString().replace(/\$|\,/g, '');
        if (isNaN(num))
            num = "0";
        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        cents = num % 100;
        num = Math.floor(num / 100).toString();
        if (cents < 10)
            cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
            num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                num.substring(num.length - (4 * i + 3));
        return (((sign) ? '' : '-') + num);
    }
    return {
        Init: Init,
        LoadData: LoadData,
        ShowModal: ShowModal,
        submitFormSave: submitFormSave,
        GetDataPopupBankCard: GetDataPopupBankCard,
        GetShop: GetShop,
        Save_BankCardOfShop: Save_BankCardOfShop,
        ShowModalBankCardOfShop: ShowModalBankCardOfShop,
    };
}

