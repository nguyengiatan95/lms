﻿var recordGrid, recordGridDetail = 0 ;
var bank = new function () {
    var invoiceType_, bankCardID_; 
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var bankCardID = $('#BankCardID').val().trim();
                if (typeof bankCardID == undefined || bankCardID == "") {
                    bankCardID = 0;
                }
                var pageIndex = options.data.page; 
                var data = {
                    BankCardID: parseFloat(bankCardID),
                    FromDate: $('#fromDate').val().trim(),
                    ToDate: $('#toDate').val().trim(),
                    PageIndex: pageIndex,
                    PageSize: options.data.pageSize
                }
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.ReportInOutMoneyBankDate, JSON.stringify(data), function (res) {
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    if (res.Result != 1) {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                    options.success(res);
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        }
        ,
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function (objQuery) {
        $("#grid").kendoGrid({ 
            dataSource: dataSource,
            //pageable: {
            //    pageSizes: [20, 30, 50, 100]
            //},
            resizable: true,
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            //dataBound: function () {
            //    for (var i = 0; i < this.columns.length; i++) {
            //        if (i != 3 || true) {
            //            this.autoFitColumn(i);
            //        }
            //    }
            //}, 
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                }, 
                {
                    field: 'BankCardName',
                    title: 'Thẻ ngân hàng',
                    textAlign: 'center',
                    template: function (row) {
                        return "<b> " + row.BankCardName + " </b>"
                    }
                },
                {
                    field: 'MoneyBeginDate',
                    title: 'Số dư đầu ngày',
                    textAlign: 'right',
                    template: function (row) {
                        if (row.MoneyBeginDate == 0) {
                            return '-';
                        }
                        var html = `${html_money(row.MoneyBeginDate)}`;
                        return html;
                    }
                },
                {
                    field: 'MoneyInBound',
                    title: 'Tổng tiền thu về',
                    textAlign: 'right',
                    template: function (row) {
                        var html = '';
                        if (row.MoneyInBound == 0) {
                            return '-';
                        }
                        return `
                          <a href="#modal_BankCard_Details_Invoice" title="Chi tiết" data-toggle="modal" onclick="bank.ShowDetailsInvoiceBankCard(${row.BankCardID},'${row.BankCardName}',1)">\
                            ${html_money(row.MoneyInBound)}
                            </a>`;
                        return html;
                    }
                },
                {

                    field: 'MoneyOutBound',
                    title: 'Tổng tiền chi trả',
                    textAlign: 'right',
                    template: function (row) {
                        if (row.MoneyOutBound == 0) {
                            return '-';
                        }
                        return `
                          <a title="Chi tiết"  data-toggle="modal" href="#modal_BankCard_Details_Invoice" onclick="bank.ShowDetailsInvoiceBankCard(${row.BankCardID},'${row.BankCardName}',2)">\
                            ${html_money(row.MoneyOutBound)}
                            </a>`;
                        return html;
                    }
                },
                {
                    field: 'TotalMoneyTransaction',
                    title: 'Tổng tiền giao dịch',
                    textAlign: 'right',
                    template: function (row) {
                        if (row.TotalMoneyTransaction == 0) {
                            return '-';
                        }
                        var html = `${html_money(row.TotalMoneyTransaction)}`;
                        return html;
                    }
                },
                {

                    field: 'MoneyEndDate',
                    title: 'Số dư cuối ngày',
                    textAlign: 'right',
                    template: function (row) {
                        if (row.MoneyEndDate == 0) {
                            return '-';
                        }
                        var html = `${html_money(row.MoneyEndDate)}`;
                        return html;
                    }
                },
                {
                    field: 'Action',
                    title: 'Hành động',
                    sortable: false,
                    width: 160,
                    template: function (row, index, datatable) {
                        var html = '';
                        return '\
                          <button class="btn btn-success btn-icon btn-sm" title="Xuất Excel" onclick="bank.ExportExport(' + row.BankCardID + ',-1)">\
                            <i class="fa fa-file-download" ></i>\
                            </button>';
                        return html;
                    }
                }
            ]
        });
    }; 
    var LoadData = function () {
        baseCommon.ButtonSubmit(false, '#btnGetData');
        var grid = $("#grid").data("kendoGrid");
        if (grid != null) {
            $("#grid").data("kendoGrid").dataSource.read();
        } else {
            InitGrid();
        } 
    }
    var Init = function () { 
        $('#fromDate,#toDate').datepicker({
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            orientation: "bottom left",
            format: 'dd-mm-yyyy',
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        }); 
        $("#btnGetData").on('click', function (e) {
            LoadData();
        });
        $("#BankCardID,#fromDate,#toDate").on('change', function (e) { 
            LoadData();
        });   
        LoadData();
    };

    var ShowDetailsInvoiceBankCard = function (bankCardID, bankCardName, invoiceType) {
        invoiceType_ = invoiceType;
        bankCardID_ = bankCardID; 
        if (invoiceType == 1) {
            $("#title_BankCard_Details_Invoice").html("Chi tiết phiếu thu thẻ " + bankCardName);
        } else {
            $("#title_BankCard_Details_Invoice").html("Chi tiết phiếu chi thẻ " + bankCardName);
        } 
        setTimeout(function () {
            DetailsInvoiceBankCard();
        }, 500);
    }
    ///////////////////////////////////////////////////////////////
    var dataSourceDetail = new kendo.data.DataSource({
        transport: {
            read: function (options) { 
                if (typeof bankCardID_ == undefined || bankCardID_ == "") {
                    bankCardID = 0;
                }
                var pageIndexDetail = options.data.page; 
                var data = {
                    InvoiceType: invoiceType_,
                    BankCardID: parseFloat(bankCardID_),
                    FromDate: $('#fromDate').val().trim(),
                    ToDate: $('#toDate').val().trim(),
                    PageIndex: pageIndexDetail,
                    PageSize: options.data.pageSize
                }
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.InvoiceInfosByBankCard, JSON.stringify(data), function (res) { 
                    if (res.Result != 1) {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                    options.success(res);
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = 0;
                if (response != null && response.Data != null && response.Data.RecordsTotal != null) {
                    total = response.Data.RecordsTotal;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                var data = [];
                if (response != null && response.Data != null && response.Data.Data != null) {
                    data = response.Data.Data;
                }
                return data; 
            },
        }
        ,
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGridDetail = function (objQuery) {
        $("#dv_result_details").kendoGrid({
            dataSource: dataSourceDetail,
            pageable: {
                pageSizes: [20, 30, 50, 100]
            },
            resizable: true,
            dataBinding: function () {
                recordGridDetail = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 3 || true) {
                        this.autoFitColumn(i);
                    }
                }
            }, 
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGridDetail #"
                },
                {
                    field: 'BankName',
                    title: 'Thẻ ngân hàng', 
                    template: function (row) {
                        var html = '<b>' + row.BankName + '</b>';
                        if (row.InvoiceID != 0) {
                            return html;
                        }
                        return "";

                    }
                },
                {
                    field: 'TransactionDate',
                    title: 'Ngày giao dịch',
                    textAlign: 'center',
                    template: function (row) {
                        var date = moment(row.TransactionDate);
                        var html = '<span class="" >' + date.format("DD/MM/YYYY HH:mm") + '</span >';
                        if (row.InvoiceID != 0) {
                            return html;
                        }
                        return "";
                    }
                }, 
                {
                    field: 'InvoiceSubTypeName',
                    title: 'Loại phiếu',
                    textAlign: 'center',
                    template: function (row) {
                        if (row.InvoiceID != 0) {
                            return row.InvoiceSubTypeName;
                        } else {
                            if (row.InvoiceType == 1) {
                                return '<b style="font-size:15px">Tổng thu :</b>';
                            } else {
                                return '<b style="font-size:15px">Tổng chi:</b>';
                            }

                        }
                    }
                },
                {
                    field: 'TransactionMoney',
                    title: 'Số tiền',
                    textAlign: 'right',
                    template: function (row) { 
                        var html = '<b>' + html_money(row.TransactionMoney) + '</b>';
                        if (row.InvoiceID != 0) {
                            return html;
                        }
                        return '<b>' + html_money(row.SumTotalMoney) + '</b>';

                    }
                },
                {
                    field: 'StatusName',
                    title: 'Trạng thái',
                    textAlign: 'left',
                },
                {

                    field: 'Note',
                    title: 'Ghi chú',
                    width: 300,
                },
                {
                    field: 'UserNameCreate',
                    title: 'Người tạo',
                    textAlign: 'center',
                    width: 120,
                }
            ]
        });
    }; 
    //////////////////////////////////////////
    var DetailsInvoiceBankCard = function (objQuery, bankCardName) {
        var grid = $("#dv_result_details").data("kendoGrid");
        if (grid != null) { 
            grid.dataSource.page(1);
        } else {
            InitGridDetail();
        }
    }

    var ExportExport = function (bankCardID = 0, InvoiceType = -1) {
        var fromDate = $('#fromDate').val().replace(/\//g, "-");
        if (typeof fromDate != "undefined" && fromDate != null && fromDate == "") {
            fromDate = fromDate.trim();
        }
        var toDate = $('#toDate').val().replace(/\//g, "-");
        if (typeof toDate != "undefined" && toDate != null && toDate == "") {
            toDate = toDate.trim();
        }
        window.location.href = "/Excel/ExcelBankCard?FromDate=" + fromDate + "&ToDate=" + toDate + "&BankCardID=" + bankCardID + "&InvoiceType=" + InvoiceType;
    };
    return {
        Init: Init,
        ShowDetailsInvoiceBankCard: ShowDetailsInvoiceBankCard,
        ExportExport: ExportExport,
    };
}