﻿var recordGrid = 0;
var smsManage = new function () {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var Status = $('#Status').val();
                if (Status == null || Status == "" || Status.trim() == "") {
                    Status = 0
                }
                var FromDate = $('#fromDate').val();
                var ToDate = $('#toDate').val();
                var data = {
                    Status: parseInt(Status),
                    FromDate: FromDate,
                    ToDate: ToDate,
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                }
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetLstSmsInfoByCondition, JSON.stringify(data), function (res) {
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    if (res.Result > 0) {
                        options.success(res.Data);
                    } else {
                        options.success(null);
                    }
                }, function (e) {
                    if (e.status == 401) {
                        window.open(LoginUrl, "_self");
                    }
                    baseCommon.ShowErrorNotLoad("Thao tác thất bại, vui lòng thử lại sau!");
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    options.success(null);
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = 0;
                if (response != null && typeof response.RecordsTotal != "undefined") {
                    total = response.RecordsTotal;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                var data = [];
                if (response != null && typeof response.Data != "undefined") {
                    data = response.Data;
                }
                return data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function () {
        $("#grid").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [10, 20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 3) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'Sender',
                    title: 'Nguồn gửi',
                    template: function (row) {
                        if (row == null) {
                            return "";
                        }
                        var date = moment(row.SmsreceivedDate);
                        var html = `${row.Sender}\
                                    <br/> <span class="item-desciption" >${ date.format("DD/MM/YYYY HH:mm")}</span > `;
                        return html;
                    }
                },
                {
                    field: 'BankCardAliasName',
                    title: 'STK'
                },
                {
                    field: 'SmsContent',
                    title: 'Nội dung',
                    width: 550
                },
                {
                    field: 'CustomerName',
                    title: 'Tên khách hàng'
                },
                {
                    field: 'IncreaseMoney',
                    title: 'Số tiền',
                    headerAttributes: { style: "text-align: right" },
                    attributes: { style: "text-align:right;" },
                    template: function (row) {
                        if (row == null || row.IncreaseMoney == null) {
                            return "";
                        }
                        return `${html_money(row.IncreaseMoney)}\
                                    <br/> <span class="item-desciption" > ${row.BankCardAliasName} </span >`;
                    }
                },
                {
                    field: 'ShopName',
                    title: 'Cửa hàng',
                    attributes: { style: "text-align:center;" },
                },
                {
                    field: 'SmsStatusName',
                    title: 'Trạng thái',
                    template: function (row) {
                        if (row == null) {
                            return "";
                        }
                        var date = moment(row.ModifyDate);
                        var html = `${row.SmsStatusName}\
                                    <br/> <span class="item-desciption" >${ date.format("DD/MM/YYYY HH:mm")}</span > `;
                        return html;
                    }
                }
                //{
                //    field: 'SmsStatusName',
                //    title: 'Trạng thái',
                //    textAlign: 'center', 
                //    template: function (row) {
                //        if (row == null || row.Status == null) {
                //            return "";
                //        }
                //        if (row.Status >= 1) {
                //            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">' + row.StatusName + '</span>';
                //        } else {
                //            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + row.StatusName + '</span>';
                //        }
                //    }
                //}, 
                //{
                //    title: "Hành động",
                //    template: function (row) { 
                //        if (typeof row != "undefined" && row != null && row.Status != null && (row.Status == invoice_Status.WaitingBanking ||
                //            row.Status == invoice_Status.Pending ||
                //            row.Status == invoice_Status.WaitingConfirm)) { 
                //            const searchRegExp = new RegExp("'|\"", 'g');
                //            var des = row.Description == null ? '' : row.Description.replace(searchRegExp, " ");
                //            var html = `<a class="btn btn-success btn-icon btn-sm" href="javascript:;"  title="Xác nhận" data-toggle="modal" data-target="#ModalInvoice" onclick="verifyInvoice.ShowModal(${row.InvoiceID} ,${row.TransactionMoney},'${row.StatusName}','${row.TransactionDate}',' ${row.UserNameCreate}','${des}',${row.Status},'${row.DestinationName == null ? '' : row.DestinationName.trim()}',' ${row.SourceName == null ? '' : row.SourceName.trim()}')"> \
                //                    <i class="fa fa-check" ></i></a>`;
                //            return html;
                //        } else {
                //            return "";
                //        } 
                //    }
                //}
            ]
        });
    };
    var Init = function () {
        $("#Status,#fromDate,#toDate").on('change', function (e) {
            GridRefresh();
        });
        baseCommon.ButtonSubmit(false, '#btnGetData');
        InitGrid();
    };
    var GridRefresh = function () {
        baseCommon.ButtonSubmit(false, '#btnGetData');
        var grid = $("#grid").data("kendoGrid")
        grid.dataSource.page(1);
    };
    return {
        Init: Init,
        GridRefresh: GridRefresh
    };
}
$(document).ready(function () {
    smsManage.Init();
});