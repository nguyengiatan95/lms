﻿var recordGrid = 0;
var recordGridCustomer = 0; 
var viewer, invoiceStatus, gridSelectedRowData;
var listImg = [];
var verifyInvoice = new function () {
    function Grid_change(e) {
        var selectedRows = this.select();
        var selectedDataItems = [];
        for (var i = 0; i < selectedRows.length; i++) {
            var dataItem = this.dataItem(selectedRows[i]);
            selectedDataItems.push(dataItem);
        }
        // selectedDataItems contains all selected data items
        gridSelectedRowData = selectedDataItems[0];
        ShowModal();
    }
    var dataSourceBankCardID = new kendo.data.DataSource({
        transport: {
            read: function (options) { 
                baseCommon.AjaxDone("GET",baseCommon.Endpoint.GetBankCard + statusCommon.Default,"", function (respone) {
                    if (respone.Result == 1) {
                        options.success(respone.Data); 
                        baseCommon.DataSource("#BankCardID_Verify", "Chọn thẻ ngân hàng", "Value", "Text", 0, respone.Data);
                        baseCommon.DataSource("#BankCardID", "Chọn thẻ ngân hàng", "Value", "Text", 0, respone.Data); 
                    }
                }); 
            }
        } 
    }); 
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) { 
                var InvoiceType = baseCommon.Option.All; 
                var InvoiceSubType = enumGroupInvoiceType.ReceiptCustomerConsider;
                var ShopID = $('#ShopID').val();
                if (ShopID == null || ShopID == "" || ShopID.trim() == "") {
                    ShopID = baseCommon.Option.All;
                }
                var BankCardID = $('#BankCardID_Verify').val();
                if (BankCardID == null || BankCardID == "" || BankCardID.trim() == "") {
                    BankCardID = baseCommon.Option.All;
                }
                var Status = $('#Status').val();
                if (Status == null || Status == "" || Status.trim() == "") {
                    Status = baseCommon.Option.All;
                }
                var FromDate = $('#fromDate').val();
                var ToDate = $('#toDate').val();
                var pageIndex = options.data.page; 
                var data = {
                    InvoiceType: InvoiceType,
                    InvoiceSubType: InvoiceSubType,
                    ShopID: parseFloat(ShopID),
                    BankCardID: parseInt(BankCardID),
                    Status: parseInt(Status),
                    FromDate: FromDate,
                    ToDate: ToDate,
                    PageIndex: pageIndex,
                    PageSize: options.data.pageSize
                } 
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetInvoiceInfosByCondition, JSON.stringify(data), function (res) {
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    if (res.Result == 1) {  
                        options.success(res.Data); 
                    }
                }, "Thao tác thất bại, vui lòng thử lại sau!"); 
            } 
        },
        serverPaging: true,
        schema: {
            total: function (response) { 
                return response.RecordsTotal; // total is returned in the "total" field of the response
            },
            data: function (response) { 
                return response.Data; // total is returned in the "total" field of the response
            },
        }, 
        pageSize: baseCommon.Option.Pagesize 
    });
    var InitGrid = function () {
        $("#grid").kendoGrid({
            dataSource: dataSource,  
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            change: Grid_change,
            selectable:"row", 
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 5 ) {
                        this.autoFitColumn(i);
                    } 
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #" 
                },
                {
                    field: 'InvoiceSubTypeName',
                    title: 'Loại phiếu', 
                    textAlign: 'left',  
                    template: function (row) {
                        if (row == null ) {
                            return "";
                        } 
                        var html = `${row.InvoiceSubTypeName} <br/> <span class="item-desciption" >${ row.InvoiceTypeName }</span >`;
                        return html;
                    } 
                }, {
                    field: 'TransactionMoney',
                    title: 'Số tiền',
                    textAlign: 'right', 
                    template: function (row) {
                        if (row == null || row.TransactionMoney == null) {
                            return "";
                        }
                        return `${html_money(row.TransactionMoney)}\
                                    <br/> <span class="item-desciption" > ${row.DestinationName} </span >`;
                    } 
                },
                {

                    field: 'SourceName',
                    title: 'Đối tượng thu/chi',
                    textAlign: 'left'
                },
                {
                    field: 'ShopName',
                    title: 'Cửa hàng',
                    textAlign: 'left'
                },
                {

                    field: 'Description',
                    title: 'Diễn giải',
                    width: 400,
                    textAlign: 'left' 
                },
                {
                    field: 'Status',
                    title: 'Trạng thái',
                    textAlign: 'center', 
                    template: function (row) { 
                        if (row == null || row.Status == null) {
                            return "";
                        }
                        if (row.Status >= 3) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">' + row.StatusName + '</span>';
                        }
                        else if (row.Status > 1 && row.Status < 3) {
                            return '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">' + row.StatusName + '</span>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + row.StatusName + '</span>';
                        }
                    }
                },
                {
                    field: 'TransactionDate',
                    title: 'Ngày giao dịch',
                    textAlign: 'left', 
                    template: function (row) {
                        if (row == null || row.TransactionDate == null) {
                            return "";
                        }
                        var date = moment(row.TransactionDate);
                        var html = date.format("DD/MM/YYYY HH:mm");
                        return html;
                    }
                },
                {
                    field: 'UserNameCreate',
                    title: 'Người/ngày tạo',
                    textAlign: 'left',
                    template: function (row) {
                        if (row == null || row.CreateDate == null) {
                            return "";
                        }
                        var date = moment(row.CreateDate);
                        var html = row.UserNameCreate + ' <br/> <span class="item-desciption" >' + date.format("DD/MM/YYYY") + '</span >';
                        return html;
                    }
                },   
                {
                    title: "Hành động",
                    template: function (row) { 
                        if (typeof row != "undefined" && row != null && row.Status != null && (row.Status == invoice_Status.WaitingBanking ||
                            row.Status == invoice_Status.Pending ||
                            row.Status == invoice_Status.WaitingConfirm)) {  
                            var html = `<span class="btn btn-success" ><i class="fa fa-check" ></i></span >`;
                            return html;
                        } else {
                            return "";
                        } 
                    }
                }
            ]
        }); 
    };
    var dataSourceCustomer = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var customerKeySearch = $('#CustomerKeySearch').val();
                //if (customerKeySearch == null || customerKeySearch == "" || customerKeySearch.trim() == "") {
                //    baseCommon.ShowErrorNotLoad("Bạn chưa nhập tên KH hoặc CMT!");
                //    return;
                //} 
                var pageIndex = options.data.page;
                //if (code != null && code != "") {
                //    code = code.trim(); 
                //    pageIndex = 1;
                //}  
                var modelCustomer = { 
                    CustomerKeySearch: customerKeySearch,
                    PageIndex: 1,//pageIndex,
                    PageSize: 1000//options.data.pageSize
                } 
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetLstLoanByFilterCustomer, JSON.stringify(modelCustomer), function (res) {
                    if (res.Result == 1) {
                        SetCustomerInfo(res.Data.Data[0]);
                        options.success(res.Data);
                    }
                },  "Thao tác thất bại, vui lòng thử lại sau!" ); 
            }
        },
        serverPaging: false,
        schema: {
            total: function (response) {
                return response.Data.length; // total is returned in the "total" field of the response
            },
            data: function (response) {
                return response.Data; // total is returned in the "total" field of the response
            },
        }
        ,
        batch: true,
        pageSize: 1//baseCommon.Option.Pagesize
    });
    var SetCustomerInfo = function (customerInfo) {
        if (typeof customerInfo == undefined || customerInfo == null || customerInfo == "") {
            return;
        } 
        $('#Invoice_CustomerName').html(customerInfo.CustomerName);
        $('#Invoice_OwnerShopName').html(customerInfo.OwnerShopName);
        $('#CustomerID').val(customerInfo.CustomerID);
        $('#LoanID').val(customerInfo.LoanID);
        $('#OwnerShopID').val(customerInfo.OwnerShopID);
        $('#LoanCreditIDOfPartner').val(customerInfo.LoanCreditIDOfPartner);
        $('#CustomerID').val(customerInfo.CustomerID); 
    };
    var CustomerInfo_Clear = function () { 
        $('#Invoice_CustomerName').html("");
        $('#Invoice_OwnerShopName').html("");
        $('#CustomerID').val(0);
        $('#LoanID').val(0);
        $('#OwnerShopID').val(0);
        $('#LoanCreditIDOfPartner').val(0);  
        $('#DestinationName').html('');
        $('#InvoiceID').val(0);
        $('#TransactionMoney').html(html_money(0));
        $('#StatusName').html('');
        $('#TransactionDate').html('');
        $('#UserNameCreate').val('');
        $('#Description').html('');

        $('#CustomerKeySearch').val('');
        var gridCustomerClear = $("#GridCustomer_Verify").data("kendoGrid");
        if (typeof gridCustomerClear != "undefined") {
            $("#GridCustomer_Verify").data("kendoGrid").dataSource.read(); 
        }
    };
    function GridCustomer_change(e) {
        var selectedRows = this.select();
        var selectedDataItems = [];
        for (var i = 0; i < selectedRows.length; i++) {
            var dataItem = this.dataItem(selectedRows[i]);
            selectedDataItems.push(dataItem);
        }
        // selectedDataItems contains all selected data items
        SetCustomerInfo(selectedDataItems[0]); 
    }
    var InitGridCustomer = function (objQuery) {
        $("#GridCustomer_Verify").kendoGrid({
            dataSource: dataSourceCustomer,
            change: GridCustomer_change, 
            selectable: "row",
            pageable: true,
            dataBinding: function () {
                recordGridCustomer = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    this.autoFitColumn(i);
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGridCustomer #"
                },
                {
                    field: "CustomerName",
                    title: "Tên KH"
                },
                {
                    field: "CustomerNumberCard",
                    title: "CMT"
                },
                {
                    field: "ContactCode",
                    title: "Mã TC"
                },
                {
                    field: "ContactStartDate",
                    title: "Ngày giải ngân",
                    template: '#= baseCommon.FormatDate(ContactStartDate,\'DD/MM/YYYY\')#',
                },
                {
                    field: "LoanInterestPaymentNextDate",
                    title: "Ngày phải đóng lãi",
                    template: '#= baseCommon.FormatDate(LoanInterestPaymentNextDate,\'DD/MM/YYYY\')#',
                },
                {
                    field: "LoanTotalMoneyCurrent",
                    title: "Số tiền gốc còn lại",
                    template: '#= html_money(LoanTotalMoneyCurrent)#',
                    attributes: { style: "text-align:right;" }
                },
                //{
                //    field: "OwnerShopName",
                //    title: "Cửa hàng"
                //},
                {
                    field: "LoanTotalMoneyDisbursement",
                    title: "Tiền GN",
                    template: '#= html_money(LoanTotalMoneyDisbursement)#',
                    attributes: { style: "text-align:right;" }
                },
                {
                    field: "LoanCreditIDOfPartner",
                    title: "Mã HĐ",
                    template: '#= "HĐ-"+LoanCreditIDOfPartner#',
                },
                {
                    field: "ProductName",
                    title: "Sản phẩm",
                },
                //{
                //    field: "LoanTotalMoneyPaid",
                //    title: "Phải thanh toán",
                //    template: '#= html_money(LoanTotalMoneyPaid)#',
                //    attributes: { style: "text-align:right;" }
                //},
                {
                    field: "LoanStatusName",
                    title: "Trạng thái"
                },
                {
                    field: "CityName",
                    title: "Tỉnh/TP"
                }
            ]
        });
    };
    var Init = function () {
        $("#fromDate,#toDate,#ShopID,#BankCardID_Verify,#Status").on('change', function (e) {
            GridRefresh();
        });
        baseCommon.ButtonSubmit(false, '#btnGetData');
        $("#TransactionMoney").on('keyup', function (e) {
            var loValue = formatCurrency($(this).val().replace(/,/g, '').replace(/\./g, ''));
            if (loValue.length > 15) {
                baseCommon.ShowErrorNotLoad("Số tiền quá lớn!");
            }
            $("#TransactionMoney").val(loValue);
            var money = $("#TransactionMoney").val().replace(/,/g, '').replace(/\./g, '');
            $("#TransactionMoney_hdd").val(money); 
        });
        $("#Verify_BankCardID").kendoDropDownList({
            dataSource: dataSourceBankCardID,
            dataTextField: "Text",
            dataValueField: "Value",
            optionLabel: {
                Text: "Chọn thẻ ngân hàng",
                Value: 0
            }
        });
        InitGrid();  
        GetShop(); 
    }; 
    var GetShop = function () {
        var data = {
            "type": 1,"generalSearch":""};
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetLenderSearch, data, function (respone) {
            if (respone.Result == 1) {
                baseCommon.DataSource("#ShopID", "Tất cả", "ID", "FullName", 0, respone.Data);
            }
        }, "Chưa lấy được danh sách cửa hàng!");
    } 
    var GridRefresh = function () {
        baseCommon.ButtonSubmit(false, '#btnGetData');
        var grid = $("#grid").data("kendoGrid")
        grid.dataSource.page(1);
    };  
    var FindCustomer = function () { 
        $('#CustomerID').val(0); 
        var gridCustomer_Verify = $("#GridCustomer_Verify").data("kendoGrid");
        if (gridCustomer_Verify != null) {
            $("#GridCustomer_Verify").data("kendoGrid").dataSource.read();
        } else {
            InitGridCustomer();
        }
    }; 
    var ShowModal = function () {
        if (gridSelectedRowData == null || gridSelectedRowData.InvoiceID == 0) {
            baseCommon.ShowErrorNotLoad("Thao tác không thành công vui lòng thử lại sau!");
            return;
        }
        CustomerInfo_Clear(); 
        invoiceStatus = gridSelectedRowData.Status;
        ButtonActionShowHide(gridSelectedRowData.Status);
        $('#ModalInvoice').modal('toggle');
        $("#VerifyInvoiceHeaderId").html("Xác minh phiếu treo");
        $('#InvoiceID').val(gridSelectedRowData.InvoiceID); 
        $('#DestinationName').html(gridSelectedRowData.DestinationName);
        $('#Invoice_CustomerName').html(gridSelectedRowData.SourceName);
        $('#Invoice_OwnerShopName').html(gridSelectedRowData.ShopName);
        $('#TransactionMoney_hdd').val(gridSelectedRowData.TransactionMoney);
        //$('#TransactionMoney').html(TransactionMoney).keyup();
        $('#TransactionMoney').html(html_money(gridSelectedRowData.TransactionMoney));
        $('#StatusName').html(gridSelectedRowData.StatusName);
        var date = moment(gridSelectedRowData.TransactionDate);
        var html = date.format("DD/MM/YYYY HH:mm");
        $('#TransactionDate').html(html);
        $('#UserNameCreate').val(gridSelectedRowData.UserNameCreate);
        $('#Description').html(gridSelectedRowData.Description);
        $('#Note_Verify').val('');
        $('#images').html('');
        InitTabImage();
        setTimeout(function () { FindCustomer(); }, 500);
    }
    var Verify = function () {  
        baseCommon.ButtonSubmit(false,'#btn_Verify_invoice');
        var InvoiceID = $('#InvoiceID').val();  
        var CreateBy = baseCommon.GetUser().UserID;  
        var Note = $('#Note_Verify').val(); 
        if (typeof CreateBy == "undefined" || CreateBy == null || CreateBy == 0 || typeof InvoiceID == "undefined" || InvoiceID == null || InvoiceID == 0) {
            baseCommon.ShowErrorNotLoad("Cập nhật không thành công vui lòng thử lại sau!");
            baseCommon.ButtonSubmit(true, '#btn_Verify_invoice');
            return;
        }  
        if (typeof listImg == "undefined" || listImg == null || listImg.length == 0) {
            baseCommon.ShowErrorNotLoad("Bạn chưa upload chứng từ ảnh!");
            baseCommon.ButtonSubmit(true, '#btn_Verify_invoice');
            return;
        } 
        var model = {
            InvoiceID: parseFloat(InvoiceID),   
            UserIDCreate: CreateBy, 
            Note: Note,
            TypeVerifyInvoiceConsider: 0 // xác minh khách hàng
        };
        baseCommon.AjaxDone("POST",baseCommon.Endpoint.VerifyInvoiceCustomer, JSON.stringify(model), function (respone) {
            baseCommon.ButtonSubmit(true, '#btn_Verify_invoice');
            GridRefresh();
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                $('#ModalInvoice').modal('hide');
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            } 
        }); 
    }
    var InvoiceAction = function (action) { 
        ButtonAction(false);
        var endpoint = baseCommon.Endpoint.ReturnInvoiceConsiderCustomer;
        var CustomerID = $('#CustomerID').val(); 
        if (action == baseCommon.VerifyInvoice.DeleteInvoice) {
            endpoint = baseCommon.Endpoint.DeleteInvoice;
        } else if (action == baseCommon.VerifyInvoice.VerifyInvoiceConsiderCustomer) {
            endpoint = baseCommon.Endpoint.VerifyInvoiceConsiderCustomer;
            if (typeof CustomerID == "undefined" || CustomerID == null || CustomerID == "" || CustomerID == 0) {
                baseCommon.ShowErrorNotLoad("Bạn chưa chọn khách hàng!");
                ButtonAction(true);
                return;
            }
            if (typeof listImg == "undefined" || listImg == null || listImg.length == 0) {
                baseCommon.ShowErrorNotLoad("Bạn chưa upload chứng từ ảnh!");
                ButtonAction(true);
                return;
            } 
        }
        
        var InvoiceID = $('#InvoiceID').val(); 
        var CreateBy = baseCommon.GetUser().UserID;
        var Note = $('#Note_Verify').val();
        if (typeof Note == "undefined" || Note == null || Note == '') {
            baseCommon.ShowErrorNotLoad("Bạn chưa nhập nội dung!");
            ButtonAction(true);
            return;
        }
        if (typeof CreateBy == "undefined" || CreateBy == null || CreateBy == 0 || typeof InvoiceID == "undefined" || InvoiceID == null || InvoiceID == 0) {
            baseCommon.ShowErrorNotLoad("Cập nhật không thành công vui lòng thử lại sau!");
            ButtonAction(true);
            return;
        } 
        var model = {
            InvoiceID: parseFloat(InvoiceID), 
            UserIDCreate: CreateBy,
            Note: Note,
            CustomerID: parseFloat(CustomerID),
            TypeVerifyInvoiceConsider: 0 // xác minh khách hàng
        }; 
        baseCommon.AjaxDone("POST",endpoint, JSON.stringify(model), function (respone) {
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                $('#ModalInvoice').modal('hide');
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
            ButtonAction(true);
            GridRefresh(); 
        }, "Thao tác không thành công vui lòng thử lại");
    } 
    var ButtonAction = function (action) { 
        baseCommon.ButtonSubmit(action, '#btn_Return_invoice');
        baseCommon.ButtonSubmit(action, '#btn_Delete_invoice');
        baseCommon.ButtonSubmit(action, '#btn_VerifyInvoiceConsiderCustomer_invoice');
        baseCommon.ButtonSubmit(action, '#btn_Verify_invoice');
    } 
    var ButtonActionShowHide = function (status) {
        $('#btnBrowserImage').hide();
        $('#btn_Verify_invoice').hide();
        $('#btn_Delete_invoice').hide();
        $('#btn_Return_invoice').hide();
        $('#btn_VerifyInvoiceConsiderCustomer_invoice').hide(); 
        $('#CustomerDiv').hide();
        $('#BankCardID_div').hide();
        $('.Note_Verify').hide();
        switch (status) {
            case invoice_Status.WaitingBanking:// Chưa xác minh được ngân hàng 
                $('#btn_VerifyInvoiceConsiderCustomer_invoice').show();
                $('#btn_Delete_invoice').show();
                $('#btnBrowserImage').show();
                $('.Note_Verify').show();
                break;
            case invoice_Status.Pending://Treo chờ xác minh  
                $('#btn_VerifyInvoiceConsiderCustomer_invoice').show();
                $('#btn_Delete_invoice').show();
                $('#CustomerDiv').show();// cho tìm khách hàng 
                $('#btnBrowserImage').show();
                $('.Note_Verify').show();
                break;
            case invoice_Status.WaitingConfirm://Chờ kế toán xác nhận 
                $('#btn_Verify_invoice').show();
                $('#btn_Delete_invoice').show();
                $('#btn_Return_invoice').show();
                $('#btnBrowserImage').show();
                $('.Note_Verify').show();
                break;
            case invoice_Status.Confirmed://Kế toán xác nhận

                break;
            case invoice_Status.Deleted://Hủy

                break;
            default:
            // code block
        } 
    }
    var InitTabImage = function () { 
        listImg = [];
        ImageViewr();
    }
    var dataSourceGridLichSu = new kendo.data.DataSource({
        transport: {
            read: function (options) { 
                var _InvoiceID = $('#InvoiceID').val();
                if (typeof _InvoiceID == "undefined" || _InvoiceID == null || _InvoiceID == "" || _InvoiceID.trim() == "") { 
                    baseCommon.ShowErrorNotLoad("Thao tác thất bại, vui lòng thử lại sau!");
                    return;
                } 
                var pageIndex = options.data.page; 
                var data = {
                    InvoiceID: parseFloat(_InvoiceID),
                    InvoceSubType: 0, 
                    PageIndex: pageIndex,
                    PageSize: baseCommon.Option.Pagesize
                };  
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetHistoryCommentInvoice, JSON.stringify(data), function (res) {
                    if (res.Result != 1) {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                    options.success(res);
                    $("#LichSuLoading").hide();
                }, "Thao tác thất bại, vui lòng thử lại sau!");
            }
        },
        serverPaging: true,
        serverAggregates: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    baseCommon.ShowErrorNotLoad("Không có dữ liệu!");
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
            aggregate: function (response) {
                if (response.Data == null) {
                    baseCommon.ShowErrorNotLoad("Không có dữ liệu!");
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var GridLichSu = function (objQuery) {
        $("#GridLichSu").kendoGrid({
            dataSource: dataSourceGridLichSu, 
            pageable: true,
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 3) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'CreateDate',
                    title: 'Ngày thao tác',
                    textAlign: 'left',
                    template: function (row) {
                        if (row == null || row.CreateDate == null) {
                            return "";
                        }
                        var date = moment(row.CreateDate);
                        var html = date.format("DD/MM/YYYY HH:mm");
                        return html;
                    }
                }, 
                { 
                    field: 'UserName',
                    title: 'Người thao tác'
                },
                {
                    field: 'Note',
                    title: 'Nội dung', 
                    width:  850
                }
                ,
                {
                    field: 'StatusName',
                    title: 'Trạng thái',
                }
            ]
        });
    };
    var InitGridLichSu = function (isVisible) {
        $("#LichSuLoading").show();
        setTimeout(function () {GridLichSu(); }, 500); 
    }
    var OpenChooseFile = function () {
        $("#fileUploadFile").click();
    }
    var UploadFile = function () {
        $("#ImageLoading").show();
        var _InvoiceID = $('#InvoiceID').val();
        if (typeof _InvoiceID == "undefined" || _InvoiceID == null || _InvoiceID == "" || _InvoiceID.trim() == "") { 
            baseCommon.ShowErrorNotLoad("Thao tác thất bại, vui lòng thử lại sau!");
            return;
        }
        var formData = new FormData(); 
        var file = $('#fileUploadFile')[0].files;
        //kiểm tra file nhỏ hơn 10M
        for (var i = 0; i < file.length; i++) {
            if (file[i].size > 10240000 || file[i].fileSize > 10240000) {
                baseCommon.ShowErrorNotLoad("Dung lượng file không được quá 10MB"); 
                $('#fileUploadFile').val(''); 
                return;
            } else {
                formData.append('files', file[i]);
            }
        }   
        listImg.push("https://fengyuanchen.github.io/viewerjs/images/tibet-2.jpg");
        listImg.push("https://fengyuanchen.github.io/viewerjs/images/tibet-1.jpg");
        listImg.push("https://fengyuanchen.github.io/viewerjs/images/tibet-7.jpg");
        UpdateFileImage();
        //baseCommon.AjaxPostFrontEnd('/VerifyInvoice/UploadFile', formData, function (res) { 
        //    if (res.result > 0) { 
        //        if (res.data != null && res.data.length > 0) { 
        //            res.data.forEach(function (item, index) {
        //                
        //                listImg.push(item.imgSrc);
        //            });
        //            UpdateFileImage();
        //        }
        //    } else {
        //        baseCommon.ShowErrorNotLoad(res.message);
        //    }
        //}, function (e) {
        //    baseCommon.ShowErrorNotLoad("Thao tác thất bại, vui lòng thử lại sau!"); 
        //}); 
       
    }
    var UpdateFileImage = function () {
        var _InvoiceID = $('#InvoiceID').val();
        if (typeof _InvoiceID == "undefined" || _InvoiceID == null || _InvoiceID == "" || _InvoiceID.trim() == "") { 
            baseCommon.ShowErrorNotLoad("Thao tác thất bại, vui lòng thử lại sau!");
            return;
        } 
        var data = {
            InvoiceID: parseFloat(_InvoiceID),
            PathImg: listImg
        };
        baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.UpdatePathImg, JSON.stringify(data), function (res) {
            if (res.Result > 0) { 
                ImageViewr(); 
            } else {
                baseCommon.ShowErrorNotLoad(res.Message);
                $("#ImageLoading").hide();
            }
        });
    };
    function ImageViewr() {
        $("#ImageLoading").show();
        $('#images').html('');
        var InvoiceID = $('#InvoiceID').val();
        if (typeof InvoiceID == "undefined" || InvoiceID == null || InvoiceID == 0) {
            baseCommon.ShowErrorNotLoad("Thao tác không thành công vui lòng thử lại sau!");
            return;
        }
        var model = {
            InvoiceID: parseFloat(InvoiceID)
        };
        baseCommon.AjaxDone("POST",baseCommon.Endpoint.GetListImgInvoiceConsider, JSON.stringify(model), function (respone) { 
            if (respone.Result > 0 && respone.Data != null && respone.Data.length > 0) { 
                listImg = respone.Data;
                respone.Data.forEach(AddImage);
                if (viewer == null || typeof viewer == "undefined") {
                    viewer = new Viewer(document.getElementById('images'));
                } else {
                    viewer.update();
                }  
                $("#ImageLoading").hide();
            } else { 
                $("#ImageLoading").hide();
            }
        });  
    }
    function AddImage(item, index) { 
        if (typeof invoiceStatus != "undefined" && invoiceStatus != null
            && (invoiceStatus == invoice_Status.WaitingBanking || invoiceStatus == invoice_Status.Pending || invoiceStatus == invoice_Status.WaitingConfirm)) {
            var html = '<div class="col-md-2 imgViewer"><div class="row"><div class="col-md-12"><img class="imgClass" data-original="' + item + '" src="' + item + '" alt="Cuo Na Lake"></div></div><div class="row"><div class="col-md-12" style="text-align: center;     color: red;"><a style="color: red;" href="javascript:void(0)" title="Xóa ảnh" onclick="verifyInvoice.DeleteImage(\'' + item + '\')"> <i class="flaticon2-rubbish-bin-delete-button"></i>Xóa</a></div></div></div>'
            $('#images').append(html);
        } else {
            var htmlNoneDelete = '<div class="col-md-2 imgViewer"><div class="row"><div class="col-md-12"><img class="imgClass" data-original="' + item + '" src="' + item + '" alt="Cuo Na Lake"></div></div> </div>'
            $('#images').append(htmlNoneDelete);
        }
    } 
    function arrayRemove(arr, value) {

        return arr.filter(function (ele) {
            return ele != value;
        });
    }
    var DeleteImage = function (src) {
        $("#ImageLoading").show();
        listImg = arrayRemove(listImg, src);
        UpdateFileImage();
    } 
    return {
        Init: Init,
        GridRefresh: GridRefresh,
        ShowModal: ShowModal,
        Verify: Verify,
        FindCustomer: FindCustomer,
        InitGridLichSu: InitGridLichSu,
        InitTabImage: InitTabImage,
        InvoiceAction: InvoiceAction,
        UploadFile: UploadFile,
        OpenChooseFile: OpenChooseFile,
        UpdateFileImage: UpdateFileImage,
        DeleteImage: DeleteImage,
        CustomerInfo_Clear: CustomerInfo_Clear
    };
}
$(document).ready(function () {
    verifyInvoice.Init();
});