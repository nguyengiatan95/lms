﻿var datatable;
var recordGrid = 0;

var reportExtra = new function () {

    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                
                var search = $('#txt_search').val();
                var Date = $('#fromDate').val();
                var formatDate = moment(Date, "DD/MM/YYYY").format('DD-MM-YYYY');
                if (Date != null && Date != "" && Date.trim() != "") {
                    Date = formatDate;
                }
                var index = options.data.page
                if ($('#txt_search').val() !== '') {
                    index = 1
                }
                var data = {
                    QuerySearch: search,
                    DateSearch: Date,
                    PageIndex: index,
                    PageSize: options.data.pageSize
                }
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.ReportExtraMoneyCustomer, JSON.stringify(data), function (res) {
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    options.success(res);
                    if (res.Result == 1) {
                    } else {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                if (response != null && response.Total != null) {
                    return response.Total;
                }
                return 0; // total is returned in the "total" field of the response 
            },
            data: function (response) {
                var data = [];
                if (response != null && response.Data != null) {
                    data = response.Data;
                }
                return data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function () {
        $("#dv_result").kendoGrid({
            dataSource: dataSource,
            selectable: "row",
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            //dataBound: function () {
            //    for (var i = 0; i < this.columns.length; i++) {
            //        if (i != 1) {
            //            this.autoFitColumn(i);
            //        }
            //    }
            //},
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #",
                    width: 50,

                },
                {
                    field: 'CustomerName',
                    title: 'Tên khách hàng',
                    width: 200,
                    template: function (row) {
                        var html = `<span style="color:blue">${row.CustomerName}
                                    <br/> <span class="item-desciption"> ${row.NumberCard} </span>\
                                    </span>`;
                        return html;
                    }
                },
                {
                    field: 'Phone',
                    title: 'Số điện thoại',
                    textalign: 'left',
                    width: 200,

                    template: function (row) {
                        var html = "";
                        if (row.Phone == null) {
                            return html;
                        }
                        html = `<span>${row.Phone}</span>`;
                        return html;
                    }
                },
                {
                    field: 'Address',
                    title: 'Địa chỉ',
                    textalign: 'left',
                    width: 300,

                    template: function (row) {
                        var html = "";
                        if (row.Address == null) {
                            return html;
                        }
                        html = `<span>${row.Address}</span>`;
                        return html;
                    }
                },

                {
                    field: 'TotalMoney',
                    title: 'Số tiền thừa',
                    width: 200,

                    template: function (row) {
                        if (row == 0 || row == null || row.TotalMoney == null) {
                            return "";
                        }
                        return html_money(row.TotalMoney);
                    }
                },
            ]
        });
    };
    var GridRefresh = function () {
        baseCommon.ButtonSubmit(false, '#btnGetData');
        var grid = $("#dv_result").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };
    var Init = function () {
        GridRefresh();
        $("#btnGetData").on('click', function (e) {
            GridRefresh();
        });
        $("#fromDate,#txt_search").on('change', function (e) {
            GridRefresh();
        });
    };

    return {
        Init: Init,
        GridRefresh: GridRefresh
    };
}

