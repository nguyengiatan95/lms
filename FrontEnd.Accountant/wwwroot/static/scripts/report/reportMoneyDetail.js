﻿var recordGrid = 0;
var ReportVatLender = new function () {
    var gridPagesize = baseCommon.Option.Pagesize;
    var isExportExcell = false;
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var ContractCode = $('#ContractCode').val();
                if (typeof ContractCode == undefined || ContractCode == null || ContractCode == "" || ContractCode.trim() == "") {
                    ContractCode = "";
                }
                var LenderID = $('#LenderID').val();
                if (typeof LenderID == undefined || LenderID == null || LenderID == "" || LenderID == 0) {
                    LenderID = -111;
                }
                var ProductID = $('#ProductID').val();
                if (typeof ProductID == undefined || ProductID == null || ProductID == "") {
                    ProductID = -1;
                }
                var FromDate = $('#fromDate').val();
                var ToDate = $('#toDate').val();
                var pageIndex = options.data.page;
                var data = {
                    ContractCode: ContractCode.trim(),
                    ProductID: parseInt(ProductID),
                    LenderID: parseInt(LenderID),
                    FromDate: FromDate,
                    ToDate: ToDate,
                    PageIndex: pageIndex,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.ReportMoneyDetail, JSON.stringify(data), function (res) {
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    if (res.Result != 1) {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                    options.success(res);
                }, function (e) {
                    if (e.status == 401) {
                        window.open(LoginUrl, "_self");
                    }
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    baseCommon.ShowErrorNotLoad("Thao tác thất bại, vui lòng thử lại sau!");
                });
            }
        },
        serverPaging: true,
        serverAggregates: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                gridPagesize = total;
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    baseCommon.ShowErrorNotLoad("Không có dữ liệu!");
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
            aggregate: function (response) {
                if (response.Data == null) {
                    baseCommon.ShowErrorNotLoad("Không có dữ liệu!");
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: gridPagesize
    });
    var InitGrid = function () {
        $("#grid").kendoGrid({
            excel: {
                fileName: "Báo cáo tiền đang có Export.xlsx",
                filterable: true,
                allPages: true
            },
            excelExport: function (e) {
                var ContractCode = $('#ContractCode').val();
                if (typeof ContractCode == undefined || ContractCode == null || ContractCode == "" || ContractCode.trim() == "") {
                    ContractCode = "";
                }
                var Lender = $("#LenderID option:selected").text();
                var LenderID = $('#LenderID').val();
                if (LenderID == null || LenderID == "" || LenderID.trim() == "") {
                    Lender = "All";
                }
                var Product = $("#ProductID option:selected").text();
                var ProductID = $('#ProductID').val();
                if (ProductID == null || ProductID == "" || ProductID.trim() == "") {
                    Product = "All";
                }
                var FromDate = $('#fromDate').val();
                var ToDate = $('#toDate').val();
                if (FromDate != null && FromDate != "") {
                    FromDate = FromDate.replace(/\//g, "");
                }
                if (ToDate != null && ToDate != "") {
                    ToDate = ToDate.replace(/\//g, "");
                }
                var fileName = "Báo cáo tiền đang có " + ContractCode + "-" + Lender + "-" + Product + "-" + FromDate + "-" + ToDate + ".xlsx";
                e.workbook.fileName = fileName;
            },
            dataSource: dataSource,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function (e) {
                var dataItem = e.sender.dataSource._data[0];
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 3 || true) {
                        this.autoFitColumn(i);
                    }
                }
                if (typeof dataItem != "undefined" && dataItem != null) {
                    var SumMoneyFineLateLender = e.sender.element.find(".MoneyFineLateLender");
                    SumMoneyFineLateLender.append("<div>" + html_money(dataItem.SumMoneyFineLateLender) + "</div>");

                    var SumMoneyFineLateTima = e.sender.element.find(".MoneyFineLateTima");
                    SumMoneyFineLateTima.append("<div>" + html_money(dataItem.SumMoneyFineLateTima) + "</div>");

                    var SumMoneyFineOriginal = e.sender.element.find(".MoneyFineOriginal");
                    SumMoneyFineOriginal.append("<div>" + html_money(dataItem.SumMoneyFineOriginal) + "</div>");

                    var SumMoneyInterest = e.sender.element.find(".MoneyInterest");
                    SumMoneyInterest.append("<div>" + html_money(dataItem.SumMoneyInterest) + "</div>");

                    var SumMoneyOriginal = e.sender.element.find(".MoneyOriginal");
                    SumMoneyOriginal.append("<div> " + html_money(dataItem.SumMoneyOriginal) + "</div>");

                    var SumMoneyService = e.sender.element.find(".MoneyService");
                    SumMoneyService.append("<div>" + html_money(dataItem.SumMoneyService) + "</div>");

                    var SumTotalMoney = e.sender.element.find(".TotalMoney");
                    SumTotalMoney.append("<div>" + html_money(dataItem.SumTotalMoney) + "</div>");

                    var SumMoneyConsultant = e.sender.element.find(".MoneyConsultant");
                    SumMoneyConsultant.append("<div>" + html_money(dataItem.SumMoneyConsultant) + "</div>");
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'ContractCode',
                    title: 'Mã HĐ',
                    template: function (row) {
                        var html = "", param = {};
                        param[baseCommon.LoanIndex.search] = row.ContractCode;
                        html = `<a  target="_blank" class="kt-link kt-link--state kt-link--primary" href="${baseCommon.StringFormat(baseCommon.Endpoint.LoanIndex, param)}">${row.ContractCode}</a>`;
                        return html;
                    }
                }, {
                    field: 'CustomerName',
                    title: 'Khách hàng'
                },
                {
                    field: 'LenderCode',
                    title: 'Mã lender'
                },
                {
                    field: 'ActionName',
                    title: 'Tên bút toán',
                    footerTemplate: "<div style='float: right;'> Tổng</div>",
                    textAlign: 'left'
                },
                {
                    field: 'TotalMoney',
                    title: 'Tiền cho vay',
                    textAlign: 'right',
                    footerTemplate: "<div class='TotalMoney' style='float: right;'></div>",
                    template: function (row) {
                        if (row == null || row.TotalMoney == null) {
                            return "";
                        }
                        return '<span style="float:right">' + html_money(row.TotalMoney) + '</span>';
                    }
                },
                {
                    field: 'MoneyOriginal',
                    title: 'Tiền gốc thu về',
                    textAlign: 'right',
                    footerTemplate: "<div class='MoneyOriginal' style='float: right;'></div>",
                    template: function (row) {
                        if (row == null || row.MoneyOriginal == null) {
                            return "";
                        }
                        return '<span style="float:right">' + html_money(row.MoneyOriginal) + '</span>';
                    }
                },
                {
                    field: 'MoneyFineOriginal',
                    title: 'Tiền phạt tất toán sớm',
                    footerTemplate: "<div class='MoneyFineOriginal' style='float: right;'></div>",
                    textAlign: 'right',
                    template: function (row) {
                        if (row == null || row.MoneyFineOriginal == null) {
                            return "";
                        }
                        return '<span style="float:right">' + html_money(row.MoneyFineOriginal) + '</span>';
                    }
                },
                {
                    field: 'MoneyInterest',
                    title: 'Tiền lãi nhà đầu tư',
                    footerTemplate: "<div class='MoneyInterest' style='float: right;'></div>",
                    textAlign: 'right',
                    template: function (row) {
                        if (row == null || row.MoneyInterest == null) {
                            return "";
                        }
                        return '<span style="float:right">' + html_money(row.MoneyInterest) + '</span>';
                    }
                },
                {
                    field: 'MoneyService',
                    title: 'Phí dịch vụ',
                    textAlign: 'right',
                    footerTemplate: "<div class='MoneyService' style='float: right;'></div>",
                    template: function (row) {
                        if (row == null || row.MoneyService == null) {
                            return "";
                        }
                        return '<span style="float:right">' + html_money(row.MoneyService) + '</span>';
                    }
                },
                {
                    field: 'MoneyConsultant',
                    title: 'Phí tư vấn',
                    textAlign: 'right',
                    footerTemplate: "<div class='MoneyConsultant' style='float: right;'></div>",
                    template: function (row) {
                        if (row == null || row.MoneyConsultant == null) {
                            return "";
                        }
                        return '<span style="float:right">' + html_money(row.MoneyConsultant) + '</span>';
                    }
                },
                {
                    field: 'MoneyFineLateTima',
                    title: 'Tiền phạt trả chậm tima',
                    textAlign: 'right',
                    footerTemplate: "<div class='MoneyFineLateTima' style='float: right;'></div>",
                    template: function (row) {
                        if (row == null || row.MoneyFineLateTima == null) {
                            return "";
                        }
                        return '<span style="float:right">' + html_money(row.MoneyFineLateTima) + '</span>';
                    }
                },
                {
                    field: 'MoneyFineLateLender',
                    title: 'Tiền phạt trả chậm lender',
                    textAlign: 'right',
                    footerTemplate: "<div class='MoneyFineLateLender' style='float: right;'></div>",
                    template: function (row) {
                        if (row == null || row.MoneyFineLateLender == null) {
                            return "";
                        }
                        return '<span style="float:right">' + html_money(row.MoneyFineLateLender) + '</span>';
                    }
                },
                {
                    field: 'CreateDate',
                    title: 'Ngày tạo',
                    textAlign: 'left',
                    template: function (row) {
                        if (row == null || row.CreateDate == null) {
                            return "";
                        }
                        var date = moment(row.CreateDate);
                        var html = date.format("DD/MM/YYYY HH:mm");
                        return html;
                    }
                }
            ]
        });
    };
    var GridRefresh = function () {
        baseCommon.ButtonSubmit(false, '#btnGetData');
        var grid = $("#grid").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };
    var Init = function () {
        $("#fromDate,#toDate,#LenderID,#ProductID").on('change', function (e) {
            GridRefresh();
        });
        $("#LenderID").select2({
            placeholder: "Chọn lender",
            allowClear: true,
            ajax: {
                url: baseCommon.Endpoint.GetLenderSearch,
                type: 'get',
                headers: {
                    'Authorization': baseCommon.GetToken()
                },
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        generalSearch: params.term, // search term
                        type: 0
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    if (data != null && typeof data != "undefined" && data.Result == 1) {
                        return {
                            results: baseCommon.RepoConvert(data.Data),
                            pagination: {
                                more: (params.page * 30) < data.Total
                            }
                        };
                    }
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 3,
            templateResult: baseCommon.FormatRepo, // omitted for brevity, see the source of this page
            templateSelection: baseCommon.FormatRepoSelection // omitted for brevity, see the source of this page
        });
        GridRefresh();
        GetProduct();
    };

    var ExportExport = function () {
        try {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var ContractCode = $('#ContractCode').val();
            var ProductID = $('#ProductID').val();
            var ProductName = $("#ProductID option:selected").text();
            var LenderID = $('#LenderID').val();
            if (typeof ContractCode == undefined || ContractCode == null || ContractCode == "" || ContractCode.trim() == "") {
                ContractCode = "";
            }
            if (typeof LenderID == undefined || LenderID == null || LenderID == "" || LenderID == 0) {
                LenderID = -111;
            }
            if (typeof ProductID == undefined || ProductID == null || ProductID == "") {
                ProductID = -1;
            }
            debugger;
            window.location.href = "/Excel/ExcelReportMoneyDetail?ContractCode=" + ContractCode + "&ProductID=" + ProductID + "&ProductName=" + ProductName + "&LenderID=" + LenderID + "&FromDate=" + fromDate + "&ToDate=" + toDate + "&PageSize=" + gridPagesize;
        } catch (err) {
            baseCommon.ShowErrorNotLoad("Thao tác thất bại, vui lòng thử lại sau!");
        }
    };
    var GetProduct = function () {
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetListProductCredit, "", function (respone) {
            if (respone.Result == 1) {
                baseCommon.DataSource("#ProductID", "Tất cả", "Value", "Text", 0, respone.Data);
            }
        });
    }
    return {
        Init: Init,
        GridRefresh: GridRefresh,
        ExportExport: ExportExport
    };
}
$(document).ready(function () {
    ReportVatLender.Init();
});