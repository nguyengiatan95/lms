﻿var recordGrid = 0;
var ReportMoneyInterest = new function () {
    var gridPagesize = baseCommon.Option.Pagesize;
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var lenderID = $('#sl_search_lenderID').val();
                if (typeof lenderID == undefined || lenderID == null || lenderID == "") {
                    lenderID = -1;
                }
                var FromDate = $('#fromDate').val();
                var ToDate = $('#toDate').val();
                if (FromDate != null && FromDate != "") {
                    FromDate = FromDate.replace(/\//g, "-");
                }
                if (ToDate != null && ToDate != "") {
                    ToDate = ToDate.replace(/\//g, "-");
                }
                var pageIndex = options.data.page;
                var data = {
                    LenderID: parseInt(lenderID),
                    FromDate: FromDate,
                    ToDate: ToDate,
                    LstMoneyType: [2],
                    PageIndex: pageIndex,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxDone('POST', baseCommon.Endpoint.ReportVATForLender, JSON.stringify(data), function (res) {
                    if (res.Result != 1) {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    options.success(res);
                }, function (e) {
                    if (e.status == 401) {
                        window.open(LoginUrl, "_self");
                    }
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    baseCommon.ShowErrorNotLoad("Thao tác thất bại, vui lòng thử lại sau!");
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                var data = [];
                if (response != null && typeof response.Data != "undefined") {
                    data = response.Data;
                }
                return data; // total is returned in the "total" field of the response
            },
        }
        ,
        batch: true,
        pageSize: gridPagesize
    });
    var InitGrid = function (objQuery) {
        $("#grid").kendoGrid({
            excelExport: function (e) {
                var Lender = $("#LenderID option:selected").text();
                var LenderID = $('#LenderID').val();
                if (LenderID == null || LenderID == "" || LenderID.trim() == "") {
                    Lender = "All";
                }
                var FromDate = $('#fromDate').val();
                var ToDate = $('#toDate').val();
                if (FromDate != null && FromDate != "") {
                    FromDate = FromDate.replace(/\//g, "");
                }
                if (ToDate != null && ToDate != "") {
                    ToDate = ToDate.replace(/\//g, "");
                }
                var fileName = "Báo cáo VAT Lender " + Lender + "-" + FromDate + "-" + ToDate + ".xlsx";
                e.workbook.fileName = fileName;
            },
            dataSource: dataSource,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 3) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'ContactCode',
                    title: 'Mã HĐ'
                }, {
                    field: 'CustomerName',
                    title: 'Khách hàng'
                },
                {

                    field: 'Address',
                    title: 'Địa chỉ',
                    width: 500,
                    textAlign: 'left'
                },
                {
                    field: 'TransactionDate',
                    title: 'Ngày giao dịch',
                    textAlign: 'left',
                    template: function (row) {
                        if (row == null || row.TransactionDate == null) {
                            return "";
                        }
                        var date = moment(row.TransactionDate);
                        var html = date.format("DD/MM/YYYY HH:mm");
                        return html;
                    }
                },
                {
                    field: 'FeeNotVat',
                    title: 'Phí (chưa VAT)',
                    headerAttributes: { style: "text-align: right" },
                    attributes: { style: "text-align:right;" },
                    template: function (row) {
                        if (row == null || row.FeeNotVat == null) {
                            return "";
                        }
                        return '<span style="float:right">' + baseCommon.html_money_2(row.FeeNotVat) + '</span>';
                    }
                },
                {
                    field: 'Vat',
                    title: 'VAT',
                    headerAttributes: { style: "text-align: right" },
                    attributes: { style: "text-align:right;" },
                    template: function (row) {
                        if (row == null || row.Vat == null) {
                            return "";
                        }
                        return '<span style="float:right">' + baseCommon.html_money_2(row.Vat) + '</span>';
                    }
                },
                {
                    field: 'TotalMoney',
                    title: 'Tổng tiền',
                    headerAttributes: { style: "text-align: right" },
                    attributes: { style: "text-align:right;" },
                    template: function (row) {
                        if (row == null || row.TotalMoney == null) {
                            return "";
                        }
                        return '<span style="float:right">' + html_money(row.TotalMoney) + '</span>';
                    }
                },
                {

                    field: 'TypeTransaction',
                    title: 'Loại giao dịch'
                }
            ]
        });
    };
    var GridRefresh = function () {
        baseCommon.ButtonSubmit(false, '#btnGetData');
        var FromDate = $('#fromDate').val();
        var DefaultDate = '08/01/2021'

        var formatFromDate = moment(FromDate, "DD/MM/YYYY").format('MM/DD/YYYY')
        var comparFromDate = new Date(formatFromDate)
        var formatDefaultDate = new Date(DefaultDate)

        if (comparFromDate < formatDefaultDate) {
            baseCommon.ShowErrorNotLoad("Ngày bắt đầu không được trước ngày 01/08/2021!");
            baseCommon.ButtonSubmit(true, '#btnGetData');
            return;
        }
        var grid = $("#grid").data("kendoGrid")
        grid.dataSource.page(1);
    };
    var Init = function () {
        var DefaultDate = '08/01/2021'
        $('#fromDate').datepicker({ dateFormat: 'dd/mm/yyyy' }).datepicker("setDate", new Date(DefaultDate));
        $("#fromDate,#toDate,#LenderID,#sl_search_lenderID").on('change', function (e) {
            GridRefresh();
        });
        setTimeout(function () {
            InitGrid();
        }, 500);
        $("#sl_search_lenderID").select2({
            placeholder: "Tất cả",
            allowClear: true,
            ajax: {
                url: baseCommon.Endpoint.GetLenderSearch,
                type: 'get',
                headers: {
                    'Authorization': baseCommon.GetToken()
                },
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        generalSearch: params.term, // search term
                        type: 0
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    if (data != null && typeof data != "undefined" && data.Result == 1) {
                        return {
                            results: repoConvert(data.Data),
                            pagination: {
                                more: (params.page * 30) < data.Total
                            }
                        };
                    }
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 3,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
    };
    var ExportExport = function () {
        var LenderID = $('#LenderID').val();
        if (LenderID == null || LenderID == "" || LenderID.trim() == "") {
            LenderID = -1
        }
        var fromDate = $('#fromDate').val().replace(/\//g, "-");
        if (typeof fromDate != "undefined" && fromDate != null && fromDate == "") {
            fromDate = fromDate.trim();
        }
        var toDate = $('#toDate').val().replace(/\//g, "-");
        if (typeof toDate != "undefined" && toDate != null && toDate == "") {
            toDate = toDate.trim();
        }
        window.location.href = "/Excel/ExcelMoneyInterestLender?LenderID=" + LenderID + "&FromDate=" + fromDate + "&ToDate=" + toDate;
    };
    function formatRepo(repo) {
        if (repo.loading) return repo.text;
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__title'>" + repo.text + "</div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
    function repoConvert(data) {
        var newObject = [];
        if (data != null && data != "") {
            $.each(data, function (key, value) {
                var newRepo = { id: value.ID, text: value.FullName };
                newObject.push(newRepo);
            });
        }
        return newObject;
    }
    return {
        Init: Init,
        GridRefresh: GridRefresh,
        ExportExport: ExportExport
    };
}
$(document).ready(function () {
    ReportMoneyInterest.Init();
});