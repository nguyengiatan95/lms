﻿var recordGrid = 0; 
var ReportVatGCash = new function () {
    var gridPagesize = baseCommon.Option.Pagesize;
    var isExportExcell = false; 
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                
                var GcashID = $('#GcashID').val();
                if (GcashID == null || GcashID == "" || GcashID.trim() == "") {
                    GcashID = -1
                } 
                var FromDate = $('#fromDate').val();
                var ToDate = $('#toDate').val(); 
                if (FromDate != null && FromDate != "") {
                    FromDate = FromDate.replace(/\//g, "-");
                }
                if (ToDate != null && ToDate != "") {
                    ToDate = ToDate.replace(/\//g, "-");
                } 
                var pageIndex = options.data.page; 
                var data = {
                    GcashID: parseInt(GcashID),
                    FromDate: FromDate,
                    ToDate: ToDate,
                    PageIndex: pageIndex,
                    PageSize: options.data.pageSize
                };  
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.ReportVatGCash, JSON.stringify(data), function (res) {
                    if (res.Result != 1) {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                    options.success(res);
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                }, function (e) {
                    if (e.status == 401) {
                        window.open(LoginUrl, "_self");
                        }
                        baseCommon.ButtonSubmit(true, '#btnGetData');
                    baseCommon.ShowErrorNotLoad("Thao tác thất bại, vui lòng thử lại sau!");
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) { 
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                } 
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    baseCommon.ShowErrorNotLoad("Không có dữ liệu!"); 
                    var fruits = [];
                    return fruits;
                } 
                return response.Data; // total is returned in the "total" field of the response
            },
        }
        ,
        batch: true,
        pageSize: gridPagesize
    });
    var InitGrid = function (objQuery) {
        $("#grid").kendoGrid({ 
            excelExport: function (e) {
                var Lender  = $("#GcashID option:selected").text();
                var GcashID = $('#GcashID').val();
                if (GcashID == null || GcashID == "" || GcashID.trim() == "") {
                    Lender = "All";
                }  
                var FromDate = $('#fromDate').val();
                var ToDate = $('#toDate').val();
                if (FromDate != null && FromDate != "") {
                    FromDate = FromDate.replace(/\//g, "");
                }
                if (ToDate != null && ToDate != "") {
                    ToDate = ToDate.replace(/\//g, "");
                }
                var fileName = "Báo cáo VAT Gcash " + Lender + "-" + FromDate + "-" + ToDate+".xlsx";
                e.workbook.fileName = fileName;
            },
            dataSource: dataSource,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 3) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'ContactCode',
                    title: 'Mã HĐ',
                    template: function (row) {
                        var html = "", param = {};
                        param[baseCommon.LoanIndex.search] = row.ContactCode;
                        html = `<a  target="_blank" class="kt-link kt-link--state kt-link--primary" href="${baseCommon.StringFormat(baseCommon.Endpoint.LoanIndex, param)}">${row.ContactCode}</a>`;
                        return html;
                    }
                }, {
                    field: 'CustomerName',
                    title: 'Khách hàng'
                },
                {

                    field: 'Address',
                    title: 'Địa chỉ',
                    width: 500,
                    textAlign: 'left'
                },
                {
                    field: 'TransactionDate',
                    title: 'Ngày giao dịch',
                    textAlign: 'left',
                    template: function (row) {
                        if (row == null || row.TransactionDate == null) {
                            return "";
                        }
                        var date = moment(row.TransactionDate);
                        var html = date.format("DD/MM/YYYY HH:mm");
                        return html;
                    }
                },
                {
                    field: 'FeeNotVat',
                    title: 'Phí (chưa VAT)',
                    headerAttributes: { style: "text-align: right" },
                    attributes: { style: "text-align:right;" },
                    template: function (row) {
                        if (row == null || row.FeeNotVat == null) {
                            return "";
                        }
                        return '<span style="float:right">' + baseCommon.html_money_2(row.FeeNotVat) + '</span>';
                    }
                },
                {
                    field: 'Vat',
                    title: 'VAT',
                    headerAttributes: { style: "text-align: right" },
                    attributes: { style: "text-align:right;" },
                    template: function (row) {
                        if (row == null || row.Vat == null) {
                            return "";
                        }
                        return '<span style="float:right">' + baseCommon.html_money_2(row.Vat) + '</span>';
                    }
                },
                {
                    field: 'TotalMoney',
                    title: 'Tổng tiền',
                    headerAttributes: { style: "text-align: right" },
                    attributes: { style: "text-align:right;" },
                    template: function (row) {
                        if (row == null || row.TotalMoney == null) {
                            return "";
                        }
                        return '<span style="float:right">' + html_money(row.TotalMoney) + '</span>';
                    }
                },
                { 
                    field: 'TypeTransaction',
                    title: 'Loại giao dịch'
                },
                { 
                    field: 'CommodityName',
                    title: 'CommodityName'
                },
                {
                    field: 'HubName',
                    title: 'HubName'
                },
                {
                    field: 'OrderNumber',
                    title: 'OrderNumber'
                },
                {
                    field: 'TypePayment',
                    title: 'TypePayment'
                },
                {
                    field: 'TaxPercentage',
                    title: 'TaxPercentage'
                },
                {
                    field: 'Notation',
                    title: 'Notation'
                },
                {
                    field: 'Denominator',
                    title: 'Denominator'
                }
            ]
        });
    };
    var GridRefresh = function () {
        baseCommon.ButtonSubmit(false, '#btnGetData');
        var grid = $("#grid").data("kendoGrid")
        grid.dataSource.page(1);
    };  
    var GetGcash = function () {
        baseCommon.AjaxDone("GET",baseCommon.Endpoint.GetGcash + "-1","", function (respone) {
            if (respone.Result == 1) {
                baseCommon.DataSource("#GcashID", "Tất cả", "Value", "Text", -1, respone.Data, -1);
            }
        }, "Chưa lấy được danh sách cửa hàng!");
    }
    var Init = function () {
        $("#fromDate,#toDate,#GcashID").on('change', function (e) {
            GridRefresh();
        });
        GetGcash();
        InitGrid();
    };
    //var ExportExport = function () {
    //    try {
    //        isExportExcell = true;
    //        var grid = $("#grid").data("kendoGrid");
    //        grid.saveAsExcel();  
    //    } catch (err) {
    //        baseCommon.ShowErrorNotLoad("Thao tác thất bại, vui lòng thử lại sau!");
    //    } 
    //};
    var ExportExport = function () {
        var GcashID = $('#GcashID').val();
        if (GcashID == null || GcashID == "" || GcashID.trim() == "") {
            GcashID = -1
        }
        var FromDate = $('#fromDate').val();
        var ToDate = $('#toDate').val();
        if (FromDate != null && FromDate != "") {
            FromDate = FromDate.replace(/\//g, "-");
        }
        if (ToDate != null && ToDate != "") {
            ToDate = ToDate.replace(/\//g, "-");
        }  
        window.location.href = "/Excel/ExcelReportVatGCash?HubID=" + GcashID + "&FromDate=" + FromDate + "&ToDate=" + ToDate;
    };
    return {
        Init: Init,
        GridRefresh: GridRefresh,
        ExportExport: ExportExport
    }; 
}
$(document).ready(function () {
    ReportVatGCash.Init();
});