﻿var datatable;
var _loanId;
var loan = new function () {

    var InitDataTable = function (objQuery) { 
        datatable = $('#dv_result').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: "/Loan/GetLstLoanInfoByCondition",
                        params: {
                            query: objQuery
                        }, 
                        headers: {
                            "Authorization": access_token 
                        } 
                        //map: function (raw) {  
                        //    var dataSet = raw;
                        //    if (typeof raw.data !== 'undefined') {
                        //        dataSet = raw.data;
                        //    }
                        //    return dataSet;
                        //}
                    }
                },
                pageSize: baseCommon.Pagesize,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: true,
                footer: false
            }, 
            // column sorting
            sortable: true,
            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 40,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                { 
                    field: 'contactCode',
                    width: 80,
                    title: 'Mã HĐ',
                    textAlign: 'left',
                    template: function (row) {
                        var html = ""; 
                        if (row == 0 || row == null || typeof row.loanID == "undefined" || row.loanID == null || row.contactCode == null || row.lenderCode == null) {
                            return html;
                        }
                        html = `<a href="javascript:;" title="Thông tin chi tiết" onclick="loan.DetailModal(${row.loanID})">${row.contactCode}</a><br/>
                                    <span class="item-desciption">${row.lenderCode}</span>`;
                        return html;
                    }
                },
                { 
                    field: 'customerName',
                    width: 120,
                    title: 'Khách hàng',
                    textAlign: 'left',
                    template: function (row) {
                        if (row == 0 || row == null ||row.customerName == null || row.customerWithHeldAmount == null ) {
                            return html;
                        }
                        var html = `<span> ${row.customerName}</span>`;
                        if (row.customerWithHeldAmount > 0) {
                            html = `<span> ${row.customerName}</span><br/>
                                    <span class="item-desciption">Số tiền hold: ${baseCommon.FormatCurrency(row.customerWithHeldAmount)}</span>`;
                        } 
                        return html;
                    }
                },
                {
                    field: 'productName',
                    width: 120,
                    title: 'Sản phẩm',
                    textAlign: 'left'
                },
                {
                    field: 'loanTotalMoneyCurrent',
                    title: 'Số tiền gốc còn lại',
                    width: 120,
                    textAlign: 'right',
                    template: function (row) {
                        if (row == 0 || row == null || row.loanTotalMoneyCurrent == null) {
                            return "";
                        }
                        return baseCommon.FormatCurrency(row.loanTotalMoneyCurrent);
                    }
                },
                {
                    field: 'loanInterestStartDate',
                    title: 'Ngày giải ngân',
                    textAlign: 'right',
                    template: function (row) {
                        if (row == 0 || row == null ||row.loanInterestStartDate == null ) {
                            return "";
                        }
                        var date = moment(row.loanInterestStartDate);
                    return date.format("DD/MM/YYYY");
                }
                },
                {
                    field: 'loanTime',
                    title: 'Thời gian vay',
                    textAlign: 'right',
                    template: function (row) {
                        if (row == 0 || row == null ||row.loanTime == null || row.unitTimeName == null) {
                            return "";
                        }
                        var html = row.loanTime + " (" + row.unitTimeName + ")";
                        return html;
                    }
                },
                {
                    field: 'loanStatusName',
                    title: 'Tình trạng',
                    textAlign: 'right',
                    template: function (row) {
                        if (row == 0 || row == null ||row.loanStatusID == null || row.loanStatusName == null) {
                            return "";
                        }
                        switch (row.loanStatusID) {
                            case 1: // dang vay
                                return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">' + row.loanStatusName + '</span>';
                            case 100: // kêt thúc - tât toán
                            case 101: // Kết thúc - Thanh Lý
                            case 102: // Kết thúc - Thanh Lý bảo hiểm
                            case 103: // Kết thúc - Bồi thường bảo hiểm
                            case 104: // Kết thúc - Đã thu đủ tiền sau khi bồi thường BH
                            case 105: // Kết thúc - Xin miễn giảm
                            case 106: // Kết thúc - Xin miễn giảm bồi thường bảo hiểm
                                return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + row.loanStatusName + '</span>';
                            default:
                                return '<span class="kt-badge kt-badge--inline kt-badge--warning">' + row.loanStatusName + '</span>';
                        }
                    }
                },
                {
                    field: 'loanInterestPaymentNextDate',
                    title: 'Ngày trả phí',
                    textAlign: 'right',
                    template: function (row) {
                        if (row == 0 || row == null ||row.loanInterestPaymentNextDate == null || row.countDownNextdate == null) {
                            return "";
                        }
                        var date = moment(row.loanInterestPaymentNextDate); 
                        var countDownNextdate = "";
                        if (row.loanStatusID != 102 && row.loanStatusID != 100 && row.loanStatusID != 101 && row.loanStatusID != 104 && row.loanStatusID != -1) {
                            var nameForCountDate = "Ngày quá hạn: "; 
                            if (row.countDownNextdate <= 0) {
                                nameForCountDate = "Ngày còn lại: ";
                            }
                            countDownNextdate = `<span class="item-desciption"> ${nameForCountDate}${row.countDownNextdate}</span>`;
                        } 
                        var html = `<span>${date.format("DD/MM/YYYY")}</span><br/>` + countDownNextdate;
                        return html;
                    }
                },
                {
                    field: 'customerTotalMoney',
                    title: 'Tiền khách đang có',
                    width: 120,
                    textAlign: 'right',
                    template: function (row) {
                        if (row == 0 || row == null ||row.customerTotalMoney == null) {
                            return "";
                        }
                        return baseCommon.FormatCurrency(row.customerTotalMoney);
                    }
                }, 
                {
                    field: 'Action',
                    title: 'Hành động',
                    sortable: false, 
                    template: function (row, index, datatable) {
                        if (row == 0 || row == null ||row.loanID == null) {
                            return "";
                        }
                        return '\
                          <a class="btn btn-primary btn-icon btn-sm" title="Sửa" data-toggle="modal" data-target="#modal_add_edit" onclick="loan.ShowModal('+ row.loanID+ ')">\
                            <i class="fa fa-edit" ></i>\ </a>\ ';
                    }
                }
            ]
        });
    } 
    var DetailModal = function (loanId) {
        _loanId = loanId;
        $('#modalFadeBody').html('');
        $.ajax({
            url: '/Loan/Detail?id=' + loanId,
            type: 'GET', 
            dataType: 'json',
            success: function (res) { 
                if (res.isSuccess == 1) {
                    $('#modalFadeBody').html(res.html);
                    $('#modalFadeId').modal('show'); 
                    loanDetail.DetailMainLoading(loanId); 
                    loanDetail.InitTabLichDongLaiPhi();
                } else {
                    baseCommon.ShowErrorNotLoad(res.message);
                }
            }  
        });
    }
    var LoadData = function () { 
        var shopId = $('#sl_search_shopID').val().trim();
        if (typeof shopId == undefined || shopId == "") {
            shopId = 0;
        }
        var lenderID = $('#sl_search_lenderID').val().trim();
        if (typeof lenderID == undefined || lenderID == "") {
            lenderID = 0;
        }
        var objQuery = {
            LoanCode: $('#txt_search').val().trim(),
            LoanStatus: $('#sl_search_status').val().trim(),
            ShopID: shopId,
            LenderID: lenderID,
            FromDate: $('#fromDate').val().trim(),
            ToDate: $('#toDate').val().trim() 
        } 
        InitDataTable(objQuery);
    }
    var PageIndex = function () { 
        if (typeof datatable != "undefined" && typeof datatable.getCurrentPage() != "undefined") {
            return datatable.API.params.pagination.page;
        }
        return 1;
    }
    var PageSize = function () {
        if (typeof datatable != "undefined" && typeof datatable.data.pageSize != "undefined") {
            return datatable.API.params.pagination.perpage;//datatable.data.pageSize; 
        }
        return 10;
    }
    var Init = function () {
        LoadData();
        $('#fromDate,#toDate').datepicker({
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            orientation: "bottom left",
            format: 'dd/mm/yyyy',
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        }); 
        $("#sl_search_status").on('change', function (e) {
            datatable.destroy();
            LoadData();
        });
        //$("#txt_search").on('keypress', function (e) {
        //    datatable.destroy();
        //    LoadData();
        //});
        $("#btnGetData").on('click', function (e) {
            datatable.destroy();
            LoadData();
        });
        $('#sl_search_shopID').select2({
            placeholder: "Chọn cửa hàng",
            tags: true 
        }); 
        //$('#sl_search_lenderID').select2({
        //    placeholder: "Chọn lender",
        //    tags: true,
        //    matcher: loan.timkiem
        //}); 
         
        $("#sl_search_lenderID").select2({
            placeholder: "Tìm lender",
            allowClear: true,
            ajax: {
                url: "http://172.26.1.32:8080/Lender/api/Lender/GetLenderSearch",
                type: 'get',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        generalSearch : params.term, // search term
                        type: 1
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 3,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
        submitFormSave();  
        baseCommon.DataSource("#sl_search_shopID", "Chọn cửa hàng", "id", "fullName", 0, listShop);
        //baseCommon.DataSource("#sl_search_lenderID", "Chọn lender", "id", "fullName", 0, listLender);  
        loan.GetProduct();
        loan.Getstasticloanstatusbyshop();
    };
    var ShowModal = function (id = 0, departmentID = 0, userName = '', fullName = '', email = '', status = 1, lstUserGroup = 0) {
        var form = $('#btn_save').closest('form');
        form.validate().destroy();
        $("#hdd_Id").val(id);
        $('#UserName').val(userName);
        $('#FullName').val(fullName);
        $('#Email').val(email);

        if (status == 1) {
            $('#kt_switch_2').attr('checked', 'checked');
        } else {
            $('#kt_switch_2').removeAttr('checked');
        }
        if (id == 0)//form tạo mới
        {
            $('#header_add_edit').text('Thêm mới');
            $('#btn_save').html('<i class="fa fa-save"></i>Thêm mới');
            baseCommon.DataSource('#LstUserGroup', 'Chọn nhóm', 'groupID', 'groupName', lstUserGroup, dataGroups);
        }
        else {
            $('#header_add_edit').text('Cập nhật');
            $('#btn_save').html('<i class="fa fa-save"></i>Cập nhật');
            baseCommon.GetDataSource("/User/GetUserByUserID?id=" + id, "", function (data) { 
                if (data.data != null && typeof data.data != "undefined") {
                    if (data.data.lstUserGroup != null && typeof data.data.lstUserGroup != "undefined") {
                        lstUserGroup = data.data.lstUserGroup;
                        baseCommon.DataSource('#LstUserGroup', 'Chọn nhóm', 'groupID', 'groupName', lstUserGroup, dataGroups);
                    }
                }
            });;
        } 
        $('#modal_add_edit').modal('toggle');
    }
    var timkiem = function (params, data) {
    }
    var GetProduct = function () {  
        $.ajax({
            type: "GET",
            url: baseCommon.Endpoint.GetListProductCredit,
            headers: {
                'Authorization': access_token
            }
        }).done(function (respone) { 
            if (respone.Result == 1) { 
                baseCommon.DataSource("#ProductID", "Tất cả", "Value", "Text", 0, respone.Data);
            }
        }).fail(function (err) { 
            baseCommon.ShowErrorNotLoad("Chưa lấy được danh sách sản phẩm");
        }); 
    }
    var submitFormSave = function () {

    }
    var Getstasticloanstatusbyshop = function () {
        $('#modalFadeBody').html('');
        $.ajax({
            url: '/Loan/Getstasticloanstatusbyshop',
            type: 'POST',
            dataType: 'json',
            success: function (res) { 
                if (typeof res.data != undefined && res.data != null) { 
                    $('#TotalLoanBorrowOverDebt').html(baseCommon.FormatCurrency(res.data.totalLoanBorrowOverDebt));
                    $('#OverDebtMoneyBorrow').html(baseCommon.FormatCurrency(res.data.overDebtMoneyBorrow));
                    $('#TotalLoanBorrow').html(baseCommon.FormatCurrency(res.data.totalLoanBorrow));
                    $('#DebtMoneyBorrow').html(baseCommon.FormatCurrency(res.data.debtMoneyBorrow));
                } 
            },
            error: function () {
            } 
        });
    }
     
    function formatRepo(repo) { 
        if (repo.loading) return repo.text;
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";
        if (repo.description) {
            markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
        }
        markup += "<div class='select2-result-repository__statistics'>" +
            "<div class='select2-result-repository__forks'><i class='fa fa-flash'></i> " + repo.forks_count + " Forks</div>" +
            "<div class='select2-result-repository__stargazers'><i class='fa fa-star'></i> " + repo.stargazers_count + " Stars</div>" +
            "<div class='select2-result-repository__watchers'><i class='fa fa-eye'></i> " + repo.watchers_count + " Watchers</div>" +
            "</div>" +
            "</div></div>";
        return markup;
    }

    function formatRepoSelection(repo) {
        return repo.full_name || repo.text;
    }
     
    return {
        Init: Init,
        LoadData: LoadData,
        ShowModal: ShowModal,
        submitFormSave: submitFormSave,
        DetailModal: DetailModal,
        Getstasticloanstatusbyshop: Getstasticloanstatusbyshop,
        timkiem: timkiem,
        GetProduct: GetProduct
    };
}
$(document).ready(function () {
    loan.Init();
});