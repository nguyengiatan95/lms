﻿var recordGrid = 0; 
var ReportVatLender = new function () {   
    var gridPagesize = baseCommon.Option.Pagesize;
    var isExportExcell = false;
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var LenderID = $('#LenderID').val(); 
                if (typeof LenderID == undefined || LenderID == null || LenderID == "") {
                    LenderID = -1;
                } 
                var FromDate = $('#fromDate').val();
                var ToDate = $('#toDate').val();
                if (FromDate != null && FromDate != "") {
                    FromDate = FromDate.replace(/\//g, "-");
                }
                if (ToDate != null && ToDate != "") {
                    ToDate = ToDate.replace(/\//g, "-");
                } 
                var pageIndex = options.data.page;
                var data = {
                    LenderID: parseInt(LenderID),
                    FromDate: FromDate,
                    ToDate: ToDate,
                    PageIndex: pageIndex,
                    PageSize: options.data.pageSize
                };  
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.ReportVatLender, JSON.stringify(data), function (res) {
                    if (res.Result != 1) {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    options.success(res);
                }, function (e) {
                    if (e.status == 401) {
                        window.open(LoginUrl, "_self");
                        }
                        baseCommon.ButtonSubmit(true, '#btnGetData');
                    baseCommon.ShowErrorNotLoad("Thao tác thất bại, vui lòng thử lại sau!");
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    baseCommon.ShowErrorNotLoad("Không có dữ liệu!"); 
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        }
        ,
        batch: true,
        pageSize: gridPagesize
    });
    var InitGrid = function (objQuery) {
        $("#grid").kendoGrid({ 
            excelExport: function (e) {
                var Lender = $("#LenderID option:selected").text();
                var LenderID = $('#LenderID').val();
                if (LenderID == null || LenderID == "" || LenderID.trim() == "") {
                    Lender = "All";
                }
                var FromDate = $('#fromDate').val();
                var ToDate = $('#toDate').val();
                if (FromDate != null && FromDate != "") {
                    FromDate = FromDate.replace(/\//g, "");
                }
                if (ToDate != null && ToDate != "") {
                    ToDate = ToDate.replace(/\//g, "");
                }
                var fileName = "Báo cáo VAT Lender " + Lender + "-" + FromDate + "-" + ToDate + ".xlsx";
                e.workbook.fileName = fileName;
            },
            dataSource: dataSource,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 3) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'ContactCode',
                    title: 'Mã HĐ'
                }, {
                    field: 'CustomerName',
                    title: 'Khách hàng'
                },
                {

                    field: 'Address',
                    title: 'Địa chỉ',
                    width: 500,
                    textAlign: 'left'
                },
                {
                    field: 'TransactionDate',
                    title: 'Ngày giao dịch',
                    textAlign: 'left',
                    template: function (row) {
                        if (row == null || row.TransactionDate == null) {
                            return "";
                        }
                        var date = moment(row.TransactionDate);
                        var html = date.format("DD/MM/YYYY HH:mm");
                        return html;
                    }
                },
                {
                    field: 'FeeNotVat',
                    title: 'Phí (chưa VAT)', 
                    headerAttributes: { style: "text-align: right" },
                    attributes: { style: "text-align:right;" },
                    template: function (row) {
                        if (row == null || row.FeeNotVat == null) {
                            return "";
                        }
                        return '<span style="float:right">' + baseCommon.html_money_2(row.FeeNotVat) + '</span>';
                    }
                },
                {
                    field: 'Vat',
                    title: 'VAT',
                    headerAttributes: { style: "text-align: right" },
                    attributes: { style: "text-align:right;" },
                    template: function (row) {
                        if (row == null || row.Vat == null) {
                            return "";
                        }
                        return '<span style="float:right">' + baseCommon.html_money_2(row.Vat) + '</span>';
                    }
                },
                {
                    field: 'TotalMoney',
                    title: 'Tổng tiền',
                    headerAttributes: { style: "text-align: right" },
                    attributes: { style: "text-align:right;" },
                    template: function (row) {
                        if (row == null || row.TotalMoney == null) {
                            return "";
                        }
                        return '<span style="float:right">' + html_money(row.TotalMoney) + '</span>';
                    }
                },
                {

                    field: 'TypeTransaction',
                    title: 'Loại giao dịch'
                }
            ]
        });
    };
    var GridRefresh = function () {
        baseCommon.ButtonSubmit(false, '#btnGetData');
        var grid = $("#grid").data("kendoGrid")
        grid.dataSource.page(1);
    };
    var Init = function () {
        $("#fromDate,#toDate,#LenderID").on('change', function (e) {
            GridRefresh();
        });
        //$("#LenderID").select2({
        //    placeholder: "Chọn lender",
        //    allowClear: true,
        //    ajax: {
        //        url: baseCommon.Endpoint.GetLenderSearch,
        //        type: 'get',
        //        headers: {
        //            'Authorization': baseCommon.GetToken()
        //        },
        //        dataType: 'json',
        //        delay: 250,
        //        data: function (params) {
        //            return {
        //                generalSearch: params.term, // search term
        //                type: 0
        //            };
        //        },
        //        processResults: function (data, params) {

        //            // parse the results into the format expected by Select2
        //            // since we are using custom formatting functions we do not need to
        //            // alter the remote JSON data, except to indicate that infinite
        //            // scrolling can be used
        //            if (data != null && typeof data != "undefined" && data.Result == 1) {
        //                return {
        //                    results: baseCommon.RepoConvert(data.Data),
        //                    pagination: {
        //                        more: (params.page * 30) < data.Total
        //                    }
        //                };
        //            }
        //            params.page = params.page || 1;

        //            return {
        //                results: data.items,
        //                pagination: {
        //                    more: (params.page * 30) < data.total_count
        //                }
        //            };
        //        },
        //        cache: true
        //    },
        //    escapeMarkup: function (markup) {
        //        return markup;
        //    }, // let our custom formatter work
        //    minimumInputLength: 3,
        //    templateResult: baseCommon.FormatRepo, // omitted for brevity, see the source of this page
        //    templateSelection: baseCommon.FormatRepoSelection // omitted for brevity, see the source of this page
        //});
        InitGrid();
    }; 
    var ExportExport = function () {
        var LenderID = $('#LenderID').val();
        if (LenderID == null || LenderID == "" || LenderID.trim() == "") {
            LenderID = -1
        }
        var fromDate = $('#fromDate').val().replace(/\//g, "-");
        if (typeof fromDate != "undefined" && fromDate != null && fromDate == "") {
            fromDate = fromDate.trim();
        }
        var toDate = $('#toDate').val().replace(/\//g, "-");
        if (typeof toDate != "undefined" && toDate != null && toDate == "") {
            toDate = toDate.trim();
        }
        window.location.href = "/Excel/ExcelReportLender?LenderID=" + LenderID + "&FromDate=" + fromDate + "&ToDate=" + toDate;
    };
    return {
        Init: Init,
        GridRefresh: GridRefresh,
        ExportExport: ExportExport
    }; 
}
$(document).ready(function () {
    ReportVatLender.Init();
});