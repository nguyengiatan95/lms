﻿var recordGrid = 0;
var ReportVatHub = new function () {
    var gridPagesize = baseCommon.Option.Pagesize;
    
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {

                var ShopID = $('#ShopID').val();
                if (ShopID == null || ShopID == "" || ShopID.trim() == "") {
                    ShopID = 0
                }
                var FromDate = $('#fromDate').val();
                var ToDate = $('#toDate').val();
                if (FromDate != null && FromDate != "") {
                    FromDate = FromDate.replace(/\//g, "-");
                }
                if (ToDate != null && ToDate != "") {
                    ToDate = ToDate.replace(/\//g, "-");
                } 
                var pageIndex = options.data.page;
                var data = {
                    HubID: parseInt(ShopID),
                    FromDate: FromDate,
                    ToDate: ToDate,
                    PageIndex: pageIndex,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.ReportVatHub, JSON.stringify(data), function (res) {
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    if (res.Result != 1) {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                    options.success(res);
                }, function (e) {
                    if (e.status == 401) {
                        window.open(LoginUrl, "_self");
                        }
                        baseCommon.ButtonSubmit(true, '#btnGetData');
                    baseCommon.ShowErrorNotLoad("Thao tác thất bại, vui lòng thử lại sau!");
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        }
        ,
        batch: true,
        pageSize: gridPagesize
    });
    var InitGrid = function (objQuery) {
        $("#grid").kendoGrid({ 
            excelExport: function (e) {
                var Lender = $("#ShopID option:selected").text();
                var LenderID = $('#ShopID').val();
                if (LenderID == null || LenderID == "" || LenderID.trim() == "") {
                    Lender = "All";
                }
                var FromDate = $('#fromDate').val();
                var ToDate = $('#toDate').val();
                if (FromDate != null && FromDate != "") {
                    FromDate = FromDate.replace(/\//g, "");
                }
                if (ToDate != null && ToDate != "") {
                    ToDate = ToDate.replace(/\//g, "");
                }
                var fileName = "Báo cáo VAT HUB " + Lender + "-" + FromDate + "-" + ToDate + ".xlsx";
                e.workbook.fileName = fileName;
            },
            dataSource: dataSource,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 3) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'ContactCode',
                    title: 'Mã HĐ', 
                    template: function (row) {
                        var html = "", param = {};
                        param[baseCommon.LoanIndex.search] = row.ContactCode;
                        html = `<a  target="_blank" class="kt-link kt-link--state kt-link--primary" href="${baseCommon.StringFormat(baseCommon.Endpoint.LoanIndex, param)}">${row.ContactCode}</a>`;
                        return html;
                    }
                }, {
                    field: 'CustomerName',
                    title: 'Khách hàng'
                },
                {

                    field: 'Address',
                    title: 'Địa chỉ',
                    width: 500,
                    textAlign: 'left'
                },
                {
                    field: 'TransactionDate',
                    title: 'Ngày giao dịch',
                    textAlign: 'left',
                    template: function (row) {
                        if (row == null || row.TransactionDate == null) {
                            return "";
                        }
                        var date = moment(row.TransactionDate);
                        var html = date.format("DD/MM/YYYY HH:mm");
                        return html;
                    }
                },
                {
                    field: 'TotalMoney',
                    title: 'Tổng tiền',
                    headerAttributes: { style: "text-align: right" },
                    attributes: { style: "text-align:right;" },
                    template: function (row) {
                        if (row == null || row.TotalMoney == null) {
                            return "";
                        }
                        return '<span style="float:right">' + html_money(row.TotalMoney) + '</span>';
                    }
                },
                {

                    field: 'TypeTransaction',
                    title: 'Loại giao dịch'
                }
            ]
        });
    };
    var GridRefresh = function () { 
        baseCommon.ButtonSubmit(false, '#btnGetData');
        var grid = $("#grid").data("kendoGrid")
        grid.dataSource.page(1);
    };
    var Init = function () {
        $("#fromDate,#toDate,#ShopID").on('change', function (e) {
            GridRefresh();
        }); 
        InitGrid();
    }; 
    var ExportExport = function () {
        var ShopID = $('#ShopID').val();
        if (ShopID == null || ShopID == "" || ShopID.trim() == "") {
            ShopID = 0
        }
        var fromDate = $('#fromDate').val().replace(/\//g, "-");
        if (typeof fromDate != "undefined" && fromDate != null && fromDate == "") {
            fromDate = fromDate.trim();
        }
        var toDate = $('#toDate').val().replace(/\//g, "-");
        if (typeof toDate != "undefined" && toDate != null && toDate == "") {
            toDate = toDate.trim();
        }
        window.location.href = "/Excel/ExcelReportVatHub?HubID=" + ShopID + "&FromDate=" + fromDate + "&ToDate=" + toDate;
    };
    return {
        Init: Init,
        GridRefresh: GridRefresh,
        ExportExport: ExportExport
    };
}
$(document).ready(function () {
    ReportVatHub.Init();
});