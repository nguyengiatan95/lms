﻿var user = new function () {
    var showAndHide = function (number) {
        if (number == 1) {
            $('#userInfo').hide()
            $('#changePass').show()
            $('#info_activeClass').removeClass('kt-widget__item kt-widget__item--active')
            $('#info_activeClass').addClass('kt-widget__item')
            $('#changePass_activeClass').addClass('kt-widget__item kt-widget__item--active')
        } else {
            $('#userInfo').show()
            $('#changePass').hide()
            $('#changePass_activeClass').removeClass('kt-widget__item kt-widget__item--active')
            $('#changePass_activeClass').addClass('kt-widget__item')
            $('#info_activeClass').addClass('kt-widget__item kt-widget__item--active')
        }
    }
    var Init = function () { }
    return {
        Init: Init,
        showAndHide: showAndHide
    }
}
$(document).ready(function () {
    user.Init();
});