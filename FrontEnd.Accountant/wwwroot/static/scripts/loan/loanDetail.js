﻿var datatableLoanDetail;
var loanInfo = {};
var recordXetDuyet = 0;
var recordGrid = 0;
var userID = 1;
var dataMain = [];
var loanDetail = new function () {
    var isShowButtomDongLai = 0;
    var DetailMainLoading = function () {
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetLoanByID + _loanId, "", function (respone) {
            baseCommon.IconWaiting(true, '#modalLoanDetailId');
            if (respone.Result == 1) {
                loanInfo = respone.Data;
                $('#modalLoanDetailId').html("Chi tiết <b>" + loanInfo.ContactCode + "</b> - Khách hàng: <b>" + loanInfo.CustomerName + "</b>");
                $('#sp_CustomerName').html(loanInfo.CustomerName);
                $('#sp_TotalMoneyDisbursement').html(html_money(loanInfo.TotalMoneyDisbursement));// baseCommon.FormatCurrency(loanInfo.TotalMoneyDisbursement)
                $('#sp_TotalMoneyCurrent').html(html_money(loanInfo.TotalMoneyCurrent));
                $('#sp_ProductName').html(loanInfo.ProductName);
                $('#sp_RateTypeName').html(loanInfo.RateTypeName);
                $('#sp_CustomerTotalMoney').html(html_money(loanInfo.CustomerTotalMoney));
                var fromDate = moment(loanInfo.FromDate);
                $('#sp_FromDate').html(fromDate.format("DD/MM/YYYY"));
                var toDate = moment(loanInfo.ToDate);
                $('#sp_ToDate').html(toDate.format("DD/MM/YYYY"));
                if (typeof loanInfo.LinkGCNChoVay != undefined && loanInfo.LinkGCNChoVay != null && loanInfo.LinkGCNChoVay != "") {
                    $('#sp_linkGCNChoVay').text("tải về");
                    $("#sp_linkGCNChoVay").attr("href", loanInfo.LinkGCNChoVay);
                }
                if (typeof loanInfo.LinkGCNVay != undefined && loanInfo.LinkGCNVay != null && loanInfo.LinkGCNVay != "") {
                    $('#sp_linkGCNVay').text("tải về");
                    $("#sp_linkGCNVay").attr("href", loanInfo.LinkGCNVay);
                }
                $('#sp_LoanTime').html(loanInfo.LoanTime + " (" + loanInfo.UnitTimeName + ")");
                var nextDate = moment(loanInfo.NextDate);
                $('#sp_NextDate').html(nextDate.format("DD/MM/YYYY"));
                var lastDateOfPay = moment(loanInfo.LastDateOfPay);
                $('#sp_LastDateOfPay').html(lastDateOfPay.format("DD/MM/YYYY"));
                $('#sp_PhiPhat').html(html_money(loanInfo.OtherMoney));
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        }, "Lấy dữ liệu đơn vay không thành công vui lòng thử lại!");
    };
    var subInitLichDongLai = function (e) {
        var tableDetailLichDongLai = '<table class="table table-bordered"> <thead class="thead-light" style="font-weight: bold; color: black;"> \
            <tr style="text-align: right;">\
                <th style="text-align: center;vertical-align: middle;"><b>'+ formattedDate(e.data.fromDate) + ' <i class="fa fa-arrow-right"></i> ' + formattedDate(e.data.toDate) + '</b> <div>( ' + e.data.countDay + ' ngày)</div> </th>\
                <th style="vertical-align: middle;"><b>Gốc</b></th>\
                <th style="vertical-align: middle;"><b>Lãi</b></th>\
                <th style="vertical-align: middle;"><b>Phí dịch vụ</b></th>\
                <th style="vertical-align: middle;"><b>Phí tư vấn</b></th>\
                <th style="vertical-align: middle;"><b>Phí trả chậm</b></th>\
            </tr> 	</thead><tbody>\
                    <tr style="text-align: right;">\
                        <th style="text-align: center;" scope="row">Phải thu</th>\
                        <td>'+ html_money_color(e.data.moneyOriginal, true) + '</td>\
                        <td>'+ html_money_color(e.data.moneyInterest, true) + '</td>\
                        <td>'+ html_money_color(e.data.moneyService, true) + '</td>\
                        <td>'+ html_money_color(e.data.moneyConsultant, true) + '</td>\
                        <td>'+ html_money_color(e.data.moneyFineLate, true) + '</td>\
                    </tr>\
                    <tr style="text-align: right;">\
                        <th style="text-align: center;" scope="row">Đã thu</th>\
                        <td>'+ html_money_color(e.data.payMoneyOriginal, false) + '</td>\
                        <td>'+ html_money_color(e.data.payMoneyInterest, false) + '</td>\
                        <td>'+ html_money_color(e.data.payMoneyService, false) + '</td>\
                        <td>'+ html_money_color(e.data.payMoneyConsultant, false) + '</td>\
                        <td>'+ html_money_color(e.data.payMoneyFineLate, false) + '</td>\
                    </tr>\
                </tbody>\
            </table >';
        var idSub = 'child_data_local_' + e.data.recordID;
        var s = e.data.recordID;
        var sss = e.detailCell[0].firstChild;
        if (sss == undefined) {
            $(tableDetailLichDongLai).attr('id', idSub).appendTo(e.detailCell);
        }
    };
    var InitTabLichDongLaiPhi = function () {
        if (typeof datatableLoanDetail != undefined && datatableLoanDetail != null) {
            datatableLoanDetail.destroy();
        }
        setTimeout(function () {
            loadPaymentSchedule();
        }, 100);
    };
    var loadPaymentSchedule = function () {
        isShowButtomDongLai = 0;
        $('#tab_paymentschedule').addClass('active');
        datatableLoanDetail = $('#Lich').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        url: "/Loan/GetLstPaymentScheduleByLoanID?loanID=" + _loanId,
                        map: function (raw) {
                            var dataSet = raw;
                            dataMain = raw
                            if (typeof raw.data !== 'undefined' && raw.data != null) {
                                dataSet = raw.data;
                                $('#GenPaymentId').hide();
                            } else {
                                $('#GenPaymentId').show();
                            }
                            return dataSet;
                        }
                    }
                },
                pageSize: 1000,
                serverPaging: false,
                serverFiltering: false,
                serverSorting: false,
                saveState: {
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                theme: 'default',
                class: 'custom-table-tima table-tima-bordered',
                scroll: false,
                footer: false
            },
            // column sorting
            sortable: false,
            pagination: false,
            detail: { title: 'Chi tiết', content: subInitLichDongLai },
            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [1000],
                    },
                },
            },
            search: {
                //input: $('#filterName'),
            },
            columns: [{
                field: 'recordID',
                title: '#',
                width: 15,
                textAlign: 'center',
                sortable: false
            },
            {
                field: 'stt',
                title: 'Kỳ',
                width: 25,
                textAlign: 'center',
                sortable: false,
                template: function (row, index, loanDetail) {
                    var pageIndex = loanDetail.API.params.pagination.page;
                    var recordDetail = 1;
                    if (pageIndex > 1) {
                        var pageSize = loanDetail.API.params.pagination.perpage;
                        recordDetail = (pageIndex - 1) * pageSize + 1;
                    }
                    return index + recordDetail;
                }
            },
            {
                field: 'payDate',
                title: 'Ngày phải đóng',
                textAlign: 'right',
                template: function (row) {
                    var date = moment(row.payDate);
                    return date.format("DD/MM/YYYY");
                }
            },
            {
                field: 'totalCustomerNeedPay',
                title: 'Số tiền cần thu',
                textAlign: 'right',
                template: function (row) {
                    return html_money_color(row.totalCustomerNeedPay, true);
                }
            },
            {
                field: 'totalCustomerPaid',
                title: 'Đã thanh toán',
                textAlign: 'right',
                template: function (row) {
                    return html_money_color(row.totalCustomerPaid, false);
                }
            },
            {
                field: 'isComplete',
                title: 'Trạng thái',
                textAlign: 'center',
                template: function (row) {
                    var html = "";
                    if (row.isComplete == 1) {
                        html += "<span class='kt-badge  kt-badge--success kt-badge--inline kt-badge--pill'>Hoàn thành</span>";
                    }
                    else {
                        html += "<span class='kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill'>Chưa hoàn thành</span>";
                    }
                    return html;
                }
            },
            {
                field: 'Action',
                title: 'Chức năng',
                sortable: false,
                template: function (row, index, datatable) {
                    var html = '-';
                    if (row.showButtonPayment == true) {
                        html = ' <a class="btn btn-primary btn-icon btn-sm" title="Đóng tiền" onclick="loanDetail.payInterest(' + row.paymentScheduleID + ')">\
                            <i class="fa fa-hand-holding-usd"></i>\
                        </a>';
                    }
                    return html;
                }
            }]
        });
    };
    var GenPaymentScheduleByLoanID = function () {
        var modelPaymentSchedule = {
            "LoanID": loanInfo.LoanID,
            "CreateBy": 0
        };
        baseCommon.AjaxDone('POST', baseCommon.Endpoint.GenPaymentScheduleByLoanID, JSON.stringify(modelPaymentSchedule), function (respone) {
            if (respone.Result == 1) {
                setTimeout(loanDetail.InitTabLichDongLaiPhi(), 1000);
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        });
    };
    var Save_giahan = function () {
        Save_Visible(false);
        var PeriodExtend = $('#giahan_periodExtend').val();
        var regex = /^[0-9]+$/;
        if (!PeriodExtend.match(regex)) {
            baseCommon.ShowErrorNotLoad("Gia hạn không được nhập chữ !");
            Save_Visible(true);
            return;
        }
        var Note = $('#giahan_note').val();
        if (loanInfo.LoanID == 0 || loanInfo.LoanID == undefined) {
            baseCommon.ShowErrorNotLoad("Thông tin đơn vay lỗi.Vui lòng thử lại!");
            Save_Visible(true);
            return;
        }
        if (PeriodExtend == null || PeriodExtend == undefined || PeriodExtend == '') {
            baseCommon.ShowErrorNotLoad("Vui lòng nhập Số kỳ !");
            Save_Visible(true);
            return;
        }
        var modelGiaHan = {
            "LoanID": loanInfo.LoanID,
            "PeriodExtend": parseInt(PeriodExtend),
            "Note": Note,
        };
        baseCommon.AjaxDone('POST', baseCommon.Endpoint.ExtendLoanTime, JSON.stringify(modelGiaHan), function (respone) {
            Save_Visible(true);
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                setTimeout(function () {
                    InitTabGiaHan();
                }, 100);
            } else {
                Save_Visible(true);
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        });
    };
    var InitTabGiaHan = function () {
        $('#hdd_giahanHD').val(loanInfo.LoanID);
        $('#giahan_fullname').val(loanInfo.CustomerName);
    };
    var InitTabLichSuXetDuyet = function () {
        GetHistoryComment()
        var recordXetDuyet = 0;

        $("#XetDuyet").kendoGrid({
            dataSource: {
                pageSize: 1000,
                //schema: {
                //    model: {
                //        id: "LoanID"
                //    }
                //}
            },

            scrollable: true,
            resizable: true,
            pageable: true,
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordXetDuyet #",
                    width: 20
                },
                {
                    field: "ShopName",
                    title: "Tên Shop",
                    width: 50
                },
                {
                    field: "FullName",
                    title: "Người thao tác",
                    width: 60
                },
                {
                    field: "CreateDate",
                    title: "Ngày",
                    width: 70,
                    template: '#= baseCommon.FormatDate(CreateDate,\'DD/MM/YYYY HH:mm\')#',
                },
                {
                    field: "Comment",
                    title: "Nội dung",
                    width: 150,
                    template: function (row) {
                        var html = "";
                        if (row == 0 || row == null || row.Comment == "undefined" || row.Comment == null) {
                            return html;
                        }
                        html = `<span>${row.Comment}</span>`;
                        return html;
                    }
                },
            ],
            dataBinding: function () {
                recordXetDuyet = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
        });
    };
    var GetHistoryComment = function () {
        var LoanId = loanInfo.LoanID;
        var modelHistory = {
            "LoanID": LoanId,
            "PageSize": 1000,
            "PageIndex": 1
        };
        baseCommon.AjaxDone('POST', baseCommon.Endpoint.GetHistoryCommentLos, JSON.stringify(modelHistory), function (respone) {
            if (respone.Result == 1) {
                recordXetDuyet = 0
                if (respone.Data != null && respone.Data != null && respone.Data != '') {
                    var dataSource = new kendo.data.DataSource({
                        data: respone.Data
                    });
                    var grid = $("#XetDuyet").data("kendoGrid");
                    grid.setDataSource(dataSource);
                } else {
                    baseCommon.ShowErrorNotLoad('Không có dữ liệu');
                }
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        });
    };
    var GetDataImagesLos = function () {
        $('#hdd_giahanHD').val(loanInfo.LoanID);
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetDataImagesLos + loanInfo.LoanID, '', function (respone) {
            if (respone.Result == 1) {
                ShowFileInModal(respone.Data);
                new FullscreenSlideshow().init($('#div-lst-img'));
                if (respone.Data == null || respone.Data == '') {
                    baseCommon.ShowErrorNotLoad('Không có dữ liệu');
                }
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        }, "Lấy danh sách chứng từ không thành công vui lòng thử lại!");
    };
    var InitTabGachNoTuyY = function () {
        $('#hdd_donglaiTP').val(loanInfo.LoanID);
        $('#donglai_MoneyOriginal').val(0);
        $('#donglai_MoneyInterest').val(0);
        $('#donglai_MoneyFineLate').val(0);
        $('#donglai_MoneyConsultant').val(0);
        $('#donglai_MoneyService').val(0);
        $('#donglai_Description').val('');
    };
    var InitTabLSChuyenKhoanVay = function () {
        GetLSChuyenKhoanVay()
        $("#ChuyenNhuongKhoanVay").kendoGrid({
            dataSource: {
                pageSize: 1000,
            },
            scrollable: true,
            resizable: true,
            pageable: true,
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #",
                    width: 20
                },
                {
                    field: "ContactCode",
                    title: "Mã HĐ",
                },
                {
                    field: "LenderCode",
                    title: "Mã NĐT",
                },
                {
                    field: "FromDate",
                    title: "Ngày bắt đầu",
                    template: '#= baseCommon.FormatDate(FromDate,\'DD/MM/YYYY\')#',
                },
                {
                    field: "ToDate",
                    title: "Ngày Kết thúc",
                    template: '#= baseCommon.FormatDate(ToDate,\'DD/MM/YYYY\')#',
                },
                {
                    field: "AccountingMethod",
                    title: "Phương thức hạch toán"
                },
                {
                    field: "CreateDate",
                    title: "Thời gian thao tác",
                    template: '#= baseCommon.FormatDate(FromDate,\'DD/MM/YYYY\')#'
                }
            ]
        });
    };
    var GetLSChuyenKhoanVay = function () {
        var LoanId = loanInfo.LoanID;
        baseCommon.AjaxDone('GET', baseCommon.Endpoint.ContractLoanWithLenderByLoanID + LoanId, "", function (respone) {
            if (respone.Result == 1) {
                recordXetDuyet = 0
                if (respone.Data != null && respone.Data != null && respone.Data != '') {
                    var dataSource = new kendo.data.DataSource({
                        data: respone.Data
                    });
                    var grid = $("#ChuyenNhuongKhoanVay").data("kendoGrid");
                    grid.setDataSource(dataSource);
                } else {
                    baseCommon.ShowErrorNotLoad('Không có dữ liệu');
                }
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        });
    };
    var Save_GachNoTuyY = function () {
        var btn = $('#btn_gachNo');
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        if (loanInfo.LoanID == 0 || loanInfo.LoanID == undefined) {
            baseCommon.ShowErrorNotLoad("Thông tin đơn vay lỗi.Vui lòng thử lại!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }
        var MoneyOriginal = parseFloat($('#donglai_MoneyOriginal').val().replace(/[^\d\.]/g, ''));
        var MoneyInterest = parseFloat($('#donglai_MoneyInterest').val().replace(/[^\d\.]/g, ''));
        var MoneyFineLate = parseFloat($('#donglai_MoneyFineLate').val().replace(/[^\d\.]/g, ''));
        var MoneyConsultant = parseFloat($('#donglai_MoneyConsultant').val().replace(/[^\d\.]/g, ''));
        var MoneyService = parseFloat($('#donglai_MoneyService').val().replace(/[^\d\.]/g, ''));
        var Description = $('#donglai_Description').val();
        if (MoneyOriginal == 0 && MoneyInterest == 0 && MoneyFineLate == 0 && MoneyConsultant == 0 && MoneyService == 0) {
            baseCommon.ShowErrorNotLoad("Số tiền phải lớn hơn 0 !");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }
        var modelDongLaiTP = {
            "LoanID": loanInfo.LoanID,
            "MoneyOriginal": MoneyOriginal,
            "MoneyInterest": MoneyInterest,
            "MoneyConsultant": MoneyConsultant,
            "MoneyService": MoneyService,
            "MoneyFineLate": MoneyFineLate,
            "CreateBy": userID,
            "Description": Description,
        };
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.PayPartialScheduleInPeriods, JSON.stringify(modelDongLaiTP), function (respone) {
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                DetailMainLoading()
                setTimeout(function () {
                    InitTabGachNoTuyY();
                }, 100);
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            }
        }, "Gạch nợ không thành công vui lòng thử lại!");
    };
    var InitDongHD = function () {
        $("#dongHD_CloseDate").on('change', function (e) {
            InitDongHD();
        });
        setTimeout(function () {
            $("#dongHD_CloseDate").kendoDatePicker({
                format: "dd/MM/yyyy" //format is used to format the value of the widget and to parse the input.
            });
        }, 100)
        var loanId = loanInfo.LoanID;
        var modelDongHD = {
            "LoanID": loanId,
            "CloseDate": $('#dongHD_CloseDate').val(),
        };
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.GetMoneyNeedCloseLoanByLoanID, JSON.stringify(modelDongHD), function (respone) {
            if (respone.Result == 1) {
                var data = respone.Data;
                AppentDongHD(respone.Data);
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        }, "Đóng HĐ không thành công vui lòng thử lại!");
    };
    var AppentDongHD = function (response) {
        $('#hdd_dongHD').val(loanInfo.LoanID);
        $('#dongHD_MoneyOriginal').html(formatCurrency(response.MoneyOriginal) + " VNĐ");
        $('#dongHD_MoneyFineLate').html(formatCurrency(response.MoneyFineLate) + " VNĐ");
        $('#dongHD_MoneyInterest').html(formatCurrency(response.MoneyInterest) + " VNĐ");
        $('#dongHD_MoneyConsultant').html(formatCurrency(response.MoneyConsultant) + " VNĐ");
        $('#dongHD_MoneyService').html(formatCurrency(response.MoneyService) + " VNĐ");
        $('#dongHD_TotalInterest').html(formatCurrency(response.TotalInterest) + " VNĐ" + " (" + response.OverDate + " ngày" + ")");
        $('#dongHD_MoneyFineOriginal').html(formatCurrency(response.MoneyFineOriginal) + " VNĐ");
        $('#dongHD_TotalMoneyCloseLoan').html(formatCurrency(response.TotalMoneyCloseLoan) + " VNĐ");
    };
    var Save_DongHD = function () {
        var btn = $('#btn_save_dongHD');
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

        var loanId = loanInfo.LoanID
        var closeDate = $("#dongHD_CloseDate").val();

        if (closeDate == null || closeDate == "") {
            baseCommon.ShowErrorNotLoad("Ngày đóng HĐ không được bỏ trống!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }
        var CompareDate = new Date().toLocaleDateString("fr-FR");
        if (closeDate < CompareDate) {
            baseCommon.ShowErrorNotLoad("Ngày đóng HĐ không được bé hơn Hôm nay!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }
        var modelDongHD = {
            "LoanID": parseInt(loanId),
            "CloseDate": closeDate,
            "CreateBy": baseCommon.GetUser().UserID
        };
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.ForceCloseLoanByLoanID, JSON.stringify(modelDongHD), function (respone) {
            if (respone.Result == 1) {
                DetailMainLoading()
                setTimeout(function () {
                    InitDongHD();
                }, 100);
                baseCommon.ShowSuccess(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            }
        }, "Đóng HĐ không thành công vui lòng thử lại!");
    };
    var InitPhiPhatCham = function () {
        setTimeout(function () {
            TablePhiPhatRefresh()
        }, 500);
        $("#loanDetail_tienGhiNo").val(0)
        $("#loanDetail_tienTraNo").val(0)

        var date = new Date();
        var data = dataMain.data;
        var filterData = data.filter(obj => {
            var payDate = new Date(obj.payDate);
            return (payDate <= date) && (obj.isComplete !== 1);
        });
        if (filterData.length > 0) {
            $('#append_select_Phiphatcham').html('<option value="0" selected>Vui lòng chọn kỳ</option>');
            $.each(filterData, function (i, item) {
                $('#append_select_Phiphatcham').append('<option value="' + item.paymentScheduleID + '">Kỳ ' + item.recordID + ' - ' + moment(item.payDate).format("DD/MM/YYYY") + '</option>');
            });
        } else {
            $('#hdd_sl_phiPhatCham').html("")
        }
        $(".kt-select2").select2({
            placeholder: "Chọn giá trị"
        });
        $("input").attr("autocomplete", "off");
    };
    var Save_PhiPhatCham = function () {
        var btn = $('#btn_ghiNo_PhiPhatCham');
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

        var loanId = parseInt(loanInfo.LoanID)
        var totalMoney = parseInt(($("#loanDetail_tienGhiNo").val()).replace(/[^\d\.]/g, ''));
        if (totalMoney == null || totalMoney == 0) {
            baseCommon.ShowErrorNotLoad("Số tiền phải lớn hơn 0!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }
        var paymentID = parseInt($("#append_select_Phiphatcham").val());
        if (paymentID == 0) {
            baseCommon.ShowErrorNotLoad("Vui lòng chọn kỳ!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }

        var modelPhiPhatCham = {
            "LoanID": loanId,
            "UserID": baseCommon.GetUser().UserID,
            "Description": '',
            "TotalMoney": totalMoney,
            "PaymentID": paymentID,
        };
        baseCommon.AjaxDone('POST', baseCommon.Endpoint.CreateLoanDebt, JSON.stringify(modelPhiPhatCham), function (respone) {
            if (respone.Result == 1) {
                setTimeout(function () {
                    TablePhiPhatRefresh();
                    $("#loanDetail_tienGhiNo").val(0);
                    $("#append_select_Phiphatcham").val(0).change();
                }, 100);
                baseCommon.ShowSuccess(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            }
        });
    };
    var Save_TraNoPhiPhatCham = function () {
        var btn = $('#btn_traNo_PhiPhatCham');
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

        var loanId = parseInt(loanInfo.LoanID)
        var totalMoney = parseInt(($("#loanDetail_tienTraNo").val()).replace(/[^\d\.]/g, ''));
        if (totalMoney == null || totalMoney == 0) {
            baseCommon.ShowErrorNotLoad("Số tiền phải lớn hơn 0!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }

        var modelTrNoPhiPhatCham = {
            "LoanID": loanId,
            "UserID": baseCommon.GetUser().UserID,
            "TotalMoney": totalMoney,
        };
        baseCommon.AjaxDone('POST', baseCommon.Endpoint.PayLoanDebtByLoanID, JSON.stringify(modelTrNoPhiPhatCham), function (respone) {
            if (respone.Result == 1) {
                setTimeout(function () {
                    TablePhiPhatRefresh();
                    $("#loanDetail_tienTraNo").val(0);
                }, 100);
                baseCommon.ShowSuccess(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            }
        });
    };
    var dataSourcePhiPhatCham = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var LoanId = loanInfo.LoanID;
                var data = { "loanID": LoanId };
                baseCommon.AjaxSuccess('GET', baseCommon.Endpoint.GetLoanDebtByLoanID, data, function (res) {
                    if (res.Result != 1) {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                    options.success(res);
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null || response.Data.length == 0) {
                    baseCommon.ShowErrorNotLoad("Không có dữ liệu!");
                    var data = [];
                    return data;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        }
        ,
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var TablePhiPhatRefresh = function () {
        var table = $("#TablePhiPhat").data("kendoGrid");
        if (table != null) {
            $("#TablePhiPhat").data("kendoGrid").dataSource.read();
        } else {
            InitGridPhiPhatCham();
        }
    };
    var InitGridPhiPhatCham = function (objQuery) {
        $("#TablePhiPhat").kendoGrid({
            dataSource: dataSourcePhiPhatCham,
            //pageable: {
            //    pageSizes: [20, 30, 50, 100],
            //    input: true
            //},
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 3) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'CreateDate',
                    title: 'Ngày tạo',
                    textAlign: 'left',
                    template: function (row) {
                        if (row == null || row.CreateDate == null || row.CreateDate == "0001-01-01T00:00:00") {
                            return "";
                        }
                        var date = moment(row.CreateDate);
                        var html = date.format("DD/MM/YYYY HH:mm");
                        return html;
                    }
                },
                {
                    field: 'TotalMoney',
                    title: 'Số tiền',
                    attributes: { style: "text-align:center;" },
                    template: function (row) {
                        return `${html_money(row.TotalMoney)}`;
                    }
                },
                {
                    field: "Description",
                    title: "Nội dung",
                    width: 350,
                    template: function (row) {
                        var html = "";
                        if (row == 0 || row == null || row.Description == "undefined" || row.Description == null) {
                            return html;
                        }
                        html = `<span>${row.Description}</span>`;
                        return html;
                    }
                },
                {
                    field: 'Status',
                    title: 'Trạng thái',
                    attributes: { style: "text-align:center;" },
                    template: function (row) {
                        if (row.Status == 1) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Hoạt động</span>';
                        }
                        if (row.Status == -1) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Xóa</span>';
                        }
                    }
                },
                {
                    title: "Hành động",
                    selectable: false,
                    template: function (row) {
                        var strAction = '';
                        if (row.ShowButtonDelete) {
                            strAction = '<button class="btn btn-outline-danger btn-icon" title="Hủy" onclick="loanDetail.DeleteLoanDebt(' + row.ReferID + ',' + row.DebtID + ')">\
                                            <i onclick="javascript:;" class="fa fa-times"></i></button> ';
                        }
                        return strAction;
                    }
                }
            ]
        });
    };
    var DeleteLoanDebt = function (loanID, debIb) {
        swal.fire({
            html: "Bạn có chắc chắn muốn hủy phí phạt <b>HĐ-" + loanID + "</b> không?",
            type: "warning",
            showCancelButton: !0,
            cancelButtonText: "Hủy",
            confirmButtonText: "Đồng ý"
        }).then(function (result) {
            if (result.value) {
                var data = {
                    "LoanID": parseInt(loanID),
                    "UserID": baseCommon.GetUser().UserID,
                    "DebtID": parseInt(debIb),
                };
                baseCommon.AjaxDone("POST", baseCommon.Endpoint.DeleteLoanDebt, JSON.stringify(data), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        setTimeout(function () {
                            TablePhiPhatRefresh();
                        }, 100);
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                }, "Hủy phí phạt không thành công vui lòng thử lại!");
            }
        });
    };

    function ShowFileInModal(data) {
        $("#m_accordion_5").html("");
        $("#div-lst-img").html("");
        if (data != null && data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                $("#m_accordion_5").append('<div class="m-accordion__item"> \
        <div class="m-accordion__item-head collapsed check-click" role="tab" id="m_accordion_5_item_1_head_'+ data[i].TypeId + '" onclick="ShowImg(' + data[i].TypeId + ')" data-toggle="collapse" href="#m_accordion_5_item_1_body_' + data[i].TypeId + '" aria-expanded="false">\
                    <div class="div_style">\
            <span class="m-accordion__item-title">'+ data[i].TypeName + ' <b style="color:red;">(' + data[i].LstFilePath.length + ')</b></span >\
                     </div>\
        </div>');
                $("#div-lst-img").append('<div id="detail_content_' + data[i].TypeId + '" style="display:none" class="row div-lst-img" ></div>');
                var lstImg = data[i].LstFilePath;
                if (lstImg != null && lstImg.length > 0) {
                    for (var k = 0; k < lstImg.length; k++) {
                        $("#detail_content_" + data[i].TypeId).append('<div class="wapperImage col-md-4">\
                            <div class="photoHD">\
                                <img src="'+ lstImg[k].FilePath.replaceAll('\\', '/') + '" />\
                             </div>\
                            <div class="div-remove">\
                                </div>\
                     </div >');
                    }
                }
                if (i == 0) {
                    $("#m_accordion_5_item_1_head_" + data[i].TypeId).css("background", "#c5b0b0");
                    $("#detail_content_" + data[i].TypeId).show();
                    //new FullscreenSlideshow().init($('#detail_content_' + data[i].TypeId));
                }
            }
        }

    };
    var Init = function () {

    };
    var loadHistoryTransaction = function () {
        KTApp.block('#kt_tabs_lichsugiaodich', {});
        $('#tbl_historyTransaction').html('');

        var loanID = loanInfo.LoanID;
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetTransactionByLoanID + loanID, "", function (respone) {
            if (respone.Result == 1) {
                var tableResource = "";
                var data = respone.Data;
                var totalMoneyMinus = 0;
                var totalMoneyPlus = 0;


                for (var i = 0; i < data.length; i++) {
                    totalMoneyMinus += data[i].TotalMoney < 0 ? data[i].TotalMoney : 0;
                    totalMoneyPlus += data[i].TotalMoney > 0 ? data[i].TotalMoney : 0;
                    tableResource += '\<tr>\
                                        <td class="text-center" style="width:60px;">\
                                            '+ (i + 1) + '\
                                        </td>\
                                        <td class="text-left">\
                                            '+ formattedDateHourMinutes(data[i].CreateDate) + '\
                                        </td>\
                                        <td class="text-left">\
                                            '+ data[i].UserName + '\
                                        </td>\
                                        <td class="text-right">\
                                            '+ (data[i].TotalMoney < 0 ? html_money(data[i].TotalMoney) : "") + '\
                                        </td>\
                                         <td class="text-right">\
                                            '+ (data[i].TotalMoney > 0 ? html_money(data[i].TotalMoney) : "") + '\
                                        </td>\
                                        <td class="text-left">'+ data[i].MoneyTypeName + '</td>\
                                        <td class="text-left">'+ data[i].ActionName + '</td>\
                                    </tr>';
                }
                tableResource += '\<tr style="background-color: #EEE8AA!important;">\
                                        <td colspan="3" class="text-right"><b>Tổng:</b></td>\
                                        <td class="text-right">\
                                            <b>'+ html_money(totalMoneyMinus) + '</b>\
                                        </td>\
                                       <td class="text-right">\
                                            <b>'+ html_money(totalMoneyPlus) + '</b>\
                                        </td>\
                                        <td colspan="2"  class="text-left"></td>\
                                    </tr>'
                tableResource += '\<tr style="background-color: #EEE8AA!important;">\
                                        <td colspan="3" class="text-right"><b>Chênh lệch:</b></td>\
                                        <td class="text-center" colspan="2">\
                                            <b>'+ html_money(totalMoneyMinus + totalMoneyPlus) + '</b>\
                                        </td>\
                                        <td colspan="2"  class="text-left"></td>\
                                    </tr>'
                $('#tbl_historyTransaction').html(tableResource);
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        }, "Có lỗi load dữ liệu ");
        KTApp.unblock('#kt_tabs_lichsugiaodich');
    };
    var payInterest = function (paymentID) {
        var loanID = loanInfo.LoanID;

        var modelRequest = {
            "loanID": loanID,
            "paymentID": paymentID,
            "userID": userID
        };
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.PaymentMoneyFullSchedule, JSON.stringify(modelRequest), function (respone) {
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                loanDetail.InitTabLichDongLaiPhi();
                loanDetail.DetailMainLoading();
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        }, "Lấy dữ liệu không thành công vui lòng thử lại!");
    };
    var Save_Visible = function (isVisible) {
        if (!isVisible) {
            $("#btn_save_giahan").addClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
            $("#btn_save_giahan").prop("disabled", true);
        } else {
            $("#btn_save_giahan").removeClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
            $("#btn_save_giahan").prop("disabled", false);
        }
    };
    $(document).on('keyup', '#dongHD_CustomerPay', function (e) {
        var loValue = formatCurrency($(this).val().replace(/,/g, '').replace(/\./g, ''));
        $("#dongHD_CustomerPay").val(loValue);
    });
    $(document).on('keyup', '#donglai_MoneyOriginal', function (e) {
        var loValue = formatCurrency($(this).val().replace(/,/g, '').replace(/\./g, ''));
        $("#donglai_MoneyOriginal").val(loValue);
    });
    $(document).on('keyup', '#donglai_MoneyInterest', function (e) {
        var loValue = formatCurrency($(this).val().replace(/,/g, '').replace(/\./g, ''));
        $("#donglai_MoneyInterest").val(loValue);
    });
    $(document).on('keyup', '#donglai_MoneyFineLate', function (e) {
        var loValue = formatCurrency($(this).val().replace(/,/g, '').replace(/\./g, ''));
        $("#donglai_MoneyFineLate").val(loValue);
    });
    $(document).on('keyup', '#donglai_MoneyConsultant', function (e) {
        var loValue = formatCurrency($(this).val().replace(/,/g, '').replace(/\./g, ''));
        $("#donglai_MoneyConsultant").val(loValue);
    });
    $(document).on('keyup', '#donglai_MoneyService', function (e) {
        var loValue = formatCurrency($(this).val().replace(/,/g, '').replace(/\./g, ''));
        $("#donglai_MoneyService").val(loValue);
    });
    $(document).on('keyup', '#loanDetail_tienTraNo', function (e) {
        var loValue = formatCurrency($(this).val().replace(/,/g, '').replace(/\./g, ''));
        $("#loanDetail_tienTraNo").val(loValue);
    });
    $(document).on('keyup', '#loanDetail_tienGhiNo', function (e) {
        var loValue = formatCurrency($(this).val().replace(/,/g, '').replace(/\./g, ''));
        $("#loanDetail_tienGhiNo").val(loValue);
    });
    function formatCurrency(num) {
        num = num.toString().replace(/\$|\,/g, '');
        if (isNaN(num))
            num = "0";
        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        cents = num % 100;
        num = Math.floor(num / 100).toString();
        if (cents < 10)
            cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
            num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                num.substring(num.length - (4 * i + 3));
        return (((sign) ? '' : '-') + num);
    };
    return {
        Init: Init,
        DetailMainLoading: DetailMainLoading,
        InitTabLichDongLaiPhi: InitTabLichDongLaiPhi,
        InitTabLichSuXetDuyet: InitTabLichSuXetDuyet,
        InitTabGiaHan: InitTabGiaHan,
        InitTabGachNoTuyY: InitTabGachNoTuyY,
        InitTabLSChuyenKhoanVay: InitTabLSChuyenKhoanVay,
        Save_giahan: Save_giahan,
        GenPaymentScheduleByLoanID: GenPaymentScheduleByLoanID,
        GetDataImagesLos: GetDataImagesLos,
        ShowImg: ShowImg,
        ShowFileInModal: ShowFileInModal,
        loadHistoryTransaction: loadHistoryTransaction,
        payInterest: payInterest,
        InitDongHD: InitDongHD,
        Save_GachNoTuyY: Save_GachNoTuyY,
        Save_Visible: Save_Visible,
        Save_DongHD: Save_DongHD,
        InitPhiPhatCham: InitPhiPhatCham,
        Save_PhiPhatCham: Save_PhiPhatCham,
        Save_TraNoPhiPhatCham: Save_TraNoPhiPhatCham,
        DeleteLoanDebt: DeleteLoanDebt,
    };
};
function ShowImg(TypeId) {
    $(".check-click").css("background", "#f7f8fa");
    $("#m_accordion_5_item_1_head_" + TypeId).css("background", "#c5b0b0");
    $(".div-lst-img").hide();
    $("#detail_content_" + TypeId).show();
    new FullscreenSlideshow().init($('#detail_content_' + TypeId));
};
$(document).ready(function () {
    loanDetail.Init();
});

