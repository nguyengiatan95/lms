﻿var datatable;
var _loanId, gridSelectedRowData, rowClicked = 0, isSearchLoan = true;
var loan = new function () {
    var Option = {
        Delete: 1,
        Detail: 0
    };
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                baseCommon.ButtonSubmit(false, '#btnGetData');
                var ProductID = $('#ProductID').val();
                if (typeof ProductID == undefined || ProductID == null || ProductID == "") {
                    ProductID = baseCommon.Option.All;
                } var shopId = $('#sl_search_shopID').val();
                if (typeof shopId == undefined || shopId == null || shopId == "") {
                    shopId = baseCommon.Option.All;
                }
                var lenderID = $('#sl_search_lenderID').val();

                if (typeof lenderID == undefined || lenderID == null || lenderID == "") {
                    lenderID = baseCommon.Option.All;
                }
                var fromDate = $('#fromDate').val();
                if (typeof fromDate != "undefined" && fromDate != null && fromDate == "") {
                    fromDate = fromDate.trim();
                }
                var toDate = $('#toDate').val();
                if (typeof toDate != "undefined" && toDate != null && toDate == "") {
                    toDate = toDate.trim();
                }
                var txt_search = $('#txt_search').val();
                if (typeof txt_search != "undefined" && txt_search != null && txt_search == "") {
                    txt_search = txt_search.trim();
                }
                var sl_search_status = $('#sl_search_status').val();
                if (typeof sl_search_status != "undefined" && sl_search_status == "") {
                    sl_search_status = sl_search_status.trim();
                }
                var data = {
                    LoanCode: txt_search,
                    LoanStatus: parseInt(sl_search_status),
                    ShopID: parseInt(shopId),
                    LenderID: parseInt(lenderID),
                    FromDate: fromDate,
                    ToDate: toDate,
                    ProductID: parseInt(ProductID),
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetLstLoanInfoByCondition, JSON.stringify(data), function (res) {
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    if (res.Result == 1) {
                        options.success(res.Data);
                    }
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                return response.RecordsTotal; // total is returned in the "total" field of the response
            },
            data: function (response) {
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        pageSize: baseCommon.Option.Pagesize
    });
    function Grid_change(e) {
        var selectedRows = this.select();
        var selectedDataItems = [];
        for (var i = 0; i < selectedRows.length; i++) {
            var dataItem = this.dataItem(selectedRows[i]);
            selectedDataItems.push(dataItem);
        }
        // selectedDataItems contains all selected data items
        gridSelectedRowData = selectedDataItems[0];
        if (rowClicked == Option.Delete) {
            rowClicked = 0;
            loan.Delete(gridSelectedRowData.LoanID, gridSelectedRowData.ContactCode);
        } else if (rowClicked == Option.LoanTransfer) {
            rowClicked = 0;
            loan.LoanTransfer(gridSelectedRowData.LoanID, gridSelectedRowData.LenderCode);
        }
        else {
            DetailModal(gridSelectedRowData.LoanID);
        }
    };
    var InitGrid = function () {
        $("#dv_result").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            change: Grid_change,
            selectable: "row",
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 5 || true) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'ContactCode',
                    width: 80,
                    title: 'Mã HĐ',
                    textAlign: 'left',
                    template: function (row) {
                        var html = "";
                        if (row == 0 || row == null || typeof row.LoanID == "undefined" || row.LoanID == null || row.ContactCode == null || row.LenderCode == null) {
                            return html;
                        }
                        html = `${row.ContactCode}<br/>
                                    <span class="item-desciption">${row.LenderCode}</span>`;
                        return html;
                    }
                },
                {
                    field: 'CustomerName',
                    width: 120,
                    title: 'Khách hàng',
                    textAlign: 'left',
                    template: function (row) {
                        var html = '';
                        if (row == 0 || row == null || row.CustomerName == null || row.CustomerWithHeldAmount == null) {
                            return html;
                        }
                        html = `${row.CustomerName}`;
                        if (row.CustomerWithHeldAmount > 0) {
                            html += `<br/><span class="item-desciption">Số tiền hold: ${baseCommon.FormatCurrency(row.CustomerWithHeldAmount)}</span>`;
                        }
                        return html;
                    }
                },
                {
                    field: 'ProductName',
                    width: 120,
                    title: 'Sản phẩm',
                    textAlign: 'left'
                },
                {
                    field: 'OwnerShopName',
                    title: 'Cửa hàng',
                    width: 85,
                    textAlign: 'center'
                },
                {
                    field: 'LoanTotalMoneyCurrent',
                    title: 'Số tiền gốc còn lại',
                    width: 150,
                    headerAttributes: { style: "text-align: right" },
                    attributes: { style: "text-align:right;" },
                    template: function (row) {
                        if (row == 0 || row == null || row.LoanTotalMoneyCurrent == null) {
                            return "";
                        }
                        return html_money(row.LoanTotalMoneyCurrent);//baseCommon.FormatCurrency(row.loanTotalMoneyCurrent);
                    }
                },
                {
                    field: 'LoanInterestStartDate',
                    title: 'Ngày giải ngân',
                    textAlign: 'right',
                    template: function (row) {
                        if (row == 0 || row == null || row.LoanInterestStartDate == null) {
                            return "";
                        }
                        var date = moment(row.LoanInterestStartDate);
                        return date.format("DD/MM/YYYY");
                    }
                },
                {
                    field: 'LoanTime',
                    title: 'Thời gian vay',
                    textAlign: 'right',
                    template: function (row) {
                        if (row == 0 || row == null || row.LoanTime == null || row.UnitTimeName == null) {
                            return "";
                        }
                        var html = row.LoanTime + " (" + row.UnitTimeName + ")";
                        return html;
                    }
                },
                //{
                //    field: 'loanTime',
                //    title: 'Cửa hàng',
                //    textAlign: 'right'
                //},
                {
                    field: 'LoanStatusName',
                    title: 'Tình trạng',
                    textAlign: 'right',
                    template: function (row) {
                        if (row == 0 || row == null || row.LoanStatusID == null || row.LoanStatusName == null) {
                            return "";
                        }
                        switch (row.LoanStatusID) {
                            case 1: // dang vay
                                return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">' + row.LoanStatusName + '</span>';
                            case 100: // kêt thúc - tât toán
                            case 101: // Kết thúc - Thanh Lý
                            case 102: // Kết thúc - Thanh Lý bảo hiểm
                            case 103: // Kết thúc - Bồi thường bảo hiểm
                            case 104: // Kết thúc - Đã thu đủ tiền sau khi bồi thường BH
                            case 105: // Kết thúc - Xin miễn giảm
                            case 106: // Kết thúc - Xin miễn giảm bồi thường bảo hiểm
                                return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + row.LoanStatusName + '</span>';
                            default:
                                return '<span class="kt-badge kt-badge--inline kt-badge--warning">' + row.LoanStatusName + '</span>';
                        }
                    }
                },
                {
                    field: 'LoanInterestPaymentNextDate',
                    title: 'Ngày trả phí',
                    textAlign: 'right',
                    template: function (row) {
                        if (row == 0 || row == null || row.LoanInterestPaymentNextDate == null || row.CountDownNextdate == null) {
                            return "";
                        }
                        var date = moment(row.LoanInterestPaymentNextDate);
                        var countDownNextdate = "";
                        if (row.LoanStatusID != 102 && row.LoanStatusID != 100 && row.LoanStatusID != 101 && row.LoanStatusID != 104 && row.LoanStatusID != -1) {
                            var nameForCountDate = "Ngày quá hạn: ";
                            if (row.CountDownNextdate <= 0) {
                                nameForCountDate = "Ngày còn lại: ";
                            }
                            countDownNextdate = `<span class="item-desciption"> ${nameForCountDate}${row.CountDownNextdate}</span>`;
                        }
                        var html = `<span>${date.format("DD/MM/YYYY")}</span><br/>` + countDownNextdate;
                        return html;
                    }
                },
                {
                    field: 'CustomerTotalMoney',
                    title: 'Tiền khách đang có',
                    width: 150,
                    headerAttributes: { style: "text-align: right" },
                    attributes: { style: "text-align:right;" },
                    template: function (row) {
                        if (row == 0 || row == null || row.CustomerTotalMoney == null) {
                            return "";
                        }
                        return html_money(row.CustomerTotalMoney);//baseCommon.FormatCurrency(row.customerTotalMoney);
                    }
                },
                {
                    field: 'Action',
                    title: 'Hành động',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var html = "";
                        if (row == 0 || row == null || row.LoanID == null) {
                            return "";
                        }
                        if (row.status == 1 || true) {
                            html = '<a class="btn btn-danger btn-icon btn-sm" title="Xóa hợp đồng"  onclick="loan.RowClick(loan.Option.Delete)">\
                            <i class="fa fa-trash-alt" ></i>\ </a>\
                             <a class="btn btn-success btn-icon btn-sm"  title ="Chuyển nhượng khoản vay" onclick="loan.RowClick(loan.Option.LoanTransfer)">\
                            <i class="fa fa-edit" ></i>\ </a >';
                        }
                        return html;
                        //<a class="btn btn-primary btn-icon btn-sm" title="Sửa" data-toggle="modal" data-target="#modal_add_edit" onclick="loan.ShowModal('+ row.loanID + ')"> <i class="fa fa-edit" ></i>\ </a >
                    }
                }
            ]
        });
    };
    var LoanTransfer = function (loanID, lenderCode) {
        $("#hdd_LoanID").val(loanID);
        $("#lenderCode").val(lenderCode);
        $('#modal_partialLoanTransfer').modal('show');
    };
    var RowClick = function (rowClick) {
        rowClicked = rowClick;
    };
    var Delete = function (loanID, codeID) {
        swal.fire({
            html: "Bạn có chắc chắn muốn xóa hợp đồng <b>" + codeID + "</b> không?",
            type: "warning",
            showCancelButton: !0,
            cancelButtonText: "Hủy",
            confirmButtonText: "Đồng ý"
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: "/Loan/DeleteLoan",
                    type: "POST",
                    data: { loanID: loanID },
                    success: function (response) {
                        if (response.response.result === 1) {
                            showMesage("success", "Xóa HĐ " + codeID + " thành công");
                            $('#btnGetData').click();
                        }
                        else {
                            showMesage("danger", response.response.message);
                        }
                    }
                });
            }
        });
    };
    var DetailModal = function (loanId) {
        _loanId = loanId;
        baseCommon.IconWaiting(false, '#modalLoanDetailId');
        $('#modalFadeBody').html('');
        $.ajax({
            url: '/Loan/Detail?id=' + loanId,
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                if (res.isSuccess == 1) {
                    $('#modalFadeBody').html(res.html);
                    $('#modalFadeId').modal('show');
                    loanDetail.DetailMainLoading(loanId);
                    setTimeout(loanDetail.InitTabLichDongLaiPhi(), 2000);
                } else {
                    baseCommon.ShowErrorNotLoad(res.message);
                }
            }
        });
    };
    var LoadData = function () {
        if (isSearchLoan) {
            baseCommon.ButtonSubmit(false, '#btnGetData');
            var grid = $("#dv_result").data("kendoGrid");
            if (grid != null) {
                grid.dataSource.page(1);
            } else {
                InitGrid();
            }
        }
    };
    var Init = function () {
        $("#fromDate,#toDate,#ProductID,#sl_search_lenderID,#sl_search_shopID,#sl_search_status").on('change', function (e) {
            LoadData();
        });
        $("#txt_search").on('keypress', function (e) {
            if (e.which === 13) {
                $('#btnGetData').click();
            }
        });
        $("#btnGetData").on('click', function (e) {
            LoadData();
        });
        $("#sl_search_lenderID,#sl_search_TransferLender").select2({
            placeholder: "Chọn mã NDT",
            allowClear: true,
            ajax: {
                url: baseCommon.Endpoint.GetLenderSearch,
                type: 'get',
                headers: {
                    'Authorization': baseCommon.GetToken()
                },
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        generalSearch: params.term, // search term
                        type: 0
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    if (data != null && typeof data != "undefined" && data.Result == 1) {
                        return {
                            results: repoConvert(data.Data),
                            pagination: {
                                more: (params.page * 30) < data.Total
                            }
                        };
                    }
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 3,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
        submitFormSave();
        loan.GetProduct();
        loan.GetShop();
        loan.Getstasticloanstatusbyshop();
        setTimeout(function () { SearchLoan(); }, 500);
    };
    var ShowModalLoanEdit = function (id = 0) {
        $.ajax({
            type: "GET",
            url: "/Loan/GetLoanByID?loanID=" + _loanId,
            async: false,
            processData: false,
            contentType: false,
            headers: {
                "Authorization": access_token
            }
        }).done(function (responce) {
            if (typeof responce != "undefined" && responce.result == 1) {
                var loanInfo = responce.data;
                $('#loanDetailId').html("Chi tiết " + loanInfo.contactCode + " - Khách hàng: " + loanInfo.customerName);
                $('#CustomerName').html(loanInfo.customerName);
                $('#TotalMoneyDisbursement').html(baseCommon.FormatCurrency(loanInfo.totalMoneyDisbursement));
                $('#TotalMoneyCurrent').html(baseCommon.FormatCurrency(loanInfo.totalMoneyCurrent));
                $('#ProductName').html(loanInfo.productName);
                $('#RateTypeName').html(loanInfo.rateTypeName);
                $('#CustomerTotalMoney').html(baseCommon.FormatCurrency(loanInfo.customerTotalMoney));
                var fromDate = moment(loanInfo.fromDate);
                $('#FromDate').html(fromDate.format("DD/MM/YYYY"));
                var toDate = moment(loanInfo.toDate);
                $('#ToDate').html(toDate.format("DD/MM/YYYY"));
                if (typeof loanInfo.linkGCNChoVay != undefined && loanInfo.linkGCNChoVay != null && loanInfo.linkGCNChoVay != "") {
                    $('#linkGCNChoVay').text("tải về");
                    $("#linkGCNChoVay").attr("href", loanInfo.linkGCNChoVay);
                }
                if (typeof loanInfo.linkGCNVay != undefined && loanInfo.linkGCNVay != null && loanInfo.linkGCNVay != "") {
                    $('#linkGCNVay').text("tải về");
                    $("#linkGCNVay").attr("href", loanInfo.linkGCNVay);
                }
            } else {
                baseCommon.ShowErrorNotLoad("Lấy dữ liệu không thành công!");
            }
        });
    };
    var ShowModal = function (id = 0, departmentID = 0, userName = '', fullName = '', email = '', status = 1, lstUserGroup = 0) {
        _loanId = id;
        var form = $('#btn_save').closest('form');
        //form.validate().destroy();
        $("#hdd_Id").val(id);
        $('#UserName').val(userName);
        $('#FullName').val(fullName);
        $('#Email').val(email);

        if (status == 1) {
            $('#kt_switch_2').attr('checked', 'checked');
        } else {
            $('#kt_switch_2').removeAttr('checked');
        }
        if (id == 0)//form tạo mới
        {
            $('#header_add_edit').text('Thêm mới');
            $('#btn_save').html('<i class="fa fa-save"></i>Thêm mới');
            baseCommon.DataSource('#LstUserGroup', 'Chọn nhóm', 'groupID', 'groupName', lstUserGroup, dataGroups);
        }
        else {
            $('#header_add_edit').text('Cập nhật');
            $('#btn_save').html('<i class="fa fa-save"></i>Cập nhật');
            baseCommon.GetDataSource("/User/GetUserByUserID?id=" + id, "", function (data) {
                if (data.data != null && typeof data.data != "undefined") {
                    if (data.data.lstUserGroup != null && typeof data.data.lstUserGroup != "undefined") {
                        lstUserGroup = data.data.lstUserGroup;
                        baseCommon.DataSource('#LstUserGroup', 'Chọn nhóm', 'groupID', 'groupName', lstUserGroup, dataGroups);
                    }
                }
            });;
        }
        $('#modal_add_edit').modal('toggle');
    };
    var timkiem = function (params, data) {
    };
    var GetProduct = function () {
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetListProductCredit, "", function (respone) {
            if (respone.Result == 1) {
                baseCommon.DataSource("#ProductID", "Tất cả", "Value", "Text", 0, respone.Data);
            }
        });
        //var p = baseCommon.AjaxGetPromise(baseCommon.Endpoint.GetListProductCredit);
        //p.done(function (respone) {
        //    if (respone.Result == 1) {
        //        baseCommon.DataSource("#ProductID", "Tất cả", "Value", "Text", 0, respone.Data);
        //    }
        //});
        //p.fail(function () {
        //    baseCommon.ShowErrorNotLoad("Chưa lấy được danh sách sản phẩm!");
        //});
    };
    var GetShop = function () {
        var data = { "type": 1, "generalSearch": "" };
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetLenderSearch, data, function (respone) {
            if (respone.Result == 1) {
                baseCommon.DataSource("#sl_search_shopID", "Tất cả", "ID", "FullName", 0, respone.Data);
            }
        }, "Chưa lấy được danh sách mã NDT!");
    };
    var submitFormSave = function () {

    };
    var Getstasticloanstatusbyshop = function (shopID) {
        if (typeof shopID == "undefined" || shopID == null || shopID == 0) {
            shopID = 3703;
        }
        w_Getstasticloanstatusbyshop(true);
        shopID = parseInt(shopID);
        var modelStasticloan = {
            "ShopID": shopID
        };
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.getstasticloanstatusbyshop, JSON.stringify(modelStasticloan), function (respone) {
            if (respone.Result == 1) {
                w_Getstasticloanstatusbyshop(false);
                $('#TotalLoanBorrowOverDebt').html(baseCommon.FormatCurrency(respone.Data.TotalLoanBorrowOverDebt));
                $('#OverDebtMoneyBorrow').html(baseCommon.FormatCurrency(respone.Data.OverDebtMoneyBorrow));
                $('#TotalLoanBorrow').html(baseCommon.FormatCurrency(respone.Data.TotalLoanBorrow));
                $('#DebtMoneyBorrow').html(baseCommon.FormatCurrency(respone.Data.DebtMoneyBorrow));
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        }, "Lấy dữ liệu thống kê không thành công vui lòng thử lại!");
        //$.ajax({
        //    url: baseCommon.Endpoint.getstasticloanstatusbyshop,
        //    type: 'POST',
        //    dataType: 'json',
        //    success: function (res) {
        //        if (typeof res.data != undefined && res.data != null) {
        //            $('#TotalLoanBorrowOverDebt').html(baseCommon.FormatCurrency(res.data.totalLoanBorrowOverDebt));
        //            $('#OverDebtMoneyBorrow').html(baseCommon.FormatCurrency(res.data.overDebtMoneyBorrow));
        //            $('#TotalLoanBorrow').html(baseCommon.FormatCurrency(res.data.totalLoanBorrow));
        //            $('#DebtMoneyBorrow').html(baseCommon.FormatCurrency(res.data.debtMoneyBorrow));
        //        }
        //    },
        //    error: function () {
        //    }
        //});
    };
    function w_Getstasticloanstatusbyshop(isShow) {

        if (isShow) {
            $('#TotalLoanBorrowOverDebt').html(0);
            $('#OverDebtMoneyBorrow').html(0);
            $('#TotalLoanBorrow').html(0);
            $('#DebtMoneyBorrow').html(0);
            $('#w_DebtMoneyBorrow').show();
            $('#w_TotalLoanBorrow').show();
            $('#w_OverDebtMoneyBorrow').show();
            $('#w_TotalLoanBorrowOverDebt').show();
        } else {
            $('#w_DebtMoneyBorrow').hide();
            $('#w_TotalLoanBorrow').hide();
            $('#w_OverDebtMoneyBorrow').hide();
            $('#w_TotalLoanBorrowOverDebt').hide();
        }
    };
    function formatRepo(repo) {

        if (repo.loading) return repo.text;
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__title'>" + repo.text + "</div>";
        return markup;
    };

    function formatRepoSelection(repo) {

        return repo.text;
    };
    function repoConvert(data) {

        var newObject = [];
        if (data != null && data != "") {
            $.each(data, function (key, value) {
                var newRepo = { id: value.ID, text: value.FullName };
                newObject.push(newRepo);
            });
        }
        return newObject;
    };
    var SearchLoan = function () {
        isSearchLoan = false;
        var paramValue;
        let searchParams = new URLSearchParams(window.location.search);
        if (searchParams.has(baseCommon.LoanIndex.loanID)) {
            paramValue = searchParams.get(baseCommon.LoanIndex.loanID);
            if (paramValue != null && paramValue != "" && paramValue.trim() != "" && !paramValue.includes("{")) {
                $('#loanID').val(parseFloat(paramValue));
            }
        }
        if (searchParams.has(baseCommon.LoanIndex.search)) {
            paramValue = searchParams.get(baseCommon.LoanIndex.search);
            if (paramValue != null && paramValue != "" && paramValue.trim() != "" && !paramValue.includes("{")) {
                $('#txt_search').val(paramValue);
            }
        }
        if (searchParams.has(baseCommon.LoanIndex.shopID)) {
            paramValue = searchParams.get(baseCommon.LoanIndex.shopID);
            if (paramValue != null && paramValue != "" && paramValue.trim() != "" && !paramValue.includes("{")) {
                paramValue = parseFloat(paramValue);
                $('#sl_search_shopID').val(parseFloat(paramValue)).change();
            }
        }
        if (searchParams.has(baseCommon.LoanIndex.lenderID)) {
            paramValue = searchParams.get(baseCommon.LoanIndex.lenderID);
            if (paramValue != null && paramValue != "" && paramValue.trim() != "" && !paramValue.includes("{")) {
                $('#sl_search_lenderID').val(parseFloat(paramValue)).change();
            }
        }
        if (searchParams.has(baseCommon.LoanIndex.productID)) {
            paramValue = searchParams.get(baseCommon.LoanIndex.productID);
            if (paramValue != null && paramValue != "" && paramValue.trim() != "" && !paramValue.includes("{")) {
                $('#ProductID').val(parseFloat(paramValue)).change();
            }
        }
        if (searchParams.has(baseCommon.LoanIndex.status)) {
            paramValue = searchParams.get(baseCommon.LoanIndex.status);
            if (paramValue != null && paramValue != "" && paramValue.trim() != "" && !paramValue.includes("{")) {
                $('#sl_search_status').val(paramValue).change();
            }
        }
        if (searchParams.has(baseCommon.LoanIndex.fromDate)) {
            paramValue = searchParams.get(baseCommon.LoanIndex.fromDate);
            if (paramValue != null && paramValue != "" && paramValue.trim() != "" && !paramValue.includes("{")) {
                $('#fromDate').val(paramValue);
            }
        }
        if (searchParams.has(baseCommon.LoanIndex.toDate)) {
            paramValue = searchParams.get(baseCommon.LoanIndex.toDate);
            if (paramValue != null && paramValue != "" && paramValue.trim() != "" && !paramValue.includes("{")) {
                $('#toDate').val(paramValue);
            }
        }
        isSearchLoan = true;
        LoadData();
    };

    var Save_LoanTransfer = function () {
        var btn = $('#btn_save_loantransfer');
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        var lenderID = $("#sl_search_TransferLender").val();
        var date = $("#date_transfer").val();
        if (lenderID == 0 || lenderID == null) {
            showMesage("danger", "Chưa chọn NDDT chuyển nhượng");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return false;
        }
        if (date == "" || date == null) {
            showMesage("danger", "Chưa chọn thời gian áp dụng");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return false;
        }
        var request = {
            "LoanID": parseInt($("#hdd_LoanID").val()),
            "LenderID": parseInt(lenderID),
            "FromDate": date
        };
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.TranferLoan, JSON.stringify(request), function (respone) {
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                $('#modal_partialLoanTransfer').modal('hide');
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            }
        });
    };

    function DownLoadFileExcel() {
        var link = document.createElement("a");
        link.download = 'file_mau_import_excel';
        link.href = '/static/Upload/mau_file_loan_tranfer.xlsx';
        link.click();
    };
    var ImportExcelStatus = function () {
        $("#upload-status").click();
    };
    $("#upload-status").change(function (evt) {
        var selectedFile = evt.target.files[0];
        if (selectedFile == undefined) {
            return;
        }
        var reader = new FileReader();
        var jsonData = [];
        var jsonError = "";
        reader.onload = function (event) {
            var data = event.target.result;
            var workbook = XLSX.read(data, {
                type: 'binary'
            });
            workbook.SheetNames.forEach(function (sheetName) {
                var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                var json_object = JSON.stringify(XL_row_object);
                var objData = JSON.parse(json_object);

                for (var i = 0; i < objData.length; i++) {
                    var newObj = {};
                    var column_ContractCode = objData[i]["ContractCode"];
                    var column_CurrentLenderCode = objData[i]["CurrentLenderCode"];
                    var column_LenderCode = objData[i]["LenderCode"];
                    var column_ApplicableDate = objData[i]["ApplicableDate"];
                    if (typeof column_ContractCode != "undefined" || typeof column_CurrentLenderCode != "undefined" || typeof column_LenderCode != "undefined" || typeof column_ApplicableDate != "undefined") {
                        if (column_ContractCode !== null && typeof column_ContractCode != "undefined" && typeof column_ContractCode === 'number') {
                            if (column_CurrentLenderCode !== null && typeof column_CurrentLenderCode != "undefined") {
                                if (column_LenderCode !== null && typeof column_LenderCode != "undefined") {
                                    if (column_ApplicableDate !== null && typeof column_ApplicableDate != "undefined") {
                                        newObj.ContractCode = column_ContractCode;
                                        newObj.CurrentLenderCode = column_CurrentLenderCode;
                                        newObj.LenderCode = column_LenderCode;
                                        newObj.FromDate = column_ApplicableDate;
                                        jsonData.push(newObj)
                                    } else {
                                        jsonError += "Cột ApplicableDate dòng: " + (i + 2) + " - bỏ trống hoặc sai cú pháp . Vui lòng xem lại file.";
                                        break;
                                    }
                                } else {
                                    jsonError += "Cột LenderCode dòng: " + (i + 2) + " - bỏ trống hoặc sai cú pháp . Vui lòng xem lại file.";
                                    break;
                                }
                            } else {
                                jsonError += "Cột CurrentLenderCode dòng: " + (i + 2) + " - bỏ trống hoặc sai cú pháp . Vui lòng xem lại file.";
                                break;
                            }
                        }
                        else {
                            jsonError += "Cột ContractCode dòng: " + (i + 2) + " - bỏ trống hoặc sai cú pháp . Vui lòng xem lại file.";
                            break;
                        }
                    }
                }
            })
            if (jsonData.length < 1 && jsonError.length < 1) {
                jsonError = "File excel không đúng mẫu, vui lòng chọn lại file.";
            }
            $("#upload-status").val("");

            if (jsonError.length > 0) {
                baseCommon.ShowErrorNotLoad(jsonError);
                return;
            }
            var requestData = {
                LstLoanLender: jsonData,
            }
            console.log(requestData)
            baseCommon.AjaxDone('POST', baseCommon.Endpoint.TranferListLoan, JSON.stringify(requestData), function (res) {
                if (res.Result == 1) {
                    baseCommon.ShowSuccess("Upload dữ liệu thành công: " + res.Message);
                } else {
                    baseCommon.ShowErrorNotLoad(res.Message);
                }
            });
        }
        reader.onerror = function (event) {
            console.error("File could not be read! Code " + event.target.error.code);

        };
        reader.readAsBinaryString(selectedFile);
    });
    return {
        Init: Init,
        LoadData: LoadData,
        ShowModal: ShowModal,
        submitFormSave: submitFormSave,
        DetailModal: DetailModal,
        Getstasticloanstatusbyshop: Getstasticloanstatusbyshop,
        timkiem: timkiem,
        GetProduct: GetProduct,
        GetShop: GetShop,
        Delete: Delete,
        RowClick: RowClick,
        Option: Option,
        LoanTransfer: LoanTransfer,
        Save_LoanTransfer: Save_LoanTransfer,
        DownLoadFileExcel: DownLoadFileExcel,
        ImportExcelStatus: ImportExcelStatus
    };
};
$(document).ready(function () {
    loan.Init();
});