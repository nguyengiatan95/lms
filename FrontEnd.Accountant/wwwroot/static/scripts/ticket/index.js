﻿var ticketLender = function () {
    var loadData = function () {
        var datatable = $('#dv_result').KTDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/Ticket/GetTicket',
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 50,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },

            // layout definition
            layout: {
                scroll: true,
                footer: false,
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
                //input: $('#txt_search'),
            },
            //translate:"vi",
            // columns definition
            columns: [
                {
                    field: 'stt',
                    title: 'STT',
                    width: 40,
                    textAlign: 'center',
                    sortable: false,
                    template: function (row, index, datatable) {
                        var pageIndex = datatable.API.params.pagination.page;
                        var record = 1;
                        if (pageIndex > 1) {
                            var pageSize = datatable.API.params.pagination.perpage;
                            record = (pageIndex - 1) * pageSize + 1;
                        }
                        return index + record;
                    }
                },
                {
                    field: 'typeName',
                    title: 'Loại',
                    textAlign: 'center',
                },
                {
                    field: 'fromDepartment',
                    title: 'Từ',
                    textAlign: 'left',
                },
                {
                    field: 'toDepartment',
                    title: 'Đến',
                    textAlign: 'left',
                },
                {
                    field: 'note',
                    title: 'Nội dung',
                    textAlign: 'left',
                },

                {
                    field: 'createDate',
                    title: 'Ngày tạo',
                    textAlign: 'center',
                    template: function (row) {
                        return `${row.createDate == null ? "" : formattedDateHourMinutes(row.createDate)} <div>${row.fromUserName}</div>`;
                    },
                },
                {
                    field: 'status',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    template: function (row) {
                        if (row.status == 1) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Đã xử lý</span>';
                        } else if (row.status == 0) {
                            return '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">Chờ xử lý</span>';
                        }
                        else if (row.status == -11) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Hủy</span>';
                        }
                    },
                },
                {
                    field: 'action',
                    title: 'Chức năng',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var htmlAction = "";
                        if (row.status == 0) {
                            if (row.type == ticket_Type_LenderRutVon) {
                                htmlAction += '\
                                  <a class="btn btn-success btn-icon btn-sm" title="Tạo phiếu rút vốn"  onclick="invoice.CreateInvoice('+ groupInvoiceType_PaySlipLenderWithdraw + ',\'Tạo phiếu chi\',\'Lender rút vốn\',' + row.ticketID + ',\'' + row.note + '\');">\
                                    <i class="fa fa-wallet" onclick=""></i>\
                                  </a>';
                            } else if (row.type == ticket_Type_LenderTamUng) {
                                htmlAction += '\
                                  <a class="btn btn-success btn-icon btn-sm" title="Tạo phiếu tạm ứng"   onclick="invoice.CreateInvoice('+ groupInvoiceType_PaySlipLenderWithdraw + ',\'Tạo phiếu chi\',\'Lender rút vốn\',' + row.ticketID + ',\'' + row.note + '\');">\
                                    <i class="fa fa-money-bill-alt"></i>\
                                  </a>';
                            }
                            htmlAction += '\
                          <a class="btn btn-danger btn-icon btn-sm" title="Hủy" onclick="ticketLender.deleteTicket('+ row.ticketID + ')">\
                            <i class="fa fa-trash"></i>\
                          </a>';
                        }
                        return htmlAction;
                    }
                }
            ],

        });
        $('#fromDate').on('change', function () {
            datatable.search($(this).val().toLowerCase(), 'fromDate');
        });
        $('#toDate').on('change', function () {
            datatable.search($(this).val().toLowerCase(), 'toDate');
        });
        $('#type_ticketID').on('change', function () {
            datatable.search($(this).val().toLowerCase(), 'type');
        });
        $('#status_ticketID').on('change', function () {
            datatable.search($(this).val().toLowerCase(), 'status');
        });
        $('#btnGetData_ticket').on('click', function () {
            datatable.search();
        });
    };
    var sumitFormSave = function () {
        $('#btn_save_ticket').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    fromDepartment: {
                        required: true,
                    },
                    toDepartment: {
                        required: true,
                    },
                    type: {
                        required: true,
                    },
                    note: {
                        required: true,
                    },
                },
                messages: {
                    fromDepartment: {
                        required: "Vui lòng chọn phòng ban gửi",
                    },
                    toDepartment: {
                        required: "Vui lòng chọn phòng ban nhận",
                    },
                    type: {
                        required: "Vui lòng chọn loại",
                    },
                    note: {
                        required: "Vui lòng nhập nội dung",
                    },
                }
            });
            if (!form.valid()) {
                return;
            }
            var fromDepartment = $('#sl_ticket_create_fromDepartment').val();
            var toDepartment = $('#sl_ticket_create_toDepartment').val();
            var type = $('#sl_ticket_create_type').val();
            var note = $('#txt_ticket_create_note').val();
            var ticketID = $('#hdd_create_ticketID').val();
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            var request = {
                "FromDepartmentID": parseInt(fromDepartment),
                "ToDepartmentID": parseInt(toDepartment),
                "FromUserID": parseInt(userLogin_UserID),
                "Type": parseInt(type),
                "Note": note,
                "TicketID": parseInt(ticketID),
            };
            if (ticketID == 0) {
                baseCommon.AjaxDone("POST",baseCommon.Endpoint.CreateTicket, JSON.stringify(request), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        $('#btnGetData').trigger('click');
                        $('#modal_add_edit_ticket').modal('hide');
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                });
            } else {
                baseCommon.AjaxDone("POST",baseCommon.Endpoint.UpdateTicket, JSON.stringify(request), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        $('#btnGetData').trigger('click');
                        $('#modal_add_edit_ticket').modal('hide');
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                } );
            }
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
        });
    };
    var loadDetailTicket = function (jsonData = '') {
        if (jsonData == '') {
            $('#headerId').text('Thêm mới');
            $('#sl_ticket_create_fromDepartment').val('').change();
            $('#sl_ticket_create_toDepartment').val('').change();
            $('#sl_ticket_create_type').val('').change();
            $('#txt_ticket_create_note').val('');
            $('#hdd_create_ticketID').val(0);
            $('#btn_save_ticket').html('<i class="fa fa-save"></i> Thêm mới');
        } else {
            var data = JSON.parse(jsonData);
            $('#headerId').text('Cập nhật');
            $('#sl_ticket_create_fromDepartment').val(data.fromDepartmentID).change();
            $('#sl_ticket_create_toDepartment').val(data.toDepartmentID).change();
            $('#sl_ticket_create_type').val(data.type).change();
            $('#txt_ticket_create_note').val(data.note);
            $('#hdd_create_ticketID').val(data.ticketID);
            $('#btn_save_ticket').html('<i class="fa fa-save"></i> Cập nhật');
        }
    }
    var deleteTicket = function (ticketID) {
        swal.fire({
            html: "Bạn có chắc chắn muốn xóa yêu cầu này không?",
            type: "warning",
            showCancelButton: !0,
            cancelButtonText: "Hủy",
            confirmButtonText: "Đồng ý"
        }).then(function (result) {
            if (result.value) {
                var request = {
                    "UserID": parseInt(userLogin_UserID),
                    "TicketID": parseInt(ticketID),
                    "Status": -11
                };
                baseCommon.AjaxDone("POST",baseCommon.Endpoint.ChangeStatusTicket, JSON.stringify(request), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        $('#btnGetData_ticket').trigger('click');
                        $('#modal_add_edit_ticket').modal('hide');
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                });
            }
        });
    }
    var confirmDone = function (ticketID) {
        var request = {
            "UserID": parseInt(userLogin_UserID),
            "TicketID": parseInt(ticketID),
            "Status": 1
        };
        baseCommon.AjaxDone("POST",baseCommon.Endpoint.ChangeStatusTicket, JSON.stringify(request), function (respone) {
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                $('#btnGetData_ticket').trigger('click');
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        } );
    }
    return {
        loadData: loadData,
        sumitFormSave: sumitFormSave,
        loadDetailTicket: loadDetailTicket,
        deleteTicket: deleteTicket,
        confirmDone: confirmDone
    };
}();