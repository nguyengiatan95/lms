﻿var isClearSession = false;
var access_token = "";
var USER_ID = 0;
var AdminUrl = '';
var baseCommon = new function () {
    var Option = {
        All: -111,
        Pagesize: 10,
        PagesizeExcel: 100000
    };
    var VerifyInvoice = {
        DeleteInvoice: 1,
        ReturnInvoiceConsiderCustomer: 2,
        VerifyInvoiceConsiderCustomer: 3
    };
    var BaseUrl, Endpoint, UserLoginModel = {}, isCalledRefreshToken = "", LoginUrl_ = LoginUrl;
    var SetBase = function () {
        BaseUrl = {
            "Admin": ApiUrl + "admin/",
            "Loan": ApiUrl + "loan/",
            "Authen": ApiUrl + "authen/",
            "Invoice": ApiUrl + "invoice/",
            "Lender": ApiUrl + "lender/",
            "Customer": ApiUrl + "customer/",
            "Transaction": ApiUrl + "transaction/",
            "Insurance": ApiUrl + "insurance/"
        };
        Endpoint = {
            "GetLoanByID": BaseUrl["Loan"] + "api/Loan/getLoanByID/",
            "GetLstLoanInfoByCondition": BaseUrl["Loan"] + "api/Loan/GetLstLoanInfoByCondition",
            "GetLstPaymentScheduleByLoanID": BaseUrl["Loan"] + "api/loan/GetLstPaymentScheduleByLoanID/",
            "GetListProductCredit": BaseUrl["Loan"] + "api/Meta/GetListProductCredit",
            "GetLenderSearch": BaseUrl["Lender"] + "api/Lender/GetLenderSearch",
            "ExtendLoanTime": BaseUrl["Loan"] + "api/loan/ExtendLoanTime",
            "GenPaymentScheduleByLoanID": BaseUrl["Loan"] + "api/loan/GenPaymentScheduleByLoanID",
            "GetDetailBankCard": BaseUrl["Loan"] + "api/Meta/GetBank/",
            "GetInvoiceInfosByConditionTableAjax": BaseUrl["Invoice"] + "api/Invoice/GetInvoiceInfosByConditionTableAjax",
            "getstasticloanstatusbyshop": BaseUrl["Loan"] + "api/report/getstasticloanstatusbyshop",
            "GetLstLoanByFilterCustomer": BaseUrl["Loan"] + "api/loan/GetLstLoanByFilterCustomer",
            "PayPartialScheduleInPeriods": BaseUrl["Loan"] + "api/loan/PayPartialScheduleInPeriods",
            "GetHistoryCommentLos": BaseUrl["Loan"] + "api/loan/GetHistoryCommentLos/",
            "GetDataImagesLos": BaseUrl["Loan"] + "api/loan/GetListImagesLos/",
            "GetTransactionByLoanID": BaseUrl["Loan"] + "api/loan/GetTransactionByLoanID/",
            "PaymentMoneyFullSchedule": BaseUrl["Loan"] + "api/loan/PaymentMoneyFullSchedule/",
            "GetBankCard": BaseUrl["Loan"] + "api/Meta/GetBankCard?BankCardID=",
            "CreateInvoiceCustomer": BaseUrl["Invoice"] + "api/Invoice/CreateInvoiceCustomer",
            "CreateInvoicePaySlipCustomer": BaseUrl["Invoice"] + "api/Invoice/CreateInvoicePaySlipCustomer",
            "CreateInvoiceBackMoneyCustomer": BaseUrl["Invoice"] + "api/Invoice/CreateInvoiceBackMoneyCustomer",
            "CreateInvoiceConsiderCustomer": BaseUrl["Invoice"] + "api/Invoice/CreateInvoiceConsiderCustomer",
            "CreateInvoiceOther": BaseUrl["Invoice"] + "api/Invoice/CreateInvoiceOther",
            "CreateInvoiceOnBehalfShop": BaseUrl["Invoice"] + "api/Invoice/CreateInvoiceOnBehalfShop",
            "CreateInvoiceBankInterest": BaseUrl["Invoice"] + "api/Invoice/CreateInvoiceBankInterest",
            "CreateInvoiceInternal": BaseUrl["Invoice"] + "api/Invoice/CreateInvoiceInternal",
            "CreateInvoiceLender": BaseUrl["Invoice"] + "api/Invoice/CreateInvoiceLender",
            "SearchInvoiceAdvanceSlipLender": BaseUrl["Invoice"] + "api/Invoice/SearchInvoiceAdvanceSlipLender",
            "GetInvoiceInfosByCondition": BaseUrl["Invoice"] + "api/Invoice/GetInvoiceInfosByCondition",
            "CreateBankCardOfShop": BaseUrl["Loan"] + "api/BankCardOfShop/CreateBankCardOfShop",
            "GetMoneyNeedCloseLoanByLoanID": BaseUrl["Loan"] + "api/loan/GetMoneyNeedCloseLoanByLoanID",
            "GetHistoryCommentLender": BaseUrl["Loan"] + "api/CommentIndemnifyInsurrance/GetHistoryComment/",
            "ReportVatHub": BaseUrl["Loan"] + "api/Report/ReportVatHub",
            "ReportVat": BaseUrl["Loan"] + "api/Report/ReportVat",
            "ReportFineInterestLate": BaseUrl["Loan"] + "api/Report/ReportFineInterestLate",
            "ReportVatLender": BaseUrl["Loan"] + "api/Report/ReportVatLender",
            "ReportPenaltyFeeVatHub": BaseUrl["Loan"] + "api/Report/ReportPenaltyFeeVatHub",
            "ReportVatGCash": BaseUrl["Loan"] + "api/Report/ReportVatGCash",
            "GetShopOfBankCardID": BaseUrl["Loan"] + "api/BankCardOfShop/GetShopOfBankCardID/",
            "VerifyInvoiceCustomer": BaseUrl["Invoice"] + "api/Invoice/VerifyInvoiceCustomer",
            "ReportMoneyDetail": BaseUrl["Loan"] + "api/loan/ReportMoneyDetail",
            //"GetShopOfBankCardID": BaseUrl["Loan"] + "api/BankCardOfShop/GetShopOfBankCardID/",
            "ForceCloseLoanByLoanID": BaseUrl["Loan"] + "api/Loan/ForceCloseLoanByLoanID",
            "GetHistoryCommentInvoice": BaseUrl["Invoice"] + "api/Invoice/GetHistoryCommentInvoice",
            "DeleteInvoice": BaseUrl["Invoice"] + "api/Invoice/DeleteInvoice",
            "ReturnInvoiceConsiderCustomer": BaseUrl["Invoice"] + "api/Invoice/ReturnInvoiceConsiderCustomer",
            "GetListImgInvoiceConsider": BaseUrl["Invoice"] + "api/Invoice/GetListImgInvoiceConsider",
            "UpdatePathImg": BaseUrl["Invoice"] + "api/Invoice/UpdatePathImg",
            "GetLstInvoiceInfoByCondition": BaseUrl["Invoice"] + "api/Invoice/GetLstInvoiceInfoByCondition",
            "VerifyInvoiceConsiderCustomer": BaseUrl["Invoice"] + "api/Invoice/VerifyInvoiceConsiderCustomer",
            "GetLstSmsInfoByCondition": BaseUrl["Loan"] + "api/sms/GetLstSmsInfoByCondition",
            "GetGcash": BaseUrl["Loan"] + "api/Meta/GetGcash/",
            "ReportVatHUbWithGCash": BaseUrl["Loan"] + "api/Report/ReportVatHUbWithGCash",
            "DisbursementWaitingLOS": BaseUrl["Loan"] + "api/LOS/DisbursementWaitingLOS",
            "GetHistoryCommentLos2": BaseUrl["Loan"] + "api/LOS/GetHistoryCommentLos",
            "SaveCommentHistoryLos": BaseUrl["Loan"] + "api/LOS/SaveCommentHistory",
            "LockLoan": BaseUrl["Loan"] + "api/LOS/LockLoan",
            "DisbursementLoan": BaseUrl["Loan"] + "api/LOS/DisbursementLoan",
            "PushLoanLender": BaseUrl["Loan"] + "api/LOS/PushLoanLender",
            "GetLoanCreditDetailLos": BaseUrl["Loan"] + "api/LOS/GetLoanCreditDetailLos/",
            "GetDataImagesLenderLos": BaseUrl["Loan"] + "api/LOS/GetListImagesLos/",
            "GetHistoryCommentLenderLos": BaseUrl["Loan"] + "api/LOS/GetHistoryCommentLos/",
            "SaveCommentLender": BaseUrl["Loan"] + "api/CommentIndemnifyInsurrance/SaveComment",
            "GetLender": BaseUrl["Loan"] + "api/Meta/GetLender",
            "GetBankCard2": BaseUrl["Loan"] + "api/Meta/GetBankCard",
            "ReturnLoanLender": BaseUrl["Loan"] + "api/LOS/ReturnLoan",
            "ReportInsurance": BaseUrl["Insurance"] + "api/Insurance/ReportInsurance",
            "GetDepartment": BaseUrl["Admin"] + "api/Department/GetDepartment",
            "CreateTicket": BaseUrl["Invoice"] + "api/Ticket/CreateTicket",
            "UpdateTicket": BaseUrl["Invoice"] + "api/Ticket/UpdateTicket",
            "ChangeStatusTicket": BaseUrl["Invoice"] + "api/Ticket/ChangeStatusTicket",
            "CountNotifyTicket": BaseUrl["Invoice"] + "api/Ticket/CountNotifyTicket",
            "TransferLoan": BaseUrl["Loan"] + "api/LOS/ChangeDisbursementByAccountant",
            "ChangeStatusAccountantToAutoDisbursement": BaseUrl["Loan"] + "api/LOS/ChangeStatusAccountantToAutoDisbursement",
            "CreateLoanByAccountant": BaseUrl["Loan"] + "api/loan/CreateLoanByAccountant",
            "GetOtpDisburmentForLoanCredit": BaseUrl["Loan"] + "api/loan/GetOtpDisburmentForLoanCredit",
            "ReconciliationStatusGetOTP": BaseUrl["Loan"] + "api/loan/ReconciliationStatusGetOTP",
            "CreateLoanDebt": BaseUrl["Loan"] + "api/Loan/CreateLoanDebt",
            "GetLoanDebtByLoanID": BaseUrl["Loan"] + "api/Loan/GetLoanDebtByLoanID",
            "PayLoanDebtByLoanID": BaseUrl["Loan"] + "api/Loan/PayLoanDebtByLoanID",
            "ReportInOutMoneyBankDate": BaseUrl["Invoice"] + "api/CashBankCardByDate/ReportInOutMoneyBankDate",
            "InvoiceInfosByBankCard": BaseUrl["Invoice"] + "api/Invoice/InvoiceInfosByBankCard",
            "LoanIndexContactCode": "/Loan/Index?ContactCode=",
            "LoanIndex": "/Loan/Index?" + LoanIndex.search + "={" + LoanIndex.search + "}&" + LoanIndex.shopID + "={" + LoanIndex.shopID + "}&" + LoanIndex.lenderID + "={" + LoanIndex.lenderID + "}&" + LoanIndex.productID + "={" + LoanIndex.productID + "}&" + LoanIndex.status + "={" + LoanIndex.status + "}&" + LoanIndex.fromDate + "={" + LoanIndex.fromDate + "}&" + LoanIndex.toDate + "={" + LoanIndex.toDate + "}&" + LoanIndex.loanID + "={" + LoanIndex.loanID + "}",
            "InvoiceIndex": "/Invoice/Index?" + InvoiceIndex.InvoiceType + "={" + InvoiceIndex.InvoiceType + "}&" + InvoiceIndex.InvoiceSubType + "={" + InvoiceIndex.InvoiceSubType + "}&" + InvoiceIndex.ShopID + "={" + InvoiceIndex.ShopID + "}&" + InvoiceIndex.BankCardID + "={" + InvoiceIndex.BankCardID + "}&" + InvoiceIndex.Status + "={" + InvoiceIndex.Status + "}&" + InvoiceIndex.FromDate + "={" + InvoiceIndex.FromDate + "}&" + InvoiceIndex.ToDate + "={" + InvoiceIndex.ToDate + "}",
            "ReportExtraMoneyCustomer": BaseUrl["Loan"] + "api/Report/ReportExtraMoneyCustomer",
            "GetRefreshToken": "/Home/GetRefreshToken",
            "ReportVATForLender": BaseUrl["Loan"] + "api/Report/ReportVATForLender",
            "ListBankCard": BaseUrl["Loan"] + "api/BankCard/ListBankCard",           
            "DeleteLoanDebt": BaseUrl["Loan"] + "api/loan/DeleteLoanDebt",          
            "ContractLoanWithLenderByLoanID": BaseUrl["Loan"] + "api/ContractLoanWithLender/ContractLoanWithLenderByLoanID/",
            "TranferLoan": BaseUrl["Loan"] + "api/loan/TranferLoan",
            "TranferListLoan": BaseUrl["Loan"] + "api/loan/TranferListLoan",
            "GetLenderSearchByLenderCode": BaseUrl["Lender"] + "api/Lender/GetLenderSearchByLenderCode"
            //
        };
    };
    var LoanIndex = {
        loanID: "loanID",
        search: "search",
        shopID: "shopID",
        lenderID: "lenderID",
        productID: "productID",
        status: "status",
        fromDate: "fromDate",
        toDate: "toDate",
    };
    var InvoiceIndex = {
        InvoiceID: "InvoiceID",
        InvoiceType: "InvoiceType",
        InvoiceSubType: "InvoiceSubType",
        ShopID: "ShopID",
        BankCardID: "BankCardID",
        Status: "Status",
        FromDate: "FromDate",
        ToDate: "ToDate",
    };
    var LoginPage = function () {
        var referrer = window.location.href;
        referrer = referrer.replace(':', '>').replace(/\//gi, '<').replace('&', '*');
        $.removeCookie(STATIC_USERMODEL, { path: '/' });
        window.location = LoginUrl + referrer;
    };
    var HomePage = function () {
        window.open(AdminUrl, "_self");
    };
    var ChangePassWord = function () {
        var urlChangePass = AdminUrl + "/User/ChangePassword";
        window.open(urlChangePass, "_self");
    };
    var InitUser = new Promise(function (myResolve, myReject) {
        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                if (userModel != null && userModel != "") {
                    var user = JSON.parse(userModel);
                    access_token = user.Token;
                    AdminUrl = user.AdminUrl;
                    USER_ID = user.UserID;
                    SetBase();
                    myResolve(user);
                } else {
                    myReject("Error");
                    LoginPage();
                }
            } else {
                LoginPage();
            }
        } catch (err) {
            LoginPage();
        }
    });
    function diff_minutes(dt2, dt1) {

        var diff = (dt2.getTime() - dt1.getTime()) / 1000;
        diff /= 60;
        return Math.round(diff);

    }
    var RefreshToken = function () {
        //ngủ cho tới khi gần hết time Token
        //mở tab mới gọi chức năng check token
        // check token nếu còn dài hạn trả lại ngay = cách gọi lại tất cả các trang 
        // nếu hết hạn thì refresh token và gọi lại tất cả các trang
        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                var user = JSON.parse(userModel);
                var remainTime = diff_minutes(new Date(user.TimeExpired), new Date());
                var waittime = 4;
                if (remainTime < waittime) {
                    //gọi RefreshToken
                    setTimeout(() => { RefreshToken(); }, 10000);
                    if (isCalledRefreshToken == "") {
                        isCalledRefreshToken = "called";
                        baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetRefreshToken, "", function (respone) {
                            if (respone.status == 1) {
                                access_token = respone.data.token;
                                user.Token = respone.data.token;
                                user.TimeExpired = respone.data.timeExpired;
                                user.TimeExpiredString = respone.data.timeExpiredString;
                                $.cookie(STATIC_USERMODEL, JSON.stringify(user), { expires: 30, path: '/', domain: user.DomainCookie, secure: true });
                            } else {
                                ShowErrorNotLoad("Phiên làm việc sắp hết hạn, vui lòng F5 thử lại!");
                            }
                            isCalledRefreshToken = "";
                        }, "Phiên làm việc sắp hết hạn, vui lòng F5 thử lại!");
                    }
                } else {
                    var timeOut = ((remainTime - waittime) + 1) * 60 * 1000;
                    setTimeout(() => {
                        isCalledRefreshToken = "";
                        RefreshToken();
                    }, timeOut);
                }
            } else {
                setTimeout(() => { RefreshToken(); }, 2000);
            }
        } catch (err) {
            setTimeout(() => { RefreshToken(); }, 2000);
        }
    }
    var Init = function () {
        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                var userModel = $.cookie(STATIC_USERMODEL);
                if (userModel != null && userModel != "") {
                    var user = JSON.parse(userModel);
                    AdminUrl = user.AdminUrl;
                    STATIC_USERMODEL = user.CookieName;
                    UserLoginModel = user.UserLoginModel;
                    RefreshToken();
                    SetBase();
                    var fullShortName = (user.FullName).split(" ");
                    var shortName = fullShortName[(fullShortName.length - 1)];
                    if (fullShortName.length >= 2) {
                        shortName = fullShortName[(fullShortName.length - 2)] + " " + shortName;
                    }
                    $('#_UserFullNameId').html(shortName);
                    $('#_UserFullNameShortId').html(user.UserName.substring(0, 1).toUpperCase());
                    $('#_UserFullNameShort2Id').html(user.UserName.substring(0, 1).toUpperCase());
                } else {
                    return null;
                }
            } else {
                LoginPage();
            }
        } catch (err) {
            LoginPage();
        }

    };
    var Pagesize = function () {
        return 20;
    };
    var GetToken = function () {
        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                var user = JSON.parse(userModel);
                return user.Token;
            } else {
                LoginPage();
                return null;
            }
        } catch (err) {
            LoginPage();
        }
    };
    var GetUser = function () {
        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                var user = JSON.parse(userModel);
                return user;
            } else {
                LoginPage();
                return null;
            }
        } catch (err) {
            LoginPage();
        }
    };
    var AjaxDone = function (method, endpoint, data, done, error, errFuntion) {
        return $.ajax({
            type: method,
            headers: {
                'Authorization': access_token
            },
            url: endpoint,
            data: data,
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).done(done).fail(function (e) {
            if (e.status == 401) {
                LoginPage();
            } else if (e.status == 403) {
                var params = (new URL(endpoint)).pathname;
                baseCommon.ShowErrorNotLoad("Vui lòng liên hệ quản lý để được phân quyền chức năng!<br>" + params);
            } else if (errFuntion == null || errFuntion == "") {
                eval(errFuntion);
            } else {
                if (error == null || error == "") {
                    error = e;
                }
                baseCommon.ShowErrorNotLoad(error);
            }
        });
    };
    var AjaxSuccess = function (method, endpoint, data, success, error) {
        return $.ajax({
            type: method,
            headers: {
                'Authorization': access_token
            },
            url: endpoint,
            data: data,
            contentType: "application/json; charset=utf-8",
            success: success,
            error: function (e) {
                if (e.status == 401) {
                    LoginPage();
                } else if (e.status == 403) {
                    var params = (new URL(endpoint)).pathname;
                    baseCommon.ShowErrorNotLoad("Vui lòng liên hệ quản lý để được phân quyền chức năng!<br>" + params);
                } else {
                    if (error == null || typeof error == "undefined" || error == "") {
                        error = e.responseText;
                    }
                    baseCommon.ShowErrorNotLoad(error);
                }
            }
        });
    };
    var SelectDataSource = function (url, element, placeholder, value, name, selectedId, data) {
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            processData: false,
            contentType: false,
        }).done(function (respone) {
            $(element).html('');
            $(element).append('<option value="">' + placeholder + '</option>');
            $.each(respone.data, function (key, item) {
                var selected = "";
                if (Array.isArray(selectedId)) {
                    if (selectedId.find((itemSelected) => itemSelected[value] === item[value])) {
                        selected = "selected";
                    }
                } else {
                    if (item[value] == selectedId) {
                        selected = "selected";
                    }
                }
                $(element).append('<option ' + selected + ' value="' + item[value] + '">' + item[name] + '</option>');
            });
        });
    };
    var GetDataSource = function (url, data, done) {
        return $.ajax({
            type: "POST",
            url: url,
            data: data,
            processData: false,
            contentType: false,
        }).done(done);
    };
    var DataSource = function (element, placeholder, value, name, selectedId, data, valueDefaul = -111) {
        try {
            $(element).html('');
            $(element).append('<option value="' + valueDefaul + '">' + placeholder + ' </option>');
            $.each(data, function (key, item) {
                var selected = "";
                if (Array.isArray(selectedId)) {
                    if (selectedId.find((itemSelected) => itemSelected[value] === item[value])) {
                        selected = "selected";
                    }
                } else {
                    if (item[value] == selectedId) {
                        selected = "selected";
                    }
                }
                $(element).append('<option ' + selected + ' value="' + item[value] + '">' + item[name] + '</option>');
            });
        } catch (err) {

        }
    };
    var FormatCurrency = function FormatCurrency(Num) {
        Num += '';
        Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
        Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
        x = Num.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1))
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        return x1 + x2;
    };
    var ShowErrorNotLoad = function (message) {
        toastr.error(message);
    };
    var ShowSuccess = function (message) {
        toastr.success(message);
    };
    function formatNumber(nStr, decSeperate, groupSeperate) {
        nStr += '';
        x = nStr.split(decSeperate);
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + groupSeperate + '$2');
        }
        return x1 + x2;
    };
    function FormatDate(dateFormat, format) {
        var date = moment(dateFormat);
        return date.format(format);
    };
    function FormatRepo(repo) {

        if (repo.loading) return repo.text;
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__title'>" + repo.text + "</div>";
        return markup;
    };

    function FormatRepoSelection(repo) {

        return repo.text;
    };
    function RepoConvert(data) {

        var newObject = [];
        if (data != null && data != "") {
            $.each(data, function (key, value) {
                var newRepo = { id: value.ID, text: value.FullName };
                newObject.push(newRepo);
            });
        }
        return newObject;
    };
    var ButtonSubmit = function (isVisible, id) {
        if (!isVisible) {
            $(id).addClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
            $(id).prop("disabled", true);
        } else {
            $(id).removeClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
            $(id).prop("disabled", false);
        }
    }
    var IconWaiting = function (isVisible, id) {
        if (!isVisible) {
            $(id).addClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
        } else {
            $(id).removeClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
        }
    }

    var html_money_2 = function (money) {
        try {
            if (Number(money) == "NaN" || Number(money) == 0) {
                return "<span class='kt-font-blue'> 0 </span>";
            }
            var numString = money.toString();
            var beforeDot = numString, afterDot = "";
            if (numString.includes(".")) {
                // chỉ lấy đến dấu "."
                var n = numString.indexOf(".");
                beforeDot = numString.substring(0, n);
                afterDot = numString.substring(n, numString.lenght);
            }
            if (beforeDot.length > 3) {
                // 000012,345,678 
                // 0.123
                for (var y = 0; beforeDot.length >= 0; y++) {
                    var zero = beforeDot.toString().substring(0, 1);
                    if (zero == "0" && beforeDot.length > 1) {
                        beforeDot = beforeDot.substring(1, beforeDot.lenght);
                    } else {
                        break;
                    }
                }
                var i, from, beforeDotTemp = "", tempNum = "", tempCount = 0;
                for (i = beforeDot.length; i > 0; i--) {
                    from = i - 1;
                    tempCount++;
                    var num = beforeDot.substring(from, i);
                    tempNum = num.concat(tempNum);
                    if (tempCount >= 3 || i <= 0) {
                        if (i > 1) {
                            tempNum = ",".concat(tempNum);
                        }
                        beforeDotTemp = tempNum.concat(beforeDotTemp);
                        tempCount = 0;
                        tempNum = "";
                    }
                }
                if (tempNum != "") {
                    beforeDotTemp = tempNum.concat(beforeDotTemp);
                }
                numString = beforeDotTemp.concat(afterDot);
            }
            if (money >= 0) {
                return "<span class='kt-font-blue'> " + numString + " </span>";
            } else {
                return "<span class='kt-font-danger'> " + numString + " </span>";
            }
        } catch (e) {
            return "<span class='kt-font-blue'> 0 </span>";
        }
    }
    var html_money = function (money) {
        try {
            if (Number(money) == "NaN" || Number(money) == 0) {
                return 0;
            }
            var numString = money.toString();
            var beforeDot = numString, afterDot = "";
            if (numString.includes(".")) {
                // chỉ lấy đến dấu "."
                var n = numString.indexOf(".");
                beforeDot = numString.substring(0, n);
                afterDot = numString.substring(n, numString.lenght);
            }
            if (beforeDot.length > 3) {
                // 000012,345,678 
                // 0.123
                for (var y = 0; beforeDot.length >= 0; y++) {
                    var zero = beforeDot.toString().substring(0, 1);
                    if (zero == "0" && beforeDot.length > 1) {
                        beforeDot = beforeDot.substring(1, beforeDot.lenght);
                    } else {
                        break;
                    }
                }
                var i, from, beforeDotTemp = "", tempNum = "", tempCount = 0;
                for (i = beforeDot.length; i > 0; i--) {
                    from = i - 1;
                    tempCount++;
                    var num = beforeDot.substring(from, i);
                    tempNum = num.concat(tempNum);
                    if (tempCount >= 3 || i <= 0) {
                        if (i > 1) {
                            tempNum = ",".concat(tempNum);
                        }
                        beforeDotTemp = tempNum.concat(beforeDotTemp);
                        tempCount = 0;
                        tempNum = "";
                    }
                }
                if (tempNum != "") {
                    beforeDotTemp = tempNum.concat(beforeDotTemp);
                }
                numString = beforeDotTemp.concat(afterDot);
            }
            return numString;
        } catch (e) {
            return 0;
        }

    }
    var StringFormat = function (stringOrg, placeholdersObject) {
        // how to use:  
        try {
            for (var propertyName in placeholdersObject) {
                var re = new RegExp('{' + propertyName + '}', 'gm');
                stringOrg = stringOrg.replace(re, placeholdersObject[propertyName]);
            }
            return stringOrg;
        } catch (e) {
            return null;
        }
    }
    return {
        InvoiceIndex: InvoiceIndex,
        LoanIndex: LoanIndex,
        StringFormat: StringFormat,
        ButtonSubmit: ButtonSubmit,
        Init: Init,
        SelectDataSource: SelectDataSource,
        DataSource: DataSource,
        GetDataSource: GetDataSource,
        AjaxDone: AjaxDone,
        AjaxSuccess: AjaxSuccess,
        HomePage: HomePage,
        FormatCurrency: FormatCurrency,
        Endpoint: Endpoint,
        GetToken: GetToken,
        ShowErrorNotLoad: ShowErrorNotLoad,
        ShowSuccess: ShowSuccess,
        formatNumber: formatNumber,
        Pagesize: Pagesize,
        // AjaxDoneFail: AjaxDoneFail,
        //AjaxPost: AjaxPost,
        //AjaxGet: AjaxGet,
        //AjaxGetPromise: AjaxGetPromise,
        //AjaxPostFrontEnd: AjaxPostFrontEnd,
        //ServicesAjaxDone: ServicesAjaxDone,
        FormatDate: FormatDate,
        LoginUrl: LoginUrl_,
        FormatRepo: FormatRepo,
        FormatRepoSelection: FormatRepoSelection,
        RepoConvert: RepoConvert,
        Option: Option,
        GetUser: GetUser,
        InitUser: InitUser,
        VerifyInvoice: VerifyInvoice,
        html_money_2: html_money_2,
        html_money: html_money,
        IconWaiting: IconWaiting,
        ChangePassWord: ChangePassWord,
        RefreshToken: RefreshToken
    };
}

$(document).ready(function () {
    baseCommon.Init();
});