﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.Objects
{
    public class TransactionDetailMoneyFineOriginal
    {
        public long TxnID { get; set; }
        public long ShopID { get; set; }
        public string ShopName { get; set; }
        public long LoanID { get; set; }
        public long MoneyAdd { get; set; }
        public long MoneySub { get; set; }
        public DateTime CreateDate { get; set; }
        public string Note { get; set; }
        public int ActionID { get; set; }
    }
}
