﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblLogLoanByDay")]
    public class TblLogLoanByDay
    {
        public DateTime ForDate { get; set; }

        public int LoanID { get; set; }

        public int? ShopID { get; set; }

        public long? TotalMoneyCurrent { get; set; }

        public DateTime? NextDate { get; set; }

        public int? Status { get; set; }

        public DateTime? LastDateOfPay { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public int? LoanTime { get; set; }

        public float? Rate { get; set; }

        public int? RateType { get; set; }

        public int? Frequency { get; set; }

        public long? PaymentMoney { get; set; }

        public long? DebitMoney { get; set; }

        public DateTime? CreateDate { get; set; }

        public int? AgencyId { get; set; }

        public int? ProductId { get; set; }

        public int? CustomerId { get; set; }

        public int? CityId { get; set; }

        public int? DistrictId { get; set; }

        public int? UserIdRemindDebt { get; set; }

        public int? YearBadDebt { get; set; }

        public int? HubID { get; set; }

        public long? InterestToDay { get; set; }

        public float? RateInterest { get; set; }

        public float? RateConsultant { get; set; }

        public float? RateService { get; set; }

        public int? CodeID { get; set; }

        public long? TotalMoney { get; set; }

        public int? TypeCloseLoan { get; set; }

        public DateTime? ModifyDate { get; set; }

    }
}
