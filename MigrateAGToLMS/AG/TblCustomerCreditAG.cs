﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblCustomerCredit")]
    public class TblCustomerCreditAG
    {
        [Key]
        public int CustomerId { get; set; }

        public string FullName { get; set; }

        public DateTime? Birthday { get; set; }

        public string Phone { get; set; }

        public string CardNumber { get; set; }

        public int? CityId { get; set; }

        public int? DistrictId { get; set; }

        public int? WardId { get; set; }

        public short? Gender { get; set; }

        public string Email { get; set; }

        public string Street { get; set; }

        public string NumberLiving { get; set; }

        public string LivingApartmentNumber { get; set; }

        public int? LivingTimeId { get; set; }

        public int? TypeOfOwnershipId { get; set; }

        public int? IsResidential { get; set; }

        public int? JobId { get; set; }

        public int? WorkingIndustryId { get; set; }

        public string CompanyName { get; set; }

        public string CompanyPhone { get; set; }

        public long? Salary { get; set; }

        public int? ReceiveYourIncome { get; set; }

        public int? RelativeFamilyId { get; set; }

        public string FullNameFamily { get; set; }

        public string PhoneFamily { get; set; }

        public int? ManufacturerId { get; set; }

        public string ModelPhone { get; set; }

        public string YearMade { get; set; }

        public decimal? ValuationAsset { get; set; }

        public DateTime? CreateDate { get; set; }

        public string Facebook { get; set; }

        public string AddressCompany { get; set; }

        public int? TypeCustomerCreditId { get; set; }

        public string AddressHouseHold { get; set; }

        public int? NumberRemarketing { get; set; }

        public int? IsMarried { get; set; }

        public int? NumberBaby { get; set; }

        public int? IsLivingTogether { get; set; }

        public int? CustomerUserId { get; set; }

        public string FullNameColleague { get; set; }

        public string PhoneColleague { get; set; }

        public int? DocumentId { get; set; }

        public string DocumentValue { get; set; }

        public string ContactNameCompany { get; set; }

        public string CompanyTaxCode { get; set; }

        public string DescriptionPositionJob { get; set; }

        public int? TypeCompany { get; set; }

        public int? CompanyDistrictId { get; set; }

        public int? CompanyCityId { get; set; }

        public string CarVersion { get; set; }

        public string CarDesign { get; set; }

        public int? CarNumberSeat { get; set; }

        public int? CardOrigin { get; set; }

        public string AddressPersonFamily { get; set; }

        public int? IsOutProvince { get; set; }

        public DateTime? CardNumberRegister { get; set; }

        public string AliaseName { get; set; }

        public string PhoneOther { get; set; }

        public string Zalo { get; set; }

        public string RegistrationCustomerId { get; set; }

        public string SocialInsuranceNumber { get; set; }

        public string SocialInsuranceNote { get; set; }

        public string FullNameFamily1 { get; set; }

        public string PhoneFamily1 { get; set; }

        public int? RelativeFamilyId1 { get; set; }

        public int? OwnerProduct { get; set; }

        public int? MotobikeCertificate { get; set; }

        public int? OwnerPhoneNumber { get; set; }

        public int? OwnerSmartPhone { get; set; }

        public int? OwnerSignCommitment { get; set; }

        public int? CarAppraisalAvaiable { get; set; }

        public string CardNumberOther { get; set; }

        public int? HomeNetwork { get; set; }

        public int? OtoInternalOrExternal { get; set; }

        public string CarName { get; set; }

        public string CarManufacturer { get; set; }

        public int? CarEngine { get; set; }

        public int? CarGearbox { get; set; }

        public int? BikeCertificate { get; set; }

        public int? CarOwner { get; set; }

        public string LicensePlates { get; set; }

        public string AccountNo { get; set; }

        public int? CompanyWardId { get; set; }

        public string LatLongAddress { get; set; }

        public string LatLongAddressCompany { get; set; }

        public int? ResultRefphoneFamily { get; set; }

        public int? ResultRefphoneFamily1 { get; set; }

        public string AddressOfLatLong { get; set; }

        public string AddressCompanyOfLatLong { get; set; }

        public string LatLongStreetCommented { get; set; }

        public string AddressGoogleMap { get; set; }

        public string AddressCompanyGoogleMap { get; set; }

        public string SecondAddress { get; set; }

        public string LatLongSecondAddress { get; set; }

        public string LISTPHONE { get; set; }

        public string Passport { get; set; }

        public int? IsLivingInProvince { get; set; }


    }
}
