﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblBankCard")]
    public class TblBankCardAG
    {
        [Key]
        public int Id { get; set; }

        public int BankId { get; set; }

        public int ShopId { get; set; }

        public bool IsHub { get; set; }

        public string ShopName { get; set; }

        public string NumberAccount { get; set; }

        public string BankCode { get; set; }

        public int Status { get; set; }

        public DateTime CreateOn { get; set; }

        public DateTime? ModifyOn { get; set; }

        public long TotalMoney { get; set; }

        public long TotalMoneyCurrent { get; set; }

        public string AccountHolderName { get; set; }

        public string BranchName { get; set; }

        public int TypePurpose { get; set; }

        public string AliasName { get; set; }

        public int ParentId { get; set; }

        public int ApplyAutoDisbursement { get; set; }
    }
}
