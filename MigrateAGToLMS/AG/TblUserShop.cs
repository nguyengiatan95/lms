﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblUserShop")]
    public class TblUserShop
    {
        public int UserID { get; set; }

        public int ShopID { get; set; }

    }
}
