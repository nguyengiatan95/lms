﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblTransactionLoan")]
    public class TblTransactionLoanAG
    {
        [Key]
        public int ID { get; set; }

        public int ShopID { get; set; }

        public int LoanID { get; set; }

        public int ActionID { get; set; }

        public int UserID { get; set; }

        public string FullName { get; set; }

        public int CusID { get; set; }

        public string CusName { get; set; }

        public long MoneyAdd { get; set; }

        public long MoneySub { get; set; }

        public string Note { get; set; }

        public DateTime CreateDate { get; set; }

        public int CodeID { get; set; }

        public string ItemName { get; set; }

        public long MoneyPawn { get; set; }

        public long MoneyInterest { get; set; }

        public long OtherMoney { get; set; }

        public long OverMoney { get; set; }

        public bool Status { get; set; }

        public string BankCode { get; set; }

        public int TransactionLoanID { get; set; }

        public long PaymentId { get; set; }

        public int BankID { get; set; }

        public long PayMoney { get; set; }

        public long PayNeed { get; set; }

        public long PayMoneyDebit { get; set; }

        public long MoneyDebit { get; set; }

        public long PayMoneyExcess { get; set; }

        public long MoneyExcess { get; set; }

        public long MoneyFine { get; set; }

        public long MoneyTransaction { get; set; }

        public float Rate { get; set; }

        public float RateAgency { get; set; }

        public float RateInterest { get; set; }

        public float RateConsultant { get; set; }

        public float RateService { get; set; }

        public int LoanExtraId { get; set; }

        public bool Fined { get; set; }

        public long MoneyFined { get; set; }

        public long MoneyFeeDisbursement { get; set; }

        public long MoneyFeeInsuranceOfCustomer { get; set; }

        public int UserIdRemindDebt { get; set; }

        public long DebitMoneyFine { get; set; }

        public int UserIdModify { get; set; }

        public string UserNameModify { get; set; }

        public string FullNameModify { get; set; }

        public DateTime? ModifyOn { get; set; }

        public long MoneyServicesTima { get; set; }

        public long MoneyOfAgency { get; set; }

        public int NumberDayDelay { get; set; }

        public decimal RateLender { get; set; }

        public decimal RateAffLender { get; set; }

        public int AgencyId { get; set; }
    }
}
