﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MigrateAGToLMS.AG
{

    public enum AG_TransactionBankCard_Type
    {
        [Description("Nạp tiền")]
        ChargingMoney = 1,

        [Description("Thu hộ")]
        Collection = 2,

        [Description("Treo")]
        Suspend = 3,

        [Description("Giải ngân")]
        Disbursement = 4,

        [Description("Hủy thu hộ")]
        CancelCollection = 5,

        [Description("Chuyển tiền")]
        TransfersMoney = 6,

        [Description("Nhận tiền chuyển")]
        ReceiveMoney = 7,

        [Description("Chi tiền")]
        PayMoney = 8,

        [Description("Thu tiền")]
        CollectMoney = 9,

        [Description("Trả lại tiền thừa")]
        GiveMoneyBack = 10,

        [Description("Thu tiền tư vấn")]
        MoneyAdvice = 11,

        [Description("Vay thêm gốc")]
        AdditionalLoan = 12,

        [Description("DVCV rut tien")]
        CashWithdrawalAgent = 13,

        [Description("Chuyển tiền thẻ nội bộ")]
        TransferBankCardInternal = 14,

        [Description("Phí chuyển tiền")]
        FeeTransferMoney = 15,

        [Description("Phí ngân hàng")]
        BankFee = 16,

        [Description("Lãi gửi ngân hàng")]
        BankInterest = 17,

        [Description("Trả công nợ")]
        PayTheDebt = 18,

        [Description("Đại lý chuyển quỹ")]
        AgentTransferFund = 19,

        [Description("Thanh toán cho lender")]
        PaidLenderByPeriod = 20,

        [Description("Lender thanh toán tiền bảo hiểm")]
        LenderPaidInsurance = 21,
        [Description("Đặt cọc cho Lender")]
        LenderDepositPayment = 22,
        
    }

    public enum AG_TransactionLoan_ActionID
    {
        [Description("Cho vay")]
        ChoVay = 1,
        [Description("Sửa HĐ")]
        SuaHd = 2,
        [Description("Đóng lãi")]
        DongLai = 3,
        [Description("Hủy đóng lãi")]
        HuyDongLai = 4,
        [Description("Trả gốc")]
        TraGoc = 5,
        [Description("Vay thêm gốc")]
        VayThemGoc = 6,
        [Description("Hủy trả gốc")]
        HuyTraGoc = 7,
        [Description("Hủy vay thêm gốc")]
        HuyVayThemGoc = 8,
        [Description("Đóng HĐ")]
        DongHd = 9,
        [Description("Nợ lãi")]
        NoLai = 10,
        [Description("Trả nợ")]
        TraNo = 11,
        [Description("Gia hạn")]
        GiaHan = 12,
        [Description("Hủy đóng HĐ")]
        HuyDongHd = 13,
        [Description("Thanh lý")]
        ThanhLy = 14,
        [Description("Hủy cho vay")]
        HuyChoVay = 15,
        [Description("Hủy Nợ Lãi")]
        HuyNoLai = 16,
        [Description("Hủy Trả Nợ")]
        HuyTraNo = 17,
        [Description("Hủy Thanh Lý")]
        HuyThanhLy = 18,
        [Description("Chuyển Nợ Xấu")]
        ChuyenNoXau = 19,
        [Description("Bỏ Nợ Xấu")]
        BoNoXau = 20,
        [Description("Tiền Tư Vấn Thu Trước")]
        TienTuVanThuTruoc = 21,
        [Description("Phí phạt trả trước gốc")]
        PhiPhatTraTruocGoc = 22,
        [Description("Hủy Phí phạt trả trước gốc")]
        HuyPhiPhatTraTruocGoc = 23,
        [Description("Phí bảo hiểm")]
        InsuranceMoneyFees = 24,
        [Description("Ghi nợ phí phạt muộn")]
        DebitMoneyFeeFineLate = 25,
        [Description("Trả nợ phí phạt muộn")]
        PayDebitMoneyFeeFineLate = 26,

        [Description("Hủy nợ phí phạt muộn")]
        DeleteTransactionMoneyFeeLate = 27,

        [Description("Thanh lý bảo hiểm")]
        LiquidationLoanInsurance = 28,

        [Description("Hủy trả nợ phí phạt muộn")]
        DeleteTransactionPayMoneyFeeLate = 29,

        [Description("Miễn giảm lãi phí")]
        Exemption = 30,

        [Description("Trả tiền lãi")]
        PayPartial_Interest = 31,

        [Description("Trả phí tư vấn")]
        PayPartial_Consultant = 32,

        [Description("Trả phí dịch vụ")]
        PayPartial_Service = 33,

        [Description("Hủy Trả Tiền Lãi")]
        DeletePayPartial_Interest = 34,

        [Description("Hủy phí tư vấn")]
        DeletePayPartial_Consultant = 35
    }

    public enum Status_TransactionBankCard
    {
        [Description("Chưa hoàn thành")]
        NotComplete = 0,
        [Description("Hoàn thành")]
        Complete = 1,
        [Description("Đã xóa")]
        Deleted = -1
    }


        public enum Base_Status
    {
        [Description("Chưa hoàn thành")]
        NotComplete = 0,
        [Description("Hoàn thành")]
        Complete = 1,
        [Description("Đã xóa")]
        Deleted = -1
    }

    public enum Type_CloseLoan
    {
        [Description("Tất toán")]
        Finalization = 0,

        [Description("Thanh Lý")]
        Liquidation = 1,

        [Description("Thanh Lý bảo hiểm")]
        LiquidationLoanInsurance = 2,

        [Description("Bồi thường bảo hiểm")]
        IndemnifyLoanInsurance = 3,
        [Description("Đã thu đủ tiền sau khi bồi thường BH")]
        PaidInsurance = 4,
        [Description("Xin miễn giảm")]
        Exemption = 5,
        [Description("Xin miễn giảm bồi thường bảo hiểm")]
        PaidInsuranceExemption = 6
    }

    public enum Status_Loan
    {
        [Description("Đang vay")]
        Lending = 11,
        [Description("Đến ngày đóng họ ")]
        ToDayPay = 12,
        [Description("Chậm Lãi")]
        DelayInterest = 13,
        [Description("Ngày cuối họ")] // Tra goc
        Delayed = 14,
        [Description("Quá hạn")]
        Delayed2 = 15,
        [Description("Nợ xấu")]
        WaitLiquidation = 100,
        //[Description("Đã Thanh Lý")]
        //Liquidated = -1,
        [Description("Đóng hợp đồng")]
        Acquision = 1,
        [Description("Đã Xóa")]
        Deleted = 0
    }

    public enum TransactionIndemnifyLoanInsurance_ActionID
    {
        [Description("Trả gốc")]
        PayOriginal = 1,
        [Description("Trả lãi")]
        PayInterest = 2,
        [Description("Phí tư vấn")]
        PayConsultant = 3,
        [Description("Phí dịch vụ")]
        PayService = 4,
        [Description("Phí tất toán trước hạn")]
        PayFineOriginalSoon = 5,
        [Description("Nợ cũ")]
        PayDebit = 6,
        [Description("Phí phạt chậm trả")]
        PayFineLate = 7
    }

    public enum StateGroupAccountMeCash
    {
        [Description("Quản trị hệ thống")]
        Admin = 1,


        [Description("TeleSales")]
        Support = 9, // Hỗ trợ Mecash

        [Description("Direct Sales")]
        Counselor = 10,// Tư Vân Viên

        [Description("Kế Toán")]
        Accountant = 11, // kế toán

        [Description("Thẩm Định Hồ Sơ")]
        Coordinator = 12, // Người điều phối cho tư vấn viên

        [Description("Quản Lý DRS")]
        SupervisionArea = 13, // Quản lý khu vực

        SupervisionRegion = 14, // Quản lý vùng

        SuperAdmin = 3, // Supper Admin

        Agency = 99,// Đại lý
        CTV = 100,

        [Description("Thẩm định thực địa")]
        FieldSurvey = 17,// Thẩm định thực địa

        [Description("Ký hợp đồng")]
        AwardContract = 18, // Ký hợp đồng

        [Description("Quản Lý Ký hợp đồng")]
        FieldSaleManager = 19, // Quản lý ký hợp đồng 

        [Description("Quản Lý Thẩm Định Thực Địa")]
        FieldSurveyManager = 20, // Quản lý Thẩm định thực địa 

        [Description("Call")]
        CallReminders = 21, // Call nhắc nợ

        [Description("Field MB")]
        GroundReminders = 22, // Thực Địa Nợ

        [Description("Thẩm định viên của HUB")]
        FieldSurveyOfHub = 23,// Thẩm định viên của HUB

        [Description("Đối tác xử lý nợ xấu")]
        PartnerDealingBadDebts = 24, // Đối tác xử lý nợ xấu

        [Description("Legal")]
        ThnPhapLy = 25, // Lender Care

        [Description("Quản lý thu hồi nợ")]
        ManagerDebtRecovery = 26, // Lender Care

        [Description("Chuyên viên kinh doanh Nguồn Vốn")]
        LenderCare = 27, // Lender Care

        [Description("Trưởng cửa hàng")]
        HeadOfHub = 28, // Lender Care

        [Description("Skip Call")]
        SkipCall = 29,

        [Description("Quản lý TĐHS")]
        LeaderCoordinator = 30,

        [Description("Call Vay1h")]
        CallVay1h = 31,

        [Description("Dịch vụ khách hàng")]
        CustomerService = 32,

        [Description("Sales Admin SPL")]
        SalesAdminSPL = 33,

        [Description("Thu Hồi Nợ TTKD")]
        ThnTTKD = 34,

        [Description("Nhóm hỗ trợ THN")]
        NHTTHN = 35,

        [Description("P.Marketing")]
        Marketing = 36,

        [Description("Field MN")]
        Field_MN = 37,

        [Description("Hub")]
        HUB = 4,

        [Description("Chuyên viên chăm sóc Lender")]
        LenderTakeCare = 38,
    }

}
