﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblLogChangePhone")]
    public class TblLogChangePhoneAG
    {
        [Key]
        public int Id { get; set; }

        public int? CustomerCreditId { get; set; }

        public int? CustomerId { get; set; }

        public int? LoanCreditId { get; set; }

        public int? LoanId { get; set; }

        public string PhoneNew { get; set; }

        public string PhoneOld { get; set; }

        public DateTime? CreateOn { get; set; }

        public int UserId { get; set; }

        public string UserFullName { get; set; }

        public string Note { get; set; }

        public long? LogChangePhoneLMSID { get; set; }

    }
}
