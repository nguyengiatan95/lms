﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{

    [Table("TblUser")]
    public class TblUserAG
    {
        [Key]
        public int Id { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public DateTime CreateDate { get; set; }

        public int Status { get; set; }

        public int GroupId { get; set; }

        public string FullName { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public DateTime? ExpiredDate { get; set; }

        public int ParentId { get; set; }

        public short MaxShop { get; set; }

        public int? WorkingTime { get; set; }

        public string Token { get; set; }

        public DateTime? ExprireToken { get; set; }

        public DateTime? DayOffWork { get; set; }

        public string PushTokenAndroid { get; set; }

        public string PushTokenIOS { get; set; }

        public bool? CoordinatorIsEnable { get; set; }

        public int? CoordinateRecord { get; set; }

        public string BankName { get; set; }

        public string BankNumber { get; set; }

        public string BankOwner { get; set; }

        public string BankBranch { get; set; }

        public string PersonalCard { get; set; }

        public int? AccessPrivate { get; set; }

        public string AccessIpAddress { get; set; }

        public string IPPhone { get; set; }

        public int? IdManageTHN { get; set; }

        public int? ManageTHN { get; set; }

        public string CiscoUsername { get; set; }

        public string CiscoPassword { get; set; }

        public string CiscoExtension { get; set; }

        public DateTime? PasswordExpired { get; set; }

        public int? ServiceCall { get; set; }

        public int? CityId { get; set; }

        public string CityName { get; set; }

        public DateTime? UserReceivedDate { get; set; }

        public long? SSOUserID { get; set; }
    }

    [Dapper.Contrib.Extensions.Table("tblUser")]
    public class tblUserAgV2
    {
        [Dapper.Contrib.Extensions.Key]
        public int Id { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public DateTime CreateDate { get; set; }

        public int Status { get; set; }

        public int GroupId { get; set; }

        public string FullName { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public DateTime? ExpiredDate { get; set; }

        public int ParentId { get; set; }

        public short MaxShop { get; set; }

        public int? WorkingTime { get; set; }

        public string Token { get; set; }

        public DateTime? ExprireToken { get; set; }

        public DateTime? DayOffWork { get; set; }

        public string PushTokenAndroid { get; set; }

        public string PushTokenIOS { get; set; }

        public bool? CoordinatorIsEnable { get; set; }

        public int? CoordinateRecord { get; set; }

        public string BankName { get; set; }

        public string BankNumber { get; set; }

        public string BankOwner { get; set; }

        public string BankBranch { get; set; }

        public string PersonalCard { get; set; }

        public int? AccessPrivate { get; set; }

        public string AccessIpAddress { get; set; }

        public string IPPhone { get; set; }

        public int? IdManageTHN { get; set; }

        public int? ManageTHN { get; set; }

        public string CiscoUsername { get; set; }

        public string CiscoPassword { get; set; }

        public string CiscoExtension { get; set; }

        public int? ServiceCall { get; set; }

        public int? CityId { get; set; }

        public string CityName { get; set; }

        public DateTime? UserReceivedDate { get; set; }
    }
}
