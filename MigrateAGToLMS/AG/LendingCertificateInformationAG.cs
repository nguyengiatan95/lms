﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblLendingCertificateInformation")]
    public class LendingCertificateInformationAG
    {
        [Key]
        public int ID { get; set; }

        public int? LoanID { get; set; }

        public DateTime? CreateDate { get; set; }

        public string NumberInsurance { get; set; }

        public string CustomerAddress { get; set; }

        public string CustomerContact_number { get; set; }

        public string CustomerDate_of_birth { get; set; }

        public string CustomerEmail { get; set; }

        public string CustomerFullname { get; set; }

        public string CustomerId_card { get; set; }

        public string CustomerPhone { get; set; }

        public string From_date { get; set; }

        public string From_hour { get; set; }

        public string From_month { get; set; }

        public string From_year { get; set; }

        public string To_date { get; set; }

        public string To_hour { get; set; }

        public string To_month { get; set; }

        public string To_year { get; set; }

        public string InvestorAddress { get; set; }

        public string InvestorContact_number { get; set; }

        public string InvestorDate_of_birth { get; set; }

        public string InvestorEmail { get; set; }

        public string InvestorFullname { get; set; }

        public string InvestorId_card { get; set; }

        public string InvestorInvest_money { get; set; }

        public string InvestorPhone { get; set; }

        public string InvestorProduct { get; set; }

        public string InvestorRate { get; set; }

        public string InvestorTax_code { get; set; }

        public string Money_Indemnification { get; set; }
    }
}
