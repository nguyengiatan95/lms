﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblLogSendStatusInsurance")]
    public class LogSendStatusInsuranceAG
    {
        [Key]
        public int ID { get; set; }

        public int? LoanID { get; set; }

        public int? UserID { get; set; }

        public string Note { get; set; }

        public short? StatusNew { get; set; }

        public short? StatusOld { get; set; }

        public DateTime? CreateDate { get; set; }
    }
}
