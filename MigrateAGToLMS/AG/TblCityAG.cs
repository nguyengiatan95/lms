﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblCity")]
    public class TblCityAG
    {
        [Key]
        public int ID { get; set; }

        public string Name { get; set; }

        public string TypeCity { get; set; }

        public int? Position { get; set; }

        public int? IsApply { get; set; }

        public int? IsCity { get; set; }

        public int? DomainID { get; set; }

        public int? RegionID { get; set; }

        public int? ProvinceCode { get; set; }

    }
}
