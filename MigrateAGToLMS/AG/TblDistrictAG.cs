﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblDistrict")]
    public class TblDistrictAG
    {
        [Key]
        public int ID { get; set; }

        public string Name { get; set; }

        public string TypeDistrict { get; set; }

        public string LongitudeLatitude { get; set; }

        public int? CityId { get; set; }

        public int? AreaID { get; set; }

        public int? IsApply { get; set; }

        public int? DistrictCode { get; set; }

    }
}
