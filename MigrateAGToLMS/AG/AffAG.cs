﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblAff")]
    public class AffAG
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }

        public string NamePlainText { get; set; }

        public string Code { get; set; }

        public string Email { get; set; }

        public string NumberCard { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime? BirthDay { get; set; }

        public short? Source { get; set; }

        public string BankNumber { get; set; }

        public long? BankID { get; set; }

        public string BankPlace { get; set; }

        public string BankCode { get; set; }

        public DateTime? ContractDate { get; set; }

        public string CodeNumber { get; set; }

        public DateTime? FinishContractDate { get; set; }

        public double? PercentComission { get; set; }

        public int? DayComission { get; set; }

        public long? CommitDisbursement { get; set; }

        public int? Status { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string Password { get; set; }

        public long? UserSaleID { get; set; }

        public long? UserTakeCareID { get; set; }

        public decimal? RateLender { get; set; }

        public long? ParentID { get; set; }

    }
}
