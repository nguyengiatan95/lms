﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblTransactionIndemnifyLoanInsurrance")]
    public class TblTransactionIndemnifyLoanInsurrance
    {
        [Key]
        public long TransactionIndemnifyLoanInsurranceID { get; set; }

        public int LoanID { get; set; }

        public int CustomerID { get; set; }

        public string CustomerName { get; set; }

        public DateTime CreateDate { get; set; }

        public int ActionID { get; set; }

        public string Note { get; set; }

        public long TotalMoney { get; set; }

        public long PaymentScheduleID { get; set; }

        public int UserID { get; set; }

        public string FullNameOfUser { get; set; }

    }
}
