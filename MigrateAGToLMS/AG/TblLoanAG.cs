﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblLoan")]
    public class TblLoanAG
    {
        [Key]
        public int ID { get; set; }

        public int UserID { get; set; }

        public int ShopID { get; set; }

        public int CustomerID { get; set; }

        public long TotalMoney { get; set; }

        public long TotalMoneyCurrent { get; set; }

        public string Collateral { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public int? LoanTime { get; set; }

        public DateTime? LastDateOfPay { get; set; }

        public float? Rate { get; set; }

        public int? RateType { get; set; }

        public short? Frequency { get; set; }

        public int? IsBefore { get; set; }

        public float? PaymentMoney { get; set; }

        public short? Status { get; set; }

        public DateTime? NextDate { get; set; }

        public int? ApproveBy { get; set; }

        public string Note { get; set; }

        public long? TotalInterest { get; set; }

        public int? DebitMoney { get; set; }

        public int? CodeID { get; set; }

        public DateTime? ModifyDate { get; set; }

        public long? InterestToDay { get; set; }

        public DateTime? FinishDate { get; set; }

        public int? ProductID { get; set; }

        public short? BriefID { get; set; }

        public int TypeCloseLoan { get; set; }

        public short? KeepOriginal { get; set; }

        public long? OverMoney { get; set; }

        public int? AgencyId { get; set; }

        public float? RateAgency { get; set; }

        public float? RateInterest { get; set; }

        public float? RateConsultant { get; set; }

        public float? RateService { get; set; }

        public int? CityID { get; set; }

        public int? DistrictID { get; set; }

        public DateTime? BadDebtTransferDate { get; set; }

        public long? TotalMoneyFine { get; set; }

        public long? TotalMoneyFineCurrent { get; set; }

        public long? FineDate { get; set; }

        public int? UserIdRemindDebt { get; set; }

        public int? SynchronizationVBI { get; set; }

        public string LinkGCNChoVay { get; set; }

        public string LinkGCNVay { get; set; }

        public long? MoneyFeeInsuranceOfCustomer { get; set; }

        public string RegistrationCustomerId { get; set; }

        public string RegistrationContractId { get; set; }

        public int? PartnerIdProcessBadDebt { get; set; }

        public long? HubID { get; set; }

        public int? CustomerInsuranceFeeState { get; set; }

        public DateTime? AlarmDate { get; set; }

        public string AlarmNote { get; set; }

        public double? Score { get; set; }

        public int? LastUserID { get; set; }

        public int? MarkLoan { get; set; }

        public long? DebitMoneyFineLate { get; set; }

        public int? StatusSendInsurance { get; set; }

        public short? ValidDocuments { get; set; }

        public int? SourceBankDisbursement { get; set; }

        public int? YearBadDebt { get; set; }

        public int? GetInformationInsuranVIB { get; set; }

        public decimal? RateLender { get; set; }

        public decimal? RateAffLender { get; set; }

        public int? ChannelSale { get; set; }

        public bool? LIsLocate { get; set; }

        public string LDeviceID { get; set; }

        public int? CampaignDataId { get; set; }

        public string StatusCampaign { get; set; }

        public int? CallDuration { get; set; }

        public DateTime? StartTimeCall { get; set; }

        public DateTime? EndTimeCall { get; set; }

        public DateTime? ConnectTimeCall { get; set; }

        public int? InsuranceCompensatorID { get; set; }

        public short? StatusLiquidation { get; set; }

        public string UsernameReceive { get; set; }

        public long? IdPush { get; set; }

        public string UsernamePush { get; set; }

        public int? TopUp { get; set; }

        public DateTime? OldLoanPaymentDate { get; set; }

        public long? OldDebitMoneyFineLate { get; set; }

        public string PhoneCustomer { get; set; }

        public string NationalCard { get; set; }

        public DateTime? WorkingTime { get; set; }
        public long MoneyFeeInsuranceMaterialCovered { get; set; }
        public decimal? FeePaymentBeforeLoan { get; set; }
    }
}
