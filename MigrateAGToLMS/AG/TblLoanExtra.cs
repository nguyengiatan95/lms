﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Dapper.Contrib.Extensions.Table("TblLoanExtra")]
    public class TblLoanExtra
    {
        [Dapper.Contrib.Extensions.Key]
        public int ID { get; set; }

        public int UserID { get; set; }

        public int ShopID { get; set; }

        public int LoanID { get; set; }

        public long TotalMoney { get; set; }

        public DateTime DateExtra { get; set; }

        public DateTime? CreateDate { get; set; }

        public string Note { get; set; }

        public string CodeBank { get; set; }

        public int? LoanExtraID { get; set; }

        public int? PaymentId { get; set; }

        public int? BankId { get; set; }

        public bool? Fined { get; set; }

        public long? MoneyFined { get; set; }

    }
}
