﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("tblProvisionsCollection")]
    public class TblProvisionsCollection
    {
        [Key]
        public int Id { get; set; }

        public int? LoanID { get; set; }

        public string UserName { get; set; }

        public int? UserId { get; set; }

        public DateTime? CreateDate { get; set; }

        public long? Money { get; set; }

        public int Status { get; set; }
        public DateTime? TimeScheduled { get; set; }

        public int NumberAccruedPeriods { get; set; }

        public string TextAccruedPeriods { get; set; }
        public long LMSID { get; set; }
    }
}
