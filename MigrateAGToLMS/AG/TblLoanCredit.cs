﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Dapper.Contrib.Extensions.Table("TblLoanCredit")]
    public class TblLoanCredit
    {
        [Dapper.Contrib.Extensions.Key]
        public int ID { get; set; }

        public int? CustomerCreditId { get; set; }

        public long? TotalMoney { get; set; }

        public int? LoanTime { get; set; }

        public decimal? RateConsultant { get; set; }

        public decimal? RateService { get; set; }

        public decimal? Rate { get; set; }

        public long? TotalReturn { get; set; }

        public short? Status { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? ModifyDate { get; set; }

        public DateTime? ApproveDate { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public int? OwerShopId { get; set; }

        public int? TypeReceivingMoney { get; set; }

        public string NameBanking { get; set; }

        public string NameCardHolder { get; set; }

        public string NumberCardBanking { get; set; }

        public string AccountNumberBanking { get; set; }

        public string Note { get; set; }

        public int? LoanID { get; set; }

        public int? Code { get; set; }

        public int? AgentLoanID { get; set; }

        public int? ApproveId { get; set; }

        public DateTime? NextDate { get; set; }

        public string ListAgency { get; set; }

        public int? TypeID { get; set; }

        public string utm_source { get; set; }

        public string utm_medium { get; set; }

        public string utm_campaign { get; set; }

        public int? SupportID { get; set; }

        public int? SupportLastID { get; set; }

        public int? CounselorID { get; set; }

        public long? TotalMoneyFirst { get; set; }

        public int? ReasonID { get; set; }

        public int? Step { get; set; }

        public DateTime? CounselorDate { get; set; }

        public int? UserIDCancel { get; set; }

        public int? CoordinatorID { get; set; }

        public bool? IsRead { get; set; }

        public int? CountCall { get; set; }

        public string Document { get; set; }

        public int? IsOwner { get; set; }

        public DateTime? BeginStartTime { get; set; }

        public int? FieldSurveyUserID { get; set; }

        public int? AwardContractID { get; set; }

        public bool? IsVerify { get; set; }

        public short? OldStatus { get; set; }

        public bool? WarningSms { get; set; }

        public int? Frequency { get; set; }

        public int? ConfirmationProfile { get; set; }

        public int? RateType { get; set; }

        public int? CompanyFieldSurveyUserID { get; set; }

        public int? CompanyFieldSurveyStatus { get; set; }

        public int? AddressFieldSurveyStatus { get; set; }

        public DateTime? LastDateSendSms { get; set; }

        public int? NumberPassDataToAi { get; set; }

        public int? PlatformType { get; set; }

        public string LinkAuthorization { get; set; }

        public int? TypeTimerId { get; set; }

        public double? Score { get; set; }

        public int? ChangedAi { get; set; }

        public string GiftCode { get; set; }

        public int? CityId { get; set; }

        public int? DistrictId { get; set; }

        public int? WardId { get; set; }

        public string utm_term { get; set; }

        public string utm_content { get; set; }

        public string DomainName { get; set; }

        public int? CountSMS { get; set; }

        public int? LoanMarkId { get; set; }

        public int? OriginalLoanCreditId { get; set; }

        public string IdOfPartenter { get; set; }

        public long? TotalMoneyExpertise { get; set; }

        public int? PriorityContract { get; set; }

        public long? TotalMoneyExpertiseLast { get; set; }

        public bool? SynchronizationDataPartenter { get; set; }

        public int? LoanAgain { get; set; }

        public string CreateByUserName { get; set; }

        public bool? ReMarketing { get; set; }

        public bool? AgencyBuyInsurance { get; set; }

        public bool? CustomerBuyInsurance { get; set; }

        public string LinkGCNChoVay { get; set; }

        public string LinkGCNVay { get; set; }

        public int? ReMarketingLoanCreditId { get; set; }

        public int? HubEmployeId { get; set; }

        public int? BankId { get; set; }

        public int? BankValue { get; set; }

        public int? PaymentState { get; set; }

        public int? PaymentShopId { get; set; }

        public DateTime? PaymentLeaseTime { get; set; }

        public bool? DisbursementFromApp { get; set; }

        public bool? IsCheckCIC { get; set; }

        public int? ApproveStatus { get; set; }

        public int? LoanPurpose { get; set; }

        public int? OrderSourceId { get; set; }

        public int? OrderSourceParentId { get; set; }

        public DateTime? CancelDateLoanCredit { get; set; }

        public DateTime? TimeSendOTP { get; set; }

        public string RequestIdOTP { get; set; }

        public string TS_Otp { get; set; }

        public int? NumberCheckCic { get; set; }

        public DateTime? LastUpdateOfVay1h { get; set; }

        public bool? IsUpdateVay1H { get; set; }

        public DateTime? LastCall { get; set; }

        public long? TsScore { get; set; }

        public string OtherPartnerLoanInfo { get; set; }

        public DateTime? CompanyFieldSurveyTime { get; set; }

        public DateTime? AddressFieldSurveyTime { get; set; }

        public decimal? DTI { get; set; }

        public int? NumberLoanOther { get; set; }

        public decimal? LoanProposed { get; set; }

        public decimal? MinLoanProposed { get; set; }

        public long? FraudScore { get; set; }

        public bool? IsTakenRegistrationCertificate { get; set; }

        public int? source_product { get; set; }

        public int? SigningContract { get; set; }

        public DateTime? CompanyFieldSurveyMeetingTime { get; set; }

        public DateTime? AddressFieldSurveyMeetingTime { get; set; }

        public int? HouseholdSameLiving { get; set; }

        public int? HoldMotobikeCertificate { get; set; }

        public int? TypeProofOfWork { get; set; }

        public int? PushSan { get; set; }

        public int? IsSan { get; set; }

        public DateTime? DateTimePushSan { get; set; }

        public string aff_source { get; set; }

        public string aff_pub { get; set; }

        public bool? IsHeadOffice { get; set; }

        public int? ReBorrow { get; set; }

        public decimal? TotalMoneyOld { get; set; }

        public bool? IsLocate { get; set; }

        public string DeviceID { get; set; }

        public int? StatusOfDeviceID { get; set; }

        public int? TypeInsurance { get; set; }

        public int? IsSendSms { get; set; }

        public decimal? LeadScore { get; set; }

        public bool? IsGetLeadScoring { get; set; }

        public int? IdSan { get; set; }

        public bool? PushToSmartDailer { get; set; }

        public DateTime? TimeSendActiveDevice { get; set; }

        public bool? IsTrackingLocation { get; set; }

        public string RefCodeLocation { get; set; }

        public int? RefCodeStatus { get; set; }

        public int? LeaderID { get; set; }

        public int? BodID { get; set; }

        public int? CreatedBy { get; set; }

        public string LabelScore { get; set; }

        public decimal? RateLender { get; set; }

        public decimal? RateAffLender { get; set; }

        public string LabelVerifyLocation { get; set; }

        public string LabelLocation { get; set; }

        public int? CompanyWardId { get; set; }

        public int? CallBackLoction { get; set; }

        public string Aff_Sid { get; set; }

        public string AffCode { get; set; }

        public string TId { get; set; }

        public short? IsVerifyOtpLDP { get; set; }

        public string ListCheckLeadQualify { get; set; }

        public DateTime? FirstModifyDate { get; set; }

        public DateTime? FirstTimeHubReceived { get; set; }

        public DateTime? FirstTimeHubFeedBack { get; set; }

        public string ResultLocation { get; set; }

        public string ResultCAC { get; set; }

    }
}
