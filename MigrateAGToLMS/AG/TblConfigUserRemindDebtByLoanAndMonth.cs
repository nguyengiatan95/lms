﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Dapper.Contrib.Extensions.Table("TblConfigUserRemindDebtByLoanAndMonth")]
    public class TblConfigUserRemindDebtByLoanAndMonth
    {
        [Dapper.Contrib.Extensions.Key]
        public int Id { get; set; }

        public int LoanId { get; set; }

        public string MonthAndYear { get; set; }

        public DateTime ForDate { get; set; }

        public string CustomerName { get; set; }

        public DateTime NextDate { get; set; }

        public string CityName { get; set; }

        public int? CityID { get; set; }

        public int? DistrictID { get; set; }

        public string DistrictName { get; set; }

        public int? ProductID { get; set; }

        public string ProductName { get; set; }

        public long? TotalMoneyCurrent { get; set; }

        public int? LastUserID { get; set; }

        public string LastUserName { get; set; }

        public int? CurrentUserID { get; set; }

        public string CurrentUserName { get; set; }

        public int? FreeUserID { get; set; }

        public string FreeUserName { get; set; }

        public short? IsResolve { get; set; }

        public int CreateUserID { get; set; }

        public DateTime CreateDate { get; set; }

        public int? IsWorking { get; set; }

        public int? CodeID { get; set; }

        public int? ProcessUserID { get; set; }

        public long? MoneyNeedCollect { get; set; }

        public long? TotalMoneyReceived { get; set; }

        public int? CountDay { get; set; }

        public DateTime? FirstUpdate { get; set; }

        public DateTime? FirstDate { get; set; }

        public short? IsGoodContract { get; set; }

        public int? TeamleadUserID { get; set; }

        public string TeamleadUserName { get; set; }

        public int? SectionManagerUserID { get; set; }

        public string SectionManagerUsername { get; set; }

        public int? DepartmentManagerUserID { get; set; }

        public string DepartmentManagerUsername { get; set; }

        public int? DpdBom { get; set; }

        public int? SecondStaffID { get; set; }

    }
}
