﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblHoldCustomerMoney")]
    public class TblHoldCustomerMoneyAG
    {
        [Key]
        public long HoldCustomerMoneyID { get; set; }

        public long CustomerID { get; set; }

        public long Amount { get; set; }

        public int Status { get; set; }

        public string Reason { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime? ModifyDate { get; set; }

        public long CreateBy { get; set; }
        public long ModifyBy { get; set; }
    }
}
