﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblSmsAnalytics")]
    public class TblSmsAnalyticsAG
    {
        [Key]
        public int Id { get; set; }

        public DateTime? CreateDate { get; set; }

        public string Sender { get; set; }

        public string SmsContent { get; set; }

        public DateTime? IncreaseTime { get; set; }

        public long? IncreaseMoney { get; set; }

        public string CustomerName { get; set; }

        public string IdCard { get; set; }

        public string Phone { get; set; }

        public DateTime? SmsreceivedDate { get; set; }

        public int? Status { get; set; }

        public string AcountNumber { get; set; }

        public long? CurrentTotalMoney { get; set; }

        public int? LoanId { get; set; }

        public int? LoanCodeId { get; set; }

        public int? CustomerId { get; set; }

        public int? BankCardId { get; set; }

        public string BankCardAliasName { get; set; }

        public int? TypeTransactionBank { get; set; }

        public string NameTransactionBank { get; set; }

        public short? TransactionBankCardStatus { get; set; }

        public int? IsAnalytics { get; set; }

        public int? ShopID { get; set; }

        public DateTime? ModifyDate { get; set; }

        public string ShopName { get; set; }

        public int SmsType { get; set; }

        public string ContentCancel { get; set; }

        public int? TransactionBankCardId { get; set; }

        public int? ReasonIdCancel { get; set; }

        public string SmsContentHashIndex { get; set; }

        public short? CuttingOffDebtType { get; set; }

    }

}
