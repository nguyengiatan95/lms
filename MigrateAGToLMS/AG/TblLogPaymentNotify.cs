﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblLogPaymentNotify")]
    public class TblLogPaymentNotify
    {
        [Key]
        public int ID { get; set; }

        public int TypeID { get; set; }

        public int ShopID { get; set; }

        public DateTime ForDate { get; set; }

        public long TotalMoney { get; set; }

        public long InterestMoney { get; set; }

        public int? DebitMoney { get; set; }

        public int CusID { get; set; }

        public string CusName { get; set; }

        public string CusPhone { get; set; }

        public string CusAddress { get; set; }

        public string Note { get; set; }

        public int? Status { get; set; }

        public int? ManageID { get; set; }

        public int? Number { get; set; }

        public long? PayMoney { get; set; }

        public int AutoID { get; set; }

        public int? CodeID { get; set; }

        public string CodeItem { get; set; }

        public string ItemName { get; set; }

        public int? FromDate { get; set; }

        public int? ToDate { get; set; }

        public int? CountDate { get; set; }

        public string NumberCard { get; set; }

        public int? AgencyID { get; set; }

        public string AgencyName { get; set; }

        public string PhoneRelationShip { get; set; }

        public int? ProductId { get; set; }

        public string ProductName { get; set; }

        public int? UserIdRemindDebt { get; set; }

        public int? RateType { get; set; }

        public long? ShopIdConsulting { get; set; }

        public int? CountProcess { get; set; }

        public string RegistrationContractId { get; set; }

        public string RegistrationCustomerId { get; set; }

        public long? OtherMoney { get; set; }

        public bool? IsHub { get; set; }

        public short? IsKeep { get; set; }

        public int? SecondUserIdRemindDebt { get; set; }

    }

}
