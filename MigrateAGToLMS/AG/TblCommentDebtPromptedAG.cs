﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblCommentDebtPrompted")]
    public class TblCommentDebtPromptedAG
    {
        [Key]
        public int Id { get; set; }

        public int? LoanId { get; set; }

        public int? TypeId { get; set; }

        public int? UserId { get; set; }

        public int? ShopId { get; set; }

        public string FullName { get; set; }

        public string Comment { get; set; }

        public DateTime? CreateDate { get; set; }

        public int? ReasonId { get; set; }

        public short? ActionPerson { get; set; }

        public short? ActionAddress { get; set; }

        public DateTime? NextDate { get; set; }

        public int? GroupID { get; set; }

        public string C_ReasonCode { get; set; }

        public long? LoanCreditIdOfPartenter { get; set; }

        public int? IsDisplay { get; set; }
        public long LMSCommentID { get; set; }

    }
}
