﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblReason")]
    public class TblReasonAG
    {
        [Key]
        public int Id { get; set; }

        public string Reason { get; set; }

        public int? Type { get; set; }

        public int? Sort { get; set; }

        public bool? IsEnable { get; set; }

        public int? ReSend { get; set; }

        public int? TypeRemarketing { get; set; }

        public int? IsApplyBlackList { get; set; }

        public int? NumberDayBlackList { get; set; }

        public string ReasonCode { get; set; }

        public string Note { get; set; }

        public int? ReasonGroupId { get; set; }

        public int? GroupUserId { get; set; }

        public int? IsCallBack { get; set; }

        public bool? IsQlf { get; set; }

        public string ReasonEng { get; set; }

    }

}
