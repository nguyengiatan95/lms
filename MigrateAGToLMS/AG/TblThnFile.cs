﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblThnFiles")]
    public class TblThnFiles
    {
        [Key]
        public long ID { get; set; }

        public string FilePath { get; set; }

        public int? UserID { get; set; }

        public long? CommentID { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? Status { get; set; }

        public long? LoanID { get; set; }

    }
}
