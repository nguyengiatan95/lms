﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("tblLenderSpicesConfig")]
    public class TblLenderSpicesConfigAG
    {
        public int ShopId { get; set; }

        public string LoanTimes { get; set; }

        public int? RateType { get; set; }

        public long MoneyMin { get; set; }

        public long MoneyMax { get; set; }

    }
}
