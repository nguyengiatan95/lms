﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblTransactionBankCard")]
    public class TblTransactionBankCardAG
    {
        [Key]
        public int Id { get; set; }

        public int UserIdAction { get; set; }

        public string UserNameAction { get; set; }

        public string FullNameAction { get; set; }

        public DateTime CreateOn { get; set; }

        public DateTime? ModifyOn { get; set; }

        public int LoanId { get; set; }

        public int LoanCodeId { get; set; }

        public int CustomerId { get; set; }

        public string CustomerName { get; set; }

        public long TotalMoney { get; set; }

        public int BankCardId { get; set; }

        public string BankCardAliasName { get; set; }

        public int CollectorsShopId { get; set; }

        public string CollectorsShopName { get; set; }

        public int OwnedShopId { get; set; }

        public string OwnedShopName { get; set; }

        public int? PayShopId { get; set; }

        public string PayShopName { get; set; }

        public int TypeTransactionBank { get; set; }

        public string NameTransactionBank { get; set; }

        public DateTime? DateTimeTransaction { get; set; }

        public short Status { get; set; }

        public string Note { get; set; }

        public int UserIdModify { get; set; }

        public string UserNameModify { get; set; }

        public string FullnameModify { get; set; }

        public int TransactionBankCardForeignKeyId { get; set; }

        public long OtherMoney { get; set; }

        public int BankCardIdReceive { get; set; }

        public string BankCardAliasNameReceive { get; set; }

        public long TotalMoneyReceive { get; set; }

        public long MoneyFeeInsuranceOfCustomer { get; set; }

        public int SmsAnalyticsId { get; set; }

        public int UserIdRemind { get; set; }

        public string FullNameUserRemind { get; set; }
    }
}
