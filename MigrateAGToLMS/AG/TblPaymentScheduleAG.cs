﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.AG
{
    [Table("TblPaymentSchedule")]
    public class TblPaymentScheduleAG
    {
        [Key]
        public long PaymentScheduleID { get; set; }

        public long LoanID { get; set; }

        public long CustomerID { get; set; }

        public long LenderID { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public DateTime? PayDate { get; set; }

        public long TotalOriginalCurrent { get; set; }

        public long MoneyOriginal { get; set; }

        public long MoneyInterest { get; set; }

        public long MoneyService { get; set; }

        public long MoneyConsultant { get; set; }

        public long MoneyFineLate { get; set; }

        public long MoneyFineInterestLate { get; set; }

        public long PayMoneyOriginal { get; set; }

        public long PayMoneyInterest { get; set; }

        public long PayMoneyService { get; set; }

        public long PayMoneyConsultant { get; set; }

        public long PayMoneyFineLate { get; set; }

        public long PayMoneyFineOriginal { get; set; }

        public long PayMoneyFineInterestLate { get; set; }

        public int IsComplete { get; set; }

        public int IsVisible { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public DateTime? FirstPaymentDate { get; set; }

        public DateTime? CompletedDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public int Status { get; set; }

        public string JsonExtra { get; set; }
        /// <summary>
        /// 0: chưa phạt, 1 đã phạt
        /// </summary>
        public int Fined { get; set; }
        /// <summary>
        /// ngày phạt sẽ dc áp dụng theo từng chính sách
        /// </summary>
        public DateTime? DateApplyFineLate { get; set; }
    }


}
