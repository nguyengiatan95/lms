﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Table("TblLenderSpicesConfig")]
    public class TblLenderSpicesConfig
    {
        [Key]
        public long LenderSpicesConfigID { get; set; }

        public long? UserID { get; set; }
        public long LenderID { get; set; }

        public string LoanTimes { get; set; }

        public string RateType { get; set; }

        public long? MoneyMin { get; set; }

        public long? MoneyMax { get; set; }

    }
}
