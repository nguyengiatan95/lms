﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Table("TblExcelReport")]
    public class TblExcelReport
    {
        [Key]
        public long ExcelReportID { get; set; }

        public int? TypeReport { get; set; }

        public long? CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string ExtraData { get; set; }

        public int? Status { get; set; }

        public string TraceIDRequest { get; set; }

        public DateTime? ModifyDate { get; set; }
        public string RequestQuery { get; set; }
        public string ReportName { get; set; }
    }

    public class RequestGetExcelByTraceID
    {
        public string TraceIDRequest { get; set; }
        public int CreateBy { get; set; }
    }
}
