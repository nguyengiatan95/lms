﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Table("TblInvoice")]
    public class TblInvoice
    {
        [Key]
        public long InvoiceID { get; set; }

        public int InvoiceType { get; set; }

        public int InvoiceSubType { get; set; }

        public string InvoiceCode { get; set; }

        public long ShopID { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime? ModifyDate { get; set; }

        public long UserIDCreate { get; set; }

        public long UserIDModify { get; set; }

        public DateTime? TransactionDate { get; set; }

        public long TotalMoney { get; set; }

        public string Note { get; set; }

        public long BankCardID { get; set; }

        public long SourceID { get; set; }

        public long DestinationID { get; set; }


        [Dapper.Contrib.Extensions.Key]
        public System.Byte[] Version { get; set; }

        public string JsonExtra { get; set; }

        public short Status { get; set; }

        public long LoanID { get; set; }

        public long MoneyFee { get; set; }

        public long MoneyVAT { get; set; }

        public long TransactionMoney { get; set; }

        public long CustomerID { get; set; }

        public long? LenderID { get; set; }

        public int JobStatus { get; set; }

        public int Payee { get; set; }

        public long PartnerID { get; set; } // ID cua Tima
        //public long TimaInvoiceID { get; set; }
    }

    public class InvoiceJsonExtra
    {
        public string SourceName { get; set; }
        public string DestinationName { get; set; }
        public string ShopName { get; set; }
        public string CreateBy { get; set; }
    }


    public class GetInvoiceType
    {
        public long SourceID { get; set; }
        public long DestinationID { get; set; }
        public int InvoiceType { get; set; }
        public int InvoiceSubType { get; set; }
        public string JsonExtra { get; set; }
    }

    public class InvoiceCompare
    {
        public long ReceiptCustomer { get; set; }
        public long CollectExpense_TotalCount { get; set; }

        public long CollectExpense_TotalMoney { get; set; }
        public long Waiting { get; set; }
        public long ReceiptCustomerConsider { get; set; }
        public long ReceiptOnBehalfShop { get; set; }
        public long ReceiptInternal { get; set; }
        public long ReceiptLenderCapital { get; set; }
        public long ReceiptInterestBank { get; set; }
        public long ReceiptLenderInsurance { get; set; }
        public long ReceiptPartner { get; set; }
        public long ReceiptLenderTopUp { get; set; }
        public long ReceiptOther { get; set; }
        public long Disbursement { get; set; }
        public long PaySlipTransferInternal { get; set; }
        public long PaySlipGiveBackExcessCustomer { get; set; }
        public long PaySlipLenderWithdraw { get; set; }
        public long PaySlipAdvancePaymentForLender { get; set; }
        public long PaySlipFeeForBank { get; set; }
        public long PaySlipOther { get; set; }
        public long PaySlipDisbursement { get; set; }
        public long PaySlipFeeTransferMoney { get; set; }
        public long PaySlipBankFee { get; set; }
        public long PaySlipTheDebt { get; set; }
        public long TransferMoneyStoreReasonOther { get; set; }



        public long MoneyReceiptCustomer { get; set; }
        public long MoneyCollectExpense_TotalCount { get; set; }

        public long MoneyCollectExpense_TotalMoney { get; set; }
        public long MoneyWaiting { get; set; }
        public long MoneyReceiptCustomerConsider { get; set; }
        public long MoneyReceiptOnBehalfShop { get; set; }
        public long MoneyReceiptInternal { get; set; }
        public long MoneyReceiptLenderCapital { get; set; }
        public long MoneyReceiptInterestBank { get; set; }
        public long MoneyReceiptLenderInsurance { get; set; }
        public long MoneyReceiptPartner { get; set; }
        public long MoneyReceiptLenderTopUp { get; set; }
        public long MoneyReceiptOther { get; set; }
        public long MoneyDisbursement { get; set; }
        public long MoneyPaySlipTransferInternal { get; set; }
        public long MoneyPaySlipGiveBackExcessCustomer { get; set; }
        public long MoneyPaySlipLenderWithdraw { get; set; }
        public long MoneyPaySlipAdvancePaymentForLender { get; set; }
        public long MoneyPaySlipFeeForBank { get; set; }
        public long MoneyPaySlipOther { get; set; }
        public long MoneyPaySlipDisbursement { get; set; }
        public long MoneyPaySlipFeeTransferMoney { get; set; }
        public long MoneyPaySlipBankFee { get; set; }
        public long MoneyPaySlipTheDebt { get; set; }
        public long MoneyTransferMoneyStoreReasonOther { get; set; }
        public long MoneyDeposit { get; set; }
        public long DepositCount { get; set; }
    }

    [Dapper.Contrib.Extensions.Table("TblLogInvoice")]
    public class TblLogInvoice
    {
        [Dapper.Contrib.Extensions.Key]
        public long LogInvoiceID { get; set; }

        public long InvoiceID { get; set; }

        public int? InvoiceType { get; set; }

        public int? Status { get; set; }

        public long? Amount { get; set; }

        public long? CustomerID { get; set; }

        public string Note { get; set; }

        public long? FromBankCardID { get; set; }

        public long? ToBankCardID { get; set; }

        public DateTime? TransactionDate { get; set; }

        public long? LoanID { get; set; }

        public long? FromShopID { get; set; }

        public long? ToShopID { get; set; }

        public long? CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string Request { get; set; }

        public long? LenderID { get; set; }
        public long PartnerID { get; set; }
        public string PathImg { get; set; }
    }
}
