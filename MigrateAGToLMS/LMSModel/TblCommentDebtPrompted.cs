﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Table("TblCommentDebtPrompted")]
    public class TblCommentDebtPrompted
    {
        [Key]
        public long CommentDebtPromptedID { get; set; }

        public long? LoanID { get; set; }

        public long? UserID { get; set; }

        public string FullName { get; set; }

        public string Comment { get; set; }

        public long? ReasonCodeID { get; set; }

        public string ReasonCode { get; set; }

        public int? IsDisplay { get; set; }

        public DateTime? CreateDate { get; set; }
        public long TimaCommentID { get; set; }
        public long TimaLoanID { get; set; }
        public string FileImage { get; set; }
        public DateTime? AppointmentDate { get; set; }
    }
}
