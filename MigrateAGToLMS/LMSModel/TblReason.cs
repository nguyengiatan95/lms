﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Table("TblReason")]
    public class TblReason
    {
        [Key]
        public long ReasonID { get; set; }

        public string Description { get; set; }

        public string ReasonCode { get; set; }

        public int? DepartmentID { get; set; }

        public int? Status { get; set; }
    }
}
