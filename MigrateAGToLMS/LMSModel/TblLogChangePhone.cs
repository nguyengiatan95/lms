﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Table("TblLogChangePhone")]
    public class TblLogChangePhone
    {
        [Key]
        public long LogChangePhoneID { get; set; }

        public long? LoanID { get; set; }

        public long? CustomerID { get; set; }

        public string OldPhone { get; set; }

        public string NewPhone { get; set; }

        public string Note { get; set; }

        public long? CreateBy { get; set; }

        public string CreateByName { get; set; }

        public DateTime? CreateDate { get; set; }
        public long TimaLogChangePhoneID { get; set; }
    }
}
