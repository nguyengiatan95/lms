﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Table("TblLogCompare")]
    public class TblLogCompare
    {
        [Key]
        public long LogCompareID { get; set; }

        public int MainType { get; set; }

        public int DetailType { get; set; }

        public string DetailNote { get; set; }

        public long TotalAG { get; set; }

        public long TotalLMS { get; set; }

        public DateTime ForDate { get; set; }

        public DateTime CreateDate { get; set; }

        public int Status { get; set; }
    }
}
