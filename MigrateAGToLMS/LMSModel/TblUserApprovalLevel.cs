﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Table("TblUserApprovalLevel")]
    public class TblUserApprovalLevel
    {
        [Key]
        public long UserApprovalLevelID { get; set; }

        public long UserID { get; set; }

        public long? ApprovalLevelBookDebtID { get; set; }

        public long? MoveToApprovalLevelBookDebtID { get; set; }

        public DateTime? CreateDate { get; set; }

        public long? CreateBy { get; set; }
        public DateTime? ModifyDate { get; set; }

        public long? ModifyBy { get; set; }
        public int Status { get; set; }

    }
}
