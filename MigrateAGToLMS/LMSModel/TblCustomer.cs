﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Table("TblCustomer")]
    public class TblCustomer
    {
        [Key]
        public long CustomerID { get; set; }

        public string FullName { get; set; }

        public string Phone { get; set; }

        public string NumberCard { get; set; }

        public DateTime? BirthDay { get; set; }

        public short? Gender { get; set; }

        public DateTime? CreateDate { get; set; }

        public int? CityID { get; set; }

        public int? DistrictID { get; set; }

        public int? WardID { get; set; }

        public string Address { get; set; }

        public string AddressHouseHold { get; set; }

        public long? TotalMoney { get; set; }

        public int? IsBorrowing { get; set; }

        public short? AutoPayment { get; set; }

        public DateTime? ModifyDate { get; set; }

        public long? TotalMoneyAccounting { get; set; }

        public string PermanentAddress { get; set; }

        public string Email { get; set; }

        public long? TimaCustomerID { get; set; }
    }

    public class CustomerCompare
    {
        public string CustomerName { get; set; }
        public long CustomerAGID { get; set; }
        public long CustomerLMSID { get; set; }
        public long TotalMoneyAG { get; set; }
        public long TotalMoneyLMS { get; set; }
    }

    public class CustomerLoan
    {
        public long LoanID { get; set; }
        public long TimaLoanID { get; set; }
        public long TimaCustomerID { get; set; }
        public long CustomerID { get; set; }
    }
}
