﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Dapper.Contrib.Extensions.Table("TblReportEmployeeHandleLoan")]
    public class TblReportEmployeeHandleLoan
    {
        [Dapper.Contrib.Extensions.Key]
        public long ReportEmployeeHandleLoanID { get; set; }

        public DateTime ReportDate { get; set; }

        public long EmployeeID { get; set; }

        public int TotalLoanHandle { get; set; }

        public int NumberLoanHandleDone { get; set; }

        public int NumberLoanNeedCollection { get; set; }

        public int NumberCustomerTopUp { get; set; }

        public int NumberComment { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }

        public string LstLoanID { get; set; }
    }
}
