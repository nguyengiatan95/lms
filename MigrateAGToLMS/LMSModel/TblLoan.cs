﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Table("TblLoan")]
    public class TblLoan
    {
        [Key]
        public long LoanID { get; set; }

        public long? CustomerID { get; set; }

        public string CustomerName { get; set; }

        public long? ProductID { get; set; }

        public string ProductName { get; set; }

        public long? TotalMoneyDisbursement { get; set; }

        public long? TotalMoneyCurrent { get; set; }

        public long? CityID { get; set; }

        public long? DistrictID { get; set; }

        public long? WardID { get; set; }

        public short? SourceMoneyDisbursement { get; set; }

        public int LoanTime { get; set; }

        public short? RateType { get; set; }

        public int? Frequency { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public DateTime? FinishDate { get; set; }

        public string ContactCode { get; set; }

        public long? OwnerShopID { get; set; }

        public long? LenderID { get; set; }

        public long? ConsultantShopID { get; set; }

        public decimal? RateInterest { get; set; }

        public decimal? RateConsultant { get; set; }

        public decimal? RateService { get; set; }

        public DateTime? NextDate { get; set; }

        public DateTime? LastDateOfPay { get; set; }

        public int? Status { get; set; }

        public int? SourcecBuyInsurance { get; set; }

        public string LinkGCNChoVay { get; set; }

        public string LinkGCNVay { get; set; }

        public long? MoneyFeeInsuranceOfCustomer { get; set; }

        public long? MoneyFeeInsuranceOfLender { get; set; }

        public string JsonExtra { get; set; }

        public long? MoneyFineLate { get; set; }

        public long? MoneyFineOriginal { get; set; }

        public long? MoneyFineInterest { get; set; }

        //public long Version { get; set; }

        public long? BankCardID { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }

        public long? LoanCreditIDOfPartner { get; set; }

        public long? TimaLoanID { get; set; }
        public int UnitRate { get; set; }
        public long TimaCodeID { get; set; }
        public long StatusSendInsurance { get; set; }

        public long MoneyFeeInsuranceMaterialCovered { get; set; }

        public decimal FeeFineOriginal { get; set; }
        public int UnitFeeFineOriginal { get; set; }
        public long? DebitMoney { get; set; }
        public int? YearBadDebt { get; set; }
    }

    public class LoanCompare
    {
        public long Disbursement_TotalLoan { get; set; }
        public long Disbursement_TotalCustomer { get; set; }
        public long Disbursement_TotalMoney { get; set; }
        public long Disbursement_TotalMoneyExpense { get; set; }
        public long Disbursement_TotalMoneyInsurance { get; set; }
        public long Disbursement_TotalMoneyLenderHold { get; set; }

        public long Loan_TotalLending { get; set; }
        public long Loan_TotalWaitingInsurance { get; set; }
        public long Loan_TotalSentInsurance { get; set; }
        public long Loan_TotalInsurancePaid { get; set; }
        public long Loan_TotalOverTime { get; set; }
        public long Loan_TotalBadDebt { get; set; }
        public long Loan_TotalClosed { get; set; }

    }

    [Table("TblLoan")]
    public class LoanUpdateFeeFineOriginal
    {
        [Dapper.Contrib.Extensions.Key]
        public long LoanID { get; set; }
        public long TimaLoanID { get; set; }
        public decimal FeeFineOriginal { get; set; }
        public int UnitFeeFineOriginal { get; set; }
        public int Status { get; set; }

        [Dapper.Contrib.Extensions.Key]
        public System.Byte[] Version { get; set; }
    }

    [Table("TblLoan")]
    public class LoanUpdateYearBadDebt
    {
        [Dapper.Contrib.Extensions.Key]
        public long LoanID { get; set; }
        public int? YearBadDebt { get; set; }
    }

    [Table("TblLoan")]
    public class LoanUpdateLoanJsonExtra
    {
        [Dapper.Contrib.Extensions.Key]
        public long LoanID { get; set; }
        public string JsonExtra { get; set; }
    }
}
