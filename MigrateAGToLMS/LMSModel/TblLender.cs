﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Dapper.Contrib.Extensions.Table("TblLender")]
    public class TblLender
    {
        [Dapper.Contrib.Extensions.Key]
        public long LenderID { get; set; }

        public string FullName { get; set; }

        public string NumberCard { get; set; }

        public DateTime? BirthDay { get; set; }

        public int? Gender { get; set; }

        public string Phone { get; set; }

        public string TaxCode { get; set; }

        public int? Status { get; set; }

        public string Address { get; set; }
        // lãi suất % theo năm
        public decimal RateInterest { get; set; }
        [Dapper.Contrib.Extensions.Key]
        public byte[] Version { get; set; }
        public long TotalMoney { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public long TimaLenderID { get; set; }
        public int IsHub { get; set; }
        public int UnitRate { get; set; }
        public int IsVerified { get; set; }

        public string InviteCode { get; set; }

        public decimal? RateLender { get; set; }

        public decimal? RateAff { get; set; }

        public string Represent { get; set; }

        public string AddressOfResidence { get; set; }

        public DateTime? CardDate { get; set; }

        public string CardPlace { get; set; }

        public string Email { get; set; }

        public string ContractCode { get; set; }

        public DateTime? ContractDate { get; set; }

        public string AccountBanking { get; set; }

        public long? BankID { get; set; }

        public long? LenderCareUserID { get; set; }

        public long? LenderTakeCareUserID { get; set; }

        public long? AffID { get; set; }

        public string RepresentPhone { get; set; }

        public string BranchOfBank { get; set; }
        public string InvitePhone { get; set; }
        public int IsGcash { get; set; }
        public int RegFromApp { get; set; }
        public int SelfEmployed { get; set; }
        public long OwnerUserID { get; set; }
        public int IsDeposit { get; set; }

    }

    public class LenderCompare
    {
        public long WaitingVerify { get; set; }
        public long IsVerified { get; set; }
        public long Active { get; set; }
        public long Lock { get; set; }
        public long IsDelete { get; set; }
    }


    [Dapper.Contrib.Extensions.Table("TblUserLender")]
    public class TblUserLender
    {
        [Key]
        public long UserLenderID { get; set; }

        public long? UserID { get; set; }

        public string FullName { get; set; }

        public string Phone { get; set; }

        public int? Gender { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string Email { get; set; }

        public string PermanentResidenceAddress { get; set; }

        public string TemporaryResidenceAddress { get; set; }

        public long? CityID { get; set; }

        public long? DistrictID { get; set; }

        public long? WardID { get; set; }

        public string NumberCard { get; set; }

        public DateTime? CardDate { get; set; }

        public int? Status { get; set; }

        public int? IsVerified { get; set; }

        public DateTime? CreateDate { get; set; }

        public string CardPlace { get; set; }

        public DateTime? ModifyDate { get; set; }
    }

    [Table("TblLender")]
    public class TblLenderOwnerShop
    {
        [Key]
        public long LenderID { get; set; }

        public long OwnerUserID { get; set; }
        public int ContractType { get; set; }
    }
}
