﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Table("TblBankCard")]
    public class TblBankCard
    {
        [Key]
        public long BankCardID { get; set; }

        public long BankID { get; set; }

        public string NumberAccount { get; set; }

        public string BankCode { get; set; }

        public int Status { get; set; }

        public DateTime CreateOn { get; set; }

        public DateTime? ModifyOn { get; set; }

        public string AccountHolderName { get; set; }

        public string BranchName { get; set; }

        public int TypePurpose { get; set; }

        public string AliasName { get; set; }

        public string AliasNameForFast { get; set; }

        public int ParentID { get; set; }
        public long TimaBankCardID { get; set; }
    }
}
