﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Dapper.Contrib.Extensions.Table("TblSettingDateBankCard")]
    public class TblSettingDateBankCard
    {
        [Dapper.Contrib.Extensions.Key]
        public long SettingDateBankCardID { get; set; }

        public long BankCardID { get; set; }

        public long TotalMoney { get; set; }

        public DateTime DateApply { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }

        public long CreateBy { get; set; }
    }
}
