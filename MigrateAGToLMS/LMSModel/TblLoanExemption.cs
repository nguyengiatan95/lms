﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Dapper.Contrib.Extensions.Table("TblLoanExemption")]
    public class TblLoanExemption
    {
        [Dapper.Contrib.Extensions.Key]
        public long LoanExemptionID { get; set; }

        public long LoanID { get; set; }

        public long TimaLoanID { get; set; }

        public DateTime CloseDate { get; set; }

        public long TotalMoneyCurrent { get; set; }

        public long MoneyOriginal { get; set; }

        public long MoneyInterest { get; set; }

        public long MoneyConsultant { get; set; }

        public long MoneyService { get; set; }

        public long MoneyFineLate { get; set; }

        public long MoneyFineOriginal { get; set; }

        public int Status { get; set; }

        public long ApprovalLevelBookDebtID { get; set; }

        public long BookDebtID { get; set; }

        public DateTime ExpirationDate { get; set; }

        public long PayMoneyOriginal { get; set; }

        public long PayMoneyInterest { get; set; }

        public long PayMoneyConsultant { get; set; }

        public long PayMoneyService { get; set; }

        public long PayMoneyFineLate { get; set; }

        public long PayMoneyFineOriginal { get; set; }

        public long CreateBy { get; set; }

        public long ApprovedBy { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }

        public string CreateByFullName { get; set; }

        public string ApproveByFullName { get; set; }

        /// <summary>
        /// LoanExemptionExtraData
        /// </summary>
        public string ExtraData { get; set; }
        public string ReasonCancel { get; set; }
        public long CustomerID { get; set; }
        public string ReferTraceIDRequest { get; set; }

    }

    public class LoanExemptionExtraDataUserChangeItem
    {
        public long UserID { get; set; }
        public DateTime CreateTime { get; set; }
        public long ApprovalLevelBookDebtID { get; set; }
    }
    public class LoanExemptionExtraData
    {
        public string ReferTraceIDRequest { get; set; }
        public List<LoanExemptionExtraDataUserChangeItem> LstUserChange { get; set; }
    }
}
