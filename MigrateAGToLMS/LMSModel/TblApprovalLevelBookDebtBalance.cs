﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Dapper.Contrib.Extensions.Table("TblApprovalLevelBookDebtBalance")]
    public class TblApprovalLevelBookDebtBalance
    {
        [Dapper.Contrib.Extensions.Key]
        public long ApprovalLevelBookDebtBalanceID { get; set; }

        public long ApprovalLevelBookDebtID { get; set; }

        public int YearMonthCurrent { get; set; }

        public long TotalMoneyCurrent { get; set; }

        public long TotalMoneySetting { get; set; }

        public string LstLoanIDHandle { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }
    }
}
