﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Table("TblDebt")]
    public class TblDebt
    {
        [Key]
        public long DebtID { get; set; }

        public long ReferID { get; set; }

        public long? TotalMoney { get; set; }

        public int? TypeID { get; set; }

        public long? TargetID { get; set; }

        public string Description { get; set; }

        public int? Status { get; set; }

        public DateTime? CreateDate { get; set; }

        public long? CreateBy { get; set; }
        public long SourceID { get; set; }
        public long ParentDebtID { get; set; }
    }
}
