﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Table("TblLoanCreditDetailLos")]
    public class TblLoanCreditDetailLos
    {
        [Key]
        public long LoanCreditDetailLosID { get; set; }

        public long LoanID { get; set; }

        public long LoanCreditIDOfPartner { get; set; }

        public string JsonExtra { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }
    }
}
