﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Table("TblUserDepartment")]
    public class TblUserDepartment
    {
        [Key]
        public long UserDepartmentID { get; set; }

        public long? UserID { get; set; }

        public long? DepartmentID { get; set; }

        public long? PositionID { get; set; }

        public int? Status { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? ModifyDate { get; set; }
    }
}
