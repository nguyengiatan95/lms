﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Table("TblGroup")]
    public class TblGroup
    {
        [Key]
        public long GroupID { get; set; }

        public string GroupName { get; set; }

        public int? Status { get; set; }
    }

}
