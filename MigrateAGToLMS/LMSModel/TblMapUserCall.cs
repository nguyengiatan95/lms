﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Dapper.Contrib.Extensions.Table("TblMapUserCall")]
    public class TblMapUserCall
    {
        [Dapper.Contrib.Extensions.Key]
        public long MapUserCallID { get; set; }

        public long UserID { get; set; }
        /// <summary>
        /// 1: Caresoft, 2: cisco
        /// </summary>
        public int TypeCall { get; set; }
        /// <summary>
        /// thông tin lấy từ 2 class UserCallCareSoftExtra, UserCallCiscoExtra
        /// </summary>
        public string JsonExtra { get; set; }

        public int Status { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }
    }

    public class UserCallCareSoftExtra
    {
        public string IPPhone { get; set; }
    }
    public class UserCallCiscoExtra
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Extension { get; set; }
    }
}
