﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Table("TblUserGroup")]
    public class TblUserGroup
    {
        [Key]
        public long UserGroupID { get; set; }

        public long? UserID { get; set; }

        public long? GroupID { get; set; }
    }
}
