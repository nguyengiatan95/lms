﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MigrateAGToLMS.LMSModel
{
    [Dapper.Contrib.Extensions.Table("TblHoldCustomerMoney")]
    public class TblHoldCustomerMoney
    {
        [Dapper.Contrib.Extensions.Key]
        public long HoldCustomerMoneyID { get; set; }

        public long CustomerID { get; set; }

        public long Amount { get; set; }

        public short? Status { get; set; }

        public string Reason { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? ModifyDate { get; set; }

        public long CreateBy { get; set; }

        public long ModifyBy { get; set; }
        public long TimaHoldCustomerMoneyID { get; set; }

    }
}
