﻿using LMS.Common.Helper;
using MigrateAGToLMS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MigrateAGToLMS
{
    class Program
    {

        static void Main(string[] args)
        {
            //var lstLoanIds = new List<int>()
            //{
            //    82880 , 90196 , 115820 , 115868 , 117450 , 115314 , 115350 , 115370 , 115360 , 115442 , 115592 , 114400 , 115292 , 115846 , 115836 , 115844 , 115856 , 115874 , 115882 , 115886 , 115906 , 115918 , 115512 , 117328 , 114612 , 116206 , 116196 , 116232 , 116256 , 116304 , 116312 , 115652 , 116106 , 116126 , 116128 , 115516 , 115520 ,
            //    93762 , 106724 ,
            //    116528 , 116538 , 116540 , 116554 , 116558 , 116566 , 113750 , 135066 , 116722 , 116728 , 116730 , 116736 , 116750 , 116760 , 93762 , 136330 , 116638 , 116798 , 116876 , 116880 , 116944 , 117234 , 116576 , 116616 , 116628 , 115060 , 116430 , 116450 , 116468 , 116500 , 117070 , 117002 , 117450 , 117116 , 117126 , 117146 , 117148 , 116670 , 116658 , 116662 , 117238 , 106724 , 117240 , 117262 , 117246 , 117264 , 117268 , 117282 , 117298 , 117300 , 135232 , 117030 , 117020 , 117036 , 117060 , 117168 , 117156 , 117188 , 117192 , 117206 , 117228 , 115450 , 136512 , 135350 , 135352 , 113152 , 116938 , 116952 , 116978 , 117000 , 135134 , 113264 , 113320 , 136998 , 137000 , 115486 , 117312 , 117328 , 117344 , 117356 , 94224 , 136916 , 136800 , 136804 , 136866 , 137216 , 134716 , 137024 , 137046 , 134668 , 134678 , 135828 , 116354 , 116384 , 116408 , 135880 , 136738 , 137188 , 137286 , 116680 , 116704 , 116690 , 116706 , 116712 , 137134 , 137138 , 137152 , 116790 , 116796 , 116810 , 116812 , 116820 , 116846 , 134948 , 116866 , 116856 , 136660 , 134832 , 116156 , 134712 , 135516 , 116910 , 116926 , 113884 , 102738 , 134358 , 134374 , 134400 , 117444 , 134430 ,
            //};
            //lstLoanIds = lstLoanIds.Distinct().ToList();
            //var lstBatch = lstLoanIds.Batch(1000);
            //foreach (var item in lstBatch)
            //{
            //    RunListLoan(item.ToList());
            //}
            //while (true)
            //{
            MoveData();
            //RerunCommentMissing();
            //List<long> lstCustomerTimaID = new List<long>
            //{209751 , 255386 , 244521 , 284121 , 240131 , 215342 , 222964 , 264686 , 274792 , 282266 , 290870

            //};
            //SyncMoneyCustomerTima(lstCustomerTimaID);
            //Compare();
            //}
            //Thread.Sleep(6000);
            //RerunLoanExtraError();
            //RerunCustomerError();
            //RunTransactionIndemnifyLoan();
            //RunAFF();
            //SyncInvoiceFromAG();
            //List<long> lstUserID = new List<long>() { 49637, 83078 };
            //SyncUserTimaByLstID(lstUserID);
            //new Service.LoanManager().UpdateLoanJsonFromLos();
            //new Service.ReasonManager().MoveReasonCodeLMSToAG();
        }
        static void MoveData()
        {
            //var watch = System.Diagnostics.Stopwatch.StartNew();
            Service.LenderManager lenderManager = new Service.LenderManager();
            Service.CustomerManager customerManager = new Service.CustomerManager();
            Service.LoanManager loanManager = new Service.LoanManager();
            Service.PaymentManager paymentManager = new Service.PaymentManager();
            Service.BankCardManager bankCardManager = new Service.BankCardManager();
            Service.InvoiceManager invoiceManager = new Service.InvoiceManager();
            Service.TransactionManager transactionManager = new Service.TransactionManager();
            Service.UserManager userManager = new Service.UserManager();
            Service.GroupManager groupManager = new Service.GroupManager();
            Service.LendingCertificateInformationManager lendingCertificateInformationManager = new Service.LendingCertificateInformationManager();
            Service.LogSendStatusInsuranceManager logSendStatusManager = new Service.LogSendStatusInsuranceManager();
            Service.ReasonManager reasonManager = new Service.ReasonManager();
            Service.CommentDebtPromptedManager commentDebtPromptedManager = new Service.CommentDebtPromptedManager();
            Service.SystemManager sysManager = new Service.SystemManager();
            Service.LoanExtraManager loanExtraManager = new Service.LoanExtraManager();
            Service.HoldMoneyManager holdMoneyManager = new Service.HoldMoneyManager();
            Service.PreparationCollectionLoanManager preparationCollectionLoanManager = new Service.PreparationCollectionLoanManager();
            Service.AssignmentLoanManager assignmentLoanManager = new Service.AssignmentLoanManager();
            Service.CustomerBankManager customerBankManager = new Service.CustomerBankManager();
            Service.CutOffLoanManager cutOffLoanManager = new Service.CutOffLoanManager();
            Service.ReportEmployeeHandleLoanManager employeeHandleLoanManager = new Service.ReportEmployeeHandleLoanManager();
            Service.LogChangePhoneManager logChangePhoneManager = new Service.LogChangePhoneManager();
            Service.LoanExemptionManager loanExemptionManager = new LoanExemptionManager();
            Service.ReportLoanDebtDailyManager reportLoanDebtDailyManager = new ReportLoanDebtDailyManager();
            //reportLoanDebtDailyManager.UpdateSourceBuyInsurance();
            //loanExemptionManager.UpdateMoneyCurrentLevelApproval();
            //transactionManager.MoveTransactionInsurance();
            //userManager.MoveUserTima();
            //invoiceManager.MoveTransactionBankCardTima(miss: true, fromDate:"16-11-2021",toDate: "17-11-2021");

            // DateTime dtNow = DateTime.Now;
            //logChangePhoneManager.MoveLogChangePhone();
            //groupManager.MoveGroupFromEnum();
            //var taskLender = Task.Run(() =>
            //{
            //    customerManager.MoveCustomerTima(true,loanTimaIds: lstLoanIds);
            //});
            //Task.WaitAll(taskLender);
            //var taskCustomer = Task.Run(() =>
            //{
            //    customerManager.MoveCustomerTima();
            //});
            //Task.WaitAll(taskCustomer);
            //var taskLoan = Task.Run(() =>
            //{
            //    loanManager.MoveLoanTima();
            //});
            //Task.WaitAll(taskLoan);
            //var taskPayment = Task.Run(() =>
            //{
            //    paymentManager.MovePaymentTima();
            //});
            //var taskTransaction = Task.Run(() =>
            //{
            //    transactionManager.MoveTransactionLoanTima();
            //});
            //var taskTransactionBankCard = Task.Run(() =>
            //{
            //    customerManager.MoveCustomerTima();
            //});
            //Task.WaitAll(taskCustomer);
            //var taskLoan = Task.Run(() =>
            //{
            //    loanManager.MoveLoanTima();
            //});
            //Task.WaitAll(taskLoan);
            //var taskPayment = Task.Run(() =>
            //{
            //    paymentManager.MovePaymentTima();
            //});
            //var taskTransaction = Task.Run(() =>
            //{
            //    transactionManager.MoveTransactionLoanTima();
            //});
            ////var taskTransactionBankCard = Task.Run(() =>
            ////{
            ////    invoiceManager.MoveTransactionBankCardTima();
            ////});
            ////var lendingTask = Task.Run(() =>
            //// {
            lendingCertificateInformationManager.MoveData();
            //// });
            ////var logSendTask = Task.Run(() =>
            ////{
            ////    logSendStatusManager.MoveData();
            ////});

            //Task.WaitAll(taskPayment, taskTransaction);
            //Console.WriteLine("Thoi gian chay :" + (DateTime.Now - dtNow).TotalMinutes);
            //var taskTransactionInsurance = Task.Run(() =>
            //{
            //    transactionManager.MoveTransactionInsurance();
            //});
            //for (int i = 4; i >0; i--)
            //{
            //    DateTime dtNowRun = DateTime.Now.AddDays(-i);
            //    DateTime todate = dtNowRun.AddDays(1);
            //    invoiceManager.MoveTransactionBankCardTima(true, dtNowRun.ToString("dd-MM-yyyy"), todate.ToString("dd-MM-yyyy"));
            //}
            //loanManager.GetListLoanAGChangeToDay();
            ////Task.Run(() =>
            //// {
            ////     reasonManager.MoveReasonTima();
            //// });


            //var taskSMS = Task.Run(() =>
            //{
            //    sysManager.MoveSMS();
            //});
            //loanExtraManager.MoveDData();
            ////var taskAssignLoan = Task.Run(() =>
            ////{
            //assignmentLoanManager.MoveData();
            ////});
            //var taskmoveuserconfigservicecall = Task.Run(() =>
            //{
            //userManager.MoveUserConfigServiceCall();
            //});
            //Task.WaitAll(taskmoveuserconfigservicecall);
            //holdMoneyManager.MoveData();
            //preparationCollectionLoanManager.MoveData();
            //customerBankManager.MoveData();
            //commentDebtPromptedManager.MoveCommentDebtPrompted();
            //cutOffLoanManager.MoveCutOffLoan();
            //employeeHandleLoanManager.MoveEmployeeHandle();
            //var watch = System.Diagnostics.Stopwatch.StartNew();
            //transactionManager.SplitTransactionTimaAndLenderFineOriginal();
            //watch.Stop();
            //var elapsedMs = watch.ElapsedMilliseconds;
            //Console.WriteLine($"time exec ElapsedMilliseconds = {elapsedMs}");
            //lenderManager.MoveLenderTima();
            //lenderManager.MoveUserLenderTima();
            //loanManager.MoveYearBadDebt();

        }
        static void Compare()
        {
            Service.LenderManager lenderManager = new Service.LenderManager();
            Service.LoanManager loanManager = new Service.LoanManager();
            Service.InvoiceManager invoiceManager = new Service.InvoiceManager();
            Service.CustomerManager customerManager = new Service.CustomerManager();
            Service.PaymentManager paymentManager = new Service.PaymentManager();
            Service.SystemManager sysManager = new Service.SystemManager();
            Service.LoanExtraManager loanExtraManager = new Service.LoanExtraManager();

            lenderManager.CompareLender();

            loanManager.CompareLoan();

            invoiceManager.CompareInvoice();

            customerManager.CompareTotalMoneyCustomer();

            sysManager.CompareComment();

            sysManager.ComparePreparationCollection();

            sysManager.CompareHoldMoney();

            sysManager.CompareSMS();

            paymentManager.ComparePayment();
            loanExtraManager.CompareLoanExtra();
        }

        static void RunListLoan(List<int> lstLoanIDs)
        {
            new CustomerManager().MoveCustomerTima(true, loanTimaIds: lstLoanIDs);
            new LoanManager().MoveLoanTima(lstLoanIDs);
            new PaymentManager().MovePaymentTima(lstLoanIDs);
            new TransactionManager().MoveTransactionLoanTima(lstLoanIDs);
            new LoanExtraManager().MoveData(lstLoanIDs);
        }
        static void MoveLoanUpdateFeeFineOriginal()
        {
            new LoanManager().MoveLoanUpdateFeeFineOriginal();
        }

        static void RerunCommentMissing()
        {
            var commentManager = new CommentDebtPromptedManager();
            DateTime createDate = new DateTime(2021, 08, 30);
            long days = (long)(DateTime.Now - createDate).TotalDays;
            for (int i = 0; i < days; i++)
            {
                createDate = createDate.AddDays(1);
                commentManager.MoveCommentWithDay(createDate);
            }
        }

        static void RerunLoanExtraError()
        {
            var loanids = new LoanExtraManager().GetLoanIdsErrorData();
            if (loanids != null)
            {
                RunListLoan(loanids);
            }
        }

        static void RerunCustomerError()
        {
            var loanids = new CustomerManager().GetCustomerError();
            if (loanids != null && loanids.Count > 0)
            {
                RunListLoan(loanids);
            }
        }

        static void RunTrackingLog()
        {
            new TrackingLoanInMonthManager().CallAPITracking();
        }

        static void RunTransactionIndemnifyLoan()
        {
            new LoanExtraManager().RunTransactionIndemnifyLoan();
        }

        static void RunAFF()
        {
            new AffManager().MoveAff();
        }
        static void SyncInvoiceFromAG()
        {
            new InvoiceManager().SyncStatusInvoiceFromAg();
        }
        static void SyncMoneyCustomerTima(List<long> lstCustomerIDAG)
        {
            new CustomerManager().SyncMoneyCustomerTima(lstCustomerIDAG);
        }
        static void SyncUserTimaByLstID(List<long> lstCustomerIDAG)
        {
            new UserManager().MoveUserTima(lstCustomerIDAG);
        }
    }
}
