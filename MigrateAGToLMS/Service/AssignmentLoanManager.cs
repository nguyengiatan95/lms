﻿using LMS.Common.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigrateAGToLMS.Service
{
    public class AssignmentLoanManager
    {
        public void MoveData()
        {
            Console.WriteLine($"-------------- TblAssignmentLoan -------------");
            var currentDate = DateTime.Now;
            DateTime firstDateOfMonth = new DateTime(currentDate.Year, currentDate.Month, 1);
            DateTime lastDateOfMonth = firstDateOfMonth.AddMonths(1).AddDays(-1);
            var assignmentLoanTab = new TableHelper<LMSModel.TblAssignmentLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var loanLmsTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var cutOffLoanTab = new TableHelper<LMSModel.TblCutOffLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var configUserRemindDebtByLoanAndMonthTab = new TableHelper<AG.TblConfigUserRemindDebtByLoanAndMonth>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            // tạm thời chỉ lấy từ đầu tháng
            // nếu có thì ko đồng bộ nữa
            var dataFirstDateOfMonthLMS = assignmentLoanTab.SetGetTop(1).WhereClause(x => x.DateAssigned == firstDateOfMonth).Query().FirstOrDefault();
            if (dataFirstDateOfMonthLMS != null && dataFirstDateOfMonthLMS.AssignmentLoanID > 0)
            {
                Console.WriteLine($"-------------- TblAssignmentLoan Exists-------------");
                assignmentLoanTab.DeleteWhere(x => x.DateAssigned == firstDateOfMonth);
            }
            int latestID = 0;
            int countRows = 0;
            List<LMSModel.TblAssignmentLoan> lstAssignInsert = new List<LMSModel.TblAssignmentLoan>();
            List<LMSModel.TblCutOffLoan> lstCutOffLoanInsert = new List<LMSModel.TblCutOffLoan>();
            while (true)
            {
                lstAssignInsert.Clear();
                lstCutOffLoanInsert.Clear();
                var lstDataAG = configUserRemindDebtByLoanAndMonthTab.SetGetTop(LMS.Common.Constants.TimaSettingConstant.MaxItemQuery)
                                                                     .WhereClause(x => x.ForDate == firstDateOfMonth && x.CurrentUserID > 0)
                                                                     .WhereClause(x => x.Id > latestID)
                                                                     .OrderBy(x => x.Id).Query();
                if (lstDataAG == null || !lstDataAG.Any())
                {
                    break;
                }
                latestID = lstDataAG.OrderBy(x => x.Id).Last().Id;
                var lstLoanAgID = lstDataAG.Select(x => (long)x.LoanId).ToList();
                var dictLoanLMSInfos = loanLmsTab.SelectColumns(x => x.TimaLoanID, x => x.LoanID, x => x.ConsultantShopID, x => x.OwnerShopID, x => x.LenderID)
                                                .SelectColumns(x => x.CustomerID)
                                                .WhereClause(x => lstLoanAgID.Contains((long)x.TimaLoanID))
                                                .Query().ToDictionary(x => x.TimaLoanID, x => x);
                foreach (var item in lstDataAG)
                {
                    var loanLMSInfo = dictLoanLMSInfos.GetValueOrDefault(item.LoanId);
                    if (loanLMSInfo == null || loanLMSInfo.LoanID < 1)
                    {
                        Console.WriteLine($"Not Found Loan TimaLoanID: {item.LoanId}");
                        continue;
                    }
                    if (item.CurrentUserID > 0)
                    {
                        lstAssignInsert.Add(new LMSModel.TblAssignmentLoan
                        {
                            CreateBy = item.CreateUserID,
                            CreateDate = item.CreateDate,
                            DateAssigned = item.ForDate,
                            DateApplyTo = item.ForDate.AddMonths(1).AddDays(-1),
                            LoanID = loanLMSInfo.LoanID,
                            ModifyDate = item.CreateDate,
                            Status = 1,
                            UserID = (long)item.CurrentUserID
                        });
                    }
                    if (item.SecondStaffID > 0)
                    {
                        lstAssignInsert.Add(new LMSModel.TblAssignmentLoan
                        {
                            CreateBy = item.CreateUserID,
                            CreateDate = item.CreateDate,
                            DateAssigned = item.ForDate,
                            DateApplyTo = item.ForDate.AddMonths(1).AddDays(-1),
                            LoanID = loanLMSInfo.LoanID,
                            ModifyDate = item.CreateDate,
                            Status = 1,
                            UserID = (long)item.SecondStaffID
                        });
                    }
                    if (item.TeamleadUserID > 0)
                    {
                        lstAssignInsert.Add(new LMSModel.TblAssignmentLoan
                        {
                            CreateBy = item.CreateUserID,
                            CreateDate = item.CreateDate,
                            DateAssigned = item.ForDate,
                            DateApplyTo = item.ForDate.AddMonths(1).AddDays(-1),
                            LoanID = loanLMSInfo.LoanID,
                            ModifyDate = item.CreateDate,
                            Status = 1,
                            UserID = (long)item.TeamleadUserID
                        });
                    }
                    if (item.SectionManagerUserID > 0)
                    {
                        lstAssignInsert.Add(new LMSModel.TblAssignmentLoan
                        {
                            CreateBy = item.CreateUserID,
                            CreateDate = item.CreateDate,
                            DateAssigned = item.ForDate,
                            DateApplyTo = item.ForDate.AddMonths(1).AddDays(-1),
                            LoanID = loanLMSInfo.LoanID,
                            ModifyDate = item.CreateDate,
                            Status = 1,
                            UserID = (long)item.SectionManagerUserID
                        });
                    }
                    if (item.DepartmentManagerUserID > 0)
                    {
                        lstAssignInsert.Add(new LMSModel.TblAssignmentLoan
                        {
                            CreateBy = item.CreateUserID,
                            CreateDate = item.CreateDate,
                            DateAssigned = item.ForDate,
                            DateApplyTo = item.ForDate.AddMonths(1).AddDays(-1),
                            LoanID = loanLMSInfo.LoanID,
                            ModifyDate = item.CreateDate,
                            Status = 1,
                            UserID = (long)item.DepartmentManagerUserID
                        });
                    }
                    //lstCutOffLoanInsert.Add(new LMSModel.TblCutOffLoan
                    //{
                    //    ConsultantShopID = (long)loanLMSInfo.ConsultantShopID,
                    //    CreateDate = item.CreateDate,
                    //    CustomerID = (long)loanLMSInfo.CustomerID,
                    //    CutOffDate = item.ForDate,
                    //    LenderID = (long)loanLMSInfo.LenderID,
                    //    OwnerShopID = (long)loanLMSInfo.OwnerShopID,
                    //    LoanID = loanLMSInfo.LoanID,
                    //    NextDate = item.NextDate,
                    //    TotalMoneyCurrent = item.TotalMoneyCurrent ?? 0
                    //});
                    countRows++;
                }
                if (lstAssignInsert.Count > 0)
                {
                    Console.WriteLine($"--- insert data assign {lstAssignInsert.Count} rows-----");
                    assignmentLoanTab.InsertBulk(lstAssignInsert);
                }
                if (lstCutOffLoanInsert.Count > 0)
                {
                    Console.WriteLine($"--- insert data cutoff {lstCutOffLoanInsert.Count} rows-----");
                    cutOffLoanTab.InsertBulk(lstCutOffLoanInsert);
                }
            }
            Console.WriteLine($"--- Finsished move data assign {countRows} rows-----");
        }
    }
}
