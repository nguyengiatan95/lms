﻿using LMS.Common.Constants;
using LMS.Common.DAL;
using LMS.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MigrateAGToLMS.Service
{
    public class LoanExtraManager
    {
        TableHelper<LMSModel.TblLoanExtra> loanExtraLMSTab;
        // Tab AG
        TableHelper<AG.TblLoanExtra> loanExtraAgTab;
        new TableHelper<LMSModel.TblLoan> loanLMSTab;
        TableHelper<AG.TblTransactionIndemnifyLoanInsurrance> loanExtraAgTransactionIndemnifyLoanInsurrance;

        TableHelper<LMSModel.TblLogCompare> logCompareTab;

        public LoanExtraManager()
        {
            loanExtraLMSTab = new TableHelper<LMSModel.TblLoanExtra>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            loanExtraAgTab = new TableHelper<AG.TblLoanExtra>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            loanLMSTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            loanExtraAgTransactionIndemnifyLoanInsurrance = new TableHelper<AG.TblTransactionIndemnifyLoanInsurrance>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            logCompareTab = new TableHelper<LMSModel.TblLogCompare>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
        }
        public long MoveData(List<int> loanIds = null)
        {
            Console.WriteLine($"-------------- TblLoanExtra -------------");
            //string sql = "truncate table TblLoanExtra";
            //loanExtraLMSTab.Execute(sql);
            int latestID = 0;
            var maxTimaLoanExtraID = loanExtraLMSTab.SelectColumns(x => x.TimaLoanExtraID)
                                                    .OrderByDescending(x => x.TimaLoanExtraID)
                                                    .SetGetTop(1)
                                                    .Query().FirstOrDefault()?.TimaLoanExtraID ?? 0;
            if (maxTimaLoanExtraID > 0)
            {
                latestID = (int)maxTimaLoanExtraID;
            }
            if (loanIds != null && loanIds.Count > 0)
            {
                loanIds = loanIds.OrderBy(x => x).ToList();
                latestID = 0;
            }

            List<LMSModel.TblLoanExtra> lstInsert = new List<LMSModel.TblLoanExtra>();
            List<long> lstLoanLmsIDDeleteExtra = new List<long>();
            int countResult = 0;
            bool setOffIdentity = true;
            try
            {
                long loanIdDeleteCurrent = 0;

                while (true)
                {
                    try
                    {
                        if (loanIds != null)
                        {
                            loanExtraAgTab.WhereClause(x => loanIds.Contains(x.LoanID));
                            setOffIdentity = false;
                        }
                        lstInsert.Clear();
                        lstLoanLmsIDDeleteExtra.Clear();
                        var lstDataAG = loanExtraAgTab.WhereClause(x => x.ID > latestID).OrderBy(x => x.ID).Query();
                        if (lstDataAG == null || !lstDataAG.Any())
                        {
                            break;
                        }
                        var lstLoanIds = lstDataAG.Select(x => x.LoanID).ToList();
                        var lstLoanLMS = loanLMSTab.SelectColumns(x => x.LoanID, x => x.TimaLoanID).WhereClause(x => lstLoanIds.Contains((int)x.TimaLoanID)).Query().ToDictionary(X => X.TimaLoanID, X => X.LoanID);
                        latestID = lstDataAG.OrderBy(x => x.ID).Last().ID;
                        foreach (var item in lstDataAG)
                        {
                            if (lstLoanLMS.ContainsKey(item.LoanID))
                            {
                                if (loanIds != null)
                                {
                                    var loanIDDeleted = lstLoanLMS.GetValueOrDefault(item.LoanID);
                                    if (loanIDDeleted > 0 && loanIDDeleted != loanIdDeleteCurrent)
                                    {
                                        loanIdDeleteCurrent = loanIDDeleted;
                                        //loanExtraLMSTab.DeleteWhere(x => x.LoanID == loanIDDeleted);
                                        lstLoanLmsIDDeleteExtra.Add(loanIdDeleteCurrent);
                                    }
                                }
                                lstInsert.Add(new LMSModel.TblLoanExtra()
                                {
                                    CreateBy = item.UserID,
                                    CreateDate = item.CreateDate.Value,
                                    LoanExtraID = item.ID,
                                    LoanID = lstLoanLMS.GetValueOrDefault(item.LoanID),
                                    ModifyDate = item.CreateDate.Value,
                                    PayDate = item.DateExtra,
                                    TimaLoanExtraID = item.ID,
                                    TimaLoanID = item.LoanID,
                                    TotalMoney = item.TotalMoney,
                                    TypeMoney = (int)LMS.Common.Constants.Transaction_TypeMoney.Original
                                });
                            }
                            countResult++;
                        }
                        if (lstLoanLmsIDDeleteExtra.Count > 0)
                        {
                            lstLoanLmsIDDeleteExtra = lstLoanLmsIDDeleteExtra.Distinct().ToList();
                            loanExtraLMSTab.DeleteWhere(x => lstLoanLmsIDDeleteExtra.Contains(x.LoanID));
                        }
                        if (lstInsert.Count > 0)
                        {
                            ////bool setOffIdentity = true;
                            //if (lstLoanIds.Count > 0)
                            //{
                            //    setOffIdentity = false;
                            //}
                            loanExtraLMSTab.InsertBulk(lstInsert, setOffIdentity: setOffIdentity);
                        }
                        Console.WriteLine($"Insert tblLoanExtra: {lstInsert.Count} success");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"move tblLoanExtra error|ex={ex.Message}-{ex.StackTrace}");
                        break;
                    }
                }
                Console.WriteLine($"Insert tblLoanExtra finished: {countResult} rows");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Insert tblLoanExtra error: {countResult} rows|ex={ex.Message}-{ex.StackTrace}");
            }
            return countResult;
        }

        public List<int> GetLoanIdsErrorData()
        {
            string query = $@"
                            select TimaLoanID from TblLoan where LoanID in (
                            SELECT T.LoanID FROM 
                            (
                            select LoanID,SUM(TotalMoney) TotalChange from TblLoanExtra group by LoanID
                            ) T INNER JOIN
                            (
                            select LoanID,TotalMoneyCurrent - TotalMoneyDisbursement as TotalChange from TblLoan where Status = 1 and TotalMoneyCurrent != TotalMoneyDisbursement
                            ) T2 ON  T.LoanID = T2.LoanID AND T.TotalChange != T2.TotalChange
                            )
                            ";
            var loans = loanLMSTab.Query(query, null);
            if (loans != null && loans.Any())
            {
                return loans.Select(x => (int)x.TimaLoanID).ToList();
            }
            return null;
        }

        public void RunTransactionIndemnifyLoan(List<long> loanIds = null)
        {
            Console.WriteLine($"-------------- TblLoanExtra RunTransactionIndemnifyLoan-------------");
            //string sql = "truncate table TblLoanExtra";
            //loanExtraLMSTab.Execute(sql);
            int latestID = 0;
            var dataListInsuranceMax = loanExtraLMSTab.SelectColumns(x => x.TransactionIndemnifyLoanInsurranceID)
                                                    .OrderByDescending(x => x.TransactionIndemnifyLoanInsurranceID)
                                                    .SetGetTop(1)
                                                    .Query();
            var maxTimaLoanExtraID = dataListInsuranceMax == null ? 0 : dataListInsuranceMax.FirstOrDefault()?.TransactionIndemnifyLoanInsurranceID ?? 0;
            if (maxTimaLoanExtraID > 0)
            {
                latestID = (int)maxTimaLoanExtraID;
            }
            if (loanIds != null && loanIds.Count > 0)
            {
                loanIds = loanIds.OrderBy(x => x).ToList();
                latestID = 0;
            }

            List<LMSModel.TblLoanExtra> lstInsert = new List<LMSModel.TblLoanExtra>();
            int countResult = 0;
            try
            {
                long loanIdDeleteCurrent = 0;
                while (true)
                {
                    try
                    {
                        if (loanIds != null)
                        {
                            loanExtraAgTransactionIndemnifyLoanInsurrance.WhereClause(x => loanIds.Contains(x.LoanID));
                        }
                        lstInsert.Clear();
                        var lstDataAG = loanExtraAgTransactionIndemnifyLoanInsurrance.SetGetTop(2000).WhereClause(x => x.TransactionIndemnifyLoanInsurranceID > latestID).OrderBy(x => x.TransactionIndemnifyLoanInsurranceID).Query();
                        if (lstDataAG == null || !lstDataAG.Any())
                        {
                            break;
                        }
                        var lstLoanIds = lstDataAG.Select(x => x.LoanID).ToList();
                        var lstLoanLMS = loanLMSTab.SelectColumns(x => x.LoanID, x => x.TimaLoanID).WhereClause(x => lstLoanIds.Contains((int)x.TimaLoanID)).Query().ToDictionary(X => X.TimaLoanID, X => X.LoanID);
                        latestID = (int)lstDataAG.OrderBy(x => x.TransactionIndemnifyLoanInsurranceID).Last().TransactionIndemnifyLoanInsurranceID;
                        foreach (var item in lstDataAG)
                        {
                            if (lstLoanLMS.ContainsKey(item.LoanID))
                            {
                                if (loanIds != null)
                                {
                                    var loanIDDeleted = lstLoanLMS.GetValueOrDefault(item.LoanID);
                                    if (loanIDDeleted > 0 && loanIDDeleted != loanIdDeleteCurrent)
                                    {
                                        loanIdDeleteCurrent = loanIDDeleted;
                                        loanExtraLMSTab.DeleteWhere(x => x.LoanID == loanIDDeleted && x.TransactionIndemnifyLoanInsurranceID > 0);
                                    }
                                }
                                lstInsert.Add(new LMSModel.TblLoanExtra()
                                {
                                    CreateBy = item.UserID,
                                    CreateDate = item.CreateDate,
                                    LoanID = lstLoanLMS.GetValueOrDefault(item.LoanID),
                                    ModifyDate = item.CreateDate,
                                    PayDate = item.CreateDate,
                                    TimaLoanID = item.LoanID,
                                    TotalMoney = item.TotalMoney,
                                    TypeMoney = (int)LMS.Common.Constants.Transaction_TypeMoney.Original,
                                    TransactionIndemnifyLoanInsurranceID = item.TransactionIndemnifyLoanInsurranceID
                                });
                            }
                            countResult++;
                        }
                        if (lstInsert.Count > 0)
                        {
                            loanExtraLMSTab.InsertBulk(lstInsert, setOffIdentity: false);
                        }
                        Console.WriteLine($"Insert tblLoanExtra RunTransactionIndemnifyLoan {lstInsert.Count} success");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"move tblLoanExtra RunTransactionIndemnifyLoan error|ex={ex.Message}-{ex.StackTrace}");
                        break;
                    }
                }
                Console.WriteLine($"Insert tblLoanExtra  RunTransactionIndemnifyLoan finished: {countResult} rows");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Insert tblLoanExtra RunTransactionIndemnifyLoan error: {countResult} rows|ex={ex.Message}-{ex.StackTrace}");
            }
        }

        public void CompareLoanExtra()
        {
            try
            {
                Console.WriteLine($"--------------  COMPARE LOAN EXTRA --------------");
                // lấy danh sách đơn vay từ lms
                var lstLoanLmsInfos = loanLMSTab.SelectColumns(x => x.TimaLoanID, x => x.LoanID).WhereClause(x => x.Status == 1).Query();
                Dictionary<long, long> dictLoanIDLmsCurrent = new Dictionary<long, long>();
                Dictionary<long, long> dictLoanIDAgCurrent = new Dictionary<long, long>();
                foreach (var item in lstLoanLmsInfos)
                {
                    if (!dictLoanIDAgCurrent.ContainsKey(item.TimaLoanID ?? 0))
                    {
                        dictLoanIDAgCurrent.Add((long)item.TimaLoanID, item.LoanID);
                    }
                    else
                    {
                        Console.WriteLine($"LoanTimaID trung item.TimaLoanID={item.TimaLoanID} - {DateTime.Now}");
                    }
                    if (!dictLoanIDLmsCurrent.ContainsKey(item.LoanID))
                    {
                        dictLoanIDLmsCurrent.Add(item.LoanID, item.TimaLoanID.Value);
                    }
                    else
                    {
                        Console.WriteLine($"LoanID lms trung item.LoanID={item.LoanID} - {DateTime.Now}");
                    }
                }
                var lstLoanExtraLmsTask = Task.Run(() =>
                {
                    var lstBatch = dictLoanIDLmsCurrent.Keys.Batch(1000);
                    Dictionary<long, List<long>> dictExtraInfos = new Dictionary<long, List<long>>();
                    foreach (var bat in lstBatch)
                    {
                        var dictQuery = loanExtraLMSTab.WhereClause(x => bat.Contains(x.LoanID)).Query().GroupBy(x => x.LoanID).ToDictionary(x => x.Key, x => x.Select(x => x.TotalMoney).ToList());
                        foreach (var item in dictQuery)
                        {
                            if (!dictExtraInfos.ContainsKey(item.Key))
                            {
                                dictExtraInfos.Add(item.Key, item.Value);
                            }
                            else
                            {
                                Console.WriteLine($"LoanLMS extra trung key LoanID={item.Key} - {DateTime.Now}");
                            }
                        }
                    }
                    return dictExtraInfos;
                });
                var lstLoanExtraAgTask = Task.Run(() =>
                {
                    var lstBatch = dictLoanIDAgCurrent.Keys.Batch(1000);
                    Dictionary<long, List<long>> dictExtraInfos = new Dictionary<long, List<long>>();
                    foreach (var bat in lstBatch)
                    {
                        var dictQuery = loanExtraAgTab.WhereClause(x => bat.Contains(x.LoanID)).Query().GroupBy(x => x.LoanID).ToDictionary(x => x.Key, x => x.Select(x => x.TotalMoney).ToList());
                        foreach (var item in dictQuery)
                        {
                            if (!dictExtraInfos.ContainsKey(item.Key))
                            {
                                dictExtraInfos.Add(item.Key, item.Value);
                            }
                            else
                            {
                                Console.WriteLine($"LoanAG extra trung key LoanID={item.Key} - {DateTime.Now}");
                            }
                        }
                    }
                    return dictExtraInfos;
                });
                Task.WaitAll(lstLoanExtraLmsTask, lstLoanExtraAgTask);
                List<LMSModel.TblLogCompare> lstLog = new List<LMSModel.TblLogCompare>();
                // lấy ag làm dữ liệu gốc
                var dictLoanExtraAGInfos = lstLoanExtraAgTask.Result;
                var dictLoanExtraLmsInfos = lstLoanExtraLmsTask.Result;
                string close_loan = LogCompare_DetailType.Loan_OrginalExtra.GetDescription();
                int totalLoanTima = 0;
                foreach (var item in lstLoanLmsInfos)
                {
                    var lstLoanExAg = dictLoanExtraAGInfos.GetValueOrDefault(item.TimaLoanID ?? 0);
                    var lstLoanExLms = dictLoanExtraLmsInfos.GetValueOrDefault(item.LoanID);
                    if (lstLoanExAg != null && (lstLoanExLms == null || lstLoanExLms.Count != lstLoanExAg.Count))
                    {
                        Console.WriteLine($"[Info] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} LoanAG = {item.TimaLoanID} - {lstLoanExAg.Count}| LoanLMS={item.LoanID} - {lstLoanExLms?.Count ?? 0}");
                        totalLoanTima++;
                        close_loan += $"{item.TimaLoanID};";
                    }
                }
                var logInsert = SetlogCompare((int)LogCompare_MainType.LoanExtra, (int)LogCompare_DetailType.Loan_OrginalExtra, 0, totalLoanTima, close_loan);
                logCompareTab.DeleteWhere(x => x.Status == 1 && x.ForDate == DateTime.Now.Date && x.MainType == (int)LogCompare_MainType.LoanExtra);
                logCompareTab.Insert(logInsert);

                Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert compare Loan extra hoan tat");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"CompareLoanExtra exception ex={ex.Message}-{ex.StackTrace}");
            }
        }
        public LMSModel.TblLogCompare SetlogCompare(int mainType, int detailType, long totalAG, long totalLMS, string note = "")
        {
            return new LMSModel.TblLogCompare()
            {
                CreateDate = DateTime.Now,
                DetailType = detailType,
                DetailNote = string.IsNullOrWhiteSpace(note) ? ((LogCompare_DetailType)detailType).GetDescription() : note,
                ForDate = DateTime.Now.Date,
                MainType = mainType,
                Status = 1,
                TotalAG = totalAG,
                TotalLMS = totalLMS
            };

        }
    }
}
