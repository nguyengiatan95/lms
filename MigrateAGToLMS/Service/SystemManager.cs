﻿using LMS.Common.Constants;
using LMS.Common.DAL;
using MigrateAGToLMS.AG;
using MigrateAGToLMS.LMSModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MigrateAGToLMS.Service
{
    public class SystemManager
    {
        TableHelper<TblCommentDebtPrompted> _commentLMSTab;
        TableHelper<TblCommentDebtPromptedAG> _commentAGTab;

        TableHelper<TblProvisionsCollection> _provisionsCollectionAGTab;
        TableHelper<TblPreparationCollectionLoan> _preparationCollectionLoanLMSTab;

        TableHelper<TblHoldCustomerMoneyAG> _holdCustomerMoneyAGTab;
        TableHelper<TblHoldCustomerMoney> _holdCustomerMoneyLMSTab;


        TableHelper<TblSmsAnalyticsAG> _smsAGTab;
        TableHelper<TblSmsAnalytics> _smsLMSTab;

        TableHelper<TblLogCompare> _logCompareTab;
        LenderManager lenderManager;
        LMS.Common.Helper.Utils _common;

        public SystemManager()
        {
            _commentLMSTab = new TableHelper<TblCommentDebtPrompted>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            _commentAGTab = new TableHelper<TblCommentDebtPromptedAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            _logCompareTab = new TableHelper<TblLogCompare>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            _preparationCollectionLoanLMSTab = new TableHelper<TblPreparationCollectionLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            _provisionsCollectionAGTab = new TableHelper<TblProvisionsCollection>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            _holdCustomerMoneyLMSTab = new TableHelper<TblHoldCustomerMoney>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            _holdCustomerMoneyAGTab = new TableHelper<TblHoldCustomerMoneyAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            _smsAGTab = new TableHelper<TblSmsAnalyticsAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            _smsLMSTab = new TableHelper<TblSmsAnalytics>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            _common = new LMS.Common.Helper.Utils();
            lenderManager = new LenderManager();
        }

        public void CompareComment()
        {
            Console.WriteLine($"-------------- CompareComment --------------");
            var commentLMS = Task.Run(() =>
            {
                return _commentLMSTab.SelectColumns(x => x.CreateDate, x => x.TimaCommentID, x => x.CommentDebtPromptedID).Query();
            });

            var commentAG = Task.Run(() =>
            {
                return _commentAGTab.SelectColumns(x => x.CreateDate, x => x.LMSCommentID, x => x.Id).Query();
            });
            Task.WaitAll(commentLMS, commentAG);
            var lstCommentLMS = commentLMS.Result;
            var lstAG = commentAG.Result;
            List<LMSModel.TblLogCompare> lstLog = new List<LMSModel.TblLogCompare>();

            long totalCommentAG = lstAG.Count();
            long totalCommentLMS = lstCommentLMS.Count();

            long totalCommentTodayLMS = lstCommentLMS.Where(x => x.CreateDate >= DateTime.Now.Date).Count();
            long totalCommentTodayAG = lstAG.Where(x => x.CreateDate >= DateTime.Now.Date).Count();

            var lstCommentLMSIds = lstCommentLMS.Where(x => x.CreateDate >= DateTime.Now.Date && x.TimaCommentID == 0).Select(x => x.CommentDebtPromptedID).ToList();
            var lstLMSIds = lstAG.Where(x => x.CreateDate >= DateTime.Now.Date && x.LMSCommentID > 0).Select(x => x.LMSCommentID).ToList();
            var lstMiss = lstCommentLMSIds.Where(x => !lstLMSIds.Contains(x)).ToList();

            lstLog.Add(lenderManager.SetlogCompare(
                        (int)LogCompare_MainType.Comment, (int)LogCompare_DetailType.Comment_ALL, totalCommentAG
                        , totalCommentLMS));
            var ids = _common.ConvertObjectToJSonV2(lstMiss);
            lstLog.Add(lenderManager.SetlogCompare(
                       (int)LogCompare_MainType.Comment, (int)LogCompare_DetailType.Comment_Today, totalCommentTodayAG
                       , totalCommentTodayLMS));
            List<int> mainType = new List<int>() { (int)LogCompare_MainType.Comment };
            _logCompareTab.DeleteWhere(x => x.Status == 1 && x.ForDate == DateTime.Now.Date && mainType.Contains(x.MainType));
            _logCompareTab.InsertBulk(lstLog);
            Console.WriteLine($"--------------DONE CompareComment --------------");

        }

        public void ComparePreparationCollection()
        {
            Console.WriteLine($"-------------- ComparePreparationCollection --------------");
            var prepareLMS = Task.Run(() =>
            {
                return _preparationCollectionLoanLMSTab.WhereClause(x => x.Status == 1).SelectColumns(x => x.CreateDate, x => x.TotalMoney).Query();
            });

            var prepareAG = Task.Run(() =>
            {
                return _provisionsCollectionAGTab.WhereClause(x => x.Status == 1).SelectColumns(x => x.CreateDate, x => x.Money).Query();
            });
            Task.WaitAll(prepareLMS, prepareAG);
            var lstLMS = prepareLMS.Result;
            var lstAG = prepareAG.Result;
            List<LMSModel.TblLogCompare> lstLog = new List<LMSModel.TblLogCompare>();

            long totalAG = lstAG.Count();
            long totalLMS = lstLMS.Count();

            long totalTodayLMS = lstLMS.Where(x => x.CreateDate >= DateTime.Now.Date).Count();
            long totalTodayAG = lstAG.Where(x => x.CreateDate >= DateTime.Now.Date).Count();

            long totalMoneyTodayAG = (long)lstAG.Where(x => x.CreateDate >= DateTime.Now.Date).Sum(x => x.Money);
            long totalMoneyTodayLMS = (long)lstLMS.Where(x => x.CreateDate >= DateTime.Now.Date).Sum(x => x.TotalMoney);



            lstLog.Add(lenderManager.SetlogCompare(
                        (int)LogCompare_MainType.PrepareLoan, (int)LogCompare_DetailType.Prepare_ALL, totalAG
                        , totalLMS));
            lstLog.Add(lenderManager.SetlogCompare(
                       (int)LogCompare_MainType.PrepareLoan, (int)LogCompare_DetailType.Prepare_Today, totalTodayAG
                       , totalTodayLMS));
            lstLog.Add(lenderManager.SetlogCompare(
                      (int)LogCompare_MainType.PrepareLoan, (int)LogCompare_DetailType.Prepare_Money_Today, totalMoneyTodayAG
                      , totalMoneyTodayLMS));
            List<int> mainType = new List<int>() { (int)LogCompare_MainType.PrepareLoan };
            _logCompareTab.DeleteWhere(x => x.Status == 1 && x.ForDate == DateTime.Now.Date && mainType.Contains(x.MainType));
            _logCompareTab.InsertBulk(lstLog);
            Console.WriteLine($"--------------DONE ComparePreparationCollection --------------");
        }


        public void CompareHoldMoney()
        {
            Console.WriteLine($"-------------- CompareHoldMoney --------------");
            var taskLMS = Task.Run(() =>
            {
                return _holdCustomerMoneyLMSTab.WhereClause(x => x.Status == 1).SelectColumns(x => x.CreateDate, x => x.Amount).Query();
            });

            var taskAG = Task.Run(() =>
            {
                return _holdCustomerMoneyAGTab.WhereClause(x => x.Status == 1).SelectColumns(x => x.CreateDate, x => x.Amount).Query();
            });
            Task.WaitAll(taskLMS, taskAG);
            var lstLMS = taskLMS.Result;
            var lstAG = taskAG.Result;
            List<LMSModel.TblLogCompare> lstLog = new List<LMSModel.TblLogCompare>();

            long totalAG = lstAG.Count();
            long totalLMS = lstLMS.Count();

            long totalTodayLMS = lstLMS.Where(x => x.CreateDate >= DateTime.Now.Date).Count();
            long totalTodayAG = lstAG.Where(x => x.CreateDate >= DateTime.Now.Date).Count();

            long totalMoneyTodayAG = (long)lstAG.Where(x => x.CreateDate >= DateTime.Now.Date).Sum(x => x.Amount);
            long totalMoneyTodayLMS = (long)lstLMS.Where(x => x.CreateDate >= DateTime.Now.Date).Sum(x => x.Amount);



            lstLog.Add(lenderManager.SetlogCompare(
                        (int)LogCompare_MainType.HoldMoney, (int)LogCompare_DetailType.HoldMoney_ALL, totalAG
                        , totalLMS));
            lstLog.Add(lenderManager.SetlogCompare(
                       (int)LogCompare_MainType.HoldMoney, (int)LogCompare_DetailType.HoldMoney_Today, totalTodayAG
                       , totalTodayLMS));
            lstLog.Add(lenderManager.SetlogCompare(
                      (int)LogCompare_MainType.HoldMoney, (int)LogCompare_DetailType.HoldMoney_Money_Today, totalMoneyTodayAG
                      , totalMoneyTodayLMS));
            List<int> mainType = new List<int>() { (int)LogCompare_MainType.HoldMoney };
            _logCompareTab.DeleteWhere(x => x.Status == 1 && x.ForDate == DateTime.Now.Date && mainType.Contains(x.MainType));
            _logCompareTab.InsertBulk(lstLog);
            Console.WriteLine($"--------------DONE CompareHoldMoney --------------");
        }

        public void CompareSMS()
        {
            Console.WriteLine($"-------------- SMS --------------");
            var taskLMS = Task.Run(() =>
            {
                return _smsLMSTab.SelectColumns(x => x.CreateDate, x => x.BankCardID).Query();
            });

            var taskAG = Task.Run(() =>
            {
                return _smsAGTab.SelectColumns(x => x.CreateDate, x => x.BankCardId).Query();
            });
            Task.WaitAll(taskLMS, taskAG);
            var lstLMS = taskLMS.Result;
            var lstAG = taskAG.Result;
            List<LMSModel.TblLogCompare> lstLog = new List<LMSModel.TblLogCompare>();

            long totalAG = lstAG.Count();
            long totalLMS = lstLMS.Count();

            long totalTodayLMS = lstLMS.Where(x => x.CreateDate >= DateTime.Now.Date).Count();
            long totalTodayAG = lstAG.Where(x => x.CreateDate >= DateTime.Now.Date).Count();

            lstLog.Add(lenderManager.SetlogCompare(
                        (int)LogCompare_MainType.SMS, (int)LogCompare_DetailType.SMS_ALL, totalAG
                        , totalLMS));
            lstLog.Add(lenderManager.SetlogCompare(
                       (int)LogCompare_MainType.SMS, (int)LogCompare_DetailType.SMS_Today, totalTodayAG
                       , totalTodayLMS));
            List<int> mainType = new List<int>() { (int)LogCompare_MainType.SMS };
            _logCompareTab.DeleteWhere(x => x.Status == 1 && x.ForDate == DateTime.Now.Date && mainType.Contains(x.MainType));
            _logCompareTab.InsertBulk(lstLog);
            Console.WriteLine($"--------------DONE SMS --------------");
        }

        public void MoveSMS()
        {
            long maxTimaCommentID = 0;
            int maxRowGet = 2000;
            try
            {
                Console.WriteLine($"-------------- Move SMS --------------");
                DateTime dtNow = DateTime.Now;
                var lstMaxTimacommentID = _smsLMSTab.SelectColumns(x => x.SmsTimaID)
                                                         .OrderByDescending(x => x.SmsTimaID)
                                                         .WhereClause(x => x.SmsTimaID > 0).SetGetTop(1).Query();
                if (lstMaxTimacommentID != null && lstMaxTimacommentID.Any())
                {
                    maxTimaCommentID = lstMaxTimacommentID.First().SmsTimaID.Value;
                }
                while (true)
                {
                    var lstSMSAG = _smsAGTab.WhereClause(x => x.Id > maxTimaCommentID).OrderBy(x => x.Id).SetGetTop(maxRowGet).Query();
                    // user chưa insert 
                    var lstNeedInsert = lstSMSAG.OrderBy(x => x.Id).ToList();
                    if (lstNeedInsert.Count > 0)
                    {
                        List<LMSModel.TblSmsAnalytics> lstData = new List<LMSModel.TblSmsAnalytics>();
                        foreach (var item in lstNeedInsert)
                        {
                            lstData.Add(new LMSModel.TblSmsAnalytics()
                            {
                                AcountNumber = item.AcountNumber,
                                BankCardAliasName = item.BankCardAliasName,
                                SmsTimaID = item.Id,
                                BankCardID = item.BankCardId ?? 0,
                                ContentCancel = item.ContentCancel,
                                ContractCode = "TC-" + item.LoanCodeId,
                                LoanCodeID = item.LoanCodeId,
                                CreateDate = item.CreateDate.Value,
                                CurrentTotalMoney = item.CurrentTotalMoney ?? 0,
                                CustomerID = item.CustomerId ?? 0,
                                CustomerName = item.CustomerName,
                                CuttingOffDebtType = item.CuttingOffDebtType ?? 0,
                                IdCard = item.IdCard,
                                IncreaseMoney = item.IncreaseMoney,
                                IncreaseTime = item.IncreaseTime,
                                IsAnalytics = item.IsAnalytics,
                                LoanID = item.LoanId ?? 0,
                                ModifyDate = item.ModifyDate,
                                NameTransactionBank = item.NameTransactionBank,
                                Phone = item.Phone,
                                ReasonIdCancel = item.ReasonIdCancel,
                                Sender = item.Sender,
                                ShopID = item.ShopID,
                                ShopName = item.ShopName,
                                SmsContent = item.SmsContent,
                                SmsreceivedDate = item.SmsreceivedDate,
                                SmsContentHashIndex = item.SmsContentHashIndex,
                                SmsType = item.SmsType,
                                Status = item.Status ?? 0,
                                TransactionBankCardId = item.TransactionBankCardId,
                                TransactionBankCardStatus = item.TransactionBankCardStatus,
                                TypeTransactionBank = item.TypeTransactionBank,
                            });
                            maxTimaCommentID = item.Id;
                        }
                        Console.WriteLine($"[Inserting] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} bat dau insert SMS");
                        _smsLMSTab.InsertBulk(lstData);
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert SMS  -  {lstData.Count()} ");
                    }
                    else
                    {
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert SMS hoan tat|maxTimaCommentID={maxTimaCommentID}");
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"[error] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} loi roi|maxTimaCommentI={maxTimaCommentID}|ex={ex.Message}-{ex.StackTrace}");
            }

        }


    }
}
