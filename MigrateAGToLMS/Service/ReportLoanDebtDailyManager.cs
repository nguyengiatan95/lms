﻿using LMS.Common.Constants;
using LMS.Common.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigrateAGToLMS.Service
{
    public class ReportLoanDebtDailyManager
    {
        public void UpdateSourceBuyInsurance()
        {
            Console.WriteLine($"--------------[{DateTime.Now}] UpdateSourceBuyInsurance -------------- ");
            var loanLMSTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var lenderLMSTab = new TableHelper<LMSModel.TblLender>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var reportLoanDebtTab = new TableHelper<LMSModel.TblReportLoanDebtDaily>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            long latestID = 0;
            int top = 1000;
            int count = 0;
            var dictShopInfos = lenderLMSTab.SelectColumns(x => x.LenderID, x => x.FullName).WhereClause(x => x.TimaLenderID == 3703 || x.IsHub == 1).Query().ToDictionary(x => x.LenderID, x => x);
            while (true)
            {
                var lstDataReport = reportLoanDebtTab.SetGetTop(top).WhereClause(x => x.ReportLoanDebtDailyID > latestID).OrderBy(x => x.ReportLoanDebtDailyID).Query();
                var lstLoanID = lstDataReport.Select(x => x.LoanID).ToList();
                var dictLoanInfos = loanLMSTab.SelectColumns(x => x.LoanID, x => x.OwnerShopID, x => x.ConsultantShopID, x => x.SourcecBuyInsurance)
                                            .WhereClause(x => lstLoanID.Contains(x.LoanID))
                                            .Query().ToDictionary(x => x.LoanID, x => x);
                if (lstDataReport == null || !lstDataReport.Any())
                {
                    break;
                }
                latestID = lstDataReport.OrderBy(x => x.ReportLoanDebtDailyID).Last().ReportLoanDebtDailyID;
                foreach (var item in lstDataReport)
                {
                    var loanInfo = dictLoanInfos.GetValueOrDefault(item.LoanID);
                    if (loanInfo == null)
                    {
                        Console.WriteLine($"khong tim thay loan ID = {item.LoanID}");
                        continue;
                    }
                    item.SourceBuyInsurance = loanInfo.SourcecBuyInsurance.GetValueOrDefault();
                    item.OwnerShopID = loanInfo.OwnerShopID.GetValueOrDefault();
                    item.ConsultantShopID = loanInfo.ConsultantShopID.GetValueOrDefault();

                    var consultanShopInfo = dictShopInfos.GetValueOrDefault(item.ConsultantShopID);
                    if (consultanShopInfo == null)
                    {
                        Console.WriteLine($"khong tim thay shop tu van loan ID = {item.LoanID}| shopid = {item.ConsultantShopID}");
                    }
                    item.OwnerShopName = dictShopInfos.GetValueOrDefault(item.OwnerShopID)?.FullName;
                    item.ConsultantShopName = consultanShopInfo?.FullName;
                    item.TypeDebt = (int)GetTypeBebtLoanByNextDate_v2(item.DPD);
                }
                reportLoanDebtTab.UpdateBulk(lstDataReport);
                Console.WriteLine($"--------------[{DateTime.Now}] Xong {count++}-------------- ");
            }

            Console.WriteLine($"--------------[{DateTime.Now}] UpdateSourceBuyInsurance Done-------------- ");
        }

        public static THN_DPD GetTypeBebtLoanByNextDate_v2(int dpdCurrent)
        {
            // có 15 nhóm từ B0 - > B13+
            // bước nhảy 30 ngày
            if (dpdCurrent < 1)
            {
                return THN_DPD.B0;
            }
            int i = dpdCurrent / 30;
            if (i > 13)
            {
                return THN_DPD.B14;
            }
            int j = dpdCurrent % 30;
            if (j == 0)
            {
                return (THN_DPD)i;
            }
            if (i + 1 > 13)
            {
                return THN_DPD.B14;
            }
            return (THN_DPD)(i + 1);
        }
    }
}
