﻿using LMS.Common.DAL;
using MigrateAGToLMS.LMSModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MigrateAGToLMS.Service
{
    public class TrackingLoanInMonthManager
    {
        public void CallAPITracking()
        {
            TableHelper<TblSettingKey> settingTab = new TableHelper<TblSettingKey>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var client = new RestClient("http://localhost:57585/api/Report/ProcessReportLoanDebtType");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            //request.AddHeader("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiMSIsIklkZW50aXR5X1VzZXJJRCI6IjEiLCJuYmYiOjE2Mzc1NDk4NjksImV4cCI6MTYzNzU5MzA2OSwiaWF0IjoxNjM3NTQ5ODY5fQ.Q6GmcDiWCjm7Xi5sWEP9SPsmyGj581jg9k6qsSwRiTM");
            request.AddParameter("text/plain", "", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                for (int i = 0; i < 100; i++)
                {
                   
                    Thread.Sleep(i * 500);
                    var setting = settingTab.WhereClause(x => x.SettingKeyID == 13).Query();
                    if (setting.FirstOrDefault().Status == 1)
                    {
                        Console.WriteLine("setting.Value:" + setting.FirstOrDefault().Value);
                        CallAPITracking();
                        break;
                    }
                }
            }
            else
            {
                Console.WriteLine("Loi roi");
            }
        }



    }
}
