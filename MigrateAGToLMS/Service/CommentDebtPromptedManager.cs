﻿using LMS.Common.DAL;
using MigrateAGToLMS.LMSModel;
using Org.BouncyCastle.Math.EC.Rfc7748;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigrateAGToLMS.Service
{
    public class CommentDebtPromptedManager
    {
        public void MoveCommentDebtPrompted(bool update = false)
        {
            try
            {
                Console.WriteLine($"-------------- CommentDebtPromptedManager -------------");

                DateTime dtNow = DateTime.Now;
                var commentLMSTab = new TableHelper<LMSModel.TblCommentDebtPrompted>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                var commentTimaTab = new TableHelper<AG.TblCommentDebtPromptedAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                var commentFileAG = new TableHelper<AG.TblThnFiles>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                LMS.Common.Helper.Utils common = new LMS.Common.Helper.Utils();
                var loanTabLMS = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                var lstMaxTimacommentID = commentLMSTab.SelectColumns(x => x.TimaCommentID)
                                                         .OrderByDescending(x => x.TimaCommentID).
                                                         SetGetTop(1).Query().FirstOrDefault()?.TimaCommentID ?? 0;
                long maxTimaCommentID = lstMaxTimacommentID;
                int maxRowGet = 2000;
                if (update)
                {
                    maxTimaCommentID = 0;
                }
                while (true)
                {
                    var lstCommentAG = commentTimaTab.WhereClause(x => x.Id > maxTimaCommentID).OrderBy(x => x.Id).SetGetTop(maxRowGet).Query();
                    if (lstCommentAG == null || !lstCommentAG.Any())
                    {
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert CommentDebtPromptedManager hoan tat maxTimaCommentID =" + maxTimaCommentID);
                        break;
                    }
                    // user chưa insert 
                    var lstCommentNeedInsert = lstCommentAG.OrderBy(x => x.Id).ToList();

                    var lstIds = lstCommentNeedInsert.Select(x => x.Id).ToList();
                    var lstCommentInLMS = commentLMSTab.WhereClause(x => lstIds.Contains((int)x.TimaCommentID)).SelectColumns(x => x.TimaCommentID, x => x.CommentDebtPromptedID)
                        .Query();
                    maxTimaCommentID = lstIds.Max();
                    if (lstCommentNeedInsert.Count > 0)
                    {
                        var lstCommentInLMSID = lstCommentInLMS.Select(x => x.TimaCommentID).ToList();

                        var lstcommentIds = lstCommentNeedInsert.Select(x => x.Id).ToList();

                        var lstCommentFile = commentFileAG.WhereClause(x => lstcommentIds.Contains((int)x.CommentID)).Query();

                        var lstLoanIds = lstCommentNeedInsert.Select(x => x.LoanId).ToList();

                        var dicLoan = loanTabLMS.SelectColumns(x => x.LoanID, x => x.TimaLoanID)
                            .WhereClause(x => lstLoanIds.Contains((int)x.TimaLoanID))
                            .Query().ToDictionary(x => x.TimaLoanID, x => x.LoanID);
                        List<LMSModel.TblCommentDebtPrompted> lstComment = new List<LMSModel.TblCommentDebtPrompted>();
                        List<LMSModel.TblCommentDebtPrompted> lstUpdate = new List<LMSModel.TblCommentDebtPrompted>();

                        foreach (var item in lstCommentNeedInsert)
                        {
                            try
                            {
                                var commentLMS = new LMSModel.TblCommentDebtPrompted()
                                {
                                    Comment = item.Comment,
                                    CreateDate = item.CreateDate,
                                    FullName = item.FullName,
                                    IsDisplay = 1,
                                    LoanID = dicLoan.ContainsKey(item.LoanId) ? dicLoan[item.LoanId] : 0,
                                    ReasonCode = item.C_ReasonCode,
                                    ReasonCodeID = item.ReasonId,
                                    TimaCommentID = item.Id,
                                    UserID = item.UserId,
                                    TimaLoanID = item.LoanId ?? 0,
                                    AppointmentDate = item.NextDate
                                    //CommentDebtPromptedID = item.Id
                                };

                                if (lstCommentFile.Where(x => x.CommentID == item.Id).Count() > 0)
                                {
                                    var lstImage = new List<ImageLosUpload>();
                                    var lstFiles = lstCommentFile.Where(x => x.CommentID == item.Id).ToList();
                                    foreach (var files in lstFiles)
                                    {
                                        lstImage.Add(new ImageLosUpload()
                                        {
                                            FileName = files.FilePath,
                                            FullPath = "https://thn.tima.vn/FileUpload" + files.FilePath,
                                            S3Path = "http://file.tima.vn",
                                            LocalPath = "https://thn.tima.vn/FileUpload",
                                            Source = "AG"
                                        });
                                    }
                                    commentLMS.FileImage = common.ConvertObjectToJSonV2(lstImage);
                                }

                                if (!lstCommentInLMSID.Contains(item.Id))
                                {
                                    lstComment.Add(commentLMS);
                                }
                                else
                                {
                                    commentLMS.CommentDebtPromptedID = lstCommentInLMS.Where(x => x.TimaCommentID == item.Id).First().CommentDebtPromptedID;
                                    lstUpdate.Add(commentLMS);
                                }
                            }
                            catch (Exception)
                            {

                            }

                        }
                        Console.WriteLine($"[Inserting] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} bat dau insert CommentDebtPromptedManager");
                        try
                        {
                            if (lstComment.Count > 0)
                            {
                                commentLMSTab.InsertBulk(lstComment);
                                Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert CommentDebtPromptedManager  -  {lstComment.Count()} ");
                            }
                            if (lstUpdate.Count > 0)
                            {
                                commentLMSTab.UpdateBulk(lstUpdate);
                                Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} update CommentDebtPromptedManager  -  {lstUpdate.Count()} ");
                            }
                        }
                        catch (Exception)
                        {
                        }
                        //commentLMSTab.UpdateBulk(lstComment);
                    }
                    else
                    {
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert CommentDebtPromptedManager hoan tat maxTimaCommentID =" + maxTimaCommentID);
                        //break;
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine($"CommentDebtPromptedManager - ERROR {ex.Message}");
            }
        }

        public void MoveImageDebtPrompted()
        {
            try
            {
                Console.WriteLine($"-------------- ImageManager -------------");

                DateTime dtNow = DateTime.Now;
                var commentLMSTab = new TableHelper<LMSModel.TblCommentDebtPrompted>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                var commentTimaTab = new TableHelper<AG.TblCommentDebtPromptedAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                var commentFileAG = new TableHelper<AG.TblThnFiles>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                LMS.Common.Helper.Utils common = new LMS.Common.Helper.Utils();
                var loanTabLMS = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                //var lstMaxTimacommentID = commentLMSTab.SelectColumns(x => x.TimaCommentID)
                //                                         .OrderByDescending(x => x.TimaCommentID)
                //                                         .WhereClause(x => x.TimaCommentID > 0 && x.TimaCommentID < 1335855).SetGetTop(1).Query();
                long maxTimaCommentID = 0;
                int maxRowGet = 2000;
                //if (lstMaxTimacommentID != null && lstMaxTimacommentID.Any())
                //{
                maxTimaCommentID = 275450;
                //}
                var lstCommentImageID = commentFileAG.SelectColumns(x => x.CommentID).Query().Select(x => x.CommentID).Distinct().OrderBy(x => x).ToList();
                while (true)
                {
                    var lstCommentImageIDChild = lstCommentImageID.Where(x => x > maxTimaCommentID).Take(maxRowGet).ToList();

                    var lstBankCardAG = commentTimaTab.WhereClause(x => x.Id > maxTimaCommentID && x.Id < 1335856 && lstCommentImageIDChild.Contains((int)x.Id))
                     .OrderBy(x => x.Id).SetGetTop(maxRowGet).Query();
                    // user chưa insert 
                    var lstCommentNeedInsert = lstBankCardAG.OrderBy(x => x.Id).ToList();
                    if (lstCommentNeedInsert.Count > 0)
                    {
                        var lstcommentIds = lstCommentNeedInsert.Select(x => x.Id).ToList();

                        var lstCommentFile = commentFileAG.WhereClause(x => lstcommentIds.Contains((int)x.CommentID)).Query();
                        var lstLoanIds = lstCommentNeedInsert.Select(x => x.LoanId).ToList();
                        var dicLoan = loanTabLMS.SelectColumns(x => x.LoanID, x => x.TimaLoanID)
                            .WhereClause(x => lstLoanIds.Contains((int)x.TimaLoanID))
                            .Query().ToDictionary(x => x.TimaLoanID, x => x.LoanID);
                        List<LMSModel.TblCommentDebtPrompted> lstComment = new List<LMSModel.TblCommentDebtPrompted>();
                        foreach (var item in lstCommentNeedInsert)
                        {
                            var commentLMS = new LMSModel.TblCommentDebtPrompted()
                            {
                                Comment = item.Comment,
                                CommentDebtPromptedID = item.Id,
                                CreateDate = item.CreateDate,
                                FullName = item.FullName,
                                IsDisplay = 1,
                                LoanID = dicLoan.ContainsKey(item.LoanId) ? dicLoan[item.LoanId] : 0,
                                ReasonCode = item.C_ReasonCode,
                                ReasonCodeID = item.ReasonId,
                                TimaCommentID = item.Id,
                                UserID = item.UserId,
                                TimaLoanID = item.LoanId ?? 0,
                                AppointmentDate = item.NextDate
                            };
                            if (lstCommentFile.Where(x => x.CommentID == item.Id).Count() > 0)
                            {
                                var lstImage = new List<ImageLosUpload>();
                                var lstFiles = lstCommentFile.Where(x => x.CommentID == item.Id).ToList();
                                foreach (var files in lstFiles)
                                {
                                    lstImage.Add(new ImageLosUpload()
                                    {
                                        FileName = files.FilePath,
                                        FullPath = "https://file.tima.vn" + files.FilePath,
                                        S3Path = "https://img.tima.vn",
                                        LocalPath = "https://file.tima.vn",
                                        Source = "AG"
                                    });
                                }
                                commentLMS.FileImage = common.ConvertObjectToJSonV2(lstImage);
                                lstComment.Add(commentLMS);
                            }
                            maxTimaCommentID = item.Id;
                        }
                        Console.WriteLine($"[Inserting] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} bat dau insert CommentDebtPromptedManager");
                        //commentLMSTab.InsertBulk(lstComment, setOffIdentity: true);
                        commentLMSTab.UpdateBulk(lstComment);
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert CommentDebtPromptedManager  -  {lstComment.Count()} ");
                    }
                    else
                    {
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert CommentDebtPromptedManager hoan tat");
                        break;
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine($"CommentDebtPromptedManager - ERROR {ex.Message}");
            }
        }

        /// <summary>
        ///  ngày cần chạy lại từ AG sang LMS
        /// </summary>
        /// <param name="createDate"></param>
        public void MoveCommentWithDay(DateTime createDate)
        {
            var commentLMSTab = new TableHelper<LMSModel.TblCommentDebtPrompted>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var commentTimaTab = new TableHelper<AG.TblCommentDebtPromptedAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            var commentFileAG = new TableHelper<AG.TblThnFiles>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            LMS.Common.Helper.Utils common = new LMS.Common.Helper.Utils();
            var loanTabLMS = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var fromdate = createDate.Date;
            var todate = createDate.Date.AddDays(1);

            // danh sách comment AG
            var commentAG = commentTimaTab.WhereClause(x => x.CreateDate >= fromdate && x.CreateDate < todate && (x.LMSCommentID == 0 || x.LMSCommentID == null)).Query();
            // danh sách comment LMS
            var dicCommentLMS = commentLMSTab.SelectColumns(x=>x.TimaCommentID,x=>x.CommentDebtPromptedID).WhereClause(x => x.CreateDate >= fromdate && x.TimaCommentID > 0).Query().ToDictionary(x => x.TimaCommentID, x => x.CommentDebtPromptedID);
            // check bắn miss sang AG 
            var lstCommentNeedInsert = new List<AG.TblCommentDebtPromptedAG>();
            foreach (var comment in commentAG)
            {
                if (!dicCommentLMS.ContainsKey(comment.Id))
                {
                    lstCommentNeedInsert.Add(comment);
                }

            }
            if (lstCommentNeedInsert.Any())
            {
                var lstcommentIds = lstCommentNeedInsert.Select(x => x.Id).ToList();

                var lstCommentFile = commentFileAG.WhereClause(x => lstcommentIds.Contains((int)x.CommentID)).Query();

                var lstLoanIds = lstCommentNeedInsert.Select(x => x.LoanId).ToList();

                var dicLoan = loanTabLMS.SelectColumns(x => x.LoanID, x => x.TimaLoanID)
                    .WhereClause(x => lstLoanIds.Contains((int)x.TimaLoanID))
                    .Query().ToDictionary(x => x.TimaLoanID, x => x.LoanID);
                List<LMSModel.TblCommentDebtPrompted> lstComment = new List<LMSModel.TblCommentDebtPrompted>();
                foreach (var item in lstCommentNeedInsert)
                {
                    try
                    {
                        var commentLMS = new LMSModel.TblCommentDebtPrompted()
                        {
                            Comment = item.Comment,
                            CreateDate = item.CreateDate,
                            FullName = item.FullName,
                            IsDisplay = 1,
                            LoanID = dicLoan.ContainsKey(item.LoanId) ? dicLoan[item.LoanId] : 0,
                            ReasonCode = item.C_ReasonCode,
                            ReasonCodeID = item.ReasonId,
                            TimaCommentID = item.Id,
                            UserID = item.UserId,
                            TimaLoanID = item.LoanId ?? 0,
                            AppointmentDate = item.NextDate
                            //CommentDebtPromptedID = item.Id
                        };

                        if (lstCommentFile.Where(x => x.CommentID == item.Id).Count() > 0)
                        {
                            var lstImage = new List<ImageLosUpload>();
                            var lstFiles = lstCommentFile.Where(x => x.CommentID == item.Id).ToList();
                            foreach (var files in lstFiles)
                            {
                                lstImage.Add(new ImageLosUpload()
                                {
                                    FileName = files.FilePath,
                                    FullPath = "https://thn.tima.vn/FileUpload" + files.FilePath,
                                    S3Path = "http://file.tima.vn",
                                    LocalPath = "https://thn.tima.vn/FileUpload",
                                    Source = "AG"
                                });
                            }
                            commentLMS.FileImage = common.ConvertObjectToJSonV2(lstImage);
                        }
                        lstComment.Add(commentLMS);
                    }
                    catch (Exception)
                    {

                    }

                }
                if (lstComment.Any())
                {
                    commentLMSTab.InsertBulk(lstComment);
                    Console.WriteLine($"[success] {createDate.ToString("dd/MM/yyyy")} insert CommentDebtPromptedManager  -  {lstComment.Count()} ");
                }
            }
            Console.WriteLine($"[success] {createDate.ToString("dd/MM/yyyy")} xong");

            // check bắn miss sang LMS
        }
    }
}
