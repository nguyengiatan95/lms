﻿using LMS.Common.Helper;
using LMS.Common.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LMS.Common.Constants;
using LMS.Entites.Dtos.LoanServices;
using MigrateAGToLMS.LMSModel;
using LMS.Entites.Dtos.LOSServices;
using RestSharp;
using System.Net;

namespace MigrateAGToLMS.Service
{
    class LoanManager
    {

        public void MoveLoanTima(List<int> IDs = null)
        {
            try
            {
                Utils common = new Utils();
                Console.WriteLine($"-------------- LOAN --------------");
                DateTime dtNow = DateTime.Now;
                var loanLMSTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                var lenderLMSTab = new TableHelper<LMSModel.TblLender>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                var customerLmsTab = new TableHelper<LMSModel.TblCustomer>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                // Tab AG
                var productCreditAgTab = new TableHelper<AG.TblProductCredit>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                var loanCreaditAgTab = new TableHelper<AG.TblLoanCredit>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                var loanTimaTab = new TableHelper<AG.TblLoanAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                var customerAGTab = new TableHelper<AG.TblCustomerAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);

                long LasTimaLoanID = 0;
                long maxTimaLoanID = 0;
                int maxRowGet = 2000;

                // Laays max ID da insert
                var taskMaxTimaLoanIDTima = Task.Run(() =>
                {
                    var lstMaxTimaLoanID = loanLMSTab.SelectColumns(x => x.TimaLoanID)
                                                  .OrderByDescending(x => x.TimaLoanID)
                                                  .WhereClause(x => x.TimaLoanID > 0).SetGetTop(1).Query();
                    if (lstMaxTimaLoanID != null && lstMaxTimaLoanID.Any())
                        return lstMaxTimaLoanID.First().TimaLoanID.Value;
                    return 0;

                });
                // lay shop cua tima + hub;
                var taskShopTima = Task.Run(() =>
                {
                    var shopTimaTab = new TableHelper<AG.TblShopAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                    var lstShopTima = Ultil.StaticEntity.LstShopTimaHubAG;
                    if (lstShopTima != null && lstShopTima.Count > 0) return lstShopTima;
                    return shopTimaTab.WhereClause(x => x.ShopID == 3703 || x.IsHub == true).Query();
                });

                Task.WaitAll(taskShopTima, taskMaxTimaLoanIDTima);
                // gans max shop 
                LasTimaLoanID = taskMaxTimaLoanIDTima.Result;
                maxTimaLoanID = LasTimaLoanID;
                if (IDs != null)
                {
                    maxTimaLoanID = 0;
                }
                var lstShopTimaHub = taskShopTima.Result;
                var lstShopTimahubID = lstShopTimaHub.Select(x => x.ShopID).ToList();
                while (true)
                {
                    if (IDs != null && IDs.Any())
                    {
                        loanTimaTab.WhereClause(x => IDs.Contains(x.ID));
                    }
                    var lstLoanAG = loanTimaTab.WhereClause(x => lstShopTimahubID.Contains(x.ShopID) && x.ID > maxTimaLoanID && x.ShopID > 0
                                              && x.Status != 0)
            .OrderBy(x => x.ID).SetGetTop(maxRowGet).Query();
                    // user chưa insert 
                    var lstLoanNeedInsert = lstLoanAG.OrderBy(x => x.ID).ToList();
                    var loanLMS = new List<TblLoan>();
                    if (IDs != null)
                    {
                        loanLMS = loanLMSTab.WhereClause(x => IDs.Contains((int)x.TimaLoanID)).Query().ToList();
                    }
                    if (lstLoanNeedInsert.Count > 0)
                    {
                        var dicLMS = loanLMS.ToDictionary(x => x.TimaLoanID, x => x.LoanID);
                        //lstLenderNeedInsert = lstLenderNeedInsert.Take(maxItemGet).ToList();
                        var dictProductCreditInfos = productCreditAgTab.Query().ToDictionary(x => x.Id, x => x);
                        List<LMSModel.TblLoan> lstLoanUpdate = new List<LMSModel.TblLoan>();
                        List<LMSModel.TblLoan> lstLoanInsert = new List<LMSModel.TblLoan>();
                        var taskCustomerLMS = Task.Run(() =>
                        {
                            var lstCustomerTimaIds = lstLoanNeedInsert.Select(x => (long)x.CustomerID).ToList().Distinct();
                            var lstCustomerLMS = customerLmsTab.WhereClause(x => lstCustomerTimaIds.Contains((long)x.TimaCustomerID)).Query();
                            return lstCustomerLMS.ToDictionary(x => x.TimaCustomerID, x => x);
                        });

                        var taskLenderLMS = Task.Run(() =>
                        {
                            var lstLenderIdsLMS = lstLoanNeedInsert.Where(x => x.AgencyId > 0).Select(x => (long)x.AgencyId).ToList().Distinct();
                            var lstShopIdsLMS = lstLoanNeedInsert.Where(x => x.HubID > 0).Select(x => (long)x.HubID).ToList().Distinct();
                            var lstLenderLMS = lenderLMSTab.WhereClause(x => (lstLenderIdsLMS.Contains(x.TimaLenderID) || lstShopIdsLMS.Contains(x.TimaLenderID) || x.TimaLenderID == 3703) && x.TimaLenderID > 0).Query();
                            return lstLenderLMS.ToDictionary(x => x.TimaLenderID, x => x);
                        });
                        var taskLoanCreditAG = Task.Run(() =>
                        {
                            var lstLoanIds = lstLoanNeedInsert.Select(x => x.ID).ToList().Distinct();
                            return loanCreaditAgTab.SelectColumns(x => x.CustomerCreditId, x => x.LoanID, x => x.ID, x => x.TypeInsurance, x => x.IdOfPartenter, x => x.PlatformType)
                            .WhereClause(x => lstLoanIds.Contains((int)x.LoanID))
                                 .Query().ToDictionary(x => x.LoanID, x => x);
                        });
                        var taskCityAG = Task.Run(() =>
                        {
                            var lstCityTima = Ultil.StaticEntity.LstCityTimaHubAG;
                            if (lstCityTima != null && lstCityTima.Count > 0) return lstCityTima;
                            var cityTabAG = new TableHelper<AG.TblCityAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                            return cityTabAG.Query();
                        });
                        var taskDistrict = Task.Run(() =>
                        {
                            var lstDistrictTima = Ultil.StaticEntity.LstDistrictTimaHubAG;
                            if (lstDistrictTima != null && lstDistrictTima.Count > 0) return lstDistrictTima;
                            var districtTabAG = new TableHelper<AG.TblDistrictAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                            return districtTabAG.Query();
                        });
                        Task.WaitAll(taskCustomerLMS, taskLenderLMS, taskLoanCreditAG, taskCityAG, taskDistrict);
                        var dicCustomerLMS = taskCustomerLMS.Result;
                        var dicLenderLMS = taskLenderLMS.Result;
                        var dictLoanCreditInfos = taskLoanCreditAG.Result;
                        //lay lender 
                        var dictCity = taskCityAG.Result.ToDictionary(x => x.ID, x => x);
                        var dictDistrict = taskDistrict.Result.ToDictionary(x => x.ID, x => x);

                        foreach (var item in lstLoanNeedInsert)
                        {
                            string customerName = "";
                            long customerId = 0;
                            long lenderId = 0;
                            long idOfPartner = 0;
                            long shopID = 0;
                            long hubID = 0;
                            int productID = 0;
                            int sourceBuyInsurance = 0;
                            if (dicCustomerLMS.ContainsKey(item.CustomerID))
                            {
                                var customerLms = dicCustomerLMS[item.CustomerID];
                                customerName = customerLms.FullName;
                                customerId = customerLms.CustomerID;
                            }

                            if (dicLenderLMS.ContainsKey((long)item.AgencyId))
                            {
                                var lenderLMS = dicLenderLMS[(long)item.AgencyId];
                                lenderId = lenderLMS.LenderID;
                            }
                            if (dictLoanCreditInfos.ContainsKey(item.ID))
                            {
                                idOfPartner = dictLoanCreditInfos[item.ID].ID;
                                if (dictLoanCreditInfos[item.ID].PlatformType == 12)
                                    long.TryParse(dictLoanCreditInfos[item.ID].IdOfPartenter, out idOfPartner);
                                sourceBuyInsurance = dictLoanCreditInfos[item.ID].TypeInsurance ?? 0;
                            }
                            if (dicLenderLMS.ContainsKey(item.ShopID))
                            {
                                shopID = dicLenderLMS[item.ShopID].LenderID;
                            }
                            if (item.HubID != null && dicLenderLMS.ContainsKey((long)item.HubID))
                            {
                                hubID = dicLenderLMS[(long)item.HubID].LenderID;
                            }
                            if (dictProductCreditInfos.ContainsKey((int)item.ProductID))
                            {
                                productID = dictProductCreditInfos[(int)item.ProductID].TypeProduct.Value;
                            }
                            LoanJsonExtra loanJsonExtra = new LoanJsonExtra
                            {
                                DistrictName = dictDistrict.GetValueOrDefault(item.DistrictID ?? 0)?.Name,
                                CityName = dictCity.GetValueOrDefault(item.CityID ?? 0)?.Name,
                                ShopName = dicLenderLMS.GetValueOrDefault(lenderId)?.FullName,
                                ConsultantShopName = dicLenderLMS.GetValueOrDefault(item.HubID ?? 0)?.FullName,
                            };
                            var jsonExtra = common.ConvertObjectToJSonV2(loanJsonExtra);
                            try
                            {
                                var objLoan = new LMSModel.TblLoan()
                                {
                                    //BankCardID = item.SourceBankDisbursement ?? 0,
                                    CityID = item.CityID,
                                    ContactCode = "TC-" + (long)item.CodeID,
                                    CreateDate = item.FromDate.Value,
                                    FromDate = item.FromDate,
                                    ModifyDate = item.FromDate.Value,
                                    FinishDate = item.FinishDate,
                                    MoneyFeeInsuranceOfCustomer = item.MoneyFeeInsuranceOfCustomer,
                                    CustomerID = customerId,
                                    CustomerName = customerName,
                                    DistrictID = item.DistrictID,
                                    Frequency = item.Frequency,
                                    LastDateOfPay = item.LastDateOfPay,
                                    LenderID = lenderId,
                                    LinkGCNChoVay = item.LinkGCNChoVay,
                                    LinkGCNVay = item.LinkGCNVay,
                                    LoanTime = item.LoanTime.Value,
                                    NextDate = item.NextDate,
                                    ProductID = item.ProductID,
                                    RateConsultant = common.ConvertRateInterestToVND(item.RateConsultant.Value),
                                    RateInterest = common.ConvertRateInterestToVND(item.RateInterest.Value),
                                    RateService = common.ConvertRateInterestToVND(item.RateService.Value),
                                    RateType = (short)item.RateType,
                                    SourcecBuyInsurance = sourceBuyInsurance,
                                    ToDate = item.ToDate,
                                    Status = ConvertStatusAG((int)item.Status, item.TypeCloseLoan),
                                    TotalMoneyCurrent = item.TotalMoneyCurrent,
                                    TimaLoanID = item.ID,
                                    TotalMoneyDisbursement = item.TotalMoney,
                                    OwnerShopID = shopID,
                                    ProductName = dictProductCreditInfos[item.ProductID.Value].Name,
                                    SourceMoneyDisbursement = (short)GetSourceMoneyDisbursement(item.SourceBankDisbursement ?? 0),
                                    MoneyFeeInsuranceOfLender = 0,
                                    JsonExtra = jsonExtra,
                                    LoanCreditIDOfPartner = idOfPartner,
                                    ConsultantShopID = item.HubID == null ? shopID : hubID,
                                    UnitRate = (int)Loan_UnitRate.Default,
                                    TimaCodeID = item.CodeID ?? 0,
                                    MoneyFineLate = item.OldDebitMoneyFineLate,
                                    StatusSendInsurance = item.StatusSendInsurance ?? 0,
                                    MoneyFeeInsuranceMaterialCovered = item.MoneyFeeInsuranceMaterialCovered,
                                    FeeFineOriginal = (item.FeePaymentBeforeLoan ?? 0) * 100,
                                    UnitFeeFineOriginal = (int)Loan_UnitRate.Percent,
                                    DebitMoney = item.DebitMoney,
                                    YearBadDebt = item.YearBadDebt
                                };
                                if (dicLMS.ContainsKey(item.ID))
                                {

                                    objLoan.LoanID = dicLMS[item.ID] == 0 ? item.ID : dicLMS[item.ID];
                                    lstLoanUpdate.Add(objLoan);
                                }
                                else
                                {
                                    lstLoanInsert.Add(objLoan);
                                }
                                maxTimaLoanID = item.ID;
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine($"MoveLoanTima LoanID-{item.ID} ERROR {ex.Message} ");
                                continue;
                            }
                        }
                        Console.WriteLine($"[Inserting] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} bat dau insert Loan");
                        if (lstLoanInsert != null && lstLoanInsert.Any())
                        {
                            loanLMSTab.InsertBulk(lstLoanInsert);
                            Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} inssert Loan  -  {lstLoanInsert.Count()} ");
                        }
                        if (lstLoanUpdate != null && lstLoanUpdate.Any())
                        {
                            loanLMSTab.UpdateBulk(lstLoanUpdate);
                            Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} Update Loan  -  {lstLoanUpdate.Count()} ");
                        }
                    }
                    else
                    {
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert Loan hoan tat");
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"MoveLoanTima - ERROR {ex.Message}");
            }
        }

        private int GetSourceMoneyDisbursement(int? sourceBankDisbursement)
        {
            switch (sourceBankDisbursement)
            {
                case 1: // Nam Á
                    return (int)Loan_SourceMoneyDisbursement.BankNamAOfLender;
                case 2: // ncb
                    return (int)Loan_SourceMoneyDisbursement.BankNCBOfLender;
                case 3: // other bank
                    return (int)Loan_SourceMoneyDisbursement.OtherBank;
                default: // 4 tiền quỹ của nhà đầu tư
                    return (int)Loan_SourceMoneyDisbursement.EscrowSource;
            }
        }

        private int ConvertStatusAG(int status, int typeCloseLoan)
        {
            switch ((AG.Status_Loan)status)
            {
                case AG.Status_Loan.Lending:
                case AG.Status_Loan.ToDayPay:
                case AG.Status_Loan.DelayInterest:
                case AG.Status_Loan.Delayed:
                case AG.Status_Loan.Delayed2:
                case AG.Status_Loan.WaitLiquidation:
                    return (int)Loan_Status.Lending;
                case AG.Status_Loan.Acquision:
                    switch ((AG.Type_CloseLoan)typeCloseLoan)
                    {
                        case AG.Type_CloseLoan.Finalization:
                            return (int)Loan_Status.Close;
                        case AG.Type_CloseLoan.Liquidation:
                            return (int)Loan_Status.CloseLiquidation;
                        case AG.Type_CloseLoan.LiquidationLoanInsurance:
                            return (int)Loan_Status.CloseLiquidationLoanInsurance;
                        case AG.Type_CloseLoan.IndemnifyLoanInsurance:
                            return (int)Loan_Status.CloseIndemnifyLoanInsurance;
                        case AG.Type_CloseLoan.PaidInsurance:
                            return (int)Loan_Status.ClosePaidInsurance;
                        case AG.Type_CloseLoan.Exemption:
                            return (int)Loan_Status.CloseExemption;
                        case AG.Type_CloseLoan.PaidInsuranceExemption:
                            return (int)Loan_Status.ClosePaidInsuranceExemption;
                        default:
                            return (int)Loan_Status.Close;
                    }
                case AG.Status_Loan.Deleted:
                    return (int)Loan_Status.Delete;
                default:
                    return (int)Loan_Status.Delete;
            }
        }



        public void CompareLoan()
        {
            LenderManager lenderManager = new LenderManager();
            Console.WriteLine($"--------------  COMPARE LOAN --------------");
            DateTime dtNow = DateTime.Now;
            var logloanLMSTab = new TableHelper<LMSModel.LoanCompare>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var logloanAGTab = new TableHelper<LMSModel.LoanCompare>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            var logCompareTab = new TableHelper<LMSModel.TblLogCompare>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var loanLMSTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var loanAGTab = new TableHelper<AG.TblLoanAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);

            //string sqlSummaryAG = string.Format(@"SELECT 
            //                        COUNT(CASE WHEN FromDate = '{0}' then 1 ELSE NULL END) as Disbursement_TotalLoan,
            //                        SUM(CASE WHEN FromDate = '{0}' then L.TotalMoney ELSE 0 END) as Disbursement_TotalMoney,
            //                        COUNT(CASE WHEN FromDate = '{0}' then CustomerID ELSE NULL END) as Disbursement_TotalCustomer,
            //                        COUNT(CASE WHEN L.Status  >= 11  then 1 ELSE NULL END) as Loan_TotalLending,
            //                        COUNT(CASE WHEN L.Status  >= 11 and DATEDIFF(day,NextDate,GETDATE()) > 30 then 1 ELSE NULL END) as Loan_TotalBadDebt,
            //                        COUNT(CASE WHEN L.Status  >= 11 and DATEDIFF(day,NextDate,GETDATE()) > 1 then 1 ELSE NULL END) as Loan_TotalOverTime,
            //                        COUNT(CASE WHEN L.Status = 1 then 1 ELSE NULL END) as Loan_TotalClosed,
            //                        COUNT(CASE WHEN L.Status = 1 and TypeCloseLoan = 3 then 1 ELSE NULL END) as Loan_TotalInsurancePaid,
            //                        Count(CASE WHEN L.Status != 0 then 1 ELSE NULL END) as TotalLoan
            //                        FROM TblLoan (NOLOCK) L inner join tblShop(NOLOCK) S ON L.ShopID = S.ShopID where S.ShopID = 3703 OR S.IsHub = 1", dtNow.ToString("yyyy-MM-dd"));

            //string sqlSummaryLMS = string.Format(@"
            //                                    SELECT 
            //                                    COUNT(CASE WHEN FromDate = '{0}'  then 1 ELSE NULL END) as Disbursement_TotalLoan,
            //                                    SUM(CASE WHEN FromDate = '{0}'  then TotalMoneyDisbursement ELSE 0 END) as Disbursement_TotalMoney,
            //                                    COUNT(CASE WHEN FromDate = '{0}'  then CustomerID ELSE NULL END) as Disbursement_TotalCustomer,
            //                                    COUNT(CASE WHEN Status = 1 then 1 ELSE NULL END) as Loan_TotalLending,
            //                                    COUNT(CASE WHEN Status = 1 and DATEDIFF(day,NextDate,GETDATE()) > 30 then 1 ELSE NULL END) as Loan_TotalBadDebt,
            //                                    COUNT(CASE WHEN Status = 1 and DATEDIFF(day,NextDate,GETDATE()) > 1 then 1 ELSE NULL END) as Loan_TotalOverTime,
            //                                    COUNT(CASE WHEN Status >= 100 then 1 ELSE NULL END) as Loan_TotalClosed,
            //                                    COUNT(CASE WHEN Status = 103 then 1 ELSE NULL END) as Loan_TotalInsurancePaid,
            //                                    Count(LoanID) as TotalLoan
            //                                    FROM TblLoan (NOLOCK)", dtNow.ToString("yyyy-MM-dd"));
            //var taskAG = Task.Run(() =>
            //{
            //    return logloanAGTab.Query(sqlSummaryAG, null).FirstOrDefault();
            //});
            //var taskLMS = Task.Run(() =>
            //{
            //    return logloanLMSTab.Query(sqlSummaryLMS, null).FirstOrDefault();
            //});
            var taskShopTima = Task.Run(() =>
            {
                var shopTimaTab = new TableHelper<AG.TblShopAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                var lstShopTima = Ultil.StaticEntity.LstShopTimaHubAG;
                if (lstShopTima != null && lstShopTima.Count > 0) return lstShopTima;
                return shopTimaTab.WhereClause(x => x.ShopID == 3703 || x.IsHub == true).Query();
            });
            Task.WaitAll(taskShopTima);
            //var loanCompareTima = taskAG.Result;
            //var loanCompareLMS = taskLMS.Result;
            var lstShopIDs = taskShopTima.Result.Select(x => x.ShopID).ToList();

            var taskLoanAG = Task.Run(() =>
            {
                return loanAGTab.SelectColumns(x => x.ID, x => x.NextDate, x => x.PaymentMoney,
                    x => x.TotalMoneyCurrent, x => x.TotalMoneyFineCurrent, x => x.LastDateOfPay, x => x.OldDebitMoneyFineLate, x => x.Status)
                .WhereClause(x => lstShopIDs.Contains(x.ShopID) && x.Status != 0).Query();
            });

            var taskLoanLMS = Task.Run(() =>
            {
                return loanLMSTab.SelectColumns(x => x.LoanID, x => x.NextDate, x => x.TimaLoanID,
                    x => x.TotalMoneyCurrent, x => x.MoneyFineLate, x => x.LastDateOfPay, x => x.Status, x => x.FromDate,
                    x => x.TotalMoneyDisbursement).Query();
            });
            Task.WaitAll(taskLoanAG, taskLoanLMS);
            var lstLoanTima = taskLoanAG.Result;
            var dicLoanLMS = taskLoanLMS.Result.ToDictionary(x => x.TimaLoanID, x => x);

            long totallNextDateError = 0;
            long totallTotalMoneyCurrent = 0;
            long totallLastDateOfPay = 0;
            long totalDebt = 0;
            long totalDisbursement_TotalLoan = 0;
            long totalLending = 0;
            long totalClosed = 0;
            long totalInsurancePaid = 0;


            string loanNextDateError = LogCompare_DetailType.Loan_NextDate.GetDescription();
            string loanTotalMoneyCurrent = LogCompare_DetailType.Loan_TotalMoneyCurrent.GetDescription();
            string loanLastDateOfPay = LogCompare_DetailType.Loan_LastDateOfPay.GetDescription();
            string loanOldDebitMoneyFineLate = LogCompare_DetailType.Loan_FineInterest.GetDescription();
            string disbursement_TotalLoan = LogCompare_DetailType.Disbursement_TotalLoan.GetDescription();
            string lending_loan = LogCompare_DetailType.Loan_TotalLending.GetDescription();
            string close_loan = LogCompare_DetailType.Loan_TotalClosed.GetDescription();
            string insurancePaid_loan = LogCompare_DetailType.Loan_TotalInsurancePaid.GetDescription();


            foreach (var item in lstLoanTima)
            {
                if (dicLoanLMS.ContainsKey(item.ID))
                {
                    var loanLMS = dicLoanLMS[item.ID];
                    if (item.NextDate != loanLMS.NextDate)
                    {
                        loanNextDateError += $" {item.ID} , ";
                        totallNextDateError += 1;
                    }
                    if (item.TotalMoneyCurrent != loanLMS.TotalMoneyCurrent)
                    {
                        totallTotalMoneyCurrent += 1;
                        loanTotalMoneyCurrent += $" {item.ID} , ";
                    }
                    if (item.LastDateOfPay != loanLMS.LastDateOfPay)
                    {
                        totallLastDateOfPay += 1;
                        loanLastDateOfPay += $" {item.ID} , ";
                    }
                    if (item.OldDebitMoneyFineLate != loanLMS.MoneyFineLate)
                    {
                        totalDebt += 1;
                        loanOldDebitMoneyFineLate += $" {item.ID} , ";
                    }
                    if (item.Status >= 11 && loanLMS.Status != 1)
                    {
                        totalLending += 1;
                        lending_loan += $" {item.ID} , ";
                    }
                    if (item.Status == 1 && loanLMS.Status < 100)
                    {
                        totalClosed += 1;
                        close_loan += $" {item.ID} , ";
                    }
                    if (item.Status == 1 && item.TypeCloseLoan == 3 && loanLMS.Status != 103)
                    {
                        totalInsurancePaid += 1;
                        insurancePaid_loan += $" {item.ID} , ";
                    }
                }
                else
                {
                    totalDisbursement_TotalLoan += 1;
                    disbursement_TotalLoan += $" {item.ID} , ";
                }
            }




            List<LMSModel.TblLogCompare> lstLog = new List<LMSModel.TblLogCompare>();
            lstLog.Add(lenderManager.SetlogCompare(
                (int)LogCompare_MainType.Disbursement, (int)LogCompare_DetailType.Disbursement_TotalLoan,
               0, totalDisbursement_TotalLoan, disbursement_TotalLoan));

            //lstLog.Add(lenderManager.SetlogCompare(
            // (int)LogCompare_MainType.Disbursement, (int)LogCompare_DetailType.Disbursement_TotalCustomer,
            // loanCompareTima.Disbursement_TotalCustomer, loanCompareLMS.Disbursement_TotalCustomer));

            //lstLog.Add(lenderManager.SetlogCompare(
            //            (int)LogCompare_MainType.Disbursement, (int)LogCompare_DetailType.Disbursement_TotalMoney,
            //            loanCompareTima.Disbursement_TotalMoney, loanCompareLMS.Disbursement_TotalMoney));

            //lstLog.Add(lenderManager.SetlogCompare(
            //            (int)LogCompare_MainType.Disbursement, (int)LogCompare_DetailType.Disbursement_TotalMoneyExpense,
            //            loanTima.Disbursement_TotalMoneyExpense, loanLMS.Disbursement_TotalMoneyExpense));

            //lstLog.Add(lenderManager.SetlogCompare(
            //                            (int)LogCompare_MainType.Disbursement, (int)LogCompare_DetailType.Disbursement_TotalMoneyInsurance,
            //                            loanTima.Disbursement_TotalMoneyInsurance, loanLMS.Disbursement_TotalMoneyInsurance));

            //lstLog.Add(lenderManager.SetlogCompare(
            //                           (int)LogCompare_MainType.Disbursement, (int)LogCompare_DetailType.Disbursement_TotalMoneyLenderHold,
            //                           loanTima.Disbursement_TotalMoneyLenderHold, loanLMS.Disbursement_TotalMoneyLenderHold));

            // loan
            lstLog.Add(lenderManager.SetlogCompare(
                                       (int)LogCompare_MainType.Loan, (int)LogCompare_DetailType.Loan_TotalLending,
                                       0, totalLending, lending_loan));

            //lstLog.Add(lenderManager.SetlogCompare(
            //                         (int)LogCompare_MainType.Loan, (int)LogCompare_DetailType.Loan_TotalWaitingInsurance,
            //                         loanTima.Loan_TotalWaitingInsurance, loanLMS.Loan_TotalWaitingInsurance));

            //lstLog.Add(lenderManager.SetlogCompare(
            //                      (int)LogCompare_MainType.Loan, (int)LogCompare_DetailType.Loan_TotalSentInsurance,
            //                      loanTima.Loan_TotalWaitingInsurance, loanLMS.Loan_TotalSentInsurance));

            lstLog.Add(lenderManager.SetlogCompare(
                                  (int)LogCompare_MainType.Loan, (int)LogCompare_DetailType.Loan_TotalInsurancePaid,
                                  0, totalInsurancePaid, insurancePaid_loan));

            //lstLog.Add(lenderManager.SetlogCompare(
            //                    (int)LogCompare_MainType.Loan, (int)LogCompare_DetailType.Loan_TotalOverTime,
            //                    loanCompareTima.Loan_TotalOverTime, loanCompareLMS.Loan_TotalOverTime));

            //lstLog.Add(lenderManager.SetlogCompare(
            //             (int)LogCompare_MainType.Loan, (int)LogCompare_DetailType.Loan_TotalBadDebt,
            //             loanCompareTima.Loan_TotalBadDebt, loanCompareLMS.Loan_TotalBadDebt));

            lstLog.Add(lenderManager.SetlogCompare(
                        (int)LogCompare_MainType.Loan, (int)LogCompare_DetailType.Loan_TotalClosed,
                        0, totalClosed, close_loan));
            lstLog.Add(lenderManager.SetlogCompare(
                                     (int)LogCompare_MainType.Loan, (int)LogCompare_DetailType.Loan_FineInterest,
                                     0, totalDebt, loanOldDebitMoneyFineLate));
            lstLog.Add(lenderManager.SetlogCompare(
                                     (int)LogCompare_MainType.Loan, (int)LogCompare_DetailType.Loan_LastDateOfPay,
                                     0, totallLastDateOfPay, loanLastDateOfPay));
            lstLog.Add(lenderManager.SetlogCompare(
                                     (int)LogCompare_MainType.Loan, (int)LogCompare_DetailType.Loan_NextDate,
                                     0, totallNextDateError, loanNextDateError));
            lstLog.Add(lenderManager.SetlogCompare(
                                   (int)LogCompare_MainType.Loan, (int)LogCompare_DetailType.Loan_TotalMoneyCurrent,
                                   0, totallTotalMoneyCurrent, loanTotalMoneyCurrent));
            List<int> mainType = new List<int>() { (int)LogCompare_MainType.Loan, (int)LogCompare_MainType.Disbursement };
            var lstCurrentLog = logCompareTab.WhereClause(x => x.Status == 1 && x.ForDate == DateTime.Now.Date && mainType.Contains(x.MainType)).Query();
            logCompareTab.DeleteList(lstCurrentLog.ToList());
            logCompareTab.InsertBulk(lstLog);
            Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert compare Loan hoan tat");
        }

        public void GetListLoanAGChangeToDay()
        {
            var tranTab = new TableHelper<AG.TblTransactionLoanAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);

            DateTime minDate = new DateTime(2021, 09, 25);
            DateTime toDate = DateTime.Now.Date;
            while (minDate < toDate)
            {
                string query = $"select distinct(LoanID) as LoanID from tblTransactionLoan(nolock) where CreateDate >= '{minDate.ToString("yyyy-MM-dd")}' and CreateDate< '{minDate.AddDays(1).ToString("yyyy-MM-dd")}' and LoanID != 110922 and loanID != 154816";
                var lstTran = tranTab.Query(query, null);
                if (lstTran != null && lstTran.Any())
                {
                    var lstLoanIDs = lstTran.Select(x => x.LoanID).ToList();
                    new CustomerManager().MoveCustomerTima(true, loanTimaIds: lstLoanIDs);
                    MoveLoanTima(lstLoanIDs);
                    new PaymentManager().MovePaymentTima(lstLoanIDs);
                    new TransactionManager().MoveTransactionLoanTima(lstLoanIDs);
                }
                minDate = minDate.AddDays(1);
            }
            //Lấy ra loanID có thay đổi hôm nay
            //call update
        }

        public void MoveLoanLMSToLOS()
        {
            var loanLMSTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMS);
            var loanLMSLOSTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            int maxItem = 2000;
            var lstMaxTimaLoanID = loanLMSLOSTab.SelectColumns(x => x.LoanID)
                                            .OrderByDescending(x => x.LoanID)
                                            .WhereClause(x => x.LoanID > 0).SetGetTop(1).Query().FirstOrDefault()?.LoanID ?? 0;
            long latestLoanID = lstMaxTimaLoanID;
            while (true)
            {
                var lstLoanLMS = loanLMSTab.SetGetTop(maxItem).WhereClause(x => x.LoanID > latestLoanID).Query();
                if (lstLoanLMS == null || !lstLoanLMS.Any())
                {
                    Console.WriteLine($"----MOVE LOAN LMS DONE");
                    return;
                }
                lstLoanLMS = lstLoanLMS.OrderBy(x => x.LoanID);
                latestLoanID = lstLoanLMS.Last().LoanID;
                loanLMSLOSTab.InsertBulk(lstLoanLMS, setOffIdentity: true);
                Console.WriteLine($"----INSERT LOAN LMS: {lstLoanLMS.Count()} item");
            }
        }

        public void MoveLoanUpdateFeeFineOriginal()
        {
            var loanLMSTab = new TableHelper<LMSModel.LoanUpdateFeeFineOriginal>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var loanTimaTab = new TableHelper<AG.TblLoanAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            long latestID = 0;
            int count = 0;
            while (true)
            {
                var lstLoanData = loanLMSTab.SetGetTop(2000).WhereClause(x => x.FeeFineOriginal < 1 && x.LoanID > latestID && x.Status == 1).Query();
                if (lstLoanData == null || !lstLoanData.Any())
                {
                    break;
                }
                lstLoanData = lstLoanData.OrderBy(x => x.LoanID);
                latestID = lstLoanData.Last().LoanID;
                var lstLoanTimaID = lstLoanData.Select(x => x.TimaLoanID).ToList();
                var lstLoanTimaInfos = loanTimaTab.SelectColumns(x => x.ID, x => x.FeePaymentBeforeLoan).WhereClause(x => lstLoanTimaID.Contains(x.ID)).Query().ToDictionary(x => (long)x.ID, x => x);
                foreach (var item in lstLoanData)
                {
                    item.FeeFineOriginal = lstLoanTimaInfos.GetValueOrDefault(item.TimaLoanID).FeePaymentBeforeLoan.Value * 100;
                    item.UnitFeeFineOriginal = (int)Loan_UnitRate.Percent;
                    count++;
                }
                loanLMSTab.UpdateBulk(lstLoanData);
            }
            Console.WriteLine($"----Update done LOAN LMS: {count} item");
        }

        public void MoveYearBadDebt()
        {
            var loanLMSTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var loanLMSUpdateYearBadDebtTab = new TableHelper<LMSModel.LoanUpdateYearBadDebt>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            //var paymentLMSTab = new TableHelper<LMSModel.TblPaymentSchedule>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var loanTimaTab = new TableHelper<AG.TblLoanAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            int top = 2000;
            long latestID = 0;
            var shopTimaTab = new TableHelper<AG.TblShopAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);

            var lstShopTimaID = shopTimaTab.WhereClause(x => x.ShopID == 3703 || x.IsHub == true).Query().Select(x => x.ShopID);
            List<LMSModel.LoanUpdateYearBadDebt> lstUpdate = new List<LoanUpdateYearBadDebt>();
            while (true)
            {
                var lstLoanLms = loanLMSTab.SelectColumns(x => x.TimaLoanID, x => x.LoanID).SetGetTop(top).WhereClause(x => x.Status == 1 && x.YearBadDebt == null && x.LoanID > latestID).OrderBy(x => x.LoanID).Query();
                if (lstLoanLms == null || !lstLoanLms.Any())
                {
                    return;
                }
                lstUpdate.Clear();
                latestID = lstLoanLms.OrderBy(x => x.LoanID).Last().LoanID;
                var dictLoanLms = lstLoanLms.ToDictionary(x => x.TimaLoanID, x => x);
                var lstLoanAGID = dictLoanLms.Keys.ToList();
                var lstDataLoanAG = loanTimaTab.SelectColumns(x => x.ID, x => x.YearBadDebt).WhereClause(x => lstLoanAGID.Contains(x.ID) && lstShopTimaID.Contains(x.ShopID)).Query();
                foreach (var item in lstDataLoanAG)
                {
                    lstUpdate.Add(new LoanUpdateYearBadDebt
                    {
                        LoanID = dictLoanLms.GetValueOrDefault(item.ID).LoanID,
                        YearBadDebt = item.YearBadDebt
                    });
                }
                if (lstUpdate.Count > 0)
                {
                    loanLMSUpdateYearBadDebtTab.UpdateBulk(lstUpdate);
                }
            }
        }

        public void UpdateLoanJsonExtra()
        {
            try
            {
                var loanLMSTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                var lenderLMSTab = new TableHelper<LMSModel.TblLender>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                var loanLMSUpdateYearBadDebtTab = new TableHelper<LMSModel.LoanUpdateLoanJsonExtra>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                int top = 1000;
                long latestID = 0;
                Utils common = new Utils();
                List<LMSModel.LoanUpdateLoanJsonExtra> lstUpdate = new List<LoanUpdateLoanJsonExtra>();
                while (true)
                {
                    var lstLoanLms = loanLMSTab.SelectColumns(x => x.LenderID, x => x.JsonExtra, x => x.LoanID).SetGetTop(top).WhereClause(x => x.LoanID > latestID).OrderBy(x => x.LoanID).Query();
                    if (lstLoanLms == null || !lstLoanLms.Any())
                    {
                        return;
                    }
                    lstUpdate.Clear();
                    latestID = lstLoanLms.OrderBy(x => x.LoanID).Last().LoanID;
                    var lstLenderID = lstLoanLms.Select(x => x.LenderID);
                    var dictLenderInfos = lenderLMSTab.SelectColumns(x => x.FullName, x => x.LenderID).WhereClause(x => lstLenderID.Contains(x.LenderID)).Query().ToDictionary(x => x.LenderID, x => x);
                    //var dictLoanLms = lstLoanLms.ToDictionary(x => x.TimaLoanID, x => x);
                    //var lstLoanAGID = dictLoanLms.Keys.ToList();
                    foreach (var item in lstLoanLms)
                    {
                        LoanUpdateLoanJsonExtra itemUpdate = new LoanUpdateLoanJsonExtra
                        {
                            LoanID = item.LoanID
                        };
                        if (!string.IsNullOrEmpty(item.JsonExtra))
                        {
                            LoanJsonExtra loanJsonExtra = common.ConvertJSonToObjectV2<LoanJsonExtra>(item.JsonExtra);
                            loanJsonExtra.ShopName = dictLenderInfos.GetValueOrDefault(item.LenderID ?? 0)?.FullName;
                            itemUpdate.JsonExtra = common.ConvertObjectToJSonV2(loanJsonExtra);
                        }
                        else
                        {
                            Console.WriteLine($"[Fail] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} LoanID = {item.LoanID} jsonExtra empty");
                        }

                        lstUpdate.Add(itemUpdate);
                    }
                    if (lstUpdate.Count > 0)
                    {
                        loanLMSUpdateYearBadDebtTab.UpdateBulk(lstUpdate);
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} update {lstUpdate.Count} rows");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"UpdateLoanJsonExtra - ERROR {ex.Message}");
            }
            Console.WriteLine($"[Done] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")}");
        }

        public void UpdateIsLocationFromAg()
        {
            var loanLMSTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            // Tab AG
            var loanCreaditAgTab = new TableHelper<AG.TblLoanCredit>().SetConnectString(Ultil.Constant.ConnectionStringAG);

            var loanLMSUpdateJsonExtra = new TableHelper<LMSModel.LoanUpdateLoanJsonExtra>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);

            Utils common = new Utils();

            string sql = @"SELECT 
lc.IsLocate, lc.LoanID, lc.OwerShopId, l.ShopID
FROM tblLoanCredit lc(nolock) join tblLoan (nolock) l on lc.LoanID = l.ID
join tblShop s(nolock) on s.ShopID = l.ShopID
where lc.IsLocate = 1 AND lc.Status >= 35
and (s.ShopID = 3703 or s.IsHub = 1)";

            var lstLoanIslocation = loanCreaditAgTab.Query(sql, new Dapper.DynamicParameters());
            var lstBatch = lstLoanIslocation.Batch(1000);
            foreach (var lst in lstBatch)
            {
                var lstLoanID = lst.Select(x => x.LoanID).ToList();
                var lstLoanLMS = loanLMSTab.SelectColumns(x => x.LoanID, x => x.JsonExtra).WhereClause(x => lstLoanID.Contains((int)x.TimaLoanID)).Query();
                if (lstLoanLMS != null && lstLoanLMS.Any())
                {
                    List<LMSModel.LoanUpdateLoanJsonExtra> lstUpdate = new List<LoanUpdateLoanJsonExtra>();
                    foreach (var item in lstLoanLMS)
                    {
                        if (!string.IsNullOrEmpty(item.JsonExtra))
                        {
                            LoanUpdateLoanJsonExtra itemUpdate = new LoanUpdateLoanJsonExtra
                            {
                                LoanID = item.LoanID
                            };
                            LoanJsonExtra loanJsonExtra = common.ConvertJSonToObjectV2<LoanJsonExtra>(item.JsonExtra);
                            if (!loanJsonExtra.IsLocate)
                            {
                                loanJsonExtra.IsLocate = true;
                                itemUpdate.JsonExtra = common.ConvertObjectToJSonV2(loanJsonExtra);
                                lstUpdate.Add(itemUpdate);
                            }
                        }
                    }
                    if (lstUpdate.Count > 0)
                    {
                        loanLMSUpdateJsonExtra.UpdateBulk(lstUpdate);
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} update {lstUpdate.Count} rows");
                    }
                }
            }
            Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} done.");
        }

        public void UpdateLoanJsonFromLos()
        {
            LMS.Entites.Dtos.LOSServices.LOSConfiguration.BaseUrl = "https://apilos.tima.vn/";
            LMS.Entites.Dtos.LOSServices.LOSConfiguration.AuthorizationLos = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiTE1TIiwiaWQiOjI0NTI2ODU0NjI1ODQ1ODUyfQ.GbJBHFdAHHorI9N5agH8zuuIMC3WG7jZVlkUuXoJgMs";

            Utils common = new Utils();
            var loanLMSTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var lstLoanCreditDetailLos = new TableHelper<LMSModel.TblLoanCreditDetailLos>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            List<int> lstStatus = new List<int>
            {
                (int)LMS.Common.Constants.Loan_Status.Lending,
                (int)LMS.Common.Constants.Loan_Status.CloseIndemnifyLoanInsurance,
            };
            var lstLoanLms = loanLMSTab.SelectColumns(x => x.LoanID, x => x.LoanCreditIDOfPartner).WhereClause(x => lstStatus.Contains((int)x.Status)).Query();
            foreach (var item in lstLoanLms)
            {
                var url = $"{LMS.Entites.Dtos.LOSServices.LOSConfiguration.BaseUrl}{string.Format(LMS.Entites.Dtos.LOSServices.LOSConfiguration.LOS_Action_GetLoanBriefDetail, item.LoanCreditIDOfPartner)}";

                Console.WriteLine($"[Start call API] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} url={url}.");
                var detailLos = CallApiLos(url, common);
                if (detailLos != null)
                {
                    try
                    {
                        lstLoanCreditDetailLos.Insert(new TblLoanCreditDetailLos
                        {
                            LoanID = item.LoanID,
                            LoanCreditIDOfPartner = (long)item.LoanCreditIDOfPartner,
                            CreateDate = DateTime.Now,
                            ModifyDate = DateTime.Now,
                            JsonExtra = common.ConvertObjectToJSonV2(detailLos)
                        });

                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert loanID={item.LoanID}|LoanCreditIDOfPartner={item.LoanCreditIDOfPartner}");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"[Error] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} Insert|loanID={item.LoanID}|LoanCreditIDOfPartner={item.LoanCreditIDOfPartner}|{ex.Message}-{ex.StackTrace}");
                    }
                }
                System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));
            }
            Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} done.");

        }
        private LoanBriefDetail CallApiLos(string url, Utils common)
        {
            LoanBriefDetail resultData = null;
            //var url = $"{LMS.Entites.Dtos.LOSServices.LOSConfiguration.BaseUrl}{LMS.Entites.Dtos.LOSServices.LOSConfiguration.LOS_Action_GetLoanDetail}?LoanId={loanCreditId}";
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.Parameters.Clear();
                request.AddHeader("Accept", "application/json");
                request.AddParameter("Authorization", LMS.Entites.Dtos.LOSServices.LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", "LMS", ParameterType.HttpHeader);
                var response = client.Execute<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var result = common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                    {
                        if (result.Data != null)
                        {
                            return common.ConvertJSonToObjectV2<LoanBriefDetail>(result.Data.ToString());
                        }
                    }
                }
                else
                {
                    Console.WriteLine($"[Error] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} GetLoanCreditDetail_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine($"[Error] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} GetLoanCreditDetail_Error|Api={url}|responseStatusCode={ex.Message}-{ex.StackTrace}");
            }
            return resultData;
        }
    }
}
