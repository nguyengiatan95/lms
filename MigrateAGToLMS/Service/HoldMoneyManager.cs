﻿using LMS.Common.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigrateAGToLMS.Service
{
    public class HoldMoneyManager
    {
        public long MoveData()
        {
            Console.WriteLine($"-------------- HoldMoney -------------");
            var loanHoldMoneyLMSTab = new TableHelper<LMSModel.TblHoldCustomerMoney>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            // Tab AG
            var loanHoldMoneyAgTab = new TableHelper<AG.TblHoldCustomerMoneyAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            var customerLMSTab = new TableHelper<LMSModel.TblCustomer>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);

            long latestID = 0;
            var maxTimaID = loanHoldMoneyLMSTab.SelectColumns(x => x.TimaHoldCustomerMoneyID)
                                                    .OrderByDescending(x => x.TimaHoldCustomerMoneyID)
                                                    .SetGetTop(1)
                                                    .Query().FirstOrDefault()?.TimaHoldCustomerMoneyID ?? 0;
            latestID = maxTimaID;
            List<LMSModel.TblHoldCustomerMoney> lstInsert = new List<LMSModel.TblHoldCustomerMoney>();
            int countResult = 0;
            while (true)
            {
                try
                {
                    lstInsert.Clear();
                    var lstDataAG = loanHoldMoneyAgTab.SetGetTop(2000).WhereClause(x => x.HoldCustomerMoneyID > latestID).OrderBy(x => x.HoldCustomerMoneyID).Query();
                    if (lstDataAG == null || lstDataAG.Count() == 0)
                    {
                        break;
                    }
                    var lstCustomerID = lstDataAG.Select(x => x.CustomerID).ToList();
                    var lstCustomer = customerLMSTab.WhereClause(x => lstCustomerID.Contains((long)x.TimaCustomerID)).Query();
                    foreach (var item in lstDataAG)
                    {

                        var holdMoney = new LMSModel.TblHoldCustomerMoney()
                        {
                            CreateBy = item.CreateBy,
                            CreateDate = item.CreateDate,
                            Amount = item.Amount,
                            ModifyBy = item.ModifyBy,
                            Reason = item.Reason,
                            ModifyDate = item.ModifyDate ?? item.CreateDate,
                            Status = (short)item.Status,
                            TimaHoldCustomerMoneyID = item.HoldCustomerMoneyID,
                            HoldCustomerMoneyID = item.HoldCustomerMoneyID
                        };
                        latestID = item.HoldCustomerMoneyID;
                        try
                        {
                            holdMoney.CustomerID = lstCustomer.Where(x => x.TimaCustomerID == item.CustomerID).FirstOrDefault().CustomerID;
                        }
                        catch (Exception)
                        {
                            holdMoney.CustomerID = 0;

                        }
                        lstInsert.Add(holdMoney);
                        
                    }
                    if (lstInsert.Count > 0)
                    {
                        loanHoldMoneyLMSTab.InsertBulk(lstInsert, setOffIdentity: true);
                    }

                    Console.WriteLine($"Insert HoldMoney: {lstInsert.Count} success");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"move HoldMoney error|ex={ex.Message}-{ex.StackTrace}");
                    break;
                }
            }
            Console.WriteLine($"Insert HoldMoney finished: {countResult} rows");
            return countResult;
        }
    }
}
