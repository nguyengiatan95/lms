﻿using LMS.Common.Constants;
using LMS.Common.DAL;
using LMS.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MigrateAGToLMS.Service
{
    class LenderManager
    {
        public void MoveLenderTima(long shopAGID = 0)
        {
            try
            {
                //if (DateTime.Now.Minute % 5 == 0)
                if (DateTime.Now.Minute > 0)
                {
                    Console.WriteLine($"-------------- LENDER --------------");
                    DateTime dtNow = DateTime.Now;
                    var lenderLMSTab = new TableHelper<LMSModel.TblLender>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                    var shopTimaTab = new TableHelper<AG.TblShopAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                    var lstMaxTimaLenderID = lenderLMSTab.SelectColumns(x => x.TimaLenderID)
                                                             .OrderByDescending(x => x.TimaLenderID)
                                                             .WhereClause(x => x.TimaLenderID > 0).SetGetTop(1).Query();
                    long maxTimaLenderID = 0;
                    long LasTimaLenderID = 0;
                    int maxRowGet = 2000;
                    if (lstMaxTimaLenderID != null && lstMaxTimaLenderID.Any())
                    {
                        LasTimaLenderID = 0;
                        maxTimaLenderID = LasTimaLenderID;
                    }

                    if (shopAGID > 0)
                    {
                        maxTimaLenderID = 0;
                        shopTimaTab.WhereClause(x => x.ShopID == shopAGID);
                    }
                    while (true)
                    {
                        var lstShopAG = shopTimaTab.WhereClause(x => x.ShopID > maxTimaLenderID)
                         .OrderBy(x => x.ShopID).SetGetTop(maxRowGet).Query();
                        var lstLenderNeedInsert = lstShopAG.OrderBy(x => x.ShopID).ToList();

                        if (lstLenderNeedInsert.Count > 0)
                        {
                            var lstlenderIDs = lstLenderNeedInsert.Select(x => x.ShopID).ToList();
                            var lstLenderLMS = lenderLMSTab.WhereClause(x => lstlenderIDs.Contains((int)x.TimaLenderID)).Query();
                            List<LMSModel.TblLender> lstLenderUpDate = new List<LMSModel.TblLender>();
                            List<LMSModel.TblLender> lstLenderInsert = new List<LMSModel.TblLender>();
                            foreach (var item in lstLenderNeedInsert)
                            {
                                int Status = 1;
                                if (item.ShopID == 3703 || item.IsHub.Value)
                                {
                                    Status = item.Status ?? 0;
                                }
                                else
                                {
                                    Status = (item.IsAgent != -1) ? item.IsAgent ?? 0 : -11;
                                }
                                var objLender = new LMSModel.TblLender()
                                {
                                    Address = item.Address,
                                    CreateDate = item.CreatedDate.Value,
                                    FullName = item.Name,
                                    Phone = item.Phone,
                                    Status = Status,
                                    RateInterest = item.RateInterest, // đơn vị tính trên 1 trịu
                                    NumberCard = item.PersonCardNumber,
                                    BirthDay = item.PersonBirthDay,
                                    Gender = item.PersonGender,
                                    TimaLenderID = item.ShopID,
                                    ModifyDate = dtNow,
                                    TotalMoney = item.TotalMoney.Value,
                                    TaxCode = item.TaxCode,
                                    LenderID = item.ShopID,
                                    IsHub = item.ShopID == 3703 || item.IsHub.Value ? 1 : 0,
                                    IsVerified = item.Status == 1 ? 1 : 0,
                                    UnitRate = (int)LMS.Common.Constants.Lender_UnitRate.Default,
                                    AccountBanking = item.AccountBanking,
                                    AddressOfResidence = item.AddressOfResidence,
                                    BankID = item.BankId,
                                    AffID = item.AffID,
                                    BranchOfBank = item.BranchOfBank,
                                    CardDate = item.CardNumberDate,
                                    CardPlace = item.CardNumberPlace,
                                    ContractCode = item.ContractEscrow,
                                    ContractDate = item.DateOfContractEscrow,
                                    Email = item.Email,
                                    InviteCode = item.InviteCode,
                                    LenderCareUserID = item.LenderCareUserId,
                                    LenderTakeCareUserID = item.LenderTakeCareUserId,
                                    RateAff = item.RateAff,
                                    RateLender = item.RateLender,
                                    Represent = item.Represent,
                                    RepresentPhone = item.PersonContactPhone,
                                    IsGcash = item.IsGCash == true ? 1 : 0,
                                    RegFromApp = item.RegFromApp == true ? 1 : 0,
                                    SelfEmployed = item.SelfEmployed ?? 0,
                                    IsDeposit = item.IsDepositMoney ?? 0

                                };
                                if (lstLenderLMS.Count(x => x.TimaLenderID == item.ShopID) > 0)
                                {
                                    lstLenderUpDate.Add(objLender);
                                }
                                else
                                {
                                    lstLenderInsert.Add(objLender);
                                }
                                maxTimaLenderID = item.ShopID;
                                if (shopAGID > 0)
                                {
                                    maxTimaLenderID = int.MaxValue;
                                }
                            }
                            Console.WriteLine($"[Inserting] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} bat dau insert lender");
                            if (lstLenderUpDate != null && lstLenderUpDate.Any())
                            {
                                lenderLMSTab.UpdateBulk(lstLenderUpDate);
                                Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} update lender  -  {lstLenderUpDate.Count()} - MaxID={maxTimaLenderID} ");
                            }
                            if (lstLenderInsert != null && lstLenderInsert.Any())
                            {
                                lenderLMSTab.InsertBulk(lstLenderInsert, setOffIdentity: true);
                                Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert lender  -  {lstLenderInsert.Count()} - MaxID={maxTimaLenderID} ");

                            }
                            Console.WriteLine($"[Sleep] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} Nghi 5 s");
                        }
                        else
                        {
                            Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert lender hoan tat");
                            break;
                        }
                    }
                }
                Console.WriteLine($"15 phut se chay lender");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"MoveLenderTima - ERROR {ex.Message}");
            }
        }

        public void CompareLender()
        {
            Console.WriteLine($"--------------  COMPARE LENDER --------------");
            DateTime dtNow = DateTime.Now;
            var lenderLMSTab = new TableHelper<LMSModel.TblLender>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var lstMaxTimaLenderID = lenderLMSTab.SelectColumns(x => x.TimaLenderID)
                                                       .OrderByDescending(x => x.TimaLenderID)
                                                       .WhereClause(x => x.TimaLenderID > 0).SetGetTop(1).Query();
            long maxTimaLenderID = 0;
            if (lstMaxTimaLenderID != null && lstMaxTimaLenderID.Any())
            {
                maxTimaLenderID = lstMaxTimaLenderID.First().TimaLenderID;
            }
            var loglenderLMSTab = new TableHelper<LMSModel.LenderCompare>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var shopTimaTab = new TableHelper<LMSModel.LenderCompare>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            var logCompareTab = new TableHelper<LMSModel.TblLogCompare>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            string sqlSummaryAG = $@"SELECT 
                                    COUNT(CASE WHEN Status !=1 then 1 ELSE NULL END) as WaitingVerify,
                                    COUNT(CASE WHEN Status = 1 then 1 ELSE NULL END) as IsVerified,
                                    COUNT(CASE WHEN Status = 1 and IsAgent = 1 then 1 ELSE NULL END) as Active,
                                    COUNT(CASE WHEN Status = 1 and IsAgent = 0 then 1 ELSE NULL END) as Lock,
                                    COUNT(CASE WHEN Status = 1 and IsAgent = -1 then 1 ELSE NULL END) as IsDelete
                                from tblshop(NOLOCK)  where IsHub != 1  and ShopID != 3703 and ShopID <={maxTimaLenderID}";


            string sqlSummaryLMS = @"
                                    SELECT 
                                        COUNT(CASE WHEN IsVerified !=1 then 1 ELSE NULL END) as WaitingVerify,
                                        COUNT(CASE WHEN IsVerified = 1 then 1 ELSE NULL END) as IsVerified,
                                        COUNT(CASE WHEN IsVerified = 1 and Status = 1 then 1 ELSE NULL END) as Active,
                                        COUNT(CASE WHEN IsVerified = 1 and Status = 0 then 1 ELSE NULL END) as Lock,
                                        COUNT(CASE WHEN IsVerified = 1 and Status = -11 then 1 ELSE NULL END) as IsDelete
                                    from TblLender(NOLOCK)  where IsHub != 1";

            var lenderTima = shopTimaTab.Query(sqlSummaryAG, null).FirstOrDefault();
            var lenderLMS = loglenderLMSTab.Query(sqlSummaryLMS, null).FirstOrDefault();
            List<LMSModel.TblLogCompare> lstLog = new List<LMSModel.TblLogCompare>();
            lstLog.Add(SetlogCompare((int)LogCompare_MainType.Lender, (int)LogCompare_DetailType.Lender_Verified, lenderTima.IsVerified, lenderLMS.IsVerified));
            lstLog.Add(SetlogCompare((int)LogCompare_MainType.Lender, (int)LogCompare_DetailType.Lender_VerifiedActive, lenderTima.Active, lenderLMS.Active));
            lstLog.Add(SetlogCompare((int)LogCompare_MainType.Lender, (int)LogCompare_DetailType.Lender_VerifiedLock, lenderTima.Lock, lenderLMS.Lock));
            lstLog.Add(SetlogCompare((int)LogCompare_MainType.Lender, (int)LogCompare_DetailType.Lender_WaitingStop, lenderTima.IsDelete, lenderLMS.IsDelete));
            lstLog.Add(SetlogCompare((int)LogCompare_MainType.Lender, (int)LogCompare_DetailType.Lender_WaitingVerify, lenderTima.WaitingVerify, lenderLMS.WaitingVerify));
            int mainType = (int)LogCompare_MainType.Lender;
            var lstCurrentLog = logCompareTab.WhereClause(x => x.Status == 1 && x.ForDate == DateTime.Now.Date && x.MainType == mainType).Query();
            logCompareTab.DeleteList(lstCurrentLog.ToList());
            logCompareTab.InsertBulk(lstLog);
            Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert compare lender hoan tat");
        }

        public LMSModel.TblLogCompare SetlogCompare(int mainType, int detailType, long totalAG, long totalLMS, string note = "")
        {
            return new LMSModel.TblLogCompare()
            {
                CreateDate = DateTime.Now,
                DetailType = detailType,
                DetailNote = string.IsNullOrWhiteSpace(note) ? ((LogCompare_DetailType)detailType).GetDescription() : note,
                ForDate = DateTime.Now.Date,
                MainType = mainType,
                Status = 1,
                TotalAG = totalAG,
                TotalLMS = totalLMS
            };

        }

        public void MoveUserLenderTima(long shopAGID = 0)
        {
            var lenderLMSTab = new TableHelper<LMSModel.TblLender>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var userLenderLMSTab = new TableHelper<LMSModel.TblUserLender>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var shopTimaTab = new TableHelper<AG.TblShopAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            var userShopTab = new TableHelper<AG.TblUserShop>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            var userLenderTima = new TableHelper<AG.TblUserAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            var userLenderSpicesTima = new TableHelper<AG.TblLenderSpicesConfigAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);

            var lenderUpdateLMSTab = new TableHelper<LMSModel.TblLenderOwnerShop>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var lenderSpicesLMSTab = new TableHelper<LMSModel.TblLenderSpicesConfig>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);



            var taskLenderLMS = Task.Run(() =>
            {
                if (shopAGID == 0)
                {
                    return lenderLMSTab.SelectColumns(x => x.OwnerUserID, x => x.LenderID, x => x.TimaLenderID)
                   .WhereClause(x => x.OwnerUserID <= 0).QueryAsync();
                }
                else
                {
                    return lenderLMSTab.WhereClause(x => x.TimaLenderID == shopAGID).QueryAsync();
                }
            });
            var taskUserLenderAG = userLenderTima.SelectColumns(x => x.Id, x => x.FullName, x => x.Username).QueryAsync();
            Task.WaitAll(taskLenderLMS, taskUserLenderAG);
            var lenderLMS = taskLenderLMS.Result;
            var lstUsers = taskUserLenderAG.Result;
            var dicUser = new Dictionary<string, AG.TblUserAG>();
            foreach (var item in lstUsers)
            {
                if (!dicUser.ContainsKey(item.Username))
                {
                    dicUser.Add(item.Username, item);
                }
            }
            var lenderAGID = lenderLMS.Select(x => x.TimaLenderID).Distinct().ToList();
            // chạy 2000 bản ghi 1 lần 
            int pageSize = 2000;
            int numberRow = lenderAGID.Count() / pageSize + 1;

            for (int i = 1; i <= numberRow; i++)
            {
                List<LMSModel.TblLenderOwnerShop> lstLenders = new List<LMSModel.TblLenderOwnerShop>();
                List<LMSModel.TblUserLender> lstUerLender = new List<LMSModel.TblUserLender>();
                List<LMSModel.TblLenderSpicesConfig> lenderConfigs = new List<LMSModel.TblLenderSpicesConfig>();

                var lenderIDSkip = lenderAGID.Skip((i - 1) * pageSize).Take(pageSize).ToList();
                // lấy dữ liệu AG
                var shopAGs = shopTimaTab.WhereClause(x => lenderIDSkip.Contains(x.ShopID))
                   .OrderBy(x => x.ShopID).Query();
                var lenderConfigSpicesAG = userLenderSpicesTima.WhereClause(x => lenderIDSkip.Contains(x.ShopId)).Query();
                var userLenderDeleteId = new List<int>();
                var LenderSpicesDeleteId = new List<int>();

                foreach (var item in shopAGs)
                {
                    try
                    {
                        long userCreate = 0;
                        if (item.SelfEmployed < 5 && dicUser.ContainsKey(item.Name))
                        {
                            userCreate = dicUser[item.Name].Id;
                        }
                        else if (item.TypeInvestment == 2)
                        {
                            userCreate = 66111;
                        }
                        else if (item.SelfEmployed >= 5 && dicUser.ContainsKey(item.Phone))
                        {
                            userCreate = dicUser[item.Phone].Id;
                        }
                        // list lender update
                        lstLenders.Add(new LMSModel.TblLenderOwnerShop()
                        {
                            LenderID = item.ShopID,
                            OwnerUserID = userCreate,
                            ContractType = (int)(item.TypeInvestment == 2 ? (item.Name.Contains("DT") ? Lender_ContractType.DTTima : Lender_ContractType.NAOfTima) : (item.Name.Contains("DT") ? Lender_ContractType.DTNormal : Lender_ContractType.NANormal))
                        });

                        LenderSpicesDeleteId.Add(item.ShopID);

                        if (lstUerLender.Where(x => x.UserID == userCreate).Count() == 0)
                        {
                            lstUerLender.Add(new LMSModel.TblUserLender()
                            {
                                CardDate = item.CardNumberDate,
                                CardPlace = item.CardNumberPlace,
                                CityID = item.CityId,
                                CreateDate = item.CreatedDate,
                                DistrictID = item.DistrictId,
                                Email = item.Email,
                                FullName = item.PersonContact,
                                Gender = item.PersonGender,
                                IsVerified = item.Status == 1 ? 1 : 0,
                                ModifyDate = item.CreatedDate,
                                NumberCard = item.PersonCardNumber,
                                PermanentResidenceAddress = item.Address,
                                TemporaryResidenceAddress = item.AddressOfResidence,
                                Phone = item.Phone,
                                Status = item.Status == (int)LenderUser_Status.Active || item.Status == (int)LenderUser_Status.UnActive ? item.Status : (int)LenderUser_Status.Waiting,
                                UserID = userCreate,
                                WardID = item.WardId,
                            });
                            userLenderDeleteId.Add((int)userCreate);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    //var userIDs = userShopAGs.Where(x => x.ShopID == item.ShopID).Select(x => x.UserID).ToList();
                    //var ownerShop = dicUser.Where(x => userIDs.Contains(x.Key)).FirstOrDefault();
                    // các case k có trong groupid = 4 thì gán luôn bằng người tạo

                }

                foreach (var item in lenderConfigSpicesAG)
                {
                    lenderConfigs.Add(new LMSModel.TblLenderSpicesConfig()
                    {
                        LenderID = item.ShopId,
                        LoanTimes = "[" + item.LoanTimes + "]",
                        MoneyMax = item.MoneyMax,
                        MoneyMin = item.MoneyMin,
                        RateType = item.RateType.ToString(),
                        UserID = lstLenders.Where(x => x.LenderID == item.ShopId).FirstOrDefault().OwnerUserID
                    });
                }
                // cập nhật OwnerUser của tbllenderLMS
                lenderUpdateLMSTab.UpdateBulk(lstLenders);
                Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} update lender  -  {lstLenders.Count()}");
                // xóa dữ liệu userLenderLMS
                var userLenderIds = userLenderLMSTab.DeleteWhere(x => userLenderDeleteId.Contains((int)x.UserID));
                Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} xoa userLender  -  {userLenderDeleteId.Count()}");
                // insert dữ liệu mới
                userLenderLMSTab.InsertBulk(lstUerLender);

                lenderSpicesLMSTab.DeleteWhere(x => LenderSpicesDeleteId.Contains((int)x.LenderID));
                lenderSpicesLMSTab.InsertBulk(lenderConfigs);
                Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} them userLender  -  {lstUerLender.Count()}");
            }
            Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert userLender hoan tat");
        }

    }
}
