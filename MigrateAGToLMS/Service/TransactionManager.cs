﻿using LMS.Common.DAL;
using LMS.Common.Helper;
using MigrateAGToLMS.AG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MigrateAGToLMS.Service
{
    public class TransactionManager
    {
        public void MoveTransactionLoanTima(List<int> loanIdsUpdate = null)
        {
            try
            {
                Console.WriteLine($"-------------- TransactionLoan -------------");
                DateTime dtNow = DateTime.Now;
                var transactionLMSTab = new TableHelper<LMSModel.TblTransaction>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                var loanLMSTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                var transactionTimaTab = new TableHelper<AG.TblTransactionLoanAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                var loanDebtLMSTab = new TableHelper<LMSModel.TblDebt>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);


                long maxTimaTransactionID = 0;
                int maxRowGet = 2000;
                // lấy tran ag
                var taskMaxTimaTransactionIDLMS = Task.Run(() =>
                {
                    return transactionLMSTab.SelectColumns(x => x.TimaTransactionID)
                                                       .OrderByDescending(x => x.TimaTransactionID)
                                                       .WhereClause(x => x.TimaTransactionID > 0).SetGetTop(1).Query();
                });

                // lấy shop hub + tima
                var taskShopTima = Task.Run(() =>
                {
                    var lstShopTima = Ultil.StaticEntity.LstShopTimaHubAG;
                    if (lstShopTima != null && lstShopTima.Count > 0) return lstShopTima;
                    var shopTimaTab = new TableHelper<AG.TblShopAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                    return shopTimaTab.WhereClause(x => x.ShopID == 3703 || x.IsHub == true).Query();
                });
                Task.WaitAll(taskMaxTimaTransactionIDLMS, taskShopTima);
                var lstShop = taskShopTima.Result.Select(x => x.ShopID);
                var lstMaxTimaTransactionID = taskMaxTimaTransactionIDLMS.Result;
                // max bản ghi đã đồng bô
                if (lstMaxTimaTransactionID != null && lstMaxTimaTransactionID.Any())
                {
                    maxTimaTransactionID = lstMaxTimaTransactionID.First().TimaTransactionID;
                }


                if (loanIdsUpdate != null)
                {
                    maxTimaTransactionID = 0;
                    var lstLmsLoanIds = loanLMSTab.SelectColumns(x => x.LoanID).WhereClause(x => loanIdsUpdate.Contains((int)x.TimaLoanID)).Query().Select(x => x.LoanID);
                    loanDebtLMSTab.DeleteWhere(x => lstLmsLoanIds.Contains(x.ReferID));
                    transactionLMSTab.DeleteWhere(x => lstLmsLoanIds.Contains(x.LoanID) && x.CreateDate < dtNow);
                }

                while (true)
                {
                    if (loanIdsUpdate != null)
                    {
                        transactionTimaTab.WhereClause(x => loanIdsUpdate.Contains(x.LoanID));
                    }
                    var lstTransactionAG = transactionTimaTab.WhereClause(x => x.ID > maxTimaTransactionID && lstShop.Contains(x.ShopID))
                                                             .OrderBy(x => x.ID).SetGetTop(maxRowGet).Query();
                    var lstTransactionNeedInsert = lstTransactionAG.OrderBy(x => x.ID).ToList();
                    if (lstTransactionNeedInsert.Count > 0)
                    {
                        List<LMSModel.TblTransaction> lstTransaction = new List<LMSModel.TblTransaction>();
                        List<LMSModel.TblDebt> lstLoanDebt = new List<LMSModel.TblDebt>();

                        var lstLoanIds = lstTransactionNeedInsert.Select(x => x.LoanID).ToList();

                        // các rate của loan lms
                        var lstLoanLMS = loanLMSTab.SelectColumns(x => x.CustomerID, x => x.TimaLoanID, x => x.LoanID, x => x.LenderID,
                            x => x.RateConsultant, x => x.RateInterest, x => x.RateService)
                                                .WhereClause(x => lstLoanIds.Contains((int)x.TimaLoanID)).Query();

                        var dicLoan = lstLoanLMS.ToDictionary(x => x.TimaLoanID, x => x);
                        var lstLmsLoanIds = dicLoan.Values.Select(x => x.LoanID).ToList();

                        foreach (var item in lstTransactionNeedInsert)
                        {
                            if (dicLoan.ContainsKey(item.LoanID))
                            {
                                var objLoan = dicLoan[item.LoanID];
                                ConvertActionAGToLMS(lstTransaction, item, objLoan, lstLoanDebt);
                            }
                        }
                        maxTimaTransactionID = lstTransactionNeedInsert.Max(x => x.ID);
                        Console.WriteLine($"[Inserting] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} bat dau insert TransactionLoan");
                        transactionLMSTab.InsertBulk(lstTransaction);
                        if (lstLoanDebt != null && lstLoanDebt.Any())
                        {

                            loanDebtLMSTab.InsertBulk(lstLoanDebt);
                            Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert lstLoanDebt  -  {lstLoanDebt.Count()} ");
                        }
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert lstTransaction  -  {lstTransaction.Count()} ");
                    }
                    else
                    {
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert TransactionLoan hoan tat");
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"TransactionLoan - ERROR {ex.Message}");
            }
        }

        void ConvertActionAGToLMS(List<LMSModel.TblTransaction> lstTransaction, AG.TblTransactionLoanAG tranAG, LMSModel.TblLoan objLoan, List<LMSModel.TblDebt> lstLoanDebt)
        {

            switch ((AG.AG_TransactionLoan_ActionID)tranAG.ActionID)
            {
                case AG.AG_TransactionLoan_ActionID.ChoVay:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.ChoVay;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Original;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.SuaHd:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.SuaHd;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Original;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.DongLai:
                    if (tranAG.MoneyInterest != 0)
                    {
                        var lstTranCalculate = CalculateMoneyInterestFromAG(tranAG, objLoan, (int)LMS.Common.Constants.Transaction_Action.DongLai, tranAG.MoneyInterest);
                        lstTransaction.AddRange(lstTranCalculate);
                    }
                    if (tranAG.OtherMoney != 0)
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.DongLai;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineInterestLate;
                        objTranLMS.TotalMoney = tranAG.OtherMoney;
                        lstTransaction.Add(objTranLMS);
                        var loanDebt = GetDefaultLoanDebt(tranAG, objLoan: objLoan);
                        lstLoanDebt.Add(loanDebt);
                    }
                    break;
                case AG.AG_TransactionLoan_ActionID.HuyDongLai:
                    if (tranAG.MoneyInterest != 0)
                    {
                        var lstTranCalculate = CalculateMoneyInterestFromAG(tranAG, objLoan, (int)LMS.Common.Constants.Transaction_Action.HuyDongLai, tranAG.MoneyInterest);
                        lstTransaction.AddRange(lstTranCalculate);
                    }
                    if (tranAG.OtherMoney != 0)
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.HuyDongLai;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineInterestLate;
                        objTranLMS.TotalMoney = tranAG.OtherMoney;
                        var loanDebt = GetDefaultLoanDebt(tranAG, (int)LMS.Common.Constants.Debt_TypeID.CancelCustomerPayFineLate, objLoan: objLoan);
                        lstLoanDebt.Add(loanDebt);
                    }
                    break;
                case AG.AG_TransactionLoan_ActionID.TraGoc:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = objTranLMS.PaymentScheduleID > 0
                            ? (int)LMS.Common.Constants.Transaction_Action.DongLai :
                             (int)LMS.Common.Constants.Transaction_Action.TraGoc;

                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Original;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.VayThemGoc:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.VayThemGoc;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Original;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.HuyTraGoc:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = objTranLMS.PaymentScheduleID > 0
                            ? (int)LMS.Common.Constants.Transaction_Action.HuyDongLai :
                             (int)LMS.Common.Constants.Transaction_Action.HuyTraGoc;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Original;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.HuyVayThemGoc:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.HuyVayThemGoc;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Original;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.DongHd:
                    if (tranAG.MoneyInterest != 0)
                    {
                        var lstTranCalculate = CalculateMoneyInterestFromAG(tranAG, objLoan, (int)LMS.Common.Constants.Transaction_Action.DongHd, tranAG.MoneyInterest);
                        lstTransaction.AddRange(lstTranCalculate);
                    }
                    if (tranAG.OtherMoney != 0)
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.DongHd;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineInterestLate;
                        objTranLMS.TotalMoney = tranAG.OtherMoney;
                        lstTransaction.Add(objTranLMS);

                        var loanDebt = GetDefaultLoanDebt(tranAG, objLoan: objLoan);
                        lstLoanDebt.Add(loanDebt);
                    }
                    if (tranAG.MoneyAdd - tranAG.MoneySub - tranAG.OtherMoney - tranAG.MoneyInterest != 0)
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.DongHd;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Original;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub - tranAG.OtherMoney - tranAG.MoneyInterest;
                        lstTransaction.Add(objTranLMS);
                    }
                    break;
                case AG.AG_TransactionLoan_ActionID.NoLai:
                    {
                        var lstTranCalculate = CalculateMoneyInterestFromAG(tranAG, objLoan, (int)LMS.Common.Constants.Transaction_Action.NoLai, tranAG.MoneyAdd - tranAG.MoneySub);
                        lstTransaction.AddRange(lstTranCalculate);
                        var loanDebt = GetDefaultLoanDebt(tranAG, (int)LMS.Common.Constants.Debt_TypeID.CustomerDebt, objLoan: objLoan);
                        lstLoanDebt.Add(loanDebt);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.TraNo:
                    {
                        var lstTranCalculate = CalculateMoneyInterestFromAG(tranAG, objLoan, (int)LMS.Common.Constants.Transaction_Action.TraNo, tranAG.MoneyAdd - tranAG.MoneySub);
                        lstTransaction.AddRange(lstTranCalculate);
                        var loanDebt = GetDefaultLoanDebt(tranAG, (int)LMS.Common.Constants.Debt_TypeID.PayCustomerDebt, objLoan: objLoan);
                        lstLoanDebt.Add(loanDebt);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.GiaHan:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.GiaHan;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Original;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.HuyDongHd:
                    if (tranAG.MoneyInterest != 0)
                    {
                        var lstTranCalculate = CalculateMoneyInterestFromAG(tranAG, objLoan, (int)LMS.Common.Constants.Transaction_Action.HuyDongHd, tranAG.MoneyInterest);
                        lstTransaction.AddRange(lstTranCalculate);
                    }
                    if (tranAG.OtherMoney != 0)
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.HuyDongHd;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineInterestLate;
                        objTranLMS.TotalMoney = tranAG.OtherMoney;
                        lstTransaction.Add(objTranLMS);
                        var loanDebt = GetDefaultLoanDebt(tranAG, (int)LMS.Common.Constants.Debt_TypeID.CancelCustomerPayFineLate, objLoan: objLoan);
                        lstLoanDebt.Add(loanDebt);
                    }
                    if (tranAG.MoneyAdd - tranAG.MoneySub - tranAG.OtherMoney - tranAG.MoneyInterest != 0)
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.HuyDongHd;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Original;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub - tranAG.OtherMoney - tranAG.MoneyInterest;
                        lstTransaction.Add(objTranLMS);
                    }
                    break;
                case AG.AG_TransactionLoan_ActionID.ThanhLy:
                    if (tranAG.MoneyInterest != 0)
                    {
                        var lstTranCalculate = CalculateMoneyInterestFromAG(tranAG, objLoan, (int)LMS.Common.Constants.Transaction_Action.ThanhLy, tranAG.MoneyInterest);
                        lstTransaction.AddRange(lstTranCalculate);
                    }
                    if (tranAG.OtherMoney != 0)
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.ThanhLy;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineInterestLate;
                        objTranLMS.TotalMoney = tranAG.OtherMoney;
                        lstTransaction.Add(objTranLMS);
                        var loanDebt = GetDefaultLoanDebt(tranAG, objLoan: objLoan);
                        lstLoanDebt.Add(loanDebt);
                    }
                    if (tranAG.MoneyAdd - tranAG.MoneySub - tranAG.OtherMoney - tranAG.MoneyInterest != 0)
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.ThanhLy;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Original;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub - tranAG.OtherMoney - tranAG.MoneyInterest;
                        lstTransaction.Add(objTranLMS);
                    }
                    break;
                case AG.AG_TransactionLoan_ActionID.HuyChoVay:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.HuyChoVay;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Original;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.HuyNoLai:
                    {
                        var lstTranCalculate = CalculateMoneyInterestFromAG(tranAG, objLoan, (int)LMS.Common.Constants.Transaction_Action.HuyNoLai, tranAG.MoneyAdd - tranAG.MoneySub);
                        lstTransaction.AddRange(lstTranCalculate);
                        var loanDebt = GetDefaultLoanDebt(tranAG, (int)LMS.Common.Constants.Debt_TypeID.CancelCustomerDebt, objLoan: objLoan);
                        lstLoanDebt.Add(loanDebt);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.HuyTraNo:
                    {
                        var lstTranCalculate = CalculateMoneyInterestFromAG(tranAG, objLoan, (int)LMS.Common.Constants.Transaction_Action.HuyTraNo, tranAG.MoneyAdd - tranAG.MoneySub);
                        lstTransaction.AddRange(lstTranCalculate);
                        var loanDebt = GetDefaultLoanDebt(tranAG, (int)LMS.Common.Constants.Debt_TypeID.CancelPayCustomerDebt, objLoan: objLoan);
                        lstLoanDebt.Add(loanDebt);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.HuyThanhLy:
                    if (tranAG.MoneyInterest != 0)
                    {
                        var lstTranCalculate = CalculateMoneyInterestFromAG(tranAG, objLoan, (int)LMS.Common.Constants.Transaction_Action.HuyThanhLy, tranAG.MoneyInterest);
                        lstTransaction.AddRange(lstTranCalculate);
                    }
                    if (tranAG.OtherMoney != 0)
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.HuyThanhLy;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineInterestLate;
                        objTranLMS.TotalMoney = tranAG.OtherMoney;
                        lstTransaction.Add(objTranLMS);
                        var loanDebt = GetDefaultLoanDebt(tranAG, (int)LMS.Common.Constants.Debt_TypeID.CancelCustomerFineLate, objLoan: objLoan);
                        lstLoanDebt.Add(loanDebt);
                    }
                    if (tranAG.MoneyAdd - tranAG.MoneySub - tranAG.OtherMoney - tranAG.MoneyInterest != 0)
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.HuyThanhLy;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Original;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub - tranAG.OtherMoney - tranAG.MoneyInterest;
                        lstTransaction.Add(objTranLMS);
                    }
                    break;
                case AG.AG_TransactionLoan_ActionID.ChuyenNoXau:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.ChuyenNoXau;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Original;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.BoNoXau:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.BoNoXau;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Original;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.TienTuVanThuTruoc:
                    {
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.PhiPhatTraTruocGoc:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.PhiPhatTraTruocGoc;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineOriginal;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.HuyPhiPhatTraTruocGoc:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.HuyPhiPhatTraTruocGoc;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineOriginal;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.InsuranceMoneyFees:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.PhiBaoHiem;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.InsuranceFee;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.DebitMoneyFeeFineLate:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.GhiNoPhiPhatMuon;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineLate;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        var loanDebt = GetDefaultLoanDebt(tranAG, (int)LMS.Common.Constants.Debt_TypeID.CustomerFineLate, objLoan: objLoan);
                        lstLoanDebt.Add(loanDebt);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.PayDebitMoneyFeeFineLate:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.TraNoPhiPhatMuon;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineLate;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        var loanDebt = GetDefaultLoanDebt(tranAG, (int)LMS.Common.Constants.Debt_TypeID.CustomerPayFineLate, objLoan: objLoan);
                        lstLoanDebt.Add(loanDebt);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.DeleteTransactionMoneyFeeLate:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.DeleteTransactionMoneyFeeLate;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineLate;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        var loanDebt = GetDefaultLoanDebt(tranAG, (int)LMS.Common.Constants.Debt_TypeID.CancelCustomerFineLate, objLoan: objLoan);
                        lstLoanDebt.Add(loanDebt);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.LiquidationLoanInsurance:
                    {
                        if (tranAG.MoneyInterest != 0)
                        {
                            var lstTranCalculate = CalculateMoneyInterestFromAG(tranAG, objLoan, (int)LMS.Common.Constants.Transaction_Action.ThanhLyBaoHiem, tranAG.MoneyInterest);
                            lstTransaction.AddRange(lstTranCalculate);
                        }
                        if (tranAG.OtherMoney != 0)
                        {
                            var objTranLMS = GetDefaultTran(tranAG, objLoan);
                            objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.ThanhLyBaoHiem;
                            objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineInterestLate;
                            objTranLMS.TotalMoney = tranAG.OtherMoney;
                            lstTransaction.Add(objTranLMS);
                            var loanDebt = GetDefaultLoanDebt(tranAG, objLoan: objLoan);
                            lstLoanDebt.Add(loanDebt);
                        }
                        if (tranAG.MoneyAdd - tranAG.MoneySub - tranAG.OtherMoney - tranAG.MoneyInterest != 0)
                        {
                            var objTranLMS = GetDefaultTran(tranAG, objLoan);
                            objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.ThanhLyBaoHiem;
                            objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Original;
                            objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub - tranAG.OtherMoney - tranAG.MoneyInterest;
                            lstTransaction.Add(objTranLMS);
                        }
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.DeleteTransactionPayMoneyFeeLate:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.DeleteTransactionPayMoneyFeeLate;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineLate;
                        objTranLMS.TotalMoney = tranAG.OtherMoney;
                        lstTransaction.Add(objTranLMS);
                        var loanDebt = GetDefaultLoanDebt(tranAG, (int)LMS.Common.Constants.Debt_TypeID.CancelCustomerPayFineLate, objLoan: objLoan);
                        lstLoanDebt.Add(loanDebt);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.Exemption:
                    if (tranAG.MoneyInterest != 0)
                    {
                        var lstTranCalculate = CalculateMoneyInterestFromAG(tranAG, objLoan, (int)LMS.Common.Constants.Transaction_Action.Exemption, tranAG.MoneyInterest);
                        lstTransaction.AddRange(lstTranCalculate);
                    }
                    if (tranAG.OtherMoney != 0)
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.Exemption;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineInterestLate;
                        objTranLMS.TotalMoney = tranAG.OtherMoney;
                        lstTransaction.Add(objTranLMS);
                        var loanDebt = GetDefaultLoanDebt(tranAG, objLoan: objLoan);
                        lstLoanDebt.Add(loanDebt);
                    }
                    if (tranAG.MoneyAdd - tranAG.MoneySub - tranAG.OtherMoney - tranAG.MoneyInterest != 0)
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.Exemption;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Original;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub - tranAG.OtherMoney - tranAG.MoneyInterest;
                        lstTransaction.Add(objTranLMS);
                    }
                    break;
                case AG.AG_TransactionLoan_ActionID.PayPartial_Interest:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.DongLai;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Interest;
                        objTranLMS.TotalMoney = tranAG.MoneyInterest;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.PayPartial_Consultant:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.DongLai;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Consultant;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.PayPartial_Service:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.DongLai;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Service;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case AG.AG_TransactionLoan_ActionID.DeletePayPartial_Interest:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.HuyDongLai;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Interest;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }

                case AG.AG_TransactionLoan_ActionID.DeletePayPartial_Consultant:
                    {
                        var objTranLMS = GetDefaultTran(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.HuyDongLai;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Consultant;
                        objTranLMS.TotalMoney = tranAG.MoneyAdd - tranAG.MoneySub;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                default:
                    break;
            }
        }

        LMSModel.TblTransaction GetDefaultTran(AG.TblTransactionLoanAG tranAG, LMSModel.TblLoan objLoan)
        {
            return new LMSModel.TblTransaction()
            {
                CreateDate = tranAG.CreateDate,
                CustomerID = objLoan.CustomerID.Value,
                LoanID = objLoan.LoanID,
                LenderID = objLoan.LenderID.Value,
                TimaTransactionID = tranAG.ID,
                PaymentScheduleID = tranAG.PaymentId,
                UserID = tranAG.UserID,
                ShopID = objLoan.OwnerShopID ?? 0
            };

        }

        public void MoveTransactionInsurance()
        {
            try
            {
                Console.WriteLine($"-------------- TransactionInsurance -------------");
                DateTime dtNow = DateTime.Now;
                var transactionLMSTab = new TableHelper<LMSModel.TblTransaction>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                var loanLMSTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                var transactionTimaTab = new TableHelper<AG.TblTransactionIndemnifyLoanInsurrance>().SetConnectString(Ultil.Constant.ConnectionStringAG);


                long maxTimaInsuranceTransactionID = 0;
                int maxRowGet = 2000;
                var lstMaxTimaTransactionID = transactionLMSTab.SelectColumns(x => x.TimaInsuranceTransactionID)
                                                       .OrderByDescending(x => x.TimaInsuranceTransactionID)
                                                       .WhereClause(x => x.TimaInsuranceTransactionID > 0).SetGetTop(1).Query();
                if (lstMaxTimaTransactionID != null && lstMaxTimaTransactionID.Any())
                {
                    maxTimaInsuranceTransactionID = lstMaxTimaTransactionID.First().TimaInsuranceTransactionID;
                }
                while (true)
                {
                    var lstTransactionAG = transactionTimaTab.WhereClause(x => x.TransactionIndemnifyLoanInsurranceID > maxTimaInsuranceTransactionID)
                                                           .OrderBy(x => x.TransactionIndemnifyLoanInsurranceID).SetGetTop(maxRowGet).Query();
                    var lstTransactionNeedInsert = lstTransactionAG.OrderBy(x => x.TransactionIndemnifyLoanInsurranceID).ToList();
                    if (lstTransactionNeedInsert.Count > 0)
                    {
                        List<LMSModel.TblTransaction> lstTransaction = new List<LMSModel.TblTransaction>();
                        var lstLoanIds = lstTransactionAG.Select(x => x.LoanID).ToList();
                        var lstLoanLMS = loanLMSTab.SelectColumns(x => x.CustomerID, x => x.TimaLoanID, x => x.LoanID, x => x.LenderID, x => x.RateConsultant, x => x.RateInterest, x => x.RateService)
                                                .WhereClause(x => lstLoanIds.Contains((int)x.TimaLoanID)).Query();
                        var dicLoan = lstLoanLMS.ToDictionary(x => x.TimaLoanID, x => x);
                        foreach (var item in lstTransactionAG)
                        {

                            if (dicLoan.ContainsKey(item.LoanID))
                            {
                                var objLoan = dicLoan[item.LoanID];
                                ConvertActionInsuranceAGToLMS(lstTransaction, item, objLoan);
                            }
                            maxTimaInsuranceTransactionID = item.TransactionIndemnifyLoanInsurranceID;
                        }
                        Console.WriteLine($"[Inserting] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} bat dau insert TransactionInsurrance");
                        transactionLMSTab.InsertBulk(lstTransaction);
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert TransactionInsurrance  -  {lstTransaction.Count()} ");
                    }
                    else
                    {
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert TransactionInsurrance hoan tat");
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"TransactionInsurrance - ERROR {ex.Message}");

            }
        }

        void ConvertActionInsuranceAGToLMS(List<LMSModel.TblTransaction> lstTransaction, AG.TblTransactionIndemnifyLoanInsurrance tranAG, LMSModel.TblLoan objLoan)
        {

            switch ((TransactionIndemnifyLoanInsurance_ActionID)tranAG.ActionID)
            {
                case TransactionIndemnifyLoanInsurance_ActionID.PayOriginal:
                    {
                        var objTranLMS = GetDefaultTranInsurance(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.ReceiptInsurance;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Original;
                        objTranLMS.TotalMoney = tranAG.TotalMoney;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case TransactionIndemnifyLoanInsurance_ActionID.PayInterest:
                    {
                        var objTranLMS = GetDefaultTranInsurance(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.ReceiptInsurance;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Interest;
                        objTranLMS.TotalMoney = tranAG.TotalMoney;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case TransactionIndemnifyLoanInsurance_ActionID.PayConsultant:
                    {
                        var objTranLMS = GetDefaultTranInsurance(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.ReceiptInsurance;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Consultant;
                        objTranLMS.TotalMoney = tranAG.TotalMoney;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case TransactionIndemnifyLoanInsurance_ActionID.PayService:
                    {
                        var objTranLMS = GetDefaultTranInsurance(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.ReceiptInsurance;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Service;
                        objTranLMS.TotalMoney = tranAG.TotalMoney;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case TransactionIndemnifyLoanInsurance_ActionID.PayFineOriginalSoon:
                    {
                        var objTranLMS = GetDefaultTranInsurance(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.ReceiptInsurance;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineOriginal;
                        objTranLMS.TotalMoney = tranAG.TotalMoney;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case TransactionIndemnifyLoanInsurance_ActionID.PayDebit:
                    {
                        var objTranLMS = GetDefaultTranInsurance(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.ReceiptInsurance;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineInterestLate;
                        objTranLMS.TotalMoney = tranAG.TotalMoney;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                case TransactionIndemnifyLoanInsurance_ActionID.PayFineLate:
                    {
                        var objTranLMS = GetDefaultTranInsurance(tranAG, objLoan);
                        objTranLMS.ActionID = (int)LMS.Common.Constants.Transaction_Action.ReceiptInsurance;
                        objTranLMS.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineLate;
                        objTranLMS.TotalMoney = tranAG.TotalMoney;
                        lstTransaction.Add(objTranLMS);
                        break;
                    }
                default:
                    break;
            }
        }

        LMSModel.TblTransaction GetDefaultTranInsurance(AG.TblTransactionIndemnifyLoanInsurrance tranAG, LMSModel.TblLoan objLoan)
        {
            return new LMSModel.TblTransaction()
            {
                CreateDate = tranAG.CreateDate,
                CustomerID = objLoan.CustomerID.Value,
                LoanID = objLoan.LoanID,
                LenderID = objLoan.LenderID.Value,
                TimaInsuranceTransactionID = tranAG.TransactionIndemnifyLoanInsurranceID,
                PaymentScheduleID = tranAG.PaymentScheduleID,
                UserID = tranAG.UserID,
            };
        }

        LMSModel.TblDebt GetDefaultLoanDebt(AG.TblTransactionLoanAG tranAG, int LoanDebt_TypeID = (int)LMS.Common.Constants.Debt_TypeID.CustomerPayFineLate, LMSModel.TblLoan objLoan = null)
        {
            long totalMoney = tranAG.DebitMoneyFine != 0 ? tranAG.DebitMoneyFine : (tranAG.OtherMoney != 0 ? Math.Abs(tranAG.OtherMoney) : Math.Abs(tranAG.MoneyAdd - tranAG.MoneySub));
            if (LoanDebt_TypeID == (int)LMS.Common.Constants.Debt_TypeID.CustomerPayFineLate)
            {
                // trả nợ thì số tiền âm
                if (totalMoney > 0)
                    totalMoney *= -1;
            }
            else if (LoanDebt_TypeID == (int)LMS.Common.Constants.Debt_TypeID.CancelCustomerPayFineLate)
            {
                // hủy trả nợ phí số tiền dương
                if (totalMoney < 0)
                {
                    totalMoney *= -1;
                }
            }
            return new LMSModel.TblDebt()
            {
                CreateBy = tranAG.UserID,
                CreateDate = tranAG.CreateDate,
                ReferID = objLoan.LoanID,
                Status = (int)LMS.Common.Constants.StatusCommon.Active,
                TargetID = objLoan.CustomerID,
                TotalMoney = totalMoney,
                TypeID = LoanDebt_TypeID,
                Description = (tranAG.UserID > 1 ? "" : "Hệ thống : ") + ((LMS.Common.Constants.Debt_TypeID)LoanDebt_TypeID).GetDescription()
            };
        }

        List<LMSModel.TblTransaction> CalculateMoneyInterestFromAG(AG.TblTransactionLoanAG tranAG, LMSModel.TblLoan objLoan, int ActionID, long totalMoney)
        {
            List<LMSModel.TblTransaction> lstTran = new List<LMSModel.TblTransaction>();
            long interest = 0;
            long service = 0;
            if (objLoan.RateInterest > 0)
            {
                var tranInterest = GetDefaultTran(tranAG, objLoan);
                interest = (long)(totalMoney * objLoan.RateInterest / (objLoan.RateInterest + objLoan.RateConsultant + objLoan.RateService));
                tranInterest.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Interest;
                tranInterest.TotalMoney = interest;
                tranInterest.ActionID = ActionID;
                lstTran.Add(tranInterest);
            }
            if (objLoan.RateService > 0)
            {
                var tranService = GetDefaultTran(tranAG, objLoan);
                service = (long)(totalMoney * objLoan.RateService / (objLoan.RateInterest + objLoan.RateConsultant + objLoan.RateService));
                tranService.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Service;
                tranService.TotalMoney = service;
                tranService.ActionID = ActionID;
                lstTran.Add(tranService);
            }
            if (objLoan.RateConsultant > 0)
            {
                var tranService = GetDefaultTran(tranAG, objLoan);
                tranService.MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.Consultant;
                tranService.TotalMoney = totalMoney - interest - service;
                tranService.ActionID = ActionID;
                lstTran.Add(tranService);
            }
            return lstTran;
        }

        public void SplitTransactionTimaAndLenderFineOriginal()
        {
            var txnLoanLMSTab = new TableHelper<LMSModel.TblTransaction>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var splitTransactionLMSTab = new TableHelper<LMSModel.TblSplitTransactionLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var transactionTimaTab = new TableHelper<AG.TblTransactionLoanAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            var txnTimaTab = new TableHelper<Objects.TransactionDetailMoneyFineOriginal>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            var txnLenderTab = new TableHelper<Objects.TransactionDetailMoneyFineOriginal>().SetConnectString(Ultil.Constant.ConnectionStringAG);

            var sqlTxnTima = @"SELECT TL.ID TxnID, TL.AgencyId ShopID, LC.LoanID, TL.MoneyAdd, TL.MoneySub, TL.CreateDate, TL.Note, TL.ActionID
                                FROM tblTransactionLoan(NOLOCK) TL
                                INNER JOIN tblLoanCredit(NOLOCK) LC ON LC.LoanID = TL.LoanID
                                WHERE (TL.ActionID = 22 OR TL.ActionID = 23) AND TL.ShopID = 3703 AND TL.CreateDate < '2021-04-01'
                                order by TL.CreateDate";

            var sqlTxnLender = @"SELECT S.Name ShopName, TL.ShopID, LC.LoanID, TL.MoneyAdd, TL.MoneySub, TL.CreateDate, TL.Note, TL.ActionID 
                                FROM tblTransactionLoan(NOLOCK) TL
                                INNER JOIN tblLoanCredit(NOLOCK) LC ON LC.AgentLoanID = TL.LoanID
                                INNER JOIN tblShop(NOLOCK) S ON TL.ShopID = S.ShopID
                                WHERE (TL.ActionID = 22 OR TL.ActionID = 23) AND TL.ShopID != 3703 AND S.IsHub = 0 AND TL.CreateDate < '2021-04-01'
                                order by TL.CreateDate";
            //var keyDictTxn = ""; // ngày tạo _ shop id _ loanId _ actionID
            var dictDataTima = txnTimaTab.Query(sqlTxnTima, null).ToDictionary(x => $"{x.CreateDate:yyyyMMddHHmmss}_{x.ShopID}_{x.LoanID}_{x.ActionID}", x => x);
            var dictDataLender = txnLenderTab.Query(sqlTxnLender, null).ToDictionary(x => $"{x.CreateDate:yyyyMMddHHmmss}_{x.ShopID}_{x.LoanID}_{x.ActionID}", x => x);
            Dictionary<long, List<LMSModel.TblSplitTransactionLoan>> dictTxnLMSInsert = new Dictionary<long, List<LMSModel.TblSplitTransactionLoan>>();
            List<LMSModel.TblSplitTransactionLoan> lstTxnLmsInsert = new List<LMSModel.TblSplitTransactionLoan>();
            List<long> lstTxnTimaID = new List<long>();

            foreach (var item in dictDataTima)
            {
                lstTxnTimaID.Add(item.Value.TxnID);
                LMSModel.TblSplitTransactionLoan txnTimaLms = new LMSModel.TblSplitTransactionLoan
                {
                    ReferTxnID = item.Value.TxnID,
                    ActionID = item.Value.ActionID,
                    CreateDate = item.Value.CreateDate,
                    LenderID = 3703,
                    LoanID = item.Value.LoanID,
                    MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineOriginal,
                    TotalMoney = item.Value.MoneyAdd > 0 ? item.Value.MoneyAdd : item.Value.MoneySub * -1 // moneysub * -1 hủy giao dịch tiền phải âm, 23 là hủy
                };
                if (!dictTxnLMSInsert.ContainsKey(item.Value.TxnID))
                {
                    dictTxnLMSInsert.Add(item.Value.TxnID, new List<LMSModel.TblSplitTransactionLoan>());
                }
                // nếu ko có giao dịch trong lender, tima hưởng toàn bộ
                // action 22, 23 ag tương đương action lms
                var txnDetailLender = dictDataLender.GetValueOrDefault(item.Key);
                if (txnDetailLender != null && txnDetailLender.ShopID > 0)
                {
                    LMSModel.TblSplitTransactionLoan txnLenderLms = new LMSModel.TblSplitTransactionLoan
                    {
                        ReferTxnID = item.Value.TxnID,
                        ActionID = item.Value.ActionID,
                        CreateDate = item.Value.CreateDate,
                        LenderID = txnDetailLender.ShopID,
                        LoanID = item.Value.LoanID,
                        MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineOriginal,
                        TotalMoney = txnDetailLender.MoneyAdd > 0 ? txnDetailLender.MoneyAdd : txnDetailLender.MoneySub * -1 // moneysub * -1 hủy giao dịch tiền phải âm
                    };

                    // tima hưởng số còn lại
                    txnTimaLms.TotalMoney -= txnLenderLms.TotalMoney;
                    dictTxnLMSInsert[item.Value.TxnID].Add(txnLenderLms);
                }
                dictTxnLMSInsert[item.Value.TxnID].Add(txnTimaLms);
            }
            var lstBatchTxn = lstTxnTimaID.Batch(2000);
            foreach (var lstTxnID in lstBatchTxn)
            {
                lstTxnLmsInsert.Clear();
                var txnLms = txnLoanLMSTab.SelectColumns(x => x.TransactionID, x => x.LoanID, x => x.LenderID, x => x.TimaTransactionID)
                                          .WhereClause(x => lstTxnID.Contains(x.TimaTransactionID)).Query();
                foreach (var itemTxnLMs in txnLms)
                {
                    var lstInsert = dictTxnLMSInsert.GetValueOrDefault(itemTxnLMs.TimaTransactionID);
                    if (lstInsert != null && lstInsert.Count > 0)
                    {
                        foreach (var txn in lstInsert)
                        {
                            txn.LoanID = itemTxnLMs.LoanID;
                            txn.ReferTxnID = itemTxnLMs.TransactionID;
                            lstTxnLmsInsert.Add(txn);
                        }
                    }
                }
                if (lstTxnLmsInsert.Count > 0)
                    splitTransactionLMSTab.InsertBulk(lstTxnLmsInsert);
            }
            SplitTransactionTimaAndLenderFineLate();
            MoveTransactionLoanLMSToSplitTimaAndLender();
        }

        public void SplitTransactionTimaAndLenderFineLate()
        {
            var txnLoanLMSTab = new TableHelper<LMSModel.TblTransaction>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var splitTransactionLMSTab = new TableHelper<LMSModel.TblSplitTransactionLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var transactionTimaTab = new TableHelper<AG.TblTransactionLoanAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            var txnTimaTab = new TableHelper<Objects.TransactionDetailMoneyFineOriginal>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            var txnLenderTab = new TableHelper<Objects.TransactionDetailMoneyFineOriginal>().SetConnectString(Ultil.Constant.ConnectionStringAG);

            var sqlTxnTima = @"SELECT TL.ID TxnID, TL.AgencyId ShopID, LC.LoanID, TL.MoneyAdd, TL.MoneySub, TL.CreateDate, TL.Note, TL.ActionID
                                FROM tblTransactionLoan(NOLOCK) TL
                                INNER JOIN tblLoanCredit(NOLOCK) LC ON LC.LoanID = TL.LoanID
                                WHERE (TL.ActionID = 26 OR TL.ActionID = 29) AND TL.ShopID = 3703
                                order by TL.CreateDate";

            var sqlTxnLender = @"SELECT TL.ID TxnID, S.Name ShopName, TL.ShopID, LC.LoanID, TL.MoneyAdd, TL.MoneySub, TL.CreateDate, TL.Note, TL.ActionID 
                                FROM tblTransactionLoan(NOLOCK) TL
                                INNER JOIN tblLoanCredit(NOLOCK) LC ON LC.AgentLoanID = TL.LoanID
                                INNER JOIN tblShop(NOLOCK) S ON TL.ShopID = S.ShopID
                                WHERE (TL.ActionID = 26 OR TL.ActionID = 29) AND TL.ShopID != 3703 AND S.IsHub = 0
                                order by TL.CreateDate";
            //var keyDictTxn = ""; // ngày tạo _ shop id _ loanId _ actionID, 
            var dictDataTima = txnTimaTab.Query(sqlTxnTima, null).GroupBy(x => $"{x.CreateDate:yyyyMMddHHmmss}_{x.ShopID}_{x.LoanID}_{x.ActionID}").ToDictionary(x => x.Key, x => x.ToList());
            var dictDataLender = txnLenderTab.Query(sqlTxnLender, null).GroupBy(x => $"{x.CreateDate:yyyyMMddHHmmss}_{x.ShopID}_{x.LoanID}_{x.ActionID}").ToDictionary(x => x.Key, x => x.ToList());
            Dictionary<long, List<LMSModel.TblSplitTransactionLoan>> dictTxnLMSInsert = new Dictionary<long, List<LMSModel.TblSplitTransactionLoan>>();
            List<LMSModel.TblSplitTransactionLoan> lstTxnLmsInsert = new List<LMSModel.TblSplitTransactionLoan>();
            List<long> lstTxnTimaID = new List<long>();

            foreach (var item in dictDataTima)
            {
                foreach (var txnTima in item.Value)
                {
                    lstTxnTimaID.Add(txnTima.TxnID);
                    if (!dictTxnLMSInsert.ContainsKey(txnTima.TxnID))
                    {
                        dictTxnLMSInsert.Add(txnTima.TxnID, new List<LMSModel.TblSplitTransactionLoan>());
                    }
                    LMSModel.TblSplitTransactionLoan txnTimaLms = new LMSModel.TblSplitTransactionLoan
                    {
                        ReferTxnID = txnTima.TxnID,
                        ActionID = txnTima.ActionID,
                        CreateDate = txnTima.CreateDate,
                        LenderID = 3703,
                        LoanID = txnTima.LoanID,
                        MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineLate,
                        TotalMoney = txnTima.MoneyAdd > 0 ? txnTima.MoneyAdd : txnTima.MoneySub * -1 // moneysub * -1 hủy giao dịch tiền phải âm, 23 là hủy
                    };
                    // nếu ko có giao dịch trong lender, tima hưởng toàn bộ
                    // action 26, 29 ag tương đương action lms
                    var lstTxnLenderInfos = dictDataLender.GetValueOrDefault(item.Key);
                    if (lstTxnLenderInfos != null && lstTxnLenderInfos.Count > 0)
                    {
                        // id ag sẽ tương ứng với id lender - số count
                        int countIndex = lstTxnLenderInfos.Count;
                        var txnDetailLender = lstTxnLenderInfos.Where(x => x.TxnID == (txnTima.TxnID + countIndex)).FirstOrDefault();
                        if (txnDetailLender != null && txnDetailLender.ShopID > 0)
                        {
                            LMSModel.TblSplitTransactionLoan txnLenderLms = new LMSModel.TblSplitTransactionLoan
                            {
                                ReferTxnID = txnTima.TxnID,
                                ActionID = txnTima.ActionID,
                                CreateDate = txnDetailLender.CreateDate,
                                LenderID = txnDetailLender.ShopID,
                                LoanID = txnTima.LoanID,
                                MoneyType = (int)LMS.Common.Constants.Transaction_TypeMoney.FineLate,
                                TotalMoney = txnDetailLender.MoneyAdd > 0 ? txnDetailLender.MoneyAdd : txnDetailLender.MoneySub * -1 // moneysub * -1 hủy giao dịch tiền phải âm
                            };

                            // tima hưởng số còn lại
                            txnTimaLms.TotalMoney -= txnLenderLms.TotalMoney;
                            dictTxnLMSInsert[txnTima.TxnID].Add(txnLenderLms);
                        }
                    }
                    dictTxnLMSInsert[txnTima.TxnID].Add(txnTimaLms);
                }
            }
            var lstBatchTxn = lstTxnTimaID.Batch(2000);
            foreach (var lstTxnID in lstBatchTxn)
            {
                lstTxnLmsInsert.Clear();
                var txnLms = txnLoanLMSTab.SelectColumns(x => x.TransactionID, x => x.LoanID, x => x.LenderID, x => x.TimaTransactionID)
                                          .WhereClause(x => lstTxnID.Contains(x.TimaTransactionID)).Query();
                foreach (var itemTxnLMs in txnLms)
                {
                    var lstInsert = dictTxnLMSInsert.GetValueOrDefault(itemTxnLMs.TimaTransactionID);
                    if (lstInsert != null && lstInsert.Count > 0)
                    {
                        foreach (var txn in lstInsert)
                        {
                            txn.LoanID = itemTxnLMs.LoanID;
                            txn.ReferTxnID = itemTxnLMs.TransactionID;
                            lstTxnLmsInsert.Add(txn);
                        }
                    }
                }
                if (lstTxnLmsInsert.Count > 0)
                    splitTransactionLMSTab.InsertBulk(lstTxnLmsInsert);
            }
        }

        private void MoveTransactionLoanLMSToSplitTimaAndLender()
        {
            var txnLoanLMSTab = new TableHelper<LMSModel.TblTransaction>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var lenderLMSTab = new TableHelper<LMSModel.TblLender>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var loanLMSTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var splitTransactionLMSTab = new TableHelper<LMSModel.TblSplitTransactionLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            long lastTxnID = 0;
            int maxLength = 2000;
            List<int> lstActionIDLMS = new List<int>
            {
                (int)LMS.Common.Constants.Transaction_Action.DongLai,
                (int)LMS.Common.Constants.Transaction_Action.HuyDongLai,
                (int)LMS.Common.Constants.Transaction_Action.DongHd,
                (int)LMS.Common.Constants.Transaction_Action.HuyDongHd,
                (int)LMS.Common.Constants.Transaction_Action.TraGoc,
                (int)LMS.Common.Constants.Transaction_Action.HuyTraGoc,
                (int)LMS.Common.Constants.Transaction_Action.TraNo,
                (int)LMS.Common.Constants.Transaction_Action.HuyTraNo,
                (int)LMS.Common.Constants.Transaction_Action.NoLai,
                (int)LMS.Common.Constants.Transaction_Action.HuyNoLai,
                (int)LMS.Common.Constants.Transaction_Action.TienTuVanThuTruoc,
                (int)LMS.Common.Constants.Transaction_Action.TraNoPhiPhatMuon,
                (int)LMS.Common.Constants.Transaction_Action.DeleteTransactionPayMoneyFeeLate,
                (int)LMS.Common.Constants.Transaction_Action.PhiPhatTraTruocGoc,
                (int)LMS.Common.Constants.Transaction_Action.HuyPhiPhatTraTruocGoc,
                (int)LMS.Common.Constants.Transaction_Action.ThanhLy,
                (int)LMS.Common.Constants.Transaction_Action.HuyThanhLy,
                (int)LMS.Common.Constants.Transaction_Action.ThanhLyBaoHiem,
                (int)LMS.Common.Constants.Transaction_Action.Exemption,
                (int)LMS.Common.Constants.Transaction_Action.PayPartial_Interest,
                (int)LMS.Common.Constants.Transaction_Action.PayPartial_Consultant,
                (int)LMS.Common.Constants.Transaction_Action.PayPartial_Service,
                (int)LMS.Common.Constants.Transaction_Action.DeletePayPartial_Interest,
                (int)LMS.Common.Constants.Transaction_Action.DeletePayPartial_Consultant,
                (int)LMS.Common.Constants.Transaction_Action.ReceiptInsurance,

            };
            while (true)
            {
                var lstTxnLMSData = txnLoanLMSTab.SetGetTop(maxLength).SelectColumns(x => x.TransactionID, x => x.CreateDate, x => x.LoanID, x => x.LenderID, x => x.ActionID, x => x.MoneyType, x => x.TotalMoney)
                                             .WhereClause(x => x.TransactionID > lastTxnID && lstActionIDLMS.Contains(x.ActionID))
                                             .OrderBy(x => x.TransactionID).Query();
                if (lstTxnLMSData == null || !lstTxnLMSData.Any())
                {
                    return;
                }
                List<long> lstTxnIDLms = new List<long>();
                List<LMSModel.TblSplitTransactionLoan> lstTxnLmsInsert = new List<LMSModel.TblSplitTransactionLoan>();
                List<long> lstLoanLmsID = new List<long>();
                List<long> lstLenderID = new List<long>();
                List<int> lstMoneyTypeLenderReceived = new List<int>
                {
                    (int)LMS.Common.Constants.Transaction_TypeMoney.Original,
                    (int)LMS.Common.Constants.Transaction_TypeMoney.Interest,
                    (int)LMS.Common.Constants.Transaction_TypeMoney.Service,
                    (int)LMS.Common.Constants.Transaction_TypeMoney.FineLate,
                };
                //lastTxnID = lstTxnLMSData.OrderBy(x => x.TransactionID).Last().TransactionID;
                foreach (var item in lstTxnLMSData)
                {
                    lstTxnIDLms.Add(item.TransactionID);
                    lstLenderID.Add(item.LenderID);
                    lstLoanLmsID.Add(item.LoanID);
                    if (lastTxnID < item.TransactionID)
                    {
                        lastTxnID = item.TransactionID;
                    }
                }
                lstLoanLmsID = lstLoanLmsID.Distinct().ToList();
                lstLenderID = lstLenderID.Distinct().ToList();

                var dictLoanInfos = loanLMSTab.SelectColumns(x => x.LoanID, x => x.LenderID, x => x.RateConsultant, x => x.RateInterest, x => x.RateService)
                                            .WhereClause(x => lstLoanLmsID.Contains(x.LoanID)).Query().ToDictionary(x => x.LoanID, x => x);

                var dictLenderInfos = lenderLMSTab.SelectColumns(x => x.LenderID, x => x.SelfEmployed)
                                            .WhereClause(x => lstLenderID.Contains(x.LenderID)).Query().ToDictionary(x => x.LenderID, x => x);

                var lstSplitDataExists = splitTransactionLMSTab.GetDistinct().SelectColumns(x => x.ReferTxnID)
                                            .WhereClause(x => lstTxnIDLms.Contains(x.ReferTxnID))
                                            .Query().ToDictionary(x => x.ReferTxnID, x => x.ReferTxnID);

                foreach (var item in lstTxnLMSData)
                {
                    if (lstSplitDataExists.ContainsKey(item.TransactionID))
                    {
                        continue;
                    }
                    // tima hưởng mặc định
                    var txnDetail = new LMSModel.TblSplitTransactionLoan
                    {
                        ActionID = item.ActionID,
                        CreateDate = item.CreateDate,
                        LenderID = 3703,
                        MoneyType = item.MoneyType,
                        LoanID = item.LoanID,
                        ReferTxnID = item.TransactionID,
                        TotalMoney = item.TotalMoney
                    };
                    // số tiền lender được hưởng
                    if (lstMoneyTypeLenderReceived.Contains(item.MoneyType))
                    {
                        if (item.MoneyType == (int)LMS.Common.Constants.Transaction_TypeMoney.FineLate)
                        {
                            // kiểm tra lender lever4 m
                            var lenderDetail = dictLenderInfos.GetValueOrDefault(item.LenderID);
                            if (lenderDetail != null && lenderDetail.SelfEmployed < (int)(int)LMS.Common.Constants.Lender_SeflEmployed.LenderOut && lenderDetail.LenderID > 0)
                            {
                                var loanDetail = dictLoanInfos.GetValueOrDefault(item.LoanID);
                                decimal rateLender = (decimal)loanDetail.RateInterest + (decimal)loanDetail.RateService;
                                decimal totalRate = rateLender + (decimal)loanDetail.RateConsultant;
                                var txnDetailLender = new LMSModel.TblSplitTransactionLoan
                                {
                                    ActionID = item.ActionID,
                                    CreateDate = item.CreateDate,
                                    LenderID = item.LenderID,
                                    MoneyType = item.MoneyType,
                                    LoanID = item.LoanID,
                                    ReferTxnID = item.TransactionID,
                                    TotalMoney = Convert.ToInt64(item.TotalMoney * (rateLender / totalRate))
                                };
                                lstTxnLmsInsert.Add(txnDetailLender);
                                txnDetail.TotalMoney -= txnDetailLender.TotalMoney;
                            }
                        }
                        else
                        {
                            txnDetail.LenderID = item.LenderID;
                        }
                    }
                    lstTxnLmsInsert.Add(txnDetail);
                }
                if (lstTxnLmsInsert.Count > 0)
                    splitTransactionLMSTab.InsertBulk(lstTxnLmsInsert);
            }
        }

    }
}
