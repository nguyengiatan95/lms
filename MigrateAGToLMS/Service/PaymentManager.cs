﻿using LMS.Common.Constants;
using LMS.Common.DAL;
using LMS.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MigrateAGToLMS.Service
{
    public class PaymentManager
    {
        TableHelper<AG.TblPaymentScheduleAG> paymentTimaTab;
        TableHelper<LMSModel.TblCustomer> customerLMSTab;
        TableHelper<LMSModel.TblLender> lenderLMSTab;
        TableHelper<LMSModel.TblPaymentSchedule> paymentLMSTab;
        TableHelper<LMSModel.TblLoan> loanLMSTab;
        TableHelper<LMSModel.TblLogCompare> logCompareTab;

        public PaymentManager()
        {
            paymentTimaTab = new TableHelper<AG.TblPaymentScheduleAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            customerLMSTab = new TableHelper<LMSModel.TblCustomer>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            lenderLMSTab = new TableHelper<LMSModel.TblLender>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            paymentLMSTab = new TableHelper<LMSModel.TblPaymentSchedule>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            loanLMSTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            logCompareTab = new TableHelper<LMSModel.TblLogCompare>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
        }

        public void MovePaymentTima(List<int> TimaloanIDs = null)
        {
            Console.WriteLine($"-------------- PaymentSchedule --------------");
            DateTime dtNow = DateTime.Now;
            long maxTimaPaymentScheduleID = 0;
            long LasTimaID = 0;
            int maxRowGet = 2000;
            // laays maxid cua kh
            var lstMaxTimaCustomerID = paymentLMSTab.SelectColumns(x => x.TimaPaymentScheduleID)
                                                   .OrderByDescending(x => x.TimaPaymentScheduleID)
                                                   .WhereClause(x => x.TimaPaymentScheduleID > 0).SetGetTop(1).Query();
            if (lstMaxTimaCustomerID != null && lstMaxTimaCustomerID.Any())
            {
                LasTimaID = lstMaxTimaCustomerID.First().TimaPaymentScheduleID;
                if (TimaloanIDs == null || TimaloanIDs.Count == 0)
                {
                    maxTimaPaymentScheduleID = LasTimaID;
                }
            }
            try
            {
                if (TimaloanIDs != null && TimaloanIDs.Count > 0)
                {
                    var lstLmsLoanIds = loanLMSTab.SelectColumns(x => x.LoanID).WhereClause(x => TimaloanIDs.Contains((int)x.TimaLoanID)).Query().Select(x => x.LoanID);
                    paymentLMSTab.DeleteWhere(x => lstLmsLoanIds.Contains(x.LoanID));
                }
                while (true)
                {
                    if (TimaloanIDs != null && TimaloanIDs.Count > 0) paymentTimaTab.WhereClause(x => TimaloanIDs.Contains((int)x.LoanID));
                    var lstPaymentAG = paymentTimaTab.WhereClause(x => x.PaymentScheduleID > maxTimaPaymentScheduleID)
                                                .OrderBy(x => x.PaymentScheduleID)
                                                .SetGetTop(maxRowGet).Query();
                    var lstPaymentNeedInsert = lstPaymentAG.OrderBy(x => x.PaymentScheduleID).ToList();
                    if (lstPaymentNeedInsert.Count > 0)
                    {

                        List<LMSModel.TblPaymentSchedule> lstPayment = new List<LMSModel.TblPaymentSchedule>();
                        List<LMSModel.TblPaymentSchedule> lstPaymentUpdate = new List<LMSModel.TblPaymentSchedule>();
                        var lstLoanIds = lstPaymentNeedInsert.Select(x => x.LoanID).ToList();
                        var dicLoanLMS = loanLMSTab.WhereClause(x => x.TimaLoanID > 0
                                     && lstLoanIds.Contains((long)x.TimaLoanID)).Query().ToDictionary(x => x.TimaLoanID, x => x);

                        foreach (var item in lstPaymentNeedInsert)
                        {
                            if (dicLoanLMS.ContainsKey(item.LoanID))
                            {
                                var payment = new LMSModel.TblPaymentSchedule()
                                {
                                    PaymentScheduleID = item.PaymentScheduleID,
                                    CompletedDate = item.CompletedDate,
                                    TimaPaymentScheduleID = item.PaymentScheduleID,
                                    CreatedDate = item.CreatedDate,
                                    FirstPaymentDate = item.FirstPaymentDate,
                                    FromDate = item.FromDate ?? dicLoanLMS[item.LoanID].FromDate,
                                    IsComplete = item.IsComplete,
                                    IsVisible = item.IsVisible,
                                    LoanID = dicLoanLMS[item.LoanID].LoanID,
                                    ModifiedDate = item.ModifiedDate,
                                    MoneyConsultant = item.MoneyConsultant,
                                    MoneyFineInterestLate = item.MoneyFineInterestLate,
                                    MoneyFineLate = item.MoneyFineLate,
                                    MoneyInterest = item.MoneyInterest,
                                    MoneyOriginal = item.MoneyOriginal,
                                    MoneyService = item.MoneyService,
                                    PayDate = item.PayDate ?? dicLoanLMS[item.LoanID].ToDate,
                                    PayMoneyConsultant = item.PayMoneyConsultant,
                                    PayMoneyFineInterestLate = item.PayMoneyFineInterestLate,
                                    PayMoneyFineLate = item.PayMoneyFineLate,
                                    PayMoneyFineOriginal = item.PayMoneyFineOriginal,
                                    PayMoneyInterest = item.PayMoneyInterest,
                                    PayMoneyOriginal = item.PayMoneyOriginal,
                                    PayMoneyService = item.PayMoneyService,
                                    Status = item.Status,
                                    ToDate = item.ToDate ?? dicLoanLMS[item.LoanID].ToDate,
                                    Fined = item.Fined,
                                    DateApplyFineLate = item.FirstPaymentDate
                                };
                                lstPayment.Add(payment);
                            }
                            maxTimaPaymentScheduleID = item.PaymentScheduleID;
                        }
                        Console.WriteLine($"[Inserting] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} bat dau insert Payment");
                        if (lstPaymentUpdate.Count > 0)
                        {
                            paymentLMSTab.UpdateBulk(lstPaymentUpdate);
                            Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} update Payment  -  {lstPaymentUpdate.Count()} ");
                        }
                        if (lstPayment.Count > 0)
                        {
                            paymentLMSTab.InsertBulk(lstPayment);
                            Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert Payment  -  {lstPayment.Count()} ");
                        }
                    }
                    else
                    {
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert thanh cong tat ca Payment");
                        break;
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        public void ComparePayment()
        {
            Console.WriteLine($"-------------- ComparePayment --------------");
            string query = $@" select LoanID,COUNT(IsComplete) as IsComplete,COUNT(IsVisible) as IsVisible ,COUNT(PaymentScheduleID)
                                 as PaymentScheduleID, SUM(MoneyOriginal + MoneyInterest + MoneyService + MoneyFineLate + MoneyFineInterestLate + MoneyConsultant) as MoneyInterest , 
                                 SUM(PayMoneyOriginal + PayMoneyInterest + PayMoneyService + PayMoneyFineLate + PayMoneyFineInterestLate + PayMoneyConsultant + PayMoneyFineOriginal) as PayMoneyInterest
                                                          from TblPaymentSchedule (NOLOCK) where Status = 1 group by LoanID";
            var taskPaymentTima = Task.Run(() =>
             {
                 return paymentTimaTab.Query(query, null);
             });
            var taskPaymentLMS = Task.Run(() =>
            {
                return paymentLMSTab.Query(query, null);
            });
            var taskLoanLMS = Task.Run(() =>
            {
                return loanLMSTab.SelectColumns(x => x.LoanID, x => x.TimaLoanID).Query();
            });
            Task.WaitAll(taskPaymentTima, taskPaymentLMS, taskLoanLMS);
            var lstPaymentTima = taskPaymentTima.Result;
            var lstPaymentLMS = taskPaymentLMS.Result;
            var dicLoanLMS = taskLoanLMS.Result.ToDictionary(x => x.LoanID, x => x.TimaLoanID);

            foreach (var item in lstPaymentLMS)
            {
                item.LoanID = dicLoanLMS.GetValueOrDefault(item.LoanID).Value;
            }
            List<int> mainType = new List<int>() { (int)LogCompare_MainType.Payment };
            var lstCurrentLog = logCompareTab.WhereClause(x => x.Status == 1 && x.ForDate == DateTime.Now.Date && mainType.Contains(x.MainType)).Query();
            logCompareTab.DeleteList(lstCurrentLog.ToList());
            long countErrorComplete = 0;
            long countErrorIsVisible = 0;
            long countLoanMiss = 0;


            string loanErrorComplete = string.Empty;
            string loanErrorIsVisible = string.Empty;
            string loanMissPayment = string.Empty;

            long countTotalPayment = 0;
            long countMoneyNeed = 0;
            long countMoneyPaid = 0;

            string loanTotalPayment = string.Empty;
            string loanMoneyNeed = string.Empty;
            string loanMoneyPaid = string.Empty;



            var dicLMS = lstPaymentLMS.ToDictionary(x => x.LoanID, x => x);
            foreach (var item in lstPaymentTima)
            {
                if (dicLMS.ContainsKey(item.LoanID))
                {
                    if (item.IsVisible != dicLMS[item.LoanID].IsVisible)
                    {
                        loanErrorIsVisible += $" {item.LoanID} , ";
                        countErrorIsVisible += 1;
                    }
                    if (item.IsComplete != dicLMS[item.LoanID].IsComplete)
                    {
                        loanErrorComplete += $" {item.LoanID} , ";
                        countErrorComplete += 1;
                    }

                    if (item.PaymentScheduleID != dicLMS[item.LoanID].PaymentScheduleID) // đang gán tạm totalcount = PaymentScheduleID
                    {
                        loanTotalPayment += $" {item.LoanID} , ";
                        countTotalPayment += 1;
                    }

                    if (item.MoneyInterest != dicLMS[item.LoanID].MoneyInterest)
                    {
                        loanMoneyNeed += $" {item.LoanID} , ";
                        countMoneyNeed += 1;
                    }

                    if (item.PayMoneyInterest != dicLMS[item.LoanID].PayMoneyInterest)
                    {
                        loanMoneyPaid += $" {item.LoanID} , ";
                        countMoneyPaid += 1;
                    }
                }

            }

            List<LMSModel.TblLogCompare> lstPayment = new List<LMSModel.TblLogCompare>();
            lstPayment.Add(new LMSModel.TblLogCompare()
            {
                CreateDate = DateTime.Now,
                DetailType = (int)LogCompare_DetailType.Payment_ListMiss,
                DetailNote = $"Danh sách loan miss : {loanMissPayment}",
                //DetailNote = LogCompare_DetailType.Payment_ListMiss.GetDescription(),
                ForDate = DateTime.Now.Date,
                MainType = (int)LogCompare_MainType.Payment,
                Status = 1,
                TotalAG = 0,
                TotalLMS = countLoanMiss
            });

            lstPayment.Add(new LMSModel.TblLogCompare()
            {
                CreateDate = DateTime.Now,
                DetailType = (int)LogCompare_DetailType.Payment_LoanErrorIsVisible,
                //DetailNote = LogCompare_DetailType.Payment_LoanErrorIsVisible.GetDescription(),
                DetailNote = $"Danh sách loan sai visible : {loanErrorIsVisible}",
                ForDate = DateTime.Now.Date,
                MainType = (int)LogCompare_MainType.Payment,
                Status = 1,
                TotalAG = 0,
                TotalLMS = countErrorIsVisible
            });
            lstPayment.Add(new LMSModel.TblLogCompare()
            {
                CreateDate = DateTime.Now,
                DetailType = (int)LogCompare_DetailType.Payment_LoanErrorIsComplete,
                DetailNote = $"Danh sách loan sai complete : {loanErrorComplete}",
               //DetailNote = LogCompare_DetailType.Payment_LoanErrorIsComplete.GetDescription(),
                ForDate = DateTime.Now.Date,
                MainType = (int)LogCompare_MainType.Payment,
                Status = 1,
                TotalAG = 0,
                TotalLMS = countErrorComplete
            });

            lstPayment.Add(new LMSModel.TblLogCompare()
            {
                CreateDate = DateTime.Now,
                DetailType = (int)LogCompare_DetailType.Payment_LoanErrorTotalCount,
                DetailNote = $"Danh sách loan sai tổng kì : {loanTotalPayment}",
                //DetailNote = LogCompare_DetailType.Payment_LoanErrorTotalCount.GetDescription(),
                ForDate = DateTime.Now.Date,
                MainType = (int)LogCompare_MainType.Payment,
                Status = 1,
                TotalAG = 0,
                TotalLMS = countTotalPayment
            });
            lstPayment.Add(new LMSModel.TblLogCompare()
            {
                CreateDate = DateTime.Now,
                DetailType = (int)LogCompare_DetailType.Payment_LoanErrorTotalNeed,
                DetailNote = $"Danh sách loan sai tổng tiền phải thu : {loanMoneyNeed}",
                //DetailNote = LogCompare_DetailType.Payment_LoanErrorTotalCount.GetDescription(),
                ForDate = DateTime.Now.Date,
                MainType = (int)LogCompare_MainType.Payment,
                Status = 1,
                TotalAG = 0,
                TotalLMS = countMoneyNeed
            });
            lstPayment.Add(new LMSModel.TblLogCompare()
            {
                CreateDate = DateTime.Now,
                DetailType = (int)LogCompare_DetailType.Payment_LoanErrorTotalPay,
                DetailNote = $"Danh sách loan sai tổng tiền phải thu : {loanMoneyPaid}",
                //DetailNote = LogCompare_DetailType.Payment_LoanErrorTotalCount.GetDescription(),
                ForDate = DateTime.Now.Date,
                MainType = (int)LogCompare_MainType.Payment,
                Status = 1,
                TotalAG = 0,
                TotalLMS = countMoneyPaid
            });

            logCompareTab.InsertBulk(lstPayment);
            //}
            Console.WriteLine($"-------------- DonePayment --------------");
        }
    }
}
