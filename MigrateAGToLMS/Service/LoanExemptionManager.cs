﻿using LMS.Common.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigrateAGToLMS.Service
{
    public class LoanExemptionManager
    {
        TableHelper<LMSModel.TblLoanExemption> _loanExemptionTab;
        // Tab AG
        TableHelper<AG.TblLoanExtra> _loanExtraAgTab;
        TableHelper<LMSModel.TblLoan> _loanLMSTab;
        TableHelper<AG.TblTransactionIndemnifyLoanInsurrance> loanExtraAgTransactionIndemnifyLoanInsurrance;
        TableHelper<LMSModel.TblApprovalLevelBookDebtBalance> _approvalLevelBookDebtBalanceTab;
        TableHelper<LMSModel.TblSettingTotalMoney> _settingTotalMoneyTab;
        TableHelper<LMSModel.TblUserApprovalLevel> _userApprovalLevelTab;
        LMS.Common.Helper.Utils _common;
        public LoanExemptionManager()
        {
            _loanExemptionTab = new TableHelper<LMSModel.TblLoanExemption>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            _loanExtraAgTab = new TableHelper<AG.TblLoanExtra>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            _loanLMSTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            loanExtraAgTransactionIndemnifyLoanInsurrance = new TableHelper<AG.TblTransactionIndemnifyLoanInsurrance>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            _common = new LMS.Common.Helper.Utils();
            _approvalLevelBookDebtBalanceTab = new TableHelper<LMSModel.TblApprovalLevelBookDebtBalance>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            _settingTotalMoneyTab = new TableHelper<LMSModel.TblSettingTotalMoney>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            _userApprovalLevelTab = new TableHelper<LMSModel.TblUserApprovalLevel>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
        }

        public void UpdateCustomerIDAndReferIDRequest()
        {
            Console.WriteLine($"-------------- UpdateCustomerIDAndReferIDRequest -------------");
            var lstData = _loanExemptionTab.WhereClause(x => x.ReferTraceIDRequest == null).Query();
            if (lstData != null && lstData.Any())
            {
                List<long> lstLoanID = new List<long>();
                foreach (var item in lstData)
                {
                    lstLoanID.Add(item.LoanID);
                    var extraData = _common.ConvertJSonToObjectV2<LMSModel.LoanExemptionExtraData>(item.ExtraData);
                    item.ReferTraceIDRequest = extraData.ReferTraceIDRequest;
                }
                var lstLoanData = _loanLMSTab.SelectColumns(x => x.LoanID, x => x.CustomerID).WhereClause(x => lstLoanID.Contains(x.LoanID)).Query();
                foreach (var item in lstData)
                {
                    item.CustomerID = lstLoanData.FirstOrDefault(x => x.LoanID == item.LoanID).CustomerID.Value;
                    _loanExemptionTab.Update(item);
                }
                //_loanExemptionTab.UpdateBulk(lstData);
            }
            Console.WriteLine($"-------------- Done UpdateCustomerIDAndReferIDRequest -------------");
        }

        public void UpdateConfigMoneyLevelApproval()
        {
            // chỉ chạy 1 lần cho tháng hiện tại
            int yearMonthCurrent = int.Parse(DateTime.Now.ToString("yyyyMM"));
            var lstSettingTotalMoneyApproval = _settingTotalMoneyTab.Query();
            foreach (var item in lstSettingTotalMoneyApproval)
            {
                _approvalLevelBookDebtBalanceTab.Insert(new LMSModel.TblApprovalLevelBookDebtBalance
                {
                    ApprovalLevelBookDebtID = item.ApprovalLevelBookDebtID,
                    CreateDate = DateTime.Now,
                    ModifyDate = DateTime.Now,
                    TotalMoneyCurrent = item.TotalMoney,
                    TotalMoneySetting = item.TotalMoney,
                    YearMonthCurrent = yearMonthCurrent
                });
            }
        }
        public void UpdateMoneyCurrentLevelApproval()
        {
            var currentDate = DateTime.Now;
            var firstDateOfMonth = new DateTime(currentDate.Year, currentDate.Month, 01);
            int yearMonthCurrent = int.Parse(DateTime.Now.ToString("yyyyMM"));
            var lstData = _loanExemptionTab.WhereClause(x => x.Status == 5 && x.CreateDate > firstDateOfMonth).Query(); // đã tất toán thành công
            var lstUserApproval = _userApprovalLevelTab.Query();
            var lstSetting = _approvalLevelBookDebtBalanceTab.Query().ToList();
            Dictionary<long, List<long>> dictLevelApprovedLoan = new Dictionary<long, List<long>>();
            foreach (var item in lstData)
            {
                // lấy ng approve
                var approvalLevelID = lstUserApproval.FirstOrDefault(x => x.UserID == item.ApprovedBy && x.ApprovalLevelBookDebtID == item.ApprovalLevelBookDebtID).ApprovalLevelBookDebtID.Value;
                if (!dictLevelApprovedLoan.ContainsKey(approvalLevelID))
                {
                    dictLevelApprovedLoan.Add(approvalLevelID, new List<long>());
                }
                dictLevelApprovedLoan[approvalLevelID].Add(item.LoanID);
                var settingDetail = lstSetting.FirstOrDefault(x => x.ApprovalLevelBookDebtID == approvalLevelID);
                settingDetail.TotalMoneyCurrent -= (item.MoneyOriginal + item.MoneyInterest + item.MoneyService + item.MoneyFineOriginal + item.MoneyFineLate + item.MoneyConsultant
                                                    - (item.PayMoneyOriginal + item.PayMoneyInterest + item.PayMoneyService + item.PayMoneyFineOriginal + item.PayMoneyFineLate + item.PayMoneyConsultant));
            }
            foreach (var item in lstSetting)
            {
                if (dictLevelApprovedLoan.ContainsKey(item.ApprovalLevelBookDebtID))
                {
                    item.LstLoanIDHandle = _common.ConvertObjectToJSonV2(dictLevelApprovedLoan[item.ApprovalLevelBookDebtID]);
                }
                _approvalLevelBookDebtBalanceTab.Update(item);
            }
            //_approvalLevelBookDebtBalanceTab.UpdateBulk(lstSetting);
        }
    }
}
