﻿using LMS.Common.Constants;
using LMS.Common.DAL;
using LMS.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MigrateAGToLMS.Service
{
    public class CustomerManager
    {
        TableHelper<AG.TblCustomerAG> customerTimaTab;
        TableHelper<AG.TblShopAG> shopTimaTab;
        TableHelper<LMSModel.TblCustomer> customerLMSTab;
        TableHelper<AG.TblLoanAG> loanAGTab;
        TableHelper<AG.TblLoanCredit> loanCreditTab;
        TableHelper<AG.TblCustomerCreditAG> customerCreditTab;
        TableHelper<LMSModel.TblLogCompare> logCompareTab;
        TableHelper<LMSModel.TblLogCustomerCash> logCustomerCashTab;
        TableHelper<LMSModel.CustomerLoan> customerLoanTab;


        public CustomerManager()
        {
            customerTimaTab = new TableHelper<AG.TblCustomerAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            shopTimaTab = new TableHelper<AG.TblShopAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            customerLMSTab = new TableHelper<LMSModel.TblCustomer>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            loanAGTab = new TableHelper<AG.TblLoanAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            loanCreditTab = new TableHelper<AG.TblLoanCredit>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            customerCreditTab = new TableHelper<AG.TblCustomerCreditAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            logCompareTab = new TableHelper<LMSModel.TblLogCompare>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            logCustomerCashTab = new TableHelper<LMSModel.TblLogCustomerCash>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            customerLoanTab = new TableHelper<LMSModel.CustomerLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
        }

        public void MoveCustomerTima(bool updateAll = false, List<int> lstCustomerIDsRerun = null, List<int> loanTimaIds = null)
        {
            Console.WriteLine($"-------------- CUSTOMER --------------");
            DateTime dtNow = DateTime.Now;

            long maxTimaCustomerID = 0;
            int maxRowGet = 2000;
            long LasTimaID = 0;
            // laays maxid cua kh
            var taskMaxTimaCustomerIDLMS = Task.Run(() =>
            {
                return customerLMSTab.SelectColumns(x => x.TimaCustomerID)
                                                    .OrderByDescending(x => x.TimaCustomerID)
                                                    .WhereClause(x => x.TimaCustomerID > 0).SetGetTop(1).Query();
            });
            // lay shop cua tima + hub;
            var taskShopTima = Task.Run(() =>
            {
                var lstShopTima = Ultil.StaticEntity.LstShopTimaHubAG;
                if (lstShopTima != null && lstShopTima.Count > 0) return lstShopTima;
                return shopTimaTab.WhereClause(x => x.ShopID == 3703 || x.IsHub == true).Query();
            });
            Task.WaitAll(taskMaxTimaCustomerIDLMS, taskShopTima);
            var lstMaxTimaCustomerID = taskMaxTimaCustomerIDLMS.Result;
            var lstShop = taskShopTima.Result;

            if (lstMaxTimaCustomerID != null && lstMaxTimaCustomerID.Any())
            {
                LasTimaID = lstMaxTimaCustomerID.First().TimaCustomerID.Value;
                // chạy lại all dữ liệu khi updateall = true
                if (!updateAll)
                {
                    maxTimaCustomerID = LasTimaID;
                }
            }
            if (loanTimaIds != null && loanTimaIds.Count > 0)
            {
                var customerIds = loanAGTab.WhereClause(x => loanTimaIds.Contains(x.ID)).Query();
                lstCustomerIDsRerun = customerIds.Select(x => x.CustomerID).ToList();
            }
            while (true)
            {
                var shopIds = lstShop.Select(x => x.ShopID).ToList();
                if (lstCustomerIDsRerun != null) customerTimaTab.WhereClause(x => lstCustomerIDsRerun.Contains(x.CustomerID));
                var lstCustomerAG = customerTimaTab.WhereClause(x => x.CustomerID > maxTimaCustomerID)
                    .WhereClause(x => shopIds.Contains((int)x.ShopID)).OrderBy(x => x.CustomerID).SetGetTop(maxRowGet).Query();
                // user chưa insert 
                List<LMSModel.TblLogCustomerCash> lstLogCash = new List<LMSModel.TblLogCustomerCash>();
                var lstCustomerNeedInsert = lstCustomerAG.OrderBy(x => x.CustomerID).ToList();
                if (lstCustomerNeedInsert.Count > 0)
                {
                    List<LMSModel.TblCustomer> lstCustomerUpDate = new List<LMSModel.TblCustomer>();
                    List<LMSModel.TblCustomer> lstCustomerInsert = new List<LMSModel.TblCustomer>();
                    var lstCustomerIDs = lstCustomerNeedInsert.Select(x => x.CustomerID).ToList();
                    // lấy loan để lấy ra loanCreditIds
                    var lstLoanAG = loanAGTab.WhereClause(x => lstCustomerIDs.Contains(x.CustomerID)).SelectColumns(x => x.CustomerID, x => x.ID).Query();
                    Dictionary<long, long> dicLoanAG = new Dictionary<long, long>();
                    var lstLoanAGIds = new List<long>();
                    foreach (var item in lstLoanAG)
                    {
                        if (!dicLoanAG.ContainsKey(item.CustomerID)) dicLoanAG.Add(item.CustomerID, item.ID);
                    }
                    lstLoanAGIds = dicLoanAG.Values.ToList();
                    // lấy loancredit để lấy ra customerCreditID
                    var lstLoanCreditAG = loanCreditTab.WhereClause(x => lstLoanAGIds.Contains((int)x.LoanID)).SelectColumns(x => x.LoanID, x => x.ID, x => x.CustomerCreditId).Query();
                    var lstCustomerCreditIds = lstLoanCreditAG.Select(x => x.CustomerCreditId).ToList();
                    // lấy customerCredit 
                    var lstCustomerCreditAG = customerCreditTab.WhereClause(x => lstCustomerCreditIds.Contains(x.CustomerId)).SelectColumns(x => x.CustomerId, x => x.Birthday, x => x.Gender, x => x.Email).Query();
                    var dicCustomerCreditAG = lstCustomerCreditAG.ToDictionary(x => x.CustomerId, x => x);

                    var DicLMS = customerLMSTab.SelectColumns(x => x.CustomerID, x => x.TimaCustomerID).WhereClause(x => lstCustomerIDs.Contains((int)x.TimaCustomerID)).Query().ToDictionary(x => (long)x.TimaCustomerID, x => x.CustomerID);

                    foreach (var item in lstCustomerNeedInsert)
                    {
                        long loanId = 0;
                        int customerCreditIds = 0;
                        try
                        {
                            loanId = dicLoanAG[item.CustomerID];
                            customerCreditIds = lstLoanCreditAG.Where(x => x.LoanID == loanId).Select(x => x.CustomerCreditId).FirstOrDefault() ?? 0;
                        }
                        catch (Exception)
                        {

                        }

                        var objCustomer = new LMSModel.TblCustomer()
                        {
                            Address = item.Address,
                            CreateDate = item.CreatedDate.Value,
                            FullName = item.Name,
                            Phone = item.Phone,
                            AddressHouseHold = item.AddressHouseHold,
                            TimaCustomerID = item.CustomerID,
                            DistrictID = item.DistrictId,
                            BirthDay = dicCustomerCreditAG.GetValueOrDefault((int)customerCreditIds)?.Birthday,
                            CityID = item.CityId,
                            Email = dicCustomerCreditAG.GetValueOrDefault((int)customerCreditIds)?.Email,
                            Gender = dicCustomerCreditAG.GetValueOrDefault((int)customerCreditIds)?.Gender == null ? 0 : dicCustomerCreditAG.GetValueOrDefault((int)customerCreditIds)?.Gender,
                            //IsBorrowing update sau
                            ModifyDate = item.CreatedDate,
                            NumberCard = item.NumberCard,
                            PermanentAddress = item.PermanentAddress,
                            TotalMoney = item.TotalMoney,
                            TotalMoneyAccounting = item.TotalMoney,
                            // ward k co
                            AutoPayment = 0,
                            CustomerID = DicLMS != null && DicLMS.ContainsKey((long)item.CustomerID) ? DicLMS[item.CustomerID] : item.CustomerID
                        };
                        if (DicLMS.ContainsKey(item.CustomerID))
                        {
                            // 12-04-2022:
                            // không cập nhật địa chỉ KH từ ag
                            // nếu đã tồn tại bên LMS do lms lấy từ los
                            //lstCustomerUpDate.Add(objCustomer);
                        }
                        else
                        {
                            lstCustomerInsert.Add(objCustomer);
                        }
                        maxTimaCustomerID = item.CustomerID;
                        lstLogCash.Add(new LMSModel.TblLogCustomerCash()
                        {
                            CreateDate = dtNow,
                            CustomerID = objCustomer.CustomerID,
                            Description = "Khởi tạo từ hệ thống cũ",
                            MoneyAdd = 0,
                            MoneyAfter = objCustomer.TotalMoney ?? 0,
                            MoneyBefore = objCustomer.TotalMoney ?? 0,
                        });
                    }
                    Console.WriteLine($"[Inserting] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} bat dau insert customer");
                    if (lstCustomerUpDate.Count > 0)
                    {
                        customerLMSTab.UpdateBulk(lstCustomerUpDate);
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} update customer  -  {lstCustomerUpDate.Count()} ");
                    }
                    if (lstCustomerInsert.Count > 0)
                    {
                        customerLMSTab.InsertBulk(lstCustomerInsert, setOffIdentity: true);
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert customer  -  {lstCustomerInsert.Count()} ");
                    }
                    var lstCustomerIds = lstLogCash.Select(x => x.CustomerID).ToList();
                    logCustomerCashTab.DeleteWhere(x => lstCustomerIds.Contains(x.CustomerID));
                    logCustomerCashTab.InsertBulk(lstLogCash);
                }
                else
                {
                    Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert tat ca customer");
                    break;
                }
            }
        }

        public void CompareTotalMoneyCustomer()
        {
            Console.WriteLine($"--------------COMPARE CUSTOMER --------------");
            LenderManager lenderManager = new LenderManager();
            var TaskCustomerAG = Task.Run(() =>
            {
                var lstShopIds = shopTimaTab.SelectColumns(x => x.ShopID).WhereClause(x => x.ShopID == 3703 || x.IsHub == true).Query().Select(x => x.ShopID).ToList();
                return customerTimaTab.SelectColumns(x => x.CustomerID, x => x.TotalMoney, x => x.Name, x => x.Phone).WhereClause(x => x.TotalMoney != 0 && lstShopIds.Contains((int)x.ShopID)).Query();
            });
            var TaskCustomerLMS = Task.Run(() =>
            {
                return customerLMSTab.SelectColumns(x => x.CustomerID, x => x.TotalMoney, x => x.FullName, x => x.Phone, x => x.TimaCustomerID)
                  .WhereClause(x => x.TotalMoney != 0).Query();
            });
            Task.WaitAll(TaskCustomerAG, TaskCustomerLMS);
            var lstCustomerAG = TaskCustomerAG.Result;
            var lstCustomerLMS = TaskCustomerLMS.Result;
            long errorPhone = 0;
            string notExistCustomerID = string.Empty;
            string listCustomerIDErrorPhone = string.Empty;
            long missCustomerID = 0;
            long totalCountMoney = 0;
            string listtotalCountMoney = LogCompare_DetailType.Customer_CountHaveMoney.GetDescription();

            foreach (var item in lstCustomerAG)
            {
                if (lstCustomerLMS.Count(x => x.TimaCustomerID == item.CustomerID) == 0)
                {
                    notExistCustomerID += $"{item.CustomerID}, ";
                    missCustomerID += 1;
                }
                else if (lstCustomerLMS.Where(x => x.Phone != item.Phone && x.TimaCustomerID == item.CustomerID).Count() > 0)
                {
                    errorPhone += 1;
                    listCustomerIDErrorPhone += $"{item.CustomerID}, ";
                }


                if (lstCustomerLMS.Where(x => x.TimaCustomerID == item.CustomerID).Count() == 0 || lstCustomerLMS.Where(x => x.TimaCustomerID == item.CustomerID && x.TotalMoney != item.TotalMoney).Count() > 0)
                {
                    totalCountMoney += 1;
                    listtotalCountMoney += $" {item.CustomerID} ,";
                }

            }
            List<LMSModel.TblLogCompare> lstLog = new List<LMSModel.TblLogCompare>();
            lstLog.Add(lenderManager.SetlogCompare(
                (int)LogCompare_MainType.Customer, (int)LogCompare_DetailType.Customer_CountHaveMoney,
                lstCustomerAG.Count(), lstCustomerLMS.Count(), listtotalCountMoney));

            lstLog.Add(lenderManager.SetlogCompare(
               (int)LogCompare_MainType.Customer, (int)LogCompare_DetailType.Customer_TotalMoney,
               lstCustomerAG.Sum(x => (long)x.TotalMoney), lstCustomerLMS.Sum(x => (long)x.TotalMoney)));

            List<int> mainType = new List<int>() { (int)LogCompare_MainType.Customer };
            var lstCurrentLog = logCompareTab.WhereClause(x => x.Status == 1 && x.ForDate == DateTime.Now.Date && mainType.Contains(x.MainType)).Query();
            logCompareTab.DeleteList(lstCurrentLog.ToList());
            logCompareTab.InsertBulk(lstLog);

            var logCompare = new LMSModel.TblLogCompare()
            {
                CreateDate = DateTime.Now,
                DetailType = (int)LogCompare_DetailType.Customer_ListErrorPhone,
                DetailNote = $"Danh sách lệch SĐT : {listCustomerIDErrorPhone}",
                ForDate = DateTime.Now.Date,
                MainType = (int)LogCompare_MainType.Customer,
                Status = 1,
                TotalAG = 0,
                TotalLMS = errorPhone
            };
            logCompareTab.Insert(logCompare);



            var logCompare1 = new LMSModel.TblLogCompare()
            {
                CreateDate = DateTime.Now,
                DetailType = (int)LogCompare_DetailType.Customer_ListMiss,
                DetailNote = $"Danh sách miss LMS : {notExistCustomerID}",
                ForDate = DateTime.Now.Date,
                MainType = (int)LogCompare_MainType.Customer,
                Status = 1,
                TotalAG = 0,
                TotalLMS = missCustomerID
            };
            logCompareTab.Insert(logCompare1);
            Console.WriteLine($"--------------DONE CUSTOMER --------------");
        }

        public List<int> GetCustomerError()
        {
            List<int> loanIdsError = new List<int>();
            string query = @"select LoanID,TimaLoanID,L.CustomerID,C.TimaCustomerID from TblLoan(NOLOCK) L inner join TblCustomer (NOLOCK) C ON L.CustomerID = C.CustomerID";
            string queryAG = @"select l.id,l.CustomerID from tblLoan(nolock) l inner join tblShop (nolock) s on l.ShopID = s.ShopID where s.ShopID = 3703 or s.IsHub = 1";
            var taskLoanLMS = customerLoanTab.QueryAsync(query, null);
            var taskLoanAG = loanAGTab.QueryAsync(queryAG, null);
            Task.WhenAll(taskLoanLMS, taskLoanAG);
            var loanLMS = taskLoanLMS.Result;
            var dicLoanAG = taskLoanAG.Result.ToDictionary(x => x.ID, x => x.CustomerID);
            foreach (var item in loanLMS)
            {
                if (dicLoanAG.ContainsKey((int)item.TimaLoanID) && dicLoanAG[(int)item.TimaLoanID] != item.TimaCustomerID)
                {
                    loanIdsError.Add((int)item.TimaLoanID);
                }
            }
            return loanIdsError;
        }

        public void SyncMoneyCustomerTima(List<long> lstCustomerTimaID)
        {
            var lstCustomerDataAg = customerTimaTab.WhereClause(x => lstCustomerTimaID.Contains(x.CustomerID)).Query();
            var lstCustomerDataLms = customerLMSTab.WhereClause(x => lstCustomerTimaID.Contains((long)x.TimaCustomerID)).Query();
            List<LMSModel.TblCustomer> lstUpdate = new List<LMSModel.TblCustomer>();
            foreach(var item in lstCustomerDataLms)
            {
                var cusAg = lstCustomerDataAg.FirstOrDefault(x => x.CustomerID == item.TimaCustomerID);
                if(cusAg == null || cusAg.CustomerID < 1)
                {
                    Console.WriteLine($"[Error] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} cusAg khong tim thay lmsID={item.CustomerID}");
                    continue;
                }
                item.TotalMoney = cusAg.TotalMoney;
                lstUpdate.Add(item);
            }
            if(lstUpdate.Count > 0)
            {
                customerLMSTab.UpdateBulk(lstUpdate);
            }
            Console.WriteLine($"[Updated] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} done");
        }
    }
}
