﻿using LMS.Common.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigrateAGToLMS.Service
{
    public class CustomerBankManager
    {
        public long MoveData()
        {
            Console.WriteLine($"-------------- CustomerBank -------------");
            var customerBankTab = new TableHelper<LMSModel.TblCustomerBank>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            // Tab AG
            var customerLMSTab = new TableHelper<LMSModel.TblCustomer>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var loanTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var customerAG = new TableHelper<AG.TblCustomerAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);

            long latestID = 0;
            var maxTimaID = customerBankTab.SelectColumns(x => x.TimaCustomerID)
                                                    .OrderByDescending(x => x.TimaCustomerID)
                                                    .SetGetTop(1)
                                                    .Query().FirstOrDefault()?.TimaCustomerID ?? 0;
            latestID = maxTimaID;
            List<LMSModel.TblCustomerBank> lstInsert = new List<LMSModel.TblCustomerBank>();
            int countResult = 0;
            while (true)
            {
                try
                {
                    lstInsert.Clear();
                    var lstDataAG = customerAG.SetGetTop(2000).WhereClause(x => x.CustomerID > latestID && x.Status_Va >= 0).OrderBy(x => x.CustomerID).Query();
                    if (lstDataAG == null || lstDataAG.Count() == 0)
                    {
                        break;
                    }
                    var lstCustomerID = lstDataAG.Select(x => x.CustomerID).ToList();
                    var lstCustomer = customerLMSTab.WhereClause(x => lstCustomerID.Contains((int)x.TimaCustomerID)).Query();
                    var lstLoanCustomerIds = lstCustomer.Select(x => x.CustomerID).ToList();
                    var lstLoan = loanTab.WhereClause(x => lstLoanCustomerIds.Contains((int)x.CustomerID)).Query();
                    foreach (var item in lstDataAG)
                    {
                        if (lstCustomer.Where(x => x.TimaCustomerID == item.CustomerID).Count() > 0)
                        {
                            var customerBank = new LMSModel.TblCustomerBank()
                            {
                                BankName = item.BankName_Va,
                                EndDate = item.EndDate,
                                AccountName = item.Account_name,
                                AccountNo = item.Account_no,
                                CreateDate = item.CreatedDate,
                                BankCode = item.BankCode_Va,
                                MapID = item.Map_id,
                                PartnerCode = 1,
                                StatusVa = item.Status_Va,
                                TimaCustomerID = item.CustomerID,
                            };

                            try
                            {
                                customerBank.CustomerID = lstCustomer.Where(x => x.TimaCustomerID == item.CustomerID).FirstOrDefault().CustomerID;
                                customerBank.LoanID = lstLoan.Where(x => x.CustomerID == customerBank.CustomerID).LastOrDefault()?.LoanID ?? 0;
                                lstInsert.Add(customerBank);
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                    latestID = lstDataAG.Last().CustomerID;
                    if (lstInsert.Count > 0)
                    {
                        customerBankTab.InsertBulk(lstInsert);
                    }
                    Console.WriteLine($"Insert TblCustomerBank: {lstInsert.Count} success");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"move TblCustomerBank error|ex={ex.Message}-{ex.StackTrace}");
                    break;
                }
            }
            Console.WriteLine($"Insert TblCustomerBank finished: {countResult} rows");
            return countResult;
        }
    }
}
