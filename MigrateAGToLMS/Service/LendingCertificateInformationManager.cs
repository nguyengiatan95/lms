﻿using LMS.Common.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigrateAGToLMS.Service
{
    public class LendingCertificateInformationManager
    {
        public void MoveData()
        {
            var common = new LMS.Common.Helper.Utils();

            Console.WriteLine($"-------------- LendingCertificateInformation --------------");
            DateTime dtNow = DateTime.Now;
            var cetificateTab = new TableHelper<LMSModel.TblLendingCertificateInformation>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var loanLMSTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var cetificateAGTab = new TableHelper<AG.LendingCertificateInformationAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);

            long maxTimaID = 0;
            maxTimaID = cetificateTab.SelectColumns(x => x.LendingCertificateInformationID).SetGetTop(1).OrderByDescending(x => x.LendingCertificateInformationID)
                            .Query().FirstOrDefault()?.LendingCertificateInformationID ?? 0;
            while (true)
            {
                List<LMSModel.TblLendingCertificateInformation> lstLMS = new List<LMSModel.TblLendingCertificateInformation>();
                var lstData = cetificateAGTab.SetGetTop(2000).WhereClause(x => x.ID > maxTimaID).OrderBy(x => x.ID).Query();
                var dicLoanLMS = loanLMSTab.SelectColumns(x => x.LoanID, x => x.TimaLoanID).WhereClause(x => x.TimaLoanID > 0).Query().ToDictionary(x => x.TimaLoanID, x => x.LoanID);
                //var lstLMS = common.ConvertParentToChild<IEnumerable<LMSModel.TblLendingCertificateInformation>, IEnumerable<AG.LendingCertificateInformationAG>>(lstData);
                foreach (var item in lstData)
                {
                    if (dicLoanLMS.ContainsKey(item.LoanID))
                    {
                        lstLMS.Add(new LMSModel.TblLendingCertificateInformation()
                        {
                            CreateDate = item.CreateDate,
                            CustomerAddress = item.CustomerAddress,
                            CustomerContactNumber = item.CustomerContact_number,
                            CustomerDateOfBirth = item.CustomerDate_of_birth,
                            CustomerEmail = item.CustomerEmail,
                            CustomerFullname = item.CustomerFullname,
                            CustomerIdCard = item.CustomerId_card,
                            CustomerPhone = item.CustomerPhone,
                            FromDate = item.From_date,
                            FromHour = item.From_hour,
                            FromMonth = item.From_month,
                            FromYear = item.From_year,
                            InvestorAddress = item.InvestorAddress,
                            InvestorContactNumber = item.InvestorContact_number,
                            InvestorDateOfBirth = item.InvestorDate_of_birth,
                            InvestorEmail = item.InvestorEmail,
                            InvestorFullname = item.InvestorFullname,
                            InvestorIdCard = item.InvestorId_card,
                            InvestorInvestMoney = item.InvestorInvest_money,
                            InvestorPhone = item.InvestorPhone,
                            InvestorProduct = item.InvestorProduct,
                            InvestorRate = item.InvestorRate,
                            InvestorTaxCode = item.InvestorTax_code,
                            LendingCertificateInformationID = item.ID,
                            LoanID = (int)dicLoanLMS[item.LoanID],
                            MoneyIndemnification = item.Money_Indemnification,
                            NumberInsurance = item.NumberInsurance,
                            ToDate = item.To_date,
                            ToHour = item.To_hour,
                            ToMonth = item.To_month,
                            ToYear = item.To_year
                        });
                    }

                }
                cetificateTab.InsertBulk(lstLMS, setOffIdentity: true);
                if (lstData == null || lstData.Count() == 0)
                {
                    break;
                }
                maxTimaID = lstData.Max(x => x.ID);
                Console.WriteLine($"--------------INSERT THANH CONG  cetificateAG {lstLMS.Count} BAN GHI--------------");
            }
            Console.WriteLine($"--------------chay xong LendingCertificateInformation --------------");
        }
    }
}
