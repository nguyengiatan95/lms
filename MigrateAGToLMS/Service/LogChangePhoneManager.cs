﻿using LMS.Common.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigrateAGToLMS.Service
{
    public class LogChangePhoneManager
    {
        TableHelper<AG.TblLogChangePhoneAG> _agLogChangePhoneTab;
        TableHelper<LMSModel.TblLogChangePhone> _lmsLogChangePhoneTab;
        TableHelper<LMSModel.TblLoan> _lmsLoanTab;
        public LogChangePhoneManager()
        {
            _agLogChangePhoneTab = new TableHelper<AG.TblLogChangePhoneAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            _lmsLogChangePhoneTab = new TableHelper<LMSModel.TblLogChangePhone>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            _lmsLoanTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
        }
        public void MoveLogChangePhone()
        {

            int countResult = 0;
            var LMSLogID = 0;
            while (true)
            {
                try
                {

                    List<LMSModel.TblLogChangePhone> lstRun = new List<LMSModel.TblLogChangePhone>();
                    //var lmsChangeLogs = _lmsLogChangePhoneTab.SetGetTop(1).OrderByDescending(x => x.TimaLogChangePhoneID).Query();
                   
                    DateTime minLMS = new DateTime(2021, 09, 20, 13, 12, 0);
                    var lstAG = _agLogChangePhoneTab.SetGetTop(2000).WhereClause(x => x.Id > LMSLogID && x.CreateOn < minLMS).OrderBy(x=>x.Id).Query();
                    if (lstAG == null || lstAG.Count() == 0)
                    {
                        break;
                    }
                    var lstLoanIds = lstAG.Select(x => x.LoanId).ToList();
                    var dicLoan = _lmsLoanTab.WhereClause(x => lstLoanIds.Contains((int)x.TimaLoanID)).Query().ToDictionary(x => x.TimaLoanID, x => x);
                    foreach (var item in lstAG)
                    {
                        try
                        {
                            lstRun.Add(new LMSModel.TblLogChangePhone()
                            {
                                CreateBy = item.UserId,
                                LoanID = dicLoan[item.LoanId].LoanID,
                                CreateByName = item.UserFullName,
                                CreateDate = item.CreateOn,
                                CustomerID = dicLoan[item.LoanId].CustomerID,
                                NewPhone = item.PhoneNew,
                                OldPhone = item.PhoneOld,
                                Note = item.Note,
                                TimaLogChangePhoneID = (long)item.Id
                            });
                            LMSLogID = item.Id;
                        }
                        catch (Exception ex)
                        {

                        }
                        countResult += 1;
                    }
                    _lmsLogChangePhoneTab.InsertBulk(lstRun);
                    Console.WriteLine($"Insert TblLogChangePhone: {lstRun.Count} success");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"move TblLogChangePhone error|ex={ex.Message}-{ex.StackTrace}");
                    break;
                }
            }
            Console.WriteLine($"Insert TblLogChangePhone finished: {countResult} rows");
        }
    }
}
