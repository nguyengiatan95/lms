﻿using LMS.Common.DAL;
using MigrateAGToLMS.AG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MigrateAGToLMS.Service
{
    public class UserManager
    {
        public void MoveUserTima()
        {
            try
            {
                Console.WriteLine($"-------------- User -------------");
                DateTime dtNow = DateTime.Now;
                var userLMSTab = new TableHelper<LMSModel.TblUser>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                var userGroupTab = new TableHelper<LMSModel.TblUserGroup>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                var userDepartmentTab = new TableHelper<LMSModel.TblUserDepartment>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);

                var lstMaxTimaUserID = userLMSTab.SelectColumns(x => x.TimaUserID)
                                                         .OrderByDescending(x => x.TimaUserID)
                                                         .WhereClause(x => x.TimaUserID > 0).SetGetTop(1).Query();
                long maxTimaBankCardID = 0;
                int maxRowGet = 2000;
                if (lstMaxTimaUserID != null && lstMaxTimaUserID.Any())
                {
                    maxTimaBankCardID = lstMaxTimaUserID.First().TimaUserID;
                }
                while (true)
                {
                    var userTimaTab = new TableHelper<AG.TblUserAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                    var lstUserAG = userTimaTab.WhereClause(x => x.Id > maxTimaBankCardID)
                    .OrderBy(x => x.Id).SetGetTop(maxRowGet).Query();
                    var lstUserInsert = lstUserAG.OrderBy(x => x.Id).ToList();
                    if (lstUserInsert.Count > 0)
                    {
                        List<LMSModel.TblUser> lstUser = new List<LMSModel.TblUser>();
                        List<LMSModel.TblUserGroup> lstUserGroup = new List<LMSModel.TblUserGroup>();
                        List<LMSModel.TblUserDepartment> lstUserDepartment = new List<LMSModel.TblUserDepartment>();
                        foreach (var item in lstUserInsert)
                        {
                            lstUser.Add(new LMSModel.TblUser()
                            {
                                UserID = item.Id,
                                UserName = item.Username,
                                TimaUserID = item.Id,
                                CreateDate = DateTime.Now,
                                DepartmentID = 0,
                                Email = item.Email,
                                FullName = item.FullName,
                                Password = item.Password,
                                Status = ConvertStatusToLMS(item.Status)
                            });
                            lstUserGroup.Add(new LMSModel.TblUserGroup()
                            {
                                UserID = item.Id,
                                GroupID = item.GroupId
                            });
                            // insert của call thn vào departmentUser vào trước
                            if (item.GroupId == 21 || item.GroupId == 26)
                            {
                                lstUserDepartment.Add(new LMSModel.TblUserDepartment()
                                {
                                    CreateDate = dtNow,
                                    DepartmentID = 105,
                                    PositionID = item.GroupId == 26 ? (int)LMS.Common.Constants.UserDepartment_PositionID.Manager : (int)LMS.Common.Constants.UserDepartment_PositionID.Staff,
                                    Status = 1,
                                    ModifyDate = dtNow,
                                    UserID = item.Id
                                });

                            }
                            maxTimaBankCardID = item.Id;
                        }
                        Console.WriteLine($"[Inserting] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} bat dau insert User");
                        userLMSTab.InsertBulk(lstUser, setOffIdentity: true);
                        userGroupTab.InsertBulk(lstUserGroup);
                        userDepartmentTab.InsertBulk(lstUserDepartment);
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert User  -  {lstUser.Count()} ");
                    }
                    else
                    {
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert User hoan tat");
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"MoveUser - ERROR {ex.Message}");
            }
        }

        int ConvertStatusToLMS(int status)
        {
            switch ((Base_Status)status)
            {
                case Base_Status.Complete:
                    {
                        return (int)LMS.Common.Constants.User_Status.Active;
                    }
                case Base_Status.Deleted:
                    {
                        return (int)LMS.Common.Constants.User_Status.Deleted;
                    }
                case Base_Status.NotComplete:
                    {
                        return (int)LMS.Common.Constants.User_Status.Locked;
                    }
                default:
                    {
                        return -1;
                    }
            }
        }

        public void MoveUserConfigServiceCall()
        {
            Console.WriteLine($"-------------- MoveUserConfigServiceCall -------------");
            var userTimaTab = new TableHelper<AG.TblUserAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            var mapUserCall = new TableHelper<LMSModel.TblMapUserCall>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            // lấy danh sách nhân viên phòng thu hồi nợ groupID in (21,26)
            // servicecall is not null
            // 2: caresoft, !=2 cisco
            var lstMaxTimaUserID = mapUserCall.SelectColumns(x => x.UserID)
                                                        .OrderByDescending(x => x.UserID)
                                                        .WhereClause(x => x.UserID > 0).SetGetTop(1).Query();
            LMS.Common.Helper.Utils common = new LMS.Common.Helper.Utils();
            long maxUserID = 0;
            int maxRowGet = 2000;
            int countRows = 0;
            List<int> lstGroupID = new List<int> { 21, 26 };
            if (lstMaxTimaUserID != null && lstMaxTimaUserID.Any())
            {
                maxUserID = lstMaxTimaUserID.First().UserID;
            }
            List<LMSModel.TblMapUserCall> lstDataInsert = new List<LMSModel.TblMapUserCall>();
            while (true)
            {
                lstDataInsert.Clear();
                var lstDataAg = userTimaTab.SetGetTop(maxRowGet).WhereClause(x => x.Id > maxUserID && x.Status == 1)
                                           .WhereClause(x => lstGroupID.Contains(x.GroupId))
                                           .OrderBy(x => x.Id).Query();
                if (lstDataAg == null || !lstDataAg.Any())
                {
                    break;
                }
                maxUserID = lstDataAg.OrderBy(x => x.Id).Last().Id;
                LMSModel.TblMapUserCall userCall;
                LMSModel.UserCallCareSoftExtra userCallCareSoft;
                LMSModel.UserCallCiscoExtra userCallCisco;
                foreach (var item in lstDataAg)
                {
                    userCall = null;
                    userCallCareSoft = null;
                    userCallCisco = null;
                    if (!string.IsNullOrEmpty(item.IPPhone))
                    {
                        userCallCareSoft = new LMSModel.UserCallCareSoftExtra
                        {
                            IPPhone = item.IPPhone
                        };
                    }
                    if (!string.IsNullOrEmpty(item.CiscoUsername))
                    {
                        userCallCisco = new LMSModel.UserCallCiscoExtra
                        {
                            Username = item.CiscoUsername,
                            Password = item.CiscoPassword,
                            Extension = item.CiscoExtension
                        };
                    }
                    if (item.ServiceCall.HasValue)
                    {
                        countRows++;
                        if (userCallCareSoft != null)
                        {
                            userCall = new LMSModel.TblMapUserCall
                            {
                                CreateDate = item.CreateDate,
                                ModifyDate = item.CreateDate,
                                JsonExtra = common.ConvertObjectToJSonV2(userCallCareSoft),
                                TypeCall = (int)LMS.Common.Constants.MapUserCall_TypeCall.Caresoft,
                                UserID = item.Id,
                                Status = item.ServiceCall.Value != 2 ? (int)LMS.Common.Constants.StatusCommon.Active : (int)LMS.Common.Constants.StatusCommon.UnActive
                            };
                            lstDataInsert.Add(userCall);
                        }
                        if (userCallCisco != null)
                        {
                            userCall = new LMSModel.TblMapUserCall
                            {
                                CreateDate = item.CreateDate,
                                ModifyDate = item.CreateDate,
                                JsonExtra = common.ConvertObjectToJSonV2(userCallCisco),
                                TypeCall = (int)LMS.Common.Constants.MapUserCall_TypeCall.Cisco,
                                UserID = item.Id,
                                Status = item.ServiceCall.Value == 2 ? (int)LMS.Common.Constants.StatusCommon.Active : (int)LMS.Common.Constants.StatusCommon.UnActive
                            };
                            lstDataInsert.Add(userCall);
                        }
                    }

                }
                if (lstDataInsert.Count > 0)
                {
                    Console.WriteLine($"--- insert data mapusercall {lstDataInsert.Count} rows-----");
                    mapUserCall.InsertBulk(lstDataInsert);
                }
            }

            Console.WriteLine($"--- Finsished move data mapusercall {countRows} rows-----");
        }


        public void MoveUserTima(List<long> lstUserIDAG)
        {
            try
            {
                Console.WriteLine($"-------------- User -------------");
                DateTime dtNow = DateTime.Now;
                var userLMSTab = new TableHelper<LMSModel.TblUser>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                var userGroupTab = new TableHelper<LMSModel.TblUserGroup>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                var userDepartmentTab = new TableHelper<LMSModel.TblUserDepartment>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);

                var lstMaxTimaUserID = userLMSTab.SelectColumns(x => x.TimaUserID)
                                                         .OrderByDescending(x => x.TimaUserID)
                                                         .WhereClause(x => x.TimaUserID > 0).SetGetTop(1).Query();
                var userTimaTab = new TableHelper<AG.TblUserAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                var lstUserAG = userTimaTab.WhereClause(x => lstUserIDAG.Contains(x.Id)).OrderBy(x => x.Id).Query();
                var lstUserInsert = lstUserAG.OrderBy(x => x.Id).ToList();
                if (lstUserInsert.Count > 0)
                {
                    List<LMSModel.TblUser> lstUser = new List<LMSModel.TblUser>();
                    List<LMSModel.TblUserGroup> lstUserGroup = new List<LMSModel.TblUserGroup>();
                    List<LMSModel.TblUserDepartment> lstUserDepartment = new List<LMSModel.TblUserDepartment>();
                    foreach (var item in lstUserInsert)
                    {
                        lstUser.Add(new LMSModel.TblUser()
                        {
                            UserID = item.Id,
                            UserName = item.Username,
                            TimaUserID = item.Id,
                            CreateDate = DateTime.Now,
                            DepartmentID = 0,
                            Email = item.Email,
                            FullName = item.FullName,
                            Password = item.Password,
                            Status = ConvertStatusToLMS(item.Status)
                        });
                        lstUserGroup.Add(new LMSModel.TblUserGroup()
                        {
                            UserID = item.Id,
                            GroupID = item.GroupId
                        });
                        // insert của call thn vào departmentUser vào trước
                        if (item.GroupId == 21 || item.GroupId == 26)
                        {
                            lstUserDepartment.Add(new LMSModel.TblUserDepartment()
                            {
                                CreateDate = dtNow,
                                DepartmentID = 105,
                                PositionID = item.GroupId == 26 ? (int)LMS.Common.Constants.UserDepartment_PositionID.Manager : (int)LMS.Common.Constants.UserDepartment_PositionID.Staff,
                                Status = 1,
                                ModifyDate = dtNow,
                                UserID = item.Id
                            });

                        }
                    }
                    Console.WriteLine($"[Inserting] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} bat dau insert User");
                    userLMSTab.InsertBulk(lstUser, setOffIdentity: true);
                    userGroupTab.InsertBulk(lstUserGroup);
                    userDepartmentTab.InsertBulk(lstUserDepartment);
                    Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert User  -  {lstUser.Count()} ");
                }
                else
                {
                    Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert User hoan tat");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"MoveUser - ERROR {ex.Message}");
            }
        }
    }
}
