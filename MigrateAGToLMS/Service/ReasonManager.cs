﻿using LMS.Common.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigrateAGToLMS.Service
{
    public class ReasonManager
    {
        public void MoveReasonTima()
        {
            try
            {
                Console.WriteLine($"-------------- ReasonManager -------------");

                DateTime dtNow = DateTime.Now;
                var reasonLMSTab = new TableHelper<LMSModel.TblReason>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                var reasonTimaTab = new TableHelper<AG.TblReasonAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                var lastReasonIDTimaData = reasonLMSTab.SelectColumns(x => x.ReasonID).OrderByDescending(x => x.ReasonID).SetGetTop(1).Query();
                var lastReasonIDTima = lastReasonIDTimaData != null && lastReasonIDTimaData.Any() ? lastReasonIDTimaData.First().ReasonID : 0;
                var lstTimaData = reasonTimaTab.WhereClause(x => x.Id > lastReasonIDTima).Query();
                List<LMSModel.TblReason> lstLMSData = new List<LMSModel.TblReason>();
                foreach (var item in lstTimaData)
                {
                    lstLMSData.Add(new LMSModel.TblReason()
                    {
                        DepartmentID = item.Type,
                        Description = item.Reason,
                        ReasonCode = item.ReasonCode,
                        ReasonID = item.Id,
                        Status = item.IsEnable == true ? (int)LMS.Common.Constants.StatusCommon.Active : (int)LMS.Common.Constants.StatusCommon.UnActive
                    });
                }
                reasonLMSTab.InsertBulk(lstLMSData, setOffIdentity: true);
                Console.WriteLine($"MoveReasonManager - SUCCESSS thanh cong");

            }
            catch (Exception ex)
            {
                Console.WriteLine($"MoveReasonManager - ERROR {ex.Message}");
            }
        }
        public void MoveReasonCodeLMSToAG()
        {
            try
            {
                Console.WriteLine($"-------------- ReasonManager -------------");

                DateTime dtNow = DateTime.Now;
                var reasonLMSTab = new TableHelper<LMSModel.TblReason>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                var reasonTimaTab = new TableHelper<AG.TblReasonAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                var lastReasonIDTimaData = reasonTimaTab.SelectColumns(x => x.Id).OrderByDescending(x => x.Id).SetGetTop(1).Query();
                var lastReasonIDTima = lastReasonIDTimaData != null && lastReasonIDTimaData.Any() ? lastReasonIDTimaData.First().Id : 0;
                var lstTimaData = reasonLMSTab.WhereClause(x => x.ReasonID > lastReasonIDTima).Query();
                List<AG.TblReasonAG> lstLMSData = new List<AG.TblReasonAG>();
                foreach (var item in lstTimaData)
                {
                    lstLMSData.Add(new AG.TblReasonAG()
                    {
                        //DepartmentID = item.Type,
                        //Description = item.Reason,
                        //ReasonCode = item.ReasonCode,
                        //ReasonID = item.Id,
                        //Status = item.IsEnable == true ? (int)LMS.Common.Constants.StatusCommon.Active : (int)LMS.Common.Constants.StatusCommon.UnActive
                        Type = item.DepartmentID,
                        Id = (int)item.ReasonID,
                        ReasonCode = item.ReasonCode,
                        Reason = item.Description,
                        IsEnable = true
                    });
                }
                reasonTimaTab.InsertBulk(lstLMSData, setOffIdentity: true);
                Console.WriteLine($"MoveReasonManager - SUCCESSS thanh cong");

            }
            catch (Exception ex)
            {
                Console.WriteLine($"MoveReasonCodeLMSToAG - ERROR {ex.Message}");
            }
        }
    }
}
