﻿using LMS.Common.Constants;
using LMS.Common.DAL;
using MigrateAGToLMS.LMSModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigrateAGToLMS.Service
{
    public class CutOffLoanManager
    {
        TableHelper<TblCutOffLoan> _cutoffLoanLMSTab;
        TableHelper<TblLoan> _loanLMSTab;
        TableHelper<AG.TblLoanAG> _loanAGTab;
        TableHelper<AG.TblLogLoanByDay> _logLoanByDayTab;
        TableHelper<TblTransaction> _transactionTab;
        public CutOffLoanManager()
        {
            _cutoffLoanLMSTab = _cutoffLoanLMSTab ?? new TableHelper<TblCutOffLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            _loanLMSTab = _loanLMSTab ?? new TableHelper<TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            _loanAGTab = _loanAGTab ?? new TableHelper<AG.TblLoanAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            _logLoanByDayTab = _logLoanByDayTab ?? new TableHelper<AG.TblLogLoanByDay>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            _transactionTab = _transactionTab ?? new TableHelper<TblTransaction>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
        }

        public void MoveCutOffLoan()
        {
            Console.WriteLine($"--------------Move MoveCutOffLoan --------------");
            DateTime dtNow = DateTime.Now;
            //string sql = "truncate table TblCutOffLoan";
            string sql = "delete TblCutOffLoan where CutOffDate>='2022-01-31'";

            _cutoffLoanLMSTab.Execute(sql);
            //DateTime MinDate = _logLoanByDayTab.Query("select min(ForDate) ForDate from tblLogLoanByDay", null).First().ForDate;
            DateTime MinDate = new DateTime(2022, 01, 31);
            DateTime MaxDate = DateTime.Now.Date;
            // Lấy loanID của tran của lms theo từng ngày từ ngày min date -> cuối ngày hôm qua

            DateTime toDate = MinDate;
            while (true)
            {
                toDate = MinDate.AddDays(1);
                if (toDate.Day == 1)
                {
                    //_cutoffLoanLMSTab.DeleteWhere(x => x.CutOffDate == MinDate);
                    var lstLogLoanAGDB = _logLoanByDayTab.SelectColumns(x => x.LoanID, x => x.ForDate, x => x.TotalMoneyCurrent, x => x.LastDateOfPay, x => x.NextDate, x => x.Status, x => x.TypeCloseLoan)
                     .WhereClause(x => x.ForDate == MinDate).Query();
                    var lstTimaLoanID = lstLogLoanAGDB.Select(x => x.LoanID).ToList();
                    int pageSize = 2000;
                    int numberRow = lstTimaLoanID.Count() / pageSize + 1;
                    for (int i = 1; i <= numberRow; i++)
                    {
                        var lstLogLoanAG = lstLogLoanAGDB.Where(x => lstTimaLoanID.Contains(x.LoanID)).ToList();
                        List<TblCutOffLoan> tblCutOffLoans = new List<TblCutOffLoan>();
                        var lstLoanIdSkip = lstTimaLoanID.Skip((i - 1) * pageSize).Take(pageSize).ToList();
                        var loanLMS = _loanLMSTab.SelectColumns(x => x.LoanID, x => x.TimaLoanID, x => x.LenderID, x => x.OwnerShopID,
                                     x => x.ConsultantShopID, x => x.CustomerID, x => x.Frequency, x => x.FromDate).WhereClause(x => lstLoanIdSkip.Contains((int)x.TimaLoanID)).Query();
                        var dicLoanLms = loanLMS.ToDictionary(x => x.TimaLoanID, x => x);
                        foreach (var item in lstLogLoanAG)
                        {
                            try
                            {
                                var loanDetailLMS = dicLoanLms.GetValueOrDefault(item.LoanID);
                                if (loanDetailLMS != null && loanDetailLMS.LoanID > 0)
                                {
                                    tblCutOffLoans.Add(new TblCutOffLoan()
                                    {
                                        LoanID = loanDetailLMS.LoanID,
                                        ConsultantShopID = loanDetailLMS.ConsultantShopID ?? 0,
                                        CreateDate = dtNow,
                                        CustomerID = loanDetailLMS.CustomerID ?? 0,
                                        CutOffDate = item.ForDate,
                                        LastDateOfPay = item.LastDateOfPay,
                                        LenderID = loanDetailLMS.LenderID ?? 0,
                                        NextDate = item.NextDate == null ? loanDetailLMS.FromDate.Value.AddMonths(loanDetailLMS.Frequency ?? 0) : item.NextDate.Value,
                                        OwnerShopID = loanDetailLMS.OwnerShopID ?? 0,
                                        TotalMoneyCurrent = item.TotalMoneyCurrent ?? 0,
                                        Status = ConvertStatusAG((int)item.Status, item.TypeCloseLoan),
                                    });
                                }

                            }
                            catch (Exception ex)
                            {

                                Console.WriteLine($"[ERRor] LoanTima : {item.LoanID} rows : {MinDate.ToString("dd/MM/yyyy")} --------------");
                            }
                        }
                        _cutoffLoanLMSTab.InsertBulk(tblCutOffLoans);
                        Console.WriteLine($"[Success] tblCutOffLoans : {tblCutOffLoans.Count} rows : {MinDate.ToString("dd/MM/yyyy")} --------------");
                    }
                }
                else
                {
                    var lstLoanTranLMS = _transactionTab.SelectColumns(x => x.LoanID).WhereClause(x => x.CreateDate >= MinDate && x.CreateDate < toDate).Query();
                    var lstLoanLMSID = lstLoanTranLMS.Select(x => x.LoanID).Distinct();
                    // check xem bao bản ghi, lấy 2000 bản ghi 1 để chạy dữ liệu
                    int pageSize = 2000;
                    int numberRow = lstLoanLMSID.Count() / pageSize + 1;
                    for (int i = 1; i <= numberRow; i++)
                    {
                        List<TblCutOffLoan> tblCutOffLoans = new List<TblCutOffLoan>();
                        var lstLoanIdSkip = lstLoanLMSID.Skip((i - 1) * pageSize).Take(pageSize).ToList();
                        // lấy loan của tima
                        var loanLMS = _loanLMSTab.SelectColumns(x => x.LoanID, x => x.TimaLoanID, x => x.LenderID, x => x.OwnerShopID,
                            x => x.ConsultantShopID, x => x.CustomerID, x => x.Frequency, x => x.FromDate)
                            .WhereClause(x => lstLoanIdSkip.Contains(x.LoanID)).Query();
                        var lstLoanTimaID = loanLMS.Select(x => x.TimaLoanID).ToList();
                        // đọc logloan của ag -> insert vào bên lms
                        var lstLogLoanAG = _logLoanByDayTab.SelectColumns(x => x.LoanID, x => x.ForDate, x => x.TotalMoneyCurrent, x => x.LastDateOfPay, x => x.NextDate, x => x.Status, x => x.TypeCloseLoan)
                            .WhereClause(x => x.ForDate == MinDate && lstLoanTimaID.Contains(x.LoanID)).Query();
                        var dicLoanLms = loanLMS.ToDictionary(x => x.TimaLoanID, x => x);
                        foreach (var item in lstLogLoanAG)
                        {
                            try
                            {
                                var objLoan = dicLoanLms.GetValueOrDefault(item.LoanID);
                                tblCutOffLoans.Add(new TblCutOffLoan()
                                {
                                    LoanID = objLoan.LoanID,
                                    ConsultantShopID = objLoan.ConsultantShopID ?? 0,
                                    CreateDate = dtNow,
                                    CustomerID = objLoan.CustomerID ?? 0,
                                    CutOffDate = item.ForDate,
                                    LastDateOfPay = item.LastDateOfPay,
                                    LenderID = objLoan.LenderID ?? 0,
                                    NextDate = item.NextDate == null ? objLoan.FromDate.Value.AddMonths(objLoan.Frequency ?? 0) : item.NextDate.Value,
                                    OwnerShopID = objLoan.OwnerShopID ?? 0,
                                    TotalMoneyCurrent = item.TotalMoneyCurrent ?? 0,
                                    Status = ConvertStatusAG((int)item.Status, item.TypeCloseLoan),
                                });
                            }
                            catch (Exception ex)
                            {

                                Console.WriteLine($"[ERRor] LoanTima : {item.LoanID} rows : {MinDate.ToString("dd/MM/yyyy")} --------------");
                            }

                        }
                        _cutoffLoanLMSTab.InsertBulk(tblCutOffLoans);
                        Console.WriteLine($"[Success] tblCutOffLoans : {tblCutOffLoans.Count} rows : {MinDate.ToString("dd/MM/yyyy")} --------------");
                    }

                }
                MinDate = MinDate.AddDays(1);
                if (MinDate >= MaxDate)
                {
                    break;
                }
                Console.WriteLine($"[Success] Move MoveCutOffLoan DONE --------------");
            }
        }
        private int ConvertStatusAG(int status, int? typeCloseLoan)
        {
            switch ((AG.Status_Loan)status)
            {
                case AG.Status_Loan.Lending:
                case AG.Status_Loan.ToDayPay:
                case AG.Status_Loan.DelayInterest:
                case AG.Status_Loan.Delayed:
                case AG.Status_Loan.Delayed2:
                case AG.Status_Loan.WaitLiquidation:
                    return (int)Loan_Status.Lending;
                case AG.Status_Loan.Acquision:
                    if (!typeCloseLoan.HasValue)
                    {
                        return (int)Loan_Status.Close;
                    }
                    switch ((AG.Type_CloseLoan)typeCloseLoan)
                    {
                        case AG.Type_CloseLoan.Finalization:
                            return (int)Loan_Status.Close;
                        case AG.Type_CloseLoan.Liquidation:
                            return (int)Loan_Status.CloseLiquidation;
                        case AG.Type_CloseLoan.LiquidationLoanInsurance:
                            return (int)Loan_Status.CloseLiquidationLoanInsurance;
                        case AG.Type_CloseLoan.IndemnifyLoanInsurance:
                            return (int)Loan_Status.CloseIndemnifyLoanInsurance;
                        case AG.Type_CloseLoan.PaidInsurance:
                            return (int)Loan_Status.ClosePaidInsurance;
                        case AG.Type_CloseLoan.Exemption:
                            return (int)Loan_Status.CloseExemption;
                        case AG.Type_CloseLoan.PaidInsuranceExemption:
                            return (int)Loan_Status.ClosePaidInsuranceExemption;
                        default:
                            return (int)Loan_Status.Close;
                    }
                case AG.Status_Loan.Deleted:
                    return (int)Loan_Status.Delete;
                default:
                    return (int)Loan_Status.Delete;
            }
        }
    }
}
