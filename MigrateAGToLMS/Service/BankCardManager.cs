﻿using LMS.Common.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MigrateAGToLMS.Service
{
    public class BankCardManager
    {
        public void MoveBankCardTima()
        {
            try
            {
                Console.WriteLine($"-------------- BankCard -------------");
                while (true)
                {
                    DateTime dtNow = DateTime.Now;
                    var bankCardLMSTab = new TableHelper<LMSModel.TblBankCard>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                    var bankCardTimaTab = new TableHelper<AG.TblBankCardAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                    var lstMaxTimaBankCardID = bankCardLMSTab.SelectColumns(x => x.TimaBankCardID)
                                                             .OrderByDescending(x => x.TimaBankCardID)
                                                             .WhereClause(x => x.TimaBankCardID > 0).SetGetTop(1).Query();
                    long maxTimaBankCardID = 0;
                    int maxRowGet = 2000;
                    if (lstMaxTimaBankCardID != null && lstMaxTimaBankCardID.Any())
                    {
                        maxTimaBankCardID = lstMaxTimaBankCardID.First().TimaBankCardID;
                    }
                    var lstBankCardAG = bankCardTimaTab.WhereClause(x => x.Id > maxTimaBankCardID)
                     .OrderBy(x => x.Id).SetGetTop(maxRowGet).Query();
                    // user chưa insert 
                    var lstBankCardNeedInsert = lstBankCardAG.OrderBy(x => x.Id).ToList();
                    if (lstBankCardNeedInsert.Count > 0)
                    {
                        List<LMSModel.TblBankCard> lstBankCard = new List<LMSModel.TblBankCard>();
                        foreach (var item in lstBankCardNeedInsert)
                        {
                            lstBankCard.Add(new LMSModel.TblBankCard()
                            {
                                BankID = item.BankId,
                                TimaBankCardID = item.Id,
                                BankCardID = item.Id,
                                AccountHolderName = item.AccountHolderName,
                                AliasName = item.AliasName,
                                AliasNameForFast = item.AliasName,
                                BankCode = item.BankCode,
                                BranchName = item.BranchName,
                                CreateOn = item.CreateOn,
                                ModifyOn = item.ModifyOn,
                                NumberAccount = item.NumberAccount,
                                ParentID = item.ParentId,
                                Status = item.Status,
                                TypePurpose = item.TypePurpose
                            });
                            maxTimaBankCardID = item.Id;
                        }
                        Console.WriteLine($"[Inserting] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} bat dau insert bankCard");
                        bankCardLMSTab.InsertBulk(lstBankCard, setOffIdentity: true);
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert bankCard  -  {lstBankCard.Count()} ");
                    }
                    else
                    {
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert bankCard hoan tat");
                        break;
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine($"MoveBankCardTima - ERROR {ex.Message}");
            }
        }


        public void MoveCashBankCardByDateLMSToLos()
        {
            var cashBankCardByDateLMSTab = new TableHelper<LMSModel.TblCashBankCardByDate>().SetConnectString(Ultil.Constant.ConnectionStringLMS);
            var cashBankCardByDateLMSLOSTab = new TableHelper<LMSModel.TblCashBankCardByDate>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            int maxItem = 2000;
            var lstMaxCashBankCardByDateID = cashBankCardByDateLMSLOSTab.SelectColumns(x => x.CashBankCardByDateID)
                                            .OrderByDescending(x => x.CashBankCardByDateID)
                                            .WhereClause(x => x.CashBankCardByDateID > 0).SetGetTop(1).Query().FirstOrDefault()?.CashBankCardByDateID ?? 0;
            long latestCashBankCardByDateID = lstMaxCashBankCardByDateID;
            while (true)
            {
                var lstLoanLMS = cashBankCardByDateLMSTab.SetGetTop(maxItem).WhereClause(x => x.CashBankCardByDateID > latestCashBankCardByDateID).Query();
                if (lstLoanLMS == null || !lstLoanLMS.Any())
                {
                    Console.WriteLine($"----MOVE TblCashBankCardByDate LMS DONE");
                    return;
                }
                lstLoanLMS = lstLoanLMS.OrderBy(x => x.CashBankCardByDateID);
                latestCashBankCardByDateID = lstLoanLMS.Last().CashBankCardByDateID;
                cashBankCardByDateLMSLOSTab.InsertBulk(lstLoanLMS, setOffIdentity: true);
                Console.WriteLine($"----INSERT TblCashBankCardByDate LMS: {lstLoanLMS.Count()} item");
            }
        }

        public void MoveSettingDateBankCardLMSToLos()
        {
            var settingDateBankCardLMSTab = new TableHelper<LMSModel.TblSettingDateBankCard>().SetConnectString(Ultil.Constant.ConnectionStringLMS);
            var settingDateBankCardLMSLOSTab = new TableHelper<LMSModel.TblSettingDateBankCard>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            int maxItem = 2000;
            var lstMaxSettingDateBankCardID = settingDateBankCardLMSLOSTab.SelectColumns(x => x.SettingDateBankCardID)
                                            .OrderByDescending(x => x.SettingDateBankCardID)
                                            .WhereClause(x => x.SettingDateBankCardID > 0).SetGetTop(1).Query().FirstOrDefault()?.SettingDateBankCardID ?? 0;
            long latestSettingDateBankCardID = lstMaxSettingDateBankCardID;
            while (true)
            {
                var lstLoanLMS = settingDateBankCardLMSTab.SetGetTop(maxItem).WhereClause(x => x.SettingDateBankCardID > latestSettingDateBankCardID).Query();
                if (lstLoanLMS == null || !lstLoanLMS.Any())
                {
                    Console.WriteLine($"----MOVE TblSettingDateBankCard LMS DONE");
                    return;
                }
                lstLoanLMS = lstLoanLMS.OrderBy(x => x.SettingDateBankCardID);
                latestSettingDateBankCardID = lstLoanLMS.Last().SettingDateBankCardID;
                settingDateBankCardLMSLOSTab.InsertBulk(lstLoanLMS, setOffIdentity: true);
                Console.WriteLine($"----INSERT TblSettingDateBankCard LMS: {lstLoanLMS.Count()} item");
            }
        }
    }
}
