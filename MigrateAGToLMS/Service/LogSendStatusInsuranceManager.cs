﻿using LMS.Common.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigrateAGToLMS.Service
{
    public class LogSendStatusInsuranceManager
    {
        public void MoveData()
        {
            Console.WriteLine($"-------------- LogSendStatusInsurance --------------");
            DateTime dtNow = DateTime.Now;
            var logSendInsuranceTab = new TableHelper<LMSModel.TblLogSendStatusInsurance>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var logSendAGTab = new TableHelper<AG.LogSendStatusInsuranceAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            long maxTimaID = 0;
            maxTimaID = logSendAGTab.SetGetTop(1).SelectColumns(x => x.ID).OrderByDescending(x=>x.ID).Query().FirstOrDefault().ID;
            while (true)
            {
                var lstData = logSendAGTab.SetGetTop(2000).WhereClause(x => x.ID > maxTimaID).OrderBy(X => X.ID).Query();
                var lstLMS = new List<LMSModel.TblLogSendStatusInsurance>();
                foreach (var item in lstData)
                {
                    lstLMS.Add(new LMSModel.TblLogSendStatusInsurance()
                    {
                        CreateDate = item.CreateDate.Value,
                        LoanID = item.LoanID ?? 0,
                        LogSendStatusInsuranceID = item.ID,
                        Note = item.Note,
                        StatusNew = item.StatusNew,
                        StatusOld = item.StatusOld,
                        UserID = item.UserID ?? 0,
                    });
                    maxTimaID = item.ID;
                }
                logSendInsuranceTab.InsertBulk(lstLMS, setOffIdentity: true);
                if (lstData == null || lstData.Count() == 0)
                {
                    break;
                }
                Console.WriteLine($"--------------INSERT THANH CONG  logSendInsuranceTab {2000} BAN GHI--------------");
            }

            Console.WriteLine($"--------------chay xong logSendInsuranceTab --------------");
        }
    }
}
