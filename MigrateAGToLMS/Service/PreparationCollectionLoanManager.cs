﻿using LMS.Common.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigrateAGToLMS.Service
{
    public class PreparationCollectionLoanManager
    {
        public long MoveData()
        {
            Console.WriteLine($"-------------- PreparationCollectionLoan -------------");
            var loanHoldMoneyLMSTab = new TableHelper<LMSModel.TblPreparationCollectionLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            // Tab AG
            var loanHoldMoneyAgTab = new TableHelper<AG.TblProvisionsCollection>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            var loanLMSTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);

            long latestID = 0;
            var maxTimaID = loanHoldMoneyLMSTab.SelectColumns(x => x.TimaPreparationCollectionLoanID)
                                                    .OrderByDescending(x => x.TimaPreparationCollectionLoanID)
                                                    .SetGetTop(1)
                                                    .Query().FirstOrDefault()?.TimaPreparationCollectionLoanID ?? 0;

            loanHoldMoneyLMSTab.DeleteWhere(x => x.TimaPreparationCollectionLoanID > 0);
            latestID = maxTimaID;
            List<LMSModel.TblPreparationCollectionLoan> lstInsert = new List<LMSModel.TblPreparationCollectionLoan>();
            int countResult = 0;
            while (true)
            {
                try
                {
                    lstInsert.Clear();
                    var lstDataAG = loanHoldMoneyAgTab.SetGetTop(2000).WhereClause(x => x.Id > latestID).OrderBy(x => x.Id).Query();
                    if (lstDataAG == null || lstDataAG.Count() == 0)
                    {
                        break;
                    }
                    var lstLoanID = lstDataAG.Select(x => x.LoanID).ToList();
                    var lstLoan = loanLMSTab.WhereClause(x => lstLoanID.Contains((int)x.TimaLoanID)).Query();
                    foreach (var item in lstDataAG)
                    {

                        var holdMoney = new LMSModel.TblPreparationCollectionLoan()
                        {
                            CreateBy = item.UserId,
                            CreateDate = item.CreateDate ?? DateTime.Now,
                            ModifyDate = item.CreateDate,
                            Status = (short)item.Status,
                            TimaPreparationCollectionLoanID = item.Id,
                            FullName = item.UserName,
                            Description = item.TextAccruedPeriods,
                            PreparationDate = item.TimeScheduled ?? item.CreateDate.Value,
                            TotalMoney = item.Money,
                            UserID = item.UserId,
                            TypeID = item.NumberAccruedPeriods >= 1 ? 1 : item.NumberAccruedPeriods,
                            PreparationCollectionLoanID = item.Id
                        };
                        latestID = item.Id;
                        try
                        {
                            holdMoney.LoanID = lstLoan.Where(x => x.TimaLoanID == item.LoanID).FirstOrDefault().LoanID;
                        }
                        catch (Exception)
                        {
                            holdMoney.LoanID = 0;

                        }
                        lstInsert.Add(holdMoney);
                    }
                    if (lstInsert.Count > 0)
                    {
                        loanHoldMoneyLMSTab.InsertBulk(lstInsert, setOffIdentity: true);
                    }

                    Console.WriteLine($"Insert PreparationCollectionLoan: {lstInsert.Count} success");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"move PreparationCollectionLoan error|ex={ex.Message}-{ex.StackTrace}");
                    break;
                }
            }
            Console.WriteLine($"Insert PreparationCollectionLoan finished: {countResult} rows");
            return countResult;
        }
    }
}
