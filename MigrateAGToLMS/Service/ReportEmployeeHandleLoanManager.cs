﻿using LMS.Common.DAL;
using LMS.Common.Helper;
using MigrateAGToLMS.AG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MigrateAGToLMS.Service
{
    public class ReportEmployeeHandleLoanManager
    {
        TableHelper<LMSModel.TblReportEmployeeHandleLoan> _employeeHandleLoanTab;
        TableHelper<AG.TblLogPaymentNotify> _logpaymentAGTab;
        TableHelper<AG.TblConfigUserRemindDebtByLoanAndMonth> _configMonthAGTab;
        TableHelper<AG.TblLogLoanByDay> _logloanAGTab;
        TableHelper<AG.TblCommentDebtPromptedAG> _commentAgTab;
        TableHelper<LMSModel.TblLoan> _loanTab;
        Utils utils;


        public ReportEmployeeHandleLoanManager()
        {
            _employeeHandleLoanTab = new TableHelper<LMSModel.TblReportEmployeeHandleLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            _logpaymentAGTab = new TableHelper<AG.TblLogPaymentNotify>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            _configMonthAGTab = new TableHelper<AG.TblConfigUserRemindDebtByLoanAndMonth>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            _logloanAGTab = new TableHelper<AG.TblLogLoanByDay>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            _commentAgTab = new TableHelper<AG.TblCommentDebtPromptedAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            _loanTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            utils = new Utils();
        }

        public void MoveEmployeeHandle()
        {
            Console.WriteLine($"--------------Move MoveEmployeeHandle --------------");
            DateTime dtNow = DateTime.Now;
            DateTime MinDate = new DateTime(2021, 01, 30);
            //DateTime MinDate = new DateTime(2021, 02, 01);
            DateTime MaxDate = new DateTime(2021, 09, 21);
            // Lấy loanID của tran của lms theo từng ngày từ ngày min date -> cuối ngày hôm qua

            DateTime toDate = MinDate;
            List<TblConfigUserRemindDebtByLoanAndMonth> monthsData = null;
            List<int?> lstStafff = new List<int?>();
            while (true)
            {
                List<LMSModel.TblReportEmployeeHandleLoan> lstReport = new List<LMSModel.TblReportEmployeeHandleLoan>();
                // ngày mùng  1 thì lấy số lượng chia đơn trong tháng ở bảng month
                if (MinDate.Day == 1 || monthsData  == null)
                {
                    DateTime dateFirst = new DateTime(MinDate.Year, MinDate.Month, 1);
                    monthsData = _configMonthAGTab.SelectColumns(x => x.LoanId, x => x.ProcessUserID, x => x.SecondStaffID, x => x.SectionManagerUserID,
                        x => x.DepartmentManagerUserID, x => x.NextDate)
                                                      .WhereClause(x => x.ForDate == dateFirst).Query().ToList();
                    var lstProcessUserID = monthsData.Select(x => x.ProcessUserID).Distinct().ToList();
                    var lstSecondStaffID = monthsData.Select(x => x.SecondStaffID).Distinct().ToList();
                    var lstDepartmentManagerUserID = monthsData.Select(x => x.DepartmentManagerUserID).Distinct().ToList();
                    var lstSectionManagerUserID = monthsData.Select(x => x.SectionManagerUserID).Distinct().ToList();


                    lstStafff = lstProcessUserID.Union(lstSecondStaffID).Union(lstDepartmentManagerUserID).Union(lstSectionManagerUserID).Distinct().Where(x => x > 0).ToList();
                }
                //lấy thông tin ở _logpaymentAGTab theo ngày + logloan theo ngày
                var taskLogPayment = Task.Run(() =>
                   {
                       return _logpaymentAGTab.SelectColumns(x => x.ID, x => x.UserIdRemindDebt, x => x.SecondUserIdRemindDebt)
                       .WhereClause(x => x.ForDate == MinDate).Query();
                   });
                var taskComment = Task.Run(() =>
                {
                    DateTime todate = MinDate.AddDays(1);
                    return _commentAgTab.SelectColumns(x => x.Id, x => x.UserId)
                    .WhereClause(x => x.CreateDate >= MinDate && x.CreateDate < todate).Query();
                });

                // lấy loan cuối ngày hôm trước là đầu ngày hôm nay
                var taskLoan = Task.Run(() =>
                {
                    DateTime todate = MinDate.AddDays(-1);
                    return _logloanAGTab.SelectColumns(x => x.LoanID, x => x.NextDate, x => x.ForDate, x => x.UserIdRemindDebt).WhereClause(x => x.ForDate == MinDate).Query();
                });
                Task.WaitAll(taskLogPayment, taskComment, taskLoan);
                var payment = taskLogPayment.Result;
                var comment = taskComment.Result;
                var loans = taskLoan.Result;

                foreach (var item in lstStafff)
                {
                    var lstLoanPayment = payment.Where(x => x.UserIdRemindDebt == item || x.SecondUserIdRemindDebt == item).Select(x => x.ID).ToList();
                    var dicLoanToday = loans.Where(x => x.UserIdRemindDebt == item && x.ForDate == MinDate).ToList().ToDictionary(x => x.LoanID, x => x.NextDate);
                    var monthDataLoan = monthsData.Where(x => x.ProcessUserID == item || x.SecondStaffID == item || x.DepartmentManagerUserID == item || x.SectionManagerUserID == item).ToList();
                    var totalProcessed = new List<int>();
                    foreach (var loansMonth in monthDataLoan)
                    {
                        if (dicLoanToday.ContainsKey(loansMonth.LoanId) && loansMonth.NextDate != dicLoanToday[loansMonth.LoanId] && lstLoanPayment.Contains(loansMonth.LoanId))
                        {
                            totalProcessed.Add(loansMonth.LoanId);
                        }
                    }
                    var lstLoanLMS = _loanTab.WhereClause(x => totalProcessed.Contains((int)x.TimaLoanID)).SelectColumns(x => x.TimaLoanID, x => x.LoanID).Query();
                    var lstLoanIDLMS = lstLoanLMS.Select(x => x.LoanID).ToList();
                    lstReport.Add(new LMSModel.TblReportEmployeeHandleLoan()
                    {
                        CreateDate = dtNow,
                        EmployeeID = item ?? 0,
                        ModifyDate = dtNow,
                        NumberComment = comment.Where(x => x.UserId == item).Count(),
                        ReportDate = MinDate,
                        TotalLoanHandle = lstLoanPayment.Count(),
                        NumberLoanHandleDone = lstLoanIDLMS.Count,
                        LstLoanID = utils.ConvertObjectToJSonV2(lstLoanIDLMS)
                    });
                }
                try
                {
                    _employeeHandleLoanTab.InsertBulk(lstReport);
                    Console.WriteLine($"[Success] tblCutOffLoans : {lstReport.Count} rows : {MinDate.ToString("dd/MM/yyyy")} --------------");
                }
                catch (Exception)
                {

                }
                MinDate = MinDate.AddDays(1);
                if (MinDate >= MaxDate)
                {
                    break;
                }
                Console.WriteLine($"[Success] Move MoveEmployeeHandle DONE --------------");
            }
        }
    }
}
