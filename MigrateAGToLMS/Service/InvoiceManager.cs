﻿using LMS.Common.Constants;
using LMS.Common.DAL;
using MigrateAGToLMS.AG;
using MigrateAGToLMS.LMSModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MigrateAGToLMS.Service
{
    public class InvoiceManager
    {
        public void MoveTransactionBankCardTima(bool miss = false, string fromDate = "", string toDate = "")
        {
            try
            {
                Console.WriteLine($"-------------- TransactionBankCard -------------");
                DateTime dtNow = DateTime.Now;
                var invoceLMSTab = new TableHelper<LMSModel.TblInvoice>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                var invoiceTimaTab = new TableHelper<AG.TblTransactionBankCardAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                var userTab = new TableHelper<AG.TblUserAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                var customerTab = new TableHelper<AG.TblCustomerAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                var shopTab = new TableHelper<AG.TblShopAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
                var loanLMSTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);

                var _common = new LMS.Common.Helper.Utils();
                LMS.Common.Helper.Utils common = new LMS.Common.Helper.Utils();
                long maxTimaInvoiceID = 0;

                int maxRowGet = 2000;
                long maxLMSNow = 0;
                var lstInvoiceAG = new List<TblTransactionBankCardAG>();

                var lstMaxTimaInvoiceID = invoceLMSTab.SelectColumns(x => x.PartnerID)
                                                 .OrderByDescending(x => x.PartnerID)
                                                 .WhereClause(x => x.PartnerID > 0).SetGetTop(1).Query();
                if (lstMaxTimaInvoiceID != null && lstMaxTimaInvoiceID.Any())
                {
                    maxLMSNow = lstMaxTimaInvoiceID.First().PartnerID;
                }
                if (!miss)
                {
                    if (lstMaxTimaInvoiceID != null && lstMaxTimaInvoiceID.Any())
                    {
                        maxTimaInvoiceID = maxLMSNow;
                    }
                }

                if (!string.IsNullOrWhiteSpace(fromDate))
                {
                    DateTime dtFromDate = _common.ConvertStringToDate(fromDate, "dd-MM-yyyy");
                    // lấy bản ghi đầu tiên của ngày đó
                    maxTimaInvoiceID = invoiceTimaTab.SetGetTop(1).OrderBy(x => x.Id).SelectColumns(x => x.Id).WhereClause(x => x.CreateOn >= dtFromDate).Query().FirstOrDefault().Id;
                    //invoceLMSTab.DeleteWhere(x => x.CreateDate >= dtFromDate);
                    maxTimaInvoiceID = maxTimaInvoiceID - 1;
                }
                while (true)
                {
                    if (!miss)
                    {
                        lstInvoiceAG = invoiceTimaTab.WhereClause(x => x.Id > maxTimaInvoiceID && x.TypeTransactionBank != 0 && x.BankCardId > 0)
                                                         .OrderBy(x => x.Id).SetGetTop(maxRowGet).Query().ToList();
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(toDate))
                        {
                            DateTime dtToDate = _common.ConvertStringToDate(toDate, "dd-MM-yyyy");
                            invoiceTimaTab.WhereClause(x => x.CreateOn < dtToDate);
                        }
                        lstInvoiceAG = invoiceTimaTab.WhereClause(x => x.Id > maxTimaInvoiceID && x.TypeTransactionBank != 0 && x.BankCardId > 0)
                                                       .OrderBy(x => x.Id).SetGetTop(maxRowGet).Query().ToList();
                        var lstInvoiceIds = lstInvoiceAG.Select(x => x.Id).ToList();
                        var lstIdLMSHas = invoceLMSTab.SelectColumns(x => x.PartnerID).WhereClause(x => lstInvoiceIds.Contains((int)x.PartnerID)).Query().Select(x => x.PartnerID).ToList();
                        var listMiss = new List<int>();
                        foreach (var item in lstInvoiceIds)
                        {
                            if (!lstIdLMSHas.Contains(item))
                            {
                                listMiss.Add(item);
                            }
                        }
                        lstInvoiceAG = lstInvoiceAG.Where(x => listMiss.Contains(x.Id)).ToList();
                        if (lstInvoiceAG.Count == 0) maxTimaInvoiceID = lstInvoiceIds.Max();
                    }
                    // chưa insert 
                    var lstInvoices = lstInvoiceAG.OrderBy(x => x.Id).ToList();
                    if (lstInvoices.Count > 0)
                    {
                        // taskuser
                        var taskUser = Task.Run(() =>
                         {
                             var lstUserIds = lstInvoices.Where(x => x.UserIdAction > 0).Select(x => x.UserIdAction).ToList();
                             return userTab.WhereClause(x => lstUserIds.Contains(x.Id)).SelectColumns(x => x.Id, x => x.Username).Query().ToDictionary(x => x.Id, x => x.Username);
                         });

                        var taskCustomer = Task.Run(() =>
                        {
                            var lstCustomer = lstInvoices.Where(x => x.CustomerId > 0).Select(x => x.CustomerId).ToList();
                            return customerTab.WhereClause(x => lstCustomer.Contains(x.CustomerID)).SelectColumns(x => x.CustomerID, x => x.Name).Query().ToDictionary(x => x.CustomerID, x => x.Name);
                        });
                        var taskPayShop = Task.Run(() =>
                        {
                            var lstshop = lstInvoices.Where(x => x.PayShopId > 0).Select(x => x.PayShopId).ToList();
                            return shopTab.WhereClause(x => lstshop.Contains(x.ShopID)).SelectColumns(x => x.ShopID, x => x.Name).Query().ToDictionary(x => x.ShopID, x => x.Name);
                        });
                        var taskLoanLMS = Task.Run(() =>
                        {
                            var lstLoanTimaIds = lstInvoices.Where(x => x.LoanId > 0).Select(x => x.LoanId).ToList();
                            return loanLMSTab.WhereClause(x => lstLoanTimaIds.Contains((int)x.TimaLoanID)).Query().ToDictionary(x => x.TimaLoanID, x => x);
                        });
                        Task.WaitAll(taskUser, taskCustomer, taskPayShop);
                        var dicUser = taskUser.Result;
                        var dicCus = taskCustomer.Result;
                        var dicPayShop = taskPayShop.Result;
                        var dicLoan = taskLoanLMS.Result;
                        //lstLenderNeedInsert = lstLenderNeedInsert.Take(maxItemGet).ToList();
                        List<LMSModel.TblInvoice> lstInvoice = new List<LMSModel.TblInvoice>();
                        foreach (var item in lstInvoices)
                        {
                            var invoce = new LMSModel.TblInvoice()
                            {
                                BankCardID = item.BankCardId,
                                PartnerID = item.Id,
                                CreateDate = item.CreateOn,
                                CustomerID = dicLoan.ContainsKey(item.LoanId) ? dicLoan[item.LoanId].CustomerID ?? 0 : item.CustomerId,
                                //InvoiceID = item.Id,
                                LoanID = dicLoan.ContainsKey(item.LoanId) ? dicLoan[item.LoanId].LoanID : item.LoanId,
                                // SourceID = item.OwnedShopId,
                                // DestinationID = item.BankCardIdReceive,
                                Note = item.Note,
                                Status = ConvertStatus(item.Status),
                                TotalMoney = item.TotalMoney,
                                TransactionDate = item.DateTimeTransaction ?? item.CreateOn,
                                TransactionMoney = item.TotalMoney,
                                UserIDCreate = item.UserIdAction,
                                UserIDModify = item.UserIdModify,
                                JobStatus = 0,
                                InvoiceCode = "",
                                MoneyFee = 0,
                                MoneyVAT = 0,
                                ShopID = item.CollectorsShopId,
                                LenderID = item.PayShopId,
                                ModifyDate = item.ModifyOn,
                                Payee = 0,
                            };
                            ConvertDetailInfoAGToLMS(invoce, item, common, dicUser, dicCus, dicPayShop, dicLoan);

                            var lstTypeLender = new List<int>()
                            {
                                (int)AG_TransactionBankCard_Type.AgentTransferFund,
                                (int)AG_TransactionBankCard_Type.PaidLenderByPeriod,
                               (int) AG_TransactionBankCard_Type.LenderPaidInsurance,
                               (int) AG_TransactionBankCard_Type.LenderDepositPayment,
                            };

                            if (lstTypeLender.Contains(item.TypeTransactionBank))
                            {
                                invoce.InvoiceCode = _common.HashMD5($"{invoce.LenderID}-{invoce.Note}-{invoce.InvoiceSubType}-{invoce.BankCardID}-{Math.Abs(invoce.TotalMoney)}-{invoce.TransactionDate.Value.Ticks}");
                            }
                            else
                            {
                                invoce.InvoiceCode = _common.HashMD5($"{invoce.Note}-{invoce.InvoiceSubType}-{invoce.BankCardID}-{Math.Abs(invoce.TotalMoney)}-{invoce.TransactionDate.Value.Ticks}");
                            }
                            if (item.TypeTransactionBank == (int)AG_TransactionBankCard_Type.TransferBankCardInternal)
                            {
                                LMSModel.TblInvoice invoiceType = new TblInvoice()
                                {
                                    BankCardID = item.BankCardIdReceive,
                                    PartnerID = item.Id,
                                    CreateDate = item.CreateOn,
                                    CustomerID = item.CustomerId,
                                    //InvoiceID = item.Id,
                                    LoanID = item.LoanId,
                                    // SourceID = item.OwnedShopId,
                                    // DestinationID = item.BankCardIdReceive,
                                    Note = item.Note,
                                    Status = ConvertStatus(item.Status),
                                    TotalMoney = -item.TotalMoney,
                                    TransactionDate = item.DateTimeTransaction ?? item.CreateOn,
                                    TransactionMoney = -item.TotalMoney,
                                    UserIDCreate = item.UserIdAction,
                                    UserIDModify = item.UserIdModify,
                                    JobStatus = 0,
                                    InvoiceCode = "",
                                    MoneyFee = 0,
                                    MoneyVAT = 0,
                                    ShopID = item.CollectorsShopId,
                                    LenderID = item.PayShopId,
                                    ModifyDate = item.ModifyOn,
                                    Payee = 0,
                                };
                                invoiceType.InvoiceSubType = (int)GroupInvoiceType.ReceiptInternal;
                                invoiceType.InvoiceType = (int)InvoiceType.Receipt;
                                invoiceType.SourceID = item.BankCardId;
                                invoiceType.DestinationID = item.BankCardIdReceive;
                                invoiceType.InvoiceCode = _common.HashMD5($"{invoiceType.Note}-{invoiceType.InvoiceSubType}-{invoiceType.BankCardID}-{Math.Abs(invoce.TotalMoney)}-{invoiceType.TransactionDate.Value.Ticks}");
                                LMSModel.InvoiceJsonExtra jsonExtra = new LMSModel.InvoiceJsonExtra();
                                long shopCollectID = item.CollectorsShopId;
                                string shopCollectName = item.CollectorsShopName;
                                long shopPayID = item.PayShopId ?? 0;
                                string shopPayName = shopPayID > 0 && dicUser.ContainsKey((int)shopPayID) ? dicUser[(int)shopPayID] : item.PayShopName;
                                jsonExtra.ShopName = shopCollectName;
                                jsonExtra.CreateBy = dicUser.ContainsKey(item.Id) ? dicUser[item.Id] : item.UserNameAction;
                                jsonExtra.SourceName = item.BankCardAliasName;
                                jsonExtra.DestinationName = item.BankCardAliasNameReceive;
                                invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                                lstInvoice.Add(invoiceType);
                            }
                            maxTimaInvoiceID = item.Id;
                            lstInvoice.Add(invoce);
                        }
                        Console.WriteLine($"[Inserting] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} bat dau insert TransactionBankCard");
                        invoceLMSTab.InsertBulk(lstInvoice);
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert TransactionBankCard  -  {lstInvoice.Count()} ");
                    }
                    else if (maxLMSNow > maxTimaInvoiceID)
                    {
                        Console.WriteLine($"[Getting] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} dang quet miss id hien tai " + maxTimaInvoiceID);
                        continue;
                    }
                    else
                    {
                        Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert TransactionBankCard hoan tat");
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"MoveBankCardTima - ERROR {ex.Message}");
            }
        }

        public void ConvertDetailInfoAGToLMS(LMSModel.TblInvoice invoiceType, TblTransactionBankCardAG invoiceAG,
        LMS.Common.Helper.Utils common, Dictionary<int, string> dicUser, Dictionary<int, string> dicCus, Dictionary<int, string> dicPayShop, Dictionary<long?, TblLoan> dicLoan
        )
        {

            long customerID = dicLoan.ContainsKey(invoiceAG.LoanId) ? dicLoan[invoiceAG.LoanId].CustomerID ?? 0 : invoiceAG.CustomerId;
            string customerName = dicLoan.ContainsKey(invoiceAG.LoanId) ? dicLoan[invoiceAG.LoanId].CustomerName : (dicCus.ContainsKey(invoiceAG.CustomerId) ? dicCus[invoiceAG.CustomerId] : invoiceAG.CustomerName);
            long bankCardID = invoiceAG.BankCardId;
            string bankCardName = invoiceAG.BankCardAliasName;
            long shopCollectID = invoiceAG.CollectorsShopId;
            string shopCollectName = invoiceAG.CollectorsShopName;
            long shopPayID = invoiceAG.PayShopId ?? 0;
            string shopPayName = shopPayID > 0 && dicUser.ContainsKey((int)shopPayID) ? dicUser[(int)shopPayID] : invoiceAG.PayShopName;
            long bankCardReceiveID = invoiceAG.BankCardIdReceive;
            string bankCardReceiveName = invoiceAG.BankCardAliasNameReceive;
            LMSModel.InvoiceJsonExtra jsonExtra = new LMSModel.InvoiceJsonExtra();
            jsonExtra.ShopName = shopCollectName;
            jsonExtra.CreateBy = dicUser.ContainsKey(invoiceAG.Id) ? dicUser[invoiceAG.Id] : invoiceAG.UserNameAction;
            switch ((AG_TransactionBankCard_Type)invoiceAG.TypeTransactionBank)
            {
                case AG_TransactionBankCard_Type.ChargingMoney: // thu tiền khách hàng
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.ReceiptCustomer;
                        invoiceType.InvoiceType = (int)InvoiceType.Receipt;
                        invoiceType.SourceID = customerID;
                        jsonExtra.SourceName = customerName;
                        invoiceType.DestinationID = bankCardID;
                        jsonExtra.DestinationName = bankCardName;
                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }
                case AG_TransactionBankCard_Type.Collection:
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.ReceiptOnBehalfShop;
                        invoiceType.InvoiceType = (int)InvoiceType.Receipt;
                        invoiceType.SourceID = customerID;
                        invoiceType.DestinationID = shopPayID;
                        jsonExtra.SourceName = customerName;
                        jsonExtra.DestinationName = shopPayName;
                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }
                case AG_TransactionBankCard_Type.Suspend:
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.ReceiptCustomerConsider;
                        invoiceType.InvoiceType = 0;
                        invoiceType.SourceID = customerID;
                        jsonExtra.SourceName = customerName;
                        invoiceType.DestinationID = shopCollectID;
                        jsonExtra.DestinationName = shopCollectName;
                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }
                case AG_TransactionBankCard_Type.Disbursement:
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.Disbursement;
                        invoiceType.InvoiceType = (int)InvoiceType.PaySlip;
                        invoiceType.SourceID = bankCardID;
                        invoiceType.DestinationID = customerID;
                        jsonExtra.SourceName = bankCardName;
                        jsonExtra.DestinationName = customerName;
                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }
                case AG_TransactionBankCard_Type.CancelCollection:
                    {
                        break;
                    }
                case AG_TransactionBankCard_Type.TransfersMoney: // chuyển tien
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.PaySlipOther;
                        invoiceType.InvoiceType = (int)InvoiceType.PaySlip;
                        invoiceType.SourceID = bankCardID;
                        jsonExtra.SourceName = bankCardName;
                        invoiceType.DestinationID = shopCollectID;
                        jsonExtra.DestinationName = shopCollectName;
                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }
                case AG_TransactionBankCard_Type.ReceiveMoney: // thu noi bo
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.ReceiptInternal;
                        invoiceType.InvoiceType = (int)InvoiceType.Receipt;
                        invoiceType.SourceID = shopCollectID;
                        jsonExtra.SourceName = shopCollectName;
                        invoiceType.DestinationID = bankCardID;
                        jsonExtra.DestinationName = bankCardName;
                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }
                case AG_TransactionBankCard_Type.PayMoney:
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.PaySlipOther;
                        invoiceType.InvoiceType = (int)InvoiceType.PaySlip;
                        invoiceType.SourceID = bankCardID;
                        jsonExtra.SourceName = bankCardName;
                        invoiceType.DestinationID = shopCollectID;
                        jsonExtra.DestinationName = shopCollectName;
                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }
                case AG_TransactionBankCard_Type.CollectMoney:
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.ReceiptOther;
                        invoiceType.InvoiceType = (int)InvoiceType.Receipt;
                        invoiceType.SourceID = shopCollectID;
                        jsonExtra.SourceName = shopCollectName;
                        invoiceType.DestinationID = bankCardID;
                        jsonExtra.DestinationName = bankCardName;

                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }
                case AG_TransactionBankCard_Type.GiveMoneyBack: // tra tien thua cho khach
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.PaySlipGiveBackExcessCustomer;
                        invoiceType.InvoiceType = (int)InvoiceType.PaySlip;
                        invoiceType.SourceID = bankCardID;
                        jsonExtra.SourceName = bankCardName;
                        invoiceType.DestinationID = customerID;
                        jsonExtra.DestinationName = customerName;
                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }

                case AG_TransactionBankCard_Type.MoneyAdvice: // tra tien thua cho khach
                    {
                        // k co du lieu
                        break;
                    }
                case AG_TransactionBankCard_Type.AdditionalLoan: // vay them goc
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.PaySlipDisbursement;
                        invoiceType.InvoiceType = (int)InvoiceType.PaySlip;
                        invoiceType.SourceID = bankCardID;
                        jsonExtra.SourceName = bankCardName;
                        invoiceType.DestinationID = customerID;
                        jsonExtra.DestinationName = customerName;
                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }
                case AG_TransactionBankCard_Type.CashWithdrawalAgent:
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.PaySlipLenderWithdraw;
                        invoiceType.InvoiceType = (int)InvoiceType.PaySlip;

                        invoiceType.SourceID = bankCardID;
                        jsonExtra.SourceName = bankCardName;
                        invoiceType.DestinationID = shopPayID;
                        jsonExtra.DestinationName = shopPayName;
                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }
                case AG_TransactionBankCard_Type.TransferBankCardInternal: // chuyen tien noi bo
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.PaySlipTransferInternal;
                        invoiceType.InvoiceType = (int)InvoiceType.PaySlip;
                        invoiceType.SourceID = bankCardID;
                        invoiceType.DestinationID = bankCardReceiveID;
                        jsonExtra.SourceName = bankCardName;
                        jsonExtra.DestinationName = bankCardReceiveName;
                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }
                case AG_TransactionBankCard_Type.FeeTransferMoney:
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.PaySlipFeeTransferMoney;
                        invoiceType.InvoiceType = (int)InvoiceType.PaySlip;
                        invoiceType.SourceID = bankCardID;
                        jsonExtra.SourceName = bankCardName;
                        invoiceType.DestinationID = shopCollectID;
                        jsonExtra.DestinationName = shopCollectName;

                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }
                case AG_TransactionBankCard_Type.BankFee:
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.PaySlipBankFee;
                        invoiceType.InvoiceType = (int)InvoiceType.PaySlip;
                        invoiceType.SourceID = bankCardID;
                        jsonExtra.SourceName = bankCardName;
                        invoiceType.DestinationID = shopCollectID;
                        jsonExtra.DestinationName = shopCollectName;
                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }
                case AG_TransactionBankCard_Type.BankInterest:
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.ReceiptInterestBank;
                        invoiceType.InvoiceType = (int)InvoiceType.Receipt;
                        invoiceType.SourceID = shopCollectID;
                        jsonExtra.SourceName = shopCollectName;
                        invoiceType.DestinationID = bankCardID;
                        jsonExtra.DestinationName = bankCardName;
                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }
                case AG_TransactionBankCard_Type.PayTheDebt:
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.PaySlipTheDebt;
                        invoiceType.InvoiceType = (int)InvoiceType.PaySlip;
                        invoiceType.SourceID = bankCardID;
                        jsonExtra.SourceName = bankCardName;
                        invoiceType.DestinationID = shopCollectID;
                        jsonExtra.DestinationName = shopCollectName;
                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }
                case AG_TransactionBankCard_Type.AgentTransferFund:
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.ReceiptLenderCapital;
                        invoiceType.InvoiceType = (int)InvoiceType.Receipt;
                        invoiceType.SourceID = shopPayID;
                        jsonExtra.SourceName = shopPayName;
                        invoiceType.DestinationID = bankCardID;
                        jsonExtra.DestinationName = bankCardName;

                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }
                case AG_TransactionBankCard_Type.PaidLenderByPeriod:
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.PaySlipLenderWithdraw;
                        invoiceType.InvoiceType = (int)InvoiceType.PaySlip;

                        invoiceType.SourceID = bankCardID;
                        jsonExtra.SourceName = bankCardName;
                        invoiceType.DestinationID = shopPayID;
                        jsonExtra.DestinationName = shopPayName;
                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }
                case AG_TransactionBankCard_Type.LenderPaidInsurance:
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.ReceiptLenderInsurance;
                        invoiceType.InvoiceType = (int)InvoiceType.Receipt;

                        invoiceType.SourceID = shopPayID;
                        jsonExtra.SourceName = shopPayName;
                        invoiceType.DestinationID = bankCardID;
                        jsonExtra.DestinationName = bankCardName;
                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }
                case AG_TransactionBankCard_Type.LenderDepositPayment:
                    {
                        invoiceType.InvoiceSubType = (int)GroupInvoiceType.LenderDepositPayment;
                        invoiceType.InvoiceType = (int)InvoiceType.PaySlip;
                        invoiceType.SourceID = bankCardID;
                        jsonExtra.SourceName = bankCardName;
                        invoiceType.DestinationID = shopPayID;
                        jsonExtra.DestinationName = shopPayName;
                        invoiceType.JsonExtra = common.ConvertObjectToJSonV2(jsonExtra);
                        break;
                    }
            }
        }

        public short ConvertStatus(int statusAG)
        {
            switch ((Status_TransactionBankCard)statusAG)
            {
                case Status_TransactionBankCard.NotComplete:
                    return (int)Invoice_Status.WaitingConfirm;
                case Status_TransactionBankCard.Complete:
                    return (int)Invoice_Status.Confirmed;
                case Status_TransactionBankCard.Deleted:
                    return (int)Invoice_Status.Deleted;
                default:
                    return (int)Invoice_Status.WaitingConfirm;
            }
        }

        public void CompareInvoice()
        {

            Console.WriteLine($"--------------  COMPARE INVOICE --------------");
            DateTime dtNow = DateTime.Now;
            var invoiceTab = new TableHelper<LMSModel.TblInvoice>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var transactionBankTab = new TableHelper<AG.TblTransactionBankCardAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            var logCompareTab = new TableHelper<LMSModel.TblLogCompare>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            LenderManager lenderManager = new LenderManager();

            List<LMSModel.TblLogCompare> lstLog = new List<LMSModel.TblLogCompare>();
            try
            {
                DateTime createDate = dtNow.Date;
                var invoiceLMSTask = Task.Run(() =>
                {
                    int typeGN = (int)GroupInvoiceType.Disbursement;
                    return invoiceTab.SelectColumns(x => x.InvoiceID, x => x.TotalMoney, x => x.InvoiceType, x => x.InvoiceSubType)
                    .WhereClause(x => x.CreateDate >= createDate && ((x.InvoiceSubType != typeGN && x.PartnerID > 0) || x.InvoiceSubType == typeGN) && x.TotalMoney != 0 && x.BankCardID > 0).Query();
                });

                var transactionBankAGTask = Task.Run(() =>
                {
                    return transactionBankTab.SelectColumns(x => x.Id, x => x.TotalMoney, x => x.TypeTransactionBank)
                    .WhereClause(x => x.CreateOn >= createDate && x.TotalMoney != 0 && x.BankCardId > 0).Query();
                });
                Task.WaitAll(invoiceLMSTask, transactionBankAGTask);
                var lstInvoice = invoiceLMSTask.Result;
                var lstTransactionAG = transactionBankAGTask.Result;
                InvoiceCompare invoiceAG = new InvoiceCompare();
                InvoiceCompare invoiceLMS = new InvoiceCompare();
                foreach (var item in lstTransactionAG)
                {
                    TotalAGCompare(invoiceAG, item);
                }

                foreach (var item in lstInvoice)
                {
                    TotalLMSCompare(invoiceLMS, item);
                }
                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.CollectExpense_TotalCount,
                                                        lstTransactionAG.Count() + lstTransactionAG.Where(x => x.TypeTransactionBank == (int)AG_TransactionBankCard_Type.TransferBankCardInternal).Count(), lstInvoice.Count()));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.CollectExpense_TotalMoney,
                                                    lstTransactionAG.Sum(x => x.TotalMoney) + lstTransactionAG.Where(x => x.TypeTransactionBank == (int)AG_TransactionBankCard_Type.TransferBankCardInternal).Sum(x => (-1) * x.TotalMoney), lstInvoice.Sum(x => x.TotalMoney)));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.Disbursement,
                                                 invoiceAG.Disbursement, invoiceLMS.Disbursement));


                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.ReceiptCustomer,
                                         invoiceAG.ReceiptCustomer, invoiceLMS.ReceiptCustomer));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.ReceiptCustomerConsider,
                                        invoiceAG.ReceiptCustomerConsider + invoiceAG.Waiting, invoiceLMS.ReceiptCustomerConsider + invoiceLMS.Waiting));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.ReceiptOnBehalfShop,
                                      invoiceAG.ReceiptOnBehalfShop, invoiceLMS.ReceiptOnBehalfShop));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.ReceiptInternal,
                                     invoiceAG.ReceiptInternal, invoiceLMS.ReceiptInternal));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.ReceiptLenderCapital,
                                   invoiceAG.ReceiptLenderCapital, invoiceLMS.ReceiptLenderCapital));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.ReceiptInterestBank,
                                 invoiceAG.ReceiptInterestBank, invoiceLMS.ReceiptInterestBank));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.ReceiptLenderInsurance,
                               invoiceAG.ReceiptLenderInsurance, invoiceLMS.ReceiptLenderInsurance));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.ReceiptPartner,
                               invoiceAG.ReceiptPartner, invoiceLMS.ReceiptPartner));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.ReceiptLenderTopUp,
                               invoiceAG.ReceiptLenderTopUp, invoiceLMS.ReceiptLenderTopUp));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.ReceiptOther,
                           invoiceAG.ReceiptOther, invoiceLMS.ReceiptOther));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.PaySlipTransferInternal,
                           invoiceAG.PaySlipTransferInternal, invoiceLMS.PaySlipTransferInternal));


                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.PaySlipGiveBackExcessCustomer,
                           invoiceAG.PaySlipGiveBackExcessCustomer, invoiceLMS.PaySlipGiveBackExcessCustomer));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.PaySlipLenderWithdraw,
                       invoiceAG.PaySlipLenderWithdraw, invoiceLMS.PaySlipLenderWithdraw));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.PaySlipAdvancePaymentForLender,
                        invoiceAG.PaySlipAdvancePaymentForLender, invoiceLMS.PaySlipAdvancePaymentForLender));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.PaySlipFeeForBank,
                            invoiceAG.PaySlipFeeForBank, invoiceLMS.PaySlipFeeForBank));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.PaySlipOther,
                           invoiceAG.PaySlipOther, invoiceLMS.PaySlipOther));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.PaySlipDisbursement,
                          invoiceAG.PaySlipDisbursement, invoiceLMS.PaySlipDisbursement));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.PaySlipFeeTransferMoney,
                           invoiceAG.PaySlipFeeTransferMoney, invoiceLMS.PaySlipFeeTransferMoney));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.PaySlipBankFee,
                          invoiceAG.PaySlipBankFee, invoiceLMS.PaySlipBankFee));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.PaySlipTheDebt,
                      invoiceAG.PaySlipTheDebt, invoiceLMS.PaySlipTheDebt));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.TransferMoneyStoreReasonOther,
                invoiceAG.TransferMoneyStoreReasonOther, invoiceLMS.TransferMoneyStoreReasonOther));


                //tiền
                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyDisbursement,
                                                 invoiceAG.MoneyDisbursement, invoiceLMS.MoneyDisbursement));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyReceiptCustomer,
                                         invoiceAG.MoneyReceiptCustomer, invoiceLMS.MoneyReceiptCustomer));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyReceiptCustomerConsider,
                                        invoiceAG.MoneyReceiptCustomerConsider + invoiceAG.Waiting, invoiceLMS.MoneyReceiptCustomerConsider + invoiceAG.Waiting));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyReceiptOnBehalfShop,
                                      invoiceAG.MoneyReceiptOnBehalfShop, invoiceLMS.MoneyReceiptOnBehalfShop));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyReceiptInternal,
                                     invoiceAG.MoneyReceiptInternal, invoiceLMS.MoneyReceiptInternal));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyReceiptLenderCapital,
                                   invoiceAG.MoneyReceiptLenderCapital, invoiceLMS.MoneyReceiptLenderCapital));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyReceiptInterestBank,
                                 invoiceAG.MoneyReceiptInterestBank, invoiceLMS.MoneyReceiptInterestBank));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyReceiptLenderInsurance,
                               invoiceAG.MoneyReceiptLenderInsurance, invoiceLMS.MoneyReceiptLenderInsurance));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyReceiptPartner,
                               invoiceAG.MoneyReceiptPartner, invoiceLMS.MoneyReceiptPartner));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyReceiptLenderTopUp,
                               invoiceAG.MoneyReceiptLenderTopUp, invoiceLMS.MoneyReceiptLenderTopUp));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyReceiptOther,
                           invoiceAG.MoneyReceiptOther, invoiceLMS.MoneyReceiptOther));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyPaySlipTransferInternal,
                           invoiceAG.MoneyPaySlipTransferInternal, invoiceLMS.MoneyPaySlipTransferInternal));


                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyPaySlipGiveBackExcessCustomer,
                           invoiceAG.MoneyPaySlipGiveBackExcessCustomer, invoiceLMS.MoneyPaySlipGiveBackExcessCustomer));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyPaySlipLenderWithdraw,
                       invoiceAG.MoneyPaySlipLenderWithdraw, invoiceLMS.MoneyPaySlipLenderWithdraw));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyPaySlipAdvancePaymentForLender,
                        invoiceAG.MoneyPaySlipAdvancePaymentForLender, invoiceLMS.MoneyPaySlipAdvancePaymentForLender));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyPaySlipFeeForBank,
                            invoiceAG.MoneyPaySlipFeeForBank, invoiceLMS.MoneyPaySlipFeeForBank));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyPaySlipOther,
                           invoiceAG.MoneyPaySlipOther, invoiceLMS.MoneyPaySlipOther));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyPaySlipDisbursement,
                          invoiceAG.MoneyPaySlipDisbursement, invoiceLMS.MoneyPaySlipDisbursement));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyPaySlipFeeTransferMoney,
                           invoiceAG.MoneyPaySlipFeeTransferMoney, invoiceLMS.MoneyPaySlipFeeTransferMoney));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyPaySlipBankFee,
                          invoiceAG.MoneyPaySlipBankFee, invoiceLMS.MoneyPaySlipBankFee));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyPaySlipTheDebt,
                      invoiceAG.MoneyPaySlipTheDebt, invoiceLMS.MoneyPaySlipTheDebt));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.MoneyTransferMoneyStoreReasonOther,
                invoiceAG.MoneyTransferMoneyStoreReasonOther, invoiceLMS.MoneyTransferMoneyStoreReasonOther));

                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.DepositCount,
             invoiceAG.DepositCount, invoiceLMS.DepositCount));
                lstLog.Add(lenderManager.SetlogCompare((int)LogCompare_MainType.CollectExpense, (int)LogCompare_DetailType.DepositMoney,
        invoiceAG.MoneyDeposit, invoiceLMS.MoneyDeposit));

                List<int> mainType = new List<int>() { (int)LogCompare_MainType.CollectExpense };

                var lstCurrentLog = logCompareTab.WhereClause(x => x.Status == 1 && x.ForDate == DateTime.Now.Date && mainType.Contains(x.MainType)).Query();
                logCompareTab.DeleteList(lstCurrentLog.ToList());
                logCompareTab.InsertBulk(lstLog);
                Console.WriteLine($"[success] {DateTime.Now.ToString("dd/MM/yyyy HH:mm")} insert invoice Loan hoan tat");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"CompareInvoice - ERROR {ex.Message}");
            }

        }

        public void TotalAGCompare(InvoiceCompare invoiceCompare, AG.TblTransactionBankCardAG invoice)
        {
            switch ((AG_TransactionBankCard_Type)invoice.TypeTransactionBank)
            {

                case AG_TransactionBankCard_Type.ChargingMoney: // thu tiền khách hàng
                    {
                        invoiceCompare.ReceiptCustomer += 1;
                        invoiceCompare.MoneyReceiptCustomer += invoice.TotalMoney;
                        break;
                    }
                case AG_TransactionBankCard_Type.Collection:
                    {
                        invoiceCompare.ReceiptOnBehalfShop += 1;
                        invoiceCompare.MoneyReceiptOnBehalfShop += invoice.TotalMoney;
                        break;
                    }
                case AG_TransactionBankCard_Type.Suspend:
                    {
                        invoiceCompare.ReceiptCustomerConsider += 1;
                        invoiceCompare.MoneyReceiptCustomerConsider += invoice.TotalMoney;
                        break;
                    }
                case AG_TransactionBankCard_Type.Disbursement:
                    {
                        invoiceCompare.Disbursement += 1;
                        invoiceCompare.MoneyDisbursement += invoice.TotalMoney;
                        break;
                    }
                case AG_TransactionBankCard_Type.CancelCollection:
                    {
                        break;
                    }
                case AG_TransactionBankCard_Type.TransfersMoney: // chuyển tien
                    {
                        invoiceCompare.PaySlipOther += 1;
                        invoiceCompare.MoneyPaySlipOther += invoice.TotalMoney;
                        break;
                    }
                case AG_TransactionBankCard_Type.ReceiveMoney: // thu noi bo
                    {
                        //invoiceCompare.ReceiptInternal += 1;
                        //invoiceCompare.MoneyReceiptInternal += invoice.TotalMoney;
                        //break;
                        break;
                    }
                case AG_TransactionBankCard_Type.PayMoney:
                    {
                        invoiceCompare.PaySlipOther += 1;
                        invoiceCompare.MoneyPaySlipOther += invoice.TotalMoney;
                        break;
                    }
                case AG_TransactionBankCard_Type.CollectMoney: // chi tieen
                    {
                        invoiceCompare.ReceiptOther += 1;
                        invoiceCompare.MoneyReceiptOther += invoice.TotalMoney;
                        break;
                    }
                case AG_TransactionBankCard_Type.GiveMoneyBack: // tra tien thua cho khach
                    {
                        invoiceCompare.PaySlipGiveBackExcessCustomer += 1;
                        invoiceCompare.MoneyPaySlipGiveBackExcessCustomer += invoice.TotalMoney;
                        break;
                    }

                case AG_TransactionBankCard_Type.MoneyAdvice: // tra tien thua cho khach
                    {
                        // k co du lieu
                        break;
                    }
                case AG_TransactionBankCard_Type.AdditionalLoan: // vay them goc
                    {
                        invoiceCompare.PaySlipDisbursement += 1;
                        invoiceCompare.MoneyPaySlipDisbursement += invoice.TotalMoney;
                        break;
                    }
                case AG_TransactionBankCard_Type.CashWithdrawalAgent:
                    {
                        invoiceCompare.MoneyPaySlipLenderWithdraw += invoice.TotalMoney;
                        invoiceCompare.PaySlipLenderWithdraw += 1;
                        break;
                    }
                case AG_TransactionBankCard_Type.TransferBankCardInternal: // chuyen tien noi bo
                    {
                        invoiceCompare.PaySlipTransferInternal += 1;
                        invoiceCompare.MoneyPaySlipTransferInternal += invoice.TotalMoney;
                        invoiceCompare.ReceiptInternal += 1;
                        invoiceCompare.MoneyReceiptInternal += (-1) * invoice.TotalMoney;
                        //break;
                        break;
                    }
                case AG_TransactionBankCard_Type.FeeTransferMoney:
                    {
                        invoiceCompare.PaySlipFeeTransferMoney += 1;
                        invoiceCompare.MoneyPaySlipFeeTransferMoney += invoice.TotalMoney;
                        break;
                    }
                case AG_TransactionBankCard_Type.BankFee:
                    {
                        invoiceCompare.PaySlipBankFee += 1;
                        invoiceCompare.MoneyPaySlipBankFee += invoice.TotalMoney;
                        break;
                    }
                case AG_TransactionBankCard_Type.BankInterest:
                    {
                        invoiceCompare.ReceiptInterestBank += 1;
                        invoiceCompare.MoneyReceiptInterestBank += invoice.TotalMoney;
                        break;
                    }
                case AG_TransactionBankCard_Type.PayTheDebt:
                    {
                        invoiceCompare.PaySlipTheDebt += 1;
                        invoiceCompare.MoneyPaySlipTheDebt += invoice.TotalMoney;
                        break;
                    }
                case AG_TransactionBankCard_Type.AgentTransferFund:
                    {
                        invoiceCompare.ReceiptLenderCapital += 1;
                        invoiceCompare.MoneyReceiptLenderCapital += invoice.TotalMoney;
                        break;
                    }
                case AG_TransactionBankCard_Type.PaidLenderByPeriod:
                    {
                        invoiceCompare.PaySlipLenderWithdraw += 1;
                        invoiceCompare.MoneyPaySlipLenderWithdraw += invoice.TotalMoney;
                        break;
                    }
                case AG_TransactionBankCard_Type.LenderPaidInsurance:
                    {
                        invoiceCompare.ReceiptLenderInsurance += 1;
                        invoiceCompare.MoneyReceiptLenderInsurance += invoice.TotalMoney;
                        break;
                    }
                case AG_TransactionBankCard_Type.LenderDepositPayment:
                    {
                        invoiceCompare.DepositCount += 1;
                        invoiceCompare.MoneyDeposit += invoice.TotalMoney;
                        break;
                    }
            }
        }
        public void TotalLMSCompare(InvoiceCompare invoiceCompare, TblInvoice invoice)
        {
            switch ((GroupInvoiceType)invoice.InvoiceSubType)
            {
                case GroupInvoiceType.Waiting:

                    invoiceCompare.Waiting += 1;
                    invoiceCompare.MoneyWaiting += invoice.TotalMoney;
                    break;
                //case GroupInvoiceType.Receipt:
                //    invoiceCompare.Receipt += 1;
                //    break;
                //case GroupInvoiceType.PaySlip:
                //    invoiceCompare.PaySlip += 1;
                //    break;
                case GroupInvoiceType.ReceiptCustomer:

                    invoiceCompare.ReceiptCustomer += 1;
                    invoiceCompare.MoneyReceiptCustomer += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.ReceiptCustomerConsider:
                    invoiceCompare.ReceiptCustomerConsider += 1;
                    invoiceCompare.MoneyReceiptCustomerConsider += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.ReceiptOnBehalfShop:
                    invoiceCompare.ReceiptOnBehalfShop += 1;
                    invoiceCompare.MoneyReceiptOnBehalfShop += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.ReceiptInternal:
                    invoiceCompare.ReceiptInternal += 1;
                    invoiceCompare.MoneyReceiptInternal += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.ReceiptLenderCapital:
                    invoiceCompare.ReceiptLenderCapital += 1;
                    invoiceCompare.MoneyReceiptLenderCapital += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.ReceiptInterestBank:
                    invoiceCompare.ReceiptInterestBank += 1;
                    invoiceCompare.MoneyReceiptInterestBank += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.ReceiptLenderInsurance:
                    invoiceCompare.ReceiptLenderInsurance += 1;
                    invoiceCompare.MoneyReceiptLenderInsurance += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.ReceiptPartner:
                    invoiceCompare.ReceiptPartner += 1;
                    invoiceCompare.MoneyReceiptPartner += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.ReceiptLenderTopUp:
                    invoiceCompare.ReceiptLenderTopUp += 1;
                    invoiceCompare.MoneyReceiptLenderTopUp += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.ReceiptOther:
                    invoiceCompare.ReceiptOther += 1;
                    invoiceCompare.MoneyReceiptOther += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.Disbursement:
                    invoiceCompare.Disbursement += 1;
                    invoiceCompare.MoneyDisbursement += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.PaySlipTransferInternal:
                    invoiceCompare.PaySlipTransferInternal += 1;
                    invoiceCompare.MoneyPaySlipTransferInternal += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.PaySlipGiveBackExcessCustomer:
                    invoiceCompare.PaySlipGiveBackExcessCustomer += 1;
                    invoiceCompare.MoneyPaySlipGiveBackExcessCustomer += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.PaySlipLenderWithdraw:
                    invoiceCompare.PaySlipLenderWithdraw += 1;
                    invoiceCompare.MoneyPaySlipLenderWithdraw += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.PaySlipAdvancePaymentForLender:
                    invoiceCompare.PaySlipAdvancePaymentForLender += 1;
                    invoiceCompare.MoneyPaySlipAdvancePaymentForLender += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.PaySlipFeeForBank:
                    invoiceCompare.PaySlipFeeForBank += 1;
                    invoiceCompare.MoneyPaySlipFeeForBank += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.PaySlipOther:
                    invoiceCompare.PaySlipOther += 1;
                    invoiceCompare.MoneyPaySlipOther += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.PaySlipDisbursement:
                    invoiceCompare.PaySlipDisbursement += 1;
                    invoiceCompare.MoneyPaySlipDisbursement += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.PaySlipFeeTransferMoney:
                    invoiceCompare.PaySlipFeeTransferMoney += 1;
                    invoiceCompare.MoneyPaySlipFeeTransferMoney += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.PaySlipBankFee:
                    invoiceCompare.PaySlipBankFee += 1;
                    invoiceCompare.MoneyPaySlipBankFee += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.PaySlipTheDebt:
                    invoiceCompare.PaySlipTheDebt += 1;
                    invoiceCompare.MoneyPaySlipTheDebt += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.TransferMoneyStoreReasonOther:
                    invoiceCompare.TransferMoneyStoreReasonOther += 1;
                    invoiceCompare.MoneyTransferMoneyStoreReasonOther += invoice.TotalMoney;
                    break;
                case GroupInvoiceType.LenderDepositPayment:
                    invoiceCompare.DepositCount += 1;
                    invoiceCompare.MoneyDeposit += invoice.TotalMoney;
                    break;
                default:
                    break;
            }
        }

        public void SyncStatusInvoiceFromAg()
        {
            LMS.Common.Helper.Utils _common = new LMS.Common.Helper.Utils();
            var invoiceLMSTab = new TableHelper<LMSModel.TblInvoice>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var invoiceTimaTab = new TableHelper<AG.TblTransactionBankCardAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            var loanLMSTab = new TableHelper<LMSModel.TblLoan>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            var _logInvoiceTab = new TableHelper<LMSModel.TblLogInvoice>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);

            var lstInvoiceLMS = invoiceLMSTab.WhereClause(x => x.Status > 0 && x.Status < 3).Query();
            var lstPartnerID = lstInvoiceLMS.Select(x => x.PartnerID);
            // các đơn bên ag đã xử lý
            var lstInvoiceAG = invoiceTimaTab.WhereClause(x => lstPartnerID.Contains(x.Id) && x.Status == 1).Query();
            if (lstInvoiceAG == null || !lstPartnerID.Any())
            {
                Console.WriteLine("---- done ---");
            }
            foreach (var item in lstInvoiceAG)
            {
                try
                {
                    var invoiceLms = lstInvoiceLMS.FirstOrDefault(x => x.PartnerID == item.Id);
                    if (invoiceLms == null || invoiceLms.InvoiceID < 1)
                    {
                        Console.WriteLine($"Khong tim thay phieu nay ben lms: partnerID={item.Id}");
                        continue;
                    }
                    if (item.LoanId < 1)
                    {
                        Console.WriteLine($"Không xác định được đơn vay AG từ phiếu: partnerID={item.Id}");
                        continue;
                    }
                    var loanLms = loanLMSTab.WhereClause(x => x.TimaLoanID == item.LoanId).Query().FirstOrDefault();
                    if (loanLms == null || loanLms.LoanID < 1)
                    {
                        Console.WriteLine($"Không xác định được đơn vay LMS từ phiếu: partnerID={item.Id}");
                        continue;
                    }
                    //var logInvoice = new TblLogInvoice
                    //{
                    //    InvoiceID = invoiceLms.InvoiceID,
                    //    InvoiceType = (int)InvoiceType.Waiting,
                    //    CustomerID = invoiceLms.CustomerID,
                    //    LenderID = invoiceLms.LenderID,
                    //    LoanID = invoiceLms.LoanID,
                    //    FromShopID = invoiceLms.ShopID,
                    //    Note = invoiceLms.Note,
                    //    TransactionDate = DateTime.Now,
                    //    CreateDate = DateTime.Now,
                    //    Amount = invoiceLms.TotalMoney,
                    //    FromBankCardID = invoiceLms.BankCardID,
                    //    CreateBy = invoiceLms.UserIDCreate,
                    //    Status = (int)Invoice_Status.Confirmed,
                    //    Request = _common.ConvertObjectToJSonV2(invoiceLms),
                    //};
                    InvoiceJsonExtra extra = _common.ConvertJSonToObjectV2<InvoiceJsonExtra>(invoiceLms.JsonExtra);
                    extra.SourceName = loanLms.CustomerName;
                    invoiceLms.CustomerID = loanLms.CustomerID.GetValueOrDefault();
                    invoiceLms.SourceID = loanLms.CustomerID.GetValueOrDefault();
                    invoiceLms.JsonExtra = _common.ConvertObjectToJSonV2(extra);
                    invoiceLms.Status = (int)Invoice_Status.Confirmed;
                    invoiceLms.ModifyDate = item.ModifyOn;
                    invoiceLms.UserIDModify = item.UserIdModify;
                    invoiceLMSTab.Update(invoiceLms);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"có lỗi xảy ra: partnerID={item.Id}|ex={ex.Message}");
                }
            }
        }
    }
}
