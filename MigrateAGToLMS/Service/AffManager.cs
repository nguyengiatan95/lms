﻿using LMS.Common.Constants;
using LMS.Common.DAL;
using MigrateAGToLMS.AG;
using MigrateAGToLMS.LMSModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigrateAGToLMS.Service
{
    public class AffManager
    {
        TableHelper<TblAff> _affTab;
        TableHelper<AffAG> _affAGTab;
        TableHelper<tblUserAgV2> _userAgTab;
        TableHelper<TblUser> _userLmsTab;
        LMS.Common.Helper.Utils _common;

        public AffManager()
        {
            _affTab = new TableHelper<TblAff>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            _affAGTab = new TableHelper<AffAG>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            _userAgTab = new TableHelper<tblUserAgV2>().SetConnectString(Ultil.Constant.ConnectionStringAG);
            _userLmsTab = new TableHelper<TblUser>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
            _common = new LMS.Common.Helper.Utils();
        }
        public void MoveAff()
        {
            List<TblAff> affs = new List<TblAff>();
            Console.WriteLine($"-------------- AFF -------------");

            var agData = _affAGTab.Query();

            foreach (var item in agData)
            {
                // kiểm tra thông tin có userid chưa
                // chưa có thì thêm bên ag trước rồi thêm vào lms
                // check theo email
                try
                {
                    var userAGDetail = _userAgTab.WhereClause(x => x.Username == item.Email).Query().FirstOrDefault();
                    long userIDAG = 0;
                    if ((userAGDetail == null || userAGDetail.Id < 1))
                    {
                        if (string.IsNullOrEmpty(item.Email))
                        {
                            continue;
                        }
                        userIDAG = _userAgTab.Insert(new tblUserAgV2
                        {
                            Username = item.Email,
                            FullName = item.Name,
                            Password = _common.HashMD5(item.PhoneNumber),
                            ParentId = 23807, // user credit
                            ExpiredDate = new DateTime(2050, 1, 1),
                            Status = 1,
                            CreateDate = DateTime.Now,
                            GroupId = 100
                        });
                        _userLmsTab.Insert(new TblUser
                        {
                            UserID = userIDAG,
                            TimaUserID = userIDAG,
                            UserName = item.Email,
                            CreateDate = DateTime.Now,
                            Email = item.Email,
                            FullName = item.Name,
                            Password = _common.HashMD5(item.PhoneNumber),
                            Status = (int)User_Status.Active,
                            UserTypeID = (int)User_UserTypeID.Aff
                        }, true);
                    }
                    else
                    {
                        userIDAG = userAGDetail.Id;
                        // kiem tra userID lms chua thi insert vao
                        var affLMS = _affTab.WhereClause(x => x.UserID == userIDAG).Query().FirstOrDefault();
                        if (affLMS == null || affLMS.UserID < 1)
                        {
                            _affTab.Insert(new TblAff
                            {
                                Address = item.Address,
                                AffID = userIDAG,
                                BankCode = item.BankCode,
                                BankID = item.BankID,
                                BankNumber = item.BankNumber,
                                BankPlace = item.BankPlace,
                                BirthDay = item.BirthDay,
                                Code = item.Code,
                                CodeNumber = item.CodeNumber,
                                CommitDisbursement = item.CommitDisbursement,
                                ContractDate = item.ContractDate,
                                CreateBy = (int)(item.UserSaleID ?? 0),
                                CreatedDate = item.CreatedDate,
                                DayComission = item.DayComission,
                                Email = item.Email,
                                FinishContractDate = item.FinishContractDate,
                                Name = item.Name,
                                NumberCard = item.NumberCard,
                                PercentComission = item.PercentComission,
                                PhoneNumber = item.PhoneNumber,
                                Source = item.Source,
                                StaffID = item.UserTakeCareID ?? 0,
                                Status = item.Status,
                                UserID = userIDAG
                            }, true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"[Exception] -{item.Code}-------------{ex.Message}");
                }
            }
            Console.WriteLine($"-------------- Xong AFF -------------");
        }
    }
}
