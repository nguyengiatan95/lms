﻿using LMS.Common.DAL;
using LMS.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigrateAGToLMS.Service
{
    public class GroupManager
    {
        public void MoveGroupFromEnum()
        {
            try
            {
                Console.WriteLine($"-------------- Group -------------");
                var groupTab = new TableHelper<LMSModel.TblGroup>().SetConnectString(Ultil.Constant.ConnectionStringLMSLOS);
                var lstGroup = Enum.GetValues(typeof(AG.StateGroupAccountMeCash)).Cast<AG.StateGroupAccountMeCash>();
                List<LMSModel.TblGroup> lstGroupLMS = new List<LMSModel.TblGroup>();
                foreach (var item in lstGroup)
                {
                    lstGroupLMS.Add(new LMSModel.TblGroup()
                    {
                        GroupID = (long)item,
                        GroupName = item.GetDescription(),
                        Status = 1
                    });
                }
                groupTab.InsertBulk(lstGroupLMS, setOffIdentity: true);
                Console.WriteLine($"INSERT GROUP THÀNH CÔNG");
            }
            catch (Exception)
            {
            }

        }
    }
}
