﻿using Confluent.Kafka;
using FluentValidation;
using FluentValidation.AspNetCore;
using LMS.Kafka.Consumer;
using LMS.Kafka.Interfaces;
using LMS.Kafka.Producer;
using LMS.PaymentServiceApi;
using LMS.PaymentServiceApi.Commands;
using LMS.PaymentServiceApi.KafkaEventHandlers;
using LMS.PaymentServiceApi.Queries;
using LMS.PaymentServiceApi.RestClients;
using LMS.PaymentServiceApi.Services;
using LMS.PaymentServiceApi.Validators;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Steeltoe.Discovery.Client;
using Steeltoe.Discovery.Eureka;
using System.Reflection;

namespace LMS.PaymentService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<KestrelServerOptions>(Configuration.GetSection("Kestrel"));
            services.AddDiscoveryClient(Configuration);
            services.AddServiceDiscovery(options => options.UseEureka());
            services.AddSingleton(Configuration);
            services.AddControllers().AddJsonOptions(opts => opts.JsonSerializerOptions.PropertyNamingPolicy = null);
            services.AddHttpContextAccessor();
            LMS.Common.Configuration.ConnectStringSetting.DefaultConnectString = Configuration["ConnectStringSetting:DefaultConnectString"];
            LMS.Common.Configuration.ConnectStringSetting.THNConnectString = Configuration["ConnectStringSetting:THNConnectString"];
            services.AddTransient(typeof(Common.DAL.ITableHelper<>), typeof(Common.DAL.TableHelper<>));
            services.AddAutoMapper(typeof(Startup));

            services.AddRestClientsService();

            services.AddMvc().AddFluentValidation();
            services.AddMediatR(Assembly.GetExecutingAssembly());

            services.AddTransient<IValidator<GenPaymentScheduleQuery>, GenPaymentScheduleQueryValidator>();
            services.AddTransient<IValidator<CreatePaymentScheduleByLoanIDCommand>, CreatePaymentScheduleByLoanIDCommandValidator>();

            services.AddTransient<IGenPaymentScheduleManager, ActionRateTypeDayBalance>();
            services.AddTransient<IGenPaymentScheduleManager, ActionRateTypeEndingBalance>();
            services.AddTransient<IGenPaymentScheduleManager, ActionRateTypeRateAmortizationMonthly>();
            services.AddTransient<IGenPaymentScheduleManager, ActionRateTypeRateDayMonthly>();
            services.AddTransient<IGenPaymentScheduleManager, ActionRateTypeReducingBalance>();
            services.AddTransient<IGenPaymentScheduleManager, ActionRateTypeRateAmortizationMonthlyTopUp>();

            services.AddTransient<LMS.Common.Helper.IApiHelper, LMS.Common.Helper.ApiHelper>();

            //đóng lãi
            services.AddTransient<IPayPaymentManager, PayOriginalManager>();
            services.AddTransient<IPayPaymentManager, PayConsultantManager>();
            services.AddTransient<IPayPaymentManager, PayInterestManager>();
            services.AddTransient<IPayPaymentManager, PayMoneyFineInterestLateManager>();
            services.AddTransient<IPayPaymentManager, PayMoneyFineLateManager>();
            services.AddTransient<IPayPaymentManager, PayServiceManager>();

            services.AddTransient<ICommonCalculatePayment, CommonCalculatePayment>();
            services.AddTransient<IValidator<UpdateStatusPaymentScheduleByLoanCommand>, UpdateStatusPaymentScheduleByLoanCommandValidator>();

            services.AddTransient<ICloseLoanManager, TypeCloseLoanFinalization>();
            services.AddTransient<ICloseLoanManager, TypeCloseLoanExemption>();
            services.AddTransient<ICloseLoanManager, TypeCloseLoanLiquidation>();
            services.AddTransient<ICloseLoanManager, TypeCloseLoanLiquidationLoanInsurance>();
            services.AddTransient<ICloseLoanManager, TypeCloseLoanPaidInsuranceExemption>();

            services.AddTransient<LMS.Common.Helper.ICalculatorInterestManager, LMS.Common.Helper.CalculatorInterestManager>();

            services.AddTransient<IDebtRestructuringManager, DebtRestructuringAverageConsultingFees>();
            services.AddTransient<IDebtRestructuringManager, DebtRestructuringConsultingFeesChangeDate>();
            services.AddTransient<IDebtRestructuringManager, DebtRestructuringConsultingFeesLastPeriods>();
            services.AddTransient<IDebtRestructuringManager, DebtRestructuringExemptionFees>();
            services.AddTransient<IDebtRestructuringManager, DebtRestructuringOriginal>();
            services.AddTransient<IDebtRestructuringManager, DebtRestructuringBothAverageOriginalConsultingFee>();
            services.AddTransient<IDebtRestructuringManager, DebtRestructuringBothOriginalConsultingFeeLastPeriods>();
            services.AddScoped<DebtRestructuringManager>();

            services.AddTransient<IGenDebtRestructuringManager, GenDebtRestructuringAverageConsultingFees>();
            services.AddTransient<IGenDebtRestructuringManager, GenDebtRestructuringConsultingFeesChangeDate>();
            services.AddTransient<IGenDebtRestructuringManager, GenDebtRestructuringConsultingFeesLastPeriods>();
            services.AddTransient<IGenDebtRestructuringManager, GenDebtRestructuringExemptionFees>();
            services.AddTransient<IGenDebtRestructuringManager, GenDebtRestructuringOriginal>();
            services.AddTransient<IGenDebtRestructuringManager, GenDebtRestructuringBothAverageOriginalConsultingFee>();
            services.AddTransient<IGenDebtRestructuringManager, GenDebtRestructuringBothOriginalConsultingFeeLastPeriods>();
            services.AddScoped<GenDebtRestructuringManager>();

            AddServiceKafka(services);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseCors(options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseAuthorization();
            app.UseDiscoveryClient();
            //Add our new middleware to the pipeline
            app.UseMiddleware<LMS.Common.Helper.RequestResponseLoggingMiddleware>();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
        public void AddServiceKafka(IServiceCollection services)
        {
            var clientConfig = new ClientConfig()
            {
                BootstrapServers = Configuration["Kafka:ClientConfigs:BootstrapServers"]
            };

            var producerConfig = new ProducerConfig(clientConfig);
            var consumerConfig = new ConsumerConfig(clientConfig)
            {
                GroupId = Configuration["spring:application:name"],
                //EnableAutoCommit = true,
                AutoOffsetReset = AutoOffsetReset.Earliest,
                StatisticsIntervalMs = 5000,
                SessionTimeoutMs = 6000
            };

            services.AddSingleton(producerConfig);
            services.AddSingleton(consumerConfig);

            services.AddSingleton(typeof(IKafkaProducer<,>), typeof(KafkaProducer<,>));
            services.AddSingleton(typeof(IKafkaConsumer<,>), typeof(KafkaConsumer<,>));

            services.AddHostedService<KafkaServiceConsumer>();

            services.AddScoped<IKafkaHandler<string, Kafka.Messages.Loan.LoanInsertSuccess>, GenPaymentScheduleByLoanIDHandler>();
        }
    }
}
