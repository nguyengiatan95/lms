﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.PaymentServices;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Services
{
    public interface IGenPaymentScheduleManager
    {
        PaymentSchedule_ActionRateType RateType { get; }
        /// <summary>
        /// </summary>
        /// <param name="totalMoneyDisbursement">số tiền giải ngân</param>
        /// <param name="interestStartDate">ngày bắt đầu tính lãi</param>
        /// <param name="loanTime">tổng số ngày vay</param>
        /// <param name="loanFrequency">tổng số ngày chu kỳ</param>
        /// <param name="rateType"></param>
        /// <param name="rateConsultant">đơn vị số tiền/ngày</param>
        /// <param name="rateInterest">đơn vị số tiền/ngày</param>
        /// <param name="rateService">đơn vị số tiền/ngày</param>
        /// <param name="loanID"></param>
        /// <param name="dayMustPay">ngày khách muốn trả</param>
        /// <returns></returns>
        IEnumerable<Entites.Dtos.PaymentServices.PaymentScheduleModel> GetListPaymentSchedules(long totalMoneyDisbursement, DateTime interestStartDate,
             int loanTime, int loanFrequency, int rateType, decimal rateConsultant, decimal rateInterest, decimal rateService, long loanID = 0, DateTime? dayMustPay = null);
    }
    // tất toán cuối kỳ ratetype 1
    public class ActionRateTypeDayBalance : IGenPaymentScheduleManager
    {
        readonly ILogger<ActionRateTypeDayBalance> _logger;
        readonly LMS.Common.Helper.ICalculatorInterestManager _calculatorInterestManager;
        public ActionRateTypeDayBalance(ILogger<ActionRateTypeDayBalance> logger, LMS.Common.Helper.ICalculatorInterestManager calculatorInterestManager)
        {
            _logger = logger;
            _calculatorInterestManager = calculatorInterestManager;
        }
        public PaymentSchedule_ActionRateType RateType => PaymentSchedule_ActionRateType.RateDay;
        public IEnumerable<PaymentScheduleModel> GetListPaymentSchedules(long totalMoneyDisbursement, DateTime interestStartDate, int loanTime, int loanFrequency, int rateType, decimal rateConsultant, decimal rateInterest, decimal rateService, long loanID = 0, DateTime? dayMustPay = null)
        {
            try
            {
                int numberOfPeriods = loanTime / loanFrequency;
                var lstPaymentScheduleResposne = new List<PaymentScheduleModel>();
                var interestDate = interestStartDate;
                DateTime dtLastDateOfPay = interestDate;
                var ratePerDay = rateConsultant + rateInterest + rateService; // 1 ngày
                var currentDate = DateTime.Now;
                for (int i = 0; i < numberOfPeriods; i++)
                {
                    var todate = dtLastDateOfPay.AddDays(loanFrequency).AddDays(-1);
                    PaymentScheduleModel paymentDetail = new PaymentScheduleModel
                    {
                        FromDate = dtLastDateOfPay,
                        ToDate = todate,
                        CreatedDate = currentDate,
                        ModifiedDate = currentDate,
                        Status = (int)PaymentSchedule_Status.Use,
                        LoanID = loanID
                    };
                    if (i == numberOfPeriods - 1)
                    {
                        paymentDetail.ToDate = interestStartDate.AddDays(loanTime - 1);
                        paymentDetail.MoneyOriginal = totalMoneyDisbursement;
                    }
                    var totalMoneyInterest = _calculatorInterestManager.Calculator(totalMoneyDisbursement, paymentDetail.FromDate, paymentDetail.ToDate, ratePerDay);// Convert.ToInt64(CalculateMoneyInterestByMonthADay(totalMoneyDisbursement, totalRate, paymentDetail.FromDate, paymentDetail.ToDate));
                    paymentDetail.MoneyInterest = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateInterest);// Convert.ToInt64(Math.Round(totalMoneyInterest / totalRate * rateInterest, 0));
                    paymentDetail.MoneyConsultant = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateConsultant);//  Convert.ToInt64(Math.Round(totalMoneyInterest / totalRate * rateConsultant, 0));
                    paymentDetail.MoneyService = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateService);// Convert.ToInt64(Math.Round(totalMoneyInterest / totalRate * rateService, 0));
                    paymentDetail.PayDate = paymentDetail.ToDate;
                    lstPaymentScheduleResposne.Add(paymentDetail);

                    dtLastDateOfPay = paymentDetail.ToDate.AddDays(1);
                }
                return lstPaymentScheduleResposne;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ActionRateTypeDayBalance_GetListPaymentSchedules_Exception|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }
    }

    // tất toán cuối kỳ ratetype 6
    public class ActionRateTypeEndingBalance : IGenPaymentScheduleManager
    {
        readonly ILogger<ActionRateTypeEndingBalance> _logger;
        readonly LMS.Common.Helper.ICalculatorInterestManager _calculatorInterestManager;
        public ActionRateTypeEndingBalance(ILogger<ActionRateTypeEndingBalance> logger, LMS.Common.Helper.ICalculatorInterestManager calculatorInterestManager)
        {
            _logger = logger;
            _calculatorInterestManager = calculatorInterestManager;
        }
        public PaymentSchedule_ActionRateType RateType => PaymentSchedule_ActionRateType.EndingBalance;
        public IEnumerable<PaymentScheduleModel> GetListPaymentSchedules(long totalMoneyDisbursement, DateTime interestStartDate, int loanTime, int loanFrequency, int rateType, decimal rateConsultant, decimal rateInterest, decimal rateService, long loanID = 0, DateTime? dayMustPay = null)
        {
            try
            {
                int numberOfPeriods = loanTime / loanFrequency;
                int frequency = loanFrequency / TimaSettingConstant.NumberDayInMonth; // = 1 tháng
                var lstPaymentScheduleResposne = new List<PaymentScheduleModel>();
                var interestDate = interestStartDate;
                DateTime dtLastDateOfPay = interestDate;
                var ratePerDay = rateConsultant + rateInterest + rateService; // 1 ngày
                var currentDate = DateTime.Now;
                for (int i = 0; i < numberOfPeriods; i++)
                {
                    var todate = dtLastDateOfPay.AddMonths(frequency).AddDays(-1);
                    PaymentScheduleModel paymentDetail = new PaymentScheduleModel
                    {
                        FromDate = dtLastDateOfPay,
                        ToDate = todate,
                        CreatedDate = currentDate,
                        ModifiedDate = currentDate,
                        Status = (int)PaymentSchedule_Status.Use,
                        LoanID = loanID
                    };
                    if (i == numberOfPeriods - 1)
                    {
                        paymentDetail.ToDate = interestStartDate.AddMonths(numberOfPeriods).AddDays(-1);
                        paymentDetail.MoneyOriginal = totalMoneyDisbursement;
                    }
                    var totalMoneyInterest = _calculatorInterestManager.Calculator(totalMoneyDisbursement, paymentDetail.FromDate, paymentDetail.ToDate, ratePerDay);// Convert.ToInt64(CalculateMoneyInterestByMonthADay(totalMoneyDisbursement, totalRate, paymentDetail.FromDate, paymentDetail.ToDate));
                    paymentDetail.MoneyInterest = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateInterest);// Convert.ToInt64(Math.Round(totalMoneyInterest / totalRate * rateInterest, 0));
                    paymentDetail.MoneyConsultant = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateConsultant);// Convert.ToInt64(Math.Round(totalMoneyInterest / totalRate * rateConsultant, 0));
                    paymentDetail.MoneyService = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateService);// Convert.ToInt64(Math.Round(totalMoneyInterest / totalRate * rateService, 0));
                    paymentDetail.PayDate = paymentDetail.ToDate;
                    lstPaymentScheduleResposne.Add(paymentDetail);

                    dtLastDateOfPay = paymentDetail.ToDate.AddDays(1);
                }
                return lstPaymentScheduleResposne;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ActionRateTypeEndingBalance_GetListPaymentSchedules_Exception|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }
    }

    // dư nợ giảm dần ratetype 10
    public class ActionRateTypeReducingBalance : IGenPaymentScheduleManager
    {
        readonly ILogger<ActionRateTypeReducingBalance> _logger;
        readonly LMS.Common.Helper.ICalculatorInterestManager _calculatorInterestManager;
        public ActionRateTypeReducingBalance(ILogger<ActionRateTypeReducingBalance> logger, LMS.Common.Helper.ICalculatorInterestManager calculatorInterestManager)
        {
            _logger = logger;
            _calculatorInterestManager = calculatorInterestManager;
        }
        public PaymentSchedule_ActionRateType RateType => PaymentSchedule_ActionRateType.ReducingBalance;

        public IEnumerable<PaymentScheduleModel> GetListPaymentSchedules(long totalMoneyDisbursement, DateTime interestStartDate, int loanTime, int loanFrequency, int rateType, decimal rateConsultant, decimal rateInterest, decimal rateService, long loanID = 0, DateTime? dayMustPay = null)
        {
            try
            {
                int numberOfPeriods = loanTime / loanFrequency;
                int frequency = loanFrequency / TimaSettingConstant.NumberDayInMonth; // 1 tháng
                var lstPaymentScheduleResposne = new List<PaymentScheduleModel>();
                var common = new LMS.Common.Helper.Utils();
                var interestDate = interestStartDate;
                var ratePerDay = rateConsultant + rateInterest + rateService; // lãi suất 1 ngày

                Decimal interestRate = Convert.ToDecimal(rateInterest * loanFrequency / TimaSettingConstant.UnitMillionMoney); // lãi suất kỳ
                long moneyOfDate = Convert.ToInt64(Math.Round(Convert.ToDouble((totalMoneyDisbursement * interestRate / 1M)) / (1 - Math.Pow(1 / Convert.ToDouble(1 + (interestRate / 1M)), numberOfPeriods)), 0));
                DateTime dtLastDateOfPay = interestDate;
                long totalMoneyCurrent = totalMoneyDisbursement;
                var currentDate = DateTime.Now;
                for (int i = 0; i < numberOfPeriods; i++)
                {
                    var toDate = dtLastDateOfPay.AddMonths(frequency).AddDays(-1);
                    PaymentScheduleModel paymentDetail = new PaymentScheduleModel
                    {
                        FromDate = dtLastDateOfPay,
                        ToDate = toDate,
                        CreatedDate = currentDate,
                        ModifiedDate = currentDate,
                        Status = (int)PaymentSchedule_Status.Use,
                        LoanID = loanID
                    };
                    var totalMoneyInterest = _calculatorInterestManager.Calculator(totalMoneyDisbursement, paymentDetail.FromDate, paymentDetail.ToDate, ratePerDay);// CalculateMoneyInterestByRateAmortization30Days(totalMoneyDisbursement, ratePerDay, paymentDetail.FromDate, paymentDetail.ToDate);
                    paymentDetail.MoneyOriginal = moneyOfDate - totalMoneyInterest;
                    if (i == numberOfPeriods - 1)
                    {
                        paymentDetail.ToDate = interestStartDate.AddMonths(numberOfPeriods).AddDays(-1);
                        paymentDetail.MoneyOriginal = totalMoneyDisbursement;
                        totalMoneyInterest = _calculatorInterestManager.Calculator(totalMoneyDisbursement, paymentDetail.FromDate, paymentDetail.ToDate, ratePerDay);//  CalculateMoneyInterestByRateAmortization30Days(totalMoneyDisbursement, ratePerDay, paymentDetail.FromDate, paymentDetail.ToDate);
                    }
                    paymentDetail.MoneyInterest = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateInterest);// Convert.ToInt64(Math.Round(totalMoneyInterest / ratePerDay * rateInterest, 0));
                    paymentDetail.MoneyService = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateService);// Convert.ToInt64(Math.Round(totalMoneyInterest / ratePerDay * rateService, 0));
                    paymentDetail.MoneyConsultant = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateConsultant);// Convert.ToInt64(Math.Round(totalMoneyInterest / ratePerDay * rateConsultant, 0));
                    paymentDetail.PayDate = paymentDetail.ToDate;

                    totalMoneyCurrent -= paymentDetail.MoneyOriginal;
                    lstPaymentScheduleResposne.Add(paymentDetail);
                    dtLastDateOfPay = toDate.AddDays(1);
                }
                return lstPaymentScheduleResposne;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ActionRateTypeReducingBalance_GetListPaymentSchedules_Exception|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }
    }

    //Tất toán cuối kỳ ratetype 14
    public class ActionRateTypeRateDayMonthly : IGenPaymentScheduleManager
    {
        readonly ILogger<ActionRateTypeRateDayMonthly> _logger;
        readonly LMS.Common.Helper.ICalculatorInterestManager _calculatorInterestManager;
        public ActionRateTypeRateDayMonthly(ILogger<ActionRateTypeRateDayMonthly> logger, LMS.Common.Helper.ICalculatorInterestManager calculatorInterestManager)
        {
            _logger = logger;
            _calculatorInterestManager = calculatorInterestManager;
        }
        public PaymentSchedule_ActionRateType RateType => PaymentSchedule_ActionRateType.RateDayMonthly;

        public IEnumerable<PaymentScheduleModel> GetListPaymentSchedules(long totalMoneyDisbursement, DateTime interestStartDate, int loanTime, int loanFrequency, int rateType, decimal rateConsultant, decimal rateInterest, decimal rateService, long loanID = 0, DateTime? dayMustPay = null)
        {
            try
            {
                var lstPaymentScheduleResposne = new List<PaymentScheduleModel>();
                DateTime dtLastDateOfPay = interestStartDate;
                DateTime dtNextDate = GetNextDateFirstByRateTypeAmoritization(interestStartDate);

                int frequency = loanFrequency / TimaSettingConstant.NumberDayInMonth;
                int numberOfPeriods = loanTime / loanFrequency;
                var ratePerDay = rateConsultant + rateInterest + rateService; // 1 ngày
                var currentDate = DateTime.Now;
                for (int i = 0; i < numberOfPeriods; i++)
                {
                    PaymentScheduleModel paymentDetail = new PaymentScheduleModel
                    {
                        FromDate = dtLastDateOfPay,
                        ToDate = dtNextDate,
                        CreatedDate = currentDate,
                        ModifiedDate = currentDate,
                        Status = (int)PaymentSchedule_Status.Use,
                        LoanID = loanID
                    };
                    if (i == numberOfPeriods - 1)
                    {
                        paymentDetail.ToDate = interestStartDate.AddMonths(numberOfPeriods).AddDays(-1);
                        paymentDetail.MoneyOriginal = totalMoneyDisbursement;
                    }
                    var totalMoneyInterest = _calculatorInterestManager.Calculator(totalMoneyDisbursement, paymentDetail.FromDate, paymentDetail.ToDate, ratePerDay);// Convert.ToInt64(CalculateMoneyInterestByMonthADay(totalMoneyDisbursement, ratePerDay, paymentDetail.FromDate, paymentDetail.ToDate));
                    paymentDetail.MoneyInterest = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateInterest);// Convert.ToInt64(Math.Round(totalMoneyInterest / ratePerDay * rateInterest, 0));
                    paymentDetail.MoneyConsultant = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateConsultant);// Convert.ToInt64(Math.Round(totalMoneyInterest / ratePerDay * rateConsultant, 0));
                    paymentDetail.MoneyService = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateService);// Convert.ToInt64(Math.Round(totalMoneyInterest / ratePerDay * rateService, 0));
                    paymentDetail.PayDate = paymentDetail.ToDate;
                    lstPaymentScheduleResposne.Add(paymentDetail);
                    // Gán lại giá trị
                    dtNextDate = dtNextDate.AddMonths(frequency);
                    dtLastDateOfPay = paymentDetail.ToDate.AddDays(1);
                }
                return lstPaymentScheduleResposne;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ActionRateTypeRateDayMonthly_GetListPaymentSchedules_Exception|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }
        private DateTime GetNextDateFirstByRateTypeAmoritization(DateTime fromDate)
        {
            if (fromDate.Day < 25)
            {
                return fromDate.AddMonths(1);
            }
            Dictionary<int, int> dictEarlyDay = new Dictionary<int, int>
            {
                { 25, 1 },
                { 26, 2 },
                { 27, 3 },
                { 28, 4 },
                { 29, 5 },
                { 30, 6 },
                { 31, 7 }
            };
            var nextDate = fromDate.AddMonths(2); // tháng T + 2

            return new DateTime(nextDate.Year, nextDate.Month, dictEarlyDay[fromDate.Day]);
        }
    }

    // Dư nợ giảm dần ratetype 13
    public class ActionRateTypeRateAmortizationMonthly : IGenPaymentScheduleManager
    {
        readonly ILogger<ActionRateTypeRateAmortizationMonthly> _logger;
        readonly LMS.Common.Helper.ICalculatorInterestManager _calculatorInterestManager;
        public ActionRateTypeRateAmortizationMonthly(ILogger<ActionRateTypeRateAmortizationMonthly> logger, LMS.Common.Helper.ICalculatorInterestManager calculatorInterestManager)
        {
            _logger = logger;
            _calculatorInterestManager = calculatorInterestManager;
        }
        public PaymentSchedule_ActionRateType RateType => PaymentSchedule_ActionRateType.RateAmortizationMonthly;

        public IEnumerable<PaymentScheduleModel> GetListPaymentSchedules(long totalMoneyDisbursement, DateTime interestStartDate, int loanTime, int loanFrequency, int rateType, decimal rateConsultant, decimal rateInterest, decimal rateService, long loanID = 0, DateTime? dayMustPay = null)
        {
            try
            {
                var lstPaymentScheduleResposne = new List<PaymentScheduleModel>();
                DateTime dtNextDate = GetNextDateFirstByRateTypeAmoritization(interestStartDate);
                int frequency = loanFrequency / TimaSettingConstant.NumberDayInMonth;
                int numberOfPeriods = loanTime / TimaSettingConstant.NumberDayInMonth;
                DateTime dtLastDateOfPay = interestStartDate;
                var ratePerDay = rateConsultant + rateInterest + rateService; // 1 ngày
                Decimal interestRate = Convert.ToDecimal(ratePerDay * loanFrequency / TimaSettingConstant.UnitMillionMoney); // lãi suất kỳ
                long moneyOfDate = Convert.ToInt64(Math.Round(Convert.ToDouble((totalMoneyDisbursement * interestRate / 1M)) / (1 - Math.Pow(1 / Convert.ToDouble(1 + (interestRate / 1M)), numberOfPeriods)), 0));
                var totalMoneyBackUp = totalMoneyDisbursement;
                long totalPayNeedInPeriods = 0;
                var currentDate = DateTime.Now;
                for (int i = 0; i < numberOfPeriods; i++)
                {
                    PaymentScheduleModel paymentDetail = new PaymentScheduleModel
                    {
                        FromDate = dtLastDateOfPay,
                        ToDate = dtNextDate,
                        CreatedDate = currentDate,
                        ModifiedDate = currentDate,
                        Status = (int)PaymentSchedule_Status.Use,
                        LoanID = loanID
                    };
                    var totalMoneyInterest = _calculatorInterestManager.Calculator(totalMoneyDisbursement, paymentDetail.FromDate, paymentDetail.ToDate, ratePerDay);//  CalculateMoneyInterestByRateAmortization30Days(totalMoneyDisbursement, ratePerDay, paymentDetail.FromDate, paymentDetail.ToDate);
                    paymentDetail.MoneyOriginal = moneyOfDate - totalMoneyInterest;
                    totalPayNeedInPeriods = moneyOfDate;
                    if (i == numberOfPeriods - 1)
                    {
                        paymentDetail.ToDate = interestStartDate.AddMonths(numberOfPeriods).AddDays(-1);
                        paymentDetail.MoneyOriginal = totalMoneyDisbursement;
                        totalMoneyInterest = _calculatorInterestManager.Calculator(totalMoneyDisbursement, paymentDetail.FromDate, paymentDetail.ToDate, ratePerDay);// CalculateMoneyInterestByRateAmortization30Days(totalMoneyDisbursement, ratePerDay, paymentDetail.FromDate, paymentDetail.ToDate);
                        totalPayNeedInPeriods = totalMoneyDisbursement + totalMoneyInterest;
                    }
                    paymentDetail.MoneyInterest = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateInterest);// Convert.ToInt64(Math.Round(totalMoneyInterest / ratePerDay * rateInterest, 0));
                    paymentDetail.MoneyConsultant = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateConsultant);// Convert.ToInt64(Math.Round(totalMoneyInterest / ratePerDay * rateConsultant, 0));
                    paymentDetail.MoneyService = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateService);// Convert.ToInt64(Math.Round(totalMoneyInterest / ratePerDay * rateService, 0));
                    paymentDetail.PayDate = paymentDetail.ToDate;
                    lstPaymentScheduleResposne.Add(paymentDetail);
                    // Gán lại giá trị
                    dtNextDate = dtNextDate.AddMonths(frequency);
                    dtLastDateOfPay = paymentDetail.ToDate.AddDays(1);
                    totalMoneyDisbursement -= paymentDetail.MoneyOriginal;
                }
                // Tính tiền chêch lệch
                long moneyDifference = totalPayNeedInPeriods - moneyOfDate;

                // Reset list
                totalMoneyDisbursement = totalMoneyBackUp;
                lstPaymentScheduleResposne.Clear();
                long moneySharingPeriods = Convert.ToInt64(moneyDifference / numberOfPeriods * TimaSettingConstant.RateMoneySharingPeriods);
                moneyOfDate += moneySharingPeriods;
                dtNextDate = GetNextDateFirstByRateTypeAmoritization(interestStartDate);
                dtLastDateOfPay = interestStartDate.AddDays(-1);
                for (int i = 0; i < numberOfPeriods; i++)
                {
                    PaymentScheduleModel paymentDetail = new PaymentScheduleModel
                    {
                        FromDate = dtLastDateOfPay.AddDays(1),
                        ToDate = dtNextDate,
                        CreatedDate = currentDate,
                        ModifiedDate = currentDate,
                        Status = (int)PaymentSchedule_Status.Use
                    };
                    var totalMoneyInterest = _calculatorInterestManager.Calculator(totalMoneyDisbursement, paymentDetail.FromDate, paymentDetail.ToDate, ratePerDay);//  CalculateMoneyInterestByRateAmortization30Days(totalMoneyDisbursement, ratePerDay, paymentDetail.FromDate, paymentDetail.ToDate);
                    paymentDetail.MoneyOriginal = moneyOfDate - totalMoneyInterest;
                    totalPayNeedInPeriods = moneyOfDate;
                    if (i == numberOfPeriods - 1)
                    {
                        paymentDetail.ToDate = interestStartDate.AddMonths(numberOfPeriods).AddDays(-1);
                        paymentDetail.MoneyOriginal = totalMoneyDisbursement;
                        totalMoneyInterest = _calculatorInterestManager.Calculator(totalMoneyDisbursement, paymentDetail.FromDate, paymentDetail.ToDate, ratePerDay);//  CalculateMoneyInterestByRateAmortization30Days(totalMoneyDisbursement, ratePerDay, paymentDetail.FromDate, paymentDetail.ToDate);
                        totalPayNeedInPeriods = totalMoneyDisbursement + totalMoneyInterest;
                    }
                    paymentDetail.MoneyInterest = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateInterest);// Convert.ToInt64(Math.Round(totalMoneyInterest / ratePerDay * rateInterest, 0));
                    paymentDetail.MoneyConsultant = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateConsultant);// Convert.ToInt64(Math.Round(totalMoneyInterest / ratePerDay * rateConsultant, 0));
                    paymentDetail.MoneyService = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateService);// Convert.ToInt64(Math.Round(totalMoneyInterest / ratePerDay * rateService, 0));
                    paymentDetail.PayDate = paymentDetail.ToDate;
                    lstPaymentScheduleResposne.Add(paymentDetail);
                    // Gán lại giá trị
                    dtNextDate = dtNextDate.AddMonths(frequency);
                    dtLastDateOfPay = paymentDetail.ToDate;
                    totalMoneyDisbursement -= paymentDetail.MoneyOriginal;
                }
                return lstPaymentScheduleResposne;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ActionRateTypeRateDayMonthly_GetListPaymentSchedules_Exception|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }
        private DateTime GetNextDateFirstByRateTypeAmoritization(DateTime fromDate)
        {
            if (fromDate.Day < 25)
            {
                return fromDate.AddMonths(1);
            }
            Dictionary<int, int> dictEarlyDay = new Dictionary<int, int>
            {
                { 25, 1 },
                { 26, 2 },
                { 27, 3 },
                { 28, 4 },
                { 29, 5 },
                { 30, 6 },
                { 31, 7 }
            };
            var nextDate = fromDate.AddMonths(2); // tháng T + 2

            return new DateTime(nextDate.Year, nextDate.Month, dictEarlyDay[fromDate.Day]);
        }

    }

    // Dư nợ giảm dần topup ratetype 12
    public class ActionRateTypeRateAmortizationMonthlyTopUp : IGenPaymentScheduleManager
    {
        readonly ILogger<ActionRateTypeRateAmortizationMonthlyTopUp> _logger;
        readonly LMS.Common.Helper.ICalculatorInterestManager _calculatorInterestManager;
        public ActionRateTypeRateAmortizationMonthlyTopUp(ILogger<ActionRateTypeRateAmortizationMonthlyTopUp> logger, LMS.Common.Helper.ICalculatorInterestManager calculatorInterestManager)
        {
            _logger = logger;
            _calculatorInterestManager = calculatorInterestManager;
        }
        private const int MinDistanceDay = 15;
        public PaymentSchedule_ActionRateType RateType => PaymentSchedule_ActionRateType.RateAmortization30DaysTopUp;

        public IEnumerable<PaymentScheduleModel> GetListPaymentSchedules(long totalMoneyDisbursement, DateTime interestStartDate, int loanTime, int loanFrequency, int rateType, decimal rateConsultant, decimal rateInterest, decimal rateService, long loanID = 0, DateTime? dayMustPay = null)
        {
            try
            {
                int numberOfPeriods = loanTime / loanFrequency;
                int frequency = loanFrequency / TimaSettingConstant.NumberDayInMonth; // 1 tháng
                var lstPaymentScheduleResposne = new List<PaymentScheduleModel>();
                var common = new LMS.Common.Helper.Utils();
                var interestDate = interestStartDate;
                var ratePerDay = rateConsultant + rateInterest + rateService; // lãi suất 1 ngày

                Decimal interestRate = Convert.ToDecimal(ratePerDay * loanFrequency / TimaSettingConstant.UnitMillionMoney); // lãi suất kỳ
                long moneyOfDate = Convert.ToInt64(Math.Round(Convert.ToDouble((totalMoneyDisbursement * interestRate / 1M)) / (1 - Math.Pow(1 / Convert.ToDouble(1 + (interestRate / 1M)), numberOfPeriods)), 0));
                DateTime dtLastDateOfPay = interestDate;
                long totalMoneyCurrent = totalMoneyDisbursement;
                var currentDate = DateTime.Now;
                //var toDate = dtLastDateOfPay.AddMonths(frequency).AddDays(-1);
                var toDate = dtLastDateOfPay.AddMonths(frequency);
                if (dayMustPay.HasValue)
                {
                    if (dayMustPay.Value.Subtract(interestDate).Days < MinDistanceDay)
                    {
                        // lấy kỳ tháng tiếp theo
                        toDate = dayMustPay.Value.AddMonths(1);
                    }
                    else
                    {
                        toDate = dayMustPay.Value;
                    }
                }
                for (int i = 0; i < numberOfPeriods; i++)
                {
                    //var toDate = dtLastDateOfPay.AddMonths(frequency).AddDays(-1);
                    PaymentScheduleModel paymentDetail = new PaymentScheduleModel
                    {
                        FromDate = dtLastDateOfPay,
                        ToDate = toDate,
                        CreatedDate = currentDate,
                        ModifiedDate = currentDate,
                        Status = (int)PaymentSchedule_Status.Use,
                        LoanID = loanID
                    };
                    var totalMoneyInterest = _calculatorInterestManager.Calculator(totalMoneyDisbursement, paymentDetail.FromDate, paymentDetail.ToDate, ratePerDay);// CalculateMoneyInterestByRateAmortization30Days(totalMoneyDisbursement, ratePerDay, paymentDetail.FromDate, paymentDetail.ToDate);
                    paymentDetail.MoneyOriginal = moneyOfDate - totalMoneyInterest;
                    if (i == numberOfPeriods - 1)
                    {
                        paymentDetail.ToDate = interestStartDate.AddMonths(numberOfPeriods).AddDays(-1);
                        paymentDetail.MoneyOriginal = totalMoneyDisbursement;
                        totalMoneyInterest = _calculatorInterestManager.Calculator(totalMoneyDisbursement, paymentDetail.FromDate, paymentDetail.ToDate, ratePerDay);// CalculateMoneyInterestByRateAmortization30Days(totalMoneyDisbursement, ratePerDay, paymentDetail.FromDate, paymentDetail.ToDate);
                    }
                    paymentDetail.MoneyInterest = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateInterest);// Convert.ToInt64(Math.Round(totalMoneyInterest / ratePerDay * rateInterest, 0));
                    paymentDetail.MoneyService = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateService);// Convert.ToInt64(Math.Round(totalMoneyInterest / ratePerDay * rateService, 0));
                    paymentDetail.MoneyConsultant = _calculatorInterestManager.Calculator(totalMoneyInterest, ratePerDay, rateConsultant);// Convert.ToInt64(Math.Round(totalMoneyInterest / ratePerDay * rateConsultant, 0));
                    paymentDetail.PayDate = paymentDetail.ToDate;

                    totalMoneyDisbursement -= paymentDetail.MoneyOriginal;
                    lstPaymentScheduleResposne.Add(paymentDetail);

                    // set giá trị kỳ tiếp theo
                    dtLastDateOfPay = toDate.AddDays(1);
                    //toDate = dtLastDateOfPay.AddMonths(frequency).AddDays(-1);
                    toDate = toDate.AddMonths(frequency);
                }
                return lstPaymentScheduleResposne;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ActionRateTypeRateAmortizationMonthlyTopUp_GetListPaymentSchedules_Exception|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }
    }
}
