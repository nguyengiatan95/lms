﻿using LMS.Common.Constants;
using LMS.Common.Helper.DeepCopyExtensions;
using LMS.PaymentServiceApi.Domain.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Services
{
    public interface IDebtRestructuringManager
    {
        PaymentSchedule_TypeDebtRestructuring TypeDebtRestructuring { get; }
        Task<ResponseActionResult> DebtRestructing(DebtRestructuringModel request);
    }
    public class DebtRestructuringManager
    {
        protected LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        protected ILogger<DebtRestructuringManager> _logger;
        protected Common.Helper.Utils _common;
        private IEnumerable<Domain.Tables.TblPaymentSchedule> _lstPayment;
        RestClients.ITransactionService _transactionService;
        public DebtRestructuringManager(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            RestClients.ITransactionService transactionService,
            ILogger<DebtRestructuringManager> logger)
        {
            _paymentScheduleTab = paymentScheduleTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _transactionService = transactionService;
            var dt = DateTime.Now;
            LastDayOfMonthCurrentRestruturing = new DateTime(dt.Year, dt.Month, 1).AddMonths(1).AddDays(-1);
        }

        protected async Task<List<Domain.Tables.TblPaymentSchedule>> GetTblPaymentSchedules(long loanID)
        {
            return (await _paymentScheduleTab.WhereClause(x => x.LoanID == loanID && x.Status == (int)PaymentSchedule_Status.Use).QueryAsync()).OrderBy(x => x.PayDate).ToList();
        }

        protected bool CheckLstPaymentValid(IEnumerable<Domain.Tables.TblPaymentSchedule> lstPayment)
        {
            if (lstPayment == null || !lstPayment.Any())
            {
                return false;
            }
            _lstPayment = lstPayment.Where(m => m.IsVisible == Convert.ToBoolean(PaymentSchedule_IsVisible.Show)).ToList();
            if (_lstPayment == null || !_lstPayment.Any())
            {
                //response.Message = "HD-" + reqDebtRestructuring.loanBriefId + "| TC-" + objLoan.CodeID + " Không còn kỳ nào để giãn nợ nữa";
                //return response;
                return false;
            }
            return true;
        }
        public List<Domain.Tables.TblPaymentSchedule> GetListPaymentActive
        {
            get { return _lstPayment != null ? _lstPayment.ToList() : new List<Domain.Tables.TblPaymentSchedule>(); }
        }

        public Domain.Models.PaymentTransactionInsertModel CreateInfoDeleteTransactionMoneyFeeLate(DebtRestructuringModel request, Domain.Tables.TblPaymentSchedule paymentDetail)
        {
            return new PaymentTransactionInsertModel
            {
                ActionID = (int)Transaction_Action.DeleteTransactionMoneyFeeLate,
                LoanID = request.LoanID,
                MoneyType = (int)Transaction_TypeMoney.FineLate,
                TotalMoney = (paymentDetail.MoneyFineLate - paymentDetail.PayMoneyFineLate) * -1,
                UserID = request.CreateBy,
                PaymentScheduleID = paymentDetail.PaymentScheduleID
            };
        }
        public async Task ExecuteCreateTransaction(List<PaymentTransactionInsertModel> lstTransaction)
        {
            if (lstTransaction != null && lstTransaction.Count > 0)
                await _transactionService.CreateTransactionAfterPayment(lstTransaction);
        }
        public DateTime LastDayOfMonthCurrentRestruturing { get; private set; }

    }
    public class DebtRestructuringOriginal : DebtRestructuringManager, IDebtRestructuringManager
    {
        readonly List<int> _lstPaymentRateType;
        public DebtRestructuringOriginal(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            RestClients.ITransactionService transactionService,
            ILogger<DebtRestructuringManager> logger) : base(paymentScheduleTab, transactionService, logger)
        {
            _lstPaymentRateType = new List<int>
            {
                (int)PaymentSchedule_ActionRateType.RateAmortizationMonth,
                (int)PaymentSchedule_ActionRateType.ReducingBalance,
                (int)PaymentSchedule_ActionRateType.RateAmortization30DaysTopUp,
                (int)PaymentSchedule_ActionRateType.RateAmortizationMonthly,
            };
        }
        public PaymentSchedule_TypeDebtRestructuring TypeDebtRestructuring => PaymentSchedule_TypeDebtRestructuring.Original;

        public async Task<ResponseActionResult> DebtRestructing(DebtRestructuringModel reqDebtRestructuring)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstPaymentScheduleNeedCollect = await this.GetTblPaymentSchedules(reqDebtRestructuring.LoanID);
                if (!CheckLstPaymentValid(lstPaymentScheduleNeedCollect))
                {
                    response.Message = MessageConstant.Payment_NotFound;
                    return response;
                }
                if (!_lstPaymentRateType.Contains(reqDebtRestructuring.LoanRateType))
                {
                    response.Message = "Chức năng khoanh gốc này không phù hợp với các đơn vay theo tất toán cuối kỳ";
                    return response;
                }
                if (reqDebtRestructuring.NumberOfRepaymentPeriod < 1)
                {
                    response.Message = "Chức năng khoanh gốc này bắt buộc phải chọn số kỳ cần khoanh";
                }
                if (reqDebtRestructuring.NumberOfRepaymentPeriod > 3)
                {
                    response.Message = "Chức năng khoanh gốc này chỉ cho phép giãn tối đa là 3 kỳ";
                }
                lstPaymentScheduleNeedCollect = this.GetListPaymentActive;
                if (lstPaymentScheduleNeedCollect.Count() - 1 < reqDebtRestructuring.NumberOfRepaymentPeriod)
                {
                    response.Message = "Số kỳ khoanh gốc lớn hơn số kỳ còn phải đóng của đơn vay (không bao gồm kỳ cuối)";
                    return response;
                }
                List<PaymentTransactionInsertModel> lstTransactionInsert = new List<PaymentTransactionInsertModel>();
                Dictionary<string, Domain.Tables.TblPaymentSchedule> dictPaymentInsertInfos = new Dictionary<string, Domain.Tables.TblPaymentSchedule>();
                long totalMoneyOriginalNeedCollect = lstPaymentScheduleNeedCollect.Sum(m => m.MoneyOriginal);
                long totalMoneyOriginalAfterRestruct = 0;
                long totalMoneyOriginal = 0;
                var currentDate = DateTime.Now;
                long totalMoneyFineLateCashBack = 0;
                // tổng gốc trong các kỳ khoanh
                // lịch thay đổi ở các kỳ
                for (int i = 0; i < reqDebtRestructuring.NumberOfRepaymentPeriod; i++)
                {
                    Domain.Tables.TblPaymentSchedule paymentNew = lstPaymentScheduleNeedCollect[i].DeepCopyByExpressionTree();
                    totalMoneyOriginal += paymentNew.MoneyOriginal - paymentNew.PayMoneyOriginal;

                    // xem như kỳ này tiền gốc đã trả
                    paymentNew.PaymentScheduleID = 0;
                    paymentNew.CreatedDate = currentDate;
                    paymentNew.MoneyOriginal = lstPaymentScheduleNeedCollect[i].PayMoneyOriginal;
                    paymentNew.ModifiedDate = currentDate;
                    totalMoneyOriginalAfterRestruct += paymentNew.MoneyOriginal;

                    lstPaymentScheduleNeedCollect[i].ModifiedDate = currentDate;
                    lstPaymentScheduleNeedCollect[i].Status = (int)PaymentSchedule_Status.NoUse;
                    // cập nhật ngày phạt vào cuối tháng của kỳ đó
                    paymentNew.DateApplyFineLate = paymentNew.PayDate.AddDays(reqDebtRestructuring.MinDayFineLate);
                    if (paymentNew.IsVisible == Convert.ToBoolean(PaymentSchedule_IsVisible.Show))
                    {
                        if (paymentNew.DateApplyFineLate < this.LastDayOfMonthCurrentRestruturing)
                        {
                            paymentNew.DateApplyFineLate = this.LastDayOfMonthCurrentRestruturing;
                            paymentNew.Fined = (int)PaymentSchedule_Fined.NotYet;
                        }
                        // cập nhật bút toán miễn trừ phí phạt trả chậm nếu có
                        if (paymentNew.MoneyFineLate - paymentNew.PayMoneyFineLate > 0)
                        {
                            var txnDetail = CreateInfoDeleteTransactionMoneyFeeLate(reqDebtRestructuring, paymentNew);
                            lstTransactionInsert.Add(txnDetail);
                            paymentNew.MoneyFineLate = paymentNew.PayMoneyFineLate;
                        }
                    }

                    // mặc định các ngày sẽ khác nhau nhưng check cho hợp lệ
                    var keyDictPayment = paymentNew.PayDate.ToString("yyyyMMdd");
                    if (!dictPaymentInsertInfos.ContainsKey(keyDictPayment))
                    {
                        dictPaymentInsertInfos.Add(keyDictPayment, paymentNew);
                    }
                }

                int soKyConLai = lstPaymentScheduleNeedCollect.Count - reqDebtRestructuring.NumberOfRepaymentPeriod;
                long avgtotalMoneyOriginal = Convert.ToInt64(Math.Round(totalMoneyOriginal * 1.0 / soKyConLai, 0));

                if (soKyConLai == 1)
                {
                    Domain.Tables.TblPaymentSchedule paymentNew = lstPaymentScheduleNeedCollect[reqDebtRestructuring.NumberOfRepaymentPeriod].DeepCopyByExpressionTree();

                    lstPaymentScheduleNeedCollect[reqDebtRestructuring.NumberOfRepaymentPeriod].ModifiedDate = currentDate;
                    lstPaymentScheduleNeedCollect[reqDebtRestructuring.NumberOfRepaymentPeriod].Status = (int)PaymentSchedule_Status.NoUse;

                    paymentNew.PaymentScheduleID = 0;
                    paymentNew.MoneyOriginal += totalMoneyOriginal;
                    paymentNew.CreatedDate = currentDate;
                    paymentNew.ModifiedDate = currentDate;
                    var keyDictPayment = paymentNew.PayDate.ToString("yyyyMMdd");
                    if (!dictPaymentInsertInfos.ContainsKey(keyDictPayment))
                    {
                        dictPaymentInsertInfos.Add(keyDictPayment, paymentNew);
                    }
                }
                else
                {
                    // giữ lại các kỳ không thay đổi
                    for (int i = reqDebtRestructuring.NumberOfRepaymentPeriod; i < lstPaymentScheduleNeedCollect.Count; i++)
                    {
                        Domain.Tables.TblPaymentSchedule paymentNew = lstPaymentScheduleNeedCollect[i].DeepCopyByExpressionTree();
                        if (i == lstPaymentScheduleNeedCollect.Count - 1) // đảm bảo tổng tiền gốc sau khi tái cơ cấu luôn luôn bằng tổng tiền giải ngân
                        {
                            paymentNew.MoneyOriginal = totalMoneyOriginalNeedCollect - totalMoneyOriginalAfterRestruct;
                        }
                        else
                        {
                            paymentNew.MoneyOriginal += avgtotalMoneyOriginal;
                            totalMoneyOriginalAfterRestruct += paymentNew.MoneyOriginal;
                        }
                        lstPaymentScheduleNeedCollect[i].Status = (int)PaymentSchedule_Status.NoUse;
                        lstPaymentScheduleNeedCollect[i].ModifiedDate = currentDate;

                        paymentNew.PaymentScheduleID = 0;
                        paymentNew.CreatedDate = currentDate;
                        paymentNew.ModifiedDate = currentDate;

                        var keyDictPayment = paymentNew.PayDate.ToString("yyyyMMdd");
                        if (!dictPaymentInsertInfos.ContainsKey(keyDictPayment))
                        {
                            dictPaymentInsertInfos.Add(keyDictPayment, paymentNew);
                        }
                    }
                }
                if (dictPaymentInsertInfos.Count > 0)
                {
                    // tiền phạt hủy giá trị âm
                    totalMoneyFineLateCashBack = lstTransactionInsert.Sum(x => x.TotalMoney) * -1;
                    _paymentScheduleTab.UpdateBulk(lstPaymentScheduleNeedCollect);
                    _paymentScheduleTab.InsertBulk(dictPaymentInsertInfos.Values.OrderBy(x => x.PayDate).ToList());
                    await this.ExecuteCreateTransaction(lstTransactionInsert);
                }
                response.SetSucces();
                response.Data = totalMoneyFineLateCashBack;
            }
            catch (Exception ex)
            {
                _logger.LogError($"DebtRestructuringOriginal|request={_common.ConvertObjectToJSonV2(reqDebtRestructuring)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
    public class DebtRestructuringAverageConsultingFees : DebtRestructuringManager, IDebtRestructuringManager
    {
        public DebtRestructuringAverageConsultingFees(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            RestClients.ITransactionService transactionService,
            ILogger<DebtRestructuringManager> logger) : base(paymentScheduleTab, transactionService, logger)
        {

        }
        public PaymentSchedule_TypeDebtRestructuring TypeDebtRestructuring => PaymentSchedule_TypeDebtRestructuring.AverageConsultingFees;

        public async Task<ResponseActionResult> DebtRestructing(DebtRestructuringModel reqDebtRestructuring)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstPaymentScheduleNeedCollect = await this.GetTblPaymentSchedules(reqDebtRestructuring.LoanID);
                if (!CheckLstPaymentValid(lstPaymentScheduleNeedCollect))
                {
                    response.Message = MessageConstant.Payment_NotFound;
                    return response;
                }
                if (reqDebtRestructuring.NumberOfRepaymentPeriod < 1)
                {
                    response.Message = "Chức năng khoanh phí tư vấn này bắt buộc phải chọn số kỳ cần khoanh";
                    return response;
                }

                if (reqDebtRestructuring.NumberOfRepaymentPeriod > 3)
                {
                    response.Message = "Chức năng khoanh phí tư vấn này chỉ cho phép giãn tối đa là 3 kỳ";
                    return response;
                }
                lstPaymentScheduleNeedCollect = this.GetListPaymentActive;

                if (lstPaymentScheduleNeedCollect.Count - 1 < reqDebtRestructuring.NumberOfRepaymentPeriod)
                {
                    response.Message = "Số kỳ khoanh phí tư vấn lớn hơn số kỳ còn phải đóng của đơn vay (không bao gồm kỳ cuối)";
                    return response;
                }

                List<PaymentTransactionInsertModel> lstTransactionInsert = new List<PaymentTransactionInsertModel>();
                Dictionary<string, Domain.Tables.TblPaymentSchedule> dictPaymentInsertInfos = new Dictionary<string, Domain.Tables.TblPaymentSchedule>();
                long totalMoneyConsultantNeedCollect = lstPaymentScheduleNeedCollect.Sum(m => m.MoneyConsultant);
                long totalMoneyConsultantAfterRestruct = 0;
                long totalMoneyConsultant = 0;
                long totalMoneyFineLateCashBack = 0;
                var dt = DateTime.Now;
                // thay đổi tại các kỳ hiện tại cần xử lý
                for (int i = 0; i < reqDebtRestructuring.NumberOfRepaymentPeriod; i++)
                {
                    Domain.Tables.TblPaymentSchedule paymentNew = lstPaymentScheduleNeedCollect[i].DeepCopyByExpressionTree();

                    totalMoneyConsultant += paymentNew.MoneyConsultant - paymentNew.PayMoneyConsultant;
                    paymentNew.MoneyConsultant = paymentNew.PayMoneyConsultant;
                    totalMoneyConsultantAfterRestruct += paymentNew.MoneyConsultant;

                    lstPaymentScheduleNeedCollect[i].ModifiedDate = dt;
                    lstPaymentScheduleNeedCollect[i].Status = (int)PaymentSchedule_Status.NoUse;

                    paymentNew.CreatedDate = dt;
                    paymentNew.ModifiedDate = dt;
                    paymentNew.PaymentScheduleID = 0;
                    // cập nhật ngày phạt vào cuối tháng của kỳ đó
                    paymentNew.DateApplyFineLate = paymentNew.PayDate.AddDays(reqDebtRestructuring.MinDayFineLate);
                    if (paymentNew.IsVisible == Convert.ToBoolean(PaymentSchedule_IsVisible.Show))
                    {
                        if (paymentNew.DateApplyFineLate < LastDayOfMonthCurrentRestruturing)
                        {
                            paymentNew.DateApplyFineLate = LastDayOfMonthCurrentRestruturing;
                            paymentNew.Fined = (int)PaymentSchedule_Fined.NotYet;
                        }
                        // cập nhật bút toán miễn trừ phí phạt trả chậm nếu có
                        if (paymentNew.MoneyFineLate - paymentNew.PayMoneyFineLate > 0)
                        {
                            var txnDetail = CreateInfoDeleteTransactionMoneyFeeLate(reqDebtRestructuring, paymentNew);
                            lstTransactionInsert.Add(txnDetail);
                            paymentNew.MoneyFineLate = paymentNew.PayMoneyFineLate;
                        }
                    }

                    // mặc định các ngày sẽ khác nhau nhưng check cho hợp lệ
                    var keyDictPayment = paymentNew.PayDate.ToString("yyyyMMdd");
                    if (!dictPaymentInsertInfos.ContainsKey(keyDictPayment))
                    {
                        dictPaymentInsertInfos.Add(keyDictPayment, paymentNew);
                    }
                }

                int soKyConLai = lstPaymentScheduleNeedCollect.Count - reqDebtRestructuring.NumberOfRepaymentPeriod;
                long avgtotalMoneyConsultant = Convert.ToInt64(Math.Round(totalMoneyConsultant * 1.0 / soKyConLai, 0));

                if (soKyConLai == 1)
                {
                    Domain.Tables.TblPaymentSchedule paymentNew = lstPaymentScheduleNeedCollect[reqDebtRestructuring.NumberOfRepaymentPeriod].DeepCopyByExpressionTree();

                    lstPaymentScheduleNeedCollect[reqDebtRestructuring.NumberOfRepaymentPeriod].ModifiedDate = dt;
                    lstPaymentScheduleNeedCollect[reqDebtRestructuring.NumberOfRepaymentPeriod].Status = (int)PaymentSchedule_Status.NoUse;

                    paymentNew.MoneyConsultant += totalMoneyConsultant;
                    paymentNew.CreatedDate = dt;
                    paymentNew.ModifiedDate = dt;
                    paymentNew.PaymentScheduleID = 0;
                    var keyDictPayment = paymentNew.PayDate.ToString("yyyyMMdd");
                    if (!dictPaymentInsertInfos.ContainsKey(keyDictPayment))
                    {
                        dictPaymentInsertInfos.Add(keyDictPayment, paymentNew);
                    }
                    else
                    {
                        dictPaymentInsertInfos[keyDictPayment].MoneyConsultant += totalMoneyConsultant;
                    }
                }
                else
                {
                    // chuyển các kỳ còn lại vào lịch
                    for (int i = reqDebtRestructuring.NumberOfRepaymentPeriod; i < lstPaymentScheduleNeedCollect.Count; i++)
                    {
                        Domain.Tables.TblPaymentSchedule paymentNew = lstPaymentScheduleNeedCollect[i].DeepCopyByExpressionTree();
                        if (i == lstPaymentScheduleNeedCollect.Count - 1) // đảm bảo tổng tiền phí tư vấn sau khi tái cơ cấu luôn luôn bằng tổng tiền trước lúc tái cơ cấu
                        {
                            paymentNew.MoneyConsultant = totalMoneyConsultantNeedCollect - totalMoneyConsultantAfterRestruct;
                        }
                        else
                        {
                            paymentNew.MoneyConsultant += avgtotalMoneyConsultant;
                            totalMoneyConsultantAfterRestruct += paymentNew.MoneyConsultant;
                        }

                        lstPaymentScheduleNeedCollect[i].ModifiedDate = dt;
                        lstPaymentScheduleNeedCollect[i].Status = (int)PaymentSchedule_Status.NoUse;

                        paymentNew.CreatedDate = dt;
                        paymentNew.ModifiedDate = dt;
                        paymentNew.PaymentScheduleID = 0;
                        var keyDictPayment = paymentNew.PayDate.ToString("yyyyMMdd");
                        if (!dictPaymentInsertInfos.ContainsKey(keyDictPayment))
                        {
                            dictPaymentInsertInfos.Add(keyDictPayment, paymentNew);
                        }
                    }
                }
                if (dictPaymentInsertInfos.Count > 0)
                {
                    // tiền phạt hủy giá trị âm
                    totalMoneyFineLateCashBack = lstTransactionInsert.Sum(x => x.TotalMoney) * -1;
                    _paymentScheduleTab.UpdateBulk(lstPaymentScheduleNeedCollect);
                    _paymentScheduleTab.InsertBulk(dictPaymentInsertInfos.Values.OrderBy(x => x.PayDate).ToList());
                    await this.ExecuteCreateTransaction(lstTransactionInsert);
                }
                response.SetSucces();
                response.Data = totalMoneyFineLateCashBack;
            }
            catch (Exception ex)
            {
                _logger.LogError($"DebtRestructuringAverageConsultingFees|request={_common.ConvertObjectToJSonV2(reqDebtRestructuring)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
    public class DebtRestructuringConsultingFeesLastPeriods : DebtRestructuringManager, IDebtRestructuringManager
    {
        public DebtRestructuringConsultingFeesLastPeriods(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            RestClients.ITransactionService transactionService,
            ILogger<DebtRestructuringManager> logger) : base(paymentScheduleTab, transactionService, logger)
        {

        }
        public PaymentSchedule_TypeDebtRestructuring TypeDebtRestructuring => PaymentSchedule_TypeDebtRestructuring.ConsultingFeesLastPeriods;

        public async Task<ResponseActionResult> DebtRestructing(DebtRestructuringModel reqDebtRestructuring)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstPaymentScheduleNeedCollect = await this.GetTblPaymentSchedules(reqDebtRestructuring.LoanID);
                if (!CheckLstPaymentValid(lstPaymentScheduleNeedCollect))
                {
                    response.Message = MessageConstant.Payment_NotFound;
                    return response;
                }
                if (reqDebtRestructuring.NumberOfRepaymentPeriod < 1)
                {
                    response.Message = "Chức năng khoanh phí tư vấn này bắt buộc phải chọn số kỳ cần khoanh";
                    return response;
                }

                if (reqDebtRestructuring.NumberOfRepaymentPeriod > 3)
                {
                    response.Message = "Chức năng khoanh phí tư vấn này chỉ cho phép giãn tối đa là 3 kỳ";
                    return response;
                }

                lstPaymentScheduleNeedCollect = this.GetListPaymentActive;
                if (lstPaymentScheduleNeedCollect.Count - 1 <= 0)
                {
                    response.Message = "hợp đồng này còn 1 kỳ thì không cần phải khoanh vùng";
                    return response;
                }

                List<PaymentTransactionInsertModel> lstTransactionInsert = new List<PaymentTransactionInsertModel>();
                Dictionary<string, Domain.Tables.TblPaymentSchedule> dictPaymentInsertInfos = new Dictionary<string, Domain.Tables.TblPaymentSchedule>();
                long totalMoneyConsultantAfterRestruct = 0;
                long totalMoneyFineLateCashBack = 0;
                var dt = DateTime.Now;
                //long totalMoneyConsultant = 0;
                for (int i = 0; i < reqDebtRestructuring.NumberOfRepaymentPeriod; i++)
                {
                    Domain.Tables.TblPaymentSchedule paymentNew = lstPaymentScheduleNeedCollect[i].DeepCopyByExpressionTree();
                    totalMoneyConsultantAfterRestruct += paymentNew.MoneyConsultant - paymentNew.PayMoneyConsultant;
                    paymentNew.MoneyConsultant = paymentNew.PayMoneyConsultant;

                    lstPaymentScheduleNeedCollect[i].ModifiedDate = dt;
                    lstPaymentScheduleNeedCollect[i].Status = (int)PaymentSchedule_Status.NoUse;

                    paymentNew.CreatedDate = dt;
                    paymentNew.ModifiedDate = dt;
                    paymentNew.PaymentScheduleID = 0;
                    // cập nhật ngày phạt vào cuối tháng của kỳ đó
                    paymentNew.DateApplyFineLate = paymentNew.PayDate.AddDays(reqDebtRestructuring.MinDayFineLate);
                    if (paymentNew.IsVisible == Convert.ToBoolean(PaymentSchedule_IsVisible.Show))
                    {
                        if (paymentNew.DateApplyFineLate < LastDayOfMonthCurrentRestruturing)
                        {
                            paymentNew.DateApplyFineLate = LastDayOfMonthCurrentRestruturing;
                            paymentNew.Fined = (int)PaymentSchedule_Fined.NotYet;
                        }
                        // cập nhật bút toán miễn trừ phí phạt trả chậm nếu có
                        if (paymentNew.MoneyFineLate - paymentNew.PayMoneyFineLate > 0)
                        {
                            var txnDetail = CreateInfoDeleteTransactionMoneyFeeLate(reqDebtRestructuring, paymentNew);
                            lstTransactionInsert.Add(txnDetail);
                            paymentNew.MoneyFineLate = paymentNew.PayMoneyFineLate;
                        }
                    }

                    var keyDictPayment = paymentNew.PayDate.ToString("yyyyMMdd");
                    if (!dictPaymentInsertInfos.ContainsKey(keyDictPayment))
                    {
                        dictPaymentInsertInfos.Add(keyDictPayment, paymentNew);
                    }
                }
                Domain.Tables.TblPaymentSchedule paymentNewLast = lstPaymentScheduleNeedCollect[lstPaymentScheduleNeedCollect.Count - 1].DeepCopyByExpressionTree();
                lstPaymentScheduleNeedCollect[lstPaymentScheduleNeedCollect.Count - 1].ModifiedDate = dt;
                lstPaymentScheduleNeedCollect[lstPaymentScheduleNeedCollect.Count - 1].Status = (int)PaymentSchedule_Status.NoUse;

                var keyDictPaymentLast = paymentNewLast.PayDate.ToString("yyyyMMdd");
                paymentNewLast.MoneyConsultant += totalMoneyConsultantAfterRestruct;
                paymentNewLast.CreatedDate = dt;
                paymentNewLast.ModifiedDate = dt;
                paymentNewLast.PaymentScheduleID = 0;
                if (!dictPaymentInsertInfos.ContainsKey(keyDictPaymentLast))
                {
                    dictPaymentInsertInfos.Add(keyDictPaymentLast, paymentNewLast);
                }
                else
                {
                    dictPaymentInsertInfos[keyDictPaymentLast].MoneyConsultant += totalMoneyConsultantAfterRestruct;
                }

                if (dictPaymentInsertInfos.Count > 0)
                {
                    // tiền phạt hủy giá trị âm
                    totalMoneyFineLateCashBack = lstTransactionInsert.Sum(x => x.TotalMoney) * -1;
                    _paymentScheduleTab.UpdateBulk(lstPaymentScheduleNeedCollect);
                    _paymentScheduleTab.InsertBulk(dictPaymentInsertInfos.Values.OrderBy(x => x.PayDate).ToList());
                    await ExecuteCreateTransaction(lstTransactionInsert);
                }
                response.SetSucces();
                response.Data = totalMoneyFineLateCashBack;
            }
            catch (Exception ex)
            {
                _logger.LogError($"DebtRestructuringConsultingFeesLastPeriods|request={_common.ConvertObjectToJSonV2(reqDebtRestructuring)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
    public class DebtRestructuringConsultingFeesChangeDate : DebtRestructuringManager, IDebtRestructuringManager
    {
        public DebtRestructuringConsultingFeesChangeDate(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            RestClients.ITransactionService transactionService,
            ILogger<DebtRestructuringManager> logger) : base(paymentScheduleTab, transactionService, logger)
        {

        }
        public PaymentSchedule_TypeDebtRestructuring TypeDebtRestructuring => PaymentSchedule_TypeDebtRestructuring.ConsultingFeesChangeDate;

        public async Task<ResponseActionResult> DebtRestructing(DebtRestructuringModel reqDebtRestructuring)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstPaymentScheduleNeedCollect = await this.GetTblPaymentSchedules(reqDebtRestructuring.LoanID);
                if (!CheckLstPaymentValid(lstPaymentScheduleNeedCollect))
                {
                    response.Message = MessageConstant.Payment_NotFound;
                    return response;
                }
                if (reqDebtRestructuring.NumberOfRepaymentPeriod < 1)
                {
                    response.Message = "Chức năng khoanh phí tư vấn này bắt buộc phải chọn số kỳ cần khoanh";
                    return response;
                }

                if (reqDebtRestructuring.NumberOfRepaymentPeriod > 3)
                {
                    response.Message = "Chức năng khoanh phí tư vấn này chỉ cho phép giãn tối đa là 3 kỳ";
                    return response;
                }

                if (String.IsNullOrEmpty(reqDebtRestructuring.AppointmentDate))
                {
                    response.Message = "Muốn chuyển phí tư vấn vào 1 ngày ngoài lịch trả nợ thì phải chọn ngày";
                    return response;
                }
                DateTime appointmentDate;

                try
                {
                    appointmentDate = DateTime.ParseExact(reqDebtRestructuring.AppointmentDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                }
                catch
                {
                    response.Message = $"Ngày chọn phải là định dạng: {TimaSettingConstant.DateTimeDayMonthYear}";
                    _logger.LogError($"DebtRestructuringConsultingFeesChangeDate|request={_common.ConvertObjectToJSonV2(reqDebtRestructuring)}|message={response.Message}");
                    return response;
                }

                List<PaymentTransactionInsertModel> lstTransactionInsert = new List<PaymentTransactionInsertModel>();
                lstPaymentScheduleNeedCollect = this.GetListPaymentActive;
                Dictionary<string, Domain.Tables.TblPaymentSchedule> dictPaymentInsertInfos = new Dictionary<string, Domain.Tables.TblPaymentSchedule>();
                long totalMoneyConsultantAfterRestruct = 0;
                long totalMoneyFineLateCashBack = 0;
                var dt = DateTime.Now;
                for (int i = 0; i < reqDebtRestructuring.NumberOfRepaymentPeriod; i++)
                {
                    Domain.Tables.TblPaymentSchedule paymentNew = lstPaymentScheduleNeedCollect[i].DeepCopyByExpressionTree();
                    totalMoneyConsultantAfterRestruct += paymentNew.MoneyConsultant - paymentNew.PayMoneyConsultant;
                    paymentNew.MoneyConsultant = paymentNew.PayMoneyConsultant;
                    paymentNew.CreatedDate = dt;
                    paymentNew.ModifiedDate = dt;
                    paymentNew.PaymentScheduleID = 0;
                    lstPaymentScheduleNeedCollect[i].ModifiedDate = dt;
                    lstPaymentScheduleNeedCollect[i].Status = (int)PaymentSchedule_Status.NoUse;
                    // cập nhật ngày phạt vào cuối tháng của kỳ đó
                    paymentNew.DateApplyFineLate = paymentNew.PayDate.AddDays(reqDebtRestructuring.MinDayFineLate);
                    if (paymentNew.IsVisible == Convert.ToBoolean(PaymentSchedule_IsVisible.Show))
                    {
                        if (paymentNew.DateApplyFineLate < LastDayOfMonthCurrentRestruturing)
                        {
                            paymentNew.DateApplyFineLate = LastDayOfMonthCurrentRestruturing;
                            paymentNew.Fined = (int)PaymentSchedule_Fined.NotYet;
                        }
                        // cập nhật bút toán miễn trừ phí phạt trả chậm nếu có
                        if (paymentNew.MoneyFineLate - paymentNew.PayMoneyFineLate > 0)
                        {
                            var txnDetail = CreateInfoDeleteTransactionMoneyFeeLate(reqDebtRestructuring, paymentNew);
                            lstTransactionInsert.Add(txnDetail);
                            paymentNew.MoneyFineLate = paymentNew.PayMoneyFineLate;
                        }
                    }
                    var keyDictPayment = paymentNew.PayDate.ToString("yyyyMMdd");
                    if (!dictPaymentInsertInfos.ContainsKey(keyDictPayment))
                    {
                        dictPaymentInsertInfos.Add(keyDictPayment, paymentNew);
                    }
                }

                Domain.Tables.TblPaymentSchedule objPaymentSchedule = new Domain.Tables.TblPaymentSchedule
                {
                    LoanID = lstPaymentScheduleNeedCollect[0].LoanID,
                    FromDate = lstPaymentScheduleNeedCollect[0].FromDate,
                    ToDate = lstPaymentScheduleNeedCollect[lstPaymentScheduleNeedCollect.Count - 1].ToDate,
                    PayDate = appointmentDate,
                    MoneyConsultant = totalMoneyConsultantAfterRestruct,
                    IsVisible = Convert.ToBoolean(PaymentSchedule_IsVisible.Show),
                    IsComplete = (int)PaymentSchedule_IsComplete.Waiting,
                    Status = (int)PaymentSchedule_Status.Use,
                    CreatedDate = dt,
                    ModifiedDate = dt
                };
                var lstInsert = dictPaymentInsertInfos.Values.ToList();
                lstInsert.Add(objPaymentSchedule);
                if (lstInsert.Count > 0)
                {
                    // tiền phạt hủy giá trị âm
                    totalMoneyFineLateCashBack = lstTransactionInsert.Sum(x => x.TotalMoney) * -1;
                    _paymentScheduleTab.UpdateBulk(lstPaymentScheduleNeedCollect);
                    _paymentScheduleTab.InsertBulk(lstInsert.OrderBy(x => x.PayDate));
                    await ExecuteCreateTransaction(lstTransactionInsert);
                }
                response.SetSucces();
                response.Data = totalMoneyFineLateCashBack;
            }
            catch (Exception ex)
            {
                _logger.LogError($"DebtRestructuringConsultingFeesChangeDate|request={_common.ConvertObjectToJSonV2(reqDebtRestructuring)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
    public class DebtRestructuringExemptionFees : DebtRestructuringManager, IDebtRestructuringManager
    {
        public DebtRestructuringExemptionFees(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            RestClients.ITransactionService transactionService,
            ILogger<DebtRestructuringManager> logger) : base(paymentScheduleTab, transactionService, logger)
        {

        }
        public PaymentSchedule_TypeDebtRestructuring TypeDebtRestructuring => PaymentSchedule_TypeDebtRestructuring.ExemptionFees;

        public async Task<ResponseActionResult> DebtRestructing(DebtRestructuringModel reqDebtRestructuring)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstPaymentScheduleNeedCollect = await this.GetTblPaymentSchedules(reqDebtRestructuring.LoanID);
                if (!CheckLstPaymentValid(lstPaymentScheduleNeedCollect))
                {
                    response.Message = MessageConstant.Payment_NotFound;
                    return response;
                }
                if (reqDebtRestructuring.NumberOfRepaymentPeriod < 1)
                {
                    response.Message = "Chức năng Miễn giảm phí tư vấn này bắt buộc phải chọn số kỳ cần miễn giảm";
                    return response;
                }

                if (reqDebtRestructuring.NumberOfRepaymentPeriod > 2)
                {
                    response.Message = "Chức năng Miễn giảm phí tư vấn này chỉ cho phép miễn giảm tối đa là 2 kỳ";
                    return response;
                }

                if (reqDebtRestructuring.PercentDiscount < 10 && reqDebtRestructuring.PercentDiscount > 100)
                {
                    response.Message = "Chức năng này phải có phần trăm miễn giảm trong khoảng 10 -> 100";
                    return response;
                }
                List<PaymentTransactionInsertModel> lstTransactionInsert = new List<PaymentTransactionInsertModel>();
                lstPaymentScheduleNeedCollect = this.GetListPaymentActive;
                Dictionary<string, Domain.Tables.TblPaymentSchedule> dictPaymentInsertInfos = new Dictionary<string, Domain.Tables.TblPaymentSchedule>();
                int PercentDiscountHold = 100 - reqDebtRestructuring.PercentDiscount;
                long totalMoneyFineLateCashBack = 0;
                var dt = DateTime.Now;
                for (int i = 0; i < reqDebtRestructuring.NumberOfRepaymentPeriod; i++)
                {
                    Domain.Tables.TblPaymentSchedule paymentNew = lstPaymentScheduleNeedCollect[i].DeepCopyByExpressionTree();
                    var sotienConLai = Convert.ToInt32(Math.Round(paymentNew.MoneyConsultant * 1.0 / 100 * PercentDiscountHold, 0));
                    if (sotienConLai >= paymentNew.PayMoneyConsultant)
                    {
                        paymentNew.MoneyConsultant = sotienConLai;
                        lstPaymentScheduleNeedCollect[i].ModifiedDate = dt;
                        paymentNew.CreatedDate = dt;
                        paymentNew.ModifiedDate = dt;
                        paymentNew.PaymentScheduleID = 0;
                        // cập nhật ngày phạt vào cuối tháng của kỳ đó
                        paymentNew.DateApplyFineLate = paymentNew.PayDate.AddDays(reqDebtRestructuring.MinDayFineLate);
                        if (paymentNew.IsVisible == Convert.ToBoolean(PaymentSchedule_IsVisible.Show))
                        {
                            if (paymentNew.DateApplyFineLate < LastDayOfMonthCurrentRestruturing)
                            {
                                paymentNew.DateApplyFineLate = LastDayOfMonthCurrentRestruturing;
                                paymentNew.Fined = (int)PaymentSchedule_Fined.NotYet;
                            }
                            // cập nhật bút toán miễn trừ phí phạt trả chậm nếu có
                            if (paymentNew.MoneyFineLate - paymentNew.PayMoneyFineLate > 0)
                            {
                                var txnDetail = CreateInfoDeleteTransactionMoneyFeeLate(reqDebtRestructuring, paymentNew);
                                lstTransactionInsert.Add(txnDetail);
                                paymentNew.MoneyFineLate = paymentNew.PayMoneyFineLate;
                            }
                        }
                        var keyDictPayment = paymentNew.PayDate.ToString("yyyyMMdd");
                        if (!dictPaymentInsertInfos.ContainsKey(keyDictPayment))
                        {
                            dictPaymentInsertInfos.Add(keyDictPayment, paymentNew);
                        }
                    }
                }
                if (dictPaymentInsertInfos.Count > 0)
                {
                    // tiền phạt hủy giá trị âm
                    totalMoneyFineLateCashBack = lstTransactionInsert.Sum(x => x.TotalMoney) * -1;
                    _paymentScheduleTab.UpdateBulk(lstPaymentScheduleNeedCollect);
                    _paymentScheduleTab.InsertBulk(dictPaymentInsertInfos.Values.OrderBy(x => x.PayDate).ToList());
                    await ExecuteCreateTransaction(lstTransactionInsert);
                }
                response.SetSucces();
                response.Data = totalMoneyFineLateCashBack;
            }
            catch (Exception ex)
            {
                _logger.LogError($"DebtRestructuringExemptionFees|request={_common.ConvertObjectToJSonV2(reqDebtRestructuring)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }

    public class DebtRestructuringBothAverageOriginalConsultingFee : DebtRestructuringManager, IDebtRestructuringManager
    {
        readonly List<int> _lstPaymentRateType;
        public DebtRestructuringBothAverageOriginalConsultingFee(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            RestClients.ITransactionService transactionService,
            ILogger<DebtRestructuringManager> logger) : base(paymentScheduleTab, transactionService, logger)
        {
            _lstPaymentRateType = new List<int>
            {
                (int)PaymentSchedule_ActionRateType.RateAmortizationMonth,
                (int)PaymentSchedule_ActionRateType.ReducingBalance,
                (int)PaymentSchedule_ActionRateType.RateAmortization30DaysTopUp,
                (int)PaymentSchedule_ActionRateType.RateAmortizationMonthly,
            };
        }
        public PaymentSchedule_TypeDebtRestructuring TypeDebtRestructuring => PaymentSchedule_TypeDebtRestructuring.BothAverageOriginalConsultingFee;

        public async Task<ResponseActionResult> DebtRestructing(DebtRestructuringModel reqDebtRestructuring)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstPaymentScheduleNeedCollect = await this.GetTblPaymentSchedules(reqDebtRestructuring.LoanID);
                if (!CheckLstPaymentValid(lstPaymentScheduleNeedCollect))
                {
                    response.Message = MessageConstant.Payment_NotFound;
                    return response;
                }
                if (!_lstPaymentRateType.Contains(reqDebtRestructuring.LoanRateType))
                {
                    response.Message = "Chức năng khoanh gốc & phí tư vấn này không phù hợp với các đơn vay theo tất toán cuối kỳ";
                    return response;
                }
                if (reqDebtRestructuring.NumberOfRepaymentPeriod < 1)
                {
                    response.Message = "Chức năng khoanh gốc & phí tư vấn này bắt buộc phải chọn số kỳ cần khoanh";
                    return response;
                }

                if (reqDebtRestructuring.NumberOfRepaymentPeriod > 3)
                {
                    response.Message = "Chức năng khoanh gốc & phí tư vấn này chỉ cho phép giãn tối đa là 3 kỳ";
                    return response;
                }
                lstPaymentScheduleNeedCollect = this.GetListPaymentActive;
                if (lstPaymentScheduleNeedCollect.Count - 1 < reqDebtRestructuring.NumberOfRepaymentPeriod)
                {
                    response.Message = "Số kỳ khoanh  gốc & phí tư vấn lớn hơn số kỳ còn phải đóng của đơn vay (không bao gồm kỳ cuối)";
                    return response;
                }

                List<PaymentTransactionInsertModel> lstTransactionInsert = new List<PaymentTransactionInsertModel>();
                Dictionary<string, Domain.Tables.TblPaymentSchedule> dictPaymentInsertInfos = new Dictionary<string, Domain.Tables.TblPaymentSchedule>();
                long totalMoneyOriginalNeedCollect = lstPaymentScheduleNeedCollect.Sum(m => m.MoneyOriginal);
                long totalMoneyOriginalAfterRestruct = 0;
                long totalMoneyOriginal = 0;

                long totalMoneyConsultantNeedCollect = lstPaymentScheduleNeedCollect.Sum(m => m.MoneyConsultant);
                long totalMoneyConsultantAfterRestruct = 0;
                long totalMoneyConsultant = 0;
                long totalMoneyFineLateCashBack = 0;
                var dt = DateTime.Now;

                for (int i = 0; i < reqDebtRestructuring.NumberOfRepaymentPeriod; i++)
                {
                    Domain.Tables.TblPaymentSchedule paymentNew = lstPaymentScheduleNeedCollect[i].DeepCopyByExpressionTree();
                    totalMoneyOriginal += paymentNew.MoneyOriginal - paymentNew.PayMoneyOriginal;
                    paymentNew.MoneyOriginal = paymentNew.PayMoneyOriginal;
                    totalMoneyOriginalAfterRestruct += paymentNew.MoneyOriginal;

                    totalMoneyConsultant += paymentNew.MoneyConsultant - paymentNew.PayMoneyConsultant;
                    paymentNew.MoneyConsultant = paymentNew.PayMoneyConsultant;
                    totalMoneyConsultantAfterRestruct += paymentNew.MoneyConsultant;

                    paymentNew.CreatedDate = dt;
                    paymentNew.ModifiedDate = dt;
                    paymentNew.PaymentScheduleID = 0;
                    lstPaymentScheduleNeedCollect[i].ModifiedDate = dt;
                    lstPaymentScheduleNeedCollect[i].Status = (int)PaymentSchedule_Status.NoUse;
                    // cập nhật ngày phạt vào cuối tháng của kỳ đó
                    paymentNew.DateApplyFineLate = paymentNew.PayDate.AddDays(reqDebtRestructuring.MinDayFineLate);
                    if (paymentNew.IsVisible == Convert.ToBoolean(PaymentSchedule_IsVisible.Show))
                    {
                        if (paymentNew.DateApplyFineLate < LastDayOfMonthCurrentRestruturing)
                        {
                            paymentNew.DateApplyFineLate = LastDayOfMonthCurrentRestruturing;
                            paymentNew.Fined = (int)PaymentSchedule_Fined.NotYet;
                        }
                        // cập nhật bút toán miễn trừ phí phạt trả chậm nếu có
                        if (paymentNew.MoneyFineLate - paymentNew.PayMoneyFineLate > 0)
                        {
                            var txnDetail = CreateInfoDeleteTransactionMoneyFeeLate(reqDebtRestructuring, paymentNew);
                            lstTransactionInsert.Add(txnDetail);
                            paymentNew.MoneyFineLate = paymentNew.PayMoneyFineLate;
                        }
                    }
                    var keyDictPayment = $"{paymentNew.PayDate:yyyyMMdd}";
                    if (!dictPaymentInsertInfos.ContainsKey(keyDictPayment))
                    {
                        dictPaymentInsertInfos.Add(keyDictPayment, paymentNew);
                    }
                }

                int soKyConLai = lstPaymentScheduleNeedCollect.Count - reqDebtRestructuring.NumberOfRepaymentPeriod;

                long avgtotalMoneyOriginal = Convert.ToInt64(Math.Round(totalMoneyOriginal * 1.0 / soKyConLai, 0));
                long avgtotalMoneyConsultant = Convert.ToInt64(Math.Round(totalMoneyConsultant * 1.0 / soKyConLai, 0));

                if (soKyConLai == 1)
                {
                    Domain.Tables.TblPaymentSchedule paymentNew = lstPaymentScheduleNeedCollect[reqDebtRestructuring.NumberOfRepaymentPeriod].DeepCopyByExpressionTree();
                    paymentNew.MoneyOriginal += totalMoneyOriginal;
                    paymentNew.MoneyConsultant += totalMoneyConsultant;
                    paymentNew.CreatedDate = dt;
                    paymentNew.ModifiedDate = dt;
                    paymentNew.PaymentScheduleID = 0;
                    lstPaymentScheduleNeedCollect[reqDebtRestructuring.NumberOfRepaymentPeriod].ModifiedDate = dt;
                    lstPaymentScheduleNeedCollect[reqDebtRestructuring.NumberOfRepaymentPeriod].Status = (int)PaymentSchedule_Status.NoUse;
                    var keyDictPayment = $"{paymentNew.PayDate:yyyyMMdd}";
                    if (!dictPaymentInsertInfos.ContainsKey(keyDictPayment))
                    {
                        dictPaymentInsertInfos.Add(keyDictPayment, paymentNew);
                    }
                    else
                    {
                        dictPaymentInsertInfos[keyDictPayment].MoneyOriginal += totalMoneyOriginal;
                        dictPaymentInsertInfos[keyDictPayment].MoneyConsultant += totalMoneyConsultant;
                    }
                }
                else
                {
                    for (int i = reqDebtRestructuring.NumberOfRepaymentPeriod; i < lstPaymentScheduleNeedCollect.Count; i++)
                    {
                        Domain.Tables.TblPaymentSchedule paymentNew = lstPaymentScheduleNeedCollect[i].DeepCopyByExpressionTree();
                        if (i == lstPaymentScheduleNeedCollect.Count - 1) // đảm bảo tổng tiền gốc sau khi tái cơ cấu luôn luôn bằng tổng tiền giải ngân
                        {
                            paymentNew.MoneyOriginal = totalMoneyOriginalNeedCollect - totalMoneyOriginalAfterRestruct;
                            paymentNew.MoneyConsultant = totalMoneyConsultantNeedCollect - totalMoneyConsultantAfterRestruct;

                        }
                        else
                        {
                            paymentNew.MoneyOriginal += avgtotalMoneyOriginal;
                            totalMoneyOriginalAfterRestruct += paymentNew.MoneyOriginal;

                            paymentNew.MoneyConsultant += avgtotalMoneyConsultant;
                            totalMoneyConsultantAfterRestruct += paymentNew.MoneyConsultant;
                        }
                        paymentNew.CreatedDate = dt;
                        paymentNew.ModifiedDate = dt;
                        paymentNew.PaymentScheduleID = 0;
                        lstPaymentScheduleNeedCollect[i].ModifiedDate = dt;
                        lstPaymentScheduleNeedCollect[i].Status = (int)PaymentSchedule_Status.NoUse;
                        var keyDictPayment = $"{paymentNew.PayDate:yyyyMMdd}";
                        if (!dictPaymentInsertInfos.ContainsKey(keyDictPayment))
                        {
                            dictPaymentInsertInfos.Add(keyDictPayment, paymentNew);
                        }
                    }
                }
                if (dictPaymentInsertInfos.Count > 0)
                {
                    // tiền phạt hủy giá trị âm
                    totalMoneyFineLateCashBack = lstTransactionInsert.Sum(x => x.TotalMoney) * -1;
                    _paymentScheduleTab.UpdateBulk(lstPaymentScheduleNeedCollect);
                    _paymentScheduleTab.InsertBulk(dictPaymentInsertInfos.Values.OrderBy(x => x.PayDate).ToList());
                    await ExecuteCreateTransaction(lstTransactionInsert);
                }
                response.SetSucces();
                response.Data = totalMoneyFineLateCashBack;
            }
            catch (Exception ex)
            {
                _logger.LogError($"DebtRestructuringBothAverageOriginalConsultingFee|request={_common.ConvertObjectToJSonV2(reqDebtRestructuring)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }

    public class DebtRestructuringBothOriginalConsultingFeeLastPeriods : DebtRestructuringManager, IDebtRestructuringManager
    {
        readonly List<int> _lstPaymentRateType;
        public DebtRestructuringBothOriginalConsultingFeeLastPeriods(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            RestClients.ITransactionService transactionService,
            ILogger<DebtRestructuringManager> logger) : base(paymentScheduleTab, transactionService, logger)
        {
            _lstPaymentRateType = new List<int>
            {
                (int)PaymentSchedule_ActionRateType.RateAmortizationMonth,
                (int)PaymentSchedule_ActionRateType.ReducingBalance,
                (int)PaymentSchedule_ActionRateType.RateAmortization30DaysTopUp,
                (int)PaymentSchedule_ActionRateType.RateAmortizationMonthly,
            };
        }
        public PaymentSchedule_TypeDebtRestructuring TypeDebtRestructuring => PaymentSchedule_TypeDebtRestructuring.BothOriginalConsultingFeeLastPeriods;

        public async Task<ResponseActionResult> DebtRestructing(DebtRestructuringModel reqDebtRestructuring)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstPaymentScheduleNeedCollect = await this.GetTblPaymentSchedules(reqDebtRestructuring.LoanID);
                if (!CheckLstPaymentValid(lstPaymentScheduleNeedCollect))
                {
                    response.Message = MessageConstant.Payment_NotFound;
                    return response;
                }
                if (!_lstPaymentRateType.Contains(reqDebtRestructuring.LoanRateType))
                {
                    response.Message = "Chức năng khoanh gốc & phí tư vấn này không phù hợp với các đơn vay theo tất toán cuối kỳ";
                    return response;
                }
                if (reqDebtRestructuring.NumberOfRepaymentPeriod < 1)
                {
                    response.Message = "Chức năng khoanh gốc & phí tư vấn này bắt buộc phải chọn số kỳ cần khoanh";
                    return response;
                }

                if (reqDebtRestructuring.NumberOfRepaymentPeriod > 3)
                {
                    response.Message = "Chức năng khoanh gốc & phí tư vấn này chỉ cho phép giãn tối đa là 3 kỳ";
                    return response;
                }

                lstPaymentScheduleNeedCollect = this.GetListPaymentActive;
                if (lstPaymentScheduleNeedCollect.Count - 1 < reqDebtRestructuring.NumberOfRepaymentPeriod)
                {
                    response.Message = "Số kỳ khoanh gốc & phí tư vấn lớn hơn số kỳ còn phải đóng của đơn vay (không bao gồm kỳ cuối)";
                    return response;
                }

                List<PaymentTransactionInsertModel> lstTransactionInsert = new List<PaymentTransactionInsertModel>();
                Dictionary<string, Domain.Tables.TblPaymentSchedule> dictPaymentInsertInfos = new Dictionary<string, Domain.Tables.TblPaymentSchedule>();
                long totalMoneyOriginalAfterRestruct = 0;
                long totalMoneyConsultantAfterRestruct = 0;
                long totalMoneyFineLateCashBack = 0;
                var dt = DateTime.Now;

                for (int i = 0; i < reqDebtRestructuring.NumberOfRepaymentPeriod; i++)
                {
                    Domain.Tables.TblPaymentSchedule paymentNew = lstPaymentScheduleNeedCollect[i].DeepCopyByExpressionTree();

                    totalMoneyOriginalAfterRestruct += paymentNew.MoneyOriginal - paymentNew.PayMoneyOriginal;
                    paymentNew.MoneyOriginal = paymentNew.PayMoneyOriginal;

                    totalMoneyConsultantAfterRestruct += paymentNew.MoneyConsultant - paymentNew.PayMoneyConsultant;
                    paymentNew.MoneyConsultant = paymentNew.PayMoneyConsultant;

                    paymentNew.CreatedDate = dt;
                    paymentNew.ModifiedDate = dt;
                    paymentNew.PaymentScheduleID = 0;
                    lstPaymentScheduleNeedCollect[i].ModifiedDate = dt;
                    lstPaymentScheduleNeedCollect[i].Status = (int)PaymentSchedule_Status.NoUse;
                    // cập nhật ngày phạt vào cuối tháng của kỳ đó
                    paymentNew.DateApplyFineLate = paymentNew.PayDate.AddDays(reqDebtRestructuring.MinDayFineLate);
                    if (paymentNew.IsVisible == Convert.ToBoolean(PaymentSchedule_IsVisible.Show))
                    {
                        if (paymentNew.DateApplyFineLate < LastDayOfMonthCurrentRestruturing)
                        {
                            paymentNew.DateApplyFineLate = LastDayOfMonthCurrentRestruturing;
                            paymentNew.Fined = (int)PaymentSchedule_Fined.NotYet;
                        }
                        // cập nhật bút toán miễn trừ phí phạt trả chậm nếu có
                        if (paymentNew.MoneyFineLate - paymentNew.PayMoneyFineLate > 0)
                        {
                            var txnDetail = CreateInfoDeleteTransactionMoneyFeeLate(reqDebtRestructuring, paymentNew);
                            lstTransactionInsert.Add(txnDetail);
                            paymentNew.MoneyFineLate = paymentNew.PayMoneyFineLate;
                        }
                    }
                    var keyDictPayment = $"{paymentNew.PayDate:yyyyMMdd}";
                    if (!dictPaymentInsertInfos.ContainsKey(keyDictPayment))
                    {
                        dictPaymentInsertInfos.Add(keyDictPayment, paymentNew);
                    }
                }

                Domain.Tables.TblPaymentSchedule paymentNewLast = lstPaymentScheduleNeedCollect[lstPaymentScheduleNeedCollect.Count - 1].DeepCopyByExpressionTree();

                paymentNewLast.MoneyOriginal += totalMoneyOriginalAfterRestruct;
                paymentNewLast.MoneyConsultant += totalMoneyConsultantAfterRestruct;
                lstPaymentScheduleNeedCollect[lstPaymentScheduleNeedCollect.Count - 1].ModifiedDate = dt;
                lstPaymentScheduleNeedCollect[lstPaymentScheduleNeedCollect.Count - 1].Status = (int)PaymentSchedule_Status.NoUse;
                paymentNewLast.CreatedDate = dt;
                paymentNewLast.ModifiedDate = dt;
                paymentNewLast.PaymentScheduleID = 0;
                var keyDictPaymentLast = $"{paymentNewLast.PayDate:yyyyMMdd}";
                if (!dictPaymentInsertInfos.ContainsKey(keyDictPaymentLast))
                {
                    dictPaymentInsertInfos.Add(keyDictPaymentLast, paymentNewLast);
                }
                else
                {
                    dictPaymentInsertInfos[keyDictPaymentLast].MoneyOriginal += totalMoneyOriginalAfterRestruct;
                    dictPaymentInsertInfos[keyDictPaymentLast].MoneyConsultant += totalMoneyConsultantAfterRestruct;
                }
                if (dictPaymentInsertInfos.Count > 0)
                {
                    // tiền phạt hủy giá trị âm
                    totalMoneyFineLateCashBack = lstTransactionInsert.Sum(x => x.TotalMoney) * -1;
                    _paymentScheduleTab.UpdateBulk(lstPaymentScheduleNeedCollect);
                    _paymentScheduleTab.InsertBulk(dictPaymentInsertInfos.Values.OrderBy(x => x.PayDate).ToList());
                    await ExecuteCreateTransaction(lstTransactionInsert);
                }
                response.SetSucces();
                response.Data = totalMoneyFineLateCashBack;
            }
            catch (Exception ex)
            {
                _logger.LogError($"DebtRestructuringBothOriginalConsultingFeeLastPeriods|request={_common.ConvertObjectToJSonV2(reqDebtRestructuring)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
