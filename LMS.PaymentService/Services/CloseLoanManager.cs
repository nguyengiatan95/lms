﻿using LMS.Common.Constants;
using LMS.PaymentServiceApi.Domain.Tables;
using LMS.PaymentServiceApi.RestClients;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Services
{
    public interface ICloseLoanManager
    {
        Common.Constants.Loan_Status TypeCloseLoan { get; }
        Task<ResponseActionResult> CloseLoan(long loanID, long moneyOriginal, long moneyIntereset, long moneyConsultant,
            long moneyFineLate, long moneyFineOriginal,long moneyService, DateTime closeDate, long createBy, long insurancePartnerCode);
    }

    /// <summary>
    /// Đóng HĐ 
    /// </summary>
    public class TypeCloseLoanFinalization : ICloseLoanManager
    {
        public Loan_Status TypeCloseLoan => Loan_Status.Close;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        ILogger<TypeCloseLoanFinalization> _logger;
        ITransactionService _transactionService;
        ILoanService _loanService;
        Common.Helper.Utils _common;
        ICommonCalculatePayment _commonCalculatePayment;
        public TypeCloseLoanFinalization(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            ITransactionService transactionService,
            ILoanService loanService,
            ICommonCalculatePayment commonCalculatePayment,
            ILogger<TypeCloseLoanFinalization> logger)
        {
            _paymentScheduleTab = paymentScheduleTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _loanService = loanService;
            _transactionService = transactionService;
            _commonCalculatePayment = commonCalculatePayment;
        }
        public async Task<ResponseActionResult> CloseLoan(long loanID, long moneyOriginal, long moneyIntereset, long moneyConsultant, 
            long moneyFineLate,
            long moneyFineOriginal, long moneyService, DateTime closeDate, long createBy, long insurancePartnerCode)
        {
            var response = new ResponseActionResult();

            var loanInfo = await _loanService.GetLoanByID(loanID);
            var currentDate = DateTime.Now;
            var paymentCloseLoanID = _paymentScheduleTab.Insert(new TblPaymentSchedule
            {
                LoanID = loanInfo.LoanID,
                FromDate = loanInfo.LastDateOfPay,
                ToDate = closeDate,
                PayDate = closeDate,
                MoneyConsultant = moneyConsultant,
                MoneyFineInterestLate = 0,
                MoneyFineLate = moneyFineLate,
                MoneyInterest = moneyIntereset,
                MoneyOriginal = moneyOriginal,
                MoneyService = moneyService,
                PayMoneyOriginal = moneyOriginal,
                PayMoneyConsultant = moneyConsultant,
                PayMoneyFineInterestLate = 0,
                PayMoneyFineOriginal = moneyFineOriginal,
                PayMoneyFineLate = moneyFineLate,
                PayMoneyInterest = moneyIntereset,
                PayMoneyService = moneyService,
                CreatedDate = currentDate,
                FirstPaymentDate = closeDate,
                CompletedDate = closeDate,
                ModifiedDate = currentDate,
                Status = (int)PaymentSchedule_Status.Use,
                IsComplete = (int)PaymentSchedule_IsComplete.Paid,
                IsVisible = true
            });
            List<Domain.Models.PaymentTransactionInsertModel> lstTran = new List<Domain.Models.PaymentTransactionInsertModel>();
            if (moneyOriginal > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Original, paymentCloseLoanID, moneyOriginal, createBy, (int)Transaction_Action.DongHd);
            }
            if (moneyIntereset > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Interest, paymentCloseLoanID, moneyIntereset, createBy, (int)Transaction_Action.DongHd);
            }
            if (moneyConsultant > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Consultant, paymentCloseLoanID, moneyConsultant, createBy, (int)Transaction_Action.DongHd);
            }
            if (moneyFineOriginal > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.FineOriginal, paymentCloseLoanID, moneyFineOriginal, createBy, (int)Transaction_Action.DongHd);
            }
            if (moneyService > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Service, paymentCloseLoanID, moneyService, createBy, (int)Transaction_Action.DongHd);
            }
            if (moneyFineLate > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.FineLate, paymentCloseLoanID, moneyFineLate, createBy, (int)Transaction_Action.DongHd);
            }
            if (lstTran.Count > 0)
            {
                _ = await _transactionService.CreateTransactionAfterPayment(lstTran);
            }
            var totalMoneyCustomerPaid = lstTran.Sum(x => x.TotalMoney);
            response.SetSucces();
            response.Data = totalMoneyCustomerPaid;
            return response;
        }
    }

    /// <summary>
    /// thanh lý 
    /// </summary>
    public class TypeCloseLoanLiquidation : ICloseLoanManager
    {
        public Loan_Status TypeCloseLoan => Loan_Status.CloseLiquidation;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        ILogger<TypeCloseLoanLiquidation> _logger;
        ITransactionService _transactionService;
        ILoanService _loanService;
        Common.Helper.Utils _common;
        ICommonCalculatePayment _commonCalculatePayment;
        public TypeCloseLoanLiquidation(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            ITransactionService transactionService,
            ILoanService loanService,
            ICommonCalculatePayment commonCalculatePayment,
            ILogger<TypeCloseLoanLiquidation> logger)
        {
            _paymentScheduleTab = paymentScheduleTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _loanService = loanService;
            _transactionService = transactionService;
            _commonCalculatePayment = commonCalculatePayment;
        }
        public async Task<ResponseActionResult> CloseLoan(long loanID, long moneyOriginal, long moneyIntereset, long moneyConsultant,
            long moneyFineLate,
            long moneyFineOriginal, long moneyService, DateTime closeDate, long createBy, long insurancePartnerCode)
        {
            var response = new ResponseActionResult();

            var loanInfo = await _loanService.GetLoanByID(loanID);
            var currentDate = DateTime.Now;
            var paymentCloseLoanID = _paymentScheduleTab.Insert(new TblPaymentSchedule
            {
                LoanID = loanInfo.LoanID,
                FromDate = loanInfo.LastDateOfPay,
                ToDate = closeDate,
                PayDate = closeDate,
                MoneyConsultant = moneyConsultant,
                MoneyFineInterestLate = 0,
                MoneyFineLate = moneyFineLate,
                MoneyInterest = moneyIntereset,
                MoneyOriginal = moneyOriginal,
                MoneyService = moneyService,
                PayMoneyOriginal = moneyOriginal,
                PayMoneyConsultant = moneyConsultant,
                PayMoneyFineInterestLate = 0,
                PayMoneyFineOriginal = moneyFineOriginal,
                PayMoneyFineLate = moneyFineLate,
                PayMoneyInterest = moneyIntereset,
                PayMoneyService = moneyService,
                CreatedDate = currentDate,
                FirstPaymentDate = closeDate,
                CompletedDate = closeDate,
                ModifiedDate = currentDate,
                Status = (int)PaymentSchedule_Status.Use,
                IsComplete = (int)PaymentSchedule_IsComplete.Paid,
                IsVisible = true
            });
            List<Domain.Models.PaymentTransactionInsertModel> lstTran = new List<Domain.Models.PaymentTransactionInsertModel>();
            if (moneyOriginal > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Original, paymentCloseLoanID, moneyOriginal, createBy, (int)Transaction_Action.ThanhLy);
            }
            if (moneyIntereset > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Interest, paymentCloseLoanID, moneyIntereset, createBy, (int)Transaction_Action.ThanhLy);
            }
            if (moneyConsultant > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Consultant, paymentCloseLoanID, moneyConsultant, createBy, (int)Transaction_Action.ThanhLy);
            }
            if (moneyFineOriginal > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.FineOriginal, paymentCloseLoanID, moneyFineOriginal, createBy, (int)Transaction_Action.ThanhLy);
            }
            if (moneyService > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Service, paymentCloseLoanID, moneyService, createBy, (int)Transaction_Action.ThanhLy);
            }
            if (moneyFineLate > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.FineLate, paymentCloseLoanID, moneyFineLate, createBy, (int)Transaction_Action.ThanhLy);
            }
            if (lstTran.Count > 0)
            {
                _ = await _transactionService.CreateTransactionAfterPayment(lstTran);
            }
            var totalMoneyCustomerPaid = lstTran.Sum(x => x.TotalMoney);
            response.SetSucces();
            response.Data = totalMoneyCustomerPaid;
            return response;
        }
    }

    /// <summary>
    /// thanh lý bảo hiểm
    /// </summary>
    public class TypeCloseLoanLiquidationLoanInsurance : ICloseLoanManager
    {
        public Loan_Status TypeCloseLoan => Loan_Status.CloseLiquidationLoanInsurance;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        ILogger<TypeCloseLoanLiquidation> _logger;
        ITransactionService _transactionService;
        ILoanService _loanService;
        Common.Helper.Utils _common;
        ICommonCalculatePayment _commonCalculatePayment;
        public TypeCloseLoanLiquidationLoanInsurance(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            ITransactionService transactionService,
            ILoanService loanService,
            ICommonCalculatePayment commonCalculatePayment,
            ILogger<TypeCloseLoanLiquidation> logger)
        {
            _paymentScheduleTab = paymentScheduleTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _loanService = loanService;
            _transactionService = transactionService;
            _commonCalculatePayment = commonCalculatePayment;
        }
        public async Task<ResponseActionResult> CloseLoan(long loanID, long moneyOriginal, long moneyIntereset, long moneyConsultant,
            long moneyFineLate,
            long moneyFineOriginal, long moneyService, DateTime closeDate, long createBy, long insurancePartnerCode)
        {
            var response = new ResponseActionResult();

            var loanInfo = await _loanService.GetLoanByID(loanID);
            var currentDate = DateTime.Now;
            var paymentCloseLoanID = _paymentScheduleTab.Insert(new TblPaymentSchedule
            {
                LoanID = loanInfo.LoanID,
                FromDate = loanInfo.LastDateOfPay,
                ToDate = closeDate,
                PayDate = closeDate,
                MoneyConsultant = moneyConsultant,
                MoneyFineInterestLate = 0,
                MoneyFineLate = moneyFineLate,
                MoneyInterest = moneyIntereset,
                MoneyOriginal = moneyOriginal,
                MoneyService = moneyService,
                PayMoneyOriginal = moneyOriginal,
                PayMoneyConsultant = moneyConsultant,
                PayMoneyFineInterestLate = 0,
                PayMoneyFineOriginal = moneyFineOriginal,
                PayMoneyFineLate = moneyFineLate,
                PayMoneyInterest = moneyIntereset,
                PayMoneyService = moneyService,
                CreatedDate = currentDate,
                FirstPaymentDate = closeDate,
                CompletedDate = closeDate,
                ModifiedDate = currentDate,
                Status = (int)PaymentSchedule_Status.Use,
                IsComplete = (int)PaymentSchedule_IsComplete.Paid,
                IsVisible = true
            });
            List<Domain.Models.PaymentTransactionInsertModel> lstTran = new List<Domain.Models.PaymentTransactionInsertModel>();
            if (moneyOriginal > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Original, paymentCloseLoanID, moneyOriginal, createBy, (int)Transaction_Action.ThanhLyBaoHiem);
            }
            if (moneyIntereset > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Interest, paymentCloseLoanID, moneyIntereset, createBy, (int)Transaction_Action.ThanhLyBaoHiem);
            }
            if (moneyConsultant > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Consultant, paymentCloseLoanID, moneyConsultant, createBy, (int)Transaction_Action.ThanhLyBaoHiem);
            }
            if (moneyFineOriginal > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.FineOriginal, paymentCloseLoanID, moneyFineOriginal, createBy, (int)Transaction_Action.ThanhLyBaoHiem);
            }
            if (moneyService > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Service, paymentCloseLoanID, moneyService, createBy, (int)Transaction_Action.ThanhLyBaoHiem);
            }
            if (moneyFineLate > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.FineLate, paymentCloseLoanID, moneyFineLate, createBy, (int)Transaction_Action.ThanhLyBaoHiem);
            }
            if (lstTran.Count > 0)
            {
                _ = await _transactionService.CreateTransactionAfterPayment(lstTran);
            }
            var totalMoneyCustomerPaid = lstTran.Sum(x => x.TotalMoney);
            response.SetSucces();
            response.Data = totalMoneyCustomerPaid;
            return response;
        }
    }

    public class TypeCloseLoanExemption : ICloseLoanManager
    {
        public Loan_Status TypeCloseLoan => Loan_Status.CloseExemption;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        ILogger<TypeCloseLoanExemption> _logger;
        ITransactionService _transactionService;
        ILoanService _loanService;
        Common.Helper.Utils _common;
        ICommonCalculatePayment _commonCalculatePayment;
        public TypeCloseLoanExemption(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            ITransactionService transactionService,
            ILoanService loanService,
            ICommonCalculatePayment commonCalculatePayment,
            ILogger<TypeCloseLoanExemption> logger)
        {
            _paymentScheduleTab = paymentScheduleTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _loanService = loanService;
            _transactionService = transactionService;
            _commonCalculatePayment = commonCalculatePayment;
        }
        public async Task<ResponseActionResult> CloseLoan(long loanID, long moneyOriginal, long moneyIntereset, long moneyConsultant,
            long moneyFineLate,
            long moneyFineOriginal, long moneyService, DateTime closeDate, long createBy, long insurancePartnerCode)
        {
            var response = new ResponseActionResult();

            var loanInfo = await _loanService.GetLoanByID(loanID);
            var currentDate = DateTime.Now;
            var paymentCloseLoanID = _paymentScheduleTab.Insert(new TblPaymentSchedule
            {
                LoanID = loanInfo.LoanID,
                FromDate = loanInfo.LastDateOfPay,
                ToDate = closeDate,
                PayDate = closeDate,
                MoneyConsultant = moneyConsultant,
                MoneyFineInterestLate = 0,
                MoneyFineLate = moneyFineLate,
                MoneyInterest = moneyIntereset,
                MoneyOriginal = moneyOriginal,
                MoneyService = moneyService,
                PayMoneyOriginal = moneyOriginal,
                PayMoneyConsultant = moneyConsultant,
                PayMoneyFineInterestLate = 0,
                PayMoneyFineOriginal = moneyFineOriginal,
                PayMoneyFineLate = moneyFineLate,
                PayMoneyInterest = moneyIntereset,
                PayMoneyService = moneyService,
                CreatedDate = currentDate,
                FirstPaymentDate = closeDate,
                CompletedDate = closeDate,
                ModifiedDate = currentDate,
                Status = (int)PaymentSchedule_Status.Use,
                IsComplete = (int)PaymentSchedule_IsComplete.Paid,
                IsVisible = true
            });
            List<Domain.Models.PaymentTransactionInsertModel> lstTran = new List<Domain.Models.PaymentTransactionInsertModel>();
            if (moneyOriginal > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Original, paymentCloseLoanID, moneyOriginal, createBy, (int)Transaction_Action.Exemption);
            }
            if (moneyIntereset > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Interest, paymentCloseLoanID, moneyIntereset, createBy, (int)Transaction_Action.Exemption);
            }
            if (moneyConsultant > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Consultant, paymentCloseLoanID, moneyConsultant, createBy, (int)Transaction_Action.Exemption);
            }
            if (moneyFineOriginal > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.FineOriginal, paymentCloseLoanID, moneyFineOriginal, createBy, (int)Transaction_Action.Exemption);
            }
            if (moneyService > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Service, paymentCloseLoanID, moneyService, createBy, (int)Transaction_Action.Exemption);
            }
            if (moneyFineLate > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.FineLate, paymentCloseLoanID, moneyFineLate, createBy, (int)Transaction_Action.Exemption);
            }
            if (lstTran.Count > 0)
            {
                _ = await _transactionService.CreateTransactionAfterPayment(lstTran);
            }
            var totalMoneyCustomerPaid = lstTran.Sum(x => x.TotalMoney);
            response.SetSucces();
            response.Data = totalMoneyCustomerPaid;
            return response;
        }
    }

    public class TypeCloseLoanPaidInsuranceExemption : ICloseLoanManager
    {
        public Loan_Status TypeCloseLoan => Loan_Status.ClosePaidInsuranceExemption;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        ILogger<TypeCloseLoanPaidInsuranceExemption> _logger;
        ITransactionService _transactionService;
        ILoanService _loanService;
        Common.Helper.Utils _common;
        ICommonCalculatePayment _commonCalculatePayment;
        public TypeCloseLoanPaidInsuranceExemption(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            ITransactionService transactionService,
            ILoanService loanService,
            ICommonCalculatePayment commonCalculatePayment,
            ILogger<TypeCloseLoanPaidInsuranceExemption> logger)
        {
            _paymentScheduleTab = paymentScheduleTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _loanService = loanService;
            _transactionService = transactionService;
            _commonCalculatePayment = commonCalculatePayment;
        }
        public async Task<ResponseActionResult> CloseLoan(long loanID, long moneyOriginal, long moneyIntereset, long moneyConsultant,
            long moneyFineLate,
            long moneyFineOriginal, long moneyService, DateTime closeDate, long createBy, long insurancePartnerCode)
        {
            var response = new ResponseActionResult();

            var loanInfo = await _loanService.GetLoanByID(loanID);
            var currentDate = DateTime.Now;
            var paymentCloseLoanID = _paymentScheduleTab.Insert(new TblPaymentSchedule
            {
                LoanID = loanInfo.LoanID,
                FromDate = loanInfo.LastDateOfPay,
                ToDate = closeDate,
                PayDate = closeDate,
                MoneyConsultant = moneyConsultant,
                MoneyFineInterestLate = 0,
                MoneyFineLate = moneyFineLate,
                MoneyInterest = moneyIntereset,
                MoneyOriginal = moneyOriginal,
                MoneyService = moneyService,
                PayMoneyOriginal = moneyOriginal,
                PayMoneyConsultant = moneyConsultant,
                PayMoneyFineInterestLate = 0,
                PayMoneyFineOriginal = moneyFineOriginal,
                PayMoneyFineLate = moneyFineLate,
                PayMoneyInterest = moneyIntereset,
                PayMoneyService = moneyService,
                CreatedDate = currentDate,
                FirstPaymentDate = closeDate,
                CompletedDate = closeDate,
                ModifiedDate = currentDate,
                Status = (int)PaymentSchedule_Status.Use,
                IsComplete = (int)PaymentSchedule_IsComplete.Paid,
                IsVisible = true
            });
            List<Domain.Models.PaymentTransactionInsertModel> lstTran = new List<Domain.Models.PaymentTransactionInsertModel>();
            if (moneyOriginal > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Original, paymentCloseLoanID, moneyOriginal, createBy, (int)Transaction_Action.ReceiptInsurance);
            }
            if (moneyIntereset > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Interest, paymentCloseLoanID, moneyIntereset, createBy, (int)Transaction_Action.ReceiptInsurance);
            }
            if (moneyConsultant > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Consultant, paymentCloseLoanID, moneyConsultant, createBy, (int)Transaction_Action.ReceiptInsurance);
            }
            if (moneyFineOriginal > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.FineOriginal, paymentCloseLoanID, moneyFineOriginal, createBy, (int)Transaction_Action.ReceiptInsurance);
            }
            if (moneyService > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.Service, paymentCloseLoanID, moneyService, createBy, (int)Transaction_Action.ReceiptInsurance);
            }
            if (moneyFineLate > 0)
            {
                _commonCalculatePayment.AddTransaction(lstTran, loanID, (int)Transaction_TypeMoney.FineLate, paymentCloseLoanID, moneyFineLate, createBy, (int)Transaction_Action.ReceiptInsurance);
            }
            if (lstTran.Count > 0)
            {
                _ = await _transactionService.CreateTransactionAfterPayment(lstTran);
            }
            var totalMoneyCustomerPaid = lstTran.Sum(x => x.TotalMoney);
            response.SetSucces();
            response.Data = totalMoneyCustomerPaid;
            return response;
        }
    }
}
