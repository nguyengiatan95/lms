﻿using LMS.Common.Constants;
using LMS.PaymentServiceApi.Domain.Models;
using LMS.PaymentServiceApi.Domain.Tables;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Services
{
    public interface IPayPaymentManager
    {
        //IEnumerable<Common.Constants.Transaction_TypeMoney> GetAllTypeMoney();
        Common.Constants.Transaction_TypeMoney TypeMoney { get; }
        IEnumerable<PaymentTransactionInsertModel> PayMoney(List<PaymentTransactionInsertModel> lstTransaction, long loanID, TblPaymentSchedule objPayment, ref long totalMoney, long createBy, int actionID = (int)Transaction_Action.DongLai);
    }

    // tiền gốc
    public class PayOriginalManager : IPayPaymentManager
    {
        ILogger<PayOriginalManager> _logger;
        ICommonCalculatePayment _commonCalculatePayment;
        public PayOriginalManager(ILogger<PayOriginalManager> logger, ICommonCalculatePayment commonCalculatePayment)
        {
            _logger = logger;
            _commonCalculatePayment = commonCalculatePayment;
        }

        public Transaction_TypeMoney TypeMoney => Transaction_TypeMoney.Original;

        public IEnumerable<PaymentTransactionInsertModel> PayMoney(List<PaymentTransactionInsertModel> lstTransaction, long loanID, TblPaymentSchedule objPayment, ref long totalMoney, long createBy, int actionID = (int)Transaction_Action.DongLai)
        {
            long totalMoneyNeedPay = objPayment.MoneyOriginal - objPayment.PayMoneyOriginal;
            if (totalMoney <= 0 || totalMoneyNeedPay <= 0)
            {
                return lstTransaction;
            }
            long remainCustomerPay = 0;
            long paidMoney = 0;
            // tinh tien
            _commonCalculatePayment.CalculateMoney(totalMoney, totalMoneyNeedPay, ref remainCustomerPay, ref paidMoney);
            totalMoney = remainCustomerPay;
            objPayment.PayMoneyOriginal += paidMoney;
            //// đúng case dư nợ giảm dần
            //// case tất toán cuối kỳ bị thiếu
            //if (!objPayment.FirstPaymentDate.HasValue)
            //{
            //    objPayment.FirstPaymentDate = DateTime.Now;
            //}
            lstTransaction = _commonCalculatePayment.AddTransaction(lstTransaction, loanID, (int)TypeMoney, objPayment.PaymentScheduleID, paidMoney, createBy, actionID);
            return lstTransaction;
        }
    }

    // tiền lãi
    public class PayInterestManager : IPayPaymentManager
    {
        ILogger<PayInterestManager> _logger;
        ICommonCalculatePayment _commonCalculatePayment;
        public PayInterestManager(ILogger<PayInterestManager> logger, ICommonCalculatePayment commonCalculatePayment)
        {
            _logger = logger;
            _commonCalculatePayment = commonCalculatePayment;
        }

        public Transaction_TypeMoney TypeMoney => Transaction_TypeMoney.Interest;

        public IEnumerable<PaymentTransactionInsertModel> PayMoney(List<PaymentTransactionInsertModel> lstTransaction, long loanID, TblPaymentSchedule objPayment, ref long totalMoney, long createBy, int actionID = (int)Transaction_Action.DongLai)
        {
            long totalMoneyNeedPay = objPayment.MoneyInterest - objPayment.PayMoneyInterest;
            if (totalMoney <= 0 || totalMoneyNeedPay <= 0)
            {
                return lstTransaction;
            }
            long remainCustomerPay = 0;
            long paidMoney = 0;
            // tinh tien
            _commonCalculatePayment.CalculateMoney(totalMoney, totalMoneyNeedPay, ref remainCustomerPay, ref paidMoney);
            totalMoney = remainCustomerPay;
            objPayment.PayMoneyInterest += paidMoney;
            lstTransaction = _commonCalculatePayment.AddTransaction(lstTransaction, loanID, (int)TypeMoney, objPayment.PaymentScheduleID, paidMoney, createBy, actionID);
            return lstTransaction;
        }
    }

    public class PayServiceManager : IPayPaymentManager
    {
        ILogger<PayServiceManager> _logger;
        ICommonCalculatePayment _commonCalculatePayment;
        public PayServiceManager(ILogger<PayServiceManager> logger, ICommonCalculatePayment commonCalculatePayment)
        {
            _logger = logger;
            _commonCalculatePayment = commonCalculatePayment;
        }

        public Transaction_TypeMoney TypeMoney => Transaction_TypeMoney.Service;

        public IEnumerable<PaymentTransactionInsertModel> PayMoney(List<PaymentTransactionInsertModel> lstTransaction, long loanID, TblPaymentSchedule objPayment, ref long totalMoney, long createBy, int actionID = (int)Transaction_Action.DongLai)
        {

            long totalMoneyNeedPay = objPayment.MoneyService - objPayment.PayMoneyService;
            if (totalMoney <= 0 || totalMoneyNeedPay <= 0)
            {
                return lstTransaction;
            }
            long remainCustomerPay = 0;
            long paidMoney = 0;
            // tinh tien
            _commonCalculatePayment.CalculateMoney(totalMoney, totalMoneyNeedPay, ref remainCustomerPay, ref paidMoney);
            totalMoney = remainCustomerPay;
            objPayment.PayMoneyService += paidMoney;
            lstTransaction = _commonCalculatePayment.AddTransaction(lstTransaction, loanID, (int)TypeMoney, objPayment.PaymentScheduleID, paidMoney, createBy, actionID);
            return lstTransaction;

        }
    }

    public class PayConsultantManager : IPayPaymentManager
    {
        ILogger<PayConsultantManager> _logger;
        ICommonCalculatePayment _commonCalculatePayment;
        public PayConsultantManager(ILogger<PayConsultantManager> logger, ICommonCalculatePayment commonCalculatePayment)
        {
            _logger = logger;
            _commonCalculatePayment = commonCalculatePayment;
        }

        public Transaction_TypeMoney TypeMoney => Transaction_TypeMoney.Consultant;

        public IEnumerable<PaymentTransactionInsertModel> PayMoney(List<PaymentTransactionInsertModel> lstTransaction, long loanID, TblPaymentSchedule objPayment, ref long totalMoney, long createBy, int actionID = (int)Transaction_Action.DongLai)
        {
            long totalMoneyNeedPay = objPayment.MoneyConsultant - objPayment.PayMoneyConsultant;
            if (totalMoney <= 0 || totalMoneyNeedPay <= 0)
            {
                return lstTransaction;
            }
            long remainCustomerPay = 0;
            long paidMoney = 0;
            // tinh tien
            _commonCalculatePayment.CalculateMoney(totalMoney, totalMoneyNeedPay, ref remainCustomerPay, ref paidMoney);
            totalMoney = remainCustomerPay;
            objPayment.PayMoneyConsultant += paidMoney;
            lstTransaction = _commonCalculatePayment.AddTransaction(lstTransaction, loanID, (int)TypeMoney, objPayment.PaymentScheduleID, paidMoney, createBy, actionID);
            return lstTransaction;
        }
    }

    public class PayMoneyFineInterestLateManager : IPayPaymentManager
    {
        ILogger<PayMoneyFineInterestLateManager> _logger;
        ICommonCalculatePayment _commonCalculatePayment;
        public PayMoneyFineInterestLateManager(ILogger<PayMoneyFineInterestLateManager> logger, ICommonCalculatePayment commonCalculatePayment)
        {
            _logger = logger;
            _commonCalculatePayment = commonCalculatePayment;
        }

        public Transaction_TypeMoney TypeMoney => Transaction_TypeMoney.FineInterestLate;

        public IEnumerable<PaymentTransactionInsertModel> PayMoney(List<PaymentTransactionInsertModel> lstTransaction, long loanID, TblPaymentSchedule objPayment, ref long totalMoney, long createBy, int actionID = (int)Transaction_Action.DongLai)
        {
        
            long totalMoneyNeedPay = objPayment.MoneyFineInterestLate - objPayment.PayMoneyFineInterestLate;
            if (totalMoney <= 0 || totalMoneyNeedPay <= 0)
            {
                return lstTransaction;
            }
            long remainCustomerPay = 0;
            long paidMoney = 0;
            // tinh tien
            _commonCalculatePayment.CalculateMoney(totalMoney, totalMoneyNeedPay, ref remainCustomerPay, ref paidMoney);
            totalMoney = remainCustomerPay;
            objPayment.PayMoneyFineInterestLate += paidMoney;
            lstTransaction = _commonCalculatePayment.AddTransaction(lstTransaction, loanID, (int)TypeMoney, objPayment.PaymentScheduleID, paidMoney, createBy, actionID);
            return lstTransaction;
        }
    }

    public class PayMoneyFineLateManager : IPayPaymentManager
    {
        ILogger<PayMoneyFineLateManager> _logger;
        ICommonCalculatePayment _commonCalculatePayment;
        public PayMoneyFineLateManager(ILogger<PayMoneyFineLateManager> logger, ICommonCalculatePayment commonCalculatePayment)
        {
            _logger = logger;
            _commonCalculatePayment = commonCalculatePayment;
        }

        public Transaction_TypeMoney TypeMoney => Transaction_TypeMoney.FineLate;

        public IEnumerable<PaymentTransactionInsertModel> PayMoney(List<PaymentTransactionInsertModel> lstTransaction, long loanID, TblPaymentSchedule objPayment, ref long totalMoney, long createBy, int actionID = (int)Transaction_Action.DongLai)
        {
            long totalMoneyNeedPay = objPayment.MoneyFineLate - objPayment.PayMoneyFineLate;
            if (totalMoney <= 0 || totalMoneyNeedPay <= 0)
            {
                return lstTransaction;
            }
            long remainCustomerPay = 0;
            long paidMoney = 0;
            // tinh tien
            _commonCalculatePayment.CalculateMoney(totalMoney, totalMoneyNeedPay, ref remainCustomerPay, ref paidMoney);
            totalMoney = remainCustomerPay;
            objPayment.PayMoneyFineLate += paidMoney;
            lstTransaction = _commonCalculatePayment.AddTransaction(lstTransaction, loanID, (int)TypeMoney, objPayment.PaymentScheduleID, paidMoney, createBy, actionID);
            return lstTransaction;
        }
    }

    public interface ICommonCalculatePayment
    {
        void CalculateMoney(long totalMoney, long totalMoneyNeedPay, ref long remainCustomerPay, ref long paidMoney);
        List<PaymentTransactionInsertModel> AddTransaction(List<PaymentTransactionInsertModel> lstTransaction, long loanID, long typeMoney, long paymentScheduleID, long paidMoney, long createBy, int actionID = (int)Transaction_Action.DongLai);
    }
    public class CommonCalculatePayment : ICommonCalculatePayment
    {
        public void CalculateMoney(long totalMoney, long totalMoneyNeedPay, ref long remainCustomerPay, ref long paidMoney)
        {
            //if (totalMoneyNeedPay >= totalMoney)
            //{
            //    paidMoney += totalMoney;
            //    remainCustomerPay = 0;
            //}
            //else if (totalMoneyNeedPay < totalMoney)
            //{
            //    paidMoney += totalMoneyNeedPay;
            //    remainCustomerPay = totalMoney - totalMoneyNeedPay;
            //}
            paidMoney = totalMoneyNeedPay > totalMoney ? totalMoney : totalMoneyNeedPay;
            remainCustomerPay = totalMoney - paidMoney;
        }
        public List<PaymentTransactionInsertModel> AddTransaction(List<PaymentTransactionInsertModel> lstTransaction, long loanID, long typeMoney, long paymentScheduleID, long paidMoney, long createBy, int actionID = (int)Transaction_Action.DongLai)
        {
            lstTransaction.Add(new PaymentTransactionInsertModel()
            {
                ActionID = actionID,
                LoanID = loanID,
                MoneyType = (int)typeMoney,
                PaymentScheduleID = paymentScheduleID,
                TotalMoney = paidMoney,
                UserID = createBy

            });
            return lstTransaction;
        }
    }
}
