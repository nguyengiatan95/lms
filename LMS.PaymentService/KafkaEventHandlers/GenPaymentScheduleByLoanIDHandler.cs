﻿using LMS.Common.Constants;
using LMS.Kafka.Interfaces;
using LMS.Kafka.Messages.Customer;
using LMS.Kafka.Messages.Payment;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.KafkaEventHandlers
{
    public class GenPaymentScheduleByLoanIDHandler : IKafkaHandler<string, LMS.Kafka.Messages.Loan.LoanInsertSuccess>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblKafkaLogEvent> _kakfaLogEventTab;
        private readonly IMediator _bus;
        LMS.Common.Helper.Utils _common = new Common.Helper.Utils();
        ILogger<GenPaymentScheduleByLoanIDHandler> _logger;
        public GenPaymentScheduleByLoanIDHandler(IMediator bus,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblKafkaLogEvent> kakfaLogEventTab,
            ILogger<GenPaymentScheduleByLoanIDHandler> logger)
        {
            _bus = bus;
            _loanTab = loanTab;
            _kakfaLogEventTab = kakfaLogEventTab;
            _logger = logger;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> HandleAsync(string key, LMS.Kafka.Messages.Loan.LoanInsertSuccess value)
        {
            LMS.Common.Constants.ResponseActionResult response = new ResponseActionResult();
            try
            {
                var loanInfos = await _loanTab.WhereClause(x => x.LoanID == value.LoanID).QueryAsync();
                if (loanInfos == null || !loanInfos.Any())
                {
                    _logger.LogError($"GenPaymentScheduleByLoanIDHandler|value={_common.ConvertObjectToJSonV2(value)}|Not_found_loan");
                    return response;
                }
                var loanInfo = loanInfos.First();
                DateTime? nextDateTopUp = null;
                // 29-08-2021 remove check topup
                //27 - 10 - 2021 mở lại topup
                //if (loanDetailLos.TopUpOfLoanBriefID > 0 && loanDetailLos.RateTypeId == (int)PaymentSchedule_ActionRateType.RateAmortization30DaysTopUp) // kiểm tra điều kiện của đơn Topup
                //{
                //    // lấy đơn cũ
                //    var loanInfoTopUp = _loanTab.SetGetTop(1).WhereClause(x => x.LoanCreditIDOfPartner == loanDetailLos.TopUpOfLoanBriefID).Query().FirstOrDefault();
                //    if (loanInfoTopUp != null && loanInfoTopUp.LoanID > 0)
                //    {
                //        nextDateTopUp = loanInfoTopUp.NextDate;
                //        //loanInfo.RateType = (int)PaymentSchedule_ActionRateType.RateAmortization30DaysTopUp;
                //    }
                //}
                if (loanInfo.RateType == (int)PaymentSchedule_ActionRateType.RateAmortization30DaysTopUp)
                {
                    var loanExtra = _common.ConvertJSonToObjectV2<Domain.Tables.LoanJsonExtra>(loanInfo.JsonExtra);
                    if (loanExtra != null && loanExtra.TopUpOfLoanBriefID > 0)
                    {
                        var loanInfoTopUp = (await _loanTab.SetGetTop(1).WhereClause(x => x.LoanCreditIDOfPartner == loanExtra.TopUpOfLoanBriefID).QueryAsync()).FirstOrDefault();
                        if (loanInfoTopUp != null && loanInfoTopUp.LoanID > 0)
                        {
                            nextDateTopUp = loanInfoTopUp.NextDate;
                        }
                    }
                }
                Commands.CreatePaymentScheduleByLoanIDCommand request = new Commands.CreatePaymentScheduleByLoanIDCommand
                {
                    TotalMoneyDisbursement = loanInfo.TotalMoneyDisbursement,
                    InterestStartDate = loanInfo.FromDate.ToString(TimaSettingConstant.DateTimeDayMonthYear),
                    LoanTime = loanInfo.LoanTime * TimaSettingConstant.NumberDayInMonth,
                    LoanFrequency = loanInfo.Frequency * TimaSettingConstant.NumberDayInMonth,
                    RateType = loanInfo.RateType,
                    RateConsultant = loanInfo.RateConsultant,
                    RateInterest = loanInfo.RateInterest,
                    RateService = loanInfo.RateService,
                    LoanID = loanInfo.LoanID,
                    DayMustPay = nextDateTopUp?.ToString(TimaSettingConstant.DateTimeDayMonthYear) ?? "",
                    IsKafka = true
                };
                response = await _bus.Send(request);
                if (response.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                {
                    _ = await _kakfaLogEventTab.InsertAsync(new Domain.Tables.TblKafkaLogEvent
                    {
                        CreateDate = DateTime.Now,
                        ModifyDate = DateTime.Now,
                        DataMessage = _common.ConvertObjectToJSonV2(new UpdateNextDateLoanInfo
                        {
                            LoanID = value.LoanID
                        }),
                        ServiceName = ServiceHostInternal.PaymentService,
                        Status = (int)KafkaLogEvent_Status.Waitting,
                        TopicName = LMS.Kafka.Constants.KafkaTopics.PaymentUpdateNextDateLoanInfo
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GenPaymentScheduleByLoanIDHandler|value={_common.ConvertObjectToJSonV2(value)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
