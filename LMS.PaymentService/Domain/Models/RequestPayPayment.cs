﻿using System;
using System.Collections.Generic;

namespace LMS.PaymentServiceApi.Domain.Models
{
    public class RequestPayPayment
    {
        public long LoanID { get; set; }
        public string NextDate { get; set; } // format: dd/MM/yyyy
        public long TotalMoney { get; set; }
        public List<long> LstMoneyType { get; set; }
        public long CreateBy { get; set; }

        public bool CheckLastPeriods { get; set; } // true: kỳ cuối đơn qua hạn không thanh toán, tự động sẽ set = true
    }
}
