﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Domain.Models
{
    public class DebtRestructuringModel
    {
        public long LoanID { get; set; }
        public int LoanRateType { get; set; }
        public int NumberOfRepaymentPeriod { get; set; }
        public string AppointmentDate { get; set; }
        public int PercentDiscount { get; set; }
        public long CreateBy { get; set; }
        public int MinDayFineLate { get; set; }
    }
}
