﻿using System.Collections.Generic;

namespace LMS.PaymentServiceApi.Domain.Models
{
    public class RequestGetMoneyNeedPay
    {
        public List<long> LstLoan { get; set; }
    }
}
