﻿using System;

namespace LMS.PaymentServiceApi.Domain.Models
{
    public class UpdateLoanAfterPaymentModel
    {
        public long LoanID { get; set; }
        public DateTime? NextDate { get; set; }
        public DateTime? LastDateOfPay { get; set; }
        public long OriginalMoney { get; set; }
    }
}
