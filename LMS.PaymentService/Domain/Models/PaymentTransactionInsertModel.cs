﻿namespace LMS.PaymentServiceApi.Domain.Models
{
    public class PaymentTransactionInsertModel
    {
        public long LoanID { get; set; }
        public long TotalMoney { get; set; }
        public int ActionID { get; set; }
        public int MoneyType { get; set; }
        public long PaymentScheduleID { get; set; }
        public long UserID { get; set; }
    }
}
