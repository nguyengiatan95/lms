﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Domain.Tables
{

    [Dapper.Contrib.Extensions.Table("tblPaymentSchedule")]
    public class TblPaymentSchedule
    {
        [Dapper.Contrib.Extensions.Key]
        public long PaymentScheduleID { get; set; }

        public long LoanID { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public DateTime PayDate { get; set; }

        public long MoneyOriginal { get; set; }

        public long MoneyInterest { get; set; }

        public long MoneyService { get; set; }

        public long MoneyConsultant { get; set; }

        public long MoneyFineLate { get; set; }

        public long MoneyFineInterestLate { get; set; }

        public long PayMoneyOriginal { get; set; }

        public long PayMoneyInterest { get; set; }

        public long PayMoneyService { get; set; }

        public long PayMoneyConsultant { get; set; }

        public long PayMoneyFineLate { get; set; }

        public long PayMoneyFineOriginal { get; set; }

        public long PayMoneyFineInterestLate { get; set; }

        public int IsComplete { get; set; }

        public bool IsVisible { get; set; }

        public DateTime ModifiedDate { get; set; }

        public DateTime? FirstPaymentDate { get; set; }

        public DateTime? CompletedDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public int Status { get; set; }

        /// <summary>
        /// 0: chưa phạt, 1 đã phạt
        /// </summary>
        public int Fined { get; set; }
        /// <summary>
        /// ngày phạt sẽ dc áp dụng theo từng chính sách
        /// </summary>
        public DateTime? DateApplyFineLate { get; set; }


    }

    [Dapper.Contrib.Extensions.Table("tblPaymentSchedule")]
    public class PaymentScheduleUpdateMoneyFine
    {
        [Dapper.Contrib.Extensions.Key]
        public long PaymentScheduleID { get; set; }
        public DateTime ModifiedDate { get; set; }
        public long MoneyFineLate { get; set; }
        public long PayMoneyFineLate { get; set; }
        public int IsComplete { get; set; }
        public bool IsVisible { get; set; }
        public DateTime? CompletedDate { get; set; }
        /// <summary>
        /// 0: chưa phạt, 1 đã phạt
        /// </summary>
        public int Fined { get; set; }
    }
}
