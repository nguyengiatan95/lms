﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Queries
{
    public class GetMoneyNeedPayForLoanQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public List<long> LstLoan { get; set; }
    }

    public class GetMoneyNeedPayForLoanQueryHandler : IRequestHandler<GetMoneyNeedPayForLoanQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        readonly ILogger<GetMoneyNeedPayForLoanQueryHandler> _logger;
        readonly Common.Helper.Utils common;
        public GetMoneyNeedPayForLoanQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> loanTab, ILogger<GetMoneyNeedPayForLoanQueryHandler> logger)
        {
            _paymentScheduleTab = loanTab.SetConnectString(LMS.Common.Configuration.ConnectStringSetting.DefaultConnectString);
            _logger = logger;
            common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetMoneyNeedPayForLoanQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();

            DateTime dtNow = DateTime.Now.Date;
            try
            {
                var lstPaymentLoan = await _paymentScheduleTab.OrderByDescending(x => x.PayDate)
                                                        .WhereClause(x => request.LstLoan.Contains(x.LoanID) && x.IsComplete != 1 && x.PayDate <= dtNow)
                                                        .QueryAsync();
                var dicLoan = new Dictionary<long, Entites.Dtos.PaymentServices.ListPaymentMoneyByLoan>();
                foreach (var item in lstPaymentLoan)
                {
                    long originalNeed = 0;
                    long interestNeed = 0;
                    long serviceNeed = 0;
                    long fineLateNeed = 0;
                    long fineInterestNeed = 0;
                    long consultantNeed = 0;
                    originalNeed = item.MoneyOriginal - item.PayMoneyOriginal;
                    interestNeed = item.MoneyInterest - item.PayMoneyInterest;
                    serviceNeed = item.MoneyService - item.PayMoneyService;
                    fineLateNeed = item.MoneyFineLate - item.PayMoneyFineLate;
                    fineInterestNeed = item.MoneyFineInterestLate - item.PayMoneyFineInterestLate;
                    consultantNeed = item.MoneyConsultant - item.PayMoneyConsultant;

                    var objPayment = new Entites.Dtos.PaymentServices.ListPaymentMoneyByLoan()
                    {
                        LoanID = item.LoanID,
                        PayDate = item.PayDate,
                        TotalMoneyNeed = consultantNeed + fineInterestNeed + fineLateNeed + interestNeed + originalNeed + serviceNeed,
                        ConsultantNeed = consultantNeed,
                        FineInterestNeed = fineInterestNeed,
                        FineLateNeed = fineLateNeed,
                        InterestNeed = interestNeed,
                        OriginalNeed = originalNeed,
                        ServiceNeed = serviceNeed
                    };
                    if (!dicLoan.ContainsKey(item.LoanID))
                    {
                        dicLoan.Add(item.LoanID, objPayment);
                    }
                    else if (item.PayDate < dicLoan[item.LoanID].PayDate)
                    {
                        dicLoan[item.LoanID] = objPayment;
                    }
                }
                response.SetSucces();
                response.Data = dicLoan.Values.ToList();
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogWarning($"GetMoneyNeedPayForLoanQueryHandler|request={common.ConvertObjectToJSonV2<GetMoneyNeedPayForLoanQuery>(request)}|ex={ex.Message}-{ex.StackTrace}");
                return response;
            }
        }
    }

}
