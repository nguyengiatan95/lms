﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Queries
{
    public class GetLstPaymentScheduleByTimaLoanIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long TimaLoanID { get; set; }
    }

    public class GetLstPaymentScheduleByTimaLoanIDQueryHandler : IRequestHandler<GetLstPaymentScheduleByTimaLoanIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly ILogger<GetLstPaymentScheduleByTimaLoanIDQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public GetLstPaymentScheduleByTimaLoanIDQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentSchedule,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            ILogger<GetLstPaymentScheduleByTimaLoanIDQueryHandler> logger)
        {
            _paymentScheduleTab = paymentSchedule.SetConnectString(LMS.Common.Configuration.ConnectStringSetting.THNConnectString);
            _loanTab = loanTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetLstPaymentScheduleByTimaLoanIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objLoan = (await _loanTab.WhereClause(x => x.TimaLoanID == request.TimaLoanID).QueryAsync()).FirstOrDefault();
                if (objLoan == null)
                {
                    response.Message = MessageConstant.ParameterInvalid;
                    return response;
                }
                var lstPaymentScheduleInfos = (await _paymentScheduleTab.WhereClause(x => x.LoanID == objLoan.LoanID && x.Status == (int)PaymentSchedule_Status.Use).QueryAsync()).ToList();
                if (lstPaymentScheduleInfos != null && lstPaymentScheduleInfos.Count > 0)
                {
                    var lstDataResposne = _common.ConvertParentToChild<List<LMS.Entites.Dtos.PaymentServices.PaymentScheduleModel>, List<Domain.Tables.TblPaymentSchedule>>(lstPaymentScheduleInfos);
                    response.SetSucces();
                    response.Data = lstDataResposne;
                }
            }
            catch (System.Exception ex)
            {
                _logger.LogError($"GetLstPaymentScheduleByTimaLoanIDQueryHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;

        }
    }
}
