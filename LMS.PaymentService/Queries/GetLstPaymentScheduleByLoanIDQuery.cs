﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Queries
{
    public class GetLstPaymentScheduleByLoanIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
    }

    public class GetLstPaymentScheduleByLoanIDQueryHandler : IRequestHandler<GetLstPaymentScheduleByLoanIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        readonly ILogger<GetLstPaymentScheduleByLoanIDQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public GetLstPaymentScheduleByLoanIDQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> loanTab, ILogger<GetLstPaymentScheduleByLoanIDQueryHandler> logger)
        {
            _paymentScheduleTab = loanTab.SetConnectString(LMS.Common.Configuration.ConnectStringSetting.THNConnectString);
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetLstPaymentScheduleByLoanIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstPaymentScheduleInfos = (await _paymentScheduleTab.WhereClause(x => x.LoanID == request.LoanID && x.Status == (int)PaymentSchedule_Status.Use).QueryAsync()).ToList();
                if (lstPaymentScheduleInfos != null && lstPaymentScheduleInfos.Count > 0)
                {
                    var lstDataResposne = _common.ConvertParentToChild<List<LMS.Entites.Dtos.PaymentServices.PaymentScheduleModel>, List<Domain.Tables.TblPaymentSchedule>>(lstPaymentScheduleInfos);
                    response.SetSucces();
                    response.Data = lstDataResposne;
                }
            }
            catch (System.Exception ex)
            {
                _logger.LogError($"GetLstPaymentScheduleByLoanIDQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
