﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.PaymentServices;
using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Queries
{
    public class AppGenPaymentScheduleQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long TotalMoneyDisbursement { get; set; }
        public int LoanTime { get; set; }
        public int RateType { get; set; }
    }
    public class AppGenPaymentScheduleQueryHandler : IRequestHandler<AppGenPaymentScheduleQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly IEnumerable<Services.IGenPaymentScheduleManager> _actionManagers;
        readonly Common.Helper.Utils _common;
        public AppGenPaymentScheduleQueryHandler(IEnumerable<Services.IGenPaymentScheduleManager> actionManagers)
        {
            _actionManagers = actionManagers;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(AppGenPaymentScheduleQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult respone = new ResponseActionResult();

            var loanTime = _common.ConvertLoanTimeLOSToLMS(request.LoanTime);
            var loanFrequency = _common.ConvertFrequencyLOSToLMS(ConstantsPayment.Frequency);
            var rateConsultant = ConstantsPayment.RateInterest - ConstantsPayment.RateInterestLender;
            var rateInterest = ConstantsPayment.RateInterestLender;
            var rateService = 0;
            var interestStartDate = DateTime.Now;

            var actionDetail = _actionManagers.FirstOrDefault(x => (int)x.RateType == request.RateType);
            if (actionDetail != null)
            {
                respone.SetSucces();
                respone.Data = actionDetail.GetListPaymentSchedules(request.TotalMoneyDisbursement, interestStartDate,
                    loanTime, loanFrequency, request.RateType, rateConsultant, rateInterest,
                    rateService);
            }
            await Task.Delay(1);
            return respone;
        }
    }
}
