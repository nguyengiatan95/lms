﻿using LMS.Common.Constants;
using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Queries
{
    public class GenPaymentScheduleQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public long TotalMoneyDisbursement { get; set; }
        public DateTime InterestStartDate { get; set; }
        [Required]
        public int LoanTime { get; set; }
        [Required]
        public int LoanFrequency { get; set; }
        [Required]
        public int RateType { get; set; }
        [Required]
        public decimal RateConsultant { get; set; } // số tiền lãi dc tính trên tiền triệu của 1 ngày
        [Required]
        public decimal RateInterest { get; set; } // số tiền lãi dc tính trên tiền triệu của 1 ngày
        [Required]
        public decimal RateService { get; set; } // số tiền lãi dc tính trên tiền triệu của 1 ngày
        public long LoanID { get; set; }
        public DateTime? DayMustPay { get; set; }
    }
    public class GenPaymentScheduleQueryHandler : IRequestHandler<GenPaymentScheduleQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly IEnumerable<Services.IGenPaymentScheduleManager> _actionManagers;
        public GenPaymentScheduleQueryHandler(IEnumerable<Services.IGenPaymentScheduleManager> actionManagers)
        {
            _actionManagers = actionManagers;
        }
        public async Task<ResponseActionResult> Handle(GenPaymentScheduleQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult respone = new ResponseActionResult();
            var actionDetail = _actionManagers.FirstOrDefault(x => (int)x.RateType == request.RateType);
            if (actionDetail != null)
            {
                respone.SetSucces();
                respone.Data = actionDetail.GetListPaymentSchedules(request.TotalMoneyDisbursement, request.InterestStartDate, request.LoanTime, request.LoanFrequency, request.RateType, request.RateConsultant, request.RateInterest, request.RateService, request.LoanID, request.DayMustPay);
            }
            await Task.Delay(1);
            return respone;
        }
    }
}
