﻿using AutoMapper;
using LMS.PaymentServiceApi.Domain.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Steeltoe.Common.Discovery;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentScheduleController : ControllerBase
    {
        private readonly IMediator bus;
        public PaymentScheduleController(IMediator bus)
        {
            this.bus = bus;
        }

        [HttpGet]
        [Route("GetLstPaymentByLoanID/{loanID}")]
        public async Task<ActionResult> GetLstPaymentByLoanID(int loanID)
        {
            var result = await bus.Send(new Queries.GetLstPaymentScheduleByLoanIDQuery { LoanID = loanID });
            return new JsonResult(result);
        }
        [HttpGet]
        [Route("GetLstPaymentByTimaLoanID/{timaLoanID}")]
        public async Task<ActionResult> GetLstPaymentByLoanID(long timaLoanID)
        {
            var result = await bus.Send(new Queries.GetLstPaymentScheduleByTimaLoanIDQuery { TimaLoanID = timaLoanID });
            return new JsonResult(result);
        }

        [HttpPost]
        [Route("GenPaymentScheduleByCondition")]
        public async Task<Common.Constants.ResponseActionResult> GenPaymentScheduleByCondition(Queries.GenPaymentScheduleQuery request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("PayPaymentByLoanID")]
        public async Task<Common.Constants.ResponseActionResult> PayPaymentByLoanID(Domain.Models.RequestPayPayment request)
        {
            var result = await bus.Send(new Commands.PayPaymentCommand
            {
                Model = request
            });
            return result;
        }

        [HttpPost]
        [Route("CreatePaymentScheduleByLoanID")]
        public async Task<Common.Constants.ResponseActionResult> CreatePaymentScheduleByLoanID(Commands.CreatePaymentScheduleByLoanIDCommand request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("GetMoneyNeedPayForLoan")]
        public async Task<Common.Constants.ResponseActionResult> GetLstPaymentScheduleByLoanID(RequestGetMoneyNeedPay requestGetMoneyNeedPay)
        {

            var result = await bus.Send(new Queries.GetMoneyNeedPayForLoanQuery
            {
                LstLoan = requestGetMoneyNeedPay.LstLoan
            });
            return result;
        }

        [HttpPost]
        [Route("PayPartialScheduleInPeriodsByLoanID")]
        public async Task<Common.Constants.ResponseActionResult> PayPartialScheduleInPeriodsByLoanID(Commands.PayPartialScheduleInPeriodsByLoanIDCommand request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("CloseLoanFromSchedule")]
        public async Task<Common.Constants.ResponseActionResult> CloseLoanFromSchedule(Commands.CloseLoanFromScheduleCommand request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("UpdateStatusPaymentScheduleByLoan")]
        public async Task<Common.Constants.ResponseActionResult> UpdateStatusPaymentScheduleByLoan(Commands.UpdateStatusPaymentScheduleByLoanCommand request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("CuttingOffPaymentLoanInsuranceByLoanID")]
        public async Task<Common.Constants.ResponseActionResult> CuttingOffPaymentLoanInsuranceByLoanID(Commands.CuttingOffPaymentLoanInsuranceByLoanIDCommand request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("ForceCloseLoanByLoan")]
        public async Task<Common.Constants.ResponseActionResult> ForceCloseLoanByLoan(Commands.CloseLoanCommand request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("ExtendPaymentScheduleByLoanID")]
        public async Task<Common.Constants.ResponseActionResult> ExtendPaymentScheduleByLoanID(Commands.ExtendPaymentScheduleByLoanIDCommand request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("WriteLstMoneyFine")]
        public async Task<Common.Constants.ResponseActionResult> WriteLstMoneyFine(List<LMS.Entites.Dtos.PaymentServices.RequestPaymentChangeMoneyFineModel> request)
        {
            return await bus.Send(new Commands.WriteLstMoneyFineCommand { Models = request });
        }

        [HttpPost]
        [Route("DebtRestructuring")]
        public async Task<Common.Constants.ResponseActionResult> DebtRestructuring(Commands.DebtResctructuringCommand request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("CancelMoneyFineByLoanID")]
        public async Task<Common.Constants.ResponseActionResult> CancelMoneyFineByLoan(Commands.CancelMoneyFineByLoanIDCommand request)
        {
            return await bus.Send(request);
        }
        [HttpPost]
        [Route("DeleteMoneyFineLateByLoanID")]
        public async Task<Common.Constants.ResponseActionResult> DeleteMoneyFineLateByLoanID(Commands.DeleteMoneyFineLateByLoanIDCommand request)
        {
            return await bus.Send(request);
        }
        [HttpPost]
        [Route("GenDebtRestructuring")]
        public async Task<Common.Constants.ResponseActionResult> GenDebtRestructuring(Queries.GenDebtResctructuringQuery request)
        {
            return await bus.Send(request);
        }
        [HttpPost]
        [Route("AppGenPaymentSchedule")]
        public async Task<Common.Constants.ResponseActionResult> AppGenPaymentSchedule(Queries.AppGenPaymentScheduleQuery request)
        {
            return await bus.Send(request);
        }
    }
}
