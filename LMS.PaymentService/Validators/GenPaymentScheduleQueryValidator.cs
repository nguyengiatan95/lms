﻿using FluentValidation;
using LMS.Common.Constants;
using LMS.PaymentServiceApi.Commands;
using LMS.PaymentServiceApi.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Validators
{
    public class GenPaymentScheduleQueryValidator : AbstractValidator<GenPaymentScheduleQuery>
    {
        public GenPaymentScheduleQueryValidator()
        {
            var conditions = ((PaymentSchedule_ActionRateType[])Enum.GetValues(typeof(PaymentSchedule_ActionRateType))).Select(c => (int)c).ToList();
            RuleFor(x => x.RateInterest).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.RateConsultant).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.RateService).GreaterThanOrEqualTo(0).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.RateType).Must(x => conditions.Contains(x)).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
        }
    }

    public class CreatePaymentScheduleByLoanIDCommandValidator : AbstractValidator<CreatePaymentScheduleByLoanIDCommand>
    {
        public CreatePaymentScheduleByLoanIDCommandValidator()
        {
            var conditions = ((PaymentSchedule_ActionRateType[])Enum.GetValues(typeof(PaymentSchedule_ActionRateType))).Select(c => (int)c).ToList();
            RuleFor(x => x.LoanID).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.RateInterest).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.RateConsultant).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.RateService).GreaterThanOrEqualTo(0).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.RateType).Must(x => conditions.Contains(x)).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.InterestStartDate).Must(x => DateTime.TryParseExact(x, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out var _)).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
        }
    }
}
