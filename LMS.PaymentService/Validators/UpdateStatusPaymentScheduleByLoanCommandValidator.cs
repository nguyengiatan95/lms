﻿using FluentValidation;
using LMS.Common.Constants;
using LMS.PaymentServiceApi.Commands;
using LMS.PaymentServiceApi.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Validators
{
    public class UpdateStatusPaymentScheduleByLoanCommandValidator : AbstractValidator<UpdateStatusPaymentScheduleByLoanCommand>
    {
        public UpdateStatusPaymentScheduleByLoanCommandValidator()
        {
            List<int> lstPaymentScheduleStatus = new List<int>() {
                (int)PaymentSchedule_Status.Delete,
                 (int)PaymentSchedule_Status.NoUse,
                 (int)PaymentSchedule_Status.Use
            };
            RuleFor(x => x.Status).Must(x => lstPaymentScheduleStatus.Contains(x)).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.LoanID).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
        }
    }
}
