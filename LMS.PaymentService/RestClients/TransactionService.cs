﻿using LMS.PaymentServiceApi.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.RestClients
{

    public interface ITransactionService
    {
        Task<long> CreateTransactionAfterPayment(List<PaymentTransactionInsertModel> request);
    }
    public class TransactionService : ITransactionService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        public TransactionService(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }
        public async Task<long> CreateTransactionAfterPayment(List<PaymentTransactionInsertModel> request)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.TransactionService}{LMS.Common.Constants.ActionApiInternal.Transaction_Action_CreateTransactionAfterPayment}";
            return await _apiHelper.ExecuteAsync<long>(url: url.ToString(), body: request, method: RestSharp.Method.POST);
        }
    }
}
