﻿using Microsoft.Extensions.DependencyInjection;

namespace LMS.PaymentServiceApi.RestClients
{
    public static class RestClientsInstaller
    {
        public static IServiceCollection AddRestClientsService(this IServiceCollection services)
        {
            services.AddTransient(typeof(ILoanService), typeof(LoanService));
            services.AddTransient(typeof(ITransactionService), typeof(TransactionService));
            return services;
        }
    }
}
