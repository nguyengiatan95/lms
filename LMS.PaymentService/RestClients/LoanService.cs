﻿using LMS.Entites.Dtos.LoanServices;
using LMS.PaymentServiceApi.Domain.Models;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.RestClients
{
    public interface ILoanService
    {
        Task<LoanDetailViewModel> GetLoanByID(long LoanID);
        Task<long> UpdateLoanAfterPayment(UpdateLoanAfterPaymentModel request);
    }
    public class LoanService : ILoanService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        public LoanService(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }

        public async Task<LoanDetailViewModel> GetLoanByID(long LoanID)
        {
            string fullPath = string.Format(LMS.Common.Constants.ActionApiInternal.Loan_Action_GetLoanByID, LoanID);
            var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{fullPath}";
            return await _apiHelper.ExecuteAsync<LoanDetailViewModel>(url: url.ToString(), body: null, method: RestSharp.Method.GET);
        }
        public async Task<long> UpdateLoanAfterPayment(UpdateLoanAfterPaymentModel request)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{LMS.Common.Constants.ActionApiInternal.Loan_Action_UpdateLoanAfterPayment}";
            return await _apiHelper.ExecuteAsync<long>(url: url.ToString(), body: request, method: RestSharp.Method.POST);
        }
    }
}
