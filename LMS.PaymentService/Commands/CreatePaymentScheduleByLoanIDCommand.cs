﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Commands
{
    public class CreatePaymentScheduleByLoanIDCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public long TotalMoneyDisbursement { get; set; }
        public string InterestStartDate { get; set; }
        [Required]
        public int LoanTime { get; set; }
        [Required]
        public int LoanFrequency { get; set; }
        [Required]
        public int RateType { get; set; }
        [Required]
        public decimal RateConsultant { get; set; }
        [Required]
        public decimal RateInterest { get; set; }
        [Required]
        public decimal RateService { get; set; }
        [Required]
        public long LoanID { get; set; }

        public string DayMustPay { get; set; }
        public bool IsKafka { get; set; } // dùng để chuyển hệ thống, mặc định = false
    }
    public class CreatePaymentScheduleByLoanIDCommandHandler : IRequestHandler<CreatePaymentScheduleByLoanIDCommand, LMS.Common.Constants.ResponseActionResult>
    {
        IEnumerable<Services.IGenPaymentScheduleManager> _actionManagers;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        ILogger<CreatePaymentScheduleByLoanIDCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        RestClients.ILoanService _loanService;
        public CreatePaymentScheduleByLoanIDCommandHandler(IEnumerable<Services.IGenPaymentScheduleManager> actionManagers,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            RestClients.ILoanService loanService,
            ILogger<CreatePaymentScheduleByLoanIDCommandHandler> logger)
        {
            _actionManagers = actionManagers;
            _paymentScheduleTab = paymentScheduleTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _loanService = loanService;
        }
        public async Task<ResponseActionResult> Handle(CreatePaymentScheduleByLoanIDCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult respone = new ResponseActionResult();
            try
            {
                DateTime interestStartDate = DateTime.ParseExact(request.InterestStartDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                DateTime? dayMustPay = null;
                if (!string.IsNullOrEmpty(request.DayMustPay))
                {
                    dayMustPay = DateTime.ParseExact(request.DayMustPay, TimaSettingConstant.DateTimeDayMonthYear, null);
                }
                var paymentExists = (await _paymentScheduleTab.SetGetTop(1).WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                if (paymentExists != null && paymentExists.PaymentScheduleID > 0)
                {
                    respone.SetSucces();
                    respone.Data = request.LoanID;
                    return respone;
                }
                var actionDetail = _actionManagers.FirstOrDefault(x => (int)x.RateType == request.RateType);
                if (actionDetail != null)
                {
                    var lstPaymentInfos = actionDetail.GetListPaymentSchedules(request.TotalMoneyDisbursement, interestStartDate, request.LoanTime, request.LoanFrequency, request.RateType, request.RateConsultant, request.RateInterest, request.RateService, request.LoanID, dayMustPay).ToList();
                    if (lstPaymentInfos != null && lstPaymentInfos.Count > 0)
                    {
                        var currentDate = DateTime.Now;
                        var lstPaymentInsert = new List<Domain.Tables.TblPaymentSchedule>();
                        foreach (var p in lstPaymentInfos)
                        {
                            Domain.Tables.TblPaymentSchedule item = new Domain.Tables.TblPaymentSchedule
                            {
                                CreatedDate = currentDate,
                                ModifiedDate = currentDate,
                                Status = 1,
                                IsVisible = false,
                                IsComplete = 0,
                                FromDate = p.FromDate,
                                ToDate = p.ToDate,
                                PayDate = p.PayDate,
                                MoneyOriginal = p.MoneyOriginal,
                                MoneyConsultant = p.MoneyConsultant,
                                MoneyFineInterestLate = p.MoneyFineInterestLate,
                                MoneyFineLate = p.MoneyFineLate,
                                MoneyInterest = p.MoneyInterest,
                                MoneyService = p.MoneyService,
                                LoanID = request.LoanID
                            };
                            lstPaymentInsert.Add(item);
                        }
                        lstPaymentInsert = lstPaymentInsert.OrderBy(x => x.FromDate).ToList();
                        _paymentScheduleTab.InsertBulk(lstPaymentInsert);
                        respone.SetSucces();
                        respone.Data = request.LoanID;
                        if (!request.IsKafka)
                        {
                            _ = await _loanService.UpdateLoanAfterPayment(new Domain.Models.UpdateLoanAfterPaymentModel
                            {
                                LoanID = request.LoanID,
                                NextDate = lstPaymentInsert.First().PayDate
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreatePaymentScheduleByLoanIDCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return respone;

        }
    }
}
