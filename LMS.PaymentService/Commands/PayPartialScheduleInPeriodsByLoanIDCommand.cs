﻿using LMS.Common.Constants;
using LMS.PaymentServiceApi.RestClients;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Commands
{
    public class PayPartialScheduleInPeriodsByLoanIDCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public long TotalMoneyPay { get; set; }
        public Transaction_TypeMoney TypeMoney { get; set; }
        public long CreateBy;
    }

    public class PayPartialScheduleInPeriodCommandHandler : IRequestHandler<PayPartialScheduleInPeriodsByLoanIDCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        ILogger<PayPartialScheduleInPeriodCommandHandler> _logger;
        ITransactionService _transactionService;
        Common.Helper.Utils _common;
        IEnumerable<Services.IPayPaymentManager> _payPaymentManagers;
        public PayPartialScheduleInPeriodCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            ITransactionService transactionService,
            IEnumerable<Services.IPayPaymentManager> payPaymentManagers,
            ILogger<PayPartialScheduleInPeriodCommandHandler> logger)
        {
            _paymentScheduleTab = paymentScheduleTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _transactionService = transactionService;
            _payPaymentManagers = payPaymentManagers;
        }
        public async Task<ResponseActionResult> Handle(PayPartialScheduleInPeriodsByLoanIDCommand request, CancellationToken cancellationToken)
        {
            var response = new ResponseActionResult();
            try
            {
                var lstPaymentSchedule = (await _paymentScheduleTab.WhereClause(x => x.LoanID == request.LoanID && x.Status == (int)PaymentSchedule_Status.Use)
                                                                   .QueryAsync()).ToList();
                if (lstPaymentSchedule == null || lstPaymentSchedule.Count == 0)
                {
                    response.Message = MessageConstant.Payment_NotFound;
                    _logger.LogError($"PayPartialScheduleInPeriodCommandHandler_Waring|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                List<Domain.Models.PaymentTransactionInsertModel> lstTran = new List<Domain.Models.PaymentTransactionInsertModel>();
                long totalMoneyCustomer = request.TotalMoneyPay;
                long totalMoneyCustomerPaid = 0;
                lstPaymentSchedule = lstPaymentSchedule.Where(x => x.IsComplete == (int)PaymentSchedule_IsComplete.Waiting).OrderBy(x => x.PayDate).ToList();
                if (lstPaymentSchedule == null || lstPaymentSchedule.Count == 0)
                {
                    response.SetSucces();
                    response.Data = totalMoneyCustomerPaid;
                    return response;
                }
                var actionDetail = _payPaymentManagers.FirstOrDefault(x => x.TypeMoney == request.TypeMoney);
                if (actionDetail == null)
                {
                    response.Message = MessageConstant.Payment_NotFound;
                    _logger.LogError($"PayPartialScheduleInPeriodCommandHandler_WarningMoneyType|request={_common.ConvertObjectToJSonV2(request)}|moneyType={request.TypeMoney}|message={response.Message}");
                    return response;
                }
                int transactionActionID = (int)Transaction_Action.DongLai;
                if (request.TypeMoney == Transaction_TypeMoney.FineLate)
                {
                    transactionActionID = (int)Transaction_Action.TraNoPhiPhatMuon;
                }
                var currentDate = DateTime.Now;
                foreach (var objPayment in lstPaymentSchedule)
                {
                    if (totalMoneyCustomer <= 0)
                    {
                        break;
                    }
                    actionDetail.PayMoney(lstTran, objPayment.LoanID, objPayment, ref totalMoneyCustomer, request.CreateBy, transactionActionID);
                    objPayment.ModifiedDate = currentDate;
                    if (!objPayment.FirstPaymentDate.HasValue)
                    {
                        objPayment.FirstPaymentDate = DateTime.Now;
                    }
                }
                if (lstTran != null && lstTran.Count > 0)
                {
                    // insert listTransaction
                    var taskTransaction = _transactionService.CreateTransactionAfterPayment(lstTran);

                    foreach (var item in lstPaymentSchedule)
                    {
                        if (item.IsComplete == (int)PaymentSchedule_IsComplete.Waiting)
                        {
                            var moneyRemainOrginal = item.MoneyOriginal - item.PayMoneyOriginal;
                            var moneyRemainInterest = item.MoneyInterest - item.PayMoneyInterest;
                            var moneyRemainConsultant = item.MoneyConsultant - item.PayMoneyConsultant;
                            var moneyRemainService = item.MoneyService - item.PayMoneyService;
                            var moneyRemainFineLate = item.MoneyFineLate - item.PayMoneyFineLate;
                            var totalNeedPayInPeriods = moneyRemainOrginal + moneyRemainInterest + moneyRemainConsultant + moneyRemainService;
                            var total = totalNeedPayInPeriods + moneyRemainFineLate;
                            if (totalNeedPayInPeriods <= TimaSettingConstant.MoneyCanChangeDPD)
                            {
                                item.IsVisible = true;
                                item.ModifiedDate = currentDate;
                            }
                            if (total == 0)
                            {
                                item.IsComplete = (int)PaymentSchedule_IsComplete.Paid;
                                item.IsVisible = true;
                                item.CompletedDate = currentDate;
                                item.ModifiedDate = currentDate;
                            }
                        }
                    }
                    _paymentScheduleTab.UpdateBulk(lstPaymentSchedule);

                    totalMoneyCustomerPaid = lstTran.Sum(x => x.TotalMoney);
                    await Task.WhenAll(taskTransaction);
                }
                response.SetSucces();
                response.Data = totalMoneyCustomerPaid;
            }
            catch (Exception ex)
            {
                _logger.LogError($"PayPartialScheduleInPeriodCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
