﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Commands
{
    public class WriteLstMoneyFineCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public List<LMS.Entites.Dtos.PaymentServices.RequestPaymentChangeMoneyFineModel> Models { get; set; }
    }
    public class WriteLstMoneyFineCommandHandler : IRequestHandler<WriteLstMoneyFineCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleQueryTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.PaymentScheduleUpdateMoneyFine> _paymentScheduleTab;
        readonly ILogger<WriteLstMoneyFineCommandHandler> _logger;
        readonly Common.Helper.Utils _common;
        public WriteLstMoneyFineCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleQueryTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.PaymentScheduleUpdateMoneyFine> paymentScheduleTab,
            ILogger<WriteLstMoneyFineCommandHandler> logger)
        {
            _paymentScheduleQueryTab = paymentScheduleQueryTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _paymentScheduleTab = paymentScheduleTab;
        }
        public async Task<ResponseActionResult> Handle(WriteLstMoneyFineCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                if (request.Models == null || request.Models.Count == 0)
                {
                    response.Message = MessageConstant.ParameterInvalid;
                    return response;
                }
                var lstPaymentScheduleID = request.Models.Select(x => x.PaymentScheduleID).ToList();
                var lstPaymentScheduleInfos = await _paymentScheduleQueryTab.WhereClause(x => lstPaymentScheduleID.Contains(x.PaymentScheduleID)).QueryAsync();
                if (lstPaymentScheduleInfos == null || !lstPaymentScheduleInfos.Any())
                {
                    response.Message = MessageConstant.Payment_NotFound;
                    return response;
                }
                var dictPaymentScheduleInfos = lstPaymentScheduleInfos.ToDictionary(x => x.PaymentScheduleID, x => x);
                DateTime currentDate = DateTime.Now;
                List<Domain.Tables.PaymentScheduleUpdateMoneyFine> lstUpdate = new List<Domain.Tables.PaymentScheduleUpdateMoneyFine>();
                foreach (var item in request.Models)
                {
                    var paymentDetail = dictPaymentScheduleInfos.GetValueOrDefault(item.PaymentScheduleID);
                    if (paymentDetail != null && paymentDetail.PaymentScheduleID > 0)
                    {
                        lstUpdate.Add(new Domain.Tables.PaymentScheduleUpdateMoneyFine
                        {
                            PaymentScheduleID = paymentDetail.PaymentScheduleID,
                            ModifiedDate = currentDate,
                            MoneyFineLate = paymentDetail.MoneyFineLate + item.MoneyAdd,
                            Fined = item.IsFullPeriod ? (int)PaymentSchedule_Fined.Fined : (int)PaymentSchedule_Fined.NotYet
                        });
                    }
                }
                if (lstUpdate.Count > 0)
                {
                    _paymentScheduleTab.UpdateBulk(lstUpdate);
                }
                response.SetSucces();
                response.Data = lstUpdate.Count;
            }
            catch (Exception ex)
            {
                _logger.LogError($"WriteLstMoneyFineCommand|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;

        }
    }
}
