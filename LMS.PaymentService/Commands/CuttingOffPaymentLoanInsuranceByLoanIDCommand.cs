﻿using LMS.Common.Constants;
using LMS.PaymentServiceApi.RestClients;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Commands
{
    public class CuttingOffPaymentLoanInsuranceByLoanIDCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public long TotalMoneyPay { get; set; }
        public Transaction_TypeMoney TypeMoney { get; set; }

        public long CreateBy { get; set; }
    }
    public class CuttingOffPaymentLoanInsuranceByLoanIDCommandHandler : IRequestHandler<CuttingOffPaymentLoanInsuranceByLoanIDCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        ILogger<CuttingOffPaymentLoanInsuranceByLoanIDCommandHandler> _logger;
        ITransactionService _transactionService;
        ILoanService _loanService;
        Common.Helper.Utils _common;
        IEnumerable<Services.IPayPaymentManager> _payPaymentManagers;
        public CuttingOffPaymentLoanInsuranceByLoanIDCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
             ITransactionService transactionService,
             ILoanService loanService,
             IEnumerable<Services.IPayPaymentManager> payPaymentManagers,
            ILogger<CuttingOffPaymentLoanInsuranceByLoanIDCommandHandler> logger)
        {
            _paymentScheduleTab = paymentScheduleTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _loanService = loanService;
            _transactionService = transactionService;
            _payPaymentManagers = payPaymentManagers;
        }
        public async Task<ResponseActionResult> Handle(CuttingOffPaymentLoanInsuranceByLoanIDCommand request, CancellationToken cancellationToken)
        {
            var response = new ResponseActionResult();
            try
            {
                response.SetSucces();
                response.Data = 0;
                var loanInfo = await _loanService.GetLoanByID(request.LoanID);
                var lstPaymentSchedule = _paymentScheduleTab.WhereClause(x => x.LoanID == request.LoanID && x.Status == (int)PaymentSchedule_Status.Use && x.IsComplete == (int)PaymentSchedule_IsComplete.ProcessInsurance)
                                                    .OrderBy(x => x.PayDate).Query().ToList();
                if (lstPaymentSchedule == null || lstPaymentSchedule.Count == 0 || lstPaymentSchedule.Count > 1)
                {
                    _logger.LogError($"CuttingOffPaymentLoanInsuranceByLoanIDCommandHandler_warning|Not found paymentschedule|request={_common.ConvertObjectToJSonV2(request)}");
                    return response;
                }
                List<Domain.Models.PaymentTransactionInsertModel> lstTran = new List<Domain.Models.PaymentTransactionInsertModel>();
                long totalMoneyCustomer = request.TotalMoneyPay;
                var actionDetail = _payPaymentManagers.FirstOrDefault(x => x.TypeMoney == request.TypeMoney);
                int transactionActionID = (int)Transaction_Action.ReceiptInsurance;
                //// bỏ ghi nhận phí phạt trả chậm theo gạch nợ thông thường -> dựa vào ag đang ghi vào bảng thanh toán bảo hiểm
                //// TblTransactionIndemnifyLoanInsurrance
                //if (request.TypeMoney == Transaction_TypeMoney.FineLate)
                //{
                //    transactionActionID = (int)Transaction_Action.TraNoPhiPhatMuon;
                //}
                var currentDate = DateTime.Now;
                var paymentDetail = lstPaymentSchedule.First();
                if (actionDetail != null)
                {
                    actionDetail.PayMoney(lstTran, request.LoanID, paymentDetail, ref totalMoneyCustomer, request.CreateBy, transactionActionID);
                    paymentDetail.ModifiedDate = currentDate;
                }
                if (lstTran != null && lstTran.Count > 0)
                {
                    // insert listTransaction
                    var taskTransaction = _transactionService.CreateTransactionAfterPayment(lstTran);

                    var moneyRemainOrginal = paymentDetail.MoneyOriginal - paymentDetail.PayMoneyOriginal;
                    var moneyRemainInterest = paymentDetail.MoneyInterest - paymentDetail.PayMoneyInterest;
                    var moneyRemainConsultant = paymentDetail.MoneyConsultant - paymentDetail.PayMoneyConsultant;
                    var moneyRemainService = paymentDetail.MoneyService - paymentDetail.PayMoneyService;
                    var moneyRemainFineLate = paymentDetail.MoneyFineLate - paymentDetail.PayMoneyFineLate;
                    var total = moneyRemainOrginal + moneyRemainInterest + moneyRemainConsultant + moneyRemainService + moneyRemainFineLate;
                    if (total == 0)
                    {
                        paymentDetail.IsComplete = (int)PaymentSchedule_IsComplete.Paid;
                        paymentDetail.IsVisible = true;
                        paymentDetail.CompletedDate = currentDate;
                        paymentDetail.ModifiedDate = currentDate;
                    }
                    // nghiệp vụ Loan sẽ cập nhật
                    //Domain.Models.UpdateLoanAfterPaymentModel updateLoanAfterPayment = new Domain.Models.UpdateLoanAfterPaymentModel()
                    //{
                    //    LoanID = request.LoanID,
                    //    LastDateOfPay = loanInfo.LastDateOfPay,
                    //    NextDate = loanInfo.NextDate
                    //};

                    //if (request.TypeMoney == Transaction_TypeMoney.Original)
                    //{
                    //    updateLoanAfterPayment.OriginalMoney = totalMoneyCustomer;
                    //}
                    //_ = _loanService.UpdateLoanAfterPayment(updateLoanAfterPayment);
                    _paymentScheduleTab.Update(paymentDetail);

                    var totalMoneyCustomerPaid = lstTran.Sum(x => x.TotalMoney);
                    response.Data = totalMoneyCustomerPaid;
                    await Task.WhenAll(taskTransaction);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CuttingOffPaymentLoanInsuranceByLoanIDCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
