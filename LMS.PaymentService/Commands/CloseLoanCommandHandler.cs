﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Commands
{
    public class CloseLoanCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public long MoneyOriginal { get; set; }
        public long MoneyService { get; set; }
        public long MoneyInterest { get; set; }
        public long MoneyConsultant { get; set; }
        public long MoneyFineOriginal { get; set; }
        public long MoneyFineLate { get; set; }
        public string CloseDate { get; set; }// dd/mm/yyyy
        public long CreateBy { get; set; }
        public int LoanStatus { get; set; }
        public long InsurancePartnerCode { get; set; }
    }
    public class CloseLoanCommandHandler : IRequestHandler<CloseLoanCommand, LMS.Common.Constants.ResponseActionResult>
    {
        ILogger<CloseLoanCommandHandler> _logger;
        Common.Helper.Utils _common;
        IEnumerable<Services.ICloseLoanManager> _closeLoanManager;
        public CloseLoanCommandHandler(
            IEnumerable<Services.ICloseLoanManager> closeManager,
            ILogger<CloseLoanCommandHandler> logger)
        {
            _closeLoanManager = closeManager;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CloseLoanCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                Loan_Status typeCloseLoan = (Loan_Status)request.LoanStatus;
                var actionDetail = _closeLoanManager.FirstOrDefault(x => x.TypeCloseLoan == typeCloseLoan);
                if (actionDetail != null)
                {
                    var closeDate = DateTime.ParseExact(request.CloseDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                    response = await actionDetail.CloseLoan(request.LoanID, request.MoneyOriginal, request.MoneyInterest, request.MoneyConsultant
                           , request.MoneyFineLate, request.MoneyFineOriginal, request.MoneyService, closeDate, request.CreateBy, request.InsurancePartnerCode);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CloseLoanCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
