﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Commands
{
    public class CancelMoneyFineByLoanIDCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public long MoneyFineLate { get; set; }
    }
    public class CancelMoneyFineByLoanIDCommandHandler : IRequestHandler<CancelMoneyFineByLoanIDCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleQueryTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.PaymentScheduleUpdateMoneyFine> _paymentScheduleTab;
        readonly ILogger<CancelMoneyFineByLoanIDCommandHandler> _logger;
        readonly Common.Helper.Utils _common;
        public CancelMoneyFineByLoanIDCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleQueryTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.PaymentScheduleUpdateMoneyFine> paymentScheduleTab,
            ILogger<CancelMoneyFineByLoanIDCommandHandler> logger)
        {
            _paymentScheduleQueryTab = paymentScheduleQueryTab;
            _paymentScheduleTab = paymentScheduleTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CancelMoneyFineByLoanIDCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstPaymentSchedules = await _paymentScheduleQueryTab.WhereClause(x => x.LoanID == request.LoanID && x.Status == (int)PaymentSchedule_Status.Use).QueryAsync();
                if (lstPaymentSchedules == null || !lstPaymentSchedules.Any())
                {
                    response.Message = MessageConstant.Payment_NotFound;
                    return response;
                }
                response.SetSucces();
                // trừ dần các kỳ gần nhất đến xa nhất dựa vào số tiền phí phạt đã có
                lstPaymentSchedules = lstPaymentSchedules.Where(x => x.MoneyFineLate > 0).OrderByDescending(x => x.PayDate);
                if (lstPaymentSchedules == null || !lstPaymentSchedules.Any())
                {
                    response.Message = MessageConstant.Payment_NotFound;
                    return response;
                }
                var currentDate = DateTime.Now;
                List<Domain.Tables.PaymentScheduleUpdateMoneyFine> lstDataUpdate = new List<Domain.Tables.PaymentScheduleUpdateMoneyFine>();
                List<object> lstDataResponse = new List<object>();
                foreach (var item in lstPaymentSchedules)
                {
                    if (request.MoneyFineLate < 1)
                    {
                        break;
                    }
                    long moneyFineMain = item.MoneyFineLate > request.MoneyFineLate ? request.MoneyFineLate : item.MoneyFineLate;
                    request.MoneyFineLate -= moneyFineMain;
                    item.MoneyFineLate -= moneyFineMain;
                    var moneyRemainOrginal = item.MoneyOriginal - item.PayMoneyOriginal;
                    var moneyRemainInterest = item.MoneyInterest - item.PayMoneyInterest;
                    var moneyRemainConsultant = item.MoneyConsultant - item.PayMoneyConsultant;
                    var moneyRemainService = item.MoneyService - item.PayMoneyService;
                    var moneyRemainFineLate = item.MoneyFineLate - item.PayMoneyFineLate;
                    var totalNeedPayInPeriods = moneyRemainOrginal + moneyRemainInterest + moneyRemainConsultant + moneyRemainService;
                    var total = totalNeedPayInPeriods + moneyRemainFineLate;
                    item.IsComplete = (int)PaymentSchedule_IsComplete.Waiting;
                    item.IsVisible = false;
                    if (totalNeedPayInPeriods <= TimaSettingConstant.MoneyCanChangeDPD)
                    {
                        item.IsVisible = true;
                    }
                    if (total <= 0)
                    {
                        item.IsComplete = (int)PaymentSchedule_IsComplete.Paid;
                        item.IsVisible = true;
                    }
                    lstDataUpdate.Add(new Domain.Tables.PaymentScheduleUpdateMoneyFine
                    {
                        PaymentScheduleID = item.PaymentScheduleID,
                        ModifiedDate = currentDate,
                        MoneyFineLate = item.MoneyFineLate,
                        PayMoneyFineLate = item.PayMoneyFineLate,
                        IsVisible = item.IsVisible,
                        IsComplete = item.IsComplete,
                        CompletedDate = item.CompletedDate,
                        Fined = item.Fined
                    });
                    lstDataResponse.Add(new
                    {
                        PaymentScheduleID = item.PaymentScheduleID,
                        MoneyFineLate = moneyFineMain

                    });
                }
                if (lstDataUpdate.Count > 0)
                {
                    _paymentScheduleTab.UpdateBulk(lstDataUpdate);
                    response.Data = lstDataResponse;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CancelMoneyFineByLoanCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
