﻿using LMS.Common.Constants;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Commands
{
    public class DebtResctructuringCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }

        /// <summary>
        /// Kiểu tái cơ cấu nợ
        /// </summary>
        public int TypeDebtRestructuring { get; set; }

        /// <summary>
        /// Số kỳ khoanh vùng trả nợ
        /// </summary>
        public int NumberOfRepaymentPeriod { get; set; }

        public string AppointmentDate { get; set; }
        public int PercentDiscount { get; set; }

        public int LoanRateType { get; set; }
        public int MinDayFineLate { get; set; }
        public long CreateBy { get; set; }
    }
    public class DebtResctructuringCommandHandler : IRequestHandler<DebtResctructuringCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly IEnumerable<Services.IDebtRestructuringManager> _actionManagers;
        public DebtResctructuringCommandHandler(IEnumerable<Services.IDebtRestructuringManager> actionManagers)
        {
            _actionManagers = actionManagers;
        }
        public async Task<ResponseActionResult> Handle(DebtResctructuringCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var actionDetail = _actionManagers.FirstOrDefault(x => (int)x.TypeDebtRestructuring == request.TypeDebtRestructuring);
            if (actionDetail == null)
            {
                response.Message = "Kiểu tái cấu trúng nợ chưa được hỗ trợ";
                return response;
            }
            return await actionDetail.DebtRestructing(new Domain.Models.DebtRestructuringModel
            {
                LoanID = request.LoanID,
                AppointmentDate = request.AppointmentDate,
                NumberOfRepaymentPeriod = request.NumberOfRepaymentPeriod,
                LoanRateType = request.LoanRateType,
                PercentDiscount = request.PercentDiscount, 
                MinDayFineLate = request.MinDayFineLate,
                CreateBy = request.CreateBy
            });
        }
    }
}
