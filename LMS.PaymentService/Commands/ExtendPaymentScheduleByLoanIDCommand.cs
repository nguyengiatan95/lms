﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Commands
{
    public class ExtendPaymentScheduleByLoanIDCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public long TotalMoneyDisbursement { get; set; }
        [Required]
        public int LoanTime { get; set; }
        [Required]
        public int LoanFrequency { get; set; }
        [Required]
        public int RateType { get; set; }
        [Required]
        public decimal RateConsultant { get; set; }
        [Required]
        public decimal RateInterest { get; set; }
        [Required]
        public decimal RateService { get; set; }
        [Required]
        public long LoanID { get; set; }
    }
    public class ExtendPaymentScheduleByLoanIDCommandHandler : IRequestHandler<ExtendPaymentScheduleByLoanIDCommand, LMS.Common.Constants.ResponseActionResult>
    {
        IEnumerable<Services.IGenPaymentScheduleManager> _actionManagers;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        ILogger<ExtendPaymentScheduleByLoanIDCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        RestClients.ILoanService _loanService;
        public ExtendPaymentScheduleByLoanIDCommandHandler(IEnumerable<Services.IGenPaymentScheduleManager> actionManagers,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            RestClients.ILoanService loanService,
            ILogger<ExtendPaymentScheduleByLoanIDCommandHandler> logger)
        {
            _actionManagers = actionManagers;
            _paymentScheduleTab = paymentScheduleTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _loanService = loanService;
        }
        public async Task<ResponseActionResult> Handle(ExtendPaymentScheduleByLoanIDCommand request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult respone = new ResponseActionResult();
                try
                {
                    respone.Message = MessageConstant.PaymentInvalid;
                    var currentDate = DateTime.Now;
                    var actionDetail = _actionManagers.FirstOrDefault(x => (int)x.RateType == request.RateType);
                    if (actionDetail != null)
                    {
                        var lstPaymentScheduleOld = _paymentScheduleTab.WhereClause(x => x.LoanID == request.LoanID && x.Status == (int)PaymentSchedule_Status.Use).Query().OrderByDescending(x => x.PayDate).ToList();
                        if (lstPaymentScheduleOld == null || lstPaymentScheduleOld.Count == 0)
                        {
                            respone.Message = MessageConstant.Payment_NotFound;
                            return respone;
                        }
                        var lastPaymentInfo = lstPaymentScheduleOld.FirstOrDefault();
                        DateTime interestStartDate = lastPaymentInfo.ToDate.AddDays(1);
                        var lstPaymentInfos = actionDetail.GetListPaymentSchedules(request.TotalMoneyDisbursement, interestStartDate, request.LoanTime, request.LoanFrequency, request.RateType, request.RateConsultant, request.RateInterest, request.RateService, request.LoanID).ToList();
                        if (lstPaymentInfos != null && lstPaymentInfos.Count > 0)
                        {
                            lastPaymentInfo.MoneyOriginal = lastPaymentInfo.PayMoneyOriginal;
                            lastPaymentInfo.ModifiedDate = currentDate;

                            var lstPaymentInsert = new List<Domain.Tables.TblPaymentSchedule>();
                            foreach (var p in lstPaymentInfos)
                            {
                                Domain.Tables.TblPaymentSchedule item = new Domain.Tables.TblPaymentSchedule
                                {
                                    CreatedDate = currentDate,
                                    ModifiedDate = currentDate,
                                    Status = (int)PaymentSchedule_Status.Use,
                                    IsVisible = false,
                                    IsComplete = (int)PaymentSchedule_IsComplete.Waiting,
                                    FromDate = p.FromDate,
                                    ToDate = p.ToDate,
                                    PayDate = p.PayDate,
                                    MoneyOriginal = p.MoneyOriginal,
                                    MoneyConsultant = p.MoneyConsultant,
                                    MoneyFineInterestLate = p.MoneyFineInterestLate,
                                    MoneyFineLate = p.MoneyFineLate,
                                    MoneyInterest = p.MoneyInterest,
                                    MoneyService = p.MoneyService,
                                    LoanID = request.LoanID
                                };
                                lstPaymentInsert.Add(item);
                            }
                            lstPaymentInsert = lstPaymentInsert.OrderBy(x => x.FromDate).ToList();
                            _paymentScheduleTab.InsertBulk(lstPaymentInsert);
                            _paymentScheduleTab.Update(lastPaymentInfo);
                            respone.SetSucces();
                            respone.Data = request.LoanID;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"ExtendPaymentScheduleByLoanIDCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                }
                return respone;
            });

        }
    }
}
