﻿using LMS.Common.Constants;
using LMS.PaymentServiceApi.Domain.Tables;
using LMS.PaymentServiceApi.RestClients;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Commands
{
    public class CloseLoanFromScheduleCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public long MoneyOriginal { get; set; }
        public long MoneyService { get; set; }
        public long MoneyInterest { get; set; }
        public long MoneyConsultant { get; set; }
        public long MoneyFineOriginal { get; set; }
        public long OtherMoney { get; set; }
        public string CloseDate { get; set; }// dd/mm/yyyy
        public long CreateBy { get; set; }
    }
    public class CloseLoanFromScheduleCommandHandler : IRequestHandler<CloseLoanFromScheduleCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        ILogger<CloseLoanFromScheduleCommandHandler> _logger;
        ITransactionService _transactionService;
        ILoanService _loanService;
        Common.Helper.Utils _common;
        IEnumerable<Services.IPayPaymentManager> _payPaymentManagers;
        public CloseLoanFromScheduleCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
             ITransactionService transactionService,
             ILoanService loanService,
             IEnumerable<Services.IPayPaymentManager> payPaymentManagers,
            ILogger<CloseLoanFromScheduleCommandHandler> logger)
        {
            _paymentScheduleTab = paymentScheduleTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _loanService = loanService;
            _transactionService = transactionService;
            _payPaymentManagers = payPaymentManagers;
        }
        public async Task<ResponseActionResult> Handle(CloseLoanFromScheduleCommand request, CancellationToken cancellationToken)
        {
            var response = new ResponseActionResult();
            try
            {
                var closeDate = DateTime.ParseExact(request.CloseDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                var currentDate = DateTime.Now;

                var loanInfoTask = _loanService.GetLoanByID(request.LoanID);
                var lstPaymentTask = _paymentScheduleTab.WhereClause(x => x.LoanID == request.LoanID && x.Status == (int)PaymentSchedule_Status.Use)
                                                    .WhereClause(x => x.IsComplete == (int)PaymentSchedule_IsComplete.Waiting)
                                                    .WhereClause(x => x.IsVisible == false)
                                                    .QueryAsync();//).OrderBy(x => x.PaymentScheduleID).ToList();

                await Task.WhenAll(loanInfoTask, lstPaymentTask);

                var loanInfo = loanInfoTask.Result;
                var lstPayment = lstPaymentTask.Result.OrderBy(x => x.PaymentScheduleID).ToList();

                if (lstPayment == null || lstPayment.Count == 0)
                {
                    response.Message = MessageConstant.Payment_NotFound;
                    return response;
                }
                Domain.Tables.TblPaymentSchedule paymentScheduleLatest = lstPayment.First();
                // giữ lại giá trị ban đầu của bảng, không cập nhật
                foreach (var item in lstPayment)
                {
                    item.ModifiedDate = currentDate;
                    item.Status = (int)PaymentSchedule_Status.NoUse;
                }
                _paymentScheduleTab.UpdateBulk(lstPayment);
                var paymentCloseLoanID = _paymentScheduleTab.Insert(new TblPaymentSchedule
                {
                    LoanID = loanInfo.LoanID,
                    FromDate = paymentScheduleLatest.FromDate,
                    ToDate = closeDate,
                    PayDate = closeDate,
                    MoneyConsultant = request.MoneyConsultant,
                    MoneyFineInterestLate = 0,
                    MoneyFineLate = request.OtherMoney,
                    MoneyInterest = request.MoneyInterest,
                    MoneyOriginal = request.MoneyOriginal,
                    MoneyService = request.MoneyService,
                    PayMoneyOriginal = request.MoneyOriginal,
                    PayMoneyConsultant = request.MoneyConsultant,
                    PayMoneyFineInterestLate = 0,
                    PayMoneyFineOriginal = request.MoneyFineOriginal,
                    PayMoneyFineLate = request.OtherMoney,
                    PayMoneyInterest = request.MoneyInterest,
                    PayMoneyService = request.MoneyService,
                    CreatedDate = currentDate,
                    FirstPaymentDate = closeDate,
                    CompletedDate = closeDate,
                    ModifiedDate = currentDate,
                    Status = (int)PaymentSchedule_Status.Use,
                    IsComplete = (int)PaymentSchedule_IsComplete.Paid,
                    IsVisible = true
                });
                List<Domain.Models.PaymentTransactionInsertModel> lstTran = new List<Domain.Models.PaymentTransactionInsertModel>();
                if (request.MoneyOriginal > 0)
                {
                    lstTran.Add(new Domain.Models.PaymentTransactionInsertModel
                    {
                        ActionID = (int)Transaction_Action.DongHd,
                        LoanID = request.LoanID,
                        MoneyType = (int)Transaction_TypeMoney.Original,
                        TotalMoney = request.MoneyOriginal,
                        UserID = request.CreateBy,
                        PaymentScheduleID = paymentCloseLoanID
                    });
                }
                if (request.MoneyInterest > 0)
                {
                    lstTran.Add(new Domain.Models.PaymentTransactionInsertModel
                    {
                        ActionID = (int)Transaction_Action.DongHd,
                        LoanID = request.LoanID,
                        MoneyType = (int)Transaction_TypeMoney.Interest,
                        TotalMoney = request.MoneyInterest,
                        UserID = request.CreateBy,
                        PaymentScheduleID = paymentCloseLoanID
                    });
                }
                if (request.MoneyConsultant > 0)
                {
                    lstTran.Add(new Domain.Models.PaymentTransactionInsertModel
                    {
                        ActionID = (int)Transaction_Action.DongHd,
                        LoanID = request.LoanID,
                        MoneyType = (int)Transaction_TypeMoney.Consultant,
                        TotalMoney = request.MoneyConsultant,
                        UserID = request.CreateBy,
                        PaymentScheduleID = paymentCloseLoanID
                    });
                }
                if (request.MoneyFineOriginal > 0)
                {
                    lstTran.Add(new Domain.Models.PaymentTransactionInsertModel
                    {
                        ActionID = (int)Transaction_Action.DongHd,
                        LoanID = request.LoanID,
                        MoneyType = (int)Transaction_TypeMoney.FineOriginal,
                        TotalMoney = request.MoneyFineOriginal,
                        UserID = request.CreateBy,
                        PaymentScheduleID = paymentCloseLoanID
                    });
                }
                if (request.MoneyService > 0)
                {
                    lstTran.Add(new Domain.Models.PaymentTransactionInsertModel
                    {
                        ActionID = (int)Transaction_Action.DongHd,
                        LoanID = request.LoanID,
                        MoneyType = (int)Transaction_TypeMoney.Service,
                        TotalMoney = request.MoneyService,
                        UserID = request.CreateBy,
                        PaymentScheduleID = paymentCloseLoanID
                    });
                }
                if (request.OtherMoney > 0)
                {
                    lstTran.Add(new Domain.Models.PaymentTransactionInsertModel
                    {
                        ActionID = (int)Transaction_Action.DongHd,
                        LoanID = request.LoanID,
                        MoneyType = (int)Transaction_TypeMoney.FineLate,
                        TotalMoney = request.OtherMoney,
                        UserID = request.CreateBy,
                        PaymentScheduleID = paymentCloseLoanID
                    });
                }
                if (lstTran.Count > 0)
                {
                    _ = await _transactionService.CreateTransactionAfterPayment(lstTran);
                }
                var totalMoneyCustomerPaid = lstTran.Sum(x => x.TotalMoney);
                response.SetSucces();
                response.Data = totalMoneyCustomerPaid;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CloseLoanFromScheduleCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
