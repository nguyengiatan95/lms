﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace LMS.PaymentServiceApi.Commands
{
    public class UpdateStatusPaymentScheduleByLoanCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public long LoanID { get; set; }
        [Required]
        public int Status { get; set; }
    }
    public class UpdateStatusPaymentScheduleByLoanCommandHandler : IRequestHandler<UpdateStatusPaymentScheduleByLoanCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        ILogger<UpdateStatusPaymentScheduleByLoanCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdateStatusPaymentScheduleByLoanCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            ILogger<UpdateStatusPaymentScheduleByLoanCommandHandler> logger)
        {
            _paymentScheduleTab = paymentScheduleTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(UpdateStatusPaymentScheduleByLoanCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult respone = new ResponseActionResult();
            try
            {
                var lstPaymentSchedule = await _paymentScheduleTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync();
                if (lstPaymentSchedule != null && lstPaymentSchedule.Count() > 0)
                {
                    lstPaymentSchedule.Select(c => { c.Status = request.Status; return c; }).ToList();
                    _paymentScheduleTab.UpdateBulk(lstPaymentSchedule);
                    respone.SetSucces();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateStatusPaymentScheduleByLoanCommand_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return respone;
        }
    }
}
