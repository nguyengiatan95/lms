﻿using LMS.Common.Constants;
using LMS.PaymentServiceApi.Domain.Models;
using LMS.PaymentServiceApi.RestClients;
using LMS.PaymentServiceApi.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.PaymentServiceApi.Commands
{
    public class PayPaymentCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public RequestPayPayment Model { get; set; }
    }
    public class PayPaymentScheduleHandler : IRequestHandler<PayPaymentCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        ILogger<PayPaymentScheduleHandler> _logger;
        ITransactionService _transactionService;
        ILoanService _loanService;
        Common.Helper.Utils _common;
        IEnumerable<Services.IPayPaymentManager> _payPaymentManagers;
        public PayPaymentScheduleHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
             ITransactionService transactionService,
             ILoanService loanService,
             IEnumerable<Services.IPayPaymentManager> payPaymentManagers,
            ILogger<PayPaymentScheduleHandler> logger)
        {
            _paymentScheduleTab = paymentScheduleTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _loanService = loanService;
            _transactionService = transactionService;
            _payPaymentManagers = payPaymentManagers;
        }

        public async Task<ResponseActionResult> Handle(PayPaymentCommand request, CancellationToken cancellationToken)
        {
            var response = new ResponseActionResult();
            DateTime dtNow = DateTime.Now;
            long totalNeedPay = 0;
            try
            {
                var nextDate = DateTime.ParseExact(request.Model.NextDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                var requestData = request.Model;
                var loanInfo = await _loanService.GetLoanByID(requestData.LoanID);
                // laays danh sach payment chua dong xong
                var lstPaymentSchedule = await _paymentScheduleTab.SetGetTop(TimaSettingConstant.MaxItemQuery)
                                                                  .WhereClause(x => x.LoanID == requestData.LoanID 
                                                                                    && x.Status == (int)PaymentSchedule_Status.Use 
                                                                                    && x.IsComplete == (int)PaymentSchedule_IsComplete.Waiting)
                                                                  .QueryAsync();
                if (lstPaymentSchedule == null || !lstPaymentSchedule.Any())
                {
                    response.Message = MessageConstant.Payment_NotFound;
                    _logger.LogError($"PayPaymentScheduleHandler_WarningNotFound|request={_common.ConvertObjectToJSonV2(request.Model)}|message={response.Message}");
                    return response;
                }
                lstPaymentSchedule = lstPaymentSchedule.OrderBy(x => x.PayDate);
                long moneyOriginalNeedPay = 0;
                long moneyServiceNeedPay = 0;
                long moneyInterestNeedPay = 0;
                long moneyConsultantNeedPay = 0;
                long moneyMoneyFineLateNeedPay = 0;
                long totalMoneyCustomerPaid = 0;
                Domain.Tables.TblPaymentSchedule paymentDetail = null;
                foreach (var item in lstPaymentSchedule)
                {
                    // đơn quá hạn và kỳ cuối ko tự động gạch nợ.
                    if (requestData.CheckLastPeriods && item.ToDate == loanInfo.ToDate && loanInfo.ToDate <= dtNow)
                    {
                        _logger.LogError($"PayPaymentScheduleHandler_Warning_Pending_Hold_LastPeriods|customerID={loanInfo.CustomerID}|loanIDs={string.Join(",", loanInfo.LoanID)}");
                        break;
                    }
                    if (item.PayDate <= nextDate)
                    {
                        moneyOriginalNeedPay = item.MoneyOriginal - item.PayMoneyOriginal;
                        moneyServiceNeedPay = item.MoneyService - item.PayMoneyService;
                        moneyInterestNeedPay = item.MoneyInterest - item.PayMoneyInterest;
                        moneyConsultantNeedPay = item.MoneyConsultant - item.PayMoneyConsultant;
                        moneyMoneyFineLateNeedPay = item.MoneyFineLate - item.PayMoneyFineLate; // check theo điều kiện typeMoney nếu có
                        totalNeedPay = moneyOriginalNeedPay + moneyServiceNeedPay + moneyInterestNeedPay + moneyConsultantNeedPay;
                        if (totalNeedPay > 0)
                        {
                            paymentDetail = item;
                            break;
                        }
                    }
                }
                if (paymentDetail == null)
                {
                    response.Message = MessageConstant.PaymentInvalid;
                    _logger.LogError($"PayPaymentScheduleHandler_WarningNotFoundDetail|request={_common.ConvertObjectToJSonV2(request.Model)}|message={response.Message}");
                    return response;
                }
                List<PaymentTransactionInsertModel> lstTran = new List<PaymentTransactionInsertModel>();
                long totalNeedPayToChangeDPD = totalNeedPay;
                if (totalNeedPayToChangeDPD <= requestData.TotalMoney + TimaSettingConstant.MoneyCanChangeDPD)
                {
                    paymentDetail.IsVisible = true;
                    paymentDetail.ModifiedDate = dtNow;
                }
                long totalMoneyCustomer = requestData.TotalMoney;

                // lấy config các loại tiền theo hợp đồng (đang để default)
                //var lstMoneyType = Enum.GetValues(typeof(Common.Constants.Transaction_TypeMoney)).Cast<Common.Constants.Transaction_TypeMoney>();
                // không xử lý phí phạt
                var lstMoneyType = new List<Common.Constants.Transaction_TypeMoney>();
                // { Transaction_TypeMoney.Original, Transaction_TypeMoney.Interest, Transaction_TypeMoney.Service, Transaction_TypeMoney.Consultant };
                if (moneyOriginalNeedPay > 0)
                {
                    lstMoneyType.Add(Transaction_TypeMoney.Original);
                    //updateLoanAfterPayment.OriginalMoney = moneyOriginalNeedPay;
                }
                if (moneyInterestNeedPay > 0)
                {
                    lstMoneyType.Add(Transaction_TypeMoney.Interest);
                }
                if (moneyServiceNeedPay > 0)
                {
                    lstMoneyType.Add(Transaction_TypeMoney.Service);
                }
                if (moneyConsultantNeedPay > 0)
                {
                    lstMoneyType.Add(Transaction_TypeMoney.Consultant);
                }
                if (moneyMoneyFineLateNeedPay > 0 && request.Model.LstMoneyType != null && request.Model.LstMoneyType.Contains((int)Transaction_TypeMoney.FineLate))
                {
                    lstMoneyType.Add(Transaction_TypeMoney.FineLate);
                }
                if (request.Model.LstMoneyType != null && request.Model.LstMoneyType.Count > 0)
                {
                    lstMoneyType = request.Model.LstMoneyType.Select(i => (Transaction_TypeMoney)i).ToList();
                }
                foreach (var item in lstMoneyType)
                {
                    var actionDetail = _payPaymentManagers.FirstOrDefault(x => x.TypeMoney == item);
                    if (actionDetail != null)
                    {
                        actionDetail.PayMoney(lstTran, paymentDetail.LoanID, paymentDetail, ref totalMoneyCustomer, requestData.CreateBy, (int)Transaction_Action.DongLai);
                        if (!paymentDetail.FirstPaymentDate.HasValue)
                        {
                            paymentDetail.FirstPaymentDate = dtNow;
                        }
                    }
                    else
                    {
                        _logger.LogError($"PayPaymentScheduleHandler_WarningMoneyType|request={_common.ConvertObjectToJSonV2(request.Model)}|moneyType={item}");
                    }
                }
                if (lstTran != null && lstTran.Count > 0)
                {
                    // update các kỳ 
                    foreach (var item in lstPaymentSchedule)
                    {
                        if (item.IsComplete == (int)PaymentSchedule_IsComplete.Waiting)
                        {
                            moneyOriginalNeedPay = item.MoneyOriginal - item.PayMoneyOriginal;
                            moneyServiceNeedPay = item.MoneyService - item.PayMoneyService;
                            moneyInterestNeedPay = item.MoneyInterest - item.PayMoneyInterest;
                            moneyConsultantNeedPay = item.MoneyConsultant - item.PayMoneyConsultant;
                            moneyMoneyFineLateNeedPay = item.MoneyFineLate - item.PayMoneyFineLate;
                            totalNeedPay = moneyOriginalNeedPay + moneyServiceNeedPay + moneyInterestNeedPay + moneyConsultantNeedPay + moneyMoneyFineLateNeedPay;
                            if (totalNeedPay == 0)
                            {
                                item.IsComplete = (int)PaymentSchedule_IsComplete.Paid;
                                item.ModifiedDate = dtNow;
                                item.CompletedDate = dtNow;
                                if (!item.FirstPaymentDate.HasValue)
                                {
                                    item.FirstPaymentDate = dtNow;
                                }
                            }
                        }
                    }
                    _paymentScheduleTab.UpdateBulk(lstPaymentSchedule);
                    //// lấy kỳ đầu tiên chưa hoàn thành gán lại cho loan
                    //var paymentDetailNext = lstPaymentSchedule.Where(x => x.IsVisible == false && x.IsComplete == (int)PaymentSchedule_IsComplete.Waiting).OrderBy(x => x.PayDate).FirstOrDefault();
                    //if (paymentDetailNext != null)
                    //{
                    //    if (paymentDetailNext.PayDate > nextDate)
                    //    {
                    //        updateLoanAfterPayment.LastDateOfPay = paymentDetailNext.FromDate.AddDays(-1);
                    //        updateLoanAfterPayment.NextDate = paymentDetailNext.PayDate;
                    //    }
                    //}
                    // insert listTransaction
                    _ = await _transactionService.CreateTransactionAfterPayment(lstTran);
                    /// 13/7 chuyển sang nghiệp vụ Loan xử lý
                    totalMoneyCustomerPaid = lstTran.Sum(x => x.TotalMoney);
                }
                response.SetSucces();
                response.Data = totalMoneyCustomerPaid;
            }
            catch (Exception ex)
            {
                _logger.LogError($"PayPaymentScheduleHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
