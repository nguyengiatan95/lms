﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AppServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AppLenderController : ControllerBase
    {
        RestClients.ILenderService _lenderService;
        LMS.Common.Helper.Utils _common;
        public AppLenderController(RestClients.ILenderService lenderService, RestClients.ILoanService loanService, RestClients.IUserService userService)
        {
            _lenderService = lenderService;
            _common = new LMS.Common.Helper.Utils();
        }

        [HttpPost]
        [Route("GetLstLoanByCondition")]
        public async Task<ActionResult> GetLstLoanByCondition(Domains.Models.RequestLoanForLender request)
        {
            var responseData = await _lenderService.GetLstLoanByCondition(request.FromDate, request.ToDate, request.DisbursementType, request.Status, request.LenderID);
            return Ok(responseData);
        }

        [HttpGet]
        [Route("GetDetailMoneyLoan")]
        public async Task<ActionResult> GetDetailMoneyLoan(long ShopID)
        {
            var responseData = await _lenderService.GetDetailMoneyLoan(ShopID);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("CreateLenderUser")]
        public async Task<ActionResult> CreateLenderUser(Domains.Models.RequestCreateLenderUser request)
        {
            var responseData = await _lenderService.CreateLenderUser(request);
            return Ok(responseData);
        }
        
    }
}