﻿using LMS.Common.Constants;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    public class CollectionController : ControllerBase
    {
        readonly RestClients.ICollectionService _collectionService;
        readonly RestClients.ILoanService _loanService;
        readonly RestClients.IUserService _userService;
        readonly LMS.Common.Helper.Utils _common;
        public CollectionController(RestClients.ICollectionService collectionService, RestClients.ILoanService loanService, RestClients.IUserService userService)
        {
            _collectionService = collectionService;
            _userService = userService;
            _loanService = loanService;
            _common = new LMS.Common.Helper.Utils();
        }
        [HttpGet]
        [Route("City/GetListCity")]
        public async Task<ActionResult> GetLstCity()
        {
            var responseData = await _collectionService.GetListCity();
            return Ok(responseData);
        }

        [HttpGet]
        [Route("Districts/GetDistricts/{cityId}")]
        public async Task<ActionResult> GetDistricts(long cityId)
        {
            var responseData = await _collectionService.GetDistrict(cityId);
            return Ok(responseData);
        }

        [HttpGet]
        [Route("Ward/GetWardByDistrictID/{districtID}")]
        public async Task<ActionResult> GetWardByDistrictID(long districtID)
        {
            var responseData = await _collectionService.GetWardByDistrictID(districtID);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("Pawn/LoadData_BadDebt")]
        public async Task<ActionResult> LoadDataBadDebt(Domains.Models.RequestGetLoan requestGet)
        {
            var responseData = await _collectionService.GetLoanByStaff(requestGet.TextSearch, requestGet.StaffID, requestGet.CityID, requestGet.DistrictID, requestGet.PageNumber, requestGet.PageSize, requestGet.Type);
            return Ok(responseData);
        }

        [HttpGet]
        [Route("Reason/GetReasonInfos")]
        public async Task<ActionResult> GetReasonInfos()
        {
            var responseData = await _collectionService.GetReasonInfo();
            return Ok(responseData);
        }
        [HttpGet]
        [Route("Comment/GetComment")]
        public async Task<ActionResult> GetComment(int LoanID)
        {
            var responseData = await _collectionService.GetListComment(LoanID);
            return Ok(responseData);
        }

        [HttpGet]
        [Route("Comment/GetListCommentCredit")]
        public async Task<ActionResult> GetListCommentCredit(int LoanID)
        {
            var responseData = await _collectionService.GetListLosComment(LoanID);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("Pawn/GetDataCloseContract")]
        public async Task<ActionResult> GetDataCloseContract(Domains.Models.RequestCloseData request)
        {
            var responseData = await _loanService.GetMoneyNeedCloseLoanByLoanID(request.LoanID, request.CloseTime, request.TimaLoanID);
            return Ok(responseData);
        }

        [HttpGet]
        [Route("Payment/GetPayment")]
        public async Task<ActionResult> GetPayment(int LoanID)
        {
            var responseData = await _loanService.GetPayment(LoanID);
            return Ok(responseData);
        }

        [HttpGet]
        [Route("Transaction/GetHistory/{loanid}")]
        public async Task<ActionResult> GetHistory(int LoanID)
        {
            var responseData = await _loanService.GetTransaction(LoanID);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("Pawn/GetFileContract")]
        public async Task<ActionResult> GetFileContract(Domains.Models.RequestByLoanID request)
        {
            var responseData = await _collectionService.GetImages(request.LoanID);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("ProvisionsCollection/InsertProvisionsCollection")]
        public async Task<ActionResult> InsertProvisionsCollection(Domains.Models.RequestInsertProvisionsCollection request)
        {
            long iTotalMoney = 0;
            long.TryParse(request.Money.Replace(",", "").Replace(".", ""), out iTotalMoney);
            long userID = _userService.GetHeaderUserID();
            string username = _userService.GetHeaderUserName();
            var preparationDate = DateTime.ParseExact(request.TimeScheduled, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture);
            var responseData = await _collectionService.InsertLMSPreparationCollection(request.LoanID, userID, username, request.strTextAccruedPeriods, iTotalMoney, (int)userID, request.NumberAccruedPeriods >= 1 ? 1 : request.NumberAccruedPeriods, preparationDate.ToString(_common.DateTimeDDMMYYYYHHMMSS), request.PreparationType);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("Pawn/GetLoanBadDebtByID")]
        public async Task<ActionResult> GetLoanBadDebtByID(Domains.Models.RequestByLoanID request)
        {
            var responseData = await _collectionService.GetLoanBadDebtByID(request.LoanID, request.UserID);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("SMS/GetListNeedSend")]
        public async Task<ActionResult> GetListNeedSend(Domains.Models.RequestSMS request)
        {
            var responseData = await _collectionService.GetListNeedSend(request.Phone, request.PageIndex, request.PageSize);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("SMS/UpdateStatusSendSms")]
        public async Task<ActionResult> UpdateStatusSendSms(Domains.Models.RequestUpdateSMS request)
        {
            var responseData = await _collectionService.UpdateStatusSendSms(request.SendSmsID, request.Status);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("Loan/GetMotorcycleSeizure")]
        public async Task<ActionResult> GettMotorcycleSeizure(Domains.Models.RequestMotorcycleSeizure request)
        {
            var responseData = await _collectionService.GetMotorcycleSeizure(request.StaffID, request.PageIndex, request.PageSize, request.GeneralSearch, request.CityID, request.DistrictID, request.WardID);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("HoldCustomerMoney/AddHoldCustomerMoney")]
        public async Task<ActionResult> AddHoldCustomerMoney(Domains.Models.RequestAddHoldCustomerMoney request)
        {
            request.CreateBy = _userService.GetHeaderUserID();
            var responseData = await _collectionService.AddHoldCustomerMoney(request.CustomerID, request.Amount, request.Reason, request.CreateBy);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("HoldCustomerMoney/UpdateStatus")]
        public async Task<ActionResult> UpdateStatus(Domains.Models.RequestDeleteHoldMoney request)
        {
            var responseData = await _collectionService.CancelHoldMoney(request.HoldCustomerMoneyID);
            return Ok(responseData);
        }

        [HttpGet]
        [Route("Meta/GetPreparationType")]
        public async Task<ActionResult> GetPreparationType()
        {
            var responseData = await _collectionService.GetPreparationType();
            return Ok(responseData);
        }

        [HttpPost]
        [Route("PaymentSchedule/AppGenPaymentSchedule")]
        public async Task<ActionResult> AppGenPaymentSchedule(Domains.Models.RequestGenPayment request)
        {
            var responseData = await _collectionService.AppGenPaymentSchedule(request.TotalMoneyDisbursement, request.LoanTime, request.RateType);
            return Ok(responseData);
        }


        [HttpGet]
        [Route("PaymentSchedule/GetLstPaymentByTimaLoanID")]
        public async Task<ActionResult> GetLstPaymentByTimaLoanID(long timaLoanID)
        {
            var responseData = await _collectionService.GetLstPaymentByTimaLoanID(timaLoanID);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("Loan/GenDebtRestructuring")]
        public async Task<ActionResult> GenDebtRestructuring(Domains.Models.RequestGenDebtRestructuring request)
        {
            var responseData = await _collectionService.GenDebtRestructuring(request.LoanID, request.TimaLoanID, request.TypeDebtRestructuring, request.NumberOfRepaymentPeriod);
            return Ok(responseData);
        }

        [HttpGet]
        [Route("AI/ListCheckInOut/{code}")]
        public async Task<ActionResult> ListCheckInOut(string code)
        {
            var responseData = await _collectionService.ListCheckInOut(code);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("Ai/CheckInOut")]
        public async Task<ActionResult> CheckInOut(Domains.Models.RequestAddCheckInOut request)
        {
            var responseData = await _collectionService.AddCheckInOut(request.Contractid, request.Act, request.Lng, request.Lat, request.Location, request.Code, request.Uid, request.Vc, request.Appk, request.Iid, request.T);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("CommentDebtPrompted/CreateCommentDebtPrompted")]
        public async Task<ActionResult> CreateCommentDebtPrompted(Domains.Models.RequestCreateComment request)
        {
            var responseData = await _collectionService.CreateComment(request.LoanID, request.UserID, request.FullName, request.Comment, request.ReasonCodeID, request.ReasonCode, request.FileImage, request.AppointmentDate, request.ActionPerson, request.ActionAddress);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("PlanHandleLoanDaily/CreatePlanHandleLoanDaily")]
        public async Task<ActionResult> CreatePlanHandleLoanDaily(Domains.Models.RequestCreatePlanHandleLoanDaily request)
        {
            var responseData = await _collectionService.CreatePlanHandleLoanDaily(request.UserID, request.FromDate, request.ToDate, request.LoanID);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("PlanHandleLoanDaily/PlanHandleLoanDaily")]
        public async Task<ActionResult> PlanHandleLoanDaily(Domains.Models.RequestGetPlanHandleLoanDaily request)
        {
            var responseData = await _collectionService.GetPlanHandleLoanDaily(request.UserID, request.FromDate, request.ToDate);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("PlanHandleLoanDaily/PlanHandleLoanDailyDetails")]
        public async Task<ActionResult> PlanHandleLoanDailyDetails(Domains.Models.RequestGetPlanHandleLoanDaily request)
        {
            var responseData = await _collectionService.PlanHandleLoanDailyDetails(request.UserID, request.FromDate, request.ToDate);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("PlanHandleLoanDaily/UpdatePlanHandleLoanDaily")]
        public async Task<ActionResult> UpdatePlanHandleLoanDaily(Domains.Models.RequestUpdatePlanHandleLoanDaily request)
        {
            var responseData = await _collectionService.UpdatePlanHandleLoanDaily(request.UserID, request.PlanHandleLoanDailyID, request.Status);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("PlanHandleLoanDaily/MovePlanHandleLoanDaily")]
        public async Task<ActionResult> MovePlanHandleLoanDaily(Domains.Models.RequestMovePlanHandleLoanDaily request)
        {
            var responseData = await _collectionService.MovePlanHandleLoanDaily(request.UserID, request.LstLoanID);
            return Ok(responseData);
        }


        [HttpGet]
        [Route("Department/GetDepartment")]
        public async Task<ActionResult> GetDepartment(long parentID)
        {
            var responseData = await _collectionService.GetDepartment(parentID);
            return Ok(responseData);
        }


        [HttpPost]
        [Route("PlanHandleLoanDaily/AppPlanDaily")]
        public async Task<ActionResult> AppPlanDaily(Domains.Models.RequestAppPlanDaily request)
        {
            var responseData = await _collectionService.AppPlanDaily(request.SearchDate, request.DepartmentID, request.LeadID, request.StaffID);
            return Ok(responseData);
        }
        [HttpGet]
        [Route("Department/GetTeamLead/{DepartmentID}")]
        public async Task<ActionResult> GetTeamLead(long DepartmentID)
        {
            var responseData = await _collectionService.GetTeamLead(DepartmentID);
            return Ok(responseData);
        }
        [HttpPost]
        [Route("User/GetStaff")]
        public async Task<ActionResult> GetStaff(Domains.Models.RequestGetStaff requestGet)
        {
            var responseData = await _collectionService.GetStaff(requestGet.DepartmentID, requestGet.ParentUserID);
            return Ok(responseData);
        }

        [HttpGet]
        [Route("AppTHN/GetSummaryPlanLoanHandleByStaff/{UserID}")]
        public async Task<ActionResult> GetSummaryPlanLoanHandleByStaff(long UserID)
        {
            var responseData = await _collectionService.GetSummaryPlanLoanHandleByStaff(UserID);
            return Ok(responseData);
        }

        [HttpPost]
        [Route("User/GetPositionUser")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetPositionUser(Domains.Models.RequestGetPositionUser request)
        {
            return await _collectionService.GetPositionUser(request.UserID);
        }

        [HttpPost]
        [Route("Customer/GetHistoryCustomerTopup")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetHistoryCustomerTopup(Domains.Models.RequestGetHistoryCustomerTopup request)
        {
            return await _collectionService.GetHistoryCustomerTopup(request);
        }

        [HttpPost]
        [Route("AppTHN/GetSummaryEmployeeHandleLoanByLstDeptID")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetSummaryEmployeeHandleLoanByLstDeptID(Domains.Models.RequestGetSummaryEmployeeHandleLoanByLstDeptID request)
        {
            return await _collectionService.GetSummaryEmployeeForPlanHandleLoanByLstDepartmentID(request);
        }


        [HttpGet]
        [Route("Pawn/GetTrandata/{loanID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetTrandata(long loanID)
        {
            return await _collectionService.GetTrandata(loanID);
        }
    }
}
