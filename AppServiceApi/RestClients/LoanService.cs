﻿using LMS.Common.Constants;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.RestClients
{
    public interface ILoanService
    {
        Task<LMS.Common.Constants.ResponseActionResult> GetMoneyNeedCloseLoanByLoanID(long loanID, string closeDate, long timaLoanID);
        Task<ResponseActionResult> GetPayment(long loanID);
        Task<ResponseActionResult> GetTransaction(long loanID);
    }
    public class LoanService : ILoanService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        ILogger<LoanService> _logger;
        LMS.Common.Helper.Utils _common;
        public LoanService(LMS.Common.Helper.IApiHelper apiHelper,
          ILogger<LoanService> logger)
        {
            _apiHelper = apiHelper;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> GetMoneyNeedCloseLoanByLoanID(long loanID, string closeDate, long timaLoanID)
        {
            Domains.Models.DataCloseContract objData = null;
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{LMS.Common.Constants.ActionApiInternal.Loan_GetMoneyNeedCloseLoanByLoanID}";
            try
            {
                var request = new
                {
                    LoanID = loanID,
                    CloseDate = closeDate,
                    TimaLoanID = timaLoanID
                };
                var dataAPI = await _apiHelper.ExecuteAsync<LMS.Entites.Dtos.LoanServices.MoneyNeedCloseLoan>(url: url, method: RestSharp.Method.POST, request);
                objData = new Domains.Models.DataCloseContract()
                {
                    CloseTime = closeDate,
                    DebitMoney = dataAPI.MoneyFineLate,
                    TotalDay = dataAPI.OverDate,
                    FlagCheck = 1,
                    TotalMoneyCurrent = dataAPI.MoneyOriginal,
                    MoneyConsultant = dataAPI.MoneyConsultant,
                    MoneyInterest = dataAPI.MoneyInterest,
                    MoneyService = dataAPI.MoneyService,
                    InterestMoney = dataAPI.TotalInterest,
                    MoneyNeed = dataAPI.TotalMoneyCloseLoan,
                    PunishMoney = dataAPI.MoneyFineOriginal
                };
                response.Data = objData;
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"LoanService_GetMoneyNeedCloseLoanByLoanID|url={url}|loanID={loanID}|closeDate={closeDate}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> GetPayment(long loanID)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            List<Domains.Models.Payment> payments = new List<Domains.Models.Payment>();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{LMS.Common.Constants.ActionApiInternal.Loan_GetLstPaymentScheduleByLoanID}/{loanID}";
            try
            {
                var dataAPI = await _apiHelper.ExecuteAsync<List<LMS.Entites.Dtos.PaymentServices.PaymentScheduleModel>>(url: url);
                if (dataAPI != null && dataAPI.Count > 0)
                {
                    foreach (var item in dataAPI)
                    {
                        payments.Add(new Domains.Models.Payment()
                        {
                            MoneyConsultant = item.MoneyConsultant,
                            FromDate = item.FromDate,
                            ToDate = item.ToDate,
                            MoneyOriginal = item.MoneyOriginal,
                            MoneyInterest = item.MoneyInterest,
                            MoneyService = item.MoneyService,
                            OtherMoney = item.MoneyFineInterestLate,
                            PayMoney = item.TotalCustomerPaid,
                            PayNeed = item.TotalCustomerNeedPay,
                            CountDate = item.CountDay,
                            Done = item.IsComplete == 1 ? true : false
                        });
                    }
                }
                response.SetSucces();
                response.Data = payments;
            }
            catch (Exception ex)
            {
                _logger.LogError($"LoanService_GetPayment|url={url}|loanID={loanID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> GetTransaction(long loanID)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            List<Domains.Models.TransactionAPP> transations = new List<Domains.Models.TransactionAPP>();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{LMS.Common.Constants.ActionApiInternal.Loan_GetTransactionByLoanID}/{loanID}";
            try
            {
                var dataAPI = await _apiHelper.ExecuteAsync<List<LMS.Entites.Dtos.TransactionServices.ListTransactionView>>(url: url);
                if (dataAPI != null && dataAPI.Count > 0)
                {
                    foreach (var item in dataAPI)
                    {
                        transations.Add(new Domains.Models.TransactionAPP()
                        {
                            CreateDate = item.CreateDate,
                            MoneyAdd = item.TotalMoney > 0 ? item.TotalMoney : 0,
                            MoneySub = item.TotalMoney < 0 ? item.TotalMoney : 0,
                            Note = item.ActionName,
                            FullName = item.UserName
                        });
                    }
                }
                response.SetSucces();
                response.Data = transations;
            }
            catch (Exception ex)
            {
                _logger.LogError($"LoanService_GetTransaction|url={url}|loanID={loanID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }


    }
}
