﻿using AppServiceApi.Domains.Models;
using LMS.Common.Constants;
using LMS.Entites.Dtos.LenderService;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.RestClients
{
    public interface ILenderService
    {
        Task<ResponseDataForAppLender> GetLstLoanByCondition(string fromDate, string toDate, int disbursementType, int status, int lenderID);

        Task<ResponseDataForAppLender> GetDetailMoneyLoan(long lenderID);
        Task<ResponseDataForAppLender> CreateLenderDigitalSignature(Domains.Models.RequestCreateLenderSignature request);
        Task<ResponseDataForAppLender> CreateLenderUser(Domains.Models.RequestCreateLenderUser request);
        
    }
    public class LenderService : ILenderService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        ILogger<LenderService> _logger;
        LMS.Common.Helper.Utils _common;
        public LenderService(LMS.Common.Helper.IApiHelper apiHelper,
            ILogger<LenderService> logger)
        {
            _apiHelper = apiHelper;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseDataForAppLender> CreateLenderDigitalSignature(RequestCreateLenderSignature request)
        {
            ResponseDataForAppLender response = new ResponseDataForAppLender();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.LenderService}{LMS.Common.Constants.ActionApiInternal.Lender_CreateLenderDigitalSignature}";
            try
            {
                var lstResponseAPI = await _apiHelper.ExecuteAsync<long>(url: url, method: RestSharp.Method.POST, request);
                response.SetSucces();
                response.Data = lstResponseAPI;
            }
            catch (Exception ex)
            {
                _logger.LogError($"LenderService_CreateLenderDigitalSignature|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseDataForAppLender> CreateLenderUser(RequestCreateLenderUser request)
        {
            ResponseDataForAppLender response = new ResponseDataForAppLender();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.LenderService}{LMS.Common.Constants.ActionApiInternal.Lender_CreateLenderUser}";
            try
            {
                var lstResponseAPI = await _apiHelper.ExecuteAsync<long>(url: url, method: RestSharp.Method.POST, request);
                if(lstResponseAPI > 0)
                {
                    response.SetSucces();
                    response.Data = lstResponseAPI;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"LenderService_CreateLenderUser|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseDataForAppLender> GetDetailMoneyLoan(long lenderID)
        {
            ResponseDataForAppLender response = new ResponseDataForAppLender();
            List<LenderDetailMoneyLoan> loanLender = new List<LenderDetailMoneyLoan>();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.LenderService}{LMS.Common.Constants.ActionApiInternal.Lender_GetDetailMoneyLoan}/{lenderID}";
            try
            {
                var lstResponseAPI = await _apiHelper.ExecuteAsync<List<LenderDetailMoneyLoan>>(url: url, method: RestSharp.Method.GET);
                response.SetSucces();
                response.Data = lstResponseAPI;
            }
            catch (Exception ex)
            {
                _logger.LogError($"LenderService_GetLstLoanByCondition|url={url}|lenderID={_common.ConvertObjectToJSonV2(lenderID)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseDataForAppLender> GetLstLoanByCondition(string fromDate, string toDate, int disbursementType, int status, int lenderID)
        {
            ResponseDataForAppLender response = new ResponseDataForAppLender();
            LoanLenderView loanLender = new LoanLenderView();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.LenderService}{LMS.Common.Constants.ActionApiInternal.Lender_GetLoanForLender}";
            var request = new
            {
                FromDate = fromDate,
                ToDate = toDate,
                DisbursementType = disbursementType,
                Status = status,
                LenderID = lenderID
            };
            try
            {
                var lstResponseAPI = await _apiHelper.ExecuteAsync<LoanLenderView>(url: url, method: RestSharp.Method.POST, request);
                response.SetSucces();
                response.Data = lstResponseAPI;
            }
            catch (Exception ex)
            {
                _logger.LogError($"LenderService_GetLstLoanByCondition|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
