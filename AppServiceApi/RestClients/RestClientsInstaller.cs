﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.RestClients
{
    public static class RestClientsInstaller
    {
        public static IServiceCollection AddRestClientsService(this IServiceCollection services)
        {
            services.AddTransient(typeof(ICollectionService), typeof(CollectionService));
            services.AddTransient(typeof(ILoanService), typeof(LoanService));
            services.AddTransient(typeof(IUserService), typeof(UserService));
            services.AddTransient(typeof(ILenderService), typeof(LenderService));
            return services;
        }
    }
}
