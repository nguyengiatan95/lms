﻿using AppServiceApi.Domains.Models;
using LMS.Common.Constants;
using LMS.Common.Helper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.RestClients
{
    public interface ICollectionService
    {
        Task<LMS.Common.Constants.ResponseActionResult> GetListCity();
        Task<LMS.Common.Constants.ResponseActionResult> GetDistrict(long cityID);
        Task<LMS.Common.Constants.ResponseActionResult> GetLoanByStaff(string TextSearch, int StaffID, int CityID, int DistrictID, int PageNumber, int PageSize, int Type = (int)StatusCommon.SearchAll);
        Task<LMS.Common.Constants.ResponseActionResult> GetReasonInfo();
        Task<LMS.Common.Constants.ResponseActionResult> GetListComment(long loanID);
        Task<LMS.Common.Constants.ResponseActionResult> GetListLosComment(long loanID);

        Task<LMS.Common.Constants.ResponseActionResult> GetImages(long loanID);

        Task<LMS.Common.Constants.ResponseActionResult> InsertLMSPreparationCollection(long loanID, long userID, string fullName, string description, long totalMoney, int createBy, long typeID, string preparationDate, int preparationType);

        Task<LMS.Common.Constants.ResponseActionResult> GetLoanBadDebtByID(long loanID, long userID);

        Task<LMS.Common.Constants.ResponseActionResult> GetListNeedSend(string phone, int pageIndex, int pageSize);

        Task<LMS.Common.Constants.ResponseActionResult> UpdateStatusSendSms(long sendSmsID, int status);

        Task<LMS.Common.Constants.ResponseActionResult> GetMotorcycleSeizure(int staffID, int pageIndex, int pageSize, string generalSearch, int cityID, int districtID, int wardID);
        Task<ResponseActionResult> AddHoldCustomerMoney(long customerID, long amount, string reason, long userID);
        Task<ResponseActionResult> CancelHoldMoney(long holdCustomerMoneyID);

        Task<LMS.Common.Constants.ResponseActionResult> GetWardByDistrictID(long districtID);
        Task<LMS.Common.Constants.ResponseActionResult> GetPreparationType();
        Task<LMS.Common.Constants.ResponseActionResult> AppGenPaymentSchedule(long totalMoneyDisbursement, int loanTime, int rateType);

        Task<LMS.Common.Constants.ResponseActionResult> GetLstPaymentByTimaLoanID(long timaLoanID);
        Task<LMS.Common.Constants.ResponseActionResult> GenDebtRestructuring(long loanID, long timaLoanID, int typeDebtRestructuring, int numberOfRepaymentPeriod);
        Task<LMS.Common.Constants.ResponseActionResult> ListCheckInOut(string code);
        Task<LMS.Common.Constants.ResponseActionResult> AddCheckInOut(long contractid, string act, double lng, double lat, string location, string code, int uid, string vc, string appk, string iid, long t);

        Task<LMS.Common.Constants.ResponseActionResult> CreateComment(long loanID, long userID, string fullName, string comment, long reasonCodeID, string reasonCode, List<LMS.Entites.Dtos.AG.ImageLosUpload> fileImage, string appointmentDate, short actionPerson, short actionAddress);
        Task<LMS.Common.Constants.ResponseActionResult> CreatePlanHandleLoanDaily(long userID, string fromDate, string toDate, long loanID);
        Task<LMS.Common.Constants.ResponseActionResult> GetPlanHandleLoanDaily(long userID, string fromDate, string toDate);
        Task<LMS.Common.Constants.ResponseActionResult> PlanHandleLoanDailyDetails(long userID, string fromDate, string toDate);
        Task<LMS.Common.Constants.ResponseActionResult> UpdatePlanHandleLoanDaily(long userID, long planHandleLoanDailyID, int status);
        Task<LMS.Common.Constants.ResponseActionResult> MovePlanHandleLoanDaily(long userID, List<long> LstLoanID);
        Task<LMS.Common.Constants.ResponseActionResult> GetDepartment(long parentID);

        Task<LMS.Common.Constants.ResponseActionResult> AppPlanDaily(string searchDate, long departmentID, long leadID, long staffID);
        Task<LMS.Common.Constants.ResponseActionResult> GetTeamLead(long departmentID);
        Task<LMS.Common.Constants.ResponseActionResult> GetStaff(long departmentID, long parentUserID);
        Task<LMS.Common.Constants.ResponseActionResult> GetSummaryPlanLoanHandleByStaff(long userID);
        Task<LMS.Common.Constants.ResponseActionResult> GetPositionUser(long userID);
        Task<LMS.Common.Constants.ResponseActionResult> GetHistoryCustomerTopup(Domains.Models.RequestGetHistoryCustomerTopup request);
        Task<LMS.Common.Constants.ResponseActionResult> GetSummaryEmployeeForPlanHandleLoanByLstDepartmentID(Domains.Models.RequestGetSummaryEmployeeHandleLoanByLstDeptID request);
        Task<LMS.Common.Constants.ResponseActionResult> GetTrandata(long loanID);
    }
    public class CollectionService : ICollectionService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        ILogger<CollectionService> _logger;

        LMS.Common.Helper.Utils _common;
        public CollectionService(LMS.Common.Helper.IApiHelper apiHelper,
            ILogger<CollectionService> logger)
        {
            _apiHelper = apiHelper;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> GetListCity()
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            List<City> lstCityInfos = new List<City>();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_Meta_GetLstCity}";
            try
            {
                var lstCityResponse = await _apiHelper.ExecuteAsync<List<Domains.Models.SelectItem>>(url: url);
                if (lstCityResponse != null && lstCityResponse.Count > 0)
                {
                    foreach (var item in lstCityResponse)
                    {
                        lstCityInfos.Add(new City
                        {
                            CityName = item.Text,
                            ID = Convert.ToInt32(item.Value)
                        });
                    }
                }
                response.SetSucces();
                response.Data = lstCityInfos;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GetListCity|url={url}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> GetDistrict(long cityID)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            List<District> districtInfos = new List<District>();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_Meta_GetLstDistrictByCity}/{cityID}";
            try
            {
                var lstCityResponse = await _apiHelper.ExecuteAsync<List<Domains.Models.SelectItem>>(url: url);
                if (lstCityResponse != null && lstCityResponse.Count > 0)
                {
                    foreach (var item in lstCityResponse)
                    {
                        districtInfos.Add(new District
                        {
                            Name = item.Text,
                            ID = Convert.ToInt32(item.Value)
                        });
                    }
                }
                response.SetSucces();
                response.Data = districtInfos;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GetDistrict|url={url}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> GetLoanByStaff(string TextSearch, int StaffID, int CityID, int DistrictID, int PageNumber, int PageSize, int Type = (int)StatusCommon.SearchAll)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            List<DataLoan> dataloans = new List<DataLoan>();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_AppTHN_ListLoanBeDivided}";
            try
            {
                var request = new
                {
                    PageIndex = PageNumber,
                    PageSize = PageSize,
                    KeySearch = TextSearch,
                    EmployeeID = StaffID,
                    CityID = CityID,
                    DistrictID = DistrictID,
                    TypeID = Type
                };
                var responseTable = await _apiHelper.ExecuteAsync<IEnumerable<LMS.Entites.Dtos.CollectionServices.Loan.LoanViewNew>>(url: url, method: RestSharp.Method.POST, request);
                var loanStaff = responseTable;
                if (loanStaff != null && loanStaff.Count() > 0)
                {
                    foreach (var item in loanStaff)
                    {
                        dataloans.Add(new DataLoan
                        {
                            Address = item.PresentAddress,
                            CodeID = item.ContractCode,
                            CustomerName = item.CustomerName,
                            //AlarmDate = item.AlarmDate,
                            //LoanCreditID = item.LoanBriefID,
                            StrOverDay = item.OverdueDays.ToString(),
                            StrStatusSendInsurance = item.LoanStatusInsuranceName,
                            TotalMoneyCurrent = item.TotalMoneyCurrent,
                            ID = item.LoanID,
                            //TypeName = ((AssignmentLoan_Type)item.AssignmentLoanType).GetDescription(),
                            Location = item.Location,
                            PlanLoanHandle = item.PlanLoanHandle,
                            LoanBriefPropertyPlateNumber = item.LoanBriefPropertyPlateNumber,
                            HouseholdAddress = item.HouseholdAddress,
                            Handling = item.Handling,
                            ShowPTP = !string.IsNullOrWhiteSpace(item.ReasonCode),
                            LstAssignmentLoanType = item.LstAssignmentLoanType,
                            TotalMoneyNeedPay = item.TotalMoneyNeedPay
                        });
                    }
                }
                response.SetSucces();
                response.Data = dataloans;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GetLoanByStaff|url={url}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> GetReasonInfo()
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            List<ReasonInfo> reasons = new List<ReasonInfo>();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_Reason_GetReason}?platform=1";
            try
            {
                var lstCityResponse = await _apiHelper.ExecuteAsync<List<Domains.Models.TblReason>>(url: url);
                if (lstCityResponse != null && lstCityResponse.Count > 0)
                {
                    foreach (var item in lstCityResponse)
                    {
                        reasons.Add(new ReasonInfo
                        {
                            Id = (int)item.ReasonID,
                            Reason = item.Description,
                            ReasonCode = item.ReasonCode,
                        });
                    }
                }
                response.SetSucces();
                response.Data = reasons;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GetReasonInfo|url={url}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> GetListComment(long loanID)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            List<CommentDebtPrompted> comments = new List<CommentDebtPrompted>();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_Comment_GetLstCommentDebtPromptedByLoanID}";
            try
            {
                var request = new
                {
                    PageIndex = 1,
                    PageSize = 100000,
                    loanID = loanID,
                };

                var lstResponseAPI = await _apiHelper.ExecuteAsync<IEnumerable<GetLstCommentDebtPromptedByLoanIDItem>>(url: url, method: RestSharp.Method.POST, request);
                if (lstResponseAPI != null && lstResponseAPI.Count() > 0)
                {
                    foreach (var item in lstResponseAPI)
                    {
                        var comment = new CommentDebtPrompted
                        {
                            Comment = System.Net.WebUtility.HtmlDecode(_common.RemoveHtmlTag(item.Comment)),
                            Reason = item.ReasonCode,
                            CreateDate = item.CreateDate,
                            FullName = item.FullName,
                            StrActionPeron = item.StrActionPeron,
                            StrActionAddress = item.StrActionAddress,
                        };
                        comment.lstFile = new List<FileUploadTHN>();
                        if (item.FileImage != null && item.FileImage.Any())
                        {
                            foreach (var images in item.FileImage)
                            {
                                comment.lstFile.Add(new FileUploadTHN()
                                {
                                    FilePath = images.FullPath
                                });
                            }
                        }
                        comments.Add(comment);
                    }
                }
                response.SetSucces();
                response.Data = comments;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GetListComment|url={url}|loanid={loanID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<LMS.Common.Constants.ResponseActionResult> GetListLosComment(long loanID)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_THN_LosHistoryComment}";
            try
            {
                var request = new
                {
                    PageIndex = 1,
                    PageSize = 100000,
                    loanID = loanID,
                };
                var lstResponseAPI = await _apiHelper.ExecuteAsync<IEnumerable<HistoryCommentCredit>>(url: url, method: RestSharp.Method.POST, request);
                foreach (var item in lstResponseAPI)
                {
                    item.Comment = System.Net.WebUtility.HtmlDecode(_common.RemoveHtmlTag(item.Comment));
                }
                response.SetSucces();
                response.Data = lstResponseAPI;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GetListlosComment|url={url}|loanid={loanID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<LMS.Common.Constants.ResponseActionResult> GetImages(long loanID)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_THN_GetImages}";
            try
            {
                var request = new
                {
                    loanID = loanID,
                };
                var lstResponseAPI = await _apiHelper.ExecuteAsync<List<ResultListImage>>(url: url, method: RestSharp.Method.POST, request);
                List<FileContract> lstFileContract = new List<FileContract>();
                if (lstResponseAPI != null && lstResponseAPI.Any())
                {
                    foreach (var item in lstResponseAPI)
                    {
                        foreach (var child in item.LstFilePath)
                        {
                            lstFileContract.Add(new FileContract()
                            {
                                FullPathImage = child.FilePath,
                                FilePath = child.FilePath,
                            });
                        }
                    }
                }
                response.SetSucces();
                response.Data = lstFileContract;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GetImages|url={url}|loanid={loanID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> InsertLMSPreparationCollection(long loanID, long userID, string fullName, string description, long totalMoney, int createBy, long typeID, string preparationDate, int preparationType)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_PreparationCollectionLoan_CreatePreparationCollectionLoan}";
            var request = new
            {
                LoanID = loanID,
                UserID = userID,
                FullName = fullName,
                Description = description,
                TotalMoney = totalMoney,
                CreateBy = createBy,
                TypeID = typeID,
                PreparationDate = preparationDate,
                PreparationType = preparationType
            };
            try
            {

                response = await _apiHelper.ExecuteAsync<ResponseActionResult>(url: url, method: RestSharp.Method.POST, request);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_InsertLMSPreparationCollection|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> GetLoanBadDebtByID(long loanID, long userID)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            PaymentNotify paymentNotify = null;
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_Loan_GetLoanCreditDetailLosByLoanID}/{loanID}?UserID={userID}";
            try
            {
                var dataLos = await _apiHelper.ExecuteAsync<MapResultLoanWaitingDisbursementDetailLOS>(url: url);
                if (dataLos != null)
                {
                    paymentNotify = new PaymentNotify()
                    {
                        //AccountName = dataLos.BankAccountName,
                        BankAccountName = dataLos.BankAccountName,
                        BankAccountNumber = dataLos.BankAccountNumber,
                        AddressCompany = dataLos.CompanyAddress,
                        AddressHouseHold = dataLos.HouseOldAddress,
                        AddressPersonFamily = dataLos.AddressPersonFamily,
                        AgencyName = dataLos.LenderCode,
                        Birthday = dataLos.Dob,
                        BankName = dataLos.BankName,
                        CodeID = int.Parse(dataLos.ContactCodeLMS.Replace("TC-", "")),
                        LoanID = loanID,
                        //TotalMoneyCurrent =  
                        //AccountNo = dataLos.acc
                        CusID = (int)dataLos.CustomerId,
                        CusPhone = dataLos.Phone,
                        CusName = dataLos.FullName,
                        CusAddress = dataLos.CustomerAddress,
                        //Note = dataLos.InsurenceNote,
                        Status = dataLos.Status,
                        FromDate = dataLos.FromDateLMS,
                        //NextDate = dataLos.
                        //PaymentMoney = dataLos.pay
                        PlatformType = dataLos.PlatformType,
                        IdOfPartenter = dataLos.LoanBriefId.ToString(),
                        ToDate = dataLos.ToDateLMS,
                        NextDate = dataLos.NextDate,
                        NumberCard = dataLos.NationalCard,
                        ProductName = dataLos.ProductName,
                        CityName = dataLos.HouseOldCityName,
                        DistrictName = dataLos.DistrictName,
                        CompanyName = dataLos.CompanyName,
                        CompanyPhone = dataLos.CompanyPhone,
                        MoneyInBag = dataLos.CustomerTotalMoney,
                        Gender = dataLos.Gender,
                        DistrictNameHouseHold = dataLos.HouseOldDistrictName,
                        CityNameHouseHold = dataLos.HouseOldCityName,
                        LoanCreditID = dataLos.LoanBriefId,
                        LoanTotalMoney = (long)dataLos.LoanAmountFinal,
                        Facebook = dataLos.FacebookAddress,
                        StrStatus = dataLos.StatusLMS,
                        CompanyTaxCode = dataLos.CompanyTaxCode,
                        CompanyCityName = dataLos.CompanyProvinceName,
                        CompanyDistrictName = dataLos.CompanyDistrictName,
                        Salary = (long)dataLos.TotalIncome,
                        JobName = dataLos.JobTitle,
                        TotalMoneyCollectOfCustomer = dataLos.TotalMoneyLoanPaid,
                        PlanHandleLoanStatus = dataLos.PlanHandleLoanStatus,
                        PlanHandleLoanExpiredType = dataLos.PlanHandleLoanExpiredType,
                        PlanHandleLoanCheckInLocationName = dataLos.PlanHandleLoanCheckInLocationName,
                        PlanHandleCodeCheckIn = dataLos.PlanHandleCodeCheckIn,
                        DebtRelief = dataLos.DebtRelief,
                        DebtStructure = dataLos.DebtStructure,
                        TotalInterestNeedPay = dataLos.TotalInterestNeedPayOnApp,
                        TotalMoneyNeedPay = dataLos.TotalMoneyNeedPayOnApp,
                        MoneyOriginalNeedPay = dataLos.MoneyOriginalNeedPayOnApp,
                        MoneyFineLateNeedPay = dataLos.MoneyFineLateNeedPayOnApp,
                        MoneyFineOriginalNeedPay = dataLos.MoneyFineOriginalNeedPayOnApp,
                        PlanHandleLoanCheckInStatus = dataLos.PlanHandleLoanCheckInStatus,
                        HoldCustomerMoneyID = dataLos.HoldCustomerMoneyID,
                        EsignBorrowerContract = dataLos.EsignBorrowerContract
                    };
                    paymentNotify.lstRelationship = new List<CustomerRelationshipInfo>();
                    if (dataLos.Relationship != null && dataLos.Relationship.Any())
                    {
                        foreach (var item in dataLos.Relationship)
                        {
                            paymentNotify.lstRelationship.Add(new CustomerRelationshipInfo()
                            {
                                FullName = item.FullName,
                                RelationshipName = item.RelationshipName,
                                Phone = item.Phone,
                                Address = item.Address,
                                RelationshipType = item.RelationshipType,
                                Id = item.Id
                            });
                        }
                    }
                    paymentNotify.lstBankVA = new List<BankVA>();
                    if (dataLos.LstBankCustomer != null && dataLos.LstBankCustomer.Any())
                    {
                        foreach (var item in dataLos.LstBankCustomer)
                        {
                            paymentNotify.lstBankVA.Add(new BankVA()
                            {
                                AccountName = item.AccountName,
                                AccountNumber = item.AccountNo,
                                BankName = item.BankName,
                            });
                        }
                    }

                    response.SetSucces();
                    response.Data = paymentNotify;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GetLoanBadDebtByID|url={url}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> GetListNeedSend(string phone, int pageIndex, int pageSize)
        {

            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_SendSms_GetLstSendSms}";
            var request = new
            {
                Status = (int)SendSms_Status.Wait,
                PhoneSend = phone,
                PageIndex = pageIndex,
                PageSize = pageSize
            };
            try
            {
                var lstResponseAPI = await _apiHelper.ExecuteAsync<List<ListSMSView>>(url: url, method: RestSharp.Method.POST, request);
                response.SetSucces();
                response.Data = lstResponseAPI;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GetListNeedSend|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> UpdateStatusSendSms(long sendSmsID, int status)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_SendSms_UpdateStatusSendSms}";
            var request = new
            {
                Status = status,
                sendSmsID = sendSmsID
            };
            try
            {
                var lstResponseAPI = await _apiHelper.ExecuteAsync(url: url, method: RestSharp.Method.POST, request);
                response.SetSucces();
                response.Data = lstResponseAPI;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_UpdateStatusSendSms|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> GetMotorcycleSeizure(int staffID, int pageIndex, int pageSize, string generalSearch, int cityID, int districtID, int wardID)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_Loan_GetMotorcycleSeizure}";
            var request = new
            {
                StaffID = staffID,
                PageIndex = pageIndex,
                PageSize = pageSize,
                GeneralSearch = generalSearch,
                CityID = cityID,
                DistrictID = districtID,
                WardID = wardID
            };
            try
            {
                var lstResponseAPI = await _apiHelper.ExecuteAsync<List<LMS.Entites.Dtos.CollectionServices.Loan.MotorcycleSeizureModel>>(url: url, method: RestSharp.Method.POST, request);
                response.SetSucces();
                response.Data = lstResponseAPI;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GetMotorcycleSeizure|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> AddHoldCustomerMoney(long customerID, long amount, string reason, long userID)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            if (string.IsNullOrEmpty(reason))
            {
                response.Message = MessageConstant.HoldCustomerMoneyReasonEmpty;
                return response;
            }
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_HoldCustomerMoney_CreateHoldCustomerMoney}";
            var request = new
            {
                CustomerID = customerID,
                Amount = amount,
                Reason = reason,
                CreateBy = userID
            };
            try
            {
                var lstResponseAPI = await _apiHelper.ExecuteAsync<long>(url: url, method: RestSharp.Method.POST, request);
                response.SetSucces();
                response.Data = lstResponseAPI;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_AddHoldCustomerMoney|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> CancelHoldMoney(long holdCustomerMoneyID)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_HoldCustomerMoney_UpdateHoldCustomerMoney}";
            var request = new
            {
                HoldCustomerMoneyID = holdCustomerMoneyID,
                Status = (int)HoldCustomerMoney_Status.Delete
            };
            try
            {
                var lstResponseAPI = await _apiHelper.ExecuteAsync<long>(url: url, method: RestSharp.Method.POST, request);
                response.SetSucces();
                response.Data = lstResponseAPI;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_CancelHoldMoney|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task<ResponseActionResult> GetWardByDistrictID(long districtID)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            List<Wards> wards = new List<Wards>();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_Meta_GetWardByDistrictID}/{districtID}";
            try
            {
                var lstWards = await _apiHelper.ExecuteAsync<List<Domains.Models.SelectItem>>(url: url);
                if (lstWards != null && lstWards.Count > 0)
                {
                    foreach (var item in lstWards)
                    {
                        wards.Add(new Wards
                        {
                            Name = item.Text,
                            ID = Convert.ToInt32(item.Value)
                        });
                    }
                }
                response.SetSucces();
                response.Data = wards;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetWardByDistrictID|url={url}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> GetPreparationType()
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_Meta_GetPreparationType}";
            try
            {
                var lstResponse = await _apiHelper.ExecuteAsync<List<Domains.Models.SelectItem>>(url: url);
                response.SetSucces();
                response.Data = lstResponse;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetWardByDistrictID|url={url}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> AppGenPaymentSchedule(long totalMoneyDisbursement, int loanTime, int rateType)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.PaymentService}{LMS.Common.Constants.ActionApiInternal.PaymentSchedule_AppGenPaymentSchedule}";
            var request = new
            {
                TotalMoneyDisbursement = totalMoneyDisbursement,
                LoanTime = loanTime,
                RateType = rateType
            };
            try
            {
                var data = await _apiHelper.ExecuteAsync<List<LMS.Entites.Dtos.PaymentServices.PaymentScheduleModel>>(url: url, method: RestSharp.Method.POST, request);
                response.SetSucces();
                response.Data = data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_AppGenPaymentSchedule|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> GetLstPaymentByTimaLoanID(long timaLoanID)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.PaymentService}{LMS.Common.Constants.ActionApiInternal.PaymentSchedule_GetLstPaymentByTimaLoanID}/{timaLoanID}";
            try
            {
                var data = await _apiHelper.ExecuteAsync<List<LMS.Entites.Dtos.PaymentServices.PaymentScheduleModel>>(url: url, method: RestSharp.Method.GET, null);
                response.SetSucces();
                response.Data = data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_CancelHoldMoney|url={url}|timaLoanID={timaLoanID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> GenDebtRestructuring(long loanID, long timaLoanID, int typeDebtRestructuring, int numberOfRepaymentPeriod)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{LMS.Common.Constants.ActionApiInternal.Loan_GenDebtRestructuring}";
            var request = new
            {
                LoanID = loanID,
                TimaLoanID = timaLoanID,
                TypeDebtRestructuring = typeDebtRestructuring,
                NumberOfRepaymentPeriod = numberOfRepaymentPeriod
            };
            try
            {
                var data = await _apiHelper.ExecuteAsync<List<LMS.Entites.Dtos.PaymentServices.GenDebtRestructuringModel>>(url: url, method: RestSharp.Method.POST, request);
                response.SetSucces();
                response.Data = data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GenDebtRestructuring|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> ListCheckInOut(string code)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_ListCheckInOut}/{code}";
            try
            {
                var data = await _apiHelper.ExecuteAsync<List<ResponseListCheckInOut>>(url: url, method: RestSharp.Method.GET, null);
                response.SetSucces();
                response.Data = data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_ListCheckInOut|url={url}|code={code}");
            }
            return response;
        }

        public async Task<ResponseActionResult> AddCheckInOut(long contractid, string act, double lng, double lat, string location, string code, int uid, string vc, string appk, string iid, long t)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_CheckInOut}";
            var urlLMS = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_PlanHandleLoanDaily_CheckInOut}";
            var urlLMSCheckOut = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_PlanHandleLoanDaily_GetResultCheckOutFromAI}";

            var dataPost = new
            {
                UserID = uid,
                ActionType = act,
                Location = location,
                Lat = lat,
                Lng = lng,
                LoanCreditID = contractid,
                TimeUnixAction = t,
                CodeAppCheckIn = code
            };
            try
            {
                var dataLMS = await _apiHelper.ExecuteAsync(urlLMS, method: RestSharp.Method.POST, dataPost);
                if (dataLMS.Result == (int)ResponseAction.Success)
                {
                    var requestAI = new
                    {
                        Contractid = contractid,
                        Act = act,
                        Lng = lng,
                        Lat = lat,
                        Location = location,
                        Code = dataLMS.Data.ToString(),
                        Uid = uid,
                        Vc = vc,
                        Appk = appk,
                        Iid = iid,
                        T = t
                    };
                    var dataAI = await _apiHelper.ExecuteAsync(url: url, method: RestSharp.Method.POST, requestAI);
                    if (dataAI.Result == (int)ResponseAction.Success)
                    {
                        response.SetSucces();
                        response.Data = dataAI;
                        var dataPostGetResult = new
                        {
                            UserID = uid,
                            ActionType = act,
                            LoanCreditID = contractid,
                            CodeAppCheckIn = dataLMS.Data.ToString()
                        };
                        _ = _apiHelper.ExecuteAsync(urlLMSCheckOut, method: RestSharp.Method.POST, dataPostGetResult);
                        await Task.Delay(100); // chờ cho action dc thực thi
                    }
                    else
                    {
                        response.Message = dataAI.Message;
                    }
                }
                else
                {
                    response.Message = dataLMS.Message;
                }

            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GenDebtRestructuring|url={url}|request={_common.ConvertObjectToJSonV2(dataPost)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task<ResponseActionResult> CreateComment(long loanID, long userID, string fullName, string comment, long reasonCodeID, string reasonCode, List<LMS.Entites.Dtos.AG.ImageLosUpload> fileImage, string appointmentDate, short actionPerson, short actionAddress)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_CommentDebtPrompted_CreateCommentDebtPrompted}";
            var request = new
            {
                LoanID = loanID,
                UserID = userID,
                FullName = fullName,
                Comment = comment,
                ReasonCodeID = reasonCodeID,
                ReasonCode = reasonCode,
                FileImage = fileImage,
                AppointmentDate = appointmentDate,
                ActionPerson = actionPerson,
                ActionAddress = actionAddress
            };
            try
            {
                var data = await _apiHelper.ExecuteAsync<CommentDebtPromptedInsertSuccessModel>(url: url, method: RestSharp.Method.POST, request);
                response.SetSucces();
                response.Data = data.CommentDebtPromptedID;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_CreateComment|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> CreatePlanHandleLoanDaily(long userID, string fromDate, string toDate, long loanID)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_PlanHandleLoanDaily_CreatePlanHandleLoanDaily}";
            var request = new
            {
                LoanID = loanID,
                UserID = userID,
                FromDate = fromDate,
                ToDate = toDate,
            };
            try
            {
                var data = await _apiHelper.ExecuteAsync<long>(url: url, method: RestSharp.Method.POST, request);
                response.SetSucces();
                response.Data = data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_CreatePlanHandleLoanDaily|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> GetPlanHandleLoanDaily(long userID, string fromDate, string toDate)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_PlanHandleLoanDaily_PlanHandleLoanDaily}";
            var request = new
            {
                UserID = userID,
                FromDate = fromDate,
                ToDate = toDate,
            };
            try
            {
                var data = await _apiHelper.ExecuteAsync<List<PlanHandleLoanDailyModel>>(url: url, method: RestSharp.Method.POST, request);
                response.SetSucces();
                response.Data = data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GetPlanHandleLoanDaily|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> PlanHandleLoanDailyDetails(long userID, string fromDate, string toDate)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_PlanHandleLoanDaily_PlanHandleLoanDailyDetails}";
            var request = new
            {
                UserID = userID,
                FromDate = fromDate,
                ToDate = toDate,
            };
            try
            {
                var data = await _apiHelper.ExecuteAsync<List<PlanHandleLoanDailyItem>>(url: url, method: RestSharp.Method.POST, request);
                response.SetSucces();
                response.Data = data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_PlanHandleLoanDailyDetails|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> UpdatePlanHandleLoanDaily(long userID, long planHandleLoanDailyID, int status)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_PlanHandleLoanDaily_UpdatePlanHandleLoanDaily}";
            var request = new
            {
                UserID = userID,
                PlanHandleLoanDailyID = planHandleLoanDailyID,
                Status = status,
            };
            try
            {
                var data = await _apiHelper.ExecuteAsync<long>(url: url, method: RestSharp.Method.POST, request);
                response.SetSucces();
                response.Data = data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_UpdatePlanHandleLoanDaily|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> MovePlanHandleLoanDaily(long userID, List<long> LstLoanID)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_PlanHandleLoanDaily_MovePlanHandleLoanDaily}";
            var request = new
            {
                UserID = userID,
                LstLoanID = LstLoanID,
            };
            try
            {
                var data = await _apiHelper.ExecuteAsync<long>(url: url, method: RestSharp.Method.POST, request);
                response.SetSucces();
                response.Data = data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_MovePlanHandleLoanDaily|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> GetDepartment(long parentID)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.AdminService}{LMS.Common.Constants.ActionApiInternal.AdminService_Department_GetDepartment}?GeneralSearch&Status={(int)StatusCommon.Active}&PageIndex=1&PageSize=100000";
            try
            {
                var data = await _apiHelper.ExecuteAsync<List<DepartmentViewList>>(url: url, method: RestSharp.Method.GET, null);
                if (parentID > 0)
                {
                    data = data.Where(x => x.ParentID == parentID && x.AppID == (int)Menu_AppID.THN).ToList();
                }
                else
                {
                    data = data.Where(x => x.ParentID == 0 && x.AppID == (int)Menu_AppID.THN).ToList();
                }
                response.SetSucces();
                response.Data = data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GetDepartment|url={url}|parentID={parentID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> AppPlanDaily(string searchDate, long departmentID, long leadID, long staffID)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_PlanHandleLoanDaily_AppPlanDaily}";
            var request = new
            {
                SearchDate = searchDate,
                LeadID = leadID,
                StaffID = staffID,
                DepartmentID = departmentID,
            };
            try
            {
                var data = await _apiHelper.ExecuteAsync<List<LMS.Entites.Dtos.CollectionServices.PlanHandleLoanDaily.AppPlanDaily>>(url: url, method: RestSharp.Method.POST, request);
                response.SetSucces();
                response.Data = data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_AppPlanDaily|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> GetTeamLead(long departmentID)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_AppTHN_GetTeamLead}/{departmentID}";
            try
            {
                var data = await _apiHelper.ExecuteAsync<List<LMS.Entites.Dtos.CollectionServices.PlanHandleLoanDaily.TeamLeadAppTHN>>(url: url, method: RestSharp.Method.GET, null);
                response.SetSucces();
                response.Data = data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GetTeamLead|url={url}|code={departmentID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> GetStaff(long departmentID, long parentUserID)
        {

            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_user_getlstemployees}";
            var request = new
            {
                DepartmentID = departmentID,
                ParentUserID = parentUserID,
                PositionID = (int)UserDepartment_PositionID.Staff
            };
            try
            {
                var data = await _apiHelper.ExecuteAsync<List<SelectItem>>(url: url, method: RestSharp.Method.POST, request);
                response.SetSucces();
                response.Data = data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GetStaff|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<ResponseActionResult> GetSummaryPlanLoanHandleByStaff(long userID)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_AppTHN_SummaryPlanLoanHandleByStaff}";
            var request = new
            {
                UserID = userID,
            };
            try
            {
                var data = await _apiHelper.ExecuteAsync<SummaryPlanLoanHandleByStaffModel>(url: url, method: RestSharp.Method.POST, request);
                response.SetSucces();
                response.Data = data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GetSummaryPlanLoanHandleByStaff|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<LMS.Common.Constants.ResponseActionResult> GetPositionUser(long userID)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_User_GetPositionUser}";
            var request = new
            {
                UserID = userID
            };
            try
            {
                var result = await _apiHelper.ExecuteAsync(url: url, method: RestSharp.Method.POST, request);
                if (result.Result == (int)ResponseAction.Success)
                {
                    response.SetSucces();
                    response.Data = _common.ConvertJSonToObjectV2<Domains.Models.EmployeeInfoModel>(result.Data.ToString());
                }
                else
                {
                    response.Message = result.Message;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GetPositionUser|url={url}|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> GetHistoryCustomerTopup(Domains.Models.RequestGetHistoryCustomerTopup request)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_THN_SearchHistoryTransaction}";
            var dataPost = new
            {
                request.FromDate,
                request.ToDate,
                request.KeySearch,
                request.PageIndex,
                request.PageSize
            };
            try
            {
                var result = await _apiHelper.ExecuteAsync(url: url, method: RestSharp.Method.POST, dataPost);
                if (result.Result == (int)ResponseAction.Success)
                {
                    response.SetSucces();
                    response.Data = _common.ConvertJSonToObjectV2<List<Domains.Models.HistoryCustomerTopupItem>>(result.Data.ToString());
                }
                else
                {
                    response.Message = result.Message;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GetHistoryCustomerTopup|url={url}|request={_common.ConvertObjectToJSonV2(dataPost)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> GetSummaryEmployeeForPlanHandleLoanByLstDepartmentID(Domains.Models.RequestGetSummaryEmployeeHandleLoanByLstDeptID request)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_AppTHN_GetSummaryEmployeeForPlanHandleLoanByLstDepartmentID}";
            var dataPost = new
            {
                request.LstDepartmentID
            };
            try
            {
                var result = await _apiHelper.ExecuteAsync(url: url, method: RestSharp.Method.POST, dataPost);
                if (result.Result == (int)ResponseAction.Success)
                {
                    response.SetSucces();
                    response.Data = _common.ConvertJSonToObjectV2<List<Domains.Models.SummaryEmployeeDepartmentItem>>(result.Data.ToString());
                }
                else
                {
                    response.Message = result.Message;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GetSummaryEmployeeForPlanHandleLoanByLstDepartmentID|url={url}|request={_common.ConvertObjectToJSonV2(dataPost)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> GetTrandata(long loanID)
        {
            var dataPost = new
            {
                LoanID = loanID,
                PageIndex = 1,
                PageSize = 1000,
                KeySearch = "",
                FromDate = "",
                ToDate = "",
                Status = 1,
                CodeTranData = ""
            };
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{LMS.Common.Constants.ActionApiInternal.CollectionService_CustomerExtensionThirdParty_SearchCustomerExtensionThirdParty}";

            try
            {
                var result = await _apiHelper.ExecuteAsync(url: url, method: RestSharp.Method.POST, dataPost);
                if (result.Result == (int)ResponseAction.Success)
                {
                    response.SetSucces();
                    response.Data = _common.ConvertJSonToObjectV2<List<Domains.Models.TrandataByLoan>>(result.Data.ToString());
                }
                else
                {
                    response.Message = result.Message;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionService_GetTrandata|url={url}|request={_common.ConvertObjectToJSonV2(dataPost)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}