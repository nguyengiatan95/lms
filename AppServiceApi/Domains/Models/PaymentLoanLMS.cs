﻿using System;

namespace AppServiceApi.Domains.Models
{
    public class RequestGenPayment
    {
        public long TotalMoneyDisbursement { get; set; }
        public int LoanTime { get; set; }
        public int RateType { get; set; }
    }

    public class RequestGenDebtRestructuring
    {
        public long LoanID { get; set; }
        public long TimaLoanID { get; set; }

        public int TypeDebtRestructuring { get; set; }
        public int NumberOfRepaymentPeriod { get; set; }
    }
}
