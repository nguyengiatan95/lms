﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.Domains.Models
{
    public class DataLoan
    {
        public long ID { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string CodeID { get; set; }
        public long TotalMoneyCurrent { get; set; }
        public DateTime AlarmDate { get; set; }
        public string StrOverDay
        {
            get; set;
        }
        public string StrStatusSendInsurance { get; set; }
        public long LoanCreditID { get; set; }
        public string TypeName { get; set; }
        public bool ShowPTP { get; set; }
        public bool Location { get; set; }
        public string LoanBriefPropertyPlateNumber { get; set; }
        public string BrandItem { get; set; } // hãng xe
        public string NameItem { get; set; } // biển số xe
        public string HouseholdAddress { get; set; }
        public bool Handling { get; set; }
        public int PlanLoanHandle { get; set; }
        public List<int> LstAssignmentLoanType { get; set; }
        public long TotalMoneyNeedPay { get; set; }
    }
    public class RequestGetLoan
    {
        public string TextSearch { get; set; }
        public int StaffID { get; set; }
        public int CityID { get; set; } = 0;
        public int DistrictID { get; set; } = 0;
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Type { get; set; }
    }

    public class ResponseForAppList<T>
    {
        public int Draw { get; set; }
        public int RecordsFiltered { get; set; }
        public int RecordsTotal { get; set; }
        public T Data { get; set; }
    }

    public class RequestCloseData
    {
        public long LoanID { get; set; }
        public string CloseTime { get; set; }
        public long TimaLoanID { get; set; }

    }

    public class RequestByLoanID
    {
        public long LoanID { get; set; }
        public long UserID { get; set; } // nhân viên xử lý đơn
    }

    public class RequestInsertProvisionsCollection
    {
        public long LoanID { get; set; }
        public string Money { get; set; }
        public int Status { get; set; }
        public string TimeScheduled { get; set; }
        public int NumberAccruedPeriods { get; set; }
        public string strTextAccruedPeriods { get; set; }
        public int PreparationType { get; set; }
    }
}
