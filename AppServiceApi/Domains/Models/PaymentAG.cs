﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.Domains.Models
{
    public class Payment
    {
        public int Id { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public long PayMoney { get; set; }

        public long InterestMoney { get; set; }

        public long OtherMoney { get; set; }

        public long PayNeed { get; set; }
    
        public bool Done
        {
            get;set;
        }

        public double CountDate { get; set; }

        public long MoneyOriginal { get; set; }

        public long MoneyInterest { get; set; }

        public long MoneyService { get; set; }

        public long MoneyConsultant { get; set; }
    }
   
}
