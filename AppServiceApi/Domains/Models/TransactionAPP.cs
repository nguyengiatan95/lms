﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.Domains.Models
{
    public class TransactionAPP
    {
        public long MoneyAdd { get; set; }
        public long MoneySub { get; set; }
        public string Note { get; set; }
        public string FullName { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
