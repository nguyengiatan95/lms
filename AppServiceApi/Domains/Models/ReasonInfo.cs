﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.Domains.Models
{
    public class ReasonInfo
    {
        public int Id { get; set; }

        public string Reason { get; set; }

        public int ReSend { get; set; }

        public int IsApplyBlackList { get; set; }

        public int NumberDayBlackList { get; set; }

        public string ReasonCode { get; set; }

        public int IsCallBack { get; set; }
        public int Type { get; set; }
    }


    [Dapper.Contrib.Extensions.Table("TblReason")]
    public class TblReason
    {
        [Dapper.Contrib.Extensions.Key]
        public long ReasonID { get; set; }

        public string Description { get; set; }

        public string ReasonCode { get; set; }

        public int DepartmentID { get; set; }

        public int Status { get; set; }
    }
}
