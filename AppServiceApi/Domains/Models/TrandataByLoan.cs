﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.Domains.Models
{
    public class TrandataByLoan
    {
        public string ContactCode { get; set; }
        public string CustomerName { get; set; }
        public string CodeTranData { get; set; }
        public string CodeTranDataDescription { get; set; }
        public string CreateByName { get; set; }
        public DateTime CreateDate { get; set; }
        public string StatusName { get; set; }
        public List<DataTran> LstTranData { get; set; }
    }

    public class DataTran
    {
        public string NoteText { get; set; }
    }
}
