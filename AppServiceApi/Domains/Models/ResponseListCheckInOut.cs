﻿namespace AppServiceApi.Domains.Models
{
    public class ResponseListCheckInOut
    {
        public long Uid { get; set; }
        public long Time { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string Act { get; set; }
        public string Location { get; set; }
        public double Duration { get; set; }
        public double Distance { get; set; }
        public bool Pass { get; set; }
        public long Contractid { get; set; }
    }

    public class RequestAddCheckInOut
    {
        public long Contractid { get; set; }
        public string Act { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string Location { get; set; }
        public string Code { get; set; }
        public int Uid { get; set; }
        public string Vc { get; set; }
        public string Appk { get; set; }
        public string Iid { get; set; }
        public long T { get; set; }
    }
}
