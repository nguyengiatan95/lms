﻿namespace AppServiceApi.Domains.Models
{
    public class SummaryPlanLoanHandleByStaffModel
    {
        public int TotalLoanAssigned { get; set; }
        public int TotalLoanNoPlan { get; set; }
    }
}
