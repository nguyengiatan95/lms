﻿using System;

namespace AppServiceApi.Domains.Models
{
    public class DepartmentViewList
    {
        public long DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public DateTime CreateDate { get; set; }
        public int Status { get; set; }
        public long ParentID { get; set; }
        public long AppID { get; set; }
    }
}
