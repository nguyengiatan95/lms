﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.Domains.Models
{
    public class RequestLoanForLender
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int DisbursementType { get; set; }
        public int Status { get; set; }
        public int LenderID { get; set; }
    }
}
