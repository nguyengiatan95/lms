﻿using System;
using System.Collections.Generic;

namespace AppServiceApi.Domains.Models
{
    public class RequestCreatePlanHandleLoanDaily
    {
        public long UserID { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public long LoanID { get; set; }
    }

    public class PlanHandleLoanDailyModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    public class PlanHandleLoanDailyItem
    {
        public long PlanHandleLoanDailyID { get; set; }
        public string ContractCode { get; set; }
        public string CustomerName { get; set; }
        public long LoanID { get; set; }
        public DateTime? TimeCheckin { get; set; }
        public DateTime? TimeCheckOut { get; set; }
        public int Status { get; set; }
        public string Reason { get; set; }
        public List<int> LstAssignmentLoanType { get; set; }
        public bool ShowPTP { get; set; }
    }
    public class RequestGetPlanHandleLoanDaily
    {
        public long UserID { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
    public class RequestUpdatePlanHandleLoanDaily
    {
        public long UserID { get; set; }
        public long PlanHandleLoanDailyID { get; set; }
        public int Status { get; set; }
    }

    public class RequestMovePlanHandleLoanDaily
    {
        public long UserID { get; set; }
        public List<long> LstLoanID { get; set; }
    }

    public class RequestAppPlanDaily
    {
        public string SearchDate { get; set; }
        public long DepartmentID { get; set; }
        public long LeadID { get; set; }
        public long StaffID { get; set; }
    }
    public class RequestGetPositionUser
    {
        public long UserID { get; set; }
    }
    public class RequestGetHistoryCustomerTopup
    {
        public string KeySearch { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }

    }
    public class RequestGetSummaryEmployeeHandleLoanByLstDeptID
    {
        public List<long> LstDepartmentID { get; set; }
    }
}
