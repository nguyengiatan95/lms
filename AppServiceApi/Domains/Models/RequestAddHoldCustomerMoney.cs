﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.Domains.Models
{
    public class RequestAddHoldCustomerMoney
    {
        public long CustomerID { get; set; }
        public long Amount { get; set; }
        public string Reason { get; set; }
        public long CreateBy { get; set; }
    }

    public class RequestDeleteHoldMoney
    {
        public long HoldCustomerMoneyID { get; set; }
    }
}
