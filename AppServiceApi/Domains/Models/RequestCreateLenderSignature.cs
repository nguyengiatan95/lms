﻿namespace AppServiceApi.Domains.Models
{
    public class RequestCreateLenderSignature
    {
        public long LenderID { get; set; }
        public string Signature { get; set; }
        public string Password { get; set; }
        public int UserID { get; set; }
    }

    public class RequestCreateLenderUser
    {
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
    }
}
