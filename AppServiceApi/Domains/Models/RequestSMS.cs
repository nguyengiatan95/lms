﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.Domains.Models
{
    public class RequestSMS
    {
        public string Phone { get; set; }
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 20;
    }

    public class ListSMSView
    {
        public long SendSmsID { get; set; }
        public string CreateByName { get; set; }
        public string StrStatus { get; set; }
        public string TemplateName { get; set; }
        public string ContentSms { get; set; }
        public DateTime CreateDate { get; set; }
        public string PhoneSend { get; set; }
        public string PhoneReceived { get; set; }
        public DateTime ModifyDate { get; set; }
    }

    public class RequestUpdateSMS
    {
        public long SendSmsID { get; set; }
        public int Status { get; set; }
    }

}
