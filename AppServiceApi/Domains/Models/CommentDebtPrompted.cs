﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.Domains.Models
{
    public class CommentDebtPrompted
    {
        public int LoanId { get; set; }
        public int TypeId { get; set; }
        public int UserId { get; set; }
        public int ShopId { get; set; }
        public string FullName { get; set; }
        public string Comment { get; set; }
        public DateTime CreateDate { get; set; }
        public string Reason { get; set; }
        public int ActionPerson { get; set; }
        public int ActionAddress { get; set; }
        public int GroupID { get; set; }
        public string StrActionPeron
        {
            get; set;
        }
        public string StrActionAddress
        {
            get; set;
        }
        public string FilePath { get; set; }
        public List<FileUploadTHN> lstFile
        {
            get; set;
        }
        public DateTime NextDate { get; set; }
    }
    public class FileUploadTHN
    {
        public string FilePath { get; set; }
    }

    public class GetLstCommentDebtPromptedByLoanIDItem
    {
        public DateTime CreateDate { get; set; }
        public string FullName { get; set; }
        public string ReasonCode { get; set; }
        public string Comment { get; set; }
        public string StrActionPeron { get; set; }
        public string StrActionAddress { get; set; }

        public List<LMS.Entites.Dtos.AG.ImageLosUpload> FileImage { get; set; }
    }


    public class HistoryCommentCredit
    {
        public DateTime CreateDate { get; set; }

        public string FullName { get; set; }

        public string Comment { get; set; }

        public string ShopName { get; set; }
        public int Id { get; set; }

        public int LoanCreditId { get; set; }

        public Byte IsDisplay { get; set; }

        public Int16 ActionId { get; set; }

        public int GroupUserId { get; set; }

        public int UserId { get; set; }
        /// <summary>
        /// Đường dẫn file ghi âm
        /// </summary>
        public string UrlImg { get; set; }

        public int S3Status { get; set; }

        /// <summary>
        /// Loại File ghi âm
        /// </summary>
        public int TypeFile { get; set; }
    }

    public class RequestCreateComment
    {
        public long LoanID { get; set; }
        public long UserID { get; set; }

        public string FullName { get; set; }
        public string Comment { get; set; }
        public long ReasonCodeID { get; set; }
        public string ReasonCode { get; set; }

        public long LoanAgID { get; set; }
        public List<LMS.Entites.Dtos.AG.ImageLosUpload> FileImage { get; set; }
        public long TimaID { get; set; }
        public string AppointmentDate { get; set; } // format dd/MM/yyyy HH:mm
        public short ActionPerson { get; set; }

        public short ActionAddress { get; set; }
    }
    public class CommentDebtPromptedInsertSuccessModel
    {
        public long CommentDebtPromptedID { get; set; }
        public long LoanID { get; set; }
    }
}
