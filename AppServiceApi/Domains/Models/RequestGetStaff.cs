﻿namespace AppServiceApi.Domains.Models
{
    public class RequestGetStaff
    {
        public long DepartmentID { get; set; }
        public long ParentUserID { get; set; }
    }
}
