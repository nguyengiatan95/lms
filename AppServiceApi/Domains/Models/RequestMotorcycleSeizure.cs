﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.Domains.Models
{
    public class RequestMotorcycleSeizure
    {
        public int StaffID { get; set; }
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 25;
        public string GeneralSearch { get; set; }
        public int CityID { get; set; } = (int)LMS.Common.Constants.StatusCommon.SearchAll;
        public int DistrictID { get; set; } = (int)LMS.Common.Constants.StatusCommon.SearchAll;
        public int WardID { get; set; } = (int)LMS.Common.Constants.StatusCommon.SearchAll;
    }
}
