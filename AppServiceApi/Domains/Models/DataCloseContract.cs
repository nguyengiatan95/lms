﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.Domains.Models
{
    public class DataCloseContract
    {
        public long MoneyInterestRemain { get; set; }
        public long PunishMoney { get; set; }
        public int FlagCheck { get; set; } = 0;
        public long TotalMoneyCurrent { get; set; }
        public string CloseTime { get; set; } = DateTime.Now.ToString("dd/MM/yyyy");
        public long DebitMoney { get; set; }
        public long InterestMoney { get; set; }
        public long MoneyNeed { get; set; }
        public double TotalDay { get; set; }

        public long MoneyInterest { get; set; }
        public long MoneyService { get; set; }
        public long MoneyConsultant { get; set; }
        public long DebitMoneyFineLate { get; set; }
    }
}
