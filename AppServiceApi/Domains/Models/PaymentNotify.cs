﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.Domains.Models
{
    public class PaymentNotify
    {
        public PaymentNotify()
        {
            this.lstBankVA = new List<BankVA>();
        }
        public int CodeID { get; set; } // Mã HĐ
        public long LoanID { get; set; } // LoanID
        public int CusID { get; set; } // ID khách hàng
        public string CusPhone { get; set; } // Số điện thoại khách hàng
        public string CusName { get; set; } // Tên khách hàng
        public string CusAddress { get; set; } // Địa chỉ khách hàng
        public int Status { get; set; } // trạng thái khoản vay
        public DateTime FromDate { get; set; } // Vay từ ngày đến ngày
        public DateTime NextDate { get; set; } // ngày đóng lãi tiếp theo

        public int PlatformType { get; set; }
        public string IdOfPartenter { get; set; }
        public DateTime ToDate { get; set; }  // Vay đến ngày
        public int CountDate
        {
            get
            {
                if (this.Status != 0)
                {
                    return (new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day) - this.NextDate).Days;
                }
                else
                {
                    return 0;
                }
            }
        } // Ngày quá hạn

        public string NumberCard { get; set; } // CM thư
        public string AgencyName { get; set; } // Tên đại lý 
        public string ProductName { get; set; } // Tên loại vay
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string CompanyName { get; set; }
        public string CompanyPhone { get; set; }
        public string AddressCompany { get; set; }
        public long MoneyInBag { get; set; }
        public int Gender { get; set; }
        public DateTime Birthday { get; set; }
        public string AddressHouseHold { get; set; }
        public string DistrictNameHouseHold { get; set; }
        public string CityNameHouseHold { get; set; }
        public long LoanCreditID { get; set; }
        public long LoanTotalMoney { get; set; }
        public string Facebook { get; set; }
        public string StrStatus
        {
            get; set;

        }

        public string CompanyTaxCode { get; set; }
        public string CompanyCityName { get; set; }
        public string CompanyDistrictName { get; set; }
        public long Salary { get; set; }
        public string JobName { get; set; }
        public string AddressPersonFamily { get; set; }
        public string BankName { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankAccountName { get; set; }
        public List<CustomerRelationshipInfo> lstRelationship { get; set; }

        public long TotalMoneyCollectOfCustomer { get; set; }

        public List<BankVA> lstBankVA { get; set; }
        public int PlanHandleLoanStatus { get; set; } // trạng thái xử lý đơn theo kế hoạch ngày
        public string PlanHandleLoanCheckInLocationName { get; set; } // địa điểm checkin nếu có
        public int PlanHandleLoanExpiredType { get; set; }// đơn đã lên kế hoạch chưa: 0: chưa, 1: còn hiệu lực, 2: hết hạn
        public string PlanHandleCodeCheckIn { get; set; }// đơn đã có checkin
        public int DebtStructure { get; set; } // tái cấu trúc nợ: 0: ko có, 1: có, 2: đã tái cấu trúc nợ
        public int DebtRelief { get; set; } // giãn nợ: 0, 1, 2
        public long TotalMoneyNeedPay { get; set; } // tổng tiền thanh toán
        public long MoneyOriginalNeedPay { get; set; } // tiền gốc phải TT
        public long TotalInterestNeedPay { get; set; } // tiền lãi + phí dịch vụ + phí tư vấn
        public long MoneyFineLateNeedPay { get; set; } // phạt trả chậm
        public long MoneyFineOriginalNeedPay { get; set; } // phạt tất toán sớm
        public bool PlanHandleLoanCheckInStatus { get; set; }// trạng thái checkin, true: thành công
        public long HoldCustomerMoneyID { get; set; }
        public string EsignBorrowerContract { get; set; }
    }

    public class CustomerRelationshipInfo
    {
        public int Id { get; set; }
        public int RelationshipType { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string RelationshipName { get; set; }

    }

    public class BankVA
    {
        public string BankName { get; set; }

        public string AccountNumber { get; set; }

        public string AccountName { get; set; }

    }


    public class MapResultLoanWaitingDisbursementDetailLOS : ResultLoanWaitingDisbursementDetailLOS
    {
        public MapResultLoanWaitingDisbursementDetailLOS()
        {
            LstBankCustomer = new List<BankCustomer>();
        }
        public string ContactCode { get; set; } // Mã HĐ

        public string LenderCode { get; set; } // mã nhà đầu tư
        public long InvestorBalance { get; set; }// số dư nhà đầu tư
        public decimal FeesInsuranceCustomer { get; set; } // phí bảo hiểm KH , sức khỏe
        public decimal FeesInsuranceMaterial { get; set; }// phí bảo hiểm vật chất
        public decimal TotalFeesCustomer { get; set; }//  phí khách hàng chịu
        public decimal AmountActuallyReceived { get; set; }// số tiền thức nhận
        public string ContactCodeLMS { get; set; }// mã HD giải ngân LMS
        public string StatusLMS { get; set; }// trạng thái LMS 

        public DateTime FromDateLMS { get; set; }
        public DateTime ToDateLMS { get; set; }
        public List<BankCustomer> LstBankCustomer { get; set; }
        public long MoneyOrginal { get; set; } // tiền gốc
        public long MoneyInterest { get; set; }// tiền lãi
        public long MoneyService { get; set; } // tiền dịch vụ
        public long MoneyConsultant { get; set; }// tiền tư vấn
        public long MoneyFineLate { get; set; } // tiền phạt trả chậm
        public long TotalMoneyLoanPaid { get; set; } // tổng tiền KH đã thanh toán
        public DateTime? LatestDateTopup { get; set; } // KH nạp tiền gần nhất
        public long CustomerTotalMoney { get; set; } // tiền KH có
        public string StrImcomeType { get; set; } // hình thức nhận lương

        public int PlanHandleLoanStatus { get; set; } // trạng thái xử lý đơn theo kế hoạch ngày
        public string PlanHandleLoanCheckInLocationName { get; set; } // địa điểm checkin nếu có
        public int PlanHandleLoanExpiredType { get; set; }// đơn đã lên kế hoạch chưa: 0: chưa, 1: còn hiệu lực, 2: hết hạn
        public string PlanHandleCodeCheckIn { get; set; }// đơn đã có checkin
        public int DebtStructure { get; set; } // tái cấu trúc nợ: 0: ko có, 1: có, 2: đã tái cấu trúc nợ
        public int DebtRelief { get; set; } // giãn nợ: 0, 1, 2
        public long TotalMoneyNeedPay { get; set; } // tổng tiền thanh toán
        public long TotalMoneyNeedPayOnApp { get; set; } // giãn nợ: 0, 1, 2
        public long MoneyOriginalNeedPayOnApp { get; set; } // tiền gốc phải TT
        public long TotalInterestNeedPayOnApp { get; set; } // tiền lãi + phí dịch vụ + phí tư vấn
        public long MoneyFineLateNeedPayOnApp { get; set; } // phạt trả chậm
        public long MoneyFineOriginalNeedPayOnApp { get; set; } // phạt tất toán sớm
        public bool PlanHandleLoanCheckInStatus { get; set; }// trạng thái checkin, true: thành công
        public long HoldCustomerMoneyID { get; set; }
    }

    public class BankCustomer
    {
        public string AccountNo { get; set; }

        public string AccountName { get; set; }

        public string BankCode { get; set; }

        public string BankName { get; set; }
    }
    public class ResultLoanWaitingDisbursementDetailLOS
    {
        public long LoanBriefId { get; set; }  //  Mã đơn vay
        public int ProductId { get; set; } //  Mã sản phẩm
        public string ProductName { get; set; } // Tên sản phẩm
        public int ProvinceId { get; set; } // Mã tỉnh thành
        public string ProvinceName { get; set; } // Tên tỉnh thành
        public int DistrictId { get; set; } // Mã quận huyện
        public string DistrictName { get; set; } // Tên quận huyện
        public int WardId { get; set; } // Mã phường xã
        public string WardName { get; set; } // Tên phường xã
        public int LoanTime { get; set; } // Thời gian vay(tháng)
        public int RateTypeId { get; set; } // Hình thức thanh toán(Mã)
        public string RateTypeName { get; set; } // Hình thức thanh toán(Tên)
        public int PlatformType { get; set; } // Mã nguồn đơn
        public string PlatformTypeName { get; set; } // Tên nguồn đơn
        public int LoanPurpose { get; set; } // Mục đích vay vốn(Mã)
        public string LoanPurposeName { get; set; } // Mục đích vay vốn (Tên)
        public bool IsCheckCic { get; set; } // check CIC(bool)
        public int ScoreCredit { get; set; } //  Điểm Score
        public bool IsTrackingLocation { get; set; } //  chia sẻ vị trí
        public int CountCall { get; set; } //  Số lần gọi điện cho khách
        public string Email { get; set; } //  Email khách hàng
        public int Frequency { get; set; } // Chu kỳ đóng lãi(tháng)
        public long ShopId { get; set; } // Mã hub tư vấn
        public string ShopName { get; set; } //  Tên hub
        public long LenderId { get; set; } // Mã lender giải ngân
        public long CustomerId { get; set; } // Mã khách hàng
        public string FullName { get; set; } // Tên khách hàng
        public string Phone { get; set; } // SDT KH
        public DateTime Dob { get; set; } // Ngày sinh KH
        /// <summary>
        /// 0: nam, 1: nữ
        /// </summary>
        public int Gender { get; set; } // Giới tính(0:Nam, 1: Nữ)
        public string Address { get; set; }

        public long BankId { get; set; } // Mã ngân hàng
        public string BankAccountName { get; set; } // Tên chủ thẻ
        public string BankAccountNumber { get; set; } // Số tài khoản
        public string BankCardNumber { get; set; } //  Số thẻ

        public DateTime NationalCardDate { get; set; } // Ngày cấp
        public string NationCardPlace { get; set; } // Nơi cấp
        public string NationalCard { get; set; } // Số CMT KH
        public decimal LoanAmount { get; set; } //  Số tiền khách đăng ký vay
        public decimal LoanAmountFinal { get; set; } //  Số tiền khách được duyệt
        public DateTime FromDate { get; set; } // Ngày bắt đầu
        public DateTime ToDate { get; set; } // Ngày kết thúc
        public DateTime NextDate { get; set; } // Ngày đóng lãi tiếp theo
        public int TypeLoanBrief { get; set; } // Loại đơn vay(Mã)
        public string TypeLoanBriefName { get; set; } // Loại đơn vay(Tên)
        public string BusinessCompanyName { get; set; } // Tên doanh nghiệp
        public string CareerBusiness { get; set; } // Ngành nghề kinh doanh
        public DateTime BusinessCertificationDate { get; set; } // Ngày cấp giấy CNĐKD/CNĐKND
        public string BusinessCertificationAddress { get; set; } // Nơi cấp giấy CNĐKD/CNĐKND
        public string HeadOffice { get; set; } // Trụ sở đăng ký kinh doanh
        public string BusinessAddress { get; set; } // Địa chỉ kinh doanh hiện tại
        public string CompanyShareholder { get; set; } // Cổ đông góp vốn nhiều nhất
        public DateTime BirthdayShareholder { get; set; } // Ngày sinh
        public string CardNumberShareholder { get; set; } // CMT
        public DateTime CardNumberShareholderDate { get; set; } // Ngày cấp
        public string PlaceOfBirthShareholder { get; set; } // Nơi sinh
        public string CustomerAddress { get; set; } // Địa chỉ chi tiết
        public decimal ConsultantRate { get; set; }
        public decimal ServiceRate { get; set; }
        public decimal LoanRate { get; set; }
        public DateTime CreatedTime { get; set; }
        public string LoanBriefComment { get; set; }
        public int TypeInsurence { get; set; } // Mua bảo hiểm (1: bảo hiểm VIB, 2: Bảo Minh)
        public int IsMerried { get; set; } // Đã có gia đình hay chưa( 0: chưa, 1: đã có)
        public int NumberBaby { get; set; } // Có mấy cháu( 0: chưa có, 1: 1 cháu, 2: 2 cháu, 3: >= 3 cháu)
        public int ReceivingMoneyType { get; set; } // Hình thức nhận tiền khách hàng(mã)
        public string ReceivingMoneyTypeName { get; set; } // Hình thức nhận tiền khách hàng(tên)
        public int JobId { get; set; } // Mã công việc
        public string CompanyName { get; set; } // Tên công ty
        public string CompanyPhone { get; set; } // SDT Công Ty
        public string CompanyAddress { get; set; } // Địa chỉ
        public string CompanyTaxCode { get; set; } // Mã số thuế
        public string CompanyProvinceName { get; set; } // Thành phố nơi làm việc
        public string CompanyDistrictName { get; set; } // Quận huyện nơi làm việc
        public string CompanyWardName { get; set; } // Phưỡng xã nơi làm việc
        public decimal TotalIncome { get; set; } // Thu nhập
        public string DescriptionJob { get; set; } // Mô tả vị trí công việc
        public bool BuyInsurenceCustomer { get; set; } // Khách hàng có mua bảo hiểm(true : có, false: không)
        /// <summary>
        /// nhân viên thẩm định
        /// </summary>
        public int ApproveId { get; set; } // Mã thẩm định
        public string ApproveName { get; set; } // Tên thẩm định
        public string ApprovePhone { get; set; } // SDT thẩm định
        public int HouseOldCityId { get; set; } // SDT thẩm định
        public string HouseOldCityName { get; set; } // Địa chỉ sổ hộ khẩu
        public int HouseOldDistrictId { get; set; } // Địa chỉ sổ hộ khẩu
        public string HouseOldDistrictName { get; set; }
        public int HouseOldWardId { get; set; }
        public string HouseOldWardName { get; set; }
        public string HouseOldAddress { get; set; }
        public int TypeDisbursement { get; set; } // Đơn vị giải ngân(1: kế toán. 2: Lender)
        public DateTime LenderReceivedDate { get; set; } // Thời gian đẩy đơn cho Lender
        public bool IsLock { get; set; } // Khóa đơn vay(true -> khóa)
        public bool IsLocate { get; set; } //  Có gắn định vị không(true: có)
        public int BrandId { get; set; } //   Hãng xe(mã)
        public string BrandName { get; set; } //  Hãng xe (tên)
        public string JobTitle { get; set; } // Tên công việc
        public List<CustomerRelationshipInfo> Relationship { get; set; }

        public int Status { get; set; } //  trạng thái đơn vay(90 -> Chờ chọn Lender, 102 -> Chờ kế toán GN, 103 - Chờ Lender GN)
        public decimal RatePercent { get; set; } // lãi suất theo năm
        public int ResidentType { get; set; } //  Hình thức sở hữu(Mã)
        public string ResidentTypeName { get; set; } // Hình thức sở hữu(Tên)
        public string InsurenceNumber { get; set; } // Số bảo hiểm
        public string InsurenceNote { get; set; } //  Ghi chú bảo hiểm
        public string FacebookAddress { get; set; } //   Facebook KH
        public int LivingWith { get; set; } // Sống cùng ai(Mã)
        public string LivingName { get; set; } // Sống cùng ai(Tên),
        public string LivingTimeName { get; set; } // Sống cùng ai(Tên),
        public string DeviceId { get; set; } // mã thiết bị định vị

        public string AddressGoogleMap { get; set; }
        public string CompanyAddressGoogleMap { get; set; }
        public int Inprocess { get; set; } // 1: queue Los đang xử lý, 0: hoàn thành
        public string Passport { get; set; } // passport

        public long TopUpOfLoanBriefID { get; set; } // id của đơn gốc topup

        public LoanBriefPropertyLOS LoanBriefProperty { get; set; }

        public string AddressNationalCard { get; set; } // địa chỉ VAT

        public bool IsReborrow { get; set; } // Khoản vay lại
        public int IsLivingInProvince { get; set; } // 1 cùng tỉnh , 0 Khác tỉnh

        public string BankName { get; set; } // tên ngân hàng

        public decimal LoanAmountExpertise { get; set; }// giá trước thẩm định
        public decimal LoanAmountExpertiseLast { get; set; } // giá sau thẩm định

        public bool BuyInsuranceProperty { get; set; }// có mua bảo hiểm vật chất
        public bool BuyInsuranceLender { get; set; }// có mua bảo hiểm lender

        public string AddressPersonFamily { get; set; } //địa chỉ cư trú
        public string FullNameColleague { get; set; }//họ và tên đồng nghiệp 
        public string PhoneColleague { get; set; }// số điện thoại đông nghiệp

        public int ImcomeType { get; set; } // hình thức nhận lương
        public decimal FeePaymentBeforeLoan { get; set; } // Phí phạt tất toán khoản vay trước hạn Lưu dữ liệu kiểu(0.05 hoặc 0.08)

        public long GroupLoanId { get; set; } //  gom nhóm hợp đồng của cùng 1 KH về 1
        public bool EsignState { get; set; } // KH có ký điện tử không
        public string EsignBorrowerContract { get; set; } //  link hợp đồng của KH vay
        public string EsignLenderContract { get; set; } // link hợp đồng của lender
    }
    public class LoanBriefPropertyLOS
    {
        public string Brand { get; set; }// hãng xe
        public string Product { get; set; }// tên xe
        public int YearMade { get; set; }//đời xe
        public string PlateNumber { get; set; }// biển kiểm soát xe máy
        public string PlateNumberCar { get; set; } // biển kiểm soát oto
        public string Chassis { get; set; }// số khung
        public string Engine { get; set; } // số máy
    }
}
