﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.Domains.Models
{
    public class District
    {
        public int ID { get; set; }

        public string Name { get; set; }
    }

    public class Wards
    {
        public int ID { get; set; }

        public string Name { get; set; }
    }
}
