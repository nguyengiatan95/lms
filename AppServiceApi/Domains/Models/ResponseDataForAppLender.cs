﻿using LMS.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.Domains.Models
{
    public class ResponseDataForAppLender
    {
        public int Status { get; set; }
        public int Total { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public ResponseDataForAppLender()
        {
            Status = (int)LMS.Common.Constants.ResponseAction.Error;
            Message = MessageConstant.ErrorInternal;
        }

        public void SetSucces()
        {
            Status = (int)LMS.Common.Constants.ResponseAction.Success;
            Message = MessageConstant.Success;
        }
    }
}
