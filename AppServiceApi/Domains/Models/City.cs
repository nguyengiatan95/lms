﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.Domains.Models
{
    public class City
    {
        public int ID { get; set; }

        public string CityName { get; set; }
        public string Title { get; set; }


        public string TypeCity { get; set; }

        public int Position { get; set; }

        public int IsApply { get; set; }

        public int IsCity { get; set; }


    }
}
