﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppServiceApi.Domains.Models
{
    public class FileContract
    {

        public int ID { get; set; }


        public int LoanID { get; set; }


        public int UserID { get; set; }


        public string FilePath { get; set; }

        public DateTime CreateDate { get; set; }


        public int Status { get; set; }

        public int IsPartner { get; set; }

        public int TypeImg { get; set; }

        public int S3Status { get; set; }

        public int TypeFile { get; set; }


        public string FullPathImage
        {
            get;set;
        }

        public string PathThumb { get; set; }
    }

    public class ResultListImage
    {
        public int? TypeId { get; set; }
        public string TypeName { get; set; }
        public List<ResultItemImage> LstFilePath { get; set; }
    }

    public class ResultItemImage
    {
        public string FilePath { get; set; }
        public string FileThumb { get; set; }
        public string TypeFile { get; set; }
    }
}
