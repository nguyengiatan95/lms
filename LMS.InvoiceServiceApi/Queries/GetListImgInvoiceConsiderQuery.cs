﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace LMS.InvoiceServiceApi.Queries
{
    public class GetListImgInvoiceConsiderQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long InvoiceID { get; set; }
    }
    public class GetListImgInvoiceConsiderQueryHandler : IRequestHandler<GetListImgInvoiceConsiderQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        ILogger<GetListImgInvoiceConsiderQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetListImgInvoiceConsiderQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            ILogger<GetListImgInvoiceConsiderQueryHandler> logger)
        {
            _invoiceTab = invoiceTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetListImgInvoiceConsiderQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    var objInvoice = _invoiceTab.WhereClause(x => x.InvoiceID == request.InvoiceID).Query().FirstOrDefault();
                    if (objInvoice != null)
                    {

                        response.Data = _common.ConvertJSonToObjectV2<List<Domain.Models.ImageLosUpload>>(objInvoice.PathImg);
                    }
                    response.SetSucces();
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"GetListImgInvoiceConsiderQueryHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                }
                return response;
            });

        }
    }
}
