﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Queries
{
    public class GetInvoiceInfosByBankCardQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public long BankCardID { get; set; }
        public int InvoiceType { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class GetInvoiceInfosByBankCardQueryHandler : IRequestHandler<GetInvoiceInfosByBankCardQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        LMS.Common.Helper.Utils _common;
        public GetInvoiceInfosByBankCardQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab)
        {
            _invoiceTab = invoiceTab;
            _userTab = userTab;
            _bankCardTab = bankCardTab;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetInvoiceInfosByBankCardQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                var fromDate = DateTime.ParseExact(request.FromDate, _common.DateTimeDDMMYYYY, CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(request.ToDate, _common.DateTimeDDMMYYYY, CultureInfo.InvariantCulture).AddDays(1);

                if (request.InvoiceType == (int)InvoiceType.Receipt)
                {
                    _invoiceTab.WhereClause(x => x.InvoiceType != (int)InvoiceType.PaySlip);
                }
                else if (request.InvoiceType == (int)InvoiceType.PaySlip)
                {
                    _invoiceTab.WhereClause(x => x.InvoiceType == request.InvoiceType);
                }
                var lstInvoiceData = _invoiceTab.WhereClause(x => x.BankCardID == request.BankCardID &&
                                       x.Status != (int)Invoice_Status.Deleted &&
                                       (x.TransactionDate >= fromDate && x.TransactionDate < toDate)).Query();

                if (lstInvoiceData != null && lstInvoiceData.Count() > 0)
                {
                    var lstBankCardID = lstInvoiceData.GroupBy(x => x.BankCardID).Select(x => x.Key);
                    var lstBankCard = _bankCardTab.WhereClause(x => lstBankCardID.Contains(x.BankCardID)).Query();

                    long sumTotalMoney = 0;
                    List<LMS.Entites.Dtos.InvoiceService.InvoiceItemViewModel> lstResult = new List<Entites.Dtos.InvoiceService.InvoiceItemViewModel>();
                    int totalRow = 0;
                    foreach (var item in lstInvoiceData)
                    {
                        var objBankCard = lstBankCard.FirstOrDefault(x => x.BankCardID == item.BankCardID);
                        Domain.Tables.InvoiceJsonExtra jsonExtra = new Domain.Tables.InvoiceJsonExtra();
                        if (!string.IsNullOrEmpty(item.JsonExtra))
                        {
                            jsonExtra = _common.ConvertJSonToObjectV2<Domain.Tables.InvoiceJsonExtra>(item.JsonExtra);
                        }
                        LMS.Entites.Dtos.InvoiceService.InvoiceItemViewModel invoice = new Entites.Dtos.InvoiceService.InvoiceItemViewModel
                        {
                            InvoiceID = item.InvoiceID,
                            CreateDate = item.CreateDate,
                            InvoiceType = item.InvoiceType,
                            InvoiceSubType = item.InvoiceSubType,
                            InvoiceSubTypeName = ((GroupInvoiceType)item.InvoiceSubType).GetDescription(),
                            InvoiceCode = item.InvoiceCode,
                            Note = item.Note,
                            InvoiceTypeName = ((InvoiceType)item.InvoiceType).GetDescription(),
                            TransactionMoney = item.TransactionMoney ?? 0,
                            Status = (int)item.Status,
                            StatusName = ((Invoice_Status)item.Status).GetDescription(),
                            ModifyDate = item.ModifyDate,
                            TransactionDate = item.TransactionDate,
                            UserNameCreate = jsonExtra.CreateBy,
                            BankName = objBankCard?.AliasName,
                            SourceName = jsonExtra.SourceName,
                            ShopName = jsonExtra.ShopName,
                            DestinationName = jsonExtra.DestinationName,
                        };

                        switch ((InvoiceType)item.InvoiceType)
                        {
                            case InvoiceType.PaySlip:
                                invoice.SourceName = jsonExtra.DestinationName;
                                invoice.DestinationName = jsonExtra.SourceName;
                                break;
                            case InvoiceType.Receipt:
                                break;
                        }
                        sumTotalMoney += item.TransactionMoney ?? 0;
                        lstResult.Add(invoice);
                    }
                    totalRow = lstResult.Count;
                    if (request.InvoiceType != (int)StatusCommon.Default)
                        lstResult = lstResult.OrderByDescending(x => x.TransactionDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();

                    if (lstResult.Count > 0)
                    {
                        lstResult.Add(new Entites.Dtos.InvoiceService.InvoiceItemViewModel
                        {
                            SumTotalMoney = sumTotalMoney,
                            InvoiceType = request.InvoiceType
                        });
                    }
                    ResponseForDataTable dataTable = new ResponseForDataTable
                    {
                        RecordsTotal = totalRow,
                        Data = lstResult,
                        RecordsFiltered = totalRow,
                        Draw = 1
                    };
                    response.Data = dataTable;
                }
                response.SetSucces();
                return response;
            });
        }
    }
}
