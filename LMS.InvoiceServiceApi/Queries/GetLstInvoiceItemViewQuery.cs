﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Queries
{
    public class GetLstInvoiceItemViewQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long ShopID { get; set; }
        public long BankCardID { get; set; }
        public int Status { get; set; }
        public int InvoiceType { get; set; }
        public int InvoiceSubType { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public long MoneySearch { get; set; }
        public long BankID { get; set; }
    }
    public class GetLstInvoiceItemViewQueryHandler : IRequestHandler<GetLstInvoiceItemViewQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankcardTab;
        LMS.Common.Helper.Utils _common;
        ILogger<GetLstInvoiceItemViewQueryHandler> _logger;
        public GetLstInvoiceItemViewQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankcardTab,
            ILogger<GetLstInvoiceItemViewQueryHandler> logger)
        {
            _invoiceTab = invoiceTab;
            _bankcardTab = bankcardTab;
            _logger = logger;
            _common = new Utils();
        }
        public async Task<ResponseActionResult> Handle(GetLstInvoiceItemViewQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            List<LMS.Entites.Dtos.InvoiceService.InvoiceItemViewModel> lstResult = new List<Entites.Dtos.InvoiceService.InvoiceItemViewModel>();
            ResponseForDataTable dataTable = new ResponseForDataTable
            {
                RecordsTotal = 0,
                Data = lstResult,
                RecordsFiltered = 0,
                Draw = 1
            };
            DateTime fromDate = DateTime.Now.AddDays(TimaSettingConstant.MaxDayAgoQuery);
            DateTime toDate = DateTime.Now.AddDays(1);
            if (!string.IsNullOrEmpty(request.FromDate))
            {
                fromDate = DateTime.ParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                fromDate = fromDate.AddSeconds(-1);
            }
            if (!string.IsNullOrEmpty(request.ToDate))
            {
                toDate = DateTime.ParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                toDate = toDate.AddDays(1);
            }
            if (request.BankID > 0)
            {
                var lstBankcard = _bankcardTab.WhereClause(x => x.BankID == request.BankID).Query();
                if (lstBankcard == null || !lstBankcard.Any())
                {
                    response.SetSucces();
                    response.Data = dataTable;
                    return response;
                }
                var bankcardIDs = lstBankcard.Select(x => x.BankCardID).ToList();
                _invoiceTab.WhereClause(x => bankcardIDs.Contains((long)x.BankCardID));
            }
            _invoiceTab.WhereClause(x => x.TransactionDate > fromDate && x.TransactionDate < toDate);
            if (request.ShopID > 0)
            {
                _invoiceTab.WhereClause(x => x.ShopID == request.ShopID);
            }
            if (request.BankCardID > 0)
            {
                _invoiceTab.WhereClause(x => x.BankCardID == request.BankCardID);
            }
            if (request.Status != (int)StatusCommon.SearchAll)
            {
                _invoiceTab.WhereClause(x => x.Status == request.Status);
            }
            if (request.InvoiceType != (int)StatusCommon.SearchAll)
            {
                _invoiceTab.WhereClause(x => x.InvoiceType == request.InvoiceType);
            }
            if (request.InvoiceSubType != (int)StatusCommon.SearchAll)
            {
                _invoiceTab.WhereClause(x => x.InvoiceSubType == request.InvoiceSubType);
            }
            if (request.MoneySearch != 0)
            {
                _invoiceTab.WhereClause(x => x.TotalMoney == request.MoneySearch);
            }
            var lstInvoiceData = await _invoiceTab.QueryAsync();

            response.SetSucces();

            response.Data = dataTable;
            if (lstInvoiceData != null && lstInvoiceData.Any())
            {
                int sizeBatch = 100;
                long totalMoneyInvoice = 0;
                var batchInvoiceInfos = lstInvoiceData.Batch(sizeBatch);
                List<Task<long>> lstTaskSum = new List<Task<long>>();
                foreach (var batch in batchInvoiceInfos)
                {
                    lstTaskSum.Add(SummTotalMoneyInvoice(batch));
                }
                lstInvoiceData = lstInvoiceData.OrderByDescending(x => x.TransactionDate);
                dataTable.RecordsTotal = lstInvoiceData.Count();
                lstInvoiceData = lstInvoiceData.Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                var lstBankCardID = lstInvoiceData.Select(x => x.BankCardID).Distinct().ToList();
                var dictBankCardInfos = _bankcardTab.WhereClause(x => lstBankCardID.Contains(x.BankCardID)).Query().ToDictionary(x => x.BankCardID, x => x);
                foreach (var item in lstInvoiceData)
                {
                    try
                    {
                        Domain.Tables.InvoiceJsonExtra jsonExtra = new Domain.Tables.InvoiceJsonExtra();
                        if (!string.IsNullOrEmpty(item.JsonExtra))
                        {
                            jsonExtra = _common.ConvertJSonToObjectV2<Domain.Tables.InvoiceJsonExtra>(item.JsonExtra);
                        }
                        var bankCardInfo = dictBankCardInfos.GetValueOrDefault((long)item.BankCardID);
                        LMS.Entites.Dtos.InvoiceService.InvoiceItemViewModel invoice = new Entites.Dtos.InvoiceService.InvoiceItemViewModel
                        {
                            InvoiceID = item.InvoiceID,
                            CreateDate = item.CreateDate,
                            InvoiceType = item.InvoiceType,
                            InvoiceSubType = item.InvoiceSubType,
                            InvoiceSubTypeName = ((GroupInvoiceType)item.InvoiceSubType).GetDescription(),
                            InvoiceCode = item.InvoiceCode,
                            Note = item.Note,
                            InvoiceTypeName = ((InvoiceType)item.InvoiceType).GetDescription(),
                            TransactionMoney = item.TransactionMoney ?? 0,
                            Status = (int)item.Status,
                            StatusName = ((Invoice_Status)item.Status).GetDescription(),
                            ModifyDate = item.ModifyDate ?? item.CreateDate,
                            UserNameCreate = jsonExtra.CreateBy,
                            SourceName = jsonExtra.SourceName,
                            ShopName = jsonExtra.ShopName,
                            DestinationName = jsonExtra.DestinationName,
                            TransactionDate = item.TransactionDate ?? item.CreateDate
                        };
                        switch ((InvoiceType)item.InvoiceType)
                        {
                            case InvoiceType.PaySlip:
                                invoice.SourceName = jsonExtra.DestinationName;
                                invoice.DestinationName = jsonExtra.SourceName;
                                break;
                            case InvoiceType.Receipt:
                                break;
                        }
                        if (!string.IsNullOrEmpty(bankCardInfo?.AliasName) && invoice.DestinationName != bankCardInfo?.AliasName)
                        {
                            invoice.DestinationName = bankCardInfo.AliasName;
                        }
                        lstResult.Add(invoice);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"item invoice-{item.InvoiceID}_Exception|ex={ex.Message}-{ex.StackTrace}");
                    }
                }

                await Task.WhenAll(lstTaskSum);
                if (lstResult.Count > 0)
                {
                    foreach (var taskSum in lstTaskSum)
                    {
                        totalMoneyInvoice += taskSum.Result;
                    }
                    lstResult[0].SumTotalMoney = totalMoneyInvoice;
                }
            }
            return response;
        }
        private async Task<long> SummTotalMoneyInvoice(IEnumerable<Domain.Tables.TblInvoice> lstInvoiceData)
        {
            return await Task.Run(() => { return lstInvoiceData.Sum(x => (long)x.TotalMoney); });
        }
    }
}
