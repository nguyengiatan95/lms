﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.InvoiceService;
using LMS.InvoiceServiceApi.Domain.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Queries
{
    public class SearchInvoiceAdvanceSlipLenderQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LenderID { get; set; }
    }
    public class SearchInvoiceAdvanceSlipLenderQueryHandler : IRequestHandler<SearchInvoiceAdvanceSlipLenderQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.Helper.Utils _common;
        public SearchInvoiceAdvanceSlipLenderQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lender)
        {
            _invoiceTab = invoiceTab;
            _lenderTab = lender;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(SearchInvoiceAdvanceSlipLenderQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                var resultData = new List<InvoiceAdvanceSlipLender>();
                var lstInvoice = _invoiceTab.WhereClause(x => x.LenderID == request.LenderID && (
                                                         x.InvoiceSubType == (int)GroupInvoiceType.ReceiptLenderTopUp ||
                                                         x.InvoiceSubType == (int)GroupInvoiceType.PaySlipAdvancePaymentForLender)).Query();
                if (lstInvoice != null && lstInvoice.Count() > 0)
                {
                    var objLender = _lenderTab.WhereClause(x => x.LenderID == request.LenderID).Query().FirstOrDefault();
                    //hoàn ứng
                    var lstReceiptLenderTopUp = lstInvoice.Where(x => x.InvoiceSubType == (int)GroupInvoiceType.ReceiptLenderTopUp).ToList();
                    //tạm ứng
                    var lstPaySlipAdvancePaymentForLender = lstInvoice.Where(x => x.InvoiceSubType == (int)GroupInvoiceType.PaySlipAdvancePaymentForLender).OrderBy(x=>x.InvoiceID).ToList();
                    if (lstPaySlipAdvancePaymentForLender != null && lstPaySlipAdvancePaymentForLender.Count() > 0)
                    {
                        if (lstReceiptLenderTopUp != null && lstReceiptLenderTopUp.Count() > 0)
                        {
                            long totalMoneyReceiptLenderTopUp = lstReceiptLenderTopUp.Sum(x => (long)x.TransactionMoney);
                            foreach (var item in lstPaySlipAdvancePaymentForLender)
                            {
                                item.TransactionMoney = item.TransactionMoney * -1;
                                var result = new List<InvoiceAdvanceSlipLender>();
                                var objInvoiceAdvanceSlipLender = new InvoiceAdvanceSlipLender();
                                if (totalMoneyReceiptLenderTopUp > 0 && totalMoneyReceiptLenderTopUp >= item.TransactionMoney)// tông tiền hoàn ứng >= tiền tạm ứng => phiếu đã hoàn ứng
                                {
                                     totalMoneyReceiptLenderTopUp = totalMoneyReceiptLenderTopUp - (long)item.TransactionMoney;
                                    continue;
                                }
                                else // tông tiền hoàn ứng < tiền tạm ứng =>
                                {
                                    objInvoiceAdvanceSlipLender.LenderName = objLender.FullName;
                                    objInvoiceAdvanceSlipLender.TransactionDate = item.TransactionDate;
                                    objInvoiceAdvanceSlipLender.MoneyAdvance = (long)item.TransactionMoney; // tiền tạm ứng
                                    objInvoiceAdvanceSlipLender.MoneyControl = 0; // tền đối soát
                                    objInvoiceAdvanceSlipLender.MoneyUnrefunded = (long)item.TransactionMoney - totalMoneyReceiptLenderTopUp;// tền chưa hoàn ứng
                                    objInvoiceAdvanceSlipLender.MoneyRefunded = totalMoneyReceiptLenderTopUp; // tền đã hoàn ứng
                                    totalMoneyReceiptLenderTopUp = 0;
                                    resultData.Add(objInvoiceAdvanceSlipLender);
                                }
                            }
                        }
                        else
                        {
                            foreach (var item in lstPaySlipAdvancePaymentForLender)
                            {
                                item.TransactionMoney = item.TransactionMoney * -1;
                                resultData.Add(new InvoiceAdvanceSlipLender
                                {
                                    LenderName = objLender.FullName,
                                    TransactionDate = item.TransactionDate,
                                    MoneyAdvance = (long)item.TransactionMoney,
                                    MoneyUnrefunded = (long)item.TransactionMoney
                                });
                            }
                        }
                        response.Data = resultData;
                    }
                }
                response.SetSucces();
                return response;
            });
        }
    }
}
