﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.InvoiceService;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Queries
{
    public class GetHistoryCommentInvoiceQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long InvoiceID { get; set; }
        public int InvoiceType { get; set; } = 0;
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class GetHistoryCommentInvoiceQueryQueryHandler : IRequestHandler<GetHistoryCommentInvoiceQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> _logInvoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;

        LMS.Common.Helper.Utils _common;
        public GetHistoryCommentInvoiceQueryQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> logInvoiceTab, LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab)
        {
            _logInvoiceTab = logInvoiceTab;
            _userTab = userTab;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetHistoryCommentInvoiceQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                List<HistoryCommentInvoiceView> historyCommentInvoiceViews = new List<HistoryCommentInvoiceView>();
                ResponseActionResult response = new ResponseActionResult();
                var lstLog = _logInvoiceTab.WhereClause(x => x.InvoiceID == request.InvoiceID && x.InvoiceType == request.InvoiceType).Query();
                lstLog = lstLog.OrderByDescending(x => x.CreateDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                response.SetSucces();
                response.Data = historyCommentInvoiceViews;
                if (!lstLog.Any()) return response;
                var lstUserIds = lstLog.Select(x => x.CreateBy).ToList();
                var dicUser = _userTab.SelectColumns(x => x.UserID, x => x.UserName).WhereClause(x => lstUserIds.Contains(x.UserID)).Query().ToDictionary(x => x.UserID, x => x.UserName);
                foreach (var item in lstLog)
                {
                    historyCommentInvoiceViews.Add(new HistoryCommentInvoiceView()
                    {
                        LogInvoiceID = item.LogInvoiceID,
                        CreateDate = item.CreateDate.Value,
                        Note = item.Note,
                        UserName = dicUser.GetValueOrDefault(item.CreateBy ?? 0),
                        ImageUrl = item.PathImg,
                        StatusName = item.Status == null || ((Invoice_Status)(item.Status)).GetDescription() == null ? "" : ((Invoice_Status)item.Status).GetDescription(),
                        Status = item.Status ?? 0
                    });
                }
                int totalCount = lstLog.Count();

                response.Total = totalCount;
                return response;
            });
        }
    }
}
