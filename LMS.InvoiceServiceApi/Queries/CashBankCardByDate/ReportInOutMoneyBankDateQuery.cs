﻿using LMS.Common.Constants;
using LMS.InvoiceServiceApi.Domain.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Queries.CashBankCardByDate
{
    public class ReportInOutMoneyBankDateQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long BankCardID { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
    public class ReportInOutMoneyBankDateHandler : IRequestHandler<ReportInOutMoneyBankDateQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCashBankCardByDate> _cashBankCardByDateTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingDateBankCard> _settingDateBankCard;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        ILogger<ReportInOutMoneyBankDateHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public ReportInOutMoneyBankDateHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblCashBankCardByDate> cashBankCardByDateTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingDateBankCard> settingDateBankCard,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab,
            ILogger<ReportInOutMoneyBankDateHandler> logger)
        {
            _settingDateBankCard = settingDateBankCard;
            _cashBankCardByDateTab = cashBankCardByDateTab;
            _bankCardTab = bankCardTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(ReportInOutMoneyBankDateQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                return await Task.Run(() =>
                {
                    var fromDate = DateTime.ParseExact(request.FromDate, _common.DateTimeDDMMYYYY, CultureInfo.InvariantCulture);
                    var toDate = DateTime.ParseExact(request.ToDate, _common.DateTimeDDMMYYYY, CultureInfo.InvariantCulture).AddDays(1);
                    var result = new List<Domain.Models.CashBankCardByDate.ReportInOutMoneyBankDate>();

                    _cashBankCardByDateTab.WhereClause(x => x.ForDate >= fromDate && x.ForDate < toDate);
                    if (request.BankCardID != 0)
                    {
                        _cashBankCardByDateTab.WhereClause(x => x.BankCardID == request.BankCardID);
                        _settingDateBankCard.WhereClause(x => x.BankCardID == request.BankCardID);
                    }
                    else
                    {
                        var lstBankCardIDActive = _bankCardTab.SelectColumns(x => x.BankCardID).WhereClause(x => x.Status == (int)BankCard_Status.Active).Query().Select(x => x.BankCardID).ToList();
                        _cashBankCardByDateTab.WhereClause(x => lstBankCardIDActive.Contains(x.BankCardID)); 
                    }
                    var dictcashBankCardByDate = _cashBankCardByDateTab.Query().GroupBy(x => x.BankCardID).ToDictionary(x => x.Key, x => x.OrderByDescending(m => m.ForDate).ToList());
                    var dictBankCardSettingInfos = _settingDateBankCard.WhereClause(x => x.DateApply >= fromDate && x.DateApply < toDate)
                                                                       .Query().GroupBy(x => x.BankCardID).ToDictionary(x => x.Key, x => x.OrderByDescending(m => m.DateApply).ToList());
                    if (dictcashBankCardByDate != null && dictcashBankCardByDate.Count() > 0)
                    {
                        foreach (var k in dictcashBankCardByDate)
                        {
                            long moneyInBound = 0;
                            long moneyOutBound = 0;
                            long moneyBeginDate = k.Value.Last().MoneyBeginDate;
                            foreach (var item in k.Value)
                            {
                                moneyInBound += item.MoneyInBound;
                                moneyOutBound += item.MoneyOutBound;
                            }
                            // giá trị đầu ngày nằm trong khoảng của setting bankcard
                            // không hiện giá trị tiền
                            if (dictBankCardSettingInfos.ContainsKey(k.Key))
                            {
                                moneyBeginDate = 0;
                                var dateApplyLatest = dictBankCardSettingInfos[k.Key].First();
                                if (dateApplyLatest.DateApply.Date == fromDate.Date)
                                {
                                    moneyBeginDate = dateApplyLatest.TotalMoney;
                                }
                            }
                            var bankCardDateDetail = new Domain.Models.CashBankCardByDate.ReportInOutMoneyBankDate
                            {
                                BankCardID = k.Key,
                                BankCardName = k.Value[0].BankCardName,
                                MoneyInBound = moneyInBound,
                                MoneyOutBound = moneyOutBound,
                                TotalMoneyTransaction = moneyInBound + moneyOutBound,
                                MoneyBeginDate = moneyBeginDate,
                                MoneyEndDate = k.Value.First().MoneyEndDate
                            };
                            result.Add(bankCardDateDetail);
                        }
                    }
                    response.SetSucces();
                    response.Data = result.OrderByDescending(x => x.MoneyInBound).ThenBy(x => x.MoneyOutBound).ThenByDescending(x => x.MoneyBeginDate).ToList();
                    return response;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"{System.Reflection.MethodBase.GetCurrentMethod().Name}_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
