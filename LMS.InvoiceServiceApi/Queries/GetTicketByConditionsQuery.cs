﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Queries
{
    public class GetTicketByConditionsQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long FromDepartmentID { get; set; }
        public long ToDepartmentID { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int Type { get; set; } = (int)StatusCommon.SearchAll;
        public int Status { get; set; } = (int)StatusCommon.SearchAll;
        public int DepartmentID { get; set; } = (int)StatusCommon.SearchAll;
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class GetTicketByConditionsQueryHandler : IRequestHandler<GetTicketByConditionsQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTicket> _ticketTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> _departmentTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;

        LMS.Common.Helper.Utils _common;
        ILogger<GetTicketByConditionsQueryHandler> _logger;
        public GetTicketByConditionsQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblTicket> ticketTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> departmentTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            ILogger<GetTicketByConditionsQueryHandler> logger)
        {
            _ticketTab = ticketTab;
            _departmentTab = departmentTab;
            _userTab = userTab;
            _logger = logger;
            _common = new Utils();
        }
        public async Task<ResponseActionResult> Handle(GetTicketByConditionsQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            DateTime fromDate = DateTime.Now.AddDays(-30);
            DateTime toDate = DateTime.Now.AddDays(1);
            if (!string.IsNullOrEmpty(request.FromDate))
            {
                fromDate = DateTime.ParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                fromDate = fromDate.AddSeconds(-1);
            }
            if (!string.IsNullOrEmpty(request.ToDate))
            {
                toDate = DateTime.ParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                toDate = toDate.AddDays(1);
            }
            _ticketTab.WhereClause(x => x.CreateDate >= fromDate && x.CreateDate < toDate);
            if (request.FromDepartmentID > 0)
            {
                _ticketTab.WhereClause(x => x.FromDepartmentID == request.FromDepartmentID);
            }
            if (request.ToDepartmentID > 0)
            {
                _ticketTab.WhereClause(x => x.ToDepartmentID == request.ToDepartmentID);
            }
            if (request.Type != (int)StatusCommon.SearchAll)
            {
                _ticketTab.WhereClause(x => x.Type == request.Type);
            }
            if (request.Status != (int)StatusCommon.SearchAll)
            {
                _ticketTab.WhereClause(x => x.Status == request.Status);
            }
            if (request.DepartmentID != (int)StatusCommon.SearchAll)
            {
                _ticketTab.WhereClause(x => x.ToDepartmentID == request.DepartmentID || x.FromDepartmentID == request.DepartmentID);
            }
            var lstTicketData = _ticketTab.Query().OrderByDescending(x => x.CreateDate).ToList();
            response.SetSucces();

            List<LMS.Entites.Dtos.InvoiceService.TicketListView> lstResult = new List<Entites.Dtos.InvoiceService.TicketListView>();
            ResponseForDataTable dataTable = new ResponseForDataTable
            {
                RecordsTotal = 0,
                Data = lstResult,
                RecordsFiltered = 0,
                Draw = 1
            };
            lstTicketData = lstTicketData.Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
            if (lstTicketData != null && lstTicketData.Any())
            {
                dataTable.RecordsTotal = lstTicketData.Count();
                var dicDepartment = _departmentTab.Query().ToDictionary(x => x.DepartmentID, x => x);
                var lstUserAction = lstTicketData.Select(x => x.FromUserID).ToList().Union(lstTicketData.Select(x => x.ToUserID));
                var dicUser = _userTab.SelectColumns(x => x.UserID, x => x.FullName, x => x.UserName).Query().ToDictionary(x => x.UserID, x => x);
                foreach (var item in lstTicketData)
                {
                    lstResult.Add(new Entites.Dtos.InvoiceService.TicketListView()
                    {
                        CreateDate = item.CreateDate,
                        FromDepartmentID = item.FromDepartmentID,
                        FromUserID = item.FromUserID,
                        JsonExtra = item.JsonExtra,
                        ModifyDate = item.ModifyDate,
                        Note = item.Note,
                        Status = item.Status,
                        TicketID = item.TicketID,
                        ModifyUserID = item.ModifyUserID,
                        ToDepartmentID = item.ToDepartmentID,
                        ToUserID = item.ToUserID,
                        Type = item.Type,
                        TypeName = ((Ticket_Type)item.Type).GetDescription(),
                        FromDepartment = dicDepartment.ContainsKey(item.FromDepartmentID ?? 0) ? dicDepartment[item.FromDepartmentID ?? 0].DepartmentName : "",
                        ToDepartment = dicDepartment.ContainsKey(item.ToDepartmentID ?? 0) ? dicDepartment[item.ToDepartmentID ?? 0].DepartmentName : "",
                        FromUserName = dicUser.GetValueOrDefault(item.FromUserID ?? 0).UserName,
                        ToUserName = item.ToUserID > 0 ? dicUser.GetValueOrDefault(item.ToUserID ?? 0).UserName : "",
                    });
                }
            }
            response.Data = dataTable;
            return response;
        }
    }
}
