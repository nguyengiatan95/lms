﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Queries
{
    public class TicketCountWaitingQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long DepartmentID { get; set; }
    }
    public class TicketCountWaitingQueryHandler : IRequestHandler<TicketCountWaitingQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTicket> _ticketTab;
        LMS.Common.Helper.Utils _common;
        ILogger<TicketCountWaitingQueryHandler> _logger;
        public TicketCountWaitingQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblTicket> ticketTab,
            ILogger<TicketCountWaitingQueryHandler> logger)
        {
            _ticketTab = ticketTab;
            _logger = logger;
            _common = new Utils();
        }
        public async Task<ResponseActionResult> Handle(TicketCountWaitingQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            DateTime fromDate = DateTime.Now.AddDays(-30);
            DateTime toDate = DateTime.Now.AddDays(1);
            long totalCount = 0;
            if (request.DepartmentID > 0)
            {
                int statusWaiting = (int)Ticket_Status.Waiting;
                totalCount = _ticketTab.SelectColumns(x => x.TicketID).WhereClause(x => x.ToDepartmentID == request.DepartmentID && x.Status == statusWaiting).Query().Count();
            }
            response.SetSucces();
            response.Data = totalCount;
            return response;
        }
    }
}
