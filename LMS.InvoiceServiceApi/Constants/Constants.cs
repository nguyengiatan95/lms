﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi
{
    public class Constants
    {
        public const int DateTransactionMinApply = 30;
        public const int MaxLengthNote = 500;
        public static string LOS_KEY = "lms@#123";
    }
}
