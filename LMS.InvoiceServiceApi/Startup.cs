using Confluent.Kafka;
using FluentValidation;
using FluentValidation.AspNetCore;
using LMS.InvoiceServiceApi.Commands;
using LMS.InvoiceServiceApi.RestClients;
using LMS.InvoiceServiceApi.Validators;
using LMS.Kafka.Consumer;
using LMS.Kafka.Interfaces;
using LMS.Kafka.Producer;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Steeltoe.Discovery.Client;
using Steeltoe.Discovery.Eureka;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<KestrelServerOptions>(Configuration.GetSection("Kestrel"));
            services.AddDiscoveryClient(Configuration);
            services.AddServiceDiscovery(options => options.UseEureka());
            services.AddSingleton(Configuration);
            services.AddControllers().AddJsonOptions(opts => opts.JsonSerializerOptions.PropertyNamingPolicy = null);
            LMS.Common.Configuration.ConnectStringSetting.DefaultConnectString = Configuration["ConnectStringSetting:DefaultConnectString"];
            services.AddTransient(typeof(Common.DAL.ITableHelper<>), typeof(Common.DAL.TableHelper<>));
            //services.AddAutoMapper(typeof(Startup));

            services.AddRestClientsService();
            services.AddTransient<LMS.Common.Helper.IApiHelper, LMS.Common.Helper.ApiHelper>();
            services.AddMvc().AddFluentValidation();
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddHttpContextAccessor();
            services.AddTransient<IValidator<CreateInvoiceBankInterestCommand>, CreateInvoiceBankInterestCommandValidator>();
            services.AddTransient<IValidator<CreateInvoiceConsiderCustomerCommand>, CreateInvoiceConsiderCustomerCommandValidator>();
            services.AddTransient<IValidator<CreateInvoiceCustomerCommand>, CreateInvoiceCustomerCommandValidator>();
            services.AddTransient<IValidator<CreateInvoiceInternalCommand>, CreateInvoiceInternalCommandValidator>();
            services.AddTransient<IValidator<CreateInvoiceLenderCommand>, CreateInvoiceLenderCommandValidator>();
            services.AddTransient<IValidator<CreateInvoiceOnBehalfShopCommand>, CreateInvoiceOnBehalfShopCommandValidator>();
            services.AddTransient<IValidator<CreateInvoicePartnerCommand>, CreateInvoicePartnerCommandValidator>();
            services.AddTransient<IValidator<CreateInvoicePaySlipCustomerCommand>, CreateInvoicePaySlipCustomerCommonValidator>();
            services.AddTransient<IValidator<VerifyInvoiceCustomerCommand>, VerifyInvoiceCustomerCommandValidator>();
            services.AddTransient<IValidator<CreateInvoiceOtherCommand>, CreateInvoiceOtherCommandValidator>();
            services.AddTransient<IValidator<CreateInvoiceBackMoneyCustomerCommand>, CreateInvoiceBackMoneyCustomerCommandValidator>();
            services.AddTransient<IValidator<VerifyInvoiceConsiderCustomerCommand>, VerifyInvoiceConsiderCustomerCommandValidator>();
            services.AddTransient<IValidator<CreateInvoiceTransferMoneyStoreCommand>, CreateInvoiceTransferMoneyStoreCommandValidator>();
            services.AddTransient<IValidator<ReturnInvoiceConsiderCustomerCommand>, ReturnInvoiceConsiderCustomerCommandValidator>();
            services.AddTransient<IValidator<DeleteInvoiceCommand>, DeleteInvoiceCommandValidator>();

            AddServiceKafka(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseCors(options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseAuthorization();
            app.UseDiscoveryClient();
            //Add our new middleware to the pipeline
            app.UseMiddleware<LMS.Common.Helper.RequestResponseLoggingMiddleware>();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        public void AddServiceKafka(IServiceCollection services)
        {
            var clientConfig = new ClientConfig()
            {
                BootstrapServers = Configuration["Kafka:ClientConfigs:BootstrapServers"]
            };

            var producerConfig = new ProducerConfig(clientConfig);
            var consumerConfig = new ConsumerConfig(clientConfig)
            {
                GroupId = Configuration["spring:application:name"],
                //EnableAutoCommit = true,
                AutoOffsetReset = AutoOffsetReset.Earliest,
                StatisticsIntervalMs = 5000,
                SessionTimeoutMs = 6000
            };

            services.AddSingleton(producerConfig);
            services.AddSingleton(consumerConfig);

            services.AddSingleton(typeof(IKafkaProducer<,>), typeof(KafkaProducer<,>));
            services.AddSingleton(typeof(IKafkaConsumer<,>), typeof(KafkaConsumer<,>));

            services.AddHostedService<KafkaServiceConsumer>();
            services.AddScoped<IKafkaHandler<string, Kafka.Messages.Loan.LoanInsertSuccess>, KafkaEventHandlers.CreateInvoiceDisbursementByLoanIDHandler>();
        }
    }
}
