﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using LMS.Common.Constants;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace LMS.InvoiceServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceController : ControllerBase
    {
        private readonly IMediator _bus;
        public InvoiceController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("CreateInvoiceCustomer")]  // phiếu thu tiền khách hàng
        public async Task<ResponseActionResult> CreateInvoiceCustomer(Commands.CreateInvoiceCustomerCommand req)
        {
            return await _bus.Send(req);
        }
        [HttpPost]
        [Route("CreateInvoiceConsiderCustomer")] //  phiếu thu treo của khách
        public async Task<ResponseActionResult> CreateInvoiceConsiderCustomer(Commands.CreateInvoiceConsiderCustomerCommand req)
        {
            return await _bus.Send(req);
        }

        [HttpPost]
        [Route("VerifyInvoiceCustomer")]  //  xác nhận thông tin khách hàng của phiếu treo-ke toan
        public async Task<ResponseActionResult> VerifyInvoiceCustomer(Commands.VerifyInvoiceCustomerCommand req)
        {
            return await _bus.Send(req);
        }

        [HttpPost]
        [Route("CreateInvoiceLender")]  //  phiếu thu lender , hoàn ứng, bảo hiểm,lender rut von, tạm ứng cho lender, phiếu đặt cọc
        public async Task<ResponseActionResult> CreateInvoiceLender(Commands.CreateInvoiceLenderCommand req)
        {
            return await _bus.Send(req);
        }

        [HttpPost]
        [Route("CreateInvoiceBankInterest")]  //  phiếu thu tiền lãi ngân hang,thu khác, phiếu chi phí Nh,chi khác, Trả phí cho NH
        public async Task<ResponseActionResult> CreateInvoiceBankInterest(Commands.CreateInvoiceBankInterestCommand req)
        {
            return await _bus.Send(req);
        }
        //[HttpPost]
        //[Route("CreateInvoiceConsiderCustomer")] //  phiếu thu treo lender
        //public async Task<ResponseActionResult> CreateInvoiceConsiderLender(Commands.CreateInvoiceConsiderCustomerCommand req)
        //{
        //    return await _bus.Send(req);
        //}
        [HttpPost]
        [Route("CreateInvoicePaySlipCustomer")] //  phiếu giải ngân kh
        public async Task<ResponseActionResult> CreateInvoicePaySlipCustomer(Commands.CreateInvoicePaySlipCustomerCommand req)
        {
            return await _bus.Send(req);
        }
        [HttpPost]
        [Route("CreateInvoiceInternal")] //  phiếu thu ,chi nội bộ
        public async Task<ResponseActionResult> CreateInvoiceInternal(Commands.CreateInvoiceInternalCommand req)
        {
            return await _bus.Send(req);
        }

        [HttpPost]
        [Route("CreateInvoiceBackMoneyCustomer")] //  trả tiền thừa cho kh
        public async Task<ResponseActionResult> CreateInvoiceBackMoneyCustomer(Commands.CreateInvoiceBackMoneyCustomerCommand req)
        {
            return await _bus.Send(req);
        }

        [HttpPost]
        [Route("CreateInvoiceOnBehalfShop")] //  phiếu thu hộ cửa hàng,
        public async Task<ResponseActionResult> CreateInvoiceOnBehalfShop(Commands.CreateInvoiceOnBehalfShopCommand req)
        {
            return await _bus.Send(req);
        }

        //[HttpPost]
        //[Route("CreateInvoicePartner")] //  phiếu thu đối tác
        //public async Task<ResponseActionResult> CreateInvoicePartner(Commands.CreateInvoicePartnerCommand req)
        //{
        //    return await _bus.Send(req);
        //}
        [HttpPost]
        [Route("CreateInvoiceOther")] //  phiếu thu ,chi khác
        public async Task<ResponseActionResult> CreateInvoiceOther(Commands.CreateInvoiceOtherCommand req)
        {
            return await _bus.Send(req);
        }
        [HttpPost]
        [Route("VerifyInvoiceConsiderCustomer")] //  xac minh phiếu treo khách hàng hub, thn
        public async Task<ResponseActionResult> VerifyInvoiceConsiderCustomer(Commands.VerifyInvoiceConsiderCustomerCommand req)
        {
            return await _bus.Send(req);
        }


        [Route("GetInvoiceInfosByCondition")]
        [HttpPost]
        public async Task<ResponseActionResult> GetInvoiceInfosByCondition(Queries.GetLstInvoiceItemViewQuery req)
        {
            return await _bus.Send(req);
        }

        [Route("InvoiceInfosByBankCard")]
        [HttpPost]
        public async Task<ResponseActionResult> GetInvoiceInfosByBankCard(Queries.GetInvoiceInfosByBankCardQuery req)
        {
            return await _bus.Send(req);
        }

        [HttpPost]
        [Route("CreateInvoiceTransferMoneyStore")] //  phiếu chuyển tiền cho cửa hàng ,công nợ , lý do khác
        public async Task<ResponseActionResult> CreateInvoiceTransferMoneyStore(Commands.CreateInvoiceTransferMoneyStoreCommand req)
        {
            return await _bus.Send(req);
        }

        [HttpPost]
        [Route("SearchInvoiceAdvanceSlipLender")] //  search phiếu tạm ứng lender
        public async Task<ResponseActionResult> SearchInvoiceAdvanceSlipLender(Queries.SearchInvoiceAdvanceSlipLenderQuery req)
        {
            return await _bus.Send(req);
        }

        [HttpPost]
        [Route("GetHistoryCommentInvoice")] // lịch sử comment
        public async Task<ResponseActionResult> GetHistoryCommentInvoice(Queries.GetHistoryCommentInvoiceQuery req)
        {
            return await _bus.Send(req);
        }


        [HttpPost]
        [Route("DeleteInvoice")] //  Hủy phiếu
        public async Task<ResponseActionResult> DeleteInvoice(Commands.DeleteInvoiceCommand req)
        {
            return await _bus.Send(req);
        }
        [HttpPost]
        [Route("ReturnInvoiceConsiderCustomer")] //  Tra lại phiếu treo
        public async Task<ResponseActionResult> ReturnInvoiceConsiderCustomer(Commands.ReturnInvoiceConsiderCustomerCommand req)
        {
            return await _bus.Send(req);
        }
        [HttpPost]
        [Route("GetListImgInvoiceConsider")] // danh sách ảnh của phiếu treo
        public async Task<ResponseActionResult> GetListImgInvoiceConsider(Queries.GetListImgInvoiceConsiderQuery req)
        {
            return await _bus.Send(req);
        }
        [HttpPost]
        [Route("UpdatePathImg")] //Update link ảnh
        public async Task<ResponseActionResult> UpdatePathImg(Commands.UpdatePathImgCommand req)
        {
            return await _bus.Send(req);
        }
        [HttpPost]
        [Route("AgChangeBankCardIDInvoice")] //đổi bankcardid
        public async Task<ResponseActionResult> AgChangeBankCardIDInvoice(Commands.AgChangeBankCardIDInvoiceCommand req)
        {
            return await _bus.Send(req);
        }
        [HttpPost]
        [Route("AgCancelInvoice")] //đổi bankcardid
        public async Task<ResponseActionResult> AgCancelInvoice(Commands.AgCancelInvoiceCommand req)
        {
            return await _bus.Send(req);
        }


        //invoiceTHN
        [Route("GetInvoiceInfosByConditionTHN")]
        [HttpPost]
        public async Task<ResponseActionResult> GetInvoiceInfosByConditionTHN(Queries.GetLstInvoiceItemViewQuery req)
        {
            req.InvoiceSubType = (int)LMS.Common.Constants.GroupInvoiceType.ReceiptCustomerConsider;
            req.InvoiceType = (int)LMS.Common.Constants.InvoiceType.Waiting;
            return await _bus.Send(req);
        }
    }
}