﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LMS.InvoiceServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TicketController : ControllerBase
    {
        private readonly IMediator _bus;
        public TicketController(IMediator bus)
        {
            this._bus = bus;
        }

        [HttpPost]
        [Route("GetTicketByConditions")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetTicketByConditions(Queries.GetTicketByConditionsQuery req)
        {
            return await _bus.Send(req);
        }

        [HttpPost]
        [Route("CreateTicket")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateTicket(Commands.CreateTicketCommand req)
        {
            return await _bus.Send(req);
        }
        [HttpPost]
        [Route("UpdateTicket")]
        public async Task<LMS.Common.Constants.ResponseActionResult> UpdateTicket(Commands.UpdateTicketCommand req)
        {
            return await _bus.Send(req);
        }

        [HttpPost]
        [Route("ChangeStatusTicket")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ChangeStatusTicket(Commands.ChangeStatusTicketCommand req)
        {
            return await _bus.Send(req);
        }

        [HttpGet]
        [Route("CountNotifyTicket")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CountNotifyTicket(int departmentID)
        {
            Queries.TicketCountWaitingQuery req = new Queries.TicketCountWaitingQuery()
            {
                DepartmentID = departmentID
            };
            return await _bus.Send(req);
        }
    }
}