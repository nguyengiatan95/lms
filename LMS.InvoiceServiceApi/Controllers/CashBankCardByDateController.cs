﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Steeltoe.Common.Discovery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CashBankCardByDateController : ControllerBase
    {
        private readonly IMediator _bus;
        public CashBankCardByDateController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("ReportInOutMoneyBankDate")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ReportInOutMoneyBankDate(Queries.CashBankCardByDate.ReportInOutMoneyBankDateQuery req)
        {
            return await _bus.Send(req);
        }
    }
}