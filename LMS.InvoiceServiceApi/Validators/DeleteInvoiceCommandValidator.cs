﻿using FluentValidation;
using LMS.Common.Constants;
using LMS.InvoiceServiceApi.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Validators
{
   public class DeleteInvoiceCommandValidator : AbstractValidator<DeleteInvoiceCommand>
    {
        public DeleteInvoiceCommandValidator()
        {
            var _common = new LMS.InvoiceServiceApi.Helper.Utils();
            RuleFor(x => x.InvoiceID).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.InvoiceNotExits);
            RuleFor(x => x.Note).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.NoteNull);
            RuleFor(x => x.Note).Must((note) => { return _common.ValidateLength(note, Constants.MaxLengthNote); }).WithMessage(LMS.Common.Constants.MessageConstant.NoteMaxLength);
            RuleFor(x => x.UserIDCreate).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.UserIDCreateNull);
        }

    }
}
