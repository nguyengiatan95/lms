﻿using FluentValidation;
using LMS.Common.Constants;
using LMS.InvoiceServiceApi.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Validators
{
    public class VerifyInvoiceCustomerCommandValidator : AbstractValidator<VerifyInvoiceCustomerCommand>
    {
        public VerifyInvoiceCustomerCommandValidator()
        {
            var _common = new LMS.InvoiceServiceApi.Helper.Utils();
            // tạm thời bỏ check đoạn này, do call từ ag qua truyền tham số khác
            //RuleFor(x => x.InvoiceID).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.InvoiceNotExits);
            //RuleFor(x => x.TotalMoney).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.TotalMoneyNull);
            //RuleFor(x => x.CustomerID).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.CustomerIDNull);
            //RuleFor(x => x.BankCardID).GreaterThanOrEqualTo(0).WithMessage(LMS.Common.Constants.MessageConstant.BankCardIDNull);
           // RuleFor(x => x.Note).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.NoteNull);
            //RuleFor(x => x.Note).Must((note) => { return _common.ValidateLength(note, Constants.MaxLengthNote); }).WithMessage(LMS.Common.Constants.MessageConstant.NoteMaxLength);
            RuleFor(x => x.UserIDCreate).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.UserIDCreateNull);
            //RuleFor(x => x.StrTransactionDate).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.StrTransactionDateNull);
           // RuleFor(x => x.StrTransactionDate).Must((surname) => { return _common.ValidateDate(surname, _common.DateTimeDDMMYYYYHHMMSS); }).WithMessage(LMS.Common.Constants.MessageConstant.CreateDateBadFormat);
        }
    }
}
