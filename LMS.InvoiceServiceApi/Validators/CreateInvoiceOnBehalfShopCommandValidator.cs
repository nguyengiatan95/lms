﻿using FluentValidation;
using LMS.Common.Constants;
using LMS.InvoiceServiceApi.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace LMS.InvoiceServiceApi.Validators
{
    public class CreateInvoiceOnBehalfShopCommandValidator : AbstractValidator<CreateInvoiceOnBehalfShopCommand>
    {
        public CreateInvoiceOnBehalfShopCommandValidator()
        {
            var _common = new LMS.InvoiceServiceApi.Helper.Utils();
            List<int> lstInvoiceSubType = new List<int>() {
                (int)GroupInvoiceType.ReceiptOnBehalfShop
            };
            RuleFor(x => x.InvoiceSubType).Must(x => lstInvoiceSubType.Contains(x)).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
            RuleFor(x => x.ShopID).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.NotIdentifiedShopID);
            RuleFor(x => x.ToShopID).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.NotIdentifiedShopID);
            RuleFor(x => x.TotalMoney).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.NotIdentifiedTotalMoney);
            RuleFor(x => x.CustomerID).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.TotalMoneyNull);
            RuleFor(x => x.BankCardID).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.NotIdentifiedFromBankCardID);
            RuleFor(x => x.Note).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.NoteNull);
            RuleFor(x => x.Note).Must((note) => { return _common.ValidateLength(note, Constants.MaxLengthNote); }).WithMessage(LMS.Common.Constants.MessageConstant.NoteMaxLength);
            RuleFor(x => x.UserIDCreate).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.UserIDCreateNull);
            RuleFor(x => x.StrTransactionDate).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.StrTransactionDateNull);
            RuleFor(x => x.StrTransactionDate).Must((surname) => { return _common.ValidateDate(surname, _common.DateTimeDDMMYYYYHHMMSS); }).WithMessage(LMS.Common.Constants.MessageConstant.CreateDateBadFormat);
        }
    }
}
