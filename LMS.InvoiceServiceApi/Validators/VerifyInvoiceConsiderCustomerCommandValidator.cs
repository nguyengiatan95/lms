﻿using FluentValidation;
using LMS.Common.Constants;
using LMS.InvoiceServiceApi.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Validators
{
    public class VerifyInvoiceConsiderCustomerCommandValidator : AbstractValidator<VerifyInvoiceConsiderCustomerCommand>
    {
        public VerifyInvoiceConsiderCustomerCommandValidator()
        {
            var _common = new LMS.InvoiceServiceApi.Helper.Utils();
            // RuleFor(x => x.InvoiceID).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.InvoiceNotExits);
            //RuleFor(x => x.CustomerID).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.NotIdentifiedCustomerID);
            //RuleFor(x => x.Note).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.NoteNull);
            //RuleFor(x => x.Note).Must((note) => { return _common.ValidateLength(note, Constants.MaxLengthNote); }).WithMessage(LMS.Common.Constants.MessageConstant.NoteMaxLength);
            RuleFor(x => x.UserIDCreate).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.UserIDCreateNull);
            //RuleFor(x => x.BankCardID).GreaterThan(0).WithMessage(LMS.Common.Constants.MessageConstant.BankCardIDNull);
        }

    }
}
