﻿using LMS.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.RestClients
{
    public interface ICollectionService
    {
        Task<ResponseActionResult> InsertConfirmSuspendedMoney(long invoiceTimaID, int userID, DateTime createDate, List<string> pathImage,
            string note, long lmsConfirmID, long timaloanID, long timaCustomerID);
    }
    public class CollectionService : ICollectionService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        public CollectionService(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }

        public async Task<ResponseActionResult> InsertConfirmSuspendedMoney(long invoiceTimaID, int userID, DateTime createDate, List<string> pathImage,
            string note, long lmsConfirmID, long timaloanID, long timaCustomerID)
        {
            var req = new
            {
                InvoiceTimaID = invoiceTimaID,
                UserID = userID,
                CreateDate = createDate.ToString("dd/MM/yyyy HH:mm"),
                Note = note,
                LmsConfirmID = lmsConfirmID,
                PathImage = pathImage,
                LoanID = timaloanID,
                CustomerID = timaCustomerID
            };
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CollectionServiceApi}{ActionApiInternal.CollectionAPI_InsertConfirmSuspendedMoney}";
            return await _apiHelper.ExecuteAsync(url.ToString(), RestSharp.Method.POST, req);
        }
    }
}
