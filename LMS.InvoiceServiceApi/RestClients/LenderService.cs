﻿using LMS.Common.Constants;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.RestClients
{
    public interface ILenderService
    {
        Task<ResponseActionResult> UpdateMoneyLender(long lenderID, long moneyAdd, string description);
        Task<LMS.Common.Constants.ResponseActionResult> CreateOrUpdateLenderFromAG(long lenderID);
    }
    public class LenderService : ILenderService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        LMS.Common.Helper.Utils _common;
        ILogger<LenderService> _logger;
        public LenderService(LMS.Common.Helper.IApiHelper apiHelper, ILogger<LenderService> logger)
        {
            _apiHelper = apiHelper;
            _common = new Common.Helper.Utils();
            _logger = logger;
        }

        public async Task<ResponseActionResult> UpdateMoneyLender(long lenderID, long moneyAdd, string description)
        {
            var req = new
            {
                LenderID = lenderID,
                MoneyAdd = moneyAdd,
                Description = description
            };
            var url = $"{LMS.Common.Constants.ServiceHostInternal.LenderService}{ActionApiInternal.Lender_UpdateMoneyLender}";
            return await _apiHelper.ExecuteAsync(url.ToString(), RestSharp.Method.POST, req);
        }
        public async Task<Common.Constants.ResponseActionResult> CreateOrUpdateLenderFromAG(long lenderID)
        {
            try
            {
                var url = $"{LMS.Common.Constants.ServiceHostInternal.LenderService}{LMS.Common.Constants.ActionApiInternal.Lender_CreateOrUpdateLenderFromAG}";
                var dataPost = new { AGLenderID = lenderID };
                return await _apiHelper.ExecuteAsync(url, RestSharp.Method.POST, dataPost);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateOrUpdateLenderFromAG_Exception|lenderID={lenderID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
    }
}
