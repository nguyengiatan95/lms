﻿using LMS.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.RestClients
{
    public interface ICustomerService
    {
        Task<ResponseActionResult> UpdateMoneyCustomer(long customerID, long moneyAdd, string description, DateTime? transactionDateTopUp = null);
    }
    public class CustomerService : ICustomerService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        public CustomerService(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }

        public async Task<ResponseActionResult> UpdateMoneyCustomer(long customerID, long moneyAdd, string description, DateTime? transactionDateTopUp = null)
        {
            var req = new
            {
                CustomerID = customerID,
                MoneyAdd = moneyAdd,
                Description = description,
                TransactionDateTopUp = transactionDateTopUp?.ToString(TimaSettingConstant.DateTimeDayMonthYearFull)
            };
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CustomerService}{ActionApiInternal.Customer_UpdateMoneyCustomer}";
            return await _apiHelper.ExecuteAsync(url.ToString(), RestSharp.Method.POST, req);
        }
    }
}
