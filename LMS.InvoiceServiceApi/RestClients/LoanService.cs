﻿using LMS.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.RestClients
{
    public interface ILoanService
    {
        Task<ResponseActionResult> ProcessSummaryBankCardByBankCardID(List<long> bankCardIDs, DateTime transactionDate);
    }
    public class LoanService : ILoanService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        public LoanService(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }

        public async Task<ResponseActionResult> ProcessSummaryBankCardByBankCardID(List<long> bankCardIDs, DateTime transactionDate)
        {
            var dataPost = new
            {
                BankCardIDs = bankCardIDs,
                TransactionDate = transactionDate.ToString(TimaSettingConstant.DateTimeDayMonthYear)
            };
            var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{ActionApiInternal.BankCard_ProcessSummaryBankCardByBankCardID}";
            return await _apiHelper.ExecuteAsync(url, RestSharp.Method.POST, dataPost);
        }
    }
}
