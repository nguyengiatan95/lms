﻿using Microsoft.Extensions.DependencyInjection;

namespace LMS.InvoiceServiceApi.RestClients
{
    public static class RestClientsInstaller
    {
        public static IServiceCollection AddRestClientsService(this IServiceCollection services)
        {
            services.AddTransient(typeof(ICustomerService), typeof(CustomerService));
            services.AddTransient(typeof(ILenderService), typeof(LenderService));
            services.AddTransient(typeof(ILoanService), typeof(LoanService));
            services.AddTransient(typeof(ICollectionService), typeof(CollectionService));
            return services;
        }
    }
}
