﻿using LMS.Common.Constants;
using LMS.Kafka.Interfaces;
using LMS.Kafka.Messages.Loan;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.KafkaEventHandlers
{
    public class CreateInvoiceDisbursementByLoanIDHandler : IKafkaHandler<string, LMS.Kafka.Messages.Loan.LoanInsertSuccess>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        private readonly IMediator _bus;
        LMS.Common.Helper.Utils _common = new Common.Helper.Utils();
        ILogger<CreateInvoiceDisbursementByLoanIDHandler> _logger;
        public CreateInvoiceDisbursementByLoanIDHandler(IMediator bus,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            ILogger<CreateInvoiceDisbursementByLoanIDHandler> logger)
        {
            _bus = bus;
            _loanTab = loanTab;
            _logger = logger;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> HandleAsync(string key, LMS.Kafka.Messages.Loan.LoanInsertSuccess value)
        {
            LMS.Common.Constants.ResponseActionResult response = new ResponseActionResult();
            try
            {
                var loanInfos = await _loanTab.WhereClause(x => x.LoanID == value.LoanID).QueryAsync();
                if (loanInfos == null || !loanInfos.Any())
                {
                    _logger.LogError($"CreateInvoiceDisbursementByLoanIDHandler|value={_common.ConvertObjectToJSonV2(value)}|Not_found_loan");
                    return response;
                }

                var loanInfo = loanInfos.First();
                string note = "";
                long moneyTranferToCustomer = 0;
                if (loanInfo.SourceMoneyDisbursement != (int)Loan_SourceMoneyDisbursement.EscrowSource)
                {
                    return response;                   
                }
                note = string.Format(TimaSettingConstant.SystemDisbursementToCustomer, loanInfo.CustomerName.ToUpper());
                moneyTranferToCustomer = loanInfo.TotalMoneyDisbursement - Convert.ToInt64(loanInfo.MoneyFeeInsuranceOfCustomer + loanInfo.MoneyFeeInsuranceMaterialCovered);

                Commands.CreateInvoicePaySlipCustomerCommand request = new Commands.CreateInvoicePaySlipCustomerCommand
                {
                    BankCardID = (long)loanInfo.BankCardID,
                    CustomerID = loanInfo.CustomerID,
                    LoanID = loanInfo.LoanID,
                    Note = note,
                    ShopID = loanInfo.OwnerShopID,
                    StrTransactionDate = loanInfo.FromDate.ToString("dd-MM-yyyy HH:mm:ss"),
                    TotalMoney = moneyTranferToCustomer,
                    UserIDCreate = value.CreateBy
                };
                return await _bus.Send(request);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateInvoiceDisbursementByLoanIDHandler|value={_common.ConvertObjectToJSonV2(value)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
