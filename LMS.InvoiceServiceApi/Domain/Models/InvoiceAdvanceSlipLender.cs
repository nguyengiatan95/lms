﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Domain.Models
{
    public class InvoiceAdvanceSlipLender
    {
        public string LenderName { get; set; }
        public DateTime ? TransactionDate { get; set; }
        public long MoneyAdvance { get; set; } // tiền tạm ứng
        public long MoneyUnrefunded { get; set; } // tền chưa hoàn ứng
        public long MoneyRefunded { get; set; } // tền đã hoàn ứng
        public long MoneyControl { get; set; } // tền đối soát
    }
}
