﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Domain.Models
{
    public class ImageLosUpload
    {
        public string FileName { get; set; }
        public string LocalPath { get; set; }
        public string S3Path { get; set; }
        public string Source { get; set; }
        public string FullPath { get; set; }
    }
}
