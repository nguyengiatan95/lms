﻿using LMS.Common.Helper;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Domain.Models
{
    public class InvoiceExtraInfo
    {
        public string SourceName { get; set; }
        public string DestinationName { get; set; }
        public string UserNameCreated { get; set; }
        public string ShopName { get; set; }
        public string UserNameVerified { get; set; }
        public List<string> LstImgVerified { get; set; }
        public string UserNameConfirmed { get; set; }
        public long LoanID { get; set; }
        public string CustomerName { get; set; }
        public string LoanContractCode { get; set; }
        public string BankCardSend { get; set; }
        public string BankCardReceived { get; set; }
        public string LenderCode { get; set; }
        public string LenderFullName { get; set; }
        public string LenderPhone { get; set; }
        public string LenderAddress { get; set; }
        public string ShopNameSend { get; set; }
        public string ShopNameReceived { get; set; }
        public string Description { get; set; }
    }
}
