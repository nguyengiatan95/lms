﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Domain.Tables
{
    [Table("TblTicket")]
    public class TblTicket
    {
        [Key]
        public long TicketID { get; set; }

        public string Note { get; set; }

        public long? FromUserID { get; set; }

        public long? FromDepartmentID { get; set; }

        public long? ToDepartmentID { get; set; }

        public long? ToUserID { get; set; }

        public int? Type { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? Status { get; set; }

        public string JsonExtra { get; set; }

        public long? ModifyUserID { get; set; }

        public DateTime? ModifyDate { get; set; }
    }
}
