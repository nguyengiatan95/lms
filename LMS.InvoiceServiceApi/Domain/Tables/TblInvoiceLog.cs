﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblLogInvoice")]
    public class TblLogInvoice
    {
        [Dapper.Contrib.Extensions.Key]
        public long LogInvoiceID { get; set; }

        public long InvoiceID { get; set; }

        public int? InvoiceType { get; set; }

        public int? Status { get; set; }

        public long? Amount { get; set; }

        public long? CustomerID { get; set; }

        public string Note { get; set; }

        public long? FromBankCardID { get; set; }

        public long? ToBankCardID { get; set; }

        public DateTime? TransactionDate { get; set; }

        public long? LoanID { get; set; }

        public long? FromShopID { get; set; }

        public long? ToShopID { get; set; }

        public long? CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string Request { get; set; }

        public long? LenderID { get; set; }
        public long PartnerID { get; set; }
        public string PathImg { get; set; }
    }
}
