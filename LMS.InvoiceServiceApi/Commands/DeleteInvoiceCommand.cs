﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Commands
{
    public class DeleteInvoiceCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public long InvoiceID { get; set; }
        [Required]
        public string Note { get; set; }
        [Required]
        public long UserIDCreate { get; set; }
        public long ShopID { get; set; }
    }
    public class DeleteInvoiceCommandHandler : IRequestHandler<DeleteInvoiceCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> _logInvoiceTab;
        ILogger<DeleteInvoiceCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public DeleteInvoiceCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> logInvoiceTab,
            ILogger<DeleteInvoiceCommandHandler> logger)
        {
            _invoiceTab = invoiceTab;
            _userTab = userTab;
            _logInvoiceTab = logInvoiceTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(DeleteInvoiceCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objInvoice = (await _invoiceTab.WhereClause(x => x.InvoiceID == request.InvoiceID).QueryAsync()).FirstOrDefault();
                if (objInvoice == null)
                {
                    response.Message = MessageConstant.InvoiceNotExits;
                    _logger.LogError($"DeleteInvoiceCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                if (objInvoice.InvoiceType != (int)InvoiceType.Waiting || objInvoice.Status == (int)Invoice_Status.Confirmed)
                {
                    response.Message = MessageConstant.NotDeleteInvoice;
                    _logger.LogError($"DeleteInvoiceCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                var objUser = await _userTab.GetByIDAsync(request.UserIDCreate);
                if (objUser == null)
                {
                    response.Message = MessageConstant.NotIdentifiedBankcardID;
                    _logger.LogError($"DeleteInvoiceCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                objInvoice.JobStatus = (int)Invoice_JobStatus.Unprocessed;
                objInvoice.Status = (int)Invoice_Status.Deleted;
                objInvoice.ModifyDate = DateTime.Now;
                if ((await _invoiceTab.UpdateAsync(objInvoice)))
                {
                    var logInvoice = new Domain.Tables.TblLogInvoice
                    {
                        InvoiceType = (int)GroupInvoiceType.Waiting,
                        Note = request.Note,
                        TransactionDate = DateTime.Now,
                        CreateDate = DateTime.Now,
                        FromShopID = request.ShopID,
                        CreateBy = request.UserIDCreate,
                        Status = (int)Invoice_Status.Deleted,
                        InvoiceID = objInvoice.InvoiceID,
                        Request = _common.ConvertObjectToJSonV2(request),
                    };
                    _ = _logInvoiceTab.InsertAsync(logInvoice);
                    response.SetSucces();
                    response.Data = objInvoice.InvoiceID;
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"DeleteInvoiceCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
