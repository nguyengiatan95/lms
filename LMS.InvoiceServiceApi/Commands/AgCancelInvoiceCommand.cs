﻿using LMS.Common.Constants;
using LMS.Kafka.Interfaces;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Commands
{
    public class AgCancelInvoiceCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long InvoiceTimaID { get; set; }
        public string Note { get; set; }
        public long UserIDCreate { get; set; }
    }
    public class AgCancelInvoiceCommandHandler : IRequestHandler<AgCancelInvoiceCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> _logInvoiceTab;
        ILogger<AgCancelInvoiceCommandHandler> _logger;
        RestClients.ICustomerService _customerService;
        LMS.Common.Helper.Utils _common;
        public AgCancelInvoiceCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> logInvoiceTab,
            RestClients.ICustomerService customerService,
            ILogger<AgCancelInvoiceCommandHandler> logger)
        {
            _invoiceTab = invoiceTab;
            _userTab = userTab;
            _logInvoiceTab = logInvoiceTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _customerService = customerService;
        }
        public async Task<ResponseActionResult> Handle(AgCancelInvoiceCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objInvoice = (await _invoiceTab.WhereClause(x => x.PartnerID == request.InvoiceTimaID).QueryAsync()).FirstOrDefault();
                if (objInvoice == null)
                {
                    response.Message = MessageConstant.InvoiceNotExits;
                    _logger.LogError($"AgCancelInvoiceCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                var objUser = (await _userTab.WhereClause(x => x.UserID == request.UserIDCreate).QueryAsync()).FirstOrDefault();
                if (objUser == null)
                {
                    response.Message = MessageConstant.NotIdentifiedBankcardID;
                    _logger.LogError($"AgCancelInvoiceCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                int statusOld = (int)objInvoice.Status;
                objInvoice.JobStatus = (int)Invoice_JobStatus.Unprocessed;
                objInvoice.Status = (int)Invoice_Status.Deleted;
                objInvoice.ModifyDate = DateTime.Now;
                if (_invoiceTab.Update(objInvoice))
                {
                    var logInvoice = new Domain.Tables.TblLogInvoice
                    {
                        InvoiceType = objInvoice.InvoiceType,
                        Note = request.Note,
                        TransactionDate = DateTime.Now,
                        CreateDate = DateTime.Now,
                        CreateBy = request.UserIDCreate,
                        Status = (int)Invoice_Status.Deleted,
                        InvoiceID = objInvoice.InvoiceID,
                        Request = _common.ConvertObjectToJSonV2(request),
                    };
                    logInvoice.LogInvoiceID = _logInvoiceTab.Insert(logInvoice);
                    response.SetSucces();
                    response.Data = objInvoice.InvoiceID;
                    if (statusOld == (int)Invoice_Status.Confirmed)
                    {
                        // hiện tại mới chỉ hủy phiếu của KH
                        List<int> lstInvoiceSubTypeCustomer = new List<int>
                        {
                            (int)GroupInvoiceType.ReceiptCustomer,
                            (int)GroupInvoiceType.ReceiptOnBehalfShop,
                            (int)GroupInvoiceType.ReceiptCustomerConsider
                        };
                        // chek source = customer => đúng dữ liệu
                        if (lstInvoiceSubTypeCustomer.Contains(objInvoice.InvoiceSubType) && objInvoice.SourceID > 0 && objInvoice.SourceID == objInvoice.CustomerID)
                        {
                            _ = _customerService.UpdateMoneyCustomer(objInvoice.SourceID.Value, objInvoice.TotalMoney.Value * -1, request.Note, objInvoice.ModifyDate);
                            //_ = _customerProducer.ProduceAsync(Kafka.Constants.KafkaTopics.CustomerBalanceChangeInfo, null, new Kafka.Messages.Customer.CustomerBalanceChangeInfo
                            //{
                            //    CustomerID = objInvoice.SourceID.Value,
                            //    TotalMoney = objInvoice.TotalMoney.Value * -1,
                            //    Note = request.Note,
                            //    TransactionDateTopUp = objInvoice.ModifyDate?.ToString(TimaSettingConstant.DateTimeDayMonthYearFull)
                            //});
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"AgCancelInvoiceCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
