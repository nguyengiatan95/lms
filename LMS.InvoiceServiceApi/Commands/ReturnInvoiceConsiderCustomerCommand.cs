﻿using LMS.Common.Constants;
using LMS.InvoiceServiceApi.Domain.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Commands
{
    public class ReturnInvoiceConsiderCustomerCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        //[Required]
        public long InvoiceID { get; set; }
        //[Required]
        public string Note { get; set; }
        //[Required]
        public long UserIDCreate { get; set; }
        public long ShopID { get; set; }
        public long InvoiceTimaID { get; set; }
    }
    public class ReturnInvoiceConsiderCustomerCommandHandler : IRequestHandler<ReturnInvoiceConsiderCustomerCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> _logInvoiceTab;
        ILogger<ReturnInvoiceConsiderCustomerCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public ReturnInvoiceConsiderCustomerCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> logInvoiceTab,
            ILogger<ReturnInvoiceConsiderCustomerCommandHandler> logger)
        {
            _invoiceTab = invoiceTab;
            _userTab = userTab;
            _logInvoiceTab = logInvoiceTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(ReturnInvoiceConsiderCustomerCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                if (request.InvoiceID > 0)
                {
                    _invoiceTab.WhereClause(x => x.InvoiceID == request.InvoiceID);
                }
                else if (request.InvoiceTimaID > 0)
                {
                    _invoiceTab.WhereClause(x => x.PartnerID == request.InvoiceTimaID);
                }
                else
                {
                    response.Message = MessageConstant.InvoiceNotExits;
                    return response;
                }
                var objInvoice = (await _invoiceTab.QueryAsync()).FirstOrDefault();
                if (objInvoice == null)
                {
                    response.Message = MessageConstant.InvoiceNotExits;
                    _logger.LogError($"ReturnInvoiceConsiderCustomerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                if (objInvoice.InvoiceType != (int)InvoiceType.Waiting || objInvoice.Status == (int)Invoice_Status.Confirmed)
                {
                    response.Message = MessageConstant.NotReturnInvoice;
                    _logger.LogError($"ReturnInvoiceConsiderCustomerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                var objUser = await _userTab.GetByIDAsync(request.UserIDCreate);
                if (objUser == null)
                {
                    response.Message = MessageConstant.NotIdentifiedBankcardID;
                    _logger.LogError($"ReturnInvoiceConsiderCustomerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                if (request.InvoiceTimaID > 0 && string.IsNullOrEmpty(request.Note))
                {
                    request.Note = $"{objUser.FullName} trả về xác nhận lại";
                }
                var jsonExtraOld = _common.ConvertJSonToObjectV2<InvoiceJsonExtra>(objInvoice.JsonExtra);
                jsonExtraOld.SourceName = string.Empty;
                objInvoice.CustomerID = 0;
                objInvoice.LoanID = 0;
                objInvoice.SourceID = 0;
                objInvoice.PathImg = string.Empty;
                objInvoice.JobStatus = (int)Invoice_JobStatus.Unprocessed;
                objInvoice.Status = (int)Invoice_Status.Pending;
                objInvoice.JsonExtra = _common.ConvertObjectToJSonV2(jsonExtraOld);
                objInvoice.ModifyDate = DateTime.Now;
                if ((await _invoiceTab.UpdateAsync(objInvoice)))
                {
                    var logInvoice = new Domain.Tables.TblLogInvoice
                    {
                        InvoiceType = (int)GroupInvoiceType.Waiting,
                        Note = request.Note,
                        TransactionDate = DateTime.Now,
                        CreateDate = DateTime.Now,
                        FromShopID = request.ShopID,
                        CreateBy = request.UserIDCreate,
                        Status = (int)Invoice_Status.Pending,
                        InvoiceID = objInvoice.InvoiceID,
                        Request = _common.ConvertObjectToJSonV2(request),
                    };
                    _ = _logInvoiceTab.InsertAsync(logInvoice);
                    response.SetSucces();
                    response.Data = objInvoice.InvoiceID;
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ReturnInvoiceConsiderCustomerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
