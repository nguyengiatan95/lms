﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Commands
{
    public class CreateTicketCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public string Note { get; set; }
        [Required]
        public long FromUserID { get; set; }
        [Required]
        public long FromDepartmentID { get; set; }
        [Required]
        public long ToDepartmentID { get; set; }
        public long ToUserID { get; set; }

        [Required]
        public int Type { get; set; }
    }

    public class CreateTicketCommandHandler : IRequestHandler<CreateTicketCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTicket> _ticketTab;
        ILogger<CreateTicketCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateTicketCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblTicket> ticketTab, ILogger<CreateTicketCommandHandler> logger)
        {
            _ticketTab = ticketTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateTicketCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var ticket = new Domain.Tables.TblTicket()
                {
                    CreateDate = DateTime.Now,
                    ModifyDate = DateTime.Now,
                    Type = request.Type,
                    FromDepartmentID = request.FromDepartmentID,
                    FromUserID = request.FromUserID,
                    Note = request.Note,
                    ModifyUserID = request.FromUserID,
                    ToDepartmentID = request.ToDepartmentID,
                    Status = (int)Ticket_Status.Waiting,
                    ToUserID = request.ToUserID,
                    JsonExtra = ""
                };
                var insertID = await _ticketTab.InsertAsync(ticket);
                if (insertID > 0)
                {
                    response.SetSucces();
                    response.Data = insertID;
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateTicketCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
