﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Commands
{
    public class CreateInvoiceConsiderCustomerCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public long TotalMoney { get; set; }
        [Required]
        public string Note { get; set; }
        [Required]
        public string StrTransactionDate { get; set; }
        [Required]
        public long UserIDCreate { get; set; }
        [Required]
        public long BankCardID { get; set; }

        public long InvoiceTimaID { get; set; }
        public long ShopID { get; set; }
    }
    public class CreateInvoiceConsiderCustomerCommandHandler : IRequestHandler<CreateInvoiceConsiderCustomerCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> _logInvoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        RestClients.ICustomerService _customerService;
        ILogger<CreateInvoiceConsiderCustomerCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateInvoiceConsiderCustomerCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> logInvoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            RestClients.ICustomerService customerService,
            ILogger<CreateInvoiceConsiderCustomerCommandHandler> logger)
        {
            _invoiceTab = invoiceTab;
            _loanTab = loanTab;
            _customerTab = customerTab;
            _bankCardTab = bankCardTab;
            _userTab = userTab;
            _logInvoiceTab = logInvoiceTab;
            _customerService = customerService;
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateInvoiceConsiderCustomerCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var transactionDate = DateTime.ParseExact(request.StrTransactionDate, _common.DateTimeDDMMYYYYHHMMSS, CultureInfo.InvariantCulture);
                if (transactionDate == DateTime.MinValue)
                {
                    transactionDate = DateTime.Now;
                }
                if ((DateTime.Now.Date.Subtract(transactionDate.Date)).Days > Constants.DateTransactionMinApply)
                {
                    response.Message = MessageConstant.StrCheckCreateDate;
                    _logger.LogError($"CreateInvoiceConsiderCustomerCommandHandler_Warning|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                }

                var objBankCard = (await _bankCardTab.WhereClause(x => x.BankCardID == request.BankCardID).QueryAsync()).FirstOrDefault();
                if (objBankCard == null)
                {
                    response.Message = MessageConstant.NotIdentifiedBankcardID;
                    _logger.LogError($"CreateInvoiceConsiderCustomerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                var objUser = (await _userTab.WhereClause(x => x.UserID == request.UserIDCreate).QueryAsync()).FirstOrDefault();
                if (objUser == null)
                {
                    response.Message = MessageConstant.NotIdentifiedUserID;
                    _logger.LogError($"CreateInvoiceConsiderCustomerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                request.ShopID = request.ShopID == 0 ? (long)ShopId.Tima : request.ShopID;
                var objShop = (await _lenderTab.WhereClause(x => x.LenderID == request.ShopID).QueryAsync()).FirstOrDefault();
                var objInvoice = _common.ConvertParentToChild<Domain.Tables.TblInvoice, CreateInvoiceConsiderCustomerCommand>(request);
                objInvoice.InvoiceType = (int)InvoiceType.Waiting;
                objInvoice.InvoiceSubType = (int)GroupInvoiceType.ReceiptCustomerConsider;
                objInvoice.CreateDate = DateTime.Now;
                objInvoice.TransactionDate = transactionDate;
                objInvoice.TransactionMoney = request.TotalMoney;
                objInvoice.DestinationID = request.BankCardID;
                objInvoice.Status = (int)Invoice_Status.Pending;
                objInvoice.JobStatus = (int)Invoice_JobStatus.Unprocessed;
                objInvoice.PartnerID = request.InvoiceTimaID;
                var invoiceExtraInfo = new Domain.Tables.InvoiceJsonExtra
                {
                    SourceName = "",
                    DestinationName = objBankCard?.AliasName,
                    ShopName = objShop?.FullName,
                    CreateBy = objUser.FullName,
                };
                objInvoice.JsonExtra = _common.ConvertObjectToJSonV2(invoiceExtraInfo);
                objInvoice.InvoiceCode = _common.HashMD5($"{request.Note}-{objInvoice.InvoiceSubType}-{request.BankCardID}-{request.TotalMoney}-{transactionDate.Ticks}");
                var invoiceCode = (await _invoiceTab.WhereClause(x => x.InvoiceCode == objInvoice.InvoiceCode).QueryAsync()).FirstOrDefault();// check phiếu đã dc tạo
                if (invoiceCode != null)
                {
                    response.Message = MessageConstant.InvoiceExits;
                    return response;
                }
                var insertID = _invoiceTab.Insert(objInvoice);
                if (insertID > 0)
                {
                    response.SetSucces();
                    response.Data = insertID;
                    var logInvoice = new Domain.Tables.TblLogInvoice
                    {
                        InvoiceID = insertID,
                        InvoiceType = (int)InvoiceType.Waiting,
                        Note = request.Note,
                        TransactionDate = transactionDate,
                        FromBankCardID = request.BankCardID,
                        CreateDate = DateTime.Now,
                        FromShopID = request.ShopID,
                        Amount = request.TotalMoney,
                        CreateBy = request.UserIDCreate,
                        Status = (int)Invoice_Status.Pending,
                        Request = _common.ConvertObjectToJSonV2(request),
                    };
                    logInvoice.LogInvoiceID = _logInvoiceTab.Insert(logInvoice);
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateInvoiceConsiderCustomerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
