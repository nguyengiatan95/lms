﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Globalization;

namespace LMS.InvoiceServiceApi.Commands
{
    public class CreateInvoiceLenderCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public int InvoiceSubType { get; set; }
        [Required]
        public long TotalMoney { get; set; }
        [Required]
        public long LenderID { get; set; }
        [Required]
        public string StrTransactionDate { get; set; }
        [Required]
        public long BankCardID { get; set; }
        [Required]
        public string Note { get; set; }
        [Required]
        public long UserIDCreate { get; set; }
        public long InvoiceTimaID { get; set; }
        public long ShopID { get; set; }

    }
    public class CreateInvoiceLenderCommandHandler : IRequestHandler<CreateInvoiceLenderCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> _logInvoiceTab;
        RestClients.ILenderService _lenderService;
        ILogger<CreateInvoiceLenderCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateInvoiceLenderCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> logInvoiceTab,
            RestClients.ILenderService lenderService,
            ILogger<CreateInvoiceLenderCommandHandler> logger)
        {
            _invoiceTab = invoiceTab;
            _loanTab = loanTab;
            _lenderTab = lenderTab;
            _bankCardTab = bankCardTab;
            _userTab = userTab;
            _logInvoiceTab = logInvoiceTab;
            _lenderService = lenderService;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateInvoiceLenderCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var transactionDate = DateTime.ParseExact(request.StrTransactionDate, _common.DateTimeDDMMYYYYHHMMSS, CultureInfo.InvariantCulture);
                if (transactionDate == DateTime.MinValue)
                {
                    transactionDate = DateTime.Now;
                }
                var logInvoice = new Domain.Tables.TblLogInvoice
                {
                    InvoiceType = request.InvoiceSubType,
                    LenderID = request.LenderID,
                    Note = request.Note,
                    TransactionDate = transactionDate,
                    CreateDate = DateTime.Now,
                    Amount = request.TotalMoney,
                    FromBankCardID = request.BankCardID,
                    CreateBy = request.UserIDCreate,
                    FromShopID = request.ShopID,
                    Request = _common.ConvertObjectToJSonV2(request),
                };
                logInvoice.LogInvoiceID = await _logInvoiceTab.InsertAsync(logInvoice);
                if ((DateTime.Now.Date.Subtract(transactionDate.Date)).Days > Constants.DateTransactionMinApply)
                {
                    response.Message = MessageConstant.StrCheckCreateDate;
                    _logger.LogError($"CreateInvoiceLenderCommandHandler_Warning|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                }
                Domain.Tables.TblLender objLender = null;
                long lenderID = request.LenderID;
                do
                {
                    var lenderInfos = await _lenderTab.WhereClause(x => x.LenderID == lenderID).QueryAsync();
                    if (lenderInfos == null || lenderInfos.Count() < 1)
                    {
                        // đồng bộ lender
                        var actionResultSyncLender = await _lenderService.CreateOrUpdateLenderFromAG(request.LenderID);
                        if (actionResultSyncLender == null || actionResultSyncLender.Result != (int)ResponseAction.Success)
                        {
                            response.Message = LMS.Common.Constants.MessageConstant.LoanNotFoundLender;
                            _logger.LogError($"CreateInvoiceLenderCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                            return response;
                        }
                        else
                        {
                            lenderID = Convert.ToInt64(actionResultSyncLender.Data);
                        }
                    }
                    else
                    {
                        objLender = lenderInfos.First();
                    }

                } while (objLender == null);
                var objBankCard = await _bankCardTab.GetByIDAsync(request.BankCardID);
                if (objBankCard == null)
                {
                    response.Message = MessageConstant.NotIdentifiedBankcardID;
                    _logger.LogError($"CreateInvoiceLenderCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                var objUser = await _userTab.GetByIDAsync(request.UserIDCreate);
                if (objUser == null)
                {
                    response.Message = MessageConstant.NotIdentifiedUserID;
                    _logger.LogError($"CreateInvoiceLenderCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }

                var lstReceipt = new List<int>(){
                          (int)GroupInvoiceType.ReceiptLenderCapital,
                          (int)GroupInvoiceType.ReceiptLenderInsurance,
                          (int)GroupInvoiceType.ReceiptLenderTopUp
                    };
                var lstPaySlip = new List<int>(){
                          (int)GroupInvoiceType.PaySlipLenderWithdraw,
                          (int)GroupInvoiceType.PaySlipAdvancePaymentForLender,
                          (int)GroupInvoiceType.LenderDepositPayment
                    };
                int invoiceType = 0;
                var sourceName = string.Empty;
                var destinationName = string.Empty;
                int sourceID = 0;
                int destinationID = 0;
                if (lstReceipt.Any(x => x == request.InvoiceSubType))
                {
                    invoiceType = (int)InvoiceType.Receipt;
                    sourceName = objLender.FullName;
                    destinationName = objBankCard.AliasName;
                    sourceID = (int)objLender.LenderID;
                    destinationID = (int)request.BankCardID;
                }
                else//phieu chi
                {
                    invoiceType = (int)InvoiceType.PaySlip;
                    request.TotalMoney *= -1;
                    sourceName = objBankCard.AliasName;
                    destinationName = objLender.FullName;
                    sourceID = (int)request.BankCardID;
                    destinationID = (int)objLender.LenderID;
                }
                request.ShopID = request.ShopID == 0 ? (long)ShopId.Tima : request.ShopID;
                var objShop = (await _lenderTab.WhereClause(x => x.LenderID == request.ShopID).QueryAsync()).FirstOrDefault();
                var objInvoice = _common.ConvertParentToChild<Domain.Tables.TblInvoice, CreateInvoiceLenderCommand>(request);
                objInvoice.InvoiceType = invoiceType;
                objInvoice.InvoiceSubType = request.InvoiceSubType;
                objInvoice.CreateDate = DateTime.Now;
                objInvoice.TransactionDate = transactionDate;
                objInvoice.TransactionMoney = request.TotalMoney;
                objInvoice.SourceID = sourceID;
                objInvoice.DestinationID = destinationID;
                objInvoice.Status = (int)Invoice_Status.Confirmed;
                objInvoice.JobStatus = (int)Invoice_JobStatus.Unprocessed;
                objInvoice.PartnerID = request.InvoiceTimaID;
                objInvoice.InvoiceCode = _common.HashMD5($"{request.LenderID}-{request.Note}-{objInvoice.InvoiceSubType}-{request.BankCardID}-{request.TotalMoney}-{transactionDate.Ticks}");
                var invoiceExtraInfo = new Domain.Tables.InvoiceJsonExtra
                {
                    SourceName = sourceName,
                    DestinationName = destinationName,
                    ShopName = objShop?.FullName,
                    CreateBy = objUser.FullName,
                };
                objInvoice.JsonExtra = _common.ConvertObjectToJSonV2(invoiceExtraInfo);
                var invoiceCode = (await _invoiceTab.WhereClause(x => x.InvoiceCode == objInvoice.InvoiceCode).QueryAsync()).FirstOrDefault();// check phiếu đã dc tạo
                if (invoiceCode != null)
                {
                    response.Message = MessageConstant.InvoiceExits;
                    return response;
                }
                var insertID = await _invoiceTab.InsertAsync(objInvoice);
                if (insertID > 0)
                {
                    long totalMoneyLenderAdd = request.TotalMoney;
                    if (request.InvoiceSubType == (int)GroupInvoiceType.LenderDepositPayment)
                    {
                        // do phiếu chi, đã âm, nhưng tiền về lender là dương
                        totalMoneyLenderAdd = request.TotalMoney * -1;
                    }
                    if (request.InvoiceSubType != (int)GroupInvoiceType.PaySlipAdvancePaymentForLender) // khác phiếu tạm ứng
                    {
                        _ = _lenderService.UpdateMoneyLender(objLender.LenderID, totalMoneyLenderAdd, request.Note);
                    }
                    response.SetSucces();
                    response.Data = insertID;
                    logInvoice.InvoiceID = insertID;
                    _ = _logInvoiceTab.UpdateAsync(logInvoice);
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateInvoiceLenderCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
