﻿using LMS.Common.Constants;
using LMS.Kafka.Interfaces;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Commands
{
    public class CreateInvoiceOnBehalfShopCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public int InvoiceSubType { get; set; }
        [Required]
        public long ShopID { get; set; }
        [Required]
        public long ToShopID { get; set; }
        [Required]
        public long TotalMoney { get; set; }
        [Required]
        public long CustomerID { get; set; }
        [Required]
        public string StrTransactionDate { get; set; }
        [Required]
        public long BankCardID { get; set; }
        [Required]
        public string Note { get; set; }
        [Required]
        public long UserIDCreate { get; set; }

        public long LoanID { get; set; }
        public long InvoiceTimaID { get; set; }
        public string CustomerNumberCard { get; set; }
        public string CustomerPhone { get; set; }

    }
    public class CreateInvoiceOnBehalfShopCommandHandler : IRequestHandler<CreateInvoiceOnBehalfShopCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> _logInvoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        RestClients.ICustomerService _customerService;
        ILogger<CreateInvoiceOnBehalfShopCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateInvoiceOnBehalfShopCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> logInvoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            RestClients.ICustomerService customerService,
            ILogger<CreateInvoiceOnBehalfShopCommandHandler> logger)
        {
            _invoiceTab = invoiceTab;
            _loanTab = loanTab;
            _customerTab = customerTab;
            _bankCardTab = bankCardTab;
            _userTab = userTab;
            _logInvoiceTab = logInvoiceTab;
            _customerService = customerService;
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateInvoiceOnBehalfShopCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var transactionDate = DateTime.ParseExact(request.StrTransactionDate, _common.DateTimeDDMMYYYYHHMMSS, CultureInfo.InvariantCulture);
                if (transactionDate == DateTime.MinValue)
                {
                    transactionDate = DateTime.Now;
                }
                long customerID = request.CustomerID;
                Domain.Tables.TblCustomer objCustomer = null;
                if (request.InvoiceTimaID > 0)
                {
                    if (request.LoanID > 0)
                    {
                        customerID = (await _loanTab.WhereClause(x => x.TimaLoanID == request.LoanID).QueryAsync()).FirstOrDefault().CustomerID;
                        objCustomer = (await _customerTab.WhereClause(x => x.CustomerID == customerID).QueryAsync()).FirstOrDefault();
                    }
                    else if (request.CustomerID > 0)
                    {
                        objCustomer = (await _customerTab.WhereClause(x => x.TimaCustomerID == customerID).QueryAsync()).FirstOrDefault();
                    }
                    else if (!string.IsNullOrEmpty(request.CustomerNumberCard))
                    {
                        var lstCustomerInfos = await _customerTab.WhereClause(x => x.NumberCard == request.CustomerNumberCard).QueryAsync();
                        var customerFirst = lstCustomerInfos.Where(x => x.CustomerID == request.CustomerID).FirstOrDefault();
                        // nếu customer fist = null && lstcustomer có nhiều hơn 1
                        // trả về thằng đầu tiên do dữ liệu bị sai khi đồng bộ
                        if ((customerFirst == null || customerFirst.CustomerID < 1) && lstCustomerInfos.Count() > 1)
                        {
                            objCustomer = lstCustomerInfos.OrderByDescending(x => x.CustomerID).FirstOrDefault();
                        }
                        else
                        {
                            objCustomer = customerFirst;
                        }
                    }
                }
                else
                {
                    objCustomer = (await _customerTab.WhereClause(x => x.CustomerID == customerID).QueryAsync()).FirstOrDefault();
                }
                if (objCustomer == null)
                {
                    response.Message = MessageConstant.NotIdentifiedCustomerID;
                    _logger.LogError($"CreateInvoiceOnBehalfShopCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                var objBankCard = await _bankCardTab.GetByIDAsync(request.BankCardID);
                if (objBankCard == null)
                {
                    response.Message = MessageConstant.NotIdentifiedBankcardID;
                    _logger.LogError($"CreateInvoiceOnBehalfShopCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                var objUser = await _userTab.GetByIDAsync(request.UserIDCreate);
                if (objUser == null)
                {
                    response.Message = MessageConstant.NotIdentifiedUserID;
                    _logger.LogError($"CreateInvoiceOnBehalfShopCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                var lstShop = await _lenderTab.WhereClause(x => x.LenderID == request.ShopID || x.LenderID == request.ToShopID).QueryAsync();
                var objInvoice = _common.ConvertParentToChild<Domain.Tables.TblInvoice, CreateInvoiceOnBehalfShopCommand>(request);
                objInvoice.InvoiceType = (int)InvoiceType.Receipt;
                objInvoice.InvoiceSubType = (int)request.InvoiceSubType;
                objInvoice.CreateDate = DateTime.Now;
                objInvoice.TransactionDate = transactionDate;
                objInvoice.TransactionMoney = request.TotalMoney;
                objInvoice.SourceID = objCustomer.CustomerID;
                objInvoice.DestinationID = objBankCard.BankCardID;
                objInvoice.Status = (int)Invoice_Status.Confirmed;
                objInvoice.JobStatus = (int)Invoice_JobStatus.Unprocessed;
                objInvoice.PartnerID = request.InvoiceTimaID;
                objInvoice.CustomerID = objCustomer.CustomerID;
                objInvoice.InvoiceCode = _common.HashMD5($"{request.Note}-{objInvoice.InvoiceSubType}-{request.BankCardID}-{request.TotalMoney}-{transactionDate.Ticks}");
                var objShop = lstShop.FirstOrDefault(x => x.LenderID == request.ShopID);
                var invoiceExtraInfo = new Domain.Tables.InvoiceJsonExtra
                {
                    SourceName = objCustomer.FullName,
                    DestinationName = objBankCard.AliasName,
                    ShopName = objShop?.FullName,
                    CreateBy = objUser.FullName,
                };
                objInvoice.JsonExtra = _common.ConvertObjectToJSonV2(invoiceExtraInfo);
                var checkInvoiceCode = (await _invoiceTab.WhereClause(x => x.InvoiceCode == objInvoice.InvoiceCode).QueryAsync()).FirstOrDefault();// check phiếu đã dc tạo
                if (checkInvoiceCode != null)
                {
                    response.Message = MessageConstant.InvoiceExits;
                    return response;
                }
                var insertID = await _invoiceTab.InsertAsync(objInvoice);
                if (insertID > 0)
                {
                    //_ = _customerService.UpdateMoneyCustomer(objCustomer.CustomerID, request.TotalMoney, request.Note, objInvoice.TransactionDate);
                    //_ = _customerProducer.ProduceAsync(Kafka.Constants.KafkaTopics.CustomerBalanceTopupInfo, null, new Kafka.Messages.Customer.CustomerBalanceTopupInfo
                    //{
                    //    CustomerID = objCustomer.CustomerID,
                    //    TotalMoney = objInvoice.TotalMoney.Value * -1,
                    //    Note = request.Note,
                    //    TransactionDateTopUp = objInvoice.TransactionDate?.ToString(TimaSettingConstant.DateTimeDayMonthYearFull)
                    //});
                    var t1 = _customerService.UpdateMoneyCustomer(objCustomer.CustomerID, request.TotalMoney, request.Note, objInvoice.TransactionDate);
                    response.SetSucces();
                    response.Data = insertID;

                    var logInvoice = new Domain.Tables.TblLogInvoice
                    {
                        InvoiceType = (int)request.InvoiceSubType,
                        CustomerID = request.CustomerID,
                        LoanID = request.LoanID,
                        Note = request.Note,
                        TransactionDate = transactionDate,
                        CreateDate = DateTime.Now,
                        Amount = request.TotalMoney,
                        FromBankCardID = request.BankCardID,
                        FromShopID = request.ShopID,
                        ToShopID = request.ToShopID,
                        CreateBy = request.UserIDCreate,
                        InvoiceID = insertID,
                        Request = _common.ConvertObjectToJSonV2(request),
                    };
                    var t2 = _logInvoiceTab.InsertAsync(logInvoice);
                    await Task.WhenAll(t1, t2);
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateInvoiceOnBehalfShopCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
