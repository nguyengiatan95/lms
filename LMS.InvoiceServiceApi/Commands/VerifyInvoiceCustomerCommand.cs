﻿using LMS.Common.Constants;
using LMS.Kafka.Interfaces;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Commands
{
    public class VerifyInvoiceCustomerCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public long InvoiceID { get; set; }
        //[Required]
        public long TotalMoney { get; set; }
        //[Required]
        public long CustomerID { get; set; }
        //[Required]
        public string StrTransactionDate { get; set; }
        // [Required]
        public long BankCardID { get; set; }
        //[Required]
        public string Note { get; set; }
        [Required]
        public long UserIDCreate { get; set; }

        public long LoanID { get; set; }

        public long InvoiceTimaID { get; set; }
        public long ShopID { get; set; }
        public string CustomerNumberCard { get; set; }
        public string CustomerPhone { get; set; }

    }
    public class VerifyInvoiceCustomerCommandHandler : IRequestHandler<VerifyInvoiceCustomerCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> _logInvoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        RestClients.ICustomerService _customerService;
        ILogger<VerifyInvoiceCustomerCommandHandler> _logger;
        RestClients.ILenderService _lenderService;
        LMS.Common.Helper.Utils _common;
        public VerifyInvoiceCustomerCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> logInvoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            RestClients.ICustomerService customerService,
            RestClients.ILenderService lenderService,
            ILogger<VerifyInvoiceCustomerCommandHandler> logger)
        {
            _invoiceTab = invoiceTab;
            _loanTab = loanTab;
            _customerTab = customerTab;
            _bankCardTab = bankCardTab;
            _userTab = userTab;
            _logInvoiceTab = logInvoiceTab;
            _customerService = customerService;
            _lenderTab = lenderTab;
            _logger = logger;
            _lenderService = lenderService;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(VerifyInvoiceCustomerCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                if (request.InvoiceID > 0)
                {
                    _invoiceTab.WhereClause(x => x.InvoiceID == request.InvoiceID);
                }
                else if (request.InvoiceTimaID > 0)
                {
                    _invoiceTab.WhereClause(x => x.PartnerID == request.InvoiceTimaID);
                }
                else
                {
                    response.Message = MessageConstant.InvoiceNotExits;
                    return response;
                }
                var objInvoice = (await _invoiceTab.QueryAsync()).FirstOrDefault();
                if (objInvoice == null)
                {
                    response.Message = MessageConstant.InvoiceNotExits;
                    _logger.LogError($"VerifyInvoiceCustomerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                if (request.InvoiceTimaID > 0)
                {
                    long customerID = request.CustomerID;
                    Domain.Tables.TblCustomer objCustomer;
                    if (request.InvoiceTimaID > 0)
                    {
                        if (request.LoanID > 0)
                        {
                            customerID = (await _loanTab.WhereClause(x => x.TimaLoanID == request.LoanID).QueryAsync()).FirstOrDefault().CustomerID;
                            objCustomer = (await _customerTab.WhereClause(x => x.CustomerID == customerID).QueryAsync()).FirstOrDefault();
                        }
                        else if (!string.IsNullOrEmpty(request.CustomerNumberCard))
                        {
                            var lstCustomerInfos = await _customerTab.WhereClause(x => x.NumberCard == request.CustomerNumberCard).QueryAsync();
                            var customerFirst = lstCustomerInfos.Where(x => x.CustomerID == request.CustomerID).FirstOrDefault();
                            // nếu customer fist = null && lstcustomer có nhiều hơn 1
                            // trả về thằng đầu tiên do dữ liệu bị sai khi đồng bộ
                            if ((customerFirst == null || customerFirst.CustomerID < 1) && lstCustomerInfos.Count() > 1)
                            {
                                objCustomer = lstCustomerInfos.OrderByDescending(x => x.CustomerID).FirstOrDefault();
                            }
                            else
                            {
                                objCustomer = customerFirst;
                            }
                        }
                        else
                        {
                            objCustomer = (await _customerTab.WhereClause(x => x.CustomerID == customerID).QueryAsync()).FirstOrDefault();
                        }
                    }
                    else
                    {
                        objCustomer = (await _customerTab.WhereClause(x => x.CustomerID == customerID).QueryAsync()).FirstOrDefault();
                    }
                    if (objCustomer == null)
                    {
                        response.Message = MessageConstant.NotIdentifiedCustomerID;
                        _logger.LogError($"VerifyInvoiceCustomerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                        return response;
                    }
                    objInvoice.CustomerID = objCustomer.CustomerID;
                    objInvoice.SourceID = objCustomer.CustomerID;
                    objInvoice.InvoiceSubType = (int)GroupInvoiceType.ReceiptCustomerConsider;
                    var objUser = await _userTab.GetByIDAsync(request.UserIDCreate);
                    if (objUser == null)
                    {
                        response.Message = MessageConstant.NotIdentifiedUserID;
                        _logger.LogError($"VerifyInvoiceCustomerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                        return response;
                    }
                    var objBankCard = await _bankCardTab.GetByIDAsync((long)objInvoice.BankCardID);
                    var objShop = (await _lenderTab.WhereClause(x => x.LenderID == request.ShopID).QueryAsync()).FirstOrDefault();
                    var invoiceExtraInfo = new Domain.Tables.InvoiceJsonExtra
                    {
                        SourceName = objCustomer.FullName,
                        DestinationName = objBankCard?.AliasName,
                        ShopName = objShop?.FullName,
                        CreateBy = objUser.FullName,
                    };
                    objInvoice.JsonExtra = _common.ConvertObjectToJSonV2(invoiceExtraInfo);
                }
                string noteCustomer = objInvoice.Note + request.Note;
                int oldStatus = (int)objInvoice.Status;
                objInvoice.Status = (int)Invoice_Status.Confirmed;
                var updateInvoice = await _invoiceTab.UpdateAsync(objInvoice);
                if (updateInvoice)
                {
                    // trường hợp trạng thái cũ ag bắn qua đã confirmed
                    // tiền kh chỉ cộng 1 lần
                    List<Task> lstTask = new List<Task>();
                    if (objInvoice.CustomerID != null && objInvoice.CustomerID != 0 && oldStatus != (int)Invoice_Status.Confirmed)
                    {
                        //_ = _customerService.UpdateMoneyCustomer((long)objInvoice.CustomerID, (long)objInvoice.TotalMoney, noteCustomer, objInvoice.TransactionDate);
                   
                        //_ = _customerService.UpdateMoneyCustomer((long)objInvoice.CustomerID, (long)objInvoice.TotalMoney, noteCustomer, objInvoice.TransactionDate);
                        //_ = _customerProducer.ProduceAsync(Kafka.Constants.KafkaTopics.CustomerBalanceTopupInfo, null, new Kafka.Messages.Customer.CustomerBalanceTopupInfo
                        //{
                        //    CustomerID = objInvoice.CustomerID.Value,
                        //    TotalMoney = request.TotalMoney,
                        //    Note = request.Note,
                        //    TransactionDateTopUp = objInvoice.TransactionDate?.ToString(TimaSettingConstant.DateTimeDayMonthYearFull)
                        //});
                        lstTask.Add(_customerService.UpdateMoneyCustomer((long)objInvoice.CustomerID, (long)objInvoice.TotalMoney, noteCustomer, objInvoice.TransactionDate));
                    }
                    if (objInvoice.LenderID != null && objInvoice.LenderID != 0 && oldStatus != (int)Invoice_Status.Confirmed)
                    {
                        lstTask.Add(_lenderService.UpdateMoneyLender((long)objInvoice.LenderID, (long)objInvoice.TotalMoney, noteCustomer));
                    }
                    var logInvoice = new Domain.Tables.TblLogInvoice
                    {
                        InvoiceID = objInvoice.InvoiceID,
                        InvoiceType = (int)InvoiceType.Waiting,
                        CustomerID = objInvoice.CustomerID,
                        LenderID = objInvoice.LenderID,
                        LoanID = objInvoice.LoanID,
                        FromShopID = request.ShopID,
                        Note = request.Note,
                        TransactionDate = DateTime.Now,
                        CreateDate = DateTime.Now,
                        Amount = objInvoice.TotalMoney,
                        FromBankCardID = objInvoice.BankCardID,
                        CreateBy = request.UserIDCreate,
                        Status = (int)Invoice_Status.Confirmed,
                        Request = _common.ConvertObjectToJSonV2(request),
                    };
                    lstTask.Add(_logInvoiceTab.InsertAsync(logInvoice));

                    response.SetSucces();
                    response.Data = objInvoice.InvoiceID;
                    await Task.WhenAll(lstTask);
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"VerifyInvoiceCustomerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
