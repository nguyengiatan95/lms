﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Commands
{
    public class UpdateTicketCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public string Note { get; set; }
        [Required]
        public long FromUserID { get; set; }
        [Required]
        public long FromDepartmentID { get; set; }
        [Required]
        public long ToDepartmentID { get; set; }
        public long ToUserID { get; set; }

        [Required]
        public int Type { get; set; }
        [Required]
        public long TicketID { get; set; }
        public int Status { get; set; }
    }

    public class UpdateTicketCommandHandler : IRequestHandler<UpdateTicketCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTicket> _ticketTab;
        ILogger<UpdateTicketCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdateTicketCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblTicket> ticketTab, ILogger<UpdateTicketCommandHandler> logger)
        {
            _ticketTab = ticketTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(UpdateTicketCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var ticket = _ticketTab.GetByID(request.TicketID);
                ticket.ModifyDate = DateTime.Now;
                ticket.Type = request.Type;
                ticket.FromDepartmentID = request.FromDepartmentID;
                //ticket.FromUserID = request.FromUserID;
                ticket.Note = request.Note;
                ticket.ModifyUserID = request.FromUserID;
                ticket.ToDepartmentID = request.ToDepartmentID;
                ticket.Status = (int)Ticket_Status.Waiting;
                ticket.ToUserID = request.ToUserID;
                ticket.JsonExtra = "";
                var update = await _ticketTab.UpdateAsync(ticket);
                if (update)
                {
                    response.SetSucces();
                    response.Data = ticket.TicketID;
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateTicketCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

    }
}
