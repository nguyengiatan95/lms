﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Commands
{
    public class CreateInvoiceInternalCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int InvoiceSubType { get; set; }
        [Required]
        public long TotalMoney { get; set; }
        [Required]
        public string StrTransactionDate { get; set; }
        [Required]
        public int Payee { get; set; }
        [Required]
        public long FromBankCardID { get; set; }
        [Required]
        public long ToBankCardID { get; set; }
        [Required]
        public string Note { get; set; }
        [Required]
        public long UserIDCreate { get; set; }
        public long MoneyFee { get; set; }
        public long BankCardID { get; set; }
        public long InvoiceTimaID { get; set; }
        public long ShopID { get; set; }
    }
    public class CreateInvoiceInternalCommandHandler : IRequestHandler<CreateInvoiceInternalCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> _logInvoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        RestClients.ICustomerService _customerService;
        ILogger<CreateInvoiceInternalCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateInvoiceInternalCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> logInvoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            RestClients.ICustomerService customerService,
            ILogger<CreateInvoiceInternalCommandHandler> logger)
        {
            _invoiceTab = invoiceTab;
            _loanTab = loanTab;
            _customerTab = customerTab;
            _bankCardTab = bankCardTab;
            _userTab = userTab;
            _logInvoiceTab = logInvoiceTab;
            _lenderTab = lenderTab;
            _customerService = customerService;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateInvoiceInternalCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var transactionDate = DateTime.ParseExact(request.StrTransactionDate, _common.DateTimeDDMMYYYYHHMMSS, CultureInfo.InvariantCulture);
                if (transactionDate == DateTime.MinValue)
                {
                    transactionDate = DateTime.Now;
                }
                var logInvoiceReq = CreateLogInvoice(request, request.InvoiceSubType, transactionDate);
                _logInvoiceTab.Insert(logInvoiceReq);
                if ((DateTime.Now.Date.Subtract(transactionDate.Date)).Days > Constants.DateTransactionMinApply)
                {
                    response.Message = MessageConstant.StrCheckCreateDate;
                    _logger.LogError($"CreateInvoiceInternalCommandHandler_Warning|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                }
                var lstBankCardID = new List<long> { request.FromBankCardID, request.ToBankCardID };
                var lstBankCard = (await _bankCardTab.WhereClause(x => lstBankCardID.Contains(x.BankCardID)).QueryAsync()).ToList();
                if (lstBankCard.Count != lstBankCard.Count)
                {
                    response.Message = MessageConstant.NotIdentifiedBankcardID;
                    _logger.LogError($"CreateInvoiceInternalCommandHandler_Warning|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                var objUser = await _userTab.GetByIDAsync(request.UserIDCreate);
                if (objUser == null)
                {
                    response.Message = MessageConstant.NotIdentifiedUserID;
                    _logger.LogError($"CreateInvoiceInternalCommandHandler_Warning|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                request.ShopID = request.ShopID == 0 ? (long)ShopId.Tima : request.ShopID;
                var objInvoice = _common.ConvertParentToChild<Domain.Tables.TblInvoice, CreateInvoiceInternalCommand>(request);
                var lstCreateInvoice = new List<Domain.Tables.TblInvoice>();
                var createInvoicePaySlip = new Domain.Tables.TblInvoice();
                var createInvoiceReceipt = new Domain.Tables.TblInvoice();
                var createInvoiceMonneyFree = new Domain.Tables.TblInvoice();
                var currentDate = DateTime.Now;
                //chi tiền nội bộ
                createInvoicePaySlip = CreateInvoice(request, (int)InvoiceType.PaySlip, (int)GroupInvoiceType.PaySlipTransferInternal, request.FromBankCardID, request.ToBankCardID, request.TotalMoney * -1, transactionDate, lstBankCard, objUser, 0);
                createInvoicePaySlip.CreateDate = currentDate;
                if (request.MoneyFee > 0 && request.Payee == (int)Invoice_Payee.Sender)
                {
                    createInvoicePaySlip.MoneyFee = request.Payee;
                }

                // check phiếu chi đã tạo chưa
                var checkInvoiceCode = await _invoiceTab.WhereClause(x => x.InvoiceCode == createInvoicePaySlip.InvoiceCode).QueryAsync();
                if (checkInvoiceCode != null && checkInvoiceCode.Count() > 0)
                {
                    response.Message = MessageConstant.InvoiceExits;
                    _logger.LogError($"CreateInvoiceInternalCommandHandler_Error|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }

                createInvoicePaySlip.InvoiceID = await _invoiceTab.InsertAsync(createInvoicePaySlip);
                if (createInvoicePaySlip.InvoiceID < 1)
                {
                    response.Message = MessageConstant.ErrorInternal;
                    _logger.LogError($"CreateInvoiceInternalCommandHandler_InsertPaySlip_Erorr|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                //thu tiền nội bộ
                createInvoiceReceipt = CreateInvoice(request, (int)InvoiceType.Receipt, (int)GroupInvoiceType.ReceiptInternal, request.FromBankCardID, request.ToBankCardID, request.TotalMoney, transactionDate, lstBankCard, objUser, createInvoicePaySlip.InvoiceID);
                createInvoiceReceipt.CreateDate = currentDate;
                if (request.MoneyFee > 0 && request.Payee == (int)Invoice_Payee.Receiver)
                {
                    createInvoiceReceipt.MoneyFee = request.Payee;
                }
                await _invoiceTab.InsertAsync(createInvoiceReceipt);
                if (request.MoneyFee > 0) // phats sinh phi giao dịch => tạo giao dịch  phí ngân hàng chuyển MoneyFee = 0
                {
                    if (request.Payee == (int)Invoice_Payee.Sender) // bên nào chịu phi
                    {
                        createInvoiceMonneyFree = CreateInvoice(request, (int)InvoiceType.PaySlip, (int)GroupInvoiceType.PaySlipFeeTransferMoney, request.FromBankCardID, request.FromBankCardID, request.MoneyFee * -1, transactionDate, lstBankCard, objUser, createInvoicePaySlip.InvoiceID);
                    }
                    else
                    {
                        createInvoiceMonneyFree = CreateInvoice(request, (int)InvoiceType.PaySlip, (int)GroupInvoiceType.PaySlipFeeTransferMoney, request.ToBankCardID, request.ToBankCardID, request.MoneyFee * -1, transactionDate, lstBankCard, objUser, createInvoicePaySlip.InvoiceID);
                    }
                    _ = _invoiceTab.InsertAsync(createInvoiceMonneyFree);
                }
                response.SetSucces();
                return response;
            }

            catch (Exception ex)
            {
                _logger.LogError($"CreateInvoiceInternalCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        private Domain.Tables.TblInvoice CreateInvoice(CreateInvoiceInternalCommand entity, int invoiceType, int invoiceSubType, long sourceBankCardID, long destinationBankCardID, long totalMoney, DateTime transactionDate, List<Domain.Tables.TblBankCard> lstBankCard, Domain.Tables.TblUser objUser, long invoiceReferID)
        {
            var objLender = _lenderTab.WhereClause(x => x.LenderID == entity.ShopID).Query().FirstOrDefault();
            var objInvoice = _common.ConvertParentToChild<Domain.Tables.TblInvoice, CreateInvoiceInternalCommand>(entity);
            objInvoice.InvoiceType = invoiceType;
            objInvoice.InvoiceSubType = invoiceSubType;
            objInvoice.CreateDate = DateTime.Now;
            objInvoice.TransactionDate = transactionDate;
            objInvoice.TransactionMoney = totalMoney;
            objInvoice.TotalMoney = totalMoney;
            // bắt đầu từ phiếu chi đi ra
            objInvoice.BankCardID = invoiceType == (int)InvoiceType.PaySlip ? sourceBankCardID : destinationBankCardID;
            objInvoice.SourceID = sourceBankCardID;
            objInvoice.DestinationID = destinationBankCardID;
            objInvoice.Status = (int)Invoice_Status.Confirmed;
            objInvoice.JobStatus = (int)Invoice_JobStatus.Unprocessed;
            objInvoice.PartnerID = entity.InvoiceTimaID;
            objInvoice.InvoiceCode = _common.HashMD5($"{entity.Note}-{objInvoice.InvoiceSubType}-{objInvoice.BankCardID}-{entity.TotalMoney}-{transactionDate.Ticks}");
            var fromBankCardInfo = lstBankCard.FirstOrDefault(x => x.BankCardID == sourceBankCardID);
            var toBankCardInfo = lstBankCard.FirstOrDefault(x => x.BankCardID == destinationBankCardID);
            var invoiceExtraInfo = new Domain.Tables.InvoiceJsonExtra
            {
                SourceName = fromBankCardInfo?.AliasName,
                DestinationName = toBankCardInfo?.AliasName ?? "",
                ShopName = objLender?.FullName,
                CreateBy = objUser.FullName,
                InvoiceReferID = invoiceReferID
            };
            objInvoice.JsonExtra = _common.ConvertObjectToJSonV2(invoiceExtraInfo);
            return objInvoice;
        }

        private Domain.Tables.TblLogInvoice CreateLogInvoice(CreateInvoiceInternalCommand entity, int InvoiceType, DateTime transactionDate)
        {
            var logInvoice = new Domain.Tables.TblLogInvoice
            {
                InvoiceType = entity.InvoiceSubType,
                Note = entity.Note,
                TransactionDate = transactionDate,
                CreateDate = DateTime.Now,
                Amount = entity.TotalMoney,
                FromBankCardID = entity.FromBankCardID,
                ToBankCardID = entity.ToBankCardID,
                CreateBy = entity.UserIDCreate,
                Request = _common.ConvertObjectToJSonV2(entity)
            };
            return logInvoice;
        }
    }
}
