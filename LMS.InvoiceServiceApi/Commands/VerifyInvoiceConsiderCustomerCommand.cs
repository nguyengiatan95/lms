﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Commands
{
    public class VerifyInvoiceConsiderCustomerCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public long InvoiceID { get; set; }
        //[Required]
        public long CustomerID { get; set; }
        [Required]
        public long UserIDCreate { get; set; }
        //[Required]
        public string Note { get; set; }
        //[Required]
        public long BankCardID { get; set; }
        public long LoanID { get; set; }
        public List<Domain.Models.ImageLosUpload> PathImg { get; set; }
        public long ShopID { get; set; }
        public long LenderID { get; set; }
        public int TypeVerifyInvoiceConsider { get; set; }

        public long InvoiceAGID { get; set; }

    }
    public class VerifyInvoiceConsiderCustomerCommandHandler : IRequestHandler<VerifyInvoiceConsiderCustomerCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> _logInvoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;


        RestClients.ICollectionService _collectionService;
        RestClients.ICustomerService _customerService;
        ILogger<VerifyInvoiceConsiderCustomerCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public VerifyInvoiceConsiderCustomerCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> logInvoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            RestClients.ICustomerService customerService,
            RestClients.ICollectionService collectionService,
            ILogger<VerifyInvoiceConsiderCustomerCommandHandler> logger)
        {
            _invoiceTab = invoiceTab;
            _customerTab = customerTab;
            _userTab = userTab;
            _logInvoiceTab = logInvoiceTab;
            _customerService = customerService;
            _lenderTab = lenderTab;
            _bankCardTab = bankCardTab;
            _collectionService = collectionService;
            _loanTab = loanTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(VerifyInvoiceConsiderCustomerCommand request, CancellationToken cancellationToken)
        {
            //long timaCustomerID = 0;
            long customerID = request.CustomerID; // ag bắn qua là timaCustomerID
            long loanID = request.LoanID;
            Domain.Tables.TblCustomer objCustomer = null;
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                Domain.Tables.TblInvoice objInvoice;
                if (request.InvoiceAGID > 0)
                {
                    objInvoice = (await _invoiceTab.WhereClause(x => x.PartnerID == request.InvoiceAGID).QueryAsync()).FirstOrDefault();
                    if (objInvoice == null || objInvoice.InvoiceID < 1)
                    {
                        response.Message = MessageConstant.InvoiceNotExits;
                        _logger.LogError($"VerifyInvoiceConsiderCustomerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                        return response;
                    }
                    var loanIDAGExis = (await _loanTab.WhereClause(x => x.TimaLoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                    if (loanIDAGExis != null)
                    {
                        loanID = loanIDAGExis.LoanID;
                        customerID = loanIDAGExis.CustomerID;
                        _customerTab.WhereClause(x => x.CustomerID == customerID);
                    }
                    else if (customerID > 0)
                    {
                        _customerTab.WhereClause(x => x.TimaCustomerID == customerID);
                    }

                }
                else
                {
                    objInvoice = (await _invoiceTab.WhereClause(x => x.InvoiceID == request.InvoiceID).QueryAsync()).FirstOrDefault();
                    if (objInvoice == null || objInvoice.InvoiceID < 1)
                    {
                        response.Message = MessageConstant.InvoiceNotExits;
                        _logger.LogError($"VerifyInvoiceConsiderCustomerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                        return response;
                    }
                }

                if (objInvoice.Status == (int)Invoice_Status.Confirmed || objInvoice.Status == (int)Invoice_Status.Deleted)
                {
                    response.Message = MessageConstant.NotChangeInvoice;
                    _logger.LogError($"VerifyInvoiceConsiderCustomerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                if (request.CustomerID == (int)StatusCommon.UnActive && request.LenderID == (int)StatusCommon.UnActive)
                {
                    response.Message = MessageConstant.NotChangeInvoice;
                    _logger.LogError($"VerifyInvoiceConsiderCustomerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                string sourceName = string.Empty;
                if (request.TypeVerifyInvoiceConsider == (int)TypeVerifyInvoiceConsider.Customer)
                {
                    //var objCustomer = (await _customerTab.WhereClause(x => x.CustomerID == request.CustomerID).QueryAsync()).FirstOrDefault();
                    if (request.InvoiceAGID < 1) // lms xác minh khách hàng
                    {
                        _customerTab.WhereClause(x => x.CustomerID == customerID);
                    }
                    objCustomer = (await _customerTab.QueryAsync()).FirstOrDefault();
                    if (objCustomer == null)
                    {
                        response.Message = MessageConstant.NotIdentifiedCustomerID;
                        _logger.LogError($"VerifyInvoiceConsiderCustomerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                        return response;
                    }
                    //timaCustomerID = request.CustomerID;
                    customerID = objCustomer.TimaCustomerID;
                    sourceName = objCustomer.FullName;
                    objInvoice.CustomerID = objCustomer.CustomerID;
                    objInvoice.SourceID = objCustomer.CustomerID;
                    objInvoice.InvoiceSubType = (int)GroupInvoiceType.ReceiptCustomerConsider;
                }
                else
                {
                    var objLender = (await _lenderTab.WhereClause(x => x.LenderID == request.LenderID).QueryAsync()).FirstOrDefault();
                    if (objLender == null)
                    {
                        response.Message = MessageConstant.NotIdentifiedLenderID;
                        _logger.LogError($"VerifyInvoiceConsiderCustomerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                        return response;
                    }
                    sourceName = objLender.FullName;
                    objInvoice.LenderID = request.LenderID;
                    objInvoice.SourceID = request.LenderID;
                    objInvoice.InvoiceSubType = (int)GroupInvoiceType.ReceiptLenderCapital;
                }

                var objUser = await _userTab.GetByIDAsync(request.UserIDCreate);
                if (objUser == null)
                {
                    response.Message = MessageConstant.NotIdentifiedUserID;
                    _logger.LogError($"VerifyInvoiceConsiderCustomerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                var objBankCard = await _bankCardTab.GetByIDAsync((long)objInvoice.BankCardID);

                var objShop = (await _lenderTab.WhereClause(x => x.LenderID == request.ShopID).QueryAsync()).FirstOrDefault();
                objInvoice.LoanID = request.LoanID != 0 ? request.LoanID : objInvoice.LoanID;
                if (objInvoice.Status == (int)Invoice_Status.Pending && request.PathImg != null)
                {
                    objInvoice.PathImg = _common.ConvertObjectToJSonV2(request.PathImg);
                }
                objInvoice.Status = (int)Invoice_Status.WaitingConfirm;
                objInvoice.ModifyDate = DateTime.Now;
                var invoiceExtraInfo = new Domain.Tables.InvoiceJsonExtra
                {
                    SourceName = sourceName,
                    DestinationName = objBankCard?.AliasName,
                    ShopName = objShop?.FullName,
                    CreateBy = objUser.FullName,
                };

                objInvoice.JsonExtra = _common.ConvertObjectToJSonV2(invoiceExtraInfo);
                var update = await _invoiceTab.UpdateAsync(objInvoice);
                if (update)
                {
                    var logInvoice = new Domain.Tables.TblLogInvoice
                    {
                        InvoiceID = request.InvoiceID,
                        InvoiceType = (int)InvoiceType.Waiting,
                        CustomerID = request.CustomerID,
                        LenderID = request.LenderID,
                        LoanID = request.LoanID,
                        Note = request.Note,
                        FromBankCardID = (long)objInvoice.BankCardID,
                        FromShopID = request.ShopID,
                        TransactionDate = DateTime.Now,
                        CreateDate = DateTime.Now,
                        CreateBy = request.UserIDCreate,
                        Status = (int)Invoice_Status.WaitingConfirm,
                        Request = _common.ConvertObjectToJSonV2(request),
                    };
                    _ = await _logInvoiceTab.InsertAsync(logInvoice);
                    // treo chờ xác minh -> bắn về ag
                    if (request.InvoiceAGID == 0)
                    {
                        var objLoan = (await _loanTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                        if (request.TypeVerifyInvoiceConsider == (int)TypeVerifyInvoiceConsider.Customer && objInvoice.Status == (int)Invoice_Status.WaitingConfirm)
                        {
                            List<string> lstImage = new List<string>();
                            foreach (var item in request.PathImg)
                            {
                                var typeFile = item.FileName.Substring(item.FileName.LastIndexOf(".") + 1);
                                string md5Key = _common.MD5(item.FileName + Constants.LOS_KEY);
                                var path = $"{item.S3Path}/{md5Key}.{typeFile}";
                                lstImage.Add($"{path}");
                            }
                            _ = await _collectionService.InsertConfirmSuspendedMoney(objInvoice.PartnerID, (int)request.UserIDCreate,
                             DateTime.Now, lstImage, request.Note, objInvoice.InvoiceID, objLoan.TimaLoanID, customerID);
                        }
                    }
                    response.SetSucces();
                    response.Data = objInvoice.InvoiceID;
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"VerifyInvoiceConsiderCustomerCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
