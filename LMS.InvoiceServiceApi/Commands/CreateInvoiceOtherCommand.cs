﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Commands
{
    public class CreateInvoiceOtherCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public int InvoiceSubType { get; set; }
        [Required]
        public long BankCardID { get; set; }
        [Required]
        public long TotalMoney { get; set; }
        [Required]
        public string Note { get; set; }
        [Required]
        public string StrTransactionDate { get; set; }
        [Required]
        public long UserIDCreate { get; set; }
        public long InvoiceTimaID { get; set; }
        public long ShopID { get; set; }

    }
    public class CreateInvoiceOtherCommandHandler : IRequestHandler<CreateInvoiceOtherCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> _logInvoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        RestClients.ILenderService _lenderService;
        ILogger<CreateInvoiceOtherCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateInvoiceOtherCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> logInvoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            RestClients.ILenderService lenderService,
            ILogger<CreateInvoiceOtherCommandHandler> logger)
        {
            _invoiceTab = invoiceTab;
            _bankCardTab = bankCardTab;
            _userTab = userTab;
            _logInvoiceTab = logInvoiceTab;
            _lenderService = lenderService;
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateInvoiceOtherCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var transactionDate = DateTime.ParseExact(request.StrTransactionDate, _common.DateTimeDDMMYYYYHHMMSS, CultureInfo.InvariantCulture);
                if (transactionDate == DateTime.MinValue)
                {
                    transactionDate = DateTime.Now;
                }
                var logInvoice = new Domain.Tables.TblLogInvoice
                {
                    InvoiceType = (int)GroupInvoiceType.ReceiptInterestBank,
                    Note = request.Note,
                    TransactionDate = transactionDate,
                    CreateDate = DateTime.Now,
                    Amount = request.TotalMoney,
                    FromShopID = request.ShopID,
                    FromBankCardID = request.BankCardID,
                    ToBankCardID = request.BankCardID,
                    CreateBy = request.UserIDCreate,
                    Request = _common.ConvertObjectToJSonV2(request),
                };
                logInvoice.LogInvoiceID = await _logInvoiceTab.InsertAsync(logInvoice);
                if ((DateTime.Now.Date.Subtract(transactionDate.Date)).Days > Constants.DateTransactionMinApply)
                {
                    response.Message = MessageConstant.StrCheckCreateDate;
                    _logger.LogError($"CreateInvoiceOtherCommandHandler_Warning|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                }
                var objBankCard = await _bankCardTab.GetByIDAsync(request.BankCardID);
                if (objBankCard == null)
                {
                    response.Message = MessageConstant.NotIdentifiedBankcardID;
                    _logger.LogError($"CreateInvoiceOtherCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                var objUser = await _userTab.GetByIDAsync(request.UserIDCreate);
                if (objUser == null)
                {
                    response.Message = MessageConstant.NotIdentifiedUserID;
                    _logger.LogError($"CreateInvoiceOtherCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                int invoiceType = 0;
                if (request.InvoiceSubType == (int)GroupInvoiceType.ReceiptOther)
                {
                    invoiceType = (int)InvoiceType.Receipt;
                }
                if (request.InvoiceSubType == (int)GroupInvoiceType.PaySlipOther)
                {
                    invoiceType = (int)InvoiceType.PaySlip;
                    request.TotalMoney = -request.TotalMoney;
                }
                request.ShopID = request.ShopID == 0 ? (long)ShopId.Tima : request.ShopID;
                var objShop = (await _lenderTab.WhereClause(x => x.LenderID == request.ShopID).QueryAsync()).FirstOrDefault();
                var objInvoice = _common.ConvertParentToChild<Domain.Tables.TblInvoice, CreateInvoiceOtherCommand>(request);
                objInvoice.InvoiceType = invoiceType;
                objInvoice.TransactionDate = transactionDate;
                objInvoice.InvoiceSubType = request.InvoiceSubType;
                objInvoice.CreateDate = DateTime.Now;
                objInvoice.TransactionMoney = request.TotalMoney;
                objInvoice.SourceID = request.BankCardID;
                objInvoice.DestinationID = request.BankCardID;
                objInvoice.Status = (int)Invoice_Status.Confirmed;
                objInvoice.JobStatus = (int)Invoice_JobStatus.Unprocessed;
                objInvoice.InvoiceCode = _common.HashMD5($"{request.Note}-{objInvoice.InvoiceSubType}-{request.BankCardID}-{request.TotalMoney}-{transactionDate.Ticks}");
                objInvoice.PartnerID = request.InvoiceTimaID;
                var invoiceExtraInfo = new Domain.Tables.InvoiceJsonExtra
                {
                    SourceName = objBankCard.AliasName,
                    DestinationName = objBankCard.AliasName,
                    ShopName = objShop?.FullName,
                    CreateBy = objUser.FullName,
                };
                objInvoice.JsonExtra = _common.ConvertObjectToJSonV2(invoiceExtraInfo);

                var invoiceCode = (await _invoiceTab.WhereClause(x => x.InvoiceCode == objInvoice.InvoiceCode).QueryAsync()).FirstOrDefault();// check phiếu đã dc tạo
                if (invoiceCode != null)
                {
                    response.Message = MessageConstant.InvoiceExits;
                    return response;
                }
                var insertID = await _invoiceTab.InsertAsync(objInvoice);
                if (insertID > 0)
                {
                    response.SetSucces();
                    response.Data = insertID;
                    logInvoice.InvoiceID = insertID;
                    _ = _logInvoiceTab.UpdateAsync(logInvoice);
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateInvoiceOtherCommand|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
