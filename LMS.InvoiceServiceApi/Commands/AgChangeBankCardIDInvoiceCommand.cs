﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Commands
{
    public class AgChangeBankCardIDInvoiceCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long BankCardIDNew { get; set; }
        public long BankCardIDOld { get; set; }
        public string Note { get; set; }
        public long UserIDCreate { get; set; }
        public long InvoiceTimaID { get; set; }
    }
    public class AgChangeBankCardIDInvoiceCommandHandler : IRequestHandler<AgChangeBankCardIDInvoiceCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> _logInvoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        RestClients.ILenderService _lenderService;
        RestClients.ILoanService _loanService;
        ILogger<AgChangeBankCardIDInvoiceCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public AgChangeBankCardIDInvoiceCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> logInvoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            RestClients.ILenderService lenderService,
            RestClients.ILoanService loanService,
            ILogger<AgChangeBankCardIDInvoiceCommandHandler> logger)
        {
            _invoiceTab = invoiceTab;
            _bankCardTab = bankCardTab;
            _userTab = userTab;
            _logInvoiceTab = logInvoiceTab;
            _lenderService = lenderService;
            _lenderTab = lenderTab;
            _loanService = loanService;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(AgChangeBankCardIDInvoiceCommand request, CancellationToken cancellationToken)
        {
            var response = new ResponseActionResult();
            try
            {
                var invoiceDetail = (await _invoiceTab.WhereClause(x => x.PartnerID == request.InvoiceTimaID).QueryAsync()).FirstOrDefault();
                if (invoiceDetail == null || invoiceDetail.InvoiceID < 1)
                {
                    response.Message = MessageConstant.InvoiceNotExits;
                    _logger.LogError($"AgChangeBankCardIDInvoiceCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                var bankcardNewInfo = (await _bankCardTab.WhereClause(x => x.BankCardID == request.BankCardIDNew).QueryAsync()).FirstOrDefault();
                var logInvoice = new Domain.Tables.TblLogInvoice
                {
                    InvoiceID = invoiceDetail.InvoiceID,
                    InvoiceType = invoiceDetail.InvoiceType,
                    CustomerID = invoiceDetail.CustomerID,
                    LoanID = invoiceDetail.LoanID,
                    Note = invoiceDetail.Note,
                    FromShopID = invoiceDetail.ShopID,
                    TransactionDate = invoiceDetail.TransactionDate,
                    CreateDate = DateTime.Now,
                    Amount = invoiceDetail.TotalMoney,
                    FromBankCardID = invoiceDetail.BankCardID,
                    ToBankCardID = request.BankCardIDNew,
                    CreateBy = request.UserIDCreate,
                    Request = _common.ConvertObjectToJSonV2(request),
                };
                logInvoice.LogInvoiceID = _logInvoiceTab.Insert(logInvoice);
                Domain.Tables.InvoiceJsonExtra invoiceExtraInfo = _common.ConvertJSonToObjectV2<Domain.Tables.InvoiceJsonExtra>(invoiceDetail.JsonExtra);
                switch ((InvoiceType)invoiceDetail.InvoiceType)
                {
                    case InvoiceType.PaySlip:
                        invoiceExtraInfo.SourceName = bankcardNewInfo?.AliasName;
                        invoiceDetail.SourceID = request.BankCardIDNew;
                        break;
                    case InvoiceType.Receipt:
                        invoiceExtraInfo.DestinationName = bankcardNewInfo?.AliasName;
                        invoiceDetail.DestinationID = request.BankCardIDNew;
                        break;
                }
                invoiceDetail.BankCardID = request.BankCardIDNew;
                invoiceDetail.ModifyDate = DateTime.Now;
                invoiceDetail.Note = $"{request.Note} {invoiceDetail.Note}";
                invoiceDetail.JsonExtra = _common.ConvertObjectToJSonV2(invoiceExtraInfo);
                _invoiceTab.Update(invoiceDetail);
                _ = _loanService.ProcessSummaryBankCardByBankCardID(new List<long> { request.BankCardIDOld, request.BankCardIDNew }, invoiceDetail.TransactionDate.Value);
                response.SetSucces();
                response.Data = invoiceDetail.InvoiceID;
            }
            catch (Exception ex)
            {
                _logger.LogError($"AgChangeBankCardIDInvoiceCommandHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
