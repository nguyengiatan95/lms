﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Commands
{
    public class CreateInvoicePartnerCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public long PartnerID { get; set; }
        [Required]
        public long TotalMoney { get; set; }
        [Required]
        public string StrTransactionDate { get; set; }
        [Required]
        public long BankCardID { get; set; }
        [Required]
        public string Note { get; set; }
        [Required]
        public long UserIDCreate { get; set; }
        public long LoanID { get; set; }
        public long ShopID { get; set; }
    }
    public class CreateInvoicePartnerCommandHandler : IRequestHandler<CreateInvoicePartnerCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> _logInvoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        RestClients.ICustomerService _customerService;
        ILogger<CreateInvoicePartnerCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateInvoicePartnerCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> logInvoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            RestClients.ICustomerService customerService,
            ILogger<CreateInvoicePartnerCommandHandler> logger)
        {
            _invoiceTab = invoiceTab;
            _loanTab = loanTab;
            _customerTab = customerTab;
            _bankCardTab = bankCardTab;
            _userTab = userTab;
            _logInvoiceTab = logInvoiceTab;
            _lenderTab = lenderTab;
            _customerService = customerService;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateInvoicePartnerCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var transactionDate = DateTime.ParseExact(request.StrTransactionDate, _common.DateTimeDDMMYYYYHHMMSS, CultureInfo.InvariantCulture);
                var logInvoice = new Domain.Tables.TblLogInvoice
                {
                    PartnerID = request.PartnerID,
                    InvoiceType = (int)GroupInvoiceType.ReceiptPartner,
                    Note = request.Note,
                    FromBankCardID = request.BankCardID,
                    TransactionDate = transactionDate,
                    CreateDate = DateTime.Now,
                    Amount = request.TotalMoney,
                    CreateBy = request.UserIDCreate,
                    Request = _common.ConvertObjectToJSonV2(request),
                };
                logInvoice.LogInvoiceID = await _logInvoiceTab.InsertAsync(logInvoice);
                if ((DateTime.Now.Date.Subtract(transactionDate.Date)).Days > Constants.DateTransactionMinApply)
                {
                    response.Message = MessageConstant.StrCheckCreateDate;
                    _logger.LogError($"CreateInvoicePartnerCommandHandler_Warning|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                }
                var objBankCard = await _bankCardTab.GetByIDAsync(request.BankCardID);
                if (objBankCard == null)
                {
                    response.Message = MessageConstant.NotIdentifiedBankcardID;
                    _logger.LogError($"CreateInvoicePartnerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                var objUser = await _userTab.GetByIDAsync(request.UserIDCreate);
                _logger.LogError($"CreateInvoicePartnerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                if (objUser == null)
                {
                    response.Message = MessageConstant.NotIdentifiedUserID;
                    _logger.LogError($"CreateInvoicePartnerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                request.ShopID = request.ShopID == 0 ? (long)ShopId.Tima : request.ShopID;
                var objShop = (await _lenderTab.WhereClause(x => x.LenderID == request.ShopID).QueryAsync()).FirstOrDefault();
                var objInvoice = _common.ConvertParentToChild<Domain.Tables.TblInvoice, CreateInvoicePartnerCommand>(request);
                objInvoice.InvoiceType = (int)InvoiceType.Receipt;
                objInvoice.InvoiceSubType = (int)GroupInvoiceType.ReceiptPartner;
                objInvoice.CreateDate = DateTime.Now;
                objInvoice.TransactionDate = transactionDate;
                objInvoice.TransactionMoney = request.TotalMoney;
                objInvoice.Status = (int)Invoice_Status.Confirmed;
                objInvoice.JobStatus = (int)Invoice_JobStatus.Unprocessed;
                objInvoice.InvoiceCode = $"{(int)GroupInvoiceType.ReceiptPartner}-{request.TotalMoney}-{transactionDate.Ticks}";
                var invoiceExtraInfo = new Domain.Tables.InvoiceJsonExtra
                {
                    SourceName = objBankCard.AliasName,
                    DestinationName = objBankCard.AliasName,
                    ShopName = objShop?.FullName,
                    CreateBy = objUser.FullName,
                };
                objInvoice.JsonExtra = _common.ConvertObjectToJSonV2(invoiceExtraInfo);
                var invoiceCode = (await _invoiceTab.WhereClause(x => x.InvoiceCode == objInvoice.InvoiceCode).QueryAsync()).FirstOrDefault();// check phiếu đã dc tạo
                if (invoiceCode != null)
                {
                    response.Message = MessageConstant.InvoiceExits;
                    return response;
                }
                var insertID = await _invoiceTab.InsertAsync(objInvoice);
                if (insertID > 0)
                {
                    response.SetSucces();
                    logInvoice.InvoiceID = insertID;
                    _ = _logInvoiceTab.UpdateAsync(logInvoice);
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateInvoicePartnerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;

        }
    }
}
