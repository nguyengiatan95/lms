﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Commands
{
    public class ChangeStatusTicketCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [System.ComponentModel.DataAnnotations.Required]
        public long TicketID { get; set; }
        public long UserID { get; set; }
        public int Status { get; set; }
    }
    public class ChangeStatusCommandHandler : IRequestHandler<ChangeStatusTicketCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTicket> _ticketTab;
        ILogger<ChangeStatusCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public ChangeStatusCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblTicket> ticketTab, ILogger<ChangeStatusCommandHandler> logger)
        {
            _ticketTab = ticketTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(ChangeStatusTicketCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var ticket = (await _ticketTab.WhereClause(x => x.TicketID == request.TicketID).QueryAsync()).FirstOrDefault();
                if (ticket.Status != (int)Ticket_Status.Waiting)
                {
                    response.Message = MessageConstant.TicketNotStatusWaiting;
                    _logger.LogError($"ChangeStatusCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                ticket.Status = (short)request.Status;
                ticket.ModifyDate = DateTime.Now;
                ticket.ModifyUserID = request.UserID;
                var update = _ticketTab.Update(ticket);
                if (update)
                {
                    response.SetSucces();
                    response.Data = ticket.TicketID;
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ChangeStatusCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

    }
}
