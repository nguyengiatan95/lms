﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Commands
{
    public class CreateInvoiceTransferMoneyStoreCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public int InvoiceSubType { get; set; }
        [Required]
        public long ShopID { get; set; }
        [Required]
        public long TotalMoney { get; set; }
        [Required]
        public string StrTransactionDate { get; set; }
        [Required]
        public long FromBankCardID { get; set; }
        [Required]
        public long ToBankCardID { get; set; }
        [Required]
        public string Note { get; set; }
        [Required]
        public long UserIDCreate { get; set; }
        public long InvoiceTimaID { get; set; }

    }
    public class CreateInvoiceTransferMoneyStoreCommandHandler : IRequestHandler<CreateInvoiceTransferMoneyStoreCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> _logInvoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        RestClients.ICustomerService _customerService;
        ILogger<CreateInvoiceTransferMoneyStoreCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateInvoiceTransferMoneyStoreCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> logInvoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            RestClients.ICustomerService customerService,
            ILogger<CreateInvoiceTransferMoneyStoreCommandHandler> logger)
        {
            _invoiceTab = invoiceTab;
            _loanTab = loanTab;
            _bankCardTab = bankCardTab;
            _userTab = userTab;
            _logInvoiceTab = logInvoiceTab;
            _customerService = customerService;
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateInvoiceTransferMoneyStoreCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var transactionDate = DateTime.ParseExact(request.StrTransactionDate, _common.DateTimeDDMMYYYYHHMMSS, CultureInfo.InvariantCulture);
                if (transactionDate == DateTime.MinValue)
                {
                    transactionDate = DateTime.Now;
                }
                var logInvoice = new Domain.Tables.TblLogInvoice
                {
                    InvoiceType = (int)request.InvoiceSubType,
                    Note = request.Note,
                    TransactionDate = transactionDate,
                    CreateDate = DateTime.Now,
                    Amount = request.TotalMoney,
                    FromBankCardID = request.FromBankCardID,
                    ToBankCardID = request.ToBankCardID,
                    FromShopID = request.ShopID,
                    ToShopID = request.ShopID,
                    CreateBy = request.UserIDCreate,
                    Request = _common.ConvertObjectToJSonV2(request),
                };
                logInvoice.LogInvoiceID = await _logInvoiceTab.InsertAsync(logInvoice);

                var lstBankCard = await _bankCardTab.WhereClause(x => x.BankCardID == request.FromBankCardID || x.BankCardID == request.ToBankCardID).QueryAsync();
                if (lstBankCard == null)
                {
                    response.Message = MessageConstant.NotIdentifiedBankcardID;
                    _logger.LogError($"CreateInvoiceTransferMoneyStoreCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                var objUser = await _userTab.GetByIDAsync(request.UserIDCreate);
                if (objUser == null)
                {
                    response.Message = MessageConstant.NotIdentifiedUserID;
                    _logger.LogError($"CreateInvoiceTransferMoneyStoreCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }
                request.TotalMoney *= -1;
                var objShop = (await _lenderTab.WhereClause(x => x.LenderID == request.ShopID).QueryAsync()).FirstOrDefault();
                var objInvoice = _common.ConvertParentToChild<Domain.Tables.TblInvoice, CreateInvoiceTransferMoneyStoreCommand>(request);
                objInvoice.InvoiceType = (int)InvoiceType.PaySlip;
                objInvoice.InvoiceSubType = (int)request.InvoiceSubType;
                objInvoice.CreateDate = DateTime.Now;
                objInvoice.TransactionDate = transactionDate;
                objInvoice.TransactionMoney = request.TotalMoney;
                objInvoice.BankCardID = request.ToBankCardID;
                objInvoice.SourceID = request.FromBankCardID;
                objInvoice.DestinationID = request.ToBankCardID;
                objInvoice.Status = (int)Invoice_Status.Confirmed;
                objInvoice.JobStatus = (int)Invoice_JobStatus.Unprocessed;
                objInvoice.PartnerID = request.InvoiceTimaID;
                objInvoice.InvoiceCode = _common.HashMD5($"{request.Note}-{objInvoice.InvoiceSubType}-{request.ToBankCardID}-{request.TotalMoney}-{transactionDate.Ticks}");
                var sourceName = lstBankCard.FirstOrDefault(x => x.BankCardID == request.FromBankCardID);
                var destinationName = lstBankCard.FirstOrDefault(x => x.BankCardID == request.ToBankCardID);
                var invoiceExtraInfo = new Domain.Tables.InvoiceJsonExtra
                {
                    SourceName = sourceName?.AliasName,
                    DestinationName = destinationName?.AliasName,
                    ShopName = objShop?.FullName,
                    CreateBy = objUser.FullName,
                };
                objInvoice.JsonExtra = _common.ConvertObjectToJSonV2(invoiceExtraInfo);
                var checkInvoiceCode = (await _invoiceTab.WhereClause(x => x.InvoiceCode == objInvoice.InvoiceCode).QueryAsync()).FirstOrDefault();// check phiếu đã dc tạo
                if (checkInvoiceCode != null)
                {
                    response.Message = MessageConstant.InvoiceExits;
                    return response;
                }
                var insertID = await _invoiceTab.InsertAsync(objInvoice);
                if (insertID > 0)
                {
                    response.SetSucces();
                    response.Data = insertID;
                    logInvoice.InvoiceID = insertID;
                    _ = _logInvoiceTab.UpdateAsync(logInvoice);
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateInvoiceTransferMoneyStoreCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
