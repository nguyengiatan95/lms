﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Commands
{
    public class UpdatePathImgCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public long InvoiceID { get; set; }
        public List<string> PathImg { get; set; }

    }
    public class UpdatePathImgCommandHandler : IRequestHandler<UpdatePathImgCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> _logInvoiceTab;
        ILogger<UpdatePathImgCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdatePathImgCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInvoice> logInvoiceTab,
            ILogger<UpdatePathImgCommandHandler> logger)
        {
            _invoiceTab = invoiceTab;
            _userTab = userTab;
            _logInvoiceTab = logInvoiceTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(UpdatePathImgCommand request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    var objInvoice = _invoiceTab.WhereClause(x => x.InvoiceID == request.InvoiceID).Query().FirstOrDefault();
                    if (objInvoice == null)
                    {
                        response.Message = MessageConstant.InvoiceNotExits;
                        return response;
                    }
                    objInvoice.PathImg = _common.ConvertObjectToJSonV2(request.PathImg);
                    var update = _invoiceTab.Update(objInvoice);
                    if (update)
                    {
                        response.SetSucces();
                        response.Data = objInvoice.InvoiceID;
                    }
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"UpdatePathImgCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                }
                return response;
            });
        }
    }
}
