﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InvoiceServiceApi.Helper
{
    public class Utils
    {
        public string DateTimeDDMMYYYY = "dd-MM-yyyy";
        public string DateTimeDDMMYYYYHHMMSS = "dd-MM-yyyy HH:mm:ss";
        public string DateTimeYYYYMMDD = "yyyyMMdd";
        public string DateTimeHHDDMMYYYY = "HH:mm dd-MM-yyyy";
        public string DateTimeDDMMYYYYHHMM = "dd/MM/yyyy HH:mm";
        public string DateTimeDDMMYYYYNoTime = "dd/MM/yyyy";

        public const string UnitTimeNameDate = "Ngày";
        public const string UnitTimeNameMonth = "Tháng";
        public const string DateTimeDayMonthYear = "dd/MM/yyyy";
        public bool ValidateLength(string note,int length)
        {
            try
            {
                if (string.IsNullOrEmpty(note))
                    return true;
                if (note.Length > length)
                    return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool ValidateDate(string strdate, string fomat)
        {
            try
            {
                DateTime dt = DateTime.ParseExact(strdate, fomat, CultureInfo.InvariantCulture);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
