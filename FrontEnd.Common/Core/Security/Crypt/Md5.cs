﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Core.Security.Crypt
{
    public class Md5
    {
        public static string GetMD5Hash(string input)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] bs = System.Text.Encoding.UTF8.GetBytes(input);
            bs = x.ComputeHash(bs);
            return Convert.ToBase64String(bs); 
        }
    }
}
