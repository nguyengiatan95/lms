﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Message
{
    public class MessageAction
    {
        public const string MessageCreateSuccess = "Thêm mới thành công!";
        public const string MessageVerifySuccess = "Thành công!";
        public const string MessageUpdateSuccess = "Cập nhật thành công!";
        public const string MessageDeleteSuccess = "Xóa dữ liệu thành công!";
        public const string MessageActionFailed = "Yêu cầu thất bại. Vui lòng thử lại sau!";
        public const string ModelStateNotValid = "Dữ liệu truyền vào không hợp lệ. Vui lòng kiểm tra lại";
        public const string DataAlreadyExists = "Dữ liệu đã tồn tại trong hệ thống. Vui lòng nhập thông tin khác";
        public const string DataIsEmpty = "Không có dữ liệu để thao tác";
        public const string DataRelated = "Đã phát sinh nghiệp vụ liên quan. Bạn không thể xóa!";
    }
}
