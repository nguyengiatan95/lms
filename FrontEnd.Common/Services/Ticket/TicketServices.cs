﻿using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Models.Ticket;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Common.Services.Ticket
{
    public interface ITicketServices
    {
        public Task<ResponseActionResult<ResponseForDataTable<List<LMS.Entites.Dtos.InvoiceService.TicketListView>>>> GetTicketConditions(string accessToken, TicketModel model);
    }
    public class TicketServices : BaseServices, ITicketServices
    {
        readonly ServiceHost _hostSetting;
        public TicketServices(IOptions<ServiceHost> options, IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _hostSetting = options.Value;
        }
        public async Task<ResponseActionResult<ResponseForDataTable<List<LMS.Entites.Dtos.InvoiceService.TicketListView>>>> GetTicketConditions(string accessToken, TicketModel model)
        {
            var response = new ResponseActionResult<ResponseForDataTable<List<LMS.Entites.Dtos.InvoiceService.TicketListView>>>();
            try
            {
                string uri = $"{_hostSetting.Invoice}{Constants.GetTicketInfosByCondition}";
                var json = JsonConvert.SerializeObject(model);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                response = await PostAsync<ResponseActionResult<ResponseForDataTable<List<LMS.Entites.Dtos.InvoiceService.TicketListView>>>>(uri, accessToken, stringContent);
                return response;
            }
            catch
            {
                response.SetError();
            }
            return response;
        }
    }
}
