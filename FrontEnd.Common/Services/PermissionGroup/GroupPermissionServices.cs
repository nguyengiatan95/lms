﻿using FrontEnd.Common.Entites.Management;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Models.Management.Permission;
using FrontEnd.Common.Services.Permission; 
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Common.Services.PermissionGroup
{
    public interface IGroupPermissionServices
    {
        public ResponseActionResult GetListPermissionByGroup(long groupID, string accessToken );
        public ResponseActionResult AddPermission(long groupID, string accessToken , List<long> lstPermission);
    }
    public class GroupPermissionServices

        : BaseServices, IGroupPermissionServices
    {
        readonly IPermissionService _permissionService;
        readonly ServiceHost _hostSetting;
        public GroupPermissionServices(IOptions<ServiceHost> options, IConfiguration configuration, IHttpClientFactory clientFactory,
            IPermissionService permissionService) : base(configuration, clientFactory)
        {
            _hostSetting = options.Value;
            _permissionService = permissionService;
        }

        public ResponseActionResult AddPermission(long groupID, string accessToken , List<long> lstPermission)
        {
            var reponseData = new ResponseActionResult();
            var request = new Models.Management.GroupPermission.CreateGroupPermissionModel()
            {
                GroupID = groupID,
                LstPermissionID = lstPermission
            };
            try
            {
                string uri = $"{_hostSetting.Admin}{Constants.AddPermissionGroup}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<ResponseActionResult<long?>>(uri, accessToken , stringContent);
                reponseData.Result = response.Result.Result;
                reponseData.Message = response.Result.Message;
                reponseData.Data = response.Result.Data;
                return reponseData;
            }
            catch
            {

            }
            return reponseData;
        }

        public ResponseActionResult GetListPermissionByGroup(long groupID, string accessToken )
        {
            ResponseActionResult response = new ResponseActionResult();
            List<JSTreeModel> lstData = new List<JSTreeModel>();
            try
            {
                // call lấy all permission
                var taskPermission = _permissionService.GetByConditions(accessToken , "", 1, 1, 100000, "LinkApi", "desc");
                // call lấy permission của 
                var taskGroupPermission = Task.Run(() =>
                {
                    var response = GetAsync<ResponseActionResult<List<TblPermissionGroup>>>($"{_hostSetting.Admin}{Constants.GetPermissionByGroupID}{groupID}", accessToken , null);
                    if (response.Result.Result == 1 && response.Result.Data.Count > 0)
                        return response.Result.Data;
                    return new List<TblPermissionGroup>();
                });
                Task.WaitAll(taskPermission, taskGroupPermission);
                var lstPermission = taskPermission.Result;
                var lstGroupPermission = taskGroupPermission.Result;
                if (lstPermission == null || lstGroupPermission == null)
                {
                    return response;
                }
                foreach (var item in lstPermission)
                {
                    JSTreeModel objData = new JSTreeModel
                    {
                        Link = item.LinkApi,
                        id = item.PermissionID.ToString(),
                        text = item.DisplayText,
                        state = new State()
                    };
                    int selected = lstGroupPermission.Count(m => m.PermissionID == item.PermissionID);
                    if (selected != 0)
                    {
                        objData.state.selected = true;
                    }
                    lstData.Add(objData);
                }
                response.Result = (int)LMS.Common.Constants.ResponseAction.Success;
                response.Data = lstData;
                response.Message = "";
                return response;
            }
            catch
            {
                return response;
            }
        }
    }
}
