﻿using FrontEnd.Common.Entites.Accountants;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Models.Loan;
using LMS.Entites.Dtos.LoanServices;
using LMS.Entites.Dtos.PaymentServices;
using LMS.Entites.Dtos.ReportServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Common.Services.Loan
{
    public interface ILoanServices
    {
        public Task<ResponseActionResult<DataResult<List<LoanItemViewModel>>>> GetLstLoanInfoByCondition(string accessToken, LoanDatatableModel model);
        public Task<ResponseActionResult<List<PaymentScheduleModel>>> GetLstPaymentScheduleByLoanID(string accessToken, int loanID); 
        public Task<ResponseActionResult<LoanDetailViewModel>> GetLoanByID(string accessToken, int loanID);
        public Task<ResponseActionResult<LoanDetailViewModel>> ExtendLoanTime(string accessToken, int loanID);
        Task<ResponseActionResult<ReportStasticsLoanBorrowModel>> Getstasticloanstatusbyshop(string accessToken, RequestReport model);
        public Task<ResponseActionResult> Delete(string accessToken, long loanID,long userID);
    }
    public class LoanServices : BaseServices, ILoanServices
    {
        readonly ServiceHost _hostSetting;
        public LoanServices(IOptions<ServiceHost> options, IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _hostSetting = options.Value;
        }
        public async Task<ResponseActionResult<DataResult<List<LoanItemViewModel>>>> GetLstLoanInfoByCondition(string accessToken, LoanDatatableModel model)
        {
            var response = new ResponseActionResult<DataResult<List<LoanItemViewModel>>>();
            try
            {
                string uri = $"{_hostSetting.Loan}{Constants.GetLstLoanInfoByCondition}";
                var json = JsonConvert.SerializeObject(model);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                response = await PostAsync<ResponseActionResult<DataResult<List<LoanItemViewModel>>>>(uri, accessToken, stringContent);
                return response;
            }
            catch
            {
                response.SetError();
            }
            return response;
        }
        public async Task<ResponseActionResult<List<PaymentScheduleModel>>> GetLstPaymentScheduleByLoanID(string accessToken, int loanID)
        {
            var response = new ResponseActionResult<List<PaymentScheduleModel>>();
            try
            {
                string uri = $"{_hostSetting.Loan}{Constants.GetLstPaymentScheduleByLoanID}{loanID}";
                response = await GetAsync<ResponseActionResult<List<PaymentScheduleModel>>>(uri, accessToken, null);
                return response;
            }
            catch
            {
                response.SetError();
            }
            return response;
        }
        public async Task<ResponseActionResult<LoanDetailViewModel>> GetLoanByID(string accessToken, int loanID)
        {
            var response = new ResponseActionResult<LoanDetailViewModel>();
            try
            {
                string uri = $"{_hostSetting.Loan}{Constants.GetLoanByID}{loanID}";
                response = await GetAsync<ResponseActionResult<LoanDetailViewModel>>(uri, accessToken, null);
                return response;
            }
            catch
            {
                response.SetError();
            }
            return response;
        }
        public async Task<ResponseActionResult<LoanDetailViewModel>> ExtendLoanTime(string accessToken, int loanID)
        {
            var response = new ResponseActionResult<LoanDetailViewModel>();
            try
            {
                string uri = $"{_hostSetting.Loan}{Constants.ExtendLoanTime}{loanID}";
                response = await GetAsync<ResponseActionResult<LoanDetailViewModel>>(uri, accessToken, null);
                return response;
            }
            catch
            {
                response.SetError();
            }
            return response;
        }
        public async Task<ResponseActionResult<ReportStasticsLoanBorrowModel>> Getstasticloanstatusbyshop(string accessToken, RequestReport model)
        {
            var response = new ResponseActionResult<ReportStasticsLoanBorrowModel>();
            try
            {
                string uri = $"{_hostSetting.Loan}{Constants.Getstasticloanstatusbyshop}";
                var json = JsonConvert.SerializeObject(model);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                response = await PostAsync<ResponseActionResult<ReportStasticsLoanBorrowModel>>(uri, accessToken, stringContent);
                return response;
            }
            catch
            {
                response.SetError();
            }
            return response;
        }

        public async Task<ResponseActionResult> Delete(string accessToken, long loanID, long userID)
        {
            var response = new ResponseActionResult();
            try
            {
                RequestDeleteLoan request = new RequestDeleteLoan()
                {
                    CreateBy = userID,
                    LoanID = loanID
                };
                string uri = $"{_hostSetting.Loan}{Constants.DeleteLoan}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                response = await PostAsync<ResponseActionResult>(uri, accessToken, stringContent);
                return response;
            }
            catch
            {
                response.Result = (int)LMS.Common.Constants.ResponseAction.Error;
                response.Message = LMS.Common.Constants.MessageConstant.ErrorInternal;
            }
            return response;
        }
    }
}
