﻿using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Common.Services.Shop
{ 
    public interface IShopService
    {
        public Task<List<Entites.Management.Shop>> GetShops(string accessToken); 
    }
    public class ShopService : BaseServices, IShopService
    {
        readonly ServiceHost _hostSetting;
        public ShopService(IOptions<ServiceHost> options, IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _hostSetting = options.Value;
        }
        
        public async Task<List<Entites.Management.Shop>> GetShops(string accessToken)
        {
            var data = new List<Entites.Management.Shop>();
            try
            {
                string uri = $"{_hostSetting.Admin}{Constants.GetShops}";
                var response = await GetAsync<ResponseActionResult<List<Entites.Management.Shop>>>(uri, accessToken, null);
                if (response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
                if (data != null && data.Count > 0)
                {
                    data = response.Data;
                }
            }
            catch
            {

            }
            return data; 
        }
         
    }
}
