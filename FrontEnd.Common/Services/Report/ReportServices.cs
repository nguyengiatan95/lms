﻿using FrontEnd.Common.Entites.Report;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Models.Report;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Common.Services.Report
{ 
    public interface IReportServices
    {
        public Task<List<ReportVatHubItem>> GetByConditions(string accessToken, ReportVatHUbWithGCashQuery request);
        Task<List<MoneyDetailLenderModel>> ReportMoneyDetail(string accessToken, ReportMoneyDetailLenderQuery request);
    }
    public class ReportServices : BaseServices, IReportServices
    {
        readonly ServiceHost _hostSetting;
        public ReportServices(IOptions<ServiceHost> options, IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _hostSetting = options.Value;
        }
        public async Task<List<ReportVatHubItem>> GetByConditions(string accessToken, ReportVatHUbWithGCashQuery request)
        {
            var data = new List<ReportVatHubItem>();
            try
            {
                string uri = $"{_hostSetting.Loan}{Constants.ReportVatHUbWithGCash}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<ResponseActionResult<List<ReportVatHubItem>>>(uri, accessToken, stringContent);
                if (response.Result == 1 && response.Data.Count > 0)
                    data = response.Data; 
            }
            catch
            {
            }
            return data;
        }
        public async Task<List<MoneyDetailLenderModel>> ReportMoneyDetail(string accessToken, ReportMoneyDetailLenderQuery request)
        {
            var data = new List<MoneyDetailLenderModel>();
            try
            {
                string uri = $"{_hostSetting.Loan}{Constants.ReportMoneyDetail}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<ResponseActionResult<List<MoneyDetailLenderModel>>>(uri, accessToken, stringContent);
                if (response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
            }
            catch
            {
            }
            return data;
        }
    }
}
