﻿using FrontEnd.Common.Entites.Management; 
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Models.Management.User;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Common.Services.User
{
    public interface IUserService
    {
        public Task<List<TblUser>> GetUsers(string accessToken , string para);
        public Task<TblUser> GetUserByUserID(string accessToken , long id); 
        public Task<ResponseActionResult> CreateUserFromAppValid(string accessToken , TblUser entity); 
        public Task<ResponseActionResult> UpdateUserFromApp(string accessToken , TblUser entity);
        public Task<ResponseActionResult> ChangePassword(string accessToken , TblUser entity);
        public Task<List<TblUserGroup>> GetUserGroupByUserId(string accessToken , int userId);
    }
    public class UserService : BaseServices, IUserService
    {
        readonly ServiceHost _hostSetting;
        public UserService(IOptions<ServiceHost> options, IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _hostSetting = options.Value;
        }
        public async Task<TblUser> GetUserByUserID(string accessToken , long id)
        {
            var data = new TblUser();
            try
            {
                var response = await GetAsync<ResponseActionResult<TblUser>>($"{_hostSetting.Admin}{Constants.GetUserByUserID}{id}", accessToken , null);
                if (response != null && response.Result == 1 && response.Data != null)
                    data = response.Data;
            }
            catch
            {

            }
            return data;
        }
        
        public async Task<List<TblUser>> GetUsers(string accessToken , string para)
        {
            var data = new List<TblUser>();
            try
            {
                string uri = $"{_hostSetting.Admin}{Constants.GetUser}{para}";
                var response = await GetAsync<ResponseActionResult<List<TblUser>>>(uri, accessToken , null);
                if (response != null && response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
                if (data != null && data.Count > 0)
                {
                    data[0].TotalCount = response.Total;
                }
            }
            catch
            {
            }
            return data;
        }
        public async Task<List<TblUserGroup>> GetUserGroupByUserId(string accessToken , int userId)
        {
            var data = new List<TblUserGroup>();
            try
            {
                string uri = $"{_hostSetting.Admin}{Constants.GetUserGroupByUserId}{userId}";
                var response = await GetAsync<ResponseActionResult<List<TblUserGroup>>>(uri, accessToken , null);
                if (response != null && response.Result == 1 && response.Data.Count > 0)
                    data = response.Data; 
            }
            catch 
            {
            }
            return data;
        }
        public async Task<ResponseActionResult> CreateUserFromAppValid(string accessToken , TblUser entity)
        {
            var response = new ResponseActionResult();
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                response = await PostAsync<ResponseActionResult>($"{_hostSetting.Admin}{Constants.CreateUserFromAppValid}", accessToken , stringContent);
            }
            catch 
            {

            }
            return response;
        }
        public async Task<ResponseActionResult> UpdateUserFromApp(string accessToken , TblUser entity)
        {
            var response = new ResponseActionResult();
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                response = await PostAsync<ResponseActionResult>($"{_hostSetting.Admin}{Constants.UpdateUserFromApp}", accessToken , stringContent);
            }
            catch 
            {

            }
            return response;
        }
        public async Task<ResponseActionResult> ChangePassword(string accessToken , TblUser entity)
        {
            var response = new ResponseActionResult();
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                response = await PostAsync<ResponseActionResult>($"{_hostSetting.Admin}{ Constants.ChangePassword}", accessToken , stringContent);
            }
            catch 
            {

            }
            return response;
        }
    }
}
