﻿using FrontEnd.Common.Entites.Management;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models; 
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Common.Services.Department
{
    public interface IDepartmentService
    {
        public Task<List<DepartmentDatatable>> GetByConditions(string accessToken ,string para);
        public ResponseActionResult AddDepartment(TblDepartment request, string accessToken );
        public ResponseActionResult UpdateDepartment(TblDepartment request, string accessToken );
    }
    public class DepartmentService : BaseServices, IDepartmentService
    {
        readonly ServiceHost _hostSetting;
        public DepartmentService(IOptions<ServiceHost> options, IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _hostSetting = options.Value;
        }
        public async Task<List<DepartmentDatatable>> GetByConditions(string accessToken , string para)
        {
            var data = new List<DepartmentDatatable>();
            try
            {
                string uri = $"{_hostSetting.Admin}{Constants.GetDepartment}{para}";
                var response = await GetAsync<ResponseActionResult<List<DepartmentDatatable>>>(uri, accessToken , null);
                if (response != null && response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
                if(data!=null && data.Count > 0)
                {
                    data[0].TotalCount = response.Total;
                }
            }
            catch
            {
            }
            return data;
        }

        public ResponseActionResult AddDepartment(TblDepartment request, string accessToken )
        {
            var reponseData = new ResponseActionResult();
            try
            {
                string uri = $"{_hostSetting.Admin}{Constants.CreateDepartment}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<ResponseActionResult<long?>>(uri, accessToken , stringContent);
                reponseData.Result = response.Result.Result;
                reponseData.Message = response.Result.Message;
                reponseData.Data = response.Result.Data;
                return reponseData;
            }
            catch 
            {

            }
            return reponseData;
        }
        public ResponseActionResult UpdateDepartment(TblDepartment request, string accessToken )
        {
            var reponseData = new ResponseActionResult();
            try
            {
                string uri = $"{_hostSetting.Admin}{Constants.UpdateDepartment}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<ResponseActionResult<long?>>(uri, accessToken , stringContent);
                reponseData.Result = response.Result.Result;
                reponseData.Message = response.Result.Message;
                reponseData.Data = response.Result.Data;
                return reponseData;
            }
            catch 
            {
            }
            return new ResponseActionResult();
        }

    }
}
