﻿using FrontEnd.Common.Entites.Upload;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using LMS.Common.Helper;
using Microsoft.AspNetCore.Http;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Common.Services.Upload
{
    public interface IUploadServices
    {
        Task<ResponseActionResult<List<Entites.Upload.UploadLOS>>> UploadImageToLos(List<IFormFile> files);
        Task<ResponseActionResult> DeleteImageToLos(string FifleName);
    }
    public class UploadServices : IUploadServices
    {
        readonly Utils _common;
        public UploadServices()
        {
            _common = new Utils();
        }
        public async Task<ResponseActionResult<List<UploadLOS>>> UploadImageToLos(List<IFormFile> files)
        {
            ResponseActionResult<List<UploadLOS>> responseAction = new ResponseActionResult<List<UploadLOS>>();
            var request = new RestRequest(Method.POST);
            foreach (var file in files)
            {
                MemoryStream target = new MemoryStream();
                file.CopyTo(target);
                byte[] data = target.ToArray();
                request.AddFileBytes("files", data, file.FileName, file.ContentType);
            }
            var url = $"{Constants.DomainLOS}{Constants.LOS_Upload_Image}";
            // easy async support
            var client = new RestClient(url);
            try
            {
                var response = await client.ExecuteAsync<LOSBaseResponse<List<UploadLOS>>>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var objData = Newtonsoft.Json.JsonConvert.DeserializeObject<LOSBaseResponse<List<UploadLOS>>>(response.Content);
                    var lstData = objData.Data as List<UploadLOS>;
                    foreach (var item in lstData)
                    {
                        item.FullPath = this.GetUrlImageReal(item);
                    }
                    responseAction.Data = lstData;
                    // thành công thì call tiếp lên để lấy link thật
                    responseAction.SetSucces();
                }
                return responseAction;
            }
            catch
            {
                return responseAction;
            }
        }

        public async Task<ResponseActionResult> DeleteImageToLos(string FifleName)
        {
            ResponseActionResult responseAction = new FrontEnd.Common.Models.ResponseActionResult();

            string md5Key = _common.MD5(FifleName + Constants.LOS_KEY);
            string strPara = $"{FifleName}&hash={md5Key}&thumb=false";
            var url = $"{Constants.DomainLOS}{Constants.LOS_Delete_Img}{strPara}";

            var request = new RestRequest(Method.DELETE);
            request.AddHeader("Content-Type", "application/json");
            var client = new RestClient(url);
            // easy async support
            try
            {
                var response = await client.ExecuteAsync(request);
                if (response.StatusCode == HttpStatusCode.OK)
                    responseAction.Result = 1;

                return responseAction;
            }
            catch
            {
                return responseAction;
            }
        }
        private string GetUrlImageReal(UploadLOS losFile)
        {
            string md5Key = _common.MD5(losFile.FileName + Constants.LOS_KEY);
            //return $"{losFile.LocalPath}?i={losFile.FileName}&hash={md5Key}&thumb=false";
            // easy async support
            var lstImagePath = new List<string> { ".jpg", ".gif", ".png", ".jpeg", ".ico" };
            foreach (var item in lstImagePath)
            {
                if (losFile.FileName.Contains(item))
                {
                    return losFile.S3Path + "/" + md5Key + item;
                }
            }
            return $"{losFile.LocalPath}?i={losFile.FileName}&hash={md5Key}&thumb=false";
        }
    }
}
