﻿using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using LMS.Entites.Dtos.LogCompareServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Common.Services.LogCompare
{
    public interface ILogCompareServices
    {
        Task<ResponseActionResult<List<SummaryCompareModel>>> GetSummaryCompareAG(string accessToken, string strSearchDate,int mainType);
    }
    public class LogCompareServices : BaseServices, ILogCompareServices
    {
        ServiceHost _hostSetting;
        public LogCompareServices(IOptions<ServiceHost> options, IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _hostSetting = options.Value;
        }

        public async Task<ResponseActionResult<List<SummaryCompareModel>>> GetSummaryCompareAG(string accessToken, string strSearchDate, int mainType)
        {
            var response = new ResponseActionResult<List<SummaryCompareModel>>();
            try
            {
                string uri = $"{_hostSetting.Admin}{Constants.GetSummaryCompare}?StrSearchDate={strSearchDate}&type={mainType}";
                response = await GetAsync<ResponseActionResult<List<SummaryCompareModel>>>(uri, accessToken, "");
                return response;
            }
            catch
            {
                response.SetError();
            }
            return response;
        }
    }
}
