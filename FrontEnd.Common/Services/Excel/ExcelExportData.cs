﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Services.Excel
{
    public interface IExcelExportData
    {
        int TypeReport { get; }
        byte[] GetDataExcelReport(string lstResponse);
    }

    public class ExcelExportDataLoan : IExcelExportData
    {

        protected IExcelService _excelService;
        private readonly Common.Helpers.Common _common;
        public ExcelExportDataLoan(IExcelService excelService)
        {
            _excelService = excelService;
            _common = new Helpers.Common();
        }
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.Loan;

        public byte[] GetDataExcelReport(string lstResponse)
        {
            var lstDataExcel = _common.ConvertJSonToObject<List<Models.ThnLoan.ResponseGetByTraceIDModel>>(lstResponse);
            List<Common.Entites.ThnLoan.ExcelGetByTraceID> lstData = new List<Common.Entites.ThnLoan.ExcelGetByTraceID>();
            if (lstDataExcel != null && lstDataExcel.Count > 0)
            {
                foreach (var item in lstDataExcel)
                {
                    lstData.Add(new Common.Entites.ThnLoan.ExcelGetByTraceID()
                    {
                        LoanID = item.LoanID,
                        LoanContactCode = item.LoanContactCode,
                        CustomerName = item.CustomerName,
                        CustomerDistrictName = item.CustomerDistrictName,
                        CustomerCityName = item.CustomerCityName,
                        POSBOM = item.POSBOM,
                        DPDBOM = item.DPDBOM,
                        AssgineeNameFirst = item.AssgineeNameFirst,
                        AssigneeNameSecond = item.AssigneeNameSecond,
                        AssigneeNameLeader = item.AssigneeNameLeader,
                        AssigneeNameDepartment = item.AssigneeNameDepartment,
                        LoanTotalMoneyReceipt = item.LoanTotalMoneyReceipt.ToString("###,0"),
                        DPDCurrent = item.DPDCurrent,
                        POSCurrent = item.POSCurrent.ToString("###,0"),
                        BucketBOM = item.BucketBOM,
                        BucketCurrent = item.BucketCurrent,
                        LoanProductName = item.LoanProductName,
                        LoanLatestComment = item.LoanLatestComment,
                        AssigneeNameLatestComment = item.AssigneeNameLatestComment,
                        CommentReasonCode = item.CommentReasonCode,
                        CommentContent = item.CommentContent,
                        LoanBadDebtYear = item.LoanBadDebtYear,
                        LoanInsuranceStatusName = item.LoanInsuranceStatusName,
                        LoanInsurancePartnerName = item.LoanInsurancePartnerName,
                        LoanDomainName = item.LoanDomainName,
                        CustomerID = item.CustomerID,
                        LoanFromDate = item.LoanFromDate,
                        LoanToDate = item.LoanToDate,
                        LoanFinishedDate = item.LoanFinishedDate,
                        CustomerBirthDate = item.CustomerBirthDate,
                        CustomerAddress = item.CustomerAddress,
                        LoanTotalMoneyCurrent = item.LoanTotalMoneyCurrent.ToString("###,0"),
                        LoanRateTypeName = item.LoanRateTypeName,
                        LoanNextDate = item.LoanNextDate,
                        LoanFequency = item.LoanFequency,
                        LoanInterestClosed = item.LoanInterestClosed.ToString("###,0"),
                        LoanConsultantFeeClosed = item.LoanConsultantFeeClosed.ToString("###,0"),
                        LoanMoneyFineOriginalClosed = item.LoanMoneyFineOriginalClosed.ToString("###,0"),
                        LoanMoneyFineLateClosed = item.LoanMoneyFineLateClosed.ToString("###,0"),
                        CustomerTotalMoney = item.CustomerTotalMoney.ToString("###,0"),
                        LoanTotalMoneyOriginalReceipt = item.LoanTotalMoneyOriginalReceipt.ToString("###,0"),
                        LoanTotalMoneyInterestReceipt = item.LoanTotalMoneyInterestReceipt.ToString("###,0"),
                        LoanTotalMoneyConsultantReceipt = item.LoanTotalMoneyConsultantReceipt.ToString("###,0"),
                        CustomerEpayAccountName = item.CustomerEpayAccountName,
                        LoanIsLocate = item.LoanIsLocate == true ? "Có" : "Không",
                        LoanShopConsultantName = item.LoanShopConsultantName,
                        LoanBKS = item.LoanBKS,
                        LoanTopup = item.LoanTopup,
                        LoanPreparationDate = item.LoanPreparationDate,
                        LoanPreparationMoney = item.LoanPreparationMoney.ToString("###,0"),
                        CustomerPayTypeName = item.CustomerPayTypeName,
                        LenderCode = item.LenderCode,
                    });
                }
            }
            return _excelService.ExportExcelBase<Common.Entites.ThnLoan.ExcelGetByTraceID>(lstData);
        }

    }

    public class ExcelHistoryCustomerTopup : IExcelExportData
    {
        protected IExcelService _excelService;
        private readonly Common.Helpers.Common _common;
        public ExcelHistoryCustomerTopup(IExcelService excelService)
        {
            _excelService = excelService;
            _common = new Helpers.Common();
        }
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.HistoryCustomerTopup;

        public byte[] GetDataExcelReport(string lstResponse)
        {
            var lstDataExcel = _common.ConvertJSonToObject<List<Common.Entites.ThnLoan.ExcelTransactionBankCard>>(lstResponse);
            return _excelService.ExportExcelBase<Common.Entites.ThnLoan.ExcelTransactionBankCard>(lstDataExcel);
        }
    }

    public class ExcelHistoryTransactionLoanByCustomer : IExcelExportData
    {
        protected IExcelService _excelService;
        private readonly Common.Helpers.Common _common;
        public ExcelHistoryTransactionLoanByCustomer(IExcelService excelService)
        {
            _excelService = excelService;
            _common = new Helpers.Common();
        }
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.HistoryTransactionLoanByCustomer;

        public byte[] GetDataExcelReport(string lstResponse)
        {
            var lstAPI = _common.ConvertJSonToObject<Entites.Excel.HistoryTransactionLoanCustomerExcel>(lstResponse);
            var lstDataExcel = _common.ConvertParentToChild<List<Entites.ThnLoan.ExcelPaymentCustomer>, List<Entites.Excel.HistoryTransactionLoanCustomerModel>>(lstAPI.Rows);
            lstDataExcel.Add(new Entites.ThnLoan.ExcelPaymentCustomer()
            {
                LoanContractCode = "Tổng",
                MoneyConsultant = lstAPI.TotalMoneyConsultant,
                MoneyFineLateLender = lstAPI.TotalMoneyFineLateLender,
                MoneyFineLateTima = lstAPI.TotalMoneyFineLateTima,
                MoneyFineOrginalLender = lstAPI.TotalMoneyFineOrginalLender,
                MoneyFineOrginalTima = lstAPI.TotalMoneyFineOrginalTima,
                MoneyInterest = lstAPI.TotalMoneyInterest,
                MoneyOriginal = lstAPI.TotalMoneyOriginal,
                MoneyService = lstAPI.TotalMoneyService,
            });
            return _excelService.ExportExcelBase<Common.Entites.ThnLoan.ExcelPaymentCustomer>(lstDataExcel);
        }
    }

    public class ExcelReportLoanDebtType : IExcelExportData
    {
        protected IExcelService _excelService;
        private readonly Common.Helpers.Common _common;
        public ExcelReportLoanDebtType(IExcelService excelService)
        {
            _excelService = excelService;
            _common = new Helpers.Common();
        }
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.ReportLoanDebtType;

        public byte[] GetDataExcelReport(string lstResponse)
        {
            var lstDataExcel = _common.ConvertJSonToObject<List<Common.Entites.ThnLoan.HistoryTrackingLoanInMonth>>(lstResponse);
            return _excelService.ExportExcelBase<Common.Entites.ThnLoan.HistoryTrackingLoanInMonth>(lstDataExcel);
        }
    }

    public class ExcelReportLoanDebtDaily : IExcelExportData
    {
        protected IExcelService _excelService;
        private readonly Common.Helpers.Common _common;
        public ExcelReportLoanDebtDaily(IExcelService excelService)
        {
            _excelService = excelService;
            _common = new Helpers.Common();
        }
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.ReportLoanDebtDaily;

        public byte[] GetDataExcelReport(string lstResponse)
        {
            var lstDataExcel = _common.ConvertJSonToObject<List<Common.Entites.ThnLoan.ExcelReportLoanDebtDailyModel>>(lstResponse);
            return _excelService.ExportExcelBase<Common.Entites.ThnLoan.ExcelReportLoanDebtDailyModel>(lstDataExcel);
        }
    }
    public class ExcelReportDayPlan : IExcelExportData
    {
        protected IExcelService _excelService;
        private readonly Common.Helpers.Common _common;
        public ExcelReportDayPlan(IExcelService excelService)
        {
            _excelService = excelService;
            _common = new Helpers.Common();
        }
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.ReportDayPlan;

        public byte[] GetDataExcelReport(string lstResponse)
        {
            var lstDataExcel = _common.ConvertJSonToObject<List<Common.Entites.Report.ReportDayPlanItem>>(lstResponse);
            return _excelService.ExportExcelBase<Common.Entites.Report.ReportDayPlanItem>(lstDataExcel);
        }
    }
    public class ExcelHistoryInteraction : IExcelExportData
    {
        protected IExcelService _excelService;
        private readonly Common.Helpers.Common _common;
        public ExcelHistoryInteraction(IExcelService excelService)
        {
            _excelService = excelService;
            _common = new Helpers.Common();
        }
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.ExcelHistoryInteraction;

        public byte[] GetDataExcelReport(string lstResponse)
        {
            var lstDataExcel = _common.ConvertJSonToObject<List<Common.Models.ThnLoan.ExcelInteractionResponse>>(lstResponse);
            return _excelService.ExportExcelBase<Common.Models.ThnLoan.ExcelInteractionResponse>(lstDataExcel);
        }
    }

    public class ExcelLenderDeposit : IExcelExportData
    {
        protected IExcelService _excelService;
        private readonly Common.Helpers.Common _common;
        public ExcelLenderDeposit(IExcelService excelService)
        {
            _excelService = excelService;
            _common = new Helpers.Common();
        }
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.Lender_Deposit;

        public byte[] GetDataExcelReport(string lstResponse)
        {
            var lstDataExcel = _common.ConvertJSonToObject<List<LMS.Entites.Dtos.LenderService.Excel.LenderDepositExcel>>(lstResponse);
            return _excelService.ExportExcelBase<LMS.Entites.Dtos.LenderService.Excel.LenderDepositExcel>(lstDataExcel);
        }
    }

    public class ExcelLenderBM01 : IExcelExportData
    {
        protected IExcelService _excelService;
        private readonly Common.Helpers.Common _common;
        public ExcelLenderBM01(IExcelService excelService)
        {
            _excelService = excelService;
            _common = new Helpers.Common();
        }
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.Lender_BM01;

        public byte[] GetDataExcelReport(string lstResponse)
        {
            var lstDataExcel = _common.ConvertJSonToObject<LMS.Entites.Dtos.LenderService.Excel.BM01Excel>(lstResponse);
            return _excelService.ExportBM01(lstDataExcel);
        }
    }
}
