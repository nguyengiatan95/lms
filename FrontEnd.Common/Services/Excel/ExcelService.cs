﻿using FrontEnd.Common.Entites.Insurance;
using FrontEnd.Common.Helpers;
using LMS.Entites.Dtos.LenderService.Excel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;

namespace FrontEnd.Common.Services.Excel
{
    public interface IExcelService
    {
        byte[] ExcelTemplateChangeStatus(List<IndemnifyDatatable> lstData);

        byte[] ExportExcelBase<T>(List<T> lstData) where T : class;
        byte[] ExportBM01(BM01Excel data);


    }
    public class ExcelService : BaseServices, IExcelService
    {
        private readonly ServiceHost _hostSetting;
        private readonly IHostingEnvironment _environment;
        public ExcelService(IOptions<ServiceHost> options, IHostingEnvironment hostingEnvironment, IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _hostSetting = options.Value;
            _environment = hostingEnvironment;
        }

        public byte[] ExportExcelBase<T>(List<T> lstData) where T : class
        {
            List<string> lstHeader = new List<string>();
            //lstHeader.Add("STT");
            Type type = typeof(T);
            string nameSheet = string.Empty;
            object[] attributes = type.GetCustomAttributes(true);
            foreach (var item in attributes)
            {
                DescriptionAttribute da = item as DescriptionAttribute;
                if (da != null)
                    nameSheet = da.Description;
            }

            // lấy danh sách member
            var properties = type.GetProperties();
            foreach (var item in properties)
            {
                object[] descriptionAttrs = item.GetCustomAttributes(typeof(DescriptionAttribute), false);
                DescriptionAttribute description = (DescriptionAttribute)descriptionAttrs[0];
                lstHeader.Add(description.Description);
            }
            List<int> lstWidth = new List<int>();
            List<ExcelHorizontalAlignment> lstAlign = new List<ExcelHorizontalAlignment>();
            List<System.Drawing.Color> lstColor = new List<System.Drawing.Color>();

            for (int i = 0; i < lstHeader.Count; i++)
            {
                lstWidth.Add(20);
                lstAlign.Add(ExcelHorizontalAlignment.Left);
                lstColor.Add(Color.Black);
            }

            ExcelPackage package = new ExcelPackage();
            var ws = package.Workbook.Worksheets.Add(nameSheet);
            // Set Font
            ws.Cells.Style.Font.Name = "Calibri";
            ws.Cells.Style.Font.Size = 11;

            // Format All Cells
            ws.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            // Format All Column
            for (int i = 1; i <= lstHeader.Count; i++)
            {
                ws.Column(i).Width = lstWidth[i - 1];
                ws.Column(i).Style.HorizontalAlignment = lstAlign[i - 1];
                ws.Column(i).Style.Font.Color.SetColor(lstColor[i - 1]);
            }
            int iPosHeader = 1;
            // mapping title header
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeader.Count].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeader.Count].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(77, 119, 204));
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeader.Count].Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(255, 255, 255));
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeader.Count].Style.Font.Bold = true;
            ws.Row(iPosHeader).Height = 16;
            for (int i = 1; i <= lstHeader.Count; i++)
            {
                ws.Cells[iPosHeader, i].Value = lstHeader[i - 1];
            }

            // Set Value
            iPosHeader++;
            if (lstData.Count > 0)
            {

                for (int i = 0; i < lstData.Count; i++)
                {
                    try
                    {
                        int j = 1;
                        // ws.Cells[iPosHeader, 1].Value = i + 1;
                        foreach (PropertyInfo prop in properties)
                        {
                            object propValue = prop.GetValue(lstData[i], null);
                            if (propValue != null)
                            {
                                // Do something with propValue
                                ws.Cells[iPosHeader, j].Value = propValue == null ? "" : propValue;
                                double number;
                                var propValueType = propValue.GetType();
                                if (propValue != null && Double.TryParse(propValue.ToString(), out number))
                                {
                                    if (propValue.ToString().Contains("."))
                                    {
                                        ws.Cells[iPosHeader, j].Style.Numberformat.Format = "#,##0.000";
                                    }
                                    else
                                    {
                                        ws.Cells[iPosHeader, j].Style.Numberformat.Format = "#,##0";
                                    }
                                }
                                if (prop.PropertyType.Name == "DateTime")
                                {
                                    ws.Cells[iPosHeader, j].Style.Numberformat.Format = "d/m/yyyy hh:mm";
                                }
                            }
                            j++;
                        }
                        iPosHeader++;
                    }
                    catch
                    {

                    }
                }

            }
            // save file
            var fileContents = package.GetAsByteArray();
            return fileContents;

        }

        public byte[] ExcelTemplateChangeStatus(List<IndemnifyDatatable> lstData)
        {
            List<string> lstHeader = new List<string> { "STT", "LoanID", "Mã TC", "Trạng thái", "Ghi chú" };


            List<string> lstHeaderTableIntro = new List<string> { "Mã trạng thái", "Trạng thái tương ứng" };

            List<int> lstWidth = new List<int>();
            List<ExcelHorizontalAlignment> lstAlign = new List<ExcelHorizontalAlignment>();
            List<System.Drawing.Color> lstColor = new List<System.Drawing.Color>();

            for (int i = 0; i < lstHeader.Count; i++)
            {
                lstWidth.Add(20);
                lstAlign.Add(ExcelHorizontalAlignment.Left);
                lstColor.Add(Color.Black);
            }

            ExcelPackage package = new ExcelPackage();
            var ws = package.Workbook.Worksheets.Add("Sheet1");
            // Set Font
            ws.Cells.Style.Font.Name = "Calibri";
            ws.Cells.Style.Font.Size = 11;

            // Format All Cells
            ws.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            // Format All Column
            for (int i = 1; i <= lstHeader.Count; i++)
            {
                ws.Column(i).Width = lstWidth[i - 1];
                ws.Column(i).Style.HorizontalAlignment = lstAlign[i - 1];
                ws.Column(i).Style.Font.Color.SetColor(lstColor[i - 1]);
            }
            int iPosHeader = 1;
            // mapping title header
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeader.Count].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeader.Count].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(77, 119, 204));
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeader.Count].Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(255, 255, 255));
            ws.Cells[iPosHeader, 1, iPosHeader, lstHeader.Count].Style.Font.Bold = true;
            ws.Row(iPosHeader).Height = 16;
            for (int i = 1; i <= lstHeader.Count; i++)
            {

                ws.Cells[iPosHeader, i].Value = lstHeader[i - 1];
            }
            // Set Value
            iPosHeader++;
            if (lstData.Count > 0)
            {
                for (int i = 0; i < lstData.Count; i++)
                {
                    ws.Cells[iPosHeader, 1].Value = i + 1;
                    ws.Cells[iPosHeader, 2].Value = lstData[i].LoanID;
                    ws.Cells[iPosHeader, 3].Value = lstData[i].ContractCode;
                    ws.Cells[iPosHeader, 4].Value = lstData[i].StatusInsurance;
                    ws.Cells[iPosHeader, 5].Value = "";
                    iPosHeader++;
                }
            }
            iPosHeader = 8;
            ws.Row(iPosHeader).Height = 16;
            for (int i = 1; i <= lstHeaderTableIntro.Count; i++)
            {
                ws.Column(iPosHeader).Width = 40;
                ws.Cells[1, iPosHeader].Value = lstHeaderTableIntro[i - 1];
                ws.Cells[1, iPosHeader].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[1, iPosHeader].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Brown);
                ws.Cells[1, iPosHeader].Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(255, 255, 255));
                ws.Cells[1, iPosHeader].Style.Font.Bold = true;
                ws.Cells[1, iPosHeader].Style.Border.BorderAround(ExcelBorderStyle.Medium);
                iPosHeader++;
            }
            ws.Cells[2, 8].Value = "-1";
            ws.Cells[2, 9].Value = "Rút hồ sơ về";
            ws.Cells[3, 8].Value = "0";
            ws.Cells[3, 9].Value = "Chưa gửi bảo hiểm";
            ws.Cells[4, 8].Value = "1";
            ws.Cells[4, 9].Value = "Đã gửi bảo hiểm";
            ws.Cells[5, 8].Value = "Nhập trạng thái là mã số. Xóa hết những đơn bạn không thay đổi nếu cần";
            ws.Cells[2, 8].Style.Border.BorderAround(ExcelBorderStyle.Medium);
            ws.Cells[2, 9].Style.Border.BorderAround(ExcelBorderStyle.Medium);
            ws.Cells[3, 8].Style.Border.BorderAround(ExcelBorderStyle.Medium);
            ws.Cells[3, 9].Style.Border.BorderAround(ExcelBorderStyle.Medium);
            ws.Cells[4, 8].Style.Border.BorderAround(ExcelBorderStyle.Medium);
            ws.Cells[4, 9].Style.Border.BorderAround(ExcelBorderStyle.Medium);
            // save file
            var fileContents = package.GetAsByteArray();
            return fileContents;
        }

        public byte[] ExportBM01(BM01Excel data)
        {
            string fileTemplate = Path.Combine(_environment.WebRootPath, "Files/BM01.xlsx");
            FileInfo existingFile = new FileInfo(fileTemplate);
            using (ExcelPackage package = new ExcelPackage(existingFile))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                int row = 13;
                worksheet.InsertRow(row, data.LstDeposit.Count);
                long TotalMoneyCurrent = 0;
                long InterestMoney = 0;
                long TotalMoneyDeposit = 0;
                long InterestAfterTax = 0;
                long TotalMoneyDepositAfterTax = 0;
                long TotalMoney = 0;

                // Bind dữ liệu
                int i = 1;
                foreach (var item in data.LstDeposit)
                {
                    TotalMoneyCurrent += item.TotalMoneyCurrent;
                    InterestMoney += item.InterestMoney;
                    TotalMoneyDeposit += item.TotalMoneyDeposit;
                    InterestAfterTax += item.InterestAfterTax;
                    TotalMoneyDepositAfterTax += item.TotalMoneyDepositAfterTax;
                    worksheet.Cells[row, 1].Value = i++;
                    worksheet.Cells[row, 2].Value = item.ContractCode;
                    worksheet.Cells[row, 3].Value = item.CustomerName;
                    worksheet.Cells[row, 4].Value = item.LenderCode;

                    worksheet.Cells[row, 5].Value = item.NextDate;
                    worksheet.Cells[row, 6].Value = item.EndDaySchedule;



                    worksheet.Cells[row, 7].Value = item.TotalMoneyCurrent;
                    worksheet.Cells[row, 8].Value = item.InterestMoney;
                    worksheet.Cells[row, 9].Value = item.TotalMoneyDeposit;
                    worksheet.Cells[row, 10].Value = item.InterestAfterTax;
                    worksheet.Cells[row, 11].Value = item.TotalMoneyDepositAfterTax;

                    worksheet.Cells[row, 5].Style.Numberformat.Format = "d/m/yyyy";
                    worksheet.Cells[row, 6].Style.Numberformat.Format = "d/m/yyyy";

                    worksheet.Cells[row, 7].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[row, 8].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[row, 9].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[row, 10].Style.Numberformat.Format = "#,##0";
                    worksheet.Cells[row, 11].Style.Numberformat.Format = "#,##0";
                    for (int j = 1; j < 12; j++)
                    {
                        worksheet.Cells[row, j].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[row, j].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[row, j].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[row, j].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                    row++;
                }

                //Tổng table 1
                worksheet.Cells[row, 7].Value = TotalMoneyCurrent;
                worksheet.Cells[row, 8].Value = InterestMoney;
                worksheet.Cells[row, 9].Value = TotalMoneyDeposit;
                worksheet.Cells[row, 10].Value = InterestAfterTax;
                worksheet.Cells[row, 11].Value = TotalMoneyDepositAfterTax;

                worksheet.Cells[row, 7].Style.Numberformat.Format = "#,##0";
                worksheet.Cells[row, 8].Style.Numberformat.Format = "#,##0";
                worksheet.Cells[row, 9].Style.Numberformat.Format = "#,##0";
                worksheet.Cells[row, 10].Style.Numberformat.Format = "#,##0";
                worksheet.Cells[row, 11].Style.Numberformat.Format = "#,##0";
                row++;

                row = row + 5;
                i = 1;
                ///map table 2
                worksheet.InsertRow(row, data.LenderInfos.Count);
                foreach (var item in data.LenderInfos)
                {
                    TotalMoney += item.TotalMoney;
                    worksheet.Cells[row, 1].Value = i++;
                    worksheet.Cells[row, 2].Value = item.LenderCode;
                    worksheet.Cells[row, 3].Value = item.FullName;
                    worksheet.Cells[row, 4].Value = item.TotalMoney;
                    worksheet.Cells[row, 5].Value = item.Represent;
                    worksheet.Cells[row, 6].Value = item.NumberBankCard;
                    worksheet.Cells[row, 7].Value = item.BankCode;

                    worksheet.Cells[row, 4].Style.Numberformat.Format = "#,##0";
                    for (int j = 1; j < 8; j++)
                    {
                        worksheet.Cells[row, j].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[row, j].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[row, j].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[row, j].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                    row++;
                }
                worksheet.Cells[row, 4].Value = TotalMoney;
                worksheet.Cells[row, 4].Style.Numberformat.Format = "#,##0";
                var fileContents = package.GetAsByteArray();
                return fileContents;
            }

        }
    }
}
