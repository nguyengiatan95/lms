﻿using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using LMS.Entites.Dtos.LenderService;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Common.Services.Lender
{
    public interface ILenderService
    {
        public Task<List<LenderSearchModel>> GetLendersAndShop(string accessToken, int type);
        Task<List<LMS.Entites.Dtos.LenderService.LenderInfoModel>> GetLenderList(string accessToken, string generalSearch, int status, int pageIndex, int pageSize, int isHub, int isverified);
    }
    public class LenderService : BaseServices, ILenderService
    {
        readonly ServiceHost _hostSetting;
        public LenderService(IOptions<ServiceHost> options, IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _hostSetting = options.Value;
        }

        public async Task<List<LenderSearchModel>> GetLendersAndShop(string accessToken, int type)
        {
            var data = new List<LenderSearchModel>();
            try
            {
                string uri = $"{_hostSetting.Lender}{Constants.GetLendersAndShop}{type}";
                var response = await GetAsync<ResponseActionResult<List<LenderSearchModel>>>(uri, accessToken, null);
                if (response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
                if (data != null && data.Count > 0)
                {
                    data = response.Data;
                }
            }
            catch
            {

            }
            return data;
        }

        public async Task<List<LMS.Entites.Dtos.LenderService.LenderInfoModel>> GetLenderList(string accessToken, string generalSearch, int status,
            int pageIndex, int pageSize, int isHub, int isverified)
        {
            var data = new List<LMS.Entites.Dtos.LenderService.LenderInfoModel>();
            try
            {
                string uri = $"{_hostSetting.Lender}{Constants.GetLenderList}";
                var request = new
                {
                    GeneralSearch = generalSearch,
                    Status = status,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    IsHub = isHub,
                    IsVerified = isverified,
                };
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<ResponseActionResult<List<LMS.Entites.Dtos.LenderService.LenderInfoModel>>>(uri, accessToken, stringContent);
                if (response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
                
                if (data != null && data.Count > 0)
                {
                    data = response.Data;
                    data[0].TotalCount = response.Total;
                }
            }
            catch
            {

            }
            return data;
        }
    }
}
