﻿using FrontEnd.Common.Entites.Insurance;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Models.Insurance;
using FrontEnd.Common.Models.Insurrance;
using FrontEnd.Common.Models.Report;
using FrontEnd.Common.Models.ThnLoan;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Common.Services.Insurance
{
    public interface IInsuranceServices
    {
        public Task<List<InsuranceDatatable>> GetByConditions(string accessToken, InsuranceModel request);
        public Task<List<ReportVatResponse>> GetByConditionsReportVat(string accessToken, ReportVatModel request);
        public Task<List<ReportVatResponse>> GetByConditionsReportFineInterestLate(string accessToken, ReportFineInterestLate request);
        public Task<List<ReportVatHubResponse>> GetByConditionsReportVatHub(string accessToken, ReportVatHub request);
        public Task<List<ReportVatHubResponse>> GetByConditionsReportPenaltyFeeVatHub(string accessToken, ReportPenaltyFeeVatHub request);
        public Task<List<ReportVatHubResponse>> GetByConditionsReportVatGCash(string accessToken, ReportVatGCash request);
        public Task<List<ReportVatHubResponse>> GetByConditionsReportVatHUbWithGCash(string accessToken, ReportVatHUbWithGCash request);
        public Task<List<ReportVatResponse>> GetByConditionsReportLender(string accessToken, ReportLender request);

        public Task<List<IndemnifyDatatable>> GetByConditionsIndemnify(string accessToken, IndemnifyModel request);
        public Task<ResponseActionResult<object>> GetByTraceID(string accessToken, GetByTraceIDModel request);
        public Task<List<ReportVatResponse>> GetByConditionsReportMoneyInterestLender(string accessToken, ReportMoneyInterestLender request);
        public Task<List<ResponseInteraction>> GetByConditionsInteraction(string accessToken, RequestInteractionModel request);
        public Task<ResponseActionResult<object>> GetByTraceIDTHN(string accessToken, GetByTraceIDModel request);
    }
    public class InsuranceService : BaseServices, IInsuranceServices
    {
        readonly ServiceHost _hostSetting;
        readonly ILogger<InsuranceService> _logger;
        public InsuranceService(IOptions<ServiceHost> options, IConfiguration configuration,
            IHttpClientFactory clientFactory,
            ILogger<InsuranceService> logger) 
            : base(configuration, clientFactory)
        {
            _hostSetting = options.Value;
            _logger = logger;
        }
        public async Task<List<InsuranceDatatable>> GetByConditions(string accessToken, InsuranceModel request)
        {
            var data = new List<InsuranceDatatable>();
            try
            {
                string uri = $"{_hostSetting.Insurance}{Constants.ReportInsurance}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<ResponseActionResult<List<InsuranceDatatable>>>(uri, accessToken, stringContent);
                if (response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
                if (data != null && data.Count > 0)
                {
                    data[0].TotalCount = response.Total;
                }
            }
            catch(Exception ex)
            {
                _logger.LogError($"GetByConditions|ex={ex.Message}-{ex.StackTrace}");
            }
            return data;
        }
        public async Task<List<IndemnifyDatatable>> GetByConditionsIndemnify(string accessToken, IndemnifyModel request)
        {
            var data = new List<IndemnifyDatatable>();
            try
            {
                string uri = $"{_hostSetting.Lender}{Constants.Indemnify}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<ResponseActionResult<List<IndemnifyDatatable>>>(uri, accessToken, stringContent);
                if (response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
                if (data != null && data.Count > 0)
                {
                    data[0].TotalCount = response.Total;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetByConditionsIndemnify|ex={ex.Message}-{ex.StackTrace}");
            }
            return data;
        }
        public async Task<List<ReportVatResponse>> GetByConditionsReportVat(string accessToken, ReportVatModel request)
        {
            var data = new List<ReportVatResponse>();
            try
            {
                string uri = $"{_hostSetting.Loan}{Constants.ReportVat}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<ResponseActionResult<List<ReportVatResponse>>>(uri, accessToken, stringContent);
                if (response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
                if (data != null && data.Count > 0)
                {
                    data[0].TotalCount = response.Total;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetByConditionsReportVat|ex={ex.Message}-{ex.StackTrace}");
            }
            return data;
        }
        public async Task<List<ReportVatResponse>> GetByConditionsReportFineInterestLate(string accessToken, ReportFineInterestLate request)
        {
            var data = new List<ReportVatResponse>();
            try
            {
                string uri = $"{_hostSetting.Loan}{Constants.ReportFineInterestLate}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<ResponseActionResult<List<ReportVatResponse>>>(uri, accessToken, stringContent);
                if (response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
                if (data != null && data.Count > 0)
                {
                    data[0].TotalCount = response.Total;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetByConditionsReportFineInterestLate|ex={ex.Message}-{ex.StackTrace}");
            }
            return data;
        }
        public async Task<List<ReportVatHubResponse>> GetByConditionsReportVatHub(string accessToken, ReportVatHub request)
        {
            var data = new List<ReportVatHubResponse>();
            try
            {
                string uri = $"{_hostSetting.Loan}{Constants.ReportVatHub}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<ResponseActionResult<List<ReportVatHubResponse>>>(uri, accessToken, stringContent);
                if (response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
                if (data != null && data.Count > 0)
                {
                    data[0].TotalCount = response.Total;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetByConditionsReportVatHub|ex={ex.Message}-{ex.StackTrace}");
            }
            return data;
        }
        public async Task<List<ReportVatHubResponse>> GetByConditionsReportPenaltyFeeVatHub(string accessToken, ReportPenaltyFeeVatHub request)
        {
            var data = new List<ReportVatHubResponse>();
            try
            {
                string uri = $"{_hostSetting.Loan}{Constants.ReportPenaltyFeeVatHub}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<ResponseActionResult<List<ReportVatHubResponse>>>(uri, accessToken, stringContent);
                if (response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
                if (data != null && data.Count > 0)
                {
                    data[0].TotalCount = response.Total;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetByConditionsReportPenaltyFeeVatHub|ex={ex.Message}-{ex.StackTrace}");
            }
            return data;
        }
        public async Task<List<ReportVatHubResponse>> GetByConditionsReportVatGCash(string accessToken, ReportVatGCash request)
        {
            var data = new List<ReportVatHubResponse>();
            try
            {
                string uri = $"{_hostSetting.Loan}{Constants.ReportVatGCash}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<ResponseActionResult<List<ReportVatHubResponse>>>(uri, accessToken, stringContent);
                if (response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
                if (data != null && data.Count > 0)
                {
                    data[0].TotalCount = response.Total;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetByConditionsReportVatGCash|ex={ex.Message}-{ex.StackTrace}");
            }
            return data;
        }
        public async Task<List<ReportVatHubResponse>> GetByConditionsReportVatHUbWithGCash(string accessToken, ReportVatHUbWithGCash request)
        {
            var data = new List<ReportVatHubResponse>();
            try
            {
                string uri = $"{_hostSetting.Loan}{Constants.ReportVatHUbWithGCash}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<ResponseActionResult<List<ReportVatHubResponse>>>(uri, accessToken, stringContent);
                if (response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
                if (data != null && data.Count > 0)
                {
                    data[0].TotalCount = response.Total;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetByConditionsReportVatHUbWithGCash|ex={ex.Message}-{ex.StackTrace}");
            }
            return data;
        }
        public async Task<List<ReportVatResponse>> GetByConditionsReportLender(string accessToken, ReportLender request)
        {
            var data = new List<ReportVatResponse>();
            try
            {
                string uri = $"{_hostSetting.Loan}{Constants.ReportVatLender}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<ResponseActionResult<List<ReportVatResponse>>>(uri, accessToken, stringContent);
                if (response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
                if (data != null && data.Count > 0)
                {
                    data[0].TotalCount = response.Total;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetByConditionsReportLender|ex={ex.Message}-{ex.StackTrace}");
            }
            return data;
        }
        public async Task<ResponseActionResult<object>> GetByTraceID(string accessToken, GetByTraceIDModel request)
        {
            var data = new ResponseActionResult<object>();
            try
            {
                string uri = $"{_hostSetting.Lender}{Constants.GetByTraceID}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<ResponseActionResult<object>>(uri, accessToken, stringContent);
                if (response.Result == 1)
                    data = response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetByTraceID|ex={ex.Message}-{ex.StackTrace}");
            }
            return data;
        }
        public async Task<ResponseActionResult<object>> GetByTraceIDTHN(string accessToken, GetByTraceIDModel request)
        {
            var data = new ResponseActionResult<object>();
            try
            {
                string uri = $"{_hostSetting.Thn}{Constants.GetByTraceID}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<ResponseActionResult<object>>(uri, accessToken, stringContent);
                if (response.Result == 1)
                    data = response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetByTraceIDTHN|ex={ex.Message}-{ex.StackTrace}");
            }
            return data;
        }
        public async Task<List<ReportVatResponse>> GetByConditionsReportMoneyInterestLender(string accessToken, ReportMoneyInterestLender request)
        {
            var data = new List<ReportVatResponse>();
            try
            {
                string uri = $"{_hostSetting.Loan}{Constants.ReportMoneyInterestLender}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<ResponseActionResult<List<ReportVatResponse>>>(uri, accessToken, stringContent);
                if (response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
                if (data != null && data.Count > 0)
                {
                    data[0].TotalCount = response.Total;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetByConditionsReportMoneyInterestLender|ex={ex.Message}-{ex.StackTrace}");
            }
            return data;
        }
        public async Task<List<ResponseInteraction>> GetByConditionsInteraction(string accessToken, RequestInteractionModel request)
        {
            var data = new List<ResponseInteraction>();
            try
            {
                string uri = $"{_hostSetting.Thn}{Constants.GetLstHistoryInteractDebtPrompted}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<ResponseActionResult<List<ResponseInteraction>>>(uri, accessToken, stringContent);
                if (response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetByConditionsInteraction|ex={ex.Message}-{ex.StackTrace}");
            }
            return data;
        }

    }
}
