﻿using FrontEnd.Common.Entites.Management;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Common.Services.Group
{ 
    public interface IGroupService
    {
        public TblGroup GetGroupByID(string accessToken , int id);
        public Task<List<TblGroup>> GetGroup(string accessToken , string generalSearch, int status, int pageIndex, int pageSize);
        public Task<ResponseActionResult> UpdateGroupFromApp(string accessToken , TblGroup entity);
        public Task<ResponseActionResult> CreateGroupFromAppValid(string accessToken , TblGroup entity);
    }
    public class GroupService : BaseServices, IGroupService
    {
        readonly ServiceHost _hostSetting;
        public GroupService(IOptions<ServiceHost> options, IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _hostSetting = options.Value;
        }
        public TblGroup GetGroupByID(string accessToken , int id)
        {
            var data = new TblGroup();
            try
            {
                var response = GetAsync<ResponseActionResult<TblGroup>>($"{_hostSetting.Admin}{Constants.GetGroupByID}{id}", accessToken , null);
                if (response.Result.Result == 1 && response.Result.Data != null)
                    data = response.Result.Data;
            }
            catch
            {

            }
            return data;
        } 
        public async Task<List<TblGroup>> GetGroup(string accessToken , string generalSearch, int status, int pageIndex, int pageSize)
        {
            var data = new List<TblGroup>();
            try
            {
                string uri = $"{_hostSetting.Admin}{Constants.GetGroup}?GeneralSearch={generalSearch}&Status={status}&PageIndex={pageIndex}&PageSize={pageSize}";
                var response = await GetAsync<ResponseActionResult<List<TblGroup>>>(uri, accessToken , null);
                if (response != null && response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
                if (data != null && data.Count > 0)
                {
                    data = response.Data;
                    data[0].TotalCount = response.Total;
                }
            }
            catch
            {

            }
            return data;
        }
        public async Task<ResponseActionResult> UpdateGroupFromApp(string accessToken , TblGroup entity)
        {
            var response = new ResponseActionResult();
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                response = await PostAsync<ResponseActionResult>($"{_hostSetting.Admin}{Constants.UpdateGroupFromApp}", accessToken , stringContent);
            }
            catch
            {

            }
            return response;
        }
        public async Task<ResponseActionResult> CreateGroupFromAppValid(string accessToken , TblGroup entity)
        {
            var response = new ResponseActionResult();
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                response = await PostAsync<ResponseActionResult>($"{_hostSetting.Admin}{Constants.CreateGroupFromAppValid}", accessToken , stringContent);
            }
            catch
            {

            }
            return response;
        }
    }
}
