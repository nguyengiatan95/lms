﻿using FrontEnd.Common.Entites.Management;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Models.Management.Permission; 
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Common.Services.Permission
{
    public interface IPermissionService
    {
        public List<TblPermission> GetPermissionByID(string accessToken , int id);
        public Task<List<PermissionDatatable>> GetByConditions(string accessToken , string generalSearch, int status, int pageIndex, int pageSize, string field, string sort);

        public ResponseActionResult AddPermission(PermissionAddModel request, string accessToken );
        public ResponseActionResult UpdatePermission(PermissionAddModel request, string accessToken );

    }
    public class PermissionService : BaseServices, IPermissionService
    {
        readonly ServiceHost _hostSetting;
        public PermissionService(IOptions<ServiceHost> options, IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _hostSetting = options.Value;
        }
        public List<TblPermission> GetPermissionByID(string accessToken , int id)
        {
            var data = new List<TblPermission>();
            try
            {
                var url = $"{_hostSetting.Admin}{Constants.GetPermissionByID}{id}";
                var response = GetAsync<ResponseActionResult<List<TblPermission>>>($"{Constants.GetPermissionByID}{id}", accessToken , null);
                if (response.Result.Result == 1 && response.Result.Data.Count > 0)
                    data = response.Result.Data;
            }
            catch
            {

            }
            return data;
        }

        public async Task<List<PermissionDatatable>> GetByConditions(string accessToken , string generalSearch, int status, int pageIndex, int pageSize, string field, string sort)
        {
            var data = new List<PermissionDatatable>();
            try
            {
                string uri = $"{_hostSetting.Admin}{Constants.GetPermissionDatatable}?GeneralSearch={generalSearch}&Status={status}&PageIndex={pageIndex}&PageSize={pageSize}&Field={field}&Sort={sort}";
                var response = await GetAsync<ResponseActionResult<List<PermissionDatatable>>>(uri, accessToken , null);
                if (response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
                if (data != null && data.Count > 0)
                {
                    data[0].TotalCount = response.Total;
                }
            }
            catch
            {

            }
            return data;
        }

        public ResponseActionResult AddPermission(PermissionAddModel request, string accessToken )
        {
            var reponseData = new ResponseActionResult();
            try
            {
                string uri = $"{_hostSetting.Admin}{Constants.AddPermission}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<ResponseActionResult<long?>>(uri, accessToken , stringContent);
                reponseData.Result = response.Result.Result;
                reponseData.Message = response.Result.Message;
                reponseData.Data = response.Result.Data;
                return reponseData;
            }
            catch
            {

            }
            return reponseData;
        }
        public ResponseActionResult UpdatePermission(PermissionAddModel request, string accessToken )
        {
            var reponseData = new ResponseActionResult();
            var data = new List<PermissionDatatable>();
            try
            {
                string uri = $"{_hostSetting.Admin}{Constants.UpdatePermission}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<ResponseActionResult<long?>>(uri, accessToken , stringContent);
                reponseData.Result = response.Result.Result;
                reponseData.Message = response.Result.Message;
                reponseData.Data = response.Result.Data;
                return reponseData;
            }
            catch
            {

            }
            return new ResponseActionResult();
        }
    }
}
