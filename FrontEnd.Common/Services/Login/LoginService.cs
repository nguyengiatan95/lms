﻿using System;
using System.Collections.Generic;
using System.Text;
using FrontEnd.Common.Entites.Management;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Models.Management.User;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json; 
using System.Net.Http; 
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using FrontEnd.Common.Models.Management.Login; 

namespace FrontEnd.Common.Services.Login
{ 
    public interface ILoginService
    {
        public Task<ResponseActionResult<UserInfoWithMenuModel>> LoginPost(TblUser entity);
        Task<ResponseActionResult> Logout(UserInfoWithMenuModel entity);
        Task<ResponseActionResult<UserInfoWithMenuModel>> GetUserInfoWithMenu(GetUserInfoWithMenuQuery entity, string token);
        Task<ResponseActionResult<UserInfoWithMenuModel>> RefreshToken(string token);
    }
    public class LoginService : BaseServices, ILoginService
    {
        readonly ServiceHost _hostSetting;
        public LoginService(IOptions<FrontEnd.Common.Helpers.ServiceHost> options, IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _hostSetting = options.Value;
        }
        public async Task<ResponseActionResult> Logout(UserInfoWithMenuModel entity)
        {
            var response = new ResponseActionResult();
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var url = $"{_hostSetting.Authen}{Constants.Logout}";
                response = await PostAsync<ResponseActionResult>(url, entity.Token, stringContent);
            }
            catch
            { 
            }
            return response;
        }
        public async Task<ResponseActionResult<UserInfoWithMenuModel>> LoginPost(TblUser entity)
        {
            var response = new ResponseActionResult<UserInfoWithMenuModel>();
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var url = $"{_hostSetting.Authen}{Constants.Login}";
                response = await PostAsync<ResponseActionResult<UserInfoWithMenuModel>>(url, "", stringContent);
            }
            catch
            {

            }
            return response;
        }
        public async Task<ResponseActionResult<UserInfoWithMenuModel>> RefreshToken(string token)
        {
            var response = new ResponseActionResult<UserInfoWithMenuModel>();
            try
            {
                var model = new UserToken
                {
                    Token = token
                };
                var json = JsonConvert.SerializeObject(model);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var url = $"{_hostSetting.Authen}{Constants.RefreshToken}";
                response = await PostAsync<ResponseActionResult<UserInfoWithMenuModel>>(url, token, stringContent);
            }
            catch
            {

            }
            return response;
        }
        public async Task<ResponseActionResult<UserInfoWithMenuModel>> GetUserInfoWithMenu(GetUserInfoWithMenuQuery entity,string token)
        {
            var response = new ResponseActionResult<UserInfoWithMenuModel>();
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var url = $"{_hostSetting.Admin}{Constants.GetUserInfoWithMenu}";
                response = await PostAsync<ResponseActionResult<UserInfoWithMenuModel>>(url, token, stringContent);
            }
            catch
            { 
            }
            return response;
        }
    }
}
