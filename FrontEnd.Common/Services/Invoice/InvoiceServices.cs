﻿using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Models.Invoice;
using LMS.Entites.Dtos.InvoiceService;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text; 
using System.Threading.Tasks;

namespace FrontEnd.Common.Services.Invoice
{ 
    public interface IInvoiceServices
    {
        public Task<ResponseActionResult<ResponseForDataTable<List<InvoiceItemViewModel>>>> GetInvoiceInfosByCondition(string accessToken, InvoiceDatatableModel model);
    }
    public class InvoiceServices : BaseServices, IInvoiceServices
    {
        readonly ServiceHost _hostSetting;
        public InvoiceServices(IOptions<ServiceHost> options, IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _hostSetting = options.Value;
        }
        public async Task<ResponseActionResult<ResponseForDataTable<List<InvoiceItemViewModel>>>> GetInvoiceInfosByCondition(string accessToken, InvoiceDatatableModel model)
        {
            var response = new ResponseActionResult<ResponseForDataTable<List<InvoiceItemViewModel>>>();
            try
            {
                string uri = $"{_hostSetting.Invoice}{Constants.GetInvoiceInfosByCondition}";
                var json = JsonConvert.SerializeObject(model);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                response = await PostAsync<ResponseActionResult<ResponseForDataTable<List<InvoiceItemViewModel >>>> (uri, accessToken, stringContent);
                return response;
            }
            catch
            {
                response.SetError();
            }
            return response;
        }
    }
}
