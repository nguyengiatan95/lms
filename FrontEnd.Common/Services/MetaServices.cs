﻿using FrontEnd.Common.Entites;
using FrontEnd.Common.Entites.Bank;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Models.Bank;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Common.Services.Bank
{
    public interface IMetaServices
    {
        public Task<List<SelectItem>> GetBankCard(string accessToken, long BankCardID = -1, int Status = -1);
    }
    public class MetaServices : BaseServices, IMetaServices
    {
        ServiceHost _hostSetting;
        public MetaServices(IOptions<ServiceHost> options, IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _hostSetting = options.Value;
        }
        public async Task<List<SelectItem>> GetBankCard(string accessToken, long BankCardID = -1, int Status = -1)
        {
            var data = new List<SelectItem>();
            try
            {
                string uri = $"{_hostSetting.Loan}{Constants.GetBankCard}?BankCardID={BankCardID}&Status={Status}";
                var response = await GetAsync<ResponseActionResult<List<SelectItem>>>(uri, accessToken);
                if (response != null && response.Result == 1 && response.Data.Count > 0)
                    data = response.Data;
            }
            catch
            {

            }
            return data;
        }
    }
}
