﻿using FrontEnd.Common.Entites.Bank;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Models.Bank;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd.Common.Services.Bank
{
    public interface IBankServices
    {
        public Task<ResponseActionResult<List<ReportInOutMoneyBankDate>>> ReportInOutMoneyBankDate(string accessToken, BankModel model);
        public Task<ResponseActionResult<ResponseForDataTable<List<LMS.Entites.Dtos.InvoiceService.InvoiceItemViewModel>>>> GetInvoiceInfosByBankCard(string accessToken, BankModel model);
        public Task<ResponseForDataTable<List<BankCardDatatable>>> GetByConditions(string accessToken, ManageBankCardModel request);
        public ResponseActionResult AddBankCard(BankCardReq request, string accessToken);
        public ResponseActionResult UpdateBankCard(BankCardReq request, string accessToken);
    }
    public class BankServices : BaseServices, IBankServices
    {
        readonly ServiceHost _hostSetting;
        public BankServices(IOptions<ServiceHost> options, IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _hostSetting = options.Value;
        }
        public async Task<ResponseActionResult<List<ReportInOutMoneyBankDate>>> ReportInOutMoneyBankDate(string accessToken, BankModel model)
        {
            var response = new ResponseActionResult<List<ReportInOutMoneyBankDate>>();
            try
            {
                string uri = $"{_hostSetting.Invoice}{Constants.ReportInOutMoneyBankDate}";
                var json = JsonConvert.SerializeObject(model);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                response = await PostAsync<ResponseActionResult<List<ReportInOutMoneyBankDate>>>(uri, accessToken, stringContent);
                return response;
            }
            catch
            {
                response.SetError();
            }
            return response;
        }

        public async Task<ResponseActionResult<ResponseForDataTable<List<LMS.Entites.Dtos.InvoiceService.InvoiceItemViewModel>>>> GetInvoiceInfosByBankCard(string accessToken, BankModel model)
        {
            var response = new ResponseActionResult<ResponseForDataTable<List<LMS.Entites.Dtos.InvoiceService.InvoiceItemViewModel>>>();
            try
            {
                string uri = $"{_hostSetting.Invoice}{Constants.InvoiceInfosByBankCard}";
                var json = JsonConvert.SerializeObject(model);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                response = await PostAsync<ResponseActionResult<ResponseForDataTable<List<LMS.Entites.Dtos.InvoiceService.InvoiceItemViewModel>>>>(uri, accessToken, stringContent);
                return response;
            }
            catch
            {
            }
            return response;
        }
        public async Task<ResponseForDataTable<List<BankCardDatatable>>> GetByConditions(string accessToken, ManageBankCardModel request)
        {
            var data = new ResponseForDataTable<List<BankCardDatatable>>();
            try
            {
                string uri = $"{_hostSetting.Loan}{Constants.BankCard_ListBankCard}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<ResponseActionResult<ResponseForDataTable<List<BankCardDatatable>>>>(uri, accessToken, stringContent);
                if (response != null && response.Result == 1 && response.Data != null)
                    data = response.Data;
            }
            catch
            {
            }
            return data;
        }
        public ResponseActionResult AddBankCard(BankCardReq request, string accessToken)
        {
            var reponseData = new ResponseActionResult();
            try
            {
                string uri = $"{_hostSetting.Loan}{Constants.BankCard_CreateBankCard}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<ResponseActionResult<long?>>(uri, accessToken, stringContent);
                reponseData.Result = response.Result.Result;
                reponseData.Message = response.Result.Message;
                reponseData.Data = response.Result.Data;
                return reponseData;
            }
            catch
            {

            }
            return reponseData;
        }
        public ResponseActionResult UpdateBankCard(BankCardReq request, string accessToken)
        {
            var reponseData = new ResponseActionResult();
            try
            {
                string uri = $"{_hostSetting.Loan}{Constants.BankCard_UpdateBankCard}";
                var json = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = PostAsync<ResponseActionResult<long?>>(uri, accessToken, stringContent);
                reponseData.Result = response.Result.Result;
                reponseData.Message = response.Result.Message;
                reponseData.Data = response.Result.Data;
                return reponseData;
            }
            catch
            {
            }
            return new ResponseActionResult();
        }
    }
}
