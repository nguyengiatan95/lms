﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using FrontEnd.Common.Entites.Management;
using Microsoft.AspNetCore.Http;

namespace FrontEnd.Common.Services
{
    public abstract class BaseServices
    {
        protected IConfiguration _baseConfig;
        private readonly IHttpClientFactory _clientFactory;
        public BaseServices(IConfiguration configuration, IHttpClientFactory clientFactory)
        {
            _baseConfig = configuration;
            _clientFactory = clientFactory;
        }

        protected async System.Threading.Tasks.Task<T> GetAsync<T>(string uri, string accessToken, string baseUri = "")
        {
            string responseJson = string.Empty;
            using (var client = _clientFactory.CreateClient())
            {
                if (string.IsNullOrEmpty(baseUri))
                    baseUri = _baseConfig["Configuration:ApiUrl"];
                client.BaseAddress = new System.Uri(baseUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", accessToken);
                var response = await client.GetAsync(baseUri + uri);
                if (response.IsSuccessStatusCode)
                {
                    responseJson = await response.Content.ReadAsStringAsync();
                }
            }
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseJson);
        }

        protected async System.Threading.Tasks.Task<T> PostAsync<T>(string uri, string accessToken, StringContent data, string baseUri = "")
        {
            string responseJson = string.Empty;
            using (var client = _clientFactory.CreateClient())
            {
                if (string.IsNullOrEmpty(baseUri))
                    baseUri = _baseConfig["Configuration:ApiUrl"];
                client.BaseAddress = new System.Uri(baseUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", accessToken);
                var response = await client.PostAsync(baseUri + uri, data);
                if (response.IsSuccessStatusCode)
                {
                    responseJson = await response.Content.ReadAsStringAsync();
                }
            }
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseJson);
        }

        protected async System.Threading.Tasks.Task<T> PutAsync<T>(string uri, string accessToken, StringContent data, string baseUri = "")
        {
            string responseJson = string.Empty;
            using (var client = _clientFactory.CreateClient())
            {
                if (string.IsNullOrEmpty(baseUri))
                    baseUri = _baseConfig["Configuration:ApiUrl"];
                client.BaseAddress = new System.Uri(baseUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", accessToken);
                var response = await client.PutAsync(baseUri + uri, data);
                if (response.IsSuccessStatusCode)
                {
                    responseJson = await response.Content.ReadAsStringAsync();
                }
            }

            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseJson);
        }

        protected async System.Threading.Tasks.Task<T> DeleteAsync<T>(string uri, string accessToken, string baseUri = "")
        {
            string responseJson = string.Empty;
            using (var client = _clientFactory.CreateClient())
            {
                if (string.IsNullOrEmpty(baseUri))
                    baseUri = _baseConfig["Configuration:ApiUrl"];
                client.BaseAddress = new System.Uri(baseUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", accessToken);
                var response = await client.DeleteAsync(baseUri + uri);
                if (response.IsSuccessStatusCode)
                {
                    responseJson = await response.Content.ReadAsStringAsync();
                }
            }

            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseJson);
        }
    }
}
