﻿using FrontEnd.Common.Models.Management.Login;
using FrontEnd.Common.Models.Management.User;
using FrontEnd.Common.Services.Login;
using LMS.Common.Constants;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Helpers
{
    public class AuthorizeFilterAttribute : ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ILoginService loginService = (ILoginService)filterContext.HttpContext.RequestServices.GetService(typeof(ILoginService));
            IConfiguration _baseConfig = (IConfiguration)filterContext.HttpContext.RequestServices.GetService(typeof(IConfiguration));
            var session = filterContext.HttpContext.Session;
            var host = filterContext.HttpContext.Request.Host.ToString();
            var requestPath = filterContext.HttpContext.Request.Path.ToString();
            var queriesString = filterContext.HttpContext.Request.QueryString.ToString();
            var url = host + requestPath + queriesString;
            var token = filterContext.HttpContext.Request.Query["token"].ToString();
            var refer = filterContext.HttpContext.Request.Query["referrer"].ToString();
            var TimeExpired = filterContext.HttpContext.Request.Query["TimeExpired"].ToString();
            var userModelSession = session.GetObjectFromJson<UserInfoWithMenuModel>(Constants.STATIC_USERMODEL);
            if (!string.IsNullOrEmpty(token) ||session == null || userModelSession == null || userModelSession.Result == null)
            { 
                var appId = (int)Menu_AppID.Admin;
                var defaulUrl = requestPath.Contains("redirect") ? "/" : requestPath;
                var acc = _baseConfig[Constants.STATIC_HOME_AccountantUrl];
                var recoveryOfLoans = _baseConfig[Constants.STATIC_HOME_RecoveryOfLoans];
                if (acc.Contains(host))
                {
                    appId = (int)Menu_AppID.Accountant; 
                }
                else if (_baseConfig[Constants.STATIC_HOME_LenderUrl].Contains(host))
                {
                    appId = (int)Menu_AppID.Lender;
                }
                else if (recoveryOfLoans.Contains(host))
                {
                    appId = (int)Menu_AppID.THN;
                }
                if (url.Contains("logout"))
                {
                    url = "";
                }
                var loginUrl = _baseConfig[Constants.STATIC_Login_URL] + url.Replace('/', '<').Replace(':', '>').Replace('&', '*');
                filterContext.Result = new RedirectResult(loginUrl);
                if ( !string.IsNullOrEmpty(token))
                {
                    // gọi API lấy menu 
                    var userModel = new GetUserInfoWithMenuQuery { AppID = appId };
                    var responce = loginService.GetUserInfoWithMenu(userModel, token).Result;
                    if (responce != null && responce.Data != null && responce.Data.UserID > 0)
                    {
                        // check điều hướng Referrer
                        responce.Data.Token = token;
                        responce.Data.LoginUrl = _baseConfig[Constants.STATIC_Login_URL];
                        responce.Data.ApiUrl = _baseConfig[Constants.STATIC_ApiUrl_URL];
                        responce.Data.AdminUrl = _baseConfig[Constants.STATIC_HOME_URL];
                        responce.Data.DomainCookie = _baseConfig[Constants.STATIC_Cookie];
                        responce.Data.CookieName = Constants.STATIC_USERMODEL;
                        DateTime fromDate = DateTime.Now;
                        if (DateTime.TryParseExact(TimeExpired,"dd/MM/yyyy HH:mm", null, System.Globalization.DateTimeStyles.None, out DateTime dateRequest))
                        {
                            fromDate = dateRequest;
                        } 
                        responce.Data.TimeExpired = fromDate;// DateTime.Now.AddMinutes(Constants.STATIC_DateExpiration_Token);
                        responce.Data.TimeExpiredString = responce.Data.TimeExpired.ToString("dd/MM/yyyy HH:mm");
                        responce.Data.UserLoginModel = new UserLoginModel
                        {
                            AdminUrl = _baseConfig[Constants.STATIC_HOME_URL],
                            AccountantUrl = _baseConfig[Constants.STATIC_HOME_AccountantUrl],
                            LenderUrl = _baseConfig[Constants.STATIC_HOME_LenderUrl],
                            THNUrl = _baseConfig[Constants.STATIC_HOME_RecoveryOfLoans],
                            PathRereshToken = Constants.STATIC_Path_RereshToken
                        };
                        filterContext.HttpContext.Session.SetObjectAsJson(Constants.STATIC_USERMODEL, responce.Data);
                        if (!string.IsNullOrEmpty(refer))
                        {
                            refer = refer.Replace("<", "/").Replace('>', ':').Replace('*', '&');
                            var from = refer.IndexOf("/");
                            var tok = refer.IndexOf("token");
                            var path = refer.Substring(from, refer.Length - from);
                            if (tok > 0)
                            {
                                path = refer.Substring(from, tok - from-1);
                            }
                            filterContext.Result = new RedirectResult(path);
                        }
                        else
                        {
                            filterContext.Result = new RedirectResult(defaulUrl);
                        }
                    }
                    else
                    {
                        filterContext.Result = new RedirectResult(loginUrl);
                    }
                }
                else
                {
                    filterContext.Result = new RedirectResult(loginUrl);
                }
            }
            else
            { 
                var tok = url.IndexOf("token");  
                var from = url.IndexOf("/"); 
                  url = url.Substring(from, url.Length - from);
                if (tok > 0)
                {
                    url = url.Substring(from, tok - from - 1);
                    filterContext.Result = new RedirectResult(url);
                }
            }
        }
    }
}
