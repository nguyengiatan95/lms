﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Helpers
{
    public class Constants
    {
        public static string STATIC_EXCEL_THN { get; set; }
        public static string STATIC_EXCEL_LENDER { get; set; } = "STATIC_EXCEL_LENDER";

        public static string STATIC_USERMODEL { get; set; }
        public static string STATIC_CALL_INFO_COOKIE { get; set; } 
        public static string STATIC_VERSION { get; set; } 
        public static string STATIC_Cookie = "Configuration:CookieCommon"; 
        //public static string STATIC_USERMODEL = "USERMODEL";
        public static string STATIC_TOKEN = "token";
        public static string STATIC_Referrer = "referrer";
        public static string STATIC_Login_URL = "AppSettings:Login";
        public static string STATIC_LogOut_Admin_URL = "AppSettings:LogOut";
        public static string STATIC_LogOut_Acc_URL = "AppSettings:LogOutAcc";
        public static string STATIC_HOME_URL = "AppSettings:AdminUrl";
        public static string STATIC_HOME_AccountantUrl = "AppSettings:AccountantUrl";
        public static string STATIC_HOME_LenderUrl = "AppSettings:LenderUrl";
        public static string STATIC_HOME_RecoveryOfLoans = "AppSettings:RecoveryOfLoans";
        public static string STATIC_CURRENTlINK = "CURRENTlINK";
        public static string STATIC_ApiUrl_URL = "Configuration:ApiUrl";
        public static string STATIC_HOME_ACC = "/";
        public static string STATIC_HOME_LENDER = "/";
        public static string STATIC_Path_RereshToken = "/Home/RefreshToken";
        public static int STATIC_DateExpiration_Token = 8; 

        //perrmission
        public static string GetPermissionByID = "api/Permission/GetPermissionByID/";
        public static string GetPermissionDatatable = "api/Permission/GetByConditions";
        public static string AddPermission = "api/Permission/CreatePermission";
        public static string UpdatePermission = "api/Permission/UpdatePermission";
         
        //User  
        public static string Login = "api/authen/login";
        public static string Logout = "api/authen/logout";
        public static string GetUserInfoWithMenu = "api/user/GetUserInfoWithMenu ";
        public static string RefreshToken = "api/authen/refeshtoken";

        public static string GetUser = "api/User/GetUser";
        public static string GetUserByUserID = "api/User/GetUserByID/"; 
        public static string GetUserByUserName = "api/User/GetUserByUserName"; 
        public static string CreateUserFromAppValid = "api/User/CreateUserFromAppValid"; 
        public static string UpdateUserFromApp = "api/User/UpdateUserFromApp";
        public static string ChangePassword = "api/User/ChangePassword";
        public static string GetUserGroupByUserId = "api/UserGroup/GetUserGroupByUserId/";

        //Group  
        public static string GetGroupByID = "api/Group/GetGroupByID/";
        public static string GetGroup = "api/Group/GetGroup";
        public static string UpdateGroupFromApp = "api/Group/UpdateGroupFromApp";
        public static string CreateGroupFromAppValid = "api/Group/CreateGroupFromAppValid";

        //GroupPermission
        public static string GetPermissionByGroupID = "api/GroupPermission/GetByGroupID/";
        public static string AddPermissionGroup = "api/GroupPermission/CreatePermission";


        //department
        public static string GetDepartment = "api/Department/GetDepartment";
        public static string GetDepartmentByID = "api/Department/GetDepartmentByID/";
        public static string CreateDepartment = "api/Department/CreateDepartmentFromAppValid";
        public static string UpdateDepartment = "api/Department/UpdateDepartmentFromApp";

        //SHOP chưa có API
        public static string GetShops = "api/Shop/GetShops";

        //Lender  chưa có API
        public static string GetLendersAndShop = "api/Lender/GetLenderSearch/";
        public static string GetLenderList = "api/Lender/GetLenderList/";


        // LOAN  
        public static string GetLstLoanInfoByCondition = "api/Loan/GetLstLoanInfoByCondition";
        public static string GetLstPaymentScheduleByLoanID = "api/loan/GetLstPaymentScheduleByLoanID/";
        public static string GetLoanByID = "api/loan/GetLoanByID/";
        public static string DeleteLoan = "api/Loan/DeleteLoan";


        public static string Getstasticloanstatusbyshop = "api/report/getstasticloanstatusbyshop";
        public static string BankCard_ListBankCard = "api/BankCard/ListBankCard";
        public static string BankCard_CreateBankCard = "api/BankCard/CreateBankCard";
        public static string BankCard_UpdateBankCard = "api/BankCard/UpdateBankCard";
        public static string ExtendLoanTime = "api/Loan/ExtendLoanTime";
        public static string ReportVat = "api/Report/ReportVat";
        public static string ReportFineInterestLate = "api/Report/ReportFineInterestLate";
        public static string ReportVatHub = "api/Report/ReportVatHub";
        public static string ReportPenaltyFeeVatHub = "api/Report/ReportPenaltyFeeVatHub";
        public static string ReportVatGCash = "api/Report/ReportVatGCash";
        public static string ReportVatHUbWithGCash = "api/Report/ReportVatHUbWithGCash";
        public static string ReportVatLender = "api/Report/ReportVatLender";
        public static string ReportMoneyDetail = "api/loan/ReportMoneyDetail";
        public static string ReportMoneyInterestLender = "api/Report/ReportVATForLender";

        // INVOICE  
        public static string GetInvoiceInfosByCondition = "api/Invoice/GetInvoiceInfosByCondition";

        public static string GetTicketInfosByCondition = "api/Ticket/GetTicketByConditions";


        //BANK 
        public static string ReportInOutMoneyBankDate = "api/CashBankCardByDate/ReportInOutMoneyBankDate";
        public static string GetBankCard = "api/Meta/GetBankCard";
        public static string InvoiceInfosByBankCard = "api/Invoice/InvoiceInfosByBankCard";

        //Insurance
        public static string ReportInsurance = "api/Insurance/ReportInsurance";
        public static string Indemnify = "api/Lender/GetReportInsurance";

        //logcompare
        public static string GetSummaryCompare = "api/LogCompare/GetSummaryCompare";

        // los
        public static string DomainLOS = "https://upl.tima.vn/";
        public static string LOS_Upload_Image = "api/image?provider=lms";
        public static string LOS_Get_Image = "api/image";
        public static string LOS_KEY = "lms@#123";
        public static string LOS_Delete_Img = "api/image?i=";
        //thn
        public static string GetByTraceID = "api/Excel/GetByTraceID";
        public static string GetLstHistoryInteractDebtPrompted = "api/CommentDebtPrompted/GetLstHistoryInteractDebtPrompted";
    }
    public class ServiceHost
    {
        public string Authen { get; set; }
        public string Admin { get; set; }
        public string Loan { get; set; }
        public string Invoice { get; set; }
        public string Lender { get; set; }
        public string Insurance { get; set; }
        public string Thn { get; set; }
    }
}
