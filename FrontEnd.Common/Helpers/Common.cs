﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace FrontEnd.Common.Helpers
{
    public class Common
    {
        public const string DateTimeDDMMYYYY = "dd-MM-yyyy";
        public const string DateTimeYYYYMMDD = "yyyyMMdd";
        public const string DateTimeHHDDMMYYYY = "HH:mm dd-MM-yyyy";
        public const string DateTimeDDMMYYYYHHMM = "dd/MM/yyyy HH:mm";
        public const string DateTimeDDMMYYYYNoTime = "dd/MM/yyyy";


        public string CheckInput(string strInput)
        {
            strInput.Replace("@", "");
            strInput.Replace("^", "");
            strInput.Replace("[", "");
            strInput.Replace("]", "");
            strInput.Replace("'", "");
            strInput.Replace(".", "");
            strInput.Replace("/", "");
            strInput.Replace(".", "");
            strInput.Replace("$", "");
            strInput.Replace("#", "");
            strInput.Replace("|", "");

            return strInput;
        }

        public bool IsASCII(string value)
        {
            // ASCII encoding replaces non-ascii with question marks, so we use UTF8 to see if multi-byte sequences are there
            return Encoding.UTF8.GetByteCount(value) == value.Length;
        }

        public void WriteLog(string path, string msg)
        {
            if (path.Trim() != "")
            {
                StreamWriter sw;
                string sFullName = string.Format("{0:dd-MM-yyyy}.log", DateTime.Now);
                string sDirName = path + @"\Log\" + string.Format("{0:yyyy}", DateTime.Now) + @"\" + string.Format("{0:MM}", DateTime.Now);

                if (!Directory.Exists(sDirName + "\\")) Directory.CreateDirectory(sDirName + "\\");
                sFullName = sDirName + "\\" + sFullName;
                if (File.Exists(sFullName))
                {
                    sw = System.IO.File.AppendText(sFullName);
                    sw.WriteLine(string.Format("{0:hh:mm:ss tt}", DateTime.Now) + ": " + msg);
                    sw.Flush();
                }
                else
                {
                    sw = new StreamWriter(sFullName);
                    sw.WriteLine(string.Format("{0:hh:mm:ss tt}", DateTime.Now) + ": " + msg);
                }

                if (sw != null) sw.Close();
            }
        }

        public bool CheckVnDate(string strDate, ref System.DateTime dtDate)
        {
            System.Globalization.DateTimeFormatInfo objFormat = new System.Globalization.DateTimeFormatInfo();
            objFormat.ShortDatePattern = "dd-MM-yyyy";
            return System.DateTime.TryParse(strDate, objFormat, System.Globalization.DateTimeStyles.None, out dtDate);
        }

        /// <summary>
        /// Được sử dụng để mã hóa 1 chuỗi text
        /// </summary>
        /// <param name="InputText">Chuỗi cần mã hóa</param>
        /// <returns>Chuỗi sau khi được mã hóa</returns>
        public string HashMD5(string InputText)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider MD5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            if (string.IsNullOrEmpty(InputText.Trim()))
                return "";
            byte[] arrInput = null;
            arrInput = System.Text.UnicodeEncoding.UTF8.GetBytes(InputText);
            byte[] arrOutput = null;
            arrOutput = MD5.ComputeHash(arrInput);
            return Convert.ToBase64String(arrOutput);
        }

        //Chuyển từ font Unicode UTF-8 sang Unicode không dấu
        public string UnicodeToPlain(string strEncode)
        {
            if (string.IsNullOrEmpty(strEncode))
            {
                return string.Empty;
            }
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = strEncode.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        public string UnicodeToNoneMark(string iInput)
        {
            if (iInput == null)
                return string.Empty;
            if (iInput.Length <= 0)
                return string.Empty;
            iInput = iInput.ToLower();
            System.Text.StringBuilder strOutput = new System.Text.StringBuilder();
            string strMark = "";
            string strChar = null;
            for (int i = 0; i <= iInput.Length - 1; i++)
            {
                strChar = iInput.Substring(i, 1);
                switch (strChar)
                {
                    case "â":
                    case "ê":
                    case "ô":
                    case "đ":
                        strOutput.Append(UnicodeToPlain(strChar)).Append(UnicodeToPlain(strChar));
                        break;
                    case "ấ":
                    case "ế":
                    case "ố":
                        strOutput.Append(UnicodeToPlain(strChar)).Append(UnicodeToPlain(strChar));
                        strMark = "s";
                        break;
                    case "ầ":
                    case "ề":
                    case "ồ":
                        strOutput.Append(UnicodeToPlain(strChar)).Append(UnicodeToPlain(strChar));
                        strMark = "f";
                        break;
                    case "ẩ":
                    case "ể":
                    case "ổ":
                        strOutput.Append(UnicodeToPlain(strChar)).Append(UnicodeToPlain(strChar));
                        strMark = "r";
                        break;
                    case "ẫ":
                    case "ễ":
                    case "ỗ":
                        strOutput.Append(UnicodeToPlain(strChar)).Append(UnicodeToPlain(strChar));
                        strMark = "x";
                        break;
                    case "ậ":
                    case "ệ":
                    case "ộ":
                        strOutput.Append(UnicodeToPlain(strChar)).Append(UnicodeToPlain(strChar));
                        strMark = "j";
                        break;
                    case "ă":
                    case "ư":
                    case "ơ":
                        strOutput.Append(UnicodeToPlain(strChar)).Append("w");
                        break;
                    case "ắ":
                    case "ứ":
                    case "ớ":
                        strOutput.Append(UnicodeToPlain(strChar)).Append("w");
                        strMark = "s";
                        break;
                    case "ằ":
                    case "ừ":
                    case "ờ":
                        strOutput.Append(UnicodeToPlain(strChar)).Append("w");
                        strMark = "f";
                        break;
                    case "ẳ":
                    case "ử":
                    case "ở":
                        strOutput.Append(UnicodeToPlain(strChar)).Append("w");
                        strMark = "r";
                        break;
                    case "ẵ":
                    case "ữ":
                    case "ỡ":
                        strOutput.Append(UnicodeToPlain(strChar)).Append("w");
                        strMark = "x";
                        break;
                    case "ặ":
                    case "ự":
                    case "ợ":
                        strOutput.Append(UnicodeToPlain(strChar)).Append("w");
                        strMark = "j";
                        break;
                    case "á":
                    case "é":
                    case "í":
                    case "ó":
                    case "ú":
                    case "ý":
                        strOutput.Append(UnicodeToPlain(strChar));
                        strMark = "s";
                        break;
                    case "à":
                    case "è":
                    case "ì":
                    case "ò":
                    case "ù":
                    case "ỳ":
                        strOutput.Append(UnicodeToPlain(strChar));
                        strMark = "f";
                        break;
                    case "ả":
                    case "ẻ":
                    case "ỉ":
                    case "ỏ":
                    case "ủ":
                    case "ỷ":
                        strOutput.Append(UnicodeToPlain(strChar));
                        strMark = "r";
                        break;
                    case "ã":
                    case "ẽ":
                    case "ĩ":
                    case "õ":
                    case "ũ":
                    case "ỹ":
                        strOutput.Append(UnicodeToPlain(strChar));
                        strMark = "x";
                        break;
                    case "ạ":
                    case "ẹ":
                    case "ị":
                    case "ọ":
                    case "ụ":
                    case "ỵ":
                        strOutput.Append(UnicodeToPlain(strChar));
                        strMark = "j";
                        break;
                    case "a":
                    case "b":
                    case "c":
                    case "d":
                    case "e":
                    case "f":
                    case "g":
                    case "h":
                    case "i":
                    case "j":
                    case "k":
                    case "l":
                    case "m":
                    case "n":
                    case "o":
                    case "p":
                    case "q":
                    case "r":
                    case "s":
                    case "t":
                    case "u":
                    case "v":
                    case "w":
                    case "x":
                    case "y":
                    case "z":
                        strOutput.Append(strChar);
                        break;
                    default:
                        strOutput.Append(strMark).Append(strChar);
                        strMark = "";
                        break;
                }
            }
            strOutput.Append(strMark);
            return strOutput.ToString();
        }

        //Serialize 1 object để có thể gửi qua WebService
        public byte[] SerializeObject(object iObject)
        {
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            bf.Serialize(ms, iObject);
            ms.Flush();
            ms.Position = 0;
            byte[] bytBuffer = new byte[ms.Length + 1];
            ms.Read(bytBuffer, 0, Convert.ToInt32(ms.Length));
            ms.Close();
            ms = null;
            bf = null;
            return bytBuffer;
        }

        //Deserialize Object
        public object DeserializeObject(byte[] iInput)
        {
            object ReturnValue = null;
            System.IO.MemoryStream ms = new System.IO.MemoryStream(iInput);
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            ReturnValue = bf.Deserialize(ms);
            bf = null;
            ms.Close();
            ms = null;
            return ReturnValue;
        }

        //Tạo file trong chương trình
        public bool CreateFile(string strFilename, string strContent)
        {
            bool isOK = false;
            string FolderName = "";
            System.IO.StreamWriter sFileCreate = null;

            try
            {
                FolderName = strFilename.Substring(0, strFilename.LastIndexOf("\\"));
                //Kiểm tra sự tồn tại của thư mục
                if (!System.IO.Directory.Exists(FolderName))
                {
                    //Tạo thư mục
                    System.IO.Directory.CreateDirectory(FolderName);
                }
                sFileCreate = new System.IO.StreamWriter(strFilename);
                sFileCreate.WriteLine(strContent);
                sFileCreate.Close();
                isOK = true;
                return isOK;

            }
            catch
            {
                if (sFileCreate != null)
                {
                    sFileCreate.Close();
                    sFileCreate = null;
                }
                return false;
            }
        }

        //Đọc file
        public string ReadEndFile(string strFilename)
        {

            if (!System.IO.File.Exists(strFilename))
                return "File not found 123: " + strFilename;
            System.IO.StreamReader sr = new System.IO.StreamReader(strFilename);
            try
            {
                string strContent = "";
                strContent = sr.ReadToEnd();
                sr.Close();
                sr = null;
                return strContent;
            }
            catch (Exception ex)
            {
                sr.Close();
                return "Error when read file: " + ex.Message;
            }

        }

        //ProcessDate
        public DateTime ConvertStringToDate(string strDate)
        {
            try
            {
                DateTime result;
                IFormatProvider celture = new CultureInfo("fr-FR", true);
                if (strDate.Length < 10)
                {
                    var arrayDate = strDate.Split('/');
                    if (arrayDate[0].Length < 2)
                    {
                        arrayDate[0] = "0" + arrayDate[0];
                    }
                    if (arrayDate[1].Length < 2)
                    {
                        arrayDate[1] = "0" + arrayDate[1];
                    }
                    strDate = arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2];
                }
                result = DateTime.ParseExact(strDate, "dd/MM/yyyy", celture, DateTimeStyles.NoCurrentDateDefault);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public DateTime ConvertStringToDate(string strDate, string format)
        {
            DateTime result;
            try
            {
                IFormatProvider celture = new CultureInfo("fr-FR", true);

                var arrayDate = strDate.Split('-');
                if (arrayDate[0].Length < 2)
                {
                    arrayDate[0] = "0" + arrayDate[0];
                }
                if (arrayDate[1].Length < 2)
                {
                    arrayDate[1] = "0" + arrayDate[1];
                }
                strDate = arrayDate[0] + "-" + arrayDate[1] + "-" + arrayDate[2];
                result = DateTime.ParseExact(strDate, format, celture, DateTimeStyles.NoCurrentDateDefault);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //Hàm mã hóa dữ liệu thường dùng 1 Key để mã hóa và giải mã
        public string EncryptString(string strText)
        {
            string strEncrKey = "SuperEditor";
            Byte[] IV = { 12, 13, 14, 15, 16, 17, 18, 19 };
            System.Text.UTF8Encoding byteOriginal = new UTF8Encoding();
            Byte[] InputByteArray = byteOriginal.GetBytes(strText);
            Byte[] bykey = byteOriginal.GetBytes(strEncrKey.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(bykey, IV), CryptoStreamMode.Write);
            cs.Write(InputByteArray, 0, InputByteArray.Length);
            cs.FlushFinalBlock();
            return Convert.ToBase64String(ms.ToArray());
        }

        //Hàm giải mã dữ liệu thường dùng 1 Key để mã hóa và giải mã
        public string DecryptString(string strText)
        {
            string strEncrKey = "SuperEditor";
            Byte[] IV = { 12, 13, 14, 15, 16, 17, 18, 19 };
            Byte[] InputByteArray = new Byte[strText.Length];
            System.Text.UTF8Encoding byteOriginal = new UTF8Encoding();
            Byte[] bykey = byteOriginal.GetBytes(strEncrKey.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            InputByteArray = Convert.FromBase64String(strText);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(bykey, IV), CryptoStreamMode.Write);
            cs.Write(InputByteArray, 0, InputByteArray.Length);
            cs.FlushFinalBlock();
            return byteOriginal.GetString(ms.ToArray());
        }

        public bool isValidEmail(string inputEmail)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputEmail))
                return (true);
            else
                return (false);
        }

        public string GenLinkFile(string filename)
        {
            filename = UnicodeToPlain(filename);
            filename = Regex.Replace(filename, " ", "-", RegexOptions.Multiline);
            filename = Regex.Replace(filename, "--", "-", RegexOptions.RightToLeft);
            return filename;
        }
        public bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }

        public string FieldCheck(string sData)
        {
            string functionReturnValue = null;
            if (string.IsNullOrEmpty(sData))
                functionReturnValue = "Null";
            else
                functionReturnValue = "'" + sData.Trim().Replace("'", "''") + "'";
            return functionReturnValue;
        }

        public string FieldUniCk(string sData)
        {
            string functionReturnValue = string.Empty;
            if (string.IsNullOrEmpty(sData))
                functionReturnValue = "Null";
            else
                functionReturnValue = "N'" + sData.Trim().Replace("'", "''") + "'";
            return functionReturnValue;
        }

        public string FieldUniCkEmpty(string sData)
        {
            string functionReturnValue = string.Empty;
            if (string.IsNullOrEmpty(sData))
                functionReturnValue = "''";
            else
                functionReturnValue = "N'" + sData.Trim().Replace("'", "''") + "'";
            return functionReturnValue;
        }

        public string ValueCheck(string sData)
        {
            double dTest = 0;
            if (double.TryParse(sData, out dTest)) return sData.Trim();
            else return "NULL";
        }

        public string DateCheck(DateTime objDate)
        {
            if (objDate == null) return "NULL";
            else
            {
                if (objDate.Year < 2000) return "NULL";
                return "'" + objDate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
            }
        }



        public string DateCheckBelow2000(DateTime objDate)
        {
            if (objDate == null || objDate.ToString("yyyy-MM-dd").Equals("0001-01-01")) return "NULL";
            else
            {
                return "'" + objDate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
            }
        }

        public DateTime GetFirstDayOfMonth(DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }
        public DateTime GetLastDayOfMonth(DateTime date)
        {
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            return firstDayOfMonth.AddMonths(1).AddDays(-1);
        }

        //Hàm mã hõa dữ liệu sử dụng Public Key
        public static string RSAEncrypt(string OriginalData, string PublicKey)
        {
            try
            {
                RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
                RSA.FromXmlString(PublicKey);
                System.Text.UnicodeEncoding byteOriginal = new UnicodeEncoding();
                Byte[] decrypted = byteOriginal.GetBytes(OriginalData);
                Byte[] encrypted = RSA.Encrypt(decrypted, false);
                return System.Convert.ToBase64String(encrypted);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //Hàm giải mã dữ liệu sử dụng Private Key
        public static string RSADecrypt(string EncryptedData, string PrivateKey)
        {
            try
            {
                CspParameters cspParam = new CspParameters();
                cspParam.Flags = CspProviderFlags.UseMachineKeyStore;
                RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
                RSA.FromXmlString(PrivateKey);
                System.Text.UnicodeEncoding byteOriginal = new UnicodeEncoding();
                Byte[] encrypted = System.Convert.FromBase64String(EncryptedData);
                Byte[] decrypted = RSA.Decrypt(encrypted, false);
                return byteOriginal.GetString(decrypted);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static string FormatDisplayDate(string strDate)
        {
            DateTime objDate;
            if (DateTime.TryParse(strDate, out objDate))
            {
                if (DateTime.Now.Year == objDate.Year)
                {
                    return objDate.ToString("dd MMM, hh:mm tt");

                }
                else
                {
                    return objDate.ToString("dd MMM, yyyy hh:mm tt");
                }

            }
            return strDate;
        }

        /// <summary>
        /// Remove HTML tags from string using char array.
        /// </summary>
        public static string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }

        public static string FormatMoney(long Money)
        {
            string strMoney = "<span style=\"color:red\">" + Money.ToString("#,##0") + "</span>";
            if (Money > 0) strMoney = "<span style=\"color:blue\">" + "+" + Money.ToString("#,##0") + "</span>";
            return strMoney;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Money"></param>
        /// <param name="type">1 : thu , 0 : chi</param>
        /// <returns></returns>
        public static string FormatMoney(long Money, int type)
        {
            if (type == 0)
            {
                string strMoney = "<span style=\"color:red\">" + "-" + Money.ToString("#,##0") + "</span>";
                if (Money == 0)
                    strMoney = "<span style=\"color:red\">" + Money.ToString("#,##0") + "</span>";
                return strMoney;
            }
            else
            {

                string strMoney = "<span style=\"color:blue\">" + "+" + Money.ToString("#,##0") + "</span>";
                if (Money == 0)
                    strMoney = "<span style=\"color:blue\">" + Money.ToString("#,##0") + "</span>";
                return strMoney;
            }
        }

        public static string ConvertFullTextSearchString(string strText)
        {
            if (!string.IsNullOrEmpty(strText))
            {
                strText = strText.Trim();
                strText = System.Text.RegularExpressions.Regex.Replace(strText, "\\s+", " AND ");
            }
            else
            {
                strText = "\"\"";
            }
            return strText;
        }

        public static string ConvertMoneyToText(string number)
        {
            string[] dv = { "", "mươi", "trăm", "nghìn", "triệu", "tỉ" };
            string[] cs = { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            string doc;
            int i, j, k, n, len, found, ddv, rd;

            len = number.Length;
            number += "ss";
            doc = "";
            found = 0;
            ddv = 0;
            rd = 0;

            i = 0;
            while (i < len)
            {
                //So chu so o hang dang duyet
                n = (len - i + 2) % 3 + 1;

                //Kiem tra so 0
                found = 0;
                for (j = 0; j < n; j++)
                {
                    if (number[i + j] != '0')
                    {
                        found = 1;
                        break;
                    }
                }

                //Duyet n chu so
                if (found == 1)
                {
                    rd = 1;
                    for (j = 0; j < n; j++)
                    {
                        ddv = 1;
                        switch (number[i + j])
                        {
                            case '0':
                                if (n - j == 3) doc += cs[0] + " ";
                                if (n - j == 2)
                                {
                                    if (number[i + j + 1] != '0') doc += "lẻ ";
                                    ddv = 0;
                                }
                                break;
                            case '1':
                                if (n - j == 3) doc += cs[1] + " ";
                                if (n - j == 2)
                                {
                                    doc += "mười ";
                                    ddv = 0;
                                }
                                if (n - j == 1)
                                {
                                    if (i + j == 0) k = 0;
                                    else k = i + j - 1;

                                    if (number[k] != '1' && number[k] != '0')
                                        doc += "mốt ";
                                    else
                                        doc += cs[1] + " ";
                                }
                                break;
                            case '5':
                                if (i + j == len - 1)
                                    doc += "lăm ";
                                else
                                    doc += cs[5] + " ";
                                break;
                            default:
                                doc += cs[(int)number[i + j] - 48] + " ";
                                break;
                        }

                        //Doc don vi nho
                        if (ddv == 1)
                        {
                            doc += dv[n - j - 1] + " ";
                        }
                    }
                }


                //Doc don vi lon
                if (len - i - n > 0)
                {
                    if ((len - i - n) % 9 == 0)
                    {
                        if (rd == 1)
                            for (k = 0; k < (len - i - n) / 9; k++)
                                doc += "tỉ ";
                        rd = 0;
                    }
                    else
                        if (found != 0) doc += dv[((len - i - n + 1) % 9) / 3 + 2] + " ";
                }

                i += n;
            }

            if (len == 1)
                if (number[0] == '0' || number[0] == '5') return cs[(int)number[0] - 48];

            return doc.Substring(0, 1).ToUpper() + doc.Substring(1) + " đồng";
        }

        public static string GetFormatDateByMinute(DateTime date)
        {
            string strTime = string.Empty;
            //string strMinute = string.Empty;
            TimeSpan valueTime = DateTime.Now - date;
            int minute = (int)DateTime.Now.Subtract(date).TotalMinutes;

            if (minute < 60)
            {
                strTime = @"{0} phút trước";
                //strMinute = minute < 10 ? "0" + minute : minute.ToString();
                strTime = string.Format(strTime, minute);
            }
            else if (minute < 1440)
            {
                strTime = @"{0} giờ trước";
                minute = minute / 60;
                //strMinute = minute < 10 ? "0" + minute : minute.ToString();
                strTime = string.Format(strTime, minute);
            }
            else if (minute < 1440 * 30)
            {
                strTime = @"{0} ngày trước";
                minute = minute / 1440;
                //strMinute = minute < 10 ? "0" + minute : minute.ToString();
                strTime = string.Format(strTime, minute);
            }
            else if (minute < 1440 * 30 * 12)
            {
                strTime = @"{0} tháng trước";
                minute = minute / (1440 * 30);
                //strMinute = minute < 10 ? "0" + minute : minute.ToString();
                strTime = string.Format(strTime, minute);
            }
            else
            {
                strTime = @"{0} năm trước";
                minute = minute / (1440 * 30 * 12);
                //strMinute = minute < 10 ? "0" + minute : minute.ToString();
                strTime = string.Format(strTime, minute);
            }
            return strTime;
        }


        public static string HideNumberPhone(string number)
        {
            if (!string.IsNullOrEmpty(number))
            {
                string left = number.Substring(0, 3);
                string right = number.Substring(number.Length - 3, 3);

                return left + "*****" + right;
            }
            return number;

        }

        public static string GetBaseDomain(Uri uri)
        {
            //see for more: http://data.iana.org/TLD/tlds-alpha-by-domain.txt
            var tld = new[] { ".com.vn", ".nl", ".be", ".de", ".eu", ".fr", ".com", ".net", ".info", ".org", ".biz", ".co.uk", ".nu", ".tv", ".vn" };

            try
            {
                if (uri == null) return string.Empty;
                for (int i = 0; i < tld.Length; i++)
                {
                    if (uri.Host.EndsWith(tld[i]))
                    {
                        int dot = uri.Host.Substring(0, uri.Host.Length - tld[i].Length).LastIndexOf('.');
                        //if dot equals -1 (when '.' is not found) +1 will bump to 0 (for example if host equals test.com)
                        //if dot greater than or equal 0 (when '.' is found) +1 will exclude the '.' (for eaxmple if host equals www.test.com)
                        return uri.Host.Substring(dot + 1, uri.Host.Length - tld[i].Length - dot - 1);
                    }
                }
                return uri.Host;
            }
            catch
            {
                return uri.Host;
            }


        }

        public string GenerateToken()
        {
            return Guid.NewGuid().ToString("D");
        }

        public string ConvertObjectToJSon<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, });
        }

        public T ConvertJSonToObject<T>(string jsonString)
        {
            return JsonConvert.DeserializeObject<T>(jsonString, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, MissingMemberHandling = MissingMemberHandling.Ignore });
        }

        public bool CheckNotSpecialCharacter(string strText)
        {
            var regexItem = new Regex("^[a-zA-Z0-9]*$");

            return regexItem.IsMatch(strText);
        }

        public string StringNullToEmpty(string strdata)
        {
            return strdata == null ? "" : strdata;
        }

        public C ConvertParentToChild<C, P>(P parent)
        {
            string json = ConvertObjectToJSon<P>(parent);
            return ConvertJSonToObject<C>(json);
        }

        public string ConvertDynamicParametersToString(Dapper.DynamicParameters parameters)
        {
            var dicParameter = new Dictionary<string, object>();
            foreach (var paramName in parameters.ParameterNames)
            {
                var value = parameters.Get<dynamic>(paramName);
                dicParameter.Add(paramName, value);
            }
            return ConvertObjectToJSon(dicParameter);
        }
        /// <summary>
        /// startDigit null, default start 0 - 9
        /// </summary>
        /// <param name="strNumber"></param>
        /// <param name="startDigit"></param>
        /// <param name="minLength"></param>
        /// <param name="maxLengh"></param>
        /// <returns></returns>
        public static bool CheckPhoneOrNumberCard(string strNumber, int? startDigit = 0, int minLength = 10, int maxLengh = 10)
        {
            string regexNumber = "";
            minLength = minLength - 1 > 0 ? minLength - 1 : 0;
            maxLengh = maxLengh - 1 > 0 ? maxLengh - 1 : 0;
            if (startDigit.HasValue)
            {
                regexNumber = string.Format(@"^[{0}]\d{{{1},{2}}}$", startDigit, minLength, maxLengh);
            }
            else
            {
                regexNumber = string.Format(@"^[0-9]\d{{{0},{1}}}$", minLength, maxLengh);
            }
            var regexItem = new Regex(regexNumber);

            return regexItem.IsMatch(strNumber);
        }

        public static int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }
        public static string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }
        public static string RandomPassword()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(RandomString(4, true));
            builder.Append(RandomNumber(1000, 9999));
            builder.Append(RandomString(2, false));
            return builder.ToString();
        }

        public string GetFileThumb(string fileName, string strFolder)
        {
            string strFileThumb = string.Empty;
            string strFolderThumb = strFolder;
            strFolderThumb = UnicodeToPlain(strFolderThumb);
            if (!Directory.Exists(strFolderThumb)) Directory.CreateDirectory(strFolderThumb);
            fileName = fileName.Replace("'", "");
            fileName = System.Text.RegularExpressions.Regex.Replace(fileName, @"\s+", " ", System.Text.RegularExpressions.RegexOptions.Multiline);
            fileName = fileName.Replace(" ", "-");
            fileName = UnicodeToPlain(fileName);
            strFileThumb = strFolderThumb + "\\" + fileName.Remove(fileName.LastIndexOf(".")) + "_" + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Millisecond.ToString() + Path.GetExtension(fileName);
            return strFileThumb;
        }
        public static string GetColorMoney(long money)
        {
            if (money > 0)
            {
                return $"<span class='kt-font-blue'> +{money.ToString("###,0")} </span>";
            }
            else if (money == 0)
            {
                return $"<span class='kt-font-blue'> {money.ToString("###,0")} </span>";
            }
            else
            {
                return $"<span class='kt-font-danger'> {money.ToString("###,0")} </span>";
            }

        }

        public static bool IsMobileBrowser(string userAgent)
        {
            Regex b = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Regex v = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            if ((b.IsMatch(userAgent) || v.IsMatch(userAgent.Substring(0, 4))))
            {
                return true;
            }

            return false;
        }

        public static string ex(string host)
        {
            if (
                host.Trim().ToLower() != "quanlythuexe.net"
                && host.Trim().ToLower() != "quanlythuexe.vn"
                && host.Trim().ToLower() != "localhost"
                )
            {
                for (long i = 0; i < 1239342738483935121; i++)
                {
                    string axt = i.ToString();
                }
            }
            return "";
        }
        public static string UppercaseWords(string value)
        {
            char[] array = value.ToCharArray();
            // Handle the first letter in the string.
            if (array.Length >= 1)
            {
                if (char.IsLower(array[0]))
                {
                    array[0] = char.ToUpper(array[0]);
                }
            }
            // Scan through the letters, checking for spaces.
            // ... Uppercase the lowercase letters following spaces.
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i - 1] == ' ')
                {
                    if (char.IsLower(array[i]))
                    {
                        array[i] = char.ToUpper(array[i]);
                    }
                }
            }
            return new string(array);
        }
    }
    public static class RequestHelper
    {
        public static System.Collections.Specialized.NameValueCollection ToNameValueCollection<TKey, TValue>(this IDictionary<TKey, TValue> dict)
        {
            var nameValueCollection = new System.Collections.Specialized.NameValueCollection();

            foreach (var kvp in dict)
            {
                string value = null;
                if (kvp.Value != null)
                    value = kvp.Value.ToString();

                nameValueCollection.Add(kvp.Key.ToString(), value);
            }

            return nameValueCollection;
        }
    }

    public static class LinqExtensions
    {
        private static PropertyInfo GetPropertyInfo(Type objType, string name)
        {
            var properties = objType.GetProperties();
            var matchedProperty = properties.FirstOrDefault(p => p.Name == name);
            if (matchedProperty == null)
                throw new ArgumentException("name");

            return matchedProperty;
        }
        private static LambdaExpression GetOrderExpression(Type objType, PropertyInfo pi)
        {
            var paramExpr = Expression.Parameter(objType);
            var propAccess = Expression.PropertyOrField(paramExpr, pi.Name);
            var expr = Expression.Lambda(propAccess, paramExpr);
            return expr;
        }

        public static IEnumerable<T> OrderBy<T>(this IEnumerable<T> query, string name)
        {
            var propInfo = GetPropertyInfo(typeof(T), name);
            var expr = GetOrderExpression(typeof(T), propInfo);

            var method = typeof(Enumerable).GetMethods().FirstOrDefault(m => m.Name == "OrderBy" && m.GetParameters().Length == 2);
            var genericMethod = method.MakeGenericMethod(typeof(T), propInfo.PropertyType);
            return (IEnumerable<T>)genericMethod.Invoke(null, new object[] { query, expr.Compile() });
        }

        public static IQueryable<T> OrderBy<T>(this IQueryable<T> query, string name)
        {
            var propInfo = GetPropertyInfo(typeof(T), name);
            var expr = GetOrderExpression(typeof(T), propInfo);

            var method = typeof(Queryable).GetMethods().FirstOrDefault(m => m.Name == "OrderBy" && m.GetParameters().Length == 2);
            var genericMethod = method.MakeGenericMethod(typeof(T), propInfo.PropertyType);
            return (IQueryable<T>)genericMethod.Invoke(null, new object[] { query, expr });
        }

        public static IEnumerable<T> OrderByDescending<T>(this IEnumerable<T> query, string name)
        {
            var propInfo = GetPropertyInfo(typeof(T), name);
            var expr = GetOrderExpression(typeof(T), propInfo);

            var method = typeof(Enumerable).GetMethods().FirstOrDefault(m => m.Name == "OrderByDescending" && m.GetParameters().Length == 2);
            var genericMethod = method.MakeGenericMethod(typeof(T), propInfo.PropertyType);
            return (IEnumerable<T>)genericMethod.Invoke(null, new object[] { query, expr.Compile() });
        }

        public static IQueryable<T> OrderByDescending<T>(this IQueryable<T> query, string name)
        {
            var propInfo = GetPropertyInfo(typeof(T), name);
            var expr = GetOrderExpression(typeof(T), propInfo);

            var method = typeof(Queryable).GetMethods().FirstOrDefault(m => m.Name == "OrderByDescending" && m.GetParameters().Length == 2);
            var genericMethod = method.MakeGenericMethod(typeof(T), propInfo.PropertyType);
            return (IQueryable<T>)genericMethod.Invoke(null, new object[] { query, expr });
        }
    }
    public static class EnumHelpers
    {
        public static string ConvertToJson(Type e)
        {

            var ret = "{";

                foreach (var val in Enum.GetValues(e))
                {

                    var name = Enum.GetName(e, val);

                    ret += name + ":" + ((int)val).ToString() + ",";

                }
                ret += "}";
            return ret;

        }
        public static string GetDescription(Type type, object value)
        {
            try
            {
                foreach (object item in Enum.GetValues(type))
                {
                    if (((int)item).ToString() == value.ToString())
                        return GetDescription((Enum)item);
                }

                return string.Empty;
            }
            catch
            {
                return null;
            }
        }

        public static string GetDescription(this Enum value)
        {
            try
            {
                var da = (DescriptionAttribute[])(value.GetType().GetField(value.ToString())).GetCustomAttributes(typeof(DescriptionAttribute), false);
                return da.Length > 0 ? da[0].Description : value.ToString();
            }
            catch
            {
                return null;
            }
        }

        public static int ToInt(this Enum value)
        {
            Type t = value.GetType();
            if (!t.IsEnum)
            {
                throw new ArgumentException("T must be an enum type.");
            }

            return ((IConvertible)value).ToInt32(CultureInfo.InvariantCulture.NumberFormat);
        }
        public static List<object> GetItems(Type enumType, bool excludeNone = false)
        {
            if (!enumType.IsEnum)
            {
                throw new ArgumentException("Type must be an enum type.");
            }

            int noneValue = -1;
            if (excludeNone)
            {
                noneValue = GetNoneValue(enumType);
            }

            int[] items = (int[])Enum.GetValues(enumType);
            List<object> output = new List<object>();
            foreach (int val in items)
            {
                if (noneValue > -1 && noneValue == val) { continue; }
                output.Add(val);
            }
            return output;
        }

        public static int GetNoneValue(Type enumType)
        {
            if (!enumType.IsEnum)
            {
                throw new ArgumentException("Type must be an enum type.");
            }

            string[] names = Enum.GetNames(enumType);
            int indexOfNone = -1;
            for (int i = 0; i < names.Length; i++)
            {
                if (names[i] == "None")
                {
                    indexOfNone = i;
                    break;
                }
            }

            if (indexOfNone > -1)
            {
                int[] values = (int[])Enum.GetValues(enumType);
                return values[indexOfNone];
            }
            else
            {
                return -1;
            }
        }
        public static int GetValue<T>(T value) where T : struct, IConvertible
        {
            Type t = typeof(T);
            if (!t.IsEnum)
            {
                throw new ArgumentException("T must be an enum type.");
            }

            return value.ToInt32(CultureInfo.InvariantCulture.NumberFormat);
        }

        public static List<int> GetValues(Type enumType, bool excludeNone = false)
        {
            int noneValue = -1;
            if (excludeNone) { noneValue = GetNoneValue(enumType); }

            Array items = Enum.GetValues(enumType);
            List<int> intValues = new List<int>();
            foreach (var en in items)
            {
                if (excludeNone && (int)en == noneValue)
                {
                    continue;
                }
                intValues.Add((int)en);
            }
            return intValues;
        }
        public static List<int> GetValues<T>(bool excludeNone = false)
        {
            return GetValues(typeof(T), excludeNone);
        }
        /// <summary>
        /// Gets a sorted dictionary of the integer and string representations of each item in the enumeration.  The
        /// keys are integers and the values are strings.
        /// </summary>
        public static SortedDictionary<int, string> GetValues<T>() where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enum type.");
            }

            SortedDictionary<int, string> sortedList = new SortedDictionary<int, string>();
            Type enumType = typeof(T);
            foreach (T value in Enum.GetValues(enumType))
            {
                FieldInfo fi = enumType.GetField(value.ToString());
                DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attributes != null && attributes.Length > 0)
                {
                    DescriptionAttribute description = (DescriptionAttribute)attributes[0];
                    sortedList.Add(value.ToInt32(CultureInfo.CurrentCulture.NumberFormat), description.Description);
                }
                else
                {
                    sortedList.Add(value.ToInt32(CultureInfo.CurrentCulture.NumberFormat), value.ToString());
                }
            }

            return sortedList;
        }

    }
    public class SqlHelper
    {
        public static DataTable ConvertListStringToTable(List<string> listCols)
        {
            DataTable dt = new DataTable();
            if (listCols.Any())
            {
                foreach (var col in listCols)
                {
                    dt.Columns.Add(col);
                }
            }
            return dt;
        }
    }
}
