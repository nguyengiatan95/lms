﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace FrontEnd.Common.Helpers
{
    public static class NameValueCollectionExtensions
    {
        public static string ToQueryString(this NameValueCollection nvc)
        {
            var array = (from key in nvc.AllKeys
                         from value in nvc.GetValues(key)
                         select string.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(value)))
                .ToArray();
            return "?" + string.Join("&", array);
        }

        public static string ToQueryObject(this object obj)
        {
            var array = new List<string>();
            Type myType = obj.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());

            foreach (PropertyInfo prop in props)
            {
                object propValue = prop.GetValue(obj, null);
                if (propValue != null && propValue.ToString() != "")
                    array.Add(string.Format("{0}={1}", prop.Name == "perpage" ? "pageSize" : prop.Name, propValue));
            }
            return "?" + string.Join("&", array);
        }
    }
}
