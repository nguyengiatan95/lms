﻿using System;
using System.Collections.Generic;
using System.Text; 
using Newtonsoft.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace FrontEnd.Common.Helpers
{
	public static class SessionExtensions
	{
		public static void SetObjectAsJson(this ISession session, string key, object value)
		{
			session.SetString(key, JsonConvert.SerializeObject(value));
			session.CommitAsync();
		}

		public static void SetObjectAsString(this ISession session, string key, string value)
		{
			session.SetString(key, value);
			session.CommitAsync();
		}
		public async static Task<string> GetObjectFromString<T>(this ISession session, string key)
		{
			await session.LoadAsync();
			var value = session.GetString(key);

			return value;
		}
		public async static Task<T> GetObjectFromJson<T>(this ISession session, string key)
		{
			string value = null;
			//int retry = 1;
			//do
			//{
			await session.LoadAsync();
			value = session.GetString(key);
			//	retry++;
			//}
			//while (value != null || retry >= 2);
			return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
		}
	}
}
