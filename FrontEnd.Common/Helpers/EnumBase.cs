﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace FrontEnd.Common.Helpers
{ 
    public enum LenderAndShop
    {
        [Description("Lender")]
        Lender = 0,
        [Description("Shop")]
        Shop = 1 
    }
}
