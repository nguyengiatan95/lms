﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace FrontEnd.Common.Entites.Insurance
{
    public class InsuranceDatatable
    {
        public string LoanCredit { get; set; }
        public string Code { get; set; }
        public string CustomerName { get; set; }
        public DateTime Fromdate { get; set; }
        public long TotalMoneyDisbursement { get; set; }
        public string TypeInsurance { get; set; }
        public long MoneyInsuranceTima { get; set; }
        public long MoneyInsuranceLender { get; set; }
        public long MoneyInsuranceCustomer { get; set; }
        public long MoneyInsuranceMaterial { get; set; }
        public long InsuranceMoney { get; set; }
        public string Status { get; set; }
        public string LinkCustomer { get; set; }
        public string PartnerCode { get; set; }
        public string LinkLender { get; set; }
        public string LinkInsuranceMaterial { get; set; }
        public string LenderName { get; set; }
        public int TotalCount { get; set; }
        public DateTime DateOfPurchaseLender { get; set; }
        public DateTime DateOfPurchaseCustomer { get; set; }
        public long LoanID { get; set; }
        public long CustomerID { get; set; }
        public long LenderID { get; set; }
        public DateTime? DisbursementDate { get; set; }
    }
    public class IndemnifyDatatable
    {
        public long LenderID { get; set; }
        public string FullName { get; set; }
        public string NumberCard { get; set; }
        public string Represent { get; set; }
        public string CustomerName { get; set; }
        public string InsuranceCode { get; set; }
        public long TotalMoneyDisbursement { get; set; }
        public long TotalMoneyCurrent { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public long TotalInterest { get; set; }
        public string SourceByInsurance { get; set; }
        public string LinkGNCLender { get; set; }
        public string StrStatusInsurance { get; set; }
        public long LoanID { get; set; }
        public string ContractCode { get; set; }
        public int TotalCount { get; set; }
        public int StatusInsurance { get; set; }
        public DateTime BirthDay { get; set; }
        public string Address { get; set; }
        public long LoanTime { get; set; }
        public long TotalInsuranceMoney { get; set; }
        public string LinkKH { get; set; }

    }

    [Description("Danh sách đơn bảo hiểm")]
    public class ExcelInsuranceLender
    {
        [Description("Tên đại lý")]
        public string FullName { get; set; }

        [Description("Tên nhà đầu tư")]
        public string Represent { get; set; }

        [Description("LoanID")]
        public string LoanID { get; set; }

        [Description("Mã TC")]
        public string ContractCode { get; set; }

        [Description("Tên khách hàng")]
        public string CustomerName { get; set; }

        [Description("Mã bảo hiểm")]
        public string InsuranceCode { get; set; }

        [Description("Ngày sinh")]
        public string StrBirthDay { get; set; }

        [Description("Số CMT")]
        public string NumberCard { get; set; }

        [Description("Địa chỉ")]
        public string Address { get; set; }

        [Description("Số tiền giải ngân")]
        public string TotalMoneyDisbursement { get; set; }

        [Description("Thời gian vay")]
        public string LoanTime { get; set; }

        [Description("Ngày giải ngân")]
        public string FromDate { get; set; }

        [Description("Ngày HĐ kết thúc")]
        public string ToDate { get; set; }

        [Description("Số tiền gốc còn lại")]
        public string TotalMoneyCurrent { get; set; }

        [Description("Lãi đã thu")]
        public string TotalInterest { get; set; }


        [Description("Số tiền được bảo hiểm")]
        public string TotalInsuranceMoney { get; set; }

        [Description("Nguồn mua bảo hiểm")]
        public string SourceByInsurance { get; set; }

        [Description("Link bảo hiểm NĐT")]
        public string LinkGNCLender { get; set; }

        [Description("Link bảo hiểm KH")]
        public string LinkKH { get; set; }
        [Description("Trạng thái")]
        public string StrStatusInsurance { get; set; }
    }

    [Description("Báo cáo mua bảo hiểm")]
    public class ExcelReportInsurance
    {
        [Description("Mã HĐ đăng ký")]
        public string loanCredit { get; set; }

        [Description("Mã HĐ giải ngân")]
        public string code { get; set; }

        [Description("Tên Khách hàng")]
        public string customerName { get; set; }

        [Description("Ngày giải ngân")]
        public string disbursementDate { get; set; }

        [Description("Số tiền giải ngân")]
        public string totalMoneyDisbursement { get; set; }

        [Description("Đối tác cho vay")]
        public string lenderName { get; set; }

        //[Description("Trạng thái")]
        //public string status { get; set; }

        [Description("Loại BH")]
        public string partnerCode { get; set; }

        [Description("Phí BH của KH")]
        public string moneyInsuranceCustomer { get; set; }

        [Description("Phí BH của Lender")]
        public string moneyInsuranceLender { get; set; }
        [Description("Phí BH vật chất")]
        public string moneyInsuranceMaterial { get; set; }

        [Description("Link KH")]
        public string linkCustomer { get; set; }

        [Description("Link Lender")]
        public string linkLender { get; set; }
        [Description("Link BH vật chất")]
        public string linkInsuranceMaterial { get; set; }

    }

    [Description("Báo cáo VAT")]
    public class ExcelReportVat
    {
        [Description("Mẫu số")]
        public string Denominator { get; set; }// Mẫu số
        [Description("Ký hiệu")]
        public string Notation { get; set; }// Ký hiệu
        [Description("Số đơn hàng")]
        public string OrderNumber { get; set; }// số đơn hàng
        [Description("Ngày HĐ")]
        public string TransactionDate { get; set; }
        [Description("Tên người mua")]
        public string CustomerName { get; set; }
        [Description("Mã khách hàng")]
        public int CustomerID { get; set; }
        [Description("Tên đơn vị")]
        public string UnitName { get; set; }

        [Description("Mã số thuế")]
        public string Taxcode { get; set; }
        [Description("Địa chỉ")]
        public string Address { get; set; }
        [Description("Tài khoản")]
        public string Account { get; set; }
        [Description("Ngân hàng")]
        public string BankName { get; set; }
        [Description("Số HĐ")]
        public string ContactCode { get; set; }

        [Description("HT thanh toán")]
        public string TypePayment { get; set; }// hình thức thanh toán

        [Description("Mã hàng")]
        public string ItemCode { get; set; }

        [Description("Tên hàng")]
        public string CommodityName { get; set; }// tên hàng
        [Description("DVT")]
        public string DVT { get; set; }

        [Description("Số lượng")]
        public string Quantily { get; set; }

        [Description("Đơn giá")]
        public string UnitPrice { get; set; }

        [Description("Tiền")]
        public string FeeNotVat { get; set; }

        [Description("% CK")]
        public string CK { get; set; }

        [Description("Tiền chiết khấu")]
        public string Discount { get; set; }

        [Description("% thuế")]
        public int TaxPercentage { get; set; }// % thuế

        [Description("Tiền thuế")]
        public string Vat { get; set; }

        [Description("Thành tiền")]
        public string TotalMoney { get; set; }


    }

    [Description("Báo cáo Phí phạt muộn")]
    public class ExcelReportFineInterestLate
    {
        [Description("Địa chỉ")]
        public string Address { get; set; }

        [Description("Mã HĐ giải ngân")]
        public string CommodityName { get; set; }

        [Description("Tên Khách hàng")]
        public string customerName { get; set; }

        [Description("Ngày giải ngân")]
        public string disbursementDate { get; set; }

        [Description("Số tiền giải ngân")]
        public string totalMoneyDisbursement { get; set; }

        [Description("Đối tác cho vay")]
        public string lenderName { get; set; }

        [Description("Trạng thái")]
        public string status { get; set; }

        [Description("Loại BH")]
        public string partnerCode { get; set; }

        [Description("Phí BH của KH")]
        public string moneyInsuranceCustomer { get; set; }

        [Description("Phí BH của Lender")]
        public string moneyInsuranceLender { get; set; }

        [Description("Link KH")]
        public string linkCustomer { get; set; }

        [Description("Link Lender")]
        public string linkLender { get; set; }

    }

    [Description("Báo cáo VAT HUB")]
    public class ExcelReportVatHub
    {
        [Description("Mẫu số")]
        public string Denominator { get; set; }// Mẫu số
        [Description("Ký hiệu")]
        public string Notation { get; set; }// Ký hiệu
        [Description("Số đơn hàng")]
        public string OrderNumber { get; set; }// số đơn hàng
        [Description("Ngày HĐ")]
        public string TransactionDate { get; set; }
        [Description("Tên người mua")]
        public string CustomerName { get; set; }
        [Description("Mã khách hàng")]
        public int CustomerID { get; set; }
        [Description("Tên đơn vị")]
        public string UnitName { get; set; }

        [Description("Mã số thuế")]
        public string Taxcode { get; set; }
        [Description("Địa chỉ")]
        public string Address { get; set; }
        [Description("Tài khoản")]
        public string Account { get; set; }
        [Description("Ngân hàng")]
        public string BankName { get; set; }
        [Description("Số HĐ")]
        public string ContactCode { get; set; }

        [Description("HT thanh toán")]
        public string TypePayment { get; set; }// hình thức thanh toán

        [Description("Mã hàng")]
        public string ItemCode { get; set; }

        [Description("Tên hàng")]
        public string CommodityName { get; set; }// tên hàng
        [Description("DVT")]
        public string DVT { get; set; }

        [Description("Số lượng")]
        public string Quantily { get; set; }

        [Description("Đơn giá")]
        public string UnitPrice { get; set; }

        [Description("Tiền")]
        public string FeeNotVat { get; set; }

        [Description("% CK")]
        public string CK { get; set; }

        [Description("Tiền chiết khấu")]
        public string Discount { get; set; }

        [Description("% thuế")]
        public int TaxPercentage { get; set; }// % thuế

        [Description("Tiền thuế")]
        public string Vat { get; set; }

        [Description("Thành tiền")]
        public string TotalMoney { get; set; }
        [Description("Tên phòng GĐ")]
        public string HubName { get; set; }

    }

    [Description("Báo cáo Phí phạt VAT HUB")]
    public class ExcelReportPenaltyFeeVatHub
    {
        [Description("Địa chỉ")]
        public string Address { get; set; }

        [Description("Mã HĐ giải ngân")]
        public string CommodityName { get; set; }

        [Description("Tên Khách hàng")]
        public string customerName { get; set; }

        [Description("Ngày giải ngân")]
        public string disbursementDate { get; set; }

        [Description("Số tiền giải ngân")]
        public string totalMoneyDisbursement { get; set; }

        [Description("Đối tác cho vay")]
        public string lenderName { get; set; }

        [Description("Trạng thái")]
        public string status { get; set; }

        [Description("Loại BH")]
        public string partnerCode { get; set; }

        [Description("Phí BH của KH")]
        public string moneyInsuranceCustomer { get; set; }

        [Description("Phí BH của Lender")]
        public string moneyInsuranceLender { get; set; }

        [Description("Link KH")]
        public string linkCustomer { get; set; }

        [Description("Link Lender")]
        public string linkLender { get; set; }

    }

    [Description("Báo cáo Gcash")]
    public class ExcelReportVatGCash
    {
        [Description("Địa chỉ")]
        public string Address { get; set; }

        [Description("Mã HĐ giải ngân")]
        public string CommodityName { get; set; }

        [Description("Tên Khách hàng")]
        public string customerName { get; set; }

        [Description("Ngày giải ngân")]
        public string disbursementDate { get; set; }

        [Description("Số tiền giải ngân")]
        public string totalMoneyDisbursement { get; set; }

        [Description("Đối tác cho vay")]
        public string lenderName { get; set; }

        [Description("Trạng thái")]
        public string status { get; set; }

        [Description("Loại BH")]
        public string partnerCode { get; set; }

        [Description("Phí BH của KH")]
        public string moneyInsuranceCustomer { get; set; }

        [Description("Phí BH của Lender")]
        public string moneyInsuranceLender { get; set; }

        [Description("Link KH")]
        public string linkCustomer { get; set; }

        [Description("Link Lender")]
        public string linkLender { get; set; }

    }

    [Description("Báo cáo Hub với Gcash")]
    public class ExcelReportVatHUbWithGCash
    {
        [Description("Địa chỉ")]
        public string Address { get; set; }

        [Description("Mã HĐ giải ngân")]
        public string CommodityName { get; set; }

        [Description("Tên Khách hàng")]
        public string customerName { get; set; }

        [Description("Ngày giải ngân")]
        public string disbursementDate { get; set; }

        [Description("Số tiền giải ngân")]
        public string totalMoneyDisbursement { get; set; }

        [Description("Đối tác cho vay")]
        public string lenderName { get; set; }

        [Description("Trạng thái")]
        public string status { get; set; }

        [Description("Loại BH")]
        public string partnerCode { get; set; }

        [Description("Phí BH của KH")]
        public string moneyInsuranceCustomer { get; set; }

        [Description("Phí BH của Lender")]
        public string moneyInsuranceLender { get; set; }

        [Description("Link KH")]
        public string linkCustomer { get; set; }

        [Description("Link Lender")]
        public string linkLender { get; set; }

    }
    [Description("Excel Thẻ ngân hàng")]
    public class ExcelBankCard
    {
        [Description("Thẻ ngân hàng")]
        public string BankName { get; set; }
        [Description("Khách hàng")]
        public string CustomerName { get; set; }
        [Description("Loại phiếu")]
        public string InvoiceSubTypeName { get; set; }
        [Description("Trạng thái")]
        public string StatusName { get; set; }
        [Description("Người tạo")]
        public string UserNameCreate { get; set; }
        [Description("Ghi chú")]
        public string Note { get; set; }
        [Description("Ngày giao dịch")]
        public string TransactionDate { get; set; }
        [Description("Thời gian nhập liệu")]
        public string CreateDate { get; set; }
        [Description("Thu")]
        public string MoneyReceipt { get; set; }
        [Description("Chi")]
        public string MoneyPaySlip { get; set; }
    }
}
