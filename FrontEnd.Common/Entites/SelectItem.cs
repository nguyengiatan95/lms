﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Entites
{
    public class SelectItem
    {
        public string Text { get; set; }
        public long Value { get; set; }
    }
}
