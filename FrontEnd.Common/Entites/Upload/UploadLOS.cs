﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Entites.Upload
{
    public class UploadLOS
    {
        public string FileName { get; set; }
        public string LocalPath { get; set; }
        public string S3Path { get; set; }
        public string Source
        {
            get;
            set;
        } = "LOS";
        public string FullPath { get; set; }
    }

    public class LOSBaseResponse<T> where T : class
    {
        public long Code { get; set; }
        public string Message { get; set; }
        public long Error { get; set; }
        public T Data { get; set; }
    }
}
