﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Models.Bank
{
    public class TblBankCard
    {
        public long BankCardID { get; set; }
        public string BankCode { get; set; }
        public string BranchName { get; set; }
        public string AliasName { get; set; }
        public string AccountNumber { get; set; }
        public string NumberAccount { get; set; }
        public string AccountName { get; set; }
        public string AccountHolder { get; set; }
        public DateTime CreateDate { get; set; }
        public int Status { get; set; }
        public int ParentID { get; set; }
        public long TotalMoney { get; set; }
        public long BankID { get; set; }
    }

    public class BankCardReq
    {
        public long BankCardID { get; set; }
        public long BankID { get; set; }
        public string NumberAccount { get; set; } // số tài khoản
        public string AccountHolderName { get; set; } // chủ tài khoản
        public string BranchName { get; set; } // teen chi nhanh
        public string AliasName { get; set; } // tên tài khoản hiển thị
        public int ParentID { get; set; }
        public long TotalMoney { get; set; }
        public long UserID { get; set; }
        public int Status { get; set; }
        public int TypePurpose { get; set; }
    }
    public class BankCardDatatable
    {
        public long BankCardID { get; set; }
        public string BankCode { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public string AccountHolder { get; set; }
        public DateTime CreateDate { get; set; }
        public int Status { get; set; }
        public long RowID { get; set; }
        public long TotalCount { get; set; }
        public long? BankID { get; set; }
        public string NumberAccount { get; set; }
        public DateTime? CreateOn { get; set; }
        public DateTime? ModifyOn { get; set; }
        public string AccountHolderName { get; set; }
        public string BranchName { get; set; }
        public short? TypePurpose { get; set; }
        public string AliasName { get; set; }
        public string AliasNameForFast { get; set; }
        public int? ParentID { get; set; }
        public long? TimaBankCardID { get; set; }
        public long TotalMoney { get; set; }
        public string strCreateOn { get { return CreateOn.Value.ToString("dd/MM/yyyy"); } }
    }
}
