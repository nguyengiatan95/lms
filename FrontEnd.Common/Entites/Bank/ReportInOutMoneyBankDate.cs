﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Entites.Bank
{
    public class ReportInOutMoneyBankDate
    {
        public DateTime ForDate { get; set; }

        public long BankCardID { get; set; } 
        public long MoneyBeginDate { get; set; }

        public long MoneyEndDate { get; set; }
        public long MoneyInBound { get; set; }
        public long MoneyOutBound { get; set; }
        public string BankCardName { get; set; }
        public long TotalMoneyTransaction { get; set; }
    }
}
