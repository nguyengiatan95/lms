﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace FrontEnd.Common.Entites.Excel
{
    public class BM01Excel
    {
        public List<DepositData> LstDeposit { get; set; }
        public List<LenderInfo> LenderInfos { get; set; }
    }
    public class DepositData
    {
        [Description("Mã Nhà Đầu Tư")]
        public string LenderCode { get; set; }
        [Description("Tên khách hàng")]
        public string CustomerName { get; set; }
        [Description("LoanId")]
        public long LoanID { get; set; }
        [Description("Mã TC")]
        public string ContractCode { get; set; }
        [Description("Ngày giải ngân")]
        public DateTime FromDate { get; set; }
        [Description("Ngày Đáo Hạn")]
        public DateTime ToDate { get; set; }
        [Description("Số tiền giải Ngân")]
        public long TotalMoney { get; set; }
        [Description("Lãi cọc")]
        public long InterestMoney { get; set; }
        [Description("Gốc cọc")]
        public long TotalMoneyCurrent { get; set; }
        [Description("Tổng tiền cọc")]
        public long TotalMoneyDeposit { get; set; }
        [Description("Trạng thái")]
        public string StatusName { get; set; }
        //[Description("Ngày phải trả nợ")]
        //public DateTime NextDate { get; set; }
        [Description("Ngày phát sinh đặt cọc")]
        public DateTime CreateDate { get; set; }
        public DateTime LastPaySchedule { get; set; }
        public DateTime EndDaySchedule { get; set; }
        public int CountDay { get; set; }
        public long LenderID { get; set; }
        public long InterestAfterTax { get; set; }
        public long TotalMoneyDepositAfterTax { get; set; }
    }

    public class LenderInfo
    {
        public string LenderCode { get; set; }
        public string FullName { get; set; }
        public long TotalMoney { get; set; }
        public long LenderID { get; set; }
        public string Represent { get; set; }
        public string NumberBankCard { get; set; }
        public string BankCode { get; set; }
    }
}
