﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Entites.Management
{
    public class Shop
    {
        public int ShopID { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        public short? Status { get; set; }

        public DateTime? CreatedAt { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public int? CityId { get; set; }

        public int? DistrictId { get; set; }

        public int? WardId { get; set; }

        public string Note { get; set; }

        public int? Received { get; set; }

        public bool? IsReceiveLoan { get; set; }

        public string Email { get; set; }

        public string InviteCode { get; set; }

        public string Lat { get; set; }

        public string Long { get; set; }

        public int? Company { get; set; }

        public int? HubType { get; set; }

        public int? UserId { get; set; }

        public string WebhookSlack { get; set; }
    }
}
