﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Entites.Management
{
    public class TblDepartment
    {
        public long DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public DateTime CreateDate { get; set; }
        public int Status { get; set; }
        public long ParentID { get; set; }
        public long AppID { get; set; }
    }
    public class DepartmentDatatable : TblDepartment
    {
        public long RowID { get; set; }
        public long TotalCount { get; set; }
        public string strCreateDate { get { return CreateDate.ToString("dd/MM/yyyy"); } }
        public string ParentName { get; set; }
    }

}
