﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Entites.Management
{
    public class TblPermissionGroup
    {
        public long GroupPermissionID { get; set; }

        public long GroupID { get; set; }

        public long PermissionID { get; set; }
    }

    public class JSTreeModel
    {
        public string id { get; set; }
        public string text { get; set; }
        public State state { get; set; }
        public string IsMenu { get; set; }
        //public string ControllerName { get; set; }
        //public string ActionName { get; set; }
        public List<JSTreeModel> children { get; set; }
        public string icon { get; set; }
        public JSTreeModel()
        {
            children = new List<JSTreeModel>();
        }
        public string Link
        {
            get;set;
        }
    }

    public class State
    {
        public bool opened { get; set; } = true;
        public bool disabled { get; set; } = false;
        public bool selected { get; set; } = false;
    }
}
