﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Entites.Management
{
    public class Lender
    {
        public int LenderID { get; set; }
        public string FullName { get; set; }
    }
}
