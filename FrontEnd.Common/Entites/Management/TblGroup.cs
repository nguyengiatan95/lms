﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Entites.Management
{
    public class TblGroup
    {
        public long GroupID { get; set; }
        public string GroupName { get; set; }
        public int Status { get; set; } 
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public long TotalCount { get; set; }
    }
}
