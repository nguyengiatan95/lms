﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Entites.Management
{
    public class UserDetail
    {
        public long UserID { get; set; }

        public long? DepartmentID { get; set; }
         
        public string FullName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public DateTime? CreateDate { get; set; }

        public string Email { get; set; }

        public int? Status { get; set; }
        public string Token { get; set; }
        public List<TblGroup> Groups { get; set; }

    }
}
