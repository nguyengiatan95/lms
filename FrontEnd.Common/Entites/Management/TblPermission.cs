﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Entites.Management
{
    public class TblPermission
    {
        public long PermissionID { get; set; }
        public string DisplayText { get; set; }
        public string LinkApi { get; set; }
        public int IsActive { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public long CreateBy { get; set; }
        public long ModfifyBy { get; set; }
    }

    public class PermissionDatatable : TblPermission
    {
        public long RowID { get; set; }
        public string StrCreateDate
        {
            get { return CreateDate.Year < 1000 ? "" : CreateDate.ToString("dd/MM/yyyy HH:mm"); }
        }
        public string StrModifyDate
        {
            get { return ModifyDate.Year < 1000 ? "": ModifyDate.ToString("dd/MM/yyyy HH:mm"); }
        }
        public long TotalCount { get; set; }
    }
}

