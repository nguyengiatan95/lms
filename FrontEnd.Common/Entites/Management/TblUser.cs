﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Entites.Management
{
    public class TblUser
    {
        public long UserID { get; set; }

        public long? DepartmentID { get; set; }


        public string FullName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public DateTime CreateDate { get; set; }

        public string Email { get; set; }

        public int? Status { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public long TotalCount { get; set; }
        public string strCreateDate { get { return CreateDate.ToString("dd/MM/yyyy"); } }
        public List<TblUserGroup> LstUserGroup { get; set; }
    }
    public class UserToken
    {  
        public string Token { get; set; } 
    }
}
