﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Entites.Management
{
    public class TblUserGroup
    {
        public long UserGroupID { get; set; }

        public long? UserID { get; set; }

        public long? GroupID { get; set; }
    }
}
