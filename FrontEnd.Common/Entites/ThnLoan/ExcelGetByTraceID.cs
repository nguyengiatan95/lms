﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace FrontEnd.Common.Entites.ThnLoan
{
    [Description("Báo cáo đơn vay")]
    public class ExcelGetByTraceID
    {
        [Description("LoanID")]
        public int LoanID { get; set; }
        [Description("Mã TC")]
        public string LoanContactCode { get; set; }
        [Description("Tên KH")]
        public string CustomerName { get; set; }
        [Description("Quận huyện tạm trú")]
        public string CustomerDistrictName { get; set; }
        [Description("Thành phố tạm trú")]
        public string CustomerCityName { get; set; }
        [Description("Dư nợ gốc so với tháng trước")]
        public int POSBOM { get; set; }
        [Description("Số ngày DPD so với tháng trước")]
        public int DPDBOM { get; set; }
        [Description("Tên nhân viên 1")]
        public string AssgineeNameFirst { get; set; }
        [Description("Tên nhân viên 2")]
        public string AssigneeNameSecond { get; set; }
        [Description("Trưởng bộ phận")]
        public string AssigneeNameLeader { get; set; }
        [Description("Trưởng phòng")]
        public string AssigneeNameDepartment { get; set; }
        [Description("Tổng tiền thu được")]
        public string LoanTotalMoneyReceipt { get; set; }
        [Description("Dpd hiện tại")]
        public long DPDCurrent { get; set; }
        [Description("Dự nợ gốc hiện tại")]
        public string POSCurrent { get; set; }
        [Description("Nhóm nợ của đơn vay tháng trước")]
        public string BucketBOM { get; set; }
        [Description("Nhóm nợ hiện tại")]
        public string BucketCurrent { get; set; }
        [Description("Mã sp")]
        public string LoanProductName { get; set; }
        [Description("Ngày tác nghiệp gần nhất")]
        public string LoanLatestComment { get; set; }
        [Description("Người tác nghiệp")]
        public string AssigneeNameLatestComment { get; set; }
        [Description("Mã tác nghiệp")]
        public string CommentReasonCode { get; set; }
        [Description("Nội dung")]
        public string CommentContent { get; set; }
        [Description("Năm nợ xấu")]
        public int LoanBadDebtYear { get; set; }
        [Description("Trạng thái bảo hiểm")]
        public string LoanInsuranceStatusName { get; set; }
        [Description("Đối tượng đền bù bảo hiểm")]
        public string LoanInsurancePartnerName { get; set; }
        [Description("Miền")]
        public string LoanDomainName { get; set; }
        [Description("Mã KH")]
        public int CustomerID { get; set; }
        [Description("Ngày giải ngân")]
        public string LoanFromDate { get; set; }
        [Description("Kết thúc dự kiến")]
        public string LoanToDate { get; set; }
        [Description("Kết thúc thực tế")]
        public string LoanFinishedDate { get; set; }
        [Description("Ngày sinh KH")]
        public string CustomerBirthDate { get; set; }
        [Description("Địa chỉ KH")]
        public string CustomerAddress { get; set; }
        [Description("Dư nợ giải ngân")]
        public string LoanTotalMoneyCurrent { get; set; }
        [Description("Hình thức thanh toán")]
        public string LoanRateTypeName { get; set; }
        [Description("Ngày đến hạn")]
        public string LoanNextDate { get; set; }
        [Description("Chu kỳ thanh toán")]
        public long LoanFequency { get; set; }
        [Description("Tiền lãi đóng hợp đồng")]
        public string LoanInterestClosed { get; set; }
        [Description("Tiền tư vấn đóng hợp đồng")]
        public string LoanConsultantFeeClosed { get; set; }
        [Description("Phí phạt trả châm, nợ cũ, phí phạt treo")]
        public string LoanMoneyFineLateClosed { get; set; }
        [Description("Phí tất toán trước hạn")]
        public string LoanMoneyFineOriginalClosed { get; set; }
        [Description("Số dư KH")]
        public string CustomerTotalMoney { get; set; }
        [Description("Tiền gốc được hạch toáng")]
        public string LoanTotalMoneyOriginalReceipt { get; set; }
        [Description("Tiền lãi được hạch toán")]
        public string LoanTotalMoneyInterestReceipt { get; set; }
        [Description("Tiền phí được hạch toán + tiền dịch vụ")]
        public string LoanTotalMoneyConsultantReceipt { get; set; }
        [Description("Tài khoản Epay")]
        public string CustomerEpayAccountName { get; set; }
        [Description("Có thiết bị định vị ko, true, false")]
        public string LoanIsLocate { get; set; }
        [Description("Cửa hàng")]
        public string LoanShopConsultantName { get; set; }
        [Description("Biển kiểm soát xe máy - LOS cung cấp")]
        public string LoanBKS { get; set; }
        [Description("Tái vay")]
        public int LoanTopup { get; set; }
        [Description("Ngày dự thu")]
        public string LoanPreparationDate { get; set; }
        [Description("Tiền dự thu")]
        public string LoanPreparationMoney { get; set; }
        [Description("Rủi ro")]
        public string CustomerPayTypeName { get; set; }
        [Description("Mã Lender")]
        public string LenderCode { get; set; }
    }

    [Description("Lịch sử nạp tiền")]
    public class ExcelTransactionBankCard
    {
        [Description("Khách hàng")]
        public string CustomerName { get; set; }
        [Description("Ngân hàng")]
        public string AliasName { get; set; }
        
        [Description("Người giao dịch")]
        public string UserCreate { get; set; }

        [Description("Số tiền")]
        public long TotalMoney { get; set; }
        [Description("Thời gian giao dịch")]
        public DateTime TransactionDate { get; set; }
        [Description("Ngày tạo")]
        public DateTime CreateDate { get; set; }
        [Description("Nội dung")]
        public string Note { get; set; }
        [Description("Trạng thái")]
        public string StrStatus { get; set; }
    }

    [Description("Giao dịch hạch toán")]
    public class ExcelPaymentCustomer
    {
        [Description("Ngày hạch toán")]
        public DateTime CreateDate { get; set; }
        [Description("Mã TC")]
        public string LoanContractCode { get; set; }
        [Description("LoanID")]
        public long LoanID { get; set; }
        [Description("AgLoanID")]
        public long AgLoanID { get; set; }
        [Description("Khách hàng")]
        public string CustomerName { get; set; }
        [Description("N﻿ote")]
        public string ActionName { get; set; }
        [Description("Cửa hàng")]
        public string OwnerShopName { get; set; }// cửa hàng giải ngân
        [Description("Tiền gốc")]
        public long MoneyOriginal { get; set; }
        [Description("Tiền Phí Tư Vấn")]
        public long MoneyConsultant { get; set; }
        [Description("Tiền Lãi")]
        public long MoneyInterest { get; set; }
        [Description("Tiền Phí dịch vụ")]
        public long MoneyService { get; set; }
        [Description("Trả trước gốc Lender được hưởng")]
        public long MoneyFineOrginalLender { get; set; }
        [Description("Trả trước gốc Tima được hưởng")]
        public long MoneyFineOrginalTima { get; set; }
        [Description("Phí phạt trả chậm của Tima")]
        public long MoneyFineLateTima { get; set; }
        [Description("Phí phạt trả chậm của Lender")]
        public long MoneyFineLateLender { get; set; }
    }

    [Description("Báo cáo nhóm nợ")]
    public class HistoryTrackingLoanInMonth
    {
        [Description("LoanID")]
        public long LoanID { get; set; }
        [Description("Mã TC")]
        public string ContractCode { get; set; }
        [Description("Tên khách hàng")]
        public string CustomerName { get; set; }
        [Description("Địa chỉ")]
        public string Address { get; set; }
        [Description("Ngày giải ngân")]
        public DateTime FromDate { get; set; }
        [Description("Ngày đáo hạn")]
        public DateTime ToDate { get; set; }
        [Description("Nhóm Nợ")]
        public string DebtType { get; set; }
        [Description("Dư nợ")]
        public long TotalMoneyCurrent { get; set; }
        [Description("Số tiền thu được")]
        public long TotalMoneyReceived { get; set; }
        [Description("Ngày kết thúc thực tế")]
        public DateTime? FinishDate { get; set; }

    }
}
