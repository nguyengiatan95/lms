﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace FrontEnd.Common.Entites.ThnLoan
{
    [Description("Báo cáo dư nợ đầu ngày")]
    public class ExcelReportLoanDebtDailyModel
    {
        [Description("Ngày chốt")]
        public DateTime ReportDate { get; set; }

        [Description("LoanID")]
        public long TimaLoanID { get; set; }
        [Description("CustomerID")]
        public long CustomerID { get; set; }

        [Description("Mã hợp đồng đăng ký vay")]
        public string LosLoanCode { get; set; }

        [Description("Mã hợp đồng sau giải ngân")]
        public string LmsContractCode { get; set; }

        [Description("Họ Tên KH")]
        public string CustomerName { get; set; }

        [Description("Ngày giải ngân")]
        public DateTime FromDate { get; set; }

        [Description("Ngày phải đáo hạn")]
        public DateTime ToDate { get; set; }

        [Description("Dư nợ đang cho vay")]
        public long TotalMoneyCurrent { get; set; }

        [Description("Tỉnh thành đơn vay")]
        public string CityName { get; set; }

        [Description("Quận huyện đơn vay")]
        public string DistrictName { get; set; }

        [Description("Ngày phải đóng tiền")]
        public DateTime NextDate { get; set; }

        [Description("Ngày trễ (DPD)")]
        public int DPD { get; set; }

        [Description("Nhóm nợ")]
        public string TypeDebtName { get; set; }

        [Description("Tổng tiền cắt 1 kỳ (CK)")]
        public long TotalMoneyNeedPay { get; set; }

        [Description("Tiền gốc CK")]
        public long MoneyOriginalNeedPay { get; set; }

        [Description("Tiền lãi CK")]
        public long MoneyInteresetNeedPay { get; set; }

        [Description("Tiền tư vấn CK")]
        public long MoneyConsultantNeedPay { get; set; }

        [Description("Tiền dịch vụ CK")]
        public long MoneyServiceNeedPay { get; set; }

        [Description("Tiền phạt trả chậm CK")]
        public long MoneyFineLateNeedPay { get; set; }

        [Description("Tổng tiền tất toán TT")]
        public long TotalMoneyCloseLoan { get; set; }

        [Description("Tiền gốc TT")]
        public long MoneyOriginalCloseLoan { get; set; }

        [Description("Tiền lãi TT")]
        public long MoneyInterestCloseLoan { get; set; }

        [Description("Tiền tư vấn TT")]
        public long MoneyConsultantCloseLoan { get; set; }

        [Description("Tiền dịch vụ TT")]
        public long MoneyServiceCloseLoan { get; set; }

        [Description("Tiền phạt trả chậm TT")]
        public long MoneyFineLateCloseLoan { get; set; }

        [Description("Tiền phạt trả trước TT")]
        public long MoneyFineOriginalCloseLoan { get; set; }

        [Description("Mã đối tác")]
        public string LenderCode { get; set; }

        [Description("Gói vay")]
        public string ProductName { get; set; }

        [Description("Mã đơn vị quản lý khoản vay")]
        public string OwnerShopName { get; set; }
        [Description("Mã đơn vị tư vấn khoản vay")]
        public string ConsultantShopName { get; set; }

        [Description("Trạng thái đặt cọc")]
        public string LoanStatusDispositName { get; set; }

        [Description("Trạng thái hồ sơ bảo hiểm")]
        public string LoanStatusInsuranceName { get; set; }
    }
}
