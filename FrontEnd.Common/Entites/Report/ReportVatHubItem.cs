﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace FrontEnd.Common.Entites.Report
{
    public class ReportVatHubItem
    {
        [Description("Cửa hàng")]
        public string HubName { get; set; }
        [Description("Loại giao dịch")]
        public string TypeTransaction { get; set; }
        [Description("Mẫu số")]
        public string Denominator { get; set; }// Mẫu số
        [Description("Ký hiệu")]
        public string Notation { get; set; }// Ký hiệu
        [Description("Số đơn hàng")]
        public string OrderNumber { get; set; }// số đơn hàng
        [Description("Ngày HĐ")]
        public DateTime? TransactionDate { get; set; }
        [Description("Tên người mua")]
        public string CustomerName { get; set; }
        [Description("Mã khách hàng")]
        public int CustomerID { get; set; }
        [Description("Tên đơn vị")]
        public string UnitName { get; set; }

        [Description("Mã số thuế")]
        public string Taxcode { get; set; }
        [Description("Địa chỉ")]
        public string Address { get; set; }
        [Description("Tài khoản")]
        public string Account { get; set; }
        [Description("Ngân hàng")]
        public string BankName { get; set; }
        [Description("Số HĐ")]
        public string ContactCode { get; set; }

        [Description("HT thanh toán")]
        public string TypePayment { get; set; }// hình thức thanh toán

        [Description("Mã hàng")]
        public string ItemCode { get; set; }

        [Description("Tên hàng")]
        public string CommodityName { get; set; }// tên hàng
        [Description("DVT")]
        public string DVT { get; set; }

        [Description("Số lượng")]
        public string Quantily { get; set; }

        [Description("Đơn giá")]
        public string UnitPrice { get; set; }

        [Description("Tiền")]
        public long FeeNotVat { get; set; }

        [Description("% CK")]
        public string CK { get; set; }

        [Description("Tiền chiết khấu")]
        public string Discount { get; set; }

        [Description("% thuế")]
        public int TaxPercentage { get; set; }// % thuế

        [Description("Tiền thuế")]
        public long Vat { get; set; }

        [Description("Thành tiền")]
        public long TotalMoney { get; set; }

    }

    [Description("Báo cáo VAT HUB với GCASH")]
    public class ReportVatHubItemExcel
    {
        [Description("Mẫu số")]
        public string Denominator { get; set; }// Mẫu số
        [Description("Ký hiệu")]
        public string Notation { get; set; }// Ký hiệu
        [Description("Số đơn hàng")]
        public string OrderNumber { get; set; }// số đơn hàng
        [Description("Ngày HĐ")]
        public string TransactionDate { get; set; }
        [Description("Tên người mua")]
        public string CustomerName { get; set; }
        [Description("Mã khách hàng")]
        public int CustomerID { get; set; }
        [Description("Tên đơn vị")]
        public string UnitName { get; set; }
        [Description("Mã số thuế")]
        public string Taxcode { get; set; }
        [Description("Địa chỉ")]
        public string Address { get; set; }
        [Description("Tài khoản")]
        public string Account { get; set; }
        [Description("Ngân hàng")]
        public string BankName { get; set; }
        [Description("Số HĐ")]
        public string ContactCode { get; set; }
        [Description("HT thanh toán")]
        public string TypePayment { get; set; }// hình thức thanh toán
        [Description("Mã hàng")]
        public string ItemCode { get; set; }

        [Description("Tên hàng")]
        public string CommodityName { get; set; }// tên hàng 
        [Description("DVT")]
        public string DVT { get; set; }
        [Description("Số lượng")]
        public string Quantily { get; set; }
        [Description("Đơn giá")]
        public string UnitPrice { get; set; }
        [Description("Tiền")]
        public string FeeNotVat { get; set; }
        [Description("% CK")]
        public string CK { get; set; }
        [Description("Tiền chiết khấu")]
        public string Discount { get; set; }
        [Description("% thuế")]
        public int TaxPercentage { get; set; }// % thuế

        [Description("Tiền thuế")]
        public string Vat { get; set; }
        [Description("Thành tiền")]
        public string TotalMoney { get; set; }

        //[Description("Cửa hàng")]
        //public string HubName { get; set; }
        //[Description("Loại giao dịch")]
        //public string TypeTransaction { get; set; }
    }
}
