﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace FrontEnd.Common.Entites.Report
{
    [Description("Báo cáo dư nợ đầu ngày")]
    public class ReportDayPlanItem
    {
        [Description("Mã TC")]
        public string ContractCode { get; set; }
        [Description("Tên khách hàng")]
        public string CustomerName { get; set; }
        [Description("Giỏ")]
        public string TypeName { get; set; }
        [Description("Mã NV xử lý")]
        public string CodeStaff { get; set; }
        [Description("Mã trưởng nhóm")]
        public string Leader { get; set; }
        [Description("Mã trưởng phòng")]
        public string Manager { get; set; }
        [Description("GĐ THN")]
        public string Director { get; set; }
        [Description("Ngày lên kế hoạch")]
        public DateTime DayPlan { get; set; }
        [Description("Ngày thực hiện kế hoạch")]
        public DateTime HandlingDate { get; set; }
        [Description("Ngày kế thúc kế hoạch")]
        public DateTime EndDate { get; set; }
        [Description("Trạng thái")]
        public string Status { get; set; }
    }
}
