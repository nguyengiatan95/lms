﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Entites.Accountants
{
    public class Invoice
    {
        public long InvoiceID { get; set; }
        public int InvoiceType { get; set; }
        public string InvoiceTypeName { get; set; }
        public int InvoiceSubType { get; set; }
        public string InvoiceSubTypeName { get; set; }
        public string InvoiceCode { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public string ShopName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string UserNameCreate { get; set; }
        public long TotalMoney { get; set; }
        public string Note { get; set; }
        public string SourceName { get; set; }
        public string DestinationName { get; set; }
        public long TransactionMoney { get; set; }
        public long TotalMoneyNoSymmetry { get; set; }
        public long TotalMoneySymmetry { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string Description
        {
            get
            {
                return $"Tạo {this.InvoiceTypeName} {this.SourceName} với nội dung: {this.Note}";
            }
        }
        public long SumTotalMoney { get; set; }
    }
    public class VerifyInvoice
    { 
        public string ImgSrc { get; set; } 
    }
}
