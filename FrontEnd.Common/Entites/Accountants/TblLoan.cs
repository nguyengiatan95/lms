﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Entites.Accountants
{
    public class TblLoan
    {
        public long LoanID { get; set; }
        public string ContactCode { get; set; }
        public string CustomerName { get; set; } 
        public string ProductName { get; set; }
        public string LenderCode { get; set; }
        public long LoanTotalMoneyDisbursement { get; set; }
        public long LoanTotalMoneyCurrent { get; set; }
        public long LoanTotalMoneyPaid { get; set; }
        public DateTime LoanInterestStartDate { get; set; }
        public int LoanTime { get; set; }
        public string LoanStatusName { get; set; }
        public int LoanStatusID { get; set; }
        public DateTime LoanInterestPaymentNextDate { get; set; }
        public long CustomerTotalMoney { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public int CountDownNextdate { get; set; }
        public string MaterialAt { get; set; }
    }
}
