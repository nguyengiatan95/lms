﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrontEnd.Common.Models.Loan
{ 
    public class LoanDatatableModel : DatatableBase
    {
        public LoanDatatableModel(IFormCollection form) : base(form)
        {
            FromDate = form["query[FromDate]"].FirstOrDefault();
            ToDate = form["query[ToDate]"].FirstOrDefault();
            LoanCode = form["query[LoanCode]"].FirstOrDefault();
            LoanStatus = int.Parse(form["query[LoanStatus]"].FirstOrDefault()??"0"); 
            ShopID = int.Parse(form["query[ShopID]"].FirstOrDefault() ?? "0");
            LenderID = int.Parse(form["query[LenderID]"].FirstOrDefault() ?? "0");
            TotalRow = int.Parse(form["query[TotalRow]"].FirstOrDefault() ?? "0");
            ProductID = int.Parse(form["query[ProductID]"].FirstOrDefault() ?? "0");
        }  
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string LoanCode { get; set; }
        public int LoanStatus { get; set; }
        public int ShopID { get; set; }
        public int LenderID { get; set; }
        public long TotalRow { get; set; }
        public int ProductID { get; set; }
    }
    public class RequestLoanItemViewModel
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string LoanCode { get; set; }
        public int LoanStatus { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int ShopID { get; set; }
        public long TotalRow { get; set; }
    }
    public class RequestReport
    { 
        public int ShopID { get; set; } 
    }

    public class RequestDeleteLoan
    {
        public long LoanID { get; set; }
        public long CreateBy { get; set; }

    }
}
