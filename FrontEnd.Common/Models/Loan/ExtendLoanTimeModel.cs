﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Models.Loan
{
    public class ExtendLoanTimeModel
    {
        public int LoanID { get; set; }
        public string Note { get; set; }
        public int PeriodExtend { get; set; }
    }
}
