﻿using FrontEnd.Common.Helpers;
using LMS.Common.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Models
{
	public class ResponseActionResult<T>
	{
		public int Result { get; set; }
		public int Total { get; set; }
		public string Message { get; set; } 
		public T Data { get; set; }
		public void SetSucces()
		{
			Result = (int)LMS.Common.Constants.ResponseAction.Success;
			Message = MessageConstant.Success;
		}
		public void SetError()
		{
			Result = (int)LMS.Common.Constants.ResponseAction.Error;
			Message = MessageConstant.ErrorInternal;
		}
	}
	public class ResponseForDataTable<T>
	{
		public int Draw { get; set; }
		public int RecordsFiltered { get; set; }
		public int RecordsTotal { get; set; }
		public T Data { get; set; } 
	}
	public class ResponseActionResult
	{
		public int Result { get; set; }
		public int Total { get; set; }
		public string Message { get; set; }
		public object Data { get; set; }
	}
	public class DataResult<T>
	{
		public int Draw { get; set; }
		public int RecordsFiltered { get; set; }
		public int RecordsTotal { get; set; }
		public T Data { get; set; }
	}
	public class KTDatatableResult
	{
		public KTDatatableMeta meta { get; set; } 
		public object data { get; set; }
	}
	public class KTDatatableMeta
	{
		public int page { get; set; }
		public int pages { get; set; }
		public int perpage { get; set; }
		public int total { get; set; }
		public string sort { get; set; }
		public string field { get; set; } 
	} 
}
