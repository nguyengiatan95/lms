﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrontEnd.Common.Models.Bank
{
    public class BankModel : DatatableBase
    {
        public BankModel(IFormCollection form) : base(form)
        {
            //FromDate = (form["query[FromDate]"].FirstOrDefault(), "dd-MM-yyyy", null);
            FromDate = form["query[FromDate]"].FirstOrDefault();
            ToDate = form["query[ToDate]"].FirstOrDefault();
            //ToDate = DateTime.ParseExact(form["query[ToDate]"].FirstOrDefault(), "dd-MM-yyyy", null);
            BankCardID = int.Parse(form["query[BankCardID]"].FirstOrDefault() ?? "0");
            InvoiceType = int.Parse(form["query[InvoiceType]"].FirstOrDefault() ?? "0");
        }
        public BankModel() { }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int BankCardID { get; set; }
        public int InvoiceType { get; set; }
    }
}