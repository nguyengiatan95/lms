﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrontEnd.Common.Models.Bank
{
    public class ManageBankCardModel : DatatableBase
    {
        public ManageBankCardModel(IFormCollection form) : base(form)
        {
            BankCardID = int.Parse(form["query[BankCardID]"].FirstOrDefault() ?? "0");
            GeneralSearch = form["query[txt_search_bank]"].FirstOrDefault();
            Status = int.Parse(form["query[sl_search_status]"].FirstOrDefault() ?? "0");
        }
        public int BankCardID { get; set; }
        public string GeneralSearch { get; set; }
        public int Status { get; set; }
    }
}
