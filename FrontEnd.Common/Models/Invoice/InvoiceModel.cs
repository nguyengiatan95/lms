﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrontEnd.Common.Models.Invoice
{
    public class InvoiceDatatableModel : DatatableBase
    {
        public InvoiceDatatableModel(IFormCollection form) : base(form)
        {
            FromDate = form["query[FromDate]"].FirstOrDefault();
            ToDate = form["query[ToDate]"].FirstOrDefault();
            LoanCode = form["query[LoanCode]"].FirstOrDefault();
            Status = int.Parse(form["query[Status]"].FirstOrDefault() ?? "-1");
            ShopID = int.Parse(form["query[ShopID]"].FirstOrDefault() ?? "-1");
            TotalRow = int.Parse(form["query[TotalRow]"].FirstOrDefault() ?? "0");
            BankCardID = int.Parse(form["query[BankCardID]"].FirstOrDefault() ?? "-1");
            InvoiceType = int.Parse(form["query[InvoiceType]"].FirstOrDefault() ?? "-1");
            InvoiceSubType = int.Parse(form["query[InvoiceSubType]"].FirstOrDefault() ?? "-1");
            MoneySearch = long.Parse(form["query[numberSearch]"].FirstOrDefault() ?? "0");
        }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string LoanCode { get; set; }
        public int Status { get; set; }
        public int ShopID { get; set; }
        public int BankCardID { get; set; }
        public long TotalRow { get; set; }
        public int InvoiceType { get; set; }
        public int InvoiceSubType { get; set; }
        public long MoneySearch { get; set; }
    }
}
