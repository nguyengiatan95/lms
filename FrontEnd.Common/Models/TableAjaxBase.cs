﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Models
{
    public class TableAjaxBase
    {
        public string page { get; set; }
        public string pages { get; set; }
        public string perpage { get; set; }
        public string total { get; set; } = "0";
        public string field { get; set; }
        public string sort { get; set; }

        public bool IsReverse { get { return sort != "asc" ? true : false; } }
        public TableAjaxBase(System.Collections.Specialized.NameValueCollection Params)
        {
            page = Params.Get("pagination[page]");
            pages = Params.Get("pagination[pages]");
            perpage = Params.Get("pagination[perpage]");
            total = Params.Get("pagination[total]");
            field = Params.Get("sort[field]");
            sort = Params.Get("sort[sort]");
            total = "0";
        }

        public TableAjaxBase()
        {

        }
        public int GetPageIndex
        {
            get
            {
                if (string.IsNullOrEmpty(page))
                {
                    return 1;
                }
                return int.Parse(page);
            }
        }
        public int GetPageSize
        {
            get
            {
                if (string.IsNullOrEmpty(perpage))
                {
                    return 1000000;
                }
                return int.Parse(perpage);
            }
        }
    }
}
