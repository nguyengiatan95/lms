﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrontEnd.Common.Models
{
    public class DatatableBase
    {
        public string page { get; set; }
        public string pages { get; set; }
        public string perpage { get; set; }
        public string total { get; set; } = "0";
        public string field { get; set; }
        public string sort { get; set; }

        public bool IsReverse { get { return sort != "asc" ? true : false; } }
        public DatatableBase(IFormCollection Params)
        {
            page = Params["pagination[page]"].FirstOrDefault();
            pages = Params["datatable[pagination][pages]"].FirstOrDefault();
            perpage = Params["pagination[perpage]"].FirstOrDefault();
            total = Params["datatable[pagination][total]"].FirstOrDefault();
            field = Params["datatable[sort][field]"].FirstOrDefault();
            sort = Params["datatable[sort][sort]"].FirstOrDefault();
            total = "0";
        }

        public DatatableBase()
        {

        }
        public int PageIndex
        {
            get
            {
                if (string.IsNullOrEmpty(page))
                {
                    return 1;
                }
                return int.Parse(page);
            }
        }
        public int PageSize
        {
            get
            {
                if (string.IsNullOrEmpty(perpage))
                {
                    return 1000000;
                }
                return int.Parse(perpage);
            }
        }
    }
}
