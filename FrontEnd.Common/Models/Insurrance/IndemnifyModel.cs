﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrontEnd.Common.Models.Insurrance
{
    public class IndemnifyModel : DatatableBase
    {
        public IndemnifyModel(IFormCollection form) : base(form)
        {
            LenderID = int.Parse(form["query[LenderID]"].FirstOrDefault() ?? "-1");
            StatusSendInsurance = int.Parse(form["query[StatusSendInsurance]"].FirstOrDefault() ?? "-111");
            TypeInsurance = int.Parse(form["query[TypeInsurance]"].FirstOrDefault() ?? "-111");
            StrFromDate = form["query[StrFromDate]"].FirstOrDefault();
            StrToDate = form["query[StrToDate]"].FirstOrDefault();
            CodeID = form["query[CodeID]"].FirstOrDefault();
            CustomerName = form["query[CustomerName]"].FirstOrDefault();
        }
        public IndemnifyModel()
        {

        }
        public string StrFromDate { get; set; }
        public string StrToDate { get; set; }
        public int LenderID { get; set; }
        public int TypeInsurance { get; set; }
        public int StatusSendInsurance { get; set; }
        public string CodeID { get; set; }
        public string CustomerName { get; set; }
    }
}
