﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrontEnd.Common.Models.Insurance
{
    public class InsuranceModel : DatatableBase
    {
        public InsuranceModel(IFormCollection form) : base(form)
        {
            LenderID = long.Parse(form["query[LenderID]"].FirstOrDefault() ?? "-1");
            //Fromdate = DateTime.ParseExact(form["query[Fromdate]"].FirstOrDefault(), "dd-MM-yyyy", null);
            Fromdate = form["query[Fromdate]"].FirstOrDefault();
            Todate = form["query[Todate]"].FirstOrDefault();

            //Todate = DateTime.ParseExact(form["query[Todate]"].FirstOrDefault(), "dd-MM-yyyy", null);
        }
        public InsuranceModel (){}
        //public string GeneralSearch { get; set; }
        //public string Status { get; set; }
        public string Fromdate { get; set; }
        public string Todate { get; set; }
        public long LenderID { get; set; }
    }
}
