﻿using FrontEnd.Common.Entites.Management;
using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Models.Management.Group
{
    public class GroupModel
    {
        public long GroupID { get; set; }
        public string GroupName { get; set; }
        public int Status { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public TblGroup ToObject()
        {
            return new TblGroup
            {
                GroupID = GroupID,
                GroupName = GroupName,
                Status = Status,
                PageIndex = PageIndex,
                PageSize = PageSize 
            };
        }
    }
}
