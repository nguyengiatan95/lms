﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Models.Management.Permission
{
    public class PermissionAddModel
    {
        public long PermissionID { get; set; }
        public string DisplayText { get; set; }
        public string LinkApi { get; set; }
        public int IsActive { get; set; }
    }
}
