﻿
using FrontEnd.Common.Entites.Management;
using LMS.Entites.Dtos.Menu;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace FrontEnd.Common.Models.Management.User
{
    public class UserModel
    {
        public string GeneralSearch { get; set; }
        public int Status { get; set; }
        public long UserID { get; set; }

        public long? DepartmentID { get; set; }

        public string FullName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public DateTime CreateDate { get; set; }

        public string Email { get; set; }

        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public List<int> LstUserGroups { get; set; }
        public List<TblUserGroup> LstUserGroup { get; set; }
        public TblUser ToObject()
        {
            return new TblUser
            {
                UserID = UserID,
                DepartmentID = DepartmentID,
                FullName = FullName,
                UserName = UserName,
                Password = Password,
                CreateDate = CreateDate,
                Email = Email,
                Status = Status,
                PageIndex = PageIndex,
                PageSize = PageSize,
                LstUserGroup = LstUserGroup
            };
        }
    }
    public class UserDatatableModel : DatatableBase
    {
        public UserDatatableModel(IFormCollection form) : base(form)
        {
            GeneralSearch = form["query[txt_search]"].FirstOrDefault();
            Status = int.Parse((form["query[sl_search_status]"].FirstOrDefault() ?? "0"));
        }
        public string GeneralSearch { get; set; }
        public int Status { get; set; }
    }
    public class UserLoginModel : UserModel
    {
        public string Token { get; set; }
        public string ApiUrl { get; set; }
        public string LoginUrl { get; set; }
        public string AdminUrl { get; set; }
        public string AccountantUrl { get; set; }
        public string LenderUrl { get; set; }
        public string THNUrl { get; set; }
        public string PathRereshToken { get; set; }
    }
    public class UserChangePassword
    {
        [Required(ErrorMessage = "User không hợp lệ!")]
        public string UserId { get; set; }

        [Required(ErrorMessage = "Mật khẩu mới không hợp lệ!")]
        [StringLength(100, MinimumLength = 6, ErrorMessage = "Mật khẩu không được ít hơn 6 ký tự, tối đa 100 ký tự!")]
        public string NewPassword { get; set; }
        [Required(ErrorMessage = "Mật khẩu mới không hợp lệ!")]
        [StringLength(100, MinimumLength = 6, ErrorMessage = "Mật khẩu không được ít hơn 6 ký tự, tối đa 100 ký tự!")]
        public string ReNewPassword { get; set; }
    }
    public class UserInfoWithMenuModel
    {
        public long UserID { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Token { get; set; }
        public DateTime TimeExpired { get; set; }
        public string TimeExpiredString { get; set; }
        public string ApiUrl { get; set; }
        public string LoginUrl { get; set; }
        public string AdminUrl { get; set; }
        public string DomainCookie { get; set; }
        public string CookieName { get; set; }
        public UserLoginModel UserLoginModel { get; set; }
        /// <summary>
        /// tạm thời gán mặc định = 3703
        /// </summary>
        public long UsershopID { get; set; }
        public List<JSTreeModel> LstMenu { get; set; }
        public int UserType { get; set; }
    }
    public class UserData
    {
        public string Token { get; set; }
        public DateTime TimeExpired { get; set; }
    }
    public class JSTreeModel
    {
        [JsonProperty("id")]
        public string ID { get; set; }
        [JsonProperty("text")]
        public string Text { get; set; }
        [JsonProperty("state")]
        public State State { get; set; }
        public string IsMenu { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        [JsonProperty("children")]
        public List<JSTreeModel> Children { get; set; }

        public string Link
        {
            get
            {
                string link = "/";
                if (!string.IsNullOrEmpty(ControllerName))
                {
                    link += ControllerName.Replace(@"/", "");
                }
                if (!string.IsNullOrEmpty(ActionName))
                {
                    link += $"/{ActionName.Replace("/", "")}";
                }
                return link;
            }
        }

        [JsonProperty("parentId")]
        public string ParentID { get; set; }
        public string Icon { get; set; }
    }

    public class State
    {
        [JsonProperty("opened")]
        public bool Opened { get; set; }

        [JsonProperty("disabled")]
        public bool Disabled { get; set; }
        [JsonProperty("selected")]
        public bool Selected { get; set; }
    }
}
