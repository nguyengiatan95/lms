﻿using FrontEnd.Common.Entites.Management;
using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Models.Management.Login
{
    public class LoginModel
    {
        public string UserName { get; set; } 
        public string Password { get; set; } 
        public TblUser ToObject()
        {
            return new TblUser
            { 
                UserName = UserName,
                Password = Password 
            };
        }
    }
    public class GetUserInfoWithMenuQuery  
    { 
        public int AppID { get; set; }
    }
}
