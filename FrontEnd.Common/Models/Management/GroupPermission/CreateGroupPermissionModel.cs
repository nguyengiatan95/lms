﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Models.Management.GroupPermission
{
    public class CreateGroupPermissionModel
    {
        public long GroupID { get; set; }
        public List<long> LstPermissionID { get; set; }
    }
}
