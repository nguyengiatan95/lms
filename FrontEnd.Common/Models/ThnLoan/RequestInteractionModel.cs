﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace FrontEnd.Common.Models.ThnLoan
{
    public class RequestInteractionModel
    {
        public string KeySearch { get; set; }
        public int UserID { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    [Description("Báo cáo Lịch sử tương tác")]
    public class ExcelInteractionResponse
    {
        [Description("LoanCreditID")]
        public string LoanCreditID { get; set; }
        [Description("LoanID")]
        public string LoanID { get; set; }
        [Description("Mã HĐGN")]
        public string ContacCode { get; set; }
        [Description("Khách hàng")]
        public string CustomerName { get; set; }
        [Description("Cb tác nghiệp")]
        public string UserFullName { get; set; }
        [Description("Ngày tác nghiệp")]
        public DateTime CreateDateComment { get; set; }
        [Description("Mã tác nghiệp")]
        public string DebitReminderCode { get; set; }
        [Description("Nội dung")]
        public string Comment { get; set; }
        [Description("Ngày hẹn")]
        public DateTime AppointmentDate { get; set; }
        [Description("UserName")]
        public string UserName { get; set; }
    }
    public class ResponseInteraction
    {
        public DateTime CreateDateComment { get; set; }
        public string UserFullName { get; set; }
        public string UserName { get; set; }
        public string ContacCode { get; set; }
        public string CustomerName { get; set; }
        public string Province { get; set; }
        public long DisbursedAmount { get; set; }
        public long CurrentOutstandingBalance { get; set; }
        public int LoanTime { get; set; }
        public string ShopName { get; set; }
        public string DebitReminderCode { get; set; }
        public DateTime AppointmentDate { get; set; }
        public string Comment { get; set; }
        public int LoanCreditID { get; set; }
        public int LoanID { get; set; }
    }
}
