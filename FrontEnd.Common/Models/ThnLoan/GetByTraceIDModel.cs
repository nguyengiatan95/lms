﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Models.ThnLoan
{
    public class GetByTraceIDModel
    {
        public int CreateBy { get; set; }
        public string TraceIDRequest { get; set; }
    }
}
