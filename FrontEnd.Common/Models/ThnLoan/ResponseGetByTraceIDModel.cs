﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Models.ThnLoan
{
    public class ResponseGetByTraceIDModel
    {
        public int LoanID { get; set; }
        public string LoanContactCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerDistrictName { get; set; }
        public string CustomerCityName { get; set; }
        public int POSBOM { get; set; }
        public int DPDBOM { get; set; }
        public string AssgineeNameFirst { get; set; }
        public string AssigneeNameSecond { get; set; }
        public string AssigneeNameLeader { get; set; }
        public string AssigneeNameDepartment { get; set; }
        public long LoanTotalMoneyReceipt { get; set; }
        public long DPDCurrent { get; set; }
        public long POSCurrent { get; set; }
        public string BucketBOM { get; set; }
        public string BucketCurrent { get; set; }
        public string LoanProductName { get; set; }
        public string LoanLatestComment { get; set; }
        public string AssigneeNameLatestComment { get; set; }
        public string CommentReasonCode { get; set; }
        public string CommentContent { get; set; }
        public int LoanBadDebtYear { get; set; }
        public string LoanInsuranceStatusName { get; set; }
        public string LoanInsurancePartnerName { get; set; }
        public string LoanDomainName { get; set; }
        public int CustomerID { get; set; }
        public string LoanFromDate { get; set; }
        public string LoanToDate { get; set; }
        public string LoanFinishedDate { get; set; }
        public string CustomerBirthDate { get; set; }
        public string CustomerAddress { get; set; }
        public long LoanTotalMoneyCurrent { get; set; }
        public string LoanRateTypeName { get; set; }
        public string LoanNextDate { get; set; }
        public long LoanFequency { get; set; }
        public long LoanInterestClosed { get; set; }
        public long LoanConsultantFeeClosed { get; set; }
        public long LoanMoneyFineLateClosed { get; set; }
        public long LoanMoneyFineOriginalClosed { get; set; }
        public long CustomerTotalMoney { get; set; }
        public long LoanTotalMoneyOriginalReceipt { get; set; }
        public long LoanTotalMoneyInterestReceipt { get; set; }
        public long LoanTotalMoneyConsultantReceipt { get; set; }
        public string CustomerEpayAccountName { get; set; }
        public bool LoanIsLocate { get; set; }
        public string LoanShopConsultantName { get; set; }
        public string LoanBKS { get; set; }
        public int LoanTopup { get; set; }
        public string LoanPreparationDate { get; set; }
        public long LoanPreparationMoney { get; set; }
        public string CustomerPayTypeName { get; set; }
        public string LenderCode { get; set; }
        public string AliasName { get; set; }

        public string UserCreate { get; set; }

        public long TotalMoney { get; set; }
        public DateTime TransactionDate { get; set; }
        public DateTime CreateDate { get; set; }
        public string Note { get; set; }
        public string StrStatus { get; set; }

    }
}
