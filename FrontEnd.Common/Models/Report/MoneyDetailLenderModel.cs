﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace FrontEnd.Common.Models.Report
{
    [Description("Sổ quỹ tiền mặt")]
    public class MoneyDetailLenderModel
    {
        [Description("Mã HĐ")]
        public string ContractCode { get; set; }
        [Description("Khách hàng")]
        public string CustomerName { get; set; }
        [Description("Mã lender")]
        public string LenderCode { get; set; }
        [Description("Tên bút toán")]
        public string ActionName { get; set; }
        [Description("Tiền cho vay")]
        public long TotalMoney { get; set; }
        [Description("Tiền gốc thu về")]
        public long MoneyOriginal { get; set; }
        [Description("Tiền phạt tất toán sớm")]
        public long MoneyFineOriginal { get; set; }
        [Description("Tiền lãi nhà đầu tư")]
        public long MoneyInterest { get; set; }
        [Description("Phí dịch vụ")]
        public long MoneyService { get; set; }
        [Description("Phí tư vấn")]
        public long MoneyConsultant { get; set; }
        [Description("Tiền phạt trả chậm tima")]
        public long MoneyFineLateTima { get; set; }
        [Description("Tiền phạt trả chậm lender")]
        public long MoneyFineLateLender { get; set; }
        [Description("Ngày tạo")]
        public DateTime CreateDate { get; set; } 
    }
}
