﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace FrontEnd.Common.Models.Report
{
    public class ReportVatModel
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class ReportVatResponse
    {
        [Description("Mẫu số")]
        public string Denominator { get; set; }// Mẫu số
        [Description("Ký hiệu")]
        public string Notation { get; set; }// Ký hiệu
        [Description("Số đơn hàng")]
        public string OrderNumber { get; set; }// số đơn hàng
        public DateTime? TransactionDate { get; set; }
        [Description("Tên người mua")]
        public string CustomerName { get; set; }
        [Description("Mã khách hàng")]
        public int CustomerID { get; set; }
        [Description("Tên đơn vị")]
        public string UnitName { get; set; }

        [Description("Mã số thuế")]
        public string Taxcode { get; set; }
        [Description("Địa chỉ")]
        public string Address { get; set; }
        [Description("Tài khoản")]
        public string Account { get; set; }
        [Description("Ngân hàng")]
        public string BankName { get; set; }
        [Description("Số HĐ")]
        public string ContactCode { get; set; }

        [Description("HT thanh toán")]
        public string TypePayment { get; set; }// hình thức thanh toán

        [Description("Mã hàng")]
        public string ItemCode { get; set; }

        [Description("Tên hàng")]
        public string CommodityName { get; set; }// tên hàng
        [Description("DVT")]
        public string DVT { get; set; }

        [Description("Số lượng")]
        public string Quantily { get; set; }

        [Description("Đơn giá")]
        public string UnitPrice { get; set; }

        [Description("Tiền")]
        public long FeeNotVat { get; set; }

        [Description("% CK")]
        public string CK { get; set; }

        [Description("Tiền chiết khấu")]
        public string Discount { get; set; }

        [Description("% thuế")]
        public int TaxPercentage { get; set; }// % thuế

        [Description("Tiền thuế")]
        public long Vat { get; set; }

        [Description("Thành tiền")]
        public long TotalMoney { get; set; }
        public string TypeTransaction { get; set; }
        public int TotalCount { get; set; }

    }
    public class ReportFineInterestLate
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class ReportVatHub
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int HubID { get; set; }
    }
    public class ReportVatHubResponse
    {
        public string Denominator { get; set; }// Mẫu số
        public string Notation { get; set; }// Ký hiệu
        public int TaxPercentage { get; set; }// % thuế
        public string TypePayment { get; set; }// hình thức thanh toán
        public string CommodityName { get; set; }// tên hàng
        public string OrderNumber { get; set; }// số đơn hàng
        public string HubName { get; set; }
        public string ContactCode { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public DateTime? TransactionDate { get; set; }
        public long FeeNotVat { get; set; }
        public long Vat { get; set; }
        public long TotalMoney { get; set; }
        public string TypeTransaction { get; set; }
        public int TotalCount { get; set; }

    }
    public class ReportPenaltyFeeVatHub
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int HubID { get; set; }
    }
    public class ReportVatGCash
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int GcashID { get; set; }
    }
    public class ReportVatHUbWithGCash
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int HubID { get; set; }
    }
    public class ReportLender
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int LenderID { get; set; }
    }
    public class ReportMoneyInterestLender
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int LenderID { get; set; }
        public List<int> LstMoneyType { get; set; }
    }
}
