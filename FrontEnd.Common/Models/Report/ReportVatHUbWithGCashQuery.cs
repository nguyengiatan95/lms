﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Models.Report
{
    public class ReportVatHUbWithGCashQuery
    {
        public long HubID { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
