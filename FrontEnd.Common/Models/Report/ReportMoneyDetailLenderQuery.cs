﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrontEnd.Common.Models.Report
{
    public class ReportMoneyDetailLenderQuery
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int ProductID { get; set; }
        public string ContractCode { get; set; }
        public int LenderID { get; set; }
    }
}
