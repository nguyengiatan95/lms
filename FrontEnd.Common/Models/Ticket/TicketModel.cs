﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrontEnd.Common.Models.Ticket
{
    public class TicketModel : DatatableBase
    {
        public TicketModel(IFormCollection form) : base(form)
        {
            FromDate = form["query[FromDate]"].FirstOrDefault();
            ToDate = form["query[ToDate]"].FirstOrDefault();
            Type = int.Parse(form["query[Type]"].FirstOrDefault() ?? "-111");
            Status = int.Parse(form["query[Status]"].FirstOrDefault() ?? "0");
            DepartmentID = int.Parse(form["query[DepartmentID]"].FirstOrDefault() ?? "-111");
        }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public int DepartmentID { get; set; }
    }
}
