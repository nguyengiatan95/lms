﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Queries.Menu
{
    public class GetMenuByConditionsQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string GeneralSearch { get; set; }
        public int Status { get; set; }
        public int AppID { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class GetMenuByConditionsQueryHandler : IRequestHandler<GetMenuByConditionsQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblMenu> _menuTab;
        ILogger<GetMenuByConditionsQueryHandler> _logger;
        Common.Helper.Utils common;
        public GetMenuByConditionsQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblMenu> menuTab, ILogger<GetMenuByConditionsQueryHandler> logger)
        {
            _menuTab = menuTab;
            _logger = logger;
            common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetMenuByConditionsQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                try
                {
                    if (!string.IsNullOrEmpty(request.GeneralSearch))
                        _menuTab.WhereClause(x => x.DisplayText.Contains(request.GeneralSearch));
                    if (request.Status != (int)LMS.Common.Constants.StatusCommon.SearchAll)
                        _menuTab.WhereClause(x => x.Status == request.Status);
                    if (request.AppID != (int)LMS.Common.Constants.StatusCommon.SearchAll)
                        _menuTab.WhereClause(x => x.AppID == request.AppID);
                    var lstMenu = _menuTab.Query();
                    var totalCount = lstMenu.Count();
                    lstMenu = lstMenu.OrderByDescending(x => x.MenuID).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                    ResponseActionResult response = new ResponseActionResult()
                    {
                        Result = (int)LMS.Common.Constants.ResponseAction.Success,
                        Data = lstMenu,
                        Total = totalCount,
                        Message = MessageConstant.Success
                    };
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"GetMenuByConditionsQueryHandler|request={common.ConvertObjectToJSonV2<GetMenuByConditionsQuery>(request)}|ex={ex.Message}");
                    return null;
                }
            });
        }
    }
}
