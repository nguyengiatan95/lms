﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Queries.Menu
{
    public class GetMenuParentQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long ParentID { get; set; }
        public int AppID { get; set; }
    }
    public class GetMenuParentQueryHandler : IRequestHandler<GetMenuParentQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblMenu> _menuTab;
        ILogger<GetMenuParentQueryHandler> _logger;
        Common.Helper.Utils common;
        public GetMenuParentQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblMenu> menuTab,
            ILogger<GetMenuParentQueryHandler> logger)
        {
            _menuTab = menuTab;
            _logger = logger;
            common = new Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetMenuParentQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                try
                {
                    ResponseActionResult response = new ResponseActionResult();
                    int activeStatus = (int)StatusCommon.Active;
                    var lstData = _menuTab.WhereClause(x => x.Status == activeStatus && x.ParentID == request.ParentID && x.AppID == request.AppID).Query(); ;
                    response.Result = (int)LMS.Common.Constants.ResponseAction.Success;
                    response.Data = lstData;
                    response.Message = "";
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"GetGroupMenuQueryHandler|request={common.ConvertObjectToJSonV2(request)}|ex={ex.Message}");
                    return null;
                }
            });
        }
    }
}
