﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.Menu;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Queries.Menu
{
    public class GetGroupMenuQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long GroupID { get; set; }
        public int AppID { get; set; }
    }
    public class GetGroupMenuQueryHandler : IRequestHandler<GetGroupMenuQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblMenu> _menuTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblMenuGroup> _menuGroupTab;
        ILogger<GetGroupMenuQueryHandler> _logger;
        Services.IMenuService _menuService;
        Common.Helper.Utils common;
        public GetGroupMenuQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblMenu> menuTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblMenuGroup> menuGroupTab, Services.IMenuService menuService,
            ILogger<GetGroupMenuQueryHandler> logger)
        {
            _menuTab = menuTab;
            _menuGroupTab = menuGroupTab;
            _logger = logger;
            _menuService = menuService;
            common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetGroupMenuQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var taskMenu = Task.Run(() =>
                {
                    int active = (int)StatusCommon.Active;
                    return _menuTab.WhereClause(x => x.Status == active && x.AppID == request.AppID).Query();
                });
                var taskMenuGroup = Task.Run(() =>
                {
                    return _menuGroupTab.WhereClause(x => x.GroupID == request.GroupID).Query();
                });
                await Task.WhenAll(taskMenu, taskMenuGroup);
                var lstMenu = taskMenu.Result.OrderBy(x => x.ParentID).ThenBy(x => x.Position);
                var lstMenuGroup = taskMenuGroup.Result;
                List<Entites.Dtos.Menu.JSTreeModel> lstData = new List<Entites.Dtos.Menu.JSTreeModel>();
                Dictionary<long, long> dictMenuIdSelected = lstMenuGroup.ToDictionary(x => x.MenuID, x => x.GroupID);
                lstData = _menuService.GenTreeMenu(lstMenu, dictMenuIdSelected);
                response.Result = (int)LMS.Common.Constants.ResponseAction.Success;
                response.Data = lstData;
                response.Message = "";
            }
            catch (Exception ex)
            {
                _logger.LogWarning($"GetGroupMenuQueryHandler|request={common.ConvertObjectToJSonV2(request)}|ex={ex.Message}");
            }
            return response;
        }
    }
}
