﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Queries.User
{
    public class GetUserQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string GeneralSearch { get; set; }
        public int Status { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class GetUserQueryHandler : IRequestHandler<GetUserQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<GetUserQueryHandler> _logger;
        Common.Helper.Utils common;
        public GetUserQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab, ILogger<GetUserQueryHandler> logger)
        {
            _userTab = userTab;
            _logger = logger;
            common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetUserQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                try
                {
                    if (!string.IsNullOrEmpty(request.GeneralSearch))
                        _userTab.WhereClause(x => x.UserName.Contains(request.GeneralSearch));
                    if (request.Status != (int)LMS.Common.Constants.StatusCommon.Default)
                        _userTab.WhereClause(x => x.Status == request.Status);

                    var lstUser = _userTab.Query();
                    var totalCount = lstUser.Count();
                    lstUser = lstUser.OrderByDescending(x => x.UserID).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                    ResponseActionResult response = new ResponseActionResult()
                    {
                        Result = (int)LMS.Common.Constants.ResponseAction.Success,
                        Data = lstUser,
                        Total = totalCount,
                        Message = MessageConstant.Success
                    };
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"GetUserQueryHandler|request={common.ConvertObjectToJSonV2<GetUserQuery>(request)}|ex={ex.Message}");
                    return null;
                }
            });
        }
    }
}
