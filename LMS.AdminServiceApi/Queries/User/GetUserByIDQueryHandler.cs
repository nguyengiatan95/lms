﻿using LMS.AdminServiceApi.Domain.Models;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Queries.User
{
    public class GetUserByIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int UserID { get; set; }
    }
    public class GetUserByIDQueryHandler : IRequestHandler<GetUserByIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> _userDepartmentTab;
        ILogger<GetUserByIDQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetUserByIDQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDepartmentTab,
        ILogger<GetUserByIDQueryHandler> logger)
        {
            _userTab = userTab;
            _userDepartmentTab = userDepartmentTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetUserByIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objResult = new GetInforUserByID();
                objResult.ObjUser = await _userTab.GetByIDAsync(request.UserID);
                objResult.LstUserDepartment = (await _userDepartmentTab.WhereClause(x => x.UserID == request.UserID).QueryAsync()).ToList();
                response.SetSucces();
                response.Data = objResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetUserByIDQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
