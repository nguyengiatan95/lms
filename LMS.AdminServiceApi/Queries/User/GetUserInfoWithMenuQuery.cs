﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Queries.User
{
    public class GetUserInfoWithMenuQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        // tham số lấy từ token với header
        [Required]
        public int AppID { get; set; }
    }
    public class GetUserInfoWithMenuQueryHandler : IRequestHandler<GetUserInfoWithMenuQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.Helper.Utils _common;
        Services.IMenuService _menuService;
        ILogger<GetUserInfoWithMenuQueryHandler> _logger;
        IHttpContextAccessor _httpContextAccessor;
        public GetUserInfoWithMenuQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            Services.IMenuService menuService,
            ILogger<GetUserInfoWithMenuQueryHandler> logger,
            IHttpContextAccessor httpContextAccessor)
        {
            _userTab = userTab;
            _menuService = menuService;
            _common = new LMS.Common.Helper.Utils();
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<ResponseActionResult> Handle(GetUserInfoWithMenuQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult respone = new ResponseActionResult();
                try
                {
                    var userID = GetHeaderUserID();
                    if (userID == 0)
                    {
                        respone.Message = "Thông tin không hợp lệ";
                        return respone;
                    }
                    LMS.Entites.Dtos.User.UserInfoWithMenuModel userInfoResponse = new Entites.Dtos.User.UserInfoWithMenuModel
                    {
                        LstMenu = new List<Entites.Dtos.Menu.JSTreeModel>(),
                        UserID = userID
                    };
                    var userDatas = _userTab.WhereClause(x => x.UserID == userID).Query();
                    if (userDatas == null || !userDatas.Any())
                    {
                        respone.Message = "Thông tin không hợp lệ";
                        return respone;
                    }
                    var userDetail = userDatas.First();
                    userInfoResponse.UserName = userDetail.UserName;
                    userInfoResponse.FullName = userDetail.FullName;
                    userInfoResponse.UsershopID = TimaSettingConstant.ShopIDTima;
                    userInfoResponse.LstMenu = _menuService.GetLstMenuByUserID(userID, request.AppID);
                    userInfoResponse.UserType = userDetail.UserTypeID;
                    respone.SetSucces();
                    respone.Data = userInfoResponse;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"GetUserInfoWithMenuQueryHandler|ex={ex.Message}-{ex.StackTrace}");
                }

                return respone;
            });
        }
        private long GetHeaderUserID()
        {
            long userID = 0;
            try
            {
                var lstHeader = _httpContextAccessor.HttpContext?.Request?.Headers;
                if (lstHeader != null && lstHeader.Any())
                {
                    var headerCheck = TimaSettingConstant.HeaderLMSUserID.ToLowerInvariant();
                    foreach (var item in lstHeader)
                    {
                        if (item.Key.ToLowerInvariant() == headerCheck)
                        {
                            userID = long.Parse(item.Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            //userID = 67119; // test
            return userID;
        }
    }
}
