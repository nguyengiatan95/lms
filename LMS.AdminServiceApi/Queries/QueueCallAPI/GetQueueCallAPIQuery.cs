﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LMS.AdminServiceApi.Domain.Models;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace LMS.AdminServiceApi.Queries.QueueCallAPI
{
    public class GetQueueCallAPIQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string StrDate { get; set; }
        public string Path { get; set; }
        public string JsonRequest { get; set; }
        public int Status { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class GetQueueCallAPIQueryHandler : IRequestHandler<GetQueueCallAPIQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> _queueCallAPITab;
        ILogger<GetQueueCallAPIQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetQueueCallAPIQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> queueCallAPITab,
            ILogger<GetQueueCallAPIQueryHandler> logger)
        {
            _queueCallAPITab = queueCallAPITab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetQueueCallAPIQuery request, CancellationToken cancellationToken)
        {

            ResponseActionResult response = new ResponseActionResult();
            var lstQueueCallAPI = new List<QueueCallAPIitem>();
            try
            {
                DateTime date = DateTime.ParseExact(request.StrDate, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture).AddDays(-1);
                _queueCallAPITab.WhereClause(x => x.CreateDate > date);

                if (!string.IsNullOrEmpty(request.Path))
                    _queueCallAPITab.WhereClause(x => x.Path.Contains(request.Path));

                if (!string.IsNullOrEmpty(request.JsonRequest))
                    _queueCallAPITab.WhereClause(x => x.JsonRequest.Contains(request.JsonRequest));

                if (request.Status != (int)StatusCommon.Default)
                    _queueCallAPITab.WhereClause(x => x.Status == request.Status);

                var result = await _queueCallAPITab.QueryAsync();
                int totalCount = result.Count();
                if (result != null && result.Count() > 0)
                {
                    result = result.OrderByDescending(x => x.QueueCallAPIID).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                    foreach (var item in result)
                    {
                        lstQueueCallAPI.Add(new QueueCallAPIitem
                        {
                            QueueCallAPIID = item.QueueCallAPIID,
                            Domain = item.Domain,
                            Path = item.Path,
                            Method = item.Method,
                            JsonRequest = item.JsonRequest,
                            JsonResponse= item.JsonResponse,
                            LastModifyDate = item.LastModifyDate,
                            Retry = item.Retry,
                            CreateDate = item.CreateDate,
                            Status = item.Status,
                            Description = item.Description,
                            StrStatus = ((QueueCallAPI_Status)item.Status).GetDescription(),
                        });
                    }
                }
                response.SetSucces();
                response.Data = lstQueueCallAPI;
                response.Total = totalCount;
                return response;

            }
            catch (Exception ex)
            {
                _logger.LogError($"GetQueueCallAPIQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
