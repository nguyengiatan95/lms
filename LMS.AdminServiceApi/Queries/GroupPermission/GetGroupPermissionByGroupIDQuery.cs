﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Queries.GroupPermission
{

    public class GetGroupPermissionByGroupIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long GroupID { get; set; }
        public string GeneralSearch { get; set; }
        public long IsHasPermission { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }

    public class GetGroupPermissionByGroupIDQueryHandler : IRequestHandler<GetGroupPermissionByGroupIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblGroupPermission> _groupPermissionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPermission> _permissionTab;
        ILogger<GetGroupPermissionByGroupIDQueryHandler> _logger;
        public GetGroupPermissionByGroupIDQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblGroupPermission> groupPermissionTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblPermission> permissionTab,
            ILogger<GetGroupPermissionByGroupIDQueryHandler> logger)
        {
            _groupPermissionTab = groupPermissionTab;
            _permissionTab = permissionTab;
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(GetGroupPermissionByGroupIDQuery request, CancellationToken cancellationToken)
        {
            Domain.Tables.PermissionGroupClientView dataPermission = new Domain.Tables.PermissionGroupClientView();
            dataPermission.LstPermission = new List<Domain.Tables.GroupPermisionAPIView>();
            int statusActive = (int)StatusCommon.Active;
            var taskPermission = Task.Run(() =>
            {

                return _permissionTab.WhereClause(x => x.IsActive == statusActive).Query();
            });

            var taskGroupPermission = Task.Run(() =>
            {
                return _groupPermissionTab.WhereClause(x => x.GroupID == request.GroupID).Query();
            });
            Task.WhenAll(taskPermission, taskGroupPermission).Wait();
            var lstPermission = taskPermission.Result;
            var lstPermissionGroup = taskGroupPermission.Result;
            foreach (var item in lstPermission)
            {
                dataPermission.LstPermission.Add(new Domain.Tables.GroupPermisionAPIView()
                {
                    LinkApi = item.LinkApi,
                    DisplayText = item.DisplayText,
                    PermissionID = item.PermissionID,
                    HasPermission = lstPermissionGroup.Count(x => x.PermissionID == item.PermissionID) > 0,
                });
            }
            dataPermission.LstHavePermissionID = dataPermission.LstPermission.Where(x => x.HasPermission).Select(x => x.PermissionID).ToList();
            if (request.IsHasPermission == 1)
            {
                dataPermission.LstPermission = dataPermission.LstPermission.Where(x => x.HasPermission).ToList();
            }
            else if (request.IsHasPermission == 0)
            {
                dataPermission.LstPermission = dataPermission.LstPermission.Where(x => x.HasPermission == false).ToList();
            }
            if (!string.IsNullOrWhiteSpace(request.GeneralSearch))
            {
                dataPermission.LstPermission = dataPermission.LstPermission.Where(x => x.DisplayText.Contains(request.GeneralSearch)).ToList();
            }
            long totalCount = dataPermission.LstPermission.Count;
            dataPermission.LstPermission = dataPermission.LstPermission.OrderBy(x => x.HasPermission).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
            if (dataPermission.LstPermission.Any()) dataPermission.LstPermission[0].TotalCount = totalCount;
            ResponseActionResult response = new ResponseActionResult()
            {
                Result = (int)LMS.Common.Constants.ResponseAction.Success,
                Data = dataPermission,
                Message = ""
            };
            return response;
        }
    }
}
