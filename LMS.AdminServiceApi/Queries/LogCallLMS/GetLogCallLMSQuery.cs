﻿using LMS.AdminServiceApi.Domain.Models;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Queries.LogCallLMS
{
    public class GetLogCallLMSQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string StrDate { get; set; }
        public string LinkUrl { get; set; }
        public string DataPost { get; set; }
        public int Status { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class GetLogCallLMSQueryHandler : IRequestHandler<GetLogCallLMSQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCallLMS> _logCallLMSTab;
        ILogger<GetLogCallLMSQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetLogCallLMSQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCallLMS> logCallLMSTab,
            ILogger<GetLogCallLMSQueryHandler> logger)
        {
            _logCallLMSTab = logCallLMSTab.SetConnectString(LMS.Common.Configuration.ConnectStringSetting.AGConnectString);
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetLogCallLMSQuery request, CancellationToken cancellationToken)
        {

            ResponseActionResult response = new ResponseActionResult();
            var lstLogCallLMS = new List<LogCallLMSItem>();
            try
            {
                DateTime date = DateTime.ParseExact(request.StrDate, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture).AddDays(-1);
                _logCallLMSTab.WhereClause(x => x.CreateDate > date);

                if (!string.IsNullOrEmpty(request.LinkUrl))
                    _logCallLMSTab.WhereClause(x => x.LinkUrl.Contains(request.LinkUrl));

                if (!string.IsNullOrEmpty(request.DataPost))
                    _logCallLMSTab.WhereClause(x => x.DataPost.Contains(request.DataPost));

                if (request.Status != (int)StatusCommon.Default)
                    _logCallLMSTab.WhereClause(x => x.Status == request.Status);

                var result = await _logCallLMSTab.QueryAsync();
                int totalCount = result.Count();
                if (result != null && result.Count() > 0)
                {
                    result = result.OrderByDescending(x => x.LogCallLMSID).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                    foreach (var item in result)
                    {
                        lstLogCallLMS.Add(new LogCallLMSItem
                        {
                            LogCallLMSID = item.LogCallLMSID,
                            LinkUrl = item.LinkUrl,
                            Method = item.Method,
                            DataPost = item.DataPost,
                            CreateDate = item.CreateDate,
                            Status = item.Status,
                            ModifyDate = item.ModifyDate,
                            ResultResponseApi = item.ResultResponseApi,
                            StrStatus = ((LogCallLMS_Status)item.Status).GetDescription(),
                        });
                    }
                }
                response.SetSucces();
                response.Data = lstLogCallLMS;
                response.Total = totalCount;
                return response;

            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLogCallLMSQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
