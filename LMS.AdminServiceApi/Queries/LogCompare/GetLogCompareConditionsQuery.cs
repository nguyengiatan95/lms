﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Queries.LogCompare
{
    public class GetLogCompareConditionsQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string StrSearchDate { get; set; }
        public int Type { get; set; }
    }
    public class GetLogCompareConditionsQueryHandler : IRequestHandler<GetLogCompareConditionsQuery,
        LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCompare> _logCompareTab;
        ILogger<GetLogCompareConditionsQueryHandler> _logger;
        public GetLogCompareConditionsQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCompare> logCompareTab
            , ILogger<GetLogCompareConditionsQueryHandler> logger)
        {
            _logCompareTab = logCompareTab.SetConnectString(LMS.Common.Configuration.ConnectStringSetting.DefaultConnectString);
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(GetLogCompareConditionsQuery request, CancellationToken cancellationToken)
        {

            return await Task.Run(() =>
            {
                try
                {
                    if (request.Type != (int)LMS.Common.Constants.StatusCommon.Default)
                    {
                        _logCompareTab.WhereClause(x => x.MainType == request.Type);
                    }
                    DateTime dateRequest = DateTime.Now.Date;
                    if (!string.IsNullOrWhiteSpace(request.StrSearchDate))
                        DateTime.TryParseExact(request.StrSearchDate, Utils.DateTimeDayMonthYear, null, System.Globalization.DateTimeStyles.None, out dateRequest);
                    var lstData = _logCompareTab.WhereClause(x => x.ForDate == dateRequest).Query();
                    ResponseActionResult response = new ResponseActionResult()
                    {
                        Result = (int)LMS.Common.Constants.ResponseAction.Success,
                        Data = lstData,
                        Total = lstData.Count(),
                        Message = MessageConstant.Success
                    };
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"GetLogCompareConditionsQueryHandler|StrSearchDate={request.StrSearchDate}|ex={ex.Message}");
                    return null;
                }

            });
        }
    }

}
