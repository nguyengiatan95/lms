﻿using LMS.AdminServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Queries.Permission
{
    public class GetPermissionByIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long PermissionID { get; set; }
    }

    public class GetPermissionByIDQueryHandler : IRequestHandler<GetPermissionByIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPermission> _permissionTab;
        ILogger<GetPermissionByIDQueryHandler> _logger;
        public GetPermissionByIDQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPermission> permissionTab, ILogger<GetPermissionByIDQueryHandler> logger)
        {
            _permissionTab = permissionTab.SetConnectString(LMS.Common.Configuration.ConnectStringSetting.DefaultConnectString);
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(GetPermissionByIDQuery request, CancellationToken cancellationToken)
        {

            return await Task.Run(() =>
            {
                var data = _permissionTab.GetByID(request.PermissionID);
                ResponseActionResult response = new ResponseActionResult()
                {
                    Result = (int)LMS.Common.Constants.ResponseAction.Success,
                    Data = data,
                    Message = ""
                };
                return response;
            });
        }
    }
}
