﻿using LMS.AdminServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LMS.Common.Helper;
using LMS.Entites.Dtos;

namespace LMS.AdminServiceApi.Queries.Permission
{
    public class GetPermissionConditionsQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string GeneralSearch { get; set; }
        public int Status { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string Field { get; set; }
        public string Sort { get; set; }
    }

    public class GetPermissionConditionsQueryHandler : IRequestHandler<GetPermissionConditionsQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPermission> _permissionTab;
        ILogger<GetPermissionConditionsQuery> _logger;
        Common.Helper.Utils common;

        public GetPermissionConditionsQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPermission> permissionTab, ILogger<GetPermissionConditionsQuery> logger)
        {
            _permissionTab = permissionTab.SetConnectString(LMS.Common.Configuration.ConnectStringSetting.DefaultConnectString);
            common = new Common.Helper.Utils();
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(GetPermissionConditionsQuery request, CancellationToken cancellationToken)
        {

            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {

                    if (!string.IsNullOrWhiteSpace(request.GeneralSearch)) _permissionTab.WhereClause(x => x.LinkApi.Contains(request.GeneralSearch));
                    if (request.Status != (int)StatusCommon.Default) _permissionTab.WhereClause(x => x.IsActive == request.Status);

                    var lstPermission = _permissionTab.Query();
                    int total = lstPermission.Count();
                    if (!string.IsNullOrWhiteSpace(request.Field))
                    {
                        request.Field = Utils.FirstCharToUpper(request.Field);
                        lstPermission = request.Sort.ToLower() == "desc" ? (lstPermission).OrderByDescending(request.Field).ToList() : (lstPermission).OrderBy(request.Field).ToList();
                    }
                    lstPermission = lstPermission.Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();

                    var lstPermissionView = new List<PermissionListView>();
                    if (lstPermission != null)
                    {
                        int startRow = (request.PageIndex - 1) * request.PageSize + 1;
                        foreach (var item in lstPermission)
                        {
                            lstPermissionView.Add(
                                new PermissionListView
                                {
                                    CreateDate = item.CreateDate,
                                    DisplayText = item.DisplayText,
                                    IsActive = item.IsActive,
                                    LinkApi = item.LinkApi,
                                    ModifyDate = item.ModifyDate,
                                    PermissionID = item.PermissionID,
                                    RowID = startRow++
                                });
                        }
                        response.Result = (int)LMS.Common.Constants.ResponseAction.Success;
                        response.Message = MessageConstant.Success;
                        response.Data = lstPermissionView;
                        response.Total = total;
                    }
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"GetPermissionConditionsQueryHandler|request={common.ConvertObjectToJSonV2<GetPermissionConditionsQuery>(request)}|ex={ex.Message}");
                    return response;
                }
            });
        }
    }
}
