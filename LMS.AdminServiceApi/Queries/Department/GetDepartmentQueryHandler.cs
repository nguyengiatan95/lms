﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Queries.Department
{
    public class GetDepartmentQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string GeneralSearch { get; set; }
        public int Status { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class GetDepartmentQueryHandler : IRequestHandler<GetDepartmentQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> _departmentTab;
        ILogger<GetDepartmentQueryHandler> _logger;
        Common.Helper.Utils common;
        public GetDepartmentQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> departmentTab, ILogger<GetDepartmentQueryHandler> logger)
        {
            _departmentTab = departmentTab;
            _logger = logger;
            common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetDepartmentQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                try
                {
                    var lstData = new List<Domain.Tables.DepartmentViewList>();
                    if (!string.IsNullOrEmpty(request.GeneralSearch))
                        _departmentTab.WhereClause(x => x.DepartmentName.Contains(request.GeneralSearch));
                    if (request.Status != (int)LMS.Common.Constants.StatusCommon.Default)
                        _departmentTab.WhereClause(x => x.Status == request.Status);

                    var lstDepartment = _departmentTab.Query();
                    var totalCount = lstDepartment.Count();
                    lstDepartment = lstDepartment.OrderByDescending(x => x.DepartmentID).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                    var lstParentIds = lstDepartment.Where(x => x.ParentID > 0).Select(x => x.ParentID).Distinct().ToList();
                    var dicParent = new Dictionary<long, string>();
                    if (lstParentIds.Count() > 0)
                    {
                        dicParent = _departmentTab.SelectColumns(x => x.DepartmentID, x => x.DepartmentName).WhereClause(x => lstParentIds.Contains(x.DepartmentID))
                                    .Query().ToDictionary(x => x.DepartmentID, x => x.DepartmentName);
                    }
                    foreach (var item in lstDepartment)
                    {
                        lstData.Add(new Domain.Tables.DepartmentViewList()
                        {
                            ParentName = dicParent.ContainsKey(item.ParentID) ? dicParent[item.ParentID] : "",
                            CreateDate = item.CreateDate,
                            DepartmentID = item.DepartmentID,
                            DepartmentName = item.DepartmentName,
                            ParentID = item.ParentID,
                            Status = item.Status,
                            AppID = item.AppID
                        });
                    }
                    ResponseActionResult response = new ResponseActionResult()
                    {
                        Result = (int)LMS.Common.Constants.ResponseAction.Success,
                        Data = lstData,
                        Total = totalCount,
                        Message = MessageConstant.Success
                    };
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"GetDepartmentQueryHandler|request={common.ConvertObjectToJSonV2<GetDepartmentQuery>(request)}|ex={ex.Message}");
                    return null;
                }
            });
        }
    }
}
