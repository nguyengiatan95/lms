﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Queries.Department
{
    public class GetDepartmentByIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int DepartmentId { get; set; }
    }
    public class GetDepartmentByIDQueryHandler : IRequestHandler<GetDepartmentByIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> _departmentTab;
        ILogger<GetDepartmentByIDQueryHandler> _logger;
        public GetDepartmentByIDQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> departmentTab, ILogger<GetDepartmentByIDQueryHandler> logger)
        {
            _departmentTab = departmentTab;
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(GetDepartmentByIDQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                var data = _departmentTab.GetByID(request.DepartmentId);
                ResponseActionResult response = new ResponseActionResult()
                {
                    Result = (int)LMS.Common.Constants.ResponseAction.Success,
                    Data = data,
                    Message = MessageConstant.Success
                };
                return response;
            });
        }
    }
}
