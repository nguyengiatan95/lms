﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Queries.Group
{
    public class GetGroupByIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int GroupId { get; set; }
    }
    public class GetGroupByIDQueryHandler : IRequestHandler<GetGroupByIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblGroup> _groupTab;
        ILogger<GetGroupByIDQueryHandler> _logger;
        public GetGroupByIDQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblGroup> groupTab, ILogger<GetGroupByIDQueryHandler> logger)
        {
            _groupTab = groupTab;
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(GetGroupByIDQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                var data = _groupTab.GetByID(request.GroupId);
                ResponseActionResult response = new ResponseActionResult()
                {
                    Result = (int)LMS.Common.Constants.ResponseAction.Success,
                    Data = data,
                    Message = MessageConstant.Success
                };
                return response;
            });
        }
    }
}
