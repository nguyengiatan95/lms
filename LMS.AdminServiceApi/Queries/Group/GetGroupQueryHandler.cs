﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Queries.Group
{
    public class GetGroupQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string GeneralSearch { get; set; }
        public int Status { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class GetGroupQueryHandler : IRequestHandler<GetGroupQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblGroup> _groupTab;
        ILogger<GetGroupQueryHandler> _logger;
        Common.Helper.Utils common;
        public GetGroupQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblGroup> groupTab, ILogger<GetGroupQueryHandler> logger)
        {
            _groupTab = groupTab;
            _logger = logger;
            common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetGroupQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                try
                {
                    if (!string.IsNullOrEmpty(request.GeneralSearch))
                        _groupTab.WhereClause(x => x.GroupName.Contains(request.GeneralSearch));
                    if (request.Status != (int)LMS.Common.Constants.StatusCommon.Default)
                        _groupTab.WhereClause(x => x.Status == request.Status);

                    var lstGroup = _groupTab.Query();
                    var totalCount = lstGroup.Count();
                    lstGroup = lstGroup.OrderByDescending(x => x.GroupID).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                    ResponseActionResult response = new ResponseActionResult()
                    {
                        Result = (int)LMS.Common.Constants.ResponseAction.Success,
                        Data = lstGroup,
                        Total = totalCount,
                        Message = MessageConstant.Success
                    };
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"GetGroupQueryHandler|request={common.ConvertObjectToJSonV2<GetGroupQuery>(request)}|ex={ex.Message}");
                    return null;
                }
            });
        }
    }
}
