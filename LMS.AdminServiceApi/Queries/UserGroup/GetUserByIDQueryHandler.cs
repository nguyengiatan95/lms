﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Queries.UserGroup
{
    public class GetUserGroupByUserId : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int UserID { get; set; }
    }
    public class GetUserGroupByUserIdHandler : IRequestHandler<GetUserGroupByUserId, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserGroup> _userGroupTab;
        ILogger<GetUserGroupByUserIdHandler> _logger;
        public GetUserGroupByUserIdHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserGroup> userGroupTab, ILogger<GetUserGroupByUserIdHandler> logger)
        {
            _userGroupTab = userGroupTab;
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(GetUserGroupByUserId request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                var data = _userGroupTab.WhereClause(x=>x.UserID== request.UserID).Query().ToList();
                ResponseActionResult response = new ResponseActionResult()
                {
                    Result = (int)LMS.Common.Constants.ResponseAction.Success,
                    Data = data,
                    Message = MessageConstant.Success
                };
                return response;
            });
        }
    }
}
