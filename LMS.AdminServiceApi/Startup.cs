using FluentValidation;
using FluentValidation.AspNetCore;
using LMS.AdminServiceApi.Validators;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Steeltoe.Discovery.Client;
using Steeltoe.Discovery.Eureka;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<KestrelServerOptions>(Configuration.GetSection("Kestrel"));
            services.AddDiscoveryClient(Configuration);
            services.AddServiceDiscovery(options => options.UseEureka());
            services.AddSingleton(Configuration);
            services.AddControllers().AddJsonOptions(opts => opts.JsonSerializerOptions.PropertyNamingPolicy = null);
            services.AddHttpContextAccessor();
            LMS.Common.Configuration.ConnectStringSetting.DefaultConnectString = Configuration["ConnectStringSetting:DefaultConnectString"];
            LMS.Common.Configuration.ConnectStringSetting.AGConnectString = Configuration["AGConfiguration:DefaultConnectString"];
            services.AddTransient(typeof(Common.DAL.ITableHelper<>), typeof(Common.DAL.TableHelper<>));
            services.AddAutoMapper(typeof(Startup));
            services.AddMvc().AddFluentValidation();
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddTransient<LMS.Common.Helper.IApiHelper, LMS.Common.Helper.ApiHelper>();
            services.AddTransient<Services.IMenuService, Services.MenuService>();
            services.AddTransient<IValidator<Queries.User.GetUserInfoWithMenuQuery>, GetUserInfoWithMenuQueryValidator>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors(options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseAuthorization();
            app.UseDiscoveryClient(); 
            //Add our new middleware to the pipeline
            app.UseMiddleware<LMS.Common.Helper.RequestResponseLoggingMiddleware>();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
