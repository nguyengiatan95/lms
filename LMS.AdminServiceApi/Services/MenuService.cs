﻿using LMS.AdminServiceApi.Domain.Tables;
using LMS.Common.Constants;
using LMS.Entites.Dtos.Menu;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Services
{
    public interface IMenuService
    {
        List<JSTreeModel> GetLstMenuByUserID(long userID, int appID);
        List<JSTreeModel> GenTreeMenu(IEnumerable<TblMenu> permissionInfos, Dictionary<long, long> dictMenuIdSelected = null);
    }
    public class MenuService : IMenuService
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserGroup> _userGroupTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblMenuGroup> _menuGroupTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblMenu> _menuTab;
        ILogger<MenuService> _logger;
        public MenuService(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserGroup> userGroupTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblMenuGroup> menuGroupTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblMenu> menuTab,
            ILogger<MenuService> logger)
        {
            _userGroupTab = userGroupTab;
            _menuGroupTab = menuGroupTab;
            _menuTab = menuTab;
            _logger = logger;
        }
        public List<JSTreeModel> GenTreeMenu(IEnumerable<TblMenu> permissionInfos, Dictionary<long, long> dictMenuIdSelected = null)
        {
            List<JSTreeModel> jSTreeModels = new List<JSTreeModel>();
            int count = permissionInfos.Count();
            if (dictMenuIdSelected == null)
            {
                dictMenuIdSelected = new Dictionary<long, long>();
            }
            for (int i = 0; i < count; i++)
            {
                var itemI = permissionInfos.ElementAt(i);
                if (itemI.ParentID == 0)
                {
                    var isSelectedParent = dictMenuIdSelected.ContainsKey(itemI.MenuID);
                    JSTreeModel nodeParent = new JSTreeModel
                    {
                        ActionName = itemI.ActionName,
                        ControllerName = itemI.ControllerName,
                        IsMenu = itemI.IsMenu.ToString(),
                        Text = itemI.DisplayText,
                        State = new State() { Selected = isSelectedParent },
                        Children = new List<JSTreeModel>(),
                        ID = itemI.MenuID.ToString(),
                        ParentID = itemI.ParentID.ToString(),
                        Icon = itemI.Icon

                    };
                    for (int j = i + 1; j < count; j++)
                    {
                        var itemJ = permissionInfos.ElementAt(j);
                        if (itemJ.ParentID == itemI.MenuID)
                        {
                            var isSelectedChild = dictMenuIdSelected.ContainsKey(itemJ.MenuID);
                            JSTreeModel nodeChild = new JSTreeModel
                            {
                                ActionName = itemJ.ActionName,
                                ControllerName = itemJ.ControllerName,
                                IsMenu = itemJ.IsMenu.ToString(),
                                Text = itemJ.DisplayText,
                                State = new State() { Selected = isSelectedChild },
                                ID = itemJ.MenuID.ToString(),
                                ParentID = itemJ.ParentID.ToString(),
                                Icon = itemJ.Icon
                            };
                            nodeParent.Children.Add(nodeChild);
                            // nếu con không được select -> remove selected của cha
                            if (!isSelectedChild)
                            {
                                nodeParent.State.Selected = false;
                            }
                        }
                    }
                    jSTreeModels.Add(nodeParent);
                }
            }
            return jSTreeModels;
        }

        public List<JSTreeModel> GetLstMenuByUserID(long userID, int appID)
        {
            List<JSTreeModel> lstMenu = new List<JSTreeModel>();
            try
            {
                if (userID != TimaSettingConstant.UserIDAdmin) // user admin
                {
                    var lstUserGroupInfos = _userGroupTab.WhereClause(x => x.UserID == userID).Query();
                    if (lstUserGroupInfos == null || !lstUserGroupInfos.Any())
                    {
                        return lstMenu;
                    }
                    var lstGroupIDs = lstUserGroupInfos.Select(x => x.GroupID);
                    var lstMenuGroupInfos = _menuGroupTab.WhereClause(x => lstGroupIDs.Contains(x.GroupID)).Query();
                    if (lstMenuGroupInfos == null || !lstMenuGroupInfos.Any())
                    {
                        return lstMenu;
                    }
                    var lstMenuIDs = lstMenuGroupInfos.Select(x => x.MenuID).Distinct();
                    _menuTab.WhereClause(x => lstMenuIDs.Contains(x.MenuID));
                }
                var lstMenuInfos = _menuTab.WhereClause(x => x.AppID == appID && x.Status == (int)LMS.Common.Constants.StatusCommon.Active).Query();
                if (lstMenuInfos == null || !lstMenuInfos.Any())
                {
                    return lstMenu;
                }
                lstMenuInfos = lstMenuInfos.OrderBy(x => x.ParentID).ThenBy(x => x.Position);
                lstMenu = GenTreeMenu(lstMenuInfos);
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLstMenuByUserID|userID={userID}|appID={appID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return lstMenu;
        }
    }
}
