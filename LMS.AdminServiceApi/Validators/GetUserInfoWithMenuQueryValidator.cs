﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Validators
{
    public class GetUserInfoWithMenuQueryValidator : AbstractValidator<Queries.User.GetUserInfoWithMenuQuery>
    {
        public GetUserInfoWithMenuQueryValidator()
        {
            var appIDs = Enum.GetValues(typeof(LMS.Common.Constants.Menu_AppID)).Cast<int>();
            RuleFor(x => x.AppID).Must(x => appIDs.Contains(x)).WithMessage(LMS.Common.Constants.MessageConstant.ParameterInvalid);
        }

    }
}
