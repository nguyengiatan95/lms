﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Models
{
    public class CreateGroupPermissionCommandModel
    {
        [System.ComponentModel.DataAnnotations.Required]
        public long GroupID { get; set; }
        public List<long> LstPermissionID { get; set; }
        public int CheckAll { get; set; } // 1 là tích all , 2 là hủy all 
    }
}
