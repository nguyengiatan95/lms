﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Models
{
    public class CreatePermissionCommandModel 
    {
        public string DisplayText { get; set; }

        public string LinkApi { get; set; }

        public int IsActive { get; set; }
    }

   
}
