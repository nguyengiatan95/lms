﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Models
{
    public class CreateUserModel
    {
        public long UserID { get; set; }

        public long? DepartmentID { get; set; }

        public string FullName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public DateTime? CreateDate { get; set; }

        public string Email { get; set; }

        public int? Status { get; set; }

        public List<Domain.Tables.TblUserGroup> LstUserGroup { get; set; }
        public List<Domain.Tables.TblUserDepartment> LstUserDepartment { get; set; }
    }
}
