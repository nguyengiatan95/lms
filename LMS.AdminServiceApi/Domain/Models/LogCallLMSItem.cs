﻿using LMS.AdminServiceApi.Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Models
{
    public class LogCallLMSItem : TblLogCallLMS
    {
        public string StrStatus { get; set; }
    }
}
