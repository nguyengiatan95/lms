﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Models
{
    public class UpdatePermissionCommandModel
    {
        public long PermissionID { get; set; }
        public string DisplayText { get; set; }
        public string LinkApi { get; set; }
        public int IsActive { get; set; }
    }
}
