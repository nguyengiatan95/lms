﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Models
{
    public class CreateMenuGroupModel
    {
        [System.ComponentModel.DataAnnotations.Required]
        public long GroupID { get; set; }
        public List<long> LstMenuID { get; set; }
        public int AppID { get; set; }
    }
}
