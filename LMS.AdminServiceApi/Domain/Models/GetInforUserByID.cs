﻿using LMS.AdminServiceApi.Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Models
{
    public class GetInforUserByID
    {
        public TblUser ObjUser { get; set; }
        public List<TblUserDepartment> LstUserDepartment { get; set; }
    }
}
