﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Models
{
    public class CreateDepartmentModel
    {
        public long DepartmentID { get; set; }
        [Required]
        public string DepartmentName { get; set; }
        public long ParentID { get; set; }
        public long AppID { get; set; }
    }
}
