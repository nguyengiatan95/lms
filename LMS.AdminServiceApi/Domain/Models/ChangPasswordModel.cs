﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Models
{
    public class ChangPasswordModel
    {
        public long UserID { get; set; }
        public  string PasswordOld { get; set; }
        public string PasswordNew { get; set; }
    }
}
