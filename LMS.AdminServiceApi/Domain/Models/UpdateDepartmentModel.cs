﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Models
{
    public class UpdateDepartmentModel
    {
        [Required]
        public long DepartmentID { get; set; }
        [Required]
        public string DepartmentName { get; set; }
        [Required]
        public int Status { get; set; }
        public long ParentID { get; set; }
        public long AppID { get; set; }


    }
}
