﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Models
{
    public class UpdateGroupModel
    {
        public long GroupID { get; set; }
        public string GroupName { get; set; }
        public int Status{ get; set; }
    }
}
