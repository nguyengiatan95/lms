﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Models
{
    public class GetDepartmentModel
    {
        public string DepartmentName { get; set; }
        public int Status { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
