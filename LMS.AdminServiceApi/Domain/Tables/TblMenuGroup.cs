﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblMenuGroup")]
    public class TblMenuGroup
    {
        [Dapper.Contrib.Extensions.Key]
        public long MenuGroupID { get; set; }

        public long GroupID { get; set; }

        public long MenuID { get; set; }

    }
}
