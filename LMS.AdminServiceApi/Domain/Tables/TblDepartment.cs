﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblDepartment")]
    public class TblDepartment
    {
        [Dapper.Contrib.Extensions.Key]
        public long DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public DateTime CreateDate { get; set; }
        public int Status { get; set; }
        public long ParentID { get; set; }
        public long AppID { get; set; }
    }

    public class DepartmentViewList : TblDepartment
    {
        public string ParentName { get; set; }

    }
}
