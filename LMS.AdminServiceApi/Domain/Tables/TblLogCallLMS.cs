﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblLogCallLMS")]
    public class TblLogCallLMS
    {
        [Dapper.Contrib.Extensions.Key]
        public long LogCallLMSID { get; set; }

        public string LinkUrl { get; set; }

        public int? Method { get; set; }

        public string DataPost { get; set; }

        public DateTime CreateDate { get; set; }

        public int Status { get; set; }

        public DateTime? ModifyDate { get; set; }

        public string ResultResponseApi { get; set; }
    }
}
