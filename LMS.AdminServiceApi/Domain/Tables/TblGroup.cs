﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblGroup")]
    public class TblGroup
    {
        [Dapper.Contrib.Extensions.Key]
        public long GroupID { get; set; }
        public string GroupName { get; set; }
        public int Status { get; set; }
    }
}
