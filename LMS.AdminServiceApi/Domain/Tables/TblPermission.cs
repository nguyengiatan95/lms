﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("tblPermission")]
    public class TblPermission
    {
        [Dapper.Contrib.Extensions.Key]
        public long PermissionID { get; set; }
        public string DisplayText { get; set; }
        public string LinkApi { get; set; }
        public int IsActive { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public long CreateBy { get; set; }
        public long ModfifyBy { get; set; }
    }
}
