﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Tables
{
    [Table("TblGroupPermission")]
    public class TblGroupPermission
    {
        [Key]
        public long GroupPermissionID { get; set; }

        public long GroupID { get; set; }

        public long PermissionID { get; set; }
    }

    public class GroupPermisionAPIView
    {
        public string LinkApi { get; set; }
        public string DisplayText { get; set; }
        public long PermissionID { get; set; }
        public bool HasPermission { get; set; }
        public long TotalCount { get; set; }
    }

    public class PermissionGroupClientView
    {
        public List<GroupPermisionAPIView> LstPermission { get; set; }
        public List<long> LstHavePermissionID { get; set; }
        public PermissionGroupClientView()
        {
            LstPermission = new List<GroupPermisionAPIView>();
            LstHavePermissionID = new List<long>();
        }
    }
}
