﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblUser")]
    public class TblUser
    {
        [Dapper.Contrib.Extensions.Key]
        public long UserID { get; set; }

        public long? DepartmentID { get; set; }


        public string FullName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public DateTime? CreateDate { get; set; }

        public string Email { get; set; }

        public int? Status { get; set; }
        public long TimaUserID { get; set; }
        public int UserTypeID { get; set; }

    }

}
