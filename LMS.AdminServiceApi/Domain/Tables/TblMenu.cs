﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblMenu")]
    public class TblMenu
    {
        [Dapper.Contrib.Extensions.Key]
        public long MenuID { get; set; }
        public int AppID { get; set; }
        public string DisplayText { get; set; }
        public long ParentID { get; set; }
        public int Position { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
        public int IsMenu { get; set; }
        public string Icon { get; set; }

    }
}
