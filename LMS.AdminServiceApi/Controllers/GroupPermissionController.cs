﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LMS.Common.Constants;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Steeltoe.Common.Discovery;

namespace LMS.AdminServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class GroupPermissionController : ControllerBase
    {
        private readonly IMediator bus;
        private readonly IMapper _mapper;
        Common.Helper.Utils _common;
        public GroupPermissionController(IMediator bus, IMapper mapper)
        {
            this.bus = bus;
            _mapper = mapper;
        }
        [HttpPost]
        [Route("CreatePermission")]

        public async Task<ResponseActionResult> CreatePermission(Domain.Models.CreateGroupPermissionCommandModel cmd)
        {
            var result = await bus.Send(new Commands.GroupPermission.CreateGroupPermissionValidCommand
            {
                GroupPermission = _mapper.Map<Domain.Models.CreateGroupPermissionCommandModel>(cmd)
            });
            return result;
        }

        [HttpPost]
        [Route("GetByGroupID")]

        public async Task<ResponseActionResult> GetByGroupID(Queries.GroupPermission.GetGroupPermissionByGroupIDQuery resquest)
        {
            var result = await bus.Send(resquest);
            return result;
        }
    }
}