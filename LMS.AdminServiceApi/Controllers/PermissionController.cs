﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LMS.Common.Constants;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Steeltoe.Common.Discovery;

namespace LMS.AdminServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PermissionController : ControllerBase
    {
        private readonly IMediator bus;
        Common.Helper.Utils _common;

        public PermissionController(IMediator bus)
        {
            this.bus = bus;
            _common = new Common.Helper.Utils();
        }

        [HttpPost]
        [Route("CreatePermission")]

        public async Task<ResponseActionResult> CreatePermission(Domain.Models.CreatePermissionCommandModel cmd)
        {
            var result = await bus.Send(new Commands.Permission.CreatePermissionValidCommand
            {
                PermissionInfo = _common.ConvertParentToChild<Domain.Models.CreatePermissionCommandModel, Domain.Models.CreatePermissionCommandModel>(cmd)
            });
            return result;
        }

        [HttpPost]
        [Route("UpdatePermission")]

        public async Task<ResponseActionResult> UpdatePermission(Domain.Models.UpdatePermissionCommandModel cmd)
        {
            var result = await bus.Send(new Commands.Permission.UpdatePermissionValidCommand
            {
                RequestUpdate = _common.ConvertParentToChild<Domain.Models.UpdatePermissionCommandModel, Domain.Models.UpdatePermissionCommandModel>(cmd)
            });
            return result;
        }

        [HttpGet]
        [Route("GetPermissionByID/{PermissionID}")]

        public async Task<ResponseActionResult> GetPermissionByID(long PermissionID)
        {
            var result = await bus.Send(new Queries.Permission.GetPermissionByIDQuery
            {
                PermissionID = PermissionID
            });
            return result;
        }

        [HttpGet]
        [Route("GetByConditions")]

        public async Task<ResponseActionResult> GetByConditions(string GeneralSearch, int Status, int PageIndex = 1, int PageSize = 50, string Field = "", string Sort = "DESC")
        {
            var result = await bus.Send(new Queries.Permission.GetPermissionConditionsQuery
            {
                GeneralSearch = GeneralSearch,
                Status = Status,
                PageIndex = PageIndex,
                PageSize = PageSize,
                Field = Field,
                Sort = Sort
            });
            return result;
        }
    }
}