﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Common.Constants;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LMS.AdminServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private readonly IMediator _bus;
        public MenuController(IMediator bus)
        {
            this._bus = bus;
        }

        [HttpPost]
        [Route("GetByConditions")]
        public async Task<ResponseActionResult> GetByConditions(Queries.Menu.GetMenuByConditionsQuery request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("CreateUpdateMenu")]
        public async Task<ResponseActionResult> CreateUpdateMenu(Domain.Tables.TblMenu menu)
        {
            Commands.Menu.CreateUpdateMenuCommand request = new Commands.Menu.CreateUpdateMenuCommand()
            {
                model = menu
            };
            return await _bus.Send(request);
        }

        [HttpGet]
        [Route("GetMenuByGroupID")]
        public async Task<ResponseActionResult> GetMenuByGroupID(long GroupID, int AppID)
        {
            Queries.Menu.GetGroupMenuQuery request = new Queries.Menu.GetGroupMenuQuery()
            {
                GroupID = GroupID,
                AppID = AppID
            };
            return await _bus.Send(request);
        }

        [HttpGet]
        [Route("GetMenuParent")]
        public async Task<ResponseActionResult> GetMenuParent(long ParentID = 0, int AppID = 0)
        {
            Queries.Menu.GetMenuParentQuery request = new Queries.Menu.GetMenuParentQuery()
            {
                ParentID = ParentID,
                AppID = AppID
            };
            return await _bus.Send(request);
        }


        [HttpPost]
        [Route("CreateMenuGroup")]

        public async Task<ResponseActionResult> CreateMenuGroup(Domain.Models.CreateMenuGroupModel cmd)
        {
            var result = await _bus.Send(new Commands.Menu.CreateMenuGroupCommand
            {
                GroupMenu = cmd
            });
            return result;
        }
    }
}