﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LMS.Common.Constants;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Steeltoe.Common.Discovery;

namespace LMS.AdminServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GroupController : ControllerBase
    {
        private readonly IMediator _bus;
        public GroupController(IMediator bus)
        {
            this._bus = bus;
        }

        [HttpPost]
        [Route("CreateGroupFromAppValid")]
        public async Task<ResponseActionResult> CreateGroupFromAppValid(Domain.Models.CreateGroupModel req)
        {
            var result = await _bus.Send(new Commands.Group.CreateValidGroupCommand
            {
                GroupInfo = req
            });
            return result;
        }
        [HttpPost]
        [Route("UpdateGroupFromApp")]
        public async Task<ResponseActionResult> UpdateGroupFromApp(Domain.Models.UpdateGroupModel req)
        {
            var result = await _bus.Send(new Commands.Group.UpdateGroupCommand
            {
                 GroupInfo= req
            });
            return result;
        }

        [HttpGet]
        [Route("GetGroup")]
        public async Task<ResponseActionResult> GetGroup(string GeneralSearch, int Status, int PageIndex, int PageSize)
        {
            var result = await _bus.Send(new Queries.Group.GetGroupQuery
            {
                GeneralSearch = GeneralSearch,
                Status = Status,
                PageIndex = PageIndex,
                PageSize = PageSize
            });
            return result;
        }
        [HttpGet]
        [Route("GetGroupByID/{GroupId}")]
        public async Task<ResponseActionResult> GetGroupByID(int GroupId)
        {
            var result = await _bus.Send(new Queries.Group.GetGroupByIDQuery
            {
                GroupId = GroupId
            });
            return result;
        }
    }
}