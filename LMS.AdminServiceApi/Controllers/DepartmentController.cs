﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LMS.Common.Constants;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Steeltoe.Common.Discovery;

namespace LMS.AdminServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly IMediator _bus;
        public DepartmentController(IMediator bus)
        {
            this._bus = bus;
        }

        [HttpPost]
        [Route("CreateDepartmentFromAppValid")]
        public async Task<ResponseActionResult> CreateDepartmentFromAppValid(Domain.Models.CreateDepartmentModel cmd)
        {
            var result = await _bus.Send(new Commands.Department.CreateValidDepartmentCommand { 
            DepartmentInfo = cmd
            });
            return result;
        }
        [HttpPost]
        [Route("UpdateDepartmentFromApp")]
        public async Task<ResponseActionResult> UpdateDepartmentFromApp(Domain.Models.UpdateDepartmentModel cmd)
        {
            var result = await _bus.Send(new Commands.Department.UpdateDepartmentCommand
            {
                DepartmentInfo = cmd
            });
            return result;
        }
        [HttpGet]
        [Route("GetDepartment")]
        public async Task<ResponseActionResult> GetDepartment(string GeneralSearch, int Status, int PageIndex, int PageSize)
        {
            var result = await _bus.Send(new Queries.Department.GetDepartmentQuery
            {
                GeneralSearch = GeneralSearch,
                Status = Status,
                PageIndex = PageIndex,
                PageSize = PageSize
            });
            return result;
        }
        [HttpGet]
        [Route("GetDepartmentByID/{DepartmentId}")]
        public async Task<ResponseActionResult> GetDepartmentByID(int DepartmentId)
        {
            var result = await _bus.Send(new Queries.Department.GetDepartmentByIDQuery
            {
                DepartmentId = DepartmentId
            });
            return result;
        }
    }
}