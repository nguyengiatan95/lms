﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Common.Constants;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace LMS.AdminServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogCallLMSController : ControllerBase
    {
        private readonly IMediator _bus;
        public LogCallLMSController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("GetLogCallLMS")]
        public async Task<ResponseActionResult> GetLogCallLMS(Queries.LogCallLMS.GetLogCallLMSQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("UpdateStatusFailToWaitingLogCallLMS")]
        public async Task<ResponseActionResult> UpdateStatusFailToWaitingLogCallLMS(Commands.LogCallLMS.UpdateStatusFailToWaitingLogCallLMSCommand request)
        {
            return await _bus.Send(request);
        }
    }
}