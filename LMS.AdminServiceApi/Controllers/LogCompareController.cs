﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Common.Constants;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LMS.AdminServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogCompareController : ControllerBase
    {
        private readonly IMediator _bus;
        public LogCompareController(IMediator bus)
        {
            this._bus = bus;
        }

        [HttpGet]
        [Route("GetSummaryCompare")]
        public async Task<ResponseActionResult> GetSummaryCompare(string StrSearchDate = "",int type = 0)
        {
            var result = await _bus.Send(new Queries.LogCompare.GetLogCompareConditionsQuery
            {
                StrSearchDate = StrSearchDate,
                Type = type
            });
            return result;
        }

    }
}