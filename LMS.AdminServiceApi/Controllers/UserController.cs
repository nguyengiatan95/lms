﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LMS.Common.Constants;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Steeltoe.Common.Discovery;

namespace LMS.AdminServiceApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IMediator _bus;
        public UserController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("CreateUserFromAppValid")]
        public async Task<ResponseActionResult> CreateUserFromAppValid(Domain.Models.CreateUserModel req)
        {
            var result = await _bus.Send(new Commands.User.CreateValidUserCommand
            {
                UserInfo = req
            });
            return result;
        }
        [HttpPost]
        [Route("UpdateUserFromApp")]
        public async Task<ResponseActionResult> UpdateUserFromApp(Domain.Models.UpdateUserModel req)
        {
            var result = await _bus.Send(new Commands.User.UpdateUserCommand
            {
                UserInfo = req
            });
            return result;
        }

        [HttpGet]
        [Route("GetUser")]
        public async Task<ResponseActionResult> GetUser(string GeneralSearch, int Status, int PageIndex, int PageSize)
        {
            var result = await _bus.Send(new Queries.User.GetUserQuery
            {
                GeneralSearch = GeneralSearch,
                Status = Status,
                PageIndex = PageIndex,
                PageSize = PageSize
            });
            return result;
        }
        [HttpGet]
        [Route("GetUserByID/{UserID}")]
        public async Task<ResponseActionResult> GetUserByID(int UserID)
        {
            var result = await _bus.Send(new Queries.User.GetUserByIDQuery
            {
                UserID = UserID
            });
            return result;
        }

        [HttpPost]
        [Route("ChangePassword")]
        public async Task<ResponseActionResult> ChangePassword(Domain.Models.ChangPasswordModel req)
        {
            var result = await _bus.Send(new Commands.User.ChangPasswordCommand
            {
                UserInfo = req
            });
            return result;
        }

        [HttpPost]
        [Route("GetUserInfoWithMenu")]
        public async Task<ResponseActionResult> GetUserInfoWithMenu(Queries.User.GetUserInfoWithMenuQuery req)
        {
            return await _bus.Send(req);
        }


        [HttpPost]
        [Route("CreateUserFromAG")]
        public async Task<ResponseActionResult> CreateUserFromAG(Commands.User.CreateUserFromAgCommand req)
        {
            return await _bus.Send(req);
        }
    }
}