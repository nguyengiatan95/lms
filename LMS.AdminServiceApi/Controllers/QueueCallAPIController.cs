﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Common.Constants;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace LMS.AdminServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QueueCallAPIController : ControllerBase
    {
        private readonly IMediator _bus;
        public QueueCallAPIController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("GetQueueCallAPI")]
        public async Task<ResponseActionResult> GetQueueCallAPI(Queries.QueueCallAPI.GetQueueCallAPIQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("UpdateStatusFailToWaitingGetQueueCallAPI")]
        public async Task<ResponseActionResult> UpdateStatusFailToWaitingGetQueueCallAPI(Commands.QueueCallAPI.UpdateStatusFailToWaitingGetQueueCallAPICommand request)
        {
            return await _bus.Send(request);
        }
    }
}