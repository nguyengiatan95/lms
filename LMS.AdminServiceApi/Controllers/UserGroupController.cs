﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LMS.Common.Constants;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Steeltoe.Common.Discovery;

namespace LMS.AdminServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserGroupController : ControllerBase
    {
        private readonly IMediator bus;
        public UserGroupController(IMediator bus)
        {
            this.bus = bus;
        }
        [HttpGet]
        [Route("GetUserGroupByUserId/{UserId}")]
        public async Task<ResponseActionResult> GetUserGroupByUserId(int UserId)
        {
            var result = await bus.Send(new Queries.UserGroup.GetUserGroupByUserId
            {
                UserID = UserId
            });
            return result;
        }
    }
}