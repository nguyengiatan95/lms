﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Commands.Menu
{
    public class CreateMenuGroupCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public Domain.Models.CreateMenuGroupModel GroupMenu { get; set; }
    }

    public class CreateMenuGroupCommandHandler : IRequestHandler<CreateMenuGroupCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblMenuGroup> _groupMenuTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblMenu> _menuTab;
        //LMS.Common.DAL.ITableHelper<Domain.Tables.TblPermission> _permissionTab;
        Common.Helper.Utils common;
        ILogger<CreateMenuGroupCommandHandler> _logger;
        public CreateMenuGroupCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblMenuGroup> groupMenuTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblMenu> menuTab,
            ILogger<CreateMenuGroupCommandHandler> logger)
        {
            _groupMenuTab = groupMenuTab;
            _menuTab = menuTab;
            common = new Common.Helper.Utils();
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(CreateMenuGroupCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstMenuData = await _menuTab.WhereClause(x => x.AppID == request.GroupMenu.AppID).QueryAsync();
                var dictMenuDataInfos = lstMenuData.ToDictionary(x => x.MenuID, x => x);
                var lstMenuIds = dictMenuDataInfos.Keys.ToList();
                // delete quyền cũ
                _ = _groupMenuTab.DeleteWhereAsync(x => x.GroupID == request.GroupMenu.GroupID && lstMenuIds.Contains(x.MenuID));
                // đảm bảo deleteasync đã chạy
                await Task.Delay(5);
                // menu chỉ có 2 cấp mới áp dùng luồng code này
                // _insert quyền mới
                var lstPermission = new List<Domain.Tables.TblMenuGroup>();
                Dictionary<long, List<Domain.Tables.TblMenu>> dictMenuInsertInfos = new Dictionary<long, List<Domain.Tables.TblMenu>>();
                if (request.GroupMenu.LstMenuID != null && request.GroupMenu.LstMenuID.Count > 0)
                {
                    foreach (var item in request.GroupMenu.LstMenuID)
                    {
                        // check nếu menu con được tích thì thêm menu cha vào
                        var menuInfo = dictMenuDataInfos.GetValueOrDefault(item);
                        if (menuInfo == null || menuInfo.MenuID < 1)
                        {
                            continue;
                        }
                        if (menuInfo.ParentID == 0)
                        {
                            if (!dictMenuInsertInfos.ContainsKey(menuInfo.MenuID))
                            {
                                dictMenuInsertInfos.Add(menuInfo.MenuID, new List<Domain.Tables.TblMenu> { menuInfo });
                                continue;
                            }
                        }
                        else
                        {
                            // menu con
                            // lấy menu cha
                            var menuParent = dictMenuDataInfos.GetValueOrDefault(menuInfo.ParentID);
                            if (menuParent == null || menuParent.MenuID < 1)
                            {
                                _logger.LogError($"CreateMenuGroupCommandHandler_NotFoundMenuParent|MenuChild={common.ConvertObjectToJSonV2(menuInfo)}");
                                continue;
                            }
                            if (!dictMenuInsertInfos.ContainsKey(menuParent.MenuID))
                            {
                                dictMenuInsertInfos.Add(menuParent.MenuID, new List<Domain.Tables.TblMenu> { menuParent });
                            }
                            dictMenuInsertInfos[menuParent.MenuID].Add(menuInfo);
                        }
                    }
                    foreach (var lstMenuItem in dictMenuInsertInfos.Values)
                    {
                        foreach (var menu in lstMenuItem)
                        {
                            lstPermission.Add(new Domain.Tables.TblMenuGroup()
                            {
                                GroupID = request.GroupMenu.GroupID,
                                MenuID = menu.MenuID
                            });
                        }

                    }
                    if (lstPermission.Count > 0)
                        _groupMenuTab.InsertBulk(lstPermission);
                }
                response.Result = (int)ResponseAction.Success;
                response.Message = MessageConstant.Success;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogWarning($"CreateMenuGroupCommandHandler|request={common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }
    }
}
