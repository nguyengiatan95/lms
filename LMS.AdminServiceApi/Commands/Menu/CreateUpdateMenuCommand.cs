﻿using LMS.AdminServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Commands.Menu
{
    public class CreateUpdateMenuCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public TblMenu model { get; set; }
    }
    public class CreateUpdateMenuCommandCommandHandler : IRequestHandler<CreateUpdateMenuCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblMenu> _menuTab;
        Common.Helper.Utils common;
        ILogger<CreateUpdateMenuCommandCommandHandler> _logger;
        public CreateUpdateMenuCommandCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblMenu> menuTab,
            ILogger<CreateUpdateMenuCommandCommandHandler> logger)
        {
            _menuTab = menuTab;
            common = new Common.Helper.Utils();
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(CreateUpdateMenuCommand request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    var controllerName = request.model.ControllerName;
                    var actionName = request.model.ActionName;
                    // check xem có chưa
                    var lstMenu = _menuTab.WhereClause(x => x.ControllerName == controllerName && x.ActionName == actionName).Query();
                    // tạo mới
                    if (request.model.MenuID == 0)
                    {

                        if (lstMenu != null && lstMenu.Count() > 0)
                        {
                            response.Message = MessageConstant.Menu_ExistControllerAction;
                            return response;
                        }
                        long id = _menuTab.Insert(request.model);
                        response.Result = (int)ResponseAction.Success;
                        response.Message = MessageConstant.Success;
                        response.Data = id;
                        return response;
                    }
                    else
                    {
                        if (lstMenu != null && lstMenu.Where(x => x.MenuID != request.model.MenuID).Count() > 0)
                        {
                            response.Message = MessageConstant.Menu_ExistControllerAction;
                            return response;
                        }
                        bool update = _menuTab.Update(request.model);
                        if (update)
                        {
                            response.Result = (int)ResponseAction.Success;
                            response.Data = request.model.MenuID;
                            response.Message = MessageConstant.Success;
                        }
                        return response;
                    }

                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"CreateUpdateMenuCommandCommandHandler|request={common.ConvertObjectToJSonV2(request)}|ex={ex.Message}");
                    return null;
                }
            });
        }
    }
}
