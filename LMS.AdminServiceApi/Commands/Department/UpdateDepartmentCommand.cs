﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Commands.Department
{
    public class UpdateDepartmentCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public Domain.Models.UpdateDepartmentModel DepartmentInfo { get; set; }
    }
    public class UpdateDepartmentCommandHandler : IRequestHandler<UpdateDepartmentCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> _departmentTab;
        ILogger<UpdateDepartmentCommand> _logger;
        Common.Helper.Utils common;
        public UpdateDepartmentCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> departmentTab, ILogger<UpdateDepartmentCommand> logger)
        {
            _departmentTab = departmentTab;
            _logger = logger;
            common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(UpdateDepartmentCommand request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    if (string.IsNullOrEmpty(request.DepartmentInfo.DepartmentName))
                    {
                        response.Message = MessageConstant.DepartmentNameNull;
                        return response;
                    }
                    var Objdepartment = _departmentTab.GetByID(request.DepartmentInfo.DepartmentID);
                    if (Objdepartment == null)
                    {
                        response.Message = MessageConstant.DepartmentNotExist;
                        return response;
                    }
                    Objdepartment.DepartmentName = request.DepartmentInfo.DepartmentName;
                    Objdepartment.Status = request.DepartmentInfo.Status;
                    Objdepartment.ParentID = request.DepartmentInfo.ParentID;
                    Objdepartment.AppID = request.DepartmentInfo.AppID;
                    var update = _departmentTab.Update(Objdepartment);
                    if (update)
                    {
                        response.Result = (int)LMS.Common.Constants.ResponseAction.Success;
                        response.Data = Objdepartment.DepartmentID;
                        response.Message = MessageConstant.Success;
                    }
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"UpdateDepartmentCommandHandler|request={common.ConvertObjectToJSonV2<UpdateDepartmentCommand>(request)}|ex={ex.Message}");
                    return response;
                }
            });
        }
    }
}
