﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Commands.Department
{
    public class CreateValidDepartmentCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public Domain.Models.CreateDepartmentModel DepartmentInfo { get; set; }
    }
    public class CreateValidDepartmentHandler : IRequestHandler<CreateValidDepartmentCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> _departmentTab;
        ILogger<CreateValidDepartmentCommand> _logger;
        Common.Helper.Utils common;
        public CreateValidDepartmentHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> departmentTab, ILogger<CreateValidDepartmentCommand> logger)
        {
            _departmentTab = departmentTab;
            _logger = logger;
            common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateValidDepartmentCommand request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {

                    if (string.IsNullOrEmpty(request.DepartmentInfo.DepartmentName))
                    {
                        response.Message = MessageConstant.DepartmentNameNull;
                        return response;
                    }
                    var checkDepartment = _departmentTab.WhereClause(x => x.DepartmentName == request.DepartmentInfo.DepartmentName).Query().FirstOrDefault();
                    if (checkDepartment != null)
                    {
                        response.Message = MessageConstant.DepartmentNameExist;
                        return response;
                    }
                    var Objdepartment = new Domain.Tables.TblDepartment
                    {
                        DepartmentName = request.DepartmentInfo.DepartmentName,
                        CreateDate = DateTime.Now,
                        Status = LMS.Common.Constants.StatusCommon.Active.GetHashCode(),
                        ParentID = request.DepartmentInfo.ParentID,
                        AppID = request.DepartmentInfo.AppID
                    };
                    var Insert = _departmentTab.Insert(Objdepartment);
                    if (Insert > 0)
                    {
                        response.Result = (int)LMS.Common.Constants.ResponseAction.Success;
                        response.Data = Insert;
                        response.Message = MessageConstant.Success;
                    }
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"CreateValidDepartmentHandler|request={common.ConvertObjectToJSonV2<CreateValidDepartmentCommand>(request)}|ex={ex.Message}");
                    return response;
                }
            });
        }
    }
}
