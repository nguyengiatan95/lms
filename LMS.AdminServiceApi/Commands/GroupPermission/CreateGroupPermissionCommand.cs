﻿using LMS.AdminServiceApi.Domain.Models;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Commands.GroupPermission
{
    public class CreateGroupPermissionValidCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public Domain.Models.CreateGroupPermissionCommandModel GroupPermission { get; set; }
    }

    public class CreateGroupPermissionValidCommandHandler : IRequestHandler<CreateGroupPermissionValidCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblGroupPermission> _groupPermissionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPermission> _permissionTab;
        Common.Helper.Utils common;
        ILogger<CreateGroupPermissionValidCommandHandler> _logger;
        public CreateGroupPermissionValidCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblGroupPermission> groupPermissionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPermission> permissionTab,
            ILogger<CreateGroupPermissionValidCommandHandler> logger)
        {
            _permissionTab = permissionTab.SetConnectString(LMS.Common.Configuration.ConnectStringSetting.DefaultConnectString);
            _groupPermissionTab = groupPermissionTab.SetConnectString(LMS.Common.Configuration.ConnectStringSetting.DefaultConnectString);
            common = new Common.Helper.Utils();
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(CreateGroupPermissionValidCommand request, CancellationToken cancellationToken)
        {

            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    _groupPermissionTab.DeleteWhere(x => x.GroupID == request.GroupPermission.GroupID);
                    if (request.GroupPermission.CheckAll == 1)
                    {
                        var baseStatus = (int)StatusCommon.Active;
                        var lstPermission = _permissionTab.WhereClause(x => x.IsActive == baseStatus).Query();
                        var lstPermissionGroup = new List<Domain.Tables.TblGroupPermission>();
                        foreach (var item in lstPermission)
                        {
                            lstPermissionGroup.Add(new Domain.Tables.TblGroupPermission()
                            {
                                GroupID = request.GroupPermission.GroupID,
                                PermissionID = item.PermissionID
                            });
                        }
                        _groupPermissionTab.InsertBulk(lstPermissionGroup);
                    }
                    else if (request.GroupPermission.LstPermissionID != null && request.GroupPermission.LstPermissionID.Count > 0)
                    {
                        var lstPermission = new List<Domain.Tables.TblGroupPermission>();
                        if (request.GroupPermission.LstPermissionID != null && request.GroupPermission.LstPermissionID.Any())
                        {
                            foreach (var item in request.GroupPermission.LstPermissionID)
                            {
                                lstPermission.Add(new Domain.Tables.TblGroupPermission()
                                {
                                    GroupID = request.GroupPermission.GroupID,
                                    PermissionID = item
                                });
                            }
                            _groupPermissionTab.InsertBulk(lstPermission);
                        }
                    }
                    response.Result = (int)ResponseAction.Success;
                    response.Message = MessageConstant.Success;
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"CreateGroupPermissionValidCommandHandler|request={common.ConvertObjectToJSonV2<CreateGroupPermissionValidCommand>(request)}|ex={ex.Message}");
                    return null;
                }
            });
        }
    }
}
