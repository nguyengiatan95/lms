﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Commands.QueueCallAPI
{
    public class UpdateStatusFailToWaitingGetQueueCallAPICommand : IRequest<ResponseActionResult>
    {
        public long QueueCallAPIID { get; set; }
    }

    public class UpdateStatusFailToWaitingGetQueueCallAPICommandHandler : IRequestHandler<UpdateStatusFailToWaitingGetQueueCallAPICommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> _queueCallAPITab;
        Common.Helper.Utils common;
        ILogger<UpdateStatusFailToWaitingGetQueueCallAPICommandHandler> _logger;
        public UpdateStatusFailToWaitingGetQueueCallAPICommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> queueCallAPITab,
            ILogger<UpdateStatusFailToWaitingGetQueueCallAPICommandHandler> logger)
        {
            _queueCallAPITab = queueCallAPITab;
            common = new Common.Helper.Utils();
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(UpdateStatusFailToWaitingGetQueueCallAPICommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objQueueCallAPI = (await _queueCallAPITab.WhereClause(x => x.QueueCallAPIID == request.QueueCallAPIID).QueryAsync()).FirstOrDefault();
                if (objQueueCallAPI == null)
                {
                    response.Message = MessageConstant.ParameterInvalid;
                    return response;
                }
                objQueueCallAPI.Status = (int)QueueCallAPI_Status.Waiting;
                objQueueCallAPI.LastModifyDate = DateTime.Now;
                if (await _queueCallAPITab.UpdateAsync(objQueueCallAPI))
                {
                    response.SetSucces();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateStatusFailToWaitingGetQueueCallAPICommandHandler|request={common.ConvertObjectToJSonV2<UpdateStatusFailToWaitingGetQueueCallAPICommand>(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
