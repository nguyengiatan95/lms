﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Commands.User
{
    public class ChangPasswordCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public Domain.Models.ChangPasswordModel UserInfo { get; set; }
    }
    public class ChangPassworCommandHandler : IRequestHandler<ChangPasswordCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<ChangPassworCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public ChangPassworCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            ILogger<ChangPassworCommandHandler> logger)
        {
            _userTab = userTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(ChangPasswordCommand request, CancellationToken cancellationToken)
        {

            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var Objuser = await _userTab.GetByIDAsync(request.UserInfo.UserID);
                if (Objuser == null)
                {
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                if (_common.HashMD5(request.UserInfo.PasswordOld) != Objuser.Password)
                {
                    response.Message = MessageConstant.UserErrorPass;
                    return response;
                }
                Objuser.Password = _common.HashMD5(request.UserInfo.PasswordNew);
                var update = await _userTab.UpdateAsync(Objuser);
                if (update)
                {
                    response.Result = (int)LMS.Common.Constants.ResponseAction.Success;
                    response.Message = MessageConstant.Success;
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogWarning($"ChangPassworCommandHandler|request={_common.ConvertObjectToJSonV2<ChangPasswordCommand>(request)}|ex={ex.Message}");
                return null;
            }
        }
    }
}
