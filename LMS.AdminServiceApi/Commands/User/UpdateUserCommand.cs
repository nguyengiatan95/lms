﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Commands.User
{
    public class UpdateUserCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public Domain.Models.UpdateUserModel UserInfo { get; set; }
    }
    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserGroup> _userGroupTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> _userDepartmentTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> _departmentTab;
        readonly ILogger<UpdateUserCommandHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public UpdateUserCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserGroup> userGroupTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDepartmentTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> departmentTab,
            ILogger<UpdateUserCommandHandler> logger)
        {
            _userTab = userTab;
            _userGroupTab = userGroupTab;
            _userDepartmentTab = userDepartmentTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _departmentTab = departmentTab;
        }
        public async Task<ResponseActionResult> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                DateTime dtNow = DateTime.Now;
                var lstUpdateDepartment = new List<Domain.Tables.TblUserDepartment>();
                var Objuser = await _userTab.GetByIDAsync(request.UserInfo.UserID);
                if (Objuser == null)
                {
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                Objuser.DepartmentID = request.UserInfo.DepartmentID == null ? Objuser.DepartmentID : request.UserInfo.DepartmentID;
                Objuser.Email = request.UserInfo.Email ?? Objuser.Email;
                Objuser.Status = request.UserInfo.Status;
                Objuser.FullName = request.UserInfo.FullName;
                if (!string.IsNullOrWhiteSpace(request.UserInfo.Password))
                {
                    Objuser.Password = _common.HashMD5(request.UserInfo.Password);
                }
                var update = await _userTab.UpdateAsync(Objuser);
                if (update)
                {
                    if (request.UserInfo.LstUserGroup != null || request.UserInfo.LstUserGroup.Count > 0)
                    {
                        //delete quyền cũ
                        _ = await _userGroupTab.DeleteWhereAsync(x => x.UserID == Objuser.UserID);
                        //insert quyền mới
                        var lstGroup = request.UserInfo.LstUserGroup.Select(c => { c.UserID = Objuser.UserID; return c; }).ToList();
                        _userGroupTab.InsertBulk(lstGroup);
                    }
                    if (request.UserInfo.LstUserDepartment.Count > 0)
                    {
                        //List<long> lstPostionID = ((UserDepartment_PositionID[])Enum.GetValues(typeof(UserDepartment_PositionID))).Select(x => (long)x).ToList();
                        var departmentIds = request.UserInfo.LstUserDepartment.Select(x => x.DepartmentID).ToList();

                        var t1 = _userDepartmentTab.DeleteWhereAsync(x => x.UserID == request.UserInfo.UserID);
                        var lstDepartmentInfosTask = _departmentTab.WhereClause(x => departmentIds.Contains(x.DepartmentID)).QueryAsync();
                        await Task.WhenAll(t1, lstDepartmentInfosTask);

                        var lstDepartmentInfos = lstDepartmentInfosTask.Result;

                        List<long> lstAppID = new List<long>();
                        foreach (var item in lstDepartmentInfos)
                        {
                            lstAppID.Add(item.AppID);
                        }
                        List<Domain.Tables.TblUserDepartment> lstInsert = new List<Domain.Tables.TblUserDepartment>();
                        //insert quyền mới
                        foreach (var item in request.UserInfo.LstUserDepartment)
                        {
                            var userDepartmentInsert = new Domain.Tables.TblUserDepartment
                            {
                                CreateDate = DateTime.Now,
                                ModifyDate = DateTime.Now,
                                ChildDepartmentID = item.ChildDepartmentID,
                                DepartmentID = item.DepartmentID,
                                ParentUserID = item.ParentUserID,
                                PositionID = item.PositionID,
                                UserID = Objuser.UserID,
                                Status = item.Status
                            };
                            lstInsert.Add(userDepartmentInsert);
                        }
                        if (lstInsert.Count > 0)
                        {
                            _userDepartmentTab.InsertBulk(lstInsert);
                        }
                        _ = UpdateParentUserIDInSystem(lstAppID);
                    }
                    response.Result = (int)LMS.Common.Constants.ResponseAction.Success;
                    response.Message = MessageConstant.Success;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateUserCommandHandler|request={_common.ConvertObjectToJSonV2<UpdateUserCommand>(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        // nếu cần thiết thì chạy lại toàn bộ cấp của nhân viên
        public async Task UpdateParentUserIDInSystem(List<long> lstAppID)
        {
            try
            {
                _userDepartmentTab.SelectColumns(x => x.UserID, x => x.PositionID, x => x.ParentUserID, x => x.DepartmentID, x => x.ChildDepartmentID, x => x.UserDepartmentID, x => x.CreateDate)
                            .JoinOn<Domain.Tables.TblUser>(ud => ud.UserID, u => u.UserID)
                            .SelectColumnsJoinOn<Domain.Tables.TblUser>(u => u.FullName, u => u.UserName)
                            .WhereClauseJoinOn<Domain.Tables.TblUser>(x => x.Status == (int)StatusCommon.Active)
                            .JoinOn<Domain.Tables.TblDepartment>(ud => ud.DepartmentID, d => d.DepartmentID)
                            .SelectColumnsJoinOn<Domain.Tables.TblDepartment>(d => d.AppID)
                            .WhereClauseJoinOn<Domain.Tables.TblDepartment>(d => lstAppID.Contains(d.AppID))
                            //.WhereClause(x => lstPostionID.Contains(x.PositionID))
                            ;
                var lstUserInSystem = await _userDepartmentTab.QueryAsync<Domain.Models.EmployeeInfoModel>();
                lstUserInSystem = lstUserInSystem.OrderByDescending(x => x.PositionID).ThenBy(x => x.ParentUserID).ToList();
                List<Domain.Tables.TblUserDepartment> lstUpdate = new List<Domain.Tables.TblUserDepartment>();
                foreach (var item in lstUserInSystem)
                {
                    var directorInfo = lstUserInSystem.FirstOrDefault(x => x.PositionID == (int)UserDepartment_PositionID.Director && x.AppID == item.AppID);
                    var managerInfo = lstUserInSystem.FirstOrDefault(x => x.PositionID == (int)UserDepartment_PositionID.Manager && x.DepartmentID == item.DepartmentID);
                    var leaderInfo = lstUserInSystem.FirstOrDefault(x => x.PositionID == (int)UserDepartment_PositionID.Staff && x.ChildDepartmentID == item.ChildDepartmentID);
                    switch ((UserDepartment_PositionID)item.PositionID)
                    {
                        case UserDepartment_PositionID.Director:
                            // lấy vị trí trưởng phòng
                            var lstUserManager = lstUserInSystem.Where(x => x.PositionID == (int)UserDepartment_PositionID.Manager && x.AppID == item.AppID).ToList();
                            foreach (var uM in lstUserManager)
                            {
                                if(uM.ParentUserID != item.UserID)
                                {
                                    uM.ParentUserID = item.UserID;
                                    lstUpdate.Add(new Domain.Tables.TblUserDepartment
                                    {
                                        UserDepartmentID = uM.UserDepartmentID,
                                        ChildDepartmentID = uM.ChildDepartmentID,
                                        CreateDate = uM.CreateDate,
                                        DepartmentID = uM.DepartmentID,
                                        ModifyDate = DateTime.Now,
                                        ParentUserID = item.UserID,
                                        UserID = uM.UserID,
                                        PositionID = uM.PositionID
                                    });
                                }
                            }
                            break;
                        case UserDepartment_PositionID.Manager:
                            // lấy vị trí trưởng nhóm
                            var lstUserLeader = lstUserInSystem.Where(x => x.DepartmentID == item.DepartmentID && x.PositionID == (int)UserDepartment_PositionID.TeamLead).ToList();
                            foreach (var uL in lstUserLeader)
                            {
                                if(uL.ParentUserID != item.UserID)
                                {
                                    uL.ParentUserID = item.UserID;
                                    lstUpdate.Add(new Domain.Tables.TblUserDepartment
                                    {
                                        UserDepartmentID = uL.UserDepartmentID,
                                        ChildDepartmentID = uL.ChildDepartmentID,
                                        CreateDate = uL.CreateDate,
                                        DepartmentID = uL.DepartmentID,
                                        ModifyDate = DateTime.Now,
                                        ParentUserID = item.UserID,
                                        UserID = uL.UserID,
                                        PositionID = uL.PositionID
                                    });
                                }
                            }
                            if (item.ParentUserID == 0 && directorInfo != null)
                            {
                                lstUpdate.Add(new Domain.Tables.TblUserDepartment
                                {
                                    UserDepartmentID = item.UserDepartmentID,
                                    ChildDepartmentID = item.ChildDepartmentID,
                                    CreateDate = item.CreateDate,
                                    DepartmentID = item.DepartmentID,
                                    ModifyDate = DateTime.Now,
                                    ParentUserID = directorInfo.UserID,
                                    UserID = item.UserID,
                                    PositionID = item.PositionID
                                });
                            }
                            break;
                        case UserDepartment_PositionID.TeamLead:
                            var lstUserStaff = lstUserInSystem.Where(x => x.ChildDepartmentID == item.ChildDepartmentID && x.PositionID == (int)UserDepartment_PositionID.Staff).ToList();
                            foreach (var uS in lstUserStaff)
                            {
                                if(uS.ParentUserID != item.UserID)
                                {
                                    uS.ParentUserID = item.UserID;
                                    lstUpdate.Add(new Domain.Tables.TblUserDepartment
                                    {
                                        UserDepartmentID = uS.UserDepartmentID,
                                        ChildDepartmentID = uS.ChildDepartmentID,
                                        CreateDate = uS.CreateDate,
                                        DepartmentID = uS.DepartmentID,
                                        ModifyDate = DateTime.Now,
                                        ParentUserID = item.UserID,
                                        UserID = uS.UserID,
                                        PositionID = uS.PositionID
                                    });
                                }
                            }
                            if (item.ParentUserID == 0)
                            {
                                if (managerInfo != null)
                                {
                                    lstUpdate.Add(new Domain.Tables.TblUserDepartment
                                    {
                                        UserDepartmentID = item.UserDepartmentID,
                                        ChildDepartmentID = item.ChildDepartmentID,
                                        CreateDate = item.CreateDate,
                                        DepartmentID = item.DepartmentID,
                                        ModifyDate = DateTime.Now,
                                        ParentUserID = managerInfo.UserID,
                                        UserID = item.UserID,
                                        PositionID = item.PositionID
                                    });

                                }
                                else if (directorInfo != null)
                                {
                                    lstUpdate.Add(new Domain.Tables.TblUserDepartment
                                    {
                                        UserDepartmentID = item.UserDepartmentID,
                                        ChildDepartmentID = item.ChildDepartmentID,
                                        CreateDate = item.CreateDate,
                                        DepartmentID = item.DepartmentID,
                                        ModifyDate = DateTime.Now,
                                        ParentUserID = directorInfo.UserID,
                                        UserID = item.UserID,
                                        PositionID = item.PositionID
                                    });
                                }
                            }
                            break;
                        case UserDepartment_PositionID.Staff:
                            if (item.ParentUserID == 0)
                            {
                                if (leaderInfo != null)
                                {
                                    lstUpdate.Add(new Domain.Tables.TblUserDepartment
                                    {
                                        UserDepartmentID = item.UserDepartmentID,
                                        ChildDepartmentID = item.ChildDepartmentID,
                                        CreateDate = item.CreateDate,
                                        DepartmentID = item.DepartmentID,
                                        ModifyDate = DateTime.Now,
                                        ParentUserID = leaderInfo.UserID,
                                        UserID = item.UserID,
                                        PositionID = item.PositionID
                                    });
                                }
                                else if (managerInfo != null)
                                {
                                    lstUpdate.Add(new Domain.Tables.TblUserDepartment
                                    {
                                        UserDepartmentID = item.UserDepartmentID,
                                        ChildDepartmentID = item.ChildDepartmentID,
                                        CreateDate = item.CreateDate,
                                        DepartmentID = item.DepartmentID,
                                        ModifyDate = DateTime.Now,
                                        ParentUserID = managerInfo.UserID,
                                        UserID = item.UserID,
                                        PositionID = item.PositionID
                                    });

                                }
                                else if (directorInfo != null)
                                {
                                    lstUpdate.Add(new Domain.Tables.TblUserDepartment
                                    {
                                        UserDepartmentID = item.UserDepartmentID,
                                        ChildDepartmentID = item.ChildDepartmentID,
                                        CreateDate = item.CreateDate,
                                        DepartmentID = item.DepartmentID,
                                        ModifyDate = DateTime.Now,
                                        ParentUserID = directorInfo.UserID,
                                        UserID = item.UserID,
                                        PositionID = item.PositionID
                                    });
                                }
                            }
                            break;
                    }
                }
                if (lstUpdate.Count > 0)
                {
                    _userDepartmentTab.UpdateBulk(lstUpdate);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateParentUserIDInSystem|{ex.Message}-{ex.StackTrace}");
            }
        }
    }
}

