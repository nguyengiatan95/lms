﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Commands.User
{
    public class CreateValidUserCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public Domain.Models.CreateUserModel UserInfo { get; set; }
    }
    public class CreateUserCommandHandler : IRequestHandler<CreateValidUserCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserGroup> _userGroupTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> _userDepartmentTab;
        readonly ILogger<CreateUserCommandHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public CreateUserCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserGroup> userGroupTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDepartmentTab,
            ILogger<CreateUserCommandHandler> logger)
        {
            _userTab = userTab;
            _userGroupTab = userGroupTab;
            _userDepartmentTab = userDepartmentTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateValidUserCommand request, CancellationToken cancellationToken)
        {

            DateTime dtNow = DateTime.Now;
            var lstUpdateDepartment = new List<Domain.Tables.TblUserDepartment>();
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                if (string.IsNullOrEmpty(request.UserInfo.FullName))
                {
                    response.Message = MessageConstant.UserNullFullName;
                    return response;
                }
                if (string.IsNullOrEmpty(request.UserInfo.UserName))
                {
                    response.Message = MessageConstant.UserNullUserName;
                    return response;
                }
                if (string.IsNullOrEmpty(request.UserInfo.Password))
                {
                    response.Message = MessageConstant.UserNullPass;
                    return response;
                }
                if (request.UserInfo.LstUserGroup == null || request.UserInfo.LstUserGroup.Count <= 0)
                {
                    response.Message = MessageConstant.UserNullPermission;
                    return response;
                }
                var checkExistUserName = (await _userTab.WhereClause(x => x.UserName == request.UserInfo.UserName).QueryAsync()).Count() > 0;
                if (checkExistUserName)
                {
                    response.Message = MessageConstant.UserExist;
                    return response;
                }
                request.UserInfo.Password = _common.HashMD5(request.UserInfo.Password);
                var ObjUser = new Domain.Tables.TblUser
                {
                    FullName = request.UserInfo.FullName,
                    DepartmentID = request.UserInfo.DepartmentID,
                    UserName = request.UserInfo.UserName,
                    Password = request.UserInfo.Password,
                    CreateDate = DateTime.Now,
                    Email = request.UserInfo.Email,
                    Status = LMS.Common.Constants.StatusCommon.Active.GetHashCode()
                };
                var UserId = await _userTab.InsertAsync(ObjUser);
                if (UserId > 0)
                {
                    var lstGroup = request.UserInfo.LstUserGroup.Select(c => { c.UserID = UserId; return c; }).ToList();
                    _userGroupTab.InsertBulk(lstGroup);
                    var lstUserDepartment = request.UserInfo.LstUserDepartment.Select(c => { c.UserID = UserId; return c; }).ToList();

                    if (ObjUser.Status == LMS.Common.Constants.StatusCommon.Active.GetHashCode())
                    {
                        var departmentIds = lstUserDepartment.Select(x => x.DepartmentID).ToList();
                        var lstCurrentPersonInDataMain = await _userDepartmentTab.WhereClause(x => departmentIds.Contains(x.DepartmentID)).QueryAsync();

                        var lstUserIds = lstCurrentPersonInDataMain.Select(x => x.UserID).ToList();
                        var lstUserActive = (await _userTab.WhereClause(x => lstUserIds.Contains(x.UserID) && x.Status == (int)StatusCommon.Active).QueryAsync()).Select(x => x.UserID).ToList();

                        foreach (var item in lstUserDepartment)
                        {
                            var lstCurrentPersonInData = lstCurrentPersonInDataMain.Where(x => x.DepartmentID == item.DepartmentID && x.PositionID != item.PositionID);
                            if (lstCurrentPersonInData != null)
                            {
                                lstCurrentPersonInData = lstCurrentPersonInData.Where(x => lstUserActive.Contains(x.UserID)).ToList();
                                var lstLead = lstCurrentPersonInData.Where(x => x.PositionID == (int)UserDepartment_PositionID.TeamLead);
                                var lstStaff = lstCurrentPersonInData.Where(x => x.PositionID == (int)UserDepartment_PositionID.Staff);
                                var lstManager = lstCurrentPersonInData.Where(x => x.PositionID == (int)UserDepartment_PositionID.Manager);
                                var lstDirector = lstCurrentPersonInData.Where(x => x.PositionID == (int)UserDepartment_PositionID.Director);

                                switch ((UserDepartment_PositionID)item.PositionID)
                                {
                                    case UserDepartment_PositionID.Staff:
                                        {
                                            item.ParentUserID = lstLead != null && lstLead.Count() > 0 ? lstLead.FirstOrDefault().UserID
                                                    : (lstManager != null && lstManager.Count() > 0 ? lstManager.FirstOrDefault().UserID
                                                    : (lstDirector != null && lstDirector.Count() > 0 ? lstDirector.FirstOrDefault().UserID : 0));

                                            break;
                                        }
                                    case UserDepartment_PositionID.TeamLead:
                                        {
                                            foreach (var child in lstStaff)
                                            {
                                                lstUpdateDepartment.Add(new Domain.Tables.TblUserDepartment()
                                                {
                                                    ChildDepartmentID = child.ChildDepartmentID,
                                                    CreateDate = child.CreateDate,
                                                    DepartmentID = child.DepartmentID,
                                                    ModifyDate = dtNow,
                                                    ParentUserID = UserId,
                                                    PositionID = child.PositionID,
                                                    UserDepartmentID = child.UserDepartmentID,
                                                    UserID = child.UserID,
                                                    Status = child.Status,
                                                });
                                            }
                                            item.ParentUserID =
                                                    lstManager != null && lstManager.Count() > 0 ? lstManager.FirstOrDefault().UserID
                                                    : (lstDirector != null && lstDirector.Count() > 0 ? lstDirector.FirstOrDefault().UserID : 0);
                                            break;
                                        }
                                    case UserDepartment_PositionID.Manager:
                                        {
                                            foreach (var child in lstLead)
                                            {
                                                lstUpdateDepartment.Add(new Domain.Tables.TblUserDepartment()
                                                {
                                                    ChildDepartmentID = child.ChildDepartmentID,
                                                    CreateDate = child.CreateDate,
                                                    DepartmentID = child.DepartmentID,
                                                    ModifyDate = dtNow,
                                                    ParentUserID = UserId,
                                                    PositionID = child.PositionID,
                                                    UserDepartmentID = child.UserDepartmentID,
                                                    UserID = child.UserID,
                                                    Status = child.Status,
                                                });
                                            }
                                            item.ParentUserID = lstDirector != null && lstDirector.Count() > 0 ? lstDirector.FirstOrDefault().UserID : 0;
                                            break;
                                        }
                                    case UserDepartment_PositionID.Director:
                                        {
                                            foreach (var child in lstManager)
                                            {
                                                lstUpdateDepartment.Add(new Domain.Tables.TblUserDepartment()
                                                {
                                                    ChildDepartmentID = child.ChildDepartmentID,
                                                    CreateDate = child.CreateDate,
                                                    DepartmentID = child.DepartmentID,
                                                    ModifyDate = dtNow,
                                                    ParentUserID = UserId,
                                                    PositionID = child.PositionID,
                                                    UserDepartmentID = child.UserDepartmentID,
                                                    UserID = child.UserID,
                                                    Status = child.Status,
                                                });
                                            }
                                            item.ParentUserID = lstDirector != null && lstDirector.Count() > 0 ? lstDirector.FirstOrDefault().UserID : 0;
                                            break;
                                        }
                                    default:
                                        break;
                                }
                            }
                        }
                    }

                    _userDepartmentTab.InsertBulk(lstUserDepartment);
                    _userDepartmentTab.UpdateBulk(lstUpdateDepartment);
                    response.Result = (int)LMS.Common.Constants.ResponseAction.Success;
                    response.Data = UserId;
                    response.Message = MessageConstant.Success;
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateUserCommandHandler|request={_common.ConvertObjectToJSonV2<CreateValidUserCommand>(request)}|ex={ex.Message}");
                return null;
            }
        }
    }
}
