﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Commands.User
{
    public class CreateUserFromAgCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long TimaUserID { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int Status { get; set; }
    }
    public class CreateUserFromAgCommandHandler : IRequestHandler<CreateUserFromAgCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.AG.TblUserAg> _userAgTab;
        readonly ILogger<CreateUserFromAgCommandHandler> _logger;
        Common.Helper.Utils _common;
        public CreateUserFromAgCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.AG.TblUserAg> userAgTab,
            ILogger<CreateUserFromAgCommandHandler> logger)
        {
            _userTab = userTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
            _userAgTab = userAgTab.SetConnectString(LMS.Common.Configuration.ConnectStringSetting.AGConnectString);
        }
        public async Task<ResponseActionResult> Handle(CreateUserFromAgCommand request, CancellationToken cancellationToken)
        {
            DateTime dtNow = DateTime.Now;
            var lstUpdateDepartment = new List<Domain.Tables.TblUserDepartment>();
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                if (string.IsNullOrEmpty(request.FullName))
                {
                    response.Message = MessageConstant.UserNullFullName;
                    return response;
                }
                if (string.IsNullOrEmpty(request.UserName))
                {
                    response.Message = MessageConstant.UserNullUserName;
                    return response;
                }
                if (string.IsNullOrEmpty(request.Password))
                {
                    response.Message = MessageConstant.UserNullPass;
                    return response;
                }
                // tima đang truyền sai userid lấy lại id từ ag
                var userAGDetail = (await _userAgTab.WhereClause(x => x.Username == request.UserName).QueryAsync()).FirstOrDefault();
                if (userAGDetail == null || userAGDetail.Id < 1)
                {
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                request.TimaUserID = userAGDetail.Id;
                var userDetail = (await _userTab.WhereClause(x => x.TimaUserID == request.TimaUserID).QueryAsync()).FirstOrDefault();
                if (userDetail == null || userDetail.UserID < 1)
                {
                    userDetail = new Domain.Tables.TblUser
                    {
                        FullName = request.FullName,
                        UserName = request.UserName,
                        Password = request.Password,
                        CreateDate = DateTime.Now,
                        Email = request.Email,
                        Status = ConvertStatusToLMS(request.Status),
                        UserID = request.TimaUserID,
                        TimaUserID = request.TimaUserID
                    };
                    userDetail.UserID = await _userTab.InsertAsync(userDetail, true);
                }
                else
                {
                    userDetail.FullName = request.FullName;
                    userDetail.Status = ConvertStatusToLMS(request.Status);
                    _ = await _userTab.UpdateAsync(userDetail);
                }
                response.SetSucces();
                response.Data = userDetail.UserID;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateUserFromAgCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }
        int ConvertStatusToLMS(int status)
        {
            return status switch
            {
                1 => (int)LMS.Common.Constants.User_Status.Active,
                0 => (int)LMS.Common.Constants.User_Status.Locked,
                _ => (int)LMS.Common.Constants.User_Status.Deleted,
            };
        }
    }
}
