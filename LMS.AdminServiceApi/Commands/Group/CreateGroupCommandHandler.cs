﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Commands.Group
{
    public class CreateValidGroupCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public Domain.Models.CreateGroupModel GroupInfo { get; set; }
    }
    public class CreateGroupCommandHandler : IRequestHandler<CreateValidGroupCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblGroup> _groupTab;
        ILogger<CreateGroupCommandHandler> _logger;
        Common.Helper.Utils common;
        public CreateGroupCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblGroup> groupTab, ILogger<CreateGroupCommandHandler> logger)
        {
            _groupTab = groupTab;
            _logger = logger;
            common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateValidGroupCommand request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {


                    if (string.IsNullOrEmpty(request.GroupInfo.GroupName))
                    {
                        response.Message = MessageConstant.GroupNameNull;
                        return response;
                    }
                    var checkGroup = _groupTab.WhereClause(x => x.GroupName == request.GroupInfo.GroupName).Query().FirstOrDefault();
                    if (checkGroup != null)
                    {
                        response.Message = MessageConstant.GroupNameExist;
                        return response;
                    }
                    var Objdepartment = new Domain.Tables.TblGroup
                    {
                        GroupName = request.GroupInfo.GroupName,
                        Status = LMS.Common.Constants.StatusCommon.Active.GetHashCode()
                    };
                    var Insert = _groupTab.Insert(Objdepartment);
                    if (Insert > 0)
                    {
                        response.Result = (int)LMS.Common.Constants.ResponseAction.Success;
                        response.Data = Insert;
                        response.Message = MessageConstant.Success;
                    }
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"CreateGroupCommandHandler|request={common.ConvertObjectToJSonV2<CreateValidGroupCommand>(request)}|ex={ex.Message}");
                    return null;
                }
            });
        }
    }
}
