﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Commands.Group
{
    public class UpdateGroupCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public Domain.Models.UpdateGroupModel GroupInfo { get; set; }
    }
    public class UpdateGroupCommandHandler : IRequestHandler<UpdateGroupCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblGroup> _groupTab;
        ILogger<UpdateGroupCommandHandler> _logger;
        Common.Helper.Utils common;
        public UpdateGroupCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblGroup> groupTab, ILogger<UpdateGroupCommandHandler> logger)
        {
            _groupTab = groupTab;
            _logger = logger;
            common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(UpdateGroupCommand request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    if (string.IsNullOrEmpty(request.GroupInfo.GroupName))
                    {
                        response.Message = MessageConstant.GroupNameNull;
                        return response;
                    }
                    var ObjGroup = _groupTab.GetByID(request.GroupInfo.GroupID);
                    if (ObjGroup == null)
                    {
                        response.Message = MessageConstant.GroupNotExist;
                        return response;
                    }
                    ObjGroup.GroupName = request.GroupInfo.GroupName;
                    ObjGroup.Status = request.GroupInfo.Status;
                    var update = _groupTab.Update(ObjGroup);
                    if (update)
                    {
                        response.Result = (int)LMS.Common.Constants.ResponseAction.Success;
                        response.Data = ObjGroup;
                        response.Message = MessageConstant.Success;
                    }
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"UpdateGroupCommandHandler|request={common.ConvertObjectToJSonV2<UpdateGroupCommand>(request)}|ex={ex.Message}");
                    return response;
                }
            });
        }
    }
}
