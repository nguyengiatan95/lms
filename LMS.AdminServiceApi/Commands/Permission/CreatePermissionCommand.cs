﻿using LMS.AdminServiceApi.Domain.Models;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Commands.Permission
{
    public class CreatePermissionValidCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public Domain.Models.CreatePermissionCommandModel PermissionInfo { get; set; }
    }

    public class CreatePermissionValidCommandHandler : IRequestHandler<CreatePermissionValidCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPermission> _permissionTab;
        ILogger<CreatePermissionValidCommandHandler> _logger;
        public CreatePermissionValidCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPermission> permissionTab, ILogger<CreatePermissionValidCommandHandler> logger)
        {
            _permissionTab = permissionTab.SetConnectString(LMS.Common.Configuration.ConnectStringSetting.DefaultConnectString);
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(CreatePermissionValidCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var lstPermission = await _permissionTab.WhereClause(x => x.LinkApi == request.PermissionInfo.LinkApi).QueryAsync();
            if (lstPermission != null && lstPermission.Any())
            {
                response.Message = Common.Constants.MessageConstant.ErrorExist;
                return response;
            }
            Domain.Tables.TblPermission tblPermission = new Domain.Tables.TblPermission()
            {
                DisplayText = request.PermissionInfo.DisplayText,
                IsActive = request.PermissionInfo.IsActive,
                LinkApi = request.PermissionInfo.LinkApi.ToLower(),
                CreateDate = DateTime.Now,
                ModifyDate = DateTime.Now
            };
            var permissionID = _permissionTab.Insert(tblPermission);
            if (permissionID > 0)
            {
                response = new ResponseActionResult()
                {
                    Result = (int)LMS.Common.Constants.ResponseAction.Success,
                    Data = request,
                    Message = ""
                };
            }
            return response;
        }
    }
}
