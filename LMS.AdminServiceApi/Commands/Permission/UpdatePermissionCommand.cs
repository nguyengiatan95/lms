﻿using LMS.AdminServiceApi.Domain.Models;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Commands.Permission
{
    public class UpdatePermissionValidCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public Domain.Models.UpdatePermissionCommandModel RequestUpdate { get; set; }
    }

    public class UpdatePermissionValidCommandHandler : IRequestHandler<UpdatePermissionValidCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPermission> _permissionTab;
        ILogger<UpdatePermissionValidCommandHandler> _logger;
        public UpdatePermissionValidCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPermission> permissionTab, ILogger<UpdatePermissionValidCommandHandler> logger)
        {
            _permissionTab = permissionTab.SetConnectString(LMS.Common.Configuration.ConnectStringSetting.DefaultConnectString);
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(UpdatePermissionValidCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var permission = _permissionTab.GetByID(request.RequestUpdate.PermissionID);
            if (permission == null || permission.PermissionID == 0)
            {
                response.Message = Common.Constants.MessageConstant.ErrorNotExist;
                return response;
            }
            var lstPermission = _permissionTab.WhereClause(x => x.LinkApi == request.RequestUpdate.LinkApi).Query();
            if (lstPermission != null && lstPermission.Any() && lstPermission.Where(x => x.PermissionID != request.RequestUpdate.PermissionID).Any())
            {
                response.Message = Common.Constants.MessageConstant.ErrorExist;
                return response;
            }
            permission.DisplayText = request.RequestUpdate.DisplayText;
            permission.IsActive = request.RequestUpdate.IsActive;
            permission.ModifyDate = DateTime.Now;
            permission.LinkApi = request.RequestUpdate.LinkApi.ToLower();
            var update = _permissionTab.Update(permission);
            if (update)
            {
                response = new ResponseActionResult()
                {
                    Result = (int)LMS.Common.Constants.ResponseAction.Success,
                    Data = request.RequestUpdate.PermissionID,
                    Message = ""
                };
            }
            return response;
        }
    }
}
