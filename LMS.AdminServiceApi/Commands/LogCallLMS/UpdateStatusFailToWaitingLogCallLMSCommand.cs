﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AdminServiceApi.Commands.LogCallLMS
{
    public class UpdateStatusFailToWaitingLogCallLMSCommand : IRequest<ResponseActionResult>
    {
        public long LogCallLMSID { get; set; }
    }

    public class UpdateStatusFailToWaitingLogCallLMSCommandCommandHandler : IRequestHandler<UpdateStatusFailToWaitingLogCallLMSCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCallLMS> _logCallLMSTab;
        Common.Helper.Utils common;
        ILogger<UpdateStatusFailToWaitingLogCallLMSCommandCommandHandler> _logger;
        public UpdateStatusFailToWaitingLogCallLMSCommandCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCallLMS> logCallLMSTab,
            ILogger<UpdateStatusFailToWaitingLogCallLMSCommandCommandHandler> logger)
        {
            _logCallLMSTab = logCallLMSTab.SetConnectString(LMS.Common.Configuration.ConnectStringSetting.AGConnectString);
            common = new Common.Helper.Utils();
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(UpdateStatusFailToWaitingLogCallLMSCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objLogCallLMS = (await _logCallLMSTab.WhereClause(x => x.LogCallLMSID == request.LogCallLMSID).QueryAsync()).FirstOrDefault();
                if (objLogCallLMS == null)
                {
                    response.Message = MessageConstant.ParameterInvalid;
                    return response;
                }
                objLogCallLMS.Status = (int)LogCallLMS_Status.Waiting;
                objLogCallLMS.ModifyDate = DateTime.Now;
                if (await _logCallLMSTab.UpdateAsync(objLogCallLMS))
                {
                    response.SetSucces();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateStatusFailToWaitingLogCallLMSCommandCommandHandler|request={common.ConvertObjectToJSonV2<UpdateStatusFailToWaitingLogCallLMSCommand>(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
