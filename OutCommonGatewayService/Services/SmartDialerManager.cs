﻿using LMS.Common.Constants;
using Microsoft.Extensions.Logging;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutGatewayApi.Services
{
    public interface ISmartDialerManager
    {
        Task<ResponseActionResult> GetCampain();
        Task<ResponseActionResult> ImportCampaign(LMS.Entites.Dtos.SmartDialer.ImportCampaignRequest dataPost);
        Task<ResponseActionResult> GetPendingOfCampaign(LMS.Entites.Dtos.SmartDialer.PendingOfCampaignRequest dataPost);
        Task<ResponseActionResult> UpdateResource(LMS.Entites.Dtos.SmartDialer.UpdateResourceRequest dataPost);
        Task<ResponseActionResult> UpdateCsq(LMS.Entites.Dtos.SmartDialer.UpdateCsqRequest dataPost);
    }
    public class SmartDialerManager : ISmartDialerManager
    {
        readonly ILogger<SmartDialerManager> _logger;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogPushCampaign> _logPushCampaignTab;
        readonly LMS.Common.Helper.Utils _common;
        readonly Domain.Models.SmartDialerConfiguration _smartDialerConfiguration;
        public SmartDialerManager(ILogger<SmartDialerManager> logger, Domain.Models.SmartDialerConfiguration smartDialerConfiguration,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogPushCampaign> logPushCampaignTab)
        {
            _logger = logger;
            _smartDialerConfiguration = smartDialerConfiguration;
            _common = new LMS.Common.Helper.Utils();
            _logPushCampaignTab = logPushCampaignTab;
        }
        public async Task<ResponseActionResult> GetCampain()
        {
            var result = new ResponseActionResult();
            string url = $"{_smartDialerConfiguration.Url}{LMS.Entites.Dtos.SmartDialer.SmartDialerConfiguration.ApiGetCampain}";
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest();
                request.Parameters.Clear();
                request.Method = Method.GET;
                request.AddHeader("Content-Type", _smartDialerConfiguration.ContentType);
                request.AddHeader("Authorization", _smartDialerConfiguration.Authorization);
                IRestResponse response = await client.ExecuteAsync(request);
                _logger.LogInformation($"GetCampain|url={url}|responseContent={response.Content}");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseData = _common.ConvertJSonToObjectV2<LMS.Entites.Dtos.SmartDialer.SmartDialerBaseResponse>(response.Content);
                    if (responseData.Code == (int)LMS.Entites.Dtos.SmartDialer.ApiResultCode.Success)
                    {
                        result.Data = _common.ConvertJSonToObjectV2<List<LMS.Entites.Dtos.SmartDialer.CampaignDetail>>(responseData.Data.ToString());
                        result.SetSucces();
                    }
                    else
                    {
                        result.Message = responseData.Message;
                        _logger.LogError($"GetCampain|url={url}|responseContent={response.Content}");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetCampain|url={url}|ex={ex.Message}-{ex.StackTrace}");
            }
            return result;
        }
        public async Task<ResponseActionResult> ImportCampaign(LMS.Entites.Dtos.SmartDialer.ImportCampaignRequest dataPost)
        {
            var result = new ResponseActionResult();
            string url = $"{_smartDialerConfiguration.Url}{LMS.Entites.Dtos.SmartDialer.SmartDialerConfiguration.ApiImportCampaign}";
            string body = "";
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest();
                request.Parameters.Clear();
                body = _common.ConvertObjectToJSonV2(dataPost);
                request.Method = Method.POST;
                request.AddHeader("Content-Type", _smartDialerConfiguration.ContentType);
                request.AddHeader("Authorization", _smartDialerConfiguration.Authorization);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = await client.ExecuteAsync(request);
                _logger.LogInformation($"ImportCampaign|url={url}|body={body}|responseContent={response.Content}");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseData = _common.ConvertJSonToObjectV2<LMS.Entites.Dtos.SmartDialer.SmartDialerBaseResponse>(response.Content);
                    if (responseData.Code == (int)LMS.Entites.Dtos.SmartDialer.ApiResultCode.Success)
                    {
                        result.SetSucces();
                    }
                    else
                    {
                        result.Message = responseData.Message;
                        _logger.LogError($"ImportCampaign|url={url}|body={body}|responseContent={response.Content}");
                    }
                }
                try
                {

                    _ = _logPushCampaignTab.InsertAsync(new Domain.Tables.TblLogPushCampaign
                    {
                        CreateDate = DateTime.Now,
                        DataPost = body,
                        ResponseContext = response.Content,
                        Url = url
                    });
                }
                catch (Exception ex)
                {
                    _logger.LogError($"ImportCampaign_InsertLog|url={url}|body={body}|ex={ex.Message}-{ex.StackTrace}");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ImportCampaign|url={url}|body={body}|ex={ex.Message}-{ex.StackTrace}");
            }
            return result;
        }

        public async Task<ResponseActionResult> GetPendingOfCampaign(LMS.Entites.Dtos.SmartDialer.PendingOfCampaignRequest dataPost)
        {
            var result = new ResponseActionResult();
            string url = $"{_smartDialerConfiguration.Url}{LMS.Entites.Dtos.SmartDialer.SmartDialerConfiguration.ApiGetPendingOfCampaign}";
            string body = "";
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest();
                request.Parameters.Clear();
                body = _common.ConvertObjectToJSonV2(dataPost);
                request.Method = Method.POST;
                request.AddHeader("Content-Type", _smartDialerConfiguration.ContentType);
                request.AddHeader("Authorization", _smartDialerConfiguration.Authorization);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = await client.ExecuteAsync(request);
                _logger.LogInformation($"GetPendingOfCampaign|url={url}|body={body}|responseContent={response.Content}");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseData = _common.ConvertJSonToObjectV2<LMS.Entites.Dtos.SmartDialer.SmartDialerBaseResponse>(response.Content);
                    if (responseData.Code == (int)LMS.Entites.Dtos.SmartDialer.ApiResultCode.Success)
                    {
                        if (responseData.Data != null)
                            result.Data = _common.ConvertJSonToObjectV2<List<LMS.Entites.Dtos.SmartDialer.PendingOfCampaignResponse>>(responseData.Data.ToString());
                        else
                        {
                            result.Data = new List<LMS.Entites.Dtos.SmartDialer.PendingOfCampaignResponse>();
                        }
                        result.SetSucces();
                    }
                    else
                    {
                        result.Message = responseData.Message;
                        _logger.LogError($"GetPendingOfCampaign|url={url}|body={body}|responseContent={response.Content}");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetPendingOfCampaign|url={url}|body={body}|ex={ex.Message}-{ex.StackTrace}");
            }
            return result;
        }

        public async Task<ResponseActionResult> UpdateResource(LMS.Entites.Dtos.SmartDialer.UpdateResourceRequest dataPost)
        {
            var result = new ResponseActionResult();
            string url = $"{_smartDialerConfiguration.Url}{LMS.Entites.Dtos.SmartDialer.SmartDialerConfiguration.ApiUpdateResource}";
            string body = "";
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest();
                request.Parameters.Clear();
                body = _common.ConvertObjectToJSonV2(dataPost);
                request.Method = Method.POST;
                request.AddHeader("Content-Type", _smartDialerConfiguration.ContentType);
                request.AddHeader("Authorization", _smartDialerConfiguration.Authorization);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = await client.ExecuteAsync(request);
                _logger.LogInformation($"UpdateResource|url={url}|body={body}|responseContent={response.Content}");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseData = _common.ConvertJSonToObjectV2<LMS.Entites.Dtos.SmartDialer.SmartDialerBaseResponse>(response.Content);
                    if (responseData.Code == (int)LMS.Entites.Dtos.SmartDialer.ApiResultCode.Success)
                    {
                        result.SetSucces();
                    }
                    else
                    {
                        result.Message = responseData.Message;
                        _logger.LogError($"UpdateResource|url={url}|body={body}|responseContent={response.Content}");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateResource|url={url}|body={body}|ex={ex.Message}-{ex.StackTrace}");
            }
            return result;
        }
        public async Task<ResponseActionResult> UpdateCsq(LMS.Entites.Dtos.SmartDialer.UpdateCsqRequest dataPost)
        {
            var result = new ResponseActionResult();
            string url = $"{_smartDialerConfiguration.Url}{LMS.Entites.Dtos.SmartDialer.SmartDialerConfiguration.ApiUpdateCsq}";
            string body = "";
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest();
                request.Parameters.Clear();
                body = _common.ConvertObjectToJSonV2(dataPost);
                request.Method = Method.POST;
                request.AddHeader("Content-Type", _smartDialerConfiguration.ContentType);
                request.AddHeader("Authorization", _smartDialerConfiguration.Authorization);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = await client.ExecuteAsync(request);
                _logger.LogInformation($"UpdateCsq|url={url}|body={body}|responseContent={response.Content}");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseData = _common.ConvertJSonToObjectV2<LMS.Entites.Dtos.SmartDialer.SmartDialerBaseResponse>(response.Content);
                    if (responseData.Code == (int)LMS.Entites.Dtos.SmartDialer.ApiResultCode.Success && responseData.Data != null)
                    {
                        result.Data = _common.ConvertJSonToObjectV2<List<LMS.Entites.Dtos.SmartDialer.UpdateCsqResponse>>(responseData.Data.ToString());
                        result.SetSucces();
                    }
                    else
                    {
                        result.Message = responseData.Message;
                        _logger.LogError($"UpdateCsq|url={url}|body={body}|responseContent={response.Content}");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateUpdateCsqResource|url={url}|body={body}|ex={ex.Message}-{ex.StackTrace}");
            }
            return result;
        }

    }
}
