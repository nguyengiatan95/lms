﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.AG;
using LMS.Entites.Dtos.SendMail;
using Microsoft.Extensions.Logging;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OutGatewayApi.Services
{
    public interface IQueueCallAPIManager
    {
        Task<ResponseActionResult> QueueCallAPI();
        Task<ResponseActionResult> CallApiAg(AgDataPostModel request);
    }
    public class QueueCallAPIManager : IQueueCallAPIManager
    {
        readonly LMS.Common.Helper.Utils _common;
        readonly ILogger<QueueCallAPIManager> _logger;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> _settingKeyTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> _queueCallAPITab;
        readonly Domain.Models.AGConfiguration _agConfiguration;
        readonly IProxyTimaManager _proxyTimaManager;
        public QueueCallAPIManager(
            ILogger<QueueCallAPIManager> logger,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> settingKeyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> queueCallAPITab,
            Domain.Models.AGConfiguration agConfiguration,
            IProxyTimaManager proxyTimaManager
          )
        {
            _logger = logger;
            _settingKeyTab = settingKeyTab;
            _queueCallAPITab = queueCallAPITab;
            _common = new LMS.Common.Helper.Utils();
            _agConfiguration = agConfiguration;
            _proxyTimaManager = proxyTimaManager;
        }
        public async Task<ResponseActionResult> QueueCallAPI()
        {
            var response = new ResponseActionResult();
            try
            {
                var keySettingInfo = (await _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.AG_QueueCallAPI).QueryAsync()).FirstOrDefault();
                if (keySettingInfo == null || keySettingInfo.Status == (int)SettingKey_Status.InActice)
                {
                    response.Message = MessageConstant.SettingKey_AG_QueueCallAPI;
                    return response;
                }
                await UpdateSettingKey(keySettingInfo, (int)SettingKey_Status.InActice);
                _ = ProccessQueueCallApiAg(keySettingInfo);
                await Task.Delay(1);
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"QueueCallAPIManager_QueueCallAPI|ex={ex.Message}|StackTrace={ex.StackTrace}");
            }
            return response;
        }
        private async Task ProccessQueueCallApiAg(Domain.Tables.TblSettingKey keySettingInfo)
        {
            long latestID = 0;
            Dictionary<long, Task> dictActionResultTask = new Dictionary<long, Task>();
            DateTime currentDate = DateTime.Now;
            try
            {
                while (true)
                {
                    // lấy danh sách chưa chạy với trạng thái (int)QueueCallAPI_Status.Waiting ,  (int)QueueCallAPI_Status.Fail
                    // 15 phút chạy lại 1 lần 
                    var dateRunError = currentDate.AddMinutes(-15);
                    int waitingRun = (int)QueueCallAPI_Status.Waiting;
                    int rerun = (int)QueueCallAPI_Status.Fail;
                    int maxRetry = TimaSettingConstant.MaxRetryCallAG;
                    var lstqueueCallAPI = await _queueCallAPITab.SetGetTop(TimaSettingConstant.MaxItemQuery)
                                                                .WhereClause(x => (x.Status == waitingRun || (x.Status == rerun && x.Retry < maxRetry && x.LastModifyDate <= dateRunError)) && x.QueueCallAPIID > latestID)
                                                                .OrderBy(x => x.QueueCallAPIID)
                                                                .QueryAsync();
                    if (lstqueueCallAPI == null || !lstqueueCallAPI.Any())
                    {
                        break;
                    }
                    latestID = lstqueueCallAPI.Last().QueueCallAPIID;
                    foreach (var item in lstqueueCallAPI)
                    {
                        if (item.Domain == LMS.Entites.Dtos.ProxyTima.ProxyTimaConfiguration.BaseUrl)
                        {
                            var sendMail = SendMailProxy(item.Domain, item.Path, item.JsonRequest);
                            dictActionResultTask.Add(item.QueueCallAPIID, sendMail);
                        }
                        else
                        {
                            var callAG = CallApiAg(new AgDataPostModel { Path = item.Path, DataPost = item.JsonRequest });
                            // QueueCallAPIAG(item.Domain, item.Path, item.JsonRequest);
                            dictActionResultTask.Add(item.QueueCallAPIID, callAG);
                        }
                    }
                    await Task.WhenAll(dictActionResultTask.Values);
                    foreach (var item in lstqueueCallAPI)
                    {
                        item.Status = (int)QueueCallAPI_Status.Fail;
                        try
                        {
                            var actionResult = ((Task<ResponseActionResult>)dictActionResultTask[item.QueueCallAPIID]).Result;
                            if (actionResult.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                                item.Status = (int)QueueCallAPI_Status.Done;

                            item.LastModifyDate = currentDate;
                            item.JsonResponse = _common.ConvertObjectToJSonV2(actionResult.Data);
                            // call lỗi thì update retry thêm 1
                            if (item.Status == (int)QueueCallAPI_Status.Fail)
                            {
                                item.Retry += 1;
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError($"ProccessQueueCallApiAg_item|item={_common.ConvertObjectToJSonV2(item)}|ex={ex.Message}-{ex.StackTrace}");
                        }
                    }
                    _queueCallAPITab.UpdateBulk(lstqueueCallAPI);
                    dictActionResultTask.Clear();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProccessQueueCallApiAg|ex={ex.Message}-{ex.StackTrace}");
            }
            await UpdateSettingKey(keySettingInfo, (int)SettingKey_Status.Active);

        }
        private async Task UpdateSettingKey(Domain.Tables.TblSettingKey objSettingKey, int status, int retryUpdateKey = 0)
        {
            bool updateResult = false;
            do
            {
                try
                {
                    objSettingKey.ModifyDate = DateTime.Now;
                    objSettingKey.Status = status;
                    updateResult = await _settingKeyTab.UpdateAsync(objSettingKey);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"ProccessQueueCallApiAg_UpdateSettingKey|ex={ex.Message}-{ex.StackTrace}");
                    // retry lại 
                    if (retryUpdateKey > 10)
                    {
                        return;
                    }
                    await Task.Delay(TimeSpan.FromSeconds(2));
                    retryUpdateKey += 1;
                }
            } while (!updateResult);

        }

        private async Task<ResponseActionResult> SendMailProxy(string doamin, string path, string body)
        {
            var resultData = new ResponseActionResult();
            try
            {
                var result = await _proxyTimaManager.SendProxyTima($"{doamin}{path}", body, Method.POST);
                if (result.Result == (int)AGResponse_Status.Success)
                    resultData.SetSucces();
                #region todo remove
                //var client = new RestClient(doamin);
                //var request = new RestRequest(path);
                //request.Method = Method.POST;
                //request.Parameters.Clear();
                //request.AddHeader("Accept", "application/json");
                //request.AddParameter("Authorization", SendMailProxyContants.Authorization, ParameterType.HttpHeader);
                //request.AddParameter("account", SendMailProxyContants.account, ParameterType.HttpHeader);
                //request.AddParameter("password", SendMailProxyContants.password, ParameterType.HttpHeader);
                //request.AddParameter("application/json", body, ParameterType.RequestBody);
                //var response = await client.ExecuteAsync(request);
                //var result = _common.ConvertJSonToObjectV2<ResponseActionResult>(response.Content);
                //if (response.StatusCode == HttpStatusCode.OK)
                //{
                //    if (result.Result == (int)AGResponse_Status.Success)
                //        resultData.SetSucces();
                //    else
                //        _logger.LogError($"SendMailProxy_Response|Api=${doamin}{path}|body={body}|response={response.Content}");
                //}
                //else
                //{
                //    _logger.LogError($"SendMailProxy_ExecuteAsync|Api=${doamin}{path}|body={body}|response={response.Content}");
                //}
                #endregion
                resultData.Data = result;
            }
            catch (Exception ex)
            {
                _logger.LogError($"SendMailProxy_Exception|Api={doamin + path}|body={body}|ex={ex.Message}-{ex.StackTrace}");
            }
            return resultData;
        }

        public async Task<ResponseActionResult> CallApiAg(AgDataPostModel model)
        {
            var resultData = new ResponseActionResult();
            var url = $"{_agConfiguration.Url}{model.Path}";
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.Parameters.Clear();
                if (!string.IsNullOrEmpty(model.DataPost))
                {
                    request.Method = Method.POST;
                    request.AddParameter("application/json", model.DataPost, ParameterType.RequestBody);
                }
                request.AddHeader("Accept", "application/json");
                request.AddParameter("Authorization", _agConfiguration.Authorization, ParameterType.HttpHeader);
                var response = await client.ExecuteAsync(request);
                _logger.LogInformation($"CallApiAg_Response|Api={url}|DataPost=${_common.ConvertObjectToJSonV2(model)}|responseContent={response.Content}");
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var result = _common.ConvertJSonToObjectV2<AGResponse>(response.Content);
                    resultData.Message = result.Message;
                    if (result.Status == (int)AGResponse_Status.Success)
                    {
                        resultData.SetSucces();
                        resultData.Data = result;
                    }
                    else
                    {
                        _logger.LogError($"CallApiAg_Response|Api={url}|DataPost=${_common.ConvertObjectToJSonV2(model)}|response={response.Content}");
                    }
                }
                else
                {
                    _logger.LogError($"CallApiAg_Response_StatusCode|Api={url}|DataPost=${_common.ConvertObjectToJSonV2(model)}|response={response.Content}");
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"CallApiAg_Exception|Api={url}|DataPost=${_common.ConvertObjectToJSonV2(model)}|ex={e.Message}-{e.StackTrace}");
            }
            return resultData;
        }
    }
}
