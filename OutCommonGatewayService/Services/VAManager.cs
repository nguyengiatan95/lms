﻿using LMS.Entites.Dtos.VAService;
using Microsoft.Extensions.Logging;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutGatewayApi.Services
{
    public interface IVAManager
    {
        VAResponse Register(RegisterVA entity);
    }
    public class VAManager : IVAManager
    {
        ILogger<VAManager> _logger;
        LMS.Common.Helper.Utils _common;
        public VAManager(ILogger<VAManager> logger)
        {
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public VAResponse Register(RegisterVA entity)
        {
            var url = VAConstants.BaseUrl;
            var result = new VAResponse();
            try
            {
                var body = _common.ConvertObjectToJSonV2(entity);
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                var jsonResponse = response.Content;
                result = _common.ConvertJSonToObjectV2<VAResponse>(response.Content);
            }
            catch (Exception ex)
            {
                _logger.LogError($"RegisterVA_Exception|Api={url}|ex={ex.Message}-{ex.StackTrace}");
                result.Message = $"{ex.Message}-{ex.StackTrace}";
            }
            return result;
        }
    }
}
