﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.InsuranceService;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OutGatewayApi.Domain.Tables;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OutGatewayApi.Services
{
    public interface IInsuranceManager
    {
        Task<ResponseActionResult> CreateBorrowerContract(CreateBorrowerContract entity);
        Task<ResponseActionResult> CreateLenderContract(CreateLenderContract entity);
        Task<ResponseActionResult> GetDoc(string contract, int type, long id);
        Task<ResponseActionResult> GetLoanCertificateInformation(string Path, int DocType);
        Task<ResponseActionResult> BuyInsuranceMaterial(CreateInsuranceMaterialReq entity);
    }
    public class InsuranceManager : IInsuranceManager
    {
        LMS.Common.Helper.SlackClient _slackClient;
        LMS.Common.Helper.Utils _common;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInsurance> _logInsuranceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTokenAI> _tokenAITab;
        Domain.Models.BaoMinhConfiguration _baoMinhConfig;
        ILogger<InsuranceManager> _logger;
        public InsuranceManager(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogInsurance> logInsuranceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTokenAI> tokenAITab,
            Domain.Models.BaoMinhConfiguration baoMinhConfig,
        ILogger<InsuranceManager> logger)
        {
            _slackClient = new LMS.Common.Helper.SlackClient();
            _common = new LMS.Common.Helper.Utils();
            _logInsuranceTab = logInsuranceTab;
            _baoMinhConfig = baoMinhConfig;
            _logger = logger;
            _tokenAITab = tokenAITab;
        }
        public async Task<ResponseActionResult> CreateBorrowerContract(CreateBorrowerContract entity)
        {
            var result = new ResponseActionResult();
            var body = JsonConvert.SerializeObject(entity.CreateContract);
            var client = new RestClient(BaoMinhConsants.BaseUrl);
            var request = new RestRequest(BaoMinhConsants.BuyInsuranceBorrower);
            request.Method = Method.POST;
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.Parameters.Clear();
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                var objLogInsurance = new Domain.Tables.TblLogInsurance
                {
                    CreateDate = DateTime.Now,
                    Request = body,
                    TypeInsurance = (int)LMS.Common.Constants.Insurance_TypeInsurance.Customer,
                    LoanID = entity.LoanID,
                    CustomerID = entity.CustomerID,
                    InsuranceCompensator = (int)Insurance_PartnerCode.BaoMinh,
                    TypeRequest = (int)LogInsurance_TypeRequest.Create
                };
                var insertID = _logInsuranceTab.Insert(objLogInsurance);

                var response = await client.ExecuteAsync(request);
                var data = new OutputCreateBorrowerContract();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var jsonResponse = response.Content;
                    data = Newtonsoft.Json.JsonConvert.DeserializeObject<OutputCreateBorrowerContract>(jsonResponse);
                    result.Data = data;
                    objLogInsurance.ResponseCode = data.Meta.ErrCode.ToString();
                    result.SetSucces();
                }
                objLogInsurance.Response = response.Content;
                objLogInsurance.LogInsuranceID = insertID;
                _logInsuranceTab.Update(objLogInsurance);
            }
            catch (Exception ex)
            {
                _logger.LogError($"InsuranceManager_CreateBorrowerContract|request={_common.ConvertObjectToJSonV2(entity)}|ex={ex.Message}|StackTrace={ex.StackTrace}");
            }
            return result;
        }
        public async Task<ResponseActionResult> CreateLenderContract(CreateLenderContract entity)
        {
            var result = new ResponseActionResult();
            var body = JsonConvert.SerializeObject(entity.CreateContract);
            var client = new RestClient(BaoMinhConsants.BaseUrl);
            var request = new RestRequest(BaoMinhConsants.BuyInsuranceLender);
            request.Method = Method.POST;
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.Parameters.Clear();
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                var objLogInsurance = new Domain.Tables.TblLogInsurance
                {
                    CreateDate = DateTime.Now,
                    Request = body,
                    TypeInsurance = (int)LMS.Common.Constants.Insurance_TypeInsurance.Lender,
                    LoanID = entity.LoanID,
                    LenderID = entity.LenderID,
                    InsuranceCompensator = (int)Insurance_PartnerCode.BaoMinh,
                    TypeRequest = (int)LogInsurance_TypeRequest.Create
                };
                var insertID = _logInsuranceTab.Insert(objLogInsurance);
                var response = await client.ExecuteAsync(request);
                var data = new OutputCreateLenderContract();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var jsonResponse = response.Content;
                    data = Newtonsoft.Json.JsonConvert.DeserializeObject<OutputCreateLenderContract>(jsonResponse);
                    objLogInsurance.ResponseCode = data.Meta.ErrCode.ToString();
                    result.Data = data;
                    result.SetSucces();
                }
                objLogInsurance.Response = response.Content;
                objLogInsurance.LogInsuranceID = insertID;
                _logInsuranceTab.Update(objLogInsurance);
            }
            catch (Exception ex)
            {
                _logger.LogWarning($"InsuranceManager_CreateLenderContract|request={_common.ConvertObjectToJSonV2(entity)}|ex={ex.Message}|StackTrace={ex.StackTrace}");
            }
            return result;
        }

        public async Task<ResponseActionResult> GetDoc(string contract, int type, long id)
        {
            var result = new ResponseActionResult();
            var client = new RestClient(BaoMinhConsants.BaseUrl);
            var request = new RestRequest(string.Format(BaoMinhConsants.GetDoc, contract, type));
            request.Method = Method.GET;
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            try
            {
                var objLogInsurance = new Domain.Tables.TblLogInsurance
                {
                    CreateDate = DateTime.Now,
                    Request = string.Format(BaoMinhConsants.GetDoc, contract, type),
                    TypeInsurance = type,
                    InsuranceCompensator = (int)Insurance_PartnerCode.BaoMinh,
                    TypeRequest = (int)LogInsurance_TypeRequest.GetDoc
                };
                if (type == (int)Insurance_TypeInsurance.Lender)
                    objLogInsurance.LenderID = id;
                if (type == (int)Insurance_TypeInsurance.Customer)
                    objLogInsurance.CustomerID = id;

                var insertID = _logInsuranceTab.Insert(objLogInsurance);

                var response = await client.ExecuteAsync(request);
                var data = new OutputGetDoc();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var jsonResponse = response.Content;
                    data = Newtonsoft.Json.JsonConvert.DeserializeObject<OutputGetDoc>(jsonResponse);
                    objLogInsurance.ResponseCode = data.Meta.ErrCode.ToString();
                    objLogInsurance.Response = jsonResponse;

                    result.Data = data;
                    result.SetSucces();
                }

                objLogInsurance.LogInsuranceID = insertID;
                _logInsuranceTab.Update(objLogInsurance);

            }
            catch (Exception ex)
            {
                _logger.LogWarning($"InsuranceManager_GetDoc|contract={contract}|type={type}|id={id}|ex={ex.Message}|StackTrace={ex.StackTrace}");
            }
            return result;

        }


        public async Task<ResponseActionResult> GetLoanCertificateInformation(string Path, int DocType = 0)
        {
            var result = new ResponseActionResult();
            try
            {
                var url = InsuranceAIConsants.BaseUrl + InsuranceAIConsants.ExtractText_Url;
                var client = new RestClient(url);
                client.Timeout = 300000;
                var request = new RestRequest(Method.POST);
                var token = Authentication(InsuranceAIConsants.ExtracText_AppId, InsuranceAIConsants.ExtracText_Key);
                if (token == null && token.Token == null)
                {
                    _logger.LogError($"InsuranceManager_GetLoanCertificateInformation|Token=null");
                    return result;
                }
                var req = new
                {
                    url = Path,
                    doc_type = DocType
                };
                var body = JsonConvert.SerializeObject(req);
                request.Parameters.Clear();
                request.AddHeader("Authorization", token.Token);
                request.AddHeader("Content-Type", "application/json");
                request.RequestFormat = DataFormat.Json;
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = await client.ExecuteAsync(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var jsonResponse = response.Content;
                    var objData = JsonConvert.DeserializeObject<LoanCertificateInformation>(jsonResponse);
                    if (objData != null)
                    {
                        result.Data = objData;
                    }
                }
                result.SetSucces();
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError($"InsuranceManager_GetLoanCertificateInformation|Path={Path}|DocType={DocType}|ex={ex.Message}|StackTrace={ex.StackTrace}");
            }
            return result;
        }

        private TblTokenAI Authentication(string AppID, string AppKey)
        {
            var objToken = new TblTokenAI();
            try
            {
                var tokenAPI = _tokenAITab.WhereClause(x => x.AppID == AppID).Query().FirstOrDefault();
                if (tokenAPI == null || tokenAPI.DateExpiration <= DateTime.Now)
                {
                    var url = InsuranceAIConsants.BaseUrl + InsuranceAIConsants.Authentication;
                    var client = new RestClient(url);
                    client.Timeout = 3000;
                    var objInput = new
                    {
                        app_id = AppID,
                        app_key = AppKey
                    };
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("cache-control", "no-cache");
                    request.AddParameter("application/json", Newtonsoft.Json.JsonConvert.SerializeObject(objInput), ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var jsonResponse = response.Content;
                        AuthenticationResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<AuthenticationResult>(jsonResponse);
                        if (result != null)
                        {
                            objToken.Token = result.Token;
                            objToken.DateExpiration = result.ExpiredDate;
                            objToken.AppID = AppID;
                            objToken.CreateDate = DateTime.Now;
                            _ = _tokenAITab.Insert(objToken);
                        }
                    }
                    return objToken;
                }
                else
                {
                    return tokenAPI;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"InsuranceManager_Authentication|AppID={AppID}|AppKey={AppKey}|ex={ex.Message}|StackTrace={ex.StackTrace}");
            }
            return objToken;
        }

        public async Task<ResponseActionResult> BuyInsuranceMaterial(CreateInsuranceMaterialReq entity)
        {
            var result = new ResponseActionResult();
            var body = JsonConvert.SerializeObject(entity.CreateInsuranceMaterial);
            var client = new RestClient(BaoMinhConsants.BaseUrl2);
            var request = new RestRequest(BaoMinhConsants.CreateInsuranceMaterial);
            request.Method = Method.POST;
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.Parameters.Clear();
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            try
            {
                var objLogInsurance = new Domain.Tables.TblLogInsurance
                {
                    CreateDate = DateTime.Now,
                    Request = body,
                    TypeInsurance = (int)LMS.Common.Constants.Insurance_TypeInsurance.Material,
                    LoanID = entity.LoanID,
                    CustomerID = entity.CustomerID,
                    InsuranceCompensator = (int)Insurance_PartnerCode.BaoMinh,
                    TypeRequest = (int)LogInsurance_TypeRequest.Create
                };
                var insertID = _logInsuranceTab.Insert(objLogInsurance);
                if (_baoMinhConfig.IsProduct) // test
                {
                    result.SetSucces();
                    var dataa = new ResultBuyInsuranceMaterial();
                    dataa.Meta = new Meta();
                    dataa.Meta.ErrCode = 200;
                    dataa.Data = new Data();
                    dataa.Data.Code = _baoMinhConfig.Code;
                    dataa.Data.Path = _baoMinhConfig.Path;
                    result.Data = dataa;
                    return result;
                }
                else
                {
                    var response = await client.ExecuteAsync(request);
                    var data = new ResultBuyInsuranceMaterial();
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var jsonResponse = response.Content;
                        data = Newtonsoft.Json.JsonConvert.DeserializeObject<ResultBuyInsuranceMaterial>(jsonResponse);
                        objLogInsurance.ResponseCode = data.Meta.ErrCode.ToString();
                        result.Data = data;
                        result.SetSucces();
                    }
                    objLogInsurance.Response = response.Content;
                    objLogInsurance.LogInsuranceID = insertID;
                    _logInsuranceTab.Update(objLogInsurance);
                }
               
            }
            catch (Exception ex)
            {
                _logger.LogWarning($"InsuranceManager_BuyInsuranceMaterial|request={_common.ConvertObjectToJSonV2(entity)}|ex={ex.Message}|StackTrace={ex.StackTrace}");
            }
            return result;
        }
    }
}
