﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.ProxyTima;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OutGatewayApi.Services
{
    public interface IProxyTimaManager
    {
        Task<ResponseActionResult> K03ProxyTima(K03ProxyTimaRequest entity);
        Task<ResponseActionResult> K06ProxyTima(K06ProxyTimaRequest entity);
        Task<ResponseActionResult> K21ProxyTima(K21ProxyTimaRequest entity);
        Task<ResponseActionResult> K22ProxyTima(K22ProxyTimaRequest entity);
        Task<ResponseActionResult> SendProxyTima(string url, string body, Method method, string agencyAccount = "");
    }
    /// <summary>
    /// Các request call qua proxy statuscode = OK, set response success
    /// lỗi mạng retry
    /// </summary>
    public class ProxyTimaManager : IProxyTimaManager
    {
        readonly ILogger<ProxyTimaManager> _logger;
        readonly LMS.Common.Helper.Utils _common;
        readonly Domain.Models.ProxyTimaConfiguration _proxtTimaConfiguration;
        public ProxyTimaManager(ILogger<ProxyTimaManager> logger, Domain.Models.ProxyTimaConfiguration proxtTimaConfiguration)
        {
            _logger = logger;
            _proxtTimaConfiguration = proxtTimaConfiguration;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> K03ProxyTima(K03ProxyTimaRequest entity)
        {
            var result = new ResponseActionResult();
            try
            {
                if (!_proxtTimaConfiguration.IsProduct)
                {
                    entity.PhoneNumber = _proxtTimaConfiguration.PhoneTest;
                }
                var url = $"{ProxyTimaConfiguration.BaseUrl + ProxyTimaConfiguration.LocationDiscovery}?PhoneNumber={entity.PhoneNumber}";
                #region todo remove
                //var client = new RestClient(url);
                //var request = new RestRequest(Method.GET);
                //request.Parameters.Clear();
                //request.AddHeader("Authorization", _proxtTimaConfiguration.Authorization);
                //request.AddHeader("account", _proxtTimaConfiguration.Account);
                //request.AddHeader("password", _proxtTimaConfiguration.Password);
                //request.AddHeader("agencyAccount", entity.AgencyAccount);
                //request.AddHeader("cache-control", "no-cache");
                //IRestResponse response = await client.ExecuteAsync(request);
                //if (response.StatusCode == System.Net.HttpStatusCode.OK)
                //{
                //    result.SetSucces();
                //    _logger.LogInformation($"K03ProxyTima|url={url}|responseConten={response.Content}");
                //    var jsonResponse = response.Content;
                //    var responseData = _common.ConvertJSonToObjectV2<ResponseActionResult>(jsonResponse);
                //    if (responseData.Result == (int)AGResponse_Status.Success)
                //    {
                //        var resultData = _common.ConvertJSonToObjectV2<K03ProxyTima>(responseData.Data.ToString());
                //        result.Data = resultData;
                //    }
                //    else
                //    {
                //        result.Message = responseData.Message;
                //        _logger.LogError($"K03ProxyTima_Error|url={url}|responseConten={response.Content}");
                //    }
                //}
                #endregion
                var responseData = await SendProxyTima(url, null, method: Method.GET, agencyAccount: entity.AgencyAccount);
                if (responseData.Result == (int)AGResponse_Status.Success)
                {
                    result.SetSucces();
                    var resultData = _common.ConvertJSonToObjectV2<K03ProxyTima>(responseData.Data.ToString());
                    result.Data = resultData;
                }
                else
                {
                    result.Message = responseData.Message;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"K03ProxyTima|request={_common.ConvertObjectToJSonV2(entity)}|ex={ex.Message}|StackTrace={ex.StackTrace}");
            }
            return result;
        }

        public async Task<ResponseActionResult> K06ProxyTima(K06ProxyTimaRequest entity)
        {
            var result = new ResponseActionResult();
            try
            {
                if (!_proxtTimaConfiguration.IsProduct)
                {
                    entity.PhoneNumber = _proxtTimaConfiguration.PhoneTest;
                }
                var url = $"{ProxyTimaConfiguration.BaseUrl + ProxyTimaConfiguration.MostContactPhone}?PhoneNumber={entity.PhoneNumber}";
                #region todo remove
                //var client = new RestClient(url);
                //var request = new RestRequest(Method.GET);
                //request.Parameters.Clear();
                //request.AddHeader("Authorization", _proxtTimaConfiguration.Authorization);
                //request.AddHeader("account", _proxtTimaConfiguration.Account);
                //request.AddHeader("password", _proxtTimaConfiguration.Password);
                //request.AddHeader("agencyAccount", entity.AgencyAccount);
                //request.AddHeader("cache-control", "no-cache");
                //IRestResponse response = await client.ExecuteAsync(request);
                //if (response.StatusCode == System.Net.HttpStatusCode.OK)
                //{
                //    result.SetSucces();
                //    _logger.LogInformation($"K06ProxyTima|url={url}|responseConten={response.Content}");
                //    var jsonResponse = response.Content;
                //    var responseData = _common.ConvertJSonToObjectV2<ResponseActionResult>(jsonResponse);
                //    if (responseData.Result == (int)AGResponse_Status.Success)
                //    {
                //        var resultData = _common.ConvertJSonToObjectV2<K06ProxyTima>(responseData.Data.ToString());
                //        result.Data = resultData;
                //    }
                //    else
                //    {
                //        result.Message = responseData.Message;
                //        _logger.LogError($"K06ProxyTima_Error|url={url}|responseConten={response.Content}");
                //    }
                //}
                #endregion
                var responseData = await SendProxyTima(url, null, method: Method.GET, agencyAccount: entity.AgencyAccount);
                if (responseData.Result == (int)AGResponse_Status.Success)
                {
                    result.SetSucces();
                    var resultData = _common.ConvertJSonToObjectV2<K06ProxyTima>(responseData.Data.ToString());
                    result.Data = resultData;
                }
                else
                {
                    result.Message = responseData.Message;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"K06ProxyTima|request={_common.ConvertObjectToJSonV2(entity)}|ex={ex.Message}|StackTrace={ex.StackTrace}");
            }
            return result;
        }

        public async Task<ResponseActionResult> K21ProxyTima(K21ProxyTimaRequest entity)
        {
            var result = new ResponseActionResult();
            try
            {
                if (!_proxtTimaConfiguration.IsProduct)
                {
                    entity.NumberCard = _proxtTimaConfiguration.NumberCardTest;
                }
                var url = $"{ProxyTimaConfiguration.BaseUrl + ProxyTimaConfiguration.KYCBriefInforById}?NumberCard={entity.NumberCard}";
                #region todo remove
                //var client = new RestClient(url);
                //var request = new RestRequest(Method.GET);
                //request.Parameters.Clear();
                //request.AddHeader("Authorization", _proxtTimaConfiguration.Authorization);
                //request.AddHeader("account", _proxtTimaConfiguration.Account);
                //request.AddHeader("password", _proxtTimaConfiguration.Password);
                //request.AddHeader("agencyAccount", entity.AgencyAccount);
                //request.AddHeader("cache-control", "no-cache");
                //IRestResponse response = await client.ExecuteAsync(request);
                //if (response.StatusCode == System.Net.HttpStatusCode.OK)
                //{
                //    result.SetSucces();
                //    _logger.LogInformation($"K21ProxyTima|url={url}|responseConten={response.Content}");
                //    var jsonResponse = response.Content;
                //    var responseData = _common.ConvertJSonToObjectV2<ResponseActionResult>(jsonResponse);
                //    if (responseData.Result == (int)AGResponse_Status.Success)
                //    {
                //        var resultData = _common.ConvertJSonToObjectV2<K21ProxyTima>(responseData.Data.ToString());
                //        result.Data = resultData;
                //    }
                //    else
                //    {
                //        result.Message = responseData.Message;
                //        _logger.LogError($"K21ProxyTima_Error|url={url}|responseConten={response.Content}");
                //    }
                //}
                #endregion 
                var responseData = await SendProxyTima(url, null, method: Method.GET, agencyAccount: entity.AgencyAccount);
                if (responseData.Result == (int)AGResponse_Status.Success)
                {
                    result.SetSucces();
                    var resultData = _common.ConvertJSonToObjectV2<K21ProxyTima>(responseData.Data.ToString());
                    result.Data = resultData;
                }
                else
                {
                    result.Message = responseData.Message;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"K21ProxyTima|request={_common.ConvertObjectToJSonV2(entity)}|ex={ex.Message}|StackTrace={ex.StackTrace}");
            }
            return result;
        }

        public async Task<ResponseActionResult> K22ProxyTima(K22ProxyTimaRequest entity)
        {
            var result = new ResponseActionResult();
            try
            {
                if (!_proxtTimaConfiguration.IsProduct)
                {
                    entity.PhoneNumber = _proxtTimaConfiguration.PhoneTest;
                }
                var url = $"{ProxyTimaConfiguration.BaseUrl + ProxyTimaConfiguration.KYCBriefInforByPhone}?PhoneNumber={entity.PhoneNumber}";
                #region todo remove
                //var client = new RestClient(url);
                //var request = new RestRequest(Method.GET);
                //request.Parameters.Clear();
                //request.AddHeader("Authorization", _proxtTimaConfiguration.Authorization);
                //request.AddHeader("account", _proxtTimaConfiguration.Account);
                //request.AddHeader("password", _proxtTimaConfiguration.Password);
                //request.AddHeader("agencyAccount", entity.AgencyAccount);
                //request.AddHeader("cache-control", "no-cache");
                //IRestResponse response = await client.ExecuteAsync(request);
                //if (response.StatusCode == System.Net.HttpStatusCode.OK)
                //{
                //    result.SetSucces();
                //    _logger.LogInformation($"K22ProxyTima|url={url}|responseConten={response.Content}");
                //    var jsonResponse = response.Content;
                //    var responseData = _common.ConvertJSonToObjectV2<ResponseActionResult>(jsonResponse);
                //    if (responseData.Result == (int)AGResponse_Status.Success)
                //    {
                //        var resultData = _common.ConvertJSonToObjectV2<K22ProxyTima>(responseData.Data.ToString());
                //        result.Data = resultData;
                //    }
                //    else
                //    {
                //        result.Message = responseData.Message;
                //        _logger.LogError($"K22ProxyTima_Error|url={url}|responseConten={response.Content}");
                //    }
                //}
                #endregion
                var responseData = await SendProxyTima(url, null, method: Method.GET, agencyAccount: entity.AgencyAccount);
                if (responseData.Result == (int)AGResponse_Status.Success)
                {
                    result.SetSucces();
                    var resultData = _common.ConvertJSonToObjectV2<K22ProxyTima>(responseData.Data.ToString());
                    result.Data = resultData;
                }
                else
                {
                    result.Message = responseData.Message;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"K22ProxyTima|request={_common.ConvertObjectToJSonV2(entity)}|ex={ex.Message}|StackTrace={ex.StackTrace}");
            }
            return result;
        }

        public async Task<ResponseActionResult> SendProxyTima(string url, string body, Method method, string agencyAccount = "")
        {
            var result = new ResponseActionResult();
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest(method);
                request.Parameters.Clear();
                request.AddHeader("Accept", "application/json");
                request.AddHeader("Authorization", _proxtTimaConfiguration.Authorization);
                request.AddHeader("account", _proxtTimaConfiguration.Account);
                request.AddHeader("password", _proxtTimaConfiguration.Password);
                if (!string.IsNullOrEmpty(agencyAccount))
                {
                    request.AddHeader("agencyAccount", agencyAccount);
                }
                if (!string.IsNullOrEmpty(body) && method == Method.POST)
                {
                    request.AddParameter("application/json", body, ParameterType.RequestBody);
                }
                request.AddHeader("cache-control", "no-cache");
                IRestResponse response = await client.ExecuteAsync(request);
                _logger.LogInformation($"SendProxyTima|url={url}|responseContent={response.Content}");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result.SetSucces();
                    var responseData = _common.ConvertJSonToObjectV2<ResponseActionResult>(response.Content);
                    result.Data = responseData.Data;
                    result.Message = responseData.Message;
                }
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError($"SendProxyTima_Error|url={url}|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }
    }
}
