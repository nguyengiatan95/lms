﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.APIAutoDisbursement;
using Microsoft.Extensions.Logging;
using OutGatewayApi.Domain.Tables;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OutGatewayApi.Services
{
    public interface IVibBankPaymentManager
    {
        CreateTransactionResponse CreateTransaction(CreateTransactionRequest dataPost);
        Task<ExecuteOtpResponse> ExecuteOtp(ExecuteOtpRequest dataPost);
        CancelTransactionResponse CancelTransaction(CancelTransactionRequest dataPost);
    }
    public class VibBankPaymentManager : IVibBankPaymentManager
    {
        public const string AI_Action_CreateTransaction_LMS = "/lms/create-transaction";
        public const string AI_Action_OptExecute_LMS = "/lms/opt-execute";
        public const string AI_Action_CreateTransaction_AG = "/create-transaction";
        public const string AI_Action_OptExecute_AG = "/opt-execute";
        public const string AI_Action_Cancel_LMS = "/lms/cancel";
        public const string AI_Action_Cancel_AG = "/cancel-transaction";
        public const string AI_Action_Logout_LMS = "/lms/logout";
        public const string AI_Action_Logout_AG = "/logout";
        public const string Domain_LMS = "http://159.65.133.130:9808/lms";
        public const string Domain_AG = "http://159.65.133.130:9908/tima-floor";
        public const int MaxRetry = 3;
        const int TimeSleep = 5;
        const string MessagerNotConnect = "ngân hàng thụ hưởng chưa tham gia dịch vụ chuyển tiền"; //nhanh qua số tìa khoản. bạn vui lòng chọn hình thức chuyển tiền khác hoặc liên hệ dvkh 24/7 (84) 2462585858 hoặc 1800 8180 để được hỗ trợ
        const string MessagerTimeOut = "kết nối gián đoạn hoặc hết thời gian";// . bạn vui long liên hệ dvkh  24/7 (84) 2462585858 hoặc 1800 8180 để được hỗ trợ
        const string MessageAccountNotValid = "số tài khoản hưởng không"; // . bạn vui lòng kiểm tra lại  hoặc liên hệ dvkh  24/7 (84) 2462585858 hoặc 1800 8180 để được hỗ trợ
        const string MessageLogin = "đăng nhập không thành công vào tài khoản"; // này. Do tài khoản vừa đăng nhập trên một thiết bị, môi trường khác. Vui lòng đợi 10-15 phút tiếp theo để có thể tiếp tục sử dụng dịch vụ
        const string MessageBankDontSupport = "support this bank"; // bad reqeust, don't support this bank
        LMS.Common.Helper.Utils _common;
        ILogger<VibBankPaymentManager> _logger;
        Domain.Models.VIBConfiguration _vibConfig;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCallApi> _logCallApiTab;
        public VibBankPaymentManager(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogCallApi> logCallApiTab,
            Domain.Models.VIBConfiguration vibConfig,
            ILogger<VibBankPaymentManager> logger)
        {
            _logCallApiTab = logCallApiTab;
            _vibConfig = vibConfig;
            _common = new LMS.Common.Helper.Utils();
            _logger = logger;
        }
        public string BaseUrl
        {
            get
            {
                return Domain_LMS;
            }
        }

        public CreateTransactionResponse CreateTransaction(CreateTransactionRequest dataPost)
        {
            string traceIndentifierResponse = _common.GenTraceIndentifier();
            CreateTransactionResponse objResultTransaction = new CreateTransactionResponse
            {
                TraceIndentifier = traceIndentifierResponse
            };

            try
            {
                var currentDate = DateTime.Now;
                var logCallApiDetail = new TblLogCallApi
                {
                    ActionCallApi = (int)LogCallApi_ActionCallApi.ExecuteCreateTransaction,
                    Status = (int)LogCallApi_StatusCallApi.CallApi,
                    NameActionCallApi = (LogCallApi_ActionCallApi.ExecuteCreateTransaction).GetDescription(),
                    InputStringCallApi = _common.ConvertObjectToJSonV2(dataPost),
                    CreateDate = currentDate,
                    ModifyDate = currentDate,
                    LoanCreditID = dataPost.LoanCreditID,
                    TraceIndentifierRequest = traceIndentifierResponse,
                    LinkCallApi = BaseUrl + AI_Action_CreateTransaction_AG,
                    CountRetry = 0
                };
                logCallApiDetail.LogCallApiID = (int)_logCallApiTab.Insert(logCallApiDetail);
                if (logCallApiDetail.LogCallApiID > 0)
                {
                    _ = ExecuteCreateTransaction(dataPost, logCallApiDetail.LogCallApiID);
                }
                else
                {
                    _logger.LogError($"CreateTransaction_InsertLog|dataPost={_common.ConvertObjectToJSonV2(dataPost)}");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"VibBankPaymentManager_CreateTransaction|dataPost={_common.ConvertObjectToJSonV2(dataPost)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return objResultTransaction;

        }

        private async Task ExecuteCreateTransaction(CreateTransactionRequest dataPost, long logCallApiID)
        {
            string parameters = "";
            try
            {
                var client = new RestClient(BaseUrl);
                var createTransactionUrl = AI_Action_CreateTransaction_AG;// "/create-transaction";
                var request = new RestRequest(createTransactionUrl)
                {
                    Method = Method.GET
                };
                var logCallApiDetail = _logCallApiTab.WhereClause(x => x.LogCallApiID == logCallApiID).Query().FirstOrDefault();
                // tài khoản test
                if (!string.IsNullOrEmpty(_vibConfig.BankAccountNumber))
                {
                    dataPost.BankAccountNumber = _vibConfig.BankAccountNumber;
                    dataPost.Name = _vibConfig.FullName;
                    dataPost.Amount = _vibConfig.LoanAmountFinal;
                    dataPost.BankValue = $"{_vibConfig.BankID}";
                }
                request.Parameters.Clear();
                request.AddParameter("n", dataPost.BankAccountNumber);
                request.AddParameter("bankId", dataPost.BankValue);
                request.AddParameter("name", dataPost.Name);
                request.AddParameter("amount", dataPost.Amount);
                request.AddParameter("mess", dataPost.Mess);
                parameters = _common.ConvertObjectToJSonV2(request.Parameters);
                _logger.LogInformation($"ExecuteCreateTransaction_Start|loanCredit={logCallApiDetail.LoanCreditID}|TraceIndentifierRequest={logCallApiDetail.TraceIndentifierRequest}|Retry={logCallApiDetail.CountRetry}|parameters={parameters}");
                var response = await client.ExecuteAsync(request);
                _logger.LogInformation($"ExecuteCreateTransaction_Finished|Retry={logCallApiDetail.CountRetry}|parameters={parameters}|StatusCode={response.StatusCode}|responseContent={response.Content}");
                logCallApiDetail.ModifyDate = DateTime.Now;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    logCallApiDetail.ResultStringCallApi = response.Content;
                    CreateTransactionResponse objResultTransaction = _common.ConvertJSonToObjectV2<CreateTransactionResponse>(response.Content);
                    var messageResponse = (objResultTransaction.Messages ?? "").ToLower();
                    switch ((VIB_AI_ResponseCodeConstant)objResultTransaction.Code)
                    {
                        case VIB_AI_ResponseCodeConstant.SUCCESS_CODE:
                            logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Success;
                            // hiện tại api ngoài có hỗ trợ response trả về
                            //logCallApiDetail.TraceIndentifierResponse = _common.GenTraceIndentifier();
                            break;
                        case VIB_AI_ResponseCodeConstant.FAIL_EXECUTE:
                        case VIB_AI_ResponseCodeConstant.FAIL_BANK_CANCEL:
                            _logger.LogError($"VibBankPaymentManager_CreateRequest_Response|retry={logCallApiDetail.CountRetry}|logCallApiID={logCallApiID}|Api={logCallApiDetail.LinkCallApi}|parameters={parameters}|StatusCode={response.StatusCode}|responseContent={response.Content}");
                            if (messageResponse.Contains(MessagerNotConnect)
                               || messageResponse.Contains(MessageAccountNotValid)
                               || messageResponse.Contains(MessageBankDontSupport))
                            {
                                //input.MessageResponse = messageResponse;
                                // sau lần chờ 10phut đầu tiên vẫn lỗi này
                                // chuyển luôn cho kế toán check
                                logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Error;
                            }
                            else
                            {
                                var timeSleep = TimeSleep;
                                if (messageResponse.Contains(MessagerTimeOut)
                                    || messageResponse.Contains(MessageLogin))
                                {
                                    timeSleep += TimeSleep;
                                }
                                if (logCallApiDetail.CountRetry < MaxRetry)
                                {
                                    logCallApiDetail.CountRetry++;
                                    await Task.Delay(TimeSpan.FromMinutes(timeSleep));
                                }
                            }
                            break;
                        default:
                            logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Error;
                            break;
                    }
                }
                else
                {
                    logCallApiDetail.CountRetry++;
                    _logger.LogError($"VibBankPaymentManager_CreateRequest_Response|retry={logCallApiDetail.CountRetry}|logCallApiID={logCallApiID}|StatusCode={response.StatusCode}|responseContent={response.Content}|Api={logCallApiDetail.LinkCallApi}|parameters={parameters}");
                }
                var lstStatusRetry = new List<int> { (int)LogCallApi_StatusCallApi.CallApi, (int)LogCallApi_StatusCallApi.Retry };
                if (logCallApiDetail.CountRetry < MaxRetry && lstStatusRetry.Contains(logCallApiDetail.Status))
                {
                    logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Retry;
                    _logCallApiTab.Update(logCallApiDetail);
                    await Task.Delay(TimeSpan.FromSeconds(1));
                    _ = ExecuteCreateTransaction(dataPost, logCallApiID);
                }
                else
                {
                    if (logCallApiDetail.Status != (int)LogCallApi_StatusCallApi.Success)
                        logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Error;
                    _logCallApiTab.Update(logCallApiDetail);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"VibBankPaymentManager_ExecuteCreateTransaction|Api={BaseUrl + AI_Action_CreateTransaction_AG}|parameters={parameters}|ex={ex.Message}-{ex.StackTrace}");
            }
        }


        public async Task<ExecuteOtpResponse> ExecuteOtp(ExecuteOtpRequest dataPost)
        {
            string traceIndentifierResponse = _common.GenTraceIndentifier();

            ExecuteOtpResponse objResultExecuteOtp = new ExecuteOtpResponse
            {
                TraceIndentifier = traceIndentifierResponse
            };
            try
            {
                var currentDate = DateTime.Now;
                var logCallApiDetail = new TblLogCallApi
                {
                    ActionCallApi = (int)LogCallApi_ActionCallApi.ConfirmeExecuteOtp,
                    Status = (int)LogCallApi_StatusCallApi.CallApi,
                    NameActionCallApi = (LogCallApi_ActionCallApi.ConfirmeExecuteOtp).GetDescription(),
                    InputStringCallApi = _common.ConvertObjectToJSonV2(dataPost),
                    CreateDate = currentDate,
                    ModifyDate = currentDate,
                    LoanCreditID = dataPost.LoanCreditID,
                    TraceIndentifierRequest = traceIndentifierResponse,
                    LinkCallApi = BaseUrl + AI_Action_CreateTransaction_AG,
                    CountRetry = 0
                };
                logCallApiDetail.LogCallApiID = (int)_logCallApiTab.Insert(logCallApiDetail);
                if (logCallApiDetail.LogCallApiID > 0)
                {
                    if (dataPost.ExecuteBy == 1)
                    {
                        objResultExecuteOtp = await ExecuteConfirmOtp(dataPost, logCallApiDetail.LogCallApiID);
                        // gán lại
                        objResultExecuteOtp.TraceIndentifier = traceIndentifierResponse;
                    }
                    else
                    {
                        _ = ExecuteConfirmOtp(dataPost, logCallApiDetail.LogCallApiID);
                    }
                }
                else
                {
                    _logger.LogError($"ExecuteOtp_InsertLog|dataPost={_common.ConvertObjectToJSonV2(dataPost)}");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"VibBankPaymentManager_ExecuteOtp|dataPost={_common.ConvertObjectToJSonV2(dataPost)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return objResultExecuteOtp;
        }
        private async Task<ExecuteOtpResponse> ExecuteConfirmOtp(ExecuteOtpRequest dataPost, long logCallApiID)
        {
            string parameters = "";
            try
            {
                var client = new RestClient(BaseUrl);
                var verifyUrl = AI_Action_OptExecute_AG;// "/opt-execute";
                var request = new RestRequest(verifyUrl);
                request.Parameters.Clear();
                request.Method = Method.GET;
                request.AddHeader("Accept", "application/json");
                request.AddParameter("transid", dataPost.TransIDRequest);
                request.AddParameter("otp", dataPost.Otp);
                parameters = _common.ConvertObjectToJSonV2(request.Parameters);

                var logCallApiDetail = _logCallApiTab.WhereClause(x => x.LogCallApiID == logCallApiID).Query().FirstOrDefault();
                _logger.LogInformation($"ExecuteConfirmOtp_Start|loanCredit={logCallApiDetail.LoanCreditID}|TraceIndentifierRequest={logCallApiDetail.TraceIndentifierRequest}|Retry={logCallApiDetail.CountRetry}|parameters={parameters}");
                var response = await client.ExecuteAsync(request);

                _logger.LogInformation($"ExecuteConfirmOtp_Finished|Api={BaseUrl + AI_Action_OptExecute_AG}|Retry={logCallApiDetail.CountRetry}|parameters={parameters}|responseContent={response.Content}");
                logCallApiDetail.ModifyDate = DateTime.Now;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    logCallApiDetail.ResultStringCallApi = response.Content;
                    var objResultExecuteOtp = _common.ConvertJSonToObjectV2<ExecuteOtpResponse>(response.Content);
                    if (objResultExecuteOtp.Code == (int)VIB_AI_ResponseCodeConstant.SUCCESS_CODE)
                    {
                        logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Success;
                    }
                    else
                    {
                        logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Error;
                        _logger.LogError($"VibBankPaymentManager_ExecuteConfirmOtp_Response|logCallApiID={logCallApiID}|Api={logCallApiDetail.LinkCallApi}|parameters={parameters}|responseContent={response.Content}");
                    }
                    return objResultExecuteOtp;
                }
                else
                {
                    logCallApiDetail.CountRetry++;
                    _logger.LogError($"VibBankPaymentManager_ExecuteConfirmOtp_Response|retry={logCallApiDetail.CountRetry}|logCallApiID={logCallApiID}|StatusCode={response.StatusCode}|responseContent={response.Content}|Api={logCallApiDetail.LinkCallApi}|parameters={parameters}");
                }
                var lstStatusRetry = new List<int> { (int)LogCallApi_StatusCallApi.CallApi, (int)LogCallApi_StatusCallApi.Retry };
                if (logCallApiDetail.CountRetry < MaxRetry && lstStatusRetry.Contains(logCallApiDetail.Status) && dataPost.ExecuteBy == 0)
                {
                    logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Retry;
                    _logCallApiTab.Update(logCallApiDetail);
                    await Task.Delay(TimeSpan.FromSeconds(5));
                    return await ExecuteConfirmOtp(dataPost, logCallApiID);
                }
                else
                {
                    logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Error;
                    _logCallApiTab.Update(logCallApiDetail);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"VibBankPaymentManager_ExecuteConfirmOtp|Api={BaseUrl + AI_Action_OptExecute_AG}|parameters={parameters}|ex={ex.Message}-{ex.StackTrace}");
            }
            return null;
        }
        public CancelTransactionResponse CancelTransaction(CancelTransactionRequest dataPost)
        {
            string traceIndentifierResponse = _common.GenTraceIndentifier();
            CancelTransactionResponse objResultCancleTransaction = new CancelTransactionResponse
            {
                TraceIndentifier = traceIndentifierResponse
            };
            try
            {
                var currentDate = DateTime.Now;
                var logCallApiDetail = new TblLogCallApi
                {
                    ActionCallApi = (int)LogCallApi_ActionCallApi.ExecuteCancelTransaction,
                    Status = (int)LogCallApi_StatusCallApi.CallApi,
                    NameActionCallApi = (LogCallApi_ActionCallApi.ExecuteCancelTransaction).GetDescription(),
                    InputStringCallApi = _common.ConvertObjectToJSonV2(dataPost),
                    CreateDate = currentDate,
                    ModifyDate = currentDate,
                    LoanCreditID = dataPost.LoanCreditID,
                    TraceIndentifierRequest = traceIndentifierResponse,
                    LinkCallApi = BaseUrl + AI_Action_Cancel_AG,
                    CountRetry = 0
                };
                logCallApiDetail.LogCallApiID = (int)_logCallApiTab.Insert(logCallApiDetail);
                if (logCallApiDetail.LogCallApiID > 0)
                {
                    _ = ExecuteCancelTransaction(dataPost.TransID, logCallApiDetail.LogCallApiID);
                }
                else
                {
                    _logger.LogError($"CancelTransaction_InsertLog|dataPost={_common.ConvertObjectToJSonV2(dataPost)}");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"VibBankPaymentManager_CancelTransaction|Api={BaseUrl + AI_Action_Cancel_AG}|dataPost={_common.ConvertObjectToJSonV2(dataPost)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return objResultCancleTransaction;
        }
        private async Task ExecuteCancelTransaction(string transID, long logCallApiID)
        {
            string parameters = "";
            try
            {
                var logCallApiDetail = _logCallApiTab.WhereClause(x => x.LogCallApiID == logCallApiID).Query().FirstOrDefault();
                var client = new RestClient(BaseUrl);
                var cancelTransactionUrl = AI_Action_Cancel_AG;// "/cancel-transaction";
                var request = new RestRequest(cancelTransactionUrl);
                request.Parameters.Clear();
                request.Method = Method.GET;
                request.AddHeader("Accept", "application/json");
                request.AddParameter("transid", transID);
                parameters = _common.ConvertObjectToJSonV2(request.Parameters);

                var response = await client.ExecuteAsync(request);
                _logger.LogInformation($"CancelTransaction_Finished|Api={BaseUrl + AI_Action_Cancel_AG}|parameters={parameters}|responseContent={response.Content}");
                logCallApiDetail.ModifyDate = DateTime.Now;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    logCallApiDetail.ResultStringCallApi = response.Content;
                    var objResultCancleTransaction = _common.ConvertJSonToObjectV2<CancelTransactionResponse>(response.Content);
                    if (objResultCancleTransaction.Code == (int)VIB_AI_ResponseCodeConstant.SUCCESS_CODE)
                    {
                        logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Success;
                    }
                    else
                    {
                        logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Error;
                        _logger.LogError($"VibBankPaymentManager_CancelTransaction_Response|retry={logCallApiDetail.CountRetry}|logCallApiID={logCallApiID}|Api={logCallApiDetail.LinkCallApi}|parameters={parameters}|responseContent={response.Content}");
                    }
                }
                else
                {
                    logCallApiDetail.CountRetry++;
                    _logger.LogError($"VibBankPaymentManager_CancelTransaction_Response|retry={logCallApiDetail.CountRetry}|logCallApiID={logCallApiID}|StatusCode={response.StatusCode}|responseContent={response.Content}|Api={logCallApiDetail.LinkCallApi}|parameters={parameters}");

                }
                var lstStatusRetry = new List<int> { (int)LogCallApi_StatusCallApi.CallApi, (int)LogCallApi_StatusCallApi.Retry };
                if (logCallApiDetail.CountRetry < MaxRetry && lstStatusRetry.Contains(logCallApiDetail.Status))
                {
                    logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Retry;
                    _logCallApiTab.Update(logCallApiDetail);
                    await Task.Delay(TimeSpan.FromSeconds(5));
                    _ = ExecuteCancelTransaction(transID, logCallApiID);
                }
                else
                {
                    logCallApiDetail.Status = (int)LogCallApi_StatusCallApi.Error;
                    _logCallApiTab.Update(logCallApiDetail);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"VibBankPaymentManager_ExecuteCancelTransaction|Api={BaseUrl + AI_Action_Cancel_AG}|transid={transID}|ex={ex.Message}-{ex.StackTrace}");
            }
        }
    }
}
