﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.LOSServices;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using OutGatewayApi.Domain;
using OutGatewayApi.Domain.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OutGatewayApi.Services
{
    public interface ILOSManager
    {
        Task<ResultLoanWaitingDisbursementDetailLOS> GetLoanCreditDetail(long loanCreditId);
        Task<List<ResultLoanDisbursmentWaiting>> DisbursementWaiting(ResultLoanDisbursmentWaitingReq req);
        Task<List<HistoryCommentCredit>> GetHistoryComment(ReqGetHistoryComment objRequest);
        Task<ResponseActionResult> SaveCommentHistory(ReqSaveCommentHistory objRequest);
        Task<ResponseActionResult> PushLoanLender(PushLoanLenderReq req);
        Task<ResponseActionResult> LockLoan(LockLoanReq req);
        Task<ResponseActionResult> DisbursementLoan(DisbursementLoanReq req);
        Task<ResponseActionResult> ReturnLoan(ReturnLoanReq req);
        Task<List<ResultListImage>> GetListImages(GetListImagesReq req);
        Task<ResponseActionResult> ChangeDisbursementByAccountant(ChangeDisbursementByAccountantReq req);
        Task<List<ResultSearchLoan>> SearchLoanCredit(ReqSearchLoanCredit objRequest);
        Task<List<WaitingMoneyDisbursementLenderLOS>> GetWaitingMoneyDisbursementLenderLOS();
        Task<List<ReportTimeDisbursementOfAccountant>> LosReportTime(DateTime fromDate, DateTime ToDate, int type, int pageCurrent, int pageSize);

        Task<List<ResultLoanWaitingBlockDetailLOS>> GetLstLoanDisbursementWaitingBlockLOS(long lenderID = 0);

        Task<ResponseActionResult> ChangePhoneLOS(ChangePhoneLOSReq req);
        Task<ResponseActionResult> UpdateLinkFacebook(UpdateLinkFacebookReq req);
        Task<ResponseActionResult> GetRelativeFamily();
        Task<ResponseActionResult> CreateRelationship(CreateRelationshipReq req);
        Task<ResponseActionResult> GetVehicleInforOfLoan(List<int> req);
        Task<ResponseActionResult> CreateLenderDigitalSignature(Domain.Models.LenderDigitalSignature req);

    }
    public class LOSManager : ILOSManager
    {
        LMS.Common.Helper.SlackClient _slackClient;
        LMS.Common.Helper.Utils _common;
        ILogger<LOSManager> _logger;
        IHttpContextAccessor _httpContextAccessor;
        public LOSManager(ILogger<LOSManager> logger, IHttpContextAccessor httpContextAccessor)
        {
            _slackClient = new LMS.Common.Helper.SlackClient();
            _common = new LMS.Common.Helper.Utils();
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
        }

        private string GetHeaderUserName()
        {
            var lstHeader = _httpContextAccessor.HttpContext?.Request?.Headers;
            if (lstHeader != null && lstHeader.Any())
            {
                var headerCheck = TimaSettingConstant.HeaderLMSUserName.ToLowerInvariant();
                foreach (var item in lstHeader)
                {
                    if (item.Key.ToLowerInvariant() == headerCheck)
                    {
                        return item.Value;
                    }
                }
            }
            // tạm thời return default
            return TimaSettingConstant.UserNameAdmin;
        }

        public async Task<ResultLoanWaitingDisbursementDetailLOS> GetLoanCreditDetail(long loanCreditId)
        {
            var resultData = new ResultLoanWaitingDisbursementDetailLOS();
            var url = $"{LMS.Entites.Dtos.LOSServices.LOSConfiguration.BaseUrl}{LMS.Entites.Dtos.LOSServices.LOSConfiguration.LOS_Action_GetLoanDetail}?LoanId={loanCreditId}";
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.Parameters.Clear();
                request.AddHeader("Accept", "application/json");
                request.AddParameter("Authorization", LMS.Entites.Dtos.LOSServices.LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"GetLoanCreditDetail|Api={url}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                    {
                        if (result.Data != null)
                        {
                            return _common.ConvertJSonToObjectV2<ResultLoanWaitingDisbursementDetailLOS>(result.Data.ToString());
                        }
                    }
                }
                else
                {
                    _logger.LogError($"GetLoanCreditDetail_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"GetLoanCreditDetail_Exception|Api={url}|ex={e.Message}-{e.StackTrace}");
            }
            return resultData;
        }

        public int ConvertStatusSearchLoanDisbursementFromAGToLOS(int statusAG)
        {
            switch (statusAG)
            {
                // AG: chờ đẩy cho lender, 
                case (int)StateLoanCreditNew.WaittingPushToLender: return 2;
                // chờ kế toán giải ngân
                case (int)StateLoanCreditNew.AccountantDisbursement: return 3;
                // chờ lender giải ngân
                case (int)StateLoanCreditNew.WaittingAgency: return 4;

                case -100: return 1;
                // 1: los tất cả
                default: return 1;
            }
        }
        public int ConvertStatusLoanDisbursementFromLOSToAG(int statusLos)
        {
            switch (statusLos)
            {
                // LOS: Chờ đẩy cho lender, 
                case 90: return (int)StateLoanCreditNew.WaittingPushToLender;
                // Chờ kế toán giải ngân
                case 102: return (int)StateLoanCreditNew.AccountantDisbursement;
                // Chờ lender giải ngân
                case 103: return (int)StateLoanCreditNew.WaittingAgency;
                // giải ngân tự động
                case 104: return (int)StateLoanCreditNew.AutoDisbursement;
                // không rõ status
                default: return -1;
            }
        }
        public async Task<List<ResultLoanDisbursmentWaiting>> DisbursementWaiting(ResultLoanDisbursmentWaitingReq req)
        {
            List<ResultLoanDisbursmentWaiting> lstData = new List<ResultLoanDisbursmentWaiting>();

            var postData = new
            {
                Page = req.Page,
                PageSize = req.PageSize,
                Type = req.TypeID,
                Search = req.Search
            };
            string body = "";
            var url = $"{LOSConfiguration.BaseUrl}{LOSConfiguration.LOS_Action_GetLoanWaiting}";
            try
            {
                body = _common.ConvertObjectToJSonV2(postData);
                var client = new RestClient(LOSConfiguration.BaseUrl);
                var request = new RestRequest(LOSConfiguration.LOS_Action_GetLoanWaiting);

                request.Method = Method.POST;
                request.Parameters.Clear();
                request.AddHeader("Accept", "application/json");
                request.AddParameter("Authorization", LMS.Entites.Dtos.LOSServices.LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"DisbursementWaiting|Api={url}|body={body}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                    {
                        if (result.Data != null)
                        {
                            lstData = _common.ConvertJSonToObjectV2<List<ResultLoanDisbursmentWaiting>>(result.Data.ToString());
                            req.TotalRecord = result.Meta.TotalRecords;
                            lstData.Select(c => { c.TotalCount = result.Meta.TotalRecords; return c; }).ToList();
                        }
                    }
                }
                else
                {
                    _logger.LogError($"GetLoanCreditDetail_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"DisbursementWaiting_Exception|Api={url}|body={body}|ex={e.Message}-{e.StackTrace}");
            }
            return lstData;
        }
        public async Task<List<HistoryCommentCredit>> GetHistoryComment(ReqGetHistoryComment objRequest)
        {
            List<HistoryCommentCredit> lstData = new List<HistoryCommentCredit>();
            var url = LOSConfiguration.BaseUrl + LOSConfiguration.LOS_Action_GetListComment + "?page=" + objRequest.Page + "&pageSize=" + objRequest.PageSize + "&LoanId=" + objRequest.LoanCreditId;
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.AddHeader("Accept", "application/json");
                request.AddParameter("Authorization", LMS.Entites.Dtos.LOSServices.LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"GetHistoryComment|Api={url}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                    {
                        if (result.Data != null)
                        {
                            var lstDataCommentOfLost = _common.ConvertJSonToObjectV2<List<ResultLoanCreditCommentHistory>>(result.Data.ToString());
                            foreach (var item in lstDataCommentOfLost)
                            {
                                var objHistoryCommentCredit = new HistoryCommentCredit();
                                objHistoryCommentCredit.Id = (int)item.LoanBriefNoteId;
                                objHistoryCommentCredit.Comment = item.Note;
                                objHistoryCommentCredit.ShopName = item.ShopName;
                                objHistoryCommentCredit.FullName = item.FullName;
                                objHistoryCommentCredit.CreateDate = item.CreatedTime;
                                lstData.Add(objHistoryCommentCredit);
                            }
                        }
                    }
                }
                else
                {
                    _logger.LogWarning($"GetHistoryComment|Api={url}|response={_common.ConvertObjectToJSonV2(response.Content)}");
                }

                return lstData;
            }
            catch (Exception e)
            {
                _logger.LogError($"GetHistoryComment_Exception|Api={url}|ex={e.Message}-{e.StackTrace}");
            }
            return lstData;
        }
        public async Task<ResponseActionResult> SaveCommentHistory(ReqSaveCommentHistory objRequest)
        {
            var resultData = new ResponseActionResult();
            string body = "";
            var url = $"{LOSConfiguration.BaseUrl}{LOSConfiguration.LOS_Action_AddComment}";
            try
            {
                body = _common.ConvertObjectToJSonV2(objRequest);
                var client = new RestClient(LOSConfiguration.BaseUrl);
                var request = new RestRequest(LOSConfiguration.LOS_Action_AddComment);

                request.Method = Method.POST;
                request.Parameters.Clear();
                request.AddHeader("Accept", "application/json");
                request.AddParameter("Authorization", LMS.Entites.Dtos.LOSServices.LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"SaveCommentHistory|Api={url}|body={body}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                        resultData.SetSucces();
                    else
                        resultData.Message = result.Meta.ErrorMessage;
                }
                else
                {
                    _logger.LogError($"SaveCommentHistory_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"SaveCommentHistory_Exception|Api={url}|body={body}|ex={e.Message}-{e.StackTrace}");
            }
            return resultData;
        }

        /// <summary>
        /// disbursementType: 1 kế toán, 2 lender, 3 tự động
        /// </summary>
        /// <param name="loanCreditID"></param>
        /// <param name="lenderID"></param>
        /// <param name="disbursementType"></param>
        /// <returns></returns>
        public async Task<ResponseActionResult> PushLoanLender(PushLoanLenderReq req)
        {
            var responseData = new ResponseActionResult();
            string body = "";
            var url = $"{LOSConfiguration.BaseUrl}{LOSConfiguration.LOS_Action_PushLoanLender}";
            try
            {
                body = _common.ConvertObjectToJSonV2(req);
                var client = new RestClient(LOSConfiguration.BaseUrl);
                var request = new RestRequest(LOSConfiguration.LOS_Action_PushLoanLender);

                request.Method = Method.POST;
                request.Parameters.Clear();
                request.AddHeader("Accept", "application/json");
                request.AddParameter("Authorization", LMS.Entites.Dtos.LOSServices.LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"PushLoanLender|Api={url}|body={body}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                    {
                        responseData.SetSucces();
                    }
                    else
                    {
                        responseData.Message = result.Meta.ErrorMessage;
                    }
                    return responseData;
                }
                else
                {
                    _logger.LogError($"PushLoanLender_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"PushLoanLender_Exception|Api={url}|body={body}|body={body}|ex={e.Message}-{e.StackTrace}");
            }
            return responseData;
        }
        /// <summary>
        /// type: 1 - mở khóa, 2: khóa đơn vay
        /// </summary>
        /// <param name="loanCreditID"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public async Task<ResponseActionResult> LockLoan(LockLoanReq req)
        {
            var resultData = new ResponseActionResult();
            //var dataRequest = new
            //{
            //    LoanId = loanCreditID,
            //    Type = type,
            //    Note = note,
            //    UserName = username
            //};
            string body = "";
            var url = $"{LOSConfiguration.BaseUrl}{LOSConfiguration.LOS_Action_LockLoan}";
            try
            {
                var client = new RestClient(LOSConfiguration.BaseUrl);
                var request = new RestRequest(LOSConfiguration.LOS_Action_LockLoan);
                body = _common.ConvertObjectToJSonV2(req);
                request.Method = Method.POST;
                request.Parameters.Clear();
                request.AddHeader("Accept", "application/json");
                request.AddParameter("Authorization", LMS.Entites.Dtos.LOSServices.LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"LockLoan|Api={url}|body={body}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                        resultData.SetSucces();
                    else
                        resultData.Message = result.Meta.ErrorMessage;
                }
                else
                {
                    _logger.LogError($"LockLoan_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }
                return resultData;
            }
            catch (Exception e)
            {
                _logger.LogError($"LockLoan_Exception|Api={url}|body={body}|ex={e.Message}-{e.StackTrace}");
            }
            return resultData;
        }
        /// <summary>
        /// disbursementType: 1 - giải ngân thành công, 2 - tất toán hợp đồng
        /// </summary>
        /// <param name="loanCreditID"></param>
        /// <param name="disbursementType"></param>
        /// <returns></returns>
        public async Task<ResponseActionResult> DisbursementLoan(DisbursementLoanReq req)
        {
            var resultData = new ResponseActionResult();
            string body = "";
            var url = $"{LOSConfiguration.BaseUrl}{LOSConfiguration.LOS_Action_Disbursement}";
            try
            {
                var client = new RestClient(LOSConfiguration.BaseUrl);
                var request = new RestRequest(LOSConfiguration.LOS_Action_Disbursement);
                body = _common.ConvertObjectToJSonV2(req);

                request.Method = Method.POST;
                request.Parameters.Clear();
                request.AddHeader("Accept", "application/json");
                request.AddParameter("Authorization", LMS.Entites.Dtos.LOSServices.LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"DisbursementLoan|Api={url}|body={body}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                        resultData.SetSucces();
                    else
                        resultData.Message = result.Meta.ErrorMessage;
                }
                else
                {
                    _logger.LogError($"DisbursementLoan_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"DisbursementLoan_Exception|Api={url}|body={body}|ex={e.Message}-{e.StackTrace}");
            }
            return resultData;
        }
        public async Task<ResponseActionResult> ReturnLoan(ReturnLoanReq req)
        {
            var responseData = new ResponseActionResult();
            string body = "";
            var url = $"{LOSConfiguration.BaseUrl}{LOSConfiguration.LOS_Action_ReturnLoan}";
            try
            {
                body = _common.ConvertObjectToJSonV2(req);
                var client = new RestClient(LOSConfiguration.BaseUrl);
                var request = new RestRequest(LOSConfiguration.LOS_Action_ReturnLoan);

                request.Method = Method.POST;
                request.Parameters.Clear();
                request.AddHeader("Accept", "application/json");
                request.AddParameter("Authorization", LMS.Entites.Dtos.LOSServices.LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"ReturnLoan|Api={url}|body={body}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                        responseData.SetSucces();
                    else
                        responseData.Message = result.Meta.ErrorMessage;
                }
                else
                {
                    _logger.LogError($"ReturnLoan_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }
                return responseData;
            }
            catch (Exception e)
            {
                _logger.LogError($"ReturnLoan_Exception|Api={url}|body={body}|ex={e.Message}-{e.StackTrace}");
            }
            return responseData;
        }
        public async Task<List<ResultListImage>> GetListImages(GetListImagesReq req)
        {
            List<ResultListImage> lstData = new List<ResultListImage>();
            string body = "";
            var url = $"{LOSConfiguration.BaseUrl}{LOSConfiguration.LOS_Action_GetListImage}";
            try
            {
                body = _common.ConvertObjectToJSonV2(req);
                var client = new RestClient(LOSConfiguration.BaseUrl);
                var request = new RestRequest(LOSConfiguration.LOS_Action_GetListImage);

                request.Method = Method.POST;
                request.Parameters.Clear();
                request.AddHeader("Accept", "application/json");
                request.AddParameter("Authorization", LMS.Entites.Dtos.LOSServices.LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"GetListImages|Api={url}|body={body}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                    {
                        if (result.Data != null)
                        {
                            return _common.ConvertJSonToObjectV2<List<ResultListImage>>(result.Data.ToString());
                        }
                    }
                }
                else
                {
                    _logger.LogError($"GetListImages_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"GetListImages_Exception|Api={url}|body={body}|ex={e.Message}-{e.StackTrace}");
            }
            return lstData;
        }
        public async Task<ResponseActionResult> ChangeDisbursementByAccountant(ChangeDisbursementByAccountantReq req)
        {
            var responseData = new ResponseActionResult();
            string body = "";
            var url = $"{LOSConfiguration.BaseUrl}{LOSConfiguration.LOS_Action_ChangeDisbursement}";
            try
            {
                body = _common.ConvertObjectToJSonV2(req);
                var client = new RestClient(LOSConfiguration.BaseUrl);
                var request = new RestRequest(LOSConfiguration.LOS_Action_ChangeDisbursement);
                request.Method = Method.POST;
                request.Parameters.Clear();
                request.AddParameter("Authorization", LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                request.AddHeader("Accept", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"ChangeDisbursementByAccountant|Api={url}|body={body}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                    {
                        responseData.SetSucces();
                        return responseData;
                    }
                    else if (result.Meta.ErrorCode == 212)
                    {
                        responseData.Message = MessageConstant.RetryMinute;
                        return responseData;
                        //System.Threading.Thread.Sleep(TimeSpan.FromSeconds(10));
                        //return ChangeDisbursementByAccountant(loanCreditID, note, username, disbursementBy, ++retry);
                    }
                    else
                    {
                        responseData.Message = result.Meta.ErrorMessage;
                    }
                }
                else
                {
                    _logger.LogError($"ChangeDisbursementByAccountant_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"ChangeDisbursementByAccountant_Exception|Api={url}|body={body}|ex={e.Message}-{e.StackTrace}");
            }
            return responseData;
        }
        public async Task<List<ResultSearchLoan>> SearchLoanCredit(ReqSearchLoanCredit objRequest)
        {
            var lstData = new List<ResultSearchLoan>();
            var body = _common.ConvertObjectToJSonV2(objRequest);
            var url = LOSConfiguration.BaseUrl + LOSConfiguration.LOS_Action_SearchLoanCredit + "?page=" + objRequest.Page + "&pageSize=" + objRequest.PageSize + "&loanBriefId=" + objRequest.LoanBriefId + "&search=" + objRequest.Search;
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest();
                request.Parameters.Clear();
                request.AddParameter("Authorization", LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                request.AddHeader("Accept", "application/json");
                request.Method = Method.GET;
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"SearchLoanCredit|Api={url}|body={body}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                    {
                        if (result.Data != null)
                        {
                            return _common.ConvertJSonToObjectV2<List<ResultSearchLoan>>(result.Data.ToString());
                        }
                    }
                }
                else
                {
                    _logger.LogError($"SearchLoanCredit_Error|Api={url}|body={body}|responseStatusCode={response.StatusCode}");
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"SearchLoanCredit_Exception|Api={url}|body={body}|ex={e.Message}-{e.StackTrace}");
            }
            return lstData;
        }
        public async Task UpdateTopUpToLos(List<ReqLoanTopUp> objRequest)
        {
            var body = _common.ConvertObjectToJSonV2(objRequest);
            var url = LOSConfiguration.BaseUrl + LOSConfiguration.LOS_Action_CanTopUp;
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest();
                request.Method = Method.POST;
                request.Parameters.Clear();
                request.AddParameter("Authorization", LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                request.AddHeader("Accept", "application/json");
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"UpdateTopUpToLos|Api={url}|body={body}|responseContent={response.Content}");
                    return;
                }
                else
                {
                    _logger.LogError($"UpdateTopUpToLos_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"UpdateTopUpToLos_Exception|Api={url}|ex={e.Message}-{e.StackTrace}");
            }
        }
        public async Task<List<WaitingMoneyDisbursementLenderLOS>> GetWaitingMoneyDisbursementLenderLOS()
        {
            List<WaitingMoneyDisbursementLenderLOS> lstData = new List<WaitingMoneyDisbursementLenderLOS>();
            var url = $"{LOSConfiguration.BaseUrl}{LOSConfiguration.LOS_Action_ChangeDisbursement}";
            try
            {
                var client = new RestClient(LOSConfiguration.BaseUrl);
                var request = new RestRequest(LOSConfiguration.LOS_Action_GetWaitingMoneyDisbursementLender);

                request.Method = Method.GET;
                request.Parameters.Clear();
                request.AddHeader("Accept", "application/json");
                request.AddParameter("Authorization", LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"GetWaitingMoneyDisbursementLenderLOS|Api={url}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                    {
                        if (result.Data != null)
                        {
                            return _common.ConvertJSonToObjectV2<List<WaitingMoneyDisbursementLenderLOS>>(result.Data.ToString());
                        }
                    }
                }
                else
                {
                    _logger.LogError($"GetWaitingMoneyDisbursementLenderLOS_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"GetWaitingMoneyDisbursementLenderLOS_Exception|Api={url}|ex={e.Message}-{e.StackTrace}");
            }
            return lstData;
        }
        public async Task<List<ReportTimeDisbursementOfAccountant>> LosReportTime(DateTime fromDate, DateTime ToDate, int type, int pageCurrent, int pageSize)
        {
            List<ReportTimeDisbursementOfAccountant> lstData = new List<ReportTimeDisbursementOfAccountant>();
            var url = LOSConfiguration.BaseUrl + LOSConfiguration.LOS_Action_LosReportTime + "?fromDate=" + fromDate.ToString("dd-MM-yyyy") + "&toDate=" + ToDate.ToString("dd-MM-yyyy") + "&type=" + type;

            try
            {
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.Parameters.Clear();
                request.AddHeader("Authorization", LOSConfiguration.AuthorizationLos);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"LosReportTime|Api={url}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                    {
                        if (result.Data != null)
                        {
                            var lstDataLos = _common.ConvertJSonToObjectV2<List<LosReportTime>>(result.Data.ToString());
                            if (lstDataLos != null && lstDataLos.Any())
                            {
                                var StartRecord = 1;
                                long totalCount = lstDataLos.Count();
                                double avergeMinute = lstDataLos.Average(x => x.Diff);
                                TimeSpan totalss = TimeSpan.FromSeconds(avergeMinute);
                                var thoigiantrungbinh = string.Format("{0:D2}:{1:D2}:{2:D2}", totalss.Hours, totalss.Minutes, totalss.Seconds);

                                foreach (var item in lstDataLos)
                                {
                                    TimeSpan timess = TimeSpan.FromSeconds(item.Diff);
                                    var time = string.Format("{0:D2}:{1:D2}:{2:D2}", timess.Hours, timess.Minutes, timess.Seconds);
                                    ReportTimeDisbursementOfAccountant data = new ReportTimeDisbursementOfAccountant()
                                    {
                                        CustomerName = item.FullName,
                                        AgencyName = item.LenderName,
                                        CodeID = item.CodeId,
                                        AvgTimeDisbursement = (int)avergeMinute,
                                        FirstTimeAccountantDisbursement = item.DisbursementTime,
                                        FirstTimeAccountantReceived = item.ReceiveTime,
                                        LoanCreditID = item.LoanBriefId,
                                        TimeDisbursementView = time,
                                        TimeDisbursement = item.Diff,
                                        TotalCount = totalCount,
                                        STT = StartRecord,
                                        TheAverageTime = thoigiantrungbinh
                                    };
                                    StartRecord++;
                                    lstData.Add(data);
                                }
                                return lstData.Skip((pageCurrent - 1) * pageSize).Take(pageSize).ToList();
                                // Cấu hình cho phân trang- Để 50 bản ghi 1 page
                            }
                        }
                    }

                }
                else
                {
                    _logger.LogError($"LosReportTime_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }

            }
            catch (Exception e)
            {
                _logger.LogError($"LosReportTime|Api={url}|ex={e.Message}-{e.StackTrace}");
            }
            return lstData;
        }
        /// <summary>
        /// 0: get all
        /// </summary>
        /// <param name="lenderID"></param>
        /// <returns></returns>
        public async Task<List<ResultLoanWaitingBlockDetailLOS>> GetLstLoanDisbursementWaitingBlockLOS(long lenderID = 0)
        {
            var lstData = new List<ResultLoanWaitingBlockDetailLOS>();
            var url = LOSConfiguration.BaseUrl + LOSConfiguration.LOS_Action_DisbursementWaitingBlock + "?lenderId=" + lenderID;
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.Parameters.Clear();
                request.AddHeader("Accept", "application/json");
                request.AddParameter("Authorization", LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"GetLstLoanDisbursementWaitingBlockLOS|Api={url}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                    {
                        if (result.Data != null)
                        {
                            return _common.ConvertJSonToObjectV2<List<ResultLoanWaitingBlockDetailLOS>>(result.Data.ToString());
                        }
                    }
                }
                else
                {
                    _logger.LogError($"GetLstLoanDisbursementWaitingBlockLOS_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }

            }
            catch (Exception e)
            {
                _logger.LogError($"GetLstLoanDisbursementWaitingBlockLOS_Exception|Api={url}|ex={e.Message}-{e.StackTrace}");
            }
            return lstData;
        }

        public async Task<bool> ChangeDisbursementByAccountant(ChangeDisbursementByAccountantReq dataRequest, int retry = 0)
        {
            string body = "";
            var url = $"{LOSConfiguration.BaseUrl}{LOSConfiguration.LOS_Action_ChangeDisbursement}";
            try
            {
                body = _common.ConvertObjectToJSonV2(dataRequest);
                var client = new RestClient(LOSConfiguration.BaseUrl);
                var request = new RestRequest(LOSConfiguration.LOS_Action_ChangeDisbursement);
                request.Method = Method.POST;
                request.Parameters.Clear();
                request.AddParameter("Authorization", LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                request.AddHeader("Accept", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"ChangeDisbursementByAccountant|Api={url}|body={body}|retry={retry}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                        return true;
                    if (result.Meta.ErrorCode == 212 && retry < 3)
                    {
                        await Task.Delay(TimeSpan.FromSeconds(5));
                        return await ChangeDisbursementByAccountant(dataRequest, ++retry);
                    }
                }
                else
                {
                    _logger.LogError($"ChangeDisbursementByAccountant_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"ChangeDisbursementByAccountant_Exception|retry={retry}|Api={url}|body={body}|ex={e.Message}-{e.StackTrace}");
            }
            return false;
        }

        public async Task<ResponseActionResult> ChangePhoneLOS(ChangePhoneLOSReq req)
        {
            var resultData = new ResponseActionResult();
            if (!LOSConfiguration.IsProduct)
            {
                resultData.SetSucces();
                return resultData;
            }
            string body = "";
            var url = $"{LOSConfiguration.BaseUrl}{LOSConfiguration.LOS_Update_Phone_Other}";
            try
            {
                var client = new RestClient(LOSConfiguration.BaseUrl);
                var request = new RestRequest(LOSConfiguration.LOS_Update_Phone_Other);
                body = _common.ConvertObjectToJSonV2(req);
                request.Method = Method.POST;
                request.Parameters.Clear();
                request.AddHeader("Accept", "application/json");
                request.AddParameter("Authorization", LMS.Entites.Dtos.LOSServices.LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"ChangePhoneLOS|Api={url}|body={body}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                        resultData.SetSucces();
                    else
                        resultData.Message = result.Meta.ErrorMessage;
                }
                else
                {
                    _logger.LogError($"ChangePhoneLOS_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }
                return resultData;
            }
            catch (Exception e)
            {
                _logger.LogError($"ChangePhoneLOS_Exception|Api={url}|body={body}|ex={e.Message}-{e.StackTrace}");
            }
            return resultData;
        }
        public async Task<ResponseActionResult> UpdateLinkFacebook(UpdateLinkFacebookReq req)
        {
            var resultData = new ResponseActionResult();
            string body = "";
            var url = $"{LOSConfiguration.BaseUrl}{LOSConfiguration.LOS_Update_LinkFacebook}";
            try
            {
                var client = new RestClient(LOSConfiguration.BaseUrl);
                var request = new RestRequest(LOSConfiguration.LOS_Update_LinkFacebook);
                body = _common.ConvertObjectToJSonV2(req);
                request.Method = Method.POST;
                request.Parameters.Clear();
                request.AddHeader("Accept", "application/json");
                request.AddParameter("Authorization", LMS.Entites.Dtos.LOSServices.LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"UpdateLinkFacebook|Api={url}|body={body}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                        resultData.SetSucces();
                    else
                        resultData.Message = result.Meta.ErrorMessage;
                }
                else
                {
                    _logger.LogError($"UpdateLinkFacebook_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }
                return resultData;
            }
            catch (Exception e)
            {
                _logger.LogError($"UpdateLinkFacebook_Exception|Api={url}|body={body}|ex={e.Message}-{e.StackTrace}");
            }
            return resultData;
        }

        public async Task<ResponseActionResult> GetRelativeFamily()
        {
            var result = new ResponseActionResult();
            var url = LOSConfiguration.BaseUrl + LOSConfiguration.LOS_Relative_Family;
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.Parameters.Clear();
                request.AddHeader("Accept", "application/json");
                request.AddParameter("Authorization", LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"GetRelativeFamily|Api={url}|responseContent={response.Content}");
                    var resultData = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (resultData.Meta.ErrorCode == 200)
                    {
                        if (resultData.Data != null)
                        {
                            result.Data = _common.ConvertJSonToObjectV2<List<RelativeFamily>>(resultData.Data.ToString());
                            result.SetSucces();
                        }
                    }
                    else
                    {
                        result.Message = resultData.Meta.ErrorMessage;
                    }
                }
                else
                {
                    _logger.LogError($"GetRelativeFamily_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"GetRelativeFamily_Exception|Api={url}|ex={e.Message}-{e.StackTrace}");
            }
            return result;
        }

        public async Task<ResponseActionResult> CreateRelationship(CreateRelationshipReq req)
        {
            var resultData = new ResponseActionResult();
            string body = "";
            var url = $"{LOSConfiguration.BaseUrl}{LOSConfiguration.LOS_Relationship}";
            try
            {
                var client = new RestClient(LOSConfiguration.BaseUrl);
                var request = new RestRequest(LOSConfiguration.LOS_Relationship);
                body = _common.ConvertObjectToJSonV2(req);
                request.Method = Method.POST;
                request.Parameters.Clear();
                request.AddHeader("Accept", "application/json");
                request.AddParameter("Authorization", LMS.Entites.Dtos.LOSServices.LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"CreateRelationship|Api={url}|body={body}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                        resultData.SetSucces();
                    else
                        resultData.Message = result.Meta.ErrorMessage;
                }
                else
                {
                    _logger.LogError($"CreateRelationship_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }
                return resultData;
            }
            catch (Exception e)
            {
                _logger.LogError($"CreateRelationship_Exception|Api={url}|body={body}|ex={e.Message}-{e.StackTrace}");
            }
            return resultData;
        }

        public async Task<ResponseActionResult> GetVehicleInforOfLoan(List<int> lstLoanBriefId)
        {
            var resultData = new ResponseActionResult();
            string body = "";
            var url = $"{LOSConfiguration.BaseUrl}{LOSConfiguration.LOS_Property_Info}";
            try
            {
                var client = new RestClient(LOSConfiguration.BaseUrl);
                var request = new RestRequest(LOSConfiguration.LOS_Property_Info);
                body = _common.ConvertObjectToJSonV2(lstLoanBriefId);
                request.Method = Method.POST;
                request.Parameters.Clear();
                request.AddHeader("Accept", "application/json");
                request.AddParameter("Authorization", LMS.Entites.Dtos.LOSServices.LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"GetVehicleInforOfLoan|Api={url}|body={body}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == 200)
                    {
                        var data = _common.ConvertJSonToObjectV2<List<LoanBriefPropertyLOS>>(result.Data.ToString());
                        resultData.SetSucces();
                        resultData.Data = data;
                    }
                    else
                        resultData.Message = result.Meta.ErrorMessage;
                }
                else
                {
                    _logger.LogError($"GetVehicleInforOfLoan_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }
                return resultData;
            }
            catch (Exception e)
            {
                _logger.LogError($"GetVehicleInforOfLoan_Exception|Api={url}|body={body}|ex={e.Message}-{e.StackTrace}");
            }
            return resultData;
        }

        public async Task<ResponseActionResult> CreateLenderDigitalSignature(LenderDigitalSignature req)
        {
            var resultData = new ResponseActionResult();
            string body = "";
            var url = $"{LOSConfiguration.BaseUrl}{LOSConfiguration.LOS_Property_LenderEsign}";
            try
            {
                var client = new RestClient(LOSConfiguration.BaseUrl);
                var request = new RestRequest(LOSConfiguration.LOS_Property_LenderEsign);
                body = _common.ConvertObjectToJSonV2(req);
                request.Method = Method.POST;
                request.Parameters.Clear();
                request.AddHeader("Accept", "application/json");
                request.AddParameter("Authorization", LMS.Entites.Dtos.LOSServices.LOSConfiguration.AuthorizationLos, ParameterType.HttpHeader);
                request.AddParameter("UserName", GetHeaderUserName(), ParameterType.HttpHeader);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = await client.ExecuteAsync<ResponseDataLOS>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _logger.LogInformation($"CreateLenderDigitalSignature|Api={url}|body={body}|responseContent={response.Content}");
                    var result = _common.ConvertJSonToObjectV2<ResponseDataLOS>(response.Content);
                    if (result.Meta.ErrorCode == TimaSettingConstant.CallSucessAPI)
                    {
                        var data = _common.ConvertJSonToObjectV2<long>(result.Data.ToString());
                        resultData.SetSucces();
                        resultData.Data = result.Meta.ErrorCode;
                    }
                    else
                        resultData.Message = result.Meta.ErrorMessage;
                }
                else
                {
                    _logger.LogError($"CreateLenderDigitalSignature_Error|Api={url}|responseStatusCode={response.StatusCode}");
                }
                return resultData;
            }
            catch (Exception e)
            {
                _logger.LogError($"GetVehicleInforOfLoan_Exception|Api={url}|body={body}|ex={e.Message}-{e.StackTrace}");
            }
            return resultData;
        }
    }
}
