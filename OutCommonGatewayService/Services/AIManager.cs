﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.AG;
using LMS.Entites.Dtos.AI;
using LMS.Entites.Dtos.InsuranceService;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OutGatewayApi.Domain.Models;
using OutGatewayApi.Domain.Tables;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutGatewayApi.Services
{
    public interface IAIManager
    {
        TblTokenAI Authentication(string AppID, string AppKey);
        Task<ResponseActionResult> TopFriendFacebook(string KeySearch);
        Task<ResponseActionResult> GetStopPoint(RequestGPS req);
        Task<ResponseActionResult> ShowHistoryComment(HistoryCommentChipReq req);
        Task<ResponseActionResult> CheckInOut(AICheckInOutReq req);
        Task<ResponseActionResult> ListCheckInOut(string code);
    }
    public class AIManager : IAIManager
    {
        AiConfiguration _aiConfiguration;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTokenAI> _tokenAITab;
        ILogger<AIManager> _logger;
        LMS.Common.Helper.Utils _common;
        public AIManager(
            AiConfiguration aiConfiguration,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTokenAI> tokenAITab,
            ILogger<AIManager> logger)
        {
            _aiConfiguration = aiConfiguration;
            _tokenAITab = tokenAITab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public TblTokenAI Authentication(string AppID, string AppKey)
        {
            var objToken = new TblTokenAI();
            try
            {
                var tokenAPI = _tokenAITab.WhereClause(x => x.AppID == AppID).Query().FirstOrDefault();
                if (tokenAPI == null || tokenAPI.DateExpiration <= DateTime.Now)
                {
                    var url = AIConfiguration.BaseUrl + AIConfiguration.Authentication;
                    var client = new RestClient(url);
                    client.Timeout = 3000;
                    var objInput = new
                    {
                        app_id = AppID,
                        app_key = AppKey
                    };
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("cache-control", "no-cache");
                    request.AddParameter("application/json", Newtonsoft.Json.JsonConvert.SerializeObject(objInput), ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var jsonResponse = response.Content;
                        AuthenticationResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<AuthenticationResult>(jsonResponse);
                        if (result != null)
                        {
                            objToken.Token = result.Token;
                            objToken.DateExpiration = result.ExpiredDate;
                            objToken.AppID = AppID;
                            objToken.CreateDate = DateTime.Now;
                            _ = _tokenAITab.Insert(objToken);
                        }
                    }
                    return objToken;
                }
                else
                {
                    return tokenAPI;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"AIManager_Authentication|AppID={AppID}|AppKey={AppKey}|ex={ex.Message}|StackTrace={ex.StackTrace}");
            }
            return objToken;
        }

        public async Task<ResponseActionResult> TopFriendFacebook(string KeySearch)
        {
            var result = new ResponseActionResult();
            try
            {
                var token = Authentication(_aiConfiguration.AppIdTopFriends, _aiConfiguration.AppKeyTopFriends);
                if (token != null)
                {
                    var req = new
                    {
                        phone = KeySearch
                    };
                    var body = JsonConvert.SerializeObject(req);
                    var url = AIConfiguration.BaseUrl + AIConfiguration.TopFriends;
                    var client = new RestClient(url);
                    client.Timeout = 15000;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("cache-control", "no-cache");
                    request.AddHeader("authorization", token.Token);
                    request.AddParameter("application/json", body, ParameterType.RequestBody);
                    IRestResponse response = await client.ExecuteAsync(request);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var jsonResponse = response.Content;
                        var data = Newtonsoft.Json.JsonConvert.DeserializeObject<FriendFacebook>(jsonResponse);
                        result.SetSucces();
                        result.Data = data;
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"AIManager_TopFriendFacebokk|KeySearch={KeySearch}|ex={ex.Message}|StackTrace={ex.StackTrace}");
            }
            return result;
        }


        public async Task<ResponseActionResult> GetStopPoint(RequestGPS req)
        {
            var result = new ResponseActionResult();
            try
            {
                var body = JsonConvert.SerializeObject(req);
                var url = AIConfiguration.BaseUrl2 + AIConfiguration.Location_StoppingPoint;
                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.Parameters.Clear();
                request.AddHeader("cache-control", "no-cache");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var jsonResponse = response.Content;
                    var data = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGPS>(jsonResponse);
                    result.SetSucces();
                    result.Data = data;
                }
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError($"AIManager_GetStopPoint|request={_common.ConvertObjectToJSonV2(req)}|ex={ex.Message}|StackTrace={ex.StackTrace}");
            }
            return result;
        }

        public async Task<ResponseActionResult> ShowHistoryComment(HistoryCommentChipReq req)
        {
            var result = new ResponseActionResult();
            var data = new List<HistoryCommentChip>();
            try
            {
                var url = $"{LMS.Entites.Dtos.AG.AGConstants.APIAG + AGConstants.HistoryCommentChip}?LoanCreditID={req.LoanCreditIDOfPartner}&PageIndex={req.PageIndex}&PageSize={req.PageSize}";
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.Parameters.Clear();
                request.AddHeader("cache-control", "no-cache");
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var jsonResponse = response.Content;
                    var responseData = Newtonsoft.Json.JsonConvert.DeserializeObject<AGResponse>(jsonResponse);
                    if (responseData.Status == (int)AGResponse_Status.Success)
                    {
                        result.Data = responseData.Data;
                        result.SetSucces();
                    }
                    else
                    {
                        result.Message = responseData.Message;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError($"AIManager_GetStopPoint|request={_common.ConvertObjectToJSonV2(req)}|ex={ex.Message}|StackTrace={ex.StackTrace}");
            }
            return result;
        }
        public async Task<ResponseActionResult> CheckInOut(AICheckInOutReq req)
        {
            var result = new ResponseActionResult();
            try
            {
                var body = JsonConvert.SerializeObject(req);
                var url = $"{AIConfiguration.BaseUrl_CheckIn_Out + AIConfiguration.CheckIn_Out}";

                var client = new RestClient(url);
                var request = new RestRequest(Method.POST);
                request.Parameters.Clear();
                request.AddHeader("cache-control", "no-cache");
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = await client.ExecuteAsync(request);

                _logger.LogInformation($"AIManager_CheckInOut|Api={url}|body={body}|responseContent={response.Content}");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var jsonResponse = response.Content;
                    var data = Newtonsoft.Json.JsonConvert.DeserializeObject<AICheckInOutResponse>(jsonResponse);
                    if (data.Message == "Success")
                        result.SetSucces();
                }
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError($"AIManager_CheckInOut|request={_common.ConvertObjectToJSonV2(req)}|ex={ex.Message}|StackTrace={ex.StackTrace}");
            }
            return result;
        }

        public async Task<ResponseActionResult> ListCheckInOut(string code)
        {
            var result = new ResponseActionResult();
            try
            {
                var url = $"{AIConfiguration.BaseUrl2_ListCheckInOut + AIConfiguration.ListCheckInOut + code}";
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                request.Parameters.Clear();
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", AIConfiguration.Token_ListCheckInOut);
                IRestResponse response = await client.ExecuteAsync(request);

                _logger.LogInformation($"AIManager_ListCheckInOut|Api={url}|code={code}|responseContent={response.Content}");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var jsonResponse = response.Content;
                    var responseData = Newtonsoft.Json.JsonConvert.DeserializeObject<AiListCheckInOut>(jsonResponse);
                    if (responseData.StatusCode == 200)
                    {
                        result.Data = responseData.Data;
                        result.SetSucces();
                    }
                    else
                    {
                        result.Message = responseData.Message;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError($"AIManager_ListCheckInOut|Code={code}|ex={ex.Message}|StackTrace={ex.StackTrace}");
            }
            return result;
        }
    }
}
