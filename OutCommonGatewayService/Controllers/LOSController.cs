﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.LOSServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OutGatewayApi.Domain;
using OutGatewayApi.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutGatewayApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LOSController : ControllerBase
    {
        Services.ILOSManager _lOSManager;
        public LOSController(Services.ILOSManager lOSManager)
        {
            _lOSManager = lOSManager;
        }
        [HttpGet]
        [Route("GetLoanCreditDetail/{loanID}")]
        public async Task<ResponseActionResult> GetLoanCreditDetail(int loanID)
        {
            ResponseActionResult result = new ResponseActionResult();
            result.SetSucces();
            result.Data = await _lOSManager.GetLoanCreditDetail(loanID);
            return result;
        }

        [HttpPost]
        [Route("DisbursementWaiting")]
        public async Task<ResponseActionResult> DisbursementWaiting(ResultLoanDisbursmentWaitingReq req)
        {
            ResponseActionResult result = new ResponseActionResult();
            result.SetSucces();
            result.Data = await _lOSManager.DisbursementWaiting(req);
            result.Total = req.TotalRecord;
            return result;
        }

        [HttpPost]
        [Route("GetHistoryComment")]
        public async Task<ResponseActionResult> GetHistoryComment(ReqGetHistoryComment req)
        {
            ResponseActionResult result = new ResponseActionResult();
            result.SetSucces();
            result.Data = await _lOSManager.GetHistoryComment(req);
            return result;
        }

        [HttpPost]
        [Route("SaveCommentHistory")]
        public async Task<ResponseActionResult> SaveCommentHistory(ReqSaveCommentHistory req)
        {
            return await _lOSManager.SaveCommentHistory(req);
        }

        [HttpPost]
        [Route("PushLoanLender")]
        public async Task<ResponseActionResult> PushLoanLender(PushLoanLenderReq req)
        {
            return await _lOSManager.PushLoanLender(req);
        }

        [HttpPost]
        [Route("LockLoan")]
        public async Task<ResponseActionResult> LockLoan(LockLoanReq req)
        {
            return await _lOSManager.LockLoan(req);
        }
        [HttpPost]
        [Route("DisbursementLoan")]
        public async Task<ResponseActionResult> DisbursementLoan(DisbursementLoanReq req)
        {
            return await _lOSManager.DisbursementLoan(req);
        }

        [HttpPost]
        [Route("ReturnLoan")]
        public async Task<ResponseActionResult> ReturnLoan(ReturnLoanReq req)
        {
            return await _lOSManager.ReturnLoan(req);
        }
        [HttpPost]
        [Route("GetListImages")]
        public async Task<ResponseActionResult> GetListImages(GetListImagesReq req)
        {
            ResponseActionResult result = new ResponseActionResult();
            result.SetSucces();
            result.Data = await _lOSManager.GetListImages(req);
            return result;
        }

        [HttpPost]
        [Route("ChangeDisbursementByAccountant")]
        public async Task<ResponseActionResult> ChangeDisbursementByAccountant(ChangeDisbursementByAccountantReq req)
        {
            return await _lOSManager.ChangeDisbursementByAccountant(req);
        }
        [HttpPost]
        [Route("SearchLoanCredit")]
        public async Task<ResponseActionResult> SearchLoanCredit(ReqSearchLoanCredit req)
        {
            ResponseActionResult result = new ResponseActionResult();
            result.SetSucces();
            result.Data = await _lOSManager.SearchLoanCredit(req);
            return result;
        }
        [HttpGet]
        [Route("GetWaitingMoneyDisbursementLenderLOS")]
        public async Task<ResponseActionResult> GetWaitingMoneyDisbursementLenderLOS()
        {
            ResponseActionResult result = new ResponseActionResult();
            result.SetSucces();
            result.Data = await _lOSManager.GetWaitingMoneyDisbursementLenderLOS();
            return result;
        }
        [HttpGet]
        [Route("LosReportTime")]
        public async Task<ResponseActionResult> LosReportTime(DateTime fromDate, DateTime ToDate, int type, int pageCurrent, int pageSize)
        {
            ResponseActionResult result = new ResponseActionResult();
            result.SetSucces();
            result.Data = await _lOSManager.LosReportTime(fromDate, ToDate, type, pageCurrent, pageSize);
            return result;
        }
        [HttpGet]
        [Route("GetLstLoanDisbursementWaitingBlockLOS")]
        public async Task<ResponseActionResult> GetLstLoanDisbursementWaitingBlockLOS(long lenderID)
        {
            ResponseActionResult result = new ResponseActionResult();
            result.SetSucces();
            result.Data = await _lOSManager.GetLstLoanDisbursementWaitingBlockLOS(lenderID);
            return result;
        }

        [HttpPost]
        [Route("ChangePhoneLOS")]
        public async Task<ResponseActionResult> ChangePhoneLOS(ChangePhoneLOSReq req)
        {
            return await _lOSManager.ChangePhoneLOS(req);
        }
        [HttpPost]
        [Route("UpdateLinkFacebook")]
        public async Task<ResponseActionResult> UpdateLinkFacebook(UpdateLinkFacebookReq req)
        {
            return await _lOSManager.UpdateLinkFacebook(req);
        }
        [HttpGet]
        [Route("GetRelativeFamilyLOS")]
        public async Task<ResponseActionResult> GetRelativeFamily()
        {
            return await _lOSManager.GetRelativeFamily();
        }
        [HttpPost]
        [Route("CreateRelationshipLOS")]
        public async Task<ResponseActionResult> CreateRelationship(CreateRelationshipReq req)
        {
            return await _lOSManager.CreateRelationship(req);
        }

        [HttpPost]
        [Route("GetVehicleInforOfLoan")]
        public async Task<ResponseActionResult> GetVehicleInforOfLoan(List<int> lstLoanBriefId)
        {
            return await _lOSManager.GetVehicleInforOfLoan(lstLoanBriefId);
        }
        [HttpPost]
        [Route("CreateLenderDigitalSignature")]
        public async Task<ResponseActionResult> CreateLenderDigitalSignature(LenderDigitalSignature req)
        {
            return await _lOSManager.CreateLenderDigitalSignature(req);
        }
    }
}
