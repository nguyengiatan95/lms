﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Common.Constants;
using LMS.Entites.Dtos.AG;
using LMS.Entites.Dtos.AI;
using Microsoft.AspNetCore.Mvc;

namespace OutGatewayApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AIController : Controller
    {
        Services.IAIManager _aIManager;
        public AIController(Services.IAIManager aIManager)
        {
            _aIManager = aIManager;
        }
        [HttpGet]
        [Route("TopFriendFacebook")]
        public async Task<ResponseActionResult> TopFriendFacebook(string KeySearch)
        {
            return await Task.Run(() =>
            {
                return _aIManager.TopFriendFacebook(KeySearch);
            });
        }
        [HttpPost]
        [Route("GetStopPoint")]
        public async Task<ResponseActionResult> GetStopPoint(RequestGPS request)
        {
            return await Task.Run(() =>
            {
                return _aIManager.GetStopPoint(request);
            });
        }
        [HttpPost]
        [Route("ShowHistoryComment")]
        public async Task<ResponseActionResult> ShowHistoryComment(HistoryCommentChipReq req)
        {
            return await Task.Run(() =>
            {
                return _aIManager.ShowHistoryComment(req);
            });
        }
        [HttpPost]
        [Route("CheckInOut")]
        public async Task<ResponseActionResult> CheckInOut(AICheckInOutReq req)
        {
            return await _aIManager.CheckInOut(req);
        }
        [HttpGet]
        [Route("ListCheckInOut/{code}")]
        public async Task<ResponseActionResult> ListCheckInOut(string code)
        {
            return await _aIManager.ListCheckInOut(code);
        }
    }
}