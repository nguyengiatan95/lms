﻿using LMS.Common.Constants;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutGatewayApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SmartDialerController : ControllerBase
    {
        readonly Services.ISmartDialerManager _smartDialerManager;
        public SmartDialerController(Services.ISmartDialerManager smartDialerManager)
        {
            _smartDialerManager = smartDialerManager;
        }
        [HttpGet]
        [Route("GetCampaign")]
        public async Task<ResponseActionResult> GetCampaign()
        {
            return await _smartDialerManager.GetCampain();
        }
        [HttpPost]
        [Route("ImportCampaign")]
        public async Task<ResponseActionResult> ImportCampaign(LMS.Entites.Dtos.SmartDialer.ImportCampaignRequest request)
        {
            return await _smartDialerManager.ImportCampaign(request);
        }
        [HttpPost]
        [Route("GetPendingOfCampaign")]
        public async Task<ResponseActionResult> GetPendingOfCampaign(LMS.Entites.Dtos.SmartDialer.PendingOfCampaignRequest request)
        {
            return await _smartDialerManager.GetPendingOfCampaign(request);
        }
        [HttpPost]
        [Route("UpdateResource")]
        public async Task<ResponseActionResult> UpdateResource(LMS.Entites.Dtos.SmartDialer.UpdateResourceRequest request)
        {
            return await _smartDialerManager.UpdateResource(request);
        }

        [HttpPost]
        [Route("UpdateCsq")]
        public async Task<ResponseActionResult> UpdateCsq(LMS.Entites.Dtos.SmartDialer.UpdateCsqRequest request)
        {
            return await _smartDialerManager.UpdateCsq(request);
        }
    }
}
