﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Common.Constants;
using LMS.Entites.Dtos.InsuranceService;
using Microsoft.AspNetCore.Mvc;

namespace OutGatewayApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InsuranceController : ControllerBase
    {
        Services.IInsuranceManager _insuranceManagerManager;
        public InsuranceController(Services.IInsuranceManager insuranceManagerManager)
        {
            _insuranceManagerManager = insuranceManagerManager;
        }

        [HttpPost]
        [Route("CreateBorrowerContract")]
        public async Task<ResponseActionResult> CreateBorrowerContract(CreateBorrowerContract entity)
        {
            return await Task.Run(() =>
            {
                return _insuranceManagerManager.CreateBorrowerContract(entity);
            });
        }
        [HttpPost]
        [Route("CreateLenderContract")]
        public async Task<ResponseActionResult> OutputCreateLenderContract(CreateLenderContract entity)
        {
            return await Task.Run(() =>
            {
                return _insuranceManagerManager.CreateLenderContract(entity);
            });
        }
        [HttpGet]
        [Route("GetDoc")]
        public async Task<ResponseActionResult> GetDoc(string Contract, int Type, long ID)
        {
            return await Task.Run(() =>
            {
                return _insuranceManagerManager.GetDoc(Contract, Type, ID);
            });
        }

        [HttpGet]
        [Route("GetInformationInsuranceAI")]
        public async Task<ResponseActionResult> GetInformationInsuranceAI(string Path, int DocType)
        {
            return await Task.Run(() =>
            {
                return _insuranceManagerManager.GetLoanCertificateInformation(Path, DocType);
            });
        }

        [HttpPost]
        [Route("CreateInsuranceMaterial")]
        public async Task<ResponseActionResult> CreateInsuranceMaterial(CreateInsuranceMaterialReq entity)
        {
            return await _insuranceManagerManager.BuyInsuranceMaterial(entity);
        }
    }
}