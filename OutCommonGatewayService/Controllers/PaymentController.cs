﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.APIAutoDisbursement;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutGatewayApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        Services.IVibBankPaymentManager _vibBankManager;
        public PaymentController(Services.IVibBankPaymentManager vibBankManager)
        {
            _vibBankManager = vibBankManager;
        }
        [HttpPost]
        [Route("VibCreateTransaction")]
        public async Task<ResponseActionResult> VibCreateTransaction(CreateTransactionRequest dataPost)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult result = new ResponseActionResult();
                result.SetSucces();
                result.Data = _vibBankManager.CreateTransaction(dataPost);
                return result;

            });
        }

        [HttpPost]
        [Route("VibExecuteOtp")]
        public async Task<ResponseActionResult> VibExecuteOtp(ExecuteOtpRequest dataPost)
        {
            ResponseActionResult result = new ResponseActionResult();
            result.SetSucces();
            result.Data = await _vibBankManager.ExecuteOtp(dataPost);
            return result;
        }
        [HttpPost]
        [Route("VibCancelTransaction")]
        public async Task<ResponseActionResult> VibCancelTransaction(CancelTransactionRequest dataPost)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult result = new ResponseActionResult();
                result.SetSucces();
                result.Data = _vibBankManager.CancelTransaction(dataPost);
                return result;

            });
        }
    }
}
