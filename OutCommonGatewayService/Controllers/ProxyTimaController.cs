﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.ProxyTima;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutGatewayApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProxyTimaController : Controller
    {
        Services.IProxyTimaManager _proxyTimaManager;
        public ProxyTimaController(Services.IProxyTimaManager proxyTimaManager)
        {
            _proxyTimaManager = proxyTimaManager;
        }
        [HttpPost]
        [Route("K03ProxyTima")]
        public async Task<ResponseActionResult> K03ProxyTima(K03ProxyTimaRequest entity)
        {
            return await _proxyTimaManager.K03ProxyTima(entity);
        }
        [HttpPost]
        [Route("K06ProxyTima")]
        public async Task<ResponseActionResult> K06ProxyTima(K06ProxyTimaRequest entity)
        {
            return await _proxyTimaManager.K06ProxyTima(entity);
        }
        [HttpPost]
        [Route("K21ProxyTima")]
        public async Task<ResponseActionResult> K21ProxyTima(K21ProxyTimaRequest entity)
        {
            return await _proxyTimaManager.K21ProxyTima(entity);
        }
        [HttpPost]
        [Route("K22ProxyTima")]
        public async Task<ResponseActionResult> K22ProxyTima(K22ProxyTimaRequest entity)
        {
            return await _proxyTimaManager.K22ProxyTima(entity);
        }
    }
}
