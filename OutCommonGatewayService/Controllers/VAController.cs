﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Common.Constants;
using LMS.Entites.Dtos.VAService;
using Microsoft.AspNetCore.Mvc;

namespace OutGatewayApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VAController : ControllerBase
    {
        Services.IVAManager _vAManager;
        public VAController(Services.IVAManager vAManager)
        {
            _vAManager = vAManager;
        }
        
        [HttpPost]
        [Route("Register")]
        public async Task<ResponseActionResult> Register(RegisterVA request)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult result = new ResponseActionResult { Result = 1 };
                var data = _vAManager.Register(request);
                result.Data = data;
                return result;
            });
        }
    }
}