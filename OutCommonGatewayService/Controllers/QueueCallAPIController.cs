﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.AG;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutGatewayApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QueueCallAPIController : Controller
    {
        Services.IQueueCallAPIManager _queueCallAPIManager;
        public QueueCallAPIController(Services.IQueueCallAPIManager queueCallAPIManager)
        {
            _queueCallAPIManager = queueCallAPIManager;
        }
        [HttpGet]
        [Route("QueueCallAPIAG")]
        public async Task<ResponseActionResult> QueueCallAPIAG()
        {
            return await _queueCallAPIManager.QueueCallAPI();
        }

        [HttpPost]
        [Route("CallApiAg")]
        public async Task<ResponseActionResult> CallApiAg(AgDataPostModel request)
        {
            return await _queueCallAPIManager.CallApiAg(request);
        }
    }
}
