﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutGatewayApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblTokenAI")]
    public class TblTokenAI
    {
        [Dapper.Contrib.Extensions.Key]
        public int TokenID { get; set; }

        public string Token { get; set; }

        public DateTime? DateExpiration { get; set; }

        public DateTime? CreateDate { get; set; }

        public string AppID { get; set; }
    }
}
