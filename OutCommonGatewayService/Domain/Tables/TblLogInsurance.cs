﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutGatewayApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblLogInsurance")]
    public class TblLogInsurance
    {
        [Dapper.Contrib.Extensions.Key]
        public long LogInsuranceID { get; set; }

        public DateTime? CreateDate { get; set; }

        public string Request { get; set; }

        public string Response { get; set; }

        public int? Status { get; set; }

        public string ResponseCode { get; set; }

        public long? LoanID { get; set; }

        public long? CustomerID { get; set; }

        public long? LenderID { get; set; }

        public int? InsuranceCompensator { get; set; }

        public int? TypeInsurance { get; set; }
        public int TypeRequest { get; set; }
    }
}
