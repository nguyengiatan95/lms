﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutGatewayApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblLogPushCampaign")]
    public class TblLogPushCampaign
    {
        [Dapper.Contrib.Extensions.Key]
        public long LogPushCampaignID { get; set; }

        public string DataPost { get; set; }

        public DateTime CreateDate { get; set; }

        public string ResponseContext { get; set; }
        public string Url { get; set; }
    }
}
