﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutGatewayApi.Domain.Models
{
    public class SmartDialerConfiguration
    {
        public string Url { get; set; }
        public string Authorization { get; set; }
        public string ContentType { get; set; }
    }
}
