﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutGatewayApi.Domain.Models
{
    public class ProxyTimaConfiguration
    {
        public string Url { get; set; }
        public bool IsProduct { get; set; }
        public string Authorization { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
        public string PhoneTest { get; set; }
        public string NumberCardTest { get; set; }
    }
}
