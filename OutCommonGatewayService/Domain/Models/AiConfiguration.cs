﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutGatewayApi.Domain.Models
{
    public class AiConfiguration
    {
        public string AppIdTopFriends { get; set; }
        public string AppKeyTopFriends { get; set; }
    }
}
