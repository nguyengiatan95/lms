﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutGatewayApi.Domain.Models
{
    public class VIBConfiguration
    {
        public string BankAccountNumber { get; set; }
        public string FullName { get; set; }
        public long LoanAmountFinal { get; set; }
        public long BankID { get; set; }
    }
}
