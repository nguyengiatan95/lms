﻿namespace OutGatewayApi.Domain.Models
{
    public class LenderDigitalSignature
    {
        public long LenderId { get; set; }
        public string AgreementId { get; set; }
        public string Passcode { get; set; }
    }
}
