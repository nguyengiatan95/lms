﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutGatewayApi.Domain.Models
{
    public class BaoMinhConfiguration
    {
        public string Code { get; set; }
        public string Path { get; set; }
        public bool IsProduct { get; set; }
    }
}
