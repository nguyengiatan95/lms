using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Steeltoe.Discovery.Client;
using Steeltoe.Discovery.Eureka;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace OutGatewayServiceApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<KestrelServerOptions>(Configuration.GetSection("Kestrel"));
            services.AddDiscoveryClient(Configuration);
            // or add a specific Discovery Client
            services.AddServiceDiscovery(options => options.UseEureka());
            services.AddControllers().AddNewtonsoftJson(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddHttpContextAccessor();
            services.AddTransient<OutGatewayApi.Services.ILOSManager, OutGatewayApi.Services.LOSManager>();

            LMS.Common.Configuration.ConnectStringSetting.DefaultConnectString = Configuration["ConnectStringSetting:DefaultConnectString"];
            LMS.Entites.Dtos.LOSServices.LOSConfiguration.BaseUrl = Configuration["LOSConfiguration:Url"];
            LMS.Entites.Dtos.LOSServices.LOSConfiguration.AuthorizationLos = Configuration["LOSConfiguration:AuthorizationLos"];
            LMS.Entites.Dtos.InsuranceService.BaoMinhConsants.BaseUrl = Configuration["BaoMinhConfiguration:Url"];
            LMS.Entites.Dtos.InsuranceService.BaoMinhConsants.BaseUrl2 = Configuration["BaoMinhConfiguration:Url2"];
            LMS.Entites.Dtos.InsuranceService.InsuranceAIConsants.BaseUrl = Configuration["InsuranceAIConfiguration:Url"];
            LMS.Entites.Dtos.VAService.VAConstants.BaseUrl = Configuration["VAConfiguration:Url"];
            LMS.Entites.Dtos.AI.AIConfiguration.BaseUrl = Configuration["AiConfiguration:Url"];
            LMS.Entites.Dtos.AI.AIConfiguration.BaseUrl2 = Configuration["AiConfiguration:Url2"];
            LMS.Entites.Dtos.LOSServices.LOSConfiguration.IsProduct = bool.Parse(Configuration["LOSConfiguration:IsProduct"]);
            LMS.Entites.Dtos.AG.AGConstants.APIAG = Configuration["AGConfiguration:APIAG"];
            LMS.Entites.Dtos.ProxyTima.ProxyTimaConfiguration.BaseUrl = Configuration["ProxyTimaConfiguration:Url"];
            LMS.Entites.Dtos.AI.AIConfiguration.BaseUrl_CheckIn_Out = Configuration["AiConfiguration:Url_CheckInOut"];
            LMS.Entites.Dtos.AI.AIConfiguration.BaseUrl2_ListCheckInOut = Configuration["AiConfiguration:ListCheckInOut"];
            LMS.Entites.Dtos.AI.AIConfiguration.Token_ListCheckInOut = Configuration["AiConfiguration:Token_ListCheckInOut"];

            services.AddTransient(typeof(LMS.Common.DAL.ITableHelper<>), typeof(LMS.Common.DAL.TableHelper<>));
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddTransient<LMS.Common.Helper.IApiHelper, LMS.Common.Helper.ApiHelper>();
            services.AddTransient<OutGatewayApi.Services.IInsuranceManager, OutGatewayApi.Services.InsuranceManager>();
            services.AddTransient<OutGatewayApi.Services.IVAManager, OutGatewayApi.Services.VAManager>();
            services.AddTransient<OutGatewayApi.Services.IVibBankPaymentManager, OutGatewayApi.Services.VibBankPaymentManager>();
            services.AddTransient<OutGatewayApi.Services.IAIManager, OutGatewayApi.Services.AIManager>();
            services.AddTransient<OutGatewayApi.Services.IQueueCallAPIManager, OutGatewayApi.Services.QueueCallAPIManager>();
            services.AddTransient<OutGatewayApi.Services.IProxyTimaManager, OutGatewayApi.Services.ProxyTimaManager>();
            services.AddTransient<OutGatewayApi.Services.ISmartDialerManager, OutGatewayApi.Services.SmartDialerManager>();

            var vibConfiguration = new OutGatewayApi.Domain.Models.VIBConfiguration();
            Configuration.GetSection(nameof(OutGatewayApi.Domain.Models.VIBConfiguration)).Bind(vibConfiguration);
            services.AddSingleton(vibConfiguration);

            var baoMinhConfiguration = new OutGatewayApi.Domain.Models.BaoMinhConfiguration();
            Configuration.GetSection(nameof(OutGatewayApi.Domain.Models.BaoMinhConfiguration)).Bind(baoMinhConfiguration);
            services.AddSingleton(baoMinhConfiguration);

            var aiConfiguration = new OutGatewayApi.Domain.Models.AiConfiguration();
            Configuration.GetSection(nameof(OutGatewayApi.Domain.Models.AiConfiguration)).Bind(aiConfiguration);
            services.AddSingleton(aiConfiguration);

            var agConfiguration = new OutGatewayApi.Domain.Models.AGConfiguration();
            Configuration.GetSection(nameof(OutGatewayApi.Domain.Models.AGConfiguration)).Bind(agConfiguration);
            services.AddSingleton(agConfiguration);

            var proxyTimaConfiguration = new OutGatewayApi.Domain.Models.ProxyTimaConfiguration();
            Configuration.GetSection(nameof(OutGatewayApi.Domain.Models.ProxyTimaConfiguration)).Bind(proxyTimaConfiguration);
            services.AddSingleton(proxyTimaConfiguration);

            var smartDialerConfiguration = new OutGatewayApi.Domain.Models.SmartDialerConfiguration();
            Configuration.GetSection(nameof(OutGatewayApi.Domain.Models.SmartDialerConfiguration)).Bind(smartDialerConfiguration);
            services.AddSingleton(smartDialerConfiguration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();
            app.UseDiscoveryClient();
            //Add our new middleware to the pipeline
            app.UseMiddleware<LMS.Common.Helper.RequestResponseLoggingMiddleware>();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
