﻿using CollectionServiceApi.Domain.Models.Loan;
using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.LoanServices;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CollectionServiceApi.Services
{
    public interface ILoanManager
    {
        Task<ResponseActionResult> GetLoanInfosByCondition(Domain.Models.Loan.RequestLoanItemViewModel request);
        Task<List<LMS.Entites.Dtos.CollectionServices.Loan.LoanItemViewModel>> GetLstLoanItemViewModels(Domain.Models.Loan.RequestLoanItemViewModel request);
    }
    public class LoanManager : ILoanManager
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> _assignmentLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> _commentDebtPromptedTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblHoldCustomerMoney> _holdCustomerMoneyTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCutOffLoan> _cutOffLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> _userDepartmentTab;
        readonly Services.IReportManager _reportManager;
        readonly ILogger<LoanManager> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public LoanManager(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> assignmentLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> commentDebtPromptedTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblHoldCustomerMoney> holdCustomerMoneyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCutOffLoan> cutOffLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDepartmentTab,
            Services.IReportManager reportManager,
            ILogger<LoanManager> logger)
        {
            _loanTab = loanTab;
            _customerTab = customerTab;
            _assignmentLoanTab = assignmentLoanTab;
            _commentDebtPromptedTab = commentDebtPromptedTab;
            _holdCustomerMoneyTab = holdCustomerMoneyTab;
            _cutOffLoanTab = cutOffLoanTab;
            _userTab = userTab;
            _userDepartmentTab = userDepartmentTab;
            _logger = logger;
            _common = new Utils();
            _reportManager = reportManager;
        }
        public async Task<ResponseActionResult> GetLoanInfosByCondition(RequestLoanItemViewModel request)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                List<LMS.Entites.Dtos.CollectionServices.Loan.LoanItemViewModel> lstLoanItemView = await GetLstLoanItemViewModels(request);
                ResponseForDataTable dataTable = new ResponseForDataTable
                {
                    RecordsTotal = 0,
                    Data = lstLoanItemView,
                    RecordsFiltered = 0,
                    Draw = 1
                };
                if (lstLoanItemView.Count > 0)
                {
                    dataTable.RecordsTotal = lstLoanItemView[0].TotalRows;
                }
                response.SetSucces();
                response.Data = dataTable;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLoanInfosByCondition|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private async Task<Dictionary<long, long>> _GetWithEmployeeID(List<long> employeeIds, int type = (int)AssignmentLoan_Type.DebtLoan)
        {
            var currentDate = DateTime.Now;
            var firtDateOfMonth = new DateTime(currentDate.Year, currentDate.Month, 1);
            var firtDateOfNextMonth = firtDateOfMonth.AddMonths(1);
            switch ((AssignmentLoan_Type)type)
            {
                case AssignmentLoan_Type.DebtLoan:
                    {
                        var lstType = new List<long>()
                        {
                           (int)AssignmentLoan_Type.DebtLoan, (int)AssignmentLoan_Type.AllDebtMoto
                        };
                        _assignmentLoanTab.WhereClause(x => lstType.Contains(x.Type));
                        break;
                    }
                case AssignmentLoan_Type.MotoSeizure:
                    {
                        var lstType = new List<long>()
                        {
                           (int)AssignmentLoan_Type.MotoSeizure, (int)AssignmentLoan_Type.AllDebtMoto
                        };
                        _assignmentLoanTab.WhereClause(x => lstType.Contains(x.Type));
                        break;
                    }
                case AssignmentLoan_Type.AllDebtMoto:
                    {
                        _assignmentLoanTab.WhereClause(x => x.Type == (int)AssignmentLoan_Type.AllDebtMoto);
                        break;
                    }
                default:
                    break;
            }
            if (employeeIds != null && employeeIds.Any())
            {
                _assignmentLoanTab.WhereClause(x => employeeIds.Contains(x.UserID));
            }
            var lstLoanIDAssignedTask = await _assignmentLoanTab.SelectColumns(x => x.LoanID)
                                                   .WhereClause(x => x.DateAssigned >= firtDateOfMonth && x.DateApplyTo < firtDateOfNextMonth && x.Status == (int)AssignmentLoan_Status.Processing)
                                                   .QueryAsync();
            var lstLoanIDAssigned = lstLoanIDAssignedTask.Select(x => x.LoanID).Distinct().ToList();
            if (lstLoanIDAssigned == null || lstLoanIDAssigned.Count == 0)
            {
                return null;
            }
            var dictLoanIDAssigned = lstLoanIDAssigned.ToDictionary(x => x, x => x);

            return dictLoanIDAssigned;
        }

        private async Task<List<long>> GetLstUserIDByParentUserID(long parentUserID, int position)
        {
            List<long> lstResult = new List<long>();
            // lấy tất cả nhân viên ở phòng thn
            _userDepartmentTab.SelectColumns(x => x.UserID, x => x.PositionID, x => x.ParentUserID, x => x.DepartmentID, x => x.ChildDepartmentID)
                  .JoinOn<Domain.Tables.TblUser>(ud => ud.UserID, u => u.UserID)
                  .WhereClauseJoinOn<Domain.Tables.TblUser>(x => x.Status == (int)StatusCommon.Active)
                  .JoinOn<Domain.Tables.TblDepartment>(ud => ud.DepartmentID, d => d.DepartmentID)
                  .WhereClauseJoinOn<Domain.Tables.TblDepartment>(d => d.AppID == (int)Menu_AppID.THN)
                  .WhereClause(x => x.PositionID <= position);
            var lstUseInfos = await _userDepartmentTab.QueryAsync<Domain.Models.EmployeeInfoModel>();

            lstResult.Add(parentUserID);
            lstUseInfos = lstUseInfos.OrderByDescending(x => x.PositionID).ThenByDescending(x => x.ParentUserID).ThenBy(x => x.UserID);
            var parentUserInfo = lstUseInfos.FirstOrDefault(x => x.UserID == parentUserID);
            long positionParentUserID = parentUserInfo.PositionID;
            // ds parentID theo từng cấp
            List<long> lstParentUserIDCurrent = new List<long>();
            // khơi tạo danh sách trước
            lstParentUserIDCurrent.Add(parentUserID);
            for (long i = positionParentUserID; i > 0; i--) // cân nhắc > 1: có child nhưng vị trí là nhân viên 
            {
                var lstUserChildren = lstUseInfos.Where(x => lstParentUserIDCurrent.Contains(x.ParentUserID)).ToList();
                if (lstUserChildren == null || !lstUserChildren.Any())
                {
                    continue;
                }
                lstParentUserIDCurrent.Clear();
                foreach (var chil in lstUserChildren)
                {
                    lstResult.Add(chil.UserID);
                    lstParentUserIDCurrent.Add(chil.UserID);
                }
            }
            return lstResult;
        }

        public async Task<List<LMS.Entites.Dtos.CollectionServices.Loan.LoanItemViewModel>> GetLstLoanItemViewModels(Domain.Models.Loan.RequestLoanItemViewModel request)
        {
            List<LMS.Entites.Dtos.CollectionServices.Loan.LoanItemViewModel> lstLoanItemView = new List<LMS.Entites.Dtos.CollectionServices.Loan.LoanItemViewModel>();
            try
            {
                var currentDate = DateTime.Now;
                var firtDateOfMonth = new DateTime(currentDate.Year, currentDate.Month, 1);
                var firtDateOfNextMonth = firtDateOfMonth.AddMonths(1);
                var lastDatePreMonth = firtDateOfMonth.AddDays(-1);

                List<long> lstCustomerIDs = new List<long>();
                List<long> lstLoanIDAssigned = new List<long>();
                Dictionary<long, long> dictLoanIDAssigned = new Dictionary<long, long>();
                long minLoanIDAssigned = int.MaxValue;
                long maxLoanIDAssgined = -1;

                if (request.EmployeeID != (int)LMS.Common.Constants.StatusCommon.SearchAll) // search theo nv đầu tiên, k truyền mới check ở dưới
                {
                    dictLoanIDAssigned = await _GetWithEmployeeID(new List<long>() { request.EmployeeID }, request.Type);
                    if (dictLoanIDAssigned == null)
                    {
                        return lstLoanItemView;
                    }
                    lstLoanIDAssigned = dictLoanIDAssigned.Keys.ToList();
                    if (lstLoanIDAssigned.Count > TimaSettingConstant.MaxItemQuery)
                    {
                        foreach (var item in lstLoanIDAssigned)
                        {
                            if (item > maxLoanIDAssgined)
                            {
                                maxLoanIDAssgined = item;
                            }
                            else if (item < minLoanIDAssigned)
                            {
                                minLoanIDAssigned = item;
                            }
                        }
                        maxLoanIDAssgined += 1;
                        minLoanIDAssigned -= 1;
                        _loanTab.WhereClause(x => x.LoanID > minLoanIDAssigned && x.LoanID < maxLoanIDAssgined);
                    }
                    else
                    {
                        _loanTab.WhereClause(x => lstLoanIDAssigned.Contains(x.LoanID));
                    }
                    //request.HasSearchLoanGroupByCustomer = true;
                }
                else if (request.LeaderID != (int)LMS.Common.Constants.StatusCommon.SearchAll) // search theo leder k truyền mới check ở dưới
                {
                    var lstUserIDChild = await GetLstUserIDByParentUserID(request.LeaderID, (int)UserDepartment_PositionID.TeamLead);
                    if (lstUserIDChild.Count < 1)
                    {
                        return lstLoanItemView;
                    }
                    dictLoanIDAssigned = await _GetWithEmployeeID(lstUserIDChild, request.Type);
                    if (dictLoanIDAssigned == null)
                    {
                        return lstLoanItemView;
                    }
                    lstLoanIDAssigned = dictLoanIDAssigned.Keys.ToList();
                    if (lstLoanIDAssigned.Count > TimaSettingConstant.MaxItemQuery)
                    {
                        foreach (var item in lstLoanIDAssigned)
                        {
                            if (item > maxLoanIDAssgined)
                            {
                                maxLoanIDAssgined = item;
                            }
                            else if (item < minLoanIDAssigned)
                            {
                                minLoanIDAssigned = item;
                            }
                        }
                        maxLoanIDAssgined += 1;
                        minLoanIDAssigned -= 1;
                        _loanTab.WhereClause(x => x.LoanID > minLoanIDAssigned && x.LoanID < maxLoanIDAssgined);
                    }
                    else
                    {
                        _loanTab.WhereClause(x => lstLoanIDAssigned.Contains(x.LoanID));
                    }
                }
                else if (request.ManagerID != (int)LMS.Common.Constants.StatusCommon.SearchAll) // search theo trưởng nhóm đầu tiên, k truyền mới check ở dưới
                {
                    var lstUserIDChild = await GetLstUserIDByParentUserID(request.ManagerID, (int)UserDepartment_PositionID.Manager);
                    if (lstUserIDChild.Count < 1)
                    {
                        return lstLoanItemView;
                    }
                    dictLoanIDAssigned = await _GetWithEmployeeID(lstUserIDChild, request.Type);
                    if (dictLoanIDAssigned == null)
                    {
                        return lstLoanItemView;
                    }
                    lstLoanIDAssigned = dictLoanIDAssigned.Keys.ToList();
                    if (lstLoanIDAssigned.Count > TimaSettingConstant.MaxItemQuery)
                    {
                        foreach (var item in lstLoanIDAssigned)
                        {
                            if (item > maxLoanIDAssgined)
                            {
                                maxLoanIDAssgined = item;
                            }
                            else if (item < minLoanIDAssigned)
                            {
                                minLoanIDAssigned = item;
                            }
                        }
                        maxLoanIDAssgined += 1;
                        minLoanIDAssigned -= 1;
                        _loanTab.WhereClause(x => x.LoanID > minLoanIDAssigned && x.LoanID < maxLoanIDAssgined);
                    }
                    else
                    {
                        _loanTab.WhereClause(x => lstLoanIDAssigned.Contains(x.LoanID));
                    }
                }
                // search theo keysearch
                if (!string.IsNullOrEmpty(request.KeySearch) || request.LoanID > 0)
                {
                    if (request.LoanID > 0)
                    {
                        _loanTab.WhereClause(x => x.LoanID == request.LoanID);
                    }
                    if (long.TryParse(request.KeySearch, out long contracCodeID) && request.KeySearch.Length < TimaSettingConstant.MaxLengthPhone - 1)
                    {
                        request.KeySearch = $"{TimaSettingConstant.PrefixContractCode}{contracCodeID}";
                        _loanTab.WhereClause(x => x.ContactCode == request.KeySearch);
                    }
                    else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternContractCode, RegexOptions.IgnoreCase).Success)
                    {
                        _loanTab.WhereClause(x => x.ContactCode == request.KeySearch);
                    }
                    else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternContractCodeLOSLoanCreditID, RegexOptions.IgnoreCase).Success)
                    {
                        var loanCreditID = long.Parse(request.KeySearch.Replace(TimaSettingConstant.PatternContractCodeLOS, "", StringComparison.OrdinalIgnoreCase));
                        _loanTab.WhereClause(x => x.LoanCreditIDOfPartner == loanCreditID);
                    }
                    else
                    {
                        if (request.KeySearch.Length == TimaSettingConstant.MaxLengthPhone && Regex.Match(request.KeySearch, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success)
                        {
                            var lstCustomerIDsTask = await _customerTab.SelectColumns(x => x.CustomerID).WhereClause(x => x.Phone == request.KeySearch).QueryAsync();
                            lstCustomerIDs = lstCustomerIDsTask.Select(x => x.CustomerID).ToList();
                        }
                        else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternNumberCard, RegexOptions.IgnoreCase).Success)
                        {
                            var lstCustomerIDsTask = await _customerTab.SelectColumns(x => x.CustomerID).WhereClause(x => x.NumberCard == request.KeySearch).QueryAsync();
                            lstCustomerIDs = lstCustomerIDsTask.Select(x => x.CustomerID).ToList();
                        }
                        if (lstCustomerIDs != null && lstCustomerIDs.Count > 0)
                        {
                            _loanTab.WhereClause(x => lstCustomerIDs.Contains((long)x.CustomerID));
                        }
                        else
                        {
                            _loanTab.WhereClause(x => x.CustomerName.Contains(request.KeySearch));
                        }
                    }
                }

                var nextDate = DateTime.Now.Date;
                var fromDate = DateTime.Now.Date;
                var toDate = DateTime.Now.Date;
                List<long> lstLoanIDComment = null;

                // search theo Số ngày quá hạn
                switch (request.DPD)
                {
                    case 0:
                        // dpd = 0
                        _loanTab.WhereClause(x => x.NextDate == nextDate);
                        break;
                    case 1:
                        // dpd từ 1 đến 4
                        fromDate = nextDate.AddDays(-5);
                        toDate = nextDate;
                        _loanTab.WhereClause(x => x.NextDate > fromDate && x.NextDate < toDate);
                        break;
                    case 5:
                        // dpd > 4
                        toDate = nextDate.AddDays(-4);
                        _loanTab.WhereClause(x => x.NextDate < toDate);
                        break;
                    case -1:
                        // dpd từ -4 đến -1
                        fromDate = nextDate;
                        toDate = nextDate.AddDays(5);
                        _loanTab.WhereClause(x => x.NextDate > fromDate && x.NextDate < toDate);
                        break;
                    case -5:
                        // dpd < -4
                        toDate = nextDate.AddDays(4);
                        _loanTab.WhereClause(x => x.NextDate > toDate);
                        break;
                    case 7: // đơn vay lấy giá trị comment theo code
                            // lấy comment trong 7 ngày gần nhất
                            // theo danh sach loan dc assign cho nhan vien
                        var sevenDayAgo = DateTime.Now.AddDays(-7).Date;
                        if (lstLoanIDAssigned.Count() > TimaSettingConstant.MaxItemQuery)
                        {
                            _commentDebtPromptedTab.WhereClause(x => x.LoanID > minLoanIDAssigned && x.LoanID < maxLoanIDAssgined);
                        }
                        else if (lstLoanIDAssigned.Count > 0)
                        {
                            _commentDebtPromptedTab.WhereClause(x => lstLoanIDAssigned.Contains(x.LoanID));
                        }
                        lstLoanIDComment = (await _commentDebtPromptedTab.SelectColumns(x => x.LoanID)
                                               .WhereClause(x => x.CreateDate > sevenDayAgo && RuleCollectionServiceHelper.LstReasonCodeForCustomer.Contains(x.ReasonCode))
                                               .QueryAsync())
                                               .Select(x => x.LoanID).Distinct().ToList();
                        if (lstLoanIDComment == null || !lstLoanIDComment.Any())
                        {
                            return lstLoanItemView;
                        }
                        break;
                    default: // không search theo DPD
                        break;
                }

                // app tìm theo ngày đơn vay có ngày hẹn trong comment
                if (!string.IsNullOrEmpty(request.AppointmentFromDate) && !string.IsNullOrEmpty(request.AppointmentToDate))
                {
                    var appointmentFromDate = DateTime.ParseExact(request.AppointmentFromDate, TimaSettingConstant.DateTimeDayMonthYear, null).AddSeconds(-1);
                    var appointmentToDate = DateTime.ParseExact(request.AppointmentToDate, TimaSettingConstant.DateTimeDayMonthYear, null).AddDays(1).Date;
                    if (lstLoanIDAssigned.Count() > TimaSettingConstant.MaxItemQuery)
                    {
                        _commentDebtPromptedTab.WhereClause(x => x.LoanID > minLoanIDAssigned && x.LoanID < maxLoanIDAssgined);
                    }
                    else if (lstLoanIDAssigned.Count > 0)
                    {
                        _commentDebtPromptedTab.WhereClause(x => lstLoanIDAssigned.Contains(x.LoanID));
                    }
                    lstLoanIDComment = (await _commentDebtPromptedTab.SelectColumns(x => x.LoanID)
                                           .WhereClause(x => x.AppointmentDate > appointmentFromDate && x.AppointmentDate < appointmentToDate)
                                           .QueryAsync())
                                           .Select(x => x.LoanID).Distinct().ToList();
                    if (lstLoanIDComment == null || !lstLoanIDComment.Any())
                    {
                        return lstLoanItemView;
                    }
                }
                // màn hình nhân viên khi search đơn đang vay mặc định lấy luôn đơn chưa bồi thường bảo hiểm
                if (request.LoanStatus != (int)StatusCommon.SearchAll)
                {
                    _loanTab.WhereClause(x => RuleCollectionServiceHelper.LstStatusLoanLending.Contains((int)x.Status) && x.StatusSendInsurance != (int)Loan_StatusSendInsurance.ProcessingComplete);
                }
                else
                {
                    _loanTab.WhereClause(x => x.Status != (int)Loan_Status.Delete);
                }
                // search thành phố
                if (request.CityID != (int)StatusCommon.SearchAll)
                {
                    _loanTab.WhereClause(x => x.CityID == request.CityID);
                }

                // search quận huyện
                if (request.DistrictID != (int)StatusCommon.SearchAll)
                {
                    _loanTab.WhereClause(x => x.CityID == request.DistrictID);
                }

                // lấy danh sách hđ
                var lstLoanInfos = (await _loanTab.QueryAsync()).ToList();

                if (lstLoanIDComment != null)
                {
                    lstLoanInfos = lstLoanInfos.Where(x => lstLoanIDComment.Contains(x.LoanID)).ToList();
                }
                List<Domain.Tables.TblLoan> lstLoanInfosAssigned = new List<Domain.Tables.TblLoan>();
                if (dictLoanIDAssigned.Count > 0)
                {
                    foreach (var item in lstLoanInfos)
                    {
                        if (dictLoanIDAssigned.ContainsKey(item.LoanID))
                        {
                            lstLoanInfosAssigned.Add(item);
                        }
                    }
                }
                else
                {
                    lstLoanInfosAssigned = lstLoanInfos;
                }

                int totalRow = lstLoanInfosAssigned.Count;
                // phân trang bản ghi
                lstLoanInfosAssigned = lstLoanInfosAssigned.OrderBy(x => x.NextDate)
                    .Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();

                List<long> lstLoanIDs = new List<long>();
                lstCustomerIDs.Clear();
                foreach (var item in lstLoanInfosAssigned)
                {
                    lstLoanIDs.Add(item.LoanID);
                    lstCustomerIDs.Add((long)item.CustomerID);
                }
                // lấy thông tin nhân viên được giao sau khi phân trang
                switch ((AssignmentLoan_Type)request.Type)
                {
                    case AssignmentLoan_Type.DebtLoan:
                        {
                            var lstType = new List<long>()
                            {
                               (int)AssignmentLoan_Type.DebtLoan, (int)AssignmentLoan_Type.AllDebtMoto
                            };
                            _assignmentLoanTab.WhereClause(x => lstType.Contains(x.Type));
                            break;
                        }
                    case AssignmentLoan_Type.MotoSeizure:
                        {
                            var lstType = new List<long>()
                            {
                               (int)AssignmentLoan_Type.MotoSeizure, (int)AssignmentLoan_Type.AllDebtMoto
                            };
                            _assignmentLoanTab.WhereClause(x => lstType.Contains(x.Type));
                            break;
                        }
                    case AssignmentLoan_Type.AllDebtMoto:
                        {
                            _assignmentLoanTab.WhereClause(x => x.Type == (int)AssignmentLoan_Type.AllDebtMoto);
                            break;
                        }
                    default:
                        break;
                }
                var dictEmployeeAssignmentInfosTask = _assignmentLoanTab.WhereClause(x => lstLoanIDs.Contains(x.LoanID) && x.DateAssigned >= firtDateOfMonth && x.DateApplyTo < firtDateOfNextMonth && x.Status == (int)StatusCommon.Active).QueryAsync();

                // lấy thông tin khách hàng sau khi phân trang
                var dictCustomerInfosTask = _customerTab.WhereClause(x => lstCustomerIDs.Contains(x.CustomerID)).QueryAsync();

                // lấy thông tin comment cuối cùng
                var dictCommentDebtPromptedTask = _reportManager.GetDictLoanCommentLatestInfosByLstLoanID(lstLoanIDs, firtDateOfMonth, firtDateOfNextMonth);

                // lấy dữ liệu ngày đầu tháng
                // được tính cutoff ngày cuối tháng
                var lstCutOffLoanDetailTask = _cutOffLoanTab.WhereClause(x => lstLoanIDs.Contains(x.LoanID) && x.CutOffDate == lastDatePreMonth).QueryAsync();

                //paymentScheduleInfos k thấy dùng
                //var dictSchedulePaymentLoanInfosTask = _paymentScheduleTab.WhereClause(x => lstLoanIDs.Contains(x.LoanID) && x.Status == (int)PaymentSchedule_Status.Use).QueryAsync();

                // lấy danh sách kh bị hold tiền
                var lstHoldCustomerMoneyInfosTask = _holdCustomerMoneyTab.WhereClause(x => lstCustomerIDs.Contains(x.CustomerID) && x.Status == (int)HoldCustomerMoney_Status.Hold).QueryAsync();

                await Task.WhenAll(dictEmployeeAssignmentInfosTask, dictCommentDebtPromptedTask, dictCustomerInfosTask, lstCutOffLoanDetailTask, lstHoldCustomerMoneyInfosTask);

                var dictEmployeeAssignmentInfos = dictEmployeeAssignmentInfosTask.Result.GroupBy(x => x.LoanID).ToDictionary(x => x.Key, x => x.ToList());
                var lstUserIDEmployees = new List<long>();
                foreach (var item in dictEmployeeAssignmentInfosTask.Result)
                {
                    lstUserIDEmployees.Add(item.UserID);

                }

                // lấy username nhân viên
                var dictEmployeeInfosTask = _userTab.SelectColumns(x => x.UserID, x => x.UserName).WhereClause(x => lstUserIDEmployees.Contains(x.UserID)).QueryAsync();

                // lấy vị trí trong phòng
                var lstUserDepartmentTask = _userDepartmentTab.WhereClause(x => lstUserIDEmployees.Contains(x.UserID)).QueryAsync();

                // lấy danh sách đơn vay theo KH
                Dictionary<long, int> dictCustomerHasMultiLoanInfos = new Dictionary<long, int>();
                if (request.HasSearchLoanGroupByCustomer)
                {
                    dictCustomerHasMultiLoanInfos = (await _loanTab.SelectColumns(x => x.CustomerID, x => x.LoanID)
                                                                .WhereClause(x => lstCustomerIDs.Contains((long)x.CustomerID))
                                                                .WhereClause(x => RuleCollectionServiceHelper.LstStatusLoanLending.Contains((int)x.Status))
                                                                .WhereClause(x => x.StatusSendInsurance != (int)Loan_StatusSendInsurance.ProcessingComplete)
                                                                .QueryAsync()).GroupBy(x => (long)x.CustomerID).ToDictionary(x => x.Key, x => x.Count());
                }


                await Task.WhenAll(dictEmployeeInfosTask, lstUserDepartmentTask);

                var dictLoanCommentInfos = dictCommentDebtPromptedTask.Result;
                var dictCustomerInfos = dictCustomerInfosTask.Result.ToDictionary(x => x.CustomerID, x => x);
                var dictCutOffLoanDetailFirstMonthInfos = lstCutOffLoanDetailTask.Result.ToDictionary(x => x.LoanID, x => x);
                //var dictSchedulePaymentLoanInfos = dictSchedulePaymentLoanInfosTask.Result.GroupBy(x => x.LoanID).ToDictionary(x => x.Key, x => x.ToList());
                var dictHoldMoneyCustomerInfos = lstHoldCustomerMoneyInfosTask.Result.ToDictionary(x => x.CustomerID, x => x);
                var dictEmployeeInfos = dictEmployeeInfosTask.Result.ToDictionary(x => x.UserID, x => x);
                var lstUserDepartmentInfos = lstUserDepartmentTask.Result;

                foreach (var item in lstLoanInfosAssigned)
                {
                    var customerInfo = dictCustomerInfos.GetValueOrDefault((long)item.CustomerID);
                    var lstEmployees = dictEmployeeAssignmentInfos.GetValueOrDefault(item.LoanID);
                    Domain.Tables.TblCommentDebtPrompted commentDetail = dictLoanCommentInfos.GetValueOrDefault(item.LoanID);
                    var cutOffLoanDetailFirstMonth = dictCutOffLoanDetailFirstMonthInfos.GetValueOrDefault(item.LoanID);
                    //var paymentScheduleInfos = dictSchedulePaymentLoanInfos.GetValueOrDefault(item.LoanID);
                    var holdMoneyInfo = dictHoldMoneyCustomerInfos.GetValueOrDefault((long)item.CustomerID);
                    int paymentStatus = 0; // chưa đóng
                    if (cutOffLoanDetailFirstMonth != null && cutOffLoanDetailFirstMonth.NextDate < item.NextDate)
                    {
                        paymentStatus = 1;
                    }
                    LMS.Entites.Dtos.CollectionServices.Loan.LoanItemViewModel loanDetail = new LMS.Entites.Dtos.CollectionServices.Loan.LoanItemViewModel
                    {
                        ContactCode = item.ContactCode,
                        CustomerPayTypeID = customerInfo.PayTypeID,
                        CustomerPayTypeName = ((Customer_PayTypeID)customerInfo.PayTypeID).GetDescription(),
                        LoanID = item.LoanID,
                        LoanTotalMoneyCurrent = item.TotalMoneyCurrent,
                        Employees = new List<LMS.Entites.Dtos.CollectionServices.Loan.EmployeeHandleLoan>(),
                        LatestDateAction = commentDetail?.CreateDate,
                        CustomerName = item.CustomerName,
                        CustomerTotalMoney = customerInfo.TotalMoney ?? 0,
                        ProductName = item.ProductName,
                        CustomerPhone = customerInfo.Phone,
                        NextDate = item.NextDate.Value,
                        CommentReasCode = commentDetail?.ReasonCode,
                        CustomerAddress = customerInfo.Address,
                        LoanBriefID = item.LoanCreditIDOfPartner ?? 0,
                        TimaLoanID = item.TimaLoanID,
                        NumberLoanCustomerHas = dictCustomerHasMultiLoanInfos.GetValueOrDefault((long)item.CustomerID)
                        // chưa có gán  AlarmDate
                    };
                    if (lstEmployees != null && lstEmployees.Any())
                    {
                        foreach (var em in lstEmployees)
                        {
                            // Lamvt said: chỉ lấy thông tin nhân viên
                            // ko lấy lead
                            var staffInfo = lstUserDepartmentInfos.Where(x => x.UserID == em.UserID && x.PositionID == (int)(int)LMS.Common.Constants.UserDepartment_PositionID.Staff).FirstOrDefault();
                            if (staffInfo != null && staffInfo.UserID > 0)
                            {
                                loanDetail.Employees.Add(new LMS.Entites.Dtos.CollectionServices.Loan.EmployeeHandleLoan
                                {
                                    UserID = em.UserID,
                                    UserName = dictEmployeeInfos.GetValueOrDefault(em.UserID)?.UserName
                                });
                            }
                            if (em.UserID == request.EmployeeID)
                            {
                                loanDetail.TypeName = ((AssignmentLoan_Type)lstEmployees.Where(x => x.UserID == request.EmployeeID).FirstOrDefault().Type).GetDescription();
                            }
                        }
                    }
                    if (holdMoneyInfo != null && holdMoneyInfo.CustomerID > 0)
                    {
                        loanDetail.CustomerTotalMoneyHold = holdMoneyInfo.Amount;
                    }
                    // những đơn đang vay
                    if (RuleCollectionServiceHelper.LstStatusLoanLending.Contains((int)item.Status))
                    {
                        if (item.SourcecBuyInsurance > (int)Loan_SourcecBuyInsurance.NO)
                        {
                            loanDetail.LoanInsuranceType = item.StatusSendInsurance;
                            loanDetail.LoanInsuranceTypeName = ((Loan_StatusSendInsurance)item.StatusSendInsurance).GetDescription();
                        }
                        // chưa tới thời điểm ghi nhận trạng thái bảo hiểm
                        if (item.ToDate.Date.AddDays(TimaSettingConstant.MaxDaySentInsurance) > currentDate.Date)
                        {
                            loanDetail.LoanInsuranceTypeName = "";
                        }
                        loanDetail.LoanPaymentStatus = paymentStatus;
                        loanDetail.LoanPaymentStatusName = paymentStatus == 0 ? RuleCollectionServiceHelper.LoanPaymentNotDonePeriods : RuleCollectionServiceHelper.LoanPaymentDonePeriods;
                        loanDetail.CountDownNextdate = Convert.ToInt32(currentDate.Date.Subtract(item.NextDate.Value.Date).TotalDays);
                    }
                    else
                    {
                        // hd kết thúc
                        // 25-01-22: lamvt: các đơn tất toán -> dpd, dư nợ hiện tại = 0
                        loanDetail.LoanPaymentStatusName = RuleCollectionServiceHelper.LoanPaymentDonePeriods;
                        loanDetail.LoanPaymentStatus = 1;
                        loanDetail.CountDownNextdate = 0;
                        loanDetail.LoanTotalMoneyCurrent = 0;
                    }
                    lstLoanItemView.Add(loanDetail);
                }
                if (lstLoanItemView.Count > 0)
                {
                    lstLoanItemView[0].TotalRows = totalRow;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLoanInfosByCondition|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return lstLoanItemView;
        }
    }
}
