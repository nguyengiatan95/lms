﻿using LMS.Common.Constants;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Services
{
    public interface IUserService
    {
        long GetHeaderUserID();

        Task<Domain.Models.EmployeeInfoModel> GetPositonUserIDLogin(long userID);
    }
    public class UserService : IUserService
    {
        readonly IHttpContextAccessor _httpContextAccessor;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> _userDeparmentTab;
        readonly ILogger<UserService> _logger;
        public UserService(IHttpContextAccessor httpContextAccessor,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDeparmentTab,
            ILogger<UserService> logger)
        {
            _httpContextAccessor = httpContextAccessor;
            _userDeparmentTab = userDeparmentTab;
            _logger = logger;
        }
        public long GetHeaderUserID()
        {
            long userID = 0;
            try
            {
                var lstHeader = _httpContextAccessor.HttpContext?.Request?.Headers;
                if (lstHeader != null && lstHeader.Any())
                {
                    var headerCheck = TimaSettingConstant.HeaderLMSUserID.ToLowerInvariant();
                    foreach (var item in lstHeader)
                    {
                        if (item.Key.ToLowerInvariant() == headerCheck)
                        {
                            userID = long.Parse(item.Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"GetHeaderUserID|ex={ex.Message}-{ex.StackTrace}");
            }
            //// test user
            //// 1, 2
            //userID = DateTime.Now.Ticks % 2 == 0 ? 1 : 2;
            //userID = 78678;// landt
            //userID = 78676;// nhungtth
            return userID;
        }

        public async Task<Domain.Models.EmployeeInfoModel> GetPositonUserIDLogin(long userID)
        {
            try
            {
                if (userID == TimaSettingConstant.UserIDAdmin)
                {
                    return new Domain.Models.EmployeeInfoModel
                    {
                        UserID = userID,
                        PositionID = (int)UserDepartment_PositionID.Director
                    };
                }
                _userDeparmentTab.SelectColumns(x => x.UserID, x => x.DepartmentID, x => x.PositionID, x => x.ParentUserID, x => x.ChildDepartmentID)
                                .WhereClause(x => x.UserID == userID)
                                .JoinOn<Domain.Tables.TblUser>(ud => ud.UserID, u => u.UserID)
                                .SelectColumnsJoinOn<Domain.Tables.TblUser>(u => u.FullName, u => u.UserName)
                                .WhereClauseJoinOn<Domain.Tables.TblUser>(x => x.Status == (int)StatusCommon.Active)
                                .JoinOn<Domain.Tables.TblDepartment>(ud => ud.DepartmentID, d => d.DepartmentID)
                                .WhereClauseJoinOn<Domain.Tables.TblDepartment>(d => d.Status == (int)StatusCommon.Active && d.AppID == (int)Menu_AppID.THN);
                var lstPositionUserHas = await _userDeparmentTab.QueryAsync<Domain.Models.EmployeeInfoModel>();
                // lấy vị trí cao nhất của user
                return lstPositionUserHas.OrderByDescending(x => x.PositionID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.LogDebug($"GetPositonUserIDLogin|{ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }
    }
}
