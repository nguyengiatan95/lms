﻿using LMS.Common.Helper;
using LMS.Entites.Dtos.CollectionServices.QueueCallAPI;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Services
{
    public interface IQueueCallAPIManager
    {
        Task<long> InsertQueueComment(long timaLoanID, string comment, int reasonID, long lmsCommentID, long userID, string username, DateTime createDate);

        Task<long> InsertProvisionsCollection(long timaLoanID, long userID, string username, DateTime createDate, long Money,
            int NumberAccruedPeriods, string TextAccruedPeriods, DateTime TimeScheduled, long LMSProvisionsCollectionID);
        Task<long> InsertConfirmSuspendedMoney(long invoiceTimaID, int userID, string createDate, List<string> pathImage,
            string note, long lmsConfirmID, long timaloanID, long timaCustomerID);
        Task<long> InsertHoldMoney(long agCustomerID, int userID, string createDate, long amount, string reason, long holdCustomerMoneyID);
        Task<long> UpdateHoldMoney(int userID, long amount, string reason, long holdCustomerMoneyID, int status, long agCustomerID);
    }
    public class QueueCallAPIManager : IQueueCallAPIManager
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> _queueCallAPITab;
        readonly Utils _common;
        readonly ILogger<QueueCallAPIManager> _logger;
        public QueueCallAPIManager(LMS.Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> queueCallAPITab,
          ILogger<QueueCallAPIManager> logger)
        {
            _queueCallAPITab = queueCallAPITab;
            _common = new Utils();
            _logger = logger;
        }

        public async Task<long> InsertConfirmSuspendedMoney
            (long invoiceTimaID, int userID, string createDate, List<string> pathImage,
            string note, long lmsConfirmID, long timaloanID, long timaCustomerID)
        {
            DateTime dtNow = DateTime.Now;
            var request = new LMSRequestConfirmSuspendedMoneyTHN()
            {
                InvoiceTimaID = invoiceTimaID,
                CreateDate = createDate,
                Note = note,
                UserID = userID,
                PathImage = pathImage,
                LoanID = timaloanID,
                LmsConfirmID = lmsConfirmID,
                CustomerID = timaCustomerID,
            };
            try
            {
                Domain.Tables.TblQueueCallAPI tblQueueCallAPI = new Domain.Tables.TblQueueCallAPI()
                {
                    CreateDate = dtNow,
                    Description = "Xác minh phiếu thu LMS sang AG",
                    Domain = LMS.Entites.Dtos.AG.AGConstants.BaseUrl,
                    JsonRequest = _common.ConvertObjectToJSonV2(request),
                    Method = "POST",
                    Path = LMS.Common.Constants.ActionApiExternal.THN_SaveConfirmSuspendedMoneyTHN,
                    Status = (int)LMS.Common.Constants.QueueCallAPI_Status.Waiting,
                    Retry = 0,
                };
                return await _queueCallAPITab.InsertAsync(tblQueueCallAPI);
            }
            catch (Exception ex)
            {
                _logger.LogError($"InsertConfirmSuspendedMoney|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                return 0;
            }
        }

        public async Task<long> InsertHoldMoney(long agCustomerID, int userID, string createDate, long amount, string reason, long holdCustomerMoneyID)
        {
            DateTime dtNow = DateTime.Now;
            var request = new
            {
                CustomerID = agCustomerID,
                CreateDate = createDate,
                Amount = amount,
                Reason = reason,
                CreateBy = userID,
                LMSHoldID = holdCustomerMoneyID
            };
            try
            {
                Domain.Tables.TblQueueCallAPI tblQueueCallAPI = new Domain.Tables.TblQueueCallAPI()
                {
                    CreateDate = dtNow,
                    Description = "Thêm hold tiền LMS sang AG",
                    Domain = LMS.Entites.Dtos.AG.AGConstants.BaseUrl,
                    JsonRequest = _common.ConvertObjectToJSonV2(request),
                    Method = "POST",
                    Path = LMS.Common.Constants.ActionApiExternal.THN_HoldCustomerMoney,
                    Status = (int)LMS.Common.Constants.QueueCallAPI_Status.Waiting,
                    Retry = 0
                };
                return await _queueCallAPITab.InsertAsync(tblQueueCallAPI);
            }
            catch (Exception ex)
            {
                _logger.LogError($"InsertHoldMoney|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                return 0;
            }
        }

        public async Task<long> InsertProvisionsCollection(long timaLoanID, long userID, string username, DateTime createDate, long Money,
            int NumberAccruedPeriods, string TextAccruedPeriods, DateTime TimeScheduled, long LMSProvisionsCollectionID)
        {
            DateTime dtNow = DateTime.Now;
            var request = new RequestSaveProvisionsCollectionTHN()
            {
                AGLoanID = timaLoanID,
                CreateDate = createDate.ToString("dd/MM/yyyy HH:mm"),
                TimeScheduled = TimeScheduled.ToString("dd/MM/yyyy HH:mm"),
                UserID = userID,
                Username = username,
                Money = Money,
                LMSID = LMSProvisionsCollectionID,
                NumberAccruedPeriods = NumberAccruedPeriods,
                TextAccruedPeriods = TextAccruedPeriods
            };
            try
            {
                Domain.Tables.TblQueueCallAPI tblQueueCallAPI = new Domain.Tables.TblQueueCallAPI()
                {
                    CreateDate = dtNow,
                    Description = "Dự thu LMS sang AG",
                    Domain = LMS.Entites.Dtos.AG.AGConstants.BaseUrl,
                    JsonRequest = _common.ConvertObjectToJSonV2(request),
                    Method = "POST",
                    Path = LMS.Common.Constants.ActionApiExternal.THN_SaveProvisionsCollection,
                    Status = (int)LMS.Common.Constants.QueueCallAPI_Status.Waiting,
                    Retry = 0,
                };
                return await _queueCallAPITab.InsertAsync(tblQueueCallAPI);
            }
            catch (Exception ex)
            {
                _logger.LogError($"InsertProvisionsCollection|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                return 0;
            }
        }

        public async Task<long> InsertQueueComment(long timaLoanID, string comment, int reasonID, long lmsCommentID, long userID, string username, DateTime createDate)
        {
            DateTime dtNow = DateTime.Now;
            RequestCallTHNComment request = new RequestCallTHNComment()
            {
                AGLoanID = timaLoanID,
                Comment = comment,
                CreateDate = createDate.ToString("dd/MM/yyyy HH:mm"),
                LMSCommentID = lmsCommentID,
                ReasonID = reasonID,
                UserID = userID,
                Username = username
            };
            try
            {
                Domain.Tables.TblQueueCallAPI tblQueueCallAPI = new Domain.Tables.TblQueueCallAPI()
                {
                    CreateDate = dtNow,
                    Description = "Comment LMS sang AG",
                    Domain = LMS.Entites.Dtos.AG.AGConstants.BaseUrl,
                    JsonRequest = _common.ConvertObjectToJSonV2(request),
                    Method = "POST",
                    Path = LMS.Common.Constants.ActionApiExternal.THN_SaveComment,
                    Status = (int)LMS.Common.Constants.QueueCallAPI_Status.Waiting,
                    Retry = 0,
                };
                return await _queueCallAPITab.InsertAsync(tblQueueCallAPI);
            }
            catch (Exception ex)
            {
                _logger.LogError($"InsertQueueComment|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                return 0;
            }
        }

        public async Task<long> UpdateHoldMoney(int userID, long amount, string reason, long holdCustomerMoneyID, int status, long agCustomerID)
        {
            DateTime dtNow = DateTime.Now;
            var request = new
            {
                Amount = amount,
                Reason = reason,
                CreateBy = userID,
                LMSHoldID = holdCustomerMoneyID,
                Status = status,
                CustomerID = agCustomerID
            };
            try
            {
                Domain.Tables.TblQueueCallAPI tblQueueCallAPI = new Domain.Tables.TblQueueCallAPI()
                {
                    CreateDate = dtNow,
                    Description = "cập nhật hold tiền LMS sang AG",
                    Domain = LMS.Entites.Dtos.AG.AGConstants.BaseUrl,
                    JsonRequest = _common.ConvertObjectToJSonV2(request),
                    Method = "POST",
                    Path = LMS.Common.Constants.ActionApiExternal.THN_UpdateHoldCustomerMoney,
                    Status = (int)LMS.Common.Constants.QueueCallAPI_Status.Waiting,
                    Retry = 0
                };
                return await _queueCallAPITab.InsertAsync(tblQueueCallAPI);
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateHoldMoney|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                return 0;
            }
        }
    }
}
