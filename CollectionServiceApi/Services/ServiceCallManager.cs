﻿using CollectionServiceApi.Domain.Models.MapUserCall;
using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using LMS.Common.Constants;
using LMS.Entites.Dtos.CollectionServices.MapUserCall;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CollectionServiceApi.Services
{
    public interface IServiceCallManager
    {
        Task<ResponseActionResult> GetLinkCall(long userID, string phone, string calloutId);
    }
    public class ServiceCallManager : IServiceCallManager
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblMapUserCall> _mapUserCallTab;
        readonly ILogger<ServiceCallManager> _logger;
        readonly PhoneSettingCareSoft _phoneSetting;
        readonly LMS.Common.Helper.Utils _common;
        public ServiceCallManager(LMS.Common.DAL.ITableHelper<Domain.Tables.TblMapUserCall> mapUserCallTab,
            PhoneSettingCareSoft phoneSetting,
            ILogger<ServiceCallManager> logger)
        {
            _mapUserCallTab = mapUserCallTab;
            _phoneSetting = phoneSetting;
            _common = new LMS.Common.Helper.Utils();
            _logger = logger;
        }
        public async Task<ResponseActionResult> GetLinkCall(long userID, string phone, string calloutId)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var userCallInfos = await _mapUserCallTab.WhereClause(x => x.UserID == userID && x.Status == (int)StatusCommon.Active).QueryAsync();
                if (userCallInfos == null || !userCallInfos.Any())
                {
                    response.Message = MessageConstant.UserNotConfigurationServiceCall;
                    return response;
                }
                var userCallDetail = userCallInfos.First();
                ServiceCallModel serviceCallInfo = new ServiceCallModel()
                {
                    TypeCall = userCallDetail.TypeCall
                };
                if (!string.IsNullOrEmpty(userCallDetail.JsonExtra))
                {
                    switch ((MapUserCall_TypeCall)userCallDetail.TypeCall)
                    {
                        case MapUserCall_TypeCall.Caresoft:
                            Domain.Tables.UserCallCareSoftExtra careSoftInfo = _common.ConvertJSonToObjectV2<Domain.Tables.UserCallCareSoftExtra>(userCallDetail.JsonExtra);
                            serviceCallInfo.CareSoftLinkCall = GenarateLinkClick2CallChoosePhone(careSoftInfo.IPPhone, phone, calloutId);
                            break;
                        case MapUserCall_TypeCall.Cisco:
                            Domain.Tables.UserCallCiscoExtra ciscoInfo = _common.ConvertJSonToObjectV2<Domain.Tables.UserCallCiscoExtra>(userCallDetail.JsonExtra);
                            serviceCallInfo.CiscoUserName = ciscoInfo.Username;
                            serviceCallInfo.CiscoPassword = ciscoInfo.Password;
                            serviceCallInfo.CiscoExtension = ciscoInfo.Extension;
                            break;
                    }
                    response.SetSucces();
                }
                response.Data = serviceCallInfo;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLinkCall|userID={userID}|phone={phone}|calloutId={calloutId}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        private string GenarateLinkClick2CallChoosePhone(string ipphone, string numercall, string calloutId)
        {
            string link;
            try
            {
                if (string.IsNullOrEmpty(numercall))
                    return "javascript:;";
                var domain = _phoneSetting.DomainAgent;
                if (!string.IsNullOrEmpty(_phoneSetting.PhoneTest))
                {
                    numercall = _phoneSetting.PhoneTest;
                }

                string strPhone = Regex.Replace(numercall, @"[^\d]", String.Empty);
                if (strPhone.Length > 2)
                {
                    numercall = strPhone.StartsWith("84") ? "0" + strPhone.Substring(2, strPhone.Length - 2) : strPhone;
                    numercall = "0" + Convert.ToInt64(numercall);
                }
                else
                    numercall = string.Empty;

                var token = GenarateCaresoftJWT(ipphone, calloutId, numercall);
                //link = "https://tongdai.tima.vn/" + domain + "/c2call?token=" + token;
                link = $"{_phoneSetting.Host}{domain}/c2call?token={token}";
            }
            catch (Exception ex)
            {
                _logger.LogError($"GenarateLinkClick2CallChoosePhone|ipphone={ipphone}|numberCall={numercall}|calloutId={calloutId}|ex={ex.Message}-{ex.StackTrace}");
                return string.Empty;
            }
            return link;
        }
        private string GenarateCaresoftJWT(string ipphone, string callout_id, string number)
        {
            var accesstoken = string.Empty;
            Dictionary<string, object> payload;
            if (string.IsNullOrWhiteSpace(callout_id))
            {
                payload = new Dictionary<string, object>
                {
                    { "ipphone", ipphone },
                    {"number", System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(number))}
                };
            }
            else
            {
                payload = new Dictionary<string, object>
                {
                    { "ipphone", ipphone },
                    { "callout_id", callout_id },
                    {"number", System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(number))}
                };
            }
            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);
            accesstoken = encoder.Encode(payload, _phoneSetting.AccessToken);
            return accesstoken;
        }
    }
}
