﻿using CollectionServiceApi.RestClients;
using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.Document;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Services
{
    public interface IExportDataDocsManager
    {
        Task<ResponseActionResult> PropertySeizure(RequestExportDataDocs request);
        Task<ResponseActionResult> DebtRestructuring(RequestExportDataDocs request);
        Task<ResponseActionResult> UnsecuredFinalDebtNotice(RequestExportDataDocs request);
        Task<ResponseActionResult> NotifyPropertySeizure(RequestExportDataDocs request);
        Task<ResponseActionResult> NotifyVoluntarilyHandingOverPropertye(RequestExportDataDocs request);
    }
    public class ExportDataDocsManager : IExportDataDocsManager
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanCloseLoan> _planCloseLoanTab;
        readonly IReportManager _reportManager;
        readonly ILOSService _losService;
        ILogger<ExportDataDocsManager> _logger;
        LMS.Common.Helper.Utils _common;
        public ExportDataDocsManager(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanCloseLoan> planCloseLoanTab,
            IReportManager reportManager,
            ILOSService losService,
            ILogger<ExportDataDocsManager> logger
        )
        {
            _loanTab = loanTab;
            _customerTab = customerTab;
            _planCloseLoanTab = planCloseLoanTab;
            _reportManager = reportManager;
            _losService = losService;
            _logger = logger;
            _common = new Utils();
        }
        public async Task<ResponseActionResult> PropertySeizure(RequestExportDataDocs request) //Quyết định thu giữ taì sản
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objExport = new PropertySeizureItem();
                var objLoan = (await _loanTab.SelectColumns(x => x.FromDate, x => x.ContactCode, x => x.CustomerName, x => x.LoanCreditIDOfPartner).WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                if (objLoan != null)
                {
                    var objLoanLos = await _losService.GetLoanCreditDetail((long)objLoan.LoanCreditIDOfPartner);
                    if (objLoanLos != null && objLoanLos.LoanBriefProperty != null)
                    {
                        objExport.Edit_Product = objLoanLos.LoanBriefProperty.Product;
                        objExport.Edit_PlateNumber = objLoanLos.LoanBriefProperty.PlateNumber;
                    }
                    objExport.EditCVTB = request.CVTB;
                    objExport.Edit_FromDate = objLoan.FromDate.ToString(TimaSettingConstant.DateTimeDayMonthYear);
                    objExport.Edit_CustomerName = objLoan.CustomerName;
                    objExport.Edit_NumberDayKeep = request.NumberDayKeep;
                    objExport.FileName = $"{objLoan.CustomerName}-{objLoan.ContactCode}";
                    response.Data = objExport;
                    response.SetSucces();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ExportDataDocsManager_PropertySeizure|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task<ResponseActionResult> DebtRestructuring(RequestExportDataDocs request) //Tái cấu trúc nợ
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objExport = new DebtRestructuringItem();
                DateTime toDay = DateTime.Now.Date;
                var firstDayOfMonthCurrent = new DateTime(toDay.Year, toDay.Month, 1);
                var endOfLastMonth = firstDayOfMonthCurrent.AddDays(-1);
                var firstDayOfLastMonth = new DateTime(endOfLastMonth.Year, endOfLastMonth.Month, 1);

                var objLoan = (await _loanTab.SelectColumns(x => x.FromDate,x=>x.ContactCode, x => x.CustomerID, x => x.CustomerName, x => x.LoanCreditIDOfPartner).WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                objExport.CustomerName = objLoan?.CustomerName;
                objExport.LoanCodeId = objLoan.ContactCode;
                objExport.TotalMoney = objLoan.TotalMoneyDisbursement.ToString(TimaSettingConstant.FormatMoney);
                objExport.FileName = $"{objLoan.CustomerName}-{objLoan.ContactCode}";
                if (objLoan != null)
                {
                    var lstLoanID = new List<long>
                    {
                        request.LoanID
                    };
                    var dictCutOffLoanEndOfMonthInfos = await _reportManager.GetDictCutOffLoanByLstLoanID(lstLoanID, endOfLastMonth);
                    if (dictCutOffLoanEndOfMonthInfos.Keys.Count > 0)
                    {
                        var loanEndOfLastMonthInfo = dictCutOffLoanEndOfMonthInfos.GetValueOrDefault(request.LoanID);
                        objExport.DebitGroup = _reportManager.GetTypeBebtLoanByNextDate(loanEndOfLastMonthInfo.NextDate, firstDayOfLastMonth);
                    }

                    var objCustomer = (await _customerTab.WhereClause(x => x.CustomerID == objLoan.CustomerID).QueryAsync()).FirstOrDefault();
                    if (objCustomer != null)
                    {
                        objExport.BirthDay = objCustomer.BirthDay == null ? "" : objCustomer.BirthDay.Value.ToString(TimaSettingConstant.DateTimeDayMonthYear);
                        objExport.NumberCard = objCustomer.NumberCard;
                    }

                    var planCloseLoan = (await _planCloseLoanTab.WhereClause(x => x.LoanID == request.LoanID && x.CloseDate == toDay).QueryAsync()).FirstOrDefault();
                    if (planCloseLoan != null)
                    {

                        var totalMoneyIntersetFee = planCloseLoan.MoneyInterest + planCloseLoan.MoneyService + planCloseLoan.MoneyConsultant;

                        objExport.TotalMoneyCurrent = planCloseLoan.MoneyOriginal.ToString(TimaSettingConstant.FormatMoney);// dư nợ gốc còn lại
                        objExport.TotalMoneyInterest = totalMoneyIntersetFee.ToString(TimaSettingConstant.FormatMoney);
                        objExport.TotalMoneyCloseLoan = planCloseLoan.TotalMoneyClose.ToString(TimaSettingConstant.FormatMoney);
                        objExport.DayPastDue = DateTime.Now.Date.Subtract(planCloseLoan.NextDate).Days;

                        objExport.TotalMoneyConsultant = planCloseLoan.MoneyConsultant.ToString(TimaSettingConstant.FormatMoney);
                        objExport.TotalMoneyFineLate = planCloseLoan.MoneyFineLate.ToString(TimaSettingConstant.FormatMoney);
                        objExport.TotalMoneyFineOriginal = planCloseLoan.MoneyFineLate.ToString(TimaSettingConstant.FormatMoney);
                    }
                }
                response.Data = objExport;
                response.SetSucces();

            }
            catch (Exception ex)
            {
                _logger.LogError($"ExportDataDocsManager_DebtRestructuring|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task<ResponseActionResult> UnsecuredFinalDebtNotice(RequestExportDataDocs request)//thông báo nợ lần cuối tín chấp
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objExport = new UnsecuredFinalDebtNoticeItem();
                var date = DateTime.ParseExact(request.DateCloseLoan, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture);
                var objLoan = (await _loanTab.SelectColumns(x => x.FromDate, x => x.ContactCode, x => x.CustomerID, x => x.CustomerName, x => x.LoanCreditIDOfPartner).WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                objExport.Edit_CustomerName = objLoan?.CustomerName;
                objExport.Edit_LoanCode = objLoan?.ContactCode;
                if (objLoan != null)
                {
                    var objCustomer = (await _customerTab.WhereClause(x => x.CustomerID == objLoan.CustomerID).QueryAsync()).FirstOrDefault();
                    if (objCustomer != null)
                    {
                        objExport.Edit_NumberCard = objCustomer.NumberCard;
                        objExport.Edit_Address = objCustomer.Address;
                    }
                    var planCloseLoan = (await _planCloseLoanTab.WhereClause(x => x.LoanID == request.LoanID && x.CloseDate == date).QueryAsync()).FirstOrDefault();
                    if (planCloseLoan != null)
                    {
                        var totalMoneyIntersetFee = planCloseLoan.MoneyInterest + planCloseLoan.MoneyService + planCloseLoan.MoneyConsultant;
                        objExport.Edit_TotalMoneyNeedCloseLoan = planCloseLoan.TotalMoneyClose.ToString(TimaSettingConstant.FormatMoney);
                        objExport.Edit_TotalMoneyIntersetFee = totalMoneyIntersetFee.ToString(TimaSettingConstant.FormatMoney);
                        objExport.Edit_TotalMoneyCurrent = planCloseLoan.MoneyOriginal.ToString(TimaSettingConstant.FormatMoney);// nợ gốc
                        objExport.Edit_DPD = DateTime.Now.Date.Subtract(planCloseLoan.NextDate).Days;
                    }
                }
                objExport.EditCVTB = request.CVTB;
                objExport.Edit_DateCloseLoan = request.DateCloseLoan;
                objExport.Edit_DateFinishCloseLoan = request.DateFinshCloseLoan;
                objExport.Edit_NameEmployees = request.NameEmployess;
                objExport.Edit_PhoneEmployees = request.PhoneEmployees;
                objExport.FileName = $"{objLoan.CustomerName}-{objLoan.ContactCode}";
                response.Data = objExport;
                response.SetSucces();

            }
            catch (Exception ex)
            {
                _logger.LogError($"ExportDataDocsManager_UnsecuredFinalDebtNotice|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task<ResponseActionResult> NotifyPropertySeizure(RequestExportDataDocs request) //Thông báo thu giữ tài sản
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objExport = new NotifyPropertySeizureItem();
                var objLoan = (await _loanTab.SelectColumns(x => x.FromDate, x => x.ContactCode, x => x.CustomerName, x => x.LoanCreditIDOfPartner).WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                if (objLoan != null)
                {
                    var objLoanLos = await _losService.GetLoanCreditDetail((long)objLoan.LoanCreditIDOfPartner);
                    if (objLoanLos != null)
                    {
                        objExport.Edit_NumberCard = objLoanLos.CardNumberShareholder;
                        objExport.Edit_Address = objLoanLos.Address;
                        objExport.Edit_Product = objLoanLos.LoanBriefProperty == null ? "" : objLoanLos.LoanBriefProperty.Product;
                        objExport.Edit_Engine = objLoanLos.LoanBriefProperty == null ? "" : objLoanLos.LoanBriefProperty.Engine;
                        objExport.Edit_Chassis = objLoanLos.LoanBriefProperty == null ? "" : objLoanLos.LoanBriefProperty.Chassis;
                    }
                    objExport.EditCVTB = request.CVTB;
                    objExport.Edit_CustomerName = objLoan.CustomerName;
                    objExport.Edit_FromDate = objLoan.FromDate.ToString(TimaSettingConstant.DateTimeDayMonthYear);
                    objExport.Edit_NameEmployees = request.NameEmployess;
                    objExport.Edit_PhoneEmployees = request.PhoneEmployees;
                    objExport.FileName = $"{objLoan.CustomerName}-{objLoan.ContactCode}";
                    response.Data = objExport;
                    response.SetSucces();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ExportDataDocsManager_NotifyPropertySeizure|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task<ResponseActionResult> NotifyVoluntarilyHandingOverPropertye(RequestExportDataDocs request) //Thông báo tự nguyện bàn giao tài sản
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objExport = new NotifyVoluntarilyHandingOverPropertyeItem();
                var objLoan = (await _loanTab.SelectColumns(x => x.FromDate, x => x.ContactCode, x => x.CustomerName, x => x.LoanCreditIDOfPartner).WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                if (objLoan != null)
                {
                    var objLoanLos = await _losService.GetLoanCreditDetail((long)objLoan.LoanCreditIDOfPartner);
                    if (objLoanLos != null && objLoanLos.LoanBriefProperty != null)
                    {
                        objExport.Edit_NumberCard = objLoanLos.CardNumberShareholder;
                        objExport.Edit_Address = objLoanLos.Address;
                    }

                    var date = DateTime.ParseExact(request.DateCloseLoan, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture);

                    var planCloseLoan = (await _planCloseLoanTab.WhereClause(x => x.LoanID == request.LoanID && x.CloseDate == date).QueryAsync()).FirstOrDefault();
                    if (planCloseLoan != null)
                    {
                        var totalMoneyIntersetFee = planCloseLoan.MoneyInterest + planCloseLoan.MoneyService + planCloseLoan.MoneyConsultant;
                        objExport.Edit_TotalMoneyNeedCloseLoan = planCloseLoan.TotalMoneyClose.ToString(TimaSettingConstant.FormatMoney);
                        objExport.Edit_TotalMoneyIntersetFee = totalMoneyIntersetFee.ToString(TimaSettingConstant.FormatMoney);
                        objExport.Edit_TotalMoneyCurrent = planCloseLoan.MoneyOriginal.ToString(TimaSettingConstant.FormatMoney);// nợ gốc
                        objExport.Edit_DPD = DateTime.Now.Date.Subtract(planCloseLoan.NextDate).Days;
                    }
                    objExport.EditCVTB = request.CVTB;
                    objExport.Edit_CustomerName = objLoan.CustomerName;
                    objExport.Edit_FromDate = objLoan.FromDate.ToString(TimaSettingConstant.DateTimeDayMonthYear);
                    objExport.Edit_NameEmployees = request.NameEmployess;
                    objExport.Edit_PhoneEmployees = request.PhoneEmployees;
                    objExport.Edit_DateCloseLoan = request.DateCloseLoan;
                    objExport.FileName = $"{objLoan.CustomerName}-{objLoan.ContactCode}";

                    response.Data = objExport;
                    response.SetSucces();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ExportDataDocsManager_NotifyVoluntarilyHandingOverPropertye|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
