﻿using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using LMS.Common.Helper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionServiceApi.Services
{
    public interface IReportManager
    {
        Task ExportExcelListLoanByEmployeeID(long employeeID, string traceIDRequest);

        Task<ResponseActionResult> ProcessSummaryReportEmployeeHandleLoan(SettingKeyReportEmployeeHandleLoan settingKeyValue);
        Task<Dictionary<long, Domain.Tables.TblCustomer>> GetDictCustomerByLstLoanID(List<long> lstLoanID);
        Task<Dictionary<long, Domain.Tables.TblLoan>> GetDictLoanInfosByLstLoanID(List<long> lstLoanID);
        Task<Dictionary<long, Domain.Tables.TblPlanCloseLoan>> GetDictPlanCloseLoanByLstLoanID(List<long> lstLoanID, DateTime currentDate);
        Task<Dictionary<long, Domain.Tables.TblCutOffLoan>> GetDictCutOffLoanByLstLoanID(List<long> lstLoanID, DateTime cutOffDate);
        Task<ResponseActionResult> ProcessReportLoanDebtType(SettingKeyReportLoanDebtType settingKeyValue);
        Task<Dictionary<long, Domain.Tables.TblCommentDebtPrompted>> GetDictLoanCommentLatestInfosByLstLoanID(List<long> lstLoanID, DateTime? fromDate = null, DateTime? toDate = null);

        string GetTypeBebtLoanByNextDate(DateTime nextDate, DateTime firstDayOfMonth);
    }
    public class ReportManager : IReportManager
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> _assignmentLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> _commentDebtPromptedTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionLoanTab;
        //readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        //readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblHoldCustomerMoney> _holdCustomerMoneyTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCutOffLoan> _cutOffLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> _userDepartmentTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCity> _cityTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanCloseLoan> _planCloseLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPreparationCollectionLoan> _preparationCollectionLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomerBank> _customerBankTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> _excelReportTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblReportEmployeeHandleLoan> _reportEmployeeHandleLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblTrackingLoanInMonth> _trackingLoanInMonthTab;


        readonly RestClients.ILoanService _loanService;
        readonly ILogger<ReportManager> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public ReportManager(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> assignmentLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> commentDebtPromptedTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionLoanTab,
            //LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            //LMS.Common.DAL.ITableHelper<Domain.Tables.TblHoldCustomerMoney> holdCustomerMoneyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCutOffLoan> cutOffLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDepartmentTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanCloseLoan> planCloseLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCity> cityTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPreparationCollectionLoan> preparationCollectionLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomerBank> customerBankTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> excelReportTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblReportEmployeeHandleLoan> reportEmployeeHandleLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTrackingLoanInMonth> trackingLoanInMonthTab,
            RestClients.ILoanService loanService,
            ILogger<ReportManager> logger)
        {
            _loanTab = loanTab;
            _customerTab = customerTab;
            _assignmentLoanTab = assignmentLoanTab;
            _commentDebtPromptedTab = commentDebtPromptedTab;
            _transactionLoanTab = transactionLoanTab;
            //_paymentScheduleTab = paymentScheduleTab;
            //_holdCustomerMoneyTab = holdCustomerMoneyTab;
            _cutOffLoanTab = cutOffLoanTab;
            _userTab = userTab;
            _userDepartmentTab = userDepartmentTab;
            _cityTab = cityTab;
            _preparationCollectionLoanTab = preparationCollectionLoanTab;
            _planCloseLoanTab = planCloseLoanTab;
            _customerBankTab = customerBankTab;
            _excelReportTab = excelReportTab;
            _invoiceTab = invoiceTab;
            _reportEmployeeHandleLoanTab = reportEmployeeHandleLoanTab;
            _trackingLoanInMonthTab = trackingLoanInMonthTab;
            _loanService = loanService;
            _logger = logger;
            _common = new Utils();
        }
        public async Task ExportExcelListLoanByEmployeeID(long employeeID, string traceIDRequest)
        {
            string dataExport = "";
            int statusExport = (int)LMS.Common.Constants.ExcelReport_Status.Success;
            try
            {
                List<LMS.Entites.Dtos.CollectionServices.ReportExcel.AssignLoanEmployeeDetail> lstDataExport = new List<LMS.Entites.Dtos.CollectionServices.ReportExcel.AssignLoanEmployeeDetail>();
                var currentDate = DateTime.Now;
                var firstDayOfMonthCurrent = new DateTime(currentDate.Year, currentDate.Month, 1);
                var dateAssigned = firstDayOfMonthCurrent.AddSeconds(-1);
                var endOfLastMonth = firstDayOfMonthCurrent.AddDays(-1);
                var firstDayOfLastMonth = new DateTime(endOfLastMonth.Year, endOfLastMonth.Month, 1);
                var endOfLastYear = new DateTime(currentDate.Year, 1, 1).AddDays(-1);
                // xuất file danh sách đầu tháng hiện tại
                // lấy danh sách loan dc gán cho nhân viên
                var lstLoanAssigned = await _assignmentLoanTab.SelectColumns(x => x.LoanID, x => x.UserID)
                                .WhereClause(x => x.DateAssigned > dateAssigned && x.Status == (int)AssignmentLoan_Status.Processing)
                                .GetDistinct()
                                .QueryAsync();
                Dictionary<long, long> dictEmployeeIDs = new Dictionary<long, long>();
                Dictionary<long, long> dictLoanIDs = new Dictionary<long, long>();
                Dictionary<long, List<long>> dictLoanAndEmployees = new Dictionary<long, List<long>>();
                // đơn vay đc gán cho nhân viên
                var dictLoanIDOfEmployeeInfos = lstLoanAssigned.Where(x => x.UserID == employeeID).ToDictionary(x => x.LoanID, x => x);
                foreach (var item in lstLoanAssigned)
                {
                    if (!dictLoanIDOfEmployeeInfos.ContainsKey(item.LoanID))
                    {
                        continue;
                    }
                    if (!dictEmployeeIDs.ContainsKey(item.UserID))
                    {
                        dictEmployeeIDs.Add(item.UserID, item.UserID);
                    }
                    // lấy nv cùng chung đơn xử lý
                    if (!dictLoanAndEmployees.ContainsKey(item.LoanID))
                    {
                        dictLoanAndEmployees.Add(item.LoanID, new List<long>());
                    }
                    dictLoanAndEmployees[item.LoanID].Add(item.UserID);
                }
                var lstLoanID = dictLoanAndEmployees.Keys.ToList();
                var lstUserIDEmployees = dictEmployeeIDs.Values.ToList();

                var dictEmployeeInfosTask = _userTab.SelectColumns(x => x.UserID, x => x.UserName).WhereClause(x => lstUserIDEmployees.Contains(x.UserID)).QueryAsync();
                var lstUserDepartmentTask = _userDepartmentTab.WhereClause(x => lstUserIDEmployees.Contains(x.UserID)).QueryAsync();
                var lstCityInfoTask = _cityTab.QueryAsync();
                //// lấy thông tin loan

                var dictCustomerInfosTask = GetDictCustomerByLstLoanID(lstLoanID);

                var dictLoanCommentLatestInfosTask = GetDictLoanCommentLatestInfosByLstLoanID(lstLoanID);
                var dictSumMoneyTypeLoanInfosTask = GetDictSumMoneyTypeLoanInfosByLstLoanID(lstLoanID);
                var dictPlanCloseLoanInfosTask = GetDictPlanCloseLoanByLstLoanID(lstLoanID, currentDate);
                var dictPreparationCollectionLoanInfosTask = GetDictPreparationCollectionLoanInfosByLstLoanID(lstLoanID);
                var dictLoanInfosTask = GetDictLoanInfosByLstLoanID(lstLoanID);
                var dictCustomerBankInfosTask = GetDictCustomerBankInfosByLstLoanID(lstLoanID);
                var dictCutOffLoanEndOfMonthInfosTask = GetDictCutOffLoanByLstLoanID(lstLoanID, endOfLastMonth);
                var dictCutOffLoanLastYearInfosTask = GetDictCutOffLoanByLstLoanID(lstLoanID, endOfLastYear);

                await Task.WhenAll(dictLoanCommentLatestInfosTask, dictSumMoneyTypeLoanInfosTask, dictPlanCloseLoanInfosTask, dictPreparationCollectionLoanInfosTask, dictLoanInfosTask);

                var dictLoanCommentLatestInfos = dictLoanCommentLatestInfosTask.Result;
                var dictSumMoneyTypeLoanInfos = dictSumMoneyTypeLoanInfosTask.Result;
                var dictPlanCloseLoanInfos = dictPlanCloseLoanInfosTask.Result;
                var dictPreparationCollectionLoanInfos = dictPreparationCollectionLoanInfosTask.Result;
                var dictLoanInfos = dictLoanInfosTask.Result;

                await Task.WhenAll(dictEmployeeInfosTask, lstUserDepartmentTask, lstCityInfoTask, dictCustomerInfosTask, dictCustomerBankInfosTask, dictCutOffLoanEndOfMonthInfosTask, dictCutOffLoanLastYearInfosTask);
                var lstUserDepartment = lstUserDepartmentTask.Result;
                var lstUserDepartmentInfos = lstUserDepartmentTask.Result;
                var dictEmployeeInfos = dictEmployeeInfosTask.Result.ToDictionary(x => x.UserID, x => x);
                var dictCityInfos = lstCityInfoTask.Result.ToDictionary(x => x.CityID, x => x);
                var dictCustomerInfos = dictCustomerInfosTask.Result;
                var dictCustomerBankInfos = dictCustomerBankInfosTask.Result;
                var dictCutOffLoanEndOfMonthInfos = dictCutOffLoanEndOfMonthInfosTask.Result;
                var dictCutOffLoanLastYearInfos = dictCutOffLoanLastYearInfosTask.Result;
                // lấy dữ liệu excel
                foreach (var item in dictLoanInfos.Values)
                {
                    Domain.Tables.LoanJsonExtra loanJsonExtra = _common.ConvertJSonToObjectV2<Domain.Tables.LoanJsonExtra>(item.JsonExtra);
                    var cityInfo = dictCityInfos.GetValueOrDefault(item.CityID);
                    var planCloseLoanInfo = dictPlanCloseLoanInfos.GetValueOrDefault(item.LoanID);
                    var preparationLoanInfo = dictPreparationCollectionLoanInfos.GetValueOrDefault(item.LoanID);
                    var customerInfo = dictCustomerInfos.GetValueOrDefault((long)item.CustomerID);
                    var customerBankInfo = dictCustomerBankInfos.GetValueOrDefault(item.LoanID);
                    var loanEndOfLastMonthInfo = dictCutOffLoanEndOfMonthInfos.GetValueOrDefault(item.LoanID);
                    try
                    {
                        LMS.Entites.Dtos.CollectionServices.ReportExcel.AssignLoanEmployeeDetail row = new LMS.Entites.Dtos.CollectionServices.ReportExcel.AssignLoanEmployeeDetail
                        {
                            LoanID = item.TimaLoanID,
                            LoanContactCode = item.ContactCode,
                            CustomerName = item.CustomerName,
                            CustomerDistrictName = loanJsonExtra.DistrictName,
                            CustomerCityName = loanJsonExtra.CityName,
                            DPDCurrent = DateTime.Now.Date.Subtract(item.NextDate.Value).Days,
                            POSCurrent = item.TotalMoneyCurrent,
                            BucketCurrent = GetTypeBebtLoanByNextDate(item.NextDate.Value, firstDayOfMonthCurrent),
                            LoanProductName = item.ProductName,
                            LoanDomainName = ((City_DomainID)(cityInfo?.DomainID ?? 0)).GetDescription(),
                            LoanFromDate = $"{item.FromDate:dd/MM/yyyy}",
                            LoanToDate = $"{item.ToDate:dd/MM/yyyy}",
                            LoanFinishedDate = $"{item.FinishDate?.ToString("dd/MM/yyyy") ?? ""}",
                            LoanTotalMoneyCurrent = item.TotalMoneyDisbursement,
                            LoanRateTypeName = ((PaymentSchedule_ActionRateType)item.RateType).GetDescription(),
                            LoanNextDate = $"{item.NextDate.Value:dd/MM/yyyy}",
                            LoanFequency = item.Frequency,
                            LoanInterestClosed = planCloseLoanInfo?.MoneyInterest ?? 0,
                            LoanConsultantFeeClosed = planCloseLoanInfo?.MoneyConsultant ?? 0,
                            LoanMoneyFineLateClosed = planCloseLoanInfo?.MoneyFineLate ?? 0,
                            LoanMoneyFineOriginalClosed = planCloseLoanInfo?.MoneyFineOriginal ?? 0,
                            LoanTotalMoneyOriginalReceipt = dictSumMoneyTypeLoanInfos.GetValueOrDefault($"{item.LoanID}_{(int)Transaction_TypeMoney.Original}")?.TotalMoney ?? 0,
                            LoanTotalMoneyInterestReceipt = dictSumMoneyTypeLoanInfos.GetValueOrDefault($"{item.LoanID}_{(int)Transaction_TypeMoney.Interest}")?.TotalMoney ?? 0,
                            LoanTotalMoneyConsultantReceipt = 0,
                            LoanShopConsultantName = loanJsonExtra.ConsultantShopName,
                            LoanTopup = loanJsonExtra.TopUpOfLoanBriefID > 0 ? 1 : 0,
                            LoanIsLocate = loanJsonExtra.IsLocate,
                            LoanBKS = loanJsonExtra.LoanBriefPropertyPlateNumber,
                            LoanPreparationDate = $"{preparationLoanInfo?.PreparationDate:dd/MM/yyyy}",
                            LoanPreparationMoney = preparationLoanInfo?.TotalMoney ?? 0,
                            CustomerAddress = customerInfo?.Address,
                            CustomerBirthDate = $"{customerInfo?.BirthDay?.ToString("dd/MM/yyyy") ?? ""}",
                            CustomerTotalMoney = customerInfo?.TotalMoney ?? 0,
                            CustomerPayTypeName = ((Customer_PayTypeID)customerInfo?.PayTypeID).GetDescription(),
                            CustomerEpayAccountName = customerBankInfo?.AccountNo,
                            LoanTotalMoneyReceivables = planCloseLoanInfo?.TotalMoneyReceivables ?? 0,
                            LenderCode = loanJsonExtra.ShopName,
                            CustomerID = item.CustomerID ?? 0

                        };
                        row.LoanTotalMoneyConsultantReceipt += dictSumMoneyTypeLoanInfos.GetValueOrDefault($"{item.LoanID}_{(int)Transaction_TypeMoney.Consultant}")?.TotalMoney ?? 0;
                        row.LoanTotalMoneyConsultantReceipt += dictSumMoneyTypeLoanInfos.GetValueOrDefault($"{item.LoanID}_{(int)Transaction_TypeMoney.Service}")?.TotalMoney ?? 0;
                        row.LoanTotalMoneyReceipt = row.LoanTotalMoneyOriginalReceipt + row.LoanTotalMoneyInterestReceipt + row.LoanTotalMoneyConsultantReceipt;

                        var lstEmloyeesID = dictLoanAndEmployees.GetValueOrDefault(item.LoanID);
                        if (lstEmloyeesID != null)
                        {
                            lstEmloyeesID = lstEmloyeesID.Distinct().ToList();
                            foreach (var emID in lstEmloyeesID)
                            {
                                var employeeInfo = dictEmployeeInfos.GetValueOrDefault(emID);
                                if (employeeInfo != null)
                                {
                                    var staffInfo = lstUserDepartmentInfos.Where(x => x.UserID == emID).FirstOrDefault();
                                    if (staffInfo != null)
                                    {
                                        switch ((LMS.Common.Constants.UserDepartment_PositionID)staffInfo.PositionID)
                                        {
                                            case UserDepartment_PositionID.Staff:
                                                if (string.IsNullOrEmpty(row.AssgineeNameFirst))
                                                {
                                                    row.AssgineeNameFirst = employeeInfo.UserName;
                                                }
                                                else
                                                {
                                                    row.AssigneeNameSecond = employeeInfo.UserName;
                                                }
                                                break;
                                            case UserDepartment_PositionID.TeamLead:
                                                row.AssigneeNameLeader = employeeInfo.UserName;
                                                break;
                                            case UserDepartment_PositionID.Manager:
                                                row.AssigneeNameDepartment = employeeInfo.UserName;
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                        var commentDetail = dictLoanCommentLatestInfos.GetValueOrDefault(item.LoanID);
                        if (commentDetail != null)
                        {
                            var employeeInfo = dictEmployeeInfos.GetValueOrDefault(commentDetail.UserID);
                            row.AssigneeNameLatestComment = employeeInfo?.UserName;
                            row.CommentContent = commentDetail.Comment;
                            row.CommentReasonCode = commentDetail.ReasonCode;
                            row.LoanLatestComment = commentDetail.CreateDate.ToString("dd/MM/yyyy HH:mm");
                        }
                        // những đơn đang vay
                        if (RuleCollectionServiceHelper.LstStatusLoanLending.Contains((int)item.Status))
                        {
                            if (item.SourcecBuyInsurance > (int)Loan_SourcecBuyInsurance.NO && item.ToDate.Date.AddDays(TimaSettingConstant.MaxDaySentInsurance) <= currentDate.Date)
                            {
                                row.LoanInsurancePartnerName = ((Loan_SourcecBuyInsurance)item.SourcecBuyInsurance).GetDescription();
                                row.LoanInsuranceStatusName = ((Loan_StatusSendInsurance)item.StatusSendInsurance).GetDescription();
                            }
                        }
                        else
                        {
                            if (item.Status != (int)Loan_Status.Close)
                            {
                                row.LoanInsurancePartnerName = ((Loan_SourcecBuyInsurance)item.SourcecBuyInsurance).GetDescription();
                                row.LoanInsuranceStatusName = ((Loan_StatusSendInsurance)item.StatusSendInsurance).GetDescription();
                            }
                        }

                        // Năm nợ xấu là đơn vay đó ở thời điểm cuối ngày 31/12 của năm đó có rơi vào nhóm nợ B3 có DPD > 90 ngày hay không ?
                        // Nếu rơi vào là nợ xấu ở ngày End Of Year thì đánh dấu lưu lại,
                        // nếu đơn đó không phải là nợ xấu của các năm trước nhưng ở thời điểm hiện tại là nợ xấu thì tạm thời coi năm nay là năm nợ xấu của đơn vay dods
                        int badDebtYear = currentDate.Year;
                        if (item.YearBadDebt.HasValue && item.YearBadDebt > 0)
                        {
                            badDebtYear = item.YearBadDebt.Value;
                        }
                        var dpdCheckBadDebtYear = row.DPDCurrent;
                        var loanInfoCutOffLastYear = dictCutOffLoanLastYearInfos.GetValueOrDefault(item.LoanID);
                        if (loanInfoCutOffLastYear != null)
                        {
                            dpdCheckBadDebtYear = endOfLastYear.Subtract(loanInfoCutOffLastYear.NextDate).Days;
                            badDebtYear = endOfLastYear.Year;
                        }

                        row.LoanBadDebtYear = badDebtYear;
                        //if (dpdCheckBadDebtYear > TimaSettingConstant.MaxDaySentInsurance)
                        //{
                        //    if (item.FromDate.Year == currentDate.Year)
                        //    {
                        //        badDebtYear = currentDate.Year;
                        //    }
                        //    row.LoanBadDebtYear = badDebtYear;
                        //}

                        // dữ liệu tháng trước
                        if (loanEndOfLastMonthInfo != null && loanEndOfLastMonthInfo.LoanID > 0)
                        {
                            row.DPDBOM = endOfLastMonth.Subtract(loanEndOfLastMonthInfo.NextDate).Days;
                            row.POSBOM = loanEndOfLastMonthInfo.TotalMoneyCurrent;
                            row.BucketBOM = GetTypeBebtLoanByNextDate(loanEndOfLastMonthInfo.NextDate, firstDayOfLastMonth);
                        }
                        lstDataExport.Add(row);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"ExportExcelListLoanByEmployeeID_Process_Error_Item|employeeID={employeeID}|TraceRequestID={traceIDRequest}|item={_common.ConvertObjectToJSonV2(item)}|ex={ex.Message}-{ex.StackTrace}");
                    }
                }
                dataExport = _common.ConvertObjectToJSonV2(lstDataExport);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ExportExcelListLoanByEmployeeID_Process_Error|employeeID={employeeID}|TraceRequestID={traceIDRequest}|ex={ex.Message}-{ex.StackTrace}");
                statusExport = (int)LMS.Common.Constants.ExcelReport_Status.Recall;
            }

            try
            {
                var detailExport = (await _excelReportTab.WhereClause(x => x.TraceIDRequest == traceIDRequest).QueryAsync()).FirstOrDefault();
                if (detailExport != null)
                {
                    detailExport.ExtraData = dataExport;
                    detailExport.Status = statusExport;
                    detailExport.ModifyDate = DateTime.Now;
                    _ = _excelReportTab.UpdateAsync(detailExport);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ExportExcelListLoanByEmployeeID_Update_Error|employeeID={employeeID}|TraceRequestID={traceIDRequest}|ex={ex.Message}-{ex.StackTrace}");
            }

        }
        public  string GetTypeBebtLoanByNextDate(DateTime nextDate, DateTime firstDayOfMonth)
        {
            return RuleCollectionServiceHelper.GetTypeBebtLoanByNextDate_v2(nextDate, firstDayOfMonth).GetDescription();
            //var dpdCurrent = DateTime.Now.Date.Subtract(nextDate).Days;
            //// có 15 nhóm từ B0 - > B13+
            //// bước nhảy 30 ngày
            //if (dpdCurrent < 1)
            //{
            //    return "B0";
            //}
            //int i = dpdCurrent / 30;
            //if (i > 13)
            //{
            //    return "B13+";
            //}
            //int j = dpdCurrent % 30;
            //if (j == 0)
            //{
            //    return $"B{i}";
            //}
            //if (i + 1 > 13)
            //{
            //    return "B13+";
            //}
            //return $"B{i + 1}";
        }

        public async Task<Dictionary<long, Domain.Tables.TblCustomer>> GetDictCustomerByLstLoanID(List<long> lstLoanID)
        {
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                long minLoanID = long.MaxValue;
                long maxLoanID = 0;
                if (lstLoanID.Count > TimaSettingConstant.MaxItemQuery)
                {
                    foreach (var id in lstLoanID)
                    {
                        if (minLoanID > id)
                        {
                            minLoanID = id;
                        }
                        if (maxLoanID < id)
                        {
                            maxLoanID = id;
                        }
                    }
                    minLoanID -= 1;
                    maxLoanID += 1;
                }
                if (maxLoanID != 0)
                {
                    _customerTab.JoinOn<Domain.Tables.TblLoan>(c => c.CustomerID, l => l.CustomerID).WhereClauseJoinOn<Domain.Tables.TblLoan>(l => l.LoanID > minLoanID && l.LoanID < maxLoanID);
                }
                else
                {
                    _customerTab.JoinOn<Domain.Tables.TblLoan>(c => c.CustomerID, l => l.CustomerID).WhereClauseJoinOn<Domain.Tables.TblLoan>(l => lstLoanID.Contains(l.LoanID));
                }
                var data = (await _customerTab.GetDistinct().QueryAsync()).ToDictionary(x => x.CustomerID, x => x);
                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                _logger.LogInformation($"GetDictCustomerByLstLoanID|TimeExeculted={elapsedMs}|LstLoanID={string.Join(",", lstLoanID)}");
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetDictCustomerByLstLoanID|LstLoanID={string.Join(",", lstLoanID)}|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }

        public async Task<Dictionary<long, Domain.Tables.TblCommentDebtPrompted>> GetDictLoanCommentLatestInfosByLstLoanID(List<long> lstLoanID, DateTime? fromDate = null, DateTime? toDate = null)
        {
            Dictionary<long, Domain.Tables.TblCommentDebtPrompted> result = new Dictionary<long, Domain.Tables.TblCommentDebtPrompted>();
            try
            {
                long minLoanID = long.MaxValue;
                long maxLoanID = 0;
                if (lstLoanID.Count > TimaSettingConstant.MaxItemQuery)
                {
                    foreach (var id in lstLoanID)
                    {
                        if (minLoanID > id)
                        {
                            minLoanID = id;
                        }
                        if (maxLoanID < id)
                        {
                            maxLoanID = id;
                        }
                    }
                    minLoanID -= 1;
                    maxLoanID += 1;
                }
                var dynamicParameter = new Dapper.DynamicParameters();
                var sqlGetTopCommentByLoanIDs = ";WITH cteComment AS ( SELECT *, ROW_NUMBER() OVER(PARTITION BY LoanID ORDER BY CreateDate DESC) AS rn FROM TblCommentDebtPrompted (NOLOCK) WHERE ";
                if (maxLoanID != 0)
                {
                    sqlGetTopCommentByLoanIDs += $"  LoanID > {minLoanID} AND LoanID < {maxLoanID} ";
                }
                else
                {
                    sqlGetTopCommentByLoanIDs += $"  LoanID IN @LstLoanID ";
                    dynamicParameter.Add("@LstLoanID", lstLoanID);
                }
                sqlGetTopCommentByLoanIDs += " AND UserID != @UserIDAdmin";
                dynamicParameter.Add("@UserIDAdmin", TimaSettingConstant.UserIDAdmin);
                if (fromDate.HasValue)
                {
                    sqlGetTopCommentByLoanIDs += $" AND CreateDate > @FromDate ";
                    dynamicParameter.Add("@FromDate", fromDate);
                }
                if (toDate.HasValue)
                {
                    sqlGetTopCommentByLoanIDs += $" AND CreateDate < @ToDate ";
                    dynamicParameter.Add("@ToDate", toDate);
                }
                sqlGetTopCommentByLoanIDs += " ) SELECT * FROM cteComment WHERE rn = 1";
                var dictCommentLatest = (await _commentDebtPromptedTab.QueryAsync(sqlGetTopCommentByLoanIDs, dynamicParameter)).ToDictionary(x => x.LoanID, x => x);
                if (maxLoanID != 0)
                {
                    foreach (var loanID in lstLoanID)
                    {
                        var commentInfo = dictCommentLatest.GetValueOrDefault(loanID);
                        if (!result.ContainsKey(loanID) && commentInfo != null && commentInfo.LoanID > 0)
                        {
                            result.Add(loanID, commentInfo);
                        }
                    }
                    return result;
                }
                else
                {
                    result = dictCommentLatest;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetDictLoanCommentLatestInfosByLstLoanID|LstLoanID={string.Join(",", lstLoanID)}|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
            return result;
        }

        private async Task<Dictionary<string, Domain.Tables.TblTransaction>> GetDictSumMoneyTypeLoanInfosByLstLoanID(List<long> lstLoanID)
        {
            Dictionary<string, Domain.Tables.TblTransaction> dictSumMoneyTypeLoanInfos = new Dictionary<string, Domain.Tables.TblTransaction>();
            try
            {
                List<int> lstMoneyTypeTxn = new List<int>
            {
                (int)Transaction_TypeMoney.Original,
                (int)Transaction_TypeMoney.Interest,
                (int)Transaction_TypeMoney.Service,
                (int)Transaction_TypeMoney.Consultant,
                (int)Transaction_TypeMoney.FineLate,
            };
                long minLoanID = long.MaxValue;
                long maxLoanID = 0;
                if (lstLoanID.Count > TimaSettingConstant.MaxItemQuery)
                {
                    foreach (var id in lstLoanID)
                    {
                        if (minLoanID > id)
                        {
                            minLoanID = id;
                        }
                        if (maxLoanID < id)
                        {
                            maxLoanID = id;
                        }
                    }
                    minLoanID -= 1;
                    maxLoanID += 1;
                }
                var dynamicParameter = new Dapper.DynamicParameters();
                var sqlSumTxnByLoanIDs = $"SELECT LoanID, MoneyType, SUM(totalMoney) TotalMoney FROM TblTransaction(NOLOCK)  WHERE ActionID != {(int)Transaction_Action.ChoVay}";

                if (maxLoanID != 0)
                {
                    sqlSumTxnByLoanIDs += $" AND LoanID > {minLoanID} AND LoanID < {maxLoanID} ";
                }
                else
                {
                    sqlSumTxnByLoanIDs += $" AND  LoanID IN @LstLoanID  ";
                    dynamicParameter.Add("@LstLoanID", lstLoanID);
                }
                sqlSumTxnByLoanIDs += " GROUP BY LoanID, MoneyType";
                var dictAllSumTxnLoanInfos = (await _transactionLoanTab.QueryAsync(sqlSumTxnByLoanIDs, dynamicParameter)).ToDictionary(x => $"{x.LoanID}_{x.MoneyType}", x => x);
                if (maxLoanID != 0)
                {
                    foreach (var loanID in lstLoanID)
                    {
                        foreach (var moneyType in lstMoneyTypeTxn)
                        {
                            var keyDict = $"{loanID}_{moneyType}";
                            var sumMoneyTypeInfo = dictAllSumTxnLoanInfos.GetValueOrDefault(keyDict);
                            if (!dictSumMoneyTypeLoanInfos.ContainsKey(keyDict) && sumMoneyTypeInfo != null && sumMoneyTypeInfo.LoanID > 0)
                            {
                                dictSumMoneyTypeLoanInfos.Add(keyDict, sumMoneyTypeInfo);
                            }
                        }
                    }
                }
                else
                {
                    dictSumMoneyTypeLoanInfos = dictAllSumTxnLoanInfos;
                }
                return dictAllSumTxnLoanInfos;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetDictSumMoneyTypeLoanInfosByLstLoanID|LstLoanID={string.Join(",", lstLoanID)}|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }

        public async Task<Dictionary<long, Domain.Tables.TblPlanCloseLoan>> GetDictPlanCloseLoanByLstLoanID(List<long> lstLoanID, DateTime currentDate)
        {
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                Dictionary<long, Domain.Tables.TblPlanCloseLoan> dictPlanCloseLoanInfos = new Dictionary<long, Domain.Tables.TblPlanCloseLoan>();
                long minLoanID = long.MaxValue;
                long maxLoanID = 0;
                if (lstLoanID.Count > TimaSettingConstant.MaxItemQuery)
                {
                    foreach (var id in lstLoanID)
                    {
                        if (minLoanID > id)
                        {
                            minLoanID = id;
                        }
                        if (maxLoanID < id)
                        {
                            maxLoanID = id;
                        }
                    }
                    minLoanID -= 1;
                    maxLoanID += 1;
                }
                if (maxLoanID != 0)
                {
                    _planCloseLoanTab.WhereClause(x => x.LoanID > minLoanID && x.LoanID < maxLoanID && x.CloseDate == currentDate.Date);
                }
                else
                {
                    _planCloseLoanTab.WhereClause(x => lstLoanID.Contains(x.LoanID) && x.CloseDate == currentDate.Date);
                }
                var dictAllPlantCloseLoanInfos = (await _planCloseLoanTab.QueryAsync()).ToDictionary(x => x.LoanID, x => x);
                if (maxLoanID != 0)
                {
                    foreach (var loanID in lstLoanID)
                    {
                        var planCloseLoanInfo = dictAllPlantCloseLoanInfos.GetValueOrDefault(loanID);
                        if (!dictPlanCloseLoanInfos.ContainsKey(loanID) && planCloseLoanInfo != null && planCloseLoanInfo.LoanID > 0)
                        {
                            dictPlanCloseLoanInfos.Add(loanID, planCloseLoanInfo);
                        }
                    }
                }
                else
                {
                    dictPlanCloseLoanInfos = dictAllPlantCloseLoanInfos;
                }
                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                _logger.LogInformation($"GetDictPlanCloseLoanByLstLoanID|TimeExeculted={elapsedMs}|LstLoanID={string.Join(",", lstLoanID)}");
                return dictPlanCloseLoanInfos;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetDictPlanCloseLoanByLstLoanID|LstLoanID={string.Join(",", lstLoanID)}|currentdate={currentDate}|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }

        private async Task<Dictionary<long, Domain.Tables.TblPreparationCollectionLoan>> GetDictPreparationCollectionLoanInfosByLstLoanID(List<long> lstLoanID)
        {
            try
            {
                Dictionary<long, Domain.Tables.TblPreparationCollectionLoan> dictPreparationCollectionLoanInfos = new Dictionary<long, Domain.Tables.TblPreparationCollectionLoan>();
                long minLoanID = long.MaxValue;
                long maxLoanID = 0;
                if (lstLoanID.Count > TimaSettingConstant.MaxItemQuery)
                {
                    foreach (var id in lstLoanID)
                    {
                        if (minLoanID > id)
                        {
                            minLoanID = id;
                        }
                        if (maxLoanID < id)
                        {
                            maxLoanID = id;
                        }
                    }
                    minLoanID -= 1;
                    maxLoanID += 1;
                }
                var dynamicParameter = new Dapper.DynamicParameters();
                var sqlGetTopPreparationCollectionLoanByLoanIDs = ";WITH ctePre AS ( SELECT *,ROW_NUMBER() OVER (PARTITION BY LoanID ORDER BY PreparationDate DESC) AS rn   FROM TblPreparationCollectionLoan (NOLOCK))";


                if (maxLoanID != 0)
                {
                    sqlGetTopPreparationCollectionLoanByLoanIDs += $"  SELECT * FROM ctePre WHERE LoanID > {minLoanID} AND LoanID < {maxLoanID}  AND rn = 1";
                }
                else
                {
                    sqlGetTopPreparationCollectionLoanByLoanIDs += $" SELECT * FROM ctePre WHERE LoanID IN @LstLoanID  AND rn = 1";
                    dynamicParameter.Add("@LstLoanID", lstLoanID);
                }
                var dictAllPreparationCollectionLoanInfos = (await _preparationCollectionLoanTab.QueryAsync(sqlGetTopPreparationCollectionLoanByLoanIDs, dynamicParameter))
                                                                                                .ToDictionary(x => (long)x.LoanID, x => x);
                if (maxLoanID != 0)
                {
                    foreach (var loanID in lstLoanID)
                    {
                        var preparationLoanInfo = dictAllPreparationCollectionLoanInfos.GetValueOrDefault(loanID);
                        if (!dictPreparationCollectionLoanInfos.ContainsKey(loanID) && preparationLoanInfo != null && preparationLoanInfo.LoanID > 0)
                        {
                            dictPreparationCollectionLoanInfos.Add(loanID, preparationLoanInfo);
                        }
                    }
                }
                else
                {
                    dictPreparationCollectionLoanInfos = dictAllPreparationCollectionLoanInfos;
                }
                return dictPreparationCollectionLoanInfos;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetDictPreparationCollectionLoanInfosByLstLoanID|LstLoanID={string.Join(",", lstLoanID)}|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }

        public async Task<Dictionary<long, Domain.Tables.TblLoan>> GetDictLoanInfosByLstLoanID(List<long> lstLoanID)
        {
            try
            {
                Dictionary<long, Domain.Tables.TblLoan> dictLoanInfos = new Dictionary<long, Domain.Tables.TblLoan>();
                long minLoanID = long.MaxValue;
                long maxLoanID = 0;
                if (lstLoanID.Count > TimaSettingConstant.MaxItemQuery)
                {
                    foreach (var id in lstLoanID)
                    {
                        if (minLoanID > id)
                        {
                            minLoanID = id;
                        }
                        if (maxLoanID < id)
                        {
                            maxLoanID = id;
                        }
                    }
                    minLoanID -= 1;
                    maxLoanID += 1;
                }
                if (maxLoanID != 0)
                {
                    _loanTab.WhereClause(x => x.LoanID > minLoanID && x.LoanID < maxLoanID);
                }
                else
                {
                    _loanTab.WhereClause(x => lstLoanID.Contains(x.LoanID));
                }
                var dictAllLoanInfos = (await _loanTab.QueryAsync()).ToDictionary(x => x.LoanID, x => x);

                if (maxLoanID != 0)
                {
                    foreach (var loanID in lstLoanID)
                    {
                        var loanInfo = dictAllLoanInfos.GetValueOrDefault(loanID);
                        if (!dictLoanInfos.ContainsKey(loanID) && loanInfo != null && loanInfo.LoanID > 0)
                        {
                            dictLoanInfos.Add(loanID, loanInfo);
                        }
                    }
                }
                else
                {
                    dictLoanInfos = dictAllLoanInfos;
                }
                return dictLoanInfos;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetDictLoanInfosByLstLoanID|LstLoanID={string.Join(",", lstLoanID)}|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }

        private async Task<Dictionary<long, Domain.Tables.TblCustomerBank>> GetDictCustomerBankInfosByLstLoanID(List<long> lstLoanID)
        {
            try
            {
                Dictionary<long, Domain.Tables.TblCustomerBank> dictCustomerBankInfos = new Dictionary<long, Domain.Tables.TblCustomerBank>();
                long minLoanID = long.MaxValue;
                long maxLoanID = 0;
                if (lstLoanID.Count > TimaSettingConstant.MaxItemQuery)
                {
                    foreach (var id in lstLoanID)
                    {
                        if (minLoanID > id)
                        {
                            minLoanID = id;
                        }
                        if (maxLoanID < id)
                        {
                            maxLoanID = id;
                        }
                    }
                    minLoanID -= 1;
                    maxLoanID += 1;
                }
                if (maxLoanID != 0)
                {
                    _customerBankTab.WhereClause(x => x.LoanID > minLoanID && x.LoanID < maxLoanID);
                }
                else
                {
                    _customerBankTab.WhereClause(x => lstLoanID.Contains(x.LoanID));
                }
                var dictAllCustomerBankInfos = (await _customerBankTab.WhereClause(x => x.StatusVa == (int)StatusCommon.Active).QueryAsync()).ToDictionary(x => x.LoanID, x => x);

                if (maxLoanID != 0)
                {
                    foreach (var loanID in lstLoanID)
                    {
                        var loanInfo = dictAllCustomerBankInfos.GetValueOrDefault(loanID);
                        if (!dictCustomerBankInfos.ContainsKey(loanID) && loanInfo != null && loanInfo.LoanID > 0)
                        {
                            dictCustomerBankInfos.Add(loanID, loanInfo);
                        }
                    }
                }
                else
                {
                    dictCustomerBankInfos = dictAllCustomerBankInfos;
                }
                return dictCustomerBankInfos;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetDictCustomerBankInfosByLstLoanID|LstLoanID={string.Join(",", lstLoanID)}|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }

        public async Task<Dictionary<long, Domain.Tables.TblCutOffLoan>> GetDictCutOffLoanByLstLoanID(List<long> lstLoanID, DateTime cutOffDate)
        {
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                Dictionary<long, Domain.Tables.TblCutOffLoan> dictCutOfLoanInfos = new Dictionary<long, Domain.Tables.TblCutOffLoan>();
                long minLoanID = long.MaxValue;
                long maxLoanID = 0;
                if (lstLoanID.Count > TimaSettingConstant.MaxItemQuery)
                {
                    foreach (var id in lstLoanID)
                    {
                        if (minLoanID > id)
                        {
                            minLoanID = id;
                        }
                        if (maxLoanID < id)
                        {
                            maxLoanID = id;
                        }
                    }
                    minLoanID -= 1;
                    maxLoanID += 1;
                }
                var dynamicParameter = new Dapper.DynamicParameters();
                var sqlGetCutOffLoanLatestByLoanIDs = ";WITH cteCutOffLoan AS ( SELECT *, ROW_NUMBER() OVER(PARTITION BY LoanID ORDER BY CutOffDate DESC) AS rn  FROM TblCutOffLoan (NOLOCK) ";
                sqlGetCutOffLoanLatestByLoanIDs += "WHERE CutOffDate <= @CutOffDate ) ";

                dynamicParameter.Add("@CutOffDate", cutOffDate.Date);
                if (maxLoanID != 0)
                {
                    sqlGetCutOffLoanLatestByLoanIDs += $"  SELECT * FROM cteCutOffLoan WHERE LoanID > {minLoanID} AND LoanID < {maxLoanID}  AND rn = 1 ";
                }
                else
                {
                    sqlGetCutOffLoanLatestByLoanIDs += $"  SELECT cte.LoanID, cte.NextDate, cte.TotalMoneyCurrent, cte.LastDateOfPay, cte.CutOffDate FROM cteCutOffLoan cte(nolock)" +
                        $" WHERE LoanID IN @LstLoanID AND rn = 1 ";
                    dynamicParameter.Add("@LstLoanID", lstLoanID);
                }
                var dictAllCutOfLoanInfos = (await _cutOffLoanTab.QueryAsync(sqlGetCutOffLoanLatestByLoanIDs, dynamicParameter)).ToDictionary(x => x.LoanID, x => x);

                if (maxLoanID != 0)
                {
                    foreach (var loanID in lstLoanID)
                    {
                        var loanInfo = dictAllCutOfLoanInfos.GetValueOrDefault(loanID);
                        if (!dictCutOfLoanInfos.ContainsKey(loanID) && loanInfo != null && loanInfo.LoanID > 0)
                        {
                            dictCutOfLoanInfos.Add(loanID, loanInfo);
                        }
                    }
                }
                else
                {
                    dictCutOfLoanInfos = dictAllCutOfLoanInfos;
                }
                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                _logger.LogInformation($"GetDictCutOffLoanByLstLoanID|TimeExeculted={elapsedMs}|LstLoanID={string.Join(",", lstLoanID)}");
                return dictCutOfLoanInfos;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetDictCutOffLoanByLstLoanID|LstLoanID={string.Join(",", lstLoanID)}|cutOffDate={cutOffDate}|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }

        public async Task<ResponseActionResult> ProcessSummaryReportEmployeeHandleLoan(SettingKeyReportEmployeeHandleLoan settingKeyValue)
        {
            ResponseActionResult response = new ResponseActionResult();
            response.SetSucces();
            var currentDate = DateTime.Now;
            try
            {
                var latestTime = settingKeyValue.LatestTime;
                if (latestTime.Date < currentDate.Date)
                {
                    _ = await ProcessSummaryReportEmployeeHandleLoanStartNewDay();
                    settingKeyValue.LatestCommentID = 0;
                    settingKeyValue.LatestTime = currentDate.Date;
                    response.Data = settingKeyValue;
                    return response;
                }
                var startDate = currentDate.Date;
                Dictionary<long, long> dictLoanIDInfos = new Dictionary<long, long>();
                List<long> lstLoanID = new List<long>();

                // lấy transaction loan từ thời điểm latestTime
                var lstTxnLoanInfosTask = _transactionLoanTab.SelectColumns(x => x.LoanID, x => x.CreateDate).WhereClause(x => x.CreateDate > latestTime && x.TotalMoney != 0).QueryAsync();

                // 1 loan có nhiều ng comment
                // 1 ng comment nhiều loan
                var lstCommentTask = _commentDebtPromptedTab.SelectColumns(x => x.UserID, x => x.LoanID, x => x.CommentDebtPromptedID)
                                                            .WhereClause(x => x.CreateDate > startDate && x.CommentDebtPromptedID > settingKeyValue.LatestCommentID)
                                                            .QueryAsync();

                await Task.WhenAll(lstTxnLoanInfosTask, lstCommentTask);

                var lstTxnLoanInfo = lstTxnLoanInfosTask.Result;
                if (lstTxnLoanInfo != null && lstTxnLoanInfo.Any())
                {
                    foreach (var item in lstTxnLoanInfo)
                    {
                        if (!dictLoanIDInfos.ContainsKey((long)item.LoanID))
                        {
                            dictLoanIDInfos.Add((long)item.LoanID, (long)item.LoanID);
                        }
                        if (settingKeyValue.LatestTime < item.CreateDate.Value)
                        {
                            settingKeyValue.LatestTime = item.CreateDate.Value;
                        }
                    }
                }
                var lstComment = lstCommentTask.Result;
                var dictUserIDCommentInfos = new Dictionary<long, int>();
                if (lstComment != null && lstComment.Any())
                {
                    foreach (var item in lstComment)
                    {
                        if (!dictLoanIDInfos.ContainsKey(item.LoanID))
                        {
                            dictLoanIDInfos.Add(item.LoanID, item.LoanID);
                        }
                        if (!dictUserIDCommentInfos.ContainsKey(item.UserID))
                        {
                            dictUserIDCommentInfos.Add(item.UserID, 0);
                        }
                        dictUserIDCommentInfos[item.UserID] += 1;
                        if (settingKeyValue.LatestCommentID < item.CommentDebtPromptedID)
                        {
                            settingKeyValue.LatestCommentID = item.CommentDebtPromptedID;
                        }
                    }
                }
                lstLoanID = dictLoanIDInfos.Values.ToList();
                var dictCutOffLoanInfosTask = GetDictCutOffLoanByLstLoanID(lstLoanID, currentDate);
                var dictLoanInfosCurrentTask = GetDictLoanInfosByLstLoanID(lstLoanID);
                var lstAssigmentLoanDataTask = GetLstAssignmentLoanInfosLoanIDs(lstLoanID);
                await Task.WhenAll(dictCutOffLoanInfosTask, dictLoanInfosCurrentTask, lstAssigmentLoanDataTask);

                var dictEmployeeIDInfos = new Dictionary<long, List<long>>();
                var dictLoanInfosCurrent = dictLoanInfosCurrentTask.Result;
                var dictCutOffLoanInfos = dictCutOffLoanInfosTask.Result;
                foreach (var item in lstAssigmentLoanDataTask.Result)
                {
                    if (!dictEmployeeIDInfos.ContainsKey(item.UserID))
                    {
                        dictEmployeeIDInfos.Add(item.UserID, new List<long>());
                    }
                    dictEmployeeIDInfos[item.UserID].Add(item.LoanID);
                }
                int totalLoanHandleDone = 0;
                var lstUserIDs = dictEmployeeIDInfos.Keys.ToList();
                var lstReportEmployeeHandleLoanData = await _reportEmployeeHandleLoanTab.WhereClause(x => x.ReportDate == latestTime.Date && lstUserIDs.Contains(x.EmployeeID)).QueryAsync();
                foreach (var item in lstReportEmployeeHandleLoanData)
                {
                    totalLoanHandleDone = 0;
                    var lstLoanIDUserHandle = dictEmployeeIDInfos.GetValueOrDefault(item.EmployeeID);
                    if (lstLoanIDUserHandle != null && lstLoanIDUserHandle.Count > 0)
                    {
                        foreach (var loanID in lstLoanIDUserHandle)
                        {
                            var loanInfo = dictLoanInfosCurrent.GetValueOrDefault(loanID);
                            var loanCutOffDetail = dictCutOffLoanInfos.GetValueOrDefault(loanID);
                            if (loanInfo != null && loanInfo.LoanID > 0)
                            {
                                if (loanCutOffDetail == null || loanCutOffDetail.LoanID < 0)
                                {
                                    // log lại lỗi để check
                                    // ko xảy ra case này
                                    continue;
                                }
                                if (loanInfo.NextDate.Value.Date != loanCutOffDetail.NextDate.Date)
                                {
                                    totalLoanHandleDone += 1;
                                }
                            }
                        }
                    }
                    item.NumberLoanHandleDone += totalLoanHandleDone;
                    item.NumberComment += dictUserIDCommentInfos.GetValueOrDefault(item.EmployeeID);
                    item.ModifyDate = currentDate;
                }
                _reportEmployeeHandleLoanTab.UpdateBulk(lstReportEmployeeHandleLoanData);
                response.Data = settingKeyValue;
                response.Result = (int)ResponseAction.Success;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSummaryReportEmployeeHandleLoan|settingKeyValue={_common.ConvertObjectToJSonV2(settingKeyValue)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private async Task<List<Domain.Tables.TblAssignmentLoan>> GetLstAssignmentLoanInfosLoanIDs(List<long> lstLoanID)
        {
            var currentDate = DateTime.Now;
            var firstDayOfMonthCurrent = new DateTime(currentDate.Year, currentDate.Month, 1);
            var dateAssigned = firstDayOfMonthCurrent.AddSeconds(-1);
            long minLoanID = long.MaxValue;
            long maxLoanID = 0;
            try
            {
                if (lstLoanID.Count > TimaSettingConstant.MaxItemQuery)
                {
                    foreach (var id in lstLoanID)
                    {
                        if (minLoanID > id)
                        {
                            minLoanID = id;
                        }
                        if (maxLoanID < id)
                        {
                            maxLoanID = id;
                        }
                    }
                    minLoanID -= 1;
                    maxLoanID += 1;
                }
                _assignmentLoanTab.WhereClause(x => x.DateAssigned > dateAssigned);
                if (maxLoanID != 0)
                {
                    _assignmentLoanTab.WhereClause(x => x.LoanID > minLoanID && x.LoanID < maxLoanID);
                }
                else
                {
                    _assignmentLoanTab.WhereClause(x => lstLoanID.Contains(x.LoanID));
                }
                var lstAssignmentLoanInfos = await _assignmentLoanTab.SelectColumns(x => x.LoanID, x => x.UserID).QueryAsync();
                if (lstAssignmentLoanInfos == null || !lstAssignmentLoanInfos.Any())
                {
                    return new List<Domain.Tables.TblAssignmentLoan>();
                }
                if (maxLoanID != 0)
                {
                    return lstAssignmentLoanInfos.Where(x => lstLoanID.Contains(x.LoanID)).ToList();
                }
                else
                {
                    return lstAssignmentLoanInfos.ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLstAssignmentLoanInfosLoanIDs|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }
        private async Task<ResponseActionResult> ProcessSummaryReportEmployeeHandleLoanStartNewDay()
        {

            ResponseActionResult response = new ResponseActionResult();
            var currentDate = DateTime.Now;
            try
            {
                var firstDayOfMonthCurrent = new DateTime(currentDate.Year, currentDate.Month, 1);
                var dateAssigned = firstDayOfMonthCurrent.AddSeconds(-1);
                // xuất file danh sách đầu tháng hiện tại
                // lấy danh sách loan dc gán cho nhân viên
                var lstLoanAssigned = await _assignmentLoanTab.SelectColumns(x => x.LoanID, x => x.UserID).WhereClause(x => x.DateAssigned > dateAssigned).QueryAsync();
                if (lstLoanAssigned == null || !lstLoanAssigned.Any())
                {
                    _logger.LogError($"ProcessSummaryReportEmployeeHandleLoanStartNewDay_waring|Not loan assigned for employee|currentDate={currentDate:dd/MM/yyyy HH:mm}");
                    return response;
                }
                Dictionary<long, List<long>> dictEmployeeHandleLoanIDInfos = new Dictionary<long, List<long>>();
                Dictionary<long, List<long>> dictLoanIDHasEmployeeHandleTogether = new Dictionary<long, List<long>>();
                Dictionary<long, Domain.Tables.TblReportEmployeeHandleLoan> dictReportEmployeeHandleLoanInfos = new Dictionary<long, Domain.Tables.TblReportEmployeeHandleLoan>();
                foreach (var item in lstLoanAssigned)
                {
                    if (!dictEmployeeHandleLoanIDInfos.ContainsKey(item.UserID))
                    {
                        dictEmployeeHandleLoanIDInfos.Add(item.UserID, new List<long>());
                    }
                    dictEmployeeHandleLoanIDInfos[item.UserID].Add(item.LoanID);
                    if (!dictLoanIDHasEmployeeHandleTogether.ContainsKey(item.LoanID))
                    {
                        dictLoanIDHasEmployeeHandleTogether.Add(item.LoanID, new List<long>());
                    }
                    dictLoanIDHasEmployeeHandleTogether[item.LoanID].Add(item.UserID);
                }
                var lstLoanID = dictLoanIDHasEmployeeHandleTogether.Keys;
                // lấy thông tin loan
                long minLoanID = long.MaxValue;
                long maxLoanID = 0;
                if (lstLoanID.Count > TimaSettingConstant.MaxItemQuery)
                {
                    foreach (var id in lstLoanID)
                    {
                        if (minLoanID > id)
                        {
                            minLoanID = id;
                        }
                        if (maxLoanID < id)
                        {
                            maxLoanID = id;
                        }
                    }
                    minLoanID -= 1;
                    maxLoanID += 1;
                    _loanTab.WhereClause(x => x.LoanID > minLoanID && x.LoanID < maxLoanID);
                }
                else
                {
                    _loanTab.WhereClause(x => lstLoanID.Contains(x.LoanID));
                }
                var lstLoanInfos = await _loanTab.SelectColumns(x => x.LoanID, x => x.NextDate)
                                                .WhereClause(x => RuleCollectionServiceHelper.LstStatusLoanLending.Contains((int)x.Status))
                                                .QueryAsync();
                var dictLoanInfos = new Dictionary<long, Domain.Tables.TblLoan>();
                var dictLoanIDEmployeeHandle = new Dictionary<long, List<long>>();
                foreach (var item in lstLoanInfos)
                {
                    if (!dictLoanIDHasEmployeeHandleTogether.ContainsKey(item.LoanID))
                    {
                        continue;
                    }
                    dictLoanInfos.Add(item.LoanID, item);
                    var lstEmployeeID = dictLoanIDHasEmployeeHandleTogether.GetValueOrDefault(item.LoanID);
                    if (lstEmployeeID != null && lstEmployeeID.Count > 0)
                    {
                        foreach (var emID in lstEmployeeID)
                        {
                            if (!dictReportEmployeeHandleLoanInfos.ContainsKey(emID))
                            {
                                dictReportEmployeeHandleLoanInfos.Add(emID, new Domain.Tables.TblReportEmployeeHandleLoan
                                {
                                    CreateDate = currentDate,
                                    ModifyDate = currentDate,
                                    EmployeeID = emID,
                                    ReportDate = currentDate.Date,
                                    TotalLoanHandle = 0,
                                    LstLoanID = ""
                                });
                                dictLoanIDEmployeeHandle.Add(emID, new List<long>());
                            }
                            if (item.NextDate.Value.Date <= currentDate.Date)
                            {
                                dictReportEmployeeHandleLoanInfos[emID].TotalLoanHandle += 1;
                                dictLoanIDEmployeeHandle[emID].Add(item.LoanID);
                            }
                        }
                    }
                }
                response.SetSucces();
                response.Data = currentDate.Date;
                if (dictReportEmployeeHandleLoanInfos.Count > 0)
                {
                    foreach (var item in dictReportEmployeeHandleLoanInfos)
                    {
                        var lstLoanIDEmployeeHandle = dictLoanIDEmployeeHandle.GetValueOrDefault(item.Key);
                        if (lstLoanIDEmployeeHandle != null && lstLoanIDEmployeeHandle.Count > 0)
                        {
                            item.Value.LstLoanID = string.Join(",", lstLoanIDEmployeeHandle);
                        }
                    }
                    _reportEmployeeHandleLoanTab.InsertBulk(dictReportEmployeeHandleLoanInfos.Values);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSummaryReportEmployeeHandleLoanStartNewDay|Not loan assigned for employee|currentDate={currentDate:dd/MM/yyyy HH:mm}|ex={ex.Message}-{ex.Message}");
            }
            return response;
        }

        public async Task<ResponseActionResult> ProcessReportLoanDebtType(SettingKeyReportLoanDebtType settingKeyValue)
        {
            ResponseActionResult response = new ResponseActionResult();
            var currentDate = DateTime.Now;
            int pageSize = TimaSettingConstant.MaxItemQuery;
            int numberRow = 1;
            try
            {
                // k truyền hoặc giá trị fromDate > hiện tại thì gán chạy ngày hm nay
                if (settingKeyValue.FromDate == null || settingKeyValue.FromDate.Year < 1900 || settingKeyValue.FromDate > currentDate) settingKeyValue.FromDate = currentDate;

                var firstDayOfMonth = new DateTime(settingKeyValue.FromDate.Year, settingKeyValue.FromDate.Month, 1);
                var lastDayBefore = firstDayOfMonth.AddDays(-1);
                var yearMonth = int.Parse(firstDayOfMonth.ToString("yyyyMM"));
                var countData = _trackingLoanInMonthTab.Query($"SELECT COUNT(TrackingLoanID) TrackingLoanID FROM TblTrackingLoanInMonth(NOLOCK) WHERE YearMonth = {yearMonth}", null).FirstOrDefault().TrackingLoanID;
                if (countData == 0)
                {
                    //ConcurrentBag<TblTrackingLoanInMonth> loanInMonths = new ConcurrentBag<TblTrackingLoanInMonth>();
                    List<TblTrackingLoanInMonth> loanInMonths = new List<TblTrackingLoanInMonth>();
                    // ngày đầu tháng lấy dữ liệu insert vào bảng TblTrackingLoanInMonth
                    int statusLending = (int)Loan_Status.Lending;
                    var cutOffLoans = await _cutOffLoanTab.WhereClause(x => x.CutOffDate == lastDayBefore && x.Status == statusLending).QueryAsync();
                    // -- Lấy cutoffLoan ngày cuối tháng trước --> k có dữ liệu thì báo k có cutoffLoan
                    if (cutOffLoans == null || !cutOffLoans.Any())
                    {
                        response.Message = MessageConstant.ReportLoanDebtType_CutOffDataError + $"{lastDayBefore.ToString("dd/MM/yyyy")}";
                        _logger.LogError($"ProcessReportLoanDebtType|Message={ response.Message}");
                        return response;
                    }
                    numberRow = cutOffLoans.Count() / pageSize + 1;
                    cutOffLoans = cutOffLoans.OrderBy(x => x.LoanID).ToList();
                    #region threads
                    List<Task<List<TblTrackingLoanInMonth>>> lstTask = new List<Task<List<TblTrackingLoanInMonth>>>();
                    for (int i = 1; i < numberRow + 1; i++)
                    {
                        var loanClone = _loanTab.Clone();
                        var customerClone = _customerTab.Clone();
                        var cutOfLoanSkip = cutOffLoans.Skip((i - 1) * pageSize).Take(pageSize).ToList();
                        var lstLoanIdSkip = cutOfLoanSkip.Select(x => x.LoanID).Distinct().OrderBy(x => x).ToList();
                        var task = Task.Run(async () =>
                        {
                            List<TblTrackingLoanInMonth> lstLoanInMonthTaskResult = new List<TblTrackingLoanInMonth>();
                            var loans = await loanClone.SelectColumns(x => x.LoanID, x => x.CustomerID, x => x.FinishDate, x => x.CityID, x => x.DistrictID, x => x.WardID, x => x.ContactCode, x => x.ProductID, x => x.FromDate, x => x.ToDate)
                                                           .WhereClause(x => lstLoanIdSkip.Contains(x.LoanID)).QueryAsync();
                            var dicLoans = loans.ToDictionary(x => x.LoanID, x => x);
                            var lstCustomerIds = loans.Where(x => x.CustomerID > 0).Select(x => x.CustomerID).ToList();
                            var customers = await customerClone.SelectColumns(x => x.CustomerID, x => x.FullName, x => x.Address).WhereClause(x => lstCustomerIds.Contains(x.CustomerID)).QueryAsync();
                            var dicCustomers = customers.ToDictionary(x => x.CustomerID, x => x);
                            // map dữ liệu
                            foreach (var item in cutOfLoanSkip)
                            {
                                try
                                {
                                    var loan = dicLoans.GetValueOrDefault(item.LoanID) ?? new TblLoan();
                                    var customer = dicCustomers.GetValueOrDefault((long)loan.CustomerID) ?? new TblCustomer();
                                    var newObject = new Domain.Models.Excel.ExtraDataTracking
                                    {
                                        CustomerName = customer.FullName,
                                        CustomerID = customer.CustomerID,
                                        FromDate = loan.FromDate,
                                        ToDate = loan.ToDate,
                                        ContractCode = loan.ContactCode,
                                        Address = customer.Address
                                    };
                                    lstLoanInMonthTaskResult.Add(new TblTrackingLoanInMonth()
                                    {
                                        CityID = loan.CityID,
                                        CreateDate = currentDate,
                                        LoanID = item.LoanID,
                                        DistrictID = loan.DistrictID,
                                        LoanStatus = item.Status,
                                        NextDateAtFirstDayOfMonth = item.NextDate,
                                        WardID = loan.WardID,
                                        ProductTypeID = loan.ProductID ?? 0,
                                        ModifyDate = currentDate,
                                        TotalLoanMoneyCurrent = item.TotalMoneyCurrent,
                                        TotalMoneyReceied = 0,
                                        LoanFinshedDate = loan.FinishDate,
                                        ExtraData = _common.ConvertObjectToJSonV2(newObject),
                                        DebtType = (int)RuleCollectionServiceHelper.GetTypeBebtLoanByNextDate_v2(item.NextDate, firstDayOfMonth),
                                        YearMonth = yearMonth
                                    });
                                }
                                catch (Exception ex)
                                {
                                    _logger.LogError($"ProcessReportLoanDebtType_loanInMonths.ADD()|cutOfLoanSkip={_common.ConvertObjectToJSonV2(item)}|ex={ex.Message}-{ex.Message}");
                                }
                            }
                            return lstLoanInMonthTaskResult;
                        });
                        lstTask.Add(task);
                    }
                    await Task.WhenAll(lstTask);
                    foreach (var task in lstTask)
                    {
                        if (task.Result.Count > 0)
                        {
                            loanInMonths.AddRange(task.Result);
                        }
                    }
                    #endregion
                    #region Paralle
                    //Parallel.For(1, numberRow + 1, new ParallelOptions { MaxDegreeOfParallelism = 4 }, (i, state) =>
                    //     {
                    //         var loanClone = _loanTab.Clone();
                    //         var customerClone = _customerTab.Clone();
                    //         var cutOfLoanSkip = cutOffLoans.Skip((i - 1) * pageSize).Take(pageSize).ToList();
                    //         var lstLoanIdSkip = cutOfLoanSkip.Select(x => x.LoanID).Distinct().OrderBy(x => x).ToList();
                    //         var loans = loanClone.SelectColumns(x => x.LoanID, x => x.CustomerID, x => x.FinishDate, x => x.CityID, x => x.DistrictID, x => x.WardID, x => x.ContactCode, x => x.ProductID, x => x.FromDate, x => x.ToDate)
                    //                                   .WhereClause(x => lstLoanIdSkip.Contains(x.LoanID)).Query();
                    //         var dicLoans = loans.ToDictionary(x => x.LoanID, x => x);
                    //         var lstCustomerIds = loans.Where(x => x.CustomerID > 0).Select(x => x.CustomerID).ToList();
                    //         var customers = customerClone.SelectColumns(x => x.CustomerID, x => x.FullName, x => x.Address).WhereClause(x => lstCustomerIds.Contains(x.CustomerID)).Query();
                    //         var dicCustomers = customers.ToDictionary(x => x.CustomerID, x => x);
                    //         // map dữ liệu
                    //         foreach (var item in cutOfLoanSkip)
                    //         {
                    //             try
                    //             {
                    //                 var loan = dicLoans.GetValueOrDefault(item.LoanID) ?? new TblLoan();
                    //                 var customer = dicCustomers.GetValueOrDefault((long)loan.CustomerID) ?? new TblCustomer();
                    //                 var newObject = new Domain.Models.Excel.ExtraDataTracking
                    //                 {
                    //                     CustomerName = customer.FullName,
                    //                     CustomerID = customer.CustomerID,
                    //                     FromDate = loan.FromDate,
                    //                     ToDate = loan.ToDate,
                    //                     ContractCode = loan.ContactCode,
                    //                     Address = customer.Address
                    //                 };
                    //                 loanInMonths.Add(new TblTrackingLoanInMonth()
                    //                 {
                    //                     CityID = loan.CityID,
                    //                     CreateDate = currentDate,
                    //                     LoanID = item.LoanID,
                    //                     DistrictID = loan.DistrictID,
                    //                     LoanStatus = item.Status,
                    //                     NextDateAtFirstDayOfMonth = item.NextDate,
                    //                     WardID = loan.WardID,
                    //                     ProductTypeID = loan.ProductID ?? 0,
                    //                     ModifyDate = currentDate,
                    //                     TotalLoanMoneyCurrent = item.TotalMoneyCurrent,
                    //                     TotalMoneyReceied = 0,
                    //                     LoanFinshedDate = loan.FinishDate,
                    //                     ExtraData = _common.ConvertObjectToJSonV2(newObject),
                    //                     DebtType = (int)RuleCollectionServiceHelper.GetTypeBebtLoanByNextDate_v2(item.NextDate),
                    //                     YearMonth = yearMonth
                    //                 });
                    //             }
                    //             catch (Exception ex)
                    //             {
                    //                 _logger.LogError($"ProcessReportLoanDebtType_loanInMonths.ADD()|cutOfLoanSkip={_common.ConvertObjectToJSonV2(item)}|ex={ex.Message}-{ex.Message}");
                    //             }

                    //         }

                    //     });
                    #endregion
                    settingKeyValue.FromDate = firstDayOfMonth;
                    if (loanInMonths.Count > 0)
                        _trackingLoanInMonthTab.InsertBulk(loanInMonths);
                }
                // chạy dữ liệu theo transaction
                DateTime nextDay = settingKeyValue.FromDate.AddDays(1);
                var transactions = await _transactionLoanTab.SelectColumns(x => x.LoanID, x => x.TotalMoney, x => x.ActionID, x => x.CreateDate, x => x.TransactionID)
                                                      .WhereClause(x => x.CreateTime >= settingKeyValue.FromDate && x.CreateTime < nextDay && x.TotalMoney != 0).QueryAsync();
                // --> lấy theo loan -> cập nhập lại hđ , chưa có loan đó thì insert thêm.

                if (transactions == null || !transactions.Any())
                {
                    response.Message = MessageConstant.ReportLoanDebtType_RunALL;
                    settingKeyValue.FromDate = nextDay;
                    settingKeyValue.LastestTransactionID = transactions != null && transactions.Any() ? transactions.Max(x => x.TransactionID) : settingKeyValue.LastestTransactionID;
                    settingKeyValue.LatestTime = currentDate;
                    response.SetSucces();
                    response.Data = settingKeyValue;
                    return response;
                }
                var listNewLoan = new List<long>();
                var dicLoanPayment = new Dictionary<long, long>();
                var listCloseLoan = new List<long>();

                var lstActionCreateLoan = new List<int>
                {
                     (int)Transaction_Action.SuaHd,(int)Transaction_Action.ChoVay,(int)Transaction_Action.HuyChoVay
                };
                foreach (var item in transactions)
                {
                    //hđ tạo mới
                    if (lstActionCreateLoan.Contains(item.ActionID))
                    {
                        listNewLoan.Add((long)item.LoanID);
                    }
                    else
                    {
                        if (!dicLoanPayment.ContainsKey(item.LoanID ?? 0))
                        {
                            dicLoanPayment.Add(item.LoanID.Value, item.TotalMoney);
                        }
                        else
                        {
                            dicLoanPayment[item.LoanID.Value] += item.TotalMoney;
                        }
                        // hợp đồng kết thúc
                        if (item.ActionID == (int)Transaction_Action.DongHd)
                        {
                            listCloseLoan.Add((long)item.LoanID);
                        }
                    }
                }
                // insert loannews _trackingLoanInMonthTab
                var loansContains = await _trackingLoanInMonthTab.SelectColumns(x => x.LoanID).WhereClause(x => x.YearMonth == yearMonth && listNewLoan.Contains((long)x.LoanID)).QueryAsync();
                var loanIdsNotContain = new List<long>();

                var loanIdsContains = loansContains.Select(x => x.LoanID).ToList();
                foreach (var item in listNewLoan)
                {
                    if (!loanIdsContains.Contains(item))
                    {
                        loanIdsNotContain.Add(item);
                    }
                }
                if (loanIdsNotContain.Any())
                {
                    var loans = await _loanTab.WhereClause(x => loanIdsNotContain.Contains(x.LoanID)).QueryAsync();
                    var lstCustomerIds = loans.Select(x => x.CustomerID).ToList();
                    var customers = await _customerTab.SelectColumns(x => x.CustomerID, x => x.FullName, x => x.Address).WhereClause(x => lstCustomerIds.Contains(x.CustomerID)).QueryAsync();
                    var dicCustomers = customers.ToDictionary(x => x.CustomerID, x => x);
                    // map dữ liệu
                    List<TblTrackingLoanInMonth> loanInMonths = new List<TblTrackingLoanInMonth>();
                    foreach (var item in loans)
                    {
                        if (item.NextDate < firstDayOfMonth.AddMonths(1))
                        {
                            var customer = dicCustomers.GetValueOrDefault((long)item.CustomerID) ?? new TblCustomer();
                            var newObject = new
                            {
                                CustomerName = customer.FullName,
                                CustomerID = customer.CustomerID,
                                FromDate = item.FromDate,
                                ToDate = item.ToDate,
                                ContractCode = item.ContactCode
                            };
                            loanInMonths.Add(new TblTrackingLoanInMonth()
                            {
                                CityID = item.CityID,
                                CreateDate = currentDate,
                                LoanID = item.LoanID,
                                DistrictID = item.DistrictID,
                                LoanStatus = item.Status.GetValueOrDefault(),
                                NextDateAtFirstDayOfMonth = item.NextDate.GetValueOrDefault(),
                                WardID = item.WardID,
                                ProductTypeID = item.ProductID ?? 0,
                                ModifyDate = currentDate,
                                TotalLoanMoneyCurrent = item.TotalMoneyCurrent,
                                TotalMoneyReceied = 0,
                                LoanFinshedDate = item.FinishDate,
                                ExtraData = _common.ConvertObjectToJSonV2(newObject),
                                DebtType = (int)RuleCollectionServiceHelper.GetTypeBebtLoanByNextDate_v2(item.NextDate.GetValueOrDefault(), firstDayOfMonth),
                                YearMonth = yearMonth
                            });
                        }
                    }
                    if (loanInMonths.Any())
                    {
                        _trackingLoanInMonthTab.InsertBulk(loanInMonths);
                    }
                }

                // update dữ liệu loans vào _trackingLoanInMonthTab
                var lstLoansPayment = dicLoanPayment.Keys.ToList().OrderByDescending(x => x);
                // lấy dữ liệu theo loanID
                numberRow = lstLoansPayment.Count() / pageSize + 1;
                #region threads
                for (int i = 1; i < numberRow + 1; i++)
                {
                    var trackLoanClone = _trackingLoanInMonthTab.Clone();
                    var loanIdsShip = lstLoansPayment.Skip((i - 1) * pageSize).Take(pageSize).ToList();
                    _ = Task.Run(async () =>
                     {
                         var trackingLoanInMonths = await trackLoanClone.WhereClause(x => x.YearMonth == yearMonth && loanIdsShip.Contains((long)x.LoanID)).QueryAsync();
                         if (trackingLoanInMonths != null && trackingLoanInMonths.Any())
                         {
                             foreach (var item in trackingLoanInMonths)
                             {
                                 var dataChange = dicLoanPayment[(long)item.LoanID];
                                 item.TotalMoneyReceied += dataChange;
                                 if (listCloseLoan.Contains((long)item.LoanID)) item.LoanFinshedDate = currentDate.Date;
                             }
                             trackLoanClone.UpdateBulk(trackingLoanInMonths);
                         }
                     });
                }
                #endregion
                #region paralle
                //Parallel.For(1, numberRow + 1, new ParallelOptions { MaxDegreeOfParallelism = 4 }, (i, state) =>
                // {
                //     var trackLoanClone = _trackingLoanInMonthTab.Clone();
                //     var loanIdsShip = lstLoansPayment.Skip((i - 1) * pageSize).Take(pageSize).ToList();
                //     var trackingLoanInMonths = trackLoanClone.WhereClause(x => x.YearMonth == yearMonth && loanIdsShip.Contains((long)x.LoanID)).Query();
                //     foreach (var item in trackingLoanInMonths)
                //     {
                //         var dataChange = dicLoanPayment[(long)item.LoanID];
                //         item.TotalMoneyReceied += dataChange;
                //         if (listCloseLoan.Contains((long)item.LoanID)) item.LoanFinshedDate = currentDate.Date;
                //     }
                //     _trackingLoanInMonthTab.UpdateBulk(trackingLoanInMonths);
                // });
                #endregion
                settingKeyValue.FromDate = nextDay;
                settingKeyValue.LastestTransactionID = transactions != null && transactions.Any() ? transactions.Max(x => x.TransactionID) : settingKeyValue.LastestTransactionID;
                settingKeyValue.LatestTime = currentDate;
                response.SetSucces();
                response.Data = settingKeyValue;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessReportLoanDebtType|settingKeyValue={settingKeyValue.FromDate:dd/MM/yyyy HH:mm}|ex={ex.Message}-{ex.Message}");
            }
            return response;
        }


    }
}
