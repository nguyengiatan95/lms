﻿using LMS.Kafka.Interfaces;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Razor.Language.TagHelperMetadata;

namespace CollectionServiceApi.KafkaEventHandlers
{
    public class CustomerBalanceTopupInfoHandler : IKafkaHandler<string, LMS.Kafka.Messages.Customer.CustomerBalanceToupSuccessInfo>
    {
        private readonly IMediator _bus;
        LMS.Common.Helper.Utils _common = new LMS.Common.Helper.Utils();
        ILogger<CustomerBalanceTopupInfoHandler> _logger;
        public CustomerBalanceTopupInfoHandler(IMediator bus, ILogger<CustomerBalanceTopupInfoHandler> logger)
        {
            _bus = bus;
            _logger = logger;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> HandleAsync(string key, LMS.Kafka.Messages.Customer.CustomerBalanceToupSuccessInfo value)
        {
            // theo dõi 1 thời gian rồi bỏ log này
            _logger.LogInformation($"CustomerBalanceTopupInfoHandler|topic={LMS.Kafka.Constants.KafkaTopics.CustomerBalanceTopupSuccessInfo}|value={_common.ConvertObjectToJSonV2(value)}|Handled");
            var request = new Commands.LoanExemption.TranferAgCloseLoanExemptionCommand
            {
                CustomerID = value.CustomerID,
                UserID = LMS.Common.Constants.TimaSettingConstant.UserIDAdmin,
                UserName = LMS.Common.Constants.TimaSettingConstant.UserNameAdmin
            };
            var t1 = _bus.Send(request);
            _ = _bus.Send(new Commands.SmartDialer.UpdatePhoneAfterCustomerTopupCommand { CustomerID = value.CustomerID });
            await Task.WhenAll(t1);
            return t1.Result;
        }
    }
}
