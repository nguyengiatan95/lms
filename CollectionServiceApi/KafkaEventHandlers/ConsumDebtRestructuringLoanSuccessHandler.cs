﻿using LMS.Kafka.Interfaces;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.KafkaEventHandlers
{
    public class ConsumDebtRestructuringLoanSuccessHandler : IKafkaHandler<string, LMS.Kafka.Messages.Loan.DebtRestructuringLoanSuccessInfo>
    {
        private readonly IMediator _bus;
        LMS.Common.Helper.Utils _common = new LMS.Common.Helper.Utils();
        ILogger<ConsumDebtRestructuringLoanSuccessHandler> _logger;
        public ConsumDebtRestructuringLoanSuccessHandler(IMediator bus, ILogger<ConsumDebtRestructuringLoanSuccessHandler> logger)
        {
            _bus = bus;
            _logger = logger;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> HandleAsync(string key, LMS.Kafka.Messages.Loan.DebtRestructuringLoanSuccessInfo value)
        {
            _logger.LogError($"ConsumDebtRestructuringLoanSuccessHandler_Waring|value={_common.ConvertObjectToJSonV2(value)}|topic={LMS.Kafka.Constants.KafkaTopics.DebtRestructuringLoanSuccess}");
            var request = new Commands.DebtRestructuringLoan.UpdateStatusByEventDebtRestructuringLoanIDCommand
            {
                LoanID = value.LoanID,
                CreateBy = value.CreateBy,
                DebtRestructuringType = value.DebtRestructuringType
            };
            return await _bus.Send(request);
        }
    }
}
