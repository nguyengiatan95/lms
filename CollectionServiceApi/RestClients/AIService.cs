﻿using LMS.Entites.Dtos.AG;
using LMS.Entites.Dtos.AI;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.RestClients
{
    public interface IAIService
    {
        Task<FriendFacebook> TopFriendFacebook(string KeySearch);
        Task<ResponseGPS> GetStopPoint(RequestGPS request);
        Task<List<HistoryCommentChip>> ShowHistoryComment(HistoryCommentChipReq req);
        Task<List<ListCheckInOut>> GetListCheckInOut(string code);
    }
    public class AIService : IAIService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        ILogger<AIService> _logger;
        public AIService(LMS.Common.Helper.IApiHelper apiHelper, ILogger<AIService> logger)
        {
            _apiHelper = apiHelper;
            _logger = logger;
        }
        public async Task<FriendFacebook> TopFriendFacebook(string KeySearch)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_AI_TopFriendFacebook}?KeySearch={KeySearch}";
            return await _apiHelper.ExecuteAsync<FriendFacebook>(url: url.ToString());
        }
        public async Task<ResponseGPS> GetStopPoint(RequestGPS request)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_AI_GetStopPoint}";
            return await _apiHelper.ExecuteAsync<ResponseGPS>(url: url.ToString(), RestSharp.Method.POST, request);
        }
        public async Task<List<HistoryCommentChip>> ShowHistoryComment(HistoryCommentChipReq request)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_AI_ShowHistoryComment}";
            return await _apiHelper.ExecuteAsync<List<HistoryCommentChip>>(url: url.ToString(), RestSharp.Method.POST, request);
        }

        public async Task<List<ListCheckInOut>> GetListCheckInOut(string code)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_ListCheckInOut}/{code}";
            return await _apiHelper.ExecuteAsync<List<ListCheckInOut>>(url: url, method: RestSharp.Method.GET, null);
        }
    }
}
