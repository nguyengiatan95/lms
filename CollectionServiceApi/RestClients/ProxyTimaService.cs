﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.AG;
using LMS.Entites.Dtos.ProxyTima;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.RestClients
{
     public interface IProxyTimaService
    {
        Task<ResponseActionResult> K03ProxyTima(K03ProxyTimaRequest entity);
        Task<ResponseActionResult> K06ProxyTima(K06ProxyTimaRequest entity);
        Task<ResponseActionResult> K21ProxyTima(K21ProxyTimaRequest entity);
        Task<ResponseActionResult> K22ProxyTima(K22ProxyTimaRequest entity);
        Task<ResponseActionResult> CallApiAg (AgDataPostModel model);
    }
    public class ProxyTimaService : IProxyTimaService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        public ProxyTimaService(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }

        public async Task<ResponseActionResult> CallApiAg(AgDataPostModel request)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_CallApiAG}";
            return await _apiHelper.ExecuteAsync(url: url.ToString(), RestSharp.Method.POST, request);
        }

        public async Task<ResponseActionResult> K03ProxyTima(K03ProxyTimaRequest request)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_ProxyTima_K03ProxyTima}";
            return await _apiHelper.ExecuteAsync(url: url.ToString(), RestSharp.Method.POST, request);
        }
        public async Task<ResponseActionResult> K06ProxyTima(K06ProxyTimaRequest request)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_ProxyTima_K06ProxyTima}";
            return await _apiHelper.ExecuteAsync(url: url.ToString(), RestSharp.Method.POST, request);
        }
        public async Task<ResponseActionResult> K21ProxyTima(K21ProxyTimaRequest request)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_ProxyTima_K21ProxyTima}";
            return await _apiHelper.ExecuteAsync(url: url.ToString(), RestSharp.Method.POST, request);
        }
        public async Task<ResponseActionResult> K22ProxyTima(K22ProxyTimaRequest request)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_ProxyTima_K22ProxyTima}";
            return await _apiHelper.ExecuteAsync(url: url.ToString(), RestSharp.Method.POST, request);
        }
    }
}
