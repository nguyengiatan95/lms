﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.RestClients
{
    public static class RestClientsInstaller
    {
        public static IServiceCollection AddRestClientsService(this IServiceCollection services)
        {
            services.AddTransient(typeof(ILOSService), typeof(LOSService));
            services.AddTransient(typeof(IPaymentScheduleService), typeof(PaymentScheduleService));
            services.AddTransient(typeof(ILoanService), typeof(LoanService));
            services.AddTransient(typeof(IAIService), typeof(AIService));
            services.AddTransient(typeof(ICustomerService), typeof(CustomerService));
            services.AddTransient(typeof(IProxyTimaService), typeof(ProxyTimaService));
            services.AddTransient(typeof(ISmartDialerGateway), typeof(SmartDialerGateway));
            return services;
        }
    }
}
