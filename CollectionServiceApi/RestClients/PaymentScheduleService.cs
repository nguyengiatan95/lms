﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.RestClients
{
    public interface IPaymentScheduleService
    {
        Task<IEnumerable<LMS.Entites.Dtos.PaymentServices.PaymentScheduleModel>> GetLstPaymentByLoanID(long loanID);
    }
    public class PaymentScheduleService : IPaymentScheduleService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        public PaymentScheduleService(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }

        public async Task<IEnumerable<LMS.Entites.Dtos.PaymentServices.PaymentScheduleModel>> GetLstPaymentByLoanID(long loanID)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.PaymentService}{LMS.Common.Constants.ActionApiInternal.PaymentSchedule_GetLstPaymentByLoanID}/{loanID}";
            return await _apiHelper.ExecuteAsync<IEnumerable<LMS.Entites.Dtos.PaymentServices.PaymentScheduleModel>>(url: url.ToString());
        }
    }
}
