﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.LOSServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.RestClients
{
    public interface ILOSService
    {
        Task<List<HistoryCommentCredit>> GetHistoryCommentLOS(int page, int pageSize, long loanCreditId);
        Task<List<ResultListImage>> GetListImages(GetListImagesReq req);
        Task<ResponseActionResult> ChangePhoneLOS(ChangePhoneLOSReq req);
        Task<LMS.Entites.Dtos.LOSServices.MapResultLoanWaitingDisbursementDetailLOS> GetLoanCreditDetail(long loanID);
        Task<List<RelativeFamily>> GetRelativeFamilyLOS();
        Task<ResponseActionResult> CreateRelationshipLOS(CreateRelationshipReq req);
        Task<List<LMS.Entites.Dtos.LOSServices.LoanBriefPropertyLOS>> GetVehicleInforOfLoan(List<long?> lstLoanBriefId);
    }
    public class LOSService : ILOSService
    {
       LMS.Common.Helper.IApiHelper _apiHelper;
        public LOSService(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }
        public async Task<List<HistoryCommentCredit>> GetHistoryCommentLOS(int page, int pageSize, long loanCreditId)
        {
            var req = new
            {
                Page = page,
                PageSize = pageSize,
                LoanCreditId = loanCreditId
            };
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_LOS_GetHistoryComment}";
            return await _apiHelper.ExecuteAsync<List<HistoryCommentCredit>>(url: url.ToString(), RestSharp.Method.POST, req);
        }
        public async Task<List<ResultListImage>> GetListImages(GetListImagesReq req)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_LOS_GetListImages}";
            return await _apiHelper.ExecuteAsync<List<ResultListImage>>(url: url.ToString(), RestSharp.Method.POST, req);
        }
        public async Task<ResponseActionResult> ChangePhoneLOS(ChangePhoneLOSReq req)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_LOS_ChangePhoneLOS}";
            return await _apiHelper.ExecuteAsync<ResponseActionResult>(url: url.ToString(), RestSharp.Method.POST, req);
        }
        public async Task<LMS.Entites.Dtos.LOSServices.MapResultLoanWaitingDisbursementDetailLOS> GetLoanCreditDetail(long loanID)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_LOS_GetLoanCreditDetail}/{loanID}";
            return await _apiHelper.ExecuteAsync<LMS.Entites.Dtos.LOSServices.MapResultLoanWaitingDisbursementDetailLOS>(url: url.ToString());
        }
        public async Task<List<RelativeFamily>> GetRelativeFamilyLOS()
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_LOS_GetRelativeFamilyLOS}";
            return await _apiHelper.ExecuteAsync<List<RelativeFamily>>(url: url.ToString());
        }
        public async Task<ResponseActionResult> CreateRelationshipLOS(CreateRelationshipReq req)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_LOS_CreateRelationshipLOS}";
            return await _apiHelper.ExecuteAsync(url: url.ToString(), RestSharp.Method.POST, req);
        }
        public async Task<List<LMS.Entites.Dtos.LOSServices.LoanBriefPropertyLOS>> GetVehicleInforOfLoan(List<long?> lstLoanBriefId)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_LOS_GetVehicleInforOfLoan}";
            return await _apiHelper.ExecuteAsync<List<LMS.Entites.Dtos.LOSServices.LoanBriefPropertyLOS>>(url: url.ToString(), RestSharp.Method.POST, lstLoanBriefId);
        }
    }
}
