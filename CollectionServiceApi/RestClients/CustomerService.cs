﻿using LMS.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.RestClients
{
    public interface ICustomerService
    {
        Task<ResponseActionResult> ChangePhone(long customerID, long loanID, long createby, string phoneChange, string note);
    }
    public class CustomerService : ICustomerService
    {

        LMS.Common.Helper.IApiHelper _apiHelper;
        public CustomerService(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }
        public async Task<ResponseActionResult> ChangePhone(long customerID, long loanID, long createby, string phoneChange, string note)
        {
            var dataPost = new
            {
                CustomerID = customerID,
                LoanID = loanID,
                Createby = createby,
                Phone = phoneChange,
                Note = note
            };
            var url = $"{LMS.Common.Constants.ServiceHostInternal.CustomerService}{LMS.Common.Constants.ActionApiInternal.Customer_ChangePhone}";
            return await _apiHelper.ExecuteAsync(url: url.ToString(), RestSharp.Method.POST, dataPost);
        }
    }
}
