﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.SmartDialer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.RestClients
{
    public interface ISmartDialerGateway
    {
        Task<ResponseActionResult> ImportCampaign(LMS.Entites.Dtos.SmartDialer.ImportCampaignRequest request);
        Task<List<LMS.Entites.Dtos.SmartDialer.PendingOfCampaignResponse>> GetPendingOfCampaign(LMS.Entites.Dtos.SmartDialer.PendingOfCampaignRequest request);
        Task<List<LMS.Entites.Dtos.SmartDialer.CampaignDetail>> GetCampaigns();
        Task<ResponseActionResult> UpdateResource(LMS.Entites.Dtos.SmartDialer.UpdateResourceRequest request);
        Task<LMS.Entites.Dtos.SmartDialer.UpdateCsqResponse> UpdateCsq(LMS.Entites.Dtos.SmartDialer.UpdateCsqRequest request);
    }
    public class SmartDialerGateway : ISmartDialerGateway
    {
        readonly LMS.Common.Helper.IApiHelper _apiHelper;
        public SmartDialerGateway(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }
        public async Task<ResponseActionResult> ImportCampaign(ImportCampaignRequest request)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_SmartDialer_ImportCampaign}";
            var response = await _apiHelper.ExecuteAsync(url: url.ToString(), RestSharp.Method.POST, body: request);
            if (response == null)
            {
                response = new ResponseActionResult();
            }
            response.Data = request;
            return response;
        }
        public async Task<List<LMS.Entites.Dtos.SmartDialer.PendingOfCampaignResponse>> GetPendingOfCampaign(LMS.Entites.Dtos.SmartDialer.PendingOfCampaignRequest request)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_SmartDialer_GetPendingOfCampaign}";
            return await _apiHelper.ExecuteAsync<List<LMS.Entites.Dtos.SmartDialer.PendingOfCampaignResponse>>(url: url.ToString(), RestSharp.Method.POST, body: request);
        }
        public async Task<List<LMS.Entites.Dtos.SmartDialer.CampaignDetail>> GetCampaigns()
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_SmartDialer_GetCampaign}";
            return await _apiHelper.ExecuteAsync<List<LMS.Entites.Dtos.SmartDialer.CampaignDetail>>(url: url.ToString(), RestSharp.Method.GET);
        }
        public async Task<ResponseActionResult> UpdateResource(LMS.Entites.Dtos.SmartDialer.UpdateResourceRequest request)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_SmartDialer_UpdateResource}";
            var response = await _apiHelper.ExecuteAsync(url: url.ToString(), RestSharp.Method.POST, body: request);
            if (response == null)
            {
                response = new ResponseActionResult();
            }
            response.Data = request;
            return response;
        }
        public async Task<LMS.Entites.Dtos.SmartDialer.UpdateCsqResponse> UpdateCsq(LMS.Entites.Dtos.SmartDialer.UpdateCsqRequest request)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{LMS.Common.Constants.ActionApiInternal.OutGatewayApi_SmartDialer_UpdateCsq}";
            var response = await _apiHelper.ExecuteAsync<List<LMS.Entites.Dtos.SmartDialer.UpdateCsqResponse>>(url: url.ToString(), RestSharp.Method.POST, body: request);
            if (response == null || !response.Any())
            {
                return null;
            }
            var detailFirst = response.FirstOrDefault();
            if(detailFirst.StatusUpdate != (int)ResponseAction.Success)
            {
                return null;
            }
            return detailFirst;
        }
    }
}
