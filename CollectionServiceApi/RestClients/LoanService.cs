﻿using LMS.Entites.Dtos.LoanServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.RestClients
{
    public interface ILoanService
    {
        Task<MoneyNeedCloseLoan> GetMoneyNeedCloseLoanByLoanID(long loanID, DateTime closeDate);
    }
    public class LoanService : ILoanService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        public LoanService(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }
        public async Task<MoneyNeedCloseLoan> GetMoneyNeedCloseLoanByLoanID(long loanID, DateTime closeDate)
        {
            var dataPost = new
            {
                LoanID = loanID,
                CloseDate = closeDate.ToString(LMS.Common.Constants.TimaSettingConstant.DateTimeDayMonthYear)
            };
            var url = $"{LMS.Common.Constants.ServiceHostInternal.LoanService}{LMS.Common.Constants.ActionApiInternal.Loan_GetMoneyNeedCloseLoanByLoanID}";
            return await _apiHelper.ExecuteAsync<MoneyNeedCloseLoan>(url: url.ToString(), RestSharp.Method.POST, dataPost);
        }
    }
}
