﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserDepartmentController : Controller
    {
        private readonly IMediator _bus;
        public UserDepartmentController(IMediator bus)
        {
            this._bus = bus;
        }

        [HttpPost]
        [Route("CreateUserDepartmentTHN")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateUserDepartmentTHN(Commands.UserDepartment.CreateUserDepartmentTHNCommand request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("GetUserDepartmentTHNByDepartment")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetUserDepartmentTHNByDepartment(Queries.UserDepartment.GetUserDepartmentTHNByDepartmentQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpGet]
        [Route("GetPostionDeparmentUser/{UserID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetPostionDeparmentbUser(long UserID)
        {
            return await _bus.Send(new Queries.UserDepartment.GetPostionDeparmentbUserQuery
            {
                UserID = UserID
            });
        }
        [HttpPost]
        [Route("TeamOfManager")]
        public async Task<LMS.Common.Constants.ResponseActionResult> TeamOfManager(Queries.UserDepartment.TeamOfManagerQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpGet]
        [Route("GetStaffByLeader/{UserID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetStaffByLeader(long UserID)
        {
            return await _bus.Send(new Queries.UserDepartment.GetStaffByLeaderQuery
            {
                UserID = UserID
            });
        }
    }
}