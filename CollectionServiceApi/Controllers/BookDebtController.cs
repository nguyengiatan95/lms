﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookDebtController : ControllerBase
    {
        private readonly IMediator _bus;
        public BookDebtController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("CreateOfUpdateBookDebt")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateOfUpdateBookDebt(Commands.BookDebt.CreateBookDebtCommand request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("ListBookDebt")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ListBookDebt(Queries.BookDebt.GetBookDebtQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("UpdateStatusBookDebt")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetBookDebt(Commands.BookDebt.UpdateStatusBookDebtCommand request)
        {
            return await _bus.Send(request);
        }
    }
}
