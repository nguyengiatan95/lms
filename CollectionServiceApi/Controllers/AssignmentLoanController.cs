﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AssignmentLoanController : ControllerBase
    {
        private readonly IMediator _bus;
        public AssignmentLoanController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("LstLoanSplitSingle")]
        public async Task<LMS.Common.Constants.ResponseActionResult> LstLoanSplitSingle(Queries.AssignmentLoan.LstLoanSplitSingleQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("LstDetailsLoanSplitSingle")]
        public async Task<LMS.Common.Constants.ResponseActionResult> LstDetailsLoanSplitSingle(Queries.AssignmentLoan.GetLstDetailsLoanSplitSingleQuery request)
        {
            return await _bus.Send(request);
        }
    }
}
