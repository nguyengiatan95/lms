﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MetaController : ControllerBase
    {
        private readonly IMediator _bus;
        public MetaController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpGet]
        [Route("GetLstCity")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstCity()
        {
            return await _bus.Send(new Queries.Meta.GetLstCityQuery());
        }

        [HttpGet]
        [Route("GetDistrictByCityID/{cityID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetDistrictByCityID(long cityID)
        {
            return await _bus.Send(new Queries.Meta.GetDistrictByCityIDQuery()
            {
                CityID = cityID
            });
        }
        [HttpGet]
        [Route("GetRelativeFamily")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetRelativeFamilyLOS()
        {
            return await _bus.Send(new Queries.LOS.GetRelativeFamilyLOSQuery() { });
        }
        [HttpPost]
        [Route("GetLstTypeFileForm")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstTypeFileForm(Queries.Meta.GetLstTypeFileFormQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpGet]
        [Route("GetDepartmentTHN")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetDepartmentTHN()
        {
            return await _bus.Send(new Queries.Meta.GetDepartmentTHNQuery());
        }
        [HttpGet]
        [Route("GetWardByDistrictID/{DistrictID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetWardByDistrictID(long DistrictID)
        {
            return await _bus.Send(new Queries.Meta.GetWardByDistrictIDQuery()
            {
                DistrictID = DistrictID
            });
        }

        [HttpGet]
        [Route("GetPreparationType")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetPreparationType()
        {
            return await _bus.Send(new Queries.Meta.GetPreparationTypeQuery());
        }
        [HttpGet]
        [Route("ConfigHoldCodeForSmartDialer")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ConfigHoldCodeForSmartDialer()
        {
            return await _bus.Send(new Queries.Meta.ConfigHoldCodeForSmartDialerQuery());
        }
        [HttpPost]
        [Route("AddConfigHoldCodeForSmartDialer")]
        public async Task<LMS.Common.Constants.ResponseActionResult> AddConfigHoldCodeForSmartDialer(Commands.ConfigHoldCodeForSmartDialer.CreateConfigHoldCodeForSmartDialerCommand request)
        {
            return await _bus.Send(request);
        }
    }
}