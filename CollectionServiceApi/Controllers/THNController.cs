﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class THNController : Controller
    {
        private readonly IMediator _bus;
        public THNController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("HistoryComment")]
        public async Task<LMS.Common.Constants.ResponseActionResult> HistoryComment(Queries.LOS.HistoryCommentQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("ListVideo")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ListVideo(Queries.LOS.ListVideoQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("ListImages")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ListImages(Queries.LOS.ListImagesQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("GetTopFriendFacebook")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetTopFriendFacebook(Queries.AI.TopFriendFacebookQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("GetGPSLoanID")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetGPSLoanID(Queries.AI.GetGPSLoanIDQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("ShowHistoryComment")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ShowHistoryComment(Queries.AI.ShowHistoryCommentQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("SearchHistoryTransaction")]
        public async Task<LMS.Common.Constants.ResponseActionResult> SearchHistoryTransaction(Domain.Models.Invoice.RequestHistoryCustomerTopupModel model)
        {
            Queries.Invoice.SearchHistoryTransactionQuery request = new Queries.Invoice.SearchHistoryTransactionQuery
            {
                Model = model
            };
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("CreateRelationship")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateRelationship(Commands.LOS.CreateRelationshipCommand request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("DivideLoanExcel")]
        public async Task<LMS.Common.Constants.ResponseActionResult> DivideLoanExcel(Commands.AssignmentLoan.CreateAssignmentLoanCommand request)
        {
            return await _bus.Send(request);
        }
    }
}