﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoanController : ControllerBase
    {
        private readonly IMediator _bus;
        Services.IUserService _userServices;
        public LoanController(IMediator bus, Services.IUserService userServices)
        {
            this._bus = bus;
            _userServices = userServices;
        }

        [HttpPost]
        [Route("GetLstLoanInfoByCondition")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstLoanInfoByCondition(Domain.Models.Loan.RequestLoanItemViewModel request)
        {
            return await _bus.Send(new Queries.Loan.GetLstLoanInfoByConditionQuery
            {
                Model = request
            });
        }

        [HttpGet]
        [Route("GetLstPaymentScheduleByLoanID/{loanID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstPaymentScheduleByLoanID(long loanID)
        {
            return await _bus.Send(new Queries.Loan.GetLstPaymentScheduleByLoanIDQuery
            {
                LoanID = loanID
            });
        }

        [HttpPost]
        [Route("GetLstLoanInfoByKeySearch")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstLoanInfoByKeySearch(Queries.Loan.GetLstLoanInfoByKeySearchQuery request)
        {
            return await _bus.Send(request);
        }


        [HttpGet]
        [Route("GetHistoryCustomerChangePhone/{loanID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetHistoryCustomerChangePhone(long loanID)
        {
            return await _bus.Send(new Queries.Loan.GetHistoryCustomerChangePhoneQuery
            {
                LoanID = loanID
            });
        }

        [HttpGet]
        [Route("GetLoanCreditDetailLosByLoanID/{LoanID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLoanCreditDetailLos(long LoanID, [FromQuery] long userID)
        {
            var result = await _bus.Send(new Queries.Loan.GetLoanCreditDetailLosByLoanIDQuery
            {
                LoanID = LoanID,
                UserID = userID
            });
            return result;
        }


        [HttpPost]
        [Route("GetMotorcycleSeizure")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetMotorcycleSeizure(Queries.Loan.GetMotorcycleSeizureQuery request)
        {
            return await _bus.Send(request);
        }


        [HttpPost]
        [Route("CreateRequestLoanExemption")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateRequestLoanExemption(Commands.LoanExemption.CreateRequestLoanExemptionCommand request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("GetLstLoanExemption")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstLoanExemption(Queries.LoanExemption.GetLstLoanExemptionByConditionsQuery request)
        {
            request.UserID = _userServices.GetHeaderUserID();
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("UpdateStatusLoanExemption")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstLoanExemption(Commands.LoanExemption.UpdateStatusLoanExemptionCommand request)
        {
            request.CreateBy = _userServices.GetHeaderUserID();
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("UploadFileLoanExemption")]
        public async Task<LMS.Common.Constants.ResponseActionResult> UploadFileLoanExemption(Commands.LoanExemption.UploadFilesLoanExemptionCommand request)
        {
            request.CreateBy = _userServices.GetHeaderUserID();
            return await _bus.Send(request);
        }

        [HttpGet]
        [Route("GetDetailLoanExemptionByID/{loanExemptionID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetDetailLoanExemptionByID(long loanExemptionID)
        {
            var result = await _bus.Send(new Queries.LoanExemption.GetDetailLoanExemptionByIDQuery
            {
                LoanExemptionID = loanExemptionID
            });
            return result;
        }

        [HttpGet]
        [Route("GetHistoryLoanExemptionByLoanExemptionID/{loanExemptionID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetHistoryLoanExemptionByLoanExemptionID(long loanExemptionID)
        {
            var result = await _bus.Send(new Queries.LoanExemption.GetHistoryLoanExemptionByLoanExemptionIDQuery
            {
                LoanExemptionID = loanExemptionID
            });
            return result;
        }
        [HttpPost]
        [Route("CreateDebtRestructuring")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateDebtRestructuring(Commands.DebtRestructuringLoan.CreateDebtRestructuringLoanCommand request)
        {
            request.CreateBy = _userServices.GetHeaderUserID();
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("UpdateFileImageDebtRestructuring")]
        public async Task<LMS.Common.Constants.ResponseActionResult> UpdateFileImageDebtRestructuring(Commands.DebtRestructuringLoan.UpdateImageDebtRestructuringLoanCommad request)
        {
            request.CreateBy = _userServices.GetHeaderUserID();
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("UpdateStatusDebtRestructuring")]
        public async Task<LMS.Common.Constants.ResponseActionResult> UpdateStatusDebtRestructuring(Commands.DebtRestructuringLoan.UpdateStatusDebtRestructuringLoanCommand request)
        {
            request.CreateBy = _userServices.GetHeaderUserID();
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("GetLstDebtRestructuringLoan")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstDebtRestructuringLoan(Queries.DebtRestructuringLoan.GetLstDebtRestructuringByConditionQuery request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("GetDetailDebtRestructuringLoanByID")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetDetailDebtRestructuringLoanByID(Queries.DebtRestructuringLoan.GetDetailDebtRestructuringByIDQuery request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("GetLstLoanInfoByPhoneDaily")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstLoanInfoByPhoneDaily(Queries.Loan.GetLstLoanInfoByPhoneDailyQuery request)
        {
            return await _bus.Send(request);
        }
    }
}
