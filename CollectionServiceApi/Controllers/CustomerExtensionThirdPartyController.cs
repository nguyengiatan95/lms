﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerExtensionThirdPartyController : Controller
    {
        private readonly IMediator _bus;
        public CustomerExtensionThirdPartyController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("CreateCustomerExtensionThirdParty")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateHoldCustomerMoney(Commands.CustomerExtensionThirdParty.CreateCustomerExtensionThirdPartyCommand request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("SearchCustomerExtensionThirdParty")]
        public async Task<LMS.Common.Constants.ResponseActionResult> SearchCustomerExtensionThirdParty(Queries.CustomerExtensionThirdParty.SearchCustomerExtensionThirdPartyQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpGet]
        [Route("TrandataCallProxy")]
        public async Task<LMS.Common.Constants.ResponseActionResult> TrandataCallProxy()
        {
            return await _bus.Send(new Commands.CustomerExtensionThirdParty.TrandataCallProxyCommand());
        }
    }
}
