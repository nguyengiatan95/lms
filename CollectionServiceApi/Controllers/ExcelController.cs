﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CollectionServiceApi.Domain.Models.Excel;
using CollectionServiceApi.Domain.Models.Report;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExcelController : ControllerBase
    {
        private readonly IMediator _bus;
        Services.IUserService _userServices;
        public ExcelController(IMediator bus, Services.IUserService userServices)
        {
            this._bus = bus;
            _userServices = userServices;
        }
        [HttpPost]
        [Route("AddRequestExcel")]
        public async Task<LMS.Common.Constants.ResponseActionResult> AddRequestExcel(Commands.Excel.AddExcelCommand request)
        {
            return await _bus.Send(new Commands.Excel.AddExcelCommand
            {
                CreateBy = request.CreateBy,
                TypeReport = request.TypeReport
            });
        }

        [HttpPost]
        [Route("GetByTraceID")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetByTraceID(Domain.Tables.RequestGetExcelByTraceID request)
        {

            return await _bus.Send(new Queries.ExcelReport.GetExcelReportByTraceIDQuery
            {
                CreateBy = request.CreateBy,
                TraceIDRequest = request.TraceIDRequest,
                UserIDLogin = _userServices.GetHeaderUserID()
            });
        }

        [HttpPost]
        [Route("GetHistoryExportFile")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetHistoryExportFile(Queries.ExcelReport.GetHistoryExportFileQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("CreateFormLoanExemptionInterest")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateFormLoanExemptionInterest(Domain.Models.Excel.RequestFormLoanExemptionModel request)
        {
            return await _bus.Send(new Commands.Excel.CreateRequestFormLoanExemptionInterestCommand()
            {
                LoanExemptionInterestDetail = request
            });
        }

        [HttpPost]
        [Route("GetFormLoanExemptionInterest")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetFormLoanExemptionInterest(Domain.Tables.RequestGetExcelByTraceID request)
        {

            return await _bus.Send(new Queries.ExcelReport.GetFormLoanExemptionInterestByTraceIDQuery
            {
                CreateBy = request.CreateBy,
                TraceIDRequest = request.TraceIDRequest,
                UserIDLogin = _userServices.GetHeaderUserID(),
                TypeReport = (int)LMS.Common.Constants.ExcelReport_TypeReport.ExemptionInterest
            });
        }

        [HttpPost]
        [Route("GetHistoryCustomerTopup")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetHistoryCustomerTopup(Domain.Models.Invoice.RequestHistoryCustomerTopupModel model)
        {
            model.UserIDLogin = _userServices.GetHeaderUserID();
            model.TypeReport = (int)LMS.Common.Constants.ExcelReport_TypeReport.HistoryCustomerTopup;
            return await _bus.Send(new Commands.Excel.CreateRequestGetHistoryCustomerTopupCommand
            {
                Model = model
            });
        }
        [HttpPost]
        [Route("GetHistoryTransactionLoanByCustomer")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetHistoryTransactionLoanByCustomer(Domain.Models.Loan.RequestHistoryTransactionLoanByCustomer model)
        {
            model.UserIDLogin = _userServices.GetHeaderUserID();
            model.TypeReport = (int)LMS.Common.Constants.ExcelReport_TypeReport.HistoryTransactionLoanByCustomer;
            return await _bus.Send(new Commands.Excel.CreateRequestGetHistoryTransactionCustomerCommand
            {
                Model = model
            });
        }

        [HttpPost]
        [Route("GetHistoryTrackingLoanInMonth")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetHistoryTrackingLoanInMonth(Domain.Models.Excel.RequestHistroryTrackingLoan model)
        {
            model.UserIDLogin = _userServices.GetHeaderUserID();
            return await _bus.Send(new Commands.Excel.CreateGetHistoryTrackingLoanInMonthCommand
            {
                Model = model
            });
        }

        [HttpPost]
        [Route("GetReportLoanDebtDaily")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetReportLoanDebtDaily(Commands.Excel.CreateRequestGetReportLoanDebtDailyCommand model)
        {
            model.CreateBy = _userServices.GetHeaderUserID();
            model.TypeReport = (int)LMS.Common.Constants.ExcelReport_TypeReport.ReportLoanDebtDaily;
            return await _bus.Send(model);
        }
        [HttpPost]
        [Route("CreateReportDay")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateReportDay(RequestCreateReportDayPlan request)
        {
            return await _bus.Send(new Commands.Excel.CreateReportDayPlanCommand
            {
                Model =request
            });
        }
        [HttpPost]
        [Route("ExcelHistoryInteraction")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ExcelHistoryInteraction(RequestExcelHistoryInteraction request)
        {
            return await _bus.Send(new Commands.Excel.ExcelHistoryInteractionCommand
            {
                Model = request
            });
        }
        [HttpPost]
        [Route("CreateExportDataDocs")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateExportDataDocs(LMS.Entites.Dtos.Document.RequestExportDataDocs request)
        {
            return await _bus.Send(new Commands.Excel.CreateExportDataDocsCommand
            {
                Model = request
            });
        }
    }
}