﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReasonController : Controller
    {
        private readonly IMediator _bus;
        private readonly Services.IUserService _userServices;
        public ReasonController(IMediator bus,
            Services.IUserService userServices)
        {
            this._bus = bus;
            _userServices = userServices;
        }
        [HttpGet]
        [Route("GetReason")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetReason([FromQuery] int platform)
        {
            var request = new Queries.Reason.GetReasonQuery
            {
                Platform = platform,
                UserIDLogin = _userServices.GetHeaderUserID()
            };
            return await _bus.Send(request);
        }
    }
}