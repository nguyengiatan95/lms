﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AppTHNController : ControllerBase
    {
        private readonly IMediator _bus;
        public AppTHNController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("ListLoanBeDivided")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ListLoanBeDivided(Queries.AppTHN.ListLoanBeDividedQuery request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("GetSummaryPlanLoanHandleByStaff")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetSummaryPlanLoanHandleByStaff(Queries.AppTHN.GetSummaryPlanLoanHandleByStaffQuery request)
        {
            return await _bus.Send(request);
        }

        [HttpGet]
        [Route("GetTeamLead/{DepartmentID}")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetTeamLead(long DepartmentID)
        {
            return await _bus.Send(new Queries.AppTHN.GetTeamLeadQuery()
            {
                DepartmentID = DepartmentID
            });
        }
        [HttpPost]
        [Route("GetSummaryEmployeeForPlanHandleLoanByLstDepartmentID")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetSummaryEmployeeForPlanHandleLoanByLstDepartmentID(Queries.UserDepartment.GetSummaryEmployeeHandleLoanByLstDeptIDQuery request)
        {
            return await _bus.Send(request);
        }
    }
}
