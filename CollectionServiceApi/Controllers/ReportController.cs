﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly IMediator _bus;
        public ReportController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("GetReportEmployeeWithLoan")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetReportEmployeeWithLoan(Queries.Report.GetReportEmployeeWithLoanQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("GetLstLoanEmployeeHandleDaily")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstLoanEmployeeHandleDaily(Queries.Report.GetLstLoanEmployeeHandleDailyQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpGet]
        [Route("ProcessSummaryEmployeeHandleLoan")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ProcessSummaryEmployeeHandleLoan()
        {
            return await _bus.Send(new Commands.Report.ProcessSummaryEmployeeHandleLoanCommand());
        }

        [HttpPost]
        [Route("ProcessReportLoanDebtType")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ProcessReportLoanDebtType()
        {
            return await _bus.Send(new Commands.Report.ProcessReportLoanDebtTypeCommand());
        }
        [HttpPost]
        [Route("ReportByDebtGroup")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ReportByDebtGroup(Queries.Report.ReportByDebtGroupQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("ReportDayPlan")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ReportDayPlan(Queries.Report.ReportDayPlanQuery request)
        {
            return await _bus.Send(request);
        }
    }
}