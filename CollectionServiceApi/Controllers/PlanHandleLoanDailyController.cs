﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlanHandleLoanDailyController : ControllerBase
    {
        private readonly IMediator _bus;
        public PlanHandleLoanDailyController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("CreatePlanHandleLoanDaily")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreatePlanHandleLoanDaily(Commands.PlanHandleLoanDaily.CreatePlanHandleLoanDailyCommand request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("PlanHandleLoanDaily")]
        public async Task<LMS.Common.Constants.ResponseActionResult> PlanHandleLoanDaily(Queries.PlanHandleLoanDaily.PlanHandleLoanDailyQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("PlanHandleLoanDailyDetails")]
        public async Task<LMS.Common.Constants.ResponseActionResult> PlanHandleLoanDailyDetails(Queries.PlanHandleLoanDaily.PlanHandleLoanDailyDetailsQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("UpdatePlanHandleLoanDaily")]
        public async Task<LMS.Common.Constants.ResponseActionResult> UpdatePlanHandleLoanDaily(Commands.PlanHandleLoanDaily.UpdatePlanHandleLoanDailyCommand request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("MovePlanHandleLoanDaily")]
        public async Task<LMS.Common.Constants.ResponseActionResult> UpdateStatusPlanHandleLoanDaily(Commands.PlanHandleLoanDaily.UpdateStatusPlanHandleLoanDailyCommand request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("DeletePlanHandleLoanDaily")]
        public async Task<LMS.Common.Constants.ResponseActionResult> DeletePlanHandleLoanDaily(Commands.PlanHandleLoanDaily.DeletePlanHandleLoanDailyCommand request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("CheckInOut")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CheckInOut(Commands.PlanHandleLoanDaily.CheckInOutCommand request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("GetResultCheckOutFromAI")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetResultCheckOutFromAI(Commands.PlanHandleLoanDaily.ProcessGetResultCheckOutFromAICommand request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("AppPlanDaily")]
        public async Task<LMS.Common.Constants.ResponseActionResult> AppPlanDaily(Queries.PlanHandleLoanDaily.AppPlanDailyQuery request)
        {
            return await _bus.Send(request);
        }

    }
}
