﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HoldCustomerMoneyController : Controller
    {
        private readonly IMediator _bus;
        public HoldCustomerMoneyController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("CreateHoldCustomerMoney")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateHoldCustomerMoney(Commands.HoldCustomerMoney.CreateHoldCustomerMoneyCommand request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("UpdateHoldCustomerMoney")]
        public async Task<LMS.Common.Constants.ResponseActionResult> UpdateHoldCustomerMoney(Commands.HoldCustomerMoney.UpdateHoldCustomerMoneyCommand request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("ListHoldCustomerMoney")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ListHoldCustomerMoney(Queries.HoldCustomerMoney.ListHoldCustomerMoneyQuery request)
        {
            return await _bus.Send(request);
        }
    }
}