﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SmartDialerController : ControllerBase
    {
        private readonly IMediator _bus;
        readonly Services.IUserService _userServices;
        public SmartDialerController(IMediator bus, Services.IUserService userServices)
        {
            this._bus = bus;
            _userServices = userServices;
        }

        [HttpPost]
        [Route("UpdateResultFromCallCenter")]
        public async Task<LMS.Common.Constants.ResponseActionResult> UpdateResultFromCallCenter(Domain.Models.SmartDialer.CallResultFromCallCenterRequest models)
        {
            var request = new Commands.SmartDialer.UpdateResultFromCallCenterCommand
            {
                Models = models
            };
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("PushPhoneDailyToCallCenter")]
        public async Task<LMS.Common.Constants.ResponseActionResult> PushPhoneDailyToCallCenter(Commands.SmartDialer.PushPhoneDailyToCallCenterCommand request)
        {
            return await _bus.Send(request);
        }

        [HttpGet]
        [Route("ProcessingThePhoneForDailyCall")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ProcessingThePhoneForDailyCall()
        {
            var request = new Commands.SmartDialer.ProcessingThePhoneForDailyCallCommand { };
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("GetConfigUserMapCampaign")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetConfigUserMapCampaign(Queries.SmartDialer.GetConfigUserMapCampaignQuery request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("UpdateResourceCampaign")]
        public async Task<LMS.Common.Constants.ResponseActionResult> UpdateResourceCampaign(Commands.SmartDialer.UpdateResourceCampaignCommand request)
        {
            request.UserIDLogin = _userServices.GetHeaderUserID();
            return await _bus.Send(request);
        }

        [HttpGet]
        [Route("GetUserCiscoForCampaign")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetUserCiscoForCampaign()
        {
            var request = new Queries.SmartDialer.GetUserCiscoForCampaignQuery();
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("GetPlanHandleCallDailyByConditions")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetPlanHandleCallDailyByConditions(Queries.SmartDialer.GetPlanHandleCallDailyByConditionsQuery request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("PendingCampaingManual")]
        public async Task<LMS.Common.Constants.ResponseActionResult> PendingCampaingManual(Commands.SmartDialer.PendingCampaingManualCommand request)
        {
            return await _bus.Send(request);
        }

    }
}
