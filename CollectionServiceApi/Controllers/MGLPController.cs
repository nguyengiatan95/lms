﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MGLPController : ControllerBase
    {
        private readonly IMediator _bus;
        Services.IUserService _userServices;
        public MGLPController(IMediator bus, Services.IUserService userServices)
        {
            this._bus = bus;
            _userServices = userServices;
        }

        [HttpPost]
        [Route("GetSettingByConditions")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetSettingByConditions(Queries.SettingInterestReduction.GetSettingByConditionsQuery request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("CreateSettingInterestReduction")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateSettingInterestReduction(List<Domain.Models.MGL.RequestCreateSetting> request)
        {
            return await _bus.Send(new Commands.MGL.CreateSettingInterestReductionCommand()
            {
                model = request,
                UserID = _userServices.GetHeaderUserID()
                //UserID = 0
            });
        }

        [HttpPost]
        [Route("GetSettingByBookDebtID")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetSettingByBookDebtID(Queries.SettingInterestReduction.GetSettingByBookDebtIDQuery request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("CreateSettingTotalMoney")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateSettingTotalMoney(Commands.MGL.CreateSettingTotalMoneyCommand request)
        {
            request.UserID = _userServices.GetHeaderUserID();
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("GetSettingMoneyByApprovalLevelBookDebtID")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetSettingMoneyByApprovalLevelBookDebtID(Queries.SettingMoney.GetSettingMoneyByApprovalLevelBookDebtIDQuery request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("TranferAgCloseLoanExemption")]
        public async Task<LMS.Common.Constants.ResponseActionResult> TranferAgCloseLoanExemption(Commands.LoanExemption.TranferAgCloseLoanExemptionCommand request)
        {
            return await _bus.Send(request);
        }

        [HttpGet]
        [Route("CancelLoanExemptionByExpired")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CancelLoanExemptionByExpired()
        {
            Commands.LoanExemption.CancelLoanExemptionByExpiredCommand request = new Commands.LoanExemption.CancelLoanExemptionByExpiredCommand();
            return await _bus.Send(request);
        }
    }
}
