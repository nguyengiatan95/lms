﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PreparationCollectionLoanController : ControllerBase
    {
        private readonly IMediator _bus;
        public PreparationCollectionLoanController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("CreatePreparationCollectionLoan")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreatePreparationCollectionLoan(Commands.PreparationCollectionLoan.CreatePreparationCollectionLoanCommand request)
        {
            return await _bus.Send(request);
        }
        [HttpGet]
        [Route("GetPreparationCollectionLoan")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetPreparationCollectionLoan(long LoanID)
        {
            var result = await _bus.Send(new Queries.PreparationCollectionLoan.GetPreparationCollectionLoanQuery
            {
                LoanID = LoanID
            });
            return result;
        }
        [HttpGet]
        [Route("SuggestedAmountLoan")]
        public async Task<LMS.Common.Constants.ResponseActionResult> SuggestedAmountLoan(long LoanID)
        {
            var result = await _bus.Send(new Queries.PreparationCollectionLoan.SuggestedAmountLoanQuery
            {
                LoanID = LoanID
            });
            return result;
        }
    }
}