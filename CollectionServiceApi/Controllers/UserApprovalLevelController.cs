﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserApprovalLevelController : ControllerBase
    {
        private readonly IMediator _bus;
        public UserApprovalLevelController(IMediator bus)
        {
            this._bus = bus;
        }

        [HttpPost]
        [Route("GetUserApprovalLevelConditions")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetUserApprovalLevelConditions(Queries.UserApprovalLevel.GetUserApprovalLevelConditionsQuery request)
        {
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("CreateOrUpdateUserAppovalLevel")]
        public async Task<LMS.Common.Constants.ResponseActionResult> UpdateApprovalLevelBookDebt(Commands.UserApprovalLevel.CreateOrUpdateUserAppovalLevelCommand request)
        {
            return await _bus.Send(request);
        }
    }
}
