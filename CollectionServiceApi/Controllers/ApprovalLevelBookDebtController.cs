﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApprovalLevelBookDebtController : ControllerBase
    {
        private readonly IMediator _bus;
        public ApprovalLevelBookDebtController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("CreateOfUpdateApprovalLevelBookDebt")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateOfUpdateApprovalLevelBookDebt(Commands.ApprovalLevelBookDebt.CreateApprovalLevelBookDebtCommand request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("GetListApprovalLevelBookDebt")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetListApprovalLevelBookDebt(Queries.ApprovalLevelBookDebt.GetListApprovalLevelBookDebtQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("UpdateApprovalLevelBookDebt")]
        public async Task<LMS.Common.Constants.ResponseActionResult> UpdateApprovalLevelBookDebt(Commands.ApprovalLevelBookDebt.UpdateApprovalLevelBookDebtCommand request)
        {
            return await _bus.Send(request);
        }
        [HttpGet]
        [Route("GetListTreeViewApprovalLevelBookDebt")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetListTreeViewApprovalLevelBookDebt()
        {
            return await _bus.Send(new Queries.ApprovalLevelBookDebt.GetListTreeViewApprovalLevelBookDebtQuery());
        }
    }
}
