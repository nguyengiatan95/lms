﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SendSmsController : ControllerBase
    {
        private readonly IMediator _bus;
        public SendSmsController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("CreateSendSms")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateSendSms(Commands.SendSms.CreateSendSmsCommand request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("GetLstSendSms")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstSendSms(Queries.SendSms.GetLstSendSmsQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("UpdateStatusSendSms")]
        public async Task<LMS.Common.Constants.ResponseActionResult> UpdateStatusSendSms(Commands.SendSms.UpdateStatusSendSmsCommand request)
        {
            return await _bus.Send(request);
        }
    }
}
