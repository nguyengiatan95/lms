﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SmsController : ControllerBase
    {
        private readonly IMediator _bus;
        public SmsController(IMediator bus)
        {
            this._bus = bus;
        }

        [HttpPost]
        [Route("GetLstSmsInfoByCondition")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstSmsInfoByCondition(Queries.Sms.GetLstSmsInfoByConditionQuery request)
        {
            return await _bus.Send(request);
        }
    }

}
