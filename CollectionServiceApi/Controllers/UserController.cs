﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IMediator _bus;
        Services.IUserService _userServices;
        public UserController(IMediator bus, Services.IUserService userServices)
        {
            this._bus = bus;
            _userServices = userServices;
        }
        [HttpGet]
        [Route("GetServiceCallInfoByUserID")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetServiceCallInfoByUserID()
        {
            return await _bus.Send(new Queries.MapUserCall.GetServiceCallInfoByUserIDQuery() { UserID = _userServices.GetHeaderUserID() });
        }

        [HttpPost]
        [Route("GetLinkCallForPhone")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLinkCallForPhone(Queries.MapUserCall.GetMapUserCallInfoByPhoneQuery request)
        {
            request.UserID = _userServices.GetHeaderUserID();
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("GetLstEmployees")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstEmployees(Queries.User.GetLstEmployeeQuery request)
        {
            //var request = new Queries.User.GetLstEmployeeQuery();
            request.UserLoginID = _userServices.GetHeaderUserID();           
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("ChangePhoneCustomerByLoanID")]
        public async Task<LMS.Common.Constants.ResponseActionResult> ChangePhoneCustomerByLoanID(Commands.ChangePhone.ChangePhoneCustomerByLoanIDCommand request)
        {
            request.CreateBy = _userServices.GetHeaderUserID();
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("SettingUserPhone")]
        public async Task<LMS.Common.Constants.ResponseActionResult> SettingUserPhone(Commands.User.SettingUserPhoneCommand request)
        {
            request.UserID = _userServices.GetHeaderUserID();
            return await _bus.Send(request);
        }

        [HttpPost]
        [Route("GetPositionUser")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetPositionUser(Queries.User.GetPositionUserQuery request)
        {
            request.UserID = _userServices.GetHeaderUserID();
            return await _bus.Send(request);
        }

    }
}
