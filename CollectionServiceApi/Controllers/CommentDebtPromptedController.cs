﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CollectionServiceApi.Domain.Models.CommentDebtPrompted;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentDebtPromptedController : ControllerBase
    {
        private readonly IMediator _bus;
        public CommentDebtPromptedController(IMediator bus)
        {
            this._bus = bus;
        }
        [HttpPost]
        [Route("GetLstCommentDebtPromptedByLoanID")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstCommentDebtPromptedByLoanID(Queries.CommentDebtPrompted.GetLstCommentDebtPromptedByLoanIDQuery request)
        {
            return await _bus.Send(request);
        }
        [HttpPost]
        [Route("CreateCommentDebtPrompted")]
        public async Task<LMS.Common.Constants.ResponseActionResult> CreateCommentDebtPrompted(Commands.CommentDebtPrompted.CreateCommentDebtPromptedCommand request)
        {
            var resultComment = await _bus.Send(request);
            if (resultComment.Result == (int)LMS.Common.Constants.ResponseAction.Success)
            {
                CommentDebtPromptedInsertSuccessModel insertDetail = (CommentDebtPromptedInsertSuccessModel)resultComment.Data;
                _ = _bus.Send(new Commands.PlanHandleLoanDaily.UpdateStatusPlanHandleLoanDailyByCommentCommand
                {
                    UserID = request.UserID,
                    LoanID = insertDetail.LoanID
                });
                _ = _bus.Send(new Commands.SmartDialer.UpdatePhoneAfterCommentCommand
                {
                    CustomerID = insertDetail.CustomerID,
                    ReasonCode = request.ReasonCode
                });
            }
            return resultComment;
        }
        [HttpPost]
        [Route("GetLstHistoryInteractDebtPrompted")]
        public async Task<LMS.Common.Constants.ResponseActionResult> GetLstHistoryInteractDebtPrompted(Queries.CommentDebtPrompted.HistoryInteractDebtPromptedQuery request)
        {
            return await _bus.Send(request);
        }
    }
}