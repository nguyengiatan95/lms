﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CollectionServiceApi.Services;
using LMS.Entites.Dtos.CollectionServices.QueueCallAPI;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CollectionServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QueueCallAPIController : ControllerBase
    {
        private readonly IMediator _bus;
        IQueueCallAPIManager _queueCallAPIManager;
        public QueueCallAPIController(IMediator bus, IQueueCallAPIManager queueCallAPIManager)
        {
            this._bus = bus;
            _queueCallAPIManager = queueCallAPIManager;
        }

        [HttpPost]
        [Route("InsertConfirmSuspendedMoney")]
        public async Task<LMS.Common.Constants.ResponseActionResult> InsertConfirmSuspendedMoney(LMSRequestConfirmSuspendedMoneyTHN request)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            long ID = await _queueCallAPIManager.InsertConfirmSuspendedMoney(request.InvoiceTimaID, (int)request.UserID
                , request.CreateDate, request.PathImage,
            request.Note, request.LmsConfirmID, request.LoanID, request.CustomerID);
            if (ID > 0)
            {
                response.SetSucces();
                response.Data = ID;
            }
            return response;
        }
    }
}