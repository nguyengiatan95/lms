﻿using LMS.Common.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi
{
    public class RedisCachedServiceHelper
    {
        //readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigHoldCodeForSmartDialer> _configHoldCodeForSmartDialerTab;
        readonly Lazy<ConnectionMultiplexer> _lazyConnection;
        readonly ILogger<RedisCachedServiceHelper> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public RedisCachedServiceHelper(IServiceScopeFactory serviceScopeFactory, RedisCacheSetting redisCacheSetting, ILogger<RedisCachedServiceHelper> logger)
        {
            //_userTab = serviceScopeFactory.CreateScope().ServiceProvider.GetRequiredService<LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser>>();
            _configHoldCodeForSmartDialerTab = serviceScopeFactory.CreateScope().ServiceProvider.GetRequiredService<LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigHoldCodeForSmartDialer>>();
            ConfigurationOptions configuration = new ConfigurationOptions
            {
                AbortOnConnectFail = redisCacheSetting.AbortConnect,
                ConnectTimeout = redisCacheSetting.ConnectTimeout,
                Password = redisCacheSetting.Password
            };
            configuration.EndPoints.Add(redisCacheSetting.Host, redisCacheSetting.Port);
            _lazyConnection = new Lazy<ConnectionMultiplexer>(() => ConnectionMultiplexer.Connect(configuration));
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        private IConnectionMultiplexer RedisConnection => _lazyConnection.Value;
        public async Task<Domain.Models.SmartDialer.ConfigHoldCodeForSmartDialerModel> GetSmartDialerReasonCodeHoldToCall()
        {
            Domain.Models.SmartDialer.ConfigHoldCodeForSmartDialerModel result = null;
            TimeSpan expiration = TimeSpan.FromSeconds(10);
            string lockKey = "lock:GetSmartDialerReasonCodeHoldToCall".ToLower();
            var db = this.RedisConnection.GetDatabase();
            RedisValue token = Environment.MachineName;
            bool wait = true;
            while (wait)
            {
                if (db.LockTake(lockKey, token, expiration))
                {
                    try
                    {
                        // you have the lock do work
                        var values = await db.StringGetAsync(new RedisKey[] { RuleCollectionServiceHelper.KeyRedistConfigHoldCodeSmartDialer });
                        if (values != null && !values.Any())
                        {
                            result = _common.ConvertJSonToObjectV2<Domain.Models.SmartDialer.ConfigHoldCodeForSmartDialerModel>(values.First());
                        }
                        else
                        {
                            var config = (await _configHoldCodeForSmartDialerTab.WhereClause(x => x.Status == (int)LMS.Common.Constants.StatusCommon.Active).QueryAsync()).FirstOrDefault();
                            if (config != null && config.ConfigHoldCodeForSmartDialerID > 0)
                            {
                                result = new Domain.Models.SmartDialer.ConfigHoldCodeForSmartDialerModel
                                {
                                    SkipTime = config.SkipTime,
                                    LstReasonCode = config.ReasonCodes.Split(',').Select(x => x.ToLower()).ToList()
                                };
                                await db.StringSetAsync(RuleCollectionServiceHelper.KeyRedistConfigHoldCodeSmartDialer, _common.ConvertObjectToJSonV2(result), expiry: TimeSpan.FromMinutes(3));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"GetSmartDialerReasonCodeHoldToCall|ex={ex.Message}-{ex.StackTrace}");
                    }
                    finally
                    {
                        db.LockRelease(lockKey, token);
                        wait = false;
                    }
                }
            }
            return result;
        }
    }
}
