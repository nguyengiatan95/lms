using CollectionServiceApi.Commands.PreparationCollectionLoan;
using CollectionServiceApi.KafkaEventHandlers;
using CollectionServiceApi.RestClients;
using CollectionServiceApi.Services;
using CollectionServiceApi.Validators;
using Confluent.Kafka;
using FluentValidation;
using FluentValidation.AspNetCore;
using LMS.Kafka.Consumer;
using LMS.Kafka.Interfaces;
using LMS.Kafka.Producer;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Steeltoe.Discovery.Client;
using Steeltoe.Discovery.Eureka;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CollectionServiceApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<KestrelServerOptions>(Configuration.GetSection("Kestrel"));
            services.AddDiscoveryClient(Configuration);
            services.AddServiceDiscovery(options => options.UseEureka());
            services.AddSingleton(Configuration);
            services.AddControllers().AddJsonOptions(opts => opts.JsonSerializerOptions.PropertyNamingPolicy = null);
            services.AddHttpContextAccessor();
            LMS.Common.Configuration.ConnectStringSetting.DefaultConnectString = Configuration["ConnectStringSetting:DefaultConnectString"];
            LMS.Entites.Dtos.AG.AGConstants.BaseUrl = Configuration["AGConfiguration:Url"];
            services.AddTransient(typeof(LMS.Common.DAL.ITableHelper<>), typeof(LMS.Common.DAL.TableHelper<>));
            services.AddMvc().AddFluentValidation();
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddTransient<LMS.Common.Helper.IApiHelper, LMS.Common.Helper.ApiHelper>();
            services.AddTransient<Services.ILoanManager, Services.LoanManager>();
            services.AddTransient<Services.IQueueCallAPIManager, Services.QueueCallAPIManager>();
            services.AddTransient<Services.IReportManager, Services.ReportManager>();
            services.AddRestClientsService();

            var phoneSetting = new Domain.Models.MapUserCall.PhoneSettingCareSoft();
            Configuration.GetSection(nameof(Domain.Models.MapUserCall.PhoneSettingCareSoft)).Bind(phoneSetting);
            services.AddSingleton(phoneSetting);

            services.AddTransient<Services.IUserService, Services.UserService>();
            services.AddTransient<Services.IServiceCallManager, Services.ServiceCallManager>();
            services.AddTransient<Queries.CustomerExtensionThirdParty.ITranDataMapingHelper, Queries.CustomerExtensionThirdParty.TranDataK03Maping>();
            services.AddTransient<Queries.CustomerExtensionThirdParty.ITranDataMapingHelper, Queries.CustomerExtensionThirdParty.TranDataK06Maping>();
            services.AddTransient<Queries.CustomerExtensionThirdParty.ITranDataMapingHelper, Queries.CustomerExtensionThirdParty.TranDataK21Maping>();
            services.AddTransient<Queries.CustomerExtensionThirdParty.ITranDataMapingHelper, Queries.CustomerExtensionThirdParty.TranDataK22Maping>();

            services.AddTransient<Queries.ExcelReport.IExcelExportDataHelper, Queries.ExcelReport.ExcelExportAssignLoanEmployeeDetail>();
            services.AddTransient<Queries.ExcelReport.IExcelExportDataHelper, Queries.ExcelReport.ExcelExportHistoryCustomerTopup>();
            services.AddTransient<Queries.ExcelReport.IExcelExportDataHelper, Queries.ExcelReport.ExcelExportHistoryTransactionLoanByCustomer>();
            services.AddTransient<Queries.ExcelReport.IExcelExportDataHelper, Queries.ExcelReport.ExcelExportReportLoanDebtType>();
            services.AddTransient<Queries.ExcelReport.IExcelExportDataHelper, Queries.ExcelReport.ExcelExportReportLoanDebtDaily>();
            services.AddTransient<Queries.ExcelReport.IExcelExportDataHelper, Queries.ExcelReport.ExcelExportReportDayPlan>();
            services.AddTransient<Queries.ExcelReport.IExcelExportDataHelper, Queries.ExcelReport.ExcelExcelHistoryInteraction>();

            services.AddTransient<Queries.ExcelReport.IExcelExportDataHelper, Queries.ExcelReport.PropertySeizure>();
            services.AddTransient<Queries.ExcelReport.IExcelExportDataHelper, Queries.ExcelReport.DebtRestructuring>();
            services.AddTransient<Queries.ExcelReport.IExcelExportDataHelper, Queries.ExcelReport.UnsecuredFinalDebtNotice>();
            services.AddTransient<Queries.ExcelReport.IExcelExportDataHelper, Queries.ExcelReport.NotifyPropertySeizure>();
            services.AddTransient<Queries.ExcelReport.IExcelExportDataHelper, Queries.ExcelReport.NotifyVoluntarilyHandingOverPropertye>();
            services.AddTransient<Queries.ExcelReport.IExcelExportDataHelper, Queries.ExcelReport.MGL>();
            services.AddTransient<IExportDataDocsManager, ExportDataDocsManager>();



            AddServiceKafka(services);
            var redisCacheSetting = new LMS.Common.Configuration.RedisCacheSetting();
            Configuration.GetSection(nameof(LMS.Common.Configuration.RedisCacheSetting)).Bind(redisCacheSetting);
            services.AddSingleton(redisCacheSetting);
            services.AddSingleton(typeof(RedisCachedServiceHelper), typeof(RedisCachedServiceHelper));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors(options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseAuthorization();
            app.UseDiscoveryClient();
            //Add our new middleware to the pipeline
            app.UseMiddleware<LMS.Common.Helper.RequestResponseLoggingMiddleware>();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        public void AddServiceKafka(IServiceCollection services)
        {
            var clientConfig = new ClientConfig()
            {
                BootstrapServers = Configuration["Kafka:ClientConfigs:BootstrapServers"]
            };

            var producerConfig = new ProducerConfig(clientConfig);
            var consumerConfig = new ConsumerConfig(clientConfig)
            {
                GroupId = Configuration["spring:application:name"],
                EnableAutoCommit = false,
                AutoOffsetReset = AutoOffsetReset.Earliest,
                StatisticsIntervalMs = 5000,
                SessionTimeoutMs = 6000
            };

            services.AddSingleton(producerConfig);
            services.AddSingleton(consumerConfig);

            services.AddSingleton(typeof(IKafkaProducer<,>), typeof(KafkaProducer<,>));
            services.AddSingleton(typeof(IKafkaConsumer<,>), typeof(KafkaConsumer<,>));

            services.AddHostedService<KafkaServiceConsumer>();

            services.AddScoped<IKafkaHandler<string, LMS.Kafka.Messages.Customer.CustomerBalanceToupSuccessInfo>, KafkaEventHandlers.CustomerBalanceTopupInfoHandler>();
            services.AddScoped<IKafkaHandler<string, LMS.Kafka.Messages.Loan.LoanCutPeriodSuccessInfo>, ConsumLoanCutPeriodSuccessHandler>();
        }
    }
}
