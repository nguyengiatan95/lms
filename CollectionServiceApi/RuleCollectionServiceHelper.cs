﻿using LMS.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi
{
    public class RuleCollectionServiceHelper
    {
        public static List<int> LstStatusLoanLending = new List<int> { (int)Loan_Status.Lending, (int)Loan_Status.CloseIndemnifyLoanInsurance };
        public static List<long> LstDepartmentID = new List<long> {
            (long)Department_ID.THN_Call,
            (long)Department_ID.THN_MN,
            (long)Department_ID.THN_MN};
        public static List<long> LstPositionID = new List<long> { (long)UserDepartment_PositionID.Staff, (long)UserDepartment_PositionID.TeamLead, (long)UserDepartment_PositionID.Manager, (int)UserDepartment_PositionID.Director };
        public const string LoanPaymentOverDue = "Trễ hạn kỳ";
        public const string LoanContractExpired = "Quá hạn";
        public const string LoanLending = "Đang vay";
        public const string LoanClosed = "Tất toán";
        public const string LoanPaymentDonePeriods = "Cắt kỳ";
        public const string LoanPaymentNotDonePeriods = "Chưa cắt kỳ";
        public const string TranDataKeyCodeK03 = "K03";
        public const string TranDataKeyCodeK06 = "K06";
        public const string TranDataKeyCodeK21 = "K21";
        public const string TranDataKeyCodeK22 = "K22";
        public static string GetStatusLoanByNextDate(int statusLoan, DateTime loanNextDate, DateTime loanToDate)
        {
            if (LstStatusLoanLending.Contains(statusLoan))
            {
                if (loanToDate.Date < DateTime.Now.Date)
                {
                    return LoanContractExpired;
                }
                if (loanNextDate.Date >= DateTime.Now.Date)
                {
                    return LoanLending;
                }
                return LoanPaymentOverDue;
            }
            return LoanClosed;
        }
        public static List<string> LstReasonCodeForCustomer = new List<string> { "PTP", "F_PTP", "C_PTP_IN", "C_PTP_OUT", "WFP", "F_WFP", "C_WFP" };
        public static List<string> AppTHNReason_PTP = new List<string> { "PTP", "F_PTP", "C_PTP_IN", "C_PTP_OUT", "WFP", "F_WFP", "C_WFP" };

        public static Dictionary<string, string> DictCodeTranData = new Dictionary<string, string>
        {
            {TranDataKeyCodeK03, "KYC 3 địa chỉ khách hàng thường xuyên lui tới"},
            {TranDataKeyCodeK06, "KYC 3 số điện thoại thường xuyên liên hệ"},
            {TranDataKeyCodeK21, "Cung cấp SĐT & các thông tin khác từ CMT"},
            {TranDataKeyCodeK22, "Cung cấp thông tin CMT từ SĐT"}
        };

        public const int TypeFileExcel = 1;
        public const int TypeFileForm = 2;
        public const int TypeFileStep = 200;
        public static THN_DPD GetTypeBebtLoanByNextDate_v2(DateTime nextDate, DateTime firstDay)
        {
            var dpdCurrent = firstDay.Date.Subtract(nextDate).Days;
            // có 15 nhóm từ B0 - > B13+
            // bước nhảy 30 ngày
            if (dpdCurrent < 1)
            {
                return THN_DPD.B0;
            }
            int i = dpdCurrent / 30;
            if (i > 13)
            {
                return THN_DPD.B14;
            }
            int j = dpdCurrent % 30;
            if (j == 0)
            {
                return (THN_DPD)i;
            }
            if (i + 1 > 13)
            {
                return THN_DPD.B14;
            }
            return (THN_DPD)(i + 1);
        }

        public const long MaxMoneyCanBeIngored = 5000; // số tiền có thể bỏ qua cho MGL

        public const string AppActionTypeCheckIN = "CHKIN";
        public const string AppActionTypeCheckOUT = "CHKOUT";
        public static List<string> LstActionCheckINOUT = new List<string> { AppActionTypeCheckIN, AppActionTypeCheckOUT };
        public static List<string> LocationCheckInOut = new List<string> { "home", "office", "other" };

        // id các sản phẩm liên quan đến xe máy
        public static List<long> LstProductIDForDebtRestructuring = new List<long> { 2, 5, 28 };
        // cấu trúc khoản vay
        public const int MinDPDBOMForDebtRestructuring = 30; // tính từ 31 trở lên
        public const int MaxDPDBOMForDebtRestructuring = 151; // tính đến 150

        // giãn nợ
        public const int MaxDPDBOMForDebtRelief = 91; // tính đến 90

        public const string ReasonCodeForWeb = "C_";
        public const string ReasonCodeForApp = "F_";

        public const int SmartDialerMaxCountRetry = 3;
        public const int SmartDialerDayOverBorderCallRef = 25;
        public const int SmartDialerMaxDPDCallRef = 3;
        public static List<string> SmartDialerLstReasonCodeHoldToCall = new List<string>();
        public static string PrefixRedisEnvironmentName = "EnvironmentName";
        public static string KeyRedistConfigHoldCodeSmartDialer = $"{PrefixRedisEnvironmentName}:CollectionServiceApi:TblConfigHoldCodeForSmartDialer";
    }
}
