﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Meta
{
    public class ConfigHoldCodeForSmartDialerQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class ConfigHoldCodeForSmartDialerQueryHandler : IRequestHandler<ConfigHoldCodeForSmartDialerQuery, ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigHoldCodeForSmartDialer> _configHoldCodeForSmartDialerTab;
        readonly ILogger<ConfigHoldCodeForSmartDialerQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public ConfigHoldCodeForSmartDialerQueryHandler(
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigHoldCodeForSmartDialer> configHoldCodeForSmartDialerTab,
        ILogger<ConfigHoldCodeForSmartDialerQueryHandler> logger)
        {
            _configHoldCodeForSmartDialerTab = configHoldCodeForSmartDialerTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(ConfigHoldCodeForSmartDialerQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var configHoldCodeForSmartDialer = (await _configHoldCodeForSmartDialerTab.QueryAsync()).FirstOrDefault();
                response.Data = configHoldCodeForSmartDialer;
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ConfigHoldCodeForSmartDialerQueryHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
