﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Meta
{
    public class GetLstCityQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class GetLstCityQueryHandler : IRequestHandler<GetLstCityQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCity> _cityTab;
        ILogger<GetLstCityQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetLstCityQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblCity> cityTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDeparmentTab,
            ILogger<GetLstCityQueryHandler> logger)
        {
            _cityTab = cityTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetLstCityQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstCityInfos = await _cityTab.QueryAsync();
                List<Domain.Models.SelectItem> lstCityResponse = new List<Domain.Models.SelectItem>();
                lstCityInfos = lstCityInfos.OrderBy(x => x.Position);
                foreach (var item in lstCityInfos)
                {
                    lstCityResponse.Add(new Domain.Models.SelectItem
                    {
                        Text = $"{item.Name}",
                        Value = item.CityID
                    });
                }
                response.Data = lstCityResponse;
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLstCityQueryHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
