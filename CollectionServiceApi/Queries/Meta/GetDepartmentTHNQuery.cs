﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Meta
{
    public class GetDepartmentTHNQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class GetDepartmentTHNQueryHandler : IRequestHandler<GetDepartmentTHNQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> _departmentTab;
        ILogger<GetDepartmentTHNQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetDepartmentTHNQueryHandler(
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> departmentTab,
        ILogger<GetDepartmentTHNQueryHandler> logger)
        {
            _departmentTab = departmentTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetDepartmentTHNQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstDepartment = await _departmentTab.WhereClause(x => x.AppID == (int)Menu_AppID.THN && x.ParentID == 0
                                                                        && x.Status == (int)Department_Status.Active).QueryAsync();

                response.Data = lstDepartment;
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetDepartmentTHNQueryHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
