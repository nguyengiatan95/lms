﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Meta
{
    public class GetPreparationTypeQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class GetPreparationTypeQueryHandler : IRequestHandler<GetPreparationTypeQuery, ResponseActionResult>
    {
        readonly ILogger<GetPreparationTypeQueryHandler> _logger;
        public GetPreparationTypeQueryHandler(
            ILogger<GetPreparationTypeQueryHandler> logger)
        {
            _logger = logger;
        }

        public async Task<ResponseActionResult> Handle(GetPreparationTypeQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                List<Domain.Models.SelectItem> dataResponse = new List<Domain.Models.SelectItem>();
                var prepareTypes = Enum.GetValues(typeof(LMS.Common.Constants.PreparationCollectionLoan_PrepareType)).Cast<LMS.Common.Constants.PreparationCollectionLoan_PrepareType>();
                foreach (var item in prepareTypes)
                {
                    dataResponse.Add(new Domain.Models.SelectItem
                    {
                        Text = $"{item.GetDescription()}",
                        Value = (int)item
                    });
                }
                response.Data = dataResponse;
                response.SetSucces();
                await Task.Delay(1);
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetPreparationTypeQueryHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}