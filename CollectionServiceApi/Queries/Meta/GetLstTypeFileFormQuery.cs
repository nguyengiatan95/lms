﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Meta
{
    public class GetLstTypeFileFormQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int TypeFile { get; set; }
    }
    public class GetLstTypeFileFormQueryHandler : IRequestHandler<GetLstTypeFileFormQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCity> _cityTab;
        ILogger<GetLstTypeFileFormQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetLstTypeFileFormQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblCity> cityTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDeparmentTab,
            ILogger<GetLstTypeFileFormQueryHandler> logger)
        {
            _cityTab = cityTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetLstTypeFileFormQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    var lstTypeReport = ((ExcelReport_TypeReport[])Enum.GetValues(typeof(ExcelReport_TypeReport)))
                                .Select(x => new Domain.Models.SelectItem { Text = x.GetDescription(), Value = (int)x }).ToList();
                    List<Domain.Models.SelectItem> lstCityResponse = new List<Domain.Models.SelectItem>();
                    switch (request.TypeFile)
                    {
                        case RuleCollectionServiceHelper.TypeFileExcel:
                            lstCityResponse = lstTypeReport.Where(x => x.Value <= RuleCollectionServiceHelper.TypeFileStep).ToList();
                            break;
                        case RuleCollectionServiceHelper.TypeFileForm:
                            lstCityResponse = lstTypeReport.Where(x => x.Value > RuleCollectionServiceHelper.TypeFileStep).ToList();
                            break;
                    }
                    response.Data = lstCityResponse;
                    response.SetSucces();
                }
                catch (Exception ex)
                {
                    _logger.LogError($"GetLstCityQueryHandler|ex={ex.Message}-{ex.StackTrace}");
                }
                return response;
            });           
        }
    }
}
