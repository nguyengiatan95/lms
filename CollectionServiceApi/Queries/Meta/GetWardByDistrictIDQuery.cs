﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace CollectionServiceApi.Queries.Meta
{
    public class GetWardByDistrictIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long DistrictID { get; set; }
    }
    public class GetWardByDistrictIDQueryHandler : IRequestHandler<GetWardByDistrictIDQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblWard> _wardTab;
        ILogger<GetWardByDistrictIDQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetWardByDistrictIDQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblWard> wardTab,
            ILogger<GetWardByDistrictIDQueryHandler> logger)
        {
            _wardTab = wardTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetWardByDistrictIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstCityInfos = await _wardTab.WhereClause(x => x.DistrictID == request.DistrictID).QueryAsync();
                List<Domain.Models.SelectItem> lstWardResponse = new List<Domain.Models.SelectItem>();
                foreach (var item in lstCityInfos)
                {
                    lstWardResponse.Add(new Domain.Models.SelectItem
                    {
                        Text = $"{item.Name}",
                        Value = item.WardID
                    });
                }
                response.Data = lstWardResponse;
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetWardByDistrictIDQueryHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
