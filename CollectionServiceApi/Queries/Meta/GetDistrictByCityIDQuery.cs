﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Meta
{
    public class GetDistrictByCityIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long CityID { get; set; }
    }
    public class GetDistrictByCỉtyIDQueryHandler : IRequestHandler<GetDistrictByCityIDQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDistrict> _districtTab;
        ILogger<GetDistrictByCỉtyIDQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetDistrictByCỉtyIDQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblDistrict> districtTab,
            ILogger<GetDistrictByCỉtyIDQueryHandler> logger)
        {
            _districtTab = districtTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetDistrictByCityIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstCityInfos = await _districtTab.WhereClause(x => x.CityID == request.CityID).QueryAsync();
                List<Domain.Models.SelectItem> lstCityResponse = new List<Domain.Models.SelectItem>();
                foreach (var item in lstCityInfos)
                {
                    lstCityResponse.Add(new Domain.Models.SelectItem
                    {
                        Text = $"{item.Name}",
                        Value = item.DistrictID
                    });
                }
                response.Data = lstCityResponse;
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetDistrictByCỉtyIDQueryHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
