﻿using CollectionServiceApi.Domain;
using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.AG;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.CommentDebtPrompted
{
    public class GetLstCommentDebtPromptedByLoanIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string ToDate { get; set; }
    }
    public class GetLstCommentDebtPromptedByLoanIDQueryHandler : IRequestHandler<GetLstCommentDebtPromptedByLoanIDQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> _commentDebtPromptedTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblReason> _reasonTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<GetLstCommentDebtPromptedByLoanIDQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetLstCommentDebtPromptedByLoanIDQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> commentDebtPromptedTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblReason> reasonTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            ILogger<GetLstCommentDebtPromptedByLoanIDQueryHandler> logger)
        {
            _commentDebtPromptedTab = commentDebtPromptedTab;
            _reasonTab = reasonTab;
            _userTab = userTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetLstCommentDebtPromptedByLoanIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstData = new List<GetLstCommentDebtPromptedByLoanIDItem>();
                int totalCount = 0;
                DateTime? toDate = null;
                if (!string.IsNullOrEmpty(request.ToDate))
                {
                    toDate = DateTime.ParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                }
                _commentDebtPromptedTab.WhereClause(x => x.LoanID == request.LoanID);
                if (toDate.HasValue)
                {
                    toDate = toDate.Value.AddDays(1).Date;
                    _commentDebtPromptedTab.WhereClause(x => x.CreateDate < toDate.Value);
                }
                var lstCommentDebtPrompted = (await _commentDebtPromptedTab.QueryAsync());
                if (lstCommentDebtPrompted != null)
                {
                    // không có phân trang
                    //totalCount = lstCommentDebtPrompted.Count();
                    //lstCommentDebtPrompted = lstCommentDebtPrompted.OrderByDescending(x => x.CreateDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                    var lstReasonID = new List<long>();
                    List<long> lstUserID = new List<long>();
                    foreach (var item in lstCommentDebtPrompted)
                    {
                        lstReasonID.Add(item.ReasonCodeID);
                        lstUserID.Add(item.UserID);
                    }
                    lstReasonID = lstReasonID.Distinct().ToList();
                    lstUserID = lstUserID.Distinct().ToList();
                    var lstReasonInforTask = _reasonTab.WhereClause(x => lstReasonID.Contains(x.ReasonID)).QueryAsync();
                    var lstUserNameInforTask = _userTab.WhereClause(x => lstUserID.Contains(x.UserID)).QueryAsync();
                    await Task.WhenAll(lstReasonInforTask, lstUserNameInforTask);
                    var dictReasonInfors = lstReasonInforTask.Result.ToDictionary(x => x.ReasonID, x => x);
                    var dictUserNameInfors = lstUserNameInforTask.Result.ToDictionary(x => x.UserID, x => x);
                    foreach (var item in lstCommentDebtPrompted)
                    {
                        lstData.Add(new GetLstCommentDebtPromptedByLoanIDItem
                        {
                            CreateDate = item.CreateDate,
                            FullName = dictUserNameInfors.GetValueOrDefault(item.UserID)?.UserName,
                            Comment = item.Comment,
                            ReasonCode = dictReasonInfors.GetValueOrDefault(item.ReasonCodeID)?.ReasonCode,
                            FileImage = _common.ConvertJSonToObjectV2<List<ImageLosUpload>>(item.FileImage),
                            StrActionAddress = item.ActionAddress > 0 ? ((CommentDebtPrompted_ActionAddress)item.ActionAddress).GetDescription() : "",
                            StrActionPeron = item.ActionPerson > 0 ? ((CommentDebtPrompted_ActionPerson)item.ActionPerson).GetDescription() : "",
                        });
                        totalCount++;
                    }
                }
                response.SetSucces();
                response.Data = lstData.OrderByDescending(x => x.CreateDate);
                response.Total = totalCount;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLstCommentDebtPromptedByLoanIDQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
