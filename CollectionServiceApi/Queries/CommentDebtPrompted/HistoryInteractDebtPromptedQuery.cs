﻿using CollectionServiceApi.Domain;
using CollectionServiceApi.Domain.Models.CommentDebtPrompted;
using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.AG;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;


namespace CollectionServiceApi.Queries.CommentDebtPrompted
{
    public class HistoryInteractDebtPromptedQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string KeySearch { get; set; }
        public long DeparmentID { get; set; }
        public long ManagerID { get; set; }
        public long LenderID { get; set; }
        public long UserID { get; set; }
        public long Staff { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class HistoryInteractDebtPromptedQueryHandler : IRequestHandler<HistoryInteractDebtPromptedQuery, ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> _commentDebtPromptedTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblReason> _reasonTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCity> _cityTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> _userDepartmentTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> _departmentTab;
        readonly ILogger<HistoryInteractDebtPromptedQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public HistoryInteractDebtPromptedQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> commentDebtPromptedTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblReason> reasonTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCity> cityTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDepartmentTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> departmentTab,
            ILogger<HistoryInteractDebtPromptedQueryHandler> logger)
        {
            _commentDebtPromptedTab = commentDebtPromptedTab;
            _reasonTab = reasonTab;
            _userTab = userTab;
            _loanTab = loanTab;
            _lenderTab = lenderTab;
            _customerTab = customerTab;
            _cityTab = cityTab;
            _userDepartmentTab = userDepartmentTab;
            _departmentTab = departmentTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(HistoryInteractDebtPromptedQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                int totalCount = 0;
                bool searchLoan = false;
                var dictCustomerInfor = new Dictionary<long, Domain.Tables.TblCustomer>();
                var dictLoanInfor = new Dictionary<long, Domain.Tables.TblLoan>();
                var objShop = new Domain.Tables.TblLender();
                var objLoan = new Domain.Tables.TblLoan();
                var objCity = new Domain.Tables.TblCity();
                var lstResult = new List<HistoryInteractDebtPrompted>();


                var fromDate = string.IsNullOrEmpty(request.FromDate) ? DateTime.Now.AddMonths(-3) : DateTime.ParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture);
                var toDate = string.IsNullOrEmpty(request.ToDate) ? DateTime.Now : DateTime.ParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture);
                toDate = toDate.AddDays(1);
                fromDate = fromDate.AddSeconds(-1);
                _commentDebtPromptedTab.WhereClause(x => x.CreateDate > fromDate && x.CreateDate < toDate);
                var lstUserID = new List<long>();
                if (request.Staff != (int)StatusCommon.SearchAll)
                    _commentDebtPromptedTab.WhereClause(x => x.UserID == request.Staff);
                else
                {
                    if (request.LenderID != (int)StatusCommon.SearchAll)  //lấy danh sách các nhân viên team leader
                    {
                        lstUserID = (await _userDepartmentTab.WhereClause(x => x.ParentUserID == request.LenderID &&
                                                                        x.DepartmentID ==request.DeparmentID &&
                                                                        x.PositionID == (int)UserDepartment_PositionID.Staff).QueryAsync()).Select(x => x.UserID).ToList();
                        lstUserID.Add(request.LenderID);
                    }
                    else
                    {
                        if (request.ManagerID != (int)StatusCommon.SearchAll)//lấy danh sách các nhân viên thuộc trưởng phòng
                        {
                            lstUserID.Add(request.ManagerID);
                            lstUserID = (await _userDepartmentTab.WhereClause(x => x.DepartmentID == request.DeparmentID &&
                                                                               x.PositionID == (int)UserDepartment_PositionID.Staff ||
                                                                               x.PositionID == (int)UserDepartment_PositionID.TeamLead).QueryAsync()).Select(x => x.UserID).ToList();
                        }
                        else// lấy tất cả theo user login
                            lstUserID = await GetUserID(request.UserID);
                    }
                    _commentDebtPromptedTab.WhereClause(x => lstUserID.Contains((long)x.UserID));
                }
                if (!string.IsNullOrEmpty(request.KeySearch))
                {
                    searchLoan = true;
                    if (Regex.Match(request.KeySearch, TimaSettingConstant.PrefixContractCode, RegexOptions.IgnoreCase).Success)
                        _loanTab.WhereClause(x => x.ContactCode == request.KeySearch);
                    else
                    {
                        if (request.KeySearch.Length == TimaSettingConstant.MaxLengthPhone && Regex.Match(request.KeySearch, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success)
                            _customerTab.WhereClause(x => x.Phone == request.KeySearch);
                        else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternNumberCard, RegexOptions.IgnoreCase).Success)
                            _customerTab.WhereClause(x => x.NumberCard == request.KeySearch);
                        else
                            _customerTab.WhereClause(x => x.FullName == request.KeySearch);

                        dictCustomerInfor = (await _customerTab.QueryAsync()).ToDictionary(x => x.CustomerID, x => x);
                        if (dictCustomerInfor.Keys.Count < 1)
                        {
                            response.SetSucces();
                            response.Total = 0;
                            return response;
                        }
                    }
                }
                if (dictCustomerInfor.Keys.Count > 0)
                {
                    _loanTab.WhereClause(x => dictCustomerInfor.Keys.Contains((long)x.CustomerID));
                }
                if (searchLoan)
                {
                    dictLoanInfor = (await _loanTab.QueryAsync()).ToDictionary(x => x.LoanID, x => x);
                }
                if (dictLoanInfor.Keys.Count > 0)
                    _commentDebtPromptedTab.WhereClause(x => dictLoanInfor.Keys.Contains(x.LoanID));

                var lstCommentDebtPrompted = (await _commentDebtPromptedTab.QueryAsync());
                totalCount = lstCommentDebtPrompted.Count();
                if (totalCount > 0)
                {
                    lstCommentDebtPrompted = lstCommentDebtPrompted.OrderByDescending(x => x.CreateDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                    int pageSize = TimaSettingConstant.MaxItemQuery;
                    int numberRow = lstCommentDebtPrompted.Count() / pageSize + 1;
                    for (int i = 1; i <= numberRow; i++)
                    {
                        var lstCommentSkip = lstCommentDebtPrompted.Skip((i - 1) * pageSize).Take(pageSize).ToList();
                        var lstLoanID = new List<long>();
                        var lstCreateBy = new List<long>();
                        var lstShop = new List<long>();
                        var lstCity = new List<long>();
                        foreach (var item in lstCommentSkip)
                        {
                            lstLoanID.Add(item.LoanID);
                            lstCreateBy.Add(item.UserID);
                        }
                        lstCreateBy = lstCreateBy.Distinct().ToList();
                        lstLoanID = lstLoanID.Distinct().ToList();

                        var dictLoanTask = (await _loanTab.SelectColumns(x => x.LoanID, x => x.CityID, x => x.ConsultantShopID, x => x.LoanCreditIDOfPartner, x => x.ContactCode,
                                                                         x => x.CustomerName, x => x.TotalMoneyDisbursement, x => x.TotalMoneyCurrent,
                                                                         x => x.LoanTime, x => x.LoanTime).WhereClause(x => lstLoanID.Contains(x.LoanID)).QueryAsync());
                        foreach (var item in dictLoanTask)
                        {
                            lstShop.Add(item.ConsultantShopID);
                            lstCity.Add(item.CityID);
                        }
                        var dictCityInfosTask = _cityTab.WhereClause(x => lstCity.Contains(x.CityID)).QueryAsync();
                        var dictLenderInfosTask = _lenderTab.WhereClause(x => lstShop.Contains(x.LenderID)).QueryAsync();
                        var dictUserInfosTask = _userTab.WhereClause(x => lstCreateBy.Contains(x.UserID)).QueryAsync();
                        await Task.WhenAll(dictCityInfosTask, dictLenderInfosTask, dictUserInfosTask);


                        var dictLoan = dictLoanTask.ToDictionary(x => x.LoanID, x => x);
                        var dictCity = dictCityInfosTask.Result.ToDictionary(x => x.CityID, x => x);
                        var dictShop = dictLenderInfosTask.Result.ToDictionary(x => x.LenderID, x => x);
                        var dictUserInfor = dictUserInfosTask.Result.ToDictionary(x => x.UserID, x => x);
                        foreach (var item in lstCommentSkip)
                        {
                            var objResult = new HistoryInteractDebtPrompted();
                            var obUser = dictUserInfor.GetValueOrDefault((long)item.UserID);
                            objLoan = dictLoan.GetValueOrDefault((long)item.LoanID);

                            objResult.LoanID = item.LoanID;
                            objResult.DebitReminderCode = item.ReasonCode;
                            objResult.AppointmentDate = item.AppointmentDate;
                            objResult.Comment = item.Comment;
                            objResult.CreateDateComment = item.CreateDate;
                            objResult.UserFullName = obUser?.FullName;
                            objResult.UserName = obUser?.UserName;

                            if (objLoan != null)
                            {
                                objShop = dictShop.GetValueOrDefault(objLoan.ConsultantShopID);
                                objCity = dictCity.GetValueOrDefault(objLoan.CityID);

                                objResult.LoanCreditID = objLoan?.LoanCreditIDOfPartner;
                                objResult.ContacCode = objLoan?.ContactCode;
                                objResult.CustomerName = objLoan?.CustomerName;
                                objResult.Province = objCity?.Name;
                                objResult.DisbursedAmount = objLoan?.TotalMoneyDisbursement;
                                objResult.CurrentOutstandingBalance = objLoan?.TotalMoneyCurrent;
                                objResult.LoanTime = objLoan?.LoanTime;
                                objResult.ShopName = objShop?.FullName;
                            }
                            lstResult.Add(objResult);
                        }
                    }
                }
                response.SetSucces();
                response.Data = lstResult;
                response.Total = totalCount;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"HistoryInteractDebtPromptedQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task<List<long>> GetUserID(long UserID)
        {
            var lstUserID = new List<long>();
            var objUserDepartment = (await _userDepartmentTab.WhereClause(x => x.UserID == UserID).QueryAsync()).FirstOrDefault();
            lstUserID.Add(UserID);
            if (objUserDepartment != null)
            {
                switch ((UserDepartment_PositionID)objUserDepartment.PositionID)
                {
                    case UserDepartment_PositionID.Director:
                        var lstDepartment = (await _departmentTab.WhereClause(x => x.AppID == (int)Menu_AppID.THN).QueryAsync()).Select(x => x.DepartmentID).ToList();
                        lstUserID = (await _userDepartmentTab.WhereClause(x => lstDepartment.Contains(x.DepartmentID)).QueryAsync()).Select(x => x.UserID).ToList();
                        break;
                    case UserDepartment_PositionID.Manager:
                        lstUserID = (await _userDepartmentTab.WhereClause(x => x.DepartmentID == objUserDepartment.DepartmentID &&
                                                                           x.PositionID == (int)UserDepartment_PositionID.Staff ||
                                                                           x.PositionID == (int)UserDepartment_PositionID.TeamLead).QueryAsync()).Select(x => x.UserID).ToList();
                        break;
                    case UserDepartment_PositionID.TeamLead:
                        lstUserID = (await _userDepartmentTab.WhereClause(x => x.DepartmentID == objUserDepartment.DepartmentID
                                                                          && x.ParentUserID == objUserDepartment.UserID
                                                                          && x.PositionID == (int)UserDepartment_PositionID.Staff).QueryAsync()).Select(x => x.UserID).ToList();
                        break;
                }
            }
            return lstUserID;
        }
    }
}
