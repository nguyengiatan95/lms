﻿using CollectionServiceApi.Domain.Models.MapUserCall;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.MapUserCall
{
    public class GetMapUserCallInfoByPhoneQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string Phone { get; set; }
        public long UserID { get; set; }
        public string CallOutID { get; set; }
    }
    public class GetMapUserCallInfoByPhoneQueryHandler : IRequestHandler<GetMapUserCallInfoByPhoneQuery, LMS.Common.Constants.ResponseActionResult>
    {
        Services.IServiceCallManager _serviceCallManager;
        ILogger<GetMapUserCallInfoByPhoneQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetMapUserCallInfoByPhoneQueryHandler(Services.IServiceCallManager serviceCallManager,
            ILogger<GetMapUserCallInfoByPhoneQueryHandler> logger)
        {
            _serviceCallManager = serviceCallManager;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetMapUserCallInfoByPhoneQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response = await _serviceCallManager.GetLinkCall(request.UserID, request.Phone, request.CallOutID);
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetMapUserCallInfoByUserIDQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

    }
}
