﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.CollectionServices.MapUserCall;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.MapUserCall
{
    public class GetServiceCallInfoByUserIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserID { get; set; }
    }
    public class GetServiceCallInfoByUserIDQueryHandler : IRequestHandler<GetServiceCallInfoByUserIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblMapUserCall> _mapUserCallTab;
        ILogger<GetServiceCallInfoByUserIDQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetServiceCallInfoByUserIDQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblMapUserCall> mapUserCallTab,
            ILogger<GetServiceCallInfoByUserIDQueryHandler> logger)
        {
            _mapUserCallTab = mapUserCallTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetServiceCallInfoByUserIDQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    var userCallInfos = _mapUserCallTab.WhereClause(x => x.UserID == request.UserID && x.Status == (int)StatusCommon.Active).Query();
                    if (userCallInfos == null || !userCallInfos.Any())
                    {
                        response.Message = MessageConstant.UserNotConfigurationServiceCall;
                        return response;
                    }
                    var userCallDetail = userCallInfos.First();
                    ServiceCallModel serviceCallInfo = new ServiceCallModel()
                    {
                        TypeCall = userCallDetail.TypeCall
                    };
                    if (!string.IsNullOrEmpty(userCallDetail.JsonExtra))
                    {
                        switch ((MapUserCall_TypeCall)userCallDetail.TypeCall)
                        {
                            case MapUserCall_TypeCall.Caresoft:
                                Domain.Tables.UserCallCareSoftExtra careSoftInfo = _common.ConvertJSonToObjectV2<Domain.Tables.UserCallCareSoftExtra>(userCallDetail.JsonExtra);
                                serviceCallInfo.IpPhone = careSoftInfo.IPPhone;
                                break;
                            case MapUserCall_TypeCall.Cisco:
                                Domain.Tables.UserCallCiscoExtra ciscoInfo = _common.ConvertJSonToObjectV2<Domain.Tables.UserCallCiscoExtra>(userCallDetail.JsonExtra);
                                serviceCallInfo.CiscoUserName = ciscoInfo.Username;
                                serviceCallInfo.CiscoPassword = ciscoInfo.Password;
                                serviceCallInfo.CiscoExtension = ciscoInfo.Extension;
                                break;
                        }
                        response.SetSucces();
                    }                    
                    response.Data = serviceCallInfo;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"GetServiceCallInfoByUserIDQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                }
                return response;
            });            
        }
    }
}
