﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.SmartDialer
{
    public class GetConfigUserMapCampaignQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class GetConfigUserMapCampaignQueryHandler : IRequestHandler<GetConfigUserMapCampaignQuery, ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleCallDaily> _planHandleCallDailyTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigUserMapSmartDialer> _configUserMapSmartDialerTab;
        readonly ILogger<GetConfigUserMapCampaignQueryHandler> _logger;
        //readonly LMS.Common.Helper.Utils _common;
        readonly RestClients.ISmartDialerGateway _smartDialerGateway;
        public GetConfigUserMapCampaignQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleCallDaily> planHandleCallDailyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigUserMapSmartDialer> configUserMapSmartDialerTab,
            RestClients.ISmartDialerGateway smartDialerGateway,
            ILogger<GetConfigUserMapCampaignQueryHandler> logger)
        {
            _logger = logger;
            //_common = new LMS.Common.Helper.Utils();
            _planHandleCallDailyTab = planHandleCallDailyTab;
            _smartDialerGateway = smartDialerGateway;
            _configUserMapSmartDialerTab = configUserMapSmartDialerTab;
        }

        public async Task<ResponseActionResult> Handle(GetConfigUserMapCampaignQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            response.SetSucces();
            List<Domain.Models.SmartDialer.ConfigUserMapCampaignModle> lstResult = new List<Domain.Models.SmartDialer.ConfigUserMapCampaignModle>();
            response.Data = lstResult;
            try
            {
                var lstConfigTask = _configUserMapSmartDialerTab.QueryAsync();
                var lstCampaignResposneTask = _smartDialerGateway.GetCampaigns();
                await Task.WhenAll(lstConfigTask, lstCampaignResposneTask);
                var lstConfig = lstConfigTask.Result;
                var lstCampaignResposne = lstCampaignResposneTask.Result;
                if (lstConfig == null || !lstConfig.Any())
                {
                    lstConfig = new List<Domain.Tables.TblConfigUserMapSmartDialer>();
                }
                foreach (var item in lstCampaignResposne)
                {
                    var configDetail = new Domain.Models.SmartDialer.ConfigUserMapCampaignModle
                    {
                        CampaignID = item.CampaignId,
                        CampaignName = item.CampaignName,
                        Status = (int)StatusCommon.Active,
                        StatusName = StatusCommon.Active.GetDescription(),
                        ModifyDate = DateTime.Now
                    };
                    var userAssigned = lstConfig.FirstOrDefault(x => x.CampaignID == item.CampaignId && x.Status == (int)StatusCommon.Active);
                    if (userAssigned != null)
                    {
                        configDetail.UserNameCisco = userAssigned.UserNameCisco;
                        configDetail.ModifyDate = userAssigned.ModiyDate;
                        configDetail.UserID = userAssigned.UserID;
                        configDetail.WrapupTime = userAssigned.WrapupTime;
                    }
                    lstResult.Add(configDetail);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetConfigUserMapCampaignQueryHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
