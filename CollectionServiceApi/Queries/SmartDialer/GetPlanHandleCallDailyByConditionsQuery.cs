﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.SmartDialer
{
    public class GetPlanHandleCallDailyByConditionsQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string CallDate { get; set; }
        public string KeySearch { get; set; }
        public long UserID { get; set; }
        public int Status { get; set; } = (int)LMS.Common.Constants.StatusCommon.SearchAll;
        public int CallResult { get; set; } = (int)LMS.Common.Constants.StatusCommon.SearchAll;
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 10;
    }
    public class GetPlanHandleCallDailyByConditionsQueryHandler : IRequestHandler<GetPlanHandleCallDailyByConditionsQuery, ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleCallDaily> _planHandleCallDailyTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigUserMapSmartDialer> _configUserMapSmartDialerTab;
        readonly ILogger<GetPlanHandleCallDailyByConditionsQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public GetPlanHandleCallDailyByConditionsQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleCallDaily> planHandleCallDailyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigUserMapSmartDialer> configUserMapSmartDialerTab,
            ILogger<GetPlanHandleCallDailyByConditionsQueryHandler> logger
            )
        {
            _planHandleCallDailyTab = planHandleCallDailyTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _customerTab = customerTab;
            _loanTab = loanTab;
            _configUserMapSmartDialerTab = configUserMapSmartDialerTab;
        }
        public async Task<ResponseActionResult> Handle(GetPlanHandleCallDailyByConditionsQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            DateTime callDate = DateTime.Now;
            List<Domain.Models.SmartDialer.PlanHandleCallDailyViewModel> lstResult = new List<Domain.Models.SmartDialer.PlanHandleCallDailyViewModel>();
            response.Data = lstResult;
            try
            {
                List<long> lstCustomerID = new List<long>();
                if (!string.IsNullOrEmpty(request.CallDate))
                {
                    callDate = DateTime.ParseExact(request.CallDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                }
                _planHandleCallDailyTab.WhereClause(x => x.CallDate == callDate.Date);
                if (request.UserID > 0)
                {
                    _planHandleCallDailyTab.WhereClause(x => x.UserID == request.UserID);
                }
                if (request.Status != (int)LMS.Common.Constants.StatusCommon.SearchAll)
                {
                    _planHandleCallDailyTab.WhereClause(x => x.Status == request.Status);
                }
                if (request.CallResult != (int)LMS.Common.Constants.StatusCommon.SearchAll)
                {
                    _planHandleCallDailyTab.WhereClause(x => x.CallResult == request.CallResult);
                }
                if (!string.IsNullOrEmpty(request.KeySearch))
                {
                    if (long.TryParse(request.KeySearch, out long contracCodeID) && request.KeySearch.Length < TimaSettingConstant.MaxLengthPhone - 1)
                    {
                        request.KeySearch = $"{TimaSettingConstant.PrefixContractCode}{contracCodeID}";
                        _loanTab.WhereClause(x => x.ContactCode == request.KeySearch);
                    }
                    else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternContractCode, RegexOptions.IgnoreCase).Success)
                    {
                        _loanTab.WhereClause(x => x.ContactCode == request.KeySearch);
                    }
                    else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternContractCodeLOSLoanCreditID, RegexOptions.IgnoreCase).Success)
                    {
                        var loanCreditID = long.Parse(request.KeySearch.Replace(TimaSettingConstant.PatternContractCodeLOS, "", StringComparison.OrdinalIgnoreCase));
                        _loanTab.WhereClause(x => x.LoanCreditIDOfPartner == loanCreditID);
                    }
                    else
                    {
                        if (request.KeySearch.Length == TimaSettingConstant.MaxLengthPhone && Regex.Match(request.KeySearch, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success)
                        {
                            var lstCustomerIDsTask = await _customerTab.SelectColumns(x => x.CustomerID).WhereClause(x => x.Phone == request.KeySearch).QueryAsync();
                            lstCustomerID = lstCustomerIDsTask.Select(x => x.CustomerID).ToList();
                        }
                        else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternNumberCard, RegexOptions.IgnoreCase).Success)
                        {
                            var lstCustomerIDsTask = await _customerTab.SelectColumns(x => x.CustomerID).WhereClause(x => x.NumberCard == request.KeySearch).QueryAsync();
                            lstCustomerID = lstCustomerIDsTask.Select(x => x.CustomerID).ToList();
                        }
                        else
                        {
                            _loanTab.WhereClause(x => x.CustomerName.Contains(request.KeySearch))
                                    .WhereClause(x => RuleCollectionServiceHelper.LstStatusLoanLending.Contains((int)x.Status));
                        }
                    }

                    if (lstCustomerID.Count == 0)
                    {
                        lstCustomerID = (await _loanTab.SelectColumns(x => x.CustomerID).QueryAsync()).Select(x => (long)x.CustomerID).ToList();
                    }
                    if (lstCustomerID.Count == 0)
                    {
                        return response;
                    }
                }
                if (lstCustomerID.Count > 0)
                {
                    _planHandleCallDailyTab.WhereClause(x => lstCustomerID.Contains(x.CustomerID));
                }
                var lstPlanData = await _planHandleCallDailyTab.QueryAsync();
                if (lstPlanData == null || !lstPlanData.Any())
                {
                    response.SetSucces();
                    return response;
                }
                int totalRows = lstPlanData.Count();
                lstCustomerID.Clear();
                lstPlanData = lstPlanData.OrderByDescending(x => x.ModifyDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize);
                if (lstPlanData == null || !lstPlanData.Any())
                {
                    response.SetSucces();
                    return response;
                }
                List<long> lstUserIDHandle = new List<long>();
                foreach (var item in lstPlanData)
                {
                    lstCustomerID.Add(item.CustomerID);
                    lstUserIDHandle.Add(item.UserID);
                }
                var customerInfosTask = _customerTab.WhereClause(x => lstCustomerID.Contains(x.CustomerID)).QueryAsync();
                var configMapUserInfosTask = _configUserMapSmartDialerTab.WhereClause(x => lstUserIDHandle.Contains(x.UserID) && x.Status == (int)StatusCommon.Active).QueryAsync();
                await Task.WhenAll(customerInfosTask, configMapUserInfosTask);
                var dictCustomerInfos = customerInfosTask.Result.ToDictionary(x => x.CustomerID, x => x);
                var dictConfigMapUserInfos = configMapUserInfosTask.Result.GroupBy(x => x.UserID).ToDictionary(x => x.Key, x => x.First());
                foreach (var item in lstPlanData)
                {
                    var configMapUser = dictConfigMapUserInfos.GetValueOrDefault(item.UserID);
                    lstResult.Add(new Domain.Models.SmartDialer.PlanHandleCallDailyViewModel
                    {
                        CallCount = item.CallCount,
                        CallResultName = ((PlanHandleCallDaily_CallResult)item.CallResult).GetDescription(),
                        ConnectTime = item.ConnectTime,
                        CustomerID = item.CustomerID,
                        CustomerName = dictCustomerInfos.GetValueOrDefault(item.CustomerID)?.FullName,
                        CustomerNameCall = item.CustomerCallName,
                        EndTime = item.EndTime?.ToString("HH:mm:ss") ?? "",
                        StartTime = item.StartTime?.ToString("HH:mm:ss") ?? "",
                        ModifyDate = item.ModifyDate,
                        PhoneCall = item.NumberPhone,
                        StatusName = ((PlanHandleCallDaily_Status)item.Status).GetDescription(),
                        LoanContractCodes = item.LoanIDs,
                        TotalLoan = item.LoanIDs.Split(',').Length,
                        CampaingName = configMapUser?.CampaignName ?? "",
                        UserNameCisco = configMapUser?.UserNameCisco ?? "",
                        DPD = item.DPD,
                        TotalMoneyOriginal = item.TotalMoneyOriginal,
                        PriorityOrderName = ((PlanHandleCallDaily_PriorityOrder)item.PriorityOrder).GetDescription()
                    });
                }
                if (lstResult.Count > 0)
                {
                    lstResult[0].TotalRows = totalRows;
                }
                response.Total = totalRows;
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetPlanHandleCallDailyByConditionsQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|{ex.Message}-{ex.StackTrace}");
            }

            return response;
        }
    }
}
