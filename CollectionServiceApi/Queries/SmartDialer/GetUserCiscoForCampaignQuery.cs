﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.SmartDialer
{
    public class GetUserCiscoForCampaignQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class GetUserCiscoForCampaignQueryHandler : IRequestHandler<GetUserCiscoForCampaignQuery, ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblMapUserCall> _mapUserCallTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigUserMapSmartDialer> _configUserMapSmartDialerTab;
        readonly ILogger<GetUserCiscoForCampaignQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public GetUserCiscoForCampaignQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblMapUserCall> mapUserCallTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigUserMapSmartDialer> configUserMapSmartDialerTab,
            ILogger<GetUserCiscoForCampaignQueryHandler> logger)
        {
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _mapUserCallTab = mapUserCallTab;
            _configUserMapSmartDialerTab = configUserMapSmartDialerTab;
        }

        public async Task<ResponseActionResult> Handle(GetUserCiscoForCampaignQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            response.SetSucces();
            List<Domain.Models.SelectItem> lstResult = new List<Domain.Models.SelectItem>();
            response.Data = lstResult;
            try
            {
                var mapUserCallCiscoInfosTask = _mapUserCallTab.SelectColumns(x => x.UserID, x => x.JsonExtra)
                                                      .JoinOn<Domain.Tables.TblUser>(m => m.UserID, u => u.UserID)
                                                      .WhereClauseJoinOn<Domain.Tables.TblUser>(u => u.Status == (int)StatusCommon.Active)
                                                      .WhereClause(x => x.TypeCall == (int)MapUserCall_TypeCall.Cisco && x.Status == (int)StatusCommon.Active)
                                                      .QueryAsync();
                await Task.WhenAll( mapUserCallCiscoInfosTask);
                var mapUserCallCiscoInfos = mapUserCallCiscoInfosTask.Result;
                foreach (var item in mapUserCallCiscoInfos)
                {
                    var userCiscoExtra = _common.ConvertJSonToObjectV2<Domain.Tables.UserCallCiscoExtra>(item.JsonExtra);
                    lstResult.Add(new Domain.Models.SelectItem
                    {
                        Value = item.UserID,
                        Text = userCiscoExtra.Username
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetUserCiscoForCampaignQueryHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
