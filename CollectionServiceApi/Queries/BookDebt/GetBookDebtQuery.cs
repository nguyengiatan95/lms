﻿using CollectionServiceApi.Domain;
using CollectionServiceApi.Domain.Models.BookDebt;
using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.AG;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.BookDebt
{
    public class GetBookDebtQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string KeySearch { get; set; }
        public int Status { get; set; }
    }
    public class GetBookDebtQueryHandler : IRequestHandler<GetBookDebtQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBookDebt> _bookDebtTab;
        ILogger<GetBookDebtQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetBookDebtQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBookDebt> bookDebtTab,
            ILogger<GetBookDebtQueryHandler> logger)
        {
            _bookDebtTab = bookDebtTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetBookDebtQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var lstDataResult = new List<GetBookDebt>();
            try
            {
                if (!string.IsNullOrEmpty(request.KeySearch))
                    _bookDebtTab.WhereClause(x => x.BookDebtName == request.KeySearch);
                if (request.Status != (int)StatusCommon.SearchAll)
                    _bookDebtTab.WhereClause(x => x.Status == request.Status);

                var lstBookDebt = (await _bookDebtTab.QueryAsync()).OrderByDescending(x => x.CreateDate);
                if (lstBookDebt.Any())
                {
                    foreach (var item in lstBookDebt)
                    {
                        lstDataResult.Add(new GetBookDebt
                        {
                            BookDebtID = item.BookDebtID,
                            BookDebtName = item.BookDebtName,
                            CreateDate = item.CreateDate,
                            Status = item.Status,
                            StrStatus = ((BookDebt_Status)item.Status).GetDescription(),
                            YearDebt = item.YearDebt,
                            DPDFrom = item.DPDFrom,
                            DPDTo = item.DPDTo
                        });
                    }
                }
                response.Data = lstDataResult;
                response.SetSucces();
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetBookDebtQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
