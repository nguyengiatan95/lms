﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.CollectionServices.Loan;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Loan
{
    public class GetMotorcycleSeizureQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long StaffID { get; set; }
        public DateTime SearchDate { get; set; } = DateTime.Now.Date;
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 25;
        public string GeneralSearch { get; set; }
        public int CityID { get; set; } = (int)LMS.Common.Constants.StatusCommon.SearchAll;
        public int DistrictID { get; set; } = (int)LMS.Common.Constants.StatusCommon.SearchAll;
        public int WardID { get; set; } = (int)LMS.Common.Constants.StatusCommon.SearchAll;
    }
    public class GetMotorcycleSeizureQueryHandler : IRequestHandler<GetMotorcycleSeizureQuery, ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> _assignmentTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;

        readonly ILogger<GetLstPaymentScheduleByLoanIDQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        readonly RestClients.ILOSService _losService;
        public GetMotorcycleSeizureQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> assignmentTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            RestClients.ILOSService losService,
            ILogger<GetLstPaymentScheduleByLoanIDQueryHandler> logger)
        {
            _assignmentTab = assignmentTab;
            _loanTab = loanTab;
            _customerTab = customerTab;
            _losService = losService;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetMotorcycleSeizureQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                // lấy dữ liệu bên LMS
                List<MotorcycleSeizureModel> motorcycleSeizureModels = new List<MotorcycleSeizureModel>();

                List<long> loanIdsAssign = null;
                List<long> customerIdsAssign = null;

                // search TC hoặc tên KH
                if (!string.IsNullOrWhiteSpace(request.GeneralSearch))
                {
                    if (long.TryParse(request.GeneralSearch, out long contracCodeID))
                    {
                        request.GeneralSearch = $"{TimaSettingConstant.PrefixContractCode}{contracCodeID}";
                        _loanTab.WhereClause(x => x.ContactCode == request.GeneralSearch);
                    }
                    else if (Regex.Match(request.GeneralSearch, TimaSettingConstant.PatternContractCode, RegexOptions.IgnoreCase).Success)
                    {
                        _loanTab.WhereClause(x => x.ContactCode == request.GeneralSearch);
                    }
                    else
                    {
                        _loanTab.WhereClause(x => x.CustomerName.Contains(request.GeneralSearch));
                    }
                    var lstLoanSearch = await _loanTab.SelectColumns(x => x.LoanID, x => x.CustomerID).QueryAsync();
                    if (lstLoanSearch != null && lstLoanSearch.Any())
                    {
                        loanIdsAssign = lstLoanSearch.Select(x => x.LoanID).ToList();
                        customerIdsAssign = lstLoanSearch.Select(x => (long)x.CustomerID).ToList();
                    }
                }
                // theo tỉnh thành , quận huyện , phường xã nơi ở của khách hàng 
                if (request.CityID != (int)StatusCommon.SearchAll)
                {
                    if (customerIdsAssign != null && customerIdsAssign.Any())
                    {
                        _customerTab.WhereClause(x => customerIdsAssign.Contains(x.CustomerID));
                    }
                    _customerTab.WhereClause(x => x.CityID == request.CityID);
                    if (request.DistrictID != (int)StatusCommon.SearchAll)
                    {
                        _customerTab.WhereClause(x => x.DistrictID == request.DistrictID);
                        if (request.WardID != (int)StatusCommon.SearchAll)
                        {
                            _customerTab.WhereClause(x => x.WardID == request.WardID);
                        }
                    }
                    var lstCustomer = await _customerTab.SelectColumns(x => x.CustomerID).QueryAsync();
                    var lstCustomerID = lstCustomer.Select(x => x.CustomerID).ToList();
                    int pageSize = TimaSettingConstant.MaxItemQuery;
                    int numberRow = lstCustomerID.Count() / pageSize + 1;
                    loanIdsAssign = new List<long>();
                    for (int i = 1; i <= numberRow; i++)
                    {
                        var lstCustomerIDSkip = lstCustomerID.Skip((i - 1) * pageSize).Take(pageSize).ToList();
                        var lstCustomerLoan = await _loanTab.SelectColumns(x => x.LoanID).WhereClause(x => lstCustomerIDSkip.Contains((int)x.CustomerID)).QueryAsync();
                        var loanIds = lstCustomerLoan.Select(x => x.LoanID);
                        loanIdsAssign.AddRange(loanIds);
                    }
                }
                if (loanIdsAssign != null && loanIdsAssign.Any() && loanIdsAssign.Count <= TimaSettingConstant.MaxItemQuery)
                {
                    _assignmentTab.WhereClause(x => loanIdsAssign.Contains(x.LoanID));
                }
                //DateAssgined, DateApplyTo
                int activeStatus = (int)AssignmentLoan_Status.Processing;
                var lstTypeTGX = new List<int>()
                {
                   (int) AssignmentLoan_Type.AllDebtMoto,
                   (int) AssignmentLoan_Type.MotoSeizure
                };
                var assignLoans = await _assignmentTab.WhereClause(x => x.DateAssigned <= request.SearchDate && x.DateApplyTo > request.SearchDate && x.UserID == request.StaffID && x.Status == activeStatus && lstTypeTGX.Contains(x.Type)).QueryAsync();
                if (loanIdsAssign != null && loanIdsAssign.Any() && loanIdsAssign.Count > TimaSettingConstant.MaxItemQuery)
                {
                    assignLoans = assignLoans.Where(x => loanIdsAssign.Contains(x.LoanID));
                }
                assignLoans = assignLoans.OrderByDescending(x => x.AssignmentLoanID).ToList();
                long totalCount = assignLoans.Count();
                assignLoans = assignLoans.Skip((int)((request.PageIndex - 1) * request.PageSize)).Take(request.PageSize).ToList();
                if (assignLoans != null && assignLoans.Any())
                {
                    // lấy loans 
                    var loanIds = assignLoans.Select(x => x.LoanID).ToList();
                    var loans = await _loanTab.SelectColumns(x => x.LoanID, x => x.CustomerName, x => x.CustomerID, x => x.ContactCode, x => x.LoanCreditIDOfPartner, x => x.TimaLoanID)
                        .WhereClause(x => loanIds.Contains(x.LoanID)).QueryAsync();
                    var dicLoan = loans.ToDictionary(x => x.LoanID, x => x);
                    // call lấy dữ liệu bên LOS Biển số xe. Hãng xe.Tên xe
                    var losIDs = loans.Select(x => x.LoanCreditIDOfPartner).ToList();
                    var responseLosTask = _losService.GetVehicleInforOfLoan(losIDs);

                    // lấy địa chỉ khách hàng
                    var customerIds = loans.Select(x => x.CustomerID).ToList();
                    var customersTask = _customerTab.SelectColumns(x => x.CustomerID, x => x.FullName, x => x.Address, x => x.AddressHouseHold)
                                                    .WhereClause(x => customerIds.Contains(x.CustomerID)).QueryAsync();
                    await Task.WhenAll(responseLosTask, customersTask);
                    var dataLos = responseLosTask.Result;
                    var dicCustomer = customersTask.Result.ToDictionary(x => x.CustomerID, x => x);
                    var dicLos = dataLos.ToDictionary(x => x.LoanBriefId, x => x);
                    foreach (var item in assignLoans)
                    {
                        var loan = dicLoan.GetValueOrDefault(item.LoanID) ?? new Domain.Tables.TblLoan();
                        var customer = dicCustomer.GetValueOrDefault(loan.CustomerID ?? 0) ?? new Domain.Tables.TblCustomer();
                        var loanLos = dicLos.GetValueOrDefault(loan.LoanCreditIDOfPartner ?? 0) ?? new LMS.Entites.Dtos.LOSServices.LoanBriefPropertyLOS();
                        motorcycleSeizureModels.Add(new MotorcycleSeizureModel()
                        {
                            Address = customer.Address,
                            Brand = loanLos.Brand,
                            CustomerID = loan.CustomerID ?? 0,
                            LoanID = loan.TimaLoanID,
                            //LoanID = loan.LoanID, lên app mới dùng cái này
                            Chassis = loanLos.Chassis,
                            ContractCode = loan.ContactCode,
                            CustomerName = customer.FullName,
                            Engine = loanLos.Engine,
                            PlateNumber = loanLos.PlateNumber,
                            PlateNumberCar = loanLos.PlateNumberCar,
                            Product = loanLos.Product,
                            YearMade = loanLos.YearMade
                        });
                    }
                }
                response.SetSucces();
                response.Data = motorcycleSeizureModels;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetMotorcycleSeizureQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;

        }
    }
}
