﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.LoanServices;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Loan
{
    public class GetLstPaymentScheduleByLoanIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
    }
    public class GetLstPaymentScheduleByLoanIDQueryHandler : IRequestHandler<GetLstPaymentScheduleByLoanIDQuery, ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly RestClients.IPaymentScheduleService _paymentScheduleService;
        readonly ILogger<GetLstPaymentScheduleByLoanIDQueryHandler> _logger;
        readonly RestClients.ILoanService _loanService;
        public GetLstPaymentScheduleByLoanIDQueryHandler(Services.ILoanManager loanManager,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            RestClients.IPaymentScheduleService paymentScheduleService,
            RestClients.ILoanService loanService,
            ILogger<GetLstPaymentScheduleByLoanIDQueryHandler> logger)
        {
            _loanTab = loanTab;
            _paymentScheduleService = paymentScheduleService;
            _logger = logger;
            _loanService = loanService;
        }
        public async Task<ResponseActionResult> Handle(GetLstPaymentScheduleByLoanIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstLoanInfos = await _loanTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync();
                if (lstLoanInfos == null || !lstLoanInfos.Any())
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }

                var loanDetail = lstLoanInfos.First();
                var currentDate = DateTime.Now;
                // 26/11: lamvt: kỳ cuối không tính tổng tiền phải tất toán                
                List<LMS.Entites.Dtos.CollectionServices.Loan.PaymentScheduleModel> lstData = new List<LMS.Entites.Dtos.CollectionServices.Loan.PaymentScheduleModel>();
                response.Data = lstData;
                var lstPaymentSchedulInfos = await _paymentScheduleService.GetLstPaymentByLoanID(request.LoanID);
                string yearAndMonthCurrent = $"{currentDate:yyyyMM}";
                if (lstPaymentSchedulInfos != null)
                {
                    // lịch trả nợ đã xong lấy những kỳ KH đã thanh toán
                    if (!RuleCollectionServiceHelper.LstStatusLoanLending.Contains((int)loanDetail.Status))
                    {
                        lstPaymentSchedulInfos = lstPaymentSchedulInfos.Where(x => x.IsVisible == true).ToList();
                    }
                    lstPaymentSchedulInfos = lstPaymentSchedulInfos.OrderBy(x => x.PayDate).ToList();
                    int flagButton = 0;
                    bool flagSetMoneyFine = false;
                    int countNumberPeriods = lstPaymentSchedulInfos.Count();
                    long moneyOriginal = 0;
                    long moneyInterest = 0;
                    long moneyFee = 0;
                    int indexNumberPeriods = 1;
                    int iPosMoneyFineLate = 0;
                    long lastPaymentID = 0; // id của kỳ còn thiếu số tiền phải đóng cho đủ kỳ
                    foreach (var item in lstPaymentSchedulInfos)
                    {
                        LMS.Entites.Dtos.CollectionServices.Loan.PaymentScheduleModel paymentDetail = new LMS.Entites.Dtos.CollectionServices.Loan.PaymentScheduleModel
                        {
                            LoanID = item.LoanID,
                            IsComplete = item.IsVisible == true ? (int)PaymentSchedule_IsComplete.Paid : (int)PaymentSchedule_IsComplete.Waiting,
                            MoneyOriginal = item.MoneyOriginal,
                            MoneyInterest = item.MoneyInterest,
                            MoneyFee = item.MoneyService + item.MoneyConsultant,
                            TotalMoneyPaid = item.PayMoneyOriginal + item.PayMoneyService + item.PayMoneyInterest + item.PayMoneyConsultant,
                            TotalMoneyNeedPay = item.MoneyOriginal + item.MoneyService + item.MoneyInterest + item.MoneyConsultant,
                            PayDate = item.PayDate
                        };
                        // đơn trong hạn tính nghiệp vụ ở dưới
                        if (loanDetail.ToDate.Date > currentDate.Date)
                        {
                            // nhảy ngày thanh toán nhưng thiếu tối thiểu
                            if (flagButton == 0 && paymentDetail.TotalMoneyPaid > 0 && paymentDetail.TotalMoneyNeedPay - paymentDetail.TotalMoneyPaid > 0)
                            {
                                moneyOriginal = item.MoneyOriginal - item.PayMoneyOriginal;
                                moneyInterest = item.MoneyInterest - item.PayMoneyInterest;
                                moneyFee = item.MoneyService + item.MoneyConsultant - item.PayMoneyService - item.PayMoneyConsultant;
                                flagButton = 1;
                                lastPaymentID = item.PaymentScheduleID;
                            }
                            // kỳ đầu tiên phải đóng tính thêm số tiền còn thiếu kỳ trước
                            if (paymentDetail.IsComplete == (int)PaymentSchedule_IsComplete.Waiting
                                && item.IsVisible == false && flagButton == 1 && lastPaymentID != item.PaymentScheduleID)
                            {
                                paymentDetail.MoneyOriginal += moneyOriginal;
                                paymentDetail.MoneyInterest += moneyInterest;
                                paymentDetail.MoneyFee += moneyFee;
                                paymentDetail.TotalMoneyNeedPay = paymentDetail.MoneyOriginal + paymentDetail.MoneyInterest + paymentDetail.MoneyFee;
                                moneyOriginal = 0;
                                moneyInterest = 0;
                                moneyFee = 0;
                                flagButton = 0;
                            }
                            // 26/11: lamvt: kỳ cuối không tính tổng tiền phải tất toán                       
                            if (paymentDetail.IsComplete == (int)PaymentSchedule_IsComplete.Waiting && item.IsVisible == false)
                            {
                                // nếu chưa gán số tiền phí phạt trả chậm thì gán vào kỳ hiện tại
                                // nếu đã có thì check thêm kỳ tiếp theo có cùng tháng không
                                if (flagSetMoneyFine && $"{paymentDetail.PayDate:yyyyMM}" == yearAndMonthCurrent)
                                {
                                    paymentDetail.MoneyFineLate = loanDetail.MoneyFineLate;
                                    paymentDetail.TotalMoneyNeedPay += loanDetail.MoneyFineLate;
                                    try
                                    {
                                        // set lại kỳ trước đó trong tháng về lại như cũ
                                        lstData[iPosMoneyFineLate - 1].MoneyFineLate = 0;
                                        lstData[iPosMoneyFineLate - 1].TotalMoneyNeedPay -= loanDetail.MoneyFineLate;
                                        iPosMoneyFineLate = indexNumberPeriods;
                                    }
                                    catch (Exception ex)
                                    {
                                        _logger.LogError($"GetLstPaymentScheduleByLoanIDQueryHandler_Warning|loanid={request.LoanID}|countNumberPeriods={countNumberPeriods}|iPosMoneyFineLate={iPosMoneyFineLate}|indexNumberPeriods={indexNumberPeriods}|ex={ex.Message}-{ex.StackTrace}");
                                    }
                                }
                                else if (!flagSetMoneyFine)
                                {
                                    paymentDetail.MoneyFineLate = loanDetail.MoneyFineLate;
                                    paymentDetail.TotalMoneyNeedPay += loanDetail.MoneyFineLate;
                                    flagSetMoneyFine = true; // đã gán phí phạt
                                    iPosMoneyFineLate = indexNumberPeriods;
                                    yearAndMonthCurrent = $"{paymentDetail.PayDate:yyyyMM}";
                                }
                                //}
                            }
                        }
                        // kỳ cuối và quá hạn
                        if (indexNumberPeriods == countNumberPeriods && paymentDetail.PayDate.Date <= currentDate.Date)
                        {
                            if (iPosMoneyFineLate > 0)
                            {
                                // set lại kỳ trước đó trong tháng về lại như cũ
                                lstData[iPosMoneyFineLate - 1].MoneyFineLate = 0;
                                lstData[iPosMoneyFineLate - 1].TotalMoneyNeedPay -= loanDetail.MoneyFineLate;
                            }
                            paymentDetail.MoneyFineLate = loanDetail.MoneyFineLate;
                            paymentDetail.TotalMoneyNeedPay += loanDetail.MoneyFineLate;
                        }
                        lstData.Add(paymentDetail);
                        indexNumberPeriods++;
                    }
                    response.Data = lstData.OrderBy(x => x.PayDate).ToList();
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLstPaymentScheduleByLoanIDQueryHandler|loanID={request.LoanID}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
