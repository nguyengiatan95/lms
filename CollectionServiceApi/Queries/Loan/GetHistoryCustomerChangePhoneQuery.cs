﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Loan
{
    public class GetHistoryCustomerChangePhoneQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
    }
    public class GetHistoryCustomerChangePhoneQueryHandler : IRequestHandler<GetHistoryCustomerChangePhoneQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogChangePhone> _logChangePhone;
        readonly ILogger<GetHistoryCustomerChangePhoneQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public GetHistoryCustomerChangePhoneQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogChangePhone> logChangePhone,
            ILogger<GetHistoryCustomerChangePhoneQueryHandler> logger)
        {
            _logChangePhone = logChangePhone;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetHistoryCustomerChangePhoneQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstHistoryChangePhone = await _logChangePhone.WhereClause(x => x.LoanID == request.LoanID).QueryAsync();
                response.SetSucces();
                List<LMS.Entites.Dtos.CollectionServices.CustomerChangePhone.HistoryCustomerChangePhoneModel> lstDataResponse = new List<LMS.Entites.Dtos.CollectionServices.CustomerChangePhone.HistoryCustomerChangePhoneModel>();
                if (lstHistoryChangePhone != null)
                {
                    lstHistoryChangePhone = lstHistoryChangePhone.OrderByDescending(x => x.LogChangePhoneID);
                    foreach (var item in lstHistoryChangePhone)
                    {
                        lstDataResponse.Add(new LMS.Entites.Dtos.CollectionServices.CustomerChangePhone.HistoryCustomerChangePhoneModel
                        {
                            CreateByName = item.CreateByName,
                            CreateDate = item.CreateDate,
                            NewPhone = item.NewPhone,
                            OldPhone = item.OldPhone,
                            Note = item.Note
                        });
                    }
                }
                response.Data = lstDataResponse;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetHistoryCustomerChangePhoneQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
