﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Loan
{
    public class GetLoanCreditDetailLosByLoanIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public long UserID { get; set; } // nhân viên xử lý đơn
    }
    public class GetLoanCreditDetailLosByLoanIDQueryHandler : IRequestHandler<GetLoanCreditDetailLosByLoanIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomerBank> _customerBankTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> _planhandleLoanDailyTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebtRestructuringLoan> _debtRestructuringLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanCloseLoan> _planCloseLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCutOffLoan> _cutoffLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblHoldCustomerMoney> _holdCustomerMoneyTab;
        //readonly RestClients.ILoanService _loanService;
        readonly RestClients.ILOSService _losService;
        readonly ILogger<GetLoanCreditDetailLosByLoanIDQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public GetLoanCreditDetailLosByLoanIDQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomerBank> customerBankTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transaciontLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> planhandleLoanDailyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebtRestructuringLoan> debtRestructuringLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanCloseLoan> planCloseLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCutOffLoan> cutoffLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblHoldCustomerMoney> holdCustomerMoneyTab,
            //RestClients.ILoanService loanService,
            RestClients.ILOSService losService,
            ILogger<GetLoanCreditDetailLosByLoanIDQueryHandler> logger
            )
        {
            _loanTab = loanTab;
            _customerBankTab = customerBankTab;
            _customerTab = customerTab;
            _lenderTab = lenderTab;
            _paymentScheduleTab = paymentScheduleTab;
            _invoiceTab = invoiceTab;
            _transactionLoanTab = transaciontLoanTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            //_loanService = loanService;
            _losService = losService;
            _planhandleLoanDailyTab = planhandleLoanDailyTab;
            _debtRestructuringLoanTab = debtRestructuringLoanTab;
            _planCloseLoanTab = planCloseLoanTab;
            _cutoffLoanTab = cutoffLoanTab;
            _holdCustomerMoneyTab = holdCustomerMoneyTab;
        }
        public async Task<ResponseActionResult> Handle(GetLoanCreditDetailLosByLoanIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var currentDate = DateTime.Now;
                var firtDateOfMonth = new DateTime(currentDate.Year, currentDate.Month, 1);
                var firtDateOfNextMonth = firtDateOfMonth.AddMonths(1);
                var lastDateOfLastMonth = firtDateOfMonth.AddDays(-1);
                var objLoan = (await _loanTab.SetGetTop(1).WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                if (objLoan == null || objLoan.LoanCreditIDOfPartner == 0)
                {
                    response.Message = MessageConstant.RequireLoanID;
                    return response;
                }
                var lstPaymentScheduleTask = _paymentScheduleTab.WhereClause(x => x.LoanID == request.LoanID)
                                                                          .WhereClause(x => x.Status == (int)PaymentSchedule_Status.Use)
                                                                          .QueryAsync();
                List<int> lstInvoiceSubTypeCustomer = new List<int>
                {
                    (int)GroupInvoiceType.ReceiptCustomer,
                    (int)GroupInvoiceType.ReceiptOnBehalfShop,
                    (int)GroupInvoiceType.ReceiptCustomerConsider
                };
                var invoiceCustomerTask = _invoiceTab.SetGetTop(1).WhereClause(x => x.SourceID == objLoan.CustomerID)
                                                     .WhereClause(x => lstInvoiceSubTypeCustomer.Contains(x.InvoiceSubType))
                                                     .WhereClause(x => x.Status == (int)Invoice_Status.Confirmed)
                                                     .OrderByDescending(x => x.TransactionDate)
                                                     .QueryAsync();
                var txnLoanTask = _transactionLoanTab.SelectColumns(x => x.TotalMoney)
                                                     .WhereClause(x => x.LoanID == request.LoanID && x.ActionID != (int)Transaction_Action.ChoVay)
                                                     .QueryAsync();
                var lenderInfoTask = _lenderTab.WhereClause(x => x.LenderID == objLoan.LenderID).QueryAsync();
                var resultLoanLosTask = _losService.GetLoanCreditDetail((long)objLoan.LoanCreditIDOfPartner);
                var customerInfosTask = _customerTab.WhereClause(x => x.CustomerID == objLoan.CustomerID).QueryAsync();
                var lstCustomerBankTask = _customerBankTab.WhereClause(x => x.CustomerID == objLoan.CustomerID && x.StatusVa == (int)CustomerBank_StatusVA.Success).QueryAsync();
                var holdCustomerInfoTask = _holdCustomerMoneyTab.SetGetTop(1).WhereClause(x => x.CustomerID == objLoan.CustomerID && x.Status == (int)HoldCustomerMoney_Status.Hold).QueryAsync();

                await Task.WhenAll(resultLoanLosTask);

                var loanLosDetail = resultLoanLosTask.Result;
                //LMS.Entites.Dtos.LoanServices.MoneyNeedCloseLoan moneyNeedCloseLoan = null;
                if (loanLosDetail != null)
                {
                    Domain.Tables.LoanJsonExtra loanJsonExtra = _common.ConvertJSonToObjectV2<Domain.Tables.LoanJsonExtra>(objLoan.JsonExtra);
                    var planCloseLoanDetailTask = _planCloseLoanTab.SetGetTop(1).WhereClause(x => x.LoanID == request.LoanID && x.CloseDate == DateTime.Now.Date).QueryAsync();
                    await Task.WhenAll(customerInfosTask, lstCustomerBankTask, lstPaymentScheduleTask, invoiceCustomerTask, txnLoanTask, lenderInfoTask, planCloseLoanDetailTask, holdCustomerInfoTask);

                    var holdCustomerDetail = holdCustomerInfoTask.Result.FirstOrDefault();
                    var planCloseLoanDetail = planCloseLoanDetailTask.Result.FirstOrDefault();
                    loanLosDetail.TotalMoneyNeedCloseLoan = -1; // không tính dc tiền
                    if (planCloseLoanDetail != null)
                    {
                        loanLosDetail.TotalMoneyNeedCloseLoan = planCloseLoanDetail.TotalMoneyClose;
                    }
                    // app lấy thêm thông tin
                    if (request.UserID > 0)
                    {
                        List<int> lstStatusLoanHandle = new List<int>
                        {
                            (int)PlanHandleLoanDaily_Status.NoProcess,
                            (int)PlanHandleLoanDaily_Status.Processed
                        };
                        var planLoanHandleDetailTask = _planhandleLoanDailyTab.SetGetTop(1).WhereClause(x => x.UserID == request.UserID && x.LoanID == request.LoanID)
                                                                          .WhereClause(x => lstStatusLoanHandle.Contains(x.Status))
                                                                          .OrderByDescending(x => x.PlanHandleLoanDailyID)
                                                                          .QueryAsync();

                        await Task.WhenAll(planLoanHandleDetailTask, planCloseLoanDetailTask);

                        var planLoanHandleDetail = planLoanHandleDetailTask.Result.FirstOrDefault();

                        loanLosDetail.DebtRelief = (int)DebtRestructuringLoan_AppView.No;
                        loanLosDetail.DebtStructure = (int)DebtRestructuringLoan_AppView.No;

                        // thuộc đơn xe máy
                        var lstDebtRestructuringLoanDataTask = _debtRestructuringLoanTab.SelectColumns(x => x.LoanID, x => x.DebtRestructuringType, x => x.Status)
                                                                               .WhereClause(x => x.LoanID == request.LoanID && x.Status != (int)DebtRestructuringLoan_Status.Deleted && x.ExpirationDate > currentDate.Date)
                                                                               .QueryAsync();
                        var cutOffLoanLastMonthTask = _cutoffLoanTab.SetGetTop(1)
                                                         .WhereClause(x => x.CutOffDate <= lastDateOfLastMonth.Date && x.LoanID == request.LoanID)
                                                         .OrderByDescending(x => x.CutOffDate)
                                                         .QueryAsync();

                        await Task.WhenAll(cutOffLoanLastMonthTask, lstDebtRestructuringLoanDataTask);

                        var cutOffLoanLastMonthDetail = cutOffLoanLastMonthTask.Result.FirstOrDefault();
                        // lấy thông tin đơn giãn nợ, cấu trúc nợ
                        var lstDebtRestructuringLoanData = lstDebtRestructuringLoanDataTask.Result;

                        int dpdBomLastMonth = 0;
                        Domain.Tables.TblDebtRestructuringLoan debtStructuringDetail = null;
                        Domain.Tables.TblDebtRestructuringLoan debtReliefDetail = null;
                        if (cutOffLoanLastMonthDetail != null)
                        {
                            dpdBomLastMonth = lastDateOfLastMonth.Date.Subtract(cutOffLoanLastMonthDetail.NextDate.Date).Days;
                        }
                        if (lstDebtRestructuringLoanData != null && lstDebtRestructuringLoanData.Any())
                        {
                            debtStructuringDetail = lstDebtRestructuringLoanData.FirstOrDefault(x => x.DebtRestructuringType == (int)DebtRestructuringLoan_DebtRestructuringType.DebtRestructure);
                            debtReliefDetail = lstDebtRestructuringLoanData.FirstOrDefault(x => x.DebtRestructuringType == (int)DebtRestructuringLoan_DebtRestructuringType.DebtRelief);
                        }
                        if (RuleCollectionServiceHelper.LstProductIDForDebtRestructuring.Contains(objLoan.ProductID.GetValueOrDefault())
                            && dpdBomLastMonth < RuleCollectionServiceHelper.MaxDPDBOMForDebtRelief)
                        {
                            // kiểm tra đã có giãn nợ chưa
                            loanLosDetail.DebtRelief = (int)DebtRestructuringLoan_AppView.Yes;
                            if (debtReliefDetail != null && debtReliefDetail.LoanID > 0)
                            {
                                switch ((DebtRestructuringLoan_Status)debtReliefDetail.Status)
                                {
                                    case DebtRestructuringLoan_Status.Approved:
                                        loanLosDetail.DebtRelief = (int)DebtRestructuringLoan_AppView.Yes;
                                        break;
                                    case DebtRestructuringLoan_Status.Handled:
                                        loanLosDetail.DebtRelief = (int)DebtRestructuringLoan_AppView.Executed;
                                        break;
                                    default:
                                        loanLosDetail.DebtRelief = (int)DebtRestructuringLoan_AppView.No;
                                        break;
                                }
                            }
                        }
                        // nếu trong tháng đã xử lý thì bỏ qua
                        if (debtStructuringDetail != null && debtStructuringDetail.LoanID > 0)
                        {
                            switch ((DebtRestructuringLoan_Status)debtStructuringDetail.Status)
                            {
                                //case DebtRestructuringLoan_Status.Waiting:
                                case DebtRestructuringLoan_Status.Approved:
                                    loanLosDetail.DebtStructure = (int)DebtRestructuringLoan_AppView.Yes;
                                    break;
                                case DebtRestructuringLoan_Status.Handled:
                                    loanLosDetail.DebtStructure = (int)DebtRestructuringLoan_AppView.Executed;
                                    break;
                                default:
                                    loanLosDetail.DebtStructure = (int)DebtRestructuringLoan_AppView.No;
                                    break;
                            }
                        }
                        else if (RuleCollectionServiceHelper.LstProductIDForDebtRestructuring.Contains(objLoan.ProductID.GetValueOrDefault())
                            && dpdBomLastMonth > RuleCollectionServiceHelper.MinDPDBOMForDebtRestructuring && dpdBomLastMonth < RuleCollectionServiceHelper.MaxDPDBOMForDebtRestructuring)
                        {
                            loanLosDetail.DebtStructure = (int)DebtRestructuringLoan_AppView.Yes;
                        }

                        if (planLoanHandleDetail != null && planLoanHandleDetail.PlanHandleLoanDailyID > 0)
                        {
                            loanLosDetail.PlanHandleLoanCheckInLocationName = planLoanHandleDetail.LocationName;
                            loanLosDetail.PlanHandleLoanCheckInStatus = planLoanHandleDetail.StatusCheckIn == (int)PlanHandleLoanDaily_StatusCheckIn.Success;
                            // nếu có checkout mà chưa pass reset lại location name
                            if (planLoanHandleDetail.TimeCheckOut.HasValue)
                            {
                                loanLosDetail.PlanHandleLoanCheckInLocationName = null;
                            }
                            loanLosDetail.PlanHandleLoanStatus = planLoanHandleDetail.Status;
                            loanLosDetail.PlanHandleCodeCheckIn = planLoanHandleDetail.CodeCheckIn;
                            if (planLoanHandleDetail.ToDate >= currentDate.Date)
                            {
                                loanLosDetail.PlanHandleLoanExpiredType = (int)PlanHandleLoanDaily_AppView.Yes;
                            }
                            else
                            {
                                loanLosDetail.PlanHandleLoanExpiredType = (int)PlanHandleLoanDaily_AppView.ExpirationDate;
                            }
                        }
                        // lấy thông tin tính tiền cho app và web cùng nhìn
                        loanLosDetail.TotalMoneyNeedPayOnApp = -1; // không tính được tiền trên app
                        if (planCloseLoanDetail != null)
                        {
                            loanLosDetail.TotalMoneyNeedPayOnApp = planCloseLoanDetail.TotalMoneyNeedPay;
                            loanLosDetail.MoneyOriginalNeedPayOnApp = planCloseLoanDetail.MoneyOriginalNeedPay;
                            loanLosDetail.TotalInterestNeedPayOnApp = planCloseLoanDetail.MoneyInterestNeedPay + planCloseLoanDetail.MoneyConsultantNeePay + planCloseLoanDetail.MoneyServiceNeedPay;
                            loanLosDetail.MoneyFineLateNeedPayOnApp = planCloseLoanDetail.MoneyFineLateNeedPay;
                            if (planCloseLoanDetail.TotalMoneyClose == planCloseLoanDetail.TotalMoneyNeedPay)
                            {
                                loanLosDetail.MoneyFineOriginalNeedPayOnApp = planCloseLoanDetail.MoneyFineOriginal;
                            }
                        }
                    }
                    // thông tin lấy từ LMS
                    loanLosDetail.LenderCode = lenderInfoTask.Result.FirstOrDefault()?.FullName;
                    loanLosDetail.StrImcomeType = ((LOS_ImcomeType)loanLosDetail.ImcomeType).GetDescription();
                    loanLosDetail.ContactCodeLMS = objLoan.ContactCode;
                    loanLosDetail.StatusLMS = RuleCollectionServiceHelper.GetStatusLoanByNextDate(objLoan.Status.Value, objLoan.NextDate.Value, objLoan.ToDate);
                    loanLosDetail.FromDateLMS = objLoan.FromDate;
                    loanLosDetail.ToDateLMS = objLoan.ToDate;
                    loanLosDetail.CustomerId = (long)objLoan.CustomerID;
                    loanLosDetail.ContactCode = $"{TimaSettingConstant.PatternContractCodeLOS}{loanLosDetail.LoanBriefId}".ToUpper();
                    loanLosDetail.MoneyFineLate = objLoan.MoneyFineLate;
                    loanLosDetail.ConsultantShopName = loanJsonExtra.ConsultantShopName;
                    loanLosDetail.NextDate = objLoan.NextDate.Value;
                    loanLosDetail.Status = objLoan.Status.Value;
                    loanLosDetail.ProductName = objLoan.ProductName;

                    var customerInfos = customerInfosTask.Result;
                    var customerDetail = customerInfos.FirstOrDefault();
                    var lstCustomerBank = lstCustomerBankTask.Result;
                    var paymentScheduleInfos = lstPaymentScheduleTask.Result;
                    var invoiceCustomer = invoiceCustomerTask.Result;
                    if (lstCustomerBank != null && lstCustomerBank.Count() > 0)
                    {
                        foreach (var item in lstCustomerBank)
                        {
                            loanLosDetail.LstBankCustomer.Add(new LMS.Entites.Dtos.LOSServices.BankCustomer
                            {
                                AccountNo = item.AccountNo,
                                AccountName = item.AccountName,
                                BankCode = item.BankCode,
                                BankName = item.BankName
                            });
                        }
                    }
                    loanLosDetail.Phone = customerDetail?.Phone;
                    loanLosDetail.FullName = customerDetail?.FullName;
                    loanLosDetail.CustomerTotalMoney = customerDetail?.TotalMoney ?? 0;
                    // lịch thanh toán
                    // đơn không có lịch lỗi data, sẽ check sau
                    if (paymentScheduleInfos != null)
                    {
                        // đơn đã hoàn thành
                        if (!RuleCollectionServiceHelper.LstStatusLoanLending.Contains((int)objLoan.Status))
                        {
                            paymentScheduleInfos = paymentScheduleInfos.Where(x => x.IsVisible == true);
                        }
                        paymentScheduleInfos = paymentScheduleInfos.OrderBy(x => x.PayDate).ToList();
                        foreach (var p in paymentScheduleInfos)
                        {
                            if (p.PayDate >= firtDateOfNextMonth)
                            {
                                break;
                            }
                            if (p.IsComplete == (int)PaymentSchedule_IsComplete.Waiting)
                            {
                                loanLosDetail.MoneyOrginal += p.MoneyOriginal - p.PayMoneyOriginal;
                                loanLosDetail.MoneyInterest += p.MoneyInterest - p.PayMoneyInterest;
                                loanLosDetail.MoneyConsultant += p.MoneyConsultant - p.PayMoneyConsultant;
                                loanLosDetail.MoneyService += p.MoneyService - p.PayMoneyService;
                            }
                        }
                        loanLosDetail.MoneyInPeriod = loanLosDetail.MoneyOrginal + loanLosDetail.MoneyInterest + loanLosDetail.MoneyConsultant + loanLosDetail.MoneyService;
                    }
                    // 10-08-2021: Lâm said: lấy tổng luôn tiền phạt trả chậm
                    loanLosDetail.TotalMoneyNeedPay = loanLosDetail.MoneyInPeriod + loanLosDetail.MoneyFineLate;


                    loanLosDetail.TotalMoneyLoanPaid = txnLoanTask.Result.Sum(x => x.TotalMoney);
                    if (invoiceCustomer != null && invoiceCustomer.Any())
                    {
                        loanLosDetail.LatestDateTopup = invoiceCustomer.FirstOrDefault().TransactionDate;
                    }
                    // lỗi api bên LOS nhưng giá trị trả về = 0
                    if (loanLosDetail.LoanBriefId == 0)
                    {
                        loanLosDetail.LoanBriefId = objLoan.LoanCreditIDOfPartner.GetValueOrDefault();
                        loanLosDetail.CustomerAddress = customerDetail.Address;
                        loanLosDetail.AddressPersonFamily = customerDetail.AddressHouseHold;
                        loanLosDetail.HouseOldAddress = customerDetail.AddressHouseHold;
                        loanLosDetail.Address = customerDetail.Address;
                        loanLosDetail.LoanAmountFinal = objLoan.TotalMoneyDisbursement;
                    }
                    loanLosDetail.HoldCustomerMoneyID = holdCustomerDetail?.HoldCustomerMoneyID ?? 0;
                }
                response.SetSucces();
                response.Data = loanLosDetail;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLoanCreditDetailLosByLoanIDQueryHandler|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
