﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.LoanServices;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Loan
{
    public class GetLstLoanInfoByConditionQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public Domain.Models.Loan.RequestLoanItemViewModel Model { get; set; }
    }
    public class GetLstLoanInfoByConditionQueryHandler : IRequestHandler<GetLstLoanInfoByConditionQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly Services.ILoanManager _loanManager;
        public GetLstLoanInfoByConditionQueryHandler(Services.ILoanManager loanManager)
        {
            _loanManager = loanManager;
        }
        public async Task<ResponseActionResult> Handle(GetLstLoanInfoByConditionQuery request, CancellationToken cancellationToken)
        {
            request.Model.LoanStatus = (int)Loan_Status.Lending;
            return await _loanManager.GetLoanInfosByCondition(request.Model);
        }
    }
}
