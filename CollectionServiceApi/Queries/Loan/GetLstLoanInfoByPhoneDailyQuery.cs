﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Loan
{
    public class GetLstLoanInfoByPhoneDailyQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string KeySearch { get; set; }
    }
    public class GetLstLoanInfoByPhoneDailyQueryHandler : IRequestHandler<GetLstLoanInfoByPhoneDailyQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly ILogger<GetLstLoanInfoByPhoneDailyQueryHandler> _logger;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleCallDaily> _planHandleCallDailyTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        readonly Services.ILoanManager _loanManager;
        public GetLstLoanInfoByPhoneDailyQueryHandler(Services.ILoanManager loanManager,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleCallDaily> planHandleCallDailyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            ILogger<GetLstLoanInfoByPhoneDailyQueryHandler> logger)
        {
            _customerTab = customerTab;
            _loanManager = loanManager;
            _planHandleCallDailyTab = planHandleCallDailyTab;
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(GetLstLoanInfoByPhoneDailyQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var currentDate = DateTime.Now;
                var dailyInfo = (await _planHandleCallDailyTab.WhereClause(x => x.CallDate == currentDate.Date && x.NumberPhone == request.KeySearch).QueryAsync()).FirstOrDefault();
                if (dailyInfo == null || dailyInfo.CustomerID < 1)
                {
                    response.Message = "Không tìm thấy thông tin đơn vay từ số đang gọi.";
                    return response;
                }
                var customerDetail = (await _customerTab.WhereClause(x => x.CustomerID == dailyInfo.CustomerID).QueryAsync()).FirstOrDefault();
                Domain.Models.SmartDialer.DetailPhoneCallWithLstLoanViewModel detailPhoneCall = new Domain.Models.SmartDialer.DetailPhoneCallWithLstLoanViewModel
                {
                    CustomerCallName = dailyInfo.CustomerCallName
                };
                Domain.Models.Loan.RequestLoanItemViewModel model = new Domain.Models.Loan.RequestLoanItemViewModel()
                {
                    KeySearch = customerDetail.Phone,
                    DepartmentID = (int)LMS.Common.Constants.StatusCommon.SearchAll,
                    LoanStatus = (int)LMS.Common.Constants.Loan_Status.Lending
                };
                detailPhoneCall.LstLoanInfos = await _loanManager.GetLstLoanItemViewModels(model);
                response.SetSucces();
                response.Data = detailPhoneCall;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLstLoanInfoByPhoneDailyQueryHandler|Phone={request.KeySearch}|{ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
