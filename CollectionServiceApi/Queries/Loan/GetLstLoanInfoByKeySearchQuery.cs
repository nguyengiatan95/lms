﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Loan
{
    public class GetLstLoanInfoByKeySearchQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string KeySearch { get; set; }
        public int LoanStatus { get; set; } = (int)LMS.Common.Constants.StatusCommon.SearchAll;
    }
    public class GetLstLoanInfoByKeySearchQueryHandler : IRequestHandler<GetLstLoanInfoByKeySearchQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly ILogger<GetLstLoanInfoByKeySearchQueryHandler> _logger;
        readonly Services.ILoanManager _loanManager;
        public GetLstLoanInfoByKeySearchQueryHandler(Services.ILoanManager loanManager,
            ILogger<GetLstLoanInfoByKeySearchQueryHandler> logger)
        {
            _loanManager = loanManager;
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(GetLstLoanInfoByKeySearchQuery request, CancellationToken cancellationToken)
        {
            Domain.Models.Loan.RequestLoanItemViewModel model = new Domain.Models.Loan.RequestLoanItemViewModel()
            {
                KeySearch = request.KeySearch,
                DepartmentID = (int)LMS.Common.Constants.StatusCommon.SearchAll,
                LoanStatus = request.LoanStatus
            };
            return await _loanManager.GetLoanInfosByCondition(model);
        }
    }
}
