﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.SettingInterestReduction
{
    public class GetSettingByBookDebtIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long BookDebtID { get; set; }
        public long ApprovalLevelBookDebtID { get; set; }
    }
    public class GetSettingByBookDebtIDQueryHandler : IRequestHandler<GetSettingByBookDebtIDQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingInterestReduction> _settingInterestReductionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBookDebt> _bookDebtTab;

        ILogger<GetSettingByBookDebtIDQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetSettingByBookDebtIDQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingInterestReduction> settingInterestReductionTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblBookDebt> bookDebtTab,
            ILogger<GetSettingByBookDebtIDQueryHandler> logger)
        {
            _settingInterestReductionTab = settingInterestReductionTab;
            _bookDebtTab = bookDebtTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetSettingByBookDebtIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                Domain.Models.MGL.SettingByBookDebtDetails data = new Domain.Models.MGL.SettingByBookDebtDetails();
                var debt = await _bookDebtTab.GetByIDAsync(request.BookDebtID);
                //data.ApplyFrom = firstData.ApplyFrom.Value.ToString(TimaSettingConstant.DateTimeDayMonthYear);
                //data.ApplyTo = firstData.ApplyFrom.Value.ToString(TimaSettingConstant.DateTimeDayMonthYear);
                data.DPDTo = debt.DPDTo;
                data.DPDFrom = debt.DPDFrom;
                data.YearDebt = debt.YearDebt;
                var settings = await _settingInterestReductionTab.WhereClause(x => x.BookDebtID == request.BookDebtID && x.ApprovalLevelBookDebtID == request.ApprovalLevelBookDebtID).QueryAsync();
                if (settings != null && settings.Any())
                {
                    var firstData = settings.FirstOrDefault();
                    foreach (var item in settings)
                    {
                        switch ((Transaction_TypeMoney)item.MoneyType)
                        {
                            //case Transaction_TypeMoney.Original:
                            //    break;
                            //case Transaction_TypeMoney.Interest:
                            //    break;
                            case Transaction_TypeMoney.Service:
                                data.ServicePercent = item.PercentMoneyReduce;
                                data.ServiceTotalMoney = item.MaxMoneyReduce;
                                break;
                            case Transaction_TypeMoney.Consultant:
                                data.ConsultantPercent = item.PercentMoneyReduce;
                                data.ConsultantTotalMoney = item.MaxMoneyReduce;
                                break;
                            case Transaction_TypeMoney.FineLate:
                                data.InterestLatePercent = item.PercentMoneyReduce;
                                data.InterestLateTotalMoney = item.MaxMoneyReduce;
                                break;
                            //case Transaction_TypeMoney.FineInterestLate:
                            //    break;
                            case Transaction_TypeMoney.FineOriginal:
                                data.PaybeforePercent = item.PercentMoneyReduce;
                                data.PaybeforeTotalMoney = item.MaxMoneyReduce;
                                break;
                            //case Transaction_TypeMoney.InsuranceFee:
                            //    break;
                            case Transaction_TypeMoney.Debt:
                                data.DebtPercent = item.PercentMoneyReduce;
                                data.DebtTotalMoney = item.MaxMoneyReduce;
                                break;
                            default:
                                break;
                        }
                    }
                }
                response.SetSucces();
                response.Data = data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetSettingByBookDebtIDQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }

}
