﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.SettingInterestReduction
{
    public class GetSettingByConditionsQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long BookDebtID { get; set; }
        public long ApprovalLevelBookDebtID { get; set; }
    }
    public class GetSettingByConditionsQueryHandler : IRequestHandler<GetSettingByConditionsQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingInterestReduction> _settingInterestReductionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> _approvalLevelBookDebtTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBookDebt> _bookDebtTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingTotalMoney> _settingTotalMoneyTab;

        ILogger<GetSettingByConditionsQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetSettingByConditionsQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingInterestReduction> settingInterestReductionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> approvalLevelBookDebtTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBookDebt> bookDebtTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingTotalMoney> settingTotalMoneyTab,
            ILogger<GetSettingByConditionsQueryHandler> logger)
        {
            _settingInterestReductionTab = settingInterestReductionTab;
            _approvalLevelBookDebtTab = approvalLevelBookDebtTab;
            _settingTotalMoneyTab = settingTotalMoneyTab;
            _bookDebtTab = bookDebtTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetSettingByConditionsQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                Domain.Models.SettingInterestReduction.SettingInterestReductionView data = new Domain.Models.SettingInterestReduction.SettingInterestReductionView();
                var taskApprovalLevelBookDebts = _approvalLevelBookDebtTab.WhereClause(x => x.Status == (int)StatusCommon.Active).QueryAsync();
                var taskBookDebts = _bookDebtTab.WhereClause(x => x.Status == (int)StatusCommon.Active).QueryAsync();
                await Task.WhenAll(taskApprovalLevelBookDebts, taskBookDebts);
                var approvalLevelBookDebts = taskApprovalLevelBookDebts.Result;
                var bookDebts = taskBookDebts.Result;
                var approvalLevelBookDebtIDs = approvalLevelBookDebts.Select(x => x.ApprovalLevelBookDebtID);
                var bookDebtIDs = bookDebts.Select(x => x.BookDebtID);
                if (request.ApprovalLevelBookDebtID != (int)StatusCommon.SearchAll)
                {
                    approvalLevelBookDebtIDs = approvalLevelBookDebtIDs.Where(x => x == request.ApprovalLevelBookDebtID).ToList();
                }
                if (request.BookDebtID != (int)StatusCommon.SearchAll)
                {
                    bookDebtIDs = bookDebtIDs.Where(x => x == request.BookDebtID).ToList();
                }

                var taskSettings = _settingInterestReductionTab.WhereClause(x => approvalLevelBookDebtIDs.Contains(x.ApprovalLevelBookDebtID) && bookDebtIDs.Contains(x.BookDebtID)).QueryAsync();
                var taskSettingMoney = _settingTotalMoneyTab.WhereClause(x => approvalLevelBookDebtIDs.Contains(x.ApprovalLevelBookDebtID)).QueryAsync();
                await Task.WhenAll(taskSettings, taskSettingMoney);

                var settings = taskSettings.Result;
                var settingMoney = taskSettingMoney.Result;

                foreach (var item in approvalLevelBookDebts)
                {
                    data.ApprovalLevelBookDebtChildViews.Add(new Domain.Models.SettingInterestReduction.ApprovalLevelBookDebtChildView()
                    {
                        ApprovalLevelBookDebtID = item.ApprovalLevelBookDebtID,
                        ApprovalLevelBookDebtName = item.ApprovalLevelBookDebtName
                    });
                }
                foreach (var item in bookDebts)
                {
                    data.BookDebtChildViews.Add(new Domain.Models.SettingInterestReduction.BookDebtChildView()
                    {
                        BookDebtID = item.BookDebtID,
                        BookDebtName = item.BookDebtName
                    });
                }
                foreach (var item in settings)
                {
                    data.SettingInterestReductionListViews.Add(new Domain.Models.SettingInterestReduction.SettingInterestReductionListView()
                    {
                        BookDebtID = item.BookDebtID,
                        ApprovalLevelBookDebtID = item.ApprovalLevelBookDebtID,
                        MaxMoneyReduce = item.MaxMoneyReduce,
                        MoneyType = item.MoneyType,
                        Note = $"{item.PercentMoneyReduce} %" + (item.MaxMoneyReduce > 0 ? $"và không quá {item.MaxMoneyReduce.ToString("###,0")} đ" : ""),
                        PercentMoneyReduce = item.PercentMoneyReduce,
                        SettingInterestReductionID = item.SettingInterestReductionID,
                        StrMoneyType = ((Transaction_TypeMoney)item.MoneyType).GetDescription()
                    });
                }
                foreach (var item in settingMoney)
                {
                    data.SettingTotalMoneyViews.Add(new Domain.Models.SettingInterestReduction.SettingTotalMoneyView()
                    {
                        ApprovalLevelBookDebtID = item.ApprovalLevelBookDebtID,
                        TotalMoney = item.TotalMoney
                    });
                }
                response.SetSucces();
                response.Data = data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetSettingByConditionsQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
