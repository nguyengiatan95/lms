﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.DebtRestructuringLoan
{
    public class GetDetailDebtRestructuringByIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long DebtRestructuringLoanID { get; set; }
    }
    public class GetDetailDebtRestructuringByIDQueryHandler : IRequestHandler<GetDetailDebtRestructuringByIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogActionDebtRestructuringLoan> _logActionDebtRestructringLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebtRestructuringLoan> _debtRestructuringLoanTab;
        readonly ILogger<GetDetailDebtRestructuringByIDQueryHandler> _logger;
        readonly Utils _common;
        public GetDetailDebtRestructuringByIDQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogActionDebtRestructuringLoan> logActionDebtRestructringLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebtRestructuringLoan> debtRestructuringLoanTab,
            ILogger<GetDetailDebtRestructuringByIDQueryHandler> logger)
        {
            _logActionDebtRestructringLoanTab = logActionDebtRestructringLoanTab;
            _logger = logger;
            _common = new Utils();
            _debtRestructuringLoanTab = debtRestructuringLoanTab;
        }

        public async Task<ResponseActionResult> Handle(GetDetailDebtRestructuringByIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var debtRestructuringLoanDetailTask = _debtRestructuringLoanTab.WhereClause(x => x.DebtRestructuringLoanID == request.DebtRestructuringLoanID).QueryAsync();
                var lstHistoryChangePhone = _logActionDebtRestructringLoanTab.WhereClause(x => x.DebtRestructuringLoanID == request.DebtRestructuringLoanID).QueryAsync();
                await Task.WhenAll(debtRestructuringLoanDetailTask, lstHistoryChangePhone);
                if (debtRestructuringLoanDetailTask.Result == null)
                {
                    response.Message = MessageConstant.NoData;
                    return response;
                }
                var debtRestructuringLoanDetail = debtRestructuringLoanDetailTask.Result.FirstOrDefault();
                response.SetSucces();
                Domain.Models.DebtRestructuringLoan.DebtRestructuringLoanModel dataResponse = new Domain.Models.DebtRestructuringLoan.DebtRestructuringLoanModel
                {
                    Histories = new List<Domain.Models.DebtRestructuringLoan.LogActionDebtRestructuringLoanItemModel>(),
                    DebtRestructuringLoanID = request.DebtRestructuringLoanID,
                    LoanContractCode = debtRestructuringLoanDetail.LoanContractCode,
                };
                foreach (var item in lstHistoryChangePhone.Result)
                {
                    dataResponse.Histories.Add(new Domain.Models.DebtRestructuringLoan.LogActionDebtRestructuringLoanItemModel
                    {
                        ActionName = ((LogActionDebtRestructuringLoan_ActionType)item.ActionType).GetDescription(),
                        CreateByName = item.CreateByName,
                        CreateDate = item.CreateDate
                    });
                }
                if (!string.IsNullOrEmpty(debtRestructuringLoanDetail.FileImage))
                {
                    dataResponse.FileImages = _common.ConvertJSonToObjectV2<List<LMS.Entites.Dtos.AG.ImageLosUpload>>(debtRestructuringLoanDetail.FileImage);
                }
                response.Data = dataResponse;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetDetailDebtRestructuringByIDQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
