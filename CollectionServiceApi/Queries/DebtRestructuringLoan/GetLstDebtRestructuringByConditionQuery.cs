﻿using CollectionServiceApi.Domain.Models.DebtRestructuringLoan;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.DebtRestructuringLoan
{
    public class GetLstDebtRestructuringByConditionQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string ContractCode { get; set; }
        public int Source { get; set; } = (int)StatusCommon.SearchAll;
        public int Status { get; set; } = (int)StatusCommon.SearchAll;
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class GetLstDebtRestructuringByConditionQueryHandler : IRequestHandler<GetLstDebtRestructuringByConditionQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebtRestructuringLoan> _debtRestructuringLoanTab;
        ILogger<GetLstDebtRestructuringByConditionQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetLstDebtRestructuringByConditionQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebtRestructuringLoan> assignmentLoanTab,
            ILogger<GetLstDebtRestructuringByConditionQueryHandler> logger)
        {
            _debtRestructuringLoanTab = assignmentLoanTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetLstDebtRestructuringByConditionQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var lstDataResult = new List<DebtRestructuringItemViewModel>();
            try
            {
                ResponseForDataTable dataTable = new ResponseForDataTable
                {
                    RecordsTotal = 0,
                    Data = lstDataResult,
                    RecordsFiltered = 0,
                    Draw = 1
                };

                response.Data = dataTable;
                response.SetSucces();
                var fromDate = DateTime.ParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                var toDate = DateTime.ParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                fromDate = fromDate.AddSeconds(-1);
                toDate = toDate.AddDays(1);
                _debtRestructuringLoanTab.WhereClause(x => x.CreateDate > fromDate && x.CreateDate < toDate);
                if (!string.IsNullOrEmpty(request.ContractCode))
                {
                    _debtRestructuringLoanTab.WhereClause(x => x.LoanContractCode == request.ContractCode);
                }
                if (request.Source != (int)StatusCommon.SearchAll)
                {
                    _debtRestructuringLoanTab.WhereClause(x => x.Source == request.Source);
                }
                if (request.Status != (int)StatusCommon.SearchAll)
                {
                    if (request.Status == (int)DebtRestructuringLoan_Status.ExpirationDate)
                    {
                        _debtRestructuringLoanTab.WhereClause(x => x.ExpirationDate < DateTime.Now.Date);
                    }
                    else
                    {
                        _debtRestructuringLoanTab.WhereClause(x => x.Status == request.Status);
                    }
                }
                var lstData = await _debtRestructuringLoanTab.QueryAsync();
                if (lstData == null || !lstData.Any())
                {
                    return response;
                }
                dataTable.RecordsTotal = lstData.Count();
                // phân trang bản ghi
                lstData = lstData.OrderByDescending(x => x.CreateDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                foreach (var item in lstData)
                {
                    Domain.Tables.DebtRestructuringLoanExtraData extraData = new Domain.Tables.DebtRestructuringLoanExtraData();
                    if (!string.IsNullOrEmpty(item.ExtraData))
                    {
                        extraData = _common.ConvertJSonToObjectV2<Domain.Tables.DebtRestructuringLoanExtraData>(item.ExtraData);
                    }
                    int status = item.Status;
                    if (item.ExpirationDate < DateTime.Now.Date)
                    {
                        status = (int)DebtRestructuringLoan_Status.ExpirationDate;
                    }
                    lstDataResult.Add(new DebtRestructuringItemViewModel
                    {
                        DebtRestructuringLoanID = item.DebtRestructuringLoanID,
                        LoanContractCode = item.LoanContractCode,
                        ProductName = item.ProductName,
                        SourceName = ((DebtRestructuringLoan_Source)item.Source).GetDescription(),
                        StatusName = ((DebtRestructuringLoan_Status)status).GetDescription(),
                        CreateByName = extraData.CreateByUserName,
                        DpdBom = item.DpdBom,
                        CreateDate = item.CreateDate,
                        DebtRestructuringStatus = status
                    });
                }

            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLstDebtRestructuringByConditionQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
