﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Report
{
    public class ReportByDebtGroupQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long MonthYear { get; set; }
        public int DebtType { get; set; }
        public int ProductTypeID { get; set; }
        public int CityID { get; set; }
    }
    public class ReportByDebtGroupQueryHandler : IRequestHandler<ReportByDebtGroupQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTrackingLoanInMonth> _trackingLoanInMonthTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblProductCredit> _productCreditTab;
        ILogger<ReportByDebtGroupQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public ReportByDebtGroupQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTrackingLoanInMonth> trackingLoanInMonthTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblProductCredit> productCreditTab,
            ILogger<ReportByDebtGroupQueryHandler> logger
            )
        {
            _trackingLoanInMonthTab = trackingLoanInMonthTab;
            _productCreditTab = productCreditTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(ReportByDebtGroupQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var lstResult = new List<Domain.Models.Report.ReportByDebtGroup>();
            try
            {
                if (request.MonthYear > 0)
                    _trackingLoanInMonthTab.WhereClause(x => x.YearMonth == request.MonthYear);

                if (request.DebtType != (int)StatusCommon.SearchAll)
                    _trackingLoanInMonthTab.WhereClause(x => x.DebtType == request.DebtType);

                if (request.ProductTypeID != (int)StatusCommon.SearchAll)
                {
                    var lstTypeProduct = (await _productCreditTab.WhereClause(x => x.TypeProduct == request.ProductTypeID).QueryAsync()).Select(x => x.TypeProduct).ToList();
                    if (lstTypeProduct.Any())
                    {
                        _trackingLoanInMonthTab.WhereClause(x => lstTypeProduct.Contains((int)x.ProductTypeID));
                    }
                }

                if (request.CityID != (int)StatusCommon.SearchAll)
                    _trackingLoanInMonthTab.WhereClause(x => x.CityID == request.CityID);

                var lstTrackingLoanInMonth = await _trackingLoanInMonthTab.SelectColumns(x => x.LoanID,
                                                                                         x => x.TotalLoanMoneyCurrent, x => x.DebtType,
                                                                                         x => x.TotalMoneyReceied).OrderBy(x => x.DebtType).QueryAsync();
                if (lstTrackingLoanInMonth.Any())
                {
                    var dictReportLoanDebtType = new Dictionary<int, List<Domain.Tables.TblTrackingLoanInMonth>>();

                    foreach (var item in lstTrackingLoanInMonth)
                    {
                        if (!dictReportLoanDebtType.Keys.Contains((int)item.DebtType))
                        {
                            dictReportLoanDebtType[item.DebtType] = new List<Domain.Tables.TblTrackingLoanInMonth>();
                        }
                        dictReportLoanDebtType[item.DebtType].Add(item);
                    }
                    foreach (var item in dictReportLoanDebtType)
                    {
                        long totalOutstandingBalanceResolved = 0;//= tổng các dòng TotalLoanMoneyCurrent khi dòng có TotalMoneyReceied > 0
                        long totalOutstandingBalanceAssigned = 0;//= tổng các dòng TotalLoanMoneyCurrent
                        long totalLoanResolved = 0;//= tổng các dòng có TotalMoneyReceied > 0
                        long totalLoanAssigned = 0; //= count LoanID
                        long totalMoneyReceied = 0; //= tổng các dòng có TotalMoneyReceied > 0
                        foreach (var k in item.Value)
                        {
                            totalLoanAssigned++;
                            totalOutstandingBalanceAssigned += k.TotalLoanMoneyCurrent;
                            if (k.TotalMoneyReceied > 0)
                            {
                                totalOutstandingBalanceResolved += k.TotalLoanMoneyCurrent;
                                totalMoneyReceied += k.TotalMoneyReceied;
                                totalLoanResolved++;
                            }
                        }
                        var objResult = new Domain.Models.Report.ReportByDebtGroup()
                        {
                            NhomNo = ((THN_DPD)item.Key).GetDescription(),
                            SoCaseGiao = totalLoanAssigned,
                            DuNo = totalOutstandingBalanceAssigned,
                            SoCaseThuDuoc = totalLoanResolved,
                            DuNoDuocGiaiQuyet = totalOutstandingBalanceResolved,
                            SoTienThuDuoc = totalMoneyReceied,
                            PhanTramTheoDuNo = Convert.ToDecimal(Math.Round((double)totalOutstandingBalanceResolved * 100 / totalOutstandingBalanceAssigned, 2)),
                            PhanTramTheoSoCase = Convert.ToDecimal(Math.Round((double)totalLoanResolved * 100 / totalLoanAssigned, 2))
                        };
                        lstResult.Add(objResult);
                    }
                }
                response.Data = lstResult;
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ReportByDebtGroupQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
