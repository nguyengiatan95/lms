﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.CollectionServices.ReportExcel;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Report
{
    public class GetLstLoanEmployeeHandleDailyQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string ReportDate { get; set; }
        public long EmployeeID { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string KeySearch { get; set; }
        public int CityID { get; set; }
        public int ProductID { get; set; }
        public GetLstLoanEmployeeHandleDailyQuery()
        {
            PageIndex = 1;
            PageSize = 20;
        }
    }
    public class GetLstLoanEmployeeHandleDailyQueryHandler : IRequestHandler<GetLstLoanEmployeeHandleDailyQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblReportEmployeeHandleLoan> _reportEmployeeHandleLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> _assignmentLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        readonly Services.IReportManager _reportManager;
        readonly ILogger<GetLstLoanEmployeeHandleDailyQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public GetLstLoanEmployeeHandleDailyQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblReportEmployeeHandleLoan> reportEmployeeHandleLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> assignmentLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            Services.IReportManager reportManager,
            ILogger<GetLstLoanEmployeeHandleDailyQueryHandler> logger
            )
        {
            _reportEmployeeHandleLoanTab = reportEmployeeHandleLoanTab;
            _userTab = userTab;
            _assignmentLoanTab = assignmentLoanTab;
            _reportManager = reportManager;
            _loanTab = loanTab;
            _customerTab = customerTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetLstLoanEmployeeHandleDailyQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            List<ReportLoanEmployeeHandleDailyDetail> lstDataHistory = new List<ReportLoanEmployeeHandleDailyDetail>();
            ResponseForDataTable dataTable = new ResponseForDataTable
            {
                RecordsTotal = 0,
                Data = lstDataHistory,
                RecordsFiltered = 0,
                Draw = 1
            };
            try
            {
                var reportDate = DateTime.ParseExact(request.ReportDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                var currentDate = DateTime.Now;
                var firstDayOfMonthCurrent = new DateTime(currentDate.Year, currentDate.Month, 1);
                var firtDateOfNextMonth = firstDayOfMonthCurrent.AddMonths(1);
                var dateAssigned = firstDayOfMonthCurrent.AddSeconds(-1);
                var endOfLastMonth = firstDayOfMonthCurrent.AddDays(-1);
                var endOfLastYear = new DateTime(currentDate.Year, 1, 1).AddDays(-1);

                var reportDetailTable = await _reportEmployeeHandleLoanTab.SetGetTop(1)
                                                                          .WhereClause(x => x.ReportDate == reportDate && x.EmployeeID == request.EmployeeID)
                                                                          .QueryAsync();

                if (reportDetailTable == null || !reportDetailTable.Any())
                {
                    return response;
                }

                var reportDetail = reportDetailTable.FirstOrDefault();
                if (reportDetail == null || reportDetail.ReportEmployeeHandleLoanID < 1 || string.IsNullOrEmpty(reportDetail.LstLoanID))
                {
                    return response;
                }
                Dictionary<long, long> dictLoanIDOfEmployeeInfos = new Dictionary<long, long>();
                try
                {
                    dictLoanIDOfEmployeeInfos = reportDetail.LstLoanID.Split(",").Select(Int64.Parse).GroupBy(x => x).ToDictionary(x => x.Key, x => x.Key);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"GetLstLoanEmployeeHandleDailyQueryHandler_Warning_LstLoanID|request={_common.ConvertObjectToJSonV2(request)}|reportDetail={_common.ConvertObjectToJSonV2(reportDetail)}|ex={ex.Message}-{ex.StackTrace}");
                    return response;
                }

                Dictionary<long, long> dictEmployeeIDs = new Dictionary<long, long>();
                Dictionary<long, long> dictLoanIDs = new Dictionary<long, long>();
                Dictionary<long, List<long>> dictLoanAndEmployees = new Dictionary<long, List<long>>();
                // đơn vay đc gán cho nhân viên
                var lstLoanID = dictLoanIDOfEmployeeInfos.Keys.ToList();

                List<long> lstCustomerIDs = new List<long>();
                // lọc lại các đơn theo điều kiện search
                if (!string.IsNullOrEmpty(request.KeySearch))
                {
                    if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternContractCode, RegexOptions.IgnoreCase).Success)
                    {
                        _loanTab.WhereClause(x => x.ContactCode == request.KeySearch);
                    }
                    else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternContractCodeLOSLoanCreditID, RegexOptions.IgnoreCase).Success)
                    {
                        var loanCreditID = long.Parse(request.KeySearch.Replace(TimaSettingConstant.PatternContractCodeLOS, "", StringComparison.OrdinalIgnoreCase));
                        _loanTab.WhereClause(x => x.LoanCreditIDOfPartner == loanCreditID);
                    }
                    else
                    {
                        if (request.KeySearch.Length == TimaSettingConstant.MaxLengthPhone && Regex.Match(request.KeySearch, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success)
                        {
                            var lstCustomerIDsTask = await _customerTab.SelectColumns(x => x.CustomerID).WhereClause(x => x.Phone == request.KeySearch).QueryAsync();
                            lstCustomerIDs = lstCustomerIDsTask.Select(x => x.CustomerID).ToList();
                        }
                        else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternNumberCard, RegexOptions.IgnoreCase).Success)
                        {
                            var lstCustomerIDsTask = await _customerTab.SelectColumns(x => x.CustomerID).WhereClause(x => x.NumberCard == request.KeySearch).QueryAsync();
                            lstCustomerIDs = lstCustomerIDsTask.Select(x => x.CustomerID).ToList();
                        }
                        if (lstCustomerIDs != null && lstCustomerIDs.Count > 0)
                        {
                            _loanTab.WhereClause(x => lstCustomerIDs.Contains((long)x.CustomerID));
                        }
                        else
                        {
                            _loanTab.WhereClause(x => x.CustomerName.Contains(request.KeySearch));
                        }
                    }
                }
                if (request.CityID != (int)LMS.Common.Constants.StatusCommon.SearchAll)
                {
                    _loanTab.WhereClause(x => x.CityID == request.CityID);
                }
                if (request.ProductID != (int)LMS.Common.Constants.StatusCommon.SearchAll)
                {
                    _loanTab.WhereClause(x => x.ProductID == request.ProductID);
                }
                long minLoanIDAssigned = int.MaxValue;
                long maxLoanIDAssgined = 0;
                if (lstLoanID.Count > TimaSettingConstant.MaxItemQuery)
                {
                    foreach (var item in lstLoanID)
                    {
                        if (item > maxLoanIDAssgined)
                        {
                            maxLoanIDAssgined = item;
                        }
                        else if (item < minLoanIDAssigned)
                        {
                            minLoanIDAssigned = item;
                        }
                    }
                    maxLoanIDAssgined += 1;
                    minLoanIDAssigned -= 1;
                    _loanTab.WhereClause(x => x.LoanID > minLoanIDAssigned && x.LoanID < maxLoanIDAssgined);
                }
                else
                {
                    _loanTab.WhereClause(x => lstLoanID.Contains(x.LoanID));
                }
                var lstLoanInfos = (await _loanTab.QueryAsync()).ToList();

                List<Domain.Tables.TblLoan> lstLoanInfosAssigned = new List<Domain.Tables.TblLoan>();
                if (maxLoanIDAssgined != 0)
                {
                    foreach (var item in lstLoanInfos)
                    {
                        if (dictLoanIDOfEmployeeInfos.ContainsKey(item.LoanID))
                        {
                            lstLoanInfosAssigned.Add(item);
                        }
                    }
                }
                else
                {
                    lstLoanInfosAssigned = lstLoanInfos;
                }

                dataTable.RecordsTotal = lstLoanInfosAssigned.Count;
                //long sumCustomerTotalMoney = 6;
                //long sumMoneyFineLate = 1;
                //long sumOldDebitMoney = 2;
                //long sumTotalMoneyNeedPay = 3;
                //long sumTotalInterest = 4;
                //long sumMoneyOriginal = 5;
                lstLoanInfosAssigned = lstLoanInfosAssigned.OrderBy(x => x.NextDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();

                lstLoanID = lstLoanInfosAssigned.Select(x => x.LoanID).Distinct().ToList();
                var dictPlanCloseLoanInfosTask = _reportManager.GetDictPlanCloseLoanByLstLoanID(lstLoanID, reportDate);
                var dictCustomerInfosTask = _reportManager.GetDictCustomerByLstLoanID(lstLoanID);
                var dictCutOffLoanReportDateInfosTask = _reportManager.GetDictCutOffLoanByLstLoanID(lstLoanID, reportDate);
                await Task.WhenAll(dictPlanCloseLoanInfosTask, dictCustomerInfosTask, dictCutOffLoanReportDateInfosTask);

                foreach (var item in lstLoanInfosAssigned)
                {
                    var cutOffLoan = dictCutOffLoanReportDateInfosTask.Result.GetValueOrDefault(item.LoanID);
                    var planCloseLoanInfo = dictPlanCloseLoanInfosTask.Result.GetValueOrDefault(item.LoanID);
                    var customerInfos = dictCustomerInfosTask.Result.GetValueOrDefault((long)item.CustomerID);
                    DateTime nextDate = item.NextDate.Value;
                    if (reportDate.Date != currentDate.Date)
                    {
                        // trường hợp dữ liệu bị thiếu chưa tổng hợp nên để = hiện tại
                        if (cutOffLoan != null)
                        {
                            nextDate = cutOffLoan.NextDate;
                        }
                    }
                    lstDataHistory.Add(new ReportLoanEmployeeHandleDailyDetail
                    {
                        LoanID = item.LoanID,
                        LoanContractCode = item.ContactCode,
                        CustomerName = item.CustomerName,
                        CountDPD = Convert.ToInt32(reportDate.Date.Subtract(nextDate).TotalDays),
                        LoanProductName = item.ProductName,
                        CustomerTotalMoney = customerInfos?.TotalMoney ?? -1,
                        MoneyConsultant = planCloseLoanInfo?.MoneyConsultant ?? -1,
                        MoneyFineLate = planCloseLoanInfo?.MoneyFineLate ?? -1,
                        MoneyService = planCloseLoanInfo?.MoneyService ?? -1,
                        MoneyInterest = planCloseLoanInfo?.MoneyInterest ?? -1,
                        OldDebitMoney = planCloseLoanInfo?.TotalMoneyReceivables ?? -1,
                        MoneyOriginal = planCloseLoanInfo?.MoneyOriginal ?? -1,
                        TotalMoneyNeedPay = planCloseLoanInfo?.TotalMoneyClose ?? -1,
                    });
                }
                //if (lstDataHistory.Count > 0)
                //{
                //    lstDataHistory[0].SumCustomerTotalMoney = sumCustomerTotalMoney;
                //    lstDataHistory[0].SumTotalMoneyNeedPay = sumTotalMoneyNeedPay;
                //    lstDataHistory[0].SumMoneyFineLate = sumMoneyFineLate;
                //    lstDataHistory[0].SumOldDebitMoney = sumOldDebitMoney;
                //    lstDataHistory[0].SumMoneyOriginal = sumMoneyOriginal;
                //    lstDataHistory[0].SumTotalInterest = sumTotalInterest;
                //}
                response.SetSucces();
                response.Data = dataTable;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetReportEmployeeWithLoanQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
