﻿using CollectionServiceApi.Domain.Models.Excel;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Report
{
    public class GetReportDebtTypeQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long MonthYear { get; set; }
        public int ProductID { get; set; }
        public int CityID { get; set; }
        public int DebtType { get; set; }

    }
    public class GetReportDebtTypeQueryHandler : IRequestHandler<GetReportDebtTypeQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTrackingLoanInMonth> _trackingLoanInMonthTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        ILogger<GetReportDebtTypeQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetReportDebtTypeQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTrackingLoanInMonth> trackingLoanInMonthTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
        ILogger<GetReportDebtTypeQueryHandler> logger)
        {
            _trackingLoanInMonthTab = trackingLoanInMonthTab;
            _loanTab = loanTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetReportDebtTypeQuery requestModel, CancellationToken cancellationToken)
        {
            int pageSize = TimaSettingConstant.MaxItemQuery;
            int numberRow = 1;
            ResponseActionResult response = new ResponseActionResult();
            List<HistoryTrackingLoanInMonth> lstReport = new List<HistoryTrackingLoanInMonth>();
            try
            {
                // dữ liệu search
                if (requestModel.ProductID != (int)StatusCommon.SearchAll)
                {
                    _trackingLoanInMonthTab.WhereClause(x => x.ProductTypeID == requestModel.ProductID);
                }
                if (requestModel.CityID != (int)StatusCommon.SearchAll)
                {
                    _trackingLoanInMonthTab.WhereClause(x => x.CityID == requestModel.CityID);
                }
                if (requestModel.DebtType != (int)StatusCommon.SearchAll)
                {
                    _trackingLoanInMonthTab.WhereClause(x => x.DebtType == requestModel.DebtType);
                }
                // lấy dữ liệu tracking
                var tracking = await _trackingLoanInMonthTab.WhereClause(x => x.YearMonth == requestModel.MonthYear).QueryAsync();
                if (tracking == null || !tracking.Any())
                {
                    response.SetSucces();
                    response.Total = 0;
                }
                numberRow = tracking.Count() / pageSize + 1;
                tracking = tracking.OrderBy(x => x.LoanID);
                // lấy loan + customer
                foreach (var item in tracking)
                {
                    var jsonData = _common.ConvertJSonToObjectV2<ExtraDataTracking>(item.ExtraData);
                    lstReport.Add(new HistoryTrackingLoanInMonth()
                    {
                        LoanID = item.LoanID,
                        Address = jsonData.Address,
                        ContractCode = jsonData.ContractCode,
                        CustomerName = jsonData.CustomerName,
                        DebtType = ((THN_DPD)item.DebtType).GetDescription(),
                        FinishDate = item.LoanFinshedDate,
                        TotalMoneyCurrent = item.TotalLoanMoneyCurrent,
                        TotalMoneyReceived = item.TotalMoneyReceied,
                        FromDate = jsonData.FromDate,
                        ToDate = jsonData.ToDate,
                    });
                }
                response.Data = lstReport;
                response.SetSucces();
                response.Total = lstReport.Count();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetReportDebtTypeQueryHandler_Exception|request={_common.ConvertObjectToJSonV2(requestModel)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
