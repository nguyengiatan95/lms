﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.CollectionServices.ReportExcel;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Report
{
    public class GetReportEmployeeWithLoanQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string ReportDate { get; set; }
        public long StaffID { get; set; }
        public long LeaderID { get; set; }
        public long ManagerID { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public GetReportEmployeeWithLoanQuery()
        {
            StaffID = (int)LMS.Common.Constants.StatusCommon.SearchAll;
            LeaderID = (int)LMS.Common.Constants.StatusCommon.SearchAll;
            ManagerID = (int)LMS.Common.Constants.StatusCommon.SearchAll;
            PageIndex = 1;
            PageSize = 20;
        }
    }
    public class GetReportEmployeeWithLoanQueryHandler : IRequestHandler<GetReportEmployeeWithLoanQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> _excelReportTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblReportEmployeeHandleLoan> _reportEmployeeHandleLoanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> _assignmentLoanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> _userDepartmentTab;
        ILogger<GetReportEmployeeWithLoanQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetReportEmployeeWithLoanQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> excelReportTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblReportEmployeeHandleLoan> reportEmployeeHandleLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> assignmentLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDepartmentTab,
            ILogger<GetReportEmployeeWithLoanQueryHandler> logger
            )
        {
            _excelReportTab = excelReportTab;
            _userTab = userTab;
            _reportEmployeeHandleLoanTab = reportEmployeeHandleLoanTab;
            _assignmentLoanTab = assignmentLoanTab;
            _userDepartmentTab = userDepartmentTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetReportEmployeeWithLoanQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            List<ReportEmployeeHandleLoanDailyDetail> lstDataHistory = new List<ReportEmployeeHandleLoanDailyDetail>();
            ResponseForDataTable dataTable = new ResponseForDataTable
            {
                RecordsTotal = 0,
                Data = lstDataHistory,
                RecordsFiltered = 0,
                Draw = 1
            };
            try
            {
                var reportDate = DateTime.ParseExact(request.ReportDate, TimaSettingConstant.DateTimeDayMonthYear, null);

                // lấy danh sách nhân viên theo cấp độ dựa vào bảng chia loan, tạm thời dùng cách này
                var currentDate = DateTime.Now;
                var firstDayOfMonthCurrent = new DateTime(currentDate.Year, currentDate.Month, 1);
                var dateAssigned = firstDayOfMonthCurrent.AddSeconds(-1);
                var endOfLastMonth = firstDayOfMonthCurrent.AddDays(-1);
                // xuất file danh sách đầu tháng hiện tại
                // lấy danh sách loan dc gán cho nhân viên
                var lstLoanAssignedTask = _assignmentLoanTab.SelectColumns(x => x.LoanID, x => x.UserID).WhereClause(x => x.DateAssigned > dateAssigned).QueryAsync();
                var lstUserDeparmentTask = _userDepartmentTab.QueryAsync();

                await Task.WhenAll(lstLoanAssignedTask, lstUserDeparmentTask);

                var lstLoanAssigned = lstLoanAssignedTask.Result;
                var lstUserDepartment = lstUserDeparmentTask.Result;

                Dictionary<long, long> dictEmployeeIDs = new Dictionary<long, long>();
                Dictionary<long, long> dictLoanIDs = new Dictionary<long, long>();
                Dictionary<long, List<long>> dictLoanAndEmployees = new Dictionary<long, List<long>>();
                List<long> lstEmployeeSelected = new List<long>();
                Dictionary<long, long> dictLoanIDOfEmployeeInfos = new Dictionary<long, long>();
                if (request.ManagerID != (int)LMS.Common.Constants.StatusCommon.SearchAll)
                {
                    lstEmployeeSelected.Add(request.ManagerID);
                }
                if (request.LeaderID != (int)LMS.Common.Constants.StatusCommon.SearchAll)
                {
                    lstEmployeeSelected.Add(request.LeaderID);
                }
                if (request.StaffID != (int)LMS.Common.Constants.StatusCommon.SearchAll)
                {
                    lstEmployeeSelected.Add(request.StaffID);
                }
                if (lstEmployeeSelected.Count > 0)
                {
                    dictLoanIDOfEmployeeInfos = lstLoanAssigned.Where(x => lstEmployeeSelected.Contains(x.UserID)).GroupBy(x => x.LoanID).ToDictionary(x => x.Key, x => x.Key);
                }
                else
                {
                    dictLoanIDOfEmployeeInfos = lstLoanAssigned.GroupBy(x => x.LoanID).ToDictionary(x => x.Key, x => x.Key);
                }
                foreach (var item in lstLoanAssigned)
                {
                    if (!dictLoanIDOfEmployeeInfos.ContainsKey(item.LoanID))
                    {
                        continue;
                    }
                    if (!dictEmployeeIDs.ContainsKey(item.UserID))
                    {
                        dictEmployeeIDs.Add(item.UserID, item.UserID);
                    }
                }
                var lstEmployeeID = new List<long>();
                // lọc lại danh sách nhân viên theo điều kiện cấp bậc search
                foreach (var userid in dictEmployeeIDs.Values)
                {
                    if (request.ManagerID == (int)LMS.Common.Constants.StatusCommon.SearchAll
                        && request.LeaderID == (int)LMS.Common.Constants.StatusCommon.SearchAll
                        && request.StaffID == (int)LMS.Common.Constants.StatusCommon.SearchAll)
                    {
                        lstEmployeeID.Add(userid);
                    }
                    else
                    {
                        var userInfo = lstUserDepartment.FirstOrDefault(x => x.UserID == userid);
                        if (userInfo == null || userInfo.UserID < 1)
                        {
                            continue;
                        }
                        switch ((UserDepartment_PositionID)userInfo.PositionID)
                        {
                            case UserDepartment_PositionID.Manager:
                                if (request.ManagerID == (int)LMS.Common.Constants.StatusCommon.SearchAll || request.ManagerID == userInfo.UserID)
                                {
                                    lstEmployeeID.Add(userid);
                                }
                                break;
                            case UserDepartment_PositionID.TeamLead:
                                if (request.LeaderID > 0)
                                {
                                    if (request.LeaderID == userid)
                                        lstEmployeeID.Add(userid);
                                }
                                else if (request.ManagerID == (int)LMS.Common.Constants.StatusCommon.SearchAll || request.ManagerID > 0)
                                {
                                    lstEmployeeID.Add(userid);
                                }
                                break;
                            case UserDepartment_PositionID.Staff:
                                if (request.StaffID > 0)
                                {
                                    if (request.StaffID == userid)
                                    {
                                        lstEmployeeID.Add(userid);
                                    }
                                }
                                else if ((request.ManagerID == (int)LMS.Common.Constants.StatusCommon.SearchAll || request.ManagerID > 0)
                                    || (request.LeaderID == (int)LMS.Common.Constants.StatusCommon.SearchAll || request.LeaderID > 0)
                                    )
                                {
                                    lstEmployeeID.Add(userid);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
                if (lstEmployeeID.Count > 0)
                {
                    var lstUserInfoTask = _reportEmployeeHandleLoanTab.WhereClause(x => x.ReportDate == reportDate && lstEmployeeID.Contains(x.EmployeeID)).QueryAsync();
                    var lstUserTabTask = _userTab.SelectColumns(x => x.UserID, x => x.UserName).WhereClause(x => lstEmployeeID.Contains(x.UserID)).QueryAsync();
                    await Task.WhenAll(lstUserInfoTask, lstUserTabTask);

                    var lstUserInfo = lstUserInfoTask.Result;
                    var dictUserInfos = lstUserTabTask.Result.ToDictionary(x => x.UserID, x => x);
                    dataTable.RecordsTotal = lstUserInfo.Count();
                    //int sumTotalLoanHandle = 0;
                    //int sumNumberLoanHandleDone = 0;
                    ////int sumNumberLoanNeedCollection = 0;
                    ////int sumNumberCustomerTopUp = 0;
                    //int sumNumberComment = 0;

                    lstUserInfo = lstUserInfo.OrderBy(x => x.TotalLoanHandle).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                    foreach (var item in lstUserInfo)
                    {
                        lstDataHistory.Add(new ReportEmployeeHandleLoanDailyDetail
                        {
                            EmployeeID = item.EmployeeID,
                            EmployeeName = dictUserInfos.GetValueOrDefault(item.EmployeeID)?.UserName,
                            TotalLoanHandle = item.TotalLoanHandle,
                            NumberLoanHandleDone = item.NumberLoanHandleDone,
                            //NumberLoanNeedCollection = item.NumberLoanNeedCollection,
                            //NumberCustomerTopUp = item.NumberCustomerTopUp,
                            NumberComment = item.NumberComment,
                        });
                        //sumNumberComment += item.NumberComment;
                        //sumTotalLoanHandle += item.TotalLoanHandle;
                        //sumNumberLoanHandleDone += item.NumberLoanHandleDone;
                        //sumNumberLoanNeedCollection += item.NumberLoanNeedCollection;
                        //sumNumberCustomerTopUp += item.NumberCustomerTopUp;
                    }

                    //if (lstDataHistory.Count > 0)
                    //{
                    //    lstDataHistory[0].SumNumberComment = sumNumberComment;
                    //    lstDataHistory[0].SumTotalLoanHandle = sumTotalLoanHandle;
                    //    lstDataHistory[0].SumNumberLoanHandleDone = sumNumberLoanHandleDone;
                    //    //lstDataHistory[0].SumNumberLoanNeedCollection = sumNumberLoanNeedCollection;
                    //    //lstDataHistory[0].SumNumberCustomerTopUp = sumNumberCustomerTopUp;
                    //}
                }
                response.SetSucces();
                response.Data = dataTable;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetReportEmployeeWithLoanQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
