﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Report
{
    public class ReportDayPlanQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int TypeID { get; set; }
        public int DepartmentManager { get; set; }
        public int DepartmentLeader { get; set; }
        public int Staff { get; set; }
        public int Status { get; set; }
        public int UserID { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }

    }
    public class ReportDayPlanQueryHandler : IRequestHandler<ReportDayPlanQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> _planHandleLoanDailyTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> _userDepartmentTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> _departmentTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> _assignmentLoanTab;
        readonly ILogger<ReportDayPlanQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public ReportDayPlanQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> planHandleLoanDailyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDepartmentTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> departmentTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> assignmentLoanTab,
            ILogger<ReportDayPlanQueryHandler> logger
            )
        {
            _planHandleLoanDailyTab = planHandleLoanDailyTab;
            _loanTab = loanTab;
            _userTab = userTab;
            _userDepartmentTab = userDepartmentTab;
            _departmentTab = departmentTab;
            _assignmentLoanTab = assignmentLoanTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(ReportDayPlanQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var lstResult = new Domain.Models.Report.ReportDayPlan();

            try
            {
                var toDate = DateTime.Now;
                var fromDate = new DateTime(toDate.Year, toDate.Month, 1);

                if (!string.IsNullOrEmpty(request.FromDate))
                    fromDate = DateTime.ParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                if (!string.IsNullOrEmpty(request.ToDate))
                    toDate = DateTime.ParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, null);

                if (toDate.Month != fromDate.Month || toDate.Year != fromDate.Year)
                {
                    response.Message = MessageConstant.ReportDayPlanCheckTime;
                    return response;
                }
                fromDate = fromDate.AddSeconds(-1);
                toDate = toDate.AddDays(1);
                if (request.TypeID == 1)//lọc theo ngày lên kế hoạch hoặc ngày kế thúc
                    _planHandleLoanDailyTab.WhereClause(x => (x.FromDate > fromDate && x.FromDate < toDate) || (x.ToDate > fromDate && x.ToDate < toDate));
                else//lọc theo ngày thực hiện kế hoạch
                    _planHandleLoanDailyTab.WhereClause(x => x.ModifyDate > fromDate && x.ModifyDate < toDate && x.Status == (int)PlanHandleLoanDaily_Status.Processed);

                //trạng thái
                if (request.Status != (int)StatusCommon.SearchAll)
                    _planHandleLoanDailyTab.WhereClause(x => x.Status == request.Status);
                else if (request.Status == (int)PlanHandleLoanDaily_Status.NoProcess)
                    _planHandleLoanDailyTab.WhereClause(x => x.Status != (int)PlanHandleLoanDaily_Status.Processed);

                var lstUserID = new List<long>();
                if (request.Staff != (int)StatusCommon.SearchAll)
                    _planHandleLoanDailyTab.WhereClause(x => x.UserID == request.Staff);
                else
                {
                    if (request.DepartmentLeader != (int)StatusCommon.SearchAll)
                    {
                        //lấy danh sách các nhân viên team leader
                        lstUserID = (await _userDepartmentTab.WhereClause(x => x.ParentUserID == request.DepartmentLeader && x.PositionID == (int)UserDepartment_PositionID.Staff).QueryAsync()).Select(x => x.UserID).ToList();
                    }
                    else
                    {
                        if (request.DepartmentManager != (int)StatusCommon.SearchAll)
                        {
                            //lấy các nhân viên thuộc leader của trưởng phòng
                            var lstDepartmentLeader = (await _userDepartmentTab.WhereClause(x => x.ParentUserID == request.DepartmentManager && x.PositionID == (int)UserDepartment_PositionID.TeamLead).QueryAsync()).Select(x => x.UserID);
                            if (lstDepartmentLeader != null)
                                lstUserID = (await _userDepartmentTab.WhereClause(x => lstDepartmentLeader.Contains(x.ParentUserID) && x.PositionID == (int)UserDepartment_PositionID.Staff).QueryAsync()).Select(x => x.UserID).ToList();
                        }
                        else// lấy tất cả theo user login
                        {
                            lstUserID = await GetUserID(request.UserID);
                        }
                    }
                    _planHandleLoanDailyTab.WhereClause(x => lstUserID.Contains((long)x.UserID));
                }
                var planHandleLoanDaily = (await _planHandleLoanDailyTab.QueryAsync()).ToList();
                var dictPlanHandleLoanDaily = new Dictionary<long, List<Domain.Tables.TblPlanHandleLoanDaily>>();
                if (planHandleLoanDaily.Any())
                {
                    planHandleLoanDaily = planHandleLoanDaily.OrderByDescending(x => x.CreateDate).ToList();
                    var lstStaff = new List<long>();
                    foreach (var item in planHandleLoanDaily)
                    {
                        lstStaff.Add((long)item.UserID);
                        if (!dictPlanHandleLoanDaily.ContainsKey(item.LoanID))
                            dictPlanHandleLoanDaily[item.LoanID] = new List<Domain.Tables.TblPlanHandleLoanDaily>();
                        dictPlanHandleLoanDaily[item.LoanID].Add(item);
                    }

                    var loanTask = _loanTab.SelectColumns(x => x.LoanID, x => x.ContactCode, x => x.CustomerName).WhereClause(x => dictPlanHandleLoanDaily.Keys.Contains(x.LoanID)).QueryAsync();
                    await Task.WhenAll(loanTask);
                    var dictLoan = loanTask.Result.ToDictionary(x => x.LoanID, x => x);
                    var lstAssignmentLoan = (await _assignmentLoanTab.SelectColumns(x => x.LoanID, x => x.Type, x => x.UserID).WhereClause(x => lstStaff.Contains(x.UserID) && dictPlanHandleLoanDaily.Keys.Contains(x.LoanID)).QueryAsync());
                    var dictUser = await GetParentUser(lstStaff);
                    var totalcount = 0;
                    var processed = 0;
                    var noProcess = 0;
                    foreach (var item in dictPlanHandleLoanDaily)
                    {
                       
                        string typeName = string.Empty;
                        string status = PlanHandleLoanDaily_Status.NoProcess.GetDescription();
                        var handlingDate = new DateTime();
                        var enDate = new DateTime();
                        if (item.Value != null)
                        {
                            //Đơn chưa xử lý và chuyển cho người khác thì không tính
                            if (!item.Value.Any(x => x.Status == (int)PlanHandleLoanDaily_Status.Processed) && item.Value.Any(x => x.Status == (int)PlanHandleLoanDaily_Status.Moved))
                                continue;

                            var assignmentLoan = lstAssignmentLoan.Where(x => x.LoanID == item.Key && x.UserID == item.Value[0].UserID).Select(x => x.Type).Distinct().ToList();
                            if (assignmentLoan.Any())
                            {
                                foreach (var type in assignmentLoan)
                                {
                                    typeName += $"{((AssignmentLoan_Type)type).GetDescription()} ";
                                }
                            }
                            //Đơn chưa xử lý và chuyển cho người khác thì không tính
                            if (!item.Value.Any(x=>x.Status == (int)PlanHandleLoanDaily_Status.Processed) && item.Value.Any(x => x.Status == (int)PlanHandleLoanDaily_Status.Moved))
                            {
                                continue;
                            }
                            var objDayplan = item.Value.Where(x => x.Status == (int)PlanHandleLoanDaily_Status.Processed).FirstOrDefault();
                            if (objDayplan != null)
                            {
                                processed++;
                                handlingDate = (DateTime)objDayplan.ModifyDate;
                                status = PlanHandleLoanDaily_Status.Processed.GetDescription();
                            }
                            else
                            {
                                noProcess++;
                            }
                            enDate = item.Value.Max(x => x.ToDate);
                            totalcount++;
                        }
                        var objUser = dictUser.GetValueOrDefault((long)item.Value[0].UserID);
                        var objLoan = dictLoan.GetValueOrDefault(item.Key);
                        lstResult.ReportDayPlanData.Add(new Domain.Models.Report.ListLoanDayPlan
                        {
                            ContractCode = objLoan.ContactCode,
                            CustomerName = objLoan.CustomerName,
                            TypeName = typeName,
                            CodeStaff = objUser?.UserName,
                            Leader = objUser.Leader,
                            Manager = objUser.Manager,
                            Director = objUser.Director,
                            DayPlan = item.Value[0].CreateDate,
                            HandlingDate = handlingDate,
                            EndDate = enDate,
                            Status = status
                        });
                    }
                    lstResult.ReportDayPlanData = lstResult.ReportDayPlanData.Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                    lstResult.TotalCount = totalcount;
                    lstResult.Processed = processed;
                    lstResult.NoProcessed = noProcess;

                    response.Total = totalcount;
                }
                response.Data = lstResult;
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ReportDayPlanQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task<List<long>> GetUserID(long UserID)
        {
            var lstUserID = new List<long>();
            var objUserDepartment = (await _userDepartmentTab.WhereClause(x => x.UserID == UserID).QueryAsync()).FirstOrDefault();
            if (objUserDepartment != null)
            {
                switch ((UserDepartment_PositionID)objUserDepartment.PositionID)
                {
                    case UserDepartment_PositionID.Director:
                        var lstDepartment = (await _departmentTab.WhereClause(x => x.AppID == (int)Menu_AppID.THN).QueryAsync()).Select(x => x.DepartmentID).ToList();
                        lstUserID = (await _userDepartmentTab.WhereClause(x => lstDepartment.Contains(x.DepartmentID)).QueryAsync()).Select(x => x.UserID).ToList();
                        break;
                    case UserDepartment_PositionID.Manager:
                        var lstDepartmentLeader = (await _userDepartmentTab.WhereClause(x => x.ParentUserID == UserID && x.PositionID == (int)UserDepartment_PositionID.TeamLead).QueryAsync()).Select(x => x.UserID);
                        lstUserID = (await _userDepartmentTab.WhereClause(x => lstDepartmentLeader.Contains(x.ParentUserID) && x.PositionID == (int)UserDepartment_PositionID.Staff).QueryAsync()).Select(x => x.UserID).ToList();
                        break;
                    case UserDepartment_PositionID.TeamLead:
                        lstUserID = (await _userDepartmentTab.WhereClause(x => x.ParentUserID == UserID && x.PositionID == (int)UserDepartment_PositionID.Staff).QueryAsync()).Select(x => x.UserID).ToList();
                        break;
                    case UserDepartment_PositionID.Staff:
                        lstUserID.Add(UserID);
                        break;
                }
            }
            return lstUserID;
        }

        public async Task<Dictionary<long, Domain.Models.Report.UserReportDayPlan>> GetParentUser(List<long> LstUserID)
        {
            LstUserID = LstUserID.Distinct().ToList();
            var dictResult = new Dictionary<long, Domain.Models.Report.UserReportDayPlan>();
            var lstUserGetName = new List<long>();
            lstUserGetName.AddRange(LstUserID);
            var lstParentLeader = new List<Domain.Tables.TblUserDepartment>();
            var lstParentManager = new List<Domain.Tables.TblUserDepartment>();
            var lstParentDirector = new List<Domain.Tables.TblUserDepartment>();

            lstParentLeader = (await _userDepartmentTab.SelectColumns(x => x.ParentUserID, x => x.UserID).WhereClause(x => LstUserID.Contains(x.UserID)).QueryAsync()).ToList();
            if (lstParentLeader != null)
            {
                //leader
                var lstParentLeaderID = lstParentLeader.Select(x => x.ParentUserID).ToList();
                lstUserGetName.AddRange(lstParentLeaderID);

                lstParentManager = (await _userDepartmentTab.SelectColumns(x => x.ParentUserID, x => x.UserID).WhereClause(x => lstParentLeaderID.Contains(x.UserID)).QueryAsync()).ToList();
                if (lstParentManager != null)
                {
                    //truowng phong
                    var lstParentManagerID = lstParentManager.Select(x => x.ParentUserID).ToList();
                    lstUserGetName.AddRange(lstParentManagerID);
                    lstParentDirector = (await _userDepartmentTab.SelectColumns(x => x.ParentUserID, x => x.UserID).WhereClause(x => lstParentManagerID.Contains(x.UserID)).QueryAsync()).ToList();
                    if (lstParentDirector != null)
                    {
                        lstUserGetName.AddRange(lstParentDirector.Select(x => x.ParentUserID).ToList());
                    }
                }
            }
            var dictUser = (await _userTab.SelectColumns(x => x.UserID, x => x.UserName, x => x.FullName).WhereClause(x => lstUserGetName.Contains(x.UserID)).QueryAsync()).ToDictionary(x => x.UserID, x => x);
            foreach (var item in LstUserID)
            {
                var userReportDayPlan = new Domain.Models.Report.UserReportDayPlan();
                userReportDayPlan.UserName = dictUser.GetValueOrDefault(item)?.UserName;

                var leader = lstParentLeader.Where(x => x.UserID == item).FirstOrDefault();
                if (leader != null)
                {
                    userReportDayPlan.Leader = dictUser.GetValueOrDefault(leader.ParentUserID)?.UserName;

                    var manager = lstParentManager.Where(x => x.UserID == leader.ParentUserID).FirstOrDefault();
                    if (manager != null)
                    {
                        userReportDayPlan.Manager = dictUser.GetValueOrDefault(manager.ParentUserID)?.UserName;
                        var director = lstParentDirector.Where(x => x.UserID == manager.ParentUserID).FirstOrDefault();
                        if (director != null)
                        {
                            userReportDayPlan.Director = dictUser.GetValueOrDefault(director.ParentUserID)?.UserName;
                        }
                    }
                }
                dictResult[item] = userReportDayPlan;
            }
            return dictResult;
        }
    }
}
