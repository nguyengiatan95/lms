﻿using CollectionServiceApi.Domain;
using CollectionServiceApi.Domain.Models.Invoice;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Invoice
{
    public class SearchHistoryTransactionQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public RequestHistoryCustomerTopupModel Model { get; set; }
    }
    public class SearchHistoryTransactionQueryHandler : IRequestHandler<SearchHistoryTransactionQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankCardTab;
        ILogger<SearchHistoryTransactionQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public SearchHistoryTransactionQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankCardTab,
            ILogger<SearchHistoryTransactionQueryHandler> logger)
        {
            _invoiceTab = invoiceTab;
            _userTab = userTab;
            _loanTab = loanTab;
            _customerTab = customerTab;
            _bankCardTab = bankCardTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(SearchHistoryTransactionQuery requestModel, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            RequestHistoryCustomerTopupModel request = requestModel.Model;
            var result = new List<HistoryCustomerTopupItem>();
            try
            {
                var lstInvoiceSubType = new List<int>
                {
                    (int)GroupInvoiceType.ReceiptCustomer,
                    (int)GroupInvoiceType.ReceiptOnBehalfShop,
                    (int)GroupInvoiceType.ReceiptCustomerConsider
                };
                var dictCustomerInfor = new Dictionary<long, Domain.Tables.TblCustomer>();
                var objCusomter = new Domain.Tables.TblCustomer();
                int totalCount = 0;
                var fromDate = DateTime.ParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture);
                toDate = toDate.AddDays(1);
                fromDate = fromDate.AddSeconds(-1);
                if (!string.IsNullOrEmpty(request.KeySearch))
                {
                    if (long.TryParse(request.KeySearch, out long contracCodeID) && request.KeySearch.Length < TimaSettingConstant.MaxLengthPhone - 1)
                    {
                        request.KeySearch = $"{TimaSettingConstant.PrefixContractCode}{contracCodeID}";
                        var custommerIDs = (await _loanTab.SelectColumns(x => x.CustomerID).WhereClause(x => x.ContactCode == request.KeySearch).QueryAsync()).Select(x => (long)x.CustomerID);
                        _customerTab.WhereClause(x => custommerIDs.Contains(x.CustomerID));
                    }
                    else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternContractCode, RegexOptions.IgnoreCase).Success)
                    {
                        var custommerIDs = (await _loanTab.SelectColumns(x => x.CustomerID).WhereClause(x => x.ContactCode == request.KeySearch).QueryAsync()).Select(x => (long)x.CustomerID);
                        _customerTab.WhereClause(x => custommerIDs.Contains(x.CustomerID));
                    }
                    else if (request.KeySearch.Length == TimaSettingConstant.MaxLengthPhone && Regex.Match(request.KeySearch, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success)
                        _customerTab.WhereClause(x => x.Phone == request.KeySearch);
                    else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternNumberCard, RegexOptions.IgnoreCase).Success)
                        _customerTab.WhereClause(x => x.NumberCard == request.KeySearch);
                    else
                        _customerTab.WhereClause(x => x.FullName == request.KeySearch);

                    dictCustomerInfor = (await _customerTab.QueryAsync()).ToDictionary(x => x.CustomerID, x => x);

                    if (dictCustomerInfor.Keys.Count == 0)
                    {
                        response.Message = MessageConstant.ErrorNotExist;
                        return response;
                    }
                    if (dictCustomerInfor.Keys.Count > 0)
                        _invoiceTab.WhereClause(x => dictCustomerInfor.Keys.Contains((long)x.SourceID));
                }
                if (request.BankCardID != (int)StatusCommon.SearchAll)
                {
                    _invoiceTab.WhereClause(x => x.BankCardID == request.BankCardID);
                }
                var lstInvoice = await _invoiceTab.WhereClause(x => lstInvoiceSubType.Contains(x.InvoiceSubType) && (x.CreateDate > fromDate && x.CreateDate < toDate) && x.Status == (int)Invoice_Status.Confirmed).QueryAsync();
                totalCount = lstInvoice.Count();
                if (totalCount > 0)
                {
                    lstInvoice = lstInvoice.OrderByDescending(x => x.CreateDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                    var lstBankCardID = new List<long>();
                    var lstUserIdCreate = new List<long>();
                    var lstCustomerID = new List<long>();
                    foreach (var item in lstInvoice)
                    {
                        lstBankCardID.Add(item.BankCardID);
                        lstUserIdCreate.Add((long)item.UserIDCreate);
                        lstCustomerID.Add((long)item.CustomerID);
                    }
                    var lstBankCard = _bankCardTab.WhereClause(x => lstBankCardID.Contains(x.BankCardID)).QueryAsync();
                    var lstUser = _userTab.WhereClause(x => lstUserIdCreate.Contains(x.UserID)).QueryAsync();
                    var lstCustomer = _customerTab.WhereClause(x => lstCustomerID.Contains(x.CustomerID)).QueryAsync();
                    await Task.WhenAll(lstBankCard, lstUser, lstCustomer);

                    var dictBankCardInfos = lstBankCard.Result.ToDictionary(x => x.BankCardID, x => x);
                    var dictUserInfos = lstUser.Result.ToDictionary(x => x.UserID, x => x);
                    var dictCustomerInfos = lstCustomer.Result.ToDictionary(x => x.CustomerID, x => x);

                    foreach (var item in lstInvoice)
                    {
                        var objBankCard = dictBankCardInfos.GetValueOrDefault((long)item.BankCardID);
                        var objUser = dictUserInfos.GetValueOrDefault((long)item.UserIDCreate);
                        objCusomter = dictCustomerInfos.GetValueOrDefault((long)item.CustomerID);
                        result.Add(new HistoryCustomerTopupItem
                        {
                            TotalMoney = item.TotalMoney,
                            TransactionDate = item.TransactionDate,//thời gian giao dịch
                            CreateDate = item.CreateDate.Value, //thời gian nạp tiền
                            AliasName = objBankCard?.AliasName,
                            CustomerName = objCusomter.FullName,
                            Note = item.Note,
                            UserCreate = objUser.FullName,
                            StrStatus = ((Invoice_Status)item.Status).GetDescription()
                        });
                    }
                }
                response.SetSucces();
                response.Total = totalCount;
                response.Data = result;
            }
            catch (Exception ex)
            {
                _logger.LogError($"SearchHistoryTransactionQueryHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
