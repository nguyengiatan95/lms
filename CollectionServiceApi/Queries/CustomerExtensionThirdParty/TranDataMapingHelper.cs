﻿using LMS.Entites.Dtos.ProxyTima;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.CustomerExtensionThirdParty
{
    public interface ITranDataMapingHelper
    {
        string KeyCodeTranData { get; }
        Task<List<TranDataItem>> GetLstTranDataItem(string jsonDataProxy);
    }
    public class TranDataMappingBase
    {
        protected LMS.Common.Helper.Utils _common { get; set; }
        public TranDataMappingBase()
        {
            _common = new LMS.Common.Helper.Utils();
        }
    }
    public class TranDataK03Maping : TranDataMappingBase, ITranDataMapingHelper
    {
        public TranDataK03Maping() : base()
        {

        }
        public string KeyCodeTranData => RuleCollectionServiceHelper.TranDataKeyCodeK03;

        public async Task<List<TranDataItem>> GetLstTranDataItem(string jsonDataProxy)
        {
            return await Task.Run(() =>
            {
                List<TranDataItem> dataResult = new List<TranDataItem>();
                try
                {
                    var dataConvert = _common.ConvertJSonToObjectV2<K03ProxyTima>(jsonDataProxy);
                    var lstDataProxy = dataConvert.Result?.ResReport?.Result?.Locationlist;
                    if (lstDataProxy != null && lstDataProxy.Count > 0)
                    {
                        foreach (var item in lstDataProxy)
                        {
                            var dataItem = new TranDataItem
                            {
                                Address = item.Location?.Address,
                                KeyCode = KeyCodeTranData,
                                Lat = item.Location?.Lat ?? -1,
                                Long = item.Location?.Long ?? -1,
                                Percent = item.Location?.Percent ?? -1,
                            };
                            dataItem.NoteText = $"Địa chỉ: {dataItem.Address} </br>% ở đó: {dataItem.Percent} </br>Lat-Lng:{dataItem.Lat}-{dataItem.Long} </br>";
                            dataResult.Add(dataItem);
                        }
                    }
                }
                catch (Exception)
                {
                }
                return dataResult;
            });
        }
    }
    public class TranDataK06Maping : TranDataMappingBase, ITranDataMapingHelper
    {
        public TranDataK06Maping() : base()
        {

        }
        public string KeyCodeTranData => RuleCollectionServiceHelper.TranDataKeyCodeK06;

        public async Task<List<TranDataItem>> GetLstTranDataItem(string jsonDataProxy)
        {
            return await Task.Run(() =>
            {
                List<TranDataItem> dataResult = new List<TranDataItem>();
                try
                {
                    var dataConvert = _common.ConvertJSonToObjectV2<K06ProxyTima>(jsonDataProxy);
                    var lstDataProxy = dataConvert.Result;
                    if (lstDataProxy != null && lstDataProxy.Count > 0)
                    {
                        foreach (var item in lstDataProxy)
                        {
                            var dataItem = new TranDataItem
                            {
                                Msisdn = item.Msisdn,
                                KeyCode = KeyCodeTranData,
                                TotalCalls = item.TotalCalls,
                                TotalDurations = item.TotalDurations,
                            };
                            dataItem.NoteText = $"SĐT: {dataItem.Msisdn} </br>Số lần gọi: {dataItem.TotalCalls} </br>Thời gian gọi: {dataItem.TotalDurations} </br>";
                            dataResult.Add(dataItem);
                        }
                    }
                }
                catch (Exception)
                {
                }
                return dataResult;
            });
        }
    }
    public class TranDataK21Maping : TranDataMappingBase, ITranDataMapingHelper
    {
        public TranDataK21Maping() : base()
        {

        }
        public string KeyCodeTranData => RuleCollectionServiceHelper.TranDataKeyCodeK21;

        public async Task<List<TranDataItem>> GetLstTranDataItem(string jsonDataProxy)
        {
            return await Task.Run(() =>
            {
                List<TranDataItem> dataResult = new List<TranDataItem>();
                try
                {
                    var dataConvert = _common.ConvertJSonToObjectV2<K21ProxyTima>(jsonDataProxy);
                    var lstDataProxy = dataConvert.Result;
                    if (lstDataProxy != null && lstDataProxy.Count > 0)
                    {
                        foreach (var item in lstDataProxy)
                        {
                            var dataItem = new TranDataItem
                            {
                                SPhone = item.SPHONE,
                                KeyCode = KeyCodeTranData,
                                SName = item.S_NAME,
                                SDOB = item.SDOB
                            };
                            dataItem.NoteText = $"SĐT: {dataItem.SPhone} </br>Tên KH: {dataItem.SName} </br>DOB: {dataItem.SDOB} </br>";
                            dataResult.Add(dataItem);
                        }
                    }
                }
                catch (Exception)
                {
                }
                return dataResult;
            });
        }
    }
    public class TranDataK22Maping : TranDataMappingBase, ITranDataMapingHelper
    {
        public TranDataK22Maping() : base()
        {

        }
        public string KeyCodeTranData => RuleCollectionServiceHelper.TranDataKeyCodeK22;

        public async Task<List<TranDataItem>> GetLstTranDataItem(string jsonDataProxy)
        {
            return await Task.Run(() =>
            {
                List<TranDataItem> dataResult = new List<TranDataItem>();
                try
                {
                    var dataConvert = _common.ConvertJSonToObjectV2<K22ProxyTima>(jsonDataProxy);
                    var lstDataProxy = dataConvert.Result;
                    if (lstDataProxy != null && lstDataProxy.Count > 0)
                    {
                        foreach (var item in lstDataProxy)
                        {
                            var dataItem = new TranDataItem
                            {
                                SNID = item.SNID,
                                KeyCode = KeyCodeTranData,
                                SName = item.SNAME,
                                SDOB = item.SDOB,
                            };
                            dataItem.NoteText = $"CMT: {dataItem.SNID} </br>Tên KH: {dataItem.SName} </br>DOB: {dataItem.SDOB} </br>";
                            dataResult.Add(dataItem);
                        }
                    }
                }
                catch (Exception)
                {
                }
                return dataResult;
            });
        }
    }
}
