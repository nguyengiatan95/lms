﻿using CollectionServiceApi.Domain;
using CollectionServiceApi.Domain.Models.CustomerExtensionThirdParty;
using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.ProxyTima;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.CustomerExtensionThirdParty
{

    public class SearchCustomerExtensionThirdPartyQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string KeySearch { get; set; }
        public int Status { get; set; }
        public long LoanID { get; set; }
        public long TimaLoanID { get; set; }
        public string FromDate { get; set; }
        public string CodeTranData { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class SearchCustomerExtensionThirdPartyQueryHandler : IRequestHandler<SearchCustomerExtensionThirdPartyQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomerExtensionThirdParty> _customerExtensionThirdPartyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        ILogger<SearchCustomerExtensionThirdPartyQueryHandler> _logger;
        IEnumerable<ITranDataMapingHelper> _tranDataMappingHelper;
        LMS.Common.Helper.Utils _common;
        public SearchCustomerExtensionThirdPartyQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomerExtensionThirdParty> customerExtensionThirdPartyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            IEnumerable<ITranDataMapingHelper> tranDataMappingHelper,
            ILogger<SearchCustomerExtensionThirdPartyQueryHandler> logger)
        {
            _customerExtensionThirdPartyTab = customerExtensionThirdPartyTab;
            _userTab = userTab;
            _loanTab = loanTab;
            _customerTab = customerTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _tranDataMappingHelper = tranDataMappingHelper;
        }
        public async Task<ResponseActionResult> Handle(SearchCustomerExtensionThirdPartyQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstResult = new List<SearchCustomerExtensionThirdParty>();
                var lstCustomer = new List<Domain.Tables.TblCustomer>();
                var dictLoanInfor = new Dictionary<long, Domain.Tables.TblLoan>();
                var dictCustomerInfor = new Dictionary<long, Domain.Tables.TblCustomer>();
                var objLoan = new Domain.Tables.TblLoan();
                int totalCount = 0;
                bool searchLoan = false;


                if (!string.IsNullOrEmpty(request.CodeTranData))
                    _customerExtensionThirdPartyTab.WhereClause(x => x.CodeTranData == request.CodeTranData);
                if (request.Status != (int)StatusCommon.SearchAll)
                    _customerExtensionThirdPartyTab.WhereClause(x => x.Status == request.Status);

                if (!string.IsNullOrEmpty(request.FromDate) && !string.IsNullOrEmpty(request.ToDate))
                {
                    var fromDate = DateTime.ParseExact(request.FromDate, _common.DateTimeDDMMYYYY, CultureInfo.InvariantCulture);
                    var toDate = DateTime.ParseExact(request.ToDate, _common.DateTimeDDMMYYYY, CultureInfo.InvariantCulture);
                    toDate = toDate.AddDays(1);
                    fromDate = fromDate.AddSeconds(-1);
                    _customerExtensionThirdPartyTab.WhereClause(x => x.CreateDate > fromDate && x.CreateDate < toDate);
                }
                if (request.LoanID > 0)
                {
                    _loanTab.WhereClause(x => x.LoanID == request.LoanID);
                    searchLoan = true;
                }
                else if (request.TimaLoanID > 0)
                {
                    _loanTab.WhereClause(x => x.TimaLoanID == request.TimaLoanID);
                    searchLoan = true;
                }
                else if (!string.IsNullOrEmpty(request.KeySearch))
                {
                    searchLoan = true;
                    if (Regex.Match(request.KeySearch, TimaSettingConstant.PrefixContractCode, RegexOptions.IgnoreCase).Success)
                    {
                        _loanTab.WhereClause(x => x.ContactCode == request.KeySearch);
                    }
                    else
                    {
                        if (request.KeySearch.Length == TimaSettingConstant.MaxLengthPhone && Regex.Match(request.KeySearch, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success)
                            _customerTab.WhereClause(x => x.Phone == request.KeySearch);
                        else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternNumberCard, RegexOptions.IgnoreCase).Success)
                            _customerTab.WhereClause(x => x.NumberCard == request.KeySearch);
                        else
                            _customerTab.WhereClause(x => x.FullName == request.KeySearch);

                        dictCustomerInfor = (await _customerTab.QueryAsync()).ToDictionary(x => x.CustomerID, x => x);
                        if (dictCustomerInfor.Keys.Count < 1)
                        {
                            response.SetSucces();
                            response.Total = 0;
                            return response;
                        }
                    }
                }

                if (dictCustomerInfor.Keys.Count > 0)
                {
                    _loanTab.WhereClause(x => dictCustomerInfor.Keys.Contains((long)x.CustomerID));
                }
                if (searchLoan)
                {
                    dictLoanInfor = (await _loanTab.QueryAsync()).ToDictionary(x => x.LoanID, x => x);
                }
                if (dictLoanInfor.Keys.Count > 0)
                    _customerExtensionThirdPartyTab.WhereClause(x => dictLoanInfor.Keys.Contains(x.LoanID));

                var lstCustomerExtensionThirdParty = (await _customerExtensionThirdPartyTab.QueryAsync());
                totalCount = lstCustomerExtensionThirdParty.Count();
                if (totalCount > 0)
                {
                    lstCustomerExtensionThirdParty = lstCustomerExtensionThirdParty.OrderByDescending(x => x.CreateDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                    var lstLoanID = new List<long>();
                    var lstCreateBy = new List<long>();
                    foreach (var item in lstCustomerExtensionThirdParty)
                    {
                        lstLoanID.Add(item.LoanID);
                        lstCreateBy.Add(item.CreateBy);
                    }
                    lstCreateBy = lstCreateBy.Distinct().ToList();
                    lstLoanID = lstLoanID.Distinct().ToList();

                    var dictLoanTask = _loanTab.WhereClause(x => lstLoanID.Contains(x.LoanID)).QueryAsync();
                    var dictUserInfosTask = _userTab.WhereClause(x => lstCreateBy.Contains(x.UserID)).QueryAsync();

                    await Task.WhenAll(dictLoanTask, dictUserInfosTask);
                    var dictLoan = dictLoanTask.Result.ToDictionary(x => x.LoanID, x => x);
                    var dictUserInfor = dictUserInfosTask.Result.ToDictionary(x => x.UserID, x => x);
                    Dictionary<long, Task> dictCustomerExtensionThirdPartyMappingTask = new Dictionary<long, Task>();
                    foreach (var item in lstCustomerExtensionThirdParty)
                    {
                        var obUser = dictUserInfor.GetValueOrDefault((long)item.CreateBy);
                        objLoan = dictLoan.GetValueOrDefault((long)item.LoanID);
                        var mappingAction = _tranDataMappingHelper.FirstOrDefault(x => x.KeyCodeTranData == item.CodeTranData.ToUpper());
                        if (mappingAction == null)
                        {
                            _logger.LogError($"SearchCustomerExtensionThirdPartyQueryHandler_NotFoundMapping|CodeTranData={item.CodeTranData}|CustomerExtensionThirdPartyID={item.CustomerExtensionThirdPartyID}");
                        }
                        if (mappingAction != null && item.Status == (int)CustomerExtensionThirdParty_Status.Success && !string.IsNullOrEmpty(item.ThirdPartyData))
                        {
                            if (!dictCustomerExtensionThirdPartyMappingTask.ContainsKey(item.CustomerExtensionThirdPartyID))
                            {
                                dictCustomerExtensionThirdPartyMappingTask.Add(item.CustomerExtensionThirdPartyID, mappingAction.GetLstTranDataItem(item.ThirdPartyData));
                            }
                        }
                        lstResult.Add(new SearchCustomerExtensionThirdParty
                        {
                            CustomerExtensionThirdPartyID = item.CustomerExtensionThirdPartyID,
                            ContactCode = objLoan.ContactCode,
                            CustomerName = objLoan.CustomerName,
                            CodeTranData = item.CodeTranData,
                            StatusName = ((CustomerExtensionThirdParty_Status)item.Status).GetDescription(),
                            CodeTranDataDescription = RuleCollectionServiceHelper.DictCodeTranData.GetValueOrDefault(item.CodeTranData),
                            ThirdPartyData = item.ThirdPartyData,
                            CreateByName = obUser?.FullName,
                            CreateDate = item.CreateDate,
                            ModifyDate = item.ModifyDate
                        });
                    }
                    if (dictCustomerExtensionThirdPartyMappingTask.Count > 0)
                    {
                        await Task.WhenAll(dictCustomerExtensionThirdPartyMappingTask.Values);
                        foreach (var item in lstResult)
                        {
                            var lstDataTranItem = (Task<List<TranDataItem>>)dictCustomerExtensionThirdPartyMappingTask.GetValueOrDefault(item.CustomerExtensionThirdPartyID);
                            if (lstDataTranItem != null)
                            {
                                item.LstTranData = lstDataTranItem.Result;
                            }
                        }
                    }
                }
                response.SetSucces();
                response.Total = totalCount;
                response.Data = lstResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"SearchCustomerExtensionThirdPartyQueryHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
