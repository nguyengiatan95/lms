﻿using CollectionServiceApi.Domain;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Reason
{
    public class GetReasonQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserIDLogin { get; set; }
        public int Platform { get; set; }
    }
    public class GetReasonQueryHandler : IRequestHandler<GetReasonQuery, ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblReason> _reasonTab;
        readonly ILogger<GetReasonQueryHandler> _logger;
        readonly Services.IUserService _userService;
        public GetReasonQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblReason> reasonTab,
            Services.IUserService userService,
            ILogger<GetReasonQueryHandler> logger)
        {
            _reasonTab = reasonTab;
            _logger = logger;
            _userService = userService;
        }
        public async Task<ResponseActionResult> Handle(GetReasonQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                // 07-04-2022 lấy theo userlogin
                var userLogin = await _userService.GetPositonUserIDLogin(request.UserIDLogin);
                switch ((UserDepartment_PositionID)userLogin.PositionID)
                {
                    case UserDepartment_PositionID.Director:
                        switch (request.Platform)
                        {
                            case 1: // app
                            case 2: //
                                _reasonTab.WhereClause(x => x.DepartmentID == (int)Department_ID.THN_MB);
                                break;
                            default:
                                _reasonTab.WhereClause(x => RuleCollectionServiceHelper.LstDepartmentID.Contains(x.DepartmentID));
                                break;
                        }
                        break;
                    case UserDepartment_PositionID.Manager:
                    case UserDepartment_PositionID.TeamLead:
                    case UserDepartment_PositionID.Staff:
                        _reasonTab.WhereClause(x => x.DepartmentID == userLogin.DepartmentID);
                        break;
                }
                
                var lstData = (await _reasonTab.WhereClause(x => x.Status == (int)Reason_Status.Active && x.ReasonCode != null).QueryAsync()).OrderBy(x => x.Postion).ToList();
                
                response.Data = lstData;
                response.SetSucces();
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetReasonQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
