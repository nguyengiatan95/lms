﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.SettingMoney
{
    public class GetSettingMoneyByApprovalLevelBookDebtIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long BookDebtID { get; set; }
        public long ApprovalLevelBookDebtID { get; set; }
    }
    public class GetSettingMoneyByApprovalLevelBookDebtIDQueryHandler : IRequestHandler<GetSettingMoneyByApprovalLevelBookDebtIDQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingTotalMoney> _settingTotalMoneyTab;
        ILogger<GetSettingMoneyByApprovalLevelBookDebtIDQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetSettingMoneyByApprovalLevelBookDebtIDQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingTotalMoney> settingTotalMoneyTab,
            ILogger<GetSettingMoneyByApprovalLevelBookDebtIDQueryHandler> logger)
        {
            _settingTotalMoneyTab = settingTotalMoneyTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetSettingMoneyByApprovalLevelBookDebtIDQuery request, CancellationToken cancellationToken)
        {

            ResponseActionResult response = new ResponseActionResult();
            try
            {
                Domain.Models.MGL.SettingMoneyDetails data = new Domain.Models.MGL.SettingMoneyDetails();
                var settings = await _settingTotalMoneyTab.WhereClause(x => x.ApprovalLevelBookDebtID == request.ApprovalLevelBookDebtID).QueryAsync();
                if (settings != null && settings.Any())
                {
                    var firstData = settings.FirstOrDefault();
                    data.TotalMoney = firstData.TotalMoney;
                    data.ListTypeMoney = firstData.ListTypeMoney;
                }
                response.SetSucces();
                response.Data = data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetSettingMoneyByApprovalLevelBookDebtIDQueryHandler|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
