﻿using CollectionServiceApi.Domain;
using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.ProxyTima;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.SendSms
{
    public class GetLstSendSmsQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int Status { get; set; }
        public string PhoneSend { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class GetLstSendSmsQueryHandler : IRequestHandler<GetLstSendSmsQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSendSms> _sendSmsTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<GetLstSendSmsQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetLstSendSmsQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSendSms> sendSmsTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            ILogger<GetLstSendSmsQueryHandler> logger)
        {
            _sendSmsTab = sendSmsTab;
            _userTab = userTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetLstSendSmsQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                int totalCount = 0;
                var lstResult = new List<Domain.Models.SendSms.SendSmsItem>();
                if (request.Status != (int)StatusCommon.SearchAll)
                    _sendSmsTab.WhereClause(x => x.Status == request.Status);
                if (!string.IsNullOrEmpty(request.PhoneSend) && request.PhoneSend.Length == TimaSettingConstant.MaxLengthPhone && Regex.Match(request.PhoneSend, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success)
                    _sendSmsTab.WhereClause(x => x.PhoneSend == request.PhoneSend);

                if (!string.IsNullOrEmpty(request.FromDate) && !string.IsNullOrEmpty(request.ToDate))
                {
                    var fromDate = DateTime.ParseExact(request.FromDate, _common.DateTimeDDMMYYYY, CultureInfo.InvariantCulture);
                    var toDate = DateTime.ParseExact(request.ToDate, _common.DateTimeDDMMYYYY, CultureInfo.InvariantCulture);
                    toDate = toDate.AddDays(1);
                    fromDate = fromDate.AddSeconds(-1);
                    _sendSmsTab.WhereClause(x => x.CreateDate > fromDate && x.CreateDate < toDate);
                }
                var lstSendSms = (await _sendSmsTab.QueryAsync());
                totalCount = lstSendSms.Count();
                if (totalCount > 0)
                {
                    lstSendSms = lstSendSms.OrderByDescending(x => x.CreateDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                    var lstCreateBy = new List<long>();
                    foreach (var item in lstSendSms)
                    {
                        lstCreateBy.Add(item.CreateBy);
                    }
                    lstCreateBy = lstCreateBy.Distinct().ToList();
                    var dictUserInfos = (await _userTab.WhereClause(x => lstCreateBy.Contains(x.UserID)).QueryAsync()).ToDictionary(x => x.UserID, x => x);
                    foreach (var item in lstSendSms)
                    {
                        var objCreateBy = dictUserInfos.GetValueOrDefault((long)item.CreateBy);
                        lstResult.Add(new Domain.Models.SendSms.SendSmsItem
                        {
                            SendSmsID = item.SendSmsID,
                            CreateByName = objCreateBy?.FullName,
                            CreateDate = item.CreateDate,
                            TemplateName = item.TemplateName,
                            ContentSms = item.ContentSms,
                            StrStatus = ((SendSms_Status)item.Status).GetDescription(),
                            PhoneReceived = item.PhoneReceived,
                            PhoneSend = item.PhoneSend,
                            ModifyDate = item.ModifyDate
                        });
                    }
                }
                response.SetSucces();
                response.Total = totalCount;
                response.Data = lstResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLstSendSmsQueryHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
