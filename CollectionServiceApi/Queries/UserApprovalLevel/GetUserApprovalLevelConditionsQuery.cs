﻿using CollectionServiceApi.Queries.User;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.UserApprovalLevel
{
    public class GetUserApprovalLevelConditionsQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long ApprovalLevelBookDebtID { get; set; }
        public string UserName { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class GetUserApprovalLevelConditionsQueryHandler : IRequestHandler<GetUserApprovalLevelConditionsQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserApprovalLevel> _userApprovalLevelTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> _approvalLevelBookDebt;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<GetUserApprovalLevelConditionsQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetUserApprovalLevelConditionsQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserApprovalLevel> userApprovalLevelTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> approvalLevelBookDebt,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
        ILogger<GetUserApprovalLevelConditionsQueryHandler> logger)
        {
            _userApprovalLevelTab = userApprovalLevelTab;
            _approvalLevelBookDebt = approvalLevelBookDebt;
            _userTab = userTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetUserApprovalLevelConditionsQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            List<Domain.Models.UserApprovalLevel.UserApprovalLevelListViews> userApprovals = new List<Domain.Models.UserApprovalLevel.UserApprovalLevelListViews>();
            try
            {
                Dictionary<long, Domain.Tables.TblUser> dicUsers = new Dictionary<long, Domain.Tables.TblUser>();
                if (request.ApprovalLevelBookDebtID != (int)StatusCommon.SearchAll)
                {
                    _userApprovalLevelTab.WhereClause(x => x.ApprovalLevelBookDebtID == request.ApprovalLevelBookDebtID);
                }
                request.UserName = request.UserName.Trim();
                if (!string.IsNullOrWhiteSpace(request.UserName))
                {
                    var users = await _userTab.WhereClause(x => x.UserName == request.UserName).QueryAsync();
                    if (users == null || !users.Any())
                    {
                        response.SetSucces();
                        response.Data = userApprovals;
                        return response;
                    }
                    var userIds = users.Select(x => x.UserID).ToList();
                    var user = users.FirstOrDefault();
                    dicUsers.Add(user.UserID, user);
                    _userApprovalLevelTab.WhereClause(x => userIds.Contains(x.UserID));
                }
                // lây danh sách
                var userApprovalData = await _userApprovalLevelTab.QueryAsync();
                // gán giá trị
                if (userApprovalData != null && userApprovalData.Any())
                {
                    response.Total = userApprovalData.Count();
                    userApprovalData = userApprovalData.Skip((int)((request.PageIndex - 1) * request.PageSize)).Take(request.PageSize).ToList();
                    var approvalLevelBookDebtIDs = userApprovalData.Select(x => x.ApprovalLevelBookDebtID).Union(userApprovalData.Select(x => x.MoveToApprovalLevelBookDebtID)).Distinct().ToList();
                    var approvals = await _approvalLevelBookDebt.WhereClause(x => approvalLevelBookDebtIDs.Contains(x.ApprovalLevelBookDebtID)).QueryAsync();
                    var dicApproval = approvals.ToDictionary(x => x.ApprovalLevelBookDebtID, x => x);

                    if (string.IsNullOrWhiteSpace(request.UserName))
                    {
                        var userIds = userApprovalData.Select(x => x.UserID).ToList();
                        var users = await _userTab.WhereClause(x => userIds.Contains(x.UserID)).QueryAsync();
                        dicUsers = users.ToDictionary(x => x.UserID, x => x);
                    }
                    foreach (var item in userApprovalData)
                    {
                        var approval = !dicApproval.ContainsKey(item.ApprovalLevelBookDebtID ?? 0) ? null : dicApproval.GetValueOrDefault(item.ApprovalLevelBookDebtID ?? 0);
                        var moveToApprovalLevelBookDebtID = item.MoveToApprovalLevelBookDebtID > 0 ? (long)item.MoveToApprovalLevelBookDebtID : (approval == null ? 0 : (long)approval.ParentID);
                        userApprovals.Add(new Domain.Models.UserApprovalLevel.UserApprovalLevelListViews()
                        {
                            ApprovalLevelBookDebtID = item.ApprovalLevelBookDebtID ?? 0,
                            ApprovalLevelBookName = approval == null ? "" : approval.ApprovalLevelBookDebtName,
                            CreateDate = item.CreateDate ?? DateTime.Now,
                            MoveApprovalLevelBookDebtID = moveToApprovalLevelBookDebtID,
                            UserName = !dicUsers.ContainsKey(item.UserID) ? "" : dicUsers.GetValueOrDefault(item.UserID).UserName,
                            MoveApprovalLevelBookName = !dicApproval.ContainsKey(moveToApprovalLevelBookDebtID) ? "" : dicApproval.GetValueOrDefault(moveToApprovalLevelBookDebtID).ApprovalLevelBookDebtName,
                            Status = item.Status,
                            UserApprovalLevelId = item.UserApprovalLevelID
                        });
                    }
                }
                response.SetSucces();
                response.Data = userApprovals;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetUserApprovalLevelConditionsQueryHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
