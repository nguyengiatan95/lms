﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.ExcelReport
{
    public class GetFormLoanExemptionInterestByTraceIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        /// <summary>
        /// improve sau khi gộp chung lại file excel, doc
        /// </summary>
        public int TypeReport { get; set; }
        public string TraceIDRequest { get; set; }
        public int CreateBy { get; set; }
        public long UserIDLogin { get; set; }
    }
    public class GetFormLoanExemptionInterestByTraceIDQueryHandler : IRequestHandler<GetFormLoanExemptionInterestByTraceIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> _excelReportTab;
        ILogger<GetFormLoanExemptionInterestByTraceIDQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetFormLoanExemptionInterestByTraceIDQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> excelReportTab,
            ILogger<GetFormLoanExemptionInterestByTraceIDQueryHandler> logger)
        {
            _excelReportTab = excelReportTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetFormLoanExemptionInterestByTraceIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                if (request.UserIDLogin != request.CreateBy && request.UserIDLogin != TimaSettingConstant.UserIDAdmin)
                {
                    response.SetSucces();
                    response.Message = MessageConstant.ExcelReport_TraceIDNotExist;
                    return response;
                }

                var excelReports = await _excelReportTab.WhereClause(x => x.CreateBy == request.CreateBy && x.TraceIDRequest == request.TraceIDRequest).QueryAsync();
                if (excelReports == null && !excelReports.Any())
                {
                    response.Message = MessageConstant.ExcelReport_TraceIDNotExist;
                    return response;
                }
                var excelData = excelReports.First();
                if (excelData.Status == (int)ExcelReport_Status.Waiting)
                {
                    //++0->thông báo user chờ xử lý xong
                    response.Message = MessageConstant.ExcelReport_WaitingProcess;
                    return response;
                }
                response.SetSucces();
                if (excelData.Status != (int)ExcelReport_Status.Success)
                {
                    response.Message = MessageConstant.ExcelReport_Error;
                    return response;
                }
                try
                {
                    var lstData = _common.ConvertJSonToObjectV2<LMS.Entites.Dtos.CollectionServices.ReportExcel.ReportFormLoanExemptionInterestDetail>(excelData.ExtraData);
                    response.Data = lstData;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"GetFormLoanExemptionInterestByTraceIDQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ExtraData={excelData.ExtraData}|ex={ex.Message}");
                    response.Message = MessageConstant.ExcelReport_Error;
                    return response;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetFormLoanExemptionInterestByTraceIDQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
