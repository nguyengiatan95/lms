﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.ExcelReport
{
    public class GetExcelReportByTraceIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string TraceIDRequest { get; set; }
        public int CreateBy { get; set; }
        public long UserIDLogin { get; set; }
    }
    public class GetExcelReportByTraceIDQueryHandler : IRequestHandler<GetExcelReportByTraceIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> _excelReportTab;
        ILogger<GetExcelReportByTraceIDQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        IEnumerable<IExcelExportDataHelper> _excelExportHelper;
        public GetExcelReportByTraceIDQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> excelReportTab,
            IEnumerable<IExcelExportDataHelper> excelExportHelper,
            ILogger<GetExcelReportByTraceIDQueryHandler> logger
            )
        {
            _excelReportTab = excelReportTab;
            _excelExportHelper = excelExportHelper;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetExcelReportByTraceIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                if (request.UserIDLogin != request.CreateBy && request.UserIDLogin != TimaSettingConstant.UserIDAdmin)
                {
                    response.SetSucces();
                    response.Message = MessageConstant.ExcelReport_TraceIDNotExist;
                    return response;
                }

                var excelReports = await _excelReportTab.WhereClause(x => x.CreateBy == request.CreateBy && x.TraceIDRequest == request.TraceIDRequest).QueryAsync();
                if (excelReports == null || !excelReports.Any())
                {
                    response.Message = MessageConstant.ExcelReport_TraceIDNotExist;
                    return response;
                }
                var excelData = excelReports.First();
                if (excelData.Status == (int)ExcelReport_Status.Waiting)
                {
                    //++0->thông báo user chờ xử lý xong
                    response.Message = MessageConstant.ExcelReport_WaitingProcess;
                    return response;
                }
                response.SetSucces();
                if (excelData.Status != (int)ExcelReport_Status.Success)
                {
                    response.Message = MessageConstant.ExcelReport_Error;
                    return response;
                }
                var excelExportDetail = _excelExportHelper.FirstOrDefault(x => x.TypeReport == excelData.TypeReport);
                if (excelExportDetail != null)
                {
                    response = excelExportDetail.GetLstDataExcelFromJson(excelData.ExtraData);
                }
                else
                {
                    response.Message = MessageConstant.ExcelReport_Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetExcelReportByTraceIDQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
