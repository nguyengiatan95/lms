﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.CollectionServices.ReportExcel;
using LMS.Entites.Dtos.Document;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.ExcelReport
{
    public interface IExcelExportDataHelper
    {
        int TypeReport { get; }
        ResponseActionResult GetLstDataExcelFromJson(string extraData);
    }
    public class ExcelExportBase
    {
        protected LMS.Common.Helper.Utils _common { get; set; }
        public ExcelExportBase()
        {
            _common = new LMS.Common.Helper.Utils();
        }
    }

    public class ExcelExportAssignLoanEmployeeDetail : ExcelExportBase, IExcelExportDataHelper
    {
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.Loan;

        public ResponseActionResult GetLstDataExcelFromJson(string extraData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.Data = _common.ConvertJSonToObjectV2<List<LMS.Entites.Dtos.CollectionServices.ReportExcel.AssignLoanEmployeeDetail>>(extraData);
                response.SetSucces();
            }
            catch
            {
            }
            return response;
        }
    }

    public class ExcelExportHistoryCustomerTopup : ExcelExportBase, IExcelExportDataHelper
    {
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.HistoryCustomerTopup;

        public ResponseActionResult GetLstDataExcelFromJson(string extraData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.Data = _common.ConvertJSonToObjectV2<List<Domain.Models.Invoice.HistoryCustomerTopupItem>>(extraData);
                response.SetSucces();
            }
            catch
            {
            }
            return response;
        }
    }

    public class ExcelExportHistoryTransactionLoanByCustomer : ExcelExportBase, IExcelExportDataHelper
    {
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.HistoryTransactionLoanByCustomer;

        public ResponseActionResult GetLstDataExcelFromJson(string extraData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.Data = _common.ConvertJSonToObjectV2<Domain.Models.Excel.HistoryTransactionLoanCustomerExcel>(extraData);
                response.SetSucces();
            }
            catch
            {
            }
            return response;
        }
    }

    public class ExcelExportReportLoanDebtType : ExcelExportBase, IExcelExportDataHelper
    {
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.ReportLoanDebtType;

        public ResponseActionResult GetLstDataExcelFromJson(string extraData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.Data = _common.ConvertJSonToObjectV2<List<Domain.Models.Excel.HistoryTrackingLoanInMonth>>(extraData);
                response.SetSucces();
            }
            catch
            {

            }
            return response;
        }
    }

    public class ExcelExportReportLoanDebtDaily : ExcelExportBase, IExcelExportDataHelper
    {
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.ReportLoanDebtDaily;

        public ResponseActionResult GetLstDataExcelFromJson(string extraData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.Data = _common.ConvertJSonToObjectV2<List<Domain.Models.Excel.ReportLoanDebtDailyModel>>(extraData);
                response.SetSucces();
            }
            catch
            {

            }
            return response;
        }
    }

    public class ExcelExportReportDayPlan : ExcelExportBase, IExcelExportDataHelper
    {
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.ReportDayPlan;

        public ResponseActionResult GetLstDataExcelFromJson(string extraData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.Data = _common.ConvertJSonToObjectV2<List<Domain.Models.Report.ListLoanDayPlan>>(extraData);
                response.SetSucces();
            }
            catch
            {

            }
            return response;
        }
    }

    #region file doc
    public class PropertySeizure : ExcelExportBase, IExcelExportDataHelper
    {
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.PropertySeizure;

        public ResponseActionResult GetLstDataExcelFromJson(string extraData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.Data = _common.ConvertJSonToObjectV2<PropertySeizureItem>(extraData);
                response.SetSucces();
            }
            catch
            {
            }
            return response;
        }
    }
    public class DebtRestructuring : ExcelExportBase, IExcelExportDataHelper
    {
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.DebtRestructuring;

        public ResponseActionResult GetLstDataExcelFromJson(string extraData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.Data = _common.ConvertJSonToObjectV2<DebtRestructuringItem>(extraData);
                response.SetSucces();
            }
            catch
            {
            }
            return response;
        }
    }
    public class UnsecuredFinalDebtNotice : ExcelExportBase, IExcelExportDataHelper
    {
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.UnsecuredFinalDebtNotice;

        public ResponseActionResult GetLstDataExcelFromJson(string extraData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.Data = _common.ConvertJSonToObjectV2<UnsecuredFinalDebtNoticeItem>(extraData);
                response.SetSucces();
            }
            catch
            {
            }
            return response;
        }
    }
    public class NotifyPropertySeizure : ExcelExportBase, IExcelExportDataHelper
    {
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.NotifyPropertySeizure;

        public ResponseActionResult GetLstDataExcelFromJson(string extraData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.Data = _common.ConvertJSonToObjectV2<NotifyPropertySeizureItem>(extraData);
                response.SetSucces();
            }
            catch
            {
            }
            return response;
        }
    }
    public class ExcelExcelHistoryInteraction : ExcelExportBase, IExcelExportDataHelper
    {
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.ExcelHistoryInteraction;

        public ResponseActionResult GetLstDataExcelFromJson(string extraData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.Data = _common.ConvertJSonToObjectV2<List<Domain.Models.CommentDebtPrompted.HistoryInteractDebtPrompted>>(extraData);
                response.SetSucces();
            }
            catch
            {

            }
            return response;
        }
    }
    public class NotifyVoluntarilyHandingOverPropertye : ExcelExportBase, IExcelExportDataHelper
    {
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.NotifyVoluntarilyHandingOverPropertye;

        public ResponseActionResult GetLstDataExcelFromJson(string extraData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.Data = _common.ConvertJSonToObjectV2<NotifyVoluntarilyHandingOverPropertyeItem>(extraData);
                response.SetSucces();
            }
            catch
            {
            }
            return response;
        }
    }
    public class MGL : ExcelExportBase, IExcelExportDataHelper
    {
        public int TypeReport => (int)LMS.Common.Constants.ExcelReport_TypeReport.ExemptionInterest;

        public ResponseActionResult GetLstDataExcelFromJson(string extraData)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.Data = _common.ConvertJSonToObjectV2<ReportFormLoanExemptionInterestDetail>(extraData);
                response.SetSucces();
            }
            catch
            {
            }
            return response;
        }
    }
    #endregion
}
