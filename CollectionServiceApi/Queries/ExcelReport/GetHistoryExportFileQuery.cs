﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.CollectionServices.ReportExcel;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.ExcelReport
{
    public class GetHistoryExportFileQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int TypeReport { get; set; }
        public long CreateBy { get; set; }
        public int Status { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        /// <summary>
        /// 1: excel, 2: biểu mẫu
        /// </summary>
        public int TypeFile { get; set; }
        public long UserID { get; set; }

        public GetHistoryExportFileQuery()
        {
            CreateBy = (int)LMS.Common.Constants.StatusCommon.SearchAll;
            TypeReport = (int)LMS.Common.Constants.StatusCommon.SearchAll;
            Status = (int)LMS.Common.Constants.StatusCommon.SearchAll;
            PageIndex = 1;
            PageSize = 20;
            TypeFile = RuleCollectionServiceHelper.TypeFileExcel;
        }
    }
    public class GetHistoryExportFileQueryHandler : IRequestHandler<GetHistoryExportFileQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> _excelReportTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemption> _loanExemptionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserApprovalLevel> _userApprovalLevelTab;
        ILogger<GetHistoryExportFileQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetHistoryExportFileQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> excelReportTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemption> loanExemptionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserApprovalLevel> userApprovalLevelTab,
            ILogger<GetHistoryExportFileQueryHandler> logger
            )
        {
            _excelReportTab = excelReportTab;
            _userTab = userTab;
            _loanExemptionTab = loanExemptionTab;
            _userApprovalLevelTab = userApprovalLevelTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(GetHistoryExportFileQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            List<HistoryExportFileDetail> lstDataHistory = new List<HistoryExportFileDetail>();
            ResponseForDataTable dataTable = new ResponseForDataTable
            {
                RecordsTotal = 0,
                Data = lstDataHistory,
                RecordsFiltered = 0,
                Draw = 1
            };
            try
            {
                if (!string.IsNullOrEmpty(request.FromDate))
                {
                    var fromDate = DateTime.ParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, null).AddSeconds(1);
                    _excelReportTab.WhereClause(x => x.CreateDate > fromDate);
                }
                if (!string.IsNullOrEmpty(request.ToDate))
                {
                    var todate = DateTime.ParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, null).AddDays(1).Date;
                    _excelReportTab.WhereClause(x => x.CreateDate < todate);
                }
                if (request.TypeReport != (int)LMS.Common.Constants.StatusCommon.SearchAll)
                {
                    _excelReportTab.WhereClause(x => x.TypeReport == request.TypeReport);
                }
                else
                {
                    //switch (request.TypeFile)
                    //{
                    //    case RuleCollectionServiceHelper.TypeFileExcel:
                    //        _excelReportTab.WhereClause(x => x.TypeReport <= RuleCollectionServiceHelper.TypeFileStep);
                    //        break;
                    //    case RuleCollectionServiceHelper.TypeFileForm:
                    //        _excelReportTab.WhereClause(x => x.TypeReport > RuleCollectionServiceHelper.TypeFileStep);
                    //        break;
                    //}
                }
                if (request.CreateBy != (int)LMS.Common.Constants.StatusCommon.SearchAll)
                {
                    _excelReportTab.WhereClause(x => x.CreateBy == request.CreateBy);
                }
                if (request.Status != (int)LMS.Common.Constants.StatusCommon.SearchAll)
                {
                    _excelReportTab.WhereClause(x => x.Status == request.Status);
                }
                var lstData = await _excelReportTab.SelectColumns(x => x.CreateBy, x => x.ExcelReportID, x => x.Status,
                                                                  x => x.TypeReport, x => x.CreateDate, x => x.ModifyDate, x => x.TraceIDRequest, x => x.ReportName).
                                                                  WhereClause(x => x.TypeReport < (int)ExcelReport_TypeReport.Lender_Deposit)
                                                                 .QueryAsync();
                dataTable.RecordsTotal = lstData.Count();
                lstData = lstData.OrderByDescending(x => x.ExcelReportID).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                List<long> lstCreateBy = lstData.Select(x => (long)x.CreateBy).Distinct().ToList();
                var dictUseInfos = (await _userTab.SelectColumns(x => x.UserID, x => x.UserName).WhereClause(x => lstCreateBy.Contains(x.UserID)).QueryAsync()).ToDictionary(x => x.UserID, x => x);

                var lstTraceIDRequest = new List<string>();
                foreach (var item in lstData)
                {
                    lstTraceIDRequest.Add(item.TraceIDRequest);
                }


                //check request mgl đã dc tạo
                var dictLoanExemption = (await _loanExemptionTab.SelectColumns(x => x.ReferTraceIDRequest)
                                           .WhereClause(x => lstTraceIDRequest.Contains(x.ReferTraceIDRequest)).QueryAsync())
                                           .ToDictionary(x => x.ReferTraceIDRequest, x => x);

                var objUserApprovalLevel = new Domain.Tables.TblUserApprovalLevel();
                if (request.UserID != LMS.Common.Constants.TimaSettingConstant.UserIDAdmin)
                    objUserApprovalLevel = (await _userApprovalLevelTab.WhereClause(x => x.UserID == request.UserID && x.ApprovalLevelBookDebtID == 0).QueryAsync()).FirstOrDefault();

                bool showButtonCreateRequest = false;
                foreach (var item in lstData)
                {
                    string reportName = ((ExcelReport_TypeReport)item.TypeReport).GetDescription();
                    if (!string.IsNullOrEmpty(item.ReportName))
                        reportName = $"{reportName}-{item.ReportName}";

                    // chưa tạo req + có quyền 
                    if (!dictLoanExemption.ContainsKey(item.TraceIDRequest) && (request.UserID == LMS.Common.Constants.TimaSettingConstant.UserIDAdmin || (objUserApprovalLevel != null && item.CreateBy == objUserApprovalLevel.UserID)))
                        showButtonCreateRequest = true;
                    else
                        showButtonCreateRequest = false;

                    lstDataHistory.Add(new HistoryExportFileDetail
                    {
                        CreateBy = item.CreateBy.Value,
                        CreateByName = dictUseInfos.GetValueOrDefault(item.CreateBy.Value)?.UserName,
                        CreateDate = item.CreateDate.Value,
                        ModifyDate = item.ModifyDate ?? item.CreateDate.Value,
                        ReportName = item.ReportName,
                        StatusName = ((ExcelReport_Status)item.Status).GetDescription(),
                        TraceIDRequest = item.TraceIDRequest,
                        TypeReport = (int)item.TypeReport,
                        ShowButtonCreateRequest = showButtonCreateRequest
                    });
                }
                response.Data = dataTable;
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetHistoryExportFileQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
