﻿using CollectionServiceApi.Domain.Models.ApprovalLevelBookDebt;
using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace CollectionServiceApi.Queries.ApprovalLevelBookDebt
{
    public class GetListApprovalLevelBookDebtQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string KeySearch { get; set; }
        public int Status { get; set; }
    }
    public class GetListApprovalLevelBookDebtQueryHandler : IRequestHandler<GetListApprovalLevelBookDebtQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> _approvalLevelBookDebtTab;
        ILogger<GetListApprovalLevelBookDebtQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetListApprovalLevelBookDebtQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> approvalLevelBookDebtTab,
            ILogger<GetListApprovalLevelBookDebtQueryHandler> logger)
        {
            _approvalLevelBookDebtTab = approvalLevelBookDebtTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetListApprovalLevelBookDebtQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var lstDataResult = new List<GetListApprovalLevelBookDebt>();
            try
            {
                if (!string.IsNullOrEmpty(request.KeySearch))
                    _approvalLevelBookDebtTab.WhereClause(x => x.ApprovalLevelBookDebtName.Contains(request.KeySearch));
                if (request.Status != (int)StatusCommon.SearchAll)
                    _approvalLevelBookDebtTab.WhereClause(x => x.Status == request.Status);

                var lstApprovalLevelBookDebt = (await _approvalLevelBookDebtTab.QueryAsync()).OrderByDescending(x => x.CreateDate);
                if (lstApprovalLevelBookDebt.Any())
                {
                    foreach (var item in lstApprovalLevelBookDebt)
                    {
                        lstDataResult.Add(new GetListApprovalLevelBookDebt()
                        {
                            ApprovalLevelBookDebtID = item.ApprovalLevelBookDebtID,
                            ApprovalLevelBookDebtName = item.ApprovalLevelBookDebtName,
                            CreateDate = item.CreateDate,
                            IsAllAccept = item.IsAllAccept,
                            Status = item.Status,
                            StrStatus = ((ApprovalLevelBookDebt_Status)item.Status).GetDescription()
                        });
                    }
                }
                response.Data = lstDataResult;
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetListApprovalLevelBookDebtQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
