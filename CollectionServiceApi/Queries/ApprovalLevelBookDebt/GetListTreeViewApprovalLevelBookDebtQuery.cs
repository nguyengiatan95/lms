﻿using CollectionServiceApi.Domain.Models.ApprovalLevelBookDebt;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.ApprovalLevelBookDebt
{
    public class GetListTreeViewApprovalLevelBookDebtQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class GetListTreeViewApprovalLevelBookDebtQueryHandler : IRequestHandler<GetListTreeViewApprovalLevelBookDebtQuery, ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> _approvalLevelBookDebtTab;
        readonly ILogger<GetListTreeViewApprovalLevelBookDebtQueryHandler> _logger;
        //readonly LMS.Common.Helper.Utils _common;
        public GetListTreeViewApprovalLevelBookDebtQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> approvalLevelBookDebtTab,
            ILogger<GetListTreeViewApprovalLevelBookDebtQueryHandler> logger)
        {
            _approvalLevelBookDebtTab = approvalLevelBookDebtTab;
            _logger = logger;
            //_common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetListTreeViewApprovalLevelBookDebtQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var listTreeViewApprovalLevelBookDebt = new List<ListTreeViewApprovalLevelBookDebt>();
                var lstApprovalLevelBookDebt = await _approvalLevelBookDebtTab.WhereClause(x => x.Status == (int)ApprovalLevelBookDebt_Status.Active).QueryAsync();
                if (lstApprovalLevelBookDebt != null)
                {
                    listTreeViewApprovalLevelBookDebt = await ResultDataTree(lstApprovalLevelBookDebt.ToList());
                }
                response.Data = listTreeViewApprovalLevelBookDebt.OrderByDescending(x => x.PostionID).ToList();
                response.SetSucces();
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetListTreeViewApprovalLevelBookDebtQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task<List<ListTreeViewApprovalLevelBookDebt>> ResultDataTree(List<Domain.Tables.TblApprovalLevelBookDebt> lstData)
        {
            try
            {
                var result = new List<ListTreeViewApprovalLevelBookDebt>();
                var lstUserID = new List<long>();
                var dicParentApprovalLevelBookDebt = new Dictionary<long, List<Domain.Tables.TblApprovalLevelBookDebt>>();
                // lấy list parentID
                var lstParentID = new List<long>();
                var lstChildren = new List<long>();
                foreach (var item in lstData)
                {
                    lstParentID.Add(item.ApprovalLevelBookDebtID);
                    lstChildren.Add(item.ParentID);
                }
                var lstChildrenDontHave = lstChildren.Where(x => !lstParentID.Contains(x)).ToList(); // các Children mà ParentID bị xóa =>> show ra cấu hình lại 
                foreach (var item in lstData)
                {
                    if (lstChildrenDontHave.Contains(item.ParentID))
                        item.ParentID = 0;

                    if (item.ParentID > 0)// dict  có parentID
                    {
                        if (!dicParentApprovalLevelBookDebt.ContainsKey(item.ParentID))
                        {
                            dicParentApprovalLevelBookDebt[item.ParentID] = new List<Domain.Tables.TblApprovalLevelBookDebt>();
                        }
                        dicParentApprovalLevelBookDebt[item.ParentID].Add(item);
                    }
                }
                var lstApprovalLevelBookDebtParent = lstData.Where(x => x.ParentID == 0).ToList();// lstApprovalLevelBookDebtParent cha

                return GetTree(lstApprovalLevelBookDebtParent, dicParentApprovalLevelBookDebt);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ResultDataTree|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
            finally
            {
                await Task.Delay(1);
            }
        }

        private List<ListTreeViewApprovalLevelBookDebt> GetTree(List<Domain.Tables.TblApprovalLevelBookDebt> lstApprovalLevelBookDebtParent,
        Dictionary<long, List<Domain.Tables.TblApprovalLevelBookDebt>> dicParentId)
        {
            List<ListTreeViewApprovalLevelBookDebt> dataResult = new List<ListTreeViewApprovalLevelBookDebt>();
            foreach (var item in lstApprovalLevelBookDebtParent)
            {
                var objDataItem = new ListTreeViewApprovalLevelBookDebt
                {
                    ID = item.ApprovalLevelBookDebtID,
                    Text = item.ApprovalLevelBookDebtName,
                    Open = true,
                    PostionID = item.Postion,
                    Children = new List<ListTreeViewApprovalLevelBookDebt>()
                };
                var childs = dicParentId.GetValueOrDefault(item.ApprovalLevelBookDebtID);
                if (childs != null && childs.Count > 0)//kiểm tra childrent có childrent ko
                {
                    objDataItem.Children = GetTree(childs, dicParentId);
                }
                dataResult.Add(objDataItem);
            }
            return dataResult;
        }
    }
}
