﻿using CollectionServiceApi.Domain.Models.ApprovalLevelBookDebt;
using CollectionServiceApi.Domain.Models.AssignmentLoan;
using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
namespace CollectionServiceApi.Queries.AssignmentLoan
{
    public class GetLstDetailsLoanSplitSingleQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class GetLstDetailsLoanSplitSingleQueryHandler : IRequestHandler<GetLstDetailsLoanSplitSingleQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> _assignmentLoanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<GetLstDetailsLoanSplitSingleQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetLstDetailsLoanSplitSingleQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> assignmentLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            ILogger<GetLstDetailsLoanSplitSingleQueryHandler> logger)
        {
            _assignmentLoanTab = assignmentLoanTab;
            _loanTab = loanTab;
            _userTab = userTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetLstDetailsLoanSplitSingleQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var lstDataResult = new List<LstLoanSplitSingle>();
            try
            {
                //lấy trong tháng
                DateTime date = DateTime.Now;
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                var lstAssignmentLoan = (await _assignmentLoanTab.WhereClause(x => x.LoanID == request.LoanID && x.Status == (int)AssignmentLoan_Status.Processing &&
                                        x.CreateDate >= firstDayOfMonth && x.CreateDate <= lastDayOfMonth).QueryAsync());

                int totalCount = 0;
                if (lstAssignmentLoan.Any())
                {
                    totalCount = lstAssignmentLoan.Count();
                    lstAssignmentLoan = lstAssignmentLoan.OrderByDescending(x => x.CreateDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                    var lstLoanID = new List<long>();
                    var lstUserID = new List<long>();
                    foreach (var item in lstAssignmentLoan)
                    {
                        lstLoanID.Add(item.LoanID);
                        lstUserID.Add(item.UserID);
                    }
                    var LoanTask = _loanTab.WhereClause(x => lstLoanID.Contains(x.LoanID)).QueryAsync();
                    var UserTask = _userTab.WhereClause(x => lstUserID.Contains(x.UserID)).QueryAsync();
                    await Task.WhenAll(LoanTask, UserTask);
                    var dictLoan = LoanTask.Result.ToDictionary(x => x.LoanID, x => x);
                    var dictUser = UserTask.Result.ToDictionary(x => x.UserID, x => x);
                    foreach (var item in lstAssignmentLoan)
                    {
                        var objLoan = dictLoan.GetValueOrDefault(item.LoanID);
                        var objUser = dictUser.GetValueOrDefault(item.UserID);
                        lstDataResult.Add(new LstLoanSplitSingle
                        {
                            ContractCode = objLoan?.ContactCode,
                            TypeName = ((AssignmentLoan_Type)item.Type).GetDescription(),
                            UserName = objUser?.UserName,
                            FullName = objUser?.FullName,
                            Status = item.Status,
                            StatusLoan = ((Loan_Status)item.Status).GetDescription()
                        });
                    }
                }

                response.Data = lstDataResult;
                response.Total = totalCount;
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLstDetailsLoanSplitSingleQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
