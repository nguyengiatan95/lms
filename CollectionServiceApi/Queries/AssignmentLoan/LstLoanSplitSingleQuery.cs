﻿using CollectionServiceApi.Domain.Models.ApprovalLevelBookDebt;
using CollectionServiceApi.Domain.Models.AssignmentLoan;
using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
namespace CollectionServiceApi.Queries.AssignmentLoan
{
    public class LstLoanSplitSingleQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string KeySearch { get; set; }
        public int Type { get; set; }
        public long UserID { get; set; }
        public int Status { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class LstLoanSplitSingleQueryHandler : IRequestHandler<LstLoanSplitSingleQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> _assignmentLoanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<LstLoanSplitSingleQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public LstLoanSplitSingleQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> assignmentLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            ILogger<LstLoanSplitSingleQueryHandler> logger)
        {
            _assignmentLoanTab = assignmentLoanTab;
            _loanTab = loanTab;
            _userTab = userTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(LstLoanSplitSingleQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var lstDataResult = new List<LstLoanSplitSingle>();
            try
            {
                var dictLoanSearch = new Dictionary<long, Domain.Tables.TblLoan>();

                if (long.TryParse(request.KeySearch, out long contracCodeID) && request.KeySearch.Length < TimaSettingConstant.MaxLengthPhone - 1)
                {
                    request.KeySearch = $"{TimaSettingConstant.PrefixContractCode}{contracCodeID}";
                    dictLoanSearch = (await _loanTab.WhereClause(x => x.ContactCode == request.KeySearch).QueryAsync()).ToDictionary(x => x.LoanID, x => x);
                }
                else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternContractCode, RegexOptions.IgnoreCase).Success)
                {
                    dictLoanSearch = (await _loanTab.WhereClause(x => x.ContactCode == request.KeySearch).QueryAsync()).ToDictionary(x => x.LoanID, x => x);
                }
                else if (!string.IsNullOrEmpty(request.KeySearch))
                {
                    response.Message = MessageConstant.ErrorNotExist;
                    response.SetSucces();
                    return response;
                }
                if (!string.IsNullOrEmpty(request.KeySearch) && dictLoanSearch.Keys.Count() > 0)
                    _assignmentLoanTab.WhereClause(x => dictLoanSearch.Keys.Contains(x.LoanID));

                if (request.Type != (int)StatusCommon.SearchAll)
                    _assignmentLoanTab.WhereClause(x => x.Type == request.Type);

                if (request.Status != (int)StatusCommon.SearchAll)
                    _assignmentLoanTab.WhereClause(x => x.Status == request.Status);

                if (request.UserID != (int)StatusCommon.SearchAll)
                    _assignmentLoanTab.WhereClause(x => x.UserID == request.UserID);

                //lấy trong tháng
                DateTime date = DateTime.Now;
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                var lstAssignmentLoan = (await _assignmentLoanTab.WhereClause(x => x.Status == (int)AssignmentLoan_Status.Processing && x.CreateDate >= firstDayOfMonth && x.CreateDate <= lastDayOfMonth).QueryAsync());
                int totalCount = 0;
                if (lstAssignmentLoan.Any())
                {
                    lstAssignmentLoan = lstAssignmentLoan.OrderByDescending(x => x.CreateDate).ToList();
                    var dictAssignmentLoan = new Dictionary<long, List<TblAssignmentLoan>>();
                    foreach (var item in lstAssignmentLoan)
                    {
                        if (!dictAssignmentLoan.ContainsKey(item.LoanID))
                            dictAssignmentLoan[item.LoanID] = new List<Domain.Tables.TblAssignmentLoan>();
                        dictAssignmentLoan[item.LoanID].Add(item);
                    }

                    totalCount = dictAssignmentLoan.Keys.Count();
                    //phân trang theo loanid
                    var lstLoanID = dictAssignmentLoan.Keys.Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                    //lst xử lý
                    var lstHandling = lstAssignmentLoan.Where(x => lstLoanID.Contains(x.LoanID)).ToList();
                    var dictHandling = new Dictionary<long, List<TblAssignmentLoan>>();
                    var lstUserID = new List<long>();
                    foreach (var item in lstHandling)
                    {
                        lstUserID.Add(item.UserID);

                        if (!dictHandling.ContainsKey(item.LoanID))
                            dictHandling[item.LoanID] = new List<Domain.Tables.TblAssignmentLoan>();
                        dictHandling[item.LoanID].Add(item);
                    }

                    var LoanTask = _loanTab.WhereClause(x => lstLoanID.Contains(x.LoanID)).QueryAsync();
                    var UserTask = _userTab.WhereClause(x => lstUserID.Contains(x.UserID)).QueryAsync();
                    await Task.WhenAll(LoanTask, UserTask);
                    var dictLoan = LoanTask.Result.ToDictionary(x => x.LoanID, x => x);
                    var dictUser = UserTask.Result.ToDictionary(x => x.UserID, x => x);
                    foreach (var item in lstLoanID)
                    {
                        var objLoan = dictLoan.GetValueOrDefault(item);
                        var lstAssignment = dictHandling[item];
                        var lstTypeName = new List<string>();
                        var lstUserName = new List<string>();
                        foreach (var j in lstAssignment)
                        {
                            var objUser = dictUser.GetValueOrDefault(j.UserID);
                            lstUserName.Add(objUser?.UserName);
                            //add check trug chỉ show 1 type
                            if (!lstTypeName.Contains(((AssignmentLoan_Type)j.Type).GetDescription()))
                                lstTypeName.Add(((AssignmentLoan_Type)j.Type).GetDescription());
                        }

                        lstDataResult.Add(new LstLoanSplitSingle
                        {
                            LoanID = item,
                            ContractCode = objLoan?.ContactCode,
                            TypeName = string.Join(",", lstTypeName),
                            UserName = string.Join(",", lstUserName),
                            StatusLoan = ((Loan_Status)objLoan.Status).GetDescription()
                        });
                    }
                }
                response.Data = lstDataResult;
                response.Total = totalCount;
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"LstLoanSplitSingleQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
