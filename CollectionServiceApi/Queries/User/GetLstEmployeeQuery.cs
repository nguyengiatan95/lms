﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.User
{
    public class GetLstEmployeeQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long DepartmentID { get; set; }
        public long PositionID { get; set; }
        public long ParentUserID { get; set; }
        public long UserLoginID { get; set; }
        public GetLstEmployeeQuery()
        {
            DepartmentID = (int)Department_ID.THN_Call;
            PositionID = (int)UserDepartment_PositionID.Staff;
        }
    }
    public class GetLstEmployeeQueryHandler : IRequestHandler<GetLstEmployeeQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> _userDepartmentTab;
        readonly ILogger<GetLstEmployeeQueryHandler> _logger;
        readonly Services.IUserService _userService;
        public GetLstEmployeeQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDepartmentTab,
            Services.IUserService userService,
            ILogger<GetLstEmployeeQueryHandler> logger)
        {
            _userDepartmentTab = userDepartmentTab;
            _logger = logger;
            _userService = userService;
        }

        public async Task<ResponseActionResult> Handle(GetLstEmployeeQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var postionUserCurrent = await _userService.GetPositonUserIDLogin(request.UserLoginID);
                // lấy tất cả nhân viên ở phòng thn
                _userDepartmentTab.SelectColumns(x => x.UserID, x => x.PositionID, x => x.ParentUserID, x => x.DepartmentID, x => x.ChildDepartmentID)
                      .JoinOn<Domain.Tables.TblUser>(ud => ud.UserID, u => u.UserID)
                      .SelectColumnsJoinOn<Domain.Tables.TblUser>(u => u.FullName, u => u.UserName)
                      .WhereClauseJoinOn<Domain.Tables.TblUser>(x => x.Status == (int)StatusCommon.Active)
                      .JoinOn<Domain.Tables.TblDepartment>(ud => ud.DepartmentID, d => d.DepartmentID)
                      .WhereClauseJoinOn<Domain.Tables.TblDepartment>(d => d.AppID == (int)Menu_AppID.THN);
                var lstUserCallInfos = await _userDepartmentTab.QueryAsync<Domain.Models.EmployeeInfoModel>();
                if(postionUserCurrent != null)
                {
                    switch ((UserDepartment_PositionID)postionUserCurrent.PositionID)
                    {
                        case UserDepartment_PositionID.Director:
                            {
                                lstUserCallInfos = lstUserCallInfos.Where(x => x.PositionID < (int)UserDepartment_PositionID.Director);
                                if (request.ParentUserID > 0)
                                {
                                    var userManager = lstUserCallInfos.FirstOrDefault(x => x.UserID == request.ParentUserID);
                                    if (userManager != null)
                                    {
                                        switch ((UserDepartment_PositionID)userManager.PositionID)
                                        {
                                            case UserDepartment_PositionID.Manager:
                                                switch ((UserDepartment_PositionID)request.PositionID)
                                                {
                                                    case UserDepartment_PositionID.TeamLead:
                                                        lstUserCallInfos = lstUserCallInfos.Where(x => x.UserID == userManager.ParentUserID || x.ParentUserID == userManager.UserID);
                                                        break;
                                                    default: // lấy nv
                                                        lstUserCallInfos = lstUserCallInfos.Where(x => x.DepartmentID == userManager.DepartmentID && x.PositionID <= userManager.PositionID);
                                                        break;
                                                }
                                                break;
                                            case UserDepartment_PositionID.TeamLead:
                                                // mặc định lấy nv
                                                lstUserCallInfos = lstUserCallInfos.Where(x => x.UserID == userManager.ParentUserID || x.ParentUserID == userManager.UserID);
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        lstUserCallInfos = null;
                                    }
                                }
                                else if (request.PositionID > 0)
                                {
                                    lstUserCallInfos = lstUserCallInfos.Where(x => x.PositionID == request.PositionID);
                                }
                            }
                            break;
                        case UserDepartment_PositionID.Manager:
                            {
                                lstUserCallInfos = lstUserCallInfos.Where(x => x.PositionID <= (int)UserDepartment_PositionID.Manager && x.DepartmentID == postionUserCurrent.DepartmentID);
                                if (request.PositionID == (int)UserDepartment_PositionID.TeamLead)
                                {
                                    lstUserCallInfos = lstUserCallInfos.Where(x => x.PositionID == request.PositionID);
                                }
                                if (request.ParentUserID > 0)
                                {
                                    lstUserCallInfos = lstUserCallInfos.Where(x => x.UserID == request.ParentUserID || x.ParentUserID == request.ParentUserID);
                                }
                            }
                            break;
                        case UserDepartment_PositionID.TeamLead:
                            lstUserCallInfos = lstUserCallInfos.Where(x => x.PositionID <= (int)UserDepartment_PositionID.TeamLead && x.ChildDepartmentID == postionUserCurrent.ChildDepartmentID);
                            lstUserCallInfos = lstUserCallInfos.Where(x => x.UserID == postionUserCurrent.UserID || x.ParentUserID == postionUserCurrent.UserID);
                            break;
                        case UserDepartment_PositionID.Staff:
                            lstUserCallInfos = lstUserCallInfos.Where(x => x.PositionID == (int)UserDepartment_PositionID.Staff && x.DepartmentID == postionUserCurrent.DepartmentID);
                            break;
                    }
                }
                Dictionary<long, Domain.Models.SelectItem> dictEmployeeInfos = new Dictionary<long, Domain.Models.SelectItem>();
                if(lstUserCallInfos != null && lstUserCallInfos.Any())
                {
                    foreach (var item in lstUserCallInfos)
                    {
                        if (!dictEmployeeInfos.ContainsKey(item.UserID))
                        {
                            dictEmployeeInfos.Add(item.UserID, new Domain.Models.SelectItem
                            {
                                Text = $"{item.FullName} - {item.UserName}",
                                Value = item.UserID
                            });
                        }

                    }
                }
                response.Data = dictEmployeeInfos.Values.OrderBy(x => x.Text).ToList();
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLstEmployeeQueryHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
