﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.User
{
    public class GetPositionUserQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserID { get; set; }
    }
    public class GetPositionUserQueryHandler : IRequestHandler<GetPositionUserQuery, LMS.Common.Constants.ResponseActionResult>
    {
        ILogger<GetPositionUserQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        Services.IUserService _userService;
        public GetPositionUserQueryHandler(Services.IUserService userService,
            ILogger<GetPositionUserQueryHandler> logger)
        {
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _userService = userService;
        }

        public async Task<ResponseActionResult> Handle(GetPositionUserQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                Domain.Models.EmployeeInfoModel detail = await _userService.GetPositonUserIDLogin(request.UserID);
                if (detail == null)
                {
                    response.Message = MessageConstant.UserNullPermission;
                    return response;
                }
                response.SetSucces();
                response.Data = detail;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetPositionUserQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
