﻿using CollectionServiceApi.Constants;
using LMS.Common.Constants;
using LMS.Entites.Dtos.AI;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.AI
{
    public class GetGPSLoanIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanCreditIDOfPartner { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int Countminutes { get; set; }
        public int TimeType { get; set; }
        public string LatLongCurrent { get; set; }
        public string DeviceId { get; set; }
    }
    public class GetGPSLoanIDQueryHandler : IRequestHandler<GetGPSLoanIDQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        RestClients.IAIService _aIService;
        RestClients.ILOSService _lLOSService;
        ILogger<GetGPSLoanIDQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetGPSLoanIDQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            RestClients.IAIService aIService,
            RestClients.ILOSService lLOSService,
            ILogger<GetGPSLoanIDQueryHandler> logger)
        {
            _loanTab = loanTab;
            _aIService = aIService;
            _lLOSService = lLOSService;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetGPSLoanIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            List<ResponseGPSData> lstData = new List<ResponseGPSData>();
            var requestGPS = new RequestGPS();
            try
            {
                requestGPS.contractid = "HD-" + request.LoanCreditIDOfPartner;
                requestGPS.imei = request.DeviceId;
                DateTime dtFromDate = DateTime.ParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture);
                DateTime dtToDate = DateTime.ParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture);
                requestGPS.time_received1 = _common.DateTimeToUnixTimesTampMilliseconds(dtFromDate);
                requestGPS.time_received2 = _common.DateTimeToUnixTimesTampMilliseconds(dtToDate);
                var responseData = await _aIService.GetStopPoint(requestGPS);
                if (responseData == null || responseData.Data == null)
                {
                    response.Message = MessageConstant.NotInforAI;
                    return response;
                }
                var lstDataAI = responseData.Data;
                lstDataAI = lstDataAI.Where(m => m.TimeToMinute >= request.Countminutes * GPSLoanConstants.Countminutes).ToList();
                double CountDay = (dtToDate - dtFromDate).TotalDays;

                if (request.TimeType == (int)TimeType_StopPoint.DayTime)// ngày
                {
                    for (int i = 0; i < CountDay; i++)
                    {
                        dtFromDate = dtFromDate.Date.AddDays(GPSLoanConstants.FromDateAddDay);
                        foreach (var item in lstDataAI)
                        {
                            if (item.startToDateTime <= dtFromDate.AddHours(GPSLoanConstants.FromDateAddHours18) && item.endToDateTime > dtFromDate.AddHours(GPSLoanConstants.FromDateAddHours7))
                            {
                                if (!lstData.Contains(item))
                                {
                                    lstData.Add(item);
                                }
                            }
                        }
                    }
                }
                else if (request.TimeType == (int)TimeType_StopPoint.Night) // đêm
                {
                    for (int i = 0; i < CountDay; i++)
                    {
                        dtFromDate = dtFromDate.Date.AddDays(GPSLoanConstants.FromDateAddDay);
                        foreach (var item in lstDataAI)
                        {
                            if (item.startToDateTime <= dtFromDate.AddDays(GPSLoanConstants.FromDateAddDay).AddHours(GPSLoanConstants.FromDateAddHours7) && item.endToDateTime >= dtFromDate.AddHours(GPSLoanConstants.FromDateAddHours18))
                            {
                                if (!lstData.Contains(item))
                                {
                                    lstData.Add(item);
                                }
                            }
                        }
                    }
                }
                else
                {
                    lstData = lstDataAI;
                }

                lstData = lstData.OrderByDescending(m => m.Start).ToList();
                response.SetSucces();
                response.Data = lstData;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetGPSLoanIDQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
