﻿using CollectionServiceApi.Constants;
using LMS.Common.Constants;
using LMS.Entites.Dtos.AG;
using LMS.Entites.Dtos.AI;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.AI
{
    public class ShowHistoryCommentQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanCreditIDOfPartner { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class ShowHistoryCommentQueryHandler : IRequestHandler<ShowHistoryCommentQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        RestClients.IAIService _aIService;
        ILogger<ShowHistoryCommentQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public ShowHistoryCommentQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            RestClients.IAIService aIService,
            ILogger<ShowHistoryCommentQueryHandler> logger)
        {
            _loanTab = loanTab;
            _aIService = aIService;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(ShowHistoryCommentQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var req = new HistoryCommentChipReq
                {
                    LoanCreditIDOfPartner = request.LoanCreditIDOfPartner,
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize
                };
                var result = await _aIService.ShowHistoryComment(req);
                if (result != null)
                {
                    response.Data = result;
                    response.Total = (int)result[0].TotalCount;
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ShowHistoryCommentQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
