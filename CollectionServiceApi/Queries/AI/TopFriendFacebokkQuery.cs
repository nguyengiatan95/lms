﻿using CollectionServiceApi.Domain;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.AI
{
    public class TopFriendFacebookQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string KeySearch { get; set; }
    }
    public class TopFriendFacebookQueryHandler : IRequestHandler<TopFriendFacebookQuery, ResponseActionResult>
    {
        RestClients.IAIService _aIService;
        ILogger<TopFriendFacebookQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public TopFriendFacebookQueryHandler(
            RestClients.IAIService aIService,
            ILogger<TopFriendFacebookQueryHandler> logger)
        {
            _aIService = aIService;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(TopFriendFacebookQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var result = await _aIService.TopFriendFacebook(request.KeySearch);
                if (result != null)
                {
                    response.SetSucces();
                    response.Data = result;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"TopFriendFacebookQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
