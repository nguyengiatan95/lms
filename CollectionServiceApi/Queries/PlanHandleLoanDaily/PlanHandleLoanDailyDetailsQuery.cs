﻿using CollectionServiceApi.Domain.Models.PlanHandleLoanDaily;
using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace CollectionServiceApi.Queries.PlanHandleLoanDaily
{
    public class PlanHandleLoanDailyDetailsQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public long UserID { get; set; }
    }
    public class PlanHandleLoanDailyDetailsQueryHandler : IRequestHandler<PlanHandleLoanDailyDetailsQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> _planHandleLoanDailyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> _commentDebtPromptedTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> _assignmentLoanTab;
        ILogger<PlanHandleLoanDailyDetailsQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        Services.IReportManager _reportManager;
        public PlanHandleLoanDailyDetailsQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> planHandleLoanDailyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> commentDebtPromptedTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> assignmentLoanTab,
            Services.IReportManager reportManager,
            ILogger<PlanHandleLoanDailyDetailsQueryHandler> logger)
        {
            _planHandleLoanDailyTab = planHandleLoanDailyTab;
            _loanTab = loanTab;
            _commentDebtPromptedTab = commentDebtPromptedTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _assignmentLoanTab = assignmentLoanTab;
            _reportManager = reportManager;

        }
        public async Task<ResponseActionResult> Handle(PlanHandleLoanDailyDetailsQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var currentDate = DateTime.Now;
            var firtDateOfMonth = new DateTime(currentDate.Year, currentDate.Month, 1);
            var firtDateOfNextMonth = firtDateOfMonth.AddMonths(1);
            firtDateOfMonth = firtDateOfMonth.AddSeconds(-1);
            try
            {
                var lstResult = new List<PlanHandleLoanDailyItem>();
                if (string.IsNullOrEmpty(request.FromDate) || string.IsNullOrEmpty(request.ToDate))
                {
                    response.Message = MessageConstant.ParameterInvalid;
                    return response;
                }
                var fromDate = DateTime.ParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture);
                fromDate = fromDate.AddSeconds(-1);
                toDate = toDate.AddDays(1);
                var lstStatus = new List<int>
                {
                    (int)PlanHandleLoanDaily_Status.NoProcess,
                    (int)PlanHandleLoanDaily_Status.Processed,
                };
                // lấy kế hoạch trong tháng hiện tại
                var lstPlanHandleLoanDaily = await _planHandleLoanDailyTab.WhereClause(x => x.UserID == request.UserID && lstStatus.Contains(x.Status) && x.FromDate > fromDate && x.ToDate < toDate).QueryAsync();
                if (lstPlanHandleLoanDaily.Any())
                {
                    var lstLoanID = new List<long>();
                    foreach (var item in lstPlanHandleLoanDaily)
                    {
                        lstLoanID.Add(item.LoanID);
                    }
                    lstLoanID = lstLoanID.Distinct().ToList();
                    // lấy comment mới nhất của mỗi đơn vay trong tháng
                    var lstCommentDebtPromptedDataTask = _reportManager.GetDictLoanCommentLatestInfosByLstLoanID(lstLoanID, firtDateOfMonth, firtDateOfNextMonth);
                    var lstLoanDataTask = _loanTab.WhereClause(x => lstLoanID.Contains(x.LoanID))
                        .SelectColumns(x => x.LoanID, x => x.ContactCode, x => x.CustomerName).QueryAsync();

                    var lstLoanAssignedTask = _assignmentLoanTab.SelectColumns(x => x.LoanID, x => x.Type)
                                                                .WhereClause(x => x.UserID == request.UserID && lstLoanID.Contains(x.LoanID) && x.Status == (int)AssignmentLoan_Status.Processing)
                                                                .WhereClause(x => x.DateAssigned > firtDateOfMonth && x.DateApplyTo < firtDateOfNextMonth).QueryAsync();
                    await Task.WhenAll(lstCommentDebtPromptedDataTask, lstLoanDataTask, lstLoanAssignedTask);

                    var dictCommentDebtPrompted = lstCommentDebtPromptedDataTask.Result.Where(x => RuleCollectionServiceHelper.AppTHNReason_PTP.Contains(x.Value.ReasonCode))
                                                                                       .ToDictionary(x => x.Key, x => x.Value.ReasonCode);

                    var dictLoanInfos = lstLoanDataTask.Result.ToDictionary(x => x.LoanID, x => x);
                    var dictLoanAssignedInfos = lstLoanAssignedTask.Result.GroupBy(x => x.LoanID).ToDictionary(x => x.Key, x => x.Select(x => x.Type).ToList());
                    foreach (var item in lstPlanHandleLoanDaily)
                    {
                        var objLoan = dictLoanInfos.GetValueOrDefault(item.LoanID);
                        lstResult.Add(new PlanHandleLoanDailyItem
                        {
                            PlanHandleLoanDailyID = item.PlanHandleLoanDailyID,
                            LoanID = item.LoanID,
                            CustomerName = objLoan?.CustomerName,
                            ContractCode = objLoan?.ContactCode,
                            TimeCheckin = item.TimeCheckin,
                            TimeCheckOut = item.TimeCheckOut,
                            Status = (int)item.Status,
                            Reason = dictCommentDebtPrompted.GetValueOrDefault(item.LoanID),
                            LstAssignmentLoanType = dictLoanAssignedInfos.GetValueOrDefault(item.LoanID),
                            ShowPTP = !string.IsNullOrEmpty(dictCommentDebtPrompted.GetValueOrDefault(item.LoanID))
                        });
                    }
                }
                response.SetSucces();
                response.Data = lstResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"PlanHandleLoanDailyDetailsQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
