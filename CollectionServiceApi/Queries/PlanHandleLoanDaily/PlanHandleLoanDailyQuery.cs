﻿using CollectionServiceApi.Domain.Models.PlanHandleLoanDaily;
using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace CollectionServiceApi.Queries.PlanHandleLoanDaily
{
    public class PlanHandleLoanDailyQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserID { get; set; }
    }
    public class PlanHandleLoanDailyQueryHandler : IRequestHandler<PlanHandleLoanDailyQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> _planHandleLoanDailyTab;
        ILogger<PlanHandleLoanDailyQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public PlanHandleLoanDailyQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> planHandleLoanDailyTab,
            ILogger<PlanHandleLoanDailyQueryHandler> logger)
        {
            _planHandleLoanDailyTab = planHandleLoanDailyTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();

        }
        public async Task<ResponseActionResult> Handle(PlanHandleLoanDailyQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                DateTime date = DateTime.Now;
                var fromDate = new DateTime(date.Year, date.Month, 1);
                var toDate = fromDate.AddMonths(1);
                fromDate = fromDate.AddSeconds(-1);
                var lstStatus = new List<int>
                {
                    (int)PlanHandleLoanDaily_Status.NoProcess,
                    (int)PlanHandleLoanDaily_Status.Processed,
                };
                var lstPlanHandleLoanDaily = (await _planHandleLoanDailyTab.WhereClause(x => x.UserID == request.UserID &&
                                              lstStatus.Contains(x.Status) && x.FromDate > fromDate && x.ToDate < toDate).QueryAsync()).OrderByDescending(x => x.CreateDate).ToList();

                var dictPlanHandleLoanDaily = new Dictionary<string, PlanHandleLoanDailyModel>();
                if (lstPlanHandleLoanDaily != null && lstPlanHandleLoanDaily.Any())
                {
                    foreach (var item in lstPlanHandleLoanDaily)
                    {
                        var key = $"{item.FromDate.ToString(TimaSettingConstant.DateTimeDayMonthYear)}-{item.ToDate.ToString(TimaSettingConstant.DateTimeDayMonthYear)}";
                        if (!dictPlanHandleLoanDaily.ContainsKey(key))
                        {
                            dictPlanHandleLoanDaily.Add(key, new PlanHandleLoanDailyModel
                            {
                                FromDate = item.FromDate,
                                ToDate = item.ToDate
                            });
                        }
                    }
                }
                response.SetSucces();
                response.Data = dictPlanHandleLoanDaily.Values.ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError($"PlanHandleLoanDailyQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
