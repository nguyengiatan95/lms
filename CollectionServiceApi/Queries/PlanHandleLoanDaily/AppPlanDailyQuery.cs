﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.PlanHandleLoanDaily
{
    public class AppPlanDailyQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string SearchDate { get; set; }
        public long DepartmentID { get; set; }
        public long LeadID { get; set; }
        public long StaffID { get; set; }
    }
    public class AppPlanDailyQueryQueryHandler : IRequestHandler<AppPlanDailyQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> _planHandleLoanDailyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> _departmentTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> _userDepartmentTab;


        ILogger<AppPlanDailyQueryQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public AppPlanDailyQueryQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> planHandleLoanDailyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> departmentTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDepartmentTab,
        ILogger<AppPlanDailyQueryQueryHandler> logger)
        {
            _planHandleLoanDailyTab = planHandleLoanDailyTab;
            _departmentTab = departmentTab;
            _userDepartmentTab = userDepartmentTab;
            _userTab = userTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();

        }
        public async Task<ResponseActionResult> Handle(AppPlanDailyQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var searchTime = DateTime.Now.Date;
            var lstData = new List<LMS.Entites.Dtos.CollectionServices.PlanHandleLoanDaily.AppPlanDaily>();
            try
            {
                if (request.StaffID != (int)StatusCommon.SearchAll && request.LeadID == (int)StatusCommon.SearchAll)
                {
                    request.LeadID = (await _userDepartmentTab.SelectColumns(x => x.ParentUserID).WhereClause(x => x.UserID == request.StaffID && x.DepartmentID == request.DepartmentID && x.Status == (int)StatusCommon.Active).QueryAsync()).FirstOrDefault().ParentUserID;
                }
                if (request.LeadID != (int)StatusCommon.SearchAll && request.DepartmentID == (int)StatusCommon.SearchAll)
                {
                    request.DepartmentID = (await _userDepartmentTab.SelectColumns(x => x.DepartmentID).WhereClause(x => x.UserID == request.LeadID && x.Status == (int)StatusCommon.Active).QueryAsync()).FirstOrDefault().DepartmentID;
                }
                var department = await _departmentTab.WhereClause(x => x.Status == (int)Department_Status.Active && x.AppID == (int)Menu_AppID.THN).QueryAsync();
                var parentDepartment = department.Where(x => x.ParentID == 0);
                // lấy user trưởng nhóm thn
                foreach (var item in parentDepartment)
                {
                    lstData.Add(new LMS.Entites.Dtos.CollectionServices.PlanHandleLoanDaily.AppPlanDaily()
                    {
                        DepartmentID = item.DepartmentID,
                        DepartmentName = item.DepartmentName,
                    });
                }
                if (request.DepartmentID != (int)StatusCommon.SearchAll)
                {
                    //lstData = lstData.Where(x => x.DepartmentID == request.DepartmentID).ToList();
                    var childDepartment = department.Where(x => x.ParentID > 0);
                    var departmentChildIds = childDepartment.Select(x => x.DepartmentID).ToList();

                    // lấy userID 
                    var lstUserDepartment = await _userDepartmentTab.SelectColumns(x => x.UserID, x => x.ChildDepartmentID, x => x.DepartmentID).WhereClause(x => x.PositionID == (int)UserDepartment_PositionID.TeamLead
                    && departmentChildIds.Contains(x.ChildDepartmentID))
                        .QueryAsync();
                    var lstUserIDs = lstUserDepartment.Select(x => x.UserID).ToList();
                    var lstUser = await _userTab.WhereClause(x => lstUserIDs.Contains(x.UserID) && x.Status == (int)StatusCommon.Active).QueryAsync();
                    var dicUser = lstUser.ToDictionary(x => x.UserID, x => x.FullName);
                    foreach (var item in lstData)
                    {
                        if (request.DepartmentID == item.DepartmentID)
                        {
                            var lstChildData = childDepartment.Where(x => x.ParentID == item.DepartmentID).ToList();
                            foreach (var child in lstChildData)
                            {
                                var lstUserDepartmentChild = lstUserDepartment.Where(x => x.ChildDepartmentID == child.DepartmentID).ToList();

                                foreach (var userDepartment in lstUserDepartmentChild)
                                {
                                    if (dicUser.ContainsKey(userDepartment.UserID))
                                    {
                                        item.TeamleadPlans.Add(new LMS.Entites.Dtos.CollectionServices.PlanHandleLoanDaily.TeamleadPlan()
                                        {
                                            LeadID = userDepartment.UserID,
                                            LeadName = child.DepartmentName + "(" + (dicUser.ContainsKey(userDepartment.UserID) ? dicUser[userDepartment.UserID] : "") + ")",
                                            DepartmentID = child.DepartmentID
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
                if (request.LeadID != (int)StatusCommon.SearchAll)
                {
                    List<long> lstUserIDs = new List<long>();
                    if (request.StaffID != (int)StatusCommon.SearchAll)
                    {
                        lstUserIDs.Add(request.StaffID);
                    }
                    else
                    {
                        lstUserIDs = (await _userDepartmentTab.SelectColumns(x => x.UserID).WhereClause(x => x.ParentUserID == request.LeadID || x.UserID == request.LeadID).QueryAsync()).Select(x => x.UserID).ToList();
                    }
                    var dicUser = (await _userTab.WhereClause(x => lstUserIDs.Contains(x.UserID) && x.Status == (int)StatusCommon.Active).QueryAsync()).ToDictionary(x => x.UserID, x => x.FullName);
                    if (!string.IsNullOrWhiteSpace(request.SearchDate))
                    {
                        searchTime = DateTime.ParseExact(request.SearchDate, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture);
                    }
                    var firstDateOfMonth = new DateTime(searchTime.Year, searchTime.Month, 1);
                    List<int> lstStatusSelected = new List<int>
                    {
                        (int)PlanHandleLoanDaily_Status.NoProcess,
                        (int)PlanHandleLoanDaily_Status.Processed
                    };
                    var lstPlans = await _planHandleLoanDailyTab.WhereClause(x => lstStatusSelected.Contains(x.Status) && x.FromDate >= firstDateOfMonth && x.ToDate <= searchTime && lstUserIDs.Contains((long)x.UserID)).QueryAsync();
                    foreach (var item in lstData)
                    {
                        foreach (var child in item.TeamleadPlans)
                        {
                            if (child.LeadID == request.LeadID)
                            {
                                foreach (var userID in lstUserIDs)
                                {
                                    if (dicUser.ContainsKey(userID))
                                    {
                                        child.StaffPlans.Add(new LMS.Entites.Dtos.CollectionServices.PlanHandleLoanDaily.StaffPlan()
                                        {
                                            StaffID = userID,
                                            StaffName = dicUser.ContainsKey(userID) ? dicUser[userID] : "",
                                            TotalDone = lstPlans.Where(x => x.Status == (int)PlanHandleLoanDaily_Status.Processed && x.UserID == userID).Count(),
                                            TotalRegister = lstPlans.Where(x => x.UserID == userID).Count(),
                                        });
                                    }
                                }
                            }

                        }
                    }
                }
                response.SetSucces();
                response.Data = lstData;

            }
            catch (Exception ex)
            {
                _logger.LogError($"AppPlanDailyQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
