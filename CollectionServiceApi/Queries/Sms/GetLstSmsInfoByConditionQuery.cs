﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.Sms
{
    public class GetLstSmsInfoByConditionQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalRow { get; set; }
        public long TotalMoney { get; set; }
        public long BankID { get; set; }
        public string SearchNote { get; set; }
    }
    public class GetLstLoanInfoByConditionQueryHandler : IRequestHandler<GetLstSmsInfoByConditionQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSmsAnalytics> _smsAnalyticsTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> _bankcardTab;
        ILogger<GetLstLoanInfoByConditionQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetLstLoanInfoByConditionQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblSmsAnalytics> smsAnalyticsTab,
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblBankCard> bankcardTab,
            ILogger<GetLstLoanInfoByConditionQueryHandler> logger)
        {
            _smsAnalyticsTab = smsAnalyticsTab;
            _bankcardTab = bankcardTab;
            _logger = logger;
            _common = new Utils();
        }
        public async Task<ResponseActionResult> Handle(GetLstSmsInfoByConditionQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            List<LMS.Entites.Dtos.Sms.SmsItemViewModel> lstSmsRespone = new List<LMS.Entites.Dtos.Sms.SmsItemViewModel>();
            try
            {
                if (!string.IsNullOrWhiteSpace(request.SearchNote) && request.TotalMoney <= 0 && request.BankID == (int)LMS.Common.Constants.StatusCommon.SearchAll)
                {
                    response.Message = MessageConstant.SMS_SearchErrorNote;
                    return response;
                }
                DateTime? fromDate = null;
                DateTime? toDate = null;
                if (DateTime.TryParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, null, System.Globalization.DateTimeStyles.None, out DateTime dateRequest))
                {
                    fromDate = dateRequest;
                }
                if (DateTime.TryParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, null, System.Globalization.DateTimeStyles.None, out dateRequest))
                {
                    toDate = dateRequest;
                }
                if (fromDate.HasValue)
                {
                    fromDate = fromDate.Value.AddSeconds(-1);
                    _smsAnalyticsTab.WhereClause(x => x.CreateDate > fromDate.Value);
                }
                if (toDate.HasValue)
                {
                    toDate = toDate.Value.AddDays(1);
                    _smsAnalyticsTab.WhereClause(x => x.CreateDate < toDate.Value);
                }
                if (request.TotalMoney > 0)
                {
                    _smsAnalyticsTab.WhereClause(x => x.IncreaseMoney == request.TotalMoney);
                }
                if (!string.IsNullOrWhiteSpace(request.SearchNote))
                {
                    _smsAnalyticsTab.WhereClause(x => x.SmsContent.Contains(request.SearchNote));
                }
                if (request.BankID != (int)LMS.Common.Constants.StatusCommon.SearchAll)
                {
                    var lstBankcard = _bankcardTab.WhereClause(x => x.BankID == request.BankID).Query();
                    if (lstBankcard == null || !lstBankcard.Any())
                    {
                        ResponseForDataTable responseForData = new ResponseForDataTable
                        {
                            RecordsTotal = request.TotalRow,
                            Data = lstSmsRespone,
                            RecordsFiltered = request.TotalRow,
                            Draw = 1
                        };
                        response.SetSucces();
                        response.Data = responseForData;
                        return response;
                    }
                    else
                    {
                        var bankCardIds = lstBankcard.Select(x => x.BankCardID).ToList();
                        _smsAnalyticsTab.WhereClause(x => bankCardIds.Contains(x.BankCardId));
                    }
                }
                var lstSmsData = (await _smsAnalyticsTab.WhereClause(x => x.Status != (int)SmsAnalytics_Status.DaHuy).QueryAsync()).OrderByDescending(x => x.SmsAnalyticsID).ToList();
                request.TotalRow = lstSmsData.Count;
                lstSmsData = lstSmsData.Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                foreach (var item in lstSmsData)
                {
                    lstSmsRespone.Add(new LMS.Entites.Dtos.Sms.SmsItemViewModel
                    {
                        SmsAnalyticsID = item.SmsAnalyticsID,
                        Sender = item.Sender,
                        SmsContent = item.SmsContent,
                        LoanCode = item.ContractCode,
                        LoanID = item.LoanId,
                        ShopID = item.ShopID ?? 0,
                        ShopName = item.ShopName,
                        CustomerID = item.CustomerId,
                        CustomerName = item.CustomerName,
                        IncreaseMoney = item.IncreaseMoney ?? 0,
                        SmsStatusName = ((SmsAnalytics_Status)item.Status).GetDescription(),
                        SmsreceivedDate = item.SmsreceivedDate ?? item.CreateDate,
                        ModifyDate = item.ModifyDate ?? item.CreateDate,
                        BankCardID = item.BankCardId,
                        BankCardAliasName = item.BankCardAliasName
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLstSmsInfoByCondition|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            ResponseForDataTable dataTable = new ResponseForDataTable
            {
                RecordsTotal = request.TotalRow,
                Data = lstSmsRespone,
                RecordsFiltered = request.TotalRow,
                Draw = 1
            };
            response.SetSucces();
            response.Data = dataTable;
            return response;
        }
    }
}
