﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.LOSServices;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace CollectionServiceApi.Queries.LOS
{
     public class ListImagesQuery : IRequest<ResponseActionResult>
    {
        public long LoanID { get; set; }
    }
    public class ListImagesQueryHandler : IRequestHandler<ListImagesQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        RestClients.ILOSService _lOSService;
        ILogger<ListImagesQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public ListImagesQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            RestClients.ILOSService lOSService,
            ILogger<ListImagesQueryHandler> logger)
        {
            _loanTab = loanTab;
            _lOSService = lOSService;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(ListImagesQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objLoan = _loanTab.WhereClause(x => x.LoanID == request.LoanID).Query().FirstOrDefault();
                if (objLoan != null && objLoan.LoanCreditIDOfPartner != null)
                {
                    var getListImagesReq = new GetListImagesReq
                    {
                        LoanBriefId = (long)objLoan.LoanCreditIDOfPartner,
                        TypeFile = (int)LOS_TypeFile.Img
                    };
                    response.Data = await _lOSService.GetListImages(getListImagesReq);
                }
                response.SetSucces();
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ListImagesQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
