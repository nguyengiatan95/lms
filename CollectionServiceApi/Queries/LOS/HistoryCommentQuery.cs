﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.LOS
{
    public class HistoryCommentQuery : IRequest<ResponseActionResult>
    {
        public long LoanID { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class HistoryCommentQueryHandler : IRequestHandler<HistoryCommentQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        RestClients.ILOSService _lOSService;
        ILogger<HistoryCommentQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public HistoryCommentQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            RestClients.ILOSService lOSService,
            ILogger<HistoryCommentQueryHandler> logger)
        {
            _loanTab = loanTab;
            _lOSService = lOSService;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(HistoryCommentQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objLoan = _loanTab.WhereClause(x => x.LoanID == request.LoanID).Query().FirstOrDefault();
                if (objLoan != null && objLoan.LoanCreditIDOfPartner != null)
                {
                    // lấy full history
                    response.Data = await _lOSService.GetHistoryCommentLOS(request.PageIndex, Int16.MaxValue, (long)objLoan.LoanCreditIDOfPartner);
                }
                response.SetSucces();
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"HistoryCommentQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
