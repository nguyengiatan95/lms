﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.LOS
{
    public class GetRelativeFamilyLOSQuery : IRequest<ResponseActionResult>
    {
    }
    public class GetRelativeFamilyLOSQueryHandler : IRequestHandler<GetRelativeFamilyLOSQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        RestClients.ILOSService _lOSService;
        ILogger<GetRelativeFamilyLOSQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetRelativeFamilyLOSQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            RestClients.ILOSService lOSService,
            ILogger<GetRelativeFamilyLOSQueryHandler> logger)
        {
            _loanTab = loanTab;
            _lOSService = lOSService;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetRelativeFamilyLOSQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                response.SetSucces();
                response.Data = await _lOSService.GetRelativeFamilyLOS();
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetRelativeFamilyLOSQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
