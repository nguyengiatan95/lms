﻿using CollectionServiceApi.Domain.Models.AppTHN;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.AppTHN
{
    public class GetSummaryPlanHandleLoanByLstPositionQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserID { get; set; }
    }
    public class GetSummaryPlanHandleLoanByLstPositionQueryHandler : IRequestHandler<GetSummaryPlanHandleLoanByLstPositionQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> _assignmentLoanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> _planHandleLoanDailyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> _commentDebtPromptedTab;
        ILogger<GetSummaryPlanHandleLoanByLstPositionQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        RestClients.ILOSService _losService;
        public GetSummaryPlanHandleLoanByLstPositionQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> assignmentLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> planHandleLoanDailyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> commentDebtPromptedTab,
            RestClients.ILOSService losService,
            ILogger<GetSummaryPlanHandleLoanByLstPositionQueryHandler> logger)
        {
            _assignmentLoanTab = assignmentLoanTab;
            _loanTab = loanTab;
            _customerTab = customerTab;
            _planHandleLoanDailyTab = planHandleLoanDailyTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _losService = losService;
            _commentDebtPromptedTab = commentDebtPromptedTab;
        }
        public async Task<ResponseActionResult> Handle(GetSummaryPlanHandleLoanByLstPositionQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var summaryDetail = new SummaryPlanLoanHandleByStaffModel();
            try
            {
                DateTime currentDate = DateTime.Now;
                var firtDateOfNextMonth = new DateTime(currentDate.Year, currentDate.Month, 1);
                var toDate = firtDateOfNextMonth.AddMonths(1);
                firtDateOfNextMonth = firtDateOfNextMonth.AddSeconds(-1);

                var assignmentLoanTask = _assignmentLoanTab.GetDistinct().SelectColumns(x => x.LoanID)
                                                        .WhereClause(x => x.UserID == request.UserID && x.DateAssigned > firtDateOfNextMonth && x.DateAssigned < toDate && x.Status == (int)AssignmentLoan_Status.Processing)
                                                        .QueryAsync();
                var planLoanHandleDataInMonthTask = _planHandleLoanDailyTab.GetDistinct().SelectColumns(x => x.LoanID)
                                                        .WhereClause(x => x.UserID == request.UserID && x.ToDate >= currentDate.Date && x.Status == (int)PlanHandleLoanDaily_Status.NoProcess)
                                                        .QueryAsync();

                await Task.WhenAll(assignmentLoanTask, planLoanHandleDataInMonthTask);
                summaryDetail.TotalLoanAssigned = assignmentLoanTask.Result.Count();
                summaryDetail.TotalLoanNoPlan = summaryDetail.TotalLoanAssigned - planLoanHandleDataInMonthTask.Result.Count();
                response.Data = summaryDetail;
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetSummaryPlanLoanHandleByStaffQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
