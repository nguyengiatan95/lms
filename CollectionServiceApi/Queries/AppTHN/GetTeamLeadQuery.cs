﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.AppTHN
{
    public class GetTeamLeadQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long DepartmentID { get; set; }
    }

    public class GetTeamLeadQueryHandler : IRequestHandler<GetTeamLeadQuery, LMS.Common.Constants.ResponseActionResult>
    {
        ILogger<GetTeamLeadQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> _departmentTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> _userDepartmentTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;


        public GetTeamLeadQueryHandler(
             LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> departmentTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDepartmentTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            ILogger<GetTeamLeadQueryHandler> logger)
        {
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _departmentTab = departmentTab;
            _userDepartmentTab = userDepartmentTab;
            _userTab = userTab;
        }

        public async Task<ResponseActionResult> Handle(GetTeamLeadQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                if (request.DepartmentID > 0)
                {
                    _departmentTab.WhereClause(x => x.ParentID == request.DepartmentID);
                }
                List<LMS.Entites.Dtos.CollectionServices.PlanHandleLoanDaily.TeamLeadAppTHN> teamleads = new List<LMS.Entites.Dtos.CollectionServices.PlanHandleLoanDaily.TeamLeadAppTHN>();
                var childDepartment = (await _departmentTab.WhereClause(x => x.Status == (int)Department_Status.Active
                                                                            && x.AppID == (int)Menu_AppID.THN).QueryAsync());

                var departmentChildIds = childDepartment.Select(x => x.DepartmentID).ToList();

                var lstUserDepartment = await _userDepartmentTab.SelectColumns(x => x.UserID, x => x.ChildDepartmentID, x => x.DepartmentID).WhereClause(x => x.PositionID == (int)UserDepartment_PositionID.TeamLead
                                                                              && departmentChildIds.Contains(x.ChildDepartmentID)).QueryAsync();
                var lstUserIDs = lstUserDepartment.Select(x => x.UserID).ToList();
                var lstUser = await _userTab.WhereClause(x => lstUserIDs.Contains(x.UserID) && x.Status == (int)StatusCommon.Active).QueryAsync();
                var dicUser = lstUser.ToDictionary(x => x.UserID, x => x.FullName);
                foreach (var child in childDepartment)
                {
                    var lstUserDepartmentChild = lstUserDepartment.Where(x => x.ChildDepartmentID == child.DepartmentID).ToList();
                    foreach (var userDepartment in lstUserDepartmentChild)
                    {
                        if (dicUser.ContainsKey(userDepartment.UserID))
                        {
                            teamleads.Add(new LMS.Entites.Dtos.CollectionServices.PlanHandleLoanDaily.TeamLeadAppTHN
                            {
                                ID = userDepartment.UserID,
                                Name = child.DepartmentName + "(" + (dicUser.ContainsKey(userDepartment.UserID) ? dicUser[userDepartment.UserID] : "") + ")"
                            });
                        }
                    }
                }
                response.SetSucces();
                response.Data = teamleads;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetTeamLeadQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
