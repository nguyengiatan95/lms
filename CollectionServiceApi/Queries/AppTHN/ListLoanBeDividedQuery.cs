﻿using CollectionServiceApi.Domain.Models.AppTHN;
using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
namespace CollectionServiceApi.Queries.AppTHN
{
    public class ListLoanBeDividedQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string KeySearch { get; set; }
        public long EmployeeID { get; set; }
        public int TypeID { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public long CityID { get; set; }
        public long DistrictID { get; set; }
    }
    public class ListLoanBeDividedQueryHandler : IRequestHandler<ListLoanBeDividedQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> _assignmentLoanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> _planHandleLoanDailyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> _commentDebtPromptedTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanCloseLoan> _planCloseLoanTab;
        ILogger<ListLoanBeDividedQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        RestClients.ILOSService _losService;
        Services.IReportManager _reportManager;
        public ListLoanBeDividedQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> assignmentLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> planHandleLoanDailyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> commentDebtPromptedTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanCloseLoan> planCloseLoanTab,
            RestClients.ILOSService losService,
            Services.IReportManager reportManager,
            ILogger<ListLoanBeDividedQueryHandler> logger)
        {
            _assignmentLoanTab = assignmentLoanTab;
            _loanTab = loanTab;
            _customerTab = customerTab;
            _planHandleLoanDailyTab = planHandleLoanDailyTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _losService = losService;
            _commentDebtPromptedTab = commentDebtPromptedTab;
            _planCloseLoanTab = planCloseLoanTab;
            _reportManager = reportManager;
        }
        public async Task<ResponseActionResult> Handle(ListLoanBeDividedQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            var lstDataResult = new List<ListLoanBeDividedItem>();
            try
            {
                List<long> lstLoanIDSearch = new List<long>();
                bool hasSearchLoan = false;
                if (!string.IsNullOrEmpty(request.KeySearch))
                {
                    List<long> lstCustomerIDs = new List<long>();
                    if (long.TryParse(request.KeySearch, out long contracCodeID) && request.KeySearch.Length < TimaSettingConstant.MaxLengthPhone - 1)
                    {
                        request.KeySearch = $"{TimaSettingConstant.PrefixContractCode}{contracCodeID}";
                        _loanTab.WhereClause(x => x.ContactCode == request.KeySearch);
                    }
                    else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternContractCode, RegexOptions.IgnoreCase).Success)
                    {
                        _loanTab.WhereClause(x => x.ContactCode == request.KeySearch);
                    }
                    else
                    {
                        if (request.KeySearch.Length == TimaSettingConstant.MaxLengthPhone && Regex.Match(request.KeySearch, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success)
                        {
                            lstCustomerIDs = (await _customerTab.SelectColumns(x => x.CustomerID)
                                                                .WhereClause(x => x.Phone == request.KeySearch)
                                                                .QueryAsync())
                                                                .Select(x => x.CustomerID).ToList();
                        }
                        else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternNumberCard, RegexOptions.IgnoreCase).Success)
                        {
                            lstCustomerIDs = (await _customerTab.SelectColumns(x => x.CustomerID)
                                                                .WhereClause(x => x.NumberCard == request.KeySearch)
                                                                .QueryAsync())
                                                                .Select(x => x.CustomerID).ToList();
                        }
                        if (lstCustomerIDs != null && lstCustomerIDs.Count > 0)
                        {
                            _loanTab.WhereClause(x => lstCustomerIDs.Contains((long)x.CustomerID));
                        }
                        else
                        {
                            _loanTab.WhereClause(x => x.CustomerName.Contains(request.KeySearch));
                        }
                    }
                    hasSearchLoan = true;
                }

                if (request.CityID > 0)
                {
                    hasSearchLoan = true;
                    _loanTab.WhereClause(x => x.CityID == request.CityID);

                }
                if (request.DistrictID > 0)
                {
                    hasSearchLoan = true;
                    _loanTab.WhereClause(x => x.DistrictID == request.DistrictID);
                }
                if (hasSearchLoan)
                {
                    lstLoanIDSearch = (await _loanTab.SelectColumns(x => x.LoanID)
                                                     .WhereClause(x => RuleCollectionServiceHelper.LstStatusLoanLending.Contains((int)x.Status))
                                                     .QueryAsync()).Select(x => x.LoanID).ToList();
                    if (lstLoanIDSearch == null || !lstLoanIDSearch.Any())
                    {
                        response.Message = MessageConstant.ErrorNotExist;
                        return response;
                    }
                    _assignmentLoanTab.WhereClause(x => lstLoanIDSearch.Contains(x.LoanID));
                }
                if (request.TypeID != (int)StatusCommon.SearchAll)
                    _assignmentLoanTab.WhereClause(x => x.Type == request.TypeID);

                DateTime currentDate = DateTime.Now;
                var firtDateOfMonthCurrent = new DateTime(currentDate.Year, currentDate.Month, 1);
                var firtDateOfNextMonth = firtDateOfMonthCurrent.AddMonths(1);
                firtDateOfMonthCurrent = firtDateOfMonthCurrent.AddSeconds(-1);

                var planHandleLoanDailyStatus = new List<int> {
                         (int)PlanHandleLoanDaily_Status.NoProcess,
                         (int)PlanHandleLoanDaily_Status.Processed
                     };

                var assignmentLoanTask = _assignmentLoanTab.WhereClause(x => x.UserID == request.EmployeeID && x.DateAssigned > firtDateOfMonthCurrent && x.DateAssigned < firtDateOfNextMonth && x.Status == (int)AssignmentLoan_Status.Processing)
                                                    .QueryAsync();
                var lstPlanHandleLoanDailyTask = _planHandleLoanDailyTab.WhereClause(x => x.UserID == request.EmployeeID
                                                                                        && planHandleLoanDailyStatus.Contains(x.Status)
                                                                                        && x.FromDate > firtDateOfMonthCurrent && x.FromDate < firtDateOfNextMonth)
                                                                        .QueryAsync();
                await Task.WhenAll(assignmentLoanTask, lstPlanHandleLoanDailyTask);
                Dictionary<long, ListLoanBeDividedItem> dictLoanAssignedInfos = new Dictionary<long, ListLoanBeDividedItem>();
                var dictPlanHandleLoanDailyInfos = lstPlanHandleLoanDailyTask.Result.GroupBy(x => x.LoanID).ToDictionary(x => x.Key, x => x.ToList());
                Dictionary<string, List<Domain.Tables.TblAssignmentLoan>> dictSortAssignmentLoanInfos = new Dictionary<string, List<Domain.Tables.TblAssignmentLoan>>();
                var lstLoanID = new List<long>();
                var assignmentLoan = assignmentLoanTask.Result;
                if (assignmentLoan.Any())
                {
                    // sắp xếp theo đơn chưa được chọn trả về trước
                    long minLoanID = long.MaxValue;
                    long maxLoanID = -1;
                    foreach (var item in assignmentLoan)
                    {
                        if (minLoanID > item.LoanID)
                        {
                            minLoanID = item.LoanID;
                        }
                        if (maxLoanID < item.LoanID)
                        {
                            maxLoanID = item.LoanID;
                        }
                        lstLoanID.Add(item.LoanID);
                    }
                    lstLoanID = lstLoanID.Distinct().ToList();
                    if (lstLoanID.Count > TimaSettingConstant.MaxItemQuery)
                    {

                        minLoanID -= 1;
                        maxLoanID += 1;
                        _loanTab.WhereClause(x => x.LoanID > minLoanID && x.LoanID < maxLoanID);
                    }
                    else
                    {
                        _loanTab.WhereClause(x => lstLoanID.Contains(x.LoanID));
                    }

                    var lstLoanInfos = await _loanTab.SelectColumns(x => x.LoanID, x => x.CustomerID, x => x.LoanCreditIDOfPartner, x => x.JsonExtra)
                                           .SelectColumns(x => x.ContactCode, x => x.CustomerName, x => x.StatusSendInsurance, x => x.NextDate, x => x.TotalMoneyCurrent)
                                           .QueryAsync();
                    var dictLoanInfos = lstLoanInfos.ToDictionary(x => x.LoanID, x => x);
                    foreach (var item in assignmentLoan)
                    {
                        // format key: có, không kế hoạch|thời gian dpd|loanID
                        var keyDictSort = "0";
                        var lstPlanHandleByLoan = dictPlanHandleLoanDailyInfos.GetValueOrDefault(item.LoanID);
                        if (lstPlanHandleByLoan != null)
                        {
                            var planDetail = lstPlanHandleByLoan.Where(x => x.ToDate >= DateTime.Now.Date).FirstOrDefault();
                            if (planDetail != null)
                                keyDictSort = "1";
                        }
                        var loanDetai = dictLoanInfos.GetValueOrDefault(item.LoanID);
                        if (loanDetai == null)
                        {
                            continue;
                        }
                        keyDictSort += $"{loanDetai.NextDate:yyyyMMdd}{item.LoanID}";
                        if (!dictSortAssignmentLoanInfos.ContainsKey(keyDictSort))
                        {
                            dictSortAssignmentLoanInfos.Add(keyDictSort, new List<TblAssignmentLoan>());
                        }
                        dictSortAssignmentLoanInfos[keyDictSort].Add(item);
                    }
                    dictSortAssignmentLoanInfos = dictSortAssignmentLoanInfos.OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value);
                    var lstKeyDictSort = dictSortAssignmentLoanInfos.Keys.Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();

                    List<Domain.Tables.TblAssignmentLoan> lstAssignmentLoanDataSort = new List<TblAssignmentLoan>();
                    foreach (var keyInDict in lstKeyDictSort)
                    {
                        lstAssignmentLoanDataSort.AddRange(dictSortAssignmentLoanInfos[keyInDict]);
                    }
                    lstLoanID.Clear();
                    var lstLoanLosID = new List<long?>();
                    var lstCustomerID = new List<long>();
                    foreach (var item in lstAssignmentLoanDataSort)
                    {
                        lstLoanID.Add(item.LoanID);
                        var loanDetail = dictLoanInfos.GetValueOrDefault(item.LoanID);
                        lstLoanLosID.Add(loanDetail.LoanCreditIDOfPartner);
                        lstCustomerID.Add((long)loanDetail.CustomerID);
                    }

                    // lấy comment mới nhất của mỗi đơn vay trong tháng
                    var dictCommentDebtPromptedTask = _reportManager.GetDictLoanCommentLatestInfosByLstLoanID(lstLoanID, firtDateOfMonthCurrent, firtDateOfNextMonth);
                    var lstPlanCloseLoanTask = _planCloseLoanTab.WhereClause(x => x.CloseDate == currentDate.Date && lstLoanID.Contains(x.LoanID)).QueryAsync();

                    await Task.WhenAll(dictCommentDebtPromptedTask, lstPlanCloseLoanTask);

                    var lstLoanLosResponseTask = _losService.GetVehicleInforOfLoan(lstLoanLosID);
                    var dictCustomerTask = _customerTab.WhereClause(x => lstCustomerID.Contains(x.CustomerID)).QueryAsync();//).ToDictionary(x => x.CustomerID, x => x);

                    await Task.WhenAll(lstLoanLosResponseTask, dictCustomerTask);

                    var dictCustomer = dictCustomerTask.Result.ToDictionary(x => x.CustomerID, x => x);
                    var dicLos = lstLoanLosResponseTask.Result?.ToDictionary(x => x.LoanBriefId, x => x) ?? new Dictionary<long, LMS.Entites.Dtos.LOSServices.LoanBriefPropertyLOS>();
                    var dictPlanCloseLoanInfos = lstPlanCloseLoanTask.Result.ToDictionary(x => x.LoanID, x => x);

                    var planHandleLoanDaily = dictPlanHandleLoanDailyInfos;
                    var dictCommentDebtPrompted = dictCommentDebtPromptedTask.Result.Where(x => RuleCollectionServiceHelper.AppTHNReason_PTP.Contains(x.Value.ReasonCode))
                                                                                      .ToDictionary(x => x.Key, x => x.Value.ReasonCode);
                    foreach (var item in lstAssignmentLoanDataSort)
                    {
                        var objLoan = dictLoanInfos.GetValueOrDefault(item.LoanID);
                        Domain.Tables.LoanJsonExtra loanJsonExtra = _common.ConvertJSonToObjectV2<Domain.Tables.LoanJsonExtra>(objLoan.JsonExtra);
                        var objCustomer = dictCustomer.GetValueOrDefault((long)objLoan.CustomerID);

                        int planLoanHandle = 0;
                        bool handling = false;
                        var lstlanHandleLoanDaily = dictPlanHandleLoanDailyInfos.GetValueOrDefault(item.LoanID);
                        if (lstlanHandleLoanDaily != null)
                        {
                            var planHandleLoanDailyDetail = lstlanHandleLoanDaily.OrderByDescending(x => x.ToDate).FirstOrDefault();

                            if (planHandleLoanDailyDetail != null)
                            {
                                if (planHandleLoanDailyDetail.ToDate < DateTime.Now.Date)
                                {
                                    planLoanHandle = 2;
                                }
                                else
                                {
                                    planLoanHandle = 1;
                                }
                            }
                            if (lstlanHandleLoanDaily.Any(x => x.Status == (int)PlanHandleLoanDaily_Status.Processed))
                                handling = true;
                        }

                        var loanLos = dicLos.GetValueOrDefault((long)objLoan.LoanCreditIDOfPartner);
                        var planCloseLoanDetail = dictPlanCloseLoanInfos.GetValueOrDefault(item.LoanID);
                        if (!dictLoanAssignedInfos.ContainsKey(item.LoanID))
                        {
                            dictLoanAssignedInfos.Add(item.LoanID, new ListLoanBeDividedItem
                            {
                                LoanId = item.LoanID,
                                ContractCode = objLoan?.ContactCode,
                                CustomerName = objLoan?.CustomerName,
                                TotalMoneyCurrent = objLoan.TotalMoneyCurrent,
                                OverdueDays = Convert.ToInt32(DateTime.Now.Date.Subtract(objLoan.NextDate.Value.Date).TotalDays),
                                Location = loanJsonExtra.IsLocate,
                                LoanBriefPropertyPlateNumber = loanJsonExtra.LoanBriefPropertyPlateNumber,
                                HouseholdAddress = objCustomer?.AddressHouseHold,
                                PresentAddress = objCustomer?.Address,
                                Handling = handling,
                                PlanLoanHandle = planLoanHandle,
                                AssignmentLoanType = item.Type,
                                LoanBriefPropertyBrandName = loanLos?.Brand,
                                LoanBriefPropertyProductName = loanLos?.Product,
                                ReasonCode = dictCommentDebtPrompted.GetValueOrDefault(item.LoanID),
                                LstAssignmentLoanType = new List<int>
                                {
                                    item.Type
                                },
                                TotalMoneyNeedPay = planCloseLoanDetail?.TotalMoneyNeedPay ?? -1, // hệ thống ko tính được tiền đầu ngày
                                LoanStatusInsuranceName = ((Loan_StatusSendInsurance)objLoan.StatusSendInsurance).GetDescription()
                            });
                        }
                        else
                        {
                            dictLoanAssignedInfos[item.LoanID].LstAssignmentLoanType.Add(item.Type);
                        }
                    }
                }
                response.Data = dictLoanAssignedInfos.Values.ToList();
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ListLoanBeDividedQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
