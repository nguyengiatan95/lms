﻿using CollectionServiceApi.Domain;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace CollectionServiceApi.Queries.HoldCustomerMoney
{
    public class ListHoldCustomerMoneyQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string CustomerName { get; set; }
        public string ContactCode { get; set; }
        public int Status { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class ListHoldCustomerMoneyQueryHandler : IRequestHandler<ListHoldCustomerMoneyQuery, ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblHoldCustomerMoney> _holdCustomerMoneyTab;
        readonly ILogger<ListHoldCustomerMoneyQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public ListHoldCustomerMoneyQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblHoldCustomerMoney> holdCustomerMoneyTab,
            ILogger<ListHoldCustomerMoneyQueryHandler> logger)
        {
            _loanTab = loanTab;
            _userTab = userTab;
            _customerTab = customerTab;
            _holdCustomerMoneyTab = holdCustomerMoneyTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(ListHoldCustomerMoneyQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var result = new List<ListHoldCustomerMoneyItem>();
                var lstLoan = new List<Domain.Tables.TblLoan>();
                var ckeckExcute = false;
                if (!string.IsNullOrEmpty(request.CustomerName))
                {
                    _loanTab.WhereClause(x => x.CustomerName == request.CustomerName);
                    ckeckExcute = true;
                }
                if (!string.IsNullOrEmpty(request.ContactCode))
                {
                    _loanTab.WhereClause(x => x.ContactCode == request.ContactCode);
                    ckeckExcute = true;
                }
                if (ckeckExcute)
                {
                    lstLoan = (await _loanTab.QueryAsync()).ToList();
                    if (lstLoan == null || lstLoan.Count() == 0)
                    {
                        response.SetSucces();
                        return response;
                    }
                }
                if (request.Status != (int)StatusCommon.Default)
                {
                    _holdCustomerMoneyTab.WhereClause(x => x.Status == request.Status);
                }
                if (lstLoan != null && lstLoan.Count > 0)
                {
                    _holdCustomerMoneyTab.WhereClause(x => lstLoan.Select(y => y.CustomerID).Contains(x.CustomerID));
                }
                var lstHoldCustomerMoney = await _holdCustomerMoneyTab.QueryAsync();
                int totalCount = lstHoldCustomerMoney.Count();
                if (totalCount > 0)
                {
                    lstHoldCustomerMoney = lstHoldCustomerMoney.OrderByDescending(x => x.HoldCustomerMoneyID)
                        .Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();
                    var lstCustomerID = new List<long>();
                    var lstUserIDCreate = new List<long>();
                    var lstUserIDDelete = new List<long>();
                    var lstUserIDAction = new List<long>();
                    foreach (var item in lstHoldCustomerMoney)
                    {
                        lstCustomerID.Add(item.CustomerID);
                        lstUserIDAction.Add(item.CreateBy);
                        if (item.Status == (int)HoldCustomerMoney_Status.Delete)
                        {
                            lstUserIDAction.Add(item.ModifyBy);
                        }
                    }
                    var lstLoanInfosTask = _loanTab.WhereClause(x => lstCustomerID.Contains((long)x.CustomerID)).QueryAsync();
                    var lstCustomerInfosTask = _customerTab.WhereClause(x => lstCustomerID.Contains((long)x.CustomerID)).QueryAsync();
                    var lstUserInfosTask = _userTab.WhereClause(x => lstUserIDAction.Contains((long)x.UserID)).QueryAsync();

                    await Task.WhenAll(lstLoanInfosTask, lstCustomerInfosTask, lstUserInfosTask);


                    var lstLoanInfo = lstLoanInfosTask.Result;
                    var dictCustomerInfo = lstCustomerInfosTask.Result.ToDictionary(x => x.CustomerID, x => x);
                    var dictUserInfosAction = lstUserInfosTask.Result.ToDictionary(x => x.UserID, x => x);
                    foreach (var item in lstHoldCustomerMoney)
                    {
                        if (lstLoanInfo.Where(x => x.CustomerID == item.CustomerID).Count() > 0)
                        {
                            var loanInfo = lstLoanInfo.FirstOrDefault(x => x.CustomerID == item.CustomerID);
                            var objCustomer = dictCustomerInfo.GetValueOrDefault(item.CustomerID);
                            var objUserNameCreate = dictUserInfosAction.GetValueOrDefault(item.CreateBy);
                            var objUserNameDelete = dictUserInfosAction.GetValueOrDefault(item.ModifyBy);
                            var statusLoan = string.Empty;
                            long moneyInWallet = 0;
                            if (loanInfo != null)
                            {
                                statusLoan = ((Loan_Status)loanInfo.Status).GetDescription();
                            }
                            if (objCustomer != null)
                                moneyInWallet = (long)objCustomer.TotalMoney;
                            result.Add(new ListHoldCustomerMoneyItem
                            {
                                HoldCustomerMoneyID = item.HoldCustomerMoneyID,
                                FullName = objCustomer?.FullName,
                                MoneyInWallet = moneyInWallet,
                                UserNameCreate = objUserNameCreate?.FullName,
                                UserNameDelete = objUserNameDelete?.FullName,
                                TotalMoney = item.Amount,
                                Reason = item?.Reason,
                                StrStatus = ((HoldCustomerMoney_Status)item.Status).GetDescription(),
                                Status = (short)item.Status,
                                StatusLoan = statusLoan,
                                CreateDate = item.CreateDate,
                                ContactCode = loanInfo?.ContactCode,
                                ListContractCode = lstLoanInfo.Where(x => x.CustomerID == item.CustomerID).Select(x => x.ContactCode).ToList()
                            });
                        }
                    }
                }
                response.SetSucces();
                response.Total = totalCount;
                response.Data = result;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ListHoldCustomerMoneyQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
