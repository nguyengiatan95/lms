﻿using CollectionServiceApi.Domain.Models.UserDepartment;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.UserDepartment
{
    public class GetStaffByLeaderQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserID { get; set; }
    }
    public class GetStaffByLeaderQueryHandler : IRequestHandler<GetStaffByLeaderQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> _userDepartmentTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> _departmentTab;
        ILogger<GetStaffByLeaderQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetStaffByLeaderQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDepartmentTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> departmentTab,
            ILogger<GetStaffByLeaderQueryHandler> logger)
        {
            _userDepartmentTab = userDepartmentTab;
            _userTab = userTab;
            _departmentTab = departmentTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetStaffByLeaderQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstResult = new List<UserDepartmentTHNByDepartment>();
                var lstUserDepartment = (await _userDepartmentTab.WhereClause(x => x.ParentUserID == request.UserID && x.Status == (int)UserDepartment_Status.Active).QueryAsync());
                if (lstUserDepartment != null)
                {
                    var lstUserID = new List<long>();
                    foreach (var item in lstUserDepartment)
                    {
                        lstUserID.Add(item.UserID);
                    }
                    var dictUserTask = _userTab.WhereClause(x => lstUserID.Contains(x.UserID) && x.Status == (int)User_Status.Active).QueryAsync();
                    var dictUser = dictUserTask.Result.ToDictionary(x => x.UserID, x => x);

                    foreach (var item in lstUserDepartment)
                    {
                        var objuser = dictUser.GetValueOrDefault(item.UserID);
                        if (objuser != null)
                        {
                            lstResult.Add(new UserDepartmentTHNByDepartment
                            {
                                ID = item.UserID,
                                Text = $"{objuser?.UserName}"
                            });
                        }
                    }
                }
                response.Data = lstResult;
                response.SetSucces();
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetStaffByLeaderQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
