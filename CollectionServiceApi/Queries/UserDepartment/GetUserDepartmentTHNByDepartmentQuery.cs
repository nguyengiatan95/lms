﻿using CollectionServiceApi.Domain.Models.UserDepartment;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.UserDepartment
{
    public class GetUserDepartmentTHNByDepartmentQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long DepartmentID { get; set; }
    }
    public class GetUserDepartmentTHNByDepartmentQueryHandler : IRequestHandler<GetUserDepartmentTHNByDepartmentQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> _userDepartmentTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<GetUserDepartmentTHNByDepartmentQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetUserDepartmentTHNByDepartmentQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDepartmentTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            ILogger<GetUserDepartmentTHNByDepartmentQueryHandler> logger)
        {
            _userDepartmentTab = userDepartmentTab;
            _userTab = userTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetUserDepartmentTHNByDepartmentQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstResult = new List<UserDepartmentTHNByDepartment>();
                var lstUserDepartment = await _userDepartmentTab.JoinOn<Domain.Tables.TblUser>(ud => ud.UserID, u => u.UserID)
                                                .WhereClause(x => x.DepartmentID == request.DepartmentID)
                                                .WhereClauseJoinOn<Domain.Tables.TblUser>(u => u.Status == (int)StatusCommon.Active)
                                                .QueryAsync();
                if (lstUserDepartment != null)
                {
                    lstResult = await ResultDataTree(lstUserDepartment.ToList());
                }
                response.Data = lstResult.OrderByDescending(x => x.PostionID).ToList();
                response.SetSucces();
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetUserDepartmentTHNByDepartmentQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task<List<UserDepartmentTHNByDepartment>> ResultDataTree(List<Domain.Tables.TblUserDepartment> lstData)
        {
            var result = new List<UserDepartmentTHNByDepartment>();
            var lstUserID = new List<long>();
            var dicParentUser = new Dictionary<long, List<Domain.Tables.TblUserDepartment>>();

            // lấy list userID
            var lstUserIds = new List<long>();
            // lấy list parentID
            var lstParentID = new List<long>();
            foreach (var item in lstData)
            {
                lstUserIds.Add(item.UserID);
                lstParentID.Add(item.ParentUserID);
            }

            var lstDontHave = new List<long>();
            foreach (var item in lstParentID)
            {
                if (!lstUserIds.Contains(item))
                    lstDontHave.Add(item);
            }
            foreach (var item in lstData)
            {
                if (lstDontHave.Contains(item.ParentUserID))
                    item.ParentUserID = 0;

                lstUserID.Add(item.UserID);
                if (item.ParentUserID > 0)// dict user có parentID
                {
                    if (!dicParentUser.ContainsKey(item.ParentUserID))
                    {
                        dicParentUser[item.ParentUserID] = new List<Domain.Tables.TblUserDepartment>();
                    }
                    dicParentUser[item.ParentUserID].Add(item);
                }
            }
            var dictUserInfo = (await _userTab.WhereClause(x => lstUserID.Contains(x.UserID) && x.Status == (int)StatusCommon.Active).QueryAsync()).ToDictionary(x => x.UserID, x => x);
            var lstUserDepartmentParent = lstData.Where(x => x.ParentUserID == 0).ToList();// lstUser Parent

            return GetTree(lstUserDepartmentParent, dictUserInfo, dicParentUser);
        }

        private List<UserDepartmentTHNByDepartment> GetTree(List<Domain.Tables.TblUserDepartment> lstUserDepartmentParent,
        Dictionary<long, Domain.Tables.TblUser> dicUser,
        Dictionary<long, List<Domain.Tables.TblUserDepartment>> dicParentId)
        {
            List<UserDepartmentTHNByDepartment> dataResult = new List<UserDepartmentTHNByDepartment>();
            foreach (var item in lstUserDepartmentParent)
            {
                var objUser = dicUser.GetValueOrDefault(item.UserID);
                if (objUser == null || objUser.UserID < 1)
                {
                    continue;
                }
                var objDataItem = new UserDepartmentTHNByDepartment
                {
                    ID = item.UserID,
                    Text = objUser?.FullName,
                    Open = true,
                    PostionID = item.PositionID,
                    Children = new List<UserDepartmentTHNByDepartment>()
                };
                //if (dicParentId.ContainsKey(item.UserID))//kiểm tra parent có childrent ko
                //{
                var childs = dicParentId.GetValueOrDefault(item.UserID);
                // var childs = dicParentId[item.UserID];
                if (childs != null && childs.Count > 0)//kiểm tra childrent có childrent ko
                {
                    objDataItem.Children = GetTree(childs, dicUser, dicParentId);
                }
                //}
                dataResult.Add(objDataItem);
            }
            return dataResult;
        }
    }
}
