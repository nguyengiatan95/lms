﻿using CollectionServiceApi.Domain.Models.UserDepartment;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.UserDepartment
{
    public class GetSummaryEmployeeHandleLoanByLstDeptIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public List<long> LstDepartmentID { get; set; }
    }
    public class GetSummaryEmployeeHandleLoanByLstDeptIDQueryHandler : IRequestHandler<GetSummaryEmployeeHandleLoanByLstDeptIDQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> _userDepartmentTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> _departmentTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> _planHandleLoanDailyTab;
        ILogger<GetSummaryEmployeeHandleLoanByLstDeptIDQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetSummaryEmployeeHandleLoanByLstDeptIDQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDepartmentTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> departmentTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> planHandleLoanDailyTab,
            ILogger<GetSummaryEmployeeHandleLoanByLstDeptIDQueryHandler> logger)
        {
            _userDepartmentTab = userDepartmentTab;
            _userTab = userTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _departmentTab = departmentTab;
            _planHandleLoanDailyTab = planHandleLoanDailyTab;
        }
        public async Task<ResponseActionResult> Handle(GetSummaryEmployeeHandleLoanByLstDeptIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                Dictionary<long, SummaryEmployeeDepartmentItem> dictDepartmentInfos = new Dictionary<long, SummaryEmployeeDepartmentItem>();
                foreach (var item in request.LstDepartmentID)
                {
                    if (!dictDepartmentInfos.ContainsKey(item))
                    {
                        dictDepartmentInfos.Add(item, new SummaryEmployeeDepartmentItem { DepartementID = item, TotalEmployees = 0, LstUserID = new List<long>() });
                    }
                }
                var lstDepartmentTask = _departmentTab.WhereClause(x => request.LstDepartmentID.Contains(x.DepartmentID) && x.AppID == (int)LMS.Common.Constants.Menu_AppID.THN)
                                                      .QueryAsync();

                // lấy tất cả nhân viên ở phòng thn
                _userDepartmentTab.SelectColumns(x => x.UserID, x => x.PositionID, x => x.ParentUserID, x => x.DepartmentID, x => x.ChildDepartmentID)
                      .JoinOn<Domain.Tables.TblUser>(ud => ud.UserID, u => u.UserID)
                      .SelectColumnsJoinOn<Domain.Tables.TblUser>(u => u.FullName, u => u.UserName)
                      .WhereClauseJoinOn<Domain.Tables.TblUser>(x => x.Status == (int)StatusCommon.Active)
                      .JoinOn<Domain.Tables.TblDepartment>(ud => ud.DepartmentID, d => d.DepartmentID)
                      .WhereClauseJoinOn<Domain.Tables.TblDepartment>(d => d.AppID == (int)Menu_AppID.THN);
                var lstUserDepartmentTask = _userDepartmentTab.QueryAsync<Domain.Models.EmployeeInfoModel>();

                await Task.WhenAll(lstDepartmentTask, lstUserDepartmentTask);

                var lstDepartment = lstDepartmentTask.Result.OrderBy(x => x.ParentID).ThenBy(x => x.DepartmentID).ToList();
                var lstUserDepartment = lstUserDepartmentTask.Result.ToList();
                List<long> lstUserSelected = new List<long>();
                foreach (var item in lstDepartment)
                {
                    if (item.ParentID == 0)
                    {
                        // cấp trưởng phòng ban
                        if (!dictDepartmentInfos.ContainsKey(item.DepartmentID))
                        {
                            continue;
                        }
                        dictDepartmentInfos[item.DepartmentID].LstUserID = lstUserDepartment.Where(x => x.DepartmentID == item.DepartmentID).Select(x => x.UserID).ToList();
                        dictDepartmentInfos[item.DepartmentID].TotalEmployees = dictDepartmentInfos[item.DepartmentID].LstUserID.Count;
                        lstUserSelected.AddRange(dictDepartmentInfos[item.DepartmentID].LstUserID);
                    }
                    else
                    {
                        // cấp trưởng nhóm
                        if (!dictDepartmentInfos.ContainsKey(item.DepartmentID))
                        {
                            continue;
                        }
                        // lấy danh sách trưởng nhóm
                        // nếu có 2 thì chỉ lấy 1 ng đầu tiên
                        // lỗi về mặt dữ liệu
                        var userDepartmentLeader = lstUserDepartment.Where(x => x.ChildDepartmentID == item.DepartmentID && x.PositionID == (int)UserDepartment_PositionID.TeamLead).FirstOrDefault();
                        if (userDepartmentLeader != null)
                        {
                            dictDepartmentInfos[item.DepartmentID].LstUserID = lstUserDepartment.Where(x => x.ParentUserID == userDepartmentLeader.UserID).Select(x => x.UserID).ToList();
                            dictDepartmentInfos[item.DepartmentID].TotalEmployees = dictDepartmentInfos[item.DepartmentID].LstUserID.Count;
                            lstUserSelected.AddRange(dictDepartmentInfos[item.DepartmentID].LstUserID);
                        }
                    }
                }
                // lấy kế hoạch chia đơn cho danh sách nhân viên tính từ đầu tháng đến hiện tại
                var currentDate = DateTime.Now;
                var firstDateOfMonth = new DateTime(currentDate.Year, currentDate.Month, 1);
                var todate = currentDate.Date.AddDays(1);
                List<int> LstPlanHandleLoanStatus = new List<int>
                {
                    (int)PlanHandleLoanDaily_Status.NoProcess,
                    (int)PlanHandleLoanDaily_Status.Processed
                };
                var dictUserHandleLoanInfos = new Dictionary<string, SummaryEmployeeHandleLoanDaily>();
                var lstPlanHandleLoanData = await _planHandleLoanDailyTab.SelectColumns(x => x.UserID, x => x.LoanID, x => x.Status)
                                                        .WhereClause(x => lstUserSelected.Contains((long)x.UserID) && LstPlanHandleLoanStatus.Contains(x.Status))
                                                        .WhereClause(x => x.FromDate > firstDateOfMonth && x.ToDate < todate).QueryAsync();
                foreach (var item in lstPlanHandleLoanData)
                {
                    var keyDict = $"{item.UserID}_{item.LoanID}";
                    if (!dictUserHandleLoanInfos.ContainsKey(keyDict))
                    {
                        dictUserHandleLoanInfos.Add(keyDict, new SummaryEmployeeHandleLoanDaily { UserID = (long)item.UserID, DictLoanHandleDaily = new Dictionary<long, long>() });
                    }
                    var userHandleDetail = dictUserHandleLoanInfos[keyDict];
                    if (!userHandleDetail.DictLoanHandleDaily.ContainsKey(item.LoanID))
                    {
                        userHandleDetail.DictLoanHandleDaily.Add(item.LoanID, item.LoanID);
                        userHandleDetail.TotalLoanHandle += 1;
                    }
                    if (item.Status == (int)PlanHandleLoanDaily_Status.Processed)
                    {
                        userHandleDetail.TotalLoanProcessed += 1;
                    }
                }
                // đếm số đơn được xử lý cho phòng ban
                foreach (var item in dictDepartmentInfos.Values)
                {
                    foreach (var userID in item.LstUserID)
                    {
                        var lstLoanUserHandle = dictUserHandleLoanInfos.Values.Where(x => x.UserID == userID);
                        if (lstLoanUserHandle != null && lstLoanUserHandle.Any())
                        {
                            foreach(var loan in lstLoanUserHandle)
                            {
                                item.TotalLoanHandle += loan.TotalLoanHandle;
                                item.TotalLoanProcessed += loan.TotalLoanProcessed;
                            }
                            
                        }
                    }
                    // xóa lst userid khi trả về client
                    item.LstUserID = null;
                }
                response.SetSucces();
                response.Data = dictDepartmentInfos.Values.ToList();
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetSummaryEmployeeHandleLoanByLstDeptIDQueryHandler|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
