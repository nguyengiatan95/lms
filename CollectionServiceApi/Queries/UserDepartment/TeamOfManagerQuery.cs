﻿using CollectionServiceApi.Domain.Models.UserDepartment;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.UserDepartment
{
    public class TeamOfManagerQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserID { get; set; }
        public int Postion { get; set; }
    }
    public class TeamOfManagerQueryHandler : IRequestHandler<TeamOfManagerQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> _userDepartmentTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> _departmentTab;
        ILogger<TeamOfManagerQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public TeamOfManagerQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDepartmentTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> departmentTab,
            ILogger<TeamOfManagerQueryHandler> logger)
        {
            _userDepartmentTab = userDepartmentTab;
            _userTab = userTab;
            _departmentTab = departmentTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(TeamOfManagerQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstResult = new List<UserDepartmentTHNByDepartment>();
                var lstUserDepartment = (await _userDepartmentTab.WhereClause(x => x.PositionID == request.Postion
                                                                             && x.ParentUserID == request.UserID
                                                                             && x.Status == (int)UserDepartment_Status.Active).QueryAsync()).ToList();
                if (lstUserDepartment != null)
                {
                    var lstUserID = new List<long>();
                    var lstDepartment = new List<long>();
                    foreach (var item in lstUserDepartment)
                    {
                        lstUserID.Add(item.UserID);
                        lstDepartment.Add(item.DepartmentID);
                    }
                    var dictUserTask = _userTab.WhereClause(x => lstUserID.Contains(x.UserID)).QueryAsync();
                    var dictDepartmentTask = _departmentTab.WhereClause(x => lstDepartment.Contains(x.DepartmentID)).QueryAsync();
                    await Task.WhenAll(dictUserTask, dictDepartmentTask);
                    var dictUser = dictUserTask.Result.ToDictionary(x => x.UserID, x => x);
                    var dictDepartment = dictDepartmentTask.Result.ToDictionary(x => x.DepartmentID, x => x);

                    foreach (var item in lstUserDepartment)
                    {
                        var objuser = dictUser.GetValueOrDefault(item.UserID);
                        var objDepartment = dictDepartment.GetValueOrDefault(item.DepartmentID);

                        lstResult.Add(new UserDepartmentTHNByDepartment
                        {
                            ID = item.UserID,
                            Text = $"{objDepartment?.DepartmentName}-{objuser?.UserName}",
                        });
                    }
                }
                response.Data = lstResult;
                response.SetSucces();
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"TeamOfManagerQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
