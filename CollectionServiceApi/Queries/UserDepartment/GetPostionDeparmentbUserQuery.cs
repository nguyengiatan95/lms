﻿using CollectionServiceApi.Domain.Models.UserDepartment;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.UserDepartment
{
    public class GetPostionDeparmentbUserQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserID { get; set; }
    }
    public class GetPostionDeparmentbUserQueryHandler : IRequestHandler<GetPostionDeparmentbUserQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> _userDepartmentTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> _departmentTab;
        ILogger<GetPostionDeparmentbUserQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetPostionDeparmentbUserQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDepartmentTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDepartment> departmentTab,
            ILogger<GetPostionDeparmentbUserQueryHandler> logger)
        {
            _userDepartmentTab = userDepartmentTab;
            _userTab = userTab;
            _departmentTab = departmentTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetPostionDeparmentbUserQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstResult = new PostionDeparmentbUser();
                var lstUserDepartment = (await _userDepartmentTab.WhereClause(x => x.UserID == request.UserID && x.Status == (int)UserDepartment_Status.Active).QueryAsync());
                if (lstUserDepartment.Count() > 0)
                {
                    var postion = lstUserDepartment.Max(x => x.PositionID);
                    if (postion == (int)UserDepartment_PositionID.Director)
                    {
                        lstUserDepartment = (await _userDepartmentTab.WhereClause(x =>
                                             x.PositionID == (int)UserDepartment_PositionID.Manager).QueryAsync());


                        var lstDepartmentID = (await _departmentTab.WhereClause(x => x.AppID == (int)Menu_AppID.THN).QueryAsync()).Select(x => x.DepartmentID).ToList();
                        var lstUser = (await _userDepartmentTab.WhereClause(x => lstDepartmentID.Contains(x.DepartmentID)).QueryAsync()).Select(x => x.UserID).ToList();

                        var lstUserInfor = (await _userTab.SelectColumns(x => x.UserID, x => x.UserName, x => x.FullName).WhereClause(x => lstUser.Contains(x.UserID) && x.Status == (int)User_Status.Active).QueryAsync());

                        foreach (var item in lstUserInfor)
                        {
                            lstResult.DataChidrend.Add(new PostionDeparmentbUserItem
                            {
                                ID = item.UserID,
                                Text = item.UserName
                            });
                        }
                    }
                    else
                    {
                        lstUserDepartment = lstUserDepartment.Where(x => x.PositionID == postion).ToList();
                    }
                    var lstUserID = new List<long>();
                    var lstDepartment = new List<long>();
                    foreach (var item in lstUserDepartment)
                    {
                        lstUserID.Add(item.UserID);
                        lstDepartment.Add(item.DepartmentID);
                    }
                    var dictUserTask = _userTab.WhereClause(x => lstUserID.Contains(x.UserID) && x.Status == (int)User_Status.Active).QueryAsync();
                    var dictDepartmentTask = _departmentTab.WhereClause(x => lstDepartment.Contains(x.DepartmentID) && x.Status == (int)Department_Status.Active).QueryAsync();
                    await Task.WhenAll(dictUserTask, dictDepartmentTask);
                    var dictUser = dictUserTask.Result.ToDictionary(x => x.UserID, x => x);
                    var dictDepartment = dictDepartmentTask.Result.ToDictionary(x => x.DepartmentID, x => x);

                    foreach (var item in lstUserDepartment)
                    {
                        var objuser = dictUser.GetValueOrDefault(item.UserID);
                        var objDepartment = dictDepartment.GetValueOrDefault(item.DepartmentID);
                        if (objuser != null)
                        {
                            lstResult.DataParent.Add(new PostionDeparmentbUserItem
                            {
                                ID = item.UserID,
                                Text = $"{objDepartment?.DepartmentName}-{objuser?.UserName}",
                                PostionID = postion
                            });
                        }
                    }
                }
                response.Data = lstResult;
                response.SetSucces();
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetPostionDeparmentbUserQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
