﻿using CollectionServiceApi.Domain.Models.LoanExemption;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.LoanExemption
{
    public class GetHistoryLoanExemptionByLoanExemptionIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanExemptionID { get; set; }
    }
    public class GetHistoryLoanExemptionByLoanExemptionIDQueryHandler : IRequestHandler<GetHistoryLoanExemptionByLoanExemptionIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLoanExemptionAction> _logLoanExemptionActionTab;
        ILogger<GetHistoryLoanExemptionByLoanExemptionIDQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetHistoryLoanExemptionByLoanExemptionIDQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLoanExemptionAction> logLoanExemptionActionTab,
            ILogger<GetHistoryLoanExemptionByLoanExemptionIDQueryHandler> logger)
        {
            _logLoanExemptionActionTab = logLoanExemptionActionTab;
            _logger = logger;
            _common = new Utils();
        }
        public async Task<ResponseActionResult> Handle(GetHistoryLoanExemptionByLoanExemptionIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            List<LogLoanExemptionActionModel> lstLoanItemView = new List<LogLoanExemptionActionModel>();
            try
            {
                response.SetSucces();
                response.Data = lstLoanItemView;
                var lstLoanExemptionData = await _logLoanExemptionActionTab.WhereClause(x => x.LoanExemptionID == request.LoanExemptionID).QueryAsync();
                if (lstLoanExemptionData == null || !lstLoanExemptionData.Any())
                {
                    return response;
                }
                foreach (var item in lstLoanExemptionData)
                {
                    lstLoanItemView.Add(new LogLoanExemptionActionModel
                    {
                        ActionType = item.ActionType,
                        ActionTypeName = ((LogLoanExemptionAction_ActionType)item.ActionType).GetDescription(),
                        CreateBy = item.CreateBy,
                        CreateByName = item.CreateByName,
                        CreateDate = item.CreateDate
                    });
                }
                response.SetSucces();
                response.Data = lstLoanItemView.OrderByDescending(x => x.CreateDate).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetHistoryLoanExemptionByLoanExemptionIDQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
