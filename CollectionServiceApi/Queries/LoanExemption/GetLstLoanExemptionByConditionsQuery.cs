﻿using CollectionServiceApi.Domain.Models.LoanExemption;
using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.LoanExemption
{
    public class GetLstLoanExemptionByConditionsQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int Status { get; set; }
        public string KeySearch { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public long UserID { get; set; } // thông tin user đang request view
        public GetLstLoanExemptionByConditionsQuery()
        {
            Status = (int)StatusCommon.SearchAll;
            PageIndex = 1;
            PageSize = 10;
        }

    }
    public class GetLstLoanExemptionByConditionsQueryHandler : IRequestHandler<GetLstLoanExemptionByConditionsQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> _approvalLevelBookDebtTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblBookDebt> _bookDebtTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemption> _loanExemptionTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblHistoryApprovalLevelBookHandleInMonth> _historyApprovalLevelBookTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingTotalMoney> _settingTotalMoneyTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserApprovalLevel> _userApprovalLevelTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebtBalance> _approvalLevelBookDebtBalanceTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLoanExemptionAction> _logLoanExemptionActionTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        readonly ILogger<GetLstLoanExemptionByConditionsQueryHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public GetLstLoanExemptionByConditionsQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemption> loanExemptionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> approvalLevelBookDebtTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBookDebt> bookDebtTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblHistoryApprovalLevelBookHandleInMonth> historyApprovalLevelBookTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingTotalMoney> settingTotalMoneyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserApprovalLevel> userApprovalLevelTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebtBalance> approvalLevelBookDebtBalanceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLoanExemptionAction> logLoanExemptionActionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            ILogger<GetLstLoanExemptionByConditionsQueryHandler> logger)
        {
            _loanExemptionTab = loanExemptionTab;
            _approvalLevelBookDebtTab = approvalLevelBookDebtTab;
            _bookDebtTab = bookDebtTab;
            _loanTab = loanTab;
            _customerTab = customerTab;
            _historyApprovalLevelBookTab = historyApprovalLevelBookTab;
            _settingTotalMoneyTab = settingTotalMoneyTab;
            _userApprovalLevelTab = userApprovalLevelTab;
            _approvalLevelBookDebtBalanceTab = approvalLevelBookDebtBalanceTab;
            _logLoanExemptionActionTab = logLoanExemptionActionTab;
            _logger = logger;
            _common = new Utils();
            _userTab = userTab;
        }
        public async Task<ResponseActionResult> Handle(GetLstLoanExemptionByConditionsQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            List<LoanExemptionViewModel> lstLoanItemView = new List<LoanExemptionViewModel>();
            List<long> lstLoanID = new List<long>();
            ResponseForDataTable dataTable = new ResponseForDataTable
            {
                RecordsTotal = 0,
                Data = lstLoanItemView,
                RecordsFiltered = 0,
                Draw = 1
            };
            response.SetSucces();
            response.Data = dataTable;

            var currentDate = DateTime.Now;
            var firstDateOfMonth = new DateTime(currentDate.Year, currentDate.Month, 1);
            var firtDateOfNextMonth = firstDateOfMonth.AddMonths(1);
            int yearMonthCurrent = int.Parse(currentDate.ToString("yyyyMM"));

            try
            {
                List<long> lstCustomerIDs = new List<long>();
                List<long> lstLevelApprovalOfUserCurrent = new List<long>();
                if (!string.IsNullOrEmpty(request.FromDate))
                {
                    var fromDate = DateTime.ParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, null).AddSeconds(1);
                    _loanExemptionTab.WhereClause(x => x.CreateDate > fromDate);
                    _logLoanExemptionActionTab.WhereClause(x => x.CreateDate > fromDate);
                }
                if (!string.IsNullOrEmpty(request.ToDate))
                {
                    var todate = DateTime.ParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, null).AddDays(1).Date;
                    _loanExemptionTab.WhereClause(x => x.CreateDate < todate);
                    _logLoanExemptionActionTab.WhereClause(x => x.CreateDate < todate);
                }
                if (!string.IsNullOrEmpty(request.KeySearch))
                {
                    if (long.TryParse(request.KeySearch, out long contracCodeID) && request.KeySearch.Length < TimaSettingConstant.MaxLengthPhone - 1)
                    {
                        request.KeySearch = $"{TimaSettingConstant.PrefixContractCode}{contracCodeID}";
                        _loanTab.WhereClause(x => x.ContactCode == request.KeySearch);
                    }
                    else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternContractCode, RegexOptions.IgnoreCase).Success)
                    {
                        _loanTab.WhereClause(x => x.ContactCode == request.KeySearch);
                    }
                    else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternContractCodeLOSLoanCreditID, RegexOptions.IgnoreCase).Success)
                    {
                        var loanCreditID = long.Parse(request.KeySearch.Replace(TimaSettingConstant.PatternContractCodeLOS, "", StringComparison.OrdinalIgnoreCase));
                        _loanTab.WhereClause(x => x.LoanCreditIDOfPartner == loanCreditID);
                    }
                    else
                    {
                        if (request.KeySearch.Length == TimaSettingConstant.MaxLengthPhone && Regex.Match(request.KeySearch, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success)
                        {
                            var lstCustomerIDsTask = await _customerTab.SelectColumns(x => x.CustomerID).WhereClause(x => x.Phone == request.KeySearch).QueryAsync();
                            lstCustomerIDs = lstCustomerIDsTask.Select(x => x.CustomerID).ToList();
                        }
                        else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternNumberCard, RegexOptions.IgnoreCase).Success)
                        {
                            var lstCustomerIDsTask = await _customerTab.SelectColumns(x => x.CustomerID).WhereClause(x => x.NumberCard == request.KeySearch).QueryAsync();
                            lstCustomerIDs = lstCustomerIDsTask.Select(x => x.CustomerID).ToList();
                        }
                        if (lstCustomerIDs != null && lstCustomerIDs.Count > 0)
                        {
                            _loanTab.WhereClause(x => lstCustomerIDs.Contains((long)x.CustomerID));
                        }
                        else
                        {
                            _loanTab.WhereClause(x => x.CustomerName.Contains(request.KeySearch));
                        }
                    }
                    var lstLoanData = await _loanTab.SetGetTop(1000).SelectColumns(x => x.LoanID).QueryAsync();
                    if (lstLoanData == null || !lstLoanData.Any())
                    {
                        return response;
                    }
                    lstLoanID = lstLoanData.Select(x => x.LoanID).ToList();
                    _loanExemptionTab.WhereClause(x => lstLoanID.Contains(x.LoanID));
                }
                if (request.Status != (int)StatusCommon.SearchAll)
                {
                    _loanExemptionTab.WhereClause(x => x.Status == request.Status);
                }
                if (request.UserID != LMS.Common.Constants.TimaSettingConstant.UserIDAdmin)
                {
                    // lấy cấp của user login
                    // danh sách đơn user đã thao tác - đã tính query ở đoạn check fromdate, todate
                    var lstLevelApprovalOfUserCurrentTask = _userApprovalLevelTab.SelectColumns(x => x.ApprovalLevelBookDebtID).WhereClause(x => x.UserID == request.UserID && x.Status == (int)StatusCommon.Active).QueryAsync();
                    var lstLoanExemptionIDUserActionTask = _logLoanExemptionActionTab.SelectColumns(x => x.LoanExemptionID).WhereClause(x => x.CreateBy == request.UserID).QueryAsync();

                    await Task.WhenAll(lstLevelApprovalOfUserCurrentTask, lstLoanExemptionIDUserActionTask);
                    var lstLoanExemptionIDUserAction = lstLoanExemptionIDUserActionTask.Result.Select(x => x.LoanExemptionID).Distinct().ToList();
                    lstLevelApprovalOfUserCurrent = lstLevelApprovalOfUserCurrentTask.Result.Select(x => (long)x.ApprovalLevelBookDebtID).ToList();
                    _loanExemptionTab.WhereClause(x => x.CreateBy == request.UserID  // ng tạo
                                                || lstLoanExemptionIDUserAction.Contains(x.LoanExemptionID)  // lịch sử đơn đã duyệt
                                                || (lstLevelApprovalOfUserCurrent.Contains(x.ApprovalLevelBookDebtID) && x.ApprovalLevelBookDebtID != 0)); // cùng cấp duyệt nhưng ko lấy cấp tạo
                }
                var lstLoanExemptionData = await _loanExemptionTab.QueryAsync();
                if (lstLoanExemptionData == null || !lstLoanExemptionData.Any())
                {
                    return response;
                }
                int totalRow = lstLoanExemptionData.Count();
                dataTable.RecordsTotal = totalRow;
                dataTable.RecordsFiltered = totalRow;
                // phân trang bản ghi
                lstLoanID.Clear();
                List<long> lstBookID = new List<long>();
                List<long> lstApprovalLevelID = new List<long>();
                List<long> lstLoanExemptionID = new List<long>();
                lstLoanExemptionData = lstLoanExemptionData.OrderByDescending(x => x.CreateDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToList();

                foreach (var item in lstLoanExemptionData)
                {
                    lstLoanID.Add(item.LoanID);
                    lstBookID.Add(item.BookDebtID);
                    lstApprovalLevelID.Add(item.ApprovalLevelBookDebtID);
                    lstLoanExemptionID.Add(item.LoanExemptionID);
                }

                lstBookID = lstBookID.Distinct().ToList();
                lstApprovalLevelID = lstApprovalLevelID.Distinct().ToList();

                var taskBookInfos = _bookDebtTab.WhereClause(x => lstBookID.Contains(x.BookDebtID)).QueryAsync();
                var taskApprovalLeverInfos = _approvalLevelBookDebtTab.WhereClause(x => lstApprovalLevelID.Contains(x.ApprovalLevelBookDebtID)).QueryAsync();
                var taskLoanInfos = _loanTab.WhereClause(x => lstLoanID.Contains(x.LoanID)).QueryAsync();
                var taskSettingTotalMoneyApproval = _settingTotalMoneyTab.WhereClause(x => lstApprovalLevelID.Contains(x.ApprovalLevelBookDebtID)).QueryAsync();

                var taskApprovalLevelBookDebBalanceInMonth = _approvalLevelBookDebtBalanceTab.WhereClause(x => lstApprovalLevelID.Contains(x.ApprovalLevelBookDebtID) && x.YearMonthCurrent == yearMonthCurrent).QueryAsync();
                var taskUserApprovalLevelInfos = _userApprovalLevelTab.WhereClause(x => lstApprovalLevelID.Contains((long)x.ApprovalLevelBookDebtID) && x.Status == (int)ApprovalLevelBookDebt_Status.Active).QueryAsync();

                await Task.WhenAll(taskBookInfos, taskApprovalLeverInfos, taskLoanInfos, taskSettingTotalMoneyApproval, taskApprovalLevelBookDebBalanceInMonth, taskUserApprovalLevelInfos);

                var dictBookInfos = taskBookInfos.Result.ToDictionary(x => x.BookDebtID, x => x);
                var dictApprovalLevelInfos = taskApprovalLeverInfos.Result.ToDictionary(x => x.ApprovalLevelBookDebtID, x => x);
                var dictLoanInfos = taskLoanInfos.Result.ToDictionary(x => x.LoanID, x => x);
                var dictApprovalLevelBookDebBalanceInMonthHandle = taskApprovalLevelBookDebBalanceInMonth.Result.ToDictionary(x => x.ApprovalLevelBookDebtID, x => x);
                var dictSettingTotalMoney = taskSettingTotalMoneyApproval.Result.ToDictionary(x => x.ApprovalLevelBookDebtID, x => x);
                var dictUserApprovalLevelInfos = new Dictionary<long, List<TblUserApprovalLevel>>();
                var lstUserID = new List<long>();
                foreach (var item in taskUserApprovalLevelInfos.Result)
                {
                    if (!dictUserApprovalLevelInfos.ContainsKey((long)item.ApprovalLevelBookDebtID))
                    {
                        dictUserApprovalLevelInfos.Add((long)item.ApprovalLevelBookDebtID, new List<TblUserApprovalLevel>());
                    }
                    dictUserApprovalLevelInfos[(long)item.ApprovalLevelBookDebtID].Add(item);
                    lstUserID.Add(item.UserID);
                }
                var dictUserInfos = (await _userTab.SelectColumns(x => x.UserID, x => x.UserName, x => x.FullName)
                                                  .WhereClause(x => lstUserID.Contains(x.UserID))
                                                  .QueryAsync())
                                                  .ToDictionary(x => x.UserID, x => x);
                List<int> lstStatusNotShowNote = new List<int>
                {
                    (int)LoanExemption_Status.Closed,
                    (int)LoanExemption_Status.ClosedFail,
                    (int)LoanExemption_Status.Cancel,
                };
                foreach (var item in lstLoanExemptionData)
                {
                    string note = "";
                    LoanExemptionExtraData loanExemptionExtraDataChange = _common.ConvertJSonToObjectV2<LoanExemptionExtraData>(item.ExtraData);
                    var userHadAction = loanExemptionExtraDataChange.LstUserChange.FirstOrDefault(x => x.UserID == request.UserID);

                    var loanDetail = dictLoanInfos.GetValueOrDefault(item.LoanID);
                    var approvalLevelBookDebtDetail = dictApprovalLevelInfos.GetValueOrDefault(item.ApprovalLevelBookDebtID);
                    // cấp phê duyệt đồng thời
                    if (!lstStatusNotShowNote.Contains(item.Status) && approvalLevelBookDebtDetail != null && approvalLevelBookDebtDetail.IsAllAccept == 1)
                    {
                        var lstUserAtSameLevel = dictUserApprovalLevelInfos.GetValueOrDefault(item.ApprovalLevelBookDebtID);
                        foreach (var u in lstUserAtSameLevel)
                        {
                            var userNotAction = loanExemptionExtraDataChange.LstUserChange.FirstOrDefault(x => x.UserID == u.UserID);
                            if (userNotAction == null || userNotAction.UserID < 1)
                            {
                                note += $"@{dictUserInfos.GetValueOrDefault(u.UserID)?.UserName},";
                            }
                        }
                        if (note.Length > 0)
                        {
                            note = $"Chờ {note.Remove(note.Length - 1)} duyệt";
                        }
                    }
                    var approvalLevelBalance = dictApprovalLevelBookDebBalanceInMonthHandle.GetValueOrDefault(item.ApprovalLevelBookDebtID);
                    if (approvalLevelBalance == null || approvalLevelBalance.ApprovalLevelBookDebtID < 1)
                    {
                        var settingApprovalDetail = dictSettingTotalMoney.GetValueOrDefault(item.ApprovalLevelBookDebtID);
                        approvalLevelBalance = new Domain.Tables.TblApprovalLevelBookDebtBalance
                        {
                            TotalMoneyCurrent = settingApprovalDetail?.TotalMoney ?? 0,
                            TotalMoneySetting = settingApprovalDetail?.TotalMoney ?? 0

                        };
                    }
                    long moneySuggetExmption = item.MoneyConsultant + item.MoneyFineLate + item.MoneyFineOriginal + item.MoneyInterest + item.MoneyOriginal + item.MoneyService;
                    moneySuggetExmption -= (item.PayMoneyConsultant + item.PayMoneyFineLate + item.PayMoneyFineOriginal + item.PayMoneyInterest + item.PayMoneyOriginal + item.PayMoneyService);
                    int hasAction = 0;
                    // user ở nhiều cấp phê duyệt
                    // vị trí đã duyệt ở cấp trước thì ko duyệt ở cấp sau
                    if (request.UserID == LMS.Common.Constants.TimaSettingConstant.UserIDAdmin
                        ||
                        (lstLevelApprovalOfUserCurrent.Contains(item.ApprovalLevelBookDebtID)
                        &&
                        (userHadAction == null || userHadAction.UserID < 1)
                        ))
                    {
                        hasAction = 1;
                    }
                    lstLoanItemView.Add(new LoanExemptionViewModel
                    {
                        ApprovalLevelBookDebtName = approvalLevelBookDebtDetail?.ApprovalLevelBookDebtName ?? "",
                        ApprovedByFullName = item.ApproveByFullName,
                        BookDebtName = dictBookInfos.GetValueOrDefault(item.BookDebtID)?.BookDebtName,
                        CreateByFullName = item.CreateByFullName,
                        CustomerName = loanDetail.CustomerName,
                        CreateDate = item.CreateDate,
                        LoanID = item.LoanID,
                        LoanContractCode = loanDetail.ContactCode,
                        LoanExemptionID = item.LoanExemptionID,
                        ModifyDate = item.ModifyDate,
                        LoanExemptionStatusName = ((LoanExemption_Status)item.Status).GetDescription(),
                        LoanExemptionStatus = item.Status,
                        ReasonCancel = item.ReasonCancel,
                        MaxMoneyApprovalLevel = approvalLevelBalance.TotalMoneySetting,
                        MoneyApprovalLevelSuccess = approvalLevelBalance.TotalMoneySetting - approvalLevelBalance.TotalMoneyCurrent,
                        MoneyApprovalLevelCurrent = approvalLevelBalance.TotalMoneyCurrent,
                        MoneySuggetExemption = moneySuggetExmption,
                        ExpirationDate = item.ExpirationDate,
                        HasAction = hasAction,
                        CustomerID = item.CustomerID,
                        Note = note
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLstLoanExemptionByConditionsQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
