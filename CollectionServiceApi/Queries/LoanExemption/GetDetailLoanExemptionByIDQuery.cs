﻿using CollectionServiceApi.Domain.Models.LoanExemption;
using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.LoanExemption
{
    public class GetDetailLoanExemptionByIDQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanExemptionID { get; set; }
    }
    public class GetDetailLoanExemptionByIDQueryHandler : IRequestHandler<GetDetailLoanExemptionByIDQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> _approvalLevelBookDebtTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBookDebt> _bookDebtTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemption> _loanExemptionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemptionFileUpload> _loanExemptionFileUploadTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> _excelReportTab;
        ILogger<GetDetailLoanExemptionByIDQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetDetailLoanExemptionByIDQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemption> loanExemptionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> approvalLevelBookDebtTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBookDebt> bookDebtTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemptionFileUpload> loanExemptionFileUploadTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> excelReportTab,
            ILogger<GetDetailLoanExemptionByIDQueryHandler> logger)
        {
            _loanExemptionTab = loanExemptionTab;
            _approvalLevelBookDebtTab = approvalLevelBookDebtTab;
            _bookDebtTab = bookDebtTab;
            _loanTab = loanTab;
            _loanExemptionFileUploadTab = loanExemptionFileUploadTab;
            _excelReportTab = excelReportTab;
            _logger = logger;
            _common = new Utils();
        }
        public async Task<ResponseActionResult> Handle(GetDetailLoanExemptionByIDQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            List<LoanExemptionViewModel> lstLoanItemView = new List<LoanExemptionViewModel>();
            List<long> lstLoanID = new List<long>();
            try
            {
                var lstLoanExemptionData = await _loanExemptionTab.SetGetTop(1).WhereClause(x => x.LoanExemptionID == request.LoanExemptionID).QueryAsync();
                if (lstLoanExemptionData == null || !lstLoanExemptionData.Any())
                {
                    return response;
                }
                var loanExempDetail = lstLoanExemptionData.FirstOrDefault();
                LoanExemptionExtraData loanExemptionExtraData = _common.ConvertJSonToObjectV2<LoanExemptionExtraData>(loanExempDetail.ExtraData);
                var taskBookInfos = _bookDebtTab.WhereClause(x => x.BookDebtID == loanExempDetail.BookDebtID).QueryAsync();
                var taskApprovalLeverInfos = _approvalLevelBookDebtTab.WhereClause(x => x.ApprovalLevelBookDebtID == loanExempDetail.ApprovalLevelBookDebtID).QueryAsync();
                var taskLoanInfos = _loanTab.WhereClause(x => x.LoanID == loanExempDetail.LoanID).QueryAsync();
                var taskFileUploads = _loanExemptionFileUploadTab.WhereClause(x => x.LoanExemptionID == loanExempDetail.LoanExemptionID && x.Status == (int)StatusCommon.Active).QueryAsync();
                var taskFormLoanExemption = _excelReportTab.SetGetTop(1).WhereClause(x => x.TraceIDRequest == loanExemptionExtraData.ReferTraceIDRequest).QueryAsync();
                await Task.WhenAll(taskBookInfos, taskApprovalLeverInfos, taskLoanInfos, taskFileUploads, taskFormLoanExemption);
                var dictBookInfos = taskBookInfos.Result.ToDictionary(x => x.BookDebtID, x => x);
                var dictApprovalLevelInfos = taskApprovalLeverInfos.Result.ToDictionary(x => x.ApprovalLevelBookDebtID, x => x);
                var dictLoanInfos = taskLoanInfos.Result.ToDictionary(x => x.LoanID, x => x);

                var loanDetail = dictLoanInfos.GetValueOrDefault(loanExempDetail.LoanID);
                LoanExemptionViewModel detailResponse = new LoanExemptionViewModel
                {
                    ApprovalLevelBookDebtName = dictApprovalLevelInfos.GetValueOrDefault(loanExempDetail.ApprovalLevelBookDebtID)?.ApprovalLevelBookDebtName,
                    ApprovedByFullName = loanExempDetail.ApproveByFullName,
                    BookDebtName = dictBookInfos.GetValueOrDefault(loanExempDetail.BookDebtID)?.BookDebtName,
                    CreateByFullName = loanExempDetail.CreateByFullName,
                    CustomerName = loanDetail.CustomerName,
                    CustomerID = loanExempDetail.CustomerID,
                    CreateDate = loanExempDetail.CreateDate,
                    LoanID = loanExempDetail.LoanID,
                    LoanContractCode = loanDetail.ContactCode,
                    LoanExemptionID = loanExempDetail.LoanExemptionID,
                    ModifyDate = loanExempDetail.ModifyDate,
                    LoanExemptionStatusName = ((LoanExemption_Status)loanExempDetail.Status).GetDescription(),
                    LoanExemptionStatus = loanExempDetail.Status,
                    ReasonCancel = loanExempDetail.ReasonCancel,
                    FileUploads = new List<UploadFileLoanExemptionModel>(),
                    FormDetail = new LMS.Entites.Dtos.CollectionServices.ReportExcel.ReportFormLoanExemptionInterestDetail()
                };
                if (taskFileUploads.Result != null && taskFileUploads.Result.Any())
                {
                    foreach (var item in taskFileUploads.Result)
                    {
                        detailResponse.FileUploads.Add(new UploadFileLoanExemptionModel
                        {
                            TypeFile = item.TypeFile,
                            FileImages = _common.ConvertJSonToObjectV2<List<LMS.Entites.Dtos.AG.ImageLosUpload>>(item.LstImg)
                        });
                    }
                }
                if (taskFormLoanExemption.Result != null && taskFormLoanExemption.Result.Any())
                {
                    var formDetail = taskFormLoanExemption.Result.FirstOrDefault();
                    detailResponse.FormDetail = _common.ConvertJSonToObjectV2<LMS.Entites.Dtos.CollectionServices.ReportExcel.ReportFormLoanExemptionInterestDetail>(formDetail.ExtraData);
                }
                response.SetSucces();
                response.Data = detailResponse;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetLstLoanExemptionByConditionsQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
