﻿using CollectionServiceApi.Domain;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Queries.PreparationCollectionLoan
{
    public class GetPreparationCollectionLoanQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
    }
    public class GetPreparationCollectionLoanQueryHandler : IRequestHandler<GetPreparationCollectionLoanQuery, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPreparationCollectionLoan> _preparationCollectionLoanTab;
        ILogger<GetPreparationCollectionLoanQueryHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public GetPreparationCollectionLoanQueryHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPreparationCollectionLoan> preparationCollectionLoanTab,
            ILogger<GetPreparationCollectionLoanQueryHandler> logger)
        {
            _preparationCollectionLoanTab = preparationCollectionLoanTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetPreparationCollectionLoanQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                return await Task.Run(() =>
                {
                    var lstData = new List<GetPreparationCollectionLoanItem>();
                    var lstPreparationCollectionLoan = _preparationCollectionLoanTab.WhereClause(x => x.LoanID == request.LoanID).Query().OrderByDescending(x => x.PreparationCollectionLoanID).ToList();
                    if (lstPreparationCollectionLoan != null)
                    {
                        foreach (var item in lstPreparationCollectionLoan)
                        {
                            lstData.Add(new GetPreparationCollectionLoanItem
                            {
                                CreateDate = item.CreateDate,
                                PreparationDate = item.PreparationDate,
                                FullName = item.FullName,
                                TotalMoney = (long)item.TotalMoney,
                                Description = item.Description,
                                StrStatus = ((PreparationCollectionLoan_Status)item.Status).GetDescription(),
                                PrepareTypeName = Enum.IsDefined(typeof(PreparationCollectionLoan_PrepareType), item.PreparationType) ?
                                ((PreparationCollectionLoan_PrepareType)item.PreparationType).GetDescription() : "",
                            });
                        }
                    }
                    response.SetSucces();
                    response.Data = lstData;
                    return response;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetPreparationCollectionLoanQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
