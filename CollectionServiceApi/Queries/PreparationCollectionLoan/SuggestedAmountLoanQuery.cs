﻿using CollectionServiceApi.Constants;
using CollectionServiceApi.Domain;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace CollectionServiceApi.Queries.PreparationCollectionLoan
{
    public class SuggestedAmountLoanQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
    }
    public class SuggestedAmountLoanQueryHandler : IRequestHandler<SuggestedAmountLoanQuery, ResponseActionResult>
    {
        ILogger<SuggestedAmountLoanQueryHandler> _logger;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        LMS.Common.Helper.Utils _common;
        public SuggestedAmountLoanQueryHandler(
         LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
         LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
         ILogger<SuggestedAmountLoanQueryHandler> logger)
        {
            _loanTab = loanTab;
            _paymentScheduleTab = paymentScheduleTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(SuggestedAmountLoanQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                return await Task.Run(() =>
                {
                    var lstData = new List<SuggestedAmountLoanItem>();
                    var objLoan = _loanTab.WhereClause(x => x.LoanID == request.LoanID).Query().FirstOrDefault();
                    if (objLoan == null)
                    {
                        response.Message = MessageConstant.LoanNotExist;
                        return response;
                    }
                    var lstpaymentSchedule = _paymentScheduleTab.WhereClause(x => x.LoanID == request.LoanID && x.Status == (int)PaymentSchedule_Status.Use).Query();
                    if (lstpaymentSchedule != null && lstpaymentSchedule.Count() > 0)
                    {
                        long outStandingBalance = objLoan.TotalMoneyDisbursement;
                        int i = 0;
                        long totalMoneyNeedPayment = 0;
                        int totalCount = lstpaymentSchedule.Count();
                        foreach (var item in lstpaymentSchedule)
                        {
                            outStandingBalance -= item.MoneyOriginal;
                            long totalMoneyInterestOfCustomer = item.MoneyInterest + item.MoneyService + item.MoneyConsultant;
                            totalMoneyNeedPayment += item.MoneyOriginal + totalMoneyInterestOfCustomer;
                            if (item.IsComplete != (int)PaymentSchedule_IsComplete.Paid)
                            {
                                i++;
                                lstData.Add(new SuggestedAmountLoanItem
                                {
                                    Value = i,
                                    Text = i + " Kỳ",
                                    TotalMoney = totalMoneyNeedPayment
                                });
                            }
                            if (i == 1)// tính tiền tất toán 
                            {
                                long moneyFinePayEarly = Convert.ToInt64(Math.Round(outStandingBalance * CollectionConstants.FeeAdvancePayment, 0));
                                long totalMoneyNeedClosePayment = Convert.ToInt64(outStandingBalance + totalMoneyInterestOfCustomer + moneyFinePayEarly);
                                lstData.Add(new SuggestedAmountLoanItem
                                {
                                    Value = totalCount + i,
                                    Text = "Tất toán",
                                    TotalMoney = totalMoneyNeedClosePayment
                                });
                            }
                        }
                    }
                    response.SetSucces();
                    response.Data = lstData.OrderBy(x => x.Value).ToList();
                    return response;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"SuggestedAmountLoanQueryHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
