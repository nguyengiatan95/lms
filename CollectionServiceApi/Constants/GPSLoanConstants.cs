﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Constants
{
    public class GPSLoanConstants
    {
        public const int FromDateAddDay = 1;
        public const int FromDateAddHours7 = 7;
        public const int FromDateAddHours18 = 18;
        public const int Countminutes = 60;
    }
}
