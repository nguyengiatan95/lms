﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Constants
{
    public class CollectionConstants
    {
        public const double FeeAdvancePayment = 0.08;
        public const string DateTimeDDMMYYYYHHMM = "dd/MM/yyyy HH:mm";
        public static string LOS_KEY = "lms@#123";
    }
}
