﻿using LMS.Kafka.Interfaces;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi
{
    public class KafkaServiceConsumer : BackgroundService
    {
        readonly IKafkaConsumer<string, LMS.Kafka.Messages.Customer.CustomerBalanceToupSuccessInfo> _consumCustomerTopupSuccesHandler;
        readonly IKafkaConsumer<string, LMS.Kafka.Messages.Loan.LoanCutPeriodSuccessInfo> _consumLoanCutPeriodsHandler;
        readonly ILogger<KafkaServiceConsumer> _logger;
        public KafkaServiceConsumer(
            IKafkaConsumer<string, LMS.Kafka.Messages.Loan.LoanCutPeriodSuccessInfo> consumLoanCutPeriodsHandler,
            IKafkaConsumer<string, LMS.Kafka.Messages.Customer.CustomerBalanceToupSuccessInfo> consumCustomerTopupSuccesHandler,
            ILogger<KafkaServiceConsumer> logger)
        {
            _consumCustomerTopupSuccesHandler = consumCustomerTopupSuccesHandler;
            _logger = logger;
            _consumLoanCutPeriodsHandler = consumLoanCutPeriodsHandler;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                var t1 = _consumCustomerTopupSuccesHandler.Consume(LMS.Kafka.Constants.KafkaTopics.CustomerBalanceTopupSuccessInfo, stoppingToken);
                var t2 = _consumLoanCutPeriodsHandler.Consume(LMS.Kafka.Constants.KafkaTopics.LoanCutPeriodSuccess, stoppingToken);
                await Task.WhenAll(t1, t2);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CollectionServiceApi|{(int)HttpStatusCode.InternalServerError}|ConsumeFailedOnTopic|{ex}");
            }
        }

        public override void Dispose()
        {
            _logger.LogError($"KafkaServiceConsumer_CollectionServiceApi_Dispose");
            _consumCustomerTopupSuccesHandler.Dispose();
            _consumLoanCutPeriodsHandler.Dispose();

            base.Dispose();
        }
    }
}
