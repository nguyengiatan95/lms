﻿using CollectionServiceApi.Commands.PreparationCollectionLoan;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Validators
{
    public class CreatePreparationCollectionLoanValidator : AbstractValidator<CreatePreparationCollectionLoanCommand>
    {
        public CreatePreparationCollectionLoanValidator() 
        {
           // RuleFor(x => x.LoanID).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.NotIdentifiedLoanID);
            RuleFor(x => x.TotalMoney).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.TotalMoneyNull);
           // RuleFor(x => x.CreateBy).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.NotIdentifiedUserID);
            RuleFor(x => x.PreparationDate).NotNull().WithMessage(LMS.Common.Constants.MessageConstant.PreparationDateNull);
        }
    }
}
