﻿using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace CollectionServiceApi.Commands.PlanHandleLoanDaily
{
    public class DeletePlanHandleLoanDailyCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserID { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
    public class DeletePlanHandleLoanDailyCommandHandler : IRequestHandler<DeletePlanHandleLoanDailyCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> _planHandleLoanDailyTab;
        ILogger<DeletePlanHandleLoanDailyCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public DeletePlanHandleLoanDailyCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> planHandleLoanDailyTab,
            ILogger<DeletePlanHandleLoanDailyCommandHandler> logger)
        {
            _planHandleLoanDailyTab = planHandleLoanDailyTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();

        }
        public async Task<ResponseActionResult> Handle(DeletePlanHandleLoanDailyCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var fromDate = DateTime.ParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture);
                var planHandleLoanDaily = (await _planHandleLoanDailyTab.WhereClause(x => x.UserID == request.UserID &&
                                            x.FromDate == fromDate && x.ToDate == toDate).QueryAsync());
                if (planHandleLoanDaily != null)
                {
                  //v  checkProcessed = planHandleLoanDaily.Where(x => x.Status == (int)PlanHandleLoanDaily_Status.Processed).ToList();
                    if (planHandleLoanDaily.Any(x => x.Status == (int)PlanHandleLoanDaily_Status.Processed))
                    {
                        response.Message = MessageConstant.NotDeletePlanHandleLoanDaily;
                        return response;
                    }
                    planHandleLoanDaily = planHandleLoanDaily.Select(x =>
                    {
                        x.Status = (int)PlanHandleLoanDaily_Status.Delete;
                        x.ModifyDate = DateTime.Now;
                        x.ModifyBy = request.UserID;
                        return x;
                    }).ToList();

                    _planHandleLoanDailyTab.UpdateBulk(planHandleLoanDaily);
                    response.SetSucces();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"DeletePlanHandleLoanDailyCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
