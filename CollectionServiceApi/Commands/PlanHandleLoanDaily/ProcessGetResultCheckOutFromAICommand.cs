﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.PlanHandleLoanDaily
{
    public class ProcessGetResultCheckOutFromAICommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserID { get; set; }
        public string ActionType { get; set; } //CHKIN / CKOUT
        public int LoanCreditID { get; set; } // id bên los
        public string CodeAppCheckIn { get; set; } // mã code app gen khi checkin
    }
    public class ProcessGetResultCheckOutFromAICommandHandler : IRequestHandler<ProcessGetResultCheckOutFromAICommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> _planHandleLoanDailyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> _commentDebtPromptedTab;
        ILogger<ProcessGetResultCheckOutFromAICommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        RestClients.IAIService _aiService;

        public ProcessGetResultCheckOutFromAICommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> planHandleLoanDailyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> commentDebtPromptedTab,
            RestClients.IAIService aiService,
            ILogger<ProcessGetResultCheckOutFromAICommandHandler> logger)
        {
            _planHandleLoanDailyTab = planHandleLoanDailyTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _loanTab = loanTab;
            _aiService = aiService;
            _commentDebtPromptedTab = commentDebtPromptedTab;
        }
        public async Task<ResponseActionResult> Handle(ProcessGetResultCheckOutFromAICommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                switch (request.ActionType.ToUpper())
                {
                    case RuleCollectionServiceHelper.AppActionTypeCheckOUT:
                        var loanDetail = (await _loanTab.SetGetTop(1).SelectColumns(x => x.LoanID).WhereClause(x => x.LoanCreditIDOfPartner == request.LoanCreditID).QueryAsync()).FirstOrDefault();
                        if (loanDetail == null)
                        {
                            response.Message = MessageConstant.LoanNotExist;
                            return response;
                        }
                        List<int> lstStatus = new List<int>
                        {
                            (int)PlanHandleLoanDaily_Status.NoProcess,
                            (int)PlanHandleLoanDaily_Status.Processed
                        };
                        var planExists = (await _planHandleLoanDailyTab.SetGetTop(1).WhereClause(x => x.UserID == request.UserID && x.LoanID == loanDetail.LoanID && x.CodeCheckIn == request.CodeAppCheckIn)
                                                                .QueryAsync()).FirstOrDefault();
                        if (planExists == null)
                        {
                            response.Message = MessageConstant.PlanHandleLoanNotFoundDetailLoan;
                            return response;
                        }
                        // đã checkin thành công ko ghi nhận
                        if (planExists.StatusCheckIn == (int)PlanHandleLoanDaily_StatusCheckIn.Success)
                        {
                            response.SetSucces();
                            // kiểm tra lại xem đã có comment chưa
                            _ = CheckHasCommentWhenCheckOut(planExists.UserID.Value, planExists.LoanID, planExists);
                            return response;
                        }
                        planExists.ModifyBy = request.UserID;
                        planExists.ModifyDate = DateTime.Now;
                        planExists.StatusCheckIn = (int)PlanHandleLoanDaily_StatusCheckIn.Processed;
                        var lstResultCheckIN = await _aiService.GetListCheckInOut(request.CodeAppCheckIn);
                        if (lstResultCheckIN != null && lstResultCheckIN.Any())
                        {
                            // trong 1 ngày chỉ cần tồn tại 1
                            var resultCheckInPass = lstResultCheckIN.FirstOrDefault(x => x.Uid == request.UserID && x.Contractid == request.LoanCreditID && x.Pass == true);
                            if (resultCheckInPass != null)
                            {
                                planExists.StatusCheckIn = (int)PlanHandleLoanDaily_StatusCheckIn.Success;
                                planExists.TypeHandle = (int)PlanHandleLoanDaily_TypeHandle.CommentAndCheckIn;
                            }
                        }
                        await _planHandleLoanDailyTab.UpdateAsync(planExists);
                        if (planExists.StatusCheckIn == (int)PlanHandleLoanDaily_StatusCheckIn.Success)
                        {
                            _ = CheckHasCommentWhenCheckOut(planExists.UserID.Value, planExists.LoanID, planExists);
                        }
                        response.SetSucces();
                        break;
                    default:
                        response.Message = MessageConstant.PlanHandleLoanNotFoundDetailLoan;
                        return response;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CheckinCheckoutCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task CheckHasCommentWhenCheckOut(long userID, long loanID, Domain.Tables.TblPlanHandleLoanDaily detail)
        {
            try
            {

                // lấy comment theo user và loanid trong ngày
                var currentDate = DateTime.Now;
                var nextDate = currentDate.Date.AddDays(1);
                var hasCommentInDay = await _commentDebtPromptedTab.SetGetTop(1).SelectColumns(x => x.LoanID)
                                                             .WhereClause(x => x.UserID == userID && x.LoanID == loanID && x.CreateDate > currentDate.Date && x.CreateDate < nextDate)
                                                             .QueryAsync();
                if (hasCommentInDay == null || !hasCommentInDay.Any())
                {
                    return;
                }
                if (detail.Status == (int)PlanHandleLoanDaily_Status.Processed)
                {
                    return;
                }
                detail.Status = (int)PlanHandleLoanDaily_Status.Processed;
                detail.ModifyBy = userID;
                detail.ModifyDate = DateTime.Now;
                _ = await _planHandleLoanDailyTab.UpdateAsync(detail);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CheckHasCommentWhenCheckOut|userID={userID}|loanid={loanID}|ex={ex.Message}-{ex.StackTrace}");
            }
        }
    }
}
