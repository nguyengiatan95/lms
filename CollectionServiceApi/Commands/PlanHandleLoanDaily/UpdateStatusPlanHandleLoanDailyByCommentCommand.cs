﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.PlanHandleLoanDaily
{
    public class UpdateStatusPlanHandleLoanDailyByCommentCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserID { get; set; }
        public long LoanID { get; set; }
        public string CommentDate { get; set; }
    }
    public class UpdateStatusPlanHandleLoanDailyByCommentCommandHandler : IRequestHandler<UpdateStatusPlanHandleLoanDailyByCommentCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> _planHandleLoanDailyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> _commentDebtPromptedTab;
        ILogger<UpdateStatusPlanHandleLoanDailyByCommentCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdateStatusPlanHandleLoanDailyByCommentCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> planHandleLoanDailyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> commentDebtPromptedTab,
            ILogger<UpdateStatusPlanHandleLoanDailyByCommentCommandHandler> logger)
        {
            _planHandleLoanDailyTab = planHandleLoanDailyTab;
            _commentDebtPromptedTab = commentDebtPromptedTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();

        }
        public async Task<ResponseActionResult> Handle(UpdateStatusPlanHandleLoanDailyByCommentCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                DateTime commentDate = DateTime.Now;
                if (!string.IsNullOrEmpty(request.CommentDate))
                    commentDate = DateTime.ParseExact(request.CommentDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                // tạm thời luôn trả về thành công
                response.SetSucces();
                var planHandleLoanDailyDetail = (await _planHandleLoanDailyTab.SetGetTop(1)
                                                                           .WhereClause(x => x.LoanID == request.LoanID && x.UserID == request.UserID && x.FromDate <= commentDate.Date && x.ToDate >= commentDate.Date)
                                                                           .WhereClause(x => x.Status == (int)PlanHandleLoanDaily_Status.NoProcess)
                                                                           .QueryAsync()).FirstOrDefault();
                if (planHandleLoanDailyDetail == null)
                {
                    response.Message = MessageConstant.ErrorNotExist;
                    return response;
                }
                if (planHandleLoanDailyDetail.StatusCheckIn != (int)PlanHandleLoanDaily_StatusCheckIn.Success)
                {
                    return response;
                }
                planHandleLoanDailyDetail.TypeHandle = (int)PlanHandleLoanDaily_TypeHandle.CommentAndCheckIn;
                planHandleLoanDailyDetail.Status = (int)PlanHandleLoanDaily_Status.Processed;
                planHandleLoanDailyDetail.ModifyBy = request.UserID;
                planHandleLoanDailyDetail.ModifyDate = DateTime.Now;
                _ = await _planHandleLoanDailyTab.UpdateAsync(planHandleLoanDailyDetail);
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateStatusPlanHandleLoanDailyByCommentCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                response.Result = (int)ResponseAction.Error;
            }
            return response;
        }
    }
}
