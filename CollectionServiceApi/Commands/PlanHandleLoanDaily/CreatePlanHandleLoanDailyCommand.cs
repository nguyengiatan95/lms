﻿using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace CollectionServiceApi.Commands.PlanHandleLoanDaily
{
    public class CreatePlanHandleLoanDailyCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserID { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int LoanID { get; set; }
    }
    public class CreatePlanHandleLoanDailyCommandHandler : IRequestHandler<CreatePlanHandleLoanDailyCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> _planHandleLoanDailyTab;
        ILogger<CreatePlanHandleLoanDailyCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreatePlanHandleLoanDailyCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> planHandleLoanDailyTab,
            ILogger<CreatePlanHandleLoanDailyCommandHandler> logger)
        {
            _planHandleLoanDailyTab = planHandleLoanDailyTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();

        }
        public async Task<ResponseActionResult> Handle(CreatePlanHandleLoanDailyCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var fromDate = DateTime.ParseExact(request.FromDate, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(request.ToDate, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture);
                List<TblPlanHandleLoanDaily> lstInsert = new List<TblPlanHandleLoanDaily>();
                while (fromDate <= toDate)
                {
                    lstInsert.Add(new TblPlanHandleLoanDaily()
                    {
                        LoanID = request.LoanID,
                        UserID = request.UserID,
                        FromDate = fromDate,
                        ToDate = fromDate,
                        CreateDate = DateTime.Now,
                        ModifyDate = DateTime.Now,
                        ModifyBy = request.UserID,
                        Status = (int)PlanHandleLoanDaily_Status.NoProcess,
                        TypeHandle = (int)PlanHandleLoanDaily_TypeHandle.NoProcess,
                        CodeCheckIn = null,
                        LatLngCheckIn = null,
                        LatLngCheckOut = null,
                        LocationName = null,
                        StatusCheckIn = (int)PlanHandleLoanDaily_StatusCheckIn.NoProcess,
                        TimeCheckin = null,
                        TimeCheckOut = null

                    });
                    fromDate = fromDate.AddDays(1);
                }
                _planHandleLoanDailyTab.InsertBulk(lstInsert);
                response.SetSucces();
                response.Data = 1;
                await Task.Delay(1);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreatePlanHandleLoanDailyCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
