﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.PlanHandleLoanDaily
{
    public class UpdateStatusByLoanCutPeriodCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public DateTime NextDate { get; set; }
        public DateTime LastDateOfPay { get; set; }
    }
    public class UpdateStatusByLoanCutPeriodCommandHandler : IRequestHandler<UpdateStatusByLoanCutPeriodCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> _planHandleLoanDailyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCutOffLoan> _cutOffLoanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        ILogger<UpdateStatusByLoanCutPeriodCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdateStatusByLoanCutPeriodCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> planHandleLoanDailyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCutOffLoan> cutOffLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            ILogger<UpdateStatusByLoanCutPeriodCommandHandler> logger)
        {
            _planHandleLoanDailyTab = planHandleLoanDailyTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _cutOffLoanTab = cutOffLoanTab;
            _loanTab = loanTab;
        }
        public async Task<ResponseActionResult> Handle(UpdateStatusByLoanCutPeriodCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                // lấy thông tin đơn vay ghi nhận gần nhất
                var currentDate = DateTime.Now;
                var loanCutOffDetailTask = _cutOffLoanTab.SetGetTop(1)
                                                .WhereClause(x => x.CutOffDate <= currentDate.Date && x.LoanID == request.LoanID)
                                                .OrderByDescending(x => x.CutOffDate)
                                                .QueryAsync();
                var lstPlanHandleDailyTask = _planHandleLoanDailyTab.WhereClause(x => x.FromDate <= currentDate.Date && x.ToDate >= currentDate.Date)
                                                                    .WhereClause(x => x.LoanID == request.LoanID && x.Status == (int)PlanHandleLoanDaily_Status.NoProcess)
                                                                    .QueryAsync();
                await Task.WhenAll(loanCutOffDetailTask, lstPlanHandleDailyTask);
                var loanCutOffDetail = loanCutOffDetailTask.Result.FirstOrDefault();
                bool hasCutPeriod = false;
                // nếu ko có trong quá khứ, kiểm tran nextdate > tháng hiện tại thì tính cắt kỳ
                // case này lỗi dữ liệu
                if (loanCutOffDetail == null || loanCutOffDetail.LoanID < 0)
                {
                    _logger.LogError($"UpdateStatusByLoanCutPeriodCommandHandler_not_found_loan_cutoff|loanID={request.LoanID}");
                    // kiểm tra đơn vay mới được giải ngân ko
                    var loanDetail = (await _loanTab.SetGetTop(1).WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                    if (loanDetail.LastDateOfPay.HasValue && loanDetail.LastDateOfPay.Value > loanDetail.FromDate)
                    {
                        hasCutPeriod = true;
                    }
                }
                else
                {
                    if (loanCutOffDetail.LastDateOfPay < request.LastDateOfPay)
                    {
                        hasCutPeriod = true;
                    }
                }
                var lstPlanHandleDaily = lstPlanHandleDailyTask.Result;
                if (hasCutPeriod && lstPlanHandleDaily != null && lstPlanHandleDaily.Any())
                {
                    foreach (var item in lstPlanHandleDaily)
                    {
                        item.Status = (int)PlanHandleLoanDaily_Status.Processed;
                        item.TypeHandle = (int)PlanHandleLoanDaily_TypeHandle.LoanCutPeriod;
                        item.ModifyBy = TimaSettingConstant.UserIDAdmin;
                        item.ModifyDate = currentDate;
                    }
                    _planHandleLoanDailyTab.UpdateBulk(lstPlanHandleDaily);
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateStatusPlanHandleLoanDailyByCommentCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                response.Result = (int)ResponseAction.Error;
            }
            return response;
        }
    }
}
