﻿using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace CollectionServiceApi.Commands.PlanHandleLoanDaily
{
    public class UpdateStatusPlanHandleLoanDailyCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserID { get; set; }
        public List<long> LstLoanID { get; set; }
    }
    public class UpdateStatusPlanHandleLoanDailyCommandHandler : IRequestHandler<UpdateStatusPlanHandleLoanDailyCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> _planHandleLoanDailyTab;
        ILogger<UpdateStatusPlanHandleLoanDailyCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdateStatusPlanHandleLoanDailyCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> planHandleLoanDailyTab,
            ILogger<UpdateStatusPlanHandleLoanDailyCommandHandler> logger)
        {
            _planHandleLoanDailyTab = planHandleLoanDailyTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();

        }
        public async Task<ResponseActionResult> Handle(UpdateStatusPlanHandleLoanDailyCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstPlanHandleLoanDaily = (await _planHandleLoanDailyTab.WhereClause(x => request.LstLoanID.Contains(x.LoanID)).QueryAsync());
                if (!lstPlanHandleLoanDaily.Any())
                {
                    response.Message = MessageConstant.ErrorNotExist;
                    return response;
                }
                lstPlanHandleLoanDaily = lstPlanHandleLoanDaily.Select(x => 
                { x.Status = (int)PlanHandleLoanDaily_Status.Moved;
                  x.ModifyDate = DateTime.Now;
                  x.ModifyBy = request.UserID;
                 return x; }).ToList();

                _planHandleLoanDailyTab.UpdateBulk(lstPlanHandleLoanDaily);
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateStatusPlanHandleLoanDailyCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
