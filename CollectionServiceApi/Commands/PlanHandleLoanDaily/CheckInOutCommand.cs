﻿using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.PlanHandleLoanDaily
{
    public class CheckInOutCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserID { get; set; }
        public string ActionType { get; set; } //CHKIN / CKOUT
        public string Location { get; set; } // office / home / other
        public int LoanCreditID { get; set; } // id bên los
        public double Lng { get; set; }
        public double Lat { get; set; }
        public long TimeUnixAction { get; set; } // milisecond
        public string CodeAppCheckIn { get; set; } // mã code app gen khi checkin
    }
    public class CheckInOutCommandHandler : IRequestHandler<CheckInOutCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> _planHandleLoanDailyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCodeUserLogin> _codeUserLoginTab;
        ILogger<CheckInOutCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CheckInOutCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> planHandleLoanDailyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCodeUserLogin> codeUserLoginTab,
            ILogger<CheckInOutCommandHandler> logger)
        {
            _planHandleLoanDailyTab = planHandleLoanDailyTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _loanTab = loanTab;
            _codeUserLoginTab = codeUserLoginTab;
        }
        public async Task<ResponseActionResult> Handle(CheckInOutCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var currentDate = DateTime.Now;
                var codeUserLoginTask = _codeUserLoginTab.SetGetTop(1).WhereClause(x => x.UserID == request.UserID && x.CreateDate > currentDate.Date && x.CreateDate < currentDate).QueryAsync();
                var loanDetailTask = _loanTab.SetGetTop(1).SelectColumns(x => x.LoanID).WhereClause(x => x.LoanCreditIDOfPartner == request.LoanCreditID).QueryAsync();
                await Task.WhenAll(codeUserLoginTask, loanDetailTask);
                var loanDetail = loanDetailTask.Result.FirstOrDefault();
                var codeUserLoginDetail = codeUserLoginTask.Result.FirstOrDefault();
                if (loanDetail == null)
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                List<int> lstStatus = new List<int>
                {
                    (int)PlanHandleLoanDaily_Status.NoProcess,
                    (int)PlanHandleLoanDaily_Status.Processed
                };
                var planExists = (await _planHandleLoanDailyTab.SetGetTop(1).WhereClause(x => x.UserID == request.UserID && x.LoanID == loanDetail.LoanID && x.FromDate <= currentDate.Date && x.ToDate >= currentDate.Date)
                                                        .WhereClause(x => lstStatus.Contains(x.Status))
                                                        .OrderBy(x => x.PlanHandleLoanDailyID)
                                                        .QueryAsync()).FirstOrDefault();
                if (planExists == null)
                {
                    response.Message = MessageConstant.PlanHandleLoanNotFoundDetailLoan;
                    return response;
                }
                planExists.ModifyBy = request.UserID;
                planExists.ModifyDate = currentDate;
                planExists.LocationName = request.Location;
                switch (request.ActionType.ToUpper())
                {
                    case RuleCollectionServiceHelper.AppActionTypeCheckIN:
                        planExists.TimeCheckin = _common.ConvertTimeUnixToDatetime(request.TimeUnixAction);
                        planExists.LatLngCheckIn = $"{request.Lat};{request.Lng}";
                        planExists.TimeCheckOut = null;
                        if (planExists.StatusCheckIn != (int)PlanHandleLoanDaily_StatusCheckIn.Success)
                        {
                            planExists.StatusCheckIn = (int)PlanHandleLoanDaily_StatusCheckIn.NoProcess;
                        }
                        planExists.LatLngCheckOut = null;
                        planExists.CodeCheckIn = codeUserLoginDetail.CodeGUI; // lms sẽ gen 1 ngày 1 lần
                        break;
                    case RuleCollectionServiceHelper.AppActionTypeCheckOUT:
                        planExists.TimeCheckOut = _common.ConvertTimeUnixToDatetime(request.TimeUnixAction);
                        if (planExists.TimeCheckOut.Value.Subtract(planExists.TimeCheckin.Value).TotalMinutes < 5)
                        {
                            response.Message = MessageConstant.PlanHandleLoanMinTimeCheckOut;
                            return response;
                        }
                        planExists.LatLngCheckOut = $"{request.Lat};{request.Lng}";
                        break;
                    default:
                        response.Message = MessageConstant.PlanHandleLoanNotFoundDetailLoan;
                        return response;
                }
                await _planHandleLoanDailyTab.UpdateAsync(planExists);
                response.SetSucces();
                response.Data = codeUserLoginDetail.CodeGUI;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CheckinCheckoutCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
