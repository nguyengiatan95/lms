﻿using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace CollectionServiceApi.Commands.PlanHandleLoanDaily
{
    public class UpdatePlanHandleLoanDailyCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserID { get; set; }
        public int PlanHandleLoanDailyID { get; set; }
        public int Status { get; set; }
    }
    public class UpdatePlanHandleLoanDailyCommandHandler : IRequestHandler<UpdatePlanHandleLoanDailyCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> _planHandleLoanDailyTab;
        ILogger<UpdatePlanHandleLoanDailyCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdatePlanHandleLoanDailyCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleLoanDaily> planHandleLoanDailyTab,
            ILogger<UpdatePlanHandleLoanDailyCommandHandler> logger)
        {
            _planHandleLoanDailyTab = planHandleLoanDailyTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();

        }
        public async Task<ResponseActionResult> Handle(UpdatePlanHandleLoanDailyCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objPlanHandleLoanDaily = (await _planHandleLoanDailyTab.WhereClause(x => x.PlanHandleLoanDailyID == request.PlanHandleLoanDailyID).QueryAsync()).FirstOrDefault();
                if (objPlanHandleLoanDaily == null)
                {
                    response.Message = MessageConstant.ErrorNotExist;
                    return response;
                }
                if (request.Status == (int)PlanHandleLoanDaily_Status.Delete &&
                    objPlanHandleLoanDaily.Status == (int)PlanHandleLoanDaily_Status.Processed)
                {
                    response.Message = MessageConstant.NotDeletePlanHandleLoanDaily;
                    return response;
                }
                objPlanHandleLoanDaily.ModifyDate = DateTime.Now;
                objPlanHandleLoanDaily.ModifyBy = request.UserID;
                objPlanHandleLoanDaily.Status = request.Status;
                var update = await _planHandleLoanDailyTab.UpdateAsync(objPlanHandleLoanDaily);
                if (update)
                {
                    response.SetSucces();
                    response.Data = objPlanHandleLoanDaily.PlanHandleLoanDailyID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdatePlanHandleLoanDailyCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
