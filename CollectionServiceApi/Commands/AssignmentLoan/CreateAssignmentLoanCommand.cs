﻿using CollectionServiceApi.Domain.Models.AssignmentLoan;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.AssignmentLoan
{
    public class CreateAssignmentLoanCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public List<DivideLoanExcel> LstUpload { get; set; }
        public long CreateBy { get; set; }
    }
    public class CreateAssignmentLoanCommandHandler : IRequestHandler<CreateAssignmentLoanCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> _assignmentLoan;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loan;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        ILogger<CreateAssignmentLoanCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateAssignmentLoanCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> assignmentLoan,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loan,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            ILogger<CreateAssignmentLoanCommandHandler> logger
        )
        {
            _assignmentLoan = assignmentLoan;
            _loan = loan;
            _userTab = userTab;
            _lenderTab = lenderTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateAssignmentLoanCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                List<Domain.Tables.TblAssignmentLoan> lstAssign = new List<Domain.Tables.TblAssignmentLoan>();

                var totalCount = request.LstUpload.Count();
                if (totalCount > 0)
                {
                    var date = DateTime.Now;
                    var lastDayMonth = date.AddMonths(1);
                    lastDayMonth = lastDayMonth.AddDays(-(lastDayMonth.Day));// ngày cuối cùng của tháng
                    var lastDateOfLastMonth = new DateTime(date.Year, date.Month, 1);
                    List<DivideLoanExcel> lstNewUpload = new List<DivideLoanExcel>();
                    foreach (var item in request.LstUpload)
                    {
                        if (item.UserName == null) item.UserName = "";
                        string[] usernames = item.UserName.Split(',');
                        for (int i = 0; i < usernames.Length; i++)
                        {
                            lstNewUpload.Add(new DivideLoanExcel()
                            {
                                UserName = usernames[i],
                                LoanIDAG = item.LoanIDAG,
                                Type = item.Type
                            });
                        }
                    }
                    var lstType = new List<int>
                    {
                       (int) AssignmentLoan_Type.DebtLoan,(int) AssignmentLoan_Type.MotoSeizure,(int) AssignmentLoan_Type.AllDebtMoto,
                    };
                    var lstUserName = lstNewUpload.Select(x => x.UserName.Trim()).Distinct().ToList();
                    var lstUsers = await _userTab.WhereClause(x => lstUserName.Contains(x.UserName) && x.Status == (int)User_Status.Active).SelectColumns(x => x.UserName, x => x.UserID).QueryAsync();
                    var dicUser = lstUsers.ToDictionary(x => x.UserName.ToLower(), x => x.UserID);
                    foreach (var item in lstType)
                    {
                        int typeAll = (int)AssignmentLoan_Type.AllDebtMoto;
                        var lstUpLoadByType = lstNewUpload.Where(x => x.Type == item).ToList();
                        int maxSize = TimaSettingConstant.MaxItemQuery;
                        int numberRow = lstUpLoadByType.Count() / maxSize + 1;
                        for (int i = 1; i <= numberRow; i++)
                        {
                            var lstLoanSkip = lstUpLoadByType.Skip((i - 1) * maxSize).Take(maxSize).ToList().ToList();
                            var lstIds = lstLoanSkip.Select(x => x.LoanIDAG);
                            var loans = await _loan.SelectColumns(x => x.LoanID, x => x.TimaLoanID).WhereClause(x => lstIds.Contains(x.TimaLoanID)).QueryAsync();
                            var dicLoan = loans.ToDictionary(x => x.TimaLoanID, x => x.LoanID);
                            var loanLms = loans.Select(x => x.LoanID).ToList();
                            // cap nhat du lieu cu
                            var lstAssignLoan = _assignmentLoan.WhereClause(x => loanLms.Contains(x.LoanID) && (x.Type == item || x.Type == typeAll) && x.CreateDate > lastDateOfLastMonth && x.CreateDate <= lastDayMonth).Query();
                            foreach (var child in lstAssignLoan)
                            {
                                child.Status = (int)StatusCommon.UnActive;
                            }
                            _assignmentLoan.UpdateBulk(lstAssignLoan);
                            foreach (var assign in lstLoanSkip)
                            {
                                if (dicLoan.ContainsKey(assign.LoanIDAG) && dicUser.ContainsKey(assign.UserName.Trim().ToLower()))
                                {
                                    lstAssign.Add(new Domain.Tables.TblAssignmentLoan()
                                    {
                                        LoanID = dicLoan[assign.LoanIDAG],
                                        Type = assign.Type,
                                        UserID = dicUser[assign.UserName.Trim().ToLower()],
                                        CreateBy = request.CreateBy,
                                        CreateDate = date,
                                        DateAssigned = lastDateOfLastMonth,
                                        DateApplyTo = lastDayMonth,
                                        ModifyDate = date,
                                        Status = (int)StatusCommon.Active
                                    });
                                }
                            }
                        }
                    }
                    _assignmentLoan.InsertBulk(lstAssign);
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateAssignmentLoanCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
