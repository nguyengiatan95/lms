﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using CollectionServiceApi.Domain.Tables;
using System.Globalization;
using CollectionServiceApi.Services;

namespace CollectionServiceApi.Commands.PreparationCollectionLoan
{
    public class CreatePreparationCollectionLoanCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public long LoanID { get; set; }

        public int TypeID { get; set; }

        public string Description { get; set; }

        public int UserID { get; set; }

        public string FullName { get; set; }
        [Required]
        public long TotalMoney { get; set; }
        [Required]
        public long CreateBy { get; set; }
        [Required]
        public string PreparationDate { get; set; }

        public long TimaID { get; set; }

        public long LoanAGID { get; set; }
        public int PreparationType { get; set; }

    }
    public class CreatePreparationCollectionLoanCommandHandler : IRequestHandler<CreatePreparationCollectionLoanCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPreparationCollectionLoan> _preparationCollectionLoanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<CreatePreparationCollectionLoanCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        IQueueCallAPIManager _queueCallAPIManager;
        public CreatePreparationCollectionLoanCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPreparationCollectionLoan> preparationCollectionLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            IQueueCallAPIManager queueCallAPIManager,
            ILogger<CreatePreparationCollectionLoanCommandHandler> logger
            )
        {
            _paymentScheduleTab = paymentScheduleTab;
            _preparationCollectionLoanTab = preparationCollectionLoanTab;
            _loanTab = loanTab;
            _queueCallAPIManager = queueCallAPIManager;
            _userTab = userTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreatePreparationCollectionLoanCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                long loanID = request.LoanID;
                Domain.Tables.TblLoan objLoan;
                DateTime dtNow = DateTime.Now;
                DateTime preparationDate = dtNow;
                if (request.TimaID > 0)
                {
                    var timaIDExist = (await _preparationCollectionLoanTab.WhereClause(x => x.TimaPreparationCollectionLoanID == request.TimaID).QueryAsync()).FirstOrDefault();
                    if (timaIDExist != null)
                    {
                        response.Message = MessageConstant.TimaPreparationCollectionLoanIDExist;
                        _logger.LogError($"CreatePreparationCollectionLoanCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                        return response;
                    }
                    objLoan = (await _loanTab.WhereClause(x => x.TimaLoanID == request.LoanAGID).QueryAsync()).FirstOrDefault();
                    if (objLoan == null)
                    {
                        response.Message = MessageConstant.RequireLoanID;
                        _logger.LogError($"CreatePreparationCollectionLoanCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                        return response;
                    }
                    loanID = objLoan.LoanID;
                    // ag bắn qua ko có giờ
                    preparationDate = DateTime.ParseExact(request.PreparationDate, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture);
                }
                else
                {
                    objLoan = (await _loanTab.WhereClause(x => x.LoanID == loanID).QueryAsync()).FirstOrDefault();
                    if (objLoan == null)
                    {
                        response.Message = MessageConstant.RequireLoanID;
                        _logger.LogError($"CreatePreparationCollectionLoanCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                        return response;
                    }
                    preparationDate = DateTime.ParseExact(request.PreparationDate, _common.DateTimeDDMMYYYYHHMMSS, CultureInfo.InvariantCulture);
                }
                if (preparationDate.Date < DateTime.Now.Date || preparationDate.Month != DateTime.Now.Month || preparationDate.Year != DateTime.Now.Year)
                {
                    response.Message = MessageConstant.InvalidDueDate;
                    _logger.LogError($"CreatePreparationCollectionLoanCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                    return response;
                }

                if (request.TypeID != (int)NumberForecastPeriods.SelfEnteredAmount && request.TypeID != (int)NumberForecastPeriods.All)// check số kỳ
                {
                    var paymentSchedule = (await _paymentScheduleTab.WhereClause(x => x.LoanID == objLoan.LoanID &&
                                                        x.Status == (int)PaymentSchedule_Status.Use &&
                                                        x.IsComplete != (int)PaymentSchedule_IsComplete.Paid).QueryAsync());
                    if (paymentSchedule == null || paymentSchedule.Count() == 0)
                    {
                        response.Message = MessageConstant.NotIdentifiedPayment;
                        return response;
                    }
                    if (paymentSchedule != null && paymentSchedule.Count() < request.TypeID)//,số kì lớn hơn kì phải thanh toán
                    {
                        response.Message = MessageConstant.InvalidNumberPaymentPeriods;
                        return response;
                    }
                    request.TypeID = (int)PreparationCollectionLoan_TypeID.AmountPeriod;
                }
                var preparationCollectionLoan = new TblPreparationCollectionLoan
                {
                    LoanID = objLoan.LoanID,
                    TypeID = request.TypeID,
                    Description = request.Description,
                    UserID = request.UserID,
                    FullName = request.FullName,
                    Status = (int)PreparationCollectionLoan_Status.Yes,
                    TotalMoney = request.TotalMoney,
                    CreateBy = request.CreateBy,
                    CreateDate = dtNow,
                    PreparationDate = preparationDate,
                    TimaPreparationCollectionLoanID = request.TimaID,
                    PreparationType = request.PreparationType
                };
                var preparationCollectionLoanID = _preparationCollectionLoanTab.Insert(preparationCollectionLoan);
                if (preparationCollectionLoanID > 0)
                {
                    if (request.TimaID == 0)
                    {
                        var objUser = (await _userTab.WhereClause(x => x.UserID == request.CreateBy).QueryAsync()).FirstOrDefault();
                        _ = _queueCallAPIManager.InsertProvisionsCollection(objLoan.TimaLoanID, request.CreateBy
                              , objUser.UserName, dtNow, request.TotalMoney, request.TypeID, request.Description, preparationDate, preparationCollectionLoanID);
                    }
                    response.SetSucces();
                    response.Data = preparationCollectionLoanID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreatePreparationCollectionLoanCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
