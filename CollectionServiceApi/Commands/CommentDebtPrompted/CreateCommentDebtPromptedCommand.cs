﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using CollectionServiceApi.Domain.Tables;
using System.Globalization;
using CollectionServiceApi.Services;
using CollectionServiceApi.Domain.Models.CommentDebtPrompted;

namespace CollectionServiceApi.Commands.CommentDebtPrompted
{
    public class CreateCommentDebtPromptedCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public long UserID { get; set; }

        public string FullName { get; set; }
        public string Comment { get; set; }
        public long ReasonCodeID { get; set; }
        public string ReasonCode { get; set; }

        public long LoanAgID { get; set; }
        public List<LMS.Entites.Dtos.AG.ImageLosUpload> FileImage { get; set; }
        public long TimaID { get; set; }
        public string AppointmentDate { get; set; } // format dd/MM/yyyy HH:mm
        public short ActionPerson { get; set; }

        public short ActionAddress { get; set; }
    }
    public class CreateCommentDebtPromptedCommandHandler : IRequestHandler<CreateCommentDebtPromptedCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> _commentDebtPromptedTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblReason> _reasonTab;
        readonly ILogger<CreateCommentDebtPromptedCommandHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        readonly IQueueCallAPIManager _queueCallAPIManager;

        public CreateCommentDebtPromptedCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> commentDebtPromptedTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblReason> reasonTab,
            IQueueCallAPIManager queueCallAPIManager,
            ILogger<CreateCommentDebtPromptedCommandHandler> logger
            )
        {
            _commentDebtPromptedTab = commentDebtPromptedTab;
            _loanTab = loanTab;
            _userTab = userTab;
            _reasonTab = reasonTab;
            _logger = logger;
            _queueCallAPIManager = queueCallAPIManager;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateCommentDebtPromptedCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                long loanID = request.LoanID;
                Domain.Tables.TblLoan objLoan;
                Domain.Tables.TblReason objReason;
                if (request.TimaID > 0)
                {
                    var timaIDExits = (await _commentDebtPromptedTab.WhereClause(x => x.TimaCommentID == request.TimaID).QueryAsync()).FirstOrDefault();
                    if (timaIDExits != null)
                    {
                        response.Message = MessageConstant.TimaCommentIDExist;
                        return response;
                    }
                    objLoan = (await _loanTab.WhereClause(x => x.TimaLoanID == request.LoanAgID).QueryAsync()).FirstOrDefault();
                    if (objLoan == null)
                    {
                        response.Message = MessageConstant.RequireLoanID;
                        _logger.LogError($"CreateCommentDebtPromptedCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                        return response;
                    }
                    loanID = objLoan.LoanID;
                    if (string.IsNullOrEmpty(request.ReasonCode))
                    {
                        objReason = (await _reasonTab.WhereClause(x => x.ReasonID == request.ReasonCodeID).QueryAsync()).FirstOrDefault();
                        request.ReasonCode = objReason?.ReasonCode;
                    }
                }
                else
                {
                    objLoan = (await _loanTab.WhereClause(x => x.LoanID == loanID).QueryAsync()).FirstOrDefault();
                    if (objLoan == null)
                    {
                        response.Message = MessageConstant.RequireLoanID;
                        _logger.LogError($"CreateCommentDebtPromptedCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                        return response;
                    }
                }
                DateTime dtNow = DateTime.Now;
                string fileImage = string.Empty;
                if (request.FileImage != null && request.FileImage.Count() > 0)
                {
                    foreach (var item in request.FileImage)
                    {
                        if (string.IsNullOrWhiteSpace(item.FullPath))
                        {
                            item.FullPath = GetUrlImageReal(item.FileName, item.LocalPath, item.S3Path);
                        }
                    }
                    fileImage = _common.ConvertObjectToJSonV2(request.FileImage);
                }
                var appointmentDate = dtNow;
                if (!string.IsNullOrEmpty(request.AppointmentDate))
                {
                    appointmentDate = DateTime.ParseExact(request.AppointmentDate, Constants.CollectionConstants.DateTimeDDMMYYYYHHMM, null);
                }
                var CommentDebtPromptedReq = new TblCommentDebtPrompted
                {
                    LoanID = objLoan.LoanID,
                    UserID = request.UserID,
                    FullName = request.FullName,
                    Comment = request.Comment,
                    ReasonCodeID = request.ReasonCodeID,
                    ReasonCode = request.ReasonCode,
                    CreateDate = dtNow,
                    IsDisplay = (int)CommentDebtPrompted_IsDisplay.Active,
                    TimaLoanID = objLoan?.TimaLoanID,
                    TimaCommentID = request.TimaID,
                    FileImage = fileImage,
                    AppointmentDate = appointmentDate,
                    ActionPerson = request.ActionPerson,
                    ActionAddress = request.ActionAddress,
                    CustomerID = objLoan.CustomerID.GetValueOrDefault()
                };
                var commentDebtPromptedID = await _commentDebtPromptedTab.InsertAsync(CommentDebtPromptedReq);
                if (commentDebtPromptedID > 0)
                {
                    if (request.TimaID == 0)
                    {
                        var user = (await _userTab.WhereClause(x => x.UserID == request.UserID).QueryAsync()).FirstOrDefault();
                        _ = await _queueCallAPIManager.InsertQueueComment(objLoan.TimaLoanID, request.Comment
                         , (int)request.ReasonCodeID, commentDebtPromptedID, user.TimaUserID, user.UserName, dtNow);
                    }
                    response.SetSucces();
                    response.Data = new CommentDebtPromptedInsertSuccessModel
                    {
                        LoanID = objLoan.LoanID,
                        CommentDebtPromptedID = commentDebtPromptedID,
                        CustomerID = CommentDebtPromptedReq.CustomerID
                    };
                }
                return response;

            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateCommentDebtPromptedCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }


        private string GetUrlImageReal(string fileName, string localPath, string s3Path)
        {

            string md5Key = _common.MD5(fileName + Constants.CollectionConstants.LOS_KEY);
            //if (s3Path != null)
            //{
            //    return $"{s3Path}?i={fileName}&hash={md5Key}&thumb=false";
            //}
            return $"{localPath}?i={fileName}&hash={md5Key}&thumb=false";
            // easy async support
        }

    }
}
