﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CollectionServiceApi.RestClients;
using LMS.Common.Constants;
using LMS.Entites.Dtos.ProxyTima;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CollectionServiceApi.Commands.CustomerExtensionThirdParty
{
    public class TrandataCallProxyCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class TrandataCallProxyCommandHandler : IRequestHandler<TrandataCallProxyCommand, LMS.Common.Constants.ResponseActionResult>
    {
        ILogger<TrandataCallProxyCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> _settingKeyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomerExtensionThirdParty> _customerExtensionThirdPartyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        IProxyTimaService _proxyTimaService;
        public TrandataCallProxyCommandHandler(
            ILogger<TrandataCallProxyCommandHandler> logger,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> settingKeyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomerExtensionThirdParty> customerExtensionThirdPartyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            IProxyTimaService proxyTimaService
         )
        {
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _settingKeyTab = settingKeyTab;
            _customerExtensionThirdPartyTab = customerExtensionThirdPartyTab;
            _proxyTimaService = proxyTimaService;
            _customerTab = customerTab;
            _loanTab = loanTab;
        }
        public async Task<ResponseActionResult> Handle(TrandataCallProxyCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var keySettingInfo = (await _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.TranDataCallProxy).QueryAsync()).FirstOrDefault();
                if (keySettingInfo == null || keySettingInfo.Status == (int)SettingKey_Status.InActice)
                {
                    response.Message = MessageConstant.SettingKey_AG_QueueCallAPI;
                    return response;
                }
                _ = ProccessCallProxy(keySettingInfo);
                await UpdateSettingKey(keySettingInfo, (int)SettingKey_Status.InActice);
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"TrandataCallProxyCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        private async Task UpdateSettingKey(Domain.Tables.TblSettingKey objSettingKey, int status)
        {
            try
            {
                objSettingKey.ModifyDate = DateTime.Now;
                objSettingKey.Status = status;
                await _settingKeyTab.UpdateAsync(objSettingKey);
            }
            catch (Exception ex)
            {
                _logger.LogError($"TrandataCallProxyCommandHandler_UpdateSettingKey|ex={ex.Message}-{ex.StackTrace}");
            }
        }
        private async Task ProccessCallProxy(Domain.Tables.TblSettingKey keySettingInfo)
        {
            Dictionary<long, Task> dictActionResultTask = new Dictionary<long, Task>();
            DateTime currentDate = DateTime.Now;
            long latestID = 0;
            try
            {
                while (true)
                {
                    var lstcustomerExtensionThirdParty = await _customerExtensionThirdPartyTab.SetGetTop(100)
                                                                .WhereClause(x => x.Status == (int)CustomerExtensionThirdParty_Status.Wait && x.CountRetries <= ProxyTimaConfiguration.CountRetries)
                                                                .WhereClause(x => x.CustomerExtensionThirdPartyID > latestID)
                                                                .QueryAsync();
                    if (lstcustomerExtensionThirdParty == null || !lstcustomerExtensionThirdParty.Any())
                    {
                        latestID = 0;
                        break;
                    }
                    var lstLoanID = new List<long>();
                    if (lstcustomerExtensionThirdParty.Count() > 0)
                    {
                        foreach (var item in lstcustomerExtensionThirdParty)
                        {
                            lstLoanID.Add(item.LoanID);
                            if (item.CustomerExtensionThirdPartyID > latestID)
                            {
                                latestID = item.CustomerExtensionThirdPartyID;
                            }
                        }
                    }
                    var lstLoan = (await _loanTab.WhereClause(x => lstLoanID.Contains(x.LoanID)).QueryAsync());
                    if (lstLoan == null || !lstLoan.Any())
                    {
                        continue;
                    }
                    var dictCustomerinfor = (await _customerTab.WhereClause(x => lstLoan.Select(y => y.CustomerID).Contains(x.CustomerID)).QueryAsync()).ToDictionary(x => x.CustomerID, x => x);
                    foreach (var item in lstcustomerExtensionThirdParty)
                    {
                        var objLoan = lstLoan.FirstOrDefault(x => x.LoanID == item.LoanID);
                        if (objLoan == null)
                            continue;

                        var objCustomer = dictCustomerinfor.GetValueOrDefault((long)objLoan.CustomerID);
                        if (objCustomer == null)
                            continue;

                        if (item.CodeTranData == CodeTranData.K03.ToString())
                        {
                            var k03ProxyTimaRequest = new K03ProxyTimaRequest()
                            {
                                PhoneNumber = objCustomer.Phone,
                                AgencyAccount = ProxyTimaConfiguration.AgencyAccount
                            };
                            var k03Proxy = _proxyTimaService.K03ProxyTima(k03ProxyTimaRequest);
                            dictActionResultTask.Add(item.CustomerExtensionThirdPartyID, k03Proxy);
                        }
                        else if (item.CodeTranData == CodeTranData.K06.ToString())
                        {
                            var k06ProxyTimaRequest = new K06ProxyTimaRequest()
                            {
                                PhoneNumber = objCustomer.Phone,
                                AgencyAccount = ProxyTimaConfiguration.AgencyAccount
                            };
                            var k06Proxy = _proxyTimaService.K06ProxyTima(k06ProxyTimaRequest);
                            dictActionResultTask.Add(item.CustomerExtensionThirdPartyID, k06Proxy);
                        }
                        else if (item.CodeTranData == CodeTranData.K21.ToString())
                        {
                            var k21ProxyTimaRequest = new K21ProxyTimaRequest()
                            {
                                NumberCard = objCustomer.NumberCard,
                                AgencyAccount = ProxyTimaConfiguration.AgencyAccount
                            };
                            var k21Proxy = _proxyTimaService.K21ProxyTima(k21ProxyTimaRequest);
                            dictActionResultTask.Add(item.CustomerExtensionThirdPartyID, k21Proxy);
                        }
                        else if (item.CodeTranData == CodeTranData.K22.ToString())
                        {
                            var k22ProxyTimaRequest = new K22ProxyTimaRequest()
                            {
                                PhoneNumber = objCustomer.Phone,
                                AgencyAccount = ProxyTimaConfiguration.AgencyAccount
                            };
                            var k22Proxy = _proxyTimaService.K22ProxyTima(k22ProxyTimaRequest);
                            dictActionResultTask.Add(item.CustomerExtensionThirdPartyID, k22Proxy);
                        }
                    }
                    await Task.WhenAll(dictActionResultTask.Values);
                    foreach (var item in lstcustomerExtensionThirdParty)
                    {
                        item.Status = (int)CustomerExtensionThirdParty_Status.Error;
                        item.ModifyDate = currentDate;
                        try
                        {
                            var actionResult = ((Task<ResponseActionResult>)dictActionResultTask[item.CustomerExtensionThirdPartyID]).Result;
                            // lỗi mạng retry
                            if (actionResult.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                            {
                                item.ThirdPartyData = actionResult.Message;
                                if (actionResult.Data != null)
                                {
                                    item.Status = (int)CustomerExtensionThirdParty_Status.Success;
                                    item.ThirdPartyData = _common.ConvertObjectToJSonV2(actionResult.Data);
                                }
                                continue;
                            }
                            item.Status = (int)CustomerExtensionThirdParty_Status.Wait;
                            item.CountRetries++;
                            if (item.CountRetries > ProxyTimaConfiguration.CountRetries)
                            {
                                _logger.LogError($"ProccessQueueCallApiAg_CountRetriesMax|item={_common.ConvertObjectToJSonV2(item)}");
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError($"ProccessQueueCallApiAg_item|item={_common.ConvertObjectToJSonV2(item)}|ex={ex.Message}-{ex.StackTrace}");
                        }
                    }
                    _customerExtensionThirdPartyTab.UpdateBulk(lstcustomerExtensionThirdParty);
                    dictActionResultTask.Clear();
                }
                latestID = 0;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProccessQueueCallApiAg|ex={ex.Message}-{ex.StackTrace}");
            }
            //bật lại key
            await UpdateSettingKey(keySettingInfo, (int)SettingKey_Status.Active);
        }
    }
}
