﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using CollectionServiceApi.Domain.Tables;
using System.Globalization;
using CollectionServiceApi.Domain.Models.CustomerExtensionThirdParty;

namespace CollectionServiceApi.Commands.CustomerExtensionThirdParty
{
    public class CreateCustomerExtensionThirdPartyCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public List<CreateCustomerExtensionThirdParty> LstData { get; set; }
        public int CreateBy { get; set; }

    }
    public class CreateCustomerExtensionThirdPartyCommandHandler : IRequestHandler<CreateCustomerExtensionThirdPartyCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomerExtensionThirdParty> _customerExtensionThirdPartyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        ILogger<CreateCustomerExtensionThirdPartyCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateCustomerExtensionThirdPartyCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomerExtensionThirdParty> customerExtensionThirdPartyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            ILogger<CreateCustomerExtensionThirdPartyCommandHandler> logger
            )
        {
            _customerExtensionThirdPartyTab = customerExtensionThirdPartyTab;
            _loanTab = loanTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateCustomerExtensionThirdPartyCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstCodeTranData = RuleCollectionServiceHelper.DictCodeTranData.Keys.ToList();
                string mes = string.Empty;
                if (request.LstData.Count() > 0)
                {
                    var dictTimaLoanIDInfos = (await _loanTab.WhereClause(x => request.LstData.Select(y => y.TimaLoanID).Contains(x.TimaLoanID)).QueryAsync()).ToDictionary(x => x.TimaLoanID, x => x);

                    var lstCustomerExtensionThirdParty = new List<TblCustomerExtensionThirdParty>();
                    var traceIDRequest = _common.GenTraceIndentifier();
                    int success = 0;
                    var lstTimaLoanIDError = new List<long>();
                    foreach (var item in request.LstData)
                    {
                        var objLoan = dictTimaLoanIDInfos.GetValueOrDefault(item.TimaLoanID);
                        if (objLoan == null || !lstCodeTranData.Any(x => x == item.CodeTranData))
                        {
                            lstTimaLoanIDError.Add(item.TimaLoanID);
                            continue;
                        }
                        lstCustomerExtensionThirdParty.Add(new TblCustomerExtensionThirdParty
                        {
                            TraceIDRequest = traceIDRequest,
                            LoanID = objLoan.LoanID,
                            TimaCodeID = objLoan.TimaCodeID,
                            CodeTranData = item.CodeTranData,
                            Status = (int)CustomerExtensionThirdParty_Status.Wait,
                            CreateBy = request.CreateBy,
                            CreateDate = DateTime.Now,
                            ModifyDate = DateTime.Now,
                            CountRetries = 0
                        });
                        success++;
                    }
                    if (lstTimaLoanIDError.Count > 0)
                    {
                        mes += $"{MessageConstant.InsertError + lstTimaLoanIDError.Count + MessageConstant.TimaLoanIDNotExist + string.Join(",", lstTimaLoanIDError)}";
                    }
                    mes += MessageConstant.InsertSuccess + success;
                    if (lstCustomerExtensionThirdParty.Count() > 0)
                    {
                        _customerExtensionThirdPartyTab.InsertBulk(lstCustomerExtensionThirdParty);
                        response.SetSucces();
                        response.Message = mes;
                        response.Data = traceIDRequest;
                        return response;
                    }
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateCustomerExtensionThirdPartyCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
