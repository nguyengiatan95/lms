﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.LOSServices;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.LOS
{
    public class CreateRelationshipCommand : IRequest<ResponseActionResult>
    {
        public long LoanID { get; set; }
        public string Phone { get; set; }
        public string FullName { get; set; }
        public int TypeID { get; set; }
        public string UserName { get; set; }
    }
    public class CreateRelationshipCommandHandler : IRequestHandler<CreateRelationshipCommand, ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        RestClients.ILOSService _lOSService;
        ILogger<CreateRelationshipCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateRelationshipCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            RestClients.ILOSService lOSService,
            ILogger<CreateRelationshipCommandHandler> logger)
        {
            _loanTab = loanTab;
            _lOSService = lOSService;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateRelationshipCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objLoan = (await _loanTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                if (objLoan == null)
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                var objRelationship = new CreateRelationshipReq
                {
                    LoanbriefId = (long)objLoan.LoanCreditIDOfPartner,
                    Phone = request.Phone,
                    FullName = request.FullName,
                    TypeId = request.TypeID,
                    UserName = request.UserName
                };
                return await _lOSService.CreateRelationshipLOS(objRelationship);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRelationshipCommandHandler_Exception|request={request}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
