﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.UserApprovalLevel
{
    public class CreateOrUpdateUserAppovalLevelCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long UserApprovalLevelID { get; set; }
        public string UserName { get; set; }
        public long ApprovalLevelBookDebtID { get; set; }
        public long MoveApprovalLevelBookDebtID { get; set; }
        public long CreateBy { get; set; }
        public int Status { get; set; }
    }
    public class CreateOrUpdateUserAppovalLevelCommandHandler : IRequestHandler<CreateOrUpdateUserAppovalLevelCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserApprovalLevel> _userApprovalLevelTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> _approvalLevelBookDebt;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<CreateOrUpdateUserAppovalLevelCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateOrUpdateUserAppovalLevelCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserApprovalLevel> userApprovalLevelTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> approvalLevelBookDebt,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
        ILogger<CreateOrUpdateUserAppovalLevelCommandHandler> logger)
        {
            _userApprovalLevelTab = userApprovalLevelTab;
            _approvalLevelBookDebt = approvalLevelBookDebt;
            _userTab = userTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(CreateOrUpdateUserAppovalLevelCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            DateTime dtNow = DateTime.Now;
            try
            {

                var users = await _userTab.WhereClause(x => x.UserName == request.UserName).QueryAsync();
                if (users == null || !users.Any())
                {
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                var user = users.First();
                if (request.UserApprovalLevelID == 0) // tạo mới
                {
                    var currentApprovals = await _userApprovalLevelTab.WhereClause(x => x.UserID == user.UserID && x.ApprovalLevelBookDebtID == request.ApprovalLevelBookDebtID).QueryAsync();
                    if (currentApprovals != null && currentApprovals.Any())
                    {
                        response.Message = MessageConstant.ErrorExist;
                        return response;
                    }
                    Domain.Tables.TblUserApprovalLevel dataInsert = new Domain.Tables.TblUserApprovalLevel()
                    {
                        ApprovalLevelBookDebtID = request.ApprovalLevelBookDebtID,
                        UserID = user.UserID,
                        CreateBy = request.CreateBy,
                        CreateDate = dtNow,
                        MoveToApprovalLevelBookDebtID = request.MoveApprovalLevelBookDebtID,
                        Status = request.Status
                    };
                    var idInsert = await _userApprovalLevelTab.InsertAsync(dataInsert);
                    response.SetSucces();
                    response.Data = idInsert;
                }
                else // cập nhật
                {
                    var currentApprovals = await _userApprovalLevelTab.WhereClause(x => x.UserApprovalLevelID == request.UserApprovalLevelID).QueryAsync();
                    if (currentApprovals == null || !currentApprovals.Any())
                    {
                        response.Message = MessageConstant.ErrorNotExist;
                        return response;
                    }
                    var dataUpdate = currentApprovals.FirstOrDefault();
                    dataUpdate.UserID = user.UserID;
                    dataUpdate.ApprovalLevelBookDebtID = request.ApprovalLevelBookDebtID;
                    dataUpdate.MoveToApprovalLevelBookDebtID = request.MoveApprovalLevelBookDebtID;
                    dataUpdate.Status = request.Status;
                    dataUpdate.ModifyBy = request.CreateBy;
                    dataUpdate.ModifyDate = dtNow;
                    var idUpdate = await _userApprovalLevelTab.UpdateAsync(dataUpdate);
                    response.SetSucces();
                    response.Data = request.UserApprovalLevelID;
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateOrUpdateUserAppovalLevelCommandHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
