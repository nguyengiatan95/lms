﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.SmartDialer
{
    public class UpdateResourceCampaignCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long CampaignID { get; set; }
        public long UserID { get; set; }
        public int WrapupTime { get; set; }
        public long UserIDLogin { get; set; }
    }
    public class UpdateResourceCampaignCommandHandler : IRequestHandler<UpdateResourceCampaignCommand, ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigUserMapSmartDialer> _configUsermapSmartDialerTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblMapUserCall> _mapUserCallTab;
        readonly ILogger<UpdateResourceCampaignCommandHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        readonly RestClients.ISmartDialerGateway _smartDialerGateway;
        readonly RedisCachedServiceHelper _redisCachedService;
        public UpdateResourceCampaignCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigUserMapSmartDialer> configUsermapSmartDialerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblMapUserCall> mapUserCallTab,
            RestClients.ISmartDialerGateway smartDialerGateway,
            RedisCachedServiceHelper redisCachedService,
            ILogger<UpdateResourceCampaignCommandHandler> logger)
        {
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _configUsermapSmartDialerTab = configUsermapSmartDialerTab;
            _mapUserCallTab = mapUserCallTab;
            _smartDialerGateway = smartDialerGateway;
            _redisCachedService = redisCachedService;
        }

        public async Task<ResponseActionResult> Handle(UpdateResourceCampaignCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                if (request.UserID < 1)
                {
                    return await CancelUserWithCampaign(request.CampaignID, request.UserIDLogin);
                }
                var lstCampaignResposneTask = _smartDialerGateway.GetCampaigns();
                var userMapCallDetailTask = _mapUserCallTab.WhereClause(x => x.UserID == request.UserID && x.TypeCall == (int)MapUserCall_TypeCall.Cisco && x.Status == (int)StatusCommon.Active).QueryAsync();
                var lstExistConfigUserSmartDialerTask = _configUsermapSmartDialerTab.WhereClause(x => (x.CampaignID == request.CampaignID || x.UserID == request.UserID)
                                                                                                     && x.Status == (int)StatusCommon.Active)
                                                                                    .QueryAsync();
                await Task.WhenAll(lstCampaignResposneTask, userMapCallDetailTask, lstExistConfigUserSmartDialerTask);
                var userMapCallDetail = userMapCallDetailTask.Result.FirstOrDefault();
                if (userMapCallDetail == null || userMapCallDetail.UserID < 1)
                {
                    response.Message = "Nhân viên chưa có thông tin cisco.";
                    return response;
                }
                var userCiscoExtra = _common.ConvertJSonToObjectV2<Domain.Tables.UserCallCiscoExtra>(userMapCallDetail.JsonExtra);
                if (userCiscoExtra == null || string.IsNullOrEmpty(userCiscoExtra.Username))
                {
                    response.Message = "Nhân viên chưa có thông tin cisco.";
                    return response;
                }
                var lstCampaignResposne = lstCampaignResposneTask.Result;
                if (lstCampaignResposne == null || !lstCampaignResposne.Any())
                {
                    response.Message = "Không có danh sách chiến dịch từ tổng đài.";
                    return response;
                }
                var campaignDetail = lstCampaignResposne.FirstOrDefault(x => x.CampaignId == request.CampaignID);
                if (campaignDetail == null || !campaignDetail.IsActive)
                {
                    response.Message = "Tổng đài đã khóa chiến dịch này.";
                    return response;
                }
                var lstExistConfigUserSmartDialer = lstExistConfigUserSmartDialerTask.Result;
                if (lstExistConfigUserSmartDialer == null || !lstExistConfigUserSmartDialer.Any())
                {
                    var actionConfigToCallCenter = await UpdateConfigToCallCenter(request, userCiscoExtra.Username);
                    if (actionConfigToCallCenter.Result != (int)ResponseAction.Success)
                    {
                        return actionConfigToCallCenter;
                    }
                    _ = await _configUsermapSmartDialerTab.InsertAsync(new Domain.Tables.TblConfigUserMapSmartDialer
                    {
                        CampaignID = request.CampaignID,
                        CampaignName = campaignDetail.CampaignName,
                        CreateDate = DateTime.Now,
                        ModiyDate = DateTime.Now,
                        ModifyBy = request.UserIDLogin,
                        Status = (int)StatusCommon.Active,
                        UserID = userMapCallDetail.UserID,
                        UserNameCisco = userCiscoExtra.Username,
                        WrapupTime = request.WrapupTime
                    });
                }
                else
                {
                    var userExist = lstExistConfigUserSmartDialer.FirstOrDefault();
                    if (userExist.CampaignID != request.CampaignID)
                    {
                        response.Message = $"Nhân viên đã được cấu hình cho chiến dịch {userExist.CampaignName}";
                        return response;
                    }
                    if (userExist.UserID == request.UserID && userExist.WrapupTime == request.WrapupTime)
                    {
                        response.SetSucces();
                        return response;
                    }
                    // xóa các user cũ trên tổng đài
                    // cập nhật lại trạng thái
                    foreach (var item in lstExistConfigUserSmartDialer)
                    {
                        // gọi hủy lên tổng đài
                        _ = _smartDialerGateway.UpdateResource(new LMS.Entites.Dtos.SmartDialer.UpdateResourceRequest
                        {
                            Campaigns = new List<LMS.Entites.Dtos.SmartDialer.UpdateResourceCampaignItem>
                            {
                            },
                            Resource = item.UserNameCisco
                        });
                        item.Status = (int)StatusCommon.UnActive;
                        item.ModifyBy = request.UserIDLogin;
                        item.ModiyDate = DateTime.Now;
                    }
                    _configUsermapSmartDialerTab.UpdateBulk(lstExistConfigUserSmartDialer);
                    var actionConfigToCallCenter = await UpdateConfigToCallCenter(request, userCiscoExtra.Username);
                    if (actionConfigToCallCenter.Result != (int)ResponseAction.Success)
                    {
                        return actionConfigToCallCenter;
                    }
                    _ = await _configUsermapSmartDialerTab.InsertAsync(new Domain.Tables.TblConfigUserMapSmartDialer
                    {
                        CampaignID = request.CampaignID,
                        CampaignName = campaignDetail.CampaignName,
                        CreateDate = DateTime.Now,
                        ModiyDate = DateTime.Now,
                        ModifyBy = request.UserIDLogin,
                        Status = (int)StatusCommon.Active,
                        UserID = userMapCallDetail.UserID,
                        UserNameCisco = userCiscoExtra.Username,
                        WrapupTime = request.WrapupTime
                    });
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateResourceCampaignCommandHandler|Request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        private async Task<ResponseActionResult> CancelUserWithCampaign(long campaignID, long modifyBy)
        {
            ResponseActionResult response = new ResponseActionResult();
            response.SetSucces();
            var lstExistConfigUserSmartDialer = await _configUsermapSmartDialerTab.WhereClause(x => x.CampaignID == campaignID && x.Status == (int)StatusCommon.Active).QueryAsync();
            if (lstExistConfigUserSmartDialer == null || !lstExistConfigUserSmartDialer.Any())
            {
                return response;
            }
            foreach (var item in lstExistConfigUserSmartDialer)
            {
                // gọi hủy lên tổng đài
                _ = _smartDialerGateway.UpdateResource(new LMS.Entites.Dtos.SmartDialer.UpdateResourceRequest
                {
                    Campaigns = new List<LMS.Entites.Dtos.SmartDialer.UpdateResourceCampaignItem>
                    {
                    },
                    Resource = item.UserNameCisco
                });
                item.Status = (int)StatusCommon.UnActive;
                item.ModifyBy = modifyBy;
                item.ModiyDate = DateTime.Now;
            }
            _configUsermapSmartDialerTab.UpdateBulk(lstExistConfigUserSmartDialer);
            return response;
        }

        private async Task<ResponseActionResult> UpdateConfigToCallCenter(UpdateResourceCampaignCommand request, string usernameCisco)
        {
            ResponseActionResult response = new ResponseActionResult();
            var actionUpdateReourceTask = _smartDialerGateway.UpdateResource(new LMS.Entites.Dtos.SmartDialer.UpdateResourceRequest
            {
                Campaigns = new List<LMS.Entites.Dtos.SmartDialer.UpdateResourceCampaignItem>
                        {
                            new LMS.Entites.Dtos.SmartDialer.UpdateResourceCampaignItem
                            {
                                CampaignId = request.CampaignID
                            }
                        },
                Resource = usernameCisco
            });
            var actionUpdateCsqTask = _smartDialerGateway.UpdateCsq(new LMS.Entites.Dtos.SmartDialer.UpdateCsqRequest
            {
                Csqs = new List<LMS.Entites.Dtos.SmartDialer.UpdateCsqItemRequest>
                        {
                            new LMS.Entites.Dtos.SmartDialer.UpdateCsqItemRequest
                            {
                                CampaignId = request.CampaignID,
                                WrapupTime = request.WrapupTime
                            }
                        }
            });
            await Task.WhenAll(actionUpdateReourceTask, actionUpdateCsqTask);
            var actionUpdateReource = actionUpdateReourceTask.Result;
            if (actionUpdateReource.Result == (int)ResponseAction.Error)
            {
                response.Message = "Có lỗi xảy ra khi cập nhật lên tổng đài.";
                return response;
            }
            var actionUpdateCsq = actionUpdateCsqTask.Result;
            if (actionUpdateCsq == null)
            {
                response.Message = "Có lỗi xảy ra khi cập nhật thời gian lên tổng đài.";
                return response;
            }
            response.SetSucces();
            return response;
        }
    }
}
