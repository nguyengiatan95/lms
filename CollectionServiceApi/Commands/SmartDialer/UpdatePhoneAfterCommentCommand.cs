﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.SmartDialer
{
    public class UpdatePhoneAfterCommentCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string ReasonCode { get; set; }
        public long CustomerID { get; set; }
    }
    public class UpdatePhoneAfterCommentCommandHandler : IRequestHandler<UpdatePhoneAfterCommentCommand, ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleCallDaily> _planHandleCallDailyTab;
        readonly ILogger<UpdatePhoneAfterCommentCommandHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        readonly RestClients.ISmartDialerGateway _smartDialerGateway;
        readonly RedisCachedServiceHelper _redisCachedService;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigHoldCodeForSmartDialer> _configHoldCodeForSmartDialerTab;
        public UpdatePhoneAfterCommentCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleCallDaily> planHandleCallDailyTab,
            RestClients.ISmartDialerGateway smartDialerGateway,
            RedisCachedServiceHelper redisCachedService,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigHoldCodeForSmartDialer> configHoldCodeForSmartDialerTab,
            ILogger<UpdatePhoneAfterCommentCommandHandler> logger)
        {
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _planHandleCallDailyTab = planHandleCallDailyTab;
            _smartDialerGateway = smartDialerGateway;
            _redisCachedService = redisCachedService;
            _configHoldCodeForSmartDialerTab = configHoldCodeForSmartDialerTab;
        }

        public async Task<ResponseActionResult> Handle(UpdatePhoneAfterCommentCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            response.SetSucces();
            try
            {
                List<Domain.Tables.TblPlanHandleCallDaily> lstUpdate = new List<Domain.Tables.TblPlanHandleCallDaily>();
                List<int> lstStatus = new List<int>
                {
                    (int)PlanHandleCallDaily_Status.Waiting,
                    (int)PlanHandleCallDaily_Status.Pushed,
                    (int)PlanHandleCallDaily_Status.Handled
                };
                //var configHoldCode = await _redisCachedService.GetSmartDialerReasonCodeHoldToCall();
                var configHoldCodeDetail = (await _configHoldCodeForSmartDialerTab.QueryAsync()).FirstOrDefault();
                if (configHoldCodeDetail == null)
                {
                    _logger.LogError($"UpdatePhoneAfterCommentCommandHandler_Warning|configHoldCode not found");
                    return response;
                }
                var configHoldCode = new Domain.Models.SmartDialer.ConfigHoldCodeForSmartDialerModel
                {
                    SkipTime = configHoldCodeDetail.SkipTime,
                    LstReasonCode = configHoldCodeDetail.ReasonCodes.Split(',').Select(x => x.ToLower()).ToList()
                };
                if (!configHoldCode.LstReasonCode.Contains(request.ReasonCode.ToLower()))
                {
                    _logger.LogInformation($"UpdatePhoneAfterCommentCommandHandler|configHoldCode.LstReasonCode={_common.ConvertObjectToJSonV2(configHoldCode.LstReasonCode)}|ReasonCode={request.ReasonCode}");
                    return response;
                }
                var existsInfos = await _planHandleCallDailyTab.WhereClause(x => x.CallDate == DateTime.Now.Date && x.CustomerID == request.CustomerID && lstStatus.Contains(x.Status)).QueryAsync();
                if (existsInfos == null || !existsInfos.Any())
                {
                    return response;
                }
                var requestGetPendingCampaign = new LMS.Entites.Dtos.SmartDialer.PendingOfCampaignRequest { };
                foreach (var item in existsInfos)
                {
                    // các số đã push lên tổng đài theo campaign
                    if (item.Status != (int)PlanHandleCallDaily_Status.Waiting)
                    {
                        requestGetPendingCampaign.CampaignId = item.CampaignID;
                    }
                    item.ModifyDate = DateTime.Now;
                    item.Status = (int)PlanHandleCallDaily_Status.Cancel;
                    item.HoldCode = request.ReasonCode;
                    item.Note = $"Hệ thống hủy không xử lý do có code {request.ReasonCode}.";
                }
                _planHandleCallDailyTab.UpdateBulk(existsInfos);
                if (requestGetPendingCampaign.CampaignId < 1)
                {
                    _logger.LogError($"UpdatePhoneAfterCommentCommandHandler_Warning|PendingOfCampaignRequest not found");
                    return response;
                }
                // gọi tổng đài tạm dừng campaign
                var lstPhonePending = await _smartDialerGateway.GetPendingOfCampaign(requestGetPendingCampaign);
                if (lstPhonePending == null || !lstPhonePending.Any())
                {
                    return response;
                }
                // remove các số ra khỏi pending nếu có
                // push lại toàn bộ số theo độ ưu tiên khi chưa xử lý
                Dictionary<long, LMS.Entites.Dtos.SmartDialer.ImportCampaignRequest> dictCampaignImport = new Dictionary<long, LMS.Entites.Dtos.SmartDialer.ImportCampaignRequest>();
                LMS.Entites.Dtos.SmartDialer.ImportCampaignRequest requestImport = new LMS.Entites.Dtos.SmartDialer.ImportCampaignRequest
                {
                    CampaignId = requestGetPendingCampaign.CampaignId,
                    Data = new List<LMS.Entites.Dtos.SmartDialer.ImportCampaignPhoneItem>()
                };
                foreach (var item in lstPhonePending)
                {
                    var existPhone = existsInfos.FirstOrDefault(x => x.NumberPhone == item.Phone1);
                    if (existPhone != null && existPhone.CampaignID > 0)
                    {
                        continue;
                    }
                    requestImport.Data.Add(new LMS.Entites.Dtos.SmartDialer.ImportCampaignPhoneItem
                    {
                        Phone = item.Phone1
                    });
                }
                if (requestImport.Data.Count > 0)
                {
                    _ = _smartDialerGateway.ImportCampaign(requestImport);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdatePhoneAfterCommentCommandHandler|Request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
