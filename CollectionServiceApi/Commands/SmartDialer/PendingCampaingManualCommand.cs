﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.SmartDialer
{
    public class PendingCampaingManualCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long CampaignID { get; set; }
    }
    public class PendingCampaingManualCommandHandler : IRequestHandler<PendingCampaingManualCommand, ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleCallDaily> _planHandleCallDailyTab;
        readonly ILogger<PendingCampaingManualCommandHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        readonly RestClients.ISmartDialerGateway _smartDialerGateway;
        public PendingCampaingManualCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleCallDaily> planHandleCallDailyTab,
            RestClients.ISmartDialerGateway smartDialerGateway,
            ILogger<PendingCampaingManualCommandHandler> logger)
        {
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _planHandleCallDailyTab = planHandleCallDailyTab;
            _smartDialerGateway = smartDialerGateway;
        }

        public async Task<ResponseActionResult> Handle(PendingCampaingManualCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            response.SetSucces();
            try
            {
                List<Domain.Tables.TblPlanHandleCallDaily> lstUpdate = new List<Domain.Tables.TblPlanHandleCallDaily>();
                List<int> lstStatus = new List<int>
                {
                    (int)PlanHandleCallDaily_Status.Pushed,
                    (int)PlanHandleCallDaily_Status.Handled
                };
                var requestGetPendingCampaign = new LMS.Entites.Dtos.SmartDialer.PendingOfCampaignRequest { CampaignId = request.CampaignID };
                // gọi tổng đài tạm dừng campaign
                var t1 = _smartDialerGateway.GetPendingOfCampaign(requestGetPendingCampaign);
                var existsInfosTask = _planHandleCallDailyTab.WhereClause(x => x.CallDate == DateTime.Now.Date && x.CampaignID == request.CampaignID && lstStatus.Contains(x.Status)).QueryAsync();
                await Task.WhenAll(existsInfosTask, t1);
                var existsInfos = existsInfosTask.Result;
                if (existsInfos == null || !existsInfos.Any())
                {
                    return response;
                }
                foreach (var item in existsInfos)
                {
                    item.ModifyDate = DateTime.Now;
                    item.Status = (int)PlanHandleCallDaily_Status.Cancel;
                    item.Note = $"Hệ thống hủy không xử lý.";
                }
                _planHandleCallDailyTab.UpdateBulk(existsInfos);
            }
            catch (Exception ex)
            {
                _logger.LogError($"PendingCampaingManualCommandHandler|Request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
