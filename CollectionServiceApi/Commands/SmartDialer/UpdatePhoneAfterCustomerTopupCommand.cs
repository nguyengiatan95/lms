﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.SmartDialer
{
    public class UpdatePhoneAfterCustomerTopupCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long CustomerID { get; set; }
    }
    public class UpdatePhoneAfterCustomerTopupCommandHandler : IRequestHandler<UpdatePhoneAfterCustomerTopupCommand, ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleCallDaily> _planHandleCallDailyTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        readonly ILogger<UpdatePhoneAfterCustomerTopupCommandHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        readonly RestClients.ISmartDialerGateway _smartDialerGateway;
        public UpdatePhoneAfterCustomerTopupCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleCallDaily> planHandleCallDailyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            RestClients.ISmartDialerGateway smartDialerGateway,
            ILogger<UpdatePhoneAfterCustomerTopupCommandHandler> logger)
        {
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _planHandleCallDailyTab = planHandleCallDailyTab;
            _smartDialerGateway = smartDialerGateway;
            _customerTab = customerTab;
        }

        public async Task<ResponseActionResult> Handle(UpdatePhoneAfterCustomerTopupCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            response.SetSucces();
            try
            {
                List<Domain.Tables.TblPlanHandleCallDaily> lstUpdate = new List<Domain.Tables.TblPlanHandleCallDaily>();
                List<int> lstStatus = new List<int>
                {
                    (int)PlanHandleCallDaily_Status.Waiting,
                    (int)PlanHandleCallDaily_Status.Pushed,
                    (int)PlanHandleCallDaily_Status.Handled
                };
                var customerDetailTask = _customerTab.SelectColumns(x => x.TotalMoney, x => x.CustomerID).WhereClause(x => x.CustomerID == request.CustomerID).QueryAsync();
                var existsInfosTask = _planHandleCallDailyTab.WhereClause(x => x.CallDate == DateTime.Now.Date && x.CustomerID == request.CustomerID && lstStatus.Contains(x.Status)).QueryAsync();
                await Task.WhenAll(customerDetailTask, existsInfosTask);
                var existsInfos = existsInfosTask.Result;
                var customerInfos = customerDetailTask.Result;
                if (customerInfos == null || !customerInfos.Any())
                {
                    return response;
                }
                if (existsInfos == null || !existsInfos.Any())
                {
                    return response;
                }
                var customerDetail = customerInfos.FirstOrDefault();
                foreach (var item in existsInfos)
                {
                    if (item.TotalMoneyNeedPay > customerDetail.TotalMoney)
                    {
                        return response;
                    }
                }
                var requestGetPendingCampaign = new LMS.Entites.Dtos.SmartDialer.PendingOfCampaignRequest { };
                foreach (var item in existsInfos)
                {
                    // các số đã push lên tổng đài theo campaign
                    if (item.Status != (int)PlanHandleCallDaily_Status.Waiting)
                    {
                        requestGetPendingCampaign.CampaignId = item.CampaignID;
                    }
                    item.ModifyDate = DateTime.Now;
                    item.Status = (int)PlanHandleCallDaily_Status.Cancel;
                    item.Note = $"Hệ thống hủy không xử lý do KH đã đủ tiền cắt kỳ.";
                }
                _planHandleCallDailyTab.UpdateBulk(existsInfos);
                if (requestGetPendingCampaign.CampaignId < 1)
                {
                    _logger.LogError($"UpdatePhoneAfterCommentCommandHandler_Warning|PendingOfCampaignRequest not found");
                    return response;
                }
                // gọi tổng đài tạm dừng campaign
                var lstPhonePending = await _smartDialerGateway.GetPendingOfCampaign(requestGetPendingCampaign);
                if (lstPhonePending == null || lstPhonePending.Any())
                {
                    return response;
                }
                // remove các số ra khỏi pending nếu có
                // push lại toàn bộ số theo độ ưu tiên khi chưa xử lý
                Dictionary<long, LMS.Entites.Dtos.SmartDialer.ImportCampaignRequest> dictCampaignImport = new Dictionary<long, LMS.Entites.Dtos.SmartDialer.ImportCampaignRequest>();
                LMS.Entites.Dtos.SmartDialer.ImportCampaignRequest requestImport = new LMS.Entites.Dtos.SmartDialer.ImportCampaignRequest
                {
                    CampaignId = requestGetPendingCampaign.CampaignId,
                    Data = new List<LMS.Entites.Dtos.SmartDialer.ImportCampaignPhoneItem>()
                };
                foreach (var item in lstPhonePending)
                {
                    var existPhone = existsInfos.FirstOrDefault(x => x.NumberPhone == item.Phone1);
                    if (existPhone != null && existPhone.CampaignID > 0)
                    {
                        continue;
                    }
                    requestImport.Data.Add(new LMS.Entites.Dtos.SmartDialer.ImportCampaignPhoneItem
                    {
                        Phone = item.Phone1
                    });
                }
                if (requestImport.Data.Count > 0)
                {
                    _ = _smartDialerGateway.ImportCampaign(requestImport);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdatePhoneAfterCommentCommandHandler|Request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
