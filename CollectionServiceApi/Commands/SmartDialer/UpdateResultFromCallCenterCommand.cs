﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.SmartDialer
{
    public class UpdateResultFromCallCenterCommand : IRequest<ResponseActionResult>
    {
        public Domain.Models.SmartDialer.CallResultFromCallCenterRequest Models { get; set; }
        public long UserID { get; set; }
    }
    public class UpdateResultFromCallCenterCommandHandler : IRequestHandler<UpdateResultFromCallCenterCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly ILogger<UpdateResultFromCallCenterCommandHandler> _logger;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleCallDaily> _planHandleCallDailyTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogSmartDialerReturn> _logSmartDialerReturnTab;
        readonly LMS.Common.Helper.Utils _common;
        readonly RestClients.ISmartDialerGateway _smartDialerGateway;
        private const int TimeServerCall = 7;
        public UpdateResultFromCallCenterCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleCallDaily> planHandleCallDailyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogSmartDialerReturn> logSmartDialerReturnTab,
            RestClients.ISmartDialerGateway smartDialerGateway,
            ILogger<UpdateResultFromCallCenterCommandHandler> logger)
        {
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _planHandleCallDailyTab = planHandleCallDailyTab;
            _smartDialerGateway = smartDialerGateway;
            _logSmartDialerReturnTab = logSmartDialerReturnTab;
        }

        public async Task<ResponseActionResult> Handle(UpdateResultFromCallCenterCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult result = new ResponseActionResult();
            result.SetSucces();
            try
            {
                // khoảng 1 phút tổng đài gọi về 1 lần
                var currentDate = DateTime.Now;
                List<long> lstIdCallCenterPushed = new List<long>();
                List<long> lstCampaignIDReturn = new List<long>();
                List<Domain.Tables.TblLogSmartDialerReturn> lstInsertLog = new List<Domain.Tables.TblLogSmartDialerReturn>();
                Dictionary<long, Domain.Models.SmartDialer.CallResultFromCallCenterItemRequest> dictPhoneInputs = new Dictionary<long, Domain.Models.SmartDialer.CallResultFromCallCenterItemRequest>();
                foreach (var item in request.Models.Inputs)
                {
                    // chỉ lấy những start date trong ngày
                    if (item.Startdatetime > currentDate.Date && !dictPhoneInputs.ContainsKey(item.Id))
                    {
                        dictPhoneInputs.Add(item.Id, item);
                        lstCampaignIDReturn.Add(Convert.ToInt64(item.Campaignid));
                    }
                }
                // kiểm tra các id đã có trong log chưa trong ngày hiện tại
                // chỉ ghi nhận id chưa có trong log
                var lstPhonePushedExists = await _logSmartDialerReturnTab.SelectColumns(x => x.ReturnID).WhereClause(x => dictPhoneInputs.Keys.Contains(x.ReturnID)).QueryAsync();
                if (lstPhonePushedExists != null && lstPhonePushedExists.Any())
                {
                    var dictPhonePushedExists = lstPhonePushedExists.GroupBy(x => x.ReturnID).ToDictionary(x => x.Key, x => x);
                    foreach (var item in request.Models.Inputs)
                    {
                        var exist = dictPhonePushedExists.GetValueOrDefault(item.Id);
                        if (exist == null || exist.Key < 1)
                        {
                            try
                            {
                                _ = await _logSmartDialerReturnTab.InsertAsync(new Domain.Tables.TblLogSmartDialerReturn
                                {
                                    ReturnID = item.Id,
                                    CreateDate = currentDate,
                                    ExtraData = _common.ConvertObjectToJSonV2(item),
                                });
                            }
                            catch (Exception ex)
                            {
                                _logger.LogError($"_logSmartDialerReturnTab.InsertAsync|phonePushed={_common.ConvertObjectToJSonV2(item)}|ex={ex.Message}-{ex.StackTrace}");
                            }
                        }
                        else
                        {
                            dictPhoneInputs.Remove(item.Id);
                        }
                    }
                }
                else
                {
                    foreach (var item in request.Models.Inputs)
                    {
                        try
                        {
                            _ = await _logSmartDialerReturnTab.InsertAsync(new Domain.Tables.TblLogSmartDialerReturn
                            {
                                ReturnID = item.Id,
                                CreateDate = currentDate,
                                ExtraData = _common.ConvertObjectToJSonV2(item),
                            });
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError($"_logSmartDialerReturnTab.InsertAsync|phonePushed={_common.ConvertObjectToJSonV2(item)}|ex={ex.Message}-{ex.StackTrace}");
                        }
                    }
                }
                List<Domain.Tables.TblPlanHandleCallDaily> lstUpdate = new List<Domain.Tables.TblPlanHandleCallDaily>();
                List<int> lstStatus = new List<int>
                {
                    (int)PlanHandleCallDaily_Status.Waiting,
                    (int)PlanHandleCallDaily_Status.Pushed,
                    (int)PlanHandleCallDaily_Status.Handled
                };
                List<int> lstStatusCallResultSuccess = new List<int>
                {
                    (int)LMS.Entites.Dtos.SmartDialer.CallResult.Voice,
                    //(int)LMS.Entites.Dtos.SmartDialer.CallResult.CallFailed,
                    //(int)LMS.Entites.Dtos.SmartDialer.CallResult.AgentDisconnect
                };
                lstCampaignIDReturn = lstCampaignIDReturn.Distinct().ToList();
                // lấy danh sách gọi theo campaignid trong ngày chưa thành công và chưa xử lý
                var lstPhoneDailyInDayByCampaignIDs = await _planHandleCallDailyTab
                                                .WhereClause(x => x.CallDate == DateTime.Now.Date && lstCampaignIDReturn.Contains(x.CampaignID) && lstStatus.Contains(x.Status))
                                                .WhereClause(x => x.CallResult != (int)PlanHandleCallDaily_CallResult.Success)
                                                .QueryAsync();
                if (lstPhoneDailyInDayByCampaignIDs == null || !lstPhoneDailyInDayByCampaignIDs.Any())
                {
                    return result;
                }
                Dictionary<long, string> dictCustomerIDHasCallSuccess = new Dictionary<long, string>();
                Dictionary<long, Dictionary<int, List<Domain.Tables.TblPlanHandleCallDaily>>> dictCampaignWithPriorityOrderPushInfos = new Dictionary<long, Dictionary<int, List<Domain.Tables.TblPlanHandleCallDaily>>>();
                foreach (var item in lstPhoneDailyInDayByCampaignIDs)
                {
                    var resultCall = dictPhoneInputs.Values.FirstOrDefault(x => x.PhoneJoined == item.NumberPhone);
                    if (resultCall != null && resultCall.Id > 0)
                    {
                        item.CallCount++;
                        item.ModifyDate = DateTime.Now;
                        item.Status = (int)PlanHandleCallDaily_Status.Handled;
                        if (resultCall.Callresult.HasValue && lstStatusCallResultSuccess.Contains((int)resultCall.Callresult))
                        {
                            item.CallResult = (int)PlanHandleCallDaily_CallResult.Success;
                            item.StartTime = resultCall.Startdatetime.AddHours(TimeServerCall); // giờ server ko xử lý +7
                            item.ConnectTime = Convert.ToInt32(resultCall.Connecttime);
                            item.EndTime = item.StartTime.Value.AddSeconds(item.ConnectTime);
                            if (!dictCustomerIDHasCallSuccess.ContainsKey(item.CustomerID))
                            {
                                dictCustomerIDHasCallSuccess.Add(item.CustomerID, $"{item.CustomerCallName}-{ item.NumberPhone}");
                            }
                        }
                        else
                        {
                            item.CallResult = (int)PlanHandleCallDaily_CallResult.Fail;
                            if (item.CallCount >= item.CallMaxCount)
                            {
                                //dictPriorty[item.PriorityOrder].RemoveAll(x => x.CustomerID == item.CustomerID);
                                item.Status = (int)PlanHandleCallDaily_Status.OverLimit;
                                item.CallCount = item.CallMaxCount;
                            }
                            else
                            {
                                if (!dictCampaignWithPriorityOrderPushInfos.ContainsKey(item.CampaignID))
                                {
                                    dictCampaignWithPriorityOrderPushInfos.Add(item.CampaignID, new Dictionary<int, List<Domain.Tables.TblPlanHandleCallDaily>> { });
                                }
                                var dictPriorty = dictCampaignWithPriorityOrderPushInfos[item.CampaignID];
                                if (!dictPriorty.ContainsKey(item.PriorityOrder))
                                {
                                    dictPriorty.Add(item.PriorityOrder, new List<Domain.Tables.TblPlanHandleCallDaily>());
                                }
                                dictPriorty[item.PriorityOrder].Add(item);
                            }
                        }
                        lstUpdate.Add(item);
                    }
                }
                if (lstUpdate.Count < 1)
                {
                    return result;
                }
                //_planHandleCallDailyTab.UpdateBulk(lstUpdate);
                // lấy ds chờ tiếp theo
                if (dictCampaignWithPriorityOrderPushInfos.Count < 1)
                {
                    var dictCampaignIDIngore = new Dictionary<long, long>();
                    foreach (var item in lstPhoneDailyInDayByCampaignIDs)
                    {
                        if (item.Status != (int)PlanHandleCallDaily_Status.Waiting && item.CallResult != (int)PlanHandleCallDaily_CallResult.Success && item.CallCount < item.CallMaxCount)
                        {
                            if (!dictCampaignIDIngore.ContainsKey(item.CampaignID))
                            {
                                dictCampaignIDIngore.Add(item.CampaignID, item.CampaignID);
                            }
                            // kiểm tra lại
                            dictCampaignWithPriorityOrderPushInfos.Remove(item.CampaignID);
                        }
                        if (item.Status == (int)PlanHandleCallDaily_Status.Waiting && !dictCampaignIDIngore.ContainsKey(item.CampaignID))
                        {
                            if (!dictCampaignWithPriorityOrderPushInfos.ContainsKey(item.CampaignID))
                            {
                                dictCampaignWithPriorityOrderPushInfos.Add(item.CampaignID, new Dictionary<int, List<Domain.Tables.TblPlanHandleCallDaily>> { });
                            }
                            var dictPriorty = dictCampaignWithPriorityOrderPushInfos[item.CampaignID];
                            if (!dictPriorty.ContainsKey(item.PriorityOrder))
                            {
                                dictPriorty.Add(item.PriorityOrder, new List<Domain.Tables.TblPlanHandleCallDaily>());
                            }
                            dictPriorty[item.PriorityOrder].Add(item);
                        }
                    }
                }

                // xử lý đẩy danh sách tiếp theo
                Dictionary<long, LMS.Entites.Dtos.SmartDialer.ImportCampaignRequest> dictCampaignImport = new Dictionary<long, LMS.Entites.Dtos.SmartDialer.ImportCampaignRequest>();
                //lstUpdate.Clear();
                List<Domain.Tables.TblPlanHandleCallDaily> lstPhoneHandled = new List<Domain.Tables.TblPlanHandleCallDaily>();
                foreach (var item in dictCampaignWithPriorityOrderPushInfos)
                {
                    var priorities = item.Value.OrderBy(x => x.Key);
                    foreach (var priority in priorities)
                    {
                        // campaign đã co
                        if (dictCampaignImport.ContainsKey(item.Key))
                        {
                            continue;
                        }
                        if (priority.Value.Count < 1)
                        {
                            continue;
                        }
                        if (priority.Key == (int)PlanHandleCallDaily_PriorityOrder.DPDInDue)
                        {
                            lstPhoneHandled = priority.Value.OrderBy(x => x.CallCount).ThenByDescending(x => x.DPD).ThenByDescending(x => x.TotalMoneyOriginal).ThenByDescending(x => x.Status).ToList();
                        }
                        else
                        {
                            lstPhoneHandled = priority.Value.OrderBy(x => x.CallCount).ThenBy(x => x.DPD).ThenByDescending(x => x.TotalMoneyOriginal).ThenByDescending(x => x.Status).ToList();
                        }
                        foreach (var phone in lstPhoneHandled)
                        {
                            if (!dictCampaignImport.ContainsKey(phone.CampaignID))
                            {
                                dictCampaignImport.Add(phone.CampaignID, new LMS.Entites.Dtos.SmartDialer.ImportCampaignRequest
                                {
                                    CampaignId = phone.CampaignID,
                                    Data = new List<LMS.Entites.Dtos.SmartDialer.ImportCampaignPhoneItem>()
                                });
                            }
                            // bỏ qua số dt đã liên hệ với số tham chiếu thành công
                            if (dictCustomerIDHasCallSuccess.ContainsKey(phone.CustomerID))
                            {
                                continue;
                            }
                            dictCampaignImport[phone.CampaignID].Data.Add(new LMS.Entites.Dtos.SmartDialer.ImportCampaignPhoneItem
                            {
                                Phone = phone.NumberPhone
                            });
                            if (phone.Status == (int)PlanHandleCallDaily_Status.Waiting)
                            {
                                phone.Status = (int)PlanHandleCallDaily_Status.Pushed;
                                phone.ModifyDate = DateTime.Now;
                                //phone.CallCount += 1;
                                lstUpdate.Add(phone);
                            }
                        }
                    }
                }
                if (lstUpdate.Count > 0)
                {
                    _planHandleCallDailyTab.UpdateBulk(lstUpdate);
                }

                // đẩy lên tổng đài
                foreach (var item in dictCampaignImport)
                {
                    if (item.Value.Data.Count > 0)
                    {
                        _ = _smartDialerGateway.ImportCampaign(item.Value);
                    }
                }

                // cập nhật các số tham chiếu không gọi nữa
                if (dictCustomerIDHasCallSuccess.Count > 0)
                {
                    var lstKeyCustomerID = dictCustomerIDHasCallSuccess.Keys.ToList();
                    var lstDailyNotYetCall = lstPhoneDailyInDayByCampaignIDs.Where(x => lstKeyCustomerID.Contains(x.CustomerID)
                                                                        && x.Status == (int)PlanHandleCallDaily_Status.Waiting).ToList();
                    if (lstDailyNotYetCall != null && lstDailyNotYetCall.Any())
                    {
                        foreach (var item in lstDailyNotYetCall)
                        {
                            item.Status = (int)PlanHandleCallDaily_Status.Cancel;
                            item.ModifyDate = DateTime.Now;
                            item.Note = $"Hệ thống hủy không xử lý do đã liên hệ với thông tin {dictCustomerIDHasCallSuccess[item.CustomerID]}";
                        }
                        _planHandleCallDailyTab.UpdateBulk(lstDailyNotYetCall);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateResultFromCallCenterCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return result;
        }
    }
}
