﻿using LMS.Common.Constants;
using LMS.Common.Helper.DeepCopyExtensions;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.SmartDialer
{
    public class ProcessingThePhoneForDailyCallCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class ProcessingThePhoneForDailyCallCommandHandler : IRequestHandler<ProcessingThePhoneForDailyCallCommand, ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleCallDaily> _planHandleCallDailyTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigUserMapSmartDialer> _configUserMapSmartDialerTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanCloseLoan> _planCloseLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> _assgimentLoanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> _commentDebtPromptedTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigHoldCodeForSmartDialer> _configHoldCodeForSmartDialerTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanCreditDetailLos> _loanCreditDetailLosTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> _paymentScheduleTab;
        readonly ILogger<ProcessingThePhoneForDailyCallCommandHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public ProcessingThePhoneForDailyCallCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleCallDaily> planHandleCallDailyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigUserMapSmartDialer> configUserMapSmartDialerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblAssignmentLoan> assgimentLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanCloseLoan> planCloseLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> commentDebtPromptedTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigHoldCodeForSmartDialer> configHoldCodeForSmartDialerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanCreditDetailLos> loanCreditDetailLosTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPaymentSchedule> paymentScheduleTab,
            ILogger<ProcessingThePhoneForDailyCallCommandHandler> logger)
        {
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _planHandleCallDailyTab = planHandleCallDailyTab;
            _loanTab = loanTab;
            _customerTab = customerTab;
            _configUserMapSmartDialerTab = configUserMapSmartDialerTab;
            _assgimentLoanTab = assgimentLoanTab;
            _planCloseLoanTab = planCloseLoanTab;
            _commentDebtPromptedTab = commentDebtPromptedTab;
            _configHoldCodeForSmartDialerTab = configHoldCodeForSmartDialerTab;
            _loanCreditDetailLosTab = loanCreditDetailLosTab;
            _paymentScheduleTab = paymentScheduleTab;
        }

        public async Task<ResponseActionResult> Handle(ProcessingThePhoneForDailyCallCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            response.SetSucces();
            var currentDate = DateTime.Now;
            try
            {
                var firstDateOfMonth = new DateTime(currentDate.Year, currentDate.Month, 1);
                var firtDateOfNextMonth = firstDateOfMonth.AddMonths(1);
                var exists = await _planHandleCallDailyTab.SetGetTop(1).WhereClause(x => x.CallDate == currentDate.Date).QueryAsync();
                if (exists != null && exists.Any())
                {
                    return response;
                }
                // lấy danh sách nv dùng smartdialer
                var lstUserConfig = await _configUserMapSmartDialerTab.WhereClause(x => x.Status == (int)LMS.Common.Constants.StatusCommon.Active).QueryAsync();
                if (lstUserConfig == null || !lstUserConfig.Any())
                {
                    return response;
                }
                var dictUserConfigInfos = lstUserConfig.GroupBy(x => x.UserID).ToDictionary(x => x.Key, x => x.FirstOrDefault());
                var lstUserID = dictUserConfigInfos.Keys.ToList();
                var lstLoanAssign = await _assgimentLoanTab.WhereClause(x => x.DateAssigned >= firstDateOfMonth.Date && x.DateApplyTo < firtDateOfNextMonth.Date)
                                                     .WhereClause(x => lstUserID.Contains(x.UserID) && x.Status == (int)StatusCommon.Active)
                                                     .QueryAsync();
                if (lstLoanAssign == null || !lstLoanAssign.Any())
                {
                    return response;
                }
                var dictLoanAssignInfos = lstLoanAssign.GroupBy(x => x.LoanID).ToDictionary(x => x.Key, x => x.Select(x => x.UserID).ToList());

                // lấy danh sách đơn vay có ngày phải đóng từ -5 trở lại
                var dayInDueStart = currentDate.AddDays(6);
                var dayCallMaxBeforeCheckCode = currentDate.AddDays(-6);
                if (dayInDueStart.Date > firtDateOfNextMonth.Date)
                {
                    dayInDueStart = firtDateOfNextMonth.Date; // ngày đầu tháng sau
                }
                if (dayCallMaxBeforeCheckCode < firstDateOfMonth)
                {
                    dayCallMaxBeforeCheckCode = firstDateOfMonth;
                }
                // các đơn phải thanh toán trong tháng
                var loanInfo = await _loanTab.SelectColumns(x => x.LoanID, x => x.CustomerID, x => x.NextDate, x => x.LoanCreditIDOfPartner, x => x.ContactCode, x => x.CustomerName)
                                             .WhereClause(x => x.NextDate < firtDateOfNextMonth.Date && RuleCollectionServiceHelper.LstStatusLoanLending.Contains((int)x.Status)).QueryAsync();
                if (loanInfo == null || !loanInfo.Any())
                {
                    return response;
                }

                var configHoldcodeInfos = (await _configHoldCodeForSmartDialerTab.WhereClause(x => x.Status == (int)StatusCommon.Active).QueryAsync()).FirstOrDefault();
                List<string> lstHoldCode = configHoldcodeInfos.ReasonCodes.Split(',').ToList();

                // chỉ xử lý đơn của nv có smartdialer
                Dictionary<long, Domain.Tables.TblPlanHandleCallDaily> dictCustomerCallInsert = new Dictionary<long, Domain.Tables.TblPlanHandleCallDaily>();

                List<long> lstLoanID = new List<long>(); // dùng để tính số tiền của kh phải cắt kỳ
                List<long> lstCustomerID = new List<long>(); // dùng để tính số tiền của kh phải cắt kỳ
                Dictionary<long, long> dictLoanIDOfCustomer = new Dictionary<long, long>();
                // xử lý đơn có điều kiện cần trước
                {
                    foreach (var item in loanInfo)
                    {
                        if (!dictLoanAssignInfos.ContainsKey(item.LoanID))
                        {
                            continue;
                        }
                        var lstUserIDHandle = dictLoanAssignInfos.GetValueOrDefault(item.LoanID);
                        if (lstUserIDHandle == null || lstUserIDHandle.Count < 1)
                        {
                            continue;
                        }
                        // nếu có 2 nhân viên có smart dialer cùng gọi 1 kh
                        // lấy mặc đinh nv đầu tiên
                        // log tạm thời nếu có trường hợp trên
                        if (lstUserIDHandle.Count > 1)
                        {
                            _logger.LogError($"Đơn {item.ContactCode} có > 1 nv smartdialer xử lý {string.Join(",", lstUserIDHandle)}");
                        }
                        var userConfig = dictUserConfigInfos.GetValueOrDefault(lstUserIDHandle.First());
                        if (userConfig == null || userConfig.CampaignID < 1)
                        {
                            _logger.LogError($"Đơn {item.ContactCode} không lấy được thông tin cấu hình cuộc gọi của nhân viên");
                            continue;
                        }
                        long customerIDCurent = item.CustomerID.GetValueOrDefault();
                        lstLoanID.Add(item.LoanID);
                        lstCustomerID.Add(customerIDCurent);
                        if (!dictLoanIDOfCustomer.ContainsKey(item.LoanID))
                        {
                            dictLoanIDOfCustomer.Add(item.LoanID, customerIDCurent);
                        }
                        var dpdCurrent = currentDate.Date.Subtract(item.NextDate.GetValueOrDefault().Date).Days;
                        if (!dictCustomerCallInsert.ContainsKey(customerIDCurent))
                        {
                            dictCustomerCallInsert.Add(customerIDCurent, new Domain.Tables.TblPlanHandleCallDaily
                            {
                                CustomerID = customerIDCurent,
                                PriorityOrder = 0, // giá trị chỉ gán 1 lần
                                CampaignID = userConfig.CampaignID,
                                CallDate = currentDate,
                                Status = (int)PlanHandleCallDaily_Status.Waiting,
                                UserID = userConfig.UserID,
                                CreateDate = currentDate,
                                ModifyDate = currentDate,
                                CustomerCallName = item.CustomerName,
                                CallResult = (int)PlanHandleCallDaily_CallResult.Waiting,
                                CallMaxCount = RuleCollectionServiceHelper.SmartDialerMaxCountRetry,
                                DPD = dpdCurrent
                            });
                        }
                        string loanIDs = dictCustomerCallInsert[customerIDCurent].LoanIDs;
                        if (string.IsNullOrEmpty(loanIDs))
                        {
                            loanIDs += $"{item.LoanID}";
                        }
                        else
                        {
                            loanIDs += $",{item.LoanID}";
                        }
                        if (dpdCurrent > 0 && dictCustomerCallInsert[customerIDCurent].PriorityOrder != (int)PlanHandleCallDaily_PriorityOrder.DPDOverDue)
                        {
                            dictCustomerCallInsert[customerIDCurent].PriorityOrder = (int)PlanHandleCallDaily_PriorityOrder.DPDOverDue;
                        }
                        else if (dictCustomerCallInsert[customerIDCurent].PriorityOrder == 0)
                        {
                            dictCustomerCallInsert[customerIDCurent].PriorityOrder = (int)PlanHandleCallDaily_PriorityOrder.DPDInDue;
                        }
                        dictCustomerCallInsert[customerIDCurent].LoanIDs = loanIDs;
                        // lấy dpd nhỏ nhất của kh khi quá hạn
                        if (dpdCurrent > 0)
                        {
                            if (dpdCurrent < dictCustomerCallInsert[customerIDCurent].DPD && dictCustomerCallInsert[customerIDCurent].DPD > 0)
                            {
                                dictCustomerCallInsert[customerIDCurent].DPD = dpdCurrent;
                            }
                            else if (dictCustomerCallInsert[customerIDCurent].DPD <= 0)
                            {
                                dictCustomerCallInsert[customerIDCurent].DPD = dpdCurrent;
                            }
                        }
                        else
                        {
                            if (dpdCurrent > dictCustomerCallInsert[customerIDCurent].DPD)
                                dictCustomerCallInsert[customerIDCurent].DPD = dpdCurrent;
                        }
                    }
                }
                lstCustomerID = lstCustomerID.Distinct().ToList();
                lstLoanID = lstLoanID.Distinct().ToList();
                // bỏ kh đủ tiền cắt kỳ
                var dictPlanCloseLoanInfosTask = GetDictPlanCloseLoanInfos(lstLoanID);
                var dictCustomerInfosTask = GetDictCustomerInfos(lstCustomerID);
                var dictCustomerHasCommentInfosTask = GetDictCustomerHasCommentInfos(lstCustomerID, dayCallMaxBeforeCheckCode, currentDate, configHoldcodeInfos);
                var dictLoanPaymentScheduleInfosTask = GetDictPaymentScheduleLoanInfos(lstLoanID);
                await Task.WhenAll(dictPlanCloseLoanInfosTask, dictCustomerInfosTask, dictCustomerHasCommentInfosTask, dictLoanPaymentScheduleInfosTask);

                var dictPlanCloseLoanInfos = dictPlanCloseLoanInfosTask.Result;
                var dictCustomerInfos = dictCustomerInfosTask.Result;
                var dictCustomerHasCommentInfos = dictCustomerHasCommentInfosTask.Result;
                var dictLoanPaymentScheduleInfos = dictLoanPaymentScheduleInfosTask.Result;
                List<Domain.Tables.TblPlanHandleCallDaily> lstInsert = new List<Domain.Tables.TblPlanHandleCallDaily>();
                lstLoanID.Clear(); // xóa các loan đã xử lý trước đó
                foreach (var item in dictCustomerCallInsert)
                {
                    try
                    {
                        // chỉ lấy những đơn có ngày phải thanh toán trong khoảng -5, 0
                        if (item.Value.DPD < -5)
                        {
                            continue;
                        }
                        var customerInfo = dictCustomerInfos.GetValueOrDefault(item.Key);
                        if (customerInfo == null || customerInfo.CustomerID < 1)
                        {
                            _logger.LogError($"Không tìm thấy KH của đơn {item.Value.LoanIDs}");
                            continue;
                        }
                        List<long> lstLoanOfCustomer = item.Value.LoanIDs.Split(",").Select(Int64.Parse).ToList();
                        foreach (var loanID in lstLoanOfCustomer)
                        {
                            var planCloseLoan = dictPlanCloseLoanInfos.GetValueOrDefault(loanID);
                            item.Value.TotalMoneyNeedPay += dictLoanPaymentScheduleInfos.GetValueOrDefault(loanID);
                            item.Value.TotalMoneyOriginal += planCloseLoan.MoneyOriginal; // tổng tiền gốc của các đơn vay trong tháng
                        }
                        // đủ tiền cắt kỳ bỏ qua
                        if (customerInfo.TotalMoney >= item.Value.TotalMoneyNeedPay)
                        {
                            continue;
                        }
                        // valid sô dt
                        if (string.IsNullOrEmpty(customerInfo.Phone) || customerInfo.Phone.Length != TimaSettingConstant.MaxLengthPhone || !Regex.Match(customerInfo.Phone, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success)
                        {
                            _logger.LogError($"Không tìm thấy số dt của KH {customerInfo.FullName}|loanIDs={item.Value.LoanIDs}");
                            continue;
                        }
                        item.Value.NumberPhone = customerInfo.Phone;
                        var lstCommentOfCustomer = dictCustomerHasCommentInfos.GetValueOrDefault(item.Key);

                        // điều kiện chung: tồn tại 1 comment có code giữ trong khoảng T-X + 1, T -> không gọi
                        dayCallMaxBeforeCheckCode = currentDate.Date.AddDays((configHoldcodeInfos.SkipTime * -1) + 1);
                        if (lstCommentOfCustomer != null && lstCommentOfCustomer.Any())
                        {
                            var commentCodeIgnore = lstCommentOfCustomer.Where(x => x.CreateDate > dayCallMaxBeforeCheckCode
                                                                                && x.CreateDate < currentDate.Date.AddDays(1)
                                                                                && lstHoldCode.Contains(x.ReasonCode)).FirstOrDefault();
                            if (commentCodeIgnore != null && commentCodeIgnore.CustomerID > 0)
                            {
                                continue;
                            }
                        }
                        if (item.Value.DPD > 0)
                        {                           
                            if (item.Value.DPD < RuleCollectionServiceHelper.SmartDialerMaxDPDCallRef && currentDate.Day >= RuleCollectionServiceHelper.SmartDialerDayOverBorderCallRef)
                            {
                                // lấy thông tin tham chiếu của KH này
                                lstLoanID.AddRange(lstLoanOfCustomer);
                            }
                            else if (item.Value.DPD >= RuleCollectionServiceHelper.SmartDialerMaxDPDCallRef)
                            {
                                // lấy thông tin tham chiếu
                                item.Value.CallMaxCount = RuleCollectionServiceHelper.SmartDialerMaxCountRetry - 1;
                                lstLoanID.AddRange(lstLoanOfCustomer);
                            }
                            lstInsert.Add(item.Value);
                        }
                        else if (item.Value.DPD < 0)
                        {
                            // kh thuộc nhóm L bỏ qua
                            if (customerInfo.PayTypeID == (int)Customer_PayTypeID.Low)
                            {
                                continue;
                            }
                            // tồn tại 1 comment trong khoang t-5, t-1
                            dayCallMaxBeforeCheckCode = currentDate.Date.AddDays(-6);
                            if (lstCommentOfCustomer != null && lstCommentOfCustomer.Any())
                            {
                                var commentCodeIgnore = lstCommentOfCustomer.Where(x => x.CreateDate > dayCallMaxBeforeCheckCode
                                                                                  && x.CreateDate < currentDate.Date
                                                                                  && lstHoldCode.Contains(x.ReasonCode)).FirstOrDefault();
                                if (commentCodeIgnore != null && commentCodeIgnore.CustomerID > 0)
                                {
                                    continue;
                                }
                            }
                            lstInsert.Add(item.Value);
                        }
                        else
                        {
                            // == 0
                            lstInsert.Add(item.Value);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"Có lỗi xảy ra khi xử lý KH customerID={item.Key}|ex={ex.Message}-{ex.StackTrace}");
                    }
                }
                if (lstLoanID.Count > 0)
                {
                    // lấy thông tin tham chiếu
                    lstLoanID = lstLoanID.Distinct().ToList();
                    // dữ liệu nhiều khả năng chết
                    var dictLoanDetailLosInfos = await GetDictLoanDetailLosInfos(lstLoanID);
                    var dictPhoneHandled = new Dictionary<string, string>();
                    foreach (var item in dictLoanDetailLosInfos)
                    {
                        var customerID = dictLoanIDOfCustomer.GetValueOrDefault(item.Key);
                        var customerCall = dictCustomerCallInsert.GetValueOrDefault(customerID);
                        int priorityOrder = (int)PlanHandleCallDaily_PriorityOrder.ReferLevle1;
                        if (item.Value != null && item.Value.listRelativesAndColleagues != null && item.Value.listRelativesAndColleagues.Count > 0)
                        {
                            foreach (var rel in item.Value.listRelativesAndColleagues)
                            {
                                if (string.IsNullOrEmpty(rel.phone))
                                {
                                    continue;
                                }
                                if (dictPhoneHandled.ContainsKey(rel.phone))
                                {
                                    continue;
                                }
                                dictPhoneHandled.Add(rel.phone, rel.phone);
                                var callRef = customerCall.DeepCopyByExpressionTree();
                                callRef.PriorityOrder = priorityOrder;
                                callRef.CustomerCallName = rel.fullName;
                                callRef.NumberPhone = rel.phone;
                                callRef.CallMaxCount = 2;
                                lstInsert.Add(callRef);
                                priorityOrder += 1;
                            }
                        }
                    }
                }

                if (lstInsert.Count > 0)
                {
                    _planHandleCallDailyTab.InsertBulk(lstInsert);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessingThePhoneForDailyCallCommand|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private async Task<Dictionary<long, Domain.Tables.TblPlanCloseLoan>> GetDictPlanCloseLoanInfos(List<long> lstLoanID)
        {
            Dictionary<long, Domain.Tables.TblPlanCloseLoan> dictResult = new Dictionary<long, Domain.Tables.TblPlanCloseLoan>();
            try
            {
                long loanIDMin = long.MaxValue;
                long loanIDMax = 0;
                _planCloseLoanTab.WhereClause(x => x.CloseDate == DateTime.Now.Date);
                if (lstLoanID.Count > TimaSettingConstant.MaxItemQuery)
                {
                    foreach (var item in lstLoanID)
                    {
                        if (loanIDMin > item)
                        {
                            loanIDMin = item;
                        }
                        if (loanIDMax < item)
                        {
                            loanIDMax = item;
                        }
                    }
                    loanIDMin -= 1;
                    loanIDMax += 1;
                    _planCloseLoanTab.WhereClause(x => x.LoanID > loanIDMin && x.LoanID < loanIDMax);
                }
                else
                {
                    _planCloseLoanTab.WhereClause(x => lstLoanID.Contains(x.LoanID));
                }
                var lstLoanInfo = await _planCloseLoanTab.QueryAsync();
                if (lstLoanInfo == null || !lstLoanInfo.Any())
                {
                    return dictResult;
                }
                var dictLoanID = lstLoanID.ToDictionary(x => x, x => x);
                foreach (var item in lstLoanInfo)
                {
                    if (!dictLoanID.ContainsKey(item.LoanID))
                    {
                        continue;
                    }
                    if (!dictResult.ContainsKey(item.LoanID))
                        dictResult.Add(item.LoanID, item);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetDictPlanCloseLoanInfos|loanID={_common.ConvertObjectToJSonV2(lstLoanID)}|{ex.Message}-{ex.StackTrace}");
            }
            return dictResult;
        }

        private async Task<Dictionary<long, Domain.Tables.TblCustomer>> GetDictCustomerInfos(List<long> lstCustomerID)
        {
            Dictionary<long, Domain.Tables.TblCustomer> dictResult = new Dictionary<long, Domain.Tables.TblCustomer>();
            try
            {
                long customerIDMin = long.MaxValue;
                long customerIDMax = 0;
                if (lstCustomerID.Count > TimaSettingConstant.MaxItemQuery)
                {
                    foreach (var item in lstCustomerID)
                    {
                        if (customerIDMin > item)
                        {
                            customerIDMin = item;
                        }
                        if (customerIDMax < item)
                        {
                            customerIDMax = item;
                        }
                    }
                    customerIDMin -= 1;
                    customerIDMax += 1;
                    _customerTab.WhereClause(x => x.CustomerID > customerIDMin && x.CustomerID < customerIDMax);
                }
                else
                {
                    _customerTab.WhereClause(x => lstCustomerID.Contains(x.CustomerID));
                }
                var lstCustomerInfos = await _customerTab.QueryAsync();
                if (lstCustomerInfos == null || !lstCustomerInfos.Any())
                {
                    return dictResult;
                }
                var dictCustomerID = lstCustomerID.ToDictionary(x => x, x => x);
                foreach (var item in lstCustomerInfos)
                {
                    if (!dictCustomerID.ContainsKey(item.CustomerID))
                    {
                        continue;
                    }
                    if (!dictResult.ContainsKey(item.CustomerID))
                        dictResult.Add(item.CustomerID, item);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetDictCustomerInfos|lstCustomerID={_common.ConvertObjectToJSonV2(lstCustomerID)}|{ex.Message}-{ex.StackTrace}");
            }
            return dictResult;
        }

        private async Task<Dictionary<long, List<Domain.Tables.TblCommentDebtPrompted>>> GetDictCustomerHasCommentInfos(List<long> lstCustomerID, DateTime fromDate, DateTime todate, Domain.Tables.TblConfigHoldCodeForSmartDialer configHoldcodeInfos)
        {
            Dictionary<long, List<Domain.Tables.TblCommentDebtPrompted>> dictResult = new Dictionary<long, List<Domain.Tables.TblCommentDebtPrompted>>();
            try
            {
                //var configHoldcodeInfos = (await _configHoldCodeForSmartDialerTab.WhereClause(x => x.Status == (int)StatusCommon.Active).QueryAsync()).FirstOrDefault();
                List<string> lstHoldCode = configHoldcodeInfos.ReasonCodes.Split(',').ToList();

                long customerIDMin = long.MaxValue;
                long customerIDMax = 0;
                _commentDebtPromptedTab.WhereClause(x => x.CreateDate > fromDate.Date && x.CreateDate < todate);
                if (lstCustomerID.Count > TimaSettingConstant.MaxItemQuery)
                {
                    foreach (var item in lstCustomerID)
                    {
                        if (customerIDMin > item)
                        {
                            customerIDMin = item;
                        }
                        if (customerIDMax < item)
                        {
                            customerIDMax = item;
                        }
                    }
                    customerIDMin -= 1;
                    customerIDMax += 1;
                    _commentDebtPromptedTab.WhereClause(x => x.CustomerID > customerIDMin && x.CustomerID < customerIDMax);
                }
                else
                {
                    _commentDebtPromptedTab.WhereClause(x => lstCustomerID.Contains(x.CustomerID));
                }
                var lstCommentInfos = await _commentDebtPromptedTab.SelectColumns(x => x.CustomerID, x => x.ReasonCode, x => x.CreateDate)
                                                                   .WhereClause(x => lstHoldCode.Contains(x.ReasonCode))
                                                                   .QueryAsync();
                if (lstCommentInfos == null || !lstCommentInfos.Any())
                {
                    return dictResult;
                }
                var dictCustomerID = lstCustomerID.ToDictionary(x => x, x => x);
                foreach (var item in lstCommentInfos)
                {
                    if (!dictCustomerID.ContainsKey(item.CustomerID))
                    {
                        continue;
                    }
                    if (!dictResult.ContainsKey(item.CustomerID))
                    {
                        dictResult.Add(item.CustomerID, new List<Domain.Tables.TblCommentDebtPrompted>());
                    }
                    dictResult[item.CustomerID].Add(item);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetDictCustomerHasComment|lstCustomerID={_common.ConvertObjectToJSonV2(lstCustomerID)}|{ex.Message}-{ex.StackTrace}");
            }
            return dictResult;
        }

        private async Task<Dictionary<long, LMS.Entites.Dtos.LOSServices.LoanBriefDetail>> GetDictLoanDetailLosInfos(List<long> lstLoanID)
        {
            Dictionary<long, LMS.Entites.Dtos.LOSServices.LoanBriefDetail> dictResult = new Dictionary<long, LMS.Entites.Dtos.LOSServices.LoanBriefDetail>();
            try
            {
                long loanIDMin = long.MaxValue;
                long loanIDMax = 0;
                if (lstLoanID.Count > TimaSettingConstant.MaxItemQuery)
                {
                    foreach (var item in lstLoanID)
                    {
                        if (loanIDMin > item)
                        {
                            loanIDMin = item;
                        }
                        if (loanIDMax < item)
                        {
                            loanIDMax = item;
                        }
                    }
                    loanIDMin -= 1;
                    loanIDMax += 1;
                    _loanCreditDetailLosTab.WhereClause(x => x.LoanID > loanIDMin && x.LoanID < loanIDMax);
                }
                else
                {
                    _loanCreditDetailLosTab.WhereClause(x => lstLoanID.Contains(x.LoanID));
                }
                var lstLoanInfo = await _loanCreditDetailLosTab.QueryAsync();
                if (lstLoanInfo == null || !lstLoanInfo.Any())
                {
                    return dictResult;
                }
                var dictLoanID = lstLoanID.ToDictionary(x => x, x => x);
                foreach (var item in lstLoanInfo)
                {
                    if (!dictLoanID.ContainsKey(item.LoanID))
                    {
                        continue;
                    }
                    if (!dictResult.ContainsKey(item.LoanID))
                    {
                        try
                        {
                            LMS.Entites.Dtos.LOSServices.LoanBriefDetail detail = _common.ConvertJSonToObjectV2<LMS.Entites.Dtos.LOSServices.LoanBriefDetail>(item.JsonExtra);
                            dictResult.Add(item.LoanID, detail);
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError($"GetDictLoanDetailLosInfos_JsonExtra|loanID={item.LoanID}|{ex.Message}-{ex.StackTrace}");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetDictLoanDetailLosInfos|loanID={_common.ConvertObjectToJSonV2(lstLoanID)}|{ex.Message}-{ex.StackTrace}");
            }
            return dictResult;
        }

        private async Task<Dictionary<long, long>> GetDictPaymentScheduleLoanInfos(List<long> lstLoanID)
        {
            Dictionary<long, long> dictResult = new Dictionary<long, long>();
            try
            {
                long loanIDMin = long.MaxValue;
                long loanIDMax = 0;
                DateTime currentDate = DateTime.Now;
                DateTime firdateOfNextMonth = new DateTime(currentDate.Year, currentDate.Month, 1).AddMonths(1);
                _paymentScheduleTab.WhereClause(x => x.PayDate < firdateOfNextMonth && x.Status == (int)PaymentSchedule_Status.Use && x.IsVisible == false);
                if (lstLoanID.Count > TimaSettingConstant.MaxItemQuery)
                {
                    foreach (var item in lstLoanID)
                    {
                        if (loanIDMin > item)
                        {
                            loanIDMin = item;
                        }
                        if (loanIDMax < item)
                        {
                            loanIDMax = item;
                        }
                    }
                    loanIDMin -= 1;
                    loanIDMax += 1;
                    _paymentScheduleTab.WhereClause(x => x.LoanID > loanIDMin && x.LoanID < loanIDMax);
                }
                else
                {
                    _paymentScheduleTab.WhereClause(x => lstLoanID.Contains(x.LoanID));
                }
                var lstPaymentScheduleInfos = await _paymentScheduleTab.QueryAsync();
                if (lstPaymentScheduleInfos == null || !lstPaymentScheduleInfos.Any())
                {
                    return dictResult;
                }
                var dictLoanID = lstLoanID.ToDictionary(x => x, x => x);
                foreach (var item in lstPaymentScheduleInfos)
                {
                    if (!dictLoanID.ContainsKey(item.LoanID))
                    {
                        continue;
                    }
                    if (!dictResult.ContainsKey(item.LoanID))
                        dictResult.Add(item.LoanID, 0);
                    dictResult[item.LoanID] += item.MoneyConsultant + item.MoneyFineInterestLate + item.MoneyFineLate + item.MoneyInterest + item.MoneyOriginal + item.MoneyService
                            - (item.PayMoneyConsultant + item.PayMoneyFineInterestLate + item.PayMoneyFineLate + item.PayMoneyInterest + item.PayMoneyOriginal + item.PayMoneyService);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetDictPaymentScheduleLoanInfos|loanID={_common.ConvertObjectToJSonV2(lstLoanID)}|{ex.Message}-{ex.StackTrace}");
            }
            return dictResult;
        }
    }
}
