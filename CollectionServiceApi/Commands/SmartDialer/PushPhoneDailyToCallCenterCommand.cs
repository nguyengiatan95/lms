﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.SmartDialer
{
    public class PushPhoneDailyToCallCenterCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int Status { get; set; } = (int)PlanHandleCallDaily_Status.Waiting;
        public int PriorityOrder { get; set; }
    }
    public class PushPhoneDailyToCallCenterCommandHandler : IRequestHandler<PushPhoneDailyToCallCenterCommand, ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleCallDaily> _planHandleCallDailyTab;
        readonly ILogger<PushPhoneDailyToCallCenterCommandHandler> _logger;
        //readonly LMS.Common.Helper.Utils _common;
        readonly RestClients.ISmartDialerGateway _smartDialerGateway;
        public PushPhoneDailyToCallCenterCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPlanHandleCallDaily> planHandleCallDailyTab,
            RestClients.ISmartDialerGateway smartDialerGateway,
            ILogger<PushPhoneDailyToCallCenterCommandHandler> logger)
        {
            _logger = logger;
            //_common = new LMS.Common.Helper.Utils();
            _planHandleCallDailyTab = planHandleCallDailyTab;
            _smartDialerGateway = smartDialerGateway;
        }

        public async Task<ResponseActionResult> Handle(PushPhoneDailyToCallCenterCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            response.SetSucces();
            try
            {
                List<Domain.Tables.TblPlanHandleCallDaily> lstUpdate = new List<Domain.Tables.TblPlanHandleCallDaily>();
                List<int> lstStatus = new List<int>
                {
                    (int)PlanHandleCallDaily_Status.Waiting,
                    (int)PlanHandleCallDaily_Status.Pushed,
                    (int)PlanHandleCallDaily_Status.Handled
                };
                if (!lstStatus.Contains(request.Status))
                {
                    _logger.LogError($"PushPhoneDailyToCallCenterCommandHandler|Status_InValid");
                    return response;
                }
                _planHandleCallDailyTab.WhereClause(x => x.CallDate == DateTime.Now.Date);
                if (request.PriorityOrder > 0)
                {
                    _planHandleCallDailyTab.WhereClause(x => x.PriorityOrder == request.PriorityOrder);
                }
                _planHandleCallDailyTab.WhereClause(x => x.Status == request.Status);
                var lstPhoneDailyInDay = await _planHandleCallDailyTab.QueryAsync();
                if (lstPhoneDailyInDay == null || !lstPhoneDailyInDay.Any())
                {
                    return response;
                }
                //lstPhoneDailyInDay = lstPhoneDailyInDay.OrderBy(x => x.PriorityOrder);//.ThenBy(x => x.Status).ThenBy(x => x.DPD).ThenByDescending(x => x.TotalMoneyOriginal);

                Dictionary<int, Dictionary<long, List<Domain.Tables.TblPlanHandleCallDaily>>> dictPriorityOrderPushInfos = new Dictionary<int, Dictionary<long, List<Domain.Tables.TblPlanHandleCallDaily>>>();

                foreach (var item in lstPhoneDailyInDay)
                {
                    if (!dictPriorityOrderPushInfos.ContainsKey(item.PriorityOrder))
                    {
                        dictPriorityOrderPushInfos.Add(item.PriorityOrder, new Dictionary<long, List<Domain.Tables.TblPlanHandleCallDaily>>());
                    }
                    var dictCampaignPush = dictPriorityOrderPushInfos[item.PriorityOrder];
                    if (!dictCampaignPush.ContainsKey(item.CampaignID))
                    {
                        dictCampaignPush.Add(item.CampaignID, new List<Domain.Tables.TblPlanHandleCallDaily>());
                    }
                    dictCampaignPush[item.CampaignID].Add(item);
                }
                Dictionary<long, LMS.Entites.Dtos.SmartDialer.ImportCampaignRequest> dictCampaignImport = new Dictionary<long, LMS.Entites.Dtos.SmartDialer.ImportCampaignRequest>();
                // thứ tự đã được sắp xếp ở trên
                foreach (var item in dictPriorityOrderPushInfos)
                {
                    // sắp xếp lại 
                    var dictCampaignHandle = item.Value;
                    List<Domain.Tables.TblPlanHandleCallDaily> lstPhoneHandled = new List<Domain.Tables.TblPlanHandleCallDaily>();
                    foreach (var cam in dictCampaignHandle)
                    {
                        if (dictCampaignImport.ContainsKey(cam.Key))
                        {
                            continue;
                        }
                        // priorityorder = 1: sort DPD tăng dần, dư nợ giảm gian
                        // priorityorder = 2: sort DPD giảm dần, dự nợ giảm dần  
                        if (item.Key == (int)PlanHandleCallDaily_PriorityOrder.DPDInDue)
                        {
                            lstPhoneHandled = cam.Value.OrderBy(x => x.CallCount).ThenByDescending(x => x.DPD).ThenByDescending(x => x.TotalMoneyOriginal).ToList();
                        }
                        else
                        {
                            lstPhoneHandled = cam.Value.OrderBy(x => x.CallCount).ThenBy(x => x.DPD).ThenByDescending(x => x.TotalMoneyOriginal).ToList();
                        }
                        foreach (var phone in lstPhoneHandled)
                        {
                            if (request.Status != (int)PlanHandleCallDaily_Status.Pushed && phone.Status == (int)PlanHandleCallDaily_Status.Pushed)
                            {
                                continue;
                            }
                            if (!dictCampaignImport.ContainsKey(cam.Key))
                            {
                                dictCampaignImport.Add(cam.Key, new LMS.Entites.Dtos.SmartDialer.ImportCampaignRequest
                                {
                                    CampaignId = cam.Key,
                                    Data = new List<LMS.Entites.Dtos.SmartDialer.ImportCampaignPhoneItem>()
                                });
                            }
                            dictCampaignImport[cam.Key].Data.Add(new LMS.Entites.Dtos.SmartDialer.ImportCampaignPhoneItem
                            {
                                Phone = phone.NumberPhone
                            });
                            if (phone.Status == (int)PlanHandleCallDaily_Status.Waiting)
                            {
                                phone.Status = (int)PlanHandleCallDaily_Status.Pushed;
                                phone.ModifyDate = DateTime.Now;
                                //phone.CallCount += 1;
                                lstUpdate.Add(phone);
                            }
                        }
                    }
                }
                if (lstUpdate.Count > 0)
                {
                    _planHandleCallDailyTab.UpdateBulk(lstUpdate);
                }
                foreach (var item in dictCampaignImport)
                {
                    if (item.Value.Data.Count > 0)
                    {
                        _ = _smartDialerGateway.ImportCampaign(item.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"PushPhoneDailyToCallCenterCommandHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
