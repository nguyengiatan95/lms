﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using CollectionServiceApi.Domain.Tables;
using System.Globalization;
using CollectionServiceApi.Constants;

namespace CollectionServiceApi.Commands.SendSms
{
    public class UpdateStatusSendSmsCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long SendSmsID { get; set; }
        public int Status { get; set; }

    }
    public class UpdateStatusSendSmsCommandHandler : IRequestHandler<UpdateStatusSendSmsCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSendSms> _sendSmsTab;
        ILogger<UpdateStatusSendSmsCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdateStatusSendSmsCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSendSms> sendSmsTab,
            ILogger<UpdateStatusSendSmsCommandHandler> logger
            )
        {
            _sendSmsTab = sendSmsTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(UpdateStatusSendSmsCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objSendSms = (await _sendSmsTab.WhereClause(x => x.SendSmsID == request.SendSmsID).QueryAsync()).FirstOrDefault();
                if (objSendSms == null)
                {
                    response.Message = MessageConstant.SendSmsIDNoteExist;
                    return response;
                }
                objSendSms.Status = request.Status;
                objSendSms.ModifyDate = DateTime.Now;
                var UpdateSendSms = await _sendSmsTab.UpdateAsync(objSendSms);
                if (UpdateSendSms)
                {
                    response.SetSucces();
                    response.Data = objSendSms.SendSmsID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateStatusSendSmsCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
