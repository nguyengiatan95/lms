﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using CollectionServiceApi.Domain.Tables;
using System.Globalization;
using CollectionServiceApi.Constants;
using CollectionServiceApi.Services;

namespace CollectionServiceApi.Commands.SendSms
{
    public class CreateSendSmsCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public string TemplateName { get; set; }

        public string ContentSms { get; set; }

        public string PhoneSend { get; set; }

        public string PhoneReceived { get; set; }

        public long CreateBy { get; set; }
    }
    public class CreateSendSmsCommandHandler : IRequestHandler<CreateSendSmsCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblSendSms> _sendSmsTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> _commentDebtPromptedTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblReason> _reasonTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly ILogger<CreateSendSmsCommandHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        readonly IQueueCallAPIManager _queueCallAPIManager;
        public CreateSendSmsCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSendSms> sendSmsTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCommentDebtPrompted> commentDebtPromptedTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblReason> reasonTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            ILogger<CreateSendSmsCommandHandler> logger,
            IQueueCallAPIManager queueCallAPIManager
            )
        {
            _sendSmsTab = sendSmsTab;
            _commentDebtPromptedTab = commentDebtPromptedTab;
            _userTab = userTab;
            _reasonTab = reasonTab;
            _loanTab = loanTab;
            _logger = logger;
            _queueCallAPIManager=queueCallAPIManager;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateSendSmsCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var date = DateTime.Now;
                var objUser = (await _userTab.WhereClause(x => x.UserID == request.CreateBy).QueryAsync()).FirstOrDefault();
                if (objUser == null)
                {
                    response.Message = MessageConstant.NotIdentifiedUserID;
                    return response;
                }
                var objLoan = (await _loanTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                if (objLoan == null)
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                var objSendSms = new Domain.Tables.TblSendSms()
                {
                    TemplateName = request.TemplateName,
                    ContentSms = _common.ConvertToUnSign3(request.ContentSms),
                    PhoneSend = request.PhoneSend,
                    PhoneReceived = request.PhoneReceived,
                    CreateBy = request.CreateBy,
                    CreateDate = date,
                    ModifyDate = date,
                    Status = (int)SendSms_Status.Wait
                };
                var sendSmsID = await _sendSmsTab.InsertAsync(objSendSms);
                if (sendSmsID > 0)
                {
                    var objReason = (await _reasonTab.WhereClause(x => x.ReasonCode == SendSmsContants.ReasonCode && x.Status == (int)Reason_Status.Active).QueryAsync()).FirstOrDefault();
                    var CommentDebtPromptedReq = new TblCommentDebtPrompted
                    {
                        LoanID = request.LoanID,
                        TimaLoanID = objLoan.TimaLoanID,
                        UserID = objUser.UserID,
                        FullName = objUser.FullName,
                        Comment = request.TemplateName,
                        ReasonCodeID = (long)objReason?.ReasonID,
                        ReasonCode = objReason?.ReasonCode,
                        CreateDate = date,
                        IsDisplay = (int)CommentDebtPrompted_IsDisplay.Active,
                        AppointmentDate = date,
                        CustomerID = objLoan.CustomerID.GetValueOrDefault()
                    };
                    var commentDebtPromptedID = await _commentDebtPromptedTab.InsertAsync(CommentDebtPromptedReq);
                    if (commentDebtPromptedID > 0)
                    {
                        DateTime dtNow = DateTime.Now;
                        _ = await _queueCallAPIManager.InsertQueueComment(objLoan.TimaLoanID, request.TemplateName
                         , (int)objReason.ReasonID, commentDebtPromptedID, objUser.TimaUserID, objUser.UserName, dtNow);
                    }
                    response.SetSucces();
                    response.Data = sendSmsID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateSendSmsCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
