﻿using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.LoanExemption
{
    public class CancelLoanExemptionByExpiredCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
    }
    public class CancelLoanExemptionByExpiredCommandHandler : IRequestHandler<CancelLoanExemptionByExpiredCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemption> _loanExemptionTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLoanExemptionAction> _logLoanExemptionActionTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebtBalance> _approvalLevelBookDebtBalanceTab;
        readonly LMS.Common.DAL.ITableHelper<TblUserApprovalLevel> _userApprovalLevelTab;
        readonly ILogger<CancelLoanExemptionByExpiredCommandHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        public CancelLoanExemptionByExpiredCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemption> loanExemptionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLoanExemptionAction> logLoanExemptionActionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebtBalance> approvalLevelBookDebtBalanceTab,
            LMS.Common.DAL.ITableHelper<TblUserApprovalLevel> userApprovalLevelTab,
            ILogger<CancelLoanExemptionByExpiredCommandHandler> logger)
        {
            _loanExemptionTab = loanExemptionTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _logLoanExemptionActionTab = logLoanExemptionActionTab;
            _approvalLevelBookDebtBalanceTab = approvalLevelBookDebtBalanceTab;
            _userApprovalLevelTab = userApprovalLevelTab;
        }

        public async Task<ResponseActionResult> Handle(CancelLoanExemptionByExpiredCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            response.SetSucces();
            try
            {
                var currentDate = DateTime.Now;
                // chỉ cho hủy khi đầu tháng
                if (currentDate.Day != 1)
                {
                    return response;
                }
                List<int> lstLoanExemptionStatusIgnored = new List<int>
                {
                    (int)LoanExemption_Status.Closed,
                    (int)LoanExemption_Status.Cancel
                };
                var lstLoanExemptionExpired = await _loanExemptionTab.WhereClause(x => x.ExpirationDate < currentDate.Date && !lstLoanExemptionStatusIgnored.Contains(x.Status)).QueryAsync();
                if (lstLoanExemptionExpired == null || !lstLoanExemptionExpired.Any())
                {
                    return response;
                }
                List<TblLogLoanExemptionAction> lstInsertLog = new List<TblLogLoanExemptionAction>();
                foreach (var item in lstLoanExemptionExpired)
                {
                    item.Status = (int)LoanExemption_Status.Cancel;
                    item.ApprovedBy = TimaSettingConstant.UserIDAdmin;
                    item.ApproveByFullName = TimaSettingConstant.UserNameAdmin;
                    item.ModifyDate = currentDate;
                    lstInsertLog.Add(new TblLogLoanExemptionAction
                    {
                        ActionType = (int)LogLoanExemptionAction_ActionType.Cancel,
                        CreateBy = TimaSettingConstant.UserIDAdmin,
                        CreateByName = TimaSettingConstant.UserNameAdmin,
                        CreateDate = DateTime.Now,
                        LoanExemptionID = item.LoanExemptionID
                    });
                }
                _loanExemptionTab.UpdateBulk(lstLoanExemptionExpired);
                _logLoanExemptionActionTab.InsertBulk(lstInsertLog);
                // cập nhật lại tháng trước
                await UpdateMoneyCurrentLevelApproval(currentDate.AddDays(-1));
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestLoanExemptionCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        public async Task UpdateMoneyCurrentLevelApproval(DateTime currentDate)
        {
            try
            {
                var firstDateOfMonth = new DateTime(currentDate.Year, currentDate.Month, 01);
                var firtDateOfNextMonth = firstDateOfMonth.AddMonths(1);
                var lstDataTask = _loanExemptionTab.WhereClause(x => x.Status == (int)LoanExemption_Status.Closed && x.CreateDate > firstDateOfMonth && x.CreateDate < firtDateOfNextMonth).QueryAsync(); // đã tất toán thành công
                var lstUserApprovalTask = _userApprovalLevelTab.QueryAsync();
                var lstSettingTask = _approvalLevelBookDebtBalanceTab.QueryAsync();
                await Task.WhenAll(lstDataTask, lstUserApprovalTask, lstSettingTask);
                var lstData = lstDataTask.Result;
                var lstUserApproval = lstUserApprovalTask.Result;
                var lstSetting = lstSettingTask.Result;
                Dictionary<long, List<long>> dictLevelApprovedLoan = new Dictionary<long, List<long>>();
                foreach (var item in lstData)
                {
                    // lấy ng approve
                    var approvalLeverDetail = lstUserApproval.FirstOrDefault(x => x.UserID == item.ApprovedBy && x.ApprovalLevelBookDebtID == item.ApprovalLevelBookDebtID);
                    if (approvalLeverDetail == null)
                    {
                        continue;
                    }
                    var approvalLevelID = approvalLeverDetail.ApprovalLevelBookDebtID.Value;
                    if (!dictLevelApprovedLoan.ContainsKey(approvalLevelID))
                    {
                        dictLevelApprovedLoan.Add(approvalLevelID, new List<long>());
                    }
                    dictLevelApprovedLoan[approvalLevelID].Add(item.LoanID);
                    var settingDetail = lstSetting.FirstOrDefault(x => x.ApprovalLevelBookDebtID == approvalLevelID);
                    settingDetail.TotalMoneyCurrent -= (item.MoneyOriginal + item.MoneyInterest + item.MoneyService + item.MoneyFineOriginal + item.MoneyFineLate + item.MoneyConsultant
                                                        - (item.PayMoneyOriginal + item.PayMoneyInterest + item.PayMoneyService + item.PayMoneyFineOriginal + item.PayMoneyFineLate + item.PayMoneyConsultant));
                }
                foreach (var item in lstSetting)
                {
                    if (dictLevelApprovedLoan.ContainsKey(item.ApprovalLevelBookDebtID))
                    {
                        item.LstLoanIDHandle = _common.ConvertObjectToJSonV2(dictLevelApprovedLoan[item.ApprovalLevelBookDebtID]);
                    }
                    _approvalLevelBookDebtBalanceTab.Update(item);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateMoneyCurrentLevelApproval|currentDate={currentDate:dd/MM/yyyy}|ex={ex.Message}-{ex.StackTrace}");
            }
        }
    }
}
