﻿using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.LoanExemption
{
    public class UploadFilesLoanExemptionCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanExemptionID { get; set; }

        public List<Domain.Models.LoanExemption.UploadFileLoanExemptionModel> FileUploads { get; set; }
        public long CreateBy { get; set; }
    }
    public class UploadFilesLoanExemptionCommandHandler : IRequestHandler<UploadFilesLoanExemptionCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> _excelReportTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemption> _loanExemptionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemptionFileUpload> _loanExemptionFileUploadTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> _approvalLevelBookDebtTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingInterestReduction> _settingInterestReductionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingTotalMoney> _settingTotalMoneyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLoanExemptionAction> _logLoanExemptionActionTab;
        ILogger<UploadFilesLoanExemptionCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UploadFilesLoanExemptionCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> excelReportTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemption> loanExemptionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemptionFileUpload> loanExemptionFileUploadTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> approvalLevelBookDebtTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingInterestReduction> settingInterestReductionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingTotalMoney> settingTotalMoneyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLoanExemptionAction> logLoanExemptionActionTab,
            ILogger<UploadFilesLoanExemptionCommandHandler> logger)
        {
            _excelReportTab = excelReportTab;
            _loanExemptionTab = loanExemptionTab;
            _loanExemptionFileUploadTab = loanExemptionFileUploadTab;
            _approvalLevelBookDebtTab = approvalLevelBookDebtTab;
            _settingInterestReductionTab = settingInterestReductionTab;
            _settingTotalMoneyTab = settingTotalMoneyTab;
            _loanTab = loanTab;
            _userTab = userTab;
            _logLoanExemptionActionTab = logLoanExemptionActionTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(UploadFilesLoanExemptionCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var userInfosTask = _userTab.SetGetTop(1).WhereClause(x => x.UserID == request.CreateBy).QueryAsync();
                var loanExemptionTask = _loanExemptionTab.SetGetTop(1).WhereClause(x => x.LoanExemptionID == request.LoanExemptionID).QueryAsync();
                await Task.WhenAll(userInfosTask, loanExemptionTask);
                var loanExemptionDetail = loanExemptionTask.Result.FirstOrDefault();
                if (loanExemptionDetail.CreateBy != request.CreateBy)
                {
                    response.Message = MessageConstant.ExcelReport_TraceIDNotExistForLoanExemption;
                    return response;
                }
                List<int> LstStatusValid = new List<int> { (int)LoanExemption_Status.WaitFileUpload, (int)LoanExemption_Status.HasFileUpload };
                if (!LstStatusValid.Contains(loanExemptionDetail.Status))
                {
                    response.Message = MessageConstant.NotChangeInvoice;
                    return response;
                }
                var currentDate = DateTime.Now;
                loanExemptionDetail.Status = (int)LoanExemption_Status.HasFileUpload;
                loanExemptionDetail.ModifyDate = currentDate;
                LoanExemptionExtraData loanExemptionExtraDataChange = _common.ConvertJSonToObjectV2<LoanExemptionExtraData>(loanExemptionDetail.ExtraData);
                loanExemptionExtraDataChange.LstUserChange.Add(new LoanExemptionExtraDataUserChangeItem
                {
                    ApprovalLevelBookDebtID = loanExemptionDetail.ApprovalLevelBookDebtID,
                    CreateTime = currentDate,
                    UserID = request.CreateBy
                });
                List<Domain.Tables.TblLoanExemptionFileUpload> lstFileInserts = new List<Domain.Tables.TblLoanExemptionFileUpload>();
                // ảnh cũ nếu có thì xóa đi
                var lstFileOld = await _loanExemptionFileUploadTab.WhereClause(x => x.LoanExemptionID == loanExemptionDetail.LoanExemptionID && x.Status == (int)StatusCommon.Active).QueryAsync();
                if (lstFileOld != null && lstFileOld.Any())
                {
                    foreach (var item in lstFileOld)
                    {
                        item.Status = (int)StatusCommon.UnActive;
                        item.ModifyBy = request.CreateBy;
                        item.ModifyDate = currentDate;
                    }
                    _loanExemptionFileUploadTab.UpdateBulk(lstFileOld);
                }
                foreach (var item in request.FileUploads)
                {
                    lstFileInserts.Add(new Domain.Tables.TblLoanExemptionFileUpload
                    {
                        CreateBy = request.CreateBy,
                        ModifyBy = request.CreateBy,
                        CreateDate = currentDate,
                        ModifyDate = currentDate,
                        Status = (int)StatusCommon.Active,
                        TypeFile = item.TypeFile,
                        LstImg = _common.ConvertObjectToJSonV2(item.FileImages),
                        LoanExemptionID = loanExemptionDetail.LoanExemptionID,
                        LoanID = loanExemptionDetail.LoanID
                    });
                }
                var t1 = _loanExemptionTab.UpdateAsync(loanExemptionDetail);
                var t2 = _logLoanExemptionActionTab.InsertAsync(new TblLogLoanExemptionAction
                {
                    ActionType = (int)LogLoanExemptionAction_ActionType.UploadFile,
                    CreateBy = request.CreateBy,
                    CreateByName = userInfosTask.Result.FirstOrDefault().UserName,
                    CreateDate = DateTime.Now,
                    LoanExemptionID = loanExemptionDetail.LoanExemptionID
                });
                _loanExemptionFileUploadTab.InsertBulk(lstFileInserts);
                await Task.WhenAll(t1, t2);
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestLoanExemptionCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
