﻿using CollectionServiceApi.Domain.Tables;
using CollectionServiceApi.RestClients;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.LoanExemption
{
    public class UpdateStatusLoanExemptionCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanExemptionID { get; set; }
        /// <summary>
        /// enum LoanExemption_NextStep
        /// </summary>
        public int Status { get; set; }
        public string ReasonCancel { get; set; }
        public long CreateBy { get; set; }
    }
    public class UpdateStatusLoanExemptionCommandHandler : IRequestHandler<UpdateStatusLoanExemptionCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemption> _loanExemptionTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> _approvalLevelBookDebtTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingInterestReduction> _settingInterestReductionTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingTotalMoney> _settingTotalMoneyTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblHistoryApprovalLevelBookHandleInMonth> _historyApprovalLevelBookHandleInMonthTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserApprovalLevel> _userApprovedLevelTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLoanExemptionAction> _logLoanExemptionActionTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebtBalance> _approvalLevelBookDebtBalanceTab;
        readonly ILogger<UpdateStatusLoanExemptionCommandHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        readonly IMediator _mediator;
        public UpdateStatusLoanExemptionCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemption> loanExemptionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> approvalLevelBookDebtTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingInterestReduction> settingInterestReductionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingTotalMoney> settingTotalMoneyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblHistoryApprovalLevelBookHandleInMonth> historyApprovalLevelBookHandleInMonthTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserApprovalLevel> userApprovedLevelTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLoanExemptionAction> logLoanExemptionActionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebtBalance> approvalLevelBookDebtBalanceTab,
            IMediator mediator,
            ILogger<UpdateStatusLoanExemptionCommandHandler> logger)
        {
            _loanExemptionTab = loanExemptionTab;
            _approvalLevelBookDebtTab = approvalLevelBookDebtTab;
            _settingInterestReductionTab = settingInterestReductionTab;
            _settingTotalMoneyTab = settingTotalMoneyTab;
            _userTab = userTab;
            _historyApprovalLevelBookHandleInMonthTab = historyApprovalLevelBookHandleInMonthTab;
            _userApprovedLevelTab = userApprovedLevelTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _logLoanExemptionActionTab = logLoanExemptionActionTab;
            _approvalLevelBookDebtBalanceTab = approvalLevelBookDebtBalanceTab;
            _mediator = mediator;
        }

        public async Task<ResponseActionResult> Handle(UpdateStatusLoanExemptionCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var currentDate = DateTime.Now;
                var userInfosTask = _userTab.WhereClause(x => x.UserID == request.CreateBy).QueryAsync();
                var loanExemptionTask = _loanExemptionTab.WhereClause(x => x.LoanExemptionID == request.LoanExemptionID).QueryAsync();
                await Task.WhenAll(userInfosTask, loanExemptionTask);
                //todo: với status hủy, check thêm lý do
                // kiểm tra ng thao tác có thuộc nhóm ng phê duyệt không
                // nếu đồng ý phải kiểm tra lại điều kiện cấu hình
                var userInfo = userInfosTask.Result.FirstOrDefault();
                var loanExemptionDetail = loanExemptionTask.Result.FirstOrDefault();
              
                // hết hạn không cho phê duyệt
                if (loanExemptionDetail.ExpirationDate.Date < currentDate.Date && request.Status == (int)LoanExemption_NextStep.Approved)
                {
                    response.Message = MessageConstant.LoanExemption_OverExpirationDate;
                    return response;
                }
                // ng tạo mới được đẩy đơn lên
                if (loanExemptionDetail.Status == (int)LoanExemption_Status.HasFileUpload && loanExemptionDetail.CreateBy != request.CreateBy)
                {
                    response.Message = MessageConstant.LoanExemption_UserApprovalLevelBookDebtNotFound;
                    return response;
                }
                List<int> lstStatusNotCancel = new List<int>
                {
                    (int)LoanExemption_Status.Closed,
                    (int)LoanExemption_Status.Cancel
                };
                LoanExemptionExtraData loanExemptionExtraDataChange = _common.ConvertJSonToObjectV2<LoanExemptionExtraData>(loanExemptionDetail.ExtraData);
                switch ((LoanExemption_NextStep)request.Status)
                {
                    case LoanExemption_NextStep.Cancel:
                        {
                            if (string.IsNullOrEmpty(request.ReasonCancel) || lstStatusNotCancel.Contains(loanExemptionDetail.Status))
                            {
                                response.Message = MessageConstant.ParameterInvalid;
                                return response;
                            }
                            loanExemptionDetail.Status = (int)LoanExemption_Status.Cancel;
                            loanExemptionDetail.ReasonCancel = request.ReasonCancel;
                            loanExemptionDetail.ApprovedBy = request.CreateBy;
                            loanExemptionDetail.ApproveByFullName = userInfo.FullName;
                            loanExemptionDetail.ModifyDate = currentDate;
                            loanExemptionExtraDataChange.LstUserChange.Add(new LoanExemptionExtraDataUserChangeItem
                            {
                                ApprovalLevelBookDebtID = loanExemptionDetail.ApprovalLevelBookDebtID,
                                CreateTime = currentDate,
                                UserID = request.CreateBy
                            });
                            loanExemptionDetail.ExtraData = _common.ConvertObjectToJSonV2(loanExemptionExtraDataChange);
                            var t1 = _logLoanExemptionActionTab.InsertAsync(new TblLogLoanExemptionAction
                            {
                                ActionType = (int)LogLoanExemptionAction_ActionType.Cancel,
                                CreateBy = request.CreateBy,
                                CreateByName = userInfo.UserName,
                                CreateDate = DateTime.Now,
                                LoanExemptionID = request.LoanExemptionID
                            });
                            var t2 = _loanExemptionTab.UpdateAsync(loanExemptionDetail);
                            await Task.WhenAll(t1, t2);
                            if (t2.Result)
                            {
                                await CashBackMoneySetting(loanExemptionDetail.LoanID);
                                response.SetSucces();
                            }
                        }
                        return response;
                    case LoanExemption_NextStep.Approved:
                        var userApproveLevelDetail = (await _userApprovedLevelTab.SetGetTop(1)
                                      .WhereClause(x => x.UserID == request.CreateBy
                                                 && x.ApprovalLevelBookDebtID == loanExemptionDetail.ApprovalLevelBookDebtID
                                                 && x.Status == (int)StatusCommon.Active)
                                      .QueryAsync()).FirstOrDefault();
                        if (userApproveLevelDetail == null || userApproveLevelDetail.UserApprovalLevelID < 1)
                        {
                            response.Message = MessageConstant.LoanExemption_UserApprovalLevelBookDebtNotFound;//  "Thông tin người phê duyệt chưa được cấu hình.";// MessageConstant.ExcelReport_TraceIDNotExist;
                            return response;
                        }
                        if (loanExemptionDetail.ApprovalLevelBookDebtID == 0)
                        {
                            loanExemptionDetail.Status = (int)LoanExemption_Status.WaitApprove;
                            loanExemptionDetail.ModifyDate = currentDate;
                            loanExemptionDetail.ApprovalLevelBookDebtID = userApproveLevelDetail.MoveToApprovalLevelBookDebtID.GetValueOrDefault();
                            loanExemptionExtraDataChange.LstUserChange.Add(new LoanExemptionExtraDataUserChangeItem
                            {
                                ApprovalLevelBookDebtID = loanExemptionDetail.ApprovalLevelBookDebtID,
                                CreateTime = currentDate,
                                UserID = request.CreateBy
                            });
                            loanExemptionDetail.ExtraData = _common.ConvertObjectToJSonV2(loanExemptionExtraDataChange);
                            var t1 = _logLoanExemptionActionTab.InsertAsync(new TblLogLoanExemptionAction
                            {
                                ActionType = (int)LogLoanExemptionAction_ActionType.Approved,
                                CreateBy = request.CreateBy,
                                CreateByName = userInfo.UserName,
                                CreateDate = DateTime.Now,
                                LoanExemptionID = request.LoanExemptionID
                            });
                            var t2 = _loanExemptionTab.UpdateAsync(loanExemptionDetail);
                            await Task.WhenAll(t1, t2);
                            response.SetSucces();
                            return response;
                        }
                        else
                        {
                            List<int> lstStatusValid = new List<int>
                            {
                                (int)LoanExemption_Status.WaitApprove,
                                (int)LoanExemption_Status.Approved
                            };
                            if (!lstStatusValid.Contains(loanExemptionDetail.Status))
                            {
                                response.Message = MessageConstant.ParameterInvalid;
                                return response;
                            }
                            return await ProcessStatusLoanExemption(loanExemptionDetail, userInfo);
                        }
                    default:
                        return response;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestLoanExemptionCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        private async Task<ResponseActionResult> ProcessStatusLoanExemption(Domain.Tables.TblLoanExemption loanExemption, TblUser userInfo)
        {
            ResponseActionResult response = new ResponseActionResult();
            var currentDate = DateTime.Now;
            var firstDateOfMonth = new DateTime(currentDate.Year, currentDate.Month, 1);
            var firtDateOfNextMonth = firstDateOfMonth.AddMonths(1);
            // lấy setting
            var settingReductionTask = _settingInterestReductionTab.WhereClause(x => x.ApprovalLevelBookDebtID == loanExemption.ApprovalLevelBookDebtID && x.BookDebtID == loanExemption.BookDebtID).QueryAsync();
            var historyApprovalTask = _historyApprovalLevelBookHandleInMonthTab
                                                            .WhereClause(x => x.ApprovalLevelBookDebtID == loanExemption.ApprovalLevelBookDebtID)
                                                            .WhereClause(x => x.CreateDate > firstDateOfMonth && x.CreateDate < firtDateOfNextMonth)
                                                            .WhereClause(x => x.Status == (int)StatusCommon.Active)
                                                            .QueryAsync();

            var approvalLevelBookDebtCurrentTask = _approvalLevelBookDebtTab.WhereClause(x => x.ApprovalLevelBookDebtID == loanExemption.ApprovalLevelBookDebtID).QueryAsync();
            var settingTotalMoneyApprovalTask = _settingTotalMoneyTab.WhereClause(x => x.ApprovalLevelBookDebtID == loanExemption.ApprovalLevelBookDebtID).QueryAsync();
            var approvalLevelBalanceTask = GetApproveLeveBookDebtBalanceByApprovalLevelBookDebtID(loanExemption.ApprovalLevelBookDebtID);
            await Task.WhenAll(settingReductionTask, historyApprovalTask, settingTotalMoneyApprovalTask, approvalLevelBookDebtCurrentTask, approvalLevelBalanceTask);

            var approvalLevelBookDebtCurrent = approvalLevelBookDebtCurrentTask.Result.FirstOrDefault();

            // lấy danh sách user phê duyệt theo cấp hiện tại

            var userApprovalLevelTask = _userApprovedLevelTab.WhereClause(x => x.ApprovalLevelBookDebtID == approvalLevelBookDebtCurrent.ApprovalLevelBookDebtID && x.Status == (int)StatusCommon.Active).QueryAsync();
            var approvalLevelBookDebtParentTask = _approvalLevelBookDebtTab.WhereClause(x => x.ApprovalLevelBookDebtID == approvalLevelBookDebtCurrent.ParentID).QueryAsync();

            await Task.WhenAll(userApprovalLevelTask, approvalLevelBookDebtParentTask);
            var approvalLevelBalanceDetail = approvalLevelBalanceTask.Result;
            var approvalLevelBookDebtParent = approvalLevelBookDebtParentTask.Result.FirstOrDefault();
            if (approvalLevelBookDebtCurrent.ParentID != 0 && (approvalLevelBookDebtParent == null || approvalLevelBookDebtParent.ApprovalLevelBookDebtID < 1))
            {
                _logger.LogError($"Không tìm thấy cấp phê duyệt ở trên cho đơn {loanExemption.LoanID}");
                response.Message = $"Không tìm thấy cấp phê duyệt ở trên cho đơn {loanExemption.LoanID}.";
                return response;
            }

            var lstMoneyLoanHandleInsert = new List<TblHistoryApprovalLevelBookHandleInMonth>();
            var dictMoneyTypeApprovalCurrent = settingReductionTask.Result.ToDictionary(x => x.MoneyType, x => x);
            var settingTotalMoneyCurrent = settingTotalMoneyApprovalTask.Result.FirstOrDefault();
            LoanExemptionExtraData loanExemptionExtraDataChange = _common.ConvertJSonToObjectV2<LoanExemptionExtraData>(loanExemption.ExtraData);
            // tính số tiền miễn giảm
            long moneySuggetExemptionOriginal = loanExemption.MoneyOriginal - loanExemption.PayMoneyOriginal;
            long moneySuggetExemptionInterest = loanExemption.MoneyInterest - loanExemption.PayMoneyInterest;
            long moneySuggetExemptionService = loanExemption.MoneyService - loanExemption.PayMoneyService;
            long moneySuggetExemptionConsultant = loanExemption.MoneyConsultant - loanExemption.PayMoneyConsultant;
            long moneySuggetExemptionFineLate = loanExemption.MoneyFineLate - loanExemption.PayMoneyFineLate;
            long moneySuggetExemptionFineOriginal = loanExemption.MoneyFineOriginal - loanExemption.PayMoneyFineOriginal;

            decimal percentMoneyOriginal = GetPercent(loanExemption.MoneyOriginal - loanExemption.PayMoneyOriginal, loanExemption.MoneyOriginal);
            decimal percentMoneyInterest = GetPercent(loanExemption.MoneyInterest - loanExemption.PayMoneyInterest, loanExemption.MoneyInterest);
            decimal percentMoneyService = GetPercent(loanExemption.MoneyService - loanExemption.PayMoneyService, loanExemption.MoneyService);
            decimal percentMoneyConsultant = GetPercent(loanExemption.MoneyConsultant - loanExemption.PayMoneyConsultant, loanExemption.MoneyConsultant);
            decimal percentMoneyFineLate = GetPercent(loanExemption.MoneyFineLate - loanExemption.PayMoneyFineLate, loanExemption.MoneyFineLate);
            decimal percentMoneyFineOriginal = GetPercent(loanExemption.MoneyFineOriginal - loanExemption.PayMoneyFineOriginal, loanExemption.MoneyFineOriginal);

            // tổng tiền đã miễn giảm của cấp phê duyệt
            List<long> lstTypeMoneySetting = _common.ConvertJSonToObjectV2<List<long>>(settingTotalMoneyCurrent.ListTypeMoney);
            //Dictionary<long, long> dictTotalMoneyTypeApproved = new Dictionary<long, long>();
            long moneySuggetExmption = loanExemption.MoneyConsultant + loanExemption.MoneyFineLate + loanExemption.MoneyFineOriginal + loanExemption.MoneyInterest + loanExemption.MoneyOriginal + loanExemption.MoneyService;
            moneySuggetExmption -= (loanExemption.PayMoneyConsultant + loanExemption.PayMoneyFineLate + loanExemption.PayMoneyFineOriginal + loanExemption.PayMoneyInterest + loanExemption.PayMoneyOriginal + loanExemption.PayMoneyService);

            bool hasApproved = true;
            long totalMoneyApproved = 0;
            // hết hạn mức phê duyệt của cấp xử lý
            // so với số tiền cb mgl
            if (approvalLevelBalanceDetail.TotalMoneyCurrent < moneySuggetExmption)
            {
                hasApproved = false;
            }
            foreach (var item in dictMoneyTypeApprovalCurrent)
            {
                // số tiền đã phê duyệt
                //long moneyTypeApproved = dictTotalMoneyTypeApproved.GetValueOrDefault(item.Key);
                switch ((Transaction_TypeMoney)item.Key)
                {
                    case Transaction_TypeMoney.Consultant:
                        if (percentMoneyConsultant > Convert.ToDecimal(item.Value.PercentMoneyReduce)
                            ||
                            (percentMoneyConsultant <= Convert.ToDecimal(item.Value.PercentMoneyReduce)
                            &&
                            item.Value.MaxMoneyReduce > 0 && moneySuggetExemptionConsultant > item.Value.MaxMoneyReduce))
                        {
                            hasApproved = false;
                        }
                        if (moneySuggetExemptionConsultant > 0)
                        {
                            moneySuggetExmption -= moneySuggetExemptionConsultant;
                            lstMoneyLoanHandleInsert.Add(new TblHistoryApprovalLevelBookHandleInMonth
                            {
                                ApprovalLevelBookDebtID = loanExemption.ApprovalLevelBookDebtID,
                                CreateDate = currentDate,
                                ModifyDate = currentDate,
                                LoanID = loanExemption.LoanID,
                                MoneyApproved = moneySuggetExemptionConsultant,
                                MoneyType = (int)item.Key,
                                Status = (int)StatusCommon.Active,
                                TimaLoanID = loanExemption.TimaLoanID,
                                CreateBy = userInfo.UserID
                            });
                            if (lstTypeMoneySetting.Contains(item.Key))
                            {
                                totalMoneyApproved += moneySuggetExemptionConsultant;
                            }
                        }
                        break;
                    case Transaction_TypeMoney.Service:
                        if (percentMoneyService > Convert.ToDecimal(item.Value.PercentMoneyReduce)
                            ||
                            (percentMoneyService <= Convert.ToDecimal(item.Value.PercentMoneyReduce)
                            &&
                            item.Value.MaxMoneyReduce > 0 && moneySuggetExemptionService > item.Value.MaxMoneyReduce))
                        {
                            hasApproved = false;
                        }
                        if (moneySuggetExemptionService > 0)
                        {
                            moneySuggetExmption -= moneySuggetExemptionService;
                            lstMoneyLoanHandleInsert.Add(new TblHistoryApprovalLevelBookHandleInMonth
                            {
                                ApprovalLevelBookDebtID = loanExemption.ApprovalLevelBookDebtID,
                                CreateDate = currentDate,
                                ModifyDate = currentDate,
                                LoanID = loanExemption.LoanID,
                                MoneyApproved = moneySuggetExemptionService,
                                MoneyType = (int)item.Key,
                                Status = (int)StatusCommon.Active,
                                TimaLoanID = loanExemption.TimaLoanID,
                                CreateBy = userInfo.UserID
                            });

                            if (lstTypeMoneySetting.Contains(item.Key))
                            {
                                totalMoneyApproved += moneySuggetExemptionService;
                            }
                        }
                        break;
                    case Transaction_TypeMoney.FineLate:
                        if (percentMoneyFineLate > Convert.ToDecimal(item.Value.PercentMoneyReduce)
                            ||
                            (percentMoneyFineLate <= Convert.ToDecimal(item.Value.PercentMoneyReduce)
                            &&
                            item.Value.MaxMoneyReduce > 0 && moneySuggetExemptionFineLate > item.Value.MaxMoneyReduce))
                        {
                            hasApproved = false;
                        }
                        if (moneySuggetExemptionFineLate > 0)
                        {
                            moneySuggetExmption -= moneySuggetExemptionFineLate;
                            lstMoneyLoanHandleInsert.Add(new TblHistoryApprovalLevelBookHandleInMonth
                            {
                                ApprovalLevelBookDebtID = loanExemption.ApprovalLevelBookDebtID,
                                CreateDate = currentDate,
                                ModifyDate = currentDate,
                                LoanID = loanExemption.LoanID,
                                MoneyApproved = moneySuggetExemptionFineLate,
                                MoneyType = (int)item.Key,
                                Status = (int)StatusCommon.Active,
                                TimaLoanID = loanExemption.TimaLoanID,
                                CreateBy = userInfo.UserID
                            });

                            if (lstTypeMoneySetting.Contains(item.Key))
                            {
                                totalMoneyApproved += moneySuggetExemptionFineLate;
                            }
                        }
                        break;
                    case Transaction_TypeMoney.FineOriginal:
                        if (percentMoneyFineOriginal > Convert.ToDecimal(item.Value.PercentMoneyReduce)
                            ||
                            (percentMoneyFineOriginal <= Convert.ToDecimal(item.Value.PercentMoneyReduce)
                            &&
                            item.Value.MaxMoneyReduce > 0 && moneySuggetExemptionFineOriginal > item.Value.MaxMoneyReduce))
                        {
                            hasApproved = false;
                        }
                        if (moneySuggetExemptionFineOriginal > 0)
                        {
                            moneySuggetExmption -= moneySuggetExemptionFineOriginal;
                            lstMoneyLoanHandleInsert.Add(new TblHistoryApprovalLevelBookHandleInMonth
                            {
                                ApprovalLevelBookDebtID = loanExemption.ApprovalLevelBookDebtID,
                                CreateDate = currentDate,
                                ModifyDate = currentDate,
                                LoanID = loanExemption.LoanID,
                                MoneyApproved = moneySuggetExemptionFineOriginal,
                                MoneyType = (int)item.Key,
                                Status = (int)StatusCommon.Active,
                                TimaLoanID = loanExemption.TimaLoanID,
                                CreateBy = userInfo.UserID
                            });

                            if (lstTypeMoneySetting.Contains(item.Key))
                            {
                                totalMoneyApproved += moneySuggetExemptionFineOriginal;
                            }
                        }
                        break;
                }
            }
            if (moneySuggetExmption > 0)
            {
                lstMoneyLoanHandleInsert.Add(new TblHistoryApprovalLevelBookHandleInMonth
                {
                    ApprovalLevelBookDebtID = loanExemption.ApprovalLevelBookDebtID,
                    CreateDate = currentDate,
                    ModifyDate = currentDate,
                    LoanID = loanExemption.LoanID,
                    MoneyApproved = moneySuggetExmption,
                    MoneyType = 0, // loại tiền ngoài cấu hình phòng phê duyệt phải chịu
                    Status = (int)StatusCommon.Active,
                    TimaLoanID = loanExemption.TimaLoanID,
                    CreateBy = userInfo.UserID
                });
            }
            // 26/01 khi số tiền phê duyệt < 5000
            // không phải xét cấp nào phê duyệt
            if (totalMoneyApproved <= RuleCollectionServiceHelper.MaxMoneyCanBeIngored)
            {
                hasApproved = true;
            }

            // 05-04-2022: bổ sung phê duyệt đồng thời
            if (approvalLevelBookDebtCurrent.IsAllAccept == 1)
            {
                var lstUserApproved = (await _logLoanExemptionActionTab.SelectColumns(x => x.CreateBy)
                                                                       .WhereClause(x => x.LoanExemptionID == loanExemption.LoanExemptionID)
                                                                       .QueryAsync())
                                                                       .Select(x => x.CreateBy).ToList();
                // cộng 1 cho ng đang thao tác duyệt
                lstUserApproved.Add(userInfo.UserID);
                var lstUserApprovalLevel = userApprovalLevelTask.Result.ToList();
                var lstUser = lstUserApprovalLevel.Where(x => lstUserApproved.Contains(x.UserID)).Select(x => x.UserID).ToList();

                if (lstUser.Count != lstUserApprovalLevel.Count)
                {
                    hasApproved = false;
                    approvalLevelBookDebtParent = null; // giữ lại cấp phê duyệt hiện tại
                }
            }
            if (!hasApproved)
            {
                // đẩy lên cấp cha phải duyệt hết cấp con
                if (approvalLevelBookDebtParent != null)
                {
                    loanExemption.ApprovalLevelBookDebtID = approvalLevelBookDebtParent.ApprovalLevelBookDebtID;
                }
                loanExemption.ModifyDate = currentDate;
                loanExemptionExtraDataChange.LstUserChange.Add(new LoanExemptionExtraDataUserChangeItem
                {
                    ApprovalLevelBookDebtID = loanExemption.ApprovalLevelBookDebtID,
                    CreateTime = currentDate,
                    UserID = userInfo.UserID
                });
                loanExemption.ExtraData = _common.ConvertObjectToJSonV2(loanExemptionExtraDataChange);
                var logActionInsert = new TblLogLoanExemptionAction
                {
                    ActionType = (int)LogLoanExemptionAction_ActionType.Approved,
                    CreateBy = userInfo.UserID,
                    CreateByName = userInfo.UserName,
                    CreateDate = DateTime.Now,
                    LoanExemptionID = loanExemption.LoanExemptionID
                };
                var t1 = _logLoanExemptionActionTab.InsertAsync(logActionInsert);
                var t2 = _loanExemptionTab.UpdateAsync(loanExemption);
                await Task.WhenAll(t1, t2);
                if (lstMoneyLoanHandleInsert.Count > 0)
                {
                    _historyApprovalLevelBookHandleInMonthTab.InsertBulk(lstMoneyLoanHandleInsert);
                }
                response.SetSucces();
                return response;
            }
            else
            {
                loanExemption.ModifyDate = currentDate;
                loanExemption.Status = (int)LoanExemption_Status.Approved;
                loanExemption.ApprovedBy = userInfo.UserID;
                loanExemption.ApproveByFullName = userInfo.FullName;
                loanExemptionExtraDataChange.LstUserChange.Add(new LoanExemptionExtraDataUserChangeItem
                {
                    ApprovalLevelBookDebtID = loanExemption.ApprovalLevelBookDebtID,
                    CreateTime = currentDate,
                    UserID = userInfo.UserID
                });
                loanExemption.ExtraData = _common.ConvertObjectToJSonV2(loanExemptionExtraDataChange);
                var logActionInsert = new TblLogLoanExemptionAction
                {
                    ActionType = (int)LogLoanExemptionAction_ActionType.Approved,
                    CreateBy = userInfo.UserID,
                    CreateByName = userInfo.UserName,
                    CreateDate = DateTime.Now,
                    LoanExemptionID = loanExemption.LoanExemptionID
                };
                var t1 = _logLoanExemptionActionTab.InsertAsync(logActionInsert);
                var t2 = _loanExemptionTab.UpdateAsync(loanExemption);
                await Task.WhenAll(t1, t2);
                if (t2.Result)
                {
                    if (lstMoneyLoanHandleInsert.Count > 0)
                    {
                        _historyApprovalLevelBookHandleInMonthTab.InsertBulk(lstMoneyLoanHandleInsert);
                    }
                    userInfo.UserID = TimaSettingConstant.UserIDAdmin;
                    userInfo.UserName = TimaSettingConstant.UserNameAdmin;
                    // trừ số tiền cấp đã phê duyệt
                    // 24/01/2022: bổ sung số tiền duyệt > 5000 mới ghi nhận trừ hạn mức
                    if (totalMoneyApproved > RuleCollectionServiceHelper.MaxMoneyCanBeIngored)
                    {
                        List<long> lstLoanHandle = new List<long>();
                        if (!string.IsNullOrEmpty(approvalLevelBalanceDetail.LstLoanIDHandle))
                        {
                            lstLoanHandle = _common.ConvertJSonToObjectV2<List<long>>(approvalLevelBalanceDetail.LstLoanIDHandle);
                        }
                        lstLoanHandle.Add(loanExemption.LoanID);
                        approvalLevelBalanceDetail.TotalMoneyCurrent -= totalMoneyApproved;
                        approvalLevelBalanceDetail.ModifyDate = DateTime.Now;
                        approvalLevelBalanceDetail.LstLoanIDHandle = _common.ConvertObjectToJSonV2(lstLoanHandle);
                        _ = _approvalLevelBookDebtBalanceTab.UpdateAsync(approvalLevelBalanceDetail);
                    }
                    await _mediator.Send(new TranferAgCloseLoanExemptionCommand
                    {
                        CustomerID = loanExemption.CustomerID,
                        UserID = userInfo.UserID,
                        UserName = userInfo.UserName
                    });
                    //await TranferAgHandle(loanExemption, userInfo);
                }
            }
            response.SetSucces();
            return response;
        }
        private decimal GetPercent(long numberator, long denominator)
        {
            if (denominator == 0)
            {
                denominator = 1;
            }
            return Convert.ToDecimal(numberator * 1.0M / denominator) * 100;
        }

        private async Task CashBackMoneySetting(long loanID)
        {
            var currentDate = DateTime.Now;
            var historyApprovalLoan = await _historyApprovalLevelBookHandleInMonthTab.WhereClause(x => x.LoanID == loanID && x.Status == (int)StatusCommon.Active).QueryAsync();
            if (historyApprovalLoan != null)
            {
                foreach (var item in historyApprovalLoan)
                {
                    item.Status = (int)StatusCommon.UnActive;
                    item.ModifyDate = currentDate;
                }
                _historyApprovalLevelBookHandleInMonthTab.UpdateBulk(historyApprovalLoan);
            }
        }
        private async Task<Domain.Tables.TblApprovalLevelBookDebtBalance> GetApproveLeveBookDebtBalanceByApprovalLevelBookDebtID(long approvalLevelBookDebtID)
        {
            int yearMonthCurrent = int.Parse(DateTime.Now.ToString("yyyyMM"));
            var detail = (await _approvalLevelBookDebtBalanceTab.SetGetTop(1).WhereClause(x => x.ApprovalLevelBookDebtID == approvalLevelBookDebtID && x.YearMonthCurrent == yearMonthCurrent).QueryAsync()).FirstOrDefault();
            if (detail == null || detail.ApprovalLevelBookDebtBalanceID < 1)
            {
                var settingTotalMoneyApprovalDetail = (await _settingTotalMoneyTab.WhereClause(x => x.ApprovalLevelBookDebtID == approvalLevelBookDebtID).QueryAsync()).FirstOrDefault();
                await _approvalLevelBookDebtBalanceTab.InsertAsync(new TblApprovalLevelBookDebtBalance
                {
                    ApprovalLevelBookDebtID = settingTotalMoneyApprovalDetail.ApprovalLevelBookDebtID,
                    CreateDate = DateTime.Now,
                    ModifyDate = DateTime.Now,
                    TotalMoneyCurrent = settingTotalMoneyApprovalDetail.TotalMoney,
                    TotalMoneySetting = settingTotalMoneyApprovalDetail.TotalMoney,
                    YearMonthCurrent = yearMonthCurrent
                });
                return await GetApproveLeveBookDebtBalanceByApprovalLevelBookDebtID(approvalLevelBookDebtID);
            }
            return detail;
        }
    }
}
