﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.LoanExemption
{
    public class CreateRequestLoanExemptionCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string TraceIDRequest { get; set; }
        public long CreateBy { get; set; }
    }
    public class CreateRequestLoanExemptionCommandHandler : IRequestHandler<CreateRequestLoanExemptionCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> _excelReportTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemption> _loanExemptionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserApprovalLevel> _userApprovedLevelTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBookDebt> _bookDebtTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLoanExemptionAction> _logLoanExemptionActionTab;
        ILogger<CreateRequestLoanExemptionCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        IMediator _mediator;
        public CreateRequestLoanExemptionCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> excelReportTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemption> loanExemptionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserApprovalLevel> userApprovedLevelTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBookDebt> bookDebtTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLoanExemptionAction> logLoanExemptionActionTab,
            IMediator mediator,
            ILogger<CreateRequestLoanExemptionCommandHandler> logger)
        {
            _excelReportTab = excelReportTab;
            _loanExemptionTab = loanExemptionTab;
            _loanTab = loanTab;
            _userTab = userTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _userApprovedLevelTab = userApprovedLevelTab;
            _logLoanExemptionActionTab = logLoanExemptionActionTab;
            _bookDebtTab = bookDebtTab;
            _mediator = mediator;
        }
        public async Task<ResponseActionResult> Handle(CreateRequestLoanExemptionCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var userInfosTask = _userTab.SetGetTop(1).WhereClause(x => x.UserID == request.CreateBy).QueryAsync();
                var excelReportsTask = _excelReportTab.SetGetTop(1).WhereClause(x => x.TraceIDRequest == request.TraceIDRequest).QueryAsync();
                var userApproveLevelInfosTask = _userApprovedLevelTab.SetGetTop(1).WhereClause(x => x.UserID == request.CreateBy && x.ApprovalLevelBookDebtID == 0).QueryAsync();
                await Task.WhenAll(userInfosTask, excelReportsTask, userApproveLevelInfosTask);
                var excelReports = excelReportsTask.Result;
                if (excelReports == null || !excelReports.Any())
                {
                    response.Message = MessageConstant.ExcelReport_TraceIDNotExistForLoanExemption;//"Không xác định được dữ liệu MGL.";// MessageConstant.ExcelReport_TraceIDNotExist;
                    return response;
                }
                var excelData = excelReports.First();
                if (excelData.Status == (int)ExcelReport_Status.Waiting)
                {
                    response.Message = MessageConstant.ExcelReport_TraceIDNotExistForLoanExemption;// "Không xác định được dữ liệu MGL.";
                    return response;
                }
                if (excelData.Status != (int)ExcelReport_Status.Success)
                {
                    response.Message = MessageConstant.ExcelReport_Error;
                    return response;
                }
                if (excelData.TypeReport != (int)ExcelReport_TypeReport.ExemptionInterest)
                {
                    response.Message = MessageConstant.ExcelReport_TraceIDNotExistForLoanExemption;// MessageConstant.ExcelReport_TraceIDNotExist;
                    return response;
                }
                if (userApproveLevelInfosTask.Result == null || !userApproveLevelInfosTask.Result.Any())
                {
                    response.Message = MessageConstant.LoanExemption_UserApprovalLevelBookDebtNotFound;// "Thông tin người tạo đề xuất chưa được cấu hình.";// MessageConstant.ExcelReport_TraceIDNotExist;
                    return response;
                }
                var userApproveLevelDetail = userApproveLevelInfosTask.Result.FirstOrDefault();
                var currentdate = DateTime.Now;
                try
                {
                    var loanExemptionDetail = _common.ConvertJSonToObjectV2<LMS.Entites.Dtos.CollectionServices.ReportExcel.ReportFormLoanExemptionInterestDetail>(excelData.ExtraData);
                    // todo: kiểm tra đơn đã có chờ duyệt chưa
                    if (loanExemptionDetail.LoanID == 0)
                    {
                        var loanInfo = (await _loanTab.SetGetTop(1).SelectColumns(x => x.LoanID, x => x.TimaLoanID, x => x.TotalMoneyCurrent)
                                                     .WhereClause(x => x.ContactCode == loanExemptionDetail.LoanContractCode).QueryAsync()).FirstOrDefault();
                        loanExemptionDetail.LoanID = loanInfo.LoanID;
                        loanExemptionDetail.TimaLoanID = loanInfo.TimaLoanID;
                        loanExemptionDetail.LoanTotalMoneyCurrent = loanInfo.TotalMoneyCurrent;
                    }
                    // check thêm điều kiện hết hiệu lực so với ngày hiện tại
                    var loanExemptionExists = await _loanExemptionTab.SetGetTop(1)
                                .WhereClause(x => x.LoanID == loanExemptionDetail.LoanID && x.Status != (int)LoanExemption_Status.Cancel)
                                .WhereClause(x => x.ExpirationDate < currentdate.Date)
                                .QueryAsync();
                    if (loanExemptionExists != null && loanExemptionExists.Any())
                    {
                        response.Message = MessageConstant.LoanExemption_Exists;//  "Đã tồn tại đơn MGL và đang trong quá trình chờ duyệt.";
                        return response;
                    }
                    // lấy thông tin cấu hình miễn giảm
                    // lấy thông tin cấp phê duyệt của ng tạo
                    // kiểm tra thông tin đơn vay thuộc cấu hình nào

                    Domain.Tables.TblBookDebt bookDebtDetail = null;
                    var bookDebtInfos = await _bookDebtTab.WhereClause(x => x.YearDebt == loanExemptionDetail.BadDebtYear).QueryAsync();
                    if (bookDebtInfos == null || !bookDebtInfos.Any())
                    {
                        response.Message = MessageConstant.LoanExemption_NotFoundBookDebt;//"Không tìm thấy cấu hình book nợ.";
                        return response;
                    }
                    if (loanExemptionDetail.BadDebtYear == DateTime.Now.Year)
                    {
                        bookDebtDetail = bookDebtInfos.Where(x => x.DPDFrom <= loanExemptionDetail.CountDPD && loanExemptionDetail.CountDPD <= x.DPDTo).FirstOrDefault();
                    }
                    else
                    {
                        bookDebtDetail = bookDebtInfos.Where(x => x.YearDebt == loanExemptionDetail.BadDebtYear).OrderByDescending(x => x.DPDTo).FirstOrDefault();
                    }
                    if (bookDebtDetail == null || bookDebtDetail.BookDebtID < 1)
                    {
                        response.Message = MessageConstant.LoanExemption_NotFoundBookDebt;//"Không tìm thấy cấu hình book nợ.";
                        return response;
                    }

                    long loanExemptionID = await _loanExemptionTab.InsertAsync(new Domain.Tables.TblLoanExemption
                    {
                        CreateBy = request.CreateBy,
                        CloseDate = loanExemptionDetail.CutOffDate,
                        CreateByFullName = userInfosTask.Result.FirstOrDefault().FullName,
                        CreateDate = DateTime.Now,
                        ExpirationDate = loanExemptionDetail.ExpirationDate,
                        LoanID = loanExemptionDetail.LoanID,
                        TimaLoanID = loanExemptionDetail.TimaLoanID,
                        ModifyDate = DateTime.Now,
                        MoneyConsultant = loanExemptionDetail.MoneyCloseLoan.MoneyConsultant,
                        MoneyFineLate = loanExemptionDetail.MoneyCloseLoan.MoneyFineLate,
                        MoneyFineOriginal = loanExemptionDetail.MoneyCloseLoan.MoneyFineOriginal,
                        MoneyInterest = loanExemptionDetail.MoneyCloseLoan.MoneyInterest,
                        MoneyOriginal = loanExemptionDetail.MoneyCloseLoan.MoneyOriginal,
                        MoneyService = loanExemptionDetail.MoneyCloseLoan.MoneyService,
                        PayMoneyConsultant = loanExemptionDetail.MoneyExpected.MoneyConsultant,
                        PayMoneyFineLate = loanExemptionDetail.MoneyExpected.MoneyFineLate,
                        PayMoneyFineOriginal = loanExemptionDetail.MoneyExpected.MoneyFineOriginal,
                        PayMoneyInterest = loanExemptionDetail.MoneyExpected.MoneyInterest,
                        PayMoneyOriginal = loanExemptionDetail.MoneyExpected.MoneyOriginal,
                        PayMoneyService = loanExemptionDetail.MoneyExpected.MoneyService,
                        TotalMoneyCurrent = loanExemptionDetail.LoanTotalMoneyCurrent,
                        Status = (int)LoanExemption_Status.WaitFileUpload,
                        ExtraData = _common.ConvertObjectToJSonV2(new Domain.Tables.LoanExemptionExtraData
                        {
                            ReferTraceIDRequest = request.TraceIDRequest,
                            LstUserChange = new List<Domain.Tables.LoanExemptionExtraDataUserChangeItem>()
                        }),
                        //ApprovalLevelBookDebtID = userApproveLevelDetail.MoveToApprovalLevelBookDebtID.GetValueOrDefault(),
                        BookDebtID = bookDebtDetail.BookDebtID,
                        CustomerID = loanExemptionDetail.CustomerID,
                        ReferTraceIDRequest = request.TraceIDRequest
                    });
                    response.Data = loanExemptionID;
                    var t1 = _logLoanExemptionActionTab.InsertAsync(new Domain.Tables.TblLogLoanExemptionAction
                    {
                        ActionType = (int)LogLoanExemptionAction_ActionType.CreateRequest,
                        CreateBy = request.CreateBy,
                        LoanExemptionID = loanExemptionID,
                        CreateDate = DateTime.Now,
                        CreateByName = userInfosTask.Result.FirstOrDefault().UserName
                    });
                    var t2 = _mediator.Send(new HoldCustomerMoney.CreateHoldCustomerMoneyCommand
                    {
                        CustomerID = loanExemptionDetail.CustomerID,
                        Amount = Int32.MaxValue,
                        CreateBy = request.CreateBy,
                        ForceUpdate = true,
                        UserIDCreate = request.CreateBy,
                        Reason = loanExemptionDetail.Note
                    });
                    await Task.WhenAll(t1, t2);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"CreateRequestLoanExemptionCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ExtraData={excelData.ExtraData}|ex={ex.Message}");
                    response.Message = MessageConstant.ExcelReport_Error;
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestLoanExemptionCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
