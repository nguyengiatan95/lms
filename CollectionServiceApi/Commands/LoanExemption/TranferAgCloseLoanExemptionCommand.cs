﻿using CollectionServiceApi.Domain.Tables;
using CollectionServiceApi.RestClients;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.LoanExemption
{
    public class TranferAgCloseLoanExemptionCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long CustomerID { get; set; }
        public long UserID { get; set; }
        public string UserName { get; set; }
    }
    public class TranferAgCloseLoanExemptionCommandHandler : IRequestHandler<TranferAgCloseLoanExemptionCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> _excelReportTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemption> _loanExemptionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserApprovalLevel> _userApprovedLevelTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBookDebt> _bookDebtTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLoanExemptionAction> _logLoanExemptionActionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        ILogger<TranferAgCloseLoanExemptionCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        IProxyTimaService _proxyTimaService;
        public TranferAgCloseLoanExemptionCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> excelReportTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoanExemption> loanExemptionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserApprovalLevel> userApprovedLevelTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBookDebt> bookDebtTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogLoanExemptionAction> logLoanExemptionActionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            IProxyTimaService proxyTimaService,
            ILogger<TranferAgCloseLoanExemptionCommandHandler> logger)
        {
            _excelReportTab = excelReportTab;
            _loanExemptionTab = loanExemptionTab;
            _loanTab = loanTab;
            _userTab = userTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _userApprovedLevelTab = userApprovedLevelTab;
            _logLoanExemptionActionTab = logLoanExemptionActionTab;
            _bookDebtTab = bookDebtTab;
            _customerTab = customerTab;
            _proxyTimaService = proxyTimaService;
        }
        public async Task<ResponseActionResult> Handle(TranferAgCloseLoanExemptionCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                // kh có > 2 đơn thì lấy đơn nào được duyệt trước
                // bổ sung thêm trạng thái thanh toán thất bại để thanh toán lại
                List<int> lstStatus = new List<int>
                {
                    (int)LoanExemption_Status.Approved,
                    (int)LoanExemption_Status.ClosedFail
                };
                var currentDate = DateTime.Now;
                var loanExemptionTask = _loanExemptionTab.WhereClause(x => x.CustomerID == request.CustomerID && lstStatus.Contains(x.Status) && x.ExpirationDate >= currentDate.Date).QueryAsync();
                var customerInfoTask = _customerTab.SetGetTop(1).SelectColumns(x => x.TotalMoney).WhereClause(x => x.CustomerID == request.CustomerID).QueryAsync();
                await Task.WhenAll(loanExemptionTask, customerInfoTask);
                var totalMoneyCustomerHas = customerInfoTask.Result.FirstOrDefault()?.TotalMoney ?? 0;
                var lstLoanExemptions = loanExemptionTask.Result;
                if (lstLoanExemptions != null && lstLoanExemptions.Any())
                {
                    // lấy đơn được duyệt trước
                    lstLoanExemptions = lstLoanExemptions.OrderBy(x => x.ModifyDate);
                    foreach (var item in lstLoanExemptions)
                    {
                        // kiểm tra tiền của KH có đủ đóng hợp đủ mới call
                        // check tiền kh lms để kiểm tra luôn tiền kh có đồng bộ khớp không
                        long totalMoneyNeedPay = item.PayMoneyOriginal + item.PayMoneyInterest + item.PayMoneyConsultant + item.PayMoneyService + item.PayMoneyFineLate + item.PayMoneyFineOriginal;
                        if (totalMoneyCustomerHas < totalMoneyNeedPay)
                        {
                            break;
                        }
                        totalMoneyCustomerHas -= totalMoneyNeedPay;
                        await TranferAgHandle(item, new TblUser { UserID = request.UserID, UserName = request.UserName });
                    }
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"TranferAgCloseLoanExemptionCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        private async Task TranferAgHandle(Domain.Tables.TblLoanExemption loanExemption, TblUser userInfo, int retry = 0)
        {
            var dataPost = new
            {
                LoanID = loanExemption.TimaLoanID,
                CloseDate = loanExemption.CloseDate.ToString(TimaSettingConstant.DateTimeDayMonthYear),
                MoneyOriginal = loanExemption.PayMoneyOriginal,
                MoneyInterest = loanExemption.PayMoneyInterest,
                MoneyConsultant = loanExemption.PayMoneyConsultant,
                MoneyService = loanExemption.PayMoneyService,
                MoneyFineLate = loanExemption.PayMoneyFineLate,
                MoneyFineOriginal = loanExemption.PayMoneyFineOriginal,
                CreateBy = loanExemption.CreateBy,
                CreateFullName = loanExemption.CreateByFullName,
                ApprovedBy = loanExemption.ApprovedBy,
                ApprovedFullName = loanExemption.ApproveByFullName
            };
            try
            {
                var logActionInsert = new TblLogLoanExemptionAction
                {
                    ActionType = (int)LogLoanExemptionAction_ActionType.WaitPayment,
                    CreateBy = userInfo.UserID,
                    CreateByName = userInfo.UserName,
                    CreateDate = DateTime.Now,
                    LoanExemptionID = loanExemption.LoanExemptionID
                };
                if (retry != 0)
                {
                    logActionInsert.ActionType = (int)LogLoanExemptionAction_ActionType.RetryPayment;
                }
                _ = _logLoanExemptionActionTab.InsertAsync(logActionInsert);
                var actionResult = await _proxyTimaService.CallApiAg(new LMS.Entites.Dtos.AG.AgDataPostModel
                {
                    Path = LMS.Entites.Dtos.AG.AGConstants.CloseLoanByExemption,
                    DataPost = _common.ConvertObjectToJSonV2(dataPost)
                });

                if (actionResult.Result != (int)ResponseAction.Success && retry < 5)
                {
                    await Task.Delay(TimeSpan.FromSeconds(2));
                    await TranferAgHandle(loanExemption, userInfo, ++retry);
                    return;
                }
                ResponseActionResult agResponseResult = _common.ConvertJSonToObjectV2<ResponseActionResult>(actionResult.Data.ToString());
                // parse 2 lần mới lấy thông tin ag trả về
                agResponseResult = _common.ConvertJSonToObjectV2<ResponseActionResult>(agResponseResult.Data.ToString());
                logActionInsert.CreateDate = DateTime.Now;
                if (agResponseResult.Result == (int)ResponseAction.Success)
                {
                    logActionInsert.ActionType = (int)LogLoanExemptionAction_ActionType.PaymentSuccess;
                    loanExemption.Status = (int)LoanExemption_Status.Closed;
                }
                else
                {
                    logActionInsert.ActionType = (int)LogLoanExemptionAction_ActionType.PaymentFail;
                    loanExemption.Status = (int)LoanExemption_Status.ClosedFail;
                    loanExemption.ReasonCancel = agResponseResult.Message;
                }
                loanExemption.ModifyDate = DateTime.Now;
                var t1 = _logLoanExemptionActionTab.InsertAsync(logActionInsert);
                var t2 = _loanExemptionTab.UpdateAsync(loanExemption);
                await Task.WhenAll(t1, t2);
            }
            catch (Exception ex)
            {
                _logger.LogError($"TranferAgHandle|loanExemptionID={loanExemption.LoanExemptionID}|dataPost={_common.ConvertObjectToJSonV2(dataPost)}|Retry={retry}-{ex.Message}-{ex.StackTrace}");
            }
        }
    }
}
