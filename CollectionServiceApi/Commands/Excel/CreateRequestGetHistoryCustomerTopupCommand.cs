﻿using CollectionServiceApi.Domain.Models.Excel;
using CollectionServiceApi.Domain.Models.Invoice;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.Excel
{
    public class CreateRequestGetHistoryCustomerTopupCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public RequestHistoryCustomerTopupModel Model { get; set; }
    }
    public class CreateRequestGetHistoryCustomerTopupCommandHandler : IRequestHandler<CreateRequestGetHistoryCustomerTopupCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> _excelReportTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        ILogger<CreateRequestGetHistoryCustomerTopupCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        Services.IReportManager _reportManager;
        RestClients.ILoanService _loanService;
        private const int MaxTimeWaitGetNext = 30; // số phút chờ lấy mới
        IMediator _mediator;
        public CreateRequestGetHistoryCustomerTopupCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> excelReportTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            Services.IReportManager reportManager,
            RestClients.ILoanService loanService,
            IMediator mediator,
            ILogger<CreateRequestGetHistoryCustomerTopupCommandHandler> logger
            )
        {
            _loanTab = loanTab;
            _excelReportTab = excelReportTab;
            _transactionTab = transactionTab;
            _customerTab = customerTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _reportManager = reportManager;
            _loanService = loanService;
            _mediator = mediator;
        }

        public async Task<ResponseActionResult> Handle(CreateRequestGetHistoryCustomerTopupCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                DateTime dtNow = DateTime.Now;
                request.Model.PageIndex = 1;
                request.Model.PageSize = Int32.MaxValue;

                var excelReports = await _excelReportTab.SetGetTop(1).SelectColumns(x => x.ExcelReportID, x => x.Status, x => x.CreateDate, x => x.RequestQuery)
                                                        .OrderByDescending(x => x.CreateDate).WhereClause(x => x.CreateBy == request.Model.CreateBy
                                          && x.TypeReport == request.Model.TypeReport).QueryAsync();
                if (excelReports == null || !excelReports.Any())
                {
                    response = await InsertExcelReport(request.Model.CreateBy, request.Model.TypeReport, dtNow, request.Model);
                    return response;
                }

                var excelCurrent = excelReports.FirstOrDefault();
                if (excelCurrent.Status == (int)ExcelReport_Status.Waiting)
                {
                    //++0->thông báo user chờ xử lý xong
                    response.Message = MessageConstant.ExcelReport_WaitingProcess;
                    return response;
                }
                if (excelCurrent.CreateDate.Value.Date == dtNow.Date)
                {
                    // trong 30p đổ lại thì trả về file cũ
                    RequestHistoryCustomerTopupModel requestQuery = _common.ConvertJSonToObjectV2<RequestHistoryCustomerTopupModel>(excelCurrent.RequestQuery);
                    if (requestQuery.FromDate == request.Model.FromDate
                        && requestQuery.ToDate == request.Model.ToDate
                        && requestQuery.KeySearch == request.Model.KeySearch
                        && requestQuery.BankCardID == request.Model.BankCardID
                        && excelCurrent.CreateDate.Value.AddMinutes(MaxTimeWaitGetNext) > dtNow)
                    {
                        response.SetSucces();
                        response.Data = excelCurrent.TraceIDRequest;
                        return response;
                    }
                }
                response = await InsertExcelReport(request.Model.CreateBy, request.Model.TypeReport, dtNow, request.Model);
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestGetHistoryCustomerTopupCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private async Task<ResponseActionResult> InsertExcelReport(long createBy, int typeReport, DateTime createDate, Domain.Models.Invoice.RequestHistoryCustomerTopupModel requestQuery)
        {
            try
            {
                ResponseActionResult response = new ResponseActionResult();
                string traceIDRequest = _common.GenTraceIndentifier();
                long excelID = await _excelReportTab.InsertAsync(new Domain.Tables.TblExcelReport()
                {
                    CreateDate = createDate,
                    CreateBy = createBy,
                    Status = (int)ExcelReport_Status.Waiting,
                    TypeReport = typeReport,
                    TraceIDRequest = traceIDRequest,
                    ModifyDate = createDate,
                    RequestQuery = _common.ConvertObjectToJSonV2(requestQuery)
                });
                if (excelID > 0)
                {
                    response.SetSucces();
                    response.Data = traceIDRequest;
                    _ = ProcessExportHistoryCustomerToup(excelID, requestQuery);
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestGetHistoryCustomerTopupCommandHandler_InsertExcelReport|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }
        private async Task ProcessExportHistoryCustomerToup(long exelID, Domain.Models.Invoice.RequestHistoryCustomerTopupModel request)
        {
            string reportName = $"{((ExcelReport_TypeReport)request.TypeReport).GetDescription()}-{request.FromDate:yyyyMMdd}_{request.ToDate:yyyyMMdd}";
            try
            {
                List<HistoryCustomerTopupItem> lstData = new List<HistoryCustomerTopupItem>();
                int statusExcel = (int)ExcelReport_Status.Recall;
                var actionResult = await _mediator.Send(new Queries.Invoice.SearchHistoryTransactionQuery
                {
                    Model = request
                });

                if (actionResult.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                {
                    lstData = (List<HistoryCustomerTopupItem>)actionResult.Data;
                    statusExcel = (int)ExcelReport_Status.Success;
                }
                _ = UpdateStatusExcel(exelID, statusExcel, _common.ConvertObjectToJSonV2(lstData), reportName);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestGetHistoryCustomerTopupCommandHandler_ProcessFormLoanExemptionInterest|request={_common.ConvertObjectToJSonV2(request)}| ex={ex.Message}-{ex.StackTrace}");
                _ = UpdateStatusExcel(exelID, (int)ExcelReport_Status.Recall, "", reportName);
            }
        }

        private async Task UpdateStatusExcel(long excelID, int status, string extraData, string reportName)
        {
            try
            {
                var excelDetail = (await _excelReportTab.WhereClause(x => x.ExcelReportID == excelID).QueryAsync()).FirstOrDefault();
                excelDetail.Status = status;
                excelDetail.ExtraData = extraData;
                excelDetail.ModifyDate = DateTime.Now;
                excelDetail.ReportName = reportName;
                _ = _excelReportTab.UpdateAsync(excelDetail);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestFormLoanCommandHandler_UpdateStatusExcel|excelID={excelID}|status={status}|jsonExtra={extraData}|ex={ex.Message}-{ex.StackTrace}");
            }
        }
    }
}
