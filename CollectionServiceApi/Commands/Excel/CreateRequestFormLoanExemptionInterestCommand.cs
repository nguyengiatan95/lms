﻿using CollectionServiceApi.Domain.Models.Excel;
using LMS.Common.Constants;
using LMS.Entites.Dtos.CollectionServices.ReportExcel;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.Excel
{
    public class CreateRequestFormLoanExemptionInterestCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public RequestFormLoanExemptionModel LoanExemptionInterestDetail { get; set; }
    }
    public class CreateRequestFormLoanExemptionInterestCommandHandler : IRequestHandler<CreateRequestFormLoanExemptionInterestCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> _excelReportTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        ILogger<CreateRequestFormLoanExemptionInterestCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        RestClients.ILoanService _loanService;
        public CreateRequestFormLoanExemptionInterestCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> excelReportTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            RestClients.ILoanService loanService,
            ILogger<CreateRequestFormLoanExemptionInterestCommandHandler> logger
            )
        {
            _loanTab = loanTab;
            _excelReportTab = excelReportTab;
            _transactionTab = transactionTab;
            _customerTab = customerTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _loanService = loanService;
        }

        public async Task<ResponseActionResult> Handle(CreateRequestFormLoanExemptionInterestCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                request.LoanExemptionInterestDetail.TypeReport = (int)ExcelReport_TypeReport.ExemptionInterest;
                DateTime cutOffDate = DateTime.ParseExact(request.LoanExemptionInterestDetail.CutOfDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                DateTime dtNow = DateTime.Now;
                var loanInfos = await _loanTab.WhereClause(x => x.LoanID == request.LoanExemptionInterestDetail.LoanID).QueryAsync();
                if (loanInfos == null || !loanInfos.Any())
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                var loanDetail = loanInfos.FirstOrDefault();
                // không KT lastDateOfPay = null, case chưa thanh toán kỳ nào
                if (loanDetail.LastDateOfPay.HasValue && loanDetail.LastDateOfPay.Value.Date >= cutOffDate.Date)
                {
                    response.Message = MessageConstant.LoanDebtPaymentIDError;
                    return response;
                }

                var excelReports = await _excelReportTab.SetGetTop(1).OrderByDescending(x => x.CreateDate).WhereClause(x => x.CreateBy == request.LoanExemptionInterestDetail.CreateBy
                                        && x.TypeReport == request.LoanExemptionInterestDetail.TypeReport).QueryAsync();
                if (excelReports == null || !excelReports.Any())
                {
                    response = await InsertExcelReport(request.LoanExemptionInterestDetail.CreateBy, request.LoanExemptionInterestDetail.TypeReport, dtNow, request.LoanExemptionInterestDetail);
                    return response;
                }

                //var excelCurrent = excelReports.FirstOrDefault();
                // tạo phiếu sai về mặt dữ liệu nhiều nên comment tạm
                //if (excelCurrent.CreateDate.Value.Date == cutOffDate.Date)
                //{
                //    // kiểm tra cùng đơn vay ko
                //    RequestFormLoanExemptionModel loanExemptionDetailExist = _common.ConvertJSonToObjectV2<RequestFormLoanExemptionModel>(excelCurrent.RequestQuery);
                //    if (loanExemptionDetailExist.LoanID == request.LoanExemptionInterestDetail.LoanID
                //        && loanExemptionDetailExist.CutOfDate == request.LoanExemptionInterestDetail.CutOfDate
                //        && loanExemptionDetailExist.MoneyExpectedReceive == request.LoanExemptionInterestDetail.MoneyExpectedReceive)
                //    {
                //        response.SetSucces();
                //        response.Data = excelCurrent.TraceIDRequest;
                //        return response;
                //    }
                //}
                response = await InsertExcelReport(request.LoanExemptionInterestDetail.CreateBy, request.LoanExemptionInterestDetail.TypeReport, dtNow, request.LoanExemptionInterestDetail);
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"AddExcelCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private async Task<ResponseActionResult> InsertExcelReport(long createBy, int typeReport, DateTime createDate, RequestFormLoanExemptionModel requestQuery)
        {
            try
            {
                ResponseActionResult response = new ResponseActionResult();
                string traceIDRequest = _common.GenTraceIndentifier();
                long excelID = await _excelReportTab.InsertAsync(new Domain.Tables.TblExcelReport()
                {
                    CreateDate = createDate,
                    CreateBy = createBy,
                    Status = (int)ExcelReport_Status.Waiting,
                    TypeReport = typeReport,
                    TraceIDRequest = traceIDRequest,
                    ModifyDate = createDate,
                    RequestQuery = _common.ConvertObjectToJSonV2(requestQuery)
                });
                if (excelID > 0)
                {
                    response.SetSucces();
                    response.Data = traceIDRequest;
                    _ = ProcessFormLoanExemptionInterest(excelID, requestQuery);
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"AddExcelCommandHandler_InsertExcelReport|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }
        private async Task ProcessFormLoanExemptionInterest(long exelID, RequestFormLoanExemptionModel request)
        {
            string reportName = "";
            try
            {
                DateTime cutOffDate = DateTime.ParseExact(request.CutOfDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                DateTime expirationDate = cutOffDate;
                var currentDate = DateTime.Now;
                var loanInfoTask = _loanTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync();
                var transactionLoanTask = _transactionTab.SelectColumns(x => x.TotalMoney)
                                                        .WhereClause(x => x.LoanID == request.LoanID && x.ActionID != (int)Transaction_Action.ChoVay && x.TotalMoney != 0)
                                                        .QueryAsync();

                var moneCloseLoanActualTask = _loanService.GetMoneyNeedCloseLoanByLoanID(request.LoanID, cutOffDate);
                await Task.WhenAll(loanInfoTask, transactionLoanTask, moneCloseLoanActualTask);

                var loanInfo = loanInfoTask.Result.FirstOrDefault();
                var moneyCloseLoan = moneCloseLoanActualTask.Result;

                if (loanInfo == null || loanInfo.LoanID < 1)
                {
                    _logger.LogError($"CreateRequestFormLoanCommandHandler_Warning|Not_found_loan|request={_common.ConvertObjectToJSonV2(request)}");
                    await UpdateStatusExcel(exelID, (int)ExcelReport_Status.Recall, "", reportName);
                    return;
                }
                if (moneyCloseLoan == null)
                {
                    _logger.LogError($"CreateRequestFormLoanCommandHandler_Warning|Not_found_money_close_loan|request={_common.ConvertObjectToJSonV2(request)}");
                    await UpdateStatusExcel(exelID, (int)ExcelReport_Status.Recall, "", reportName);
                    return;
                }
                // = 0 lấy từ ví hiện có của KH
                if (request.MoneyExpectedReceive == 0)
                {
                    request.MoneyExpectedReceive = (await _customerTab.WhereClause(x => x.CustomerID == loanInfo.CustomerID).QueryAsync()).FirstOrDefault()?.TotalMoney ?? 0;
                }
                // mặc định lấy cuối tháng hiện tại
                if (string.IsNullOrEmpty(request.ExpirationDate))
                {
                    expirationDate = new DateTime(currentDate.Year, currentDate.Month, 1).AddMonths(1).AddDays(-1);
                }
                else
                {
                    expirationDate = DateTime.ParseExact(request.ExpirationDate, TimaSettingConstant.DateTimeDayMonthYear, null);
                }
                int badDebtYear = currentDate.Year;
                if (loanInfo.YearBadDebt.HasValue && loanInfo.YearBadDebt > 0)
                {
                    badDebtYear = loanInfo.YearBadDebt.Value;
                }
                ReportFormLoanExemptionInterestDetail loanExemptionInterestDetail = new ReportFormLoanExemptionInterestDetail
                {
                    CutOffDate = cutOffDate,
                    CreateDate = currentDate,
                    BadDebtYear = badDebtYear,
                    CountDPD = cutOffDate.Date.Subtract(loanInfo.NextDate.Value.Date).Days,
                    CustomerName = loanInfo.CustomerName,
                    LoanContractCode = loanInfo.ContactCode,
                    LoanFromDate = loanInfo.FromDate,
                    LoanProductName = loanInfo.ProductName,
                    LoanToDate = loanInfo.ToDate,
                    LoanTotalMoneyDisbrusement = loanInfo.TotalMoneyDisbursement,
                    LoanTotalMoneyReceived = transactionLoanTask.Result.Sum(x => x.TotalMoney),
                    MoneyCloseLoan = new FormLoanExemptionInterestTypeMoney
                    {
                        MoneyConsultant = moneyCloseLoan.MoneyConsultant,
                        MoneyFineLate = moneyCloseLoan.MoneyFineLate - moneyCloseLoan.DebitMoney,
                        MoneyFineOriginal = moneyCloseLoan.MoneyFineOriginal,
                        MoneyInterest = moneyCloseLoan.MoneyInterest,
                        MoneyOldDebit = moneyCloseLoan.DebitMoney,
                        MoneyOriginal = moneyCloseLoan.MoneyOriginal,
                        MoneyService = moneyCloseLoan.MoneyService,
                        TotalMoneyNeedPay = moneyCloseLoan.TotalMoneyCloseLoan
                    },
                    MoneyExpected = new FormLoanExemptionInterestTypeMoney(),
                    MoneyPercent = new FormLoanExemptionInterestPercent(),
                    Note = request.Note,
                    ApprovedByName = request.ApprovedByName,
                    DepartmentName = request.DepartmentName,
                    ExpirationDate = expirationDate,
                    LoanID = loanInfo.LoanID,
                    TimaLoanID = loanInfo.TimaLoanID,
                    LoanTotalMoneyCurrent = loanInfo.TotalMoneyCurrent,
                    CustomerID = loanInfo.CustomerID.Value
                };

                // tiền miễn giảm dự kiến thu
                long remainMoneyPay = request.MoneyExpectedReceive > moneyCloseLoan.MoneyOriginal ? moneyCloseLoan.MoneyOriginal : request.MoneyExpectedReceive;
                loanExemptionInterestDetail.MoneyExpected.TotalMoneyNeedPay = request.MoneyExpectedReceive;

                // tiền gôc
                loanExemptionInterestDetail.MoneyExpected.MoneyOriginal = remainMoneyPay;
                request.MoneyExpectedReceive -= remainMoneyPay;
                if (request.MoneyExpectedReceive < 0)
                {
                    request.MoneyExpectedReceive = 0;
                }

                remainMoneyPay = request.MoneyExpectedReceive > moneyCloseLoan.MoneyInterest ? moneyCloseLoan.MoneyInterest : request.MoneyExpectedReceive;
                // tiền lãi
                loanExemptionInterestDetail.MoneyExpected.MoneyInterest = remainMoneyPay;
                request.MoneyExpectedReceive -= remainMoneyPay;
                if (request.MoneyExpectedReceive < 0)
                {
                    request.MoneyExpectedReceive = 0;
                }

                remainMoneyPay = request.MoneyExpectedReceive > moneyCloseLoan.DebitMoney ? moneyCloseLoan.DebitMoney : request.MoneyExpectedReceive;
                // tiền nợ cũ
                loanExemptionInterestDetail.MoneyExpected.MoneyOldDebit = remainMoneyPay;
                request.MoneyExpectedReceive -= remainMoneyPay;
                if (request.MoneyExpectedReceive < 0)
                {
                    request.MoneyExpectedReceive = 0;
                }

                remainMoneyPay = request.MoneyExpectedReceive > moneyCloseLoan.MoneyFineOriginal ? moneyCloseLoan.MoneyFineOriginal : request.MoneyExpectedReceive;
                // tất toán trước hạn
                loanExemptionInterestDetail.MoneyExpected.MoneyFineOriginal = remainMoneyPay;
                request.MoneyExpectedReceive -= remainMoneyPay;
                if (request.MoneyExpectedReceive < 0)
                {
                    request.MoneyExpectedReceive = 0;
                }

                remainMoneyPay = request.MoneyExpectedReceive > moneyCloseLoan.MoneyService ? moneyCloseLoan.MoneyService : request.MoneyExpectedReceive;
                // phí dịch vụ
                loanExemptionInterestDetail.MoneyExpected.MoneyService = remainMoneyPay;
                request.MoneyExpectedReceive -= remainMoneyPay;
                if (request.MoneyExpectedReceive < 0)
                {
                    request.MoneyExpectedReceive = 0;
                }

                remainMoneyPay = request.MoneyExpectedReceive > moneyCloseLoan.MoneyConsultant ? moneyCloseLoan.MoneyConsultant : request.MoneyExpectedReceive;
                // phí tư vấn
                loanExemptionInterestDetail.MoneyExpected.MoneyConsultant = remainMoneyPay;
                request.MoneyExpectedReceive -= remainMoneyPay;
                if (request.MoneyExpectedReceive < 0)
                {
                    request.MoneyExpectedReceive = 0;
                }

                remainMoneyPay = request.MoneyExpectedReceive > moneyCloseLoan.MoneyFineLate ? moneyCloseLoan.MoneyFineLate : request.MoneyExpectedReceive;
                // phí phạt treo/ trả chậm
                loanExemptionInterestDetail.MoneyExpected.MoneyFineLate = remainMoneyPay;

                // tiền đề xuất giảm
                loanExemptionInterestDetail.MoneySuggetExemption = new FormLoanExemptionInterestTypeMoney
                {
                    TotalMoneyNeedPay = loanExemptionInterestDetail.MoneyCloseLoan.TotalMoneyNeedPay - loanExemptionInterestDetail.MoneyExpected.TotalMoneyNeedPay,
                    MoneyOriginal = loanExemptionInterestDetail.MoneyCloseLoan.MoneyOriginal - loanExemptionInterestDetail.MoneyExpected.MoneyOriginal,
                    MoneyInterest = loanExemptionInterestDetail.MoneyCloseLoan.MoneyInterest - loanExemptionInterestDetail.MoneyExpected.MoneyInterest,
                    MoneyOldDebit = loanExemptionInterestDetail.MoneyCloseLoan.MoneyOldDebit - loanExemptionInterestDetail.MoneyExpected.MoneyOldDebit,
                    MoneyConsultant = loanExemptionInterestDetail.MoneyCloseLoan.MoneyConsultant - loanExemptionInterestDetail.MoneyExpected.MoneyConsultant,
                    MoneyService = loanExemptionInterestDetail.MoneyCloseLoan.MoneyService - loanExemptionInterestDetail.MoneyExpected.MoneyService,
                    MoneyFineLate = loanExemptionInterestDetail.MoneyCloseLoan.MoneyFineLate - loanExemptionInterestDetail.MoneyExpected.MoneyFineLate,
                    MoneyFineOriginal = loanExemptionInterestDetail.MoneyCloseLoan.MoneyFineOriginal - loanExemptionInterestDetail.MoneyExpected.MoneyFineOriginal
                };
                if (loanExemptionInterestDetail.MoneySuggetExemption.MoneyOriginal > 0)
                {
                    loanExemptionInterestDetail.MoneyPercent.MoneyOriginal = Convert.ToDecimal(Math.Round(loanExemptionInterestDetail.MoneySuggetExemption.MoneyOriginal * 1.0 / loanExemptionInterestDetail.MoneyCloseLoan.MoneyOriginal * 100, 2));
                }
                if (loanExemptionInterestDetail.MoneySuggetExemption.MoneyInterest > 0)
                {
                    loanExemptionInterestDetail.MoneyPercent.MoneyInterest = Convert.ToDecimal(Math.Round(loanExemptionInterestDetail.MoneySuggetExemption.MoneyInterest * 1.0 / loanExemptionInterestDetail.MoneyCloseLoan.MoneyInterest * 100, 2));
                }
                if (loanExemptionInterestDetail.MoneySuggetExemption.MoneyOldDebit > 0)
                {
                    loanExemptionInterestDetail.MoneyPercent.MoneyOldDebit = Convert.ToDecimal(Math.Round(loanExemptionInterestDetail.MoneySuggetExemption.MoneyOldDebit * 1.0 / loanExemptionInterestDetail.MoneyCloseLoan.MoneyOldDebit * 100, 2));
                }
                if (loanExemptionInterestDetail.MoneySuggetExemption.MoneyConsultant > 0)
                {
                    loanExemptionInterestDetail.MoneyPercent.MoneyConsultant = Convert.ToDecimal(Math.Round(loanExemptionInterestDetail.MoneySuggetExemption.MoneyConsultant * 1.0 / loanExemptionInterestDetail.MoneyCloseLoan.MoneyConsultant * 100, 2));
                }
                if (loanExemptionInterestDetail.MoneySuggetExemption.MoneyService > 0)
                {
                    loanExemptionInterestDetail.MoneyPercent.MoneyService = Convert.ToDecimal(Math.Round(loanExemptionInterestDetail.MoneySuggetExemption.MoneyService * 1.0 / loanExemptionInterestDetail.MoneyCloseLoan.MoneyService * 100, 2));
                }
                if (loanExemptionInterestDetail.MoneySuggetExemption.MoneyFineLate > 0)
                {
                    loanExemptionInterestDetail.MoneyPercent.MoneyFineLate = Convert.ToDecimal(Math.Round(loanExemptionInterestDetail.MoneySuggetExemption.MoneyFineLate * 1.0 / loanExemptionInterestDetail.MoneyCloseLoan.MoneyFineLate * 100, 2));
                }
                if (loanExemptionInterestDetail.MoneySuggetExemption.MoneyFineOriginal > 0)
                {
                    loanExemptionInterestDetail.MoneyPercent.MoneyFineOriginal = Convert.ToDecimal(Math.Round(loanExemptionInterestDetail.MoneySuggetExemption.MoneyFineOriginal * 1.0 / loanExemptionInterestDetail.MoneyCloseLoan.MoneyFineOriginal * 100, 2));
                }
                reportName = $"{loanExemptionInterestDetail.CustomerName}-{loanExemptionInterestDetail.LoanContractCode}";
                await UpdateStatusExcel(exelID, (int)ExcelReport_Status.Success, _common.ConvertObjectToJSonV2(loanExemptionInterestDetail), reportName);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestFormLoanCommandHandler_ProcessFormLoanExemptionInterest|request={_common.ConvertObjectToJSonV2(request)}| ex={ex.Message}-{ex.StackTrace}");
                await UpdateStatusExcel(exelID, (int)ExcelReport_Status.Recall, "", reportName);
            }
        }

        private async Task UpdateStatusExcel(long excelID, int status, string extraData, string reportName)
        {
            try
            {
                var excelDetail = (await _excelReportTab.WhereClause(x => x.ExcelReportID == excelID).QueryAsync()).FirstOrDefault();
                excelDetail.Status = status;
                excelDetail.ExtraData = extraData;
                excelDetail.ModifyDate = DateTime.Now;
                excelDetail.ReportName = reportName;
                _ = await _excelReportTab.UpdateAsync(excelDetail);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestFormLoanCommandHandler_UpdateStatusExcel|excelID={excelID}|status={status}|jsonExtra={extraData}|ex={ex.Message}-{ex.StackTrace}");
            }
        }
    }
}
