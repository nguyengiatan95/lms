﻿using CollectionServiceApi.Domain.Models.Excel;
using CollectionServiceApi.Domain.Models.Report;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace CollectionServiceApi.Commands.Excel
{
    public class CreateExportDataDocsCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public LMS.Entites.Dtos.Document.RequestExportDataDocs Model { get; set; }
    }
    public class CreateExportDataDocsCommandHandler : IRequestHandler<CreateExportDataDocsCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> _excelReportTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblTrackingLoanInMonth> _trackingLoanInMonthTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        readonly ILogger<CreateExportDataDocsCommandHandler> _logger;
        readonly LMS.Common.Helper.Utils _common;
        readonly Services.IReportManager _reportManager;
        readonly Services.IExportDataDocsManager _exportDataDocsManager;
        readonly RestClients.ILoanService _loanService;
        readonly IMediator _mediator;
        private const int MaxTimeWaitGetNext = TimaSettingConstant.MaxTimeWaitGetNext; // số phút chờ lấy mới
        public CreateExportDataDocsCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> excelReportTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTrackingLoanInMonth> transactionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            Services.IReportManager reportManager,
            RestClients.ILoanService loanService,
            Services.IExportDataDocsManager exportDataDocsManager,
            IMediator mediator,
            ILogger<CreateExportDataDocsCommandHandler> logger
            )
        {
            _loanTab = loanTab;
            _excelReportTab = excelReportTab;
            _trackingLoanInMonthTab = transactionTab;
            _customerTab = customerTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _reportManager = reportManager;
            _loanService = loanService;
            _exportDataDocsManager = exportDataDocsManager;
            _mediator = mediator;
        }

        public async Task<ResponseActionResult> Handle(CreateExportDataDocsCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                DateTime dtNow = DateTime.Now;
                var excelReports = await _excelReportTab.SetGetTop(1).SelectColumns(x => x.ExcelReportID, x => x.Status, x => x.CreateDate, x => x.RequestQuery, x => x.TraceIDRequest)
                                                        .OrderByDescending(x => x.CreateDate).WhereClause(x => x.CreateBy == request.Model.UserID
                                          && x.TypeReport == request.Model.TypeDoc).QueryAsync();
                if (excelReports != null && excelReports.Any())
                {
                    var excelCurrent = excelReports.FirstOrDefault();
                    if (excelCurrent.Status == (int)ExcelReport_Status.Waiting)
                    {
                        //++0->thông báo user chờ xử lý xong
                        response.Message = MessageConstant.ExcelReport_WaitingProcess;
                        return response;
                    }

                    if (excelCurrent.CreateDate.Value.Date == dtNow.Date)
                    {
                        // trong 30p đổ lại thì trả về file cũ
                        LMS.Entites.Dtos.Document.RequestExportDataDocs requestQuery = _common.ConvertJSonToObjectV2<LMS.Entites.Dtos.Document.RequestExportDataDocs>(excelCurrent.RequestQuery);
                        if (requestQuery.TypeDoc == request.Model.TypeDoc
                            && requestQuery.LoanID == request.Model.LoanID
                            && requestQuery.CVTB == request.Model.CVTB
                            && requestQuery.DateCloseLoan == request.Model.DateCloseLoan
                            && requestQuery.DateFinshCloseLoan == request.Model.DateFinshCloseLoan
                            && requestQuery.NumberDayKeep == request.Model.NumberDayKeep
                            && requestQuery.NameEmployess == request.Model.NameEmployess
                            && requestQuery.PhoneEmployees == request.Model.PhoneEmployees
                            && requestQuery.UserID == request.Model.UserID
                            && excelCurrent.CreateDate.Value.AddMinutes(MaxTimeWaitGetNext) > dtNow)
                        {
                            response.SetSucces();
                            response.Data = excelCurrent.TraceIDRequest;
                            return response;
                        }
                    }
                }
                response = await InsertExcelReport(request.Model.UserID, request.Model.TypeDoc, dtNow, request.Model);
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateExportDataDocsCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private async Task<ResponseActionResult> InsertExcelReport(long createBy, int typeReport, DateTime createDate, LMS.Entites.Dtos.Document.RequestExportDataDocs requestQuery)
        {
            try
            {
                ResponseActionResult response = new ResponseActionResult();
                string traceIDRequest = _common.GenTraceIndentifier();
                long excelID = await _excelReportTab.InsertAsync(new Domain.Tables.TblExcelReport()
                {
                    CreateDate = createDate,
                    CreateBy = createBy,
                    Status = (int)ExcelReport_Status.Waiting,
                    TypeReport = typeReport,
                    TraceIDRequest = traceIDRequest,
                    ModifyDate = createDate,
                    RequestQuery = _common.ConvertObjectToJSonV2(requestQuery)
                });
                if (excelID > 0)
                {
                    response.SetSucces();
                    response.Data = traceIDRequest;
                    _ = ProcessExportReportDayPlan(excelID, requestQuery);
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateExportDataDocsCommandHandler_InsertExcelReport|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }

        private async Task ProcessExportReportDayPlan(long exelID, LMS.Entites.Dtos.Document.RequestExportDataDocs request)
        {
            var objLoan = (await _loanTab.SelectColumns(x => x.CustomerName, x => x.ContactCode).WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
            string reportName = $"{((ExcelReport_TypeReport)request.TypeDoc).GetDescription()}-{objLoan.CustomerName}-{objLoan.ContactCode}";
            try
            {
                string jsonData = string.Empty;
                int statusExcel = (int)ExcelReport_Status.Recall;

                var response = await GetListData(request);
                if (response != null)
                {
                    jsonData= _common.ConvertObjectToJSonV2(response.Data);
                    statusExcel = (int)ExcelReport_Status.Success;
                }
                _ = UpdateStatusExcel(exelID, statusExcel, jsonData, reportName);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessExportReportDayPlan|request={_common.ConvertObjectToJSonV2(request)}| ex={ex.Message}-{ex.StackTrace}");
                _ = UpdateStatusExcel(exelID, (int)ExcelReport_Status.Recall, "", reportName);
            }
        }

        private async Task UpdateStatusExcel(long excelID, int status, string extraData, string reportName)
        {
            try
            {
                var excelDetail = (await _excelReportTab.WhereClause(x => x.ExcelReportID == excelID).QueryAsync()).FirstOrDefault();
                excelDetail.Status = status;
                excelDetail.ExtraData = extraData;
                excelDetail.ModifyDate = DateTime.Now;
                excelDetail.ReportName = reportName;
                _ = _excelReportTab.UpdateAsync(excelDetail);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateExportDataDocsCommandHandler_UpdateStatusExcel|excelID={excelID}|status={status}|jsonExtra={extraData}|ex={ex.Message}-{ex.StackTrace}");
            }
        }
        private async Task<ResponseActionResult> GetListData(LMS.Entites.Dtos.Document.RequestExportDataDocs request)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                switch ((ExcelReport_TypeReport)request.TypeDoc)
                {
                    case ExcelReport_TypeReport.PropertySeizure:
                        response = await _exportDataDocsManager.PropertySeizure(request);
                        break;
                    case ExcelReport_TypeReport.DebtRestructuring:
                        response = await _exportDataDocsManager.DebtRestructuring(request);
                        break;
                    case ExcelReport_TypeReport.UnsecuredFinalDebtNotice:
                        response = await _exportDataDocsManager.UnsecuredFinalDebtNotice(request);
                        break;
                    case ExcelReport_TypeReport.NotifyPropertySeizure:
                        response = await _exportDataDocsManager.NotifyPropertySeizure(request);
                        break;
                    case ExcelReport_TypeReport.NotifyVoluntarilyHandingOverPropertye:
                        response = await _exportDataDocsManager.NotifyVoluntarilyHandingOverPropertye(request);
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateExportDataDocsCommandHandler_GetListData|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
