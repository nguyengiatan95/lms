﻿using CollectionServiceApi.Domain.Models.Excel;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.Excel
{
    public class CreateRequestGetReportLoanDebtDailyCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string ReportDate { get; set; }
        public int TypeReport { get; set; }
        public long CreateBy { get; set; }
        public string KeySearch { get; set; }
        public int TypeDebt { get; set; } = (int)LMS.Common.Constants.StatusCommon.SearchAll;
    }
    public class CreateRequestGetReportLoanDebtDailyCommandHandler : IRequestHandler<CreateRequestGetReportLoanDebtDailyCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> _excelReportTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblReportLoanDebtDaily> _reportLoanDebtDailyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCity> _cityTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDistrict> _districtTab;
        ILogger<CreateRequestGetReportLoanDebtDailyCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateRequestGetReportLoanDebtDailyCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> excelReportTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblReportLoanDebtDaily> reportLoanDebtDailyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCity> cityTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDistrict> districtTab,
            ILogger<CreateRequestGetReportLoanDebtDailyCommandHandler> logger
            )
        {
            _loanTab = loanTab;
            _excelReportTab = excelReportTab;
            _reportLoanDebtDailyTab = reportLoanDebtDailyTab;
            _customerTab = customerTab;
            _lenderTab = lenderTab;
            _cityTab = cityTab;
            _districtTab = districtTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(CreateRequestGetReportLoanDebtDailyCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                DateTime dtNow = DateTime.Now;
                if (!DateTime.TryParseExact(request.ReportDate, TimaSettingConstant.DateTimeDayMonthYear, null, System.Globalization.DateTimeStyles.None, out _))
                {
                    response.Message = MessageConstant.ErrorFormatDate;
                    return response;
                }

                var excelReports = await _excelReportTab.SetGetTop(1).SelectColumns(x => x.ExcelReportID, x => x.Status, x => x.CreateDate, x => x.RequestQuery)
                                                        .OrderByDescending(x => x.CreateDate).WhereClause(x => x.CreateBy == request.CreateBy
                                          && x.TypeReport == request.TypeReport).QueryAsync();
                if (excelReports == null || !excelReports.Any())
                {
                    response = await InsertExcelReport(request.CreateBy, request.TypeReport, dtNow, request);
                    return response;
                }

                var excelCurrent = excelReports.FirstOrDefault();
                if (excelCurrent.Status == (int)ExcelReport_Status.Waiting)
                {
                    //++0->thông báo user chờ xử lý xong
                    response.Message = MessageConstant.ExcelReport_WaitingProcess;
                    return response;
                }
                if (excelCurrent.CreateDate.Value.Date == dtNow.Date)
                {
                    CreateRequestGetReportLoanDebtDailyCommand requestQuery = _common.ConvertJSonToObjectV2<CreateRequestGetReportLoanDebtDailyCommand>(excelCurrent.RequestQuery);
                    if (requestQuery.ReportDate == request.ReportDate
                        && requestQuery.KeySearch == request.KeySearch
                        && requestQuery.TypeDebt == request.TypeDebt)
                    {
                        response.SetSucces();
                        response.Data = excelCurrent.TraceIDRequest;
                        return response;
                    }
                }
                response = await InsertExcelReport(request.CreateBy, request.TypeReport, dtNow, request);
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestGetReportLoanDebtDailyCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private async Task<ResponseActionResult> InsertExcelReport(long createBy, int typeReport, DateTime createDate, CreateRequestGetReportLoanDebtDailyCommand requestQuery)
        {
            try
            {
                ResponseActionResult response = new ResponseActionResult();
                string traceIDRequest = _common.GenTraceIndentifier();
                long excelID = await _excelReportTab.InsertAsync(new Domain.Tables.TblExcelReport()
                {
                    CreateDate = createDate,
                    CreateBy = createBy,
                    Status = (int)ExcelReport_Status.Waiting,
                    TypeReport = typeReport,
                    TraceIDRequest = traceIDRequest,
                    ModifyDate = createDate,
                    RequestQuery = _common.ConvertObjectToJSonV2(requestQuery)
                });
                if (excelID > 0)
                {
                    response.SetSucces();
                    response.Data = traceIDRequest;
                    _ = ProcessExportReportLoanDebtDaily(excelID, requestQuery);
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestGetReportLoanDebtDailyCommandHandler_InsertExcelReport|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }
        private async Task ProcessExportReportLoanDebtDaily(long exelID, CreateRequestGetReportLoanDebtDailyCommand request)
        {
            string reportName = $"{((ExcelReport_TypeReport)request.TypeReport).GetDescription()}";
            try
            {
                var fromDate = DateTime.ParseExact(request.ReportDate, TimaSettingConstant.DateTimeDayMonthYear, null, System.Globalization.DateTimeStyles.None);
                reportName += $"-{fromDate:yyyyMMdd}";

                int statusExcel = (int)ExcelReport_Status.Success;
                var result = new List<Domain.Models.Excel.ReportLoanDebtDailyModel>();
                if (!string.IsNullOrEmpty(request.KeySearch))
                {
                    reportName += $"_{request.KeySearch}";
                    if (long.TryParse(request.KeySearch, out long contracCodeID) && request.KeySearch.Length < TimaSettingConstant.MaxLengthPhone - 1)
                    {
                        request.KeySearch = $"{TimaSettingConstant.PrefixContractCode}{contracCodeID}";
                        _reportLoanDebtDailyTab.WhereClause(x => x.LmsContractCode == request.KeySearch);
                    }
                    else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternContractCode, RegexOptions.IgnoreCase).Success)
                    {
                        _reportLoanDebtDailyTab.WhereClause(x => x.LmsContractCode == request.KeySearch);
                    }
                    else
                    {
                        _customerTab.WhereClause(x => x.FullName == request.KeySearch);
                        _reportLoanDebtDailyTab.WhereClause(x => x.CustomerName == request.KeySearch);
                    }
                }
                if (request.TypeDebt != (int)StatusCommon.SearchAll)
                {
                    reportName += $"-{((THN_DPD)request.TypeDebt).GetDescription()}";
                    _reportLoanDebtDailyTab.WhereClause(x => x.TypeDebt == request.TypeDebt);
                }
                var lstDataTask = _reportLoanDebtDailyTab.WhereClause(x => x.ReportDate == fromDate).QueryAsync();
                var lstCityTask = _cityTab.SelectColumns(x => x.CityID, x => x.Name).QueryAsync();
                var lstDistrictTask = _districtTab.SelectColumns(x => x.DistrictID, x => x.Name).QueryAsync();
                await Task.WhenAll(lstCityTask, lstDistrictTask, lstDataTask);
                var dictCityInfos = lstCityTask.Result.ToDictionary(x => x.CityID, x => x);
                var dictDistrictInfos = lstDistrictTask.Result.ToDictionary(x => x.DistrictID, x => x);
                foreach (var item in lstDataTask.Result)
                {
                    var detail = _common.ConvertParentToChild<Domain.Models.Excel.ReportLoanDebtDailyModel, Domain.Tables.TblReportLoanDebtDaily>(item);
                    detail.CityName = dictCityInfos.GetValueOrDefault(item.CityID)?.Name;
                    detail.DistrictName = dictDistrictInfos.GetValueOrDefault(item.DistrictID)?.Name;
                    detail.LoanStatusName = ((Loan_Status)item.LoanStatus).GetDescription();
                    detail.LoanStatusInsuranceName = "";
                    if (item.DPD > TimaSettingConstant.MaxDaySentInsurance && item.SourceByInsurance != (int)Loan_SourcecBuyInsurance.NO)
                        detail.LoanStatusInsuranceName = ((Loan_StatusSendInsurance)item.LoanStatusInsurance).GetDescription();
                    detail.LoanStatusDispositName = "Chua xu ly";
                    detail.TypeDebtName = ((THN_DPD)item.TypeDebt).GetDescription();
                    result.Add(detail);
                }
                await UpdateStatusExcel(exelID, statusExcel, _common.ConvertObjectToJSonV2(result), reportName);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestGetReportLoanDebtDailyCommandHandler_ProcessExportHistoryTransactionLoanByCustomer|request={_common.ConvertObjectToJSonV2(request)}| ex={ex.Message}-{ex.StackTrace}");
                _ = UpdateStatusExcel(exelID, (int)ExcelReport_Status.Recall, "", reportName);
            }
        }

        private async Task UpdateStatusExcel(long excelID, int status, string extraData, string reportName)
        {
            try
            {
                var excelDetail = (await _excelReportTab.WhereClause(x => x.ExcelReportID == excelID).QueryAsync()).FirstOrDefault();
                excelDetail.Status = status;
                excelDetail.ExtraData = extraData;
                excelDetail.ModifyDate = DateTime.Now;
                excelDetail.ReportName = reportName;
                _ = await _excelReportTab.UpdateAsync(excelDetail);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestGetReportLoanDebtDailyCommandHandler_UpdateStatusExcel|excelID={excelID}|status={status}|jsonExtra={extraData}|ex={ex.Message}-{ex.StackTrace}");
            }
        }
    }
}
