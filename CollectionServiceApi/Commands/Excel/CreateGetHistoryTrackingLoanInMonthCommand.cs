﻿using CollectionServiceApi.Domain.Models.Excel;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.Excel
{
    public class CreateGetHistoryTrackingLoanInMonthCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public RequestHistroryTrackingLoan Model { get; set; }
    }
    public class CreateGetHistoryTrackingLoanInMonthCommandHandler : IRequestHandler<CreateGetHistoryTrackingLoanInMonthCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> _excelReportTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTrackingLoanInMonth> _trackingLoanInMonthTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        ILogger<CreateGetHistoryTrackingLoanInMonthCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        Services.IReportManager _reportManager;
        RestClients.ILoanService _loanService;
        IMediator _mediator;
        private const int MaxTimeWaitGetNext = TimaSettingConstant.MaxTimeWaitGetNext; // số phút chờ lấy mới
        public CreateGetHistoryTrackingLoanInMonthCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> excelReportTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTrackingLoanInMonth> transactionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            Services.IReportManager reportManager,
            RestClients.ILoanService loanService,
             IMediator mediator,
            ILogger<CreateGetHistoryTrackingLoanInMonthCommandHandler> logger
            )
        {
            _loanTab = loanTab;
            _excelReportTab = excelReportTab;
            _trackingLoanInMonthTab = transactionTab;
            _customerTab = customerTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _reportManager = reportManager;
            _loanService = loanService;
            _mediator = mediator;
        }

        public async Task<ResponseActionResult> Handle(CreateGetHistoryTrackingLoanInMonthCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                DateTime dtNow = DateTime.Now;
                var excelReports = await _excelReportTab.SetGetTop(1).SelectColumns(x => x.ExcelReportID, x => x.Status, x => x.CreateDate, x => x.RequestQuery,x=>x.TraceIDRequest)
                                                        .OrderByDescending(x => x.CreateDate).WhereClause(x => x.CreateBy == request.Model.CreateBy
                                          && x.TypeReport == request.Model.TypeReport).QueryAsync();
                if (excelReports != null && excelReports.Any())
                {
                    var excelCurrent = excelReports.FirstOrDefault();
                    if (excelCurrent.Status == (int)ExcelReport_Status.Waiting)
                    {
                        //++0->thông báo user chờ xử lý xong
                        response.Message = MessageConstant.ExcelReport_WaitingProcess;
                        return response;
                    }

                    if (excelCurrent.CreateDate.Value.Date == dtNow.Date)
                    {
                        // trong 30p đổ lại thì trả về file cũ
                        RequestHistroryTrackingLoan requestQuery = _common.ConvertJSonToObjectV2<RequestHistroryTrackingLoan>(excelCurrent.RequestQuery);
                        if (requestQuery.MonthYear == request.Model.MonthYear
                            && requestQuery.CreateBy == request.Model.CreateBy
                            && requestQuery.ProductID == request.Model.ProductID
                            && requestQuery.CityID == request.Model.CityID
                            && excelCurrent.CreateDate.Value.AddMinutes(MaxTimeWaitGetNext) > dtNow)
                        {
                            response.SetSucces();
                            response.Data = excelCurrent.TraceIDRequest;
                            return response;
                        }
                    }
                }
                response = await InsertExcelReport(request.Model.CreateBy, request.Model.TypeReport, dtNow, request.Model);
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateGetHistoryTrackingLoanInMonthCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private async Task<ResponseActionResult> InsertExcelReport(long createBy, int typeReport, DateTime createDate, RequestHistroryTrackingLoan requestQuery)
        {
            try
            {
                ResponseActionResult response = new ResponseActionResult();
                string traceIDRequest = _common.GenTraceIndentifier();
                long excelID = await _excelReportTab.InsertAsync(new Domain.Tables.TblExcelReport()
                {
                    CreateDate = createDate,
                    CreateBy = createBy,
                    Status = (int)ExcelReport_Status.Waiting,
                    TypeReport = typeReport,
                    TraceIDRequest = traceIDRequest,
                    ModifyDate = createDate,
                    RequestQuery = _common.ConvertObjectToJSonV2(requestQuery)
                });
                if (excelID > 0)
                {
                    response.SetSucces();
                    response.Data = traceIDRequest;
                    _ = ProcessExportReportTracking(excelID, requestQuery);
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateGetHistoryTrackingLoanInMonthCommandHandler_InsertExcelReport|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }

        private async Task ProcessExportReportTracking(long exelID, RequestHistroryTrackingLoan request)
        {
            string reportName = $"{((ExcelReport_TypeReport)request.TypeReport).GetDescription()}-{request.MonthYear}";
            try
            {
                List<HistoryTrackingLoanInMonth> lstData = new List<HistoryTrackingLoanInMonth>();
                int statusExcel = (int)ExcelReport_Status.Recall;
                var actionResult = await _mediator.Send(new Queries.Report.GetReportDebtTypeQuery
                {
                    MonthYear = request.MonthYear,
                    ProductID = request.ProductID,
                    CityID = request.CityID,
                    DebtType = request.DebtType
                });
                if (actionResult.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                {
                    lstData = (List<HistoryTrackingLoanInMonth>)actionResult.Data;
                    statusExcel = (int)ExcelReport_Status.Success;
                }
                _ = UpdateStatusExcel(exelID, statusExcel, _common.ConvertObjectToJSonV2(lstData), reportName);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessExportReportTracking|request={_common.ConvertObjectToJSonV2(request)}| ex={ex.Message}-{ex.StackTrace}");
                _ = UpdateStatusExcel(exelID, (int)ExcelReport_Status.Recall, "", reportName);
            }
        }

        private async Task UpdateStatusExcel(long excelID, int status, string extraData, string reportName)
        {
            try
            {
                var excelDetail = (await _excelReportTab.WhereClause(x => x.ExcelReportID == excelID).QueryAsync()).FirstOrDefault();
                excelDetail.Status = status;
                excelDetail.ExtraData = extraData;
                excelDetail.ModifyDate = DateTime.Now;
                excelDetail.ReportName = reportName;
                _ = _excelReportTab.UpdateAsync(excelDetail);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateGetHistoryTrackingLoanInMonthCommandHandler_UpdateStatusExcel|excelID={excelID}|status={status}|jsonExtra={extraData}|ex={ex.Message}-{ex.StackTrace}");
            }
        }
    }

}
