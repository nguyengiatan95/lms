﻿using CollectionServiceApi.Domain.Models.CommentDebtPrompted;
using CollectionServiceApi.Domain.Models.Excel;
using CollectionServiceApi.Domain.Models.Report;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.Excel
{
    public class ExcelHistoryInteractionCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public RequestExcelHistoryInteraction Model { get; set; }
    }
    public class ExcelHistoryInteractionCommandHandler : IRequestHandler<ExcelHistoryInteractionCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> _excelReportTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTrackingLoanInMonth> _trackingLoanInMonthTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        ILogger<ExcelHistoryInteractionCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        Services.IReportManager _reportManager;
        RestClients.ILoanService _loanService;
        IMediator _mediator;
        private const int MaxTimeWaitGetNext = TimaSettingConstant.MaxTimeWaitGetNext; // số phút chờ lấy mới
        public ExcelHistoryInteractionCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> excelReportTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTrackingLoanInMonth> transactionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            Services.IReportManager reportManager,
            RestClients.ILoanService loanService,
             IMediator mediator,
            ILogger<ExcelHistoryInteractionCommandHandler> logger
            )
        {
            _loanTab = loanTab;
            _excelReportTab = excelReportTab;
            _trackingLoanInMonthTab = transactionTab;
            _customerTab = customerTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _reportManager = reportManager;
            _loanService = loanService;
            _mediator = mediator;
        }

        public async Task<ResponseActionResult> Handle(ExcelHistoryInteractionCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                DateTime dtNow = DateTime.Now;
                var excelReports = await _excelReportTab.SetGetTop(1).SelectColumns(x => x.ExcelReportID, x => x.Status, x => x.CreateDate, x => x.RequestQuery, x => x.TraceIDRequest)
                                                        .OrderByDescending(x => x.CreateDate).WhereClause(x => x.CreateBy == request.Model.UserID
                                          && x.TypeReport == request.Model.TypeReport).QueryAsync();
                if (excelReports != null && excelReports.Any())
                {
                    var excelCurrent = excelReports.FirstOrDefault();
                    if (excelCurrent.Status == (int)ExcelReport_Status.Waiting)
                    {
                        //++0->thông báo user chờ xử lý xong
                        response.Message = MessageConstant.ExcelReport_WaitingProcess;
                        return response;
                    }

                    if (excelCurrent.CreateDate.Value.Date == dtNow.Date)
                    {
                        // trong 30p đổ lại thì trả về file cũ
                        RequestExcelHistoryInteraction requestQuery = _common.ConvertJSonToObjectV2<RequestExcelHistoryInteraction>(excelCurrent.RequestQuery);
                        if (requestQuery.KeySearch == request.Model.KeySearch
                            && requestQuery.DeparmentID == request.Model.DeparmentID
                            && requestQuery.ManagerID == request.Model.ManagerID
                            && requestQuery.LenderID == request.Model.LenderID
                            && requestQuery.UserID == request.Model.UserID
                            && requestQuery.Staff == request.Model.Staff
                            && requestQuery.FromDate == request.Model.FromDate
                            && requestQuery.ToDate == request.Model.ToDate
                            && excelCurrent.CreateDate.Value.AddMinutes(MaxTimeWaitGetNext) > dtNow)
                        {
                            response.SetSucces();
                            response.Data = excelCurrent.TraceIDRequest;
                            return response;
                        }
                    }
                }
                response = await InsertExcelReport(request.Model.UserID, request.Model.TypeReport, dtNow, request.Model);
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ExcelHistoryInteractionCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private async Task<ResponseActionResult> InsertExcelReport(long createBy, int typeReport, DateTime createDate, RequestExcelHistoryInteraction requestQuery)
        {
            try
            {
                ResponseActionResult response = new ResponseActionResult();
                string traceIDRequest = _common.GenTraceIndentifier();
                long excelID = await _excelReportTab.InsertAsync(new Domain.Tables.TblExcelReport()
                {
                    CreateDate = createDate,
                    CreateBy = createBy,
                    Status = (int)ExcelReport_Status.Waiting,
                    TypeReport = typeReport,
                    TraceIDRequest = traceIDRequest,
                    ModifyDate = createDate,
                    RequestQuery = _common.ConvertObjectToJSonV2(requestQuery)
                });
                if (excelID > 0)
                {
                    response.SetSucces();
                    response.Data = traceIDRequest;
                    _ = ProcessExcelHistoryInteraction(excelID, requestQuery);
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ExcelHistoryInteractionCommandHandler_InsertExcelReport|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }

        private async Task ProcessExcelHistoryInteraction(long exelID, RequestExcelHistoryInteraction request)
        {
            string reportName = $"{((ExcelReport_TypeReport)request.TypeReport).GetDescription()}-{request.FromDate}-{request.ToDate}";
            try
            {
                List<HistoryInteractDebtPrompted> lstData = new List<HistoryInteractDebtPrompted>();
                int statusExcel = (int)ExcelReport_Status.Recall;
                var actionResult = await _mediator.Send(new Queries.CommentDebtPrompted.HistoryInteractDebtPromptedQuery
                {
                    KeySearch =request.KeySearch,
                    DeparmentID =request.DeparmentID,
                    ManagerID = request.ManagerID,
                    LenderID = request.LenderID,
                    UserID = request.UserID,
                    Staff = request.Staff,
                    FromDate = request.FromDate,
                    ToDate = request.ToDate,
                    PageIndex =request.PageIndex,
                    PageSize = request.PageSize
                });
                if (actionResult.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                {
                    lstData = (List<HistoryInteractDebtPrompted>)actionResult.Data;
                    statusExcel = (int)ExcelReport_Status.Success;
                }
                _ = UpdateStatusExcel(exelID, statusExcel, _common.ConvertObjectToJSonV2(lstData), reportName);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessExcelHistoryInteraction|request={_common.ConvertObjectToJSonV2(request)}| ex={ex.Message}-{ex.StackTrace}");
                _ = UpdateStatusExcel(exelID, (int)ExcelReport_Status.Recall, "", reportName);
            }
        }

        private async Task UpdateStatusExcel(long excelID, int status, string extraData, string reportName)
        {
            try
            {
                var excelDetail = (await _excelReportTab.WhereClause(x => x.ExcelReportID == excelID).QueryAsync()).FirstOrDefault();
                excelDetail.Status = status;
                excelDetail.ExtraData = extraData;
                excelDetail.ModifyDate = DateTime.Now;
                excelDetail.ReportName = reportName;
                _ = _excelReportTab.UpdateAsync(excelDetail);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ExcelHistoryInteractionCommandHandler_UpdateStatusExcel|excelID={excelID}|status={status}|jsonExtra={extraData}|ex={ex.Message}-{ex.StackTrace}");
            }
        }
    }

}
