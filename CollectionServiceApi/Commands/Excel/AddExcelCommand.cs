﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.Excel
{
    public class AddExcelCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int TypeReport { get; set; }
        public int CreateBy { get; set; }
        public string RequestQuery { get; set; }
    }
    public class AddExcelCommandHandler : IRequestHandler<AddExcelCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> _excelReportTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionTab;
        ILogger<AddExcelCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        Services.IReportManager _reportManager;
        public AddExcelCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> excelReportTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionTab,
            Services.IReportManager reportManager,
            ILogger<AddExcelCommandHandler> logger
            )
        {
            _excelReportTab = excelReportTab;
            _transactionTab = transactionTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _reportManager = reportManager;
        }

        public async Task<ResponseActionResult> Handle(AddExcelCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                DateTime dtNow = DateTime.Now;
                //nghiệp vụ: kiểm tra typereport và createBy đã có chưa:
                var excelReports = await _excelReportTab.SetGetTop(1).OrderByDescending(x => x.CreateDate).WhereClause(x => x.CreateBy == request.CreateBy && x.TypeReport == request.TypeReport).QueryAsync();
                if (excelReports == null || !excelReports.Any())
                {
                    response = await InsertExcelReport(request.CreateBy, request.TypeReport, dtNow, request.RequestQuery);
                    return response;
                }
                var excelCurrent = excelReports.FirstOrDefault();
                //+rồi->check status:
                if (excelCurrent.Status == (int)ExcelReport_Status.Waiting)
                {
                    //++0->thông báo user chờ xử lý xong
                    response.Message = MessageConstant.ExcelReport_WaitingProcess;
                    return response;
                }
                if (excelCurrent.Status == (int)ExcelReport_Status.Success)
                {
                    //++1->kiểm tra CreateDate so với thời gian transaction loan gần nhất
                    var lstTran = await _transactionTab.SelectColumns(x => x.CreateDate).SetGetTop(1).OrderByDescending(x => x.CreateDate).QueryAsync();
                    DateTime lastTransactionDate = lstTran.FirstOrDefault().CreateDate.Value;
                    //+++CreateDate >= LatestDatetimeTransactionLoan->trả về TraceIDRequest đã có
                    if (excelCurrent.ModifyDate >= lastTransactionDate)
                    {
                        response.SetSucces();
                        response.Data = excelCurrent.TraceIDRequest;
                        return response;
                    }
                    //+++CreateDate < LatestDateTimeTransactionLoan->thêm mới và trả về TraceIDRequest mới sinh ra
                    response = await InsertExcelReport(request.CreateBy, request.TypeReport, dtNow, request.RequestQuery);
                    return response;
                }
                if (excelCurrent.Status == (int)ExcelReport_Status.Recall)
                {
                    //++2->thêm mới và trả về TraceIDRequest mới sinh ra
                    response = await InsertExcelReport(request.CreateBy, request.TypeReport, dtNow, request.RequestQuery);
                    return response;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"AddExcelCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private async Task<ResponseActionResult> InsertExcelReport(long createBy, int typeReport, DateTime createDate, string requestQuery)
        {
            try
            {
                ResponseActionResult response = new ResponseActionResult();
                string traceIDRequest = _common.GenTraceIndentifier();
                long excelID = await _excelReportTab.InsertAsync(new Domain.Tables.TblExcelReport()
                {
                    CreateDate = createDate,
                    CreateBy = createBy,
                    Status = (int)ExcelReport_Status.Waiting,
                    TypeReport = typeReport,
                    TraceIDRequest = traceIDRequest,
                    ModifyDate = createDate,
                    RequestQuery = requestQuery
                });
                if (excelID > 0)
                {
                    response.SetSucces();
                    response.Data = traceIDRequest;
                    _ = _reportManager.ExportExcelListLoanByEmployeeID(createBy, traceIDRequest: traceIDRequest);
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"AddExcelCommandHandler_InsertExcelReport|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }
    }
}
