﻿using CollectionServiceApi.Domain.Models.Excel;
using CollectionServiceApi.Domain.Models.Invoice;
using CollectionServiceApi.Domain.Models.Loan;
using LMS.Common.Constants;
using LMS.Common.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.Excel
{
    public class CreateRequestGetHistoryTransactionCustomerCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public RequestHistoryTransactionLoanByCustomer Model { get; set; }
    }
    public class CreateRequestGetHistoryTransactionCustomerCommandHandler : IRequestHandler<CreateRequestGetHistoryTransactionCustomerCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> _excelReportTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> _transactionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSplitTransactionLoan> _splitTransactionLoanTab;
        ILogger<CreateRequestGetHistoryTransactionCustomerCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        Services.IReportManager _reportManager;
        RestClients.ILoanService _loanService;
        private const int MaxTimeWaitGetNext = 30; // số phút chờ lấy mới
        public CreateRequestGetHistoryTransactionCustomerCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblExcelReport> excelReportTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblTransaction> transactionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSplitTransactionLoan> splitTransactionLoanTab,
            Services.IReportManager reportManager,
            RestClients.ILoanService loanService,
            ILogger<CreateRequestGetHistoryTransactionCustomerCommandHandler> logger
            )
        {
            _loanTab = loanTab;
            _excelReportTab = excelReportTab;
            _transactionTab = transactionTab;
            _customerTab = customerTab;
            _lenderTab = lenderTab;
            _splitTransactionLoanTab = splitTransactionLoanTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _reportManager = reportManager;
            _loanService = loanService;
        }

        public async Task<ResponseActionResult> Handle(CreateRequestGetHistoryTransactionCustomerCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                DateTime dtNow = DateTime.Now;
                request.Model.PageIndex = 1;
                request.Model.PageSize = Int32.MaxValue;
                if (!DateTime.TryParseExact(request.Model.FromDate, _common.DateTimeDDMMYYYY, null, System.Globalization.DateTimeStyles.None, out _))
                {
                    response.Message = MessageConstant.ErrorFormatDate;
                    return response;
                }
                if (!DateTime.TryParseExact(request.Model.ToDate, _common.DateTimeDDMMYYYY, null, System.Globalization.DateTimeStyles.None, out _))
                {
                    response.Message = MessageConstant.ErrorFormatDate;
                    return response;
                }

                var excelReports = await _excelReportTab.SetGetTop(1).SelectColumns(x => x.ExcelReportID, x => x.Status, x => x.CreateDate, x => x.RequestQuery)
                                                        .OrderByDescending(x => x.CreateDate).WhereClause(x => x.CreateBy == request.Model.CreateBy
                                          && x.TypeReport == request.Model.TypeReport).QueryAsync();
                if (excelReports == null || !excelReports.Any())
                {
                    response = await InsertExcelReport(request.Model.CreateBy, request.Model.TypeReport, dtNow, request.Model);
                    return response;
                }

                var excelCurrent = excelReports.FirstOrDefault();
                if (excelCurrent.Status == (int)ExcelReport_Status.Waiting)
                {
                    //++0->thông báo user chờ xử lý xong
                    response.Message = MessageConstant.ExcelReport_WaitingProcess;
                    return response;
                }
                if (excelCurrent.CreateDate.Value.Date == dtNow.Date)
                {
                    // trong 30p đổ lại thì trả về file cũ
                    RequestHistoryTransactionLoanByCustomer requestQuery = _common.ConvertJSonToObjectV2<RequestHistoryTransactionLoanByCustomer>(excelCurrent.RequestQuery);
                    if (requestQuery.FromDate == request.Model.FromDate
                        && requestQuery.ToDate == request.Model.ToDate
                        && requestQuery.KeySearch == request.Model.KeySearch
                        && excelCurrent.CreateDate.Value.AddMinutes(MaxTimeWaitGetNext) > dtNow)
                    {
                        response.SetSucces();
                        response.Data = excelCurrent.TraceIDRequest;
                        return response;
                    }
                }
                response = await InsertExcelReport(request.Model.CreateBy, request.Model.TypeReport, dtNow, request.Model);
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestGetHistoryTransactionCustomerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private async Task<ResponseActionResult> InsertExcelReport(long createBy, int typeReport, DateTime createDate, RequestHistoryTransactionLoanByCustomer requestQuery)
        {
            try
            {
                ResponseActionResult response = new ResponseActionResult();
                string traceIDRequest = _common.GenTraceIndentifier();
                long excelID = await _excelReportTab.InsertAsync(new Domain.Tables.TblExcelReport()
                {
                    CreateDate = createDate,
                    CreateBy = createBy,
                    Status = (int)ExcelReport_Status.Waiting,
                    TypeReport = typeReport,
                    TraceIDRequest = traceIDRequest,
                    ModifyDate = createDate,
                    RequestQuery = _common.ConvertObjectToJSonV2(requestQuery)
                });
                if (excelID > 0)
                {
                    response.SetSucces();
                    response.Data = traceIDRequest;
                    _ = ProcessExportHistoryTransactionLoanByCustomer(excelID, requestQuery);
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestGetHistoryTransactionCustomerCommandHandler_InsertExcelReport|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }
        private async Task ProcessExportHistoryTransactionLoanByCustomer(long exelID, RequestHistoryTransactionLoanByCustomer request)
        {
            string reportName = $"{((ExcelReport_TypeReport)request.TypeReport).GetDescription()}";
            try
            {
                var fromDate = DateTime.ParseExact(request.FromDate, _common.DateTimeDDMMYYYY, null, System.Globalization.DateTimeStyles.None);
                var toDate = DateTime.ParseExact(request.ToDate, _common.DateTimeDDMMYYYY, null, System.Globalization.DateTimeStyles.None);
                reportName += $"-{fromDate:yyyyMMdd}_{toDate:yyyyMMdd}";
                HistoryTransactionLoanCustomerExcel result = new HistoryTransactionLoanCustomerExcel()
                {
                    Rows = new List<HistoryTransactionLoanCustomerModel>()
                };
                int statusExcel = (int)ExcelReport_Status.Success;
                List<long> lstLoanID = new List<long>();
                if (!string.IsNullOrEmpty(request.KeySearch))
                {
                    reportName += $"_{request.KeySearch}";
                    // lịch sử đơn vay
                    if (long.TryParse(request.KeySearch, out long contracCodeID) && request.KeySearch.Length < TimaSettingConstant.MaxLengthPhone - 1)
                    {
                        request.KeySearch = $"{TimaSettingConstant.PrefixContractCode}{contracCodeID}";
                        _loanTab.WhereClause(x => x.ContactCode == request.KeySearch);
                    }
                    else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternContractCode, RegexOptions.IgnoreCase).Success)
                    {
                        _loanTab.WhereClause(x => x.ContactCode == request.KeySearch);
                    }
                    else
                    {
                        if (request.KeySearch.Length == TimaSettingConstant.MaxLengthPhone && Regex.Match(request.KeySearch, TimaSettingConstant.PatternPhone, RegexOptions.IgnoreCase).Success)
                            _customerTab.WhereClause(x => x.Phone == request.KeySearch);
                        else if (Regex.Match(request.KeySearch, TimaSettingConstant.PatternNumberCard, RegexOptions.IgnoreCase).Success)
                            _customerTab.WhereClause(x => x.NumberCard == request.KeySearch);
                        else
                            _customerTab.WhereClause(x => x.FullName == request.KeySearch);
                        var lstCustomerID = (await _customerTab.QueryAsync()).Select(x => x.CustomerID).ToList();

                        if (lstCustomerID.Count == 0)
                        {
                            await UpdateStatusExcel(exelID, statusExcel, _common.ConvertObjectToJSonV2(result), reportName);
                            return;
                        }
                        _loanTab.WhereClause(x => lstCustomerID.Contains((long)x.CustomerID));
                    }

                    lstLoanID = (await _loanTab.SelectColumns(x => x.LoanID).QueryAsync()).Select(x => x.LoanID).ToList();
                    if (lstLoanID.Count == 0)
                    {
                        await UpdateStatusExcel(exelID, statusExcel, _common.ConvertObjectToJSonV2(result), reportName);
                        return;
                    }
                }
                if (lstLoanID.Count > 0)
                {
                    _splitTransactionLoanTab.WhereClause(x => lstLoanID.Contains(x.LoanID));
                }
                var lstTxnDataTask = _splitTransactionLoanTab.WhereClause(x => x.CreateDate > fromDate && x.CreateDate < toDate).QueryAsync();
                var dictLenderInfosTask = _lenderTab.SelectColumns(x => x.LenderID, x => x.FullName).WhereClause(x => x.LenderID == TimaSettingConstant.ShopIDTima || x.IsHub == 1).QueryAsync();
                await Task.WhenAll(lstTxnDataTask, dictLenderInfosTask);
                var lstBatchData = lstTxnDataTask.Result.Batch(TimaSettingConstant.MaxItemQuery);
                Dictionary<long, Domain.Tables.TblLender> dictLenderInfos = dictLenderInfosTask.Result.ToDictionary(x => x.LenderID, x => x);
                Dictionary<long, long> dictLoanID = new Dictionary<long, long>();
                Dictionary<long, Domain.Tables.TblLoan> dictLoanInfosCached = new Dictionary<long, Domain.Tables.TblLoan>();
                Dictionary<long, Domain.Tables.TblLoan> dictLoanInfosQuery = new Dictionary<long, Domain.Tables.TblLoan>();

                foreach (var lst in lstBatchData)
                {
                    dictLoanID.Clear();
                    foreach (var item in lst)
                    {
                        if (!dictLoanID.ContainsKey(item.LoanID) && !dictLoanInfosCached.ContainsKey(item.LoanID))
                        {
                            dictLoanID.Add(item.LoanID, item.LoanID);
                        }
                    }
                    if (dictLoanID.Count > 0)
                    {
                        dictLoanInfosQuery = (await _loanTab.SelectColumns(x => x.LoanID, x => x.TimaLoanID, x => x.CustomerName, x => x.OwnerShopID)
                                                  .WhereClause(x => dictLoanID.Keys.Contains(x.LoanID)).QueryAsync()).ToDictionary(x => x.LoanID, x => x);
                    }
                    foreach (var item in lst)
                    {
                        var loanInfo = dictLoanInfosCached.GetValueOrDefault(item.LoanID);
                        if (loanInfo == null || loanInfo.LoanID < 1)
                        {
                            loanInfo = dictLoanInfosQuery.GetValueOrDefault(item.LoanID);
                            if (loanInfo != null && loanInfo.LoanID > 0)
                            {
                                if (!dictLoanInfosCached.ContainsKey(item.LoanID))
                                {
                                    dictLoanInfosCached.Add(item.LoanID, loanInfo);
                                }
                                else
                                {
                                    dictLoanInfosCached[item.LoanID] = loanInfo;
                                }
                            }
                            else
                            {
                                _logger.LogError($"LoanID={item.LoanID}_NotFound");
                            }
                        }
                        if(loanInfo == null)
                        {
                            loanInfo = new Domain.Tables.TblLoan();
                        }
                        var ownerShop = dictLenderInfos.GetValueOrDefault((loanInfo?.OwnerShopID ?? 0));
                        var rowExcel = new HistoryTransactionLoanCustomerModel
                        {
                            LoanID = item.LoanID,
                            AgLoanID = loanInfo.TimaLoanID,
                            LoanContractCode = loanInfo.ContactCode,
                            ActionName = ((Transaction_Action)item.ActionID).GetDescription(),
                            CreateDate = item.CreateDate,
                            CustomerName = loanInfo.CustomerName,
                            OwnerShopName = ownerShop?.FullName,
                        };
                        switch ((Transaction_TypeMoney)item.MoneyType)
                        {
                            case Transaction_TypeMoney.Consultant:
                                rowExcel.MoneyConsultant = item.TotalMoney;
                                result.TotalMoneyConsultant += item.TotalMoney;
                                break;
                            case Transaction_TypeMoney.FineLate:
                                if (item.LenderID == TimaSettingConstant.ShopIDTima)
                                {
                                    rowExcel.MoneyFineLateTima = item.TotalMoney;
                                    result.TotalMoneyFineLateTima += item.TotalMoney;
                                }
                                else
                                {
                                    rowExcel.MoneyFineLateLender = item.TotalMoney;
                                    result.TotalMoneyFineLateLender += item.TotalMoney;
                                }
                                break;
                            case Transaction_TypeMoney.FineOriginal:
                                if (item.LenderID == TimaSettingConstant.ShopIDTima)
                                {
                                    rowExcel.MoneyFineOrginalTima = item.TotalMoney;
                                    result.TotalMoneyFineOrginalTima += item.TotalMoney;
                                }
                                else
                                {
                                    rowExcel.MoneyFineOrginalLender = item.TotalMoney;
                                    result.TotalMoneyFineOrginalLender += item.TotalMoney;
                                }
                                break;
                            case Transaction_TypeMoney.Interest:
                                rowExcel.MoneyInterest = item.TotalMoney;
                                result.TotalMoneyInterest += item.TotalMoney;
                                break;
                            case Transaction_TypeMoney.Original:
                                rowExcel.MoneyOriginal = item.TotalMoney;
                                result.TotalMoneyOriginal += item.TotalMoney;
                                break;
                            case Transaction_TypeMoney.Service:
                                rowExcel.MoneyService = item.TotalMoney;
                                result.TotalMoneyService += item.TotalMoney;
                                break;
                        }
                        result.Rows.Add(rowExcel);
                    }
                }

                await UpdateStatusExcel(exelID, statusExcel, _common.ConvertObjectToJSonV2(result), reportName);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestGetHistoryTransactionCustomerCommandHandler_ProcessExportHistoryTransactionLoanByCustomer|request={_common.ConvertObjectToJSonV2(request)}| ex={ex.Message}-{ex.StackTrace}");
                _ = UpdateStatusExcel(exelID, (int)ExcelReport_Status.Recall, "", reportName);
            }
        }

        private async Task UpdateStatusExcel(long excelID, int status, string extraData, string reportName)
        {
            try
            {
                var excelDetail = (await _excelReportTab.WhereClause(x => x.ExcelReportID == excelID).QueryAsync()).FirstOrDefault();
                excelDetail.Status = status;
                excelDetail.ExtraData = extraData;
                excelDetail.ModifyDate = DateTime.Now;
                excelDetail.ReportName = reportName;
                _ = await _excelReportTab.UpdateAsync(excelDetail);
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateRequestGetHistoryTransactionCustomerCommandHandler_UpdateStatusExcel|excelID={excelID}|status={status}|jsonExtra={extraData}|ex={ex.Message}-{ex.StackTrace}");
            }
        }
    }
}
