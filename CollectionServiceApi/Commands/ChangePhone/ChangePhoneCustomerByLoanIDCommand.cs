﻿using CollectionServiceApi.Domain.Models.AG;
using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.Entites.Dtos.AG;
using LMS.Entites.Dtos.LOSServices;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.ChangePhone
{
    public class ChangePhoneCustomerByLoanIDCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string Phone { get; set; }
        public long CustomerID { get; set; }
        public long CreateBy { get; set; }
        public long LoanID { get; set; }
        public string Note { get; set; }
        public long AGLoanID { get; set; }
        public long CreatebyAG { get; set; }
    }
    public class ChangePhoneCustomerByLoanIDHandler : IRequestHandler<ChangePhoneCustomerByLoanIDCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> _queueCallAPITab;
        ILogger<ChangePhoneCustomerByLoanIDHandler> _logger;
        LMS.Common.Helper.Utils _common;
        RestClients.ICustomerService _customerService;
        RestClients.ILOSService _lOSService;
        public ChangePhoneCustomerByLoanIDHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblQueueCallAPI> queueCallAPITab,
            RestClients.ICustomerService customerService,
            RestClients.ILOSService lOSService,
            ILogger<ChangePhoneCustomerByLoanIDHandler> logger)
        {
            _loanTab = loanTab;
            _userTab = userTab;
            _queueCallAPITab = queueCallAPITab;
            _customerService = customerService;
            _lOSService = lOSService;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();

        }

        public async Task<ResponseActionResult> Handle(ChangePhoneCustomerByLoanIDCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objUser = new Domain.Tables.TblUser();
                var objLoan = new Domain.Tables.TblLoan();
                if (request.AGLoanID != 0)
                {
                    objLoan = (await _loanTab.WhereClause(x => x.TimaLoanID == request.AGLoanID).QueryAsync()).FirstOrDefault();
                    if (objLoan == null)
                    {
                        response.Message = MessageConstant.LoanNotExist;
                        return response;
                    }
                    request.LoanID = objLoan.LoanID;
                    request.CustomerID = (long)objLoan.CustomerID;

                    objUser = (await _userTab.WhereClause(x => x.TimaUserID == request.CreatebyAG).QueryAsync()).FirstOrDefault();
                    if (objUser == null)
                    {
                        response.Message = MessageConstant.NotIdentifiedUserID;
                        return response;
                    }
                    request.CreateBy = objUser.UserID;
                }
                else
                {
                    objLoan = (await _loanTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync()).FirstOrDefault();
                    if (objLoan == null)
                    {
                        response.Message = MessageConstant.LoanNotExist;
                        return response;
                    }
                    objUser = (await _userTab.WhereClause(x => x.UserID == request.CreateBy).QueryAsync()).FirstOrDefault();
                    if (objUser == null)
                    {
                        response.Message = MessageConstant.NotIdentifiedUserID;
                        return response;
                    }
                }

                if (objLoan.CustomerID != request.CustomerID)
                {
                    response.Message = MessageConstant.CustomerNotExist;
                    return response;
                }

                var result = await _customerService.ChangePhone(request.CustomerID, request.LoanID, objUser.UserID, request.Phone, request.Note);
                if (result.Result != (int)LMS.Common.Constants.ResponseAction.Success)
                {
                    return response;
                }
                if (request.AGLoanID == 0) //call sang Los + luu TblQueueCallAPI
                {
                    var changePhoneAg = new ChangePhoneAG
                    {
                        LoanID = objLoan.TimaLoanID,
                        Phone = request.Phone,
                        Note = request.Note,
                        LogChangePhoneLMSID = (long)result.Data,
                        UserID = objUser.TimaUserID,
                        FullName = objUser.FullName
                    };
                    var queueCallAPI = new Domain.Tables.TblQueueCallAPI
                    {
                        Domain = AGConstants.BaseUrl,
                        Path = AGConstants.ChangePhone,
                        Method = "POST",
                        JsonRequest = _common.ConvertObjectToJSonV2(changePhoneAg),
                        Status = 0,
                        Retry = 0,
                        CreateDate = DateTime.Now,
                        Description = QueueCallAPI_Description.ChangePhone.GetDescription()
                    };
                    _ = _queueCallAPITab.InsertAsync(queueCallAPI);

                    var changePhoneLOSReq = new ChangePhoneLOSReq
                    {
                        LoanbriefId = (long)objLoan.LoanCreditIDOfPartner,
                        Phone = request.Phone,
                        Application = "LMS"
                    };
                    _ = _lOSService.ChangePhoneLOS(changePhoneLOSReq);
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ChangePhoneCustomerByLoanIDHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
