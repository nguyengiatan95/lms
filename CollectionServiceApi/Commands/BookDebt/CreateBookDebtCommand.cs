﻿using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.BookDebt
{
    public class CreateBookDebtCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long BookDebtID { get; set; }
        public string BookDebtName { get; set; }
        public int Status { get; set; }
        public int YearDebt { get; set; }
        public int DPDFrom { get; set; }
        public int DPDTo { get; set; }
        public int CreateBy { get; set; }
    }
    public class CreateBookDebtCommandHandler : IRequestHandler<CreateBookDebtCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBookDebt> _bookDebtTab;
        ILogger<CreateBookDebtCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateBookDebtCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBookDebt> bookDebtTab,
            ILogger<CreateBookDebtCommandHandler> logger)
        {
            _bookDebtTab = bookDebtTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();

        }
        public async Task<ResponseActionResult> Handle(CreateBookDebtCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                if (request.BookDebtID == 0)
                {
                    var objBookDebt = (await _bookDebtTab.WhereClause(x => x.YearDebt == request.YearDebt && x.DPDFrom == request.DPDFrom
                                                                     && x.DPDTo == request.DPDTo && x.Status == (int)BookDebt_Status.Active).QueryAsync()).FirstOrDefault();
                    if (objBookDebt != null)
                    {
                        response.Message = MessageConstant.CoincidesInforBookDebt;
                        return response;
                    }
                    if (string.IsNullOrEmpty(request.BookDebtName))
                    {
                        response.Message = MessageConstant.ParameterInvalid;
                        return response;
                    }
                    var bookDebt = new TblBookDebt()
                    {
                        BookDebtName = request.BookDebtName,
                        CreateDate = DateTime.Now,
                        ModifyDate = DateTime.Now,
                        Status = request.Status,
                        YearDebt = request.YearDebt,
                        DPDFrom = request.DPDFrom,
                        DPDTo = request.DPDTo,
                        CreateBy = request.CreateBy
                    };
                    var bookDebtID = await _bookDebtTab.InsertAsync(bookDebt);
                    if (bookDebtID > 0)
                        response.Data = bookDebtID;
                }
                else
                {
                    var objBookDebt = (await _bookDebtTab.WhereClause(x => x.BookDebtID == request.BookDebtID).QueryAsync()).FirstOrDefault();
                    if (objBookDebt == null)
                    {
                        response.Message = MessageConstant.ParameterInvalid;
                        return response;
                    }
                    objBookDebt.Status = request.Status;
                    objBookDebt.BookDebtName = request.BookDebtName;
                    objBookDebt.YearDebt = request.YearDebt;
                    objBookDebt.DPDFrom = request.DPDFrom;
                    objBookDebt.DPDTo = request.DPDTo;
                    objBookDebt.ModifyDate = DateTime.Now;
                    if (await _bookDebtTab.UpdateAsync(objBookDebt))
                        response.Data = objBookDebt.BookDebtID;
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateBookDebtCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
