﻿using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.BookDebt
{
    public class UpdateStatusBookDebtCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int Status { get; set; }
        public long BookDebtID { get; set; }
    }
    public class UpdateStatusBookDebtCommandHandler : IRequestHandler<UpdateStatusBookDebtCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblBookDebt> _bookDebtTab;
        ILogger<UpdateStatusBookDebtCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdateStatusBookDebtCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblBookDebt> bookDebtTab,
            ILogger<UpdateStatusBookDebtCommandHandler> logger)
        {
            _bookDebtTab = bookDebtTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();

        }
        public async Task<ResponseActionResult> Handle(UpdateStatusBookDebtCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var objBookDebt = (await _bookDebtTab.WhereClause(x => x.BookDebtID == request.BookDebtID).QueryAsync()).FirstOrDefault();
                if (objBookDebt == null)
                {
                    response.Message = MessageConstant.ParameterInvalid;
                    return response;
                }
                objBookDebt.Status = request.Status;
                if (await _bookDebtTab.UpdateAsync(objBookDebt))
                {
                    response.SetSucces();
                    response.Data = objBookDebt.BookDebtID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateStatusBookDebtCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}

