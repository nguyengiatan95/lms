﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using CollectionServiceApi.Domain.Tables;
using System.Globalization;

namespace CollectionServiceApi.Commands.HoldCustomerMoney
{
    public class UpdateHoldCustomerMoneyCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long HoldCustomerMoneyID { get; set; }
        public long Amount { get; set; }
        public int Status { get; set; }
        public string Reason { get; set; }
        public long ModifyBy { get; set; }
        public long TimaID { get; set; }
        public long TimaCustomerID { get; set; }

    }
    public class UpdateHoldCustomerMoneyCommandHandler : IRequestHandler<UpdateHoldCustomerMoneyCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblHoldCustomerMoney> _holdCustomerMoneyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<UpdateHoldCustomerMoneyCommandHandler> _logger;
        Services.IQueueCallAPIManager _queueCallAPIManager;
        LMS.Common.Helper.Utils _common;

        public UpdateHoldCustomerMoneyCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblHoldCustomerMoney> holdCustomerMoneyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            Services.IQueueCallAPIManager queueCallAPIManager,
        ILogger<UpdateHoldCustomerMoneyCommandHandler> logger
            )
        {
            _holdCustomerMoneyTab = holdCustomerMoneyTab;
            _customerTab = customerTab;
            _userTab = userTab;
            _queueCallAPIManager = queueCallAPIManager;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(UpdateHoldCustomerMoneyCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                if (request.HoldCustomerMoneyID > 0)
                {
                    _holdCustomerMoneyTab.WhereClause(x => x.HoldCustomerMoneyID == request.HoldCustomerMoneyID);
                }
                else if (request.TimaCustomerID > 0)
                {
                    var userInfoTask = _userTab.WhereClause(x => x.UserID == request.ModifyBy).QueryAsync();
                    var customerDetailTask = _customerTab.WhereClause(x => x.TimaCustomerID == request.TimaCustomerID).QueryAsync();
                    await Task.WhenAll(userInfoTask, customerDetailTask);
                    var customerDetail = customerDetailTask.Result.FirstOrDefault();
                    var userInfo = userInfoTask.Result.FirstOrDefault();


                    if (customerDetail == null || customerDetail.CustomerID < 1)
                    {
                        _logger.LogError($"UpdateHoldCustomerMoneyCommandHandler_Not_found_customer|request={_common.ConvertObjectToJSonV2(request)}");
                        return response;
                    }
                    _holdCustomerMoneyTab.WhereClause(x => x.CustomerID == customerDetail.CustomerID && x.Status == (int)HoldCustomerMoney_Status.Hold);
                    if (userInfo == null || userInfo.UserID < 1)
                    {
                        _logger.LogError($"UpdateHoldCustomerMoneyCommandHandler_Waring|Không tìm thấy thông tin người thao tác|request={_common.ConvertObjectToJSonV2(request)}");
                    }
                    //request.Reason = $"{userInfo?.FullName} hủy giữ tiền {customerDetail.FullName}";
                }

                var holdCustomerMoneyInfor = (await _holdCustomerMoneyTab.QueryAsync()).FirstOrDefault();
                if (holdCustomerMoneyInfor == null)
                {
                    response.Message = MessageConstant.HoldCustomerMoneyIDNotExist;
                    return response;
                }
                if (request.Status == (int)HoldCustomerMoney_Status.Hold)
                {
                    holdCustomerMoneyInfor.Amount = request.Amount;
                    holdCustomerMoneyInfor.Reason = request.Reason;
                }
                holdCustomerMoneyInfor.ModifyDate = DateTime.Now;
                holdCustomerMoneyInfor.ModifyBy = request.ModifyBy;
                holdCustomerMoneyInfor.Status = (short)request.Status;
                if (await _holdCustomerMoneyTab.UpdateAsync(holdCustomerMoneyInfor))
                {
                    var customer = (await _customerTab.WhereClause(x => x.CustomerID == holdCustomerMoneyInfor.CustomerID).QueryAsync()).FirstOrDefault();
                    _ = _queueCallAPIManager.UpdateHoldMoney((int)request.ModifyBy, holdCustomerMoneyInfor.Amount, holdCustomerMoneyInfor.Reason, holdCustomerMoneyInfor.HoldCustomerMoneyID, request.Status, customer.TimaCustomerID);
                    response.SetSucces();
                    response.Data = holdCustomerMoneyInfor.HoldCustomerMoneyID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateHoldCustomerMoneyCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
