﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using CollectionServiceApi.Domain.Tables;
using System.Globalization;

namespace CollectionServiceApi.Commands.HoldCustomerMoney
{
    public class CreateHoldCustomerMoneyCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long CustomerID { get; set; }

        public long Amount { get; set; }

        public string Reason { get; set; }

        public long CreateBy { get; set; }

        public long TimaID { get; set; }
        public long UserIDCreate { get; set; }
        public long AGCustomerID { get; set; }
        /// <summary>
        /// false: chỉ thêm vào
        /// true: nếu có thì cập nhật, nghiệp vụ từ mgl hold
        /// </summary>
        public bool ForceUpdate { get; set; }

    }
    public class CreateHoldCustomerMoneyCommandHandler : IRequestHandler<CreateHoldCustomerMoneyCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblHoldCustomerMoney> _holdCustomerMoneyTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        ILogger<CreateHoldCustomerMoneyCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        Services.IQueueCallAPIManager _queueCallAPIManager;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        public CreateHoldCustomerMoneyCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblHoldCustomerMoney> holdCustomerMoneyTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            ILogger<CreateHoldCustomerMoneyCommandHandler> logger,
            Services.IQueueCallAPIManager queueCallAPIManager
            )
        {
            _holdCustomerMoneyTab = holdCustomerMoneyTab;
            _userTab = userTab;
            _logger = logger;
            _queueCallAPIManager = queueCallAPIManager;
            _customerTab = customerTab;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateHoldCustomerMoneyCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var currentDate = DateTime.Now;
                Domain.Tables.TblHoldCustomerMoney holdCustomerMoneyInfor;
                if (request.TimaID > 0)
                {
                    holdCustomerMoneyInfor = (await _holdCustomerMoneyTab.SetGetTop(1).WhereClause(x => x.TimaHoldCustomerMoneyID == request.TimaID).QueryAsync()).FirstOrDefault();
                    if (holdCustomerMoneyInfor != null)
                    {
                        response.Message = MessageConstant.CustomerHoldOrderMoney;
                        _logger.LogError($"CreateHoldCustomerMoneyCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                        return response;
                    }

                    var objCustomer = (await _customerTab.SetGetTop(1).WhereClause(x => x.TimaCustomerID == request.AGCustomerID).QueryAsync()).FirstOrDefault();
                    if (objCustomer == null)
                    {
                        response.Message = MessageConstant.CustomerNotExist;
                        _logger.LogError($"CreateHoldCustomerMoneyCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                        return response;
                    }
                    var objUser = (await _userTab.SetGetTop(1).WhereClause(x => x.TimaUserID == request.UserIDCreate).QueryAsync()).FirstOrDefault();
                    if (objUser == null)
                    {
                        response.Message = MessageConstant.UserNotExist;
                        _logger.LogError($"CreateHoldCustomerMoneyCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                        return response;
                    }
                    request.CustomerID = objCustomer.CustomerID;
                    request.CreateBy = objUser.UserID;
                    // ag thiếu nghiệp vụ đồng bộ unhold, lms xử lý thay phần đồng bộ đó
                    // cập nhật lại status của khách hàng trước đó về unhold nếu có
                    var holdCustomerInfosExists = await _holdCustomerMoneyTab.WhereClause(x => x.CustomerID == request.CustomerID && x.Status == (int)HoldCustomerMoney_Status.Hold).QueryAsync();
                    if (holdCustomerInfosExists != null && holdCustomerInfosExists.Any())
                    {
                        foreach (var item in holdCustomerInfosExists)
                        {
                            item.Status = (int)HoldCustomerMoney_Status.UnHold;
                            item.ModifyBy = request.CreateBy;
                            item.ModifyDate = currentDate;
                        }
                        _holdCustomerMoneyTab.UpdateBulk(holdCustomerInfosExists);
                    }
                }
                else
                {
                    var holdCustomerInfosExists = (await _holdCustomerMoneyTab.WhereClause(x => x.CustomerID == request.CustomerID && x.Status == (int)HoldCustomerMoney_Status.Hold).QueryAsync());
                    if (holdCustomerInfosExists != null && holdCustomerInfosExists.Any())
                    {
                        if (!request.ForceUpdate)
                        {
                            response.Message = MessageConstant.CustomerHoldOrderMoney;
                            _logger.LogError($"CreateHoldCustomerMoneyCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|message={response.Message}");
                            return response;
                        }
                        else
                        {
                            foreach (var item in holdCustomerInfosExists)
                            {
                                item.Status = (int)HoldCustomerMoney_Status.UnHold;
                                item.ModifyBy = request.CreateBy;
                                item.ModifyDate = currentDate;
                            }
                            _holdCustomerMoneyTab.UpdateBulk(holdCustomerInfosExists);
                        }
                    }

                }

                var objHoldCustomerMoney = new TblHoldCustomerMoney
                {
                    CustomerID = request.CustomerID,
                    Amount = request.Amount,
                    Status = (int)HoldCustomerMoney_Status.Hold,
                    Reason = request.Reason,
                    CreateDate = currentDate,
                    ModifyDate = currentDate,
                    CreateBy = request.CreateBy,
                    ModifyBy = request.CreateBy,
                    TimaHoldCustomerMoneyID = request.TimaID
                };
                var holdCustomerMoneyID = _holdCustomerMoneyTab.Insert(objHoldCustomerMoney);
                if (holdCustomerMoneyID > 0)
                {
                    if (request.TimaID == 0)
                    {
                        var customer = (await _customerTab.WhereClause(x => x.CustomerID == request.CustomerID).QueryAsync()).FirstOrDefault();
                        _ = _queueCallAPIManager.InsertHoldMoney(customer.TimaCustomerID, (int)request.CreateBy, DateTime.Now.ToString("yyyy-MM-dd HH:mm"), request.Amount, request.Reason, holdCustomerMoneyID);
                    }
                    response.SetSucces();
                    response.Data = holdCustomerMoneyID;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateHoldCustomerMoneyCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
