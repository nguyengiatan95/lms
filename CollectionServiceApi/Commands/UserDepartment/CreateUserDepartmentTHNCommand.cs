﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.UserDepartment
{
    public class CreateUserDepartmentTHNCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int DepartmentID { get; set; }
        public List<Domain.Models.UserDepartment.CreateUserDepartmentTHN> LstUserDepartment { get; set; }

    }
    public class CreateUserDepartmentTHNCommandHandler : IRequestHandler<CreateUserDepartmentTHNCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> _userDepartmentTab;
        ILogger<CreateUserDepartmentTHNCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateUserDepartmentTHNCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserDepartment> userDepartmentTab,
            ILogger<CreateUserDepartmentTHNCommandHandler> logger
            )
        {
            _userDepartmentTab = userDepartmentTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateUserDepartmentTHNCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstUserID = new List<long>();
                if (request.LstUserDepartment.Count > 0)
                {
                    foreach (var item in request.LstUserDepartment)
                    {
                        lstUserID.Add(item.UserID);
                    }
                    var lstUpdate = (await _userDepartmentTab.WhereClause(x => lstUserID.Contains(x.UserID) && x.DepartmentID == request.DepartmentID).QueryAsync());
                    var lstRequest = request.LstUserDepartment.ToDictionary(x => x.UserID, x => x);
                    foreach (var item in lstUpdate)
                    {
                        var objRequest = lstRequest.GetValueOrDefault(item.UserID);
                        item.ParentUserID = objRequest.ParentUserID;
                        item.ModifyDate = DateTime.Now;
                    }
                    _userDepartmentTab.UpdateBulk(lstUpdate);
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateUserDepartmentTHNCommandHandler_Exception|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
