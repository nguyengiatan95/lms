﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.User
{
    public class SettingUserPhoneCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public int TypeCall { get; set; }
        public string IpPhone { get; set; }
        public string CiscoUserName { get; set; }
        public string CiscoPassword { get; set; }
        public string CiscoExtension { get; set; }
        public long UserID { get; set; }
    }
    public class SettingUserPhoneCommandHandler : IRequestHandler<SettingUserPhoneCommand, LMS.Common.Constants.ResponseActionResult>
    {
        ILogger<SettingUserPhoneCommandHandler> _logger;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblMapUserCall> _mapUserCallTab;
        LMS.Common.Helper.Utils _common;
        public SettingUserPhoneCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblMapUserCall> mapUserCallTab,
            ILogger<SettingUserPhoneCommandHandler> logger
            )
        {
            _mapUserCallTab = mapUserCallTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
        }

        public async Task<ResponseActionResult> Handle(SettingUserPhoneCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            DateTime dtNow = DateTime.Now;
            try
            {
                var lstTypeCall = Enum.GetValues(typeof(LMS.Common.Constants.MapUserCall_TypeCall)).Cast<LMS.Common.Constants.MapUserCall_TypeCall>().Select(x => (int)x).ToList();
                if (!lstTypeCall.Contains(request.TypeCall))
                {
                    response.Message = MessageConstant.MapUserCall_InvalidTypeCall;
                    return response;
                }
                var mapUsers = await _mapUserCallTab.WhereClause(x => x.UserID == request.UserID && lstTypeCall.Contains(x.TypeCall)).QueryAsync();
                int flag = 0;

                if (mapUsers != null && mapUsers.Any())
                {
                    // cập nhật
                    foreach (var item in mapUsers)
                    {
                        item.ModifyDate = dtNow;
                        if (item.TypeCall != request.TypeCall)
                        {
                            item.Status = (int)StatusCommon.UnActive;
                        }
                        else
                        {
                            flag = 1;
                            item.Status = (int)StatusCommon.Active;
                            item.JsonExtra = GetJsonExtraByTypeCall(request.TypeCall, request.CiscoUserName, request.CiscoPassword, request.CiscoExtension, request.IpPhone);
                        }
                    }
                    _mapUserCallTab.UpdateBulk(mapUsers);
                }
                if (flag == 0)
                {
                    // insert mới
                    var userCall = new Domain.Tables.TblMapUserCall
                    {
                        CreateDate = dtNow,
                        ModifyDate = dtNow,
                        JsonExtra = GetJsonExtraByTypeCall(request.TypeCall, request.CiscoUserName, request.CiscoPassword, request.CiscoExtension, request.IpPhone),
                        TypeCall = request.TypeCall,
                        UserID = request.UserID,
                        Status = (int)LMS.Common.Constants.StatusCommon.Active
                    };
                    await _mapUserCallTab.InsertAsync(userCall);
                }
                response.SetSucces();
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"SettingUserPhoneCommandHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private string GetJsonExtraByTypeCall(int typeCall, string ciscoUserName, string ciscoPassword, string ciscoExtension, string ipPhone)
        {
            switch ((LMS.Common.Constants.MapUserCall_TypeCall)typeCall)
            {
                case LMS.Common.Constants.MapUserCall_TypeCall.Cisco:
                    {
                        var userCallCisco = new Domain.Tables.UserCallCiscoExtra
                        {
                            Username = ciscoUserName,
                            Password = ciscoPassword,
                            Extension = ciscoExtension
                        };
                        return _common.ConvertObjectToJSonV2(userCallCisco);
                    }
                case LMS.Common.Constants.MapUserCall_TypeCall.Caresoft:
                    {
                        Domain.Tables.UserCallCareSoftExtra userCallCareSoftExtra = new Domain.Tables.UserCallCareSoftExtra()
                        {
                            IPPhone = ipPhone
                        };
                        return _common.ConvertObjectToJSonV2(userCallCareSoftExtra);
                    }
                default:
                    return "";
            }
        }
    }
}
