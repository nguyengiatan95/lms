﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.MGL
{
    public class CreateSettingTotalMoneyCommand : IRequest<ResponseActionResult>
    {
        public long ApprovalLevelBookDebtID { get; set; }
        public long TotalMoney { get; set; }
        public string ListTypeMoney { get; set; }
        public long UserID { get; set; }
    }
    public class CreateSettingTotalMoneyCommandHandler : IRequestHandler<CreateSettingTotalMoneyCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingTotalMoney> _settingTotalMoneyTab;
        ILogger<CreateSettingTotalMoneyCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateSettingTotalMoneyCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingTotalMoney> settingTotalMoneyTab,
            ILogger<CreateSettingTotalMoneyCommandHandler> logger)
        {
            _settingTotalMoneyTab = settingTotalMoneyTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();

        }

        public async Task<ResponseActionResult> Handle(CreateSettingTotalMoneyCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            DateTime dtNow = DateTime.Now;
            try
            {
                var settingMoney = await _settingTotalMoneyTab.WhereClause(x => x.ApprovalLevelBookDebtID == request.ApprovalLevelBookDebtID).QueryAsync();
                if (settingMoney != null && settingMoney.Any())
                {
                    var currentSetting = settingMoney.FirstOrDefault();
                    currentSetting.TotalMoney = request.TotalMoney;
                    currentSetting.ListTypeMoney = request.ListTypeMoney;
                    await _settingTotalMoneyTab.UpdateAsync(currentSetting);
                }
                else
                {
                    Domain.Tables.TblSettingTotalMoney settingTotalMoney = new Domain.Tables.TblSettingTotalMoney()
                    {
                        ListTypeMoney = request.ListTypeMoney,
                        TotalMoney = request.TotalMoney,
                        ApprovalLevelBookDebtID = request.ApprovalLevelBookDebtID,
                        CreateDate = dtNow,
                        CreateBy = request.UserID
                    };
                    await _settingTotalMoneyTab.InsertAsync(settingTotalMoney);
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateSettingTotalMoneyCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
