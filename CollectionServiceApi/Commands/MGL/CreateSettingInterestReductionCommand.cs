﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.MGL
{
    public class CreateSettingInterestReductionCommand : IRequest<ResponseActionResult>
    {
        public List<Domain.Models.MGL.RequestCreateSetting> model { get; set; }
        public long UserID { get; set; }
    }
    public class CreateSettingInterestReductionCommandHandler : IRequestHandler<CreateSettingInterestReductionCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingInterestReduction> _settingInterestReductionTab;
        ILogger<CreateSettingInterestReductionCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateSettingInterestReductionCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingInterestReduction> settingInterestReductionTab,
            ILogger<CreateSettingInterestReductionCommandHandler> logger)
        {
            _settingInterestReductionTab = settingInterestReductionTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();

        }

        public async Task<ResponseActionResult> Handle(CreateSettingInterestReductionCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                DateTime dtNow = DateTime.Now;
                List<Domain.Tables.TblSettingInterestReduction> settingUpdate = new List<Domain.Tables.TblSettingInterestReduction>();
                List<Domain.Tables.TblSettingInterestReduction> settingInsert = new List<Domain.Tables.TblSettingInterestReduction>();

                if (request.model == null || !request.model.Any())
                {
                    response.Message = MessageConstant.SettingInterestReduction_InvalidData;
                    return response;
                }

                var bookDebtIDs = request.model.Select(x => x.BookDebtID);
                var approvalLevelBookDebtIDs = request.model.Select(x => x.ApprovalLevelBookDebtID);
                var settings = await _settingInterestReductionTab.WhereClause(x => approvalLevelBookDebtIDs.Contains(x.ApprovalLevelBookDebtID) && bookDebtIDs.Contains(x.BookDebtID)).QueryAsync();
                foreach (var item in request.model)
                {
                    var setting = new Domain.Tables.TblSettingInterestReduction()
                    {
                        BookDebtID = item.BookDebtID,
                        MoneyType = item.MoneyType,
                        ApprovalLevelBookDebtID = item.ApprovalLevelBookDebtID,
                        CreateBy = request.UserID,
                        CreateDate = dtNow,
                        MaxMoneyReduce = item.MaxMoneyReduce,
                        PercentMoneyReduce = item.PercentMoneyReduce,
                        //DPDFrom = item.DPDFrom,
                        //DPDTo = item.DPDTo,
                        //ApplyFrom = DateTime.ParseExact(item.ApplyFrom, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture),
                        //ApplyTo = DateTime.ParseExact(item.ApplyTo, TimaSettingConstant.DateTimeDayMonthYear, CultureInfo.InvariantCulture),
                    };
                    if (settings.Where(x => x.BookDebtID == item.BookDebtID && x.ApprovalLevelBookDebtID == item.ApprovalLevelBookDebtID && x.MoneyType == item.MoneyType).Count() > 0)
                    {
                        var currentData = settings.Where(x => x.BookDebtID == item.BookDebtID && x.ApprovalLevelBookDebtID == item.ApprovalLevelBookDebtID && x.MoneyType == item.MoneyType).FirstOrDefault();
                        setting.CreateBy = currentData.CreateBy;
                        setting.CreateDate = currentData.CreateDate;
                        setting.SettingInterestReductionID = currentData.SettingInterestReductionID;
                        settingUpdate.Add(setting);
                    }
                    else
                    {
                        settingInsert.Add(setting);
                    }
                }
                if (settingInsert.Count > 0)
                {
                    _settingInterestReductionTab.InsertBulk(settingInsert);
                }
                if (settingUpdate.Count > 0)
                {
                    _settingInterestReductionTab.UpdateBulk(settingUpdate);
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateSettingInterestReductionCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
