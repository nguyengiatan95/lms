﻿using CollectionServiceApi.Domain.Models.ApprovalLevelBookDebt;
using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.ApprovalLevelBookDebt
{
    public class UpdateApprovalLevelBookDebtCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public List<LstApprovalLevelBookDebt> LstApprovalLevelBookDebt { get; set; }
    }
    public class UpdateApprovalLevelBookDebtCommandHandler : IRequestHandler<UpdateApprovalLevelBookDebtCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> _approvalLevelBookDebtTab;
        ILogger<UpdateApprovalLevelBookDebtCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdateApprovalLevelBookDebtCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> approvalLevelBookDebtTab,
            ILogger<UpdateApprovalLevelBookDebtCommandHandler> logger)
        {
            _approvalLevelBookDebtTab = approvalLevelBookDebtTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();

        }
        public async Task<ResponseActionResult> Handle(UpdateApprovalLevelBookDebtCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var lstApprovalLevelBookDebtID = new List<long>();
                Dictionary<long, LstApprovalLevelBookDebt> lstRequest = new Dictionary<long, LstApprovalLevelBookDebt>();
                if (request.LstApprovalLevelBookDebt.Count > 0)
                {
                    foreach (var item in request.LstApprovalLevelBookDebt)
                    {
                        lstApprovalLevelBookDebtID.Add(item.ApprovalLevelBookDebtID);
                        if (!lstRequest.ContainsKey(item.ApprovalLevelBookDebtID))
                        {
                            lstRequest.Add(item.ApprovalLevelBookDebtID, item);
                        }
                    }
                    var lstUpdate = (await _approvalLevelBookDebtTab.WhereClause(x => lstApprovalLevelBookDebtID.Contains(x.ApprovalLevelBookDebtID)).QueryAsync());
                    //var lstRequest = request.LstApprovalLevelBookDebt.ToDictionary(x => x.ApprovalLevelBookDebtID, x => x);
                    foreach (var item in lstUpdate)
                    {
                        var objRequest = lstRequest.GetValueOrDefault(item.ApprovalLevelBookDebtID);
                        item.ParentID = objRequest.ParentID;
                        item.ModifyDate = DateTime.Now;
                        item.Postion = objRequest.Postion;
                    }
                    _approvalLevelBookDebtTab.UpdateBulk(lstUpdate);
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateApprovalLevelBookDebtCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
