﻿using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace CollectionServiceApi.Commands.ApprovalLevelBookDebt
{
    public class CreateApprovalLevelBookDebtCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long ApprovalLevelBookDebtID { get; set; }
        public string ApprovalLevelBookDebtName { get; set; }
        public int IsAllAccept { get; set; }
        public int Status { get; set; }
    }
    public class CreateApprovalLevelBookDebtCommandHandler : IRequestHandler<CreateApprovalLevelBookDebtCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> _approvalLevelBookDebtTab;
        ILogger<CreateApprovalLevelBookDebtCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateApprovalLevelBookDebtCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblApprovalLevelBookDebt> approvalLevelBookDebtTab,
            ILogger<CreateApprovalLevelBookDebtCommandHandler> logger)
        {
            _approvalLevelBookDebtTab = approvalLevelBookDebtTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();

        }
        public async Task<ResponseActionResult> Handle(CreateApprovalLevelBookDebtCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                if (request.ApprovalLevelBookDebtID == 0)
                {
                    if (string.IsNullOrEmpty(request.ApprovalLevelBookDebtName))
                    {
                        response.Message = MessageConstant.ParameterInvalid;
                        return response;
                    }
                    var objApprovalLevelBookDebt = (await _approvalLevelBookDebtTab.WhereClause(x => x.ApprovalLevelBookDebtName == request.ApprovalLevelBookDebtName
                                                              && x.Status == (int)ApprovalLevelBookDebt_Status.Active).QueryAsync()).FirstOrDefault();
                    if (objApprovalLevelBookDebt != null)
                    {
                        response.Message = MessageConstant.ApprovalLevelBookDebtNameExist;
                        return response;
                    }
                    var approvalLevelBookDebt = new TblApprovalLevelBookDebt()
                    {
                        ApprovalLevelBookDebtName = request.ApprovalLevelBookDebtName,
                        IsAllAccept = request.IsAllAccept,
                        CreateDate = DateTime.Now,
                        ModifyDate = DateTime.Now,
                        Status = request.Status
                    };
                    var approvalLevelBookDebtID = await _approvalLevelBookDebtTab.InsertAsync(approvalLevelBookDebt);
                    if (approvalLevelBookDebtID > 0)
                        response.Data = approvalLevelBookDebtID;
                }
                else
                {
                    var objapprovalLevelBookDebt = (await _approvalLevelBookDebtTab.WhereClause(x => x.ApprovalLevelBookDebtID == request.ApprovalLevelBookDebtID).QueryAsync()).FirstOrDefault();
                    if (objapprovalLevelBookDebt == null)
                    {
                        response.Message = MessageConstant.ParameterInvalid;
                        return response;
                    }
                    objapprovalLevelBookDebt.IsAllAccept = request.IsAllAccept;
                    objapprovalLevelBookDebt.Status = request.Status;
                    objapprovalLevelBookDebt.ApprovalLevelBookDebtName = request.ApprovalLevelBookDebtName;
                    objapprovalLevelBookDebt.ModifyDate = DateTime.Now;
                    if (await _approvalLevelBookDebtTab.UpdateAsync(objapprovalLevelBookDebt))
                        response.Data = objapprovalLevelBookDebt.ApprovalLevelBookDebtID;
                }
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateApprovalLevelBookDebtCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
