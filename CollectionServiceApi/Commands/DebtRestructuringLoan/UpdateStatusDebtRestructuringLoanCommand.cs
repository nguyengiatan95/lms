﻿using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.DebtRestructuringLoan
{
    public class UpdateStatusDebtRestructuringLoanCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long DebtRestructuringLoanID { get; set; }
        public long CreateBy { get; set; }
        public int Status { get; set; }
    }
    public class UpdateStatusDebtRestructuringLoanCommandHandler : IRequestHandler<UpdateStatusDebtRestructuringLoanCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebtRestructuringLoan> _debtRestructuringLoanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogActionDebtRestructuringLoan> _logActionDebtRestructuringLoanTab;
        ILogger<UpdateStatusDebtRestructuringLoanCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdateStatusDebtRestructuringLoanCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebtRestructuringLoan> debtRestructuringLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogActionDebtRestructuringLoan> logActionDebtRestructuringLoanTab,
            ILogger<UpdateStatusDebtRestructuringLoanCommandHandler> logger)
        {
            _loanTab = loanTab;
            _userTab = userTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _debtRestructuringLoanTab = debtRestructuringLoanTab;
            _logActionDebtRestructuringLoanTab = logActionDebtRestructuringLoanTab;
        }

        public async Task<ResponseActionResult> Handle(UpdateStatusDebtRestructuringLoanCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var currentDate = DateTime.Now;
                var firstDateOfMonth = new DateTime(currentDate.Year, currentDate.Month, 1);
                var firstDateOfNextMonth = firstDateOfMonth.AddMonths(1);

                var userInfosTask = _userTab.WhereClause(x => x.UserID == request.CreateBy).QueryAsync();
                var loanDebtRestructuringInfosTask = _debtRestructuringLoanTab.SetGetTop(1)
                                                                .WhereClause(x => x.DebtRestructuringLoanID == request.DebtRestructuringLoanID && x.CreateDate > firstDateOfMonth && x.ExpirationDate < firstDateOfNextMonth)
                                                                .WhereClause(x => x.Status != (int)DebtRestructuringLoan_Status.Deleted)
                                                                .QueryAsync();
                await Task.WhenAll(userInfosTask, loanDebtRestructuringInfosTask);
                var debtRestructuringDetail = loanDebtRestructuringInfosTask.Result.FirstOrDefault();
                if (debtRestructuringDetail == null || debtRestructuringDetail.LoanID < 1)
                {
                    response.Message = MessageConstant.NoData;
                    return response;
                }
                var userInfo = userInfosTask.Result.FirstOrDefault();
                if (userInfo == null || userInfo.UserID < 1)
                {
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                int actionType = 0;
                switch ((DebtRestructuringLoan_Status)request.Status)
                {
                    case DebtRestructuringLoan_Status.Deleted:
                        // nếu đơn chưa đc thực thi và có nguồn từ base
                        if (debtRestructuringDetail.Status == (int)DebtRestructuringLoan_Status.Handled || debtRestructuringDetail.Source != (int)DebtRestructuringLoan_Source.Base)
                        {
                            response.Message = MessageConstant.DebtRestructuringLoan_NotDelete;
                            return response;
                        }
                        actionType = (int)LogActionDebtRestructuringLoan_ActionType.Deleted;
                        break;
                    case DebtRestructuringLoan_Status.Approved:
                        // chưa có quy định cho approved
                        actionType = (int)LogActionDebtRestructuringLoan_ActionType.Approved;
                        response.Message = MessageConstant.InforNotChange;
                        return response;
                    case DebtRestructuringLoan_Status.Handled:
                        //actionType = (int)LogActionDebtRestructuringLoan_ActionType.Executed;
                        response.Message = MessageConstant.InforNotChange;
                        return response;
                        //break;
                    default:
                        response.Message = MessageConstant.InforNotChange;
                        return response;
                }
                var t1 = _logActionDebtRestructuringLoanTab.InsertAsync(new TblLogActionDebtRestructuringLoan
                {
                    CreateBy = request.CreateBy,
                    ActionType = actionType,
                    CreateDate = currentDate,
                    DebtRestructuringLoanID = debtRestructuringDetail.DebtRestructuringLoanID,
                    JsonDataOld = _common.ConvertObjectToJSonV2(debtRestructuringDetail),
                    CreateByName = userInfo.UserName
                });
                debtRestructuringDetail.ModifyDate = currentDate;
                debtRestructuringDetail.Status = request.Status;
                var t2 = _debtRestructuringLoanTab.UpdateAsync(debtRestructuringDetail);
                await Task.WhenAll(t1, t2);
                response.SetSucces();
                response.Data = debtRestructuringDetail.DebtRestructuringLoanID;
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateImageDebtRestructuringLoanCommadHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
