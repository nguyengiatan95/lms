﻿using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.DebtRestructuringLoan
{
    public class UpdateImageDebtRestructuringLoanCommad : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long DebtRestructuringLoanID { get; set; }
        public long CreateBy { get; set; }
        public List<LMS.Entites.Dtos.AG.ImageLosUpload> FileUploads { get; set; }
    }
    public class UpdateImageDebtRestructuringLoanCommadHandler : IRequestHandler<UpdateImageDebtRestructuringLoanCommad, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebtRestructuringLoan> _debtRestructuringLoanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogActionDebtRestructuringLoan> _logActionDebtRestructuringLoanTab;
        ILogger<UpdateImageDebtRestructuringLoanCommadHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdateImageDebtRestructuringLoanCommadHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebtRestructuringLoan> debtRestructuringLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogActionDebtRestructuringLoan> logActionDebtRestructuringLoanTab,
            ILogger<UpdateImageDebtRestructuringLoanCommadHandler> logger)
        {
            _loanTab = loanTab;
            _userTab = userTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _debtRestructuringLoanTab = debtRestructuringLoanTab;
            _logActionDebtRestructuringLoanTab = logActionDebtRestructuringLoanTab;
        }

        public async Task<ResponseActionResult> Handle(UpdateImageDebtRestructuringLoanCommad request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var currentDate = DateTime.Now;
                var firstDateOfMonth = new DateTime(currentDate.Year, currentDate.Month, 1);
                var firstDateOfNextMonth = firstDateOfMonth.AddMonths(1);

                var userInfosTask = _userTab.WhereClause(x => x.UserID == request.CreateBy).QueryAsync();
                var loanDebtRestructuringInfosTask = _debtRestructuringLoanTab.SetGetTop(1)
                                                                .WhereClause(x => x.DebtRestructuringLoanID == request.DebtRestructuringLoanID && x.CreateDate > firstDateOfMonth && x.ExpirationDate < firstDateOfNextMonth)
                                                                .WhereClause(x => x.Status != (int)DebtRestructuringLoan_Status.Deleted)
                                                                .QueryAsync();
                await Task.WhenAll(userInfosTask, loanDebtRestructuringInfosTask);
                var debtRestructuringDetail = loanDebtRestructuringInfosTask.Result.FirstOrDefault();
                if (debtRestructuringDetail == null || debtRestructuringDetail.LoanID < 1)
                {
                    response.Message = MessageConstant.NoData;
                    return response;
                }
                var userInfo = userInfosTask.Result.FirstOrDefault();
                if (userInfo == null || userInfo.UserID < 1)
                {
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                var t1 = _logActionDebtRestructuringLoanTab.InsertAsync(new TblLogActionDebtRestructuringLoan
                {
                    CreateBy = request.CreateBy,
                    ActionType = (int)LogActionDebtRestructuringLoan_ActionType.Update,
                    CreateDate = currentDate,
                    DebtRestructuringLoanID = debtRestructuringDetail.DebtRestructuringLoanID,
                    JsonDataOld = _common.ConvertObjectToJSonV2(debtRestructuringDetail),
                    CreateByName = userInfo.UserName
                });
                debtRestructuringDetail.FileImage = _common.ConvertObjectToJSonV2(request.FileUploads);
                debtRestructuringDetail.ModifyDate = currentDate;
                var t2 = _debtRestructuringLoanTab.UpdateAsync(debtRestructuringDetail);
                await Task.WhenAll(t1, t2);
                response.SetSucces();
                response.Data = debtRestructuringDetail.DebtRestructuringLoanID;
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateImageDebtRestructuringLoanCommadHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
