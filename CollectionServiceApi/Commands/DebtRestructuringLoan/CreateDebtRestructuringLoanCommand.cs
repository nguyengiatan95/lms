﻿using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.DebtRestructuringLoan
{
    public class CreateDebtRestructuringLoanCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string ContractCode { get; set; }
        public long CreateBy { get; set; }
        public List<LMS.Entites.Dtos.AG.ImageLosUpload> FileUploads { get; set; }
    }
    public class CreateDebtRestructuringLoanCommandHandler : IRequestHandler<CreateDebtRestructuringLoanCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebtRestructuringLoan> _debtRestructuringLoanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogActionDebtRestructuringLoan> _logActionDebtRestructuringLoanTab;
        ILogger<CreateDebtRestructuringLoanCommandHandler> _logger;
        Services.IReportManager _reportManager;
        LMS.Common.Helper.Utils _common;
        public CreateDebtRestructuringLoanCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebtRestructuringLoan> debtRestructuringLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogActionDebtRestructuringLoan> logActionDebtRestructuringLoanTab,
            Services.IReportManager reportManager,
            ILogger<CreateDebtRestructuringLoanCommandHandler> logger)
        {
            _loanTab = loanTab;
            _userTab = userTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _debtRestructuringLoanTab = debtRestructuringLoanTab;
            _logActionDebtRestructuringLoanTab = logActionDebtRestructuringLoanTab;
            _reportManager = reportManager;
        }

        public async Task<ResponseActionResult> Handle(CreateDebtRestructuringLoanCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var currentDate = DateTime.Now;
                var firstDateOfMonth = new DateTime(currentDate.Year, currentDate.Month, 1);
                var firstDateOfNextMonth = firstDateOfMonth.AddMonths(1);
                var endOfLastMonth = firstDateOfMonth.AddDays(-1);

                var userInfosTask = _userTab.WhereClause(x => x.UserID == request.CreateBy).QueryAsync();
                var loanInfosTask = _loanTab.WhereClause(x => x.ContactCode == request.ContractCode).QueryAsync();
                await Task.WhenAll(userInfosTask, loanInfosTask);
                var loanInfo = loanInfosTask.Result.FirstOrDefault();
                if (loanInfo == null || loanInfo.LoanID < 1)
                {
                    response.Message = MessageConstant.LoanNotExist;
                    return response;
                }
                var userInfo = userInfosTask.Result.FirstOrDefault();
                if (userInfo == null || userInfo.UserID < 1)
                {
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                var loanDebtRestructuringExists = await _debtRestructuringLoanTab.SetGetTop(1)
                                                                .WhereClause(x => x.LoanID == loanInfo.LoanID && x.CreateDate > firstDateOfMonth && x.ExpirationDate < firstDateOfNextMonth)
                                                                .WhereClause(x => x.Status != (int)DebtRestructuringLoan_Status.Deleted)
                                                                .QueryAsync();
                if (loanDebtRestructuringExists != null && loanDebtRestructuringExists.Any())
                {
                    response.Message = MessageConstant.LoanExist;
                    return response;
                }
                var dictCutoffLoanLastMonth = await _reportManager.GetDictCutOffLoanByLstLoanID(new List<long> { loanInfo.LoanID }, endOfLastMonth);
                var id = await _debtRestructuringLoanTab.InsertAsync(new TblDebtRestructuringLoan
                {
                    LoanID = loanInfo.LoanID,
                    DebtRestructuringType = (int)DebtRestructuringLoan_DebtRestructuringType.DebtRestructure,
                    Status = (int)DebtRestructuringLoan_Status.Approved, // trước mắt cho phép tạo là approved luôn
                    CreateDate = currentDate,
                    ModifyDate = currentDate,
                    ExpirationDate = firstDateOfNextMonth.AddSeconds(-1),
                    CreateBy = request.CreateBy,
                    ApprovedBy = request.CreateBy,
                    Source = (int)DebtRestructuringLoan_Source.Base,
                    ProductID = loanInfo.ProductID.GetValueOrDefault(),
                    ProductName = loanInfo.ProductName,
                    CustomerID = loanInfo.CustomerID.GetValueOrDefault(),
                    CustomerName = loanInfo.CustomerName,
                    LoanContractCode = loanInfo.ContactCode,
                    FileImage = _common.ConvertObjectToJSonV2(request.FileUploads),
                    ExtraData = _common.ConvertObjectToJSonV2(new DebtRestructuringLoanExtraData
                    {
                        CreateByUserName = userInfo.UserName,
                        ApprovedByUserName = userInfo.UserName
                    }),
                    DpdBom = endOfLastMonth.Subtract((dictCutoffLoanLastMonth.GetValueOrDefault(loanInfo.LoanID)?.NextDate ?? DateTime.MinValue)).Days
                });
                _ = await _logActionDebtRestructuringLoanTab.InsertAsync(new TblLogActionDebtRestructuringLoan
                {
                    ActionType = (int)LogActionDebtRestructuringLoan_ActionType.Insert,
                    CreateBy = request.CreateBy,
                    CreateDate = currentDate,
                    DebtRestructuringLoanID = id,
                    CreateByName = userInfo.UserName
                });
                response.SetSucces();
                response.Data = id;
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateDebtRestructuringLoanCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
