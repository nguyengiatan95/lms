﻿using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.DebtRestructuringLoan
{
    public class UpdateStatusByEventDebtRestructuringLoanIDCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public long LoanID { get; set; }
        public long CreateBy { get; set; }
        public int DebtRestructuringType { get; set; }
    }
    public class UpdateStatusByEventDebtRestructuringLoanIDCommandHandler : IRequestHandler<UpdateStatusByEventDebtRestructuringLoanIDCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCutOffLoan> _cutOffLoanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebtRestructuringLoan> _debtRestructuringLoanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogActionDebtRestructuringLoan> _logActionDebtRestructuringLoanTab;
        ILogger<UpdateStatusByEventDebtRestructuringLoanIDCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public UpdateStatusByEventDebtRestructuringLoanIDCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblDebtRestructuringLoan> debtRestructuringLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLogActionDebtRestructuringLoan> logActionDebtRestructuringLoanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCutOffLoan> cutOffLoanTab,
            ILogger<UpdateStatusByEventDebtRestructuringLoanIDCommandHandler> logger)
        {
            _loanTab = loanTab;
            _userTab = userTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _debtRestructuringLoanTab = debtRestructuringLoanTab;
            _logActionDebtRestructuringLoanTab = logActionDebtRestructuringLoanTab;
            _cutOffLoanTab = cutOffLoanTab;
        }

        public async Task<ResponseActionResult> Handle(UpdateStatusByEventDebtRestructuringLoanIDCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var currentDate = DateTime.Now;
                var firstDateOfMonth = new DateTime(currentDate.Year, currentDate.Month, 1);
                var firstDateOfNextMonth = firstDateOfMonth.AddMonths(1);

                var userInfosTask = _userTab.WhereClause(x => x.UserID == request.CreateBy).QueryAsync();
                var loanDebtRestructuringInfosTask = _debtRestructuringLoanTab.SetGetTop(1)
                                                                .WhereClause(x => x.LoanID == request.LoanID && x.CreateDate > firstDateOfMonth && x.ExpirationDate < firstDateOfNextMonth)
                                                                .WhereClause(x => x.Status != (int)DebtRestructuringLoan_Status.Deleted && x.DebtRestructuringType == request.DebtRestructuringType)
                                                                .QueryAsync();
                var loanInfosTask = _loanTab.WhereClause(x => x.LoanID == request.LoanID).QueryAsync();
                await Task.WhenAll(userInfosTask, loanDebtRestructuringInfosTask, loanInfosTask);
                var loanInfo = loanInfosTask.Result.FirstOrDefault();
                if (loanInfo == null || loanInfo.LoanID < 1)
                {
                    return response;
                }
                var userInfo = userInfosTask.Result.FirstOrDefault();
                if (userInfo == null || userInfo.UserID < 1)
                {
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                int actionType = (int)LogActionDebtRestructuringLoan_ActionType.Executed;
                var debtRestructuringDetail = loanDebtRestructuringInfosTask.Result.FirstOrDefault();
                if (debtRestructuringDetail == null || debtRestructuringDetail.LoanID < 1)
                {
                    var lastDateOfLastMonth = firstDateOfMonth.AddDays(-1);
                    var cutOffLoanDetail = (await _cutOffLoanTab.SetGetTop(1).WhereClause(x => x.LoanID == request.LoanID && x.CutOffDate <= lastDateOfLastMonth.Date).QueryAsync()).FirstOrDefault();
                    // loại giãn nợ              
                    var expiradtionDate = firstDateOfNextMonth.AddDays(-1);
                    if (request.DebtRestructuringType == (int)DebtRestructuringLoan_DebtRestructuringType.DebtRelief)
                    {
                        expiradtionDate = firstDateOfMonth.AddYears(100);
                    }
                    debtRestructuringDetail = new Domain.Tables.TblDebtRestructuringLoan
                    {
                        LoanID = request.LoanID,
                        DebtRestructuringType = request.DebtRestructuringType,
                        CreateBy = request.CreateBy,
                        ApprovedBy = request.CreateBy,
                        CreateDate = currentDate,
                        ModifyDate = currentDate,
                        ExpirationDate = expiradtionDate,
                        Source = (int)DebtRestructuringLoan_Source.System,
                        ProductID = loanInfo.ProductID.GetValueOrDefault(),
                        ProductName = loanInfo.ProductName,
                        CustomerID = loanInfo.CustomerID.GetValueOrDefault(),
                        CustomerName = loanInfo.CustomerName,
                        LoanContractCode = loanInfo.ContactCode,
                        DpdBom = lastDateOfLastMonth.Date.Subtract((cutOffLoanDetail?.NextDate.Date ?? DateTime.MinValue.Date)).Days,
                        Status = (int)DebtRestructuringLoan_Status.Handled,
                        DebtRestructuingBy = request.CreateBy,
                        ExtraData = _common.ConvertObjectToJSonV2(new DebtRestructuringLoanExtraData
                        {
                            ApprovedByUserName = userInfo.UserName,
                            CreateByUserName = userInfo.UserName,
                            DebtRestructuringByUserName = userInfo.UserName
                        })
                    };
                    debtRestructuringDetail.DebtRestructuringLoanID = await _debtRestructuringLoanTab.InsertAsync(debtRestructuringDetail);
                    actionType = (int)LogActionDebtRestructuringLoan_ActionType.Executed;
                }
                else
                {
                    DebtRestructuringLoanExtraData extraData = _common.ConvertJSonToObjectV2<DebtRestructuringLoanExtraData>(debtRestructuringDetail.ExtraData);
                    extraData.DebtRestructuringByUserName = userInfo.UserName;
                    debtRestructuringDetail.DebtRestructuingBy = userInfo.UserID;
                    debtRestructuringDetail.ModifyDate = currentDate;
                    debtRestructuringDetail.Status = (int)DebtRestructuringLoan_Status.Handled;
                    var t2 = await _debtRestructuringLoanTab.UpdateAsync(debtRestructuringDetail);
                }
                _ = await _logActionDebtRestructuringLoanTab.InsertAsync(new TblLogActionDebtRestructuringLoan
                {
                    CreateBy = request.CreateBy,
                    ActionType = actionType,
                    CreateDate = currentDate,
                    DebtRestructuringLoanID = debtRestructuringDetail.DebtRestructuringLoanID,
                    JsonDataOld = _common.ConvertObjectToJSonV2(debtRestructuringDetail),
                    CreateByName = userInfo.UserName
                });
                response.SetSucces();
                response.Data = debtRestructuringDetail.DebtRestructuringLoanID;
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateImageDebtRestructuringLoanCommadHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
