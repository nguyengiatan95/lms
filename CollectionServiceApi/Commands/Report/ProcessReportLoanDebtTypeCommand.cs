﻿using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.Report
{
    public class ProcessReportLoanDebtTypeCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {

    }
    public class ProcessReportLoanDebtTypeCommandHandler : IRequestHandler<ProcessReportLoanDebtTypeCommand, LMS.Common.Constants.ResponseActionResult>
    {
        readonly ILogger<ProcessReportLoanDebtTypeCommandHandler> _logger;
        readonly LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> _settingKeyTab;
        readonly LMS.Common.Helper.Utils _common;
        readonly Services.IReportManager _reportManager;
        public ProcessReportLoanDebtTypeCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> settingKeyTab,
            Services.IReportManager reportManager,
            ILogger<ProcessReportLoanDebtTypeCommandHandler> logger
            )
        {
            _settingKeyTab = settingKeyTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _reportManager = reportManager;
        }

        public async Task<ResponseActionResult> Handle(ProcessReportLoanDebtTypeCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var keySettingInfo = (await _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.ReportLoanDebtType_ProcessEveryHour).QueryAsync()).FirstOrDefault();
                if (keySettingInfo == null)
                {
                    keySettingInfo = new Domain.Tables.TblSettingKey
                    {
                        CreateDate = DateTime.Now,
                        KeyName = LMS.Common.Constants.SettingKey_KeyValue.ReportLoanDebtType_ProcessEveryHour,
                        Status = (int)SettingKey_Status.Active,
                        ModifyDate = DateTime.Now,
                        Value = _common.ConvertObjectToJSonV2(new SettingKeyReportLoanDebtType
                        {
                            LatestTime = DateTime.Now.AddDays(-1),
                            LastestTransactionID = 0
                        })
                    };
                    keySettingInfo.SettingKeyID = await _settingKeyTab.InsertAsync(keySettingInfo);
                }
                if (keySettingInfo.Status == (int)SettingKey_Status.InActice)
                {
                    response.Message = MessageConstant.SettingKey_PlanCloseLoan_ReportLoanDebtType_ProcessEveryHourStatus;
                    return response;
                }
                SettingKeyReportLoanDebtType settingKeyValue = new SettingKeyReportLoanDebtType() { LatestTime = DateTime.Now.AddDays(-1), LastestTransactionID = 0 };
                if (!string.IsNullOrEmpty(keySettingInfo.Value))
                {
                    try
                    {
                        settingKeyValue = _common.ConvertJSonToObjectV2<SettingKeyReportLoanDebtType>(keySettingInfo.Value);
                    }
                    catch
                    {
                        settingKeyValue = new SettingKeyReportLoanDebtType
                        {
                            LatestTime = DateTime.Now.AddDays(-1),
                            LastestTransactionID = 0
                        };
                        keySettingInfo.Value = _common.ConvertObjectToJSonV2(settingKeyValue);
                    }
                }
                keySettingInfo.Status = (int)SettingKey_Status.InActice;
                keySettingInfo.ModifyDate = DateTime.Now;
                _ = await _settingKeyTab.UpdateAsync(keySettingInfo);
                _ = RunUntilFinished(keySettingInfo, settingKeyValue);
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSummaryEmployeeHandleLoanCommandHandler|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private async Task RunUntilFinished(Domain.Tables.TblSettingKey keySettingInfo, SettingKeyReportLoanDebtType settingkeyValue)
        {
            try
            {
                // process hiện tại chỉ chạy 1 lần là xong
                var actionResult = await _reportManager.ProcessReportLoanDebtType(settingkeyValue);
                if (actionResult.Result == (int)ResponseAction.Success)
                {
                    var settingKeyValue = (SettingKeyReportLoanDebtType)actionResult.Data;
                    settingKeyValue.CountTimeHandleFinished = Convert.ToInt64(DateTime.Now.Subtract(keySettingInfo.ModifyDate).TotalMilliseconds);
                    keySettingInfo.Status = (int)SettingKey_Status.Active;
                    keySettingInfo.ModifyDate = DateTime.Now;
                    keySettingInfo.Value = _common.ConvertObjectToJSonV2(settingKeyValue);
                    _ = await _settingKeyTab.UpdateAsync(keySettingInfo);
                    return;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ProcessSummaryEmployeeHandleLoanCommandHandler_RunUntilFinished|ex={ex.Message}-{ex.StackTrace}");
            }
        }
    }

}
