﻿using CollectionServiceApi.Domain.Tables;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CollectionServiceApi.Commands.ConfigHoldCodeForSmartDialer
{
    public class CreateConfigHoldCodeForSmartDialerCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public List<string> LstReasonCode { get; set; }
        public int SkipTime { get; set; }

    }
    public class CreateConfigHoldCodeForSmartDialerCommandHandler : IRequestHandler<CreateConfigHoldCodeForSmartDialerCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigHoldCodeForSmartDialer> _configHoldCodeForSmartDialerTab;
        ILogger<CreateConfigHoldCodeForSmartDialerCommandHandler> _logger;
        LMS.Common.Helper.Utils _common;
        public CreateConfigHoldCodeForSmartDialerCommandHandler(
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblConfigHoldCodeForSmartDialer> configHoldCodeForSmartDialerTab,
            ILogger<CreateConfigHoldCodeForSmartDialerCommandHandler> logger)
        {
            _configHoldCodeForSmartDialerTab = configHoldCodeForSmartDialerTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();

        }
        public async Task<ResponseActionResult> Handle(CreateConfigHoldCodeForSmartDialerCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var configHoldCodeForSmartDialer = (await _configHoldCodeForSmartDialerTab.QueryAsync()).FirstOrDefault();
                string strReasonCode = string.Join(",", request.LstReasonCode);
                configHoldCodeForSmartDialer.ReasonCodes =strReasonCode;
                configHoldCodeForSmartDialer.SkipTime =request.SkipTime;
                if (await _configHoldCodeForSmartDialerTab.UpdateAsync(configHoldCodeForSmartDialer))
                {
                    response.SetSucces();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateConfigHoldCodeForSmartDialerCommandHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
