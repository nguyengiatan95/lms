﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblLogChangePhone")]
    public class TblLogChangePhone
    {
        [Dapper.Contrib.Extensions.Key]
        public long LogChangePhoneID { get; set; }

        public long LoanID { get; set; }

        public long CustomerID { get; set; }

        public string OldPhone { get; set; }

        public string NewPhone { get; set; }

        public long? CreateBy { get; set; }

        public string CreateByName { get; set; }

        public DateTime CreateDate { get; set; }
        public string Note { get; set; }
    }
}
