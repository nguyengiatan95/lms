﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblPreparationCollectionLoan")]
    public class TblPreparationCollectionLoan
    {
        [Dapper.Contrib.Extensions.Key]
        public long PreparationCollectionLoanID { get; set; }

        public long? LoanID { get; set; }

        public int? TypeID { get; set; }

        public long? UserID { get; set; }

        public string FullName { get; set; }

        public long? TotalMoney { get; set; }

        public string Description { get; set; }

        public DateTime PreparationDate { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime? ModifyDate { get; set; }

        public long? CreateBy { get; set; }

        public int? Status { get; set; }
        public long TimaPreparationCollectionLoanID { get; set; }
        public int PreparationType { get; set; }


    }
}
