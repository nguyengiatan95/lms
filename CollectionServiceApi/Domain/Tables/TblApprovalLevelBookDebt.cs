﻿using System;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblApprovalLevelBookDebt")]
    public class TblApprovalLevelBookDebt
    {
        [Dapper.Contrib.Extensions.Key]
        public long ApprovalLevelBookDebtID { get; set; }

        public string ApprovalLevelBookDebtName { get; set; }

        public int ParentID { get; set; }
        public int Postion { get; set; }

        public DateTime? CreateDate { get; set; }

        public int Status { get; set; }

        public DateTime? ModifyDate { get; set; }
        public int IsAllAccept { get; set; }
    }
}
