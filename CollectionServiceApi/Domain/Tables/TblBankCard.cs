﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblBankCard")]
    public class TblBankCard
    {
        [Dapper.Contrib.Extensions.Key]
        public long BankCardID { get; set; }

        public long? BankID { get; set; }

        public string NumberAccount { get; set; }

        public string BankCode { get; set; }

        public short? Status { get; set; }

        public DateTime? CreateOn { get; set; }

        public DateTime? ModifyOn { get; set; }

        public string AccountHolderName { get; set; }

        public string BranchName { get; set; }

        public short? TypePurpose { get; set; }

        public string AliasName { get; set; }

        public string AliasNameForFast { get; set; }

        public int? ParentID { get; set; }

        public int? ApplyAutoDisbursement { get; set; }

        public string AccountancyLevel1 { get; set; }

        public string AccountancyLevel2 { get; set; }

        public long? CompanyID { get; set; }
    }
}
