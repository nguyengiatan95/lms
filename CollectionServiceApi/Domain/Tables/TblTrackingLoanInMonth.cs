﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblTrackingLoanInMonth")]
    public class TblTrackingLoanInMonth
    {
        [Dapper.Contrib.Extensions.Key]
        public long TrackingLoanID { get; set; }

        public long LoanID { get; set; }

        public DateTime? NextDateAtFirstDayOfMonth { get; set; }

        public long TotalLoanMoneyCurrent { get; set; }

        public long ProductTypeID { get; set; }

        public long CityID { get; set; }

        public long DistrictID { get; set; }

        public long WardID { get; set; }

        public int LoanStatus { get; set; }

        public long TotalMoneyReceied { get; set; }

        public int YearMonth { get; set; }

        public int DebtType { get; set; }

        public DateTime? LoanFinshedDate { get; set; }

        public string ExtraData { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? ModifyDate { get; set; }
    }
}
