﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblWard")]
    public class TblWard
    {
        [Dapper.Contrib.Extensions.Key]
        public long WardID { get; set; }

        public string Name { get; set; }

        public string TypeWard { get; set; }

        public string LongitudeLatitude { get; set; }

        public long? DistrictID { get; set; }
    }
}
