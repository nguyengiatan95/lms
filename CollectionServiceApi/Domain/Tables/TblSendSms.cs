﻿  using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblSendSms")]
    public class TblSendSms
    {
        [Dapper.Contrib.Extensions.Key]
        public long SendSmsID { get; set; }

        public string TemplateName { get; set; }

        public string ContentSms { get; set; }

        public string PhoneSend { get; set; }

        public string PhoneReceived { get; set; }

        public long CreateBy { get; set; }

        public int? Status { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? ModifyDate { get; set; }
    }
}
