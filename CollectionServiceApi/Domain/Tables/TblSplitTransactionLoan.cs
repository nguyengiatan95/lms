﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblSplitTransactionLoan")]
    public class TblSplitTransactionLoan
    {
        [Dapper.Contrib.Extensions.Key]
        public long SplitTransactionLoanID { get; set; }

        public long LoanID { get; set; }

        public long LenderID { get; set; }
        /// <summary>
        /// enum Transaction_TypeMoney
        /// </summary>
        public long TotalMoney { get; set; }
        /// <summary>
        /// enum Transaction_Action
        /// </summary>
        public int ActionID { get; set; }

        public int MoneyType { get; set; }

        public long ReferTxnID { get; set; }

        public DateTime CreateDate { get; set; }
    }
}
