﻿using System;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblPlanHandleLoanDaily")]
    public class TblPlanHandleLoanDaily
    {
        [Dapper.Contrib.Extensions.Key]
        public long PlanHandleLoanDailyID { get; set; }

        public long? UserID { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public long LoanID { get; set; }

        public int Status { get; set; }

        public int? TypeHandle { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? ModifyDate { get; set; }

        public long? ModifyBy { get; set; }
        public DateTime? TimeCheckin { get; set; }
        public DateTime? TimeCheckOut { get; set; }
        public string LatLngCheckIn { get; set; }
        public string LatLngCheckOut { get; set; }
        public string LocationName { get; set; }
        public int StatusCheckIn { get; set; }
        public string CodeCheckIn { get; set; }
    }
}
