﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblLogLoanExemptionAction")]
    public class TblLogLoanExemptionAction
    {
        [Dapper.Contrib.Extensions.Key]
        public long LogLoanExemptionActionID { get; set; }

        public long LoanExemptionID { get; set; }

        public int ActionType { get; set; }

        public DateTime CreateDate { get; set; }

        public long CreateBy { get; set; }

        public string CreateByName { get; set; }
    }
}
