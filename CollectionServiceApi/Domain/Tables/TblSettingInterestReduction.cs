﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Table("TblSettingInterestReduction")]
    public class TblSettingInterestReduction
    {
        [Key]
        public long SettingInterestReductionID { get; set; }

        public long ApprovalLevelBookDebtID { get; set; }

        public long BookDebtID { get; set; }

        public long MoneyType { get; set; }

        public decimal PercentMoneyReduce { get; set; }

        public long MaxMoneyReduce { get; set; }

        public DateTime CreateDate { get; set; }

        public long CreateBy { get; set; }
        //public DateTime? ApplyFrom { get; set; }

        //public DateTime? ApplyTo { get; set; }

        //public int DPDFrom { get; set; }

        //public int DPDTo { get; set; }
    }
}
