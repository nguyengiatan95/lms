﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblPlanHandleCallDaily")]
    public class TblPlanHandleCallDaily
    {
        [Dapper.Contrib.Extensions.Key]
        public long PlanHandleCallDailyID { get; set; }

        public DateTime CallDate { get; set; }

        public string NumberPhone { get; set; }

        public long CustomerID { get; set; }

        public int PriorityOrder { get; set; }

        public long UserID { get; set; }

        public long CampaignID { get; set; }

        public int Status { get; set; }

        public int CallMaxCount { get; set; }

        public int CallCount { get; set; }

        public int CallResult { get; set; }

        public int DPD { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }

        public string Note { get; set; }
        public string CustomerCallName { get; set; }
        // cách nhau = ','
        public string LoanIDs { get; set; }
        public long TotalMoneyNeedPay { get; set; }
        public string HoldCode { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int ConnectTime { get; set; }
        public long TotalMoneyOriginal { get; set; }
    }
}
