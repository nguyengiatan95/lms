﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblCustomerExtensionThirdParty")]
    public class TblCustomerExtensionThirdParty
    {
        [Dapper.Contrib.Extensions.Key]
        public long CustomerExtensionThirdPartyID { get; set; }

        public string TraceIDRequest { get; set; }

        public long LoanID { get; set; }

        public long TimaCodeID { get; set; }

        public string CodeTranData { get; set; }

        public short Status { get; set; }

        public long CreateBy { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }

        public int? CountRetries { get; set; }

        public string ThirdPartyData { get; set; }

    }
}
