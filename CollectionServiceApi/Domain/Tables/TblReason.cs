﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblReason")]
    public class TblReason
    {
        [Dapper.Contrib.Extensions.Key]
        public long ReasonID { get; set; }

        public string Description { get; set; }

        public string ReasonCode { get; set; }

        public int DepartmentID { get; set; }

        public int Status { get; set; }

        public int Postion { get; set; }
    }

}
