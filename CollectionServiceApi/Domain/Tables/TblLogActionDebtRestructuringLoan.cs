﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblLogActionDebtRestructuringLoan")]
    public class TblLogActionDebtRestructuringLoan
    {
        [Dapper.Contrib.Extensions.Key]
        public long LogDebtRestructuringLoanID { get; set; }

        public long DebtRestructuringLoanID { get; set; }

        public long CreateBy { get; set; }

        public int ActionType { get; set; }

        public DateTime CreateDate { get; set; }

        public string JsonDataOld { get; set; }
        public string CreateByName { get; set; }
    }
}
