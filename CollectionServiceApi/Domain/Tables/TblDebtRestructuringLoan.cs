﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblDebtRestructuringLoan")]
    public class TblDebtRestructuringLoan
    {
        [Dapper.Contrib.Extensions.Key]
        public long DebtRestructuringLoanID { get; set; }

        public long LoanID { get; set; }

        public int DebtRestructuringType { get; set; }

        public int Status { get; set; }

        public string Reason { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }

        public DateTime ExpirationDate { get; set; }

        public long CreateBy { get; set; }

        public long? ApprovedBy { get; set; }

        public long? DebtRestructuingBy { get; set; }

        public int Source { get; set; }

        public long ProductID { get; set; }

        public string ProductName { get; set; }

        public long CustomerID { get; set; }

        public string CustomerName { get; set; }

        public string LoanContractCode { get; set; }

        public string ExtraData { get; set; }

        public string FileImage { get; set; }
        public int DpdBom { get; set; }
    }

    public class DebtRestructuringLoanExtraData
    {
        public string CreateByUserName { get; set; }
        public string ApprovedByUserName { get; set; }
        public string DebtRestructuringByUserName { get; set; }
    }
}
