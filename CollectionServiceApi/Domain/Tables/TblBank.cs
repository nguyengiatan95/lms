﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblBank")]
    public class TblBank
    {
        [Dapper.Contrib.Extensions.Key]
        public int Id { get; set; }

        public string NameBank { get; set; }

        public string Code { get; set; }

        public bool? Status { get; set; }

        public short? TypeBank { get; set; }

        public int? ValueOfBank { get; set; }
    }
}
