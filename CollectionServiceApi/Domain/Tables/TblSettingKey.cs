﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("tblSettingKey")]
    public class TblSettingKey
    {
        [Dapper.Contrib.Extensions.Key]
        public long SettingKeyID { get; set; }
        public string KeyName { get; set; }
        public string Value { get; set; }
        public int Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public int? Sleep { get; set; }
        public string EndDate { get; set; }
    }

    public class SettingKeyReportEmployeeHandleLoan
    {
        public DateTime LatestTime { get; set; }
        public long LatestCommentID { get; set; }
        public long CountTimeHandleFinished { get; set; }
    }

     public class SettingKeyReportLoanDebtType
    {
        public DateTime LatestTime { get; set; }
        public long LastestTransactionID { get; set; }
        public long CountTimeHandleFinished { get; set; }
        public DateTime FromDate { get; set; }
    }
}
