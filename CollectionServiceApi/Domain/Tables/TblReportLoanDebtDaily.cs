﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{

    [Dapper.Contrib.Extensions.Table("TblReportLoanDebtDaily")]
    public class TblReportLoanDebtDaily
    {

        [Dapper.Contrib.Extensions.Key]
        public long ReportLoanDebtDailyID { get; set; }

        public DateTime ReportDate { get; set; }

        public long LoanID { get; set; }

        public long TimaLoanID { get; set; }

        public long LosLoanID { get; set; }

        public string LosLoanCode { get; set; }

        public string LmsContractCode { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public long TotalMoneyDisbursement { get; set; }

        public long TotalMoneyCurrent { get; set; }

        public DateTime NextDate { get; set; }

        public DateTime? LastDateOfPay { get; set; }

        public int DPD { get; set; }

        public int TypeDebt { get; set; }

        public long TotalMoneyNeedPay { get; set; }

        public long MoneyOriginalNeedPay { get; set; }

        public long MoneyInteresetNeedPay { get; set; }

        public long MoneyConsultantNeedPay { get; set; }

        public long MoneyServiceNeedPay { get; set; }

        public long MoneyFineLateNeedPay { get; set; }

        public long TotalMoneyCloseLoan { get; set; }

        public long MoneyOriginalCloseLoan { get; set; }

        public long MoneyInterestCloseLoan { get; set; }

        public long MoneyConsultantCloseLoan { get; set; }

        public long MoneyServiceCloseLoan { get; set; }

        public long MoneyFineLateCloseLoan { get; set; }

        public long MoneyFineOriginalCloseLoan { get; set; }

        public long CustomerID { get; set; }

        public string CustomerName { get; set; }

        public long ProductID { get; set; }

        public string ProductName { get; set; }

        public long CityID { get; set; }

        public long DistrictID { get; set; }

        public long WardID { get; set; }

        public long LenderID { get; set; }

        public string LenderCode { get; set; }

        public int? LoanStatus { get; set; }

        public int LoanStatusDeposit { get; set; }

        public int LoanStatusInsurance { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }
        public long OwnerShopID { get; set; }
        public string OwnerShopName { get; set; }
        public long ConsultantShopID { get; set; }
        public string ConsultantShopName { get; set; }
        public int SourceByInsurance { get; set; }
    }
}
