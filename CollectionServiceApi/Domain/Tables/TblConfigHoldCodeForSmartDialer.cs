﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblConfigHoldCodeForSmartDialer")]
    public class TblConfigHoldCodeForSmartDialer
    {
        [Dapper.Contrib.Extensions.Key]
        public long ConfigHoldCodeForSmartDialerID { get; set; }

        public int SkipTime { get; set; }

        public string ReasonCodes { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }

        public int Status { get; set; }
    }
}
