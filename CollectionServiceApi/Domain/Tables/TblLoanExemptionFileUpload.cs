﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblLoanExemptionFileUpload")]
    public class TblLoanExemptionFileUpload
    {
        [Dapper.Contrib.Extensions.Key]
        public long LoanExemptionFileUploadID { get; set; }

        public long LoanExemptionID { get; set; }
        public long LoanID { get; set; }
        public int TypeFile { get; set; }

        public string LstImg { get; set; }

        public int Status { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }

        public long CreateBy { get; set; }

        public long ModifyBy { get; set; }
    }
}
