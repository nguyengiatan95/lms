﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblCustomerBank")]
    public class TblCustomerBank
    {
        [Dapper.Contrib.Extensions.Key]
        public long CustomerBankID { get; set; }

        public string AccountNo { get; set; }

        public string AccountName { get; set; }

        public string BankCode { get; set; }

        public string BankName { get; set; }

        public string MapID { get; set; }

        public int? StatusVa { get; set; }

        public DateTime? EndDate { get; set; }

        public long? CustomerID { get; set; }

        public DateTime? CreateDate { get; set; }

        public int? PartnerCode { get; set; }

        public long LoanID { get; set; }
        public int? Repeat { get; set; }

    }
}
