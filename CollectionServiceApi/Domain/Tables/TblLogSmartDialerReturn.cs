﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblLogSmartDialerReturn")]
    public class TblLogSmartDialerReturn
    {
        [Dapper.Contrib.Extensions.Key]
        public long LogSmartDialerReturnID { get; set; }

        public long ReturnID { get; set; }

        public string ExtraData { get; set; }

        public DateTime CreateDate { get; set; }
    }
}
