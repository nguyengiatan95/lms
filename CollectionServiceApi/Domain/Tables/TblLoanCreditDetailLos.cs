﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{

    [Dapper.Contrib.Extensions.Table("TblLoanCreditDetailLos")]
    public class TblLoanCreditDetailLos
    {
        [Dapper.Contrib.Extensions.Key]
        public long LoanCreditDetailLosID { get; set; }

        public long LoanID { get; set; }

        public long LoanCreditIDOfPartner { get; set; }

        public string JsonExtra { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }
    }
}
