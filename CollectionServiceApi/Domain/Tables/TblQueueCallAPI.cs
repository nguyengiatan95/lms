﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Table("TblQueueCallAPI")]
    public class TblQueueCallAPI
    {
        [Key]
        public long QueueCallAPIID { get; set; }

        public string Domain { get; set; }

        public string Path { get; set; }

        public string Method { get; set; }

        public string JsonRequest { get; set; }

        public string JsonResponse { get; set; }

        public DateTime? LastModifyDate { get; set; }

        public short? Retry { get; set; }

        public DateTime? CreateDate { get; set; }

        public int? Status { get; set; }

        public string Description { get; set; }
    }

}
