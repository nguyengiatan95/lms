﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblConfigUserMapSmartDialer")]
    public class TblConfigUserMapSmartDialer
    {
        [Dapper.Contrib.Extensions.Key]
        public long ConfigUserMapSmartDialerID { get; set; }

        public long CampaignID { get; set; }

        public string UserNameCisco { get; set; }

        public long UserID { get; set; }

        public int Status { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModiyDate { get; set; }

        public long ModifyBy { get; set; }
        public string CampaignName { get; set; }
        public int WrapupTime { get; set; }
    }
}
