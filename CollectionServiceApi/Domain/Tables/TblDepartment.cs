﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblDepartment")]
    public class TblDepartment
    {
        [Dapper.Contrib.Extensions.Key]
        public long DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public DateTime CreateDate { get; set; }
        public int Status { get; set; }
        public long ParentID { get; set; }
        public int AppID { get; set; }
    }
}
