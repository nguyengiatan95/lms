﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblHistoryApprovalLevelBookHandleInMonth")]
    public class TblHistoryApprovalLevelBookHandleInMonth
    {
        [Dapper.Contrib.Extensions.Key]
        public long HistoryID { get; set; }

        public long ApprovalLevelBookDebtID { get; set; }

        public long LoanID { get; set; }

        public long TimaLoanID { get; set; }

        public long MoneyApproved { get; set; }

        public int MoneyType { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }

        public int Status { get; set; }
        public long CreateBy { get; set; }
    }
}
