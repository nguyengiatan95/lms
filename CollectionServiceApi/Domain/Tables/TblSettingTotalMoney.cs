﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Table("TblSettingTotalMoney")]
    public class TblSettingTotalMoney
    {
        [Key]
        public long SettingTotalMoneyID { get; set; }

        public long TotalMoney { get; set; }

        public long ApprovalLevelBookDebtID { get; set; }

        public string ListTypeMoney { get; set; }

        public DateTime? CreateDate { get; set; }

        public long? CreateBy { get; set; }
    }
}
