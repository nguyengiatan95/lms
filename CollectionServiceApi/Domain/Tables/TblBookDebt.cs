﻿using System;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblBookDebt")]
    public class TblBookDebt
    {
        [Dapper.Contrib.Extensions.Key]
        public long BookDebtID { get; set; }
        public string BookDebtName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }

        public int Status { get; set; }

        public int CreateBy { get; set; }

        public int YearDebt { get; set; }
        public int DPDFrom { get; set; }
        public int DPDTo { get; set; }
    }
}
