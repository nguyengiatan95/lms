﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblCutOffLoan")]
    public class TblCutOffLoan
    {
        [Dapper.Contrib.Extensions.Key]
        public long CutOffLoanID { get; set; }

        public long LoanID { get; set; }

        public DateTime NextDate { get; set; }

        public long TotalMoneyCurrent { get; set; }

        public DateTime LastDateOfPay { get; set; }

        public DateTime CutOffDate { get; set; }

        public long CustomerID { get; set; }

        public long LenderID { get; set; }

        public long OwnerShopID { get; set; }

        public long ConsultantShopID { get; set; }

        public DateTime CreateDate { get; set; }

        public int Status { get; set; }
    }
}
