﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblAssignmentLoan")]
    public class TblAssignmentLoan
    {
        [Dapper.Contrib.Extensions.Key]
        public long AssignmentLoanID { get; set; }

        public long UserID { get; set; }

        public long LoanID { get; set; }

        public DateTime DateAssigned { get; set; }

        public DateTime DateApplyTo { get; set; }

        public int Status { get; set; }

        public long CreateBy { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }
        public int Type { get; set; }
    }

}
