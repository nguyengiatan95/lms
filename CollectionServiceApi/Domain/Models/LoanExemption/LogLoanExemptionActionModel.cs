﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.LoanExemption
{
    public class LogLoanExemptionActionModel
    {
        public long LoanExemptionID { get; set; }

        public int ActionType { get; set; }

        public DateTime CreateDate { get; set; }

        public long CreateBy { get; set; }

        public string CreateByName { get; set; }
        public string ActionTypeName { get; set; }
    }
}
