﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.LoanExemption
{
    public class UploadFileLoanExemptionModel
    {
        public int TypeFile { get; set; }
        public List<LMS.Entites.Dtos.AG.ImageLosUpload> FileImages { get; set; }
    }
}
