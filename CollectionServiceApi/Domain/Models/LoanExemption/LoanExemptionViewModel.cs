﻿using LMS.Entites.Dtos.CollectionServices.ReportExcel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.LoanExemption
{
    public class LoanExemptionViewModel
    {
        public long LoanExemptionID { get; set; }
        public long LoanID { get; set; }
        public string LoanContractCode { get; set; }
        public string CustomerName { get; set; }
        public string LoanExemptionStatusName { get; set; }
        public int LoanExemptionStatus { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string CreateByFullName { get; set; }
        public string ApprovedByFullName { get; set; }
        public string BookDebtName { get; set; }
        public string ApprovalLevelBookDebtName { get; set; }
        public string ReasonCancel { get; set; }
        /// <summary>
        /// lấy chi tiết sẽ show trường này
        /// </summary>
        public List<UploadFileLoanExemptionModel> FileUploads { get; set; }
        public ReportFormLoanExemptionInterestDetail FormDetail { get; set; }

        public long MoneySuggetExemption { get; set; }
        public long MaxMoneyApprovalLevel { get; set; }
        public long MoneyApprovalLevelCurrent { get; set; }
        public long MoneyApprovalLevelSuccess { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int HasAction { get; set; }
        public long CustomerID { get; set; }
        public string Note { get; set; }// ghi chú cấp phê duyệt đồng thời còn chưa thao tác
    }
}
