﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.Invoice
{
    public class SearchHistoryTransactionItem : Domain.Tables.TblInvoice
    {
        public string UserCreate { get; set; }
        public string AliasName { get; set; }
        public string CustomerName { get; set; }
        public string StrStatus { get; set; }
    }

    public class HistoryCustomerTopupItem
    {
        public string Note { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime TransactionDate { get; set; }
        public long TotalMoney { get; set; }
        public string UserCreate { get; set; }
        public string AliasName { get; set; }
        public string CustomerName { get; set; }
        public string StrStatus { get; set; }
    }
}
