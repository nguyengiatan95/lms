﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.Invoice
{
    public class RequestHistoryCustomerTopupModel
    {
        public string KeySearch { get; set; }
        public long BankCardID { get; set; } = (int)LMS.Common.Constants.StatusCommon.SearchAll;
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }

        public int TypeReport { get; set; } = (int)LMS.Common.Constants.ExcelReport_TypeReport.HistoryCustomerTopup;
        public long CreateBy { get; set; }
        public long UserIDLogin { get; set; }
    }
}
