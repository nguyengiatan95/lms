﻿using System;

namespace CollectionServiceApi.Domain.Models.BookDebt
{
    public class GetBookDebt
    {
        public long BookDebtID { get; set; }

        public string BookDebtName { get; set; }

        public DateTime? CreateDate { get; set; }

        public int Status { get; set; }

        public string StrStatus { get; set; }
        public int YearDebt { get; set; }
        public int DPDFrom { get; set; }
        public int DPDTo { get; set; }
    }
}
