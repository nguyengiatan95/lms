﻿using System.Collections.Generic;

namespace CollectionServiceApi.Domain.Models.AppTHN
{
    public class ListLoanBeDividedItem
    {
        public long LoanId { get; set; }    
        public string ContractCode { get; set;}
        public string CustomerName { get; set; }
        public long TotalMoneyCurrent { get; set; } // dư nợ hiện tại
        public long OverdueDays { get; set; }
        public bool Location { get; set; }
        public string LoanBriefPropertyPlateNumber { get; set; }// biển số xe
        public string HouseholdAddress { get; set; } //địa chỉ hộ khẩu
        public string PresentAddress { get; set; } //địa chỉ thường trú
        public bool Handling { get; set; } //đã xử lý hay chưa
        public int PlanLoanHandle { get; set; } // đơn có trong kế hoạch hay ko 0: chưa có 1: đã có 2: quá hạn
        public int AssignmentLoanType { get; set; } // loại giỏ
        public string ReasonCode { get; set; } // != "" trả về comment có mã code PTP
        public string LoanBriefPropertyBrandName { get; set; } // hãng xe
        public string LoanBriefPropertyProductName { get; set; } // tên xe
        public List<int> LstAssignmentLoanType { get; set; } // danh sách loại giỏ
        public string LoanStatusInsuranceName { get; set; }
        public long TotalMoneyNeedPay { get; set; }
    }
}
