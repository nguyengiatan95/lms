﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.AppTHN
{
    public class SummaryPlanLoanHandleByStaffModel
    {
        public int TotalLoanAssigned { get; set; }
        public int TotalLoanNoPlan { get; set; }
    }
}
