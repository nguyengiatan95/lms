﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.MapUserCall
{
    public class PhoneSettingCareSoft
    {
        public string AccessToken { get; set; }
        public string DomainAgent { get; set; }
        public string Host { get; set; }
        public string PhoneTest { get; set; }
    }
}
