﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.CommentDebtPrompted
{
    public class CommentDebtPromptedInsertSuccessModel
    {
        public long CommentDebtPromptedID { get; set; }
        public long LoanID { get; set; }
        public long CustomerID { get; set; }
    }
}
