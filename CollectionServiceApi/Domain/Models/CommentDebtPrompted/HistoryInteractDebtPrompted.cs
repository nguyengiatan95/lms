﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.CommentDebtPrompted
{
    public class HistoryInteractDebtPrompted
    {
        public DateTime CreateDateComment { get; set; }
        public string UserFullName { get; set; } 
        public string ContacCode { get; set; }
        public string CustomerName { get; set; }
        public string Province { get; set; }

        public long? DisbursedAmount { get; set; }// Số tiền vay giải ngân
        public long? CurrentOutstandingBalance { get; set; } //số tiền dư nợ hiện tại
        public int? LoanTime { get; set; }
        public string ShopName { get; set; }
        public string DebitReminderCode { get; set; }
        public DateTime? AppointmentDate { get; set; }
        public string Comment { get; set; }
        public long LoanID { get; set; }
        public long? LoanCreditID { get; set; }
        public string UserName { get; set; }
    }
}
