﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models
{
    public class EmployeeInfoModel
    {
        public long UserID { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public long DepartmentID { get; set; }
        public long PositionID { get; set; }
        public long ParentUserID { get; set; }
        public long ChildDepartmentID { get; set; }
    }
}
