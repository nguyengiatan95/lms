﻿using LMS.Entites.Dtos.ProxyTima;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.CustomerExtensionThirdParty
{
    public class SearchCustomerExtensionThirdParty
    {
        public long CustomerExtensionThirdPartyID { get; set; }
        public string ContactCode { get; set; }
        public string CustomerName { get; set; }
        public string CodeTranData { get; set; }
        public string StatusName { get; set; }
        public string CodeTranDataDescription { get; set; }
        public string ThirdPartyData { get; set; }
        public string CreateByName { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public List<TranDataItem> LstTranData { get; set; }
    }
}
