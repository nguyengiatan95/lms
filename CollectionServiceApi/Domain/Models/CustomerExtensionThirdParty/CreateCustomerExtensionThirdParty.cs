﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.CustomerExtensionThirdParty
{
    public class CreateCustomerExtensionThirdParty
    {
        public long TimaLoanID { get; set; }
        public string CodeTranData { get; set; }
    }
}
