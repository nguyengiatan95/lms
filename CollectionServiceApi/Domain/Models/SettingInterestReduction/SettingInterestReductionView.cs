﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.SettingInterestReduction
{
    public class SettingInterestReductionView
    {
        public List<BookDebtChildView> BookDebtChildViews { get; set; }
        public List<ApprovalLevelBookDebtChildView> ApprovalLevelBookDebtChildViews { get; set; }
        public List<SettingInterestReductionListView> SettingInterestReductionListViews { get; set; }
        public List<SettingTotalMoneyView> SettingTotalMoneyViews { get; set; }
        public SettingInterestReductionView()
        {
            BookDebtChildViews = new List<BookDebtChildView>();
            ApprovalLevelBookDebtChildViews = new List<ApprovalLevelBookDebtChildView>();
            SettingInterestReductionListViews = new List<SettingInterestReductionListView>();
            SettingTotalMoneyViews = new List<SettingTotalMoneyView>();
        }
    }
    public class BookDebtChildView
    {
        public long BookDebtID { get; set; }
        public string BookDebtName { get; set; }
    }
    public class ApprovalLevelBookDebtChildView
    {
        public long ApprovalLevelBookDebtID { get; set; }

        public string ApprovalLevelBookDebtName { get; set; }
    }
    public class SettingInterestReductionListView
    {
        public long SettingInterestReductionID { get; set; }

        public long ApprovalLevelBookDebtID { get; set; }

        public long BookDebtID { get; set; }

        public long MoneyType { get; set; }

        public decimal PercentMoneyReduce { get; set; }

        public long MaxMoneyReduce { get; set; }

        public string StrMoneyType { get; set; }
        public string Note { get; set; }
    }
    public class SettingTotalMoneyView
    {
        public long TotalMoney { get; set; }
        public long ApprovalLevelBookDebtID { get; set; }
    }
}
