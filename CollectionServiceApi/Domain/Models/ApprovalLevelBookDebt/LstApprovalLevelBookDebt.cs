﻿namespace CollectionServiceApi.Domain.Models.ApprovalLevelBookDebt
{
    public class LstApprovalLevelBookDebt
    {
        public long ApprovalLevelBookDebtID { get; set; }

        public int Postion { get; set; }

        public int ParentID { get; set; }
    }
}
