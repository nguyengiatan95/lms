﻿using System.Collections.Generic;

namespace CollectionServiceApi.Domain.Models.ApprovalLevelBookDebt
{
    public class ListTreeViewApprovalLevelBookDebt
    {
        public long ID { get; set; }
        public string Text { get; set; }
        public bool Open { get; set; }
        public long PostionID { get; set; }
        public List<ListTreeViewApprovalLevelBookDebt> Children { get; set; }
    }
}
