﻿using System;

namespace CollectionServiceApi.Domain.Models.ApprovalLevelBookDebt
{
    public class GetListApprovalLevelBookDebt
    {
        public long ApprovalLevelBookDebtID { get; set; }

        public string ApprovalLevelBookDebtName { get; set; }

        public int Postion { get; set; }

        public int ParentID { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int Status { get; set; }
        public string StrStatus { get; set; }
        public int IsAllAccept { get; set; }
    }
}
