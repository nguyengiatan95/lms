﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.MGL
{
    public class SettingByBookDebtDetails
    {
        //public string ApplyTo { get; set; }
        //public string ApplyFrom { get; set; }
        public int YearDebt { get; set; }
        public int DPDFrom { get; set; }
        public int DPDTo { get; set; }
        public decimal ConsultantPercent { get; set; }
        public long ConsultantTotalMoney { get; set; }
        public decimal ServicePercent { get; set; }
        public long ServiceTotalMoney { get; set; }
        public decimal InterestLatePercent { get; set; }
        public long InterestLateTotalMoney { get; set; }
        public decimal PaybeforePercent { get; set; }
        public long PaybeforeTotalMoney { get; set; }
        public decimal DebtPercent { get; set; }
        public long DebtTotalMoney { get; set; }
    }

    public class SettingMoneyDetails
    {
        public long TotalMoney { get; set; }
        public string ListTypeMoney { get; set; }
    }
}
