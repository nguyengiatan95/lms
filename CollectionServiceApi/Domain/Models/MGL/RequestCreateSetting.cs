﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.MGL
{
    public class RequestCreateSetting
    {
        public long BookDebtID { get; set; }
        public long ApprovalLevelBookDebtID { get; set; }
        //public string ApplyTo { get; set; }
        //public string ApplyFrom { get; set; }
        //public int DPDFrom { get; set; }
        //public int DPDTo { get; set; }
        public long MoneyType { get; set; }
        public decimal PercentMoneyReduce { get; set; }
        public long MaxMoneyReduce { get; set; }
    }
}
