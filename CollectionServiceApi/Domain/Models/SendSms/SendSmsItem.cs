﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.SendSms
{
    public class SendSmsItem
    {
        public long SendSmsID { get; set; }
        public string CreateByName { get; set; }
        public string StrStatus { get; set; }
        public string TemplateName { get; set; }

        public string ContentSms { get; set; }

        public string PhoneSend { get; set; }

        public string PhoneReceived { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? ModifyDate { get; set; }
    }
}
