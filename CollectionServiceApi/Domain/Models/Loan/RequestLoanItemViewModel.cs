﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.Loan
{
    public class RequestLoanItemViewModel
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string KeySearch { get; set; }
        public long LoanID { get; set; }

        public List<long> LstCustomerID { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalRow { get; set; }
        public int DPD { get; set; }
        public long EmployeeID { get; set; }
        public int LoanStatus { get; set; }
        public int LoanStatusSendInsurance { get; set; }
        public int CityID { get; set; }
        public int DistrictID { get; set; }

        public string AppointmentFromDate { get; set; }
        public string AppointmentToDate { get; set; }
        public long ManagerID { get; set; } // trưởng phòng
        public long DepartmentID { get; set; } // phòng ban
        public long LeaderID { get; set; } // trưởng nhóm
        public int Type { get; set; }

        public bool HasSearchLoanGroupByCustomer { get; set; } // true: đếm số đơn vay theo KH theo nhân viên, mở rộng cho tất cả đều thấy
        public RequestLoanItemViewModel()
        {
            //LoanStatus = (int)LMS.Common.Constants.StatusCommon.SearchAll;
            //ShopID = (int)LMS.Common.Constants.StatusCommon.SearchAll;
            //ProductID = (int)LMS.Common.Constants.StatusCommon.SearchAll;
            //LenderID = (int)LMS.Common.Constants.StatusCommon.SearchAll;
            //ConsultantShopID = (int)LMS.Common.Constants.StatusCommon.SearchAll;
            DPD = (int)LMS.Common.Constants.StatusCommon.SearchAll;
            EmployeeID = (long)LMS.Common.Constants.StatusCommon.SearchAll;
            PageIndex = 1;
            PageSize = LMS.Common.Constants.TimaSettingConstant.MaxItemQuery;
            LoanStatus = (int)LMS.Common.Constants.StatusCommon.SearchAll;
            LoanStatusSendInsurance = (int)LMS.Common.Constants.StatusCommon.SearchAll;
            CityID = (int)LMS.Common.Constants.StatusCommon.SearchAll;
            DistrictID = (int)LMS.Common.Constants.StatusCommon.SearchAll;
            DepartmentID = (int)LMS.Common.Constants.Department_ID.THN_Call;
            ManagerID = (int)LMS.Common.Constants.StatusCommon.SearchAll;
            LeaderID = (int)LMS.Common.Constants.StatusCommon.SearchAll;
            Type = (int)LMS.Common.Constants.StatusCommon.SearchAll;
            HasSearchLoanGroupByCustomer = true;
        }
    }

    public class RequestHistoryTransactionLoanByCustomer
    {
        public string KeySearch { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }

        public int TypeReport { get; set; } = (int)LMS.Common.Constants.ExcelReport_TypeReport.HistoryTransactionLoanByCustomer;
        public long CreateBy { get; set; }
        public long UserIDLogin { get; set; }
    }
}
