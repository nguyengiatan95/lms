﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.DebtRestructuringLoan
{
    public class DebtRestructuringLoanModel
    {
        public long DebtRestructuringLoanID { get; set; }
        public string LoanContractCode { get; set; }
        public List<LogActionDebtRestructuringLoanItemModel> Histories { get; set; }
        public List<LMS.Entites.Dtos.AG.ImageLosUpload> FileImages { get; set; }
        //public List<string> FileImages { get; set; }
    }
    public class LogActionDebtRestructuringLoanItemModel
    {
        public string CreateByName { get; set; }
        public string ActionName { get; set; }
        public DateTime CreateDate { get; set; }
    }

    public class DebtRestructuringItemViewModel
    {
        public long DebtRestructuringLoanID { get; set; }
        public string LoanContractCode { get; set; }
        public string SourceName { get; set; }
        public string ProductName { get; set; }
        public int DpdBom { get; set; }
        public string StatusName { get; set; }
        public string CreateByName { get; set; }
        public DateTime CreateDate { get; set; }
        public int DebtRestructuringStatus { get; set; }
    }
}
