﻿namespace CollectionServiceApi.Domain.Models.AssignmentLoan
{
    public class LstLoanSplitSingle
    {
        public long LoanID { get; set; }
        public string ContractCode { get; set; }
        public string TypeName { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string StatusLoan { get; set; }
        public int Status { get; set; }
    }
}
