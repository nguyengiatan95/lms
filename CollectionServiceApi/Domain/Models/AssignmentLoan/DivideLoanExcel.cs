﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.AssignmentLoan
{
    public class DivideLoanExcel
    {
        public long LoanIDAG { get; set; }
        //public string UserNameNew { get; set; }
        public string UserName { get; set; }
        public int Type { get; set; }
        public long LoanID { get; set; }
        public long UserID { get; set; }
        //public long UserIDNew { get; set; }
        //public long UserID { get; set; }
    }
}
