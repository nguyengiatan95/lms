﻿using System.Collections.Generic;

namespace CollectionServiceApi.Domain.Models.UserDepartment
{
    public class PostionDeparmentbUser
    {
        public PostionDeparmentbUser()
        {
            DataParent = new List<PostionDeparmentbUserItem>();
            DataChidrend = new List<PostionDeparmentbUserItem>();
        }
        public List<PostionDeparmentbUserItem> DataParent { get; set; }
        public List<PostionDeparmentbUserItem> DataChidrend { get; set; }
    }
    public class PostionDeparmentbUserItem
    {
        public long ID { get; set; }
        public string Text { get; set; }
        public bool Open { get; set; }
        public long PostionID { get; set; }
    }
}
