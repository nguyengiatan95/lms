﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.UserDepartment
{
    public class CreateUserDepartmentTHN
    {
        public long UserID { get; set; }
        public long PositionID { get; set; }
        public long ParentUserID { get; set; }
    }
}
