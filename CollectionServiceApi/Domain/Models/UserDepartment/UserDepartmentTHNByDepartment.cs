﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.UserDepartment
{
    public class UserDepartmentTHNByDepartment
    {
        public long ID { get; set; }
        public string Text { get; set; }
        public bool Open { get; set; }
        public long PostionID { get; set; }
        public List<UserDepartmentTHNByDepartment> Children { get; set; }
    }
    public class SummaryEmployeeDepartmentItem
    {
        public long DepartementID { get; set; }
        public int TotalEmployees { get; set; }
        public int TotalLoanHandle { get; set; }
        public int TotalLoanProcessed { get; set; }
        public List<long> LstUserID { get; set; }
    }
    public class SummaryEmployeeHandleLoanDaily
    {
        public long UserID { get; set; }
        public int TotalLoanHandle { get; set; }
        public int TotalLoanProcessed { get; set; }
        public Dictionary<long, long> DictLoanHandleDaily { get; set; }
    }
}
