﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models
{
    public class SelectItem
    {
        public string Text { get; set; }
        public long Value { get; set; }
        public object Option { get; set; }
    }
}
