﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.AG
{
    public class ChangePhoneAG
    {
        public long LoanID { get; set; }
        public string Phone { get; set; }
        public string Note { get; set; }
        public long LogChangePhoneLMSID { get; set; }
        public long UserID { get; set; }
        public string FullName { get; set; }
    }
}
