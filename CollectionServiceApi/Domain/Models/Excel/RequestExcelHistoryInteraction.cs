﻿using CollectionServiceApi.Queries.CommentDebtPrompted;

namespace CollectionServiceApi.Domain.Models.Excel
{
    public class RequestExcelHistoryInteraction : HistoryInteractDebtPromptedQuery
    {
        public int TypeReport { get; set; } = (int)LMS.Common.Constants.ExcelReport_TypeReport.ExcelHistoryInteraction;
    }
}
