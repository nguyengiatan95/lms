﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.Excel
{
    public class HistoryTrackingLoanInMonth
    {
        public long LoanID { get; set; }
        public string ContractCode { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string DebtType { get; set; }
        public long TotalMoneyCurrent { get; set; }
        public long TotalMoneyReceived { get; set; }
        public DateTime? FinishDate { get; set; }

    }

    public class RequestHistroryTrackingLoan
    {
        public int TypeReport { get; set; } = (int)LMS.Common.Constants.ExcelReport_TypeReport.ReportLoanDebtType;
        public long MonthYear { get; set; }
        public long CreateBy { get; set; }
        public long UserIDLogin { get; set; }
        public int ProductID { get; set; }
        public int CityID { get; set; }
        public int DebtType { get; set; }

    }

    public class ExtraDataTracking
    {
        public string CustomerName { get; set; }
        public long CustomerID { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string ContractCode { get; set; }
        public string Address { get; set; }
    }
}
