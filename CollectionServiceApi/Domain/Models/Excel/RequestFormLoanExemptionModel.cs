﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.Excel
{
    public class RequestFormLoanExemptionModel
    {
        public long LoanID { get; set; }
        public string CutOfDate { get; set; }
        public long MoneyExpectedReceive { get; set; }
        public int TypeReport { get; set; }
        public int CreateBy { get; set; }
        public string Note { get; set; }
        public string DepartmentName { get; set; }
        public string ExpirationDate { get; set; }
        public string ApprovedByName { get; set; }
    }
}
