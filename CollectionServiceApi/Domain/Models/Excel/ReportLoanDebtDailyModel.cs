﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.Excel
{
    public class ReportLoanDebtDailyModel: Tables.TblReportLoanDebtDaily
    {
        public string LoanStatusName { get; set; }
        public string LoanStatusInsuranceName { get; set; }
        public string LoanStatusDispositName { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public string TypeDebtName { get; set; }
    }
}
