﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.Excel
{
    public class HistoryTransactionLoanCustomerModel
    {
        public DateTime CreateDate { get; set; }
        public string LoanContractCode { get; set; }
        public long LoanID { get; set; }
        public long AgLoanID { get; set; }
        public string CustomerName { get; set; }
        public string ActionName { get; set; }
        public string OwnerShopName { get; set; }// cửa hàng giải ngân
        public long MoneyOriginal { get; set; }
        public long MoneyConsultant { get; set; }
        public long MoneyInterest { get; set; }
        public long MoneyService { get; set; }
        public long MoneyFineOrginalLender { get; set; }
        public long MoneyFineOrginalTima { get; set; }
        public long MoneyFineLateTima { get; set; }
        public long MoneyFineLateLender { get; set; }
    }
    public class HistoryTransactionLoanCustomerExcel
    {
        public List<HistoryTransactionLoanCustomerModel> Rows { get; set; }
        public long TotalMoneyOriginal { get; set; }
        public long TotalMoneyConsultant { get; set; }
        public long TotalMoneyInterest { get; set; }
        public long TotalMoneyService { get; set; }
        public long TotalMoneyFineOrginalLender { get; set; }
        public long TotalMoneyFineOrginalTima { get; set; }
        public long TotalMoneyFineLateTima { get; set; }
        public long TotalMoneyFineLateLender { get; set; }
    }
}
