﻿namespace CollectionServiceApi.Domain.Models.Report
{
    public class RequestCreateReportDayPlan
    {
        public int TypeReport { get; set; } = (int)LMS.Common.Constants.ExcelReport_TypeReport.ReportDayPlan;
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int TypeID { get; set; }
        public int DepartmentManager { get; set; }
        public int DepartmentLeader { get; set; }
        public int Staff { get; set; }
        public int Status { get; set; }
        public int UserID { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
