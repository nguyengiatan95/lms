﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.Report
{
    public class ReportByDebtGroup
    {
        public string NhomNo { get; set; }
        public long SoCaseGiao { get; set; }
        public long DuNo { get; set; }
        public long SoCaseThuDuoc { get; set; }
        public long DuNoDuocGiaiQuyet { get; set; }
        public decimal PhanTramTheoDuNo { get; set; }
        public decimal PhanTramTheoSoCase { get; set; }
        public long SoTienThuDuoc { get; set; }
    }
}
