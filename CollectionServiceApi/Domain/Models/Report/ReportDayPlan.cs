﻿using System;
using System.Collections.Generic;

namespace CollectionServiceApi.Domain.Models.Report
{
    public class ReportDayPlan
    {
        public ReportDayPlan()
        {
            ReportDayPlanData = new List<ListLoanDayPlan>();
        }
        public int TotalCount { get; set; }
        public int Processed { get; set; }
        public int NoProcessed { get; set; }
        public List<ListLoanDayPlan> ReportDayPlanData { get; set; }
    }
    public class ListLoanDayPlan
    {
        public string ContractCode { get; set; }
        public string CustomerName { get; set; }
        public string TypeName { get; set; }
        public string CodeStaff { get; set; }
        public string Leader { get; set; }
        public string Manager { get; set; }
        public string Director { get; set; }
        public DateTime? DayPlan { get; set; }
        public DateTime HandlingDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
    }
    public class UserReportDayPlan
    {
        public string UserName { get; set; }
        public string Leader { get; set; }
        public string Manager { get; set; }
        public string Director { get; set; }
    }
}
