﻿
using System;
using System.Collections.Generic;

namespace CollectionServiceApi.Domain.Models.PlanHandleLoanDaily
{
    public class PlanHandleLoanDailyModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
    public class PlanHandleLoanDailyItem
    {
        public long PlanHandleLoanDailyID { get; set; }
        public string ContractCode { get; set; }
        public string CustomerName { get; set; }
        public long LoanID { get; set; }
        public DateTime? TimeCheckin { get; set; }
        public DateTime? TimeCheckOut { get; set; }
        public int Status { get; set; }
        public string Reason { get; set; }
        public List<int> LstAssignmentLoanType { get; set; }
        public bool ShowPTP { get; set; }
    }
}
