﻿using System;

namespace CollectionServiceApi.Domain.Models.UserApprovalLevel
{
    public class UserApprovalLevelListViews
    {
        public long UserApprovalLevelId { get; set; }
        public string UserName { get; set; }
        public long ApprovalLevelBookDebtID { get; set; }
        public string ApprovalLevelBookName { get; set; }
        public long MoveApprovalLevelBookDebtID { get; set; }
        public string MoveApprovalLevelBookName { get; set; }
        public DateTime CreateDate { get; set; }
        public int Status { get; set; }
    }
}
