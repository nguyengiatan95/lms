﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.SmartDialer
{
    public class ConfigHoldCodeForSmartDialerModel
    {
        public int SkipTime { get; set; }
        public List<string> LstReasonCode { get; set; }
    }
}
