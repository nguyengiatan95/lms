﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.SmartDialer
{
    public class DetailPhoneCallWithLstLoanViewModel
    {
        public string CustomerCallName { get; set; }
        public List<LMS.Entites.Dtos.CollectionServices.Loan.LoanItemViewModel> LstLoanInfos { get; set; }
    }
}
