﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.SmartDialer
{
    public class ConfigUserMapCampaignModle
    {
        public string CampaignName { get; set; }
        public long CampaignID { get; set; }
        public string UserNameCisco { get; set; }
        public long UserID { get; set; }
        public string StatusName { get; set; }
        public int Status { get; set; }
        public DateTime ModifyDate { get; set; }
        public int WrapupTime { get; set; }
    }
}
