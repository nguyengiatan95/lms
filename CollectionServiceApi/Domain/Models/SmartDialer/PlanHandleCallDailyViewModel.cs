﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.SmartDialer
{
    public class PlanHandleCallDailyViewModel
    {
        public string UserNameCisco { get; set; }
        public long CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string LoanContractCodes { get; set; }
        public string PhoneCall { get; set; }
        public string CustomerNameCall { get; set; }
        public string StatusName { get; set; }
        public string CallResultName { get; set; }
        public int CallCount { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int ConnectTime { get; set; }
        public DateTime ModifyDate { get; set; }
        public int TotalLoan { get; set; }
        public int TotalRows { get; set; }
        public string CampaingName { get; set; }
        public int DPD { get; set; }
        public long TotalMoneyOriginal { get; set; }
        public string PriorityOrderName { get; set; }
    }
}
