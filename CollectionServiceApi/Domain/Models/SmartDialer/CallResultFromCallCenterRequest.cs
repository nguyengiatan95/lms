﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain.Models.SmartDialer
{
    public class CallResultFromCallCenterRequest
    {
        public List<CallResultFromCallCenterItemRequest> Inputs { get; set; }
    }
    public class CallResultFromCallCenterItemRequest
    {
        public long Id { get; set; }
        public string PhoneJoined { get; set; }
        public DateTime Startdatetime { get; set; }
        public float? Connecttime { get; set; }
        public float Campaignid { get; set; }
        public float? Callresult { get; set; }
        public string StartdatetimeString { get; set; } // định dạng dd-MM-yyyy HH:mm
    }
}
