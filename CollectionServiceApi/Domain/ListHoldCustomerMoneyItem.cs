﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain
{
    public class ListHoldCustomerMoneyItem
    {
        public long HoldCustomerMoneyID { get; set; }
        public string ContactCode { get; set; }
        public string FullName { get; set; }
        public long MoneyInWallet { get; set; }
        public string StatusLoan { get; set; }
        public DateTime? CreateDate { get; set; }
        public string StrStatus { get; set; }
        public int Status { get; set; }
        public string UserNameCreate { get; set; }
        public string UserNameDelete { get; set; }
        public long TotalMoney { get; set; }
        public string Reason { get; set; }
        public List<string> ListContractCode { get; set; }
        public ListHoldCustomerMoneyItem()
        {
            ListContractCode = new List<string>();
        }
    }
}
