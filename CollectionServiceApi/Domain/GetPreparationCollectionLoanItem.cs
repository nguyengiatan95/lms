﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain
{
    public class GetPreparationCollectionLoanItem
    {
        public DateTime CreateDate { get; set; }
        public string StrStatus { get; set; }
        public DateTime PreparationDate { get; set; }
        public string FullName { get; set; }
        public long TotalMoney { get; set; }
        public string Description { get; set; }
        public string PrepareTypeName { get; set; }
    }
}
