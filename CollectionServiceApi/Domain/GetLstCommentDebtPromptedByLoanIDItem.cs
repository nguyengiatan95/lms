﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionServiceApi.Domain
{
    public class GetLstCommentDebtPromptedByLoanIDItem
    {
        public DateTime CreateDate { get; set; }
        public string FullName { get; set; }
        public string ReasonCode { get; set; }
        public string Comment { get; set; }
        public List<LMS.Entites.Dtos.AG.ImageLosUpload> FileImage { get; set; }
        public string StrActionPeron { get; set; }
        public string StrActionAddress { get; set; }
    }
}
