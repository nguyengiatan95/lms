﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Services.PermissionGroup;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Management.Controllers
{
    public class GroupPermissionController : BaseController
    {
        private IGroupPermissionServices _groupPermissionService;
        public GroupPermissionController(IConfiguration configuration, IGroupPermissionServices groupPermissionService)
            : base(configuration)
        {
            _groupPermissionService = groupPermissionService;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GetPermission(long GroupID)
        {
            var lstData =  _groupPermissionService.GetListPermissionByGroup(GroupID,GetToken());
            return Json(lstData);
        }

        [HttpPost]
        public  IActionResult AddPermission(List<long> lstPermission, int GroupID)
        {
            var data = _groupPermissionService.AddPermission(GroupID, GetToken(), lstPermission);
            return Json(data);
        }
    }
}