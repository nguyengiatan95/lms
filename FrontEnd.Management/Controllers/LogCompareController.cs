﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Services.LogCompare;
using LMS.Entites.Dtos.LogCompareServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Management.Controllers
{
    public class LogCompareController : BaseController
    {
        private ILogCompareServices _logCompareServices;
        public LogCompareController(IConfiguration configuration, ILogCompareServices logCompareServices)
            : base(configuration)
        {
            _logCompareServices = logCompareServices;
        }
        public IActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> GetSummaryCompare()
        {
            var dataRequest = Request.Form.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.ToString());
            var dicts = Request.Form.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.ToString());
            var request = dicts.ToNameValueCollection();
            TableAjaxBase tableAjax = new TableAjaxBase(request);

            string datetimeSearch = Request.Form["query[datetimeSearch]"].ToString() == "" ? DateTime.Now.ToString("dd/MM/yyyy") : Request.Form["query[datetimeSearch]"].ToString();
            int mainType = Request.Form["query[sl_search_MainType]"].ToString() == "" ? (int)LMS.Common.Constants.StatusCommon.Default : int.Parse(Request.Form["query[sl_search_MainType]"].ToString());
            if (string.IsNullOrEmpty(tableAjax.field))
            {
                tableAjax.field = "MainType";
                tableAjax.sort = "desc";
            }
            var lstResponse = _logCompareServices.GetSummaryCompareAG(GetToken(), datetimeSearch, mainType);
            List<SummaryCompareModel> lstData = lstResponse.Result.Data;
            tableAjax.total = lstData.Count.ToString();
            return Json(new { meta = tableAjax, data = lstData });
        }
    }
}