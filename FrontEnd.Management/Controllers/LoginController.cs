﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Core.Security.Crypt;
using FrontEnd.Common.Entites.Management;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models.Management.Login;
using FrontEnd.Common.Models.Management.User;
using FrontEnd.Common.Services.Login;
using FrontEnd.Common.Services.User; 
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace FrontEnd.Management.Controllers
{
    public class LoginController : Controller
    {
        protected IConfiguration _baseConfig;
        private ILoginService loginService;
        private IUserService _user;
        public LoginController(IConfiguration configuration, ILoginService _loginService, IUserService user)
        {
            loginService = _loginService;
            _user = user;
            _baseConfig = configuration;
        }
        public IActionResult Index(string referrer = "")
        {
            ViewBag.ApiUrl = _baseConfig[Constants.STATIC_ApiUrl_URL];
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> LoginPost([FromBody]LoginModel req)
        {
            try
            {
                req.UserName = req.UserName.Trim().ToLower();
                req.Password = Md5.GetMD5Hash(req.Password.Trim());
                var responce = await loginService.LoginPost(req.ToObject());
                if (responce != null && responce.Result == 1)
                {
                    var model = new GetUserInfoWithMenuQuery { AppID = 1};//(int)LMS.Common.Constants.Menu_AppID.Admin 
                    var getUserInfoWithMenu = await loginService.GetUserInfoWithMenu(model, responce.Data.Token);
                    if (getUserInfoWithMenu != null && getUserInfoWithMenu.Result == 1 && getUserInfoWithMenu.Data != null)
                    { 
                        getUserInfoWithMenu.Data.Token = responce.Data.Token;
                        getUserInfoWithMenu.Data.TimeExpired = responce.Data.TimeExpired;
                        // getUserInfoWithMenu.Data.TimeExpired = DateTime.Now.AddMinutes(Constants.STATIC_DateExpiration_Token); 
                        getUserInfoWithMenu.Data.TimeExpiredString = getUserInfoWithMenu.Data.TimeExpired.ToString("dd/MM/yyyy HH:mm");
                        getUserInfoWithMenu.Data.UserLoginModel = new UserLoginModel
                        {
                            AdminUrl = _baseConfig[Constants.STATIC_HOME_URL],
                            AccountantUrl = _baseConfig[Constants.STATIC_HOME_AccountantUrl],
                            LenderUrl = _baseConfig[Constants.STATIC_HOME_LenderUrl],
                            THNUrl = _baseConfig[Constants.STATIC_HOME_RecoveryOfLoans],
                            PathRereshToken = Constants.STATIC_Path_RereshToken
                        }; 
                        getUserInfoWithMenu.Data.LoginUrl = _baseConfig[Constants.STATIC_Login_URL];
                        getUserInfoWithMenu.Data.ApiUrl = _baseConfig[Constants.STATIC_ApiUrl_URL];
                        getUserInfoWithMenu.Data.AdminUrl = _baseConfig[Constants.STATIC_HOME_URL];
                        getUserInfoWithMenu.Data.DomainCookie = _baseConfig[Constants.STATIC_Cookie];
                        getUserInfoWithMenu.Data.CookieName = Constants.STATIC_USERMODEL;
                        HttpContext.Session.SetObjectAsJson(Constants.STATIC_USERMODEL, getUserInfoWithMenu.Data);
                        var value = getUserInfoWithMenu.Data;
                        value.LstMenu = null;
                        var cookie = JsonConvert.SerializeObject(value);
                        CookieOptions option = new CookieOptions();
                        // option.HttpOnly = false;
                        option.Secure = true;
                        option.Expires = DateTime.Now.AddDays(30);
                        option.Domain = _baseConfig[Constants.STATIC_Cookie];
                        Response.Cookies.Append(Constants.STATIC_USERMODEL, cookie, option); 
                        return Json(new { status = 1, message = "Đăng nhập thành công!", data = JsonConvert.SerializeObject(getUserInfoWithMenu.Data) });
                    }
                }
                return Json(new { status = 0, message = "Thông tin đăng nhập không đúng!" });
            }
            catch (Exception)
            {
                return Json(new { status = 0, message = "Xảy ra lỗi khi đăng nhập. Vui lòng thử lại." });
            } 
        }
        [Route("logout.html")]
        public async Task<IActionResult> Logout()
        {
            var user = await HttpContext.Session.GetObjectFromJson<UserInfoWithMenuModel>(Constants.STATIC_USERMODEL);
            if (user != null && !string.IsNullOrEmpty(user.Token)) {
                var responce = await loginService.Logout(user);
            } 
            HttpContext.Session.SetObjectAsJson(Constants.STATIC_USERMODEL, null);
            RemoveCookie(Constants.STATIC_USERMODEL);
            RemoveCookie(Constants.STATIC_CALL_INFO_COOKIE); 
            return Redirect(_baseConfig[Constants.STATIC_Login_URL]);
        }
        [Route("redirect")]
        [HttpGet]
        public async Task<IActionResult> RedirectIndex(string token)
        {
            try
            {
                //Validate Token
                if (string.IsNullOrEmpty(token))
                    return Redirect(_baseConfig[Constants.STATIC_Login_URL]);
                var user = await _user.GetUserByUserID(token, 1);
                var users = new UserLoginModel { UserID = user.UserID, UserName = user.UserName,FullName = user.FullName, Token = token };
                HttpContext.Session.SetObjectAsJson(Constants.STATIC_USERMODEL, users); 
                return Redirect(Constants.STATIC_HOME_URL);
            }
            catch (Exception)
            {
                return Redirect(_baseConfig[Constants.STATIC_Login_URL]);
            }
        }
        public void SetCookie(string key, string value, int? expireTime)
        {
            CookieOptions option = new CookieOptions();
            if (expireTime.HasValue)
                option.Expires = DateTime.Now.AddHours(expireTime.Value);
            else
                option.Expires = DateTime.Now.AddHours(12);
            Response.Cookies.Append(key, value, option);
        }
        public void RemoveCookie(string key)
        { 
            Response.Cookies.Delete(key);
        }
    }
}
