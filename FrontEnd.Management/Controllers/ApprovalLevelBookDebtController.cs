﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Management.Controllers
{
    public class ApprovalLevelBookDebtController : BaseController
    {
        public ApprovalLevelBookDebtController(IConfiguration configution) : base(configution)
        {
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult ConfigurationApprovalLevelBookDebt()
        {
            return View();
        }
    }
}
