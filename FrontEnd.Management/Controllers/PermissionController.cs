﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Services.Permission;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Management.Controllers
{
    public class PermissionController : BaseController
    {
        private IPermissionService permissionService;
        public PermissionController(IConfiguration configuration, IPermissionService _permissionService)
            : base(configuration)
        {
            permissionService = _permissionService;
        }
        public IActionResult Index()
        {
            return View();
        }
        public ActionResult Permissions()
        {
            var userGroups = permissionService.GetPermissionByID(GetToken(), 0);
            return Json(userGroups);
        }

        public async Task<ActionResult> GetListData()
        {
            var dataRequest = Request.Form.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.ToString());


            var dicts = Request.Form.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.ToString());
            var request = dicts.ToNameValueCollection();
            TableAjaxBase tableAjax = new TableAjaxBase(request);
            //string Status = Request.Form["query[Status]"];

            string generalSearch = Request.Form["query[txt_search_api]"];
            int Status = string.IsNullOrEmpty(Request.Form["query[Status]"]) ? -1 : int.Parse(Request.Form["query[Status]"]);
            if (string.IsNullOrEmpty(tableAjax.field))
            {
                tableAjax.field = "CreateDate";
                tableAjax.sort = "desc";
            }
            var lstData = await permissionService.GetByConditions(GetToken(), generalSearch, Status, int.Parse(tableAjax.page), int.Parse(tableAjax.perpage), tableAjax.field, tableAjax.sort);
            tableAjax.total = lstData != null && lstData.Any() ? lstData[0].TotalCount.ToString() : "0";
            // Return
            return Json(new { meta = tableAjax, data = lstData });
        }

        [HttpPost]
        public ActionResult AddOrUpDate(Common.Models.Management.Permission.PermissionAddModel request)
        {
            if (request.PermissionID == 0)
            {
                var response =  permissionService.AddPermission(request, GetToken());
                return Json(response);
            }
            else
            {
                var response = permissionService.UpdatePermission(request, GetToken());
                return Json(response);
            }
            // Return
        }


    }
}
