﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using FrontEnd.Management.Models;
using Microsoft.Extensions.Configuration;
using FrontEnd.Common.Models.Management.User;
using FrontEnd.Common.Services.Login;
using Microsoft.AspNetCore.Http;
using FrontEnd.Common.Helpers;
using Newtonsoft.Json;
using System.Net.Http;
using System.Xml.Linq;

namespace FrontEnd.Management.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;
        private ILoginService loginService;
        public HomeController(ILogger<HomeController> logger,IConfiguration configuration, ILoginService _loginService)
           : base(configuration)
        { 
            _logger = logger;
            loginService = _loginService;
        }

        public IActionResult Index()
        {
            var user = new UserLoginModel {Token=GetToken()};
            ViewBag.AdminUrl = _baseConfig["AppSettings:AdminUrl"];
            ViewBag.AccountantUrl = _baseConfig["AppSettings:AccountantUrl"];
            ViewBag.LenderUrl = _baseConfig["AppSettings:LenderUrl"];
            ViewBag.RecoveryOfLoans = _baseConfig["AppSettings:RecoveryOfLoans"];
            ViewBag.Token = GetToken();
            return View(user);
        }
        public IActionResult Dashboard()
        { 
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public ActionResult GetTokenForHomePage()
        {
            var token = GetToken();
            var homeUrl = _baseConfig["AppSettings:AdminUrl"];
            return Json(new { data = token, url = homeUrl });
        }
        [HttpPost]
        public async Task<IActionResult> GetRefreshToken()
        {
            // check nếu chưa đến hạn 
            var dataToken = new Common.Models.ResponseActionResult<UserInfoWithMenuModel>();
            try
            {
                var value = await HttpContext.Session.GetObjectFromJson<UserInfoWithMenuModel>(Constants.STATIC_USERMODEL);
                if ((DateTime.Now - value.TimeExpired).TotalMinutes < 5)
                {
                    dataToken = await loginService.RefreshToken(GetToken());
                    value.Token = dataToken.Data.Token;
                    value.TimeExpired = dataToken.Data.TimeExpired;
                    value.TimeExpiredString = value.TimeExpired.ToString("dd/MM/yyyy HH:mm");
                    HttpContext.Session.SetObjectAsJson(Constants.STATIC_USERMODEL, value);
                    return Json(new { status = 1, data = value });
                }
                return Json(new { status = 0 });
            }
            catch (Exception)
            {
                return Json(new { status = 0 });
            } 
            
        }
        public async Task<IActionResult> RefreshToken()
        {
            // check nếu chưa đến hạn
            var value = await HttpContext.Session.GetObjectFromJson<UserInfoWithMenuModel>(Constants.STATIC_USERMODEL);
            if ((DateTime.Now - value.TimeExpired).TotalMinutes < 5)
            {
                var refresh = await loginService.RefreshToken(GetToken());
                var mes = ThongBaoViaTelegram(-461400009, "RefreshToken " + DateTime.Now.ToString("dd/MM/yyyy HH:mm") + " : " + refresh.Data.Token);
                CookieOptions option = new CookieOptions();
                option.Expires = DateTime.Now.AddDays(30);
                if (refresh != null && refresh.Result == 1 && !string.IsNullOrEmpty(refresh.Data.ToString()) && value != null)
                { 
                    value.Token = refresh.Data.Token;
                    value.TimeExpired = refresh.Data.TimeExpired;
                   // value.TimeExpired = DateTime.Now.AddMinutes(Constants.STATIC_DateExpiration_Token);
                    value.TimeExpiredString = value.TimeExpired.ToString("dd/MM/yyyy HH:mm"); 
                    HttpContext.Session.SetObjectAsJson(Constants.STATIC_USERMODEL, value);
                    //
                    value.LstMenu = null;
                    var cookie = JsonConvert.SerializeObject(value);
                    option.Domain = _baseConfig[Constants.STATIC_Cookie];
                    option.HttpOnly = true;
                    option.Secure = true;
                    Response.Cookies.Append(Constants.STATIC_USERMODEL, cookie, option);
                }
                else
                {
                    HttpContext.Session.SetObjectAsJson(Constants.STATIC_USERMODEL, "");
                    Response.Cookies.Append(Constants.STATIC_USERMODEL, "", option);
                }
            } 
            return View();
        }
        public string ThongBaoViaTelegram(int groupId, string text)
        {
            try
            {
                if (!string.IsNullOrEmpty(text))
                {
                    using (var client = new HttpClient())
                    {
                        var response = client.GetAsync("https://api.telegram.org/bot1289672837:AAFjY383npbd-3_a8ERsH13QFS8MBaw9LZs/sendMessage?chat_id=" + groupId + "&text=" + text).Result;

                    }
                }
                return text;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
