﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Management.Controllers
{
    public class LogCallAgController : BaseController
    {
        public LogCallAgController(IConfiguration configuration)
              : base(configuration)
        {
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult LMStoAG()
        {
            return View();
        }
    }
}