﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Core.Security.Crypt;
using FrontEnd.Common.Entites.Management;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Message;
using FrontEnd.Common.Models;
using FrontEnd.Common.Models.Management.User;
using FrontEnd.Common.Services.Department;
using FrontEnd.Common.Services.Group;
using FrontEnd.Common.Services.User;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Management.Controllers
{
    public class UserController : BaseController
    {
        private IUserService _userService;
        private IDepartmentService _departmentService;
        private IGroupService _groupService;
        public UserController(IConfiguration configuration, IUserService userService, IDepartmentService departmentService, IGroupService groupService)
            : base(configuration)
        {
            _userService = userService;
            _departmentService = departmentService;
            _groupService = groupService;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<ActionResult> GetListData()
        {
            var modal = new UserDatatableModel(HttpContext.Request.Form);
            var lstData = await _userService.GetUsers(GetToken(), modal.ToQueryObject());
            modal.total = lstData != null && lstData.Any() ? lstData[0].TotalCount.ToString() : "0";
            // Return
            return Json(new { meta = modal, data = lstData });
        }
        public async Task<ActionResult> GetUserByUserID(int id)
        {
            var dataUser = await _userService.GetUserByUserID(GetToken(), id);
            dataUser.LstUserGroup = await _userService.GetUserGroupByUserId(GetToken(), id);
            return Json(new { data = dataUser });
        }
        [HttpPost]
        public async Task<ActionResult> AddOrUpDate(UserModel request)
        {
            if (request.LstUserGroups != null && request.LstUserGroups.Count > 0)
            {
                foreach (var id in request.LstUserGroups)
                {
                    request.LstUserGroup.Add(new TblUserGroup { GroupID = id, UserID = request.UserID });
                }
            }
            if (request.UserID == 0)
            {
                var response = await _userService.CreateUserFromAppValid(GetToken(), request.ToObject());
                return Json(response);
            }
            else
            {
                var user = await _userService.GetUserByUserID(GetToken(), request.UserID);
                if (user == null || user.UserID == 0)
                {
                    return Json(new { Result = 0, Message = "Tên đăng nhập không tồn tại, vui lòng thử lại!" });
                }
                var response = await _userService.UpdateUserFromApp(GetToken(), request.ToObject());
                if (response.Result == 1)
                {
                    return Json(new { Result = 1, Message = "Cập nhật thành công!" });
                }
                else
                {
                    return Json(new { Result = 0, Message = "Cập nhật không thành công, vui lòng thử lại!" });
                }
            }
            // Return
        }
        public IActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> SaveChangePassword(UserChangePassword request)
        {
            if (!ModelState.IsValid)
            {
                var message =
                    ModelState.Where(modelState => modelState.Value.Errors.Count > 0)
                        .Select(x => x.Value)
                        .FirstOrDefault();
                return Json(new { Result = 1, Message = message });
            }
            request.NewPassword = Md5.GetMD5Hash(request.NewPassword);
            //var response = await _userService.CreateUserFromAppValid(GetToken(), );
            return Json(new { Result = 1, Message = "Cập nhật thành công!" });
        }
    }
}
