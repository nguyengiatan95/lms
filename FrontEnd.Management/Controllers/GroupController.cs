﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Entites.Management;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Services.Group;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Management.Controllers
{
    public class GroupController : BaseController
    {
        private IGroupService groupService;
        public GroupController(IConfiguration configuration, IGroupService _groupService)
            : base(configuration)
        {
            groupService = _groupService;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<ActionResult> GetListData()
        {
            var dataRequest = Request.Form.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.ToString()); 
            var dicts = Request.Form.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.ToString());
            var request = dicts.ToNameValueCollection();
            TableAjaxBase tableAjax = new TableAjaxBase(request); 

            string generalSearch = Request.Form["query[txt_search]"];
            int Status = string.IsNullOrEmpty(Request.Form["query[Status]"]) ? -1 : int.Parse(Request.Form["query[Status]"]);
            if (string.IsNullOrEmpty(tableAjax.field))
            {
                tableAjax.field = "groupID";
                tableAjax.sort = "desc";
            }
            var lstData = await groupService.GetGroup(GetToken(), generalSearch, Status, tableAjax.GetPageIndex, tableAjax.GetPageSize);
            tableAjax.total = lstData != null && lstData.Any() ? lstData[0].TotalCount.ToString() : "0";
            return Json(new { meta = tableAjax, data = lstData });
        }
        [HttpPost]
        public async Task<ActionResult> AddOrUpDate(Common.Models.Management.Group.GroupModel request)
        {
            if (request.GroupID == 0)
            {
                var response = await groupService.CreateGroupFromAppValid(GetToken(), request.ToObject());
                return Json(response);
            }
            else
            {
                var response = await groupService.UpdateGroupFromApp(GetToken(), request.ToObject());
                response.Data = response.Data.ToString();
                return Json(response);
            }
        }
        [HttpPost]
        public async Task<ActionResult> GetAll()
        {  
            TableAjaxBase tableAjax = new TableAjaxBase ();

            string generalSearch = "";
            int Status = 1;
            if (string.IsNullOrEmpty(tableAjax.field))
            {
                tableAjax.field = "groupID";
                tableAjax.sort = "desc"; 
            }
            var lstData = await groupService.GetGroup(GetToken(), generalSearch, Status, tableAjax.GetPageIndex, tableAjax.GetPageSize);
            tableAjax.total = lstData != null && lstData.Any() ? lstData.Count.ToString() : "0";
            return Json(new { meta = tableAjax, data = lstData });
        }
    }
}