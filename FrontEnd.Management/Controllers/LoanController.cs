﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Management.Controllers
{
    public class LoanController : BaseController
    {
        public LoanController(IConfiguration configuration)
              : base(configuration)
        { 
        }
        public IActionResult Index()
        {
            return View();
        }
    }
}