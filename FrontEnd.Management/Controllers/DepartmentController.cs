﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Models;
using FrontEnd.Common.Services.Department; 
using FrontEnd.Management.Models.Department;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FrontEnd.Management.Controllers
{
    public class DepartmentController : BaseController
    {
        private IDepartmentService _departmentService;
        public DepartmentController(IConfiguration configuration, IDepartmentService departmentService)
           : base(configuration)
        {
            _departmentService = departmentService;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<ActionResult> GetListData()
        {
            var modal = new DerpartmentDatatable(HttpContext.Request.Form);
            var lstData = await _departmentService.GetByConditions(GetToken(), modal.ToQueryObject());
            modal.total = lstData != null && lstData.Any() ? lstData[0].TotalCount.ToString() : "0";
            // Return
            return Json(new { meta = modal, data = lstData });
        }
        [HttpPost]
        public ActionResult AddOrUpDate(FrontEnd.Common.Entites.Management.TblDepartment request)
        {
            if (request.DepartmentID == 0)
            {
                var response = _departmentService.AddDepartment(request, GetToken());
                return Json(response);
            }
            else
            {
                var response = _departmentService.UpdateDepartment(request, GetToken());
                return Json(response);
            }
            // Return
        }
    }
}