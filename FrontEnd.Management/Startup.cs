using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontEnd.Common.Helpers;
using FrontEnd.Common.Services.Department;
using FrontEnd.Common.Services.Group;
using FrontEnd.Common.Services.LogCompare;
using FrontEnd.Common.Services.Login;
using FrontEnd.Common.Services.Permission;
using FrontEnd.Common.Services.PermissionGroup;
using FrontEnd.Common.Services.User;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace FrontEnd.Management
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<KestrelServerOptions>(Configuration.GetSection("Kestrel")); 
            services.Configure<FrontEnd.Common.Helpers.ServiceHost>(Configuration.GetSection("ServiceHost")); 
            services.AddControllersWithViews();
            services.AddSession();
            services.AddHttpClient();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IPermissionService, PermissionService>();
            services.AddTransient<IGroupService, GroupService>();
            services.AddTransient<IDepartmentService, DepartmentService>();
            services.AddTransient<IGroupPermissionServices, GroupPermissionServices>();
            services.AddTransient<ILoginService, LoginService>();
            services.AddTransient<ILogCompareServices, LogCompareServices>();
             
            Constants.STATIC_USERMODEL = Configuration["Configuration:CookieName"].ToString();
            Constants.STATIC_CALL_INFO_COOKIE = Configuration["Configuration:STATIC_CALL_INFO_COOKIE"].ToString();
            Constants.STATIC_VERSION = $"{DateTime.Now.Ticks}";// Configuration["Configuration:JsVersion"].ToString();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline. 
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();
            }
            //app.UseHttpsRedirection();
            app.UseSession();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
