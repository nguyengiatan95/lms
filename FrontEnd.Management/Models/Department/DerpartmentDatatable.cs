﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEnd.Management.Models.Department
{
    public class DerpartmentDatatable : DatatableBase
    {
        public DerpartmentDatatable(IFormCollection form) : base(form)
        {
            GeneralSearch = form["query[txt_search_department]"].FirstOrDefault();
            Status = form["query[sl_search_status]"].FirstOrDefault();
        }
        public string GeneralSearch { get; set; }
        public string Status { get; set; }
    }
}
