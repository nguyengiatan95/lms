﻿
var user = new function () {
    var dataDepartment = [];
    var childDepartmentID = [];
    var lstUserDepartment = [];
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var data = {
                    GeneralSearch: $('#txt_search').val().trim(),
                    Status: parseInt($('#sl_search_status').val().trim()),
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxDone('GET', baseCommon.Endpoint.GetUser, (data), function (res) {
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    if (res.Result == 1) {
                        options.success(res);
                    }
                }, "Thao tác thất bại, vui lòng thử lại sau!");
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function (objQuery) {
        $("#dv_result").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            selectable: "row",
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 2) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {

                    field: 'UserName',
                    title: 'User Name',
                    textAlign: 'left'
                },
                {

                    field: 'FullName',
                    title: 'Họ và tên',
                    width: 350,
                    textAlign: 'left'
                },
                {
                    field: 'CreateDate',
                    title: 'Ngày tạo',
                    textAlign: 'center',
                    template: function (row) {
                        return formattedDate(row.CreateDate);
                    }
                },
                {
                    field: 'Status',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    // callback function support for column rendering
                    template: function (row) {
                        if (row.Status == 1) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Hoạt Động</span>';
                        } else if (row.Status == 0) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Khóa</span>';
                        } else {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Xóa</span>';
                        }
                    }
                }
                ,
                {
                    field: 'Action',
                    title: 'Hành động',
                    sortable: false,
                    width: 160,
                    template: function (row, index, datatable) {
                        return '\
                          <a class="btn btn-primary btn-icon btn-sm" title="Sửa" data-toggle="modal" data-target="#modal_add_edit" onclick="user.ShowModal('+ row.UserID + ',' + row.DepartmentID + ',\'' + row.UserName + '\',\'' + row.FullName + '\',\'' + row.Email + '\',' + row.Status + ',' + row.LstUserGroup + ')">\
                            <i class="fa fa-edit" ></i>\
                            </a>\
					';
                    }
                }
            ]
        });
    };
    var GridRefresh = function () {
        baseCommon.ButtonSubmit(false, '#btnGetData');
        var grid = $("#dv_result").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };
    var Init = function () {
        GridRefresh();
        $("#sl_search_status").on('change', function (e) {
            GridRefresh();
        });
        $("#txt_search").on('keypress', function (e) {
            GridRefresh();
        });
        $("#btnGetData").on('click', function (e) {
            GridRefresh();
        });
        $('#LstUserGroup').select2({
            placeholder: "Chọn nhóm",
            tags: true
        });
        submitFormSave();

        var data = new FormData();
        data.append("txt_search", "");
        data.append("query[Status]", 1);
        data.append("sl_search_status", 1);
        data.append("txt_search_department", "");
        data.append("perpage", 1000000);
        data.append("PageIndex", 1);
        data.append("PageSize", 1000000); //
        baseCommon.GetDataSource("/Group/GetListData", data, function (data) { dataGroups = data.data; });
        var data = { "GeneralSearch": "", "Status": 1, "PageIndex": 1, "PageSize": baseCommon.Option.AllPage }
        baseCommon.AjaxSuccess("GET", baseCommon.Endpoint.GetDepartment, data, function (respone) {
            if (respone.Result == 1) {
                dataDepartment = jQuery.grep(respone.Data, function (a) {
                    return a.ParentID == 0;
                });
                childDepartmentID = jQuery.grep(respone.Data, function (a) {
                    return a.ParentID > 0;
                });
                //console.log(childDepartmentID);
                //baseCommon.DataSource("#DepartmentID", "Chọn phòng ban", "DepartmentID", "DepartmentName", 0, dataDepartment);
                //baseCommon.DataSource("#ChildDepartmentID", "Chọn phòng ban", "DepartmentID", "DepartmentName", 0, childDepartmentID);
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        });
        loadFromRepeat();
    };
    var ShowModal = function (id = 0, departmentID = 0, userName = '', fullName = '', email = '', status = 1, lstUserGroup = 0) {
        var form = $('#btn_save').closest('form');
        form.validate().destroy();
        $("#hdd_Id").val(id);
        $('#UserName').val(userName);
        $('#FullName').val(fullName);
        $('#Email').val(email);

        if (status == 1) {
            $('#kt_switch_2').attr('checked', 'checked');
        } else {
            $('#kt_switch_2').removeAttr('checked');
        }
        if (id == 0)//form tạo mới
        {
            $('#header_add_edit').text('Thêm mới');
            $('#btn_save').html('<i class="fa fa-save"></i>Thêm mới');
            baseCommon.DataSource('#LstUserGroup', 'Chọn nhóm', 'groupID', 'groupName', lstUserGroup, dataGroups);
            var appenHtml = '';
            appenHtml += `<div class="row form-group" data-repeater-item ><label class="col-form-label col-lg-3 col-sm-4 col-4">
                                    Phòng ban
                                </label>
                                <div class="col-lg-3 col-sm-4 col-4">
                                    <select class="form-control kt-select2 DepartmentID" name="[${0}][DepartmentID]" style="width:100%" onchange="user.loadDepartmentChild(this.value, 0);">
                                        
                                    </select>
                                </div>
                                <div class="col-lg-3 col-sm-4 col-4">
                                    <select class="form-control kt-select2 ChildDepartmentID" name="[${0}][ChildDepartmentID]" style="width:100%">

                                    </select>
                                </div>
                                <div class="col-lg-2 col-sm-4 col-4">
                                    <select class="form-control kt-select2" name="[${0}][PositionID]" style="width:100%">
                                            <option value="1">Nhân viên</option>
                                            <option value="2" >Trưởng nhóm</option>
                                            <option value="3">Trưởng phòng</option>
                                            <option value="4">Giám đốc</option>
                                    </select>
                                </div>
                                <div class="col-lg-1">
                                    <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon">
                                        <i class="la la-remove"></i>
                                    </a>
                                </div></div>`;
            $('#dv_department_group').html('');
            $('#dv_department_group').append(appenHtml);
            $(".kt-select2").select2({
                placeholder: "Chọn giá trị"
            });

            loadDepartment();
            //loadDepartmentChild();
        }
        else {
            $('#header_add_edit').text('Cập nhật');
            $('#btn_save').html('<i class="fa fa-save"></i>Cập nhật');
            baseCommon.GetDataSource("/User/GetUserByUserID?id=" + id, "", function (data) {
                if (data.data != null && typeof data.data != "undefined") {
                    if (data.data.departmentID == 0) {
                        $('#DepartmentID').val(-111).change();
                    } else {
                        $('#DepartmentID').val(data.data.departmentID).change();
                    }
                    if (data.data.lstUserGroup != null && typeof data.data.lstUserGroup != "undefined") {
                        lstUserGroup = data.data.lstUserGroup;
                        baseCommon.DataSource('#LstUserGroup', 'Chọn nhóm', 'groupID', 'groupName', lstUserGroup, dataGroups);
                    }
                }
            });
            baseCommon.AjaxSuccess('GET', baseCommon.Endpoint.GetUserByID + "/" + id, null, function (respone) {
                if (respone.Result == 1) {
                    lstUserDepartment = respone.Data.LstUserDepartment;
                    var appenHtml = '';
                    $.each(lstUserDepartment, function (index, value) {
                        appenHtml += `<div class="row form-group" data-repeater-item ><label class="col-form-label col-lg-3 col-sm-4 col-4">
                                    Phòng ban
                                </label>
                                <div class="col-lg-3 col-sm-4 col-4">
                                    <select class="form-control kt-select2 DepartmentID" name="[${index}][DepartmentID]" style="width:100%" onchange="user.loadDepartmentChild(this.value, ${index})";>
                                        
                                    </select>
                                </div>
                               <div class="col-lg-3 col-sm-4 col-4">
                                    <select class="form-control kt-select2 ChildDepartmentID" name="[${index}][ChildDepartmentID]" style="width:100%">

                                    </select>
                                </div>
                                <div class="col-lg-2 col-sm-4 col-4">
                                    <select class="form-control kt-select2" name="[${index}][PositionID]" style="width:100%">
                                            <option value="1">Nhân viên</option>
                                            <option value="2" >Trưởng nhóm</option>
                                            <option value="3">Trưởng phòng</option>
                                            <option value="4">Giám đốc</option>
                                    </select>
                                </div>
                                <div class="col-lg-1">
                                    <a href="javascript:;" data-repeater-delete="" class="btn btn-danger btn-icon">
                                        <i class="la la-remove"></i>
                                    </a>
                                </div></div>`;
                    });
                    $('#dv_department_group').html('');
                    $('#dv_department_group').append(appenHtml);
                    $(".kt-select2").select2({
                        placeholder: "Chọn giá trị"
                    });

                    loadDepartment();

                    $.each(lstUserDepartment, function (i, value) {
                        debugger
                        $(`[name='[${i}][DepartmentID]']`).val(value.DepartmentID).change();
                        $(`[name='[${i}][PositionID]']`).val(value.PositionID).change();
                        loadDepartmentChild(value.DepartmentID, i);
                        setTimeout(function () {
                            $(`[name='[${i}][ChildDepartmentID]']`).val(value.ChildDepartmentID).change();
                        }, 500);
                    });
                } else {

                }
            }, "Có lỗi trong quá trình xử lý, vui lòng thử lại sau");


        }
        // baseCommon.DataSource('#DepartmentID', 'Chọn phòng ban', 'departmentID', 'departmentName', departmentID, dataDepartments); 
        $('#modal_add_edit').modal('toggle');
    }
    var loadFromRepeat = function () {
        $("#departmentGroup_repeat").repeater({
            initEmpty: !1,
            defaultValues: {

            },
            show: function () {
                $(this).slideDown()
            },
            hide: function (e) {
                $(this).slideUp(e)
            }
        });
    };
    var initFormRepeat = function () {
        setTimeout(function () {
            $(".kt-select2").select2({
                placeholder: "Chọn giá trị"
            });
            loadDepartment();
            //loadDepartmentChild();
        }, 500);
        // map giá trị cho phòng ban

    }
    var loadDepartment = function () {
        var elemArray = document.getElementsByClassName('DepartmentID');
        for (var i = 0; i < elemArray.length; i++) {
            var elem = elemArray[i].value;
            if (elem == '' || elem == null) {
                $(`[name='[${i}][DepartmentID]']`).html('');
                $(`[name='[${i}][DepartmentID]']`).append(`<option value=''></option>`);
                $.each(dataDepartment, function (index, value) {
                    $(`[name='[${i}][DepartmentID]']`).append(`<option value='${value.DepartmentID}'>${value.DepartmentName}</option>`);
                });
            }
        }
    }
    var loadDepartmentChild = function (parentDepartment, i) {

        var elemArray = document.getElementsByClassName('ChildDepartmentID');
        var currentChild = jQuery.grep(childDepartmentID, function (a) {
            return a.ParentID == parseInt(parentDepartment);
        });
        $(`[name='[${i}][ChildDepartmentID]']`).html('');
        $(`[name='[${i}][ChildDepartmentID]']`).append(`<option value='0'>Chọn phòng ban</option>`);
        $.each(currentChild, function (index, value) {
            $(`[name='[${i}][ChildDepartmentID]']`).append(`<option value='${value.DepartmentID}'>${value.DepartmentName}</option>`);
        });
    }


    var submitFormSave = function () {
        $('#btn_save').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    UserName: {
                        required: true,
                    },
                    DepartmentID: {
                        required: true,
                    },
                    LstUserGroup: {
                        required: true,
                    }
                },
                messages: {
                    UserName: {
                        required: "Vui lòng nhập UserName",
                    },
                    DepartmentID: {
                        required: "Vui lòng chọn phòng ban",
                    },
                    LstUserGroup: {
                        required: "Vui lòng chọn nhóm",
                    }
                }
            });
            var urlAPI = baseCommon.Endpoint.CreateUserFromAppValid;
            var lstDepartmentUser = [];
            var elemArray = document.getElementsByClassName('DepartmentID');
            for (var i = 0; i < elemArray.length; i++) {
                var elem = elemArray[i].value;
                var positionId = $(`[name='[${i}][PositionID]']`).val();
                var childDepartmentID = $(`[name='[${i}][ChildDepartmentID]']`).val();
                console.log(positionId);
                if (elem == '') {
                    showMesage("danger", 'Bạn chưa chọn phòng ban');
                    return;
                }
                if (positionId == '' || positionId == null) {
                    showMesage("danger", 'Bạn chưa chọn vị trí');
                    return;
                }
                var flag = 0;
                $.each(lstDepartmentUser, function (index, value) {
                    if (parseInt(elem) == value.DepartmentID) {
                        showMesage("danger", 'Phòng ban này đã tồn tại, vui lòng chọn lại');
                        flag = 1;
                    }
                });
                if (flag > 0) {
                    return;
                }
                // thêm mới
                var dateNow = new Date();
                var formatedDate = dateNow.toJSON();
                var requestDepartment = {
                    DepartmentID: parseInt(elem),
                    PositionID: parseInt(positionId),
                    Status: 1,
                    CreateDate: formatedDate,
                    ModifyDate: formatedDate,
                    ParentUserID: 0,
                    ChildDepartmentID: parseInt(childDepartmentID)
                }
                lstDepartmentUser.push(requestDepartment);
            }
            if (parseInt($('#hdd_Id').val()) > 0) {
                $.each(lstDepartmentUser, function (i, value) {
                    $.each(lstUserDepartment, function (item, child) {
                        if (value.DepartmentID == child.DepartmentID) {
                            value.Status = 1;
                            value.CreateDate = child.CreateDate;
                            value.ParentUserID = child.ParentUserID;
                        }
                    });
                });
                urlAPI = baseCommon.Endpoint.UpdateUserFromApp;
            }
            if (!form.valid()) {
                return;
            }
            var lstUserGroup = $('#LstUserGroup').val();
            var lstUserGroups = [];
            $.each(lstUserGroup, function (index, value) {
                var objRequest = {
                    GroupID: parseInt(value)
                }
                lstUserGroups.push(objRequest);
            });
            // lấy listDepartment
            var model = {
                UserID: parseInt($('#hdd_Id').val()),
                Status: $('#kt_switch_2').prop('checked') ? 1 : 0,
                UserName: $('#UserName').val(),
                FullName: $('#FullName').val(),
                DepartmentID: $('#DepartmentID').val(),
                Email: $('#Email').val(),
                LstUserGroup: lstUserGroups,
                Password: $('#Password').val(),
                LstUserDepartment: lstDepartmentUser
            };
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            // var data = form.serialize();
            baseCommon.AjaxSuccess('POST', urlAPI, JSON.stringify(model), function (respone) {
                if (respone.Result == 1) {
                    GridRefresh();
                    baseCommon.ShowSuccess(respone.Message);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                } else {
                    baseCommon.ShowErrorNotLoad(respone.Message);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                }
            }, "Có lỗi trong quá trình xử lý, vui lòng thử lại sau");
        });
    }
    return {
        Init: Init,
        GridRefresh: GridRefresh,
        ShowModal: ShowModal,
        submitFormSave: submitFormSave,
        initFormRepeat: initFormRepeat,
        loadDepartmentChild: loadDepartmentChild
    };
}

