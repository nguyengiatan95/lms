﻿var ApiUrl;
var LoginJs = new function () { 
    var BaseUrl = {
        "Authen": ApiUrl + "authen/",
        "Admin": ApiUrl + "admin/" 
    },
    Endpoint = {
        "Login": BaseUrl["Authen"] + "api/authen/login",
        "LoginPost": "/Login/LoginPost",
        "GetUserInfoWithMenu": BaseUrl["Admin"] + "api/User/GetUserInfoWithMenu", 
        },
    AppId = {
        "Admin": 1,
        "Accountant": 2,
        "Lender": 3,
        "THN": 4
    },
    Constants = {
        "STATIC_USERMODEL": STATIC_USERMODEL,
        "STATIC_MENU": "MENU"
    },
    Cookie = {
        "Expires": 30
    };
    var AjaxPost = function (endpoint, data, done, fail, token) {
        return $.ajax({
            type: "POST",
            headers: {
                'Authorization': token
            },
            url: endpoint,
            data: data,
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).done(done).fail(fail);
    };
    var CheckLogin = function() {
        try { 
            var userModel = $.cookie(Constants.STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                var user = JSON.parse(userModel);
                access_token = user.Token;
                if (access_token != null && access_token != "" && access_token.trim() != "") {
                    var data = { AppId: AppId.Admin };
                    AjaxPost(Endpoint.GetUserInfoWithMenu, JSON.stringify(data), function (respone) {
                        if (respone.Result == 1 && user.UserLoginModel != null) {
                            var url_string = window.location.href;
                            var referrer = new URL(url_string).searchParams.get("referrer");
                            CheckReferrer(referrer, user);
                        } else {
                            $.removeCookie(Constants.STATIC_USERMODEL, { path: '/' });
                            $('#formLogin').show();
                            $('#preCheckLogin').hide();
                        }
                    }, function (respone) {
                            $.removeCookie(Constants.STATIC_USERMODEL, { path: '/' });
                        $('#formLogin').show();
                        $('#preCheckLogin').hide();
                    }, access_token);
                }  
            } 
            //cho login bằng user-pass
            $('#formLogin').show();
            $('#preCheckLogin').hide();
        } catch (err) {
            LoginPage();
        }
    };
    var handleSignInFormSubmit = function () {
        $('#kt_login_signin_submit').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    username: {
                        required: true,
                    },
                    password: {
                        required: true
                    }
                },
                messages: {
                    username: {
                        required: "Vui lòng nhập tên đăng nhập",
                    },
                    password: {
                        required: "Vui lòng nhập mật khẩu ",
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true); 
            var username = $('#Username').val();
            var password = $('#Password').val();
            var url_string = window.location.href;
            var referrer = new URL(url_string).searchParams.get("referrer");
            var model = { UserName: username, Password: password }
            AjaxPost(Endpoint.LoginPost, JSON.stringify(model), function (respone) {
                if (respone.status == 1) {
                    // Add cookie 
                    var remember = document.getElementById("remember").checked;
                    if (remember != undefined && remember == true) {
                        createCookie("cm_username", username, 30);
                        createCookie("cm_password", password, 30);
                    }
                    else {
                        createCookie("cm_username", username, -1);
                        createCookie("cm_password", password, -1);
                    }
                    CheckReferrer(referrer, JSON.parse(respone.data));
                } else {
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    showErrorMsg(form, 'danger',respone.message);
                }
            }, function (respone) {
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                showErrorMsg(form, 'danger', respone.message);
            });
        });
    }
    var CheckReferrer = function (referrer, userModel) {
        try {
            var userLoginModel = userModel.UserLoginModel;
            //localhost:44342/Invoice/Index" 
            //http://localhost:44342"
            var accountantUrlHost = userLoginModel.AccountantUrl.substring(userLoginModel.AccountantUrl.indexOf("//") + 2, userLoginModel.AccountantUrl.length);
            var lenderUrlHost = userLoginModel.LenderUrl.substring(userLoginModel.LenderUrl.indexOf("//") + 2, userLoginModel.LenderUrl.length);
            var recoveryOfLoansUrlHost = userLoginModel.THNUrl.substring(userLoginModel.THNUrl.indexOf("//") + 2, userLoginModel.THNUrl.length);
            if (referrer == null || typeof referrer == "undefined" || referrer == "") {
                window.open("/", "_self");
            } else { 
                //// referrer = referrer.replace(/-/gi, '/');
                //var from = referrer.indexOf("<<");
                //if (from < 0) {
                //    from = 0;
                //} else {
                //    from += 2;
                //}
               // var to = referrer.indexOf("tima.vn") + 7;
               //// var newUrl = (new URL(url));  
                var referrerHost = referrer.replace(/>/gi, ':').replace(/</gi, '/').replace(/\*/gi, '&');//newUrl.hostname + newUrl.port;;//referrer.substring(from, to).replace(/>/gi, ':');
                // var converReferrer = referrer.replace(/>/gi, ':').replace(/</gi, '/').replace(/\*/gi, '&');
                var pathString = "/redirect?token=" + userModel.Token + "&referrer=" + referrer + "&TimeExpired=" + userModel.TimeExpiredString;
                if (referrerHost.includes(accountantUrlHost)) {
                    var url = userLoginModel.AccountantUrl + pathString;
                    window.open(url, "_self");
                } else if (referrerHost.includes(lenderUrlHost)) {
                    var url = userLoginModel.LenderUrl + pathString;
                    window.open(url, "_self");
                } else if (referrerHost.includes(recoveryOfLoansUrlHost)) {
                    var url = userLoginModel.THNUrl + pathString;
                    window.open(url, "_self");
                } else { 
                    var url = referrer.replace(/>/gi, ':').replace(/</gi, '/').replace(/\*/gi, '&');
                    var urlFrom = url.indexOf("/");
                    var getParam = url.substring(urlFrom, url.length);
                    if (getParam == null || getParam == "") {
                        getParam = '/';
                    }
                    window.open(getParam, "_self");
                }
            }
        } catch (e) {
            window.open('/', "_self");
        } 
    }; 
    var checkRemmember = function () { 
        var username = readCookie("cm_username");
        if (username != '') {
            var password = readCookie("cm_password");
            $("#kt_form_login").find("#username").val(username);
            $("#kt_form_login").find("#password").val(password);
            $("#kt_form_login").find("#remember").prop('checked', true);
            $("#kt_form_login").find("#remember").closest("span").addClass("checked");
        }
        else {
            $("#kt_form_login").find("#username").val('');
            $("#kt_form_login").find("#password").val('');
        }
    };
    var createCookie = function (name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        }
        else var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    };
    var readCookie = function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return '';
    };
    var showErrorMsg = function (form, type, msg) {
        var alert = $('<div class="alert alert-' + type + ' alert-dismissible" role="alert">\
			<div class="alert-text">'+ msg + '</div>\
			<div class="alert-close">\
                <i class="flaticon2-cross kt-icon-sm" data-dismiss="alert"></i>\
            </div>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        KTUtil.animateClass(alert[0], 'fadeIn animated');
        alert.find('span').html(msg);
    }

    return {
        // public functions
        init: function () { 
            handleSignInFormSubmit(); 
            checkRemmember();
            CheckLogin();
        } 
    };
}
// Class Initialization


 