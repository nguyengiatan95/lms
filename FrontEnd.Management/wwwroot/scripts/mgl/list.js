﻿var recordGrid = 0;
var statusStr = ["Có lỗi xảy ra", "Chờ xử lý"];
var loanID;
var mgl = new function () {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var reportId = $('#sl_reportId').val();
                if (reportId == null || reportId == "") {
                    reportId = -111
                }
                var statusId = $('#sl_statusId').val();
                if (statusId == null || statusId == "") {
                    statusId = -111
                }
                var employeeId = $('#sl_employee').val();
                var fromDate = $('#fromDate').val();
                var toDate = $('#toDate').val();

                var data = {
                    TypeReport: parseInt(reportId),
                    FromDate: fromDate,
                    ToDate: toDate,
                    Status: parseInt(statusId),
                    CreateBy: parseInt(employeeId),
                    TypeFile: 2,
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize,
                }
                baseCommon.AjaxDone('POST', baseCommon.Endpoint.GetHistoryExportFile, JSON.stringify(data), function (res) {
                    options.success(res.Data);
                    if (res.Result == 1) {
                    } else {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = 0;
                if (response != null && typeof response.RecordsTotal != "undefined") {
                    total = response.RecordsTotal;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                var data = [];
                if (response != null && typeof response.Data != "undefined") {
                    data = response.Data;
                }
                return data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function () {
        $("#dv_result").kendoGrid({
            dataSource: dataSource,
            selectable: "row",
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 2) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'CreateByName',
                    title: 'UserName',
                },
                {
                    field: 'ReportName',
                    title: 'Tên Report',
                    width: 350
                },
                {
                    field: 'CreateDate',
                    title: 'Thời gian thao tác',
                    textAlign: 'center',
                    template: function (row) {
                        if (row == 0 || row == null || row.CreateDate == null || row.CreateDate == "0001-01-01T00:00:00") {
                            return "";
                        }
                        var date = moment(row.CreateDate);
                        return date.format("DD/MM/YYYY HH:mm:ss");
                    }
                },
                {
                    field: 'StatusName',
                    title: 'Trạng thái',
                    headerAttributes: { style: "text-align: center" },
                    attributes: { style: "text-align:center;" },
                    template: function (row) {
                        return '<span>' + row.StatusName + '</span>';
                    },
                },
                {
                    title: "Hành động",
                    template: function (row) {
                        var html = ''
                        if (statusStr.includes(row.StatusName)) {
                            return html;
                        } else {
                            html = '<button class="btn btn-success" title="Xuất excel" onclick="mgl.printGrid(' + row.CreateBy + ',\'' + row.TraceIDRequest + '\')">\
                                            <i onclick="javascript:;" class="fa fa-print"></i>In mẫu</button> ';
                        }
                        if (row.StatusName == "Thành công") {
                            html += '<button class="btn btn-success" title="Xuất excel" onclick="mgl.PushMGL(\'' + row.ReportName + '\',\'' + row.TraceIDRequest + '\')">\
                                            <i onclick="javascript:;" class="fa fa-check"></i>Đẩy hồ sơ</button> ';
                        }
                        return html;
                    }
                }
            ]
        });
    };
    var GetDataSelectEmployee = function () {
        var data = {
            PositionID: -111,
        };
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.Getlstemployees, JSON.stringify(data), function (respone) {
            if (respone.Result == 1) {
                baseCommon.DataSource("#sl_employee", 'Tất cả', "Value", "Text", 0, respone.Data);
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        }, "Lấy danh sách nhân viên không thành công vui lòng thử lại!");
    };
    var GridRefresh = function () {
        var FromDate = $('#fromDate').val();
        var ToDate = $('#toDate').val();
        var formatFromDate = moment(FromDate, "DD/MM/YYYY").format('MM/DD/YYYY');
        var formatToDate = moment(ToDate, "DD/MM/YYYY").format('MM/DD/YYYY');
        var diff = Math.abs(new Date(formatFromDate) - new Date(formatToDate));
        var compareDate = diff / (1000 * 3600 * 24);
        if (compareDate > 30) {
            baseCommon.ShowErrorNotLoad("Ngày tìm kiếm không được quá 30 ngày!");
            return;
        }
        var grid = $("#dv_result").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };
    var Init = function () {
        GetDataSelectEmployee();
        setFromDateToDate();
        CustomerList();
        setTimeout(function () {
            InitGrid();
        }, 1300);
        $("#btnGetData").on('click', function (e) {
            GridRefresh();
        });
        $("#fromDate,#toDate,#sl_reportId,#sl_statusId,#sl_employee,#txt_Reason").on('change', function (e) {
            GridRefresh();
            var reasonID = $("#txt_Reason").val()
            if (reasonID === '3') {
                $('#form_reason').show()
                $('#txt_hiddenReason').val('')
            } else {
                $('#form_reason').hide()
            }
        });
    };
    var setFromDateToDate = function () {
        var date = new Date();
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        $("#fromDate").datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            language: "vi",
        }).datepicker("setDate", getFormattedDate(firstDay));
        $("#toDate").datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            language: "vi",
        }).datepicker("setDate", getFormattedDate(date));
    }
    var printGrid = function (createBy, traceIDRequest) {
        var data = {
            TraceIDRequest: traceIDRequest,
            CreateBy: createBy,
        }
        baseCommon.AjaxDone('POST', baseCommon.Endpoint.GetFormLoanExemptionInterest, JSON.stringify(data), function (res) {
            if (res.Result == 1 && res.Data !== null) {
                var ngayGiaiNgan = moment(res.Data.LoanFromDate).format("DD/MM/YYYY");
                var ngayKetThuc = moment(res.Data.LoanToDate).format("DD/MM/YYYY");
                var khoanVayDenNgay = moment(res.Data.CutOffDate).format("DD/MM/YYYY");
                var ngayTao = moment(res.Data.CreateDate).format("DD/MM/YYYY");
                var win = window.open('', '', 'width=10000,height=10000, resizable=1, scrollbars=1'),
                    doc = win.document.open();
                $.get("/static/scripts/mgl/mgl_0.html?v=" + staticVersionMGL, function (data) {
                    data = data.replace('txt_TenKH', res.Data.CustomerName ?? '');
                    data = data.replace('txt_MaTC', res.Data.LoanContractCode ?? '');
                    data = data.replace('txt_NgayGiaiNgan', ngayGiaiNgan ?? '');
                    data = data.replace('txt_BookNo', res.Data.BadDebtYear ?? '');
                    data = data.replace('txt_NgayKetThuc', ngayKetThuc ?? '');
                    data = data.replace('txt_SoTienGiaiNgan', formatCurrency(res.Data.LoanTotalMoneyDisbrusement) ?? '');
                    data = data.replace('txt_SoTienDaThu', formatCurrency(res.Data.LoanTotalMoneyReceived) ?? '');
                    data = data.replace('txt_TongNghiaVuPhaiTra', formatCurrency(res.Data.MoneyCloseLoan.TotalMoneyNeedPay) ?? '');
                    data = data.replace('txt_SoTienThuDuKien', formatCurrency(res.Data.MoneyExpected.TotalMoneyNeedPay) ?? '');
                    data = data.replace('txt_SoTienDeXuatMienGiam', formatCurrency(res.Data.MoneySuggetExemption.TotalMoneyNeedPay) ?? '');
                    data = data.replace('txt_TongNghiaVuPhaiTraGoc', formatCurrency(res.Data.MoneyCloseLoan.MoneyOriginal) ?? '');
                    data = data.replace('txt_SoTienThuDuKienGoc', formatCurrency(res.Data.MoneyExpected.MoneyOriginal) ?? '');
                    data = data.replace('txt_SoTienDeXuatMienGiamGoc', formatCurrency(res.Data.MoneySuggetExemption.MoneyOriginal) ?? '');
                    data = data.replace('txt_TongNghiaVuPhaiTraLai', formatCurrency(res.Data.MoneyCloseLoan.MoneyInterest) ?? '');
                    data = data.replace('txt_SoTienThuDuKienLai', formatCurrency(res.Data.MoneyExpected.MoneyInterest) ?? '');
                    data = data.replace('txt_SoTienDeXuatMienGiamLai', formatCurrency(res.Data.MoneySuggetExemption.MoneyInterest) ?? '');
                    data = data.replace('txt_TongNghiaVuPhaiTraNoCu', formatCurrency(res.Data.MoneyCloseLoan.MoneyOldDebit) ?? '');
                    data = data.replace('txt_SoTienThuDuKienNoCu', formatCurrency(res.Data.MoneyExpected.MoneyOldDebit) ?? '');
                    data = data.replace('txt_SoTienDeXuatMienGiamNoCu', formatCurrency(res.Data.MoneySuggetExemption.MoneyOldDebit) ?? '');
                    data = data.replace('txt_TongNghiaVuPhaiTraPhi', formatCurrency(res.Data.MoneyCloseLoan.TotalFee) ?? '');
                    data = data.replace('txt_SoTienThuDuKienPhi', formatCurrency(res.Data.MoneyExpected.TotalFee) ?? '');
                    data = data.replace('txt_SoTienDeXuatMienGiamPhi', formatCurrency(res.Data.MoneySuggetExemption.TotalFee) ?? '');
                    data = data.replace('txt_TongNghiaVuPhaiTraPhiTuVan', formatCurrency(res.Data.MoneyCloseLoan.MoneyConsultant) ?? '');
                    data = data.replace('txt_SoTienThuDuKienPhiTuVan', formatCurrency(res.Data.MoneyExpected.MoneyConsultant) ?? '');
                    data = data.replace('txt_SoTienDeXuatMienGiamPhiTuVan', formatCurrency(res.Data.MoneySuggetExemption.MoneyConsultant) ?? '');
                    data = data.replace('txt_TongNghiaVuPhaiTraPhiDichVu', formatCurrency(res.Data.MoneyCloseLoan.MoneyService) ?? '');
                    data = data.replace('txt_SoTienThuDuKienPhiDichVu', formatCurrency(res.Data.MoneyExpected.MoneyService) ?? '');
                    data = data.replace('txt_SoTienDeXuatMienGiamPhiDichVu', formatCurrency(res.Data.MoneySuggetExemption.MoneyService) ?? '');
                    data = data.replace('txt_TongNghiaVuPhaiTraPhiPhatTreo', formatCurrency(res.Data.MoneyCloseLoan.MoneyFineLate) ?? '');
                    data = data.replace('txt_SoTienDeXuatMienGiamPhiPhatTreo', formatCurrency(res.Data.MoneySuggetExemption.MoneyFineLate) ?? '');
                    data = data.replace('txt_TongNghiaVuPhaiTraPhiTatToan', formatCurrency(res.Data.MoneyCloseLoan.MoneyFineOriginal) ?? '');
                    data = data.replace('txt_SoTienDeXuatMienGiamPhiTatToan', formatCurrency(res.Data.MoneySuggetExemption.MoneyFineOriginal) ?? '');
                    data = data.replace('txt_SoTienVayBanDau', formatCurrency(res.Data.LoanTotalMoneyDisbrusement) ?? '');
                    data = data.replace('txt_ThanhToanChoCongTy', formatCurrency(res.Data.LoanTotalMoneyReceived) ?? '');
                    data = data.replace('txt_SoTienThuDuKienPhiPhatTreo', formatCurrency(res.Data.MoneyExpected.MoneyFineLate) ?? '');
                    data = data.replace('txt_DangQuaHanNgay', res.Data.CountDPD ?? '');
                    data = data.replace('txt_KHThanhToan', formatCurrency(res.Data.MoneyExpected.TotalMoneyNeedPay) ?? '');
                    data = data.replace('txt_DPD', res.Data.CountDPD ?? '');
                    data = data.replace('txt_NhomSanPham', res.Data.LoanProductName ?? '');
                    data = data.replace('txt_SoTienThuDuKienPhiTatToan', formatCurrency(res.Data.MoneyExpected.MoneyFineOriginal) ?? '');
                    data = data.replace('txt_TiLeMienGiam', '');
                    data = data.replace('txt_TiLeMienGiamGoc', res.Data.MoneyPercent.MoneyOriginal > 0 ? (res.Data.MoneyPercent.MoneyOriginal + ' %') : '');
                    data = data.replace('txt_TiLeMienGiamLai', res.Data.MoneyPercent.MoneyInterest > 0 ? (res.Data.MoneyPercent.MoneyInterest + ' %') : '');
                    data = data.replace('txt_TiLeMienGiamNoCu', res.Data.MoneyPercent.MoneyOldDebit > 0 ? (res.Data.MoneyPercent.MoneyOldDebit + ' %') : '');
                    data = data.replace('txt_TiLeMienGiamPhi', '');
                    data = data.replace('txt_TiLeMienGiamPhiTuVan', res.Data.MoneyPercent.MoneyConsultant > 0 ? (res.Data.MoneyPercent.MoneyConsultant + ' %') : '');
                    data = data.replace('txt_TiLeMienGiamPhiDichVu', res.Data.MoneyPercent.MoneyService > 0 ? (res.Data.MoneyPercent.MoneyService + ' %') : '');
                    data = data.replace('txt_TiLeMienGiamPhiPhatTreo', res.Data.MoneyPercent.MoneyFineLate > 0 ? (res.Data.MoneyPercent.MoneyFineLate + ' %') : '');
                    data = data.replace('txt_TiLeMienGiamPhiTatToan', res.Data.MoneyPercent.MoneyFineOriginal > 0 ? (res.Data.MoneyPercent.MoneyFineOriginal + ' %') : '');
                    data = data.replace('txt_KhoanVayDenNgay', khoanVayDenNgay ?? '');
                    data = data.replace('txt_NgayTao', ngayTao ?? '');
                    data = data.replace('txt_GetDay', moment(res.Data.CreateDate).format("DD") ?? '');
                    data = data.replace('txt_GetMonth', moment(res.Data.CreateDate).format("MM") ?? '');
                    data = data.replace('txt_GetYear', moment(res.Data.CreateDate).format("YYYY") ?? '');
                    data = data.replace('txt_PhongBan', res.Data.DepartmentName ?? '');
                    data = data.replace('txt_NgayHieuLuc', moment(res.Data.ExpirationDate).format("DD/MM/YYYY") ?? '');
                    data = data.replace('txt_TenCap', res.Data.ApprovedByName ?? '');
                    data = data.replace('txt_NoiDung', res.Data.Note ?? '');
                    console.log(data);
                    doc.write(data);
                    doc.close();
                    //win.print();
                });
            }
            else {
                baseCommon.ShowErrorNotLoad(res.Message);
            }
        });
    }
    //// partial CreateMGL
    var CustomerInfo_Clear = function () {
        var dataSource = new kendo.data.DataSource({
            data: []
        });
        var grid = $("#lstCustomer").data("kendoGrid");
        if (typeof grid != "undefined" && grid != null) {
            grid.setDataSource(dataSource);
        }
        //
    }
    var InitCreateMGL = function () {
        $("#txt_moneyCustomer").val(0)
        $("#sl_statusCreateMGLId").val(-111).change()
        $("#txt_contactCode").val("")
        $("#txt_approvalName").val("")
        $("#txt_Reason").val(1).change()
        $("#txt_departmentName").val(1).change()
        CustomerInfo_Clear()
        setDateInPartial()
    }
    function Grid_change(e) {
        var selectedRows = this.select();
        var selectedDataItems = [];
        for (var i = 0; i < selectedRows.length; i++) {
            var dataItem = this.dataItem(selectedRows[i]);
            selectedDataItems.push(dataItem);
        }
        // selectedDataItems contains all selected data items
        gridSelectedRowData = selectedDataItems[0];
        $("#txt_moneyCustomer").val(formatCurrency(gridSelectedRowData.CustomerTotalMoney))
        $("#hdd_customerID").val(gridSelectedRowData.CustomerID)
        loanID = gridSelectedRowData.LoanID;
    }
    var CustomerList = function () {
        $("#lstCustomer").kendoGrid({
            dataSource: [],
            selectable: "row",
            change: Grid_change,
            dataBound: function (e) {
                for (var i = 0; i < this.columns.length; i++) {
                    this.autoFitColumn(i);
                }
                e.sender.select("tr:eq(0)");
            },
            columns: [
                {
                    field: "CustomerName",
                    title: "Tên KH"
                },
                {
                    field: "CustomerNumberCard",
                    title: "CMT"
                },
                {
                    field: "ContactCode",
                    title: "Mã TC"
                },
                {
                    field: "ContactStartDate",
                    title: "Ngày giải ngân",
                    template: '#= baseCommon.FormatDate(ContactStartDate,\'DD/MM/YYYY\')#',
                },
                {
                    field: "LoanInterestPaymentNextDate",
                    title: "Ngày phải đóng lãi",
                    template: '#= baseCommon.FormatDate(LoanInterestPaymentNextDate,\'DD/MM/YYYY\')#',
                },
                {
                    field: "LoanTotalMoneyCurrent",
                    title: "Số tiền gốc còn lại",
                    template: '#= html_money(LoanTotalMoneyCurrent)#',
                    attributes: { style: "text-align:right;" }
                },
                {
                    field: "OwnerShopName",
                    title: "Cửa hàng"
                },
                {
                    field: "LoanTotalMoneyDisbursement",
                    title: "Tiền GN",
                    template: '#= html_money(LoanTotalMoneyDisbursement)#',
                    attributes: { style: "text-align:right;" }
                },
                {
                    field: "CustomerTotalMoney",
                    title: "Tiền Trong Ví",
                    template: '#= html_money(CustomerTotalMoney)#',
                    attributes: { style: "text-align:right;" }
                },
                {
                    field: "LoanCreditIDOfPartner",
                    title: "Mã HĐ",
                    template: '#= "HĐ-"+LoanCreditIDOfPartner#',
                },
                {
                    field: "ProductName",
                    title: "Sản phẩm",
                },
                {
                    field: "LoanStatusName",
                    title: "Trạng thái"
                },
                {
                    field: "CityName",
                    title: "Tỉnh/TP"
                }
            ]
        });
    }
    var GetCustomer = function () {
        try {
            var dataSource = new kendo.data.DataSource({
                data: []
            });
            var grid = $("#lstCustomer").data("kendoGrid");
            grid.setDataSource(dataSource);
            var customerKeySearch = $('#txt_contactCode').val();
            if (customerKeySearch == null || customerKeySearch == "" || customerKeySearch.trim() == "") {
                baseCommon.ShowErrorNotLoad("Bạn chưa nhập tên KH hoặc Mã HĐ!");
                return;
            }
            var modelCustomer = {
                "CustomerKeySearch": customerKeySearch
            };
            baseCommon.AjaxDone("POST", baseCommon.Endpoint.GetLstLoanByFilterCustomer, JSON.stringify(modelCustomer), function (respone) {
                if (respone.Result == 1) {
                    if (respone.Data != null && respone.Data.Data != null && respone.Data.Data.length > 0) {
                        var dataSource = new kendo.data.DataSource({
                            data: respone.Data.Data
                        });
                        var grid = $("#lstCustomer").data("kendoGrid");
                        grid.setDataSource(dataSource);
                    } else {
                        baseCommon.ShowErrorNotLoad("Không tìm thấy khách hàng phù hợp!");
                    }
                } else {
                    baseCommon.ShowErrorNotLoad(respone.Message);
                }
            });
        } catch (err) {
            baseCommon.ShowErrorNotLoad("Lấy dữ liệu không thành công vui lòng thử lại!");
        }
    }
    var Save_CreateMGL = function () {
        var btn = $('#btn_holdMoneyCustomer');
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

        var MoneyExpectedReceive = parseFloat($('#txt_moneyCustomer').val().replace(/[^\d\.]/g, ''));
        if (MoneyExpectedReceive == null || MoneyExpectedReceive == 0) {
            baseCommon.ShowErrorNotLoad("Số tiền không được nhỏ hơn 0!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }
        var CutOfDate = $("#CutOfDate").val();

        var Reason = $("#txt_Reason option:selected").text();
        var checkReason = $("#txt_Reason").val();

        if (checkReason === '3') {
            var inputReason = $("#txt_hiddenReason").val();
            if (inputReason == null || inputReason == '') {
                baseCommon.ShowErrorNotLoad("Vui lòng nhập lý do!");
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                return;
            }
            Reason = inputReason
        }
        var departmentName = $("#txt_departmentName option:selected").text();

        var approvalName = $('#txt_approvalName').val();
        if (approvalName == null || approvalName == '') {
            baseCommon.ShowErrorNotLoad("Tên cấp phê duyệt không được để trống!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }
        var effectiveTime = $('#txt_effectiveTime').val()

        var model = {
            "LoanID": loanID,
            "CutOfDate": CutOfDate,
            "MoneyExpectedReceive": MoneyExpectedReceive,
            "CreateBy": baseCommon.GetUser().UserID,
            "Note": Reason,
            "DepartmentName": departmentName,
            "ExpirationDate": effectiveTime,
            "ApprovedByName": approvalName,
        };
        baseCommon.AjaxDone('POST', baseCommon.Endpoint.CreateFormLoanExemptionInterest, JSON.stringify(model), function (respone) {
            if (respone.Result == 1) {
                CustomerInfo_Clear();
                setTimeout(function () {
                    $("#btn_closeCreateMGL").click();
                    GridRefresh();
                }, 100);
                baseCommon.ShowSuccess(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            }
        }, "Không thành công vui lòng thử lại!");
    }
    function formatCurrency(num) {
        num = num.toString().replace(/\$|\,/g, '');
        if (isNaN(num))
            num = "0";
        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        cents = num % 100;
        num = Math.floor(num / 100).toString();
        if (cents < 10)
            cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
            num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                num.substring(num.length - (4 * i + 3));
        return (((sign) ? '' : '-') + num);
    }
    var GetDataSelectGetLstTypeFileForm = function () {
        var data = {
            TypeFile: 2,
        };
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.GetLstTypeFileForm, JSON.stringify(data), function (respone) {
            if (respone.Result == 1) {
                baseCommon.DataSource("#sl_statusCreateMGLId", 'Tất cả', "Value", "Text", 0, respone.Data);
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        }, "Lấy danh sách không thành công vui lòng thử lại!");
    };
    var setDateInPartial = function () {
        var date = new Date();
        var lastDayOfMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        $("#txt_effectiveTime").datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            language: "vi",
        }).datepicker("setDate", getFormattedDate(lastDayOfMonth));
        $("#CutOfDate").datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            language: "vi",
        }).datepicker("setDate", getFormattedDate(date));
    }
    $(document).on('keyup', '#txt_moneyCustomer', function (e) {
        var loValue = formatCurrency($(this).val().replace(/,/g, '').replace(/\./g, ''));
        $("#txt_moneyCustomer").val(loValue);
    });

    var PushMGL = function (reportName = "", traceIDRequest ="") {
        swal.fire({
            html: "Bạn có chắc chắn đẩy hồ sơ <b>" + reportName + "<b>?",
            type: "warning",
            showCancelButton: !0,
            cancelButtonText: "Hủy",
            confirmButtonText: "Đồng ý"
        }).then(function (result) {
            if (result.value) {
                var request = {
                    "TraceIDRequest": traceIDRequest,
                    "CreateBy": USER_ID,
                };
                baseCommon.AjaxDone("POST", baseCommon.Endpoint.CreateRequestLoanExemption, JSON.stringify(request), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        GridRefresh()
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                }, "Đẩy hồ sơ không thành công vui lòng thử lại!");
            }
        });
    };
    return {
        Init: Init,
        GridRefresh: GridRefresh,
        GetDataSelectEmployee: GetDataSelectEmployee,
        printGrid: printGrid,
        InitCreateMGL: InitCreateMGL,
        Save_CreateMGL: Save_CreateMGL,
        GetCustomer: GetCustomer,
        PushMGL: PushMGL
    };
}
$(document).ready(function () {
    mgl.Init();
});
