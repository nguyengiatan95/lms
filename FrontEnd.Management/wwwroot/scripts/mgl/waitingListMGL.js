﻿var recordGrid = 0;
var statusStr = ["Có lỗi xảy ra", "Chờ xử lý"];
var LoanExemptionID;
var RequestUpload_GiayTo = [];
var RequestUpload_MGL = [];
var ListDeleteIMG = [];
var DataImagesMGL;
var DataImagesGiayTo;
var waitingListMGL = new function () {
    var viewerChungTu;
    var viewerGiayTo;
    var Option = {
        Detail: 0,
        UpdateProfile: 1,
        ProfileBrowsing: 2,
        CannelProfile: 3,
    };
    var rowClicked = Option.Detail;
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var keysearch = $("#txtKeysearch").val();
                var statusId = $('#sl_statusId').val();
                if (statusId == null || statusId == "") {
                    statusId = -111
                }
                var fromDate = $('#fromDate').val();
                var toDate = $('#toDate').val();
                var data = {
                    FromDate: fromDate,
                    ToDate: toDate,
                    KeySearch: keysearch,
                    Status: parseInt(statusId),
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize,
                }
                baseCommon.AjaxDone('POST', baseCommon.Endpoint.Getlstloanexemption, JSON.stringify(data), function (res) {
                    options.success(res.Data);
                    if (res.Result == 1) {
                    } else {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = 0;
                if (response != null && typeof response.RecordsTotal != "undefined") {
                    total = response.RecordsTotal;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                var data = [];
                if (response != null && typeof response.Data != "undefined") {
                    data = response.Data;
                }
                return data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function () {
        $("#dv_result").kendoGrid({
            dataSource: dataSource,
            selectable: "row",
            change: Grid_change,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
           /*     this.autoFitColumn(i);*/
                //for (var i = 0; i < this.columns.length; i++) {
                //    if (i != 2) {
                //        this.autoFitColumn(i);
                //    }
                //}
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #",
                    width: 80
                },
                {
                    field: 'CustomerName',
                    title: 'Tên khách hàng',
                    template: function (row) {
                        var html = "<div>" + row.CustomerName + "<br /> \
                                    <span class='item-desciption'>"+ row.LoanContractCode + "</span>\
                                    </div>";
                        return html;
                    }
                },
                {
                    field: 'CreateDate',
                    title: 'Thời gian tạo',
                    textAlign: 'center',
                    template: function (row) {
                        if (row == 0 || row == null || row.CreateDate == null || row.CreateDate == "0001-01-01T00:00:00") {
                            return "";
                        }
                        var date = moment(row.CreateDate).format("DD/MM/YYYY HH:mm:ss");
                        if (row == 0 || row == null || row.ModifyDate == null || row.ModifyDate == "0001-01-01T00:00:00") {
                            return "";
                        }
                        var modifyDate = moment(row.ModifyDate).format("DD/MM/YYYY HH:mm:ss");
                        var html = "<div>" + date + " <br />\
                                    <span class='item-desciption'><i>Thời gian cập nhật gần nhất </i></span>  <br />\
                                       "+ modifyDate + "</div>";
                        return html;
                    }
                },
                {
                    field: 'CreateByFullName',
                    title: 'Người tạo yêu cầu',
                    template: function (row) {
                        var html = "<div>" + row.CreateByFullName + "<br /> \
                                    <span> Cấp phê duyệt : <i>"+ row.ApprovalLevelBookDebtName + "</i></span>\
                                    </div>";
                        return html;
                    }
                },
                {
                    field: 'MoneySuggetExemption',
                    title: 'Số tiền đề xuất miễn giảm',
                    template: function (row) {
                        return html_money(row.MoneySuggetExemption)
                    }
                },
                {
                    field: 'CreateByFullName',
                    title: 'Hạn mức phê duyệt trong tháng',
                    template: function (row) {
                        var html = "<div>" + html_money(row.MaxMoneyApprovalLevel) + "<br />\
                                    <span class='item-desciption'> Hạn mức còn lại :"+ html_money(row.MoneyApprovalLevelCurrent) + "</span><br />\
                                    <span class='item-desciption'> Số tiền đã phê duyệt :"+ html_money(row.MoneyApprovalLevelSuccess) + "</span>\
                                    </div>";
                        return html;
                    }
                },
                {
                    field: 'LoanExemptionStatusName',
                    title: 'Trạng thái',
                    width: 300,
                    attributes: { width: "60px" },
                    template: function (row) {
                        var html = "<div><b>" + row.LoanExemptionStatusName + "</b><br /> </div>";
                        if (row.LoanExemptionStatus == 6) {
                            html += "<br /><b> Ghi chú:</b> <span><i>" + row.ReasonCancel + "</i></span>";
                        } else if (row.LoanExemptionStatus == 7) {
                            html += "<br /><b> Lỗi:</b> <span><i>" + row.ReasonCancel + "</i></span>";
                        }
                        return html;
                    }
                },
                //{
                //    field: 'ReasonCancel',
                //    title: 'Lý do hủy',
                //},
                {
                    title: "Hành động",
                    template: function (row) {
                        var html = ''
                        //chờ câp nhật chứng từ
                        if (row.LoanExemptionStatus == 1) {
                            html += '<button class="btn btn-success" data-toggle="modal" data-target="#model_update_profile" title="Cập nhật giấy tờ" onclick="waitingListMGL.RowClick(' + waitingListMGL.Option.UpdateProfile + ',\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                                            Cập nhật giấy tờ</button> ';
                            html += '<button class="btn btn-success" data-toggle="modal" data-target="#model_cannelProfileMGL" title="Hủy" onclick="waitingListMGL.RowClick(' + waitingListMGL.Option.CannelProfile + ',\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                                            Hủy</button> ';
                        }
                        else if (row.LoanExemptionStatus == 2) {
                            html += '<button class="btn btn-success" title="Đẩy hồ sơ" onclick="waitingListMGL.RowClick(' + waitingListMGL.Option.ProfileBrowsing + ',\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                                          Đẩy hồ sơ</button> ';
                            html += '<button class="btn btn-success" data-toggle="modal" data-target="#model_cannelProfileMGL" title="Hủy" onclick="waitingListMGL.RowClick(' + waitingListMGL.Option.CannelProfile + ',\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                                            Hủy</button> ';
                        } else if (row.LoanExemptionStatus == 3) {
                            html += '<button class="btn btn-success" title="Duyệt" onclick="waitingListMGL.RowClick(' + waitingListMGL.Option.ProfileBrowsing + ',\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                                          Duyệt</button> ';
                            html += '<button class="btn btn-success" data-toggle="modal" data-target="#model_cannelProfileMGL" title="Hủy" onclick="waitingListMGL.RowClick(' + waitingListMGL.Option.CannelProfile + ',\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                                            Hủy</button> ';
                        }
                        //if (row.LoanExemptionStatus != 5) {

                        //    html += '<button class="btn btn-success" title="Duyệt" onclick="waitingListMGL.RowClick(' + waitingListMGL.Option.ProfileBrowsing + ',\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                        //                  Duyệt</button> ';
                        //}
                        return html;
                    }
                }
            ]
        });
    };
    var GridRefresh = function () {
        var FromDate = $('#fromDate').val();
        var ToDate = $('#toDate').val();
        var formatFromDate = moment(FromDate, "DD/MM/YYYY").format('MM/DD/YYYY');
        var formatToDate = moment(ToDate, "DD/MM/YYYY").format('MM/DD/YYYY');
        var diff = Math.abs(new Date(formatFromDate) - new Date(formatToDate));
        var compareDate = diff / (1000 * 3600 * 24);
        if (compareDate > 30) {
            baseCommon.ShowErrorNotLoad("Ngày tìm kiếm không được quá 30 ngày!");
            return;
        }
        var grid = $("#dv_result").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };
    var RowClick = function (rowClick, row) {
        rowClicked = rowClick;
        var data = JSON.parse(row);
        if (rowClicked == Option.UpdateProfile) {
            waitingListMGL.UpdateProfile(row);
        } else if (rowClicked == Option.ProfileBrowsing) {
            waitingListMGL.ProfileBrowsing(row);
        }
        else if (rowClicked == Option.CannelProfile) {
            waitingListMGL.ShowCannelProfile(row)
        } else {
            waitingListMGL.ShowDetail(jsonData);
        }
    };
    function Grid_change(e) {
        var selectedRows = this.select();
        var selectedDataItems = [];
        for (var i = 0; i < selectedRows.length; i++) {
            var dataItem = this.dataItem(selectedRows[i]);
            selectedDataItems.push(dataItem);
        }
        // selectedDataItems contains all selected data items
        gridSelectedRowData = selectedDataItems[0];
        setTimeout(function () {
            var jsonData = JSON.stringify(gridSelectedRowData);
            if (rowClicked == Option.Detail) {
                waitingListMGL.ShowDetail(jsonData);
            }
            rowClicked = Option.Detail;
        }, 5);
    }

    var resetShowDetail = function () {
        $("#lb_dl_Customer").text("");
        $("#lb_dl_Customer").text("");
        $("#lb_dl_BookDebtName").text("");
        $("#lb_dl_LoanFromDate").text("");
        $("#lb_dl_LoanToDate").text("");
        $("#lb_dl_DPD").text("");
        $("#lb_dl_GroupProduct").text("");
        $("#lb_dl_LoanTotalMoneyDisbrusement").text("");
        $("#lb_dl_LoanTotalMoneyReceived").text("");
        //pt
        $("#lb_dl_TotalMoneyNeedPay").text("");
        $("#lb_dl_MoneyOriginal").text("");
        $("#lb_dl_MoneyInterest").text("");
        $("#lb_dl_MoneyOldDebit").text("");
        $("#lb_dl_TotalFee").text("");
        $("#lb_dl_MoneyConsultant").text("");
        $("#lb_dl_MoneyService").text("");
        $("#lb_dl_MoneyFineLate").text("");
        $("#lb_dl_MoneyFineOriginal").text("");
        //dk
        $("#lb_dl_dk_TotalMoneyNeedPay").text("");
        $("#lb_dl_dk_MoneyOriginal").text("");
        $("#lb_dl_dk_MoneyInterest").text("");
        $("#lb_dl_dk_MoneyOldDebit").text("");
        $("#lb_dl_dk_TotalFee").text("");
        $("#lb_dl_dk_MoneyConsultant").text("");
        $("#lb_dl_dk_MoneyService").text("");
        $("#lb_dl_dk_MoneyFineLate").text("");
        $("#lb_dl_dk_MoneyFineOriginal").text("");

        //dxm
        $("#lb_dl_dxm_TotalMoneyNeedPay").text("");
        $("#lb_dl_dxm_MoneyOriginal").text("");
        $("#lb_dl_dxm_MoneyInterest").text("");
        $("#lb_dl_dxm_MoneyOldDebit").text("");
        $("#lb_dl_dxm_TotalFee").text("");
        $("#lb_dl_dxm_MoneyConsultant").text("");
        $("#lb_dl_dxm_MoneyService").text("");
        $("#lb_dl_dxm_MoneyFineLate").text("");
        $("#lb_dl_dxm_MoneyFineOriginal").text("");

        //tlm
        $("#lb_dl_tlm_TotalMoneyNeedPay").text("");
        $("#lb_dl_tlm_MoneyOriginal").text("");
        $("#lb_dl_tlm_MoneyInterest").text("");
        $("#lb_dl_tlm_MoneyOldDebit").text("");
        $("#lb_dl_tlm_TotalFee").text("");
        $("#lb_dl_tlm_MoneyConsultant").text("");
        $("#lb_dl_tlm_MoneyService").text("");
        $("#lb_dl_tlm_MoneyFineLate").text("");
        $("#lb_dl_tlm_MoneyFineOriginal").text("");
    }
    var ShowDetail = function (jsonData = '') {
        resetShowDetail()
        var data = JSON.parse(jsonData);
        $("#lb_title").text("Thông tin hồ sơ MGL")
        baseCommon.AjaxDone("Get", baseCommon.Endpoint.GetDetailLoanExemptionByID + data.LoanExemptionID, "", function (respone) {
            if (respone.Result == 1) {
                var dataResult = respone.Data;
                LoanExemptionID = dataResult.LoanExemptionID;
                $("#lb_dl_Customer").text(dataResult.CustomerName);
                $("#lb_dl_LoanContractCode").text(dataResult.LoanContractCode);
                $("#lb_dl_BookDebtName").text(dataResult.BookDebtName);

                var dataFormDetail = dataResult.FormDetail;

                var dateLoanFromDate = moment(dataFormDetail.LoanFromDate);
                var loanFromDate = dateLoanFromDate.format("DD/MM/YYYY");
                $("#lb_dl_LoanFromDate").text(loanFromDate);

                var dateLoanToDate = moment(dataFormDetail.LoanToDate);
                var loanToDate = dateLoanToDate.format("DD/MM/YYYY");
                $("#lb_dl_LoanToDate").text(loanToDate);

                $("#lb_dl_DPD").text(dataFormDetail.CountDPD);
                $("#lb_dl_GroupProduct").text(dataFormDetail.LoanProductName);
                $("#lb_dl_LoanTotalMoneyDisbrusement").text(baseCommon.FormatCurrency(dataFormDetail.LoanTotalMoneyDisbrusement));
                $("#lb_dl_LoanTotalMoneyReceived").text(baseCommon.FormatCurrency(dataFormDetail.LoanTotalMoneyReceived));

                var moneyCloseLoan = dataFormDetail.MoneyCloseLoan;
                $("#lb_dl_TotalMoneyNeedPay").text(baseCommon.FormatCurrency(moneyCloseLoan.TotalMoneyNeedPay)); //tổng tiền phải trả (PT)
                $("#lb_dl_MoneyOriginal").text(baseCommon.FormatCurrency(moneyCloseLoan.MoneyOriginal));//tiền gốc PT
                $("#lb_dl_MoneyInterest").text(baseCommon.FormatCurrency(moneyCloseLoan.MoneyInterest));//tiền lãi PT
                $("#lb_dl_MoneyOldDebit").text(baseCommon.FormatCurrency(moneyCloseLoan.MoneyOldDebit));//nợ cũ PT
                $("#lb_dl_TotalFee").text(baseCommon.FormatCurrency(moneyCloseLoan.TotalFee));//phi PT
                $("#lb_dl_MoneyConsultant").text(baseCommon.FormatCurrency(moneyCloseLoan.MoneyConsultant));//phi tư vấn PT
                $("#lb_dl_MoneyService").text(baseCommon.FormatCurrency(moneyCloseLoan.MoneyService));//phi dịch vụ PT
                $("#lb_dl_MoneyFineLate").text(baseCommon.FormatCurrency(moneyCloseLoan.MoneyFineLate));//phi dịch vụ PT
                $("#lb_dl_MoneyFineOriginal").text(baseCommon.FormatCurrency(moneyCloseLoan.MoneyFineOriginal));//phi phạt tất tôans PT

                var moneyExpected = dataFormDetail.MoneyExpected;
                $("#lb_dl_dk_TotalMoneyNeedPay").text(baseCommon.FormatCurrency(moneyExpected.TotalMoneyNeedPay)); //tổng tiền phải trả (dk)
                $("#lb_dl_dk_MoneyOriginal").text(baseCommon.FormatCurrency(moneyExpected.MoneyOriginal));//tiền gốc dk
                $("#lb_dl_dk_MoneyInterest").text(baseCommon.FormatCurrency(moneyExpected.MoneyInterest));//tiền lãi dk
                $("#lb_dl_dk_MoneyOldDebit").text(baseCommon.FormatCurrency(moneyExpected.MoneyOldDebit));//nợ cũ dk
                $("#lb_dl_dk_TotalFee").text(baseCommon.FormatCurrency(moneyExpected.TotalFee));//phi dk
                $("#lb_dl_dk_MoneyConsultant").text(baseCommon.FormatCurrency(moneyExpected.MoneyConsultant));//phi tư vấn dk
                $("#lb_dl_dk_MoneyService").text(baseCommon.FormatCurrency(moneyExpected.MoneyService));//phi dịch vụ dk
                $("#lb_dl_dk_MoneyFineLate").text(baseCommon.FormatCurrency(moneyExpected.MoneyFineLate));//phi dịch vụ dk
                $("#lb_dl_dk_MoneyFineOriginal").text(baseCommon.FormatCurrency(moneyExpected.MoneyFineOriginal));//phi phạt tất tôans dk

                //dxm
                var moneySuggetExemption = dataFormDetail.MoneySuggetExemption;
                $("#lb_dl_dxm_TotalMoneyNeedPay").text(baseCommon.FormatCurrency(moneySuggetExemption.TotalMoneyNeedPay)); //tổng tiền phải trả dxm
                $("#lb_dl_dxm_MoneyOriginal").text(baseCommon.FormatCurrency(moneySuggetExemption.MoneyOriginal));//tiền gốc dxm
                $("#lb_dl_dxm_MoneyInterest").text(baseCommon.FormatCurrency(moneySuggetExemption.MoneyInterest));//tiền lãi dxm
                $("#lb_dl_dxm_MoneyOldDebit").text(baseCommon.FormatCurrency(moneySuggetExemption.MoneyOldDebit));//nợ cũ dxm
                $("#lb_dl_dxm_TotalFee").text(baseCommon.FormatCurrency(moneySuggetExemption.TotalFee));//phi dxm
                $("#lb_dl_dxm_MoneyConsultant").text(baseCommon.FormatCurrency(moneySuggetExemption.MoneyConsultant));//phi tư vấn dxm
                $("#lb_dl_dxm_MoneyService").text(baseCommon.FormatCurrency(moneySuggetExemption.MoneyService));//phi dịch vụ dxm
                $("#lb_dl_dxm_MoneyFineLate").text(baseCommon.FormatCurrency(moneySuggetExemption.MoneyFineLate));//phi dịch vụ dxm
                $("#lb_dl_dxm_MoneyFineOriginal").text(baseCommon.FormatCurrency(moneySuggetExemption.MoneyFineOriginal));//phi phạt tất tôans dxm

                //tlm
                var moneyPercent = dataFormDetail.MoneyPercent;
                /*             $("#lb_dl_tlm_TotalMoneyNeedPay").text(baseCommon.FormatCurrency(moneyPercent.TotalMoneyNeedPay)); //tổng tiền phải trả tlm*/
                $("#lb_dl_tlm_MoneyOriginal").text(baseCommon.FormatCurrency(moneyPercent.MoneyOriginal));//tiền gốc tlm
                $("#lb_dl_tlm_MoneyInterest").text(baseCommon.FormatCurrency(moneyPercent.MoneyInterest));//tiền lãi tlm
                $("#lb_dl_tlm_MoneyOldDebit").text(baseCommon.FormatCurrency(moneyPercent.MoneyOldDebit));//nợ cũ tlm
                /*$("#lb_dl_tlm_TotalFee").text(baseCommon.FormatCurrency(moneyPercent.TotalFee));//phi tlm*/
                $("#lb_dl_tlm_MoneyConsultant").text(baseCommon.FormatCurrency(moneyPercent.MoneyConsultant));//phi tư vấn tlm
                $("#lb_dl_tlm_MoneyService").text(baseCommon.FormatCurrency(moneyPercent.MoneyService));//phi dịch vụ tlm
                $("#lb_dl_tlm_MoneyFineLate").text(baseCommon.FormatCurrency(moneyPercent.MoneyFineLate));//phi dịch vụ tlm
                $("#lb_dl_tlm_MoneyFineOriginal").text(baseCommon.FormatCurrency(moneyPercent.MoneyFineOriginal));//phi phạt tất tôans tlm

                //$("#lb_CustomerName").text(dataResult.CustomerName);
                //$("#lb_LoanContractCode").text(dataResult.LoanContractCode);
                //$("#lb_CreateByFullName").text(dataResult.CreateByFullName);

                //$("#lb_LoanExemptionStatusName").text(dataResult.LoanExemptionStatusName);
                //$("#lb_BookDebtName").text(dataResult.BookDebtName);
                //var date = moment(dataResult.CreateDate);
                //var createDate = date.format("DD/MM/YYYY HH:mm:ss");
                //$("#lb_CreateDate").text(createDate);
                var dataIMG = dataResult.FileUploads;

                if (dataIMG.length > 0) {
                    var dataMGL = dataIMG.filter(x => x.TypeFile == 1);
                    if (dataMGL.length > 0) {
                        DataImagesMGL = dataMGL[0].FileImages;
                        ShowImgMGL()
                    }
                    var dataGiayTo = dataIMG.filter(x => x.TypeFile == 2);
                    if (dataGiayTo.length > 0) {
                        DataImagesGiayTo = dataGiayTo[0].FileImages;
                        ShowImgGiayTo()
                    }
                }
                $(".nav-link").removeClass("active");
                $(".tab-pane").removeClass("active");
                $("#tab_MGL").addClass("active");
                $("#TabMGLImg").addClass("active");

                $('#details_profile').modal('show');
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        });
        /*  $('#details_profile').modal('show');*/
    }
    var ShowHistory = function () {
        DataHistory();
    }
    var dataSourceHistory = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                baseCommon.AjaxDone('GET', baseCommon.Endpoint.GetHistoryLoanExemptionByLoanExemptionID + LoanExemptionID, "", function (res) {
                    if (res.Result == 1) {
                        options.success(res);
                        console.log(res)
                    } else {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = 0;
                if (response != null && typeof response.RecordsTotal != "undefined") {
                    total = response.RecordsTotal;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                var data = [];
                if (response != null && typeof response.Data != "undefined") {
                    data = response.Data;
                }
                return data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var DataHistory = function () {
        $("#dv_result_lichsu").kendoGrid({
            dataSource: dataSourceHistory,
            selectable: "row",
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 2) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'ActionTypeName',
                    title: 'Thao tác',
                },
                {
                    field: 'CreateByName',
                    title: 'Người thao tác',
                    width: 160,
                },
                {
                    field: 'CreateDate',
                    title: 'Thời gian thao tác',
                    textAlign: 'center',
                    template: function (row) {
                        if (row == 0 || row == null || row.CreateDate == null || row.CreateDate == "0001-01-01T00:00:00") {
                            return "";
                        }
                        var date = moment(row.CreateDate);
                        return date.format("DD/MM/YYYY HH:mm:ss");
                    }
                }
            ]
        });
    };

    function ShowImgMGL() {
        $("#div-lst-img-mgl").html("")
        if (DataImagesMGL != null && DataImagesMGL.length > 0) {
            $("#div-lst-img-mgl").append('<div id="detail_content_mgl" class="row div-lst-img" ></div>');
            var lstImg = DataImagesMGL;
            if (lstImg != null && lstImg.length > 0) {
                for (var k = 0; k < lstImg.length; k++) {
                    $("#detail_content_mgl").append('<div class="wapperImage col-md-3">\
                            <div class="photoHD">\
                                <img data-original="'+ lstImg[k].FullPath.replaceAll('\\', '/') + '" src="' + lstImg[k].FullPath.replaceAll('\\', '/') + '" width="300px" height="auto" />\
                             </div>\
                            <div class="div-remove">\
                                </div>\
                     </div >');
                }
            }
            $("#m_accordion_5_item_1_head_").css("background", "#c5b0b0");
            $("#detail_content_mgl").show();
        }
        if (viewerChungTu == null || typeof viewerChungTu == "undefined") {
            var options = {
                // inline: true,
                url: 'data-original'
            };
            viewerChungTu = new Viewer(document.getElementById('div-lst-img-mgl'), options);
        } else {
            viewerChungTu.update();
        }

    }

    function ShowImgGiayTo() {
        $("#div-lst-img-giayto").html("")
        if (DataImagesGiayTo != null && DataImagesGiayTo.length > 0) {
            $("#div-lst-img-giayto").append('<div id="detail_content" class="row div-lst-img-giayto" ></div>');
            var lstImg = DataImagesGiayTo;
            if (lstImg != null && lstImg.length > 0) {
                for (var k = 0; k < lstImg.length; k++) {
                    $("#detail_content").append('<div class="wapperImage col-md-3">\
                            <div class="photoHD">\
                                <img data-original="'+ lstImg[k].FullPath.replaceAll('\\', '/') + '" src="' + lstImg[k].FullPath.replaceAll('\\', '/') + '" width="300px" height="auto" />\
                             </div>\
                            <div class="div-remove">\
                                </div>\
                     </div >');
                }
            }
            $("#m_accordion_5_item_1_head_").css("background", "#c5b0b0");
            $("#detail_content").show();
        }
        if (viewerGiayTo == null || typeof viewerGiayTo == "undefined") {
            var options = {
                // inline: true,
                url: 'data-original'
            };
            viewerGiayTo = new Viewer(document.getElementById('div-lst-img-giayto'), options);
        } else {
            viewerGiayTo.update();
        }
    }

    var ProfileBrowsing = function (jsonData = '') {
        var data = JSON.parse(jsonData);
        swal.fire({
            html: "Bạn có chắc chẵn muốn duyệt MGL đơn vay <b>" + data.LoanContractCode + "<b>?",
            type: "warning",
            showCancelButton: !0,
            cancelButtonText: "Hủy",
            confirmButtonText: "Đồng ý"
        }).then(function (result) {
            if (result.value) {
                var request = {
                    "LoanExemptionID": parseInt(data.LoanExemptionID),
                    "Status": 2
                };
                baseCommon.AjaxDone("POST", baseCommon.Endpoint.UpdateStatusLoanExemption, JSON.stringify(request), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        setTimeout(() => { location.reload(); }, 200);
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                }, "Duyệt hồ sơ không thành công vui lòng thử lại!");
            }
        });
    };
    var ShowCannelProfile = function (jsonData = '') {
        var data = JSON.parse(jsonData);
        $("#txtTitleMGL").text(data.LoanContractCode);
        $("#hdd_LoanExemptionID").val(data.LoanExemptionID);
    }

    var CannelProfile = function () {
        $('#btn_CannelProfileMGL').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    txt_CannelProfileMGL_Reason: {
                        required: true
                    }
                },
                messages: {
                    txt_CannelProfileMGL_Reason: {
                        required: "Vui lòng nhập lý do hủy"
                    }
                }
            });
            if (!form.valid()) {
                return;
            }
            var reason = $("#txt_CannelProfileMGL_Reason").val();
            var request = {
                LoanExemptionID: parseInt($("#hdd_LoanExemptionID").val()),
                Status: 1,
                ReasonCancel: reason
            }
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            baseCommon.AjaxDone("POST", baseCommon.Endpoint.UpdateStatusLoanExemption, JSON.stringify(request), function (respone) {
                if (respone.Result == 1) {
                    baseCommon.ShowSuccess(respone.Message);
                    setTimeout(() => { location.reload(); }, 200);
                    $('#model_cannelProfileMGL').modal('hide');
                } else {
                    baseCommon.ShowErrorNotLoad(respone.Message);
                }
            });
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
        });
    };

    var UpdateProfile = function (jsonData = '') {
        var data = JSON.parse(jsonData);
        var title = "Thông tin hồ sơ MGL hợp đồng " + data.LoanContractCode;
        $("#lb_update_profile_title").text(title);
        LoanExemptionID = data.LoanExemptionID;
        $("#hd_Profile_LoanExemptionID").val(data.LoanExemptionID);
        $("#lb_update_CustomerName").text(data.CustomerName);
        $("#lb_update_LoanContractCode").text(data.LoanContractCode);
        $("#lb_update_CreateByFullName").text(data.CreateByFullName);
        var date = moment(data.CreateDate);
        var createDate = date.format("DD/MM/YYYY HH:mm:ss");
        $("#lb_update_CreateDate").text(createDate);
        $('#details_update_profile').modal('show');
    }
    var UploadImageLos_GiayTo = function () {
        $('#dvUpload_GiayTo').html('');

        var countFile = 0;
        var ListFile = [];
        var strUpload = '<input type="hidden" id="hdd_file_upload_GiayTo" name="FileName" />\
            <div class="dropzone dropzone-default" id="dropzone" action="/Content/Index">\
                <div class="dropzone-msg dz-message needsclick">\
                    <h3 class="dropzone-msg-title">Thả tệp vào đây hoặc nhấp để tải lên</h3>\
                    <span class="dropzone-msg-desc">Chỉ cho phép tải lên hình ảnh</span>\
                </div>\
            </div>';
        $("div#dvUpload_GiayTo").html(strUpload);
        Dropzone.prototype.defaultOptions.dictDefaultMessage = "";
        var dropzoneOptions = {
            maxFiles: 10,
            maxFilesize: 10, // MB
            addRemoveLinks: true,
            acceptedFiles: "image/*",
            clickable: true,
            uploadMultiple: true,
            parallelUploads: 10,
            autoProcessQueue: false,
        };
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("div#dropzone", dropzoneOptions);
        myDropzone.on("addedfile", function (file) {
            countFile += 1;
            ListFile.push(file);
            var formData = new FormData();
            formData.append("files", file);
            //for (var i = 0; i < ListFile.length; i++) {
            //    formData.append("files", ListFile[i]);
            //}
            UploadImg(formData, 2, file.name)

        });
        myDropzone.on("removedfile", function (file) {
            const index = ListFile.indexOf(file);
            if (index > -1) {
                ListFile.splice(index, 1);
            }
            countFile -= 1;
            var data = ListDeleteIMG.filter(x => x.NameFile == file.name);
            var objDelete = data[0].Data[0];
            DeleteIMG(objDelete)
        });
        var createButtonViewImage = function (urlImage, ImageID, fileName) {
            var div = document.createElement('div');
            div.setAttribute('id', "inputGroup" + ImageID);
            div.setAttribute('class', 'kt-btn-group kt-btn-group--pill btn-group btn-group-sm kt-margin-top  kt--margin-5');
            var a = document.createElement('a');
            a.setAttribute('id', "btnViewDetail" + ImageID);
            a.setAttribute('href', urlImage);
            a.setAttribute('data-lightbox', "roadtrip");
            a.setAttribute('class', 'kt-btn btn btn-secondary');
            a.setAttribute('title', fileName);
            a.innerHTML = '<i class="fa fa-search-plus"></i>';
            div.append(a);
            return div;
        }
    };
    var UploadImageLos_MGL = function () {
        $('#dvUpload_MGL').html('');
        var countFile = 0;
        var ListFile = [];
        var strUpload = '<input type="hidden" id="hdd_file_upload_MGL" name="FileName" />\
            <div class="dropzone dropzone-default" id="dropzone" action="/Content/Index">\
                <div class="dropzone-msg dz-message needsclick">\
                    <h3 class="dropzone-msg-title">Thả tệp vào đây hoặc nhấp để tải lên</h3>\
                    <span class="dropzone-msg-desc">Chỉ cho phép tải lên hình ảnh</span>\
                </div>\
            </div>';
        $("div#dvUpload_MGL").html(strUpload);
        Dropzone.prototype.defaultOptions.dictDefaultMessage = "";
        var dropzoneOptions = {
            maxFiles: 10,
            maxFilesize: 10, // MB
            addRemoveLinks: true,
            acceptedFiles: "image/*",
            clickable: true,
            uploadMultiple: true,
            parallelUploads: 10,
            autoProcessQueue: false,
        };
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("div#dropzone", dropzoneOptions);
        myDropzone.on("addedfile", function (file) {
            countFile += 1;
            /*           ListFile.push(file);*/
            var formData = new FormData();
            formData.append("files", file);
            UploadImg(formData, 1, file.name)

        });
        myDropzone.on("removedfile", function (file) {
            const index = ListFile.indexOf(file);
            if (index > -1) {
                ListFile.splice(index, 1);
            }
            countFile -= 1;
            var data = ListDeleteIMG.filter(x => x.NameFile == file.name);
            var objDelete = data[0].Data[0];
            DeleteIMG(objDelete)
        });
        var createButtonViewImage = function (urlImage, ImageID, fileName) {
            var div = document.createElement('div');
            div.setAttribute('id', "inputGroup" + ImageID);
            div.setAttribute('class', 'kt-btn-group kt-btn-group--pill btn-group btn-group-sm kt-margin-top  kt--margin-5');
            var a = document.createElement('a');
            a.setAttribute('id', "btnViewDetail" + ImageID);
            a.setAttribute('href', urlImage);
            a.setAttribute('data-lightbox', "roadtrip");
            a.setAttribute('class', 'kt-btn btn btn-secondary');
            a.setAttribute('title', fileName);
            a.innerHTML = '<i class="fa fa-search-plus"></i>';
            div.append(a);
            return div;
        }
    };
    var UploadImg = function (formData, typeFile, nameFile) {
        $.ajax({
            url: baseCommon.Endpoint.UploadImageLos,
            data: formData,
            type: 'POST',
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (responeParent) {
                if (responeParent.result == 1) {
                    var lstDelete = {
                        NameFile: nameFile,
                        Data: responeParent.data
                    }
                    //add ảnh có thể xóa
                    ListDeleteIMG.push(lstDelete);
                    console.log(ListDeleteIMG)
                    //add ảnh đã đẩy los 
                    if (typeFile == 1) {
                        RequestUpload_MGL.push(responeParent.data[0]);
                    } else {
                        RequestUpload_GiayTo.push(responeParent.data[0]);
                    }
                } else {
                    baseCommon.ShowErrorNotLoad("Lỗi chưa upload được ảnh lên hệ thống");
                }
            },
        });
    }

    var DeleteIMG = function (objData) {
        $.ajax({
            url: baseCommon.Endpoint.DeleteImage,
            data: { FifleName: objData.fileName },
            type: 'POST',
            success: function (responeParent) {
                if (responeParent.result == 1) {
                } else {
                }
            },
        });
    }
    var SaveImgProfile = function () {
        $('#btn_updateProfile').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
            });
            if (!form.valid()) {
                return;
            }
            var loanExemptionID = $("#hd_Profile_LoanExemptionID").val()
            if (loanExemptionID == null || loanExemptionID == 0) {
                baseCommon.ShowErrorNotLoad("Lỗi không tồn tại hồ sơ MGL");
                return false;
            }
            var fileUpload = [];
            if (RequestUpload_MGL.length > 0) {
                var fileImages_MGL = {
                    TypeFile: 1,
                    FileImages: RequestUpload_MGL
                }
                fileUpload.push(fileImages_MGL)
            } else {
                baseCommon.ShowErrorNotLoad("Chưa upload thông tin chứng từ MGL");
                return false;
            }
            if (RequestUpload_GiayTo.length > 0) {
                var fileImages_GiayTo = {
                    TypeFile: 2,
                    FileImages: RequestUpload_GiayTo
                }
                fileUpload.push(fileImages_GiayTo)
            }
            if (fileUpload.length <= 0) {
                baseCommon.ShowErrorNotLoad("Bạn chưa upload thông tin chứng từ");
                return false;
            }
            console.log(fileUpload)
            var request = {
                LoanExemptionID: parseInt(loanExemptionID),
                CreateBy: USER_ID,
                FileUploads: fileUpload
            }
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            baseCommon.AjaxDone("POST", baseCommon.Endpoint.Uploadfileloanexemption, JSON.stringify(request), function (respone) {
                if (respone.Result == 1) {
                    ListDeleteIMG = [];
                    baseCommon.ShowSuccess(respone.Message);
                    $('#model_update_profile').modal('hide');
                    $('#btnGetData').trigger('click');
                    setTimeout(() => { location.reload(); }, 200);
                } else {
                    baseCommon.ShowErrorNotLoad(respone.Message);
                }
            });
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
        });
    };

    var Init = function () {
        SaveImgProfile();
        setFromDateToDate();
        setTimeout(function () {
            InitGrid();
        }, 1300);
        $("#btnGetData").on('click', function (e) {
            GridRefresh();
        });
        $("#fromDate,#toDate,#sl_reportId,#sl_statusId").on('change', function (e) {
            GridRefresh();
        });
        UploadImageLos_GiayTo();
        UploadImageLos_MGL();
        RequestUpload_GiayTo = [];
        RequestUpload_MGL = [];
        ListDeleteIMG = [];
        //xóa hết ảnh đã đẩy lên
        $('#model_update_profile').on('hidden.bs.modal', function () {
            if (ListDeleteIMG.length > 0) {
                for (var i = 0; i < ListDeleteIMG.length; i++) {
                    var objDataDelete = ListDeleteIMG[i];
                    var objData = objDataDelete.Data[0];
                    DeleteIMG(objData)
                    setTimeout(() => { location.reload(); }, 200);
                }
            }
        })

    };
    var setFromDateToDate = function () {
        var date = new Date();
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        $("#fromDate").datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            language: "vi",
        }).datepicker("setDate", getFormattedDate(firstDay));
        $("#toDate").datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            language: "vi",
        }).datepicker("setDate", getFormattedDate(date));
    }
    function formatCurrency(num) {
        num = num.toString().replace(/\$|\,/g, '');
        if (isNaN(num))
            num = "0";
        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        cents = num % 100;
        num = Math.floor(num / 100).toString();
        if (cents < 10)
            cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
            num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                num.substring(num.length - (4 * i + 3));
        return (((sign) ? '' : '-') + num);
    }
    return {
        Init: Init,
        RowClick: RowClick,
        Option: Option,
        GridRefresh: GridRefresh,
        ProfileBrowsing: ProfileBrowsing,
        CannelProfile: CannelProfile,
        ShowCannelProfile: ShowCannelProfile,
        ShowDetail: ShowDetail,
        UpdateProfile: UpdateProfile,
        UploadImageLos_GiayTo: UploadImageLos_GiayTo,
        UploadImageLos_MGL: UploadImageLos_MGL,
        SaveImgProfile: SaveImgProfile,
        ShowHistory: ShowHistory
    };
}

