﻿var configUser = function () {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var approvalLevelBookDebtID = $('#sl_search_ApprovalLevelBookDebtID').val();
                if (approvalLevelBookDebtID == null || approvalLevelBookDebtID == "") {
                    approvalLevelBookDebtID = -111
                }
                var userName = $('#txt_search_username').val();
                var data = {
                    ApprovalLevelBookDebtID: parseInt(approvalLevelBookDebtID),
                    UserName: userName,
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize,
                }
                baseCommon.AjaxDone('POST', baseCommon.Endpoint.GetUserApprovalLevelConditions, JSON.stringify(data), function (res) {
                    options.success([]);
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    options.success(res);
                    if (res.Result == 1) {
                    } else {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                return response.Total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                var data = [];
                if (response != null && typeof response.Data != "undefined" && response.Data != null) {
                    data = response.Data;
                }
                return data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function () {

        $("#dv_result_user_config").kendoGrid({
            dataSource: dataSource,
            selectable: "row",
            pageable: {
                pageSizes: [20, 30, 50],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {

            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #",
                    width: 100
                },
                {
                    field: 'UserName',
                    title: 'Tài khoản',
                },
                {
                    field: 'ApprovalLevelBookName',
                    title: 'Cấp phê duyệt',
                    template: function (row) {
                        if (row.ApprovalLevelBookDebtID == 0) {
                            return "Tạo MGLP";
                        }
                        return row.ApprovalLevelBookName;
                    }
                },
                {
                    field: 'MoveApprovalLevelBookName',
                    title: 'Đẩy lên',
                },
                {
                    field: 'CreateDate',
                    title: 'Ngày tạo',
                    textAlign: 'center',
                    template: function (row) {
                        if (row == 0 || row == null || row.CreateDate == null || row.CreateDate == "0001-01-01T00:00:00") {
                            return "";
                        }
                        var date = moment(row.CreateDate);
                        return date.format("DD/MM/YYYY HH:mm:ss");
                    }
                },
                {
                    field: 'Status',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    template: function (row) {
                        if (row.Status == 1) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Hoạt Động</span>';
                        } else if (row.Status == 0) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Khóa</span>';
                        }
                    },
                },
                {
                    title: 'Chức năng',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += ' <a class="btn btn-warning btn-icon btn-sm" title="Cập nhật book nợ" data-toggle="modal" data-target="#model_create_config_user"  onclick="configUser.loadInfoModal(\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                                      <i class="fa fa-edit"></i>\
                                     </a> ';
                        return html;
                    }
                }
            ]
        });
    };
    var GridRefresh = function () {
        var grid = $("#dv_result_user_config").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };
    var loadInfoModal = function (jsonData = '') {
        if (jsonData != '') {
            var data = JSON.parse(jsonData);

            if (data.ApprovalLevelBookDebtID == 0) {
                $('#ck_create_mglp').prop('checked', true).trigger('change');
            } else {
                $('#ck_create_mglp').prop('checked', false).trigger('change');
            }
            $("#hdd_create_userApprovalLevelID").val(data.UserApprovalLevelId);
            $('#sl_create_approvalLevelBookDebtID').val(data.ApprovalLevelBookDebtID).change();
            setTimeout(function () {
                $('#sl_money_moveapprovalLevelBookDebtID').val(data.MoveApprovalLevelBookDebtID).change();
            }, 1000);
            $("input[name=txt_create_Status]").removeAttr("checked");
            $("#txt_create_username").val(data.UserName);
            data.Status = data.Status == null ? 0 : data.Status;
            $("input[name=txt_create_Status][value=" + data.Status + "]").attr('checked', 'checked');
        } else {
            $("#hdd_create_userApprovalLevelID").val(0);
            $('#sl_create_approvalLevelBookDebtID').val('').change();
            $('#sl_money_moveapprovalLevelBookDebtID').val('').change();
            $("input[name=txt_create_Status]").removeAttr("checked");
            $("#txt_create_username").val('');
            $('#ck_create_mglp').prop('checked', false).trigger('change');
            $("input[name=txt_create_Status][value=1]").attr('checked', 'checked');
        }

    };
    var saveData = function () {
        var userApprovalLevelID = $("#hdd_create_userApprovalLevelID").val();
        var approvalLevelBookDebtID = $("#sl_create_approvalLevelBookDebtID").val();
        var moveApprovalLevelBookDebtID = $("#sl_money_moveapprovalLevelBookDebtID").val();
        var username = $("#txt_create_username").val();
        var status = parseInt($("input[name='txt_create_Status']:checked").val());

        var isCreate = $('#ck_create_mglp').is(":checked")
        if (isCreate) {
            approvalLevelBookDebtID = 0;
        }
        if (approvalLevelBookDebtID == -111 || (isCreate == false && approvalLevelBookDebtID == 0)) {
            baseCommon.ShowErrorNotLoad("Vui lòng chọn cấp phê duyệt");
            return;
        }
        if (username == '' || username <= 0) {
            baseCommon.ShowErrorNotLoad("Vui lòng nhập tên tài khoản");
            return;
        }
        if (approvalLevelBookDebtID == 0 && (moveApprovalLevelBookDebtID == -111 || moveApprovalLevelBookDebtID == 0 || moveApprovalLevelBookDebtID == null)) {
            baseCommon.ShowErrorNotLoad("Vui lòng chọn cấp phê duyệt bạn chuyển hđ lên");
            return;
        }
        if (approvalLevelBookDebtID != 0) {
            moveApprovalLevelBookDebtID = 0;
        }
        var dataRequest = {
            "UserApprovalLevelID": parseInt(userApprovalLevelID),
            "ApprovalLevelBookDebtID": parseInt(approvalLevelBookDebtID),
            "MoveApprovalLevelBookDebtID": parseInt(moveApprovalLevelBookDebtID),
            "Status": status,
            "UserName": username,
        };
        var btn = $('#btn_save_userSetting');
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.CreateOrUpdateUserAppovalLevel, JSON.stringify(dataRequest), function (respone) {
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                $('#model_create_config_user').modal('hide');
                GridRefresh();
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            }
        });
    };
    var init = function () {
        setTimeout(function () {
            InitGrid();
        }, 1300);
        $("#btnGetData").on('click', function (e) {
            GridRefresh();
        });
        $("#sl_search_ApprovalLevelBookDebtID").on('change', function (e) {
            GridRefresh();
        });
        $("#ck_create_mglp").on('change', function (e) {
            var approvalLevelBookDebtID = 1;
            var isCreate = $('#ck_create_mglp').is(":checked");
            if (isCreate) {
                approvalLevelBookDebtID = 0;
            }
            if (approvalLevelBookDebtID == 0) {
                $('#hdd_movetoApproval').show();
                $('#hdd_Approval').hide();
            } else {
                $('#hdd_movetoApproval').hide();
                $('#hdd_Approval').show();
            }
        });
    };
    return {
        init: init,
        saveData: saveData,
        loadInfoModal: loadInfoModal
    }
}();