﻿var settingMGLP = new function () {
    var loadData = function () {
        var bookDebtChildViews = [];
        var approvalLevelBookDebtChildViews = [];
        var settingInterestReductionListViews = [];
        var tableData = '';
        var footerTable = '';
        $('#dv_result_setting').html('');
        var BookDebtID = $('#sl_search_BookDebtID').val();
        var ApprovalLevelBookDebtID = $('#sl_search_ApprovalLevelBookDebtID').val();
        var requestParam = {
            "BookDebtID": parseInt(BookDebtID),
            "ApprovalLevelBookDebtID": parseInt(ApprovalLevelBookDebtID)
        };
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.GetSettingByConditions, JSON.stringify(requestParam), function (respone) {
            if (respone.Result == 1) {
                bookDebtChildViews = respone.Data.BookDebtChildViews;
                approvalLevelBookDebtChildViews = respone.Data.ApprovalLevelBookDebtChildViews;
                settingInterestReductionListViews = respone.Data.SettingInterestReductionListViews;
                settingTotalMoneyViews = respone.Data.SettingTotalMoneyViews;
                tableData += '<table class="table table-bordered table-hover">';
                tableData += `<thead style="background: #5578eb;color: white">
                                    <tr>
                                        <th rowspan="2">BOOK NỢ</th>
                                        <th rowspan="2" style="text-align:left">CẤU PHẦN NỢ ĐƯỢC MIỄN GIẢM</th>
                                        <th colspan="${approvalLevelBookDebtChildViews.length}">CẤP PHÊ DUYỆT</th>
                                    </tr>`;
                tableData += `<tr>`
                footerTable += '<tr style="background: #c9c975;"><td></td><td><b>TỔNG TIỀN GIỚI HẠN</b></td>';
                $.each(approvalLevelBookDebtChildViews, function (index, approvalLevelBookDebt) {
                    tableData += `<th>${approvalLevelBookDebt.ApprovalLevelBookDebtName}</th>`;
                    var settingMoneys = $.grep(settingTotalMoneyViews, function (p) { return p.ApprovalLevelBookDebtID == approvalLevelBookDebt.ApprovalLevelBookDebtID });
                    if (settingMoneys.length > 0) {
                        $.each(settingMoneys, function (idx, childData) {
                            footerTable += `<td><b>${formatCurrency(childData.TotalMoney)}</b></td >`;
                        });

                    } else {
                        footerTable += '<td>Chưa cấu hình</td>';
                    }
                });
                footerTable += '</tr>';
                tableData += `</tr></thead > `;
                tableData += ` <tbody>`;
                $.each(bookDebtChildViews, function (indexbookDebtChild, bookDebtChild) {
                    tableData += `<tr>`;
                    var countDataBookDebtTotalList = $.grep(settingInterestReductionListViews, function (p) { return p.BookDebtID == bookDebtChild.BookDebtID });
                    var countDataBookDebt = [];
                    $.each(countDataBookDebtTotalList, function (indexA, dataBookDebt) {
                        var requestParam = {
                            "BookDebtName": dataBookDebt.dataBookDebt,
                            "StrMoneyType": dataBookDebt.StrMoneyType,
                            "MoneyType": dataBookDebt.MoneyType,
                            "Note": dataBookDebt.Note
                        };
                        if (countDataBookDebt.filter(vendor => vendor['MoneyType'] === requestParam.MoneyType) <= 0) {
                            countDataBookDebt.push(dataBookDebt);
                        }
                    });
                    tableData += `  <td style="vertical-align:middle" rowspan="${countDataBookDebt.length == 0 ? 1 : countDataBookDebt.length}" >${bookDebtChild.BookDebtName}</td>`;
                    $.each(countDataBookDebt, function (indexcountDataBookDebt, countDataBookDebtData) {
                        tableData += `<td class="kt-align-left" >${countDataBookDebtData.StrMoneyType}</td>`;
                        $.each(approvalLevelBookDebtChildViews, function (indexApprovalLevelBookDebt, approvalLevelBookDebt) {
                            var dataMoney = $.grep(settingInterestReductionListViews, function (p) { return p.BookDebtID == bookDebtChild.BookDebtID && p.ApprovalLevelBookDebtID == approvalLevelBookDebt.ApprovalLevelBookDebtID && p.MoneyType == countDataBookDebtData.MoneyType });
                            if (dataMoney.length > 0) {
                                $.each(dataMoney, function (idx, childData) {
                                    tableData += `<td class="kt-align-center">${childData.Note}</td>`;
                                });
                            } else {
                                tableData += `<td class="kt-align-center"></td>`;
                            }
                        });
                        tableData += `</tr>`;
                    });
                });
                tableData += footerTable;
                tableData += `</tbody>`;
                tableData += '</table>';

                $('#dv_result_setting').append(tableData);
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        });
    };
    var loadDataSetting = function () {
        var BookDebtID = $('#sl_setting_bookDebtID').val();
        var ApprovalLevelBookDebtID = $('#sl_setting_ApprovalLevelBookDebtID').val();
        if (BookDebtID == -111 && ApprovalLevelBookDebtID == -111) {
            $("#sl_setting_applyTo").val('');
            $("#sl_setting_applyFrom").val('');
            $("#sl_setting_dpdFrom").val('');
            $("#sl_setting_dpdTo").val('');

            $('#create_consultant_percent').val('');
            $('#create_service_percent').val('');
            $('#create_interestLate_percent').val('');
            $('#create_paybefore_percent').val('');
            $('#create_debt_percent').val('');

            $('#create_consultant_totalMoney').val('');
            $('#create_service_totalMoney').val('');
            $('#create_interestLate_totalMoney').val('');
            $('#create_paybefore_totalMoney').val('');
            $('#create_debt_totalMoney').val('');
        }
        if (BookDebtID == -111 || ApprovalLevelBookDebtID == -111) {
            return;
        }
        var requestParam = {
            "BookDebtID": parseInt(BookDebtID),
            "ApprovalLevelBookDebtID": parseInt(ApprovalLevelBookDebtID)
        };
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.GetSettingByBookDebtID, JSON.stringify(requestParam), function (respone) {
            if (respone.Result == 1) {
                var data = respone.Data;
                //$("#sl_setting_applyTo").val(data.ApplyTo);
                $("#sl_setting_applyTo").val(data.YearDebt);
                $("#sl_setting_dpdFrom").val(data.DPDFrom < -1000 ? "" : formatNumber(data.DPDFrom));
                $("#sl_setting_dpdTo").val(data.DPDTo > 10000 ? "" : formatNumber(data.DPDTo));

                $('#create_consultant_percent').val(data.ConsultantPercent);
                $('#create_service_percent').val(data.ServicePercent);
                $('#create_interestLate_percent').val(data.InterestLatePercent);
                $('#create_paybefore_percent').val(data.PaybeforePercent);
                $('#create_debt_percent').val(data.DebtPercent);

                $('#create_consultant_totalMoney').val(formatCurrency(data.ConsultantTotalMoney));
                $('#create_service_totalMoney').val(formatCurrency(data.ServiceTotalMoney));
                $('#create_interestLate_totalMoney').val(formatCurrency(data.InterestLateTotalMoney));
                $('#create_paybefore_totalMoney').val(formatCurrency(data.PaybeforeTotalMoney));
                $('#create_debt_totalMoney').val(formatCurrency(data.DebtTotalMoney));
            }
        });
    }

    var loadDataSettingMoney = function () {
        var ApprovalLevelBookDebtID = $('#sl_money_approvalLevelBookDebtID').val();
        var checkedData = [3, 4, 5, 7, 9];
        if (ApprovalLevelBookDebtID == -111) {
            $('#txt_money_totalMax').val(0);
            $('[name="ck_typeMoney"]').each(function () {
                if (checkedData.find(x => x == $(this).val()) > 0) {
                    $(this).prop('checked', true);
                } else {
                    $(this).prop('checked', false);
                }
            });
        }
        if (ApprovalLevelBookDebtID == -111) {
            return;
        }
        var requestParam = {
            "ApprovalLevelBookDebtID": parseInt(ApprovalLevelBookDebtID)
        };
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.GetSettingMoneyByApprovalLevelBookDebtID, JSON.stringify(requestParam), function (respone) {
            if (respone.Result == 1) {
                var data = respone.Data;
                $("#txt_money_totalMax").val(formatCurrency(data.TotalMoney));
                if (data.ListTypeMoney == null) {
                    checkedData = [3, 4, 5, 7, 9];
                } else {
                    checkedData = jQuery.parseJSON(data.ListTypeMoney);
                }
                $('[name="ck_typeMoney"]').each(function () {
                    if (checkedData.find(x => x == $(this).val()) > 0) {
                        $(this).prop('checked', true);
                    } else {
                        $(this).prop('checked', false);
                    }
                });
            }

        });
    }
    var init = function () {
        $('#sl_setting_bookDebtID , #sl_setting_ApprovalLevelBookDebtID').on('change', function () {
            loadDataSetting();
        });
        $('#sl_search_BookDebtID , #sl_search_ApprovalLevelBookDebtID').on('change', function () {
            loadData();
        });
        $('#sl_money_approvalLevelBookDebtID').on('change', function () {
            loadDataSettingMoney();
        });
    }
    var saveSetting = function () {
        var bookDebtID = $("#sl_setting_bookDebtID").val();
        var approvalLevelBookDebtID = $("#sl_setting_ApprovalLevelBookDebtID").val();
        //var applyTo = $("#sl_setting_applyTo").val();
        //var applyFrom = $("#sl_setting_applyFrom").val();
        //var dpdFrom = $("#sl_setting_dpdFrom").val() == "" ? -10000 : parseInt(formatTextMoneyToNumber($("#sl_setting_dpdFrom").val()));
        //var dpdTo = $("#sl_setting_dpdTo").val() == "" ? 100000 : parseInt(formatTextMoneyToNumber($("#sl_setting_dpdTo").val()));
        var consultant_percent = $('#create_consultant_percent').val() == "" ? 0 : parseFloat($('#create_consultant_percent').val().replace(/,/g, ''));
        var service_percent = $('#create_service_percent').val() == "" ? 0 : parseFloat($('#create_service_percent').val().replace(/,/g, ''));
        var interestLate_percent = $('#create_interestLate_percent').val() == "" ? 0 : parseFloat($('#create_interestLate_percent').val().replace(/,/g, ''));
        var paybefore_percent = $('#create_paybefore_percent').val() == "" ? 0 : parseFloat($('#create_paybefore_percent').val().replace(/,/g, ''));
        var debt_percent = $('#create_debt_percent').val() == "" ? 0 : parseFloat($('#create_debt_percent').val().replace(/,/g, ''));

        var consultant_TotalMoney = $('#create_consultant_totalMoney').val() == "" ? 0 : parseInt(formatTextMoneyToNumber($('#create_consultant_totalMoney').val()));
        var service_TotalMoney = $('#create_service_totalMoney').val() == "" ? 0 : parseInt(formatTextMoneyToNumber($('#create_service_totalMoney').val()));
        var interestLate_totalMoney = $('#create_interestLate_totalMoney').val() == "" ? 0 : parseInt(formatTextMoneyToNumber($('#create_interestLate_totalMoney').val()));
        var paybefore_totalMoney = $('#create_paybefore_totalMoney').val() == "" ? 0 : parseInt(formatTextMoneyToNumber($('#create_paybefore_totalMoney').val()));
        var debt_totalMoney = $('#create_debt_totalMoney').val() == "" ? 0 : parseInt(formatTextMoneyToNumber($('#create_debt_totalMoney').val()));


        if (bookDebtID == -111 || bookDebtID == "") {
            baseCommon.ShowErrorNotLoad("Vui lòng chọn book nợ");
            return;
        }
        if (approvalLevelBookDebtID == -111 || approvalLevelBookDebtID == "") {
            baseCommon.ShowErrorNotLoad("Vui lòng chọn cấp phê duyệt");
            return;
        }
        //if (applyFrom == "" || applyTo == "") {
        //    baseCommon.ShowErrorNotLoad("Vui lòng chọn ngày Giải ngân ");
        //    return;
        //}
        if (consultant_percent > 100 || consultant_percent < 0) {
            baseCommon.ShowErrorNotLoad("Vui lòng nhập % tiền tư vấn <= 100 %");
            return;
        }
        if (service_percent > 100 || service_percent < 0) {
            baseCommon.ShowErrorNotLoad("Vui lòng nhập % tiền dịch vụ <= 100 %");
            return;
        }

        if (interestLate_percent > 100 || interestLate_percent < 0) {
            baseCommon.ShowErrorNotLoad("Vui lòng nhập % Phí phạt chậm trả <= 100 %");
            return;
        }

        if (paybefore_percent > 100 || paybefore_percent < 0) {
            baseCommon.ShowErrorNotLoad("Vui lòng nhập % Phí tất toán trước hạn <= 100 %");
            return;
        }

        if (debt_percent > 100 || debt_percent < 0) {
            baseCommon.ShowErrorNotLoad("Vui lòng nhập % nợ <= 100 %");
            return;
        }
        var btn = $('#btn_saveSetting');
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        var Consultant = 4; Service = 3; FineLate = 5; FineOriginal = 7; Debt = 9;
        var lstListMoney = [Consultant, Service, FineLate, FineOriginal, Debt];
        var dataRequest = [];

        for (var i = 0; i < lstListMoney.length; i++) {
            var dataPush = {
                "BookDebtID": parseInt(bookDebtID),
                "ApprovalLevelBookDebtID": parseInt(approvalLevelBookDebtID),
                //"ApplyTo": applyTo,
                //"ApplyFrom": applyFrom,
                //"DPDFrom": dpdFrom,
                //"DPDTo": dpdTo,
                "MoneyType": 0,
                "PercentMoneyReduce": 0,
                "MaxMoneyReduce": 0
            };
            dataPush.MoneyType = lstListMoney[i];
            if (lstListMoney[i] == Consultant) {
                dataPush.PercentMoneyReduce = consultant_percent;
                dataPush.MaxMoneyReduce = consultant_TotalMoney;
                dataRequest.push(dataPush);
            } else if (lstListMoney[i] == Service) {
                dataPush.PercentMoneyReduce = service_percent;
                dataPush.MaxMoneyReduce = service_TotalMoney;
                dataRequest.push(dataPush);
            } else if (lstListMoney[i] == FineLate) {
                dataPush.PercentMoneyReduce = interestLate_percent;
                dataPush.MaxMoneyReduce = interestLate_totalMoney;
                dataRequest.push(dataPush);
            } else if (lstListMoney[i] == FineOriginal) {
                dataPush.PercentMoneyReduce = paybefore_percent;
                dataPush.MaxMoneyReduce = paybefore_totalMoney;
                dataRequest.push(dataPush);
            } else if (lstListMoney[i] == Debt) {
                dataPush.PercentMoneyReduce = debt_percent;
                dataPush.MaxMoneyReduce = debt_totalMoney;
                dataRequest.push(dataPush);
            }
        }
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.CreateSettingInterestReduction, JSON.stringify(dataRequest), function (respone) {
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                $('#model_createSetting').modal('hide');
                loadData();
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            }
        });
    }


    var saveSettingMoney = function () {
        var approvalLevelBookDebtID = $("#sl_money_approvalLevelBookDebtID").val();
        var totalMax = $("#txt_money_totalMax").val();
        if (approvalLevelBookDebtID == -111 || approvalLevelBookDebtID == "") {
            baseCommon.ShowErrorNotLoad("Vui lòng chọn cấp phê duyệt");
            return;
        }
        if (totalMax == '' || totalMax <= 0) {
            baseCommon.ShowErrorNotLoad("Vui lòng nhập số tiền hàng tháng ");
            return;
        }
        var ListTypeMoney = [];
        $('[name="ck_typeMoney"]:checked').each(function () {
            ListTypeMoney.push(parseInt($(this).val()));
        });
        var dataRequest = {
            "ApprovalLevelBookDebtID": parseInt(approvalLevelBookDebtID),
            "TotalMoney": parseInt(formatTextMoneyToNumber(totalMax)),
            "ListTypeMoney": JSON.stringify(ListTypeMoney)
        };
        var btn = $('#btn_saveSetting_totalMoney');
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.CreateSettingTotalMoney, JSON.stringify(dataRequest), function (respone) {
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                $('#model_createSetting_totalMoney').modal('hide');
                loadData();
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            }
        });
    }
    return {
        loadData: loadData,
        saveSetting: saveSetting,
        init: init,
        saveSettingMoney: saveSettingMoney,
    }
}();
