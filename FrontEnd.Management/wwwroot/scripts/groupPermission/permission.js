﻿var groupPermission = function () {
    var loadGroupActive = function () {
        $.ajax({
            type: "POST",
            url: "/Group/GetAll",
        }).done(function (respone) {
            $('#sl_search_group').html('');
            $('#sl_search_group').append('<option value="">Chọn nhóm</option>');
            $.each(respone.data, function (key, item) { 
                $('#sl_search_group').append('<option value="' + item.groupID + '">' + item.groupName + '</option>');
            });
        });
    };
    init = function () {
        $('#sl_search_group').on('change', function () {
            if (this.value == 0) {
                $("#kt_tree_permission").jstree('destroy');
            } else {
                getPermissionStaff(this.value);
            }
        });
    };
    var getPermissionStaff = function (GroupID) {
        if (GroupID > 0) {
            $.ajax({
                async: true,
                type: "POST",
                url: '/GroupPermission/GetPermission',
                data: { GroupID: GroupID },
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    loadTrees(data.data, GroupID);
                }
            });
        }
    };
    loadTrees = function (jsondata, GroupID) {
        $("#kt_tree_permission").jstree('destroy');
        $('#kt_tree_permission').on('changed.jstree', function (e, data) {
            if (data.action == "select_node" || data.action == "deselect_node") {
                $.ajax({
                    url: '/GroupPermission/AddPermission',
                    method: 'POST',
                    data: {
                        lstPermission: data.selected, GroupID: GroupID
                    },
                    success: function (response, status, xhr, $form) {
                        if (response.result == 1) {
                            showToart('success', "Thay đổi quyền thành công");
                        }
                        else if (response.result == -1) {
                            showToart('error', response.message);
                        }
                        else {
                            showToart('error', response.message);
                        };
                    }
                });
            }
        }).jstree({
            'plugins': ["wholerow", "checkbox", "types"],
            'core': {
                "check_callback": true,
                "themes": {
                    "responsive": true
                },
                'data': jsondata
            },
            "types": {
                "default": {
                    "icon": "fa fa-folder kt--font-success"
                },
                "file": {
                    "icon": "fa fa-file  kt--font-warning"
                }
            },
        });
    };

    return {
        init: function () {
            loadGroupActive();
            init();
            $('#sl_search_group').select2({
                placeholder: "Chọn nhóm"
            });
        },
    };
}();
