﻿var configurationApprovalLevelBookDebt = function () {
    var lstIdsChange = [];
    var jsonData = [];
    var LoadData = function () {
        baseCommon.AjaxSuccess('Get', baseCommon.Endpoint.GetListTreeViewApprovalLevelBookDebt, "", function (respone) {
            if (respone.Result == 1) {
                    loadTree(respone.Data);
                } else {
                    baseCommon.ShowErrorNotLoad(respone.Message);
                }
            }, "Lấy danh sách không thành công");
    }
    var loadTree = function (dataAPI) {
        var dataTree = [];
        jsonData = [];
        lstIdsChange = [];
        var jsonDataFormat = JSON.stringify(dataAPI)
        jsonDataFormat = jsonDataFormat.toLowerCase();
        $("#kt_tree_employees").jstree('destroy');
        jsonData = JSON.parse(jsonDataFormat);
        changeTextList(jsonData);
   
        $('#kt_tree_employees').bind("move_node.jstree", function (e, data) {
           
            //if (parseInt(data.node.parents.length) >= 5) {
            //    baseCommon.ShowErrorNotLoad(`Vui lòng chỉ kéo đến cấp 4, kéo lại ${data.node.text}`);
            //    return
            //}
            console.log(data)
            $.each(lstIdsChange, function (key, value) {
                position = value.positionid; 
                if (value.ApprovalLevelBookDebtID == parseInt(data.node.id)) {
                    lstIdsChange.splice(key, 1);
                }
            });
            let datapush = {
                ApprovalLevelBookDebtID: parseInt(data.node.id),
                ParentID: data.node.parent == "#" ? 0 : parseInt(data.node.parent),
                Postion: data.node.parents.length,
            }
            lstIdsChange.push(datapush);
        }).jstree({
            core: {
                themes: {
                    responsive: !1
                },
                check_callback: !0,
                data: jsonData
            },
            types: {
                default: {
                    icon: "fa fa-user kt-font-success",
                    open: true
                },
                file: {
                    icon: "fa fa-user  kt-font-success"
                }
            },
            state: {
                key: "demo2"
            },
            plugins: ["dnd", "state", "types"]
        });
    };
  
    var changeTextList = function (children) {
        console.log(children)
        $.each(children, function (keyChild, child) {
            child.text = titleCase(child.text);
            switch (parseInt(child.postionid)) {
                case 1: {
                    child.icon = "fa fa-user kt-font-primary";
                    break;
                }
                case 2: {
                    child.icon = "fa fa-user kt-font-success";
                    break;
                }

                case 3: {
                    child.icon = "fa fa-user kt-font-warning";
                    break;
                }
                case 4: {
                    child.icon = "fa fa-user kt-font-dark";
                    break;
                }
                default: {
                    break;
                }
            }
            if (child.children != null && child.children.length > 0) {
                changeTextList(child.children);
            }
        })
    }

    var saveData = function () {
        $('#btn_save_setting').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            if (lstIdsChange == null || lstIdsChange.length == 0) {
                baseCommon.ShowErrorNotLoad("Bạn chưa có cấu hình gì mới");
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                return;
            }
            var request = {
                LstApprovalLevelBookDebt: lstIdsChange
            }
            baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.UpdateApprovalLevelBookDebt, JSON.stringify(request), function (respone) {
                if (respone.Result == 1) {
                    baseCommon.ShowSuccess(respone.Message);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    setTimeout(function () {
                        $('#sl_search_department').trigger('change');
                    }, 200);
                } else {
                    baseCommon.ShowErrorNotLoad(respone.Message);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                }
            }, "Lưu dữ liệu bị lỗi , vui lòng thử lại sau");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
        });
    };
    var init = function () {
        LoadData();
        saveData();
    }
    return {
        LoadData: LoadData,
        saveData: saveData,
        init: init
    }
}();
