﻿var recordGrid = 0;
var approvalLevelBookDebt = new function () {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var GeneralSearch = $('#txt_search').val();
                var Status = parseInt($('#sl_search_status').val());
                var data = {
                    KeySearch: GeneralSearch,
                    Status: Status
                }
                baseCommon.AjaxDone('POST', baseCommon.Endpoint.GetListApprovalLevelBookDebt, JSON.stringify(data), function (res) {
                    options.success([]);
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    options.success(res);
                    if (res.Result == 1) {
                    } else {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                return response.Total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                var data = [];
                if (response != null && typeof response.Data != "undefined" && response.Data != null) {
                    data = response.Data;
                }
                return data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function () {
        $("#grid").kendoGrid({
            dataSource: dataSource,
            selectable: "row",
            pageable: {
                pageSizes: [20, 30, 50],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 3) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'ApprovalLevelBookDebtName',
                    title: 'Cấp phê duyệt',
                    textAlign: 'left',
                },
                {
                    field: 'IsAllAccept',
                    title: 'Số lượng duyệt',
                    textAlign: 'left',
                    template: function (row) {
                        return row.IsAllAccept == 1 ? "Tất cả người trong cấp duyệt" : "1 người duyệt";
                    },
                },
                {
                    field: 'StrStatus',
                    title: 'Trạng thái',
                    textAlign: 'left',
                },
                {
                    field: 'CreateDate',
                    title: 'Ngày tạo',
                    textAlign: 'left',
                    template: function (row) {
                        return row.CreateDate == null ? "" : formattedDateHourMinutes(row.CreateDate);
                    },
                },
                {
                    title: 'Chức năng',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += ' <a class="btn btn-warning btn-icon btn-sm" title="Cập nhật cấp phê duyệt" data-toggle="modal" data-target="#modal_create_ApprovalLevelBookDebt"  onclick="approvalLevelBookDebt.ShowModal(\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                                      <i class="fa fa-edit"></i>\
                                     </a> ';
                        return html;
                    }
                }
            ]
        });
    };
    var GridRefresh = function () {
        var grid = $("#grid").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };
    var SaveApprovalLevelBookDebt = function () {
        $('#btn_save_ApprovalLevelBookDebt').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    txt_ApprovalLevelBookDebtName: {
                        required: true,
                    }
                },
                messages: {
                    txt_ApprovalLevelBookDebtName: {
                        required: "Vui lòng nhập cấp phê duyệt",
                    }
                }
            });
            if (!form.valid()) {
                return;
            }
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            var request = {
                "ApprovalLevelBookDebtID": parseInt($("#hdd_ApprovalLevelBookDebtID").val()),
                "ApprovalLevelBookDebtName": $("#txt_ApprovalLevelBookDebtName").val(),
                "Status": parseInt($("input[name='txt_Status']:checked").val()),
                "IsAllAccept": parseInt($("input[name='txt_all_accept']:checked").val()),
            };
            baseCommon.AjaxDone("POST", baseCommon.Endpoint.CreateOfUpdateApprovalLevelBookDebt, JSON.stringify(request), function (respone) {
                if (respone.Result == 1) {
                    baseCommon.ShowSuccess(respone.Message);
                    $('#btnGetData').trigger('click');
                    $('#modal_create_ApprovalLevelBookDebt').modal('hide');
                } else {
                    baseCommon.ShowErrorNotLoad(respone.Message);
                }
            });

            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
        });
    };

    var ShowModal = function (jsonData = '') {
        var data = JSON.parse(jsonData);
        $("#hdd_ApprovalLevelBookDebtID").val(data.ApprovalLevelBookDebtID);
        $("#txt_ApprovalLevelBookDebtName").val(data.ApprovalLevelBookDebtName);
        $("input[name=txt_Status]").removeAttr("checked");
        data.Status = data.Status == null ? 0 : data.Status;
        $("input[name=txt_all_accept]").removeAttr("checked");
        $("input[name=txt_Status][value=" + data.Status + "]").attr('checked', 'checked').change();
        $("input[name=txt_all_accept][value=" + data.IsAllAccept + "]").attr('checked', 'checked').change();
    }
    var ResetModal = function () {
        $("#txt_ApprovalLevelBookDebtName").val("");
        $("#hdd_ApprovalLevelBookDebtID").val(0);
        $("input[name=txt_Status]").removeAttr("checked");
        $("input[name=txt_all_accept]").removeAttr("checked");
        var status = 1;
        $("input[name=txt_Status][value=" + status + "]").attr('checked', 'checked').change();
        $("input[name=txt_all_accept][value=0]").attr('checked', 'checked').change();
    }

    var Init = function () {
        GridRefresh();
        $("#sl_search_status").on('change', function (e) {
            GridRefresh();
        });
        $("#btnGetData").on('click', function (e) {
            GridRefresh();
        });
        $("#txt_search").on('keypress', function (e) {
            if (e.which === 13) {
                GridRefresh();
            }
        });
    };
    return {
        Init: Init,
        GridRefresh: GridRefresh,
        SaveApprovalLevelBookDebt: SaveApprovalLevelBookDebt,
        ShowModal: ShowModal,
        ResetModal: ResetModal
    };
}
$(document).ready(function () {
    approvalLevelBookDebt.Init();
});
