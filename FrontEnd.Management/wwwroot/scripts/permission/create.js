﻿var permissionAdd = function () {
    var loadInfo = function (permissionID = 0, linkApi = '', displayText = '', isActive = 1) {
        var form = $('#btn_save_permission').closest('form');
        form.validate().destroy();
        $('#txt_edit_LinkApi').val(linkApi);
        $('#txt_edit_DisplayText').val(displayText);
        $('#hdd_edit_permissionID').val(permissionID);
        if (isActive == 1) {
            $('#kt_switch_2').prop('checked', true).trigger('change');
        } else {
            $('#kt_switch_2').prop('checked', false).trigger('change');
        }
        if (permissionID == 0)//form tạo mới
        {
            $('#header_edit_permission').text('Thêm mới Link API');
            $('#btn_save_permission').html('<i class="fa fa-save"></i>Thêm mới');
        }
        else {
            $('#header_edit_permission').text('Cập nhật Link API');
            $('#btn_save_permission').html('<i class="fa fa-save"></i>Cập nhật');
        }
    };
    var sumitFormSave = function () {
        $('#btn_save_permission').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    LinkApi: {
                        required: true,
                    },
                    DisplayText: {
                        required: true,
                    },
                },
                messages: {
                    LinkApi: {
                        required: "Vui lòng nhập link API",
                    },
                    DisplayText: {
                        required: "Vui lòng nhập miêu tả",
                    },
                }
            });

            if (!form.valid()) {
                return;
            }
            var IsActive = $('#kt_switch_2').prop('checked') ? 1 : 0;
            var PermissionID = $('#hdd_edit_permissionID').val();
            var LinkApi = $('#txt_edit_LinkApi').val();
            var DisplayText = $('#txt_edit_DisplayText').val(); 
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            var data = {
                PermissionID: parseInt(PermissionID), IsActive: IsActive, LinkApi: LinkApi, DisplayText: DisplayText
            }; 
            var endpoint = baseCommon.Endpoint.CreatePermission;
            if (PermissionID > 0) {
                endpoint = baseCommon.Endpoint.UpdatePermission;
            }
            baseCommon.AjaxDone("POST", endpoint, JSON.stringify(data), function (respone) { 
                if (respone.Result == 1) {
                    showMesage("success", PermissionID == 0 ? "Tạo mới quyền thành công" : "Cập nhật quyền thành công");
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    $('#hdd_edit_permissionID').val(respone.data);
                    $('#header_edit_permission').text('Cập nhật Link API');
                    $('#btn_save_permission').html('<i class="fa fa-save"></i>Cập nhật');
                    $('#sl_search_status').change();
                    $('#btnGetData').click();
                    setTimeout(function () {
                        $('#btn_exit_permission').click();
                    }, 1000);
                    
                } else {
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    showMesage("danger", respone.Message);
                }
            }); 
        });
    }
    return {
        loadInfo: loadInfo,
        sumitFormSave: sumitFormSave
    }
}()