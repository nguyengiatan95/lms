﻿var permissionList = function () {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                
                var data = {
                    GeneralSearch: $('#txt_search_api').val().trim(),
                    Status: parseInt($('#sl_search_status').val().trim()),
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxDone('GET', baseCommon.Endpoint.GetByConditionsPermission, (data), function (res) {
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    if (res.Result == 1) {
                        options.success(res);
                    }
                }, "Thao tác thất bại, vui lòng thử lại sau!");
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function (objQuery) {
        $("#dv_result_permission_list").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            selectable: "row",
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 1) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'LinkApi',
                    title: 'Link Api',
                    width: 450,
                    template: function (row) {
                        // callback function support for column rendering
                        return '<a class="kt-font-primary  kt-link"  title="Sửa">' + row.LinkApi + '</a>';
                    },
                },
                {
                    field: 'DisplayText',
                    title: 'Miêu tả',
                    textAlign: 'left',
                },

                {
                    field: 'CreateDate',
                    title: 'Ngày tạo',
                    textAlign: 'center',
                    template: function (row) {
                        // callback function support for column rendering
                        return formattedDate(row.CreateDate);
                    },
                },
                {
                    field: 'IsActive',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    // callback function support for column rendering
                    template: function (row) {
                        if (row.IsActive == 1) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Hoạt Động</span>';
                        } else if (row.IsActive == 0) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Khóa</span>';
                        }
                    },
                },
                {
                    field: 'action',
                    title: 'Chức năng',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        return '\
                          <a class="btn btn-primary btn-icon btn-sm" title="Sửa" data-toggle="modal" data-target="#modal_edit_permission" onclick="permissionAdd.loadInfo('+ row.PermissionID + ',\'' + row.LinkApi + '\',\'' + row.DisplayText + '\',' + row.IsActive + ')">\
                            <i class="fa fa-edit" ></i>\
                            </a>\
					';
                    }
                }
            ]
        });
    };
    var GridRefresh = function () {
        baseCommon.ButtonSubmit(false, '#btnGetData');
        var grid = $("#dv_result_permission_list").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };
    var Init = function () {
        GridRefresh();
        $("#sl_search_status").on('change', function (e) {
            GridRefresh();
        });
        $("#txt_search_api").on('keypress', function (e) {
            GridRefresh();
        });
        $("#btnGetData").on('click', function (e) {
            GridRefresh();
        });
    }
    return {
        Init: Init,
    }
}();
$(document).ready(function () {
    permissionList.Init();
});