﻿var recordGrid = 0;
var bookDebt = new function () {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var GeneralSearch = $('#txt_search').val();
                var Status = parseInt($('#sl_search_status').val());
                var data = {
                    KeySearch: GeneralSearch,
                    Status: Status
                }
                baseCommon.AjaxDone('POST', baseCommon.Endpoint.ListBookDebt, JSON.stringify(data), function (res) {
                    options.success([]);
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    options.success(res);
                    if (res.Result == 1) {
                    } else {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                });
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                return response.Total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                var data = [];
                if (response != null && typeof response.Data != "undefined" && response.Data != null) {
                    data = response.Data;
                }
                return data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function () {

        $("#grid").kendoGrid({
            dataSource: dataSource,
            selectable: "row",
            pageable: {
                pageSizes: [20, 30, 50],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 3) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'BookDebtName',
                    title: 'Book nợ',
                    textAlign: 'left',
                },
                {
                    field: 'YearDebt',
                    title: 'Năm nợ xấu',
                    textAlign: 'left',
                },
                {
                    field: 'DPDFrom',
                    title: 'DPD từ khoảng',
                    textAlign: 'left',
                },
                {
                    field: 'DPDTo',
                    title: 'DPD đến khoảng',
                    textAlign: 'left',
                },
                {
                    field: 'StrStatus',
                    title: 'Trạng thái',
                    textAlign: 'left',
                },
                {
                    field: 'CreateDate',
                    title: 'Ngày tạo',
                    textAlign: 'left',
                    template: function (row) {
                        return row.CreateDate == null ? "" : formattedDateHourMinutes(row.CreateDate);
                    },
                },
                {
                    title: 'Chức năng',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        var html = '';
                        html += ' <a class="btn btn-warning btn-icon btn-sm" title="Cập nhật" data-toggle="modal" data-target="#modal_create_bookDebt"  onclick="bookDebt.ShowModal(\'' + JSON.stringify(row).replace(/"/g, '&quot;') + '\')">\
                                      <i class="fa fa-edit"></i>\
                                     </a> ';
                        return html;
                    }
                }
            ]
        });
    };
    var GridRefresh = function () {
        var grid = $("#grid").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };
    var SaveBookDebt = function () {
        $('#btn_save_bookdebt').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    txt_BookDebt: {
                        required: true,
                    },
                    txt_YearDebt: {
                        required: true,
                    }
                },
                messages: {
                    txt_BookDebt: {
                        required: "Vui lòng nhập book nợ",
                    },
                    txt_YearDebt: {
                        required: "Vui lòng nhập năm nợ xấu",
                    }
                }
            });
            if (!form.valid()) {
                return;
            }
            var dPDFrom = $("#txt_DPDFrom").val();
            if (dPDFrom == "" || dPDFrom == null) {
                dPDFrom = 0
            }
            var dPDTo = $("#txt_DPDTo").val();
            if (dPDTo == "" || dPDTo == null) {
                dPDTo = 0
            }
            if (parseInt($("#txt_YearDebt").val()) < 2015) {
                baseCommon.ShowErrorNotLoad("Năm nợ xấu không được nhỏ hơn năm 2015");
                return false
            }
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            var request = {
                "BookDebtID": parseInt($("#hdd_BookDebtID").val()),
                "BookDebtName": $("#txt_BookDebt").val(),
                "Status": parseInt($("input[name='txt_Status']:checked").val()),
                "YearDebt": parseInt($("#txt_YearDebt").val()),
                "DPDFrom": parseInt(dPDFrom),
                "DPDTo": parseInt(dPDTo),
                "CreateBy": USER_ID
            };

            baseCommon.AjaxDone("POST", baseCommon.Endpoint.CreateOfUpdateBookDebt, JSON.stringify(request), function (respone) {
                if (respone.Result == 1) {
                    baseCommon.ShowSuccess(respone.Message);
                    $('#btnGetData').trigger('click');
                    $('#modal_create_bookDebt').modal('hide');
                } else {
                    baseCommon.ShowErrorNotLoad(respone.Message);
                }
            });

            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
        });
    };

    var ShowModal = function (jsonData = '') {
        var data = JSON.parse(jsonData);
        $("#txt_BookDebt").val(data.BookDebtName);
        $("#hdd_BookDebtID").val(data.BookDebtID);
        $("#txt_YearDebt").val(data.YearDebt);
        $("#txt_DPDFrom").val(data.DPDFrom);
        $("#txt_DPDTo").val(data.DPDTo);
        $("input[name=txt_Status]").removeAttr("checked");
        data.Status = data.Status == null ? 0 : data.Status;
        $("input[name=txt_Status][value=" + data.Status + "]").attr('checked', 'checked');
    }
    var ResetModal = function () {
        $("#txt_BookDebt").val("");
        $("#hdd_BookDebtID").val(0);
        $("#txt_YearDebt").val("");
        $("#txt_DPDFrom").val("");
        $("#txt_DPDTo").val("");
        $("input[name=txt_Status]").removeAttr("checked");
        var status = 1;
        $("input[name=txt_Status][value=" + status + "]").attr('checked', 'checked');
    }

    var Init = function () {
        GridRefresh();
        $("#btnGetData").on('click', function (e) {
            GridRefresh();
        });
        $("#sl_search_status").on('change', function (e) {
            GridRefresh();
        });
        $("#txt_search").on('keypress', function (e) {
            if (e.which === 13) {
                GridRefresh();
            }
        });
        $('#txt_YearDebt').keypress(function (e) {
            var charCode = (e.which) ? e.which : event.keyCode
            if (String.fromCharCode(charCode).match(/[^0-9]/g))
                return false;
        });
    };
    return {
        Init: Init,
        GridRefresh: GridRefresh,
        SaveBookDebt: SaveBookDebt,
        ShowModal: ShowModal,
        ResetModal: ResetModal
    };
}
$(document).ready(function () {
    bookDebt.Init();
});
