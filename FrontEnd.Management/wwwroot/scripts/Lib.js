﻿function showMesage(type, msg) {
    var content = {};
    content.message = msg;
    var notify = $.notify(content, {
        type: type,
        allow_dismiss: true,
        newest_on_top: true,
        mouse_over: false,
        showProgressbar: false,
        spacing: 10,
        timer: 300,
        placement: {
            from: "top",
            align: "right"
        },
        offset: {
            x: 30,
            y: 30
        },
        delay: 200,
        z_index: 1051,
        animate: {
            enter: 'animated ' + 'bounce',
            exit: 'animated ' + 'bounce'
        }
    });
}

function formatMoneyAndDisplay(nStr, decSeperate, groupSeperate) {
    var strHTML = '';
    if (nStr > 0) {
        strHTML = '<span style="color:blue">+' + formatNumber(nStr, decSeperate, groupSeperate) + '</span>';
    } else if (nStr == 0) {
        strHTML = '<span style="color:blue">' + formatNumber(nStr, decSeperate, groupSeperate) + '</span>';
    } else {
        strHTML = '<span style="color:red">' + formatNumber(nStr, decSeperate, groupSeperate) + '</span>';
    }

    return strHTML;
}

function formatNumber(nStr, decSeperate, groupSeperate) {
    nStr += '';
    x = nStr.split(decSeperate);
    x1 = x[0];
    x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + groupSeperate + '$2');
    }
    return x1 + x2;
}
function formattedDate(date) {
    if (date == "0001-01-01T00:00:00") {
        return '';
    } else {
        var d = new Date(date);
        let month = String(d.getMonth() + 1);
        let day = String(d.getDate());
        const year = String(d.getFullYear());
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        return `${day}/${month}/${year}`;
    }

}
function formattedDay(date) {
    var d = new Date(date);
    let month = String(d.getMonth() + 1);
    let day = String(d.getDate());
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return `${day}/${month}`;
}
function formattedDate_DD_MM_YYYY(date) {
    var d = new Date(date);
    let month = String(d.getMonth() + 1);
    let day = String(d.getDate());
    const year = String(d.getFullYear());
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return `${day}-${month}-${year}`;
}
function formattedDateHourMinutes(date) {
    if (date == "0001-01-01T00:00:00") {
        return '';
    } else {
        if ($(window).width() < 1000) {
            var d = new Date(date);
            let month = String(d.getUTCMonth() + 1);
            let day = String(d.getUTCDate());
            const year = String(d.getUTCFullYear());
            let hour = "" + d.getUTCHours(); if (hour.length == 1) { hour = "0" + hour; }
            let minute = "" + d.getUTCMinutes(); if (minute.length == 1) { minute = "0" + minute; }
            //let second = "" + d.getSeconds(); if (second.length == 1) { second = "0" + second; }
            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            return `${day}/${month}/${year} ${hour}:${minute} `;
        } else {
            var d = new Date(date);
            let month = String(d.getMonth() + 1);
            let day = String(d.getDate());
            const year = String(d.getFullYear());
            let hour = "" + d.getHours(); if (hour.length == 1) { hour = "0" + hour; }
            let minute = "" + d.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
            //let second = "" + d.getSeconds(); if (second.length == 1) { second = "0" + second; }
            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            return `${day}/${month}/${year} ${hour}:${minute} `;
        }

    }

}

function typingNumber(controlID) {
    $(document).on('keyup', '#' + controlID, function (e) {
        if (IgnoreMinus(this, e.which)) return;
        var loValue = formatCurrency($(this).val().replace(/,/g, '').replace(/\./g, ''));
        $("#" + controlID).val(loValue);
    });
};

$(document).on('keyup', '.money_input', function (e) {
    if (IgnoreMinus(this, e.which)) return;
    var loValue = formatCurrency($(this).val().replace(/,/g, '').replace(/\./g, ''));
    $(this).val(loValue);
    var value = Math.abs(loValue.replace(/,/g, '').replace(/\./g, ''));
    var stchu = numbertotext(value);
    $(this).attr('data-original-title', stchu).tooltip('show');
});



function ConvertStringToDateByID(sID) {

    var strValue = $('#' + sID).val();
    var parts = strValue.split('-');
    var dDate = new Date(parts[2], parts[1] - 1, parts[0]);

    return dDate;
}

function DateAdd(oldDate, totalDay) {
    if (totalDay == '' || isNaN(totalDay)) return oldDate;
    var newDate = oldDate;
    newDate.setDate(oldDate.getDate() + parseInt(totalDay));
    return newDate;
}

function GenrateDate(date) {
    var vdateBegin = "";
    vdateBegin = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    vdateBegin += '-';
    vdateBegin += (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);
    vdateBegin += '-' + date.getFullYear();

    return vdateBegin;
}

function getMonday(d) {
    d = new Date(d);
    var day = d.getDay(),
        diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
    return new Date(d.setDate(diff));
}
function getEndOfWeek(date) {
    date = getMonday(date);
    date.setDate(date.getDate() + 6);
    return date;
}
var showToart = function (type, message) {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "500",
        "timeOut": "3000",
        "extendedTimeOut": "500",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    if (type == "success") {
        toastr.success(message);
    } else {
        toastr.error(message);
    }
};

var formatCurrency = function (num) {
    num = num.toString().replace(/\$|\,/g, '');
    if (isNaN(num))
        num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.50000000001);
    cents = num % 100;
    num = Math.floor(num / 100).toString();
    if (cents < 10)
        cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        num = num.substring(0, num.length - (4 * i + 3)) + ',' +
            num.substring(num.length - (4 * i + 3));
    return (((sign) ? '' : '-') + num);
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

function typingNumberNoMoney(controlId) {
    $('#' + controlId).keyup(function (event) {
        // skip for arrow keys
        if (event.which >= 37 && event.which <= 40) return;
        // format number
        $(this).val(function (index, num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            return (((sign) ? '' : '-') + num);
        });
    });
};
function IgnoreMinus(element, which) {
    if (which == 189 || which == 109) {
        $(element).val('-');
        return true;
    }
    return false;
}
var ChuSo = new Array(" không ", " một ", " hai ", " ba ", " bốn ", " năm ", " sáu ", " bảy ", " tám ", " chín ");
var Tien = new Array("", " nghìn", " triệu", " tỷ", " nghìn tỷ", " triệu tỷ");
//1. Hàm đọc số có ba chữ số;
function DocSo3ChuSo(baso) {
    var tram;
    var chuc;
    var donvi;
    var KetQua = "";
    tram = parseInt(baso / 100);
    chuc = parseInt((baso % 100) / 10);
    donvi = baso % 10;
    if (tram == 0 && chuc == 0 && donvi == 0) return "";
    if (tram != 0) {
        KetQua += ChuSo[tram] + " trăm ";
        if ((chuc == 0) && (donvi != 0)) KetQua += " linh ";
    }
    if ((chuc != 0) && (chuc != 1)) {
        KetQua += ChuSo[chuc] + " mươi";
        if ((chuc == 0) && (donvi != 0)) KetQua = KetQua + " linh ";
    }
    if (chuc == 1) KetQua += " mười ";
    switch (donvi) {
        case 1:
            if ((chuc != 0) && (chuc != 1)) {
                KetQua += " mốt ";
            }
            else {
                KetQua += ChuSo[donvi];
            }
            break;
        case 5:
            if (chuc == 0) {
                KetQua += ChuSo[donvi];
            }
            else {
                KetQua += " lăm ";
            }
            break;
        default:
            if (donvi != 0) {
                KetQua += ChuSo[donvi];
            }
            break;
    }
    return KetQua;
}
//2. Hàm đọc số thành chữ (Sử dụng hàm đọc số có ba chữ số)
function DocTienBangChu(SoTien) {
    var lan = 0;
    var i = 0;
    var so = 0;
    var KetQua = "";
    var tmp = "";
    var ViTri = new Array();
    if (SoTien < 0) return "Số tiền âm !";
    if (SoTien == 0) return "Không đồng !";
    if (SoTien > 0) {
        so = SoTien;
    }
    else {
        so = -SoTien;
    }
    if (SoTien > 8999999999999999) {
        //SoTien = 0;
        return "Số quá lớn!";
    }
    ViTri[5] = Math.floor(so / 1000000000000000);
    if (isNaN(ViTri[5]))
        ViTri[5] = "0";
    so = so - parseFloat(ViTri[5].toString()) * 1000000000000000;
    ViTri[4] = Math.floor(so / 1000000000000);
    if (isNaN(ViTri[4]))
        ViTri[4] = "0";
    so = so - parseFloat(ViTri[4].toString()) * 1000000000000;
    ViTri[3] = Math.floor(so / 1000000000);
    if (isNaN(ViTri[3]))
        ViTri[3] = "0";
    so = so - parseFloat(ViTri[3].toString()) * 1000000000;
    ViTri[2] = parseInt(so / 1000000);
    if (isNaN(ViTri[2]))
        ViTri[2] = "0";
    ViTri[1] = parseInt((so % 1000000) / 1000);
    if (isNaN(ViTri[1]))
        ViTri[1] = "0";
    ViTri[0] = parseInt(so % 1000);
    if (isNaN(ViTri[0]))
        ViTri[0] = "0";
    if (ViTri[5] > 0) {
        lan = 5;
    }
    else if (ViTri[4] > 0) {
        lan = 4;
    }
    else if (ViTri[3] > 0) {
        lan = 3;
    }
    else if (ViTri[2] > 0) {
        lan = 2;
    }
    else if (ViTri[1] > 0) {
        lan = 1;
    }
    else {
        lan = 0;
    }
    for (i = lan; i >= 0; i--) {
        tmp = DocSo3ChuSo(ViTri[i]);
        KetQua += tmp;
        if (ViTri[i] > 0) KetQua += Tien[i];
        if ((i > 0) && (tmp.length > 0)) KetQua += ',';//&& (!string.IsNullOrEmpty(tmp))
    }
    if (KetQua.substring(KetQua.length - 1) == ',') {
        KetQua = KetQua.substring(0, KetQua.length - 1);
    }
    KetQua = KetQua.substring(1, 2).toUpperCase() + KetQua.substring(2);
    return KetQua;//.substring(0, 1);//.toUpperCase();// + KetQua.substring(1);
}
function numbertotext(value) {
    var v_doc_so = docso(value);
    return stchu = v_doc_so.substring(2, 1).toUpperCase() + v_doc_so.substring(2, v_doc_so.length) + " đồng";
}
function numbertoPercent(value) {
    var v_doc_so = docso(value);
    return stchu = v_doc_so.substring(2, 1).toUpperCase() + v_doc_so.substring(2, v_doc_so.length) + " %";
}
function formatNullToEmpty(strData) {
    if (strData == null || strData == undefined || strData == 'null') {
        return "";
    }
    return strData;
}
function docso(so) {
    if (so == 0) return ' ' + mangso[0];
    var chuoi = "",
        hauto = "";
    do {
        ty = so % 1000000000;
        so = Math.floor(so / 1000000000);
        if (so > 0) {
            chuoi = dochangtrieu(ty, true) + hauto + chuoi;
        } else {
            chuoi = dochangtrieu(ty, false) + hauto + chuoi;
        }
        hauto = " tỷ";
    } while (so > 0);
    return chuoi;
}
function dochangtrieu(so, daydu) {
    var chuoi = "";
    trieu = Math.floor(so / 1000000);
    so = so % 1000000;
    if (trieu > 0) {
        chuoi = docblock(trieu, daydu) + " triệu";
        daydu = true;
    }
    nghin = Math.floor(so / 1000);
    so = so % 1000;
    if (nghin > 0) {
        chuoi += docblock(nghin, daydu) + " nghìn";
        daydu = true;
    }
    if (so > 0) {
        chuoi += docblock(so, daydu);
    }
    return chuoi;
}
function docblock(so, daydu) {
    var chuoi = "";
    tram = Math.floor(so / 100);
    so = so % 100;
    if (daydu || tram > 0) {
        chuoi = " " + mangso[tram] + " trăm";
        chuoi += dochangchuc(so, true);
    } else {
        chuoi = dochangchuc(so, false);
    }
    return chuoi;
}
var mangso = ['không', 'một', 'hai', 'ba', 'bốn', 'năm', 'sáu', 'bảy', 'tám', 'chín'];
function dochangchuc(so, daydu) {
    var chuoi = "";
    chuc = Math.floor(so / 10);
    donvi = so % 10;
    if (chuc > 1) {
        chuoi = " " + mangso[chuc] + " mươi";
        if (donvi == 1) {
            chuoi += " mốt";
        }
    } else if (chuc == 1) {
        chuoi = " mười";
        if (donvi == 1) {
            chuoi += " một";
        }
    } else if (daydu && donvi > 0) {
        chuoi = " lẻ";
    }
    if (donvi == 5 && chuc >= 1) {
        chuoi += " lăm";
    } else if (donvi > 1 || (donvi == 1 && chuc == 0)) {
        chuoi += " " + mangso[donvi];
    }
    return chuoi;
}

function functionInputMoney(controlID) {
    $(document).on('keyup', '#' + controlID, function (e) {
        if (IgnoreMinus(this, e.which)) return;
        var loValue = formatCurrency($(this).val().replace(/,/g, '').replace(/\./g, ''));
        $("#" + controlID).val(loValue);
        var value = Math.abs(loValue.replace(/,/g, '').replace(/\./g, ''));
        var stchu = numbertotext(value);
        $(this).attr('data-original-title', stchu).tooltip('show');
    });
}
var getFormattedDate = function (date) {
    var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return day + '/' + month + '/' + year;
}

var convertStringToDate = function (dateString) {

    var dateParts = dateString.split("/");

    // month is 0-based, that's why we need dataParts[1] - 1
    var dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
    return dateObject;
}

function parseDate(str) {
    var mdy = str.split('/');
    return new Date(mdy[2], mdy[0] - 1, mdy[1]);
}

function datediff(first, second) {
    // Take the difference between the dates and divide by milliseconds per day.
    // Round to nearest whole number to deal with DST.
    return Math.round((second - first) / (1000 * 60 * 60 * 24));
}

function monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
}

function html_money(money) {
    if (money == 0) {
        return "<span class='kt-font-blue'> " + formatCurrency(money);
    }
    return money > 0 ? "<span class='kt-font-blue'> " + formatCurrency(money) + " </span>" :
        "<span class='kt-font-danger'> " + formatCurrency(money) + " </span>";
}
function formatTextMoneyToNumber(strText) {
    return strText.replace(/,/g, '').replace(/\./g, '');
}


String.prototype.convertStringToDateFormat = function(format) {
    var normalized = this.replace(/[^a-zA-Z0-9]/g, '-');
    var normalizedFormat = format.toLowerCase().replace(/[^a-zA-Z0-9]/g, '-');
    var formatItems = normalizedFormat.split('-');
    var dateItems = normalized.split('-');

    var monthIndex = formatItems.indexOf("mm");
    var dayIndex = formatItems.indexOf("dd");
    var yearIndex = formatItems.indexOf("yyyy");
    var hourIndex = formatItems.indexOf("hh");
    var minutesIndex = formatItems.indexOf("ii");
    var secondsIndex = formatItems.indexOf("ss");

    var today = new Date();

    var year = yearIndex > -1 ? dateItems[yearIndex] : today.getFullYear();
    var month = monthIndex > -1 ? dateItems[monthIndex] - 1 : today.getMonth() - 1;
    var day = dayIndex > -1 ? dateItems[dayIndex] : today.getDate();

    var hour = hourIndex > -1 ? dateItems[hourIndex] : today.getHours();
    var minute = minutesIndex > -1 ? dateItems[minutesIndex] : today.getMinutes();
    var second = secondsIndex > -1 ? dateItems[secondsIndex] : today.getSeconds();

    return new Date(year, month, day, hour, minute, second);
};
function titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
        // You do not need to check if i is larger than splitStr length, as your for does that for you
        // Assign it back to the array
        splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    // Directly return the joined string
    return splitStr.join(' ');
}