﻿var menu = new function () {
    var TypeMenu = {
        ParentID: 1,
        ChildrenID: 2
    };
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) { 
                var customerKeySearch = $('#txt_search_menu').val();
                var type = $('#sl_search_menu').val();
                var appId = $('#sl_search_AppId').val();
                if (appId == null || appId == "" || appId == 0) {
                    appId = -1
                }
                var data = {
                    GeneralSearch: customerKeySearch,
                    Status: parseInt(type),
                    AppId: parseInt(appId),
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetByConditionsMenu, JSON.stringify(data), function (res) {
                    if (res.Result == 1) {
                        options.success(res);
                    }
                }, "Thao tác thất bại, vui lòng thử lại sau!");
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    function Grid_change(e) {
        var selectedRows = this.select();
        var selectedDataItems = [];
        for (var i = 0; i < selectedRows.length; i++) {
            var dataItem = this.dataItem(selectedRows[i]);
            selectedDataItems.push(dataItem);
        }
        // selectedDataItems contains all selected data items
        gridSelectedRowData = selectedDataItems[0]; 
        initAddOrUpdate(gridSelectedRowData);
    }
    var InitGrid = function (objQuery) {
        $("#grid").kendoGrid({
            dataSource: dataSource,
            change: Grid_change,
            pageable: {
                pageSizes: [20, 30, 50, 100],
            },
            resizable: true,
            selectable: "row",
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 1) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'DisplayText',
                    title: 'Tên hiển thị',
                    textAlign: 'left',
                    width: 300
                },
                {
                    field: 'ControllerName',
                    title: 'ControllerName',
                    textAlign: 'left',
                },
                {
                    field: 'ActionName',
                    title: 'Action',
                    textAlign: 'left',
                },
                {
                    field: 'Position',
                    title: 'Vị trí',
                    textAlign: 'left',
                },
                {
                    field: 'StrStatus',
                    title: 'Trạng thái',
                    attributes: { style: "text-align:center;" },
                    template: function (row) {
                        if (row.Status == 1) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Hoạt động</span>';
                        }
                        if (row.Status == 0) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Ngừng hoạt động</span>';
                        }
                    }
                },
                {
                    title: "Hành động",
                    selectable: false,
                    template: function (row) {
                        var html = '';
                        html = '<span title="Sửa"  class="btn btn-primary btn-icon btn-sm"> <i class="fa fa-edit"> </i> </span> ';
                        return html;
                    }
                }
            ]
        });
    };
    var Init = function () {
        baseCommon.DataSource("#sl_search_AppId", "Tất cả", "Key", "Value", 0, menu_AppIDs);
        baseCommon.DataSource("#AppID", "Chọn phân hệ", "Key", "Value", 0, menu_AppIDs);
        $("#sl_search_AppId,#sl_search_menu").on('change', function (e) {
            GridRefresh();
        }); 
        $("#TypeMenu").on('change', function (e) {
            var value = this.value;
            if (value == TypeMenu.ChildrenID) {
                $("#menu_ParentID").removeAttr("style"); 
            } else {
                $("#menu_ParentID").css("display", "none");
            }
        });
        $("#AppID").on('change', function (e) {
            getMenuParent(0);
        });
        GridRefresh();
    }
    var GridRefresh = function () {
        var grid = $("#grid").data("kendoGrid");
        if (grid == null) {
            InitGrid();
        } else {
            $("#grid").data("kendoGrid").dataSource.read();
        }
    };
    var Delete = function (loanID, codeID) {
        swal.fire({
            html: "Bạn có chắc chắn muốn xóa hợp đồng <b>" + codeID + "</b> không?",
            type: "warning",
            showCancelButton: !0,
            cancelButtonText: "Hủy",
            confirmButtonText: "Đồng ý"
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: "/Loan/DeleteLoan",
                    type: "POST",
                    data: { loanID: loanID },
                    success: function (response) {
                        if (response.response.result === 1) {
                            showMesage("success", "Xóa HĐ " + codeID + " thành công");
                            $('#btnGetData').click();
                        }
                        else {
                            showMesage("danger", response.response.message);
                        }
                    }
                });
            }
        });
    };
    var getMenuParent = function () {
        var AppID = $("#AppID").val();
        var parentID = $("#hdd_ParentID").val();
        if (AppID == null || typeof AppID == "undefined" || AppID == "" || AppID == 0) { 
            return;
        }
        var data = {
            AppID: parseInt(AppID)
        };
        baseCommon.AjaxSuccess("GET", baseCommon.Endpoint.GetMenuParent, data, function (respone) {
            if (respone.Result == 1) {
                baseCommon.DataSource("#ParentID", "Menu Cha", "MenuID", "DisplayText", parentID, respone.Data);
            }
        }, "Lấy danh sách Menu không thành công vui lòng thử lại!");
    }
    var initAddOrUpdate = function (model) {
        $("#modal_add_edit_menu").modal("toggle");
       
        if (typeof model == "undefined" || model == null|| model.MenuID == 0) { 
            $('#txt_menu_name').val('');
            $('#txt_menu_controller').val('');
            $('#txt_menu_action').val('');
            $('#txt_menu_icon').val('');
            $('#txt_menu_position').val('');
            $('#txt_menu_description').val('');
            $("input[name=status_menu]").val([1]);
            $("#ParentID").val(0).change();
        } else {
            $('#AppID').val(model.AppID).change();  
            $("#hdd_ParentID").val(model.ParentID);
            getMenuParent()
            $("#hdd_MenuID").val(model.MenuID);
            var typeMenu = TypeMenu.ParentID;
            if (model.ParentID > 0) {
                typeMenu = TypeMenu.ChildrenID;
            }
            $("#TypeMenu").val(typeMenu).change(); 
            if (model.DisplayText == 'null' || model.DisplayText == '') {
                $('#txt_menu_name').val('');
            } else {
                $('#txt_menu_name').val(model.DisplayText);
            }
            if (model.ControllerName == 'null' || model.ControllerName == '') {
                $('#txt_menu_controller').val('');
            } else {
                $('#txt_menu_controller').val(model.ControllerName);
            }
            if (model.ActionName == 'null' || model.ActionName == '') {
                $('#txt_menu_action').val('');
            } else {
                $('#txt_menu_action').val(model.ActionName);
            }
            if (model.Icon == 'null' || model.Icon == '') {
                $('#txt_menu_icon').val('');
            } else {
                $('#txt_menu_icon').val(model.Icon);
            }
            if (model.Position == 'null' || model.Position == '') {
                $('#txt_menu_position').val('');
            } else {
                $('#txt_menu_position').val(model.Position);
            }
            if (model.Description == 'null' || model.Description == '') {
                $('#txt_menu_description').val('');
            } else {
                $('#txt_menu_description').val(model.Description);
            }  
            if (model.IsMenu == 1) {
                $('#IsMenu').attr('checked', 'checked');
            } else {
                $('#IsMenu').removeAttr('checked');
            }
            $("input[name=status_menu]").val([model.Status]);
        }
    }
    var save_AddOrUpdate = function () {
        var btn = $('#btn_save_menu');
        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

        var appID = $("#AppID").val();
        var isMenu = $('#IsMenu').prop('checked') ? 1 : 0; 
        var menuID = $("#hdd_MenuID").val();
        var parentID = $("#ParentID").val(); 
        var typeMenu = $("#TypeMenu").val(); 
        if (appID == null || typeof appID == "undefined" || appID == "" || appID == 0) {
            baseCommon.ShowErrorNotLoad("Bạn chưa chọn phân hệ!");
            return;
        }
        if ( typeMenu == TypeMenu.ParentID) {
            parentID = 0;
        }
        if ((parentID == null || parentID == "" || parentID == 0) && typeMenu == TypeMenu.ChildrenID) {
            baseCommon.ShowErrorNotLoad("Vui lòng chọn Menu Cha!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }

        var menuName = $("#txt_menu_name").val();
        if (menuName == null || menuName == "") {
            baseCommon.ShowErrorNotLoad("Tên Menu không được bỏ trống!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }

        var menuController = $("#txt_menu_controller").val();
        if (menuController == null || menuController == "") {
            baseCommon.ShowErrorNotLoad("Tên Controller không được bỏ trống!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }

        var menuAction = $("#txt_menu_action").val();
        if (menuAction == null || menuAction == "") {
            baseCommon.ShowErrorNotLoad("Tên Action không được bỏ trống!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }

        var menuIcon = $("#txt_menu_icon").val();
        if (menuIcon == null || menuIcon == "") {
            baseCommon.ShowErrorNotLoad("Icon không được bỏ trống!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }

        var menuPosition = $("#txt_menu_position").val();
        if (menuPosition == null || menuPosition == "") {
            baseCommon.ShowErrorNotLoad("Thứ tự không được bỏ trống!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        } else if (menuPosition <= 0) {
            baseCommon.ShowErrorNotLoad("Thứ tự không được nhỏ hơn 0!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }
        var regex = /^[0-9]+$/;
        if (!menuPosition.match(regex)) {
            baseCommon.ShowErrorNotLoad("Thứ tự phải là số!");
            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            return;
        }
        var menuDescription = $("#txt_menu_description").val();
        var status = $("input[name=status_menu]:checked").val();

        var modelCreateMenu = {
            "MenuID": parseInt(menuID),
            "AppID": parseInt(appID),
            "DisplayText": menuName,
            "ParentID": parseInt(parentID),
            "Position": parseInt(menuPosition),
            "ControllerName": menuController,
            "ActionName": menuAction,
            "Status": parseInt(status),
            "Description": menuDescription,
            "IsMenu": parseInt(isMenu),
            "Icon": menuIcon,
        };
        baseCommon.AjaxDone("POST", baseCommon.Endpoint.CreateUpdateMenu, JSON.stringify(modelCreateMenu), function (respone) {
            if (respone.Result == 1) {
                baseCommon.ShowSuccess(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                $("#btn_exit_menu").click();
                GridRefresh()
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            }
        });
    }
    return {
        TypeMenu: TypeMenu,
        Init: Init,
        save_AddOrUpdate: save_AddOrUpdate,
        initAddOrUpdate: initAddOrUpdate,
        Delete: Delete,
        GridRefresh: GridRefresh,
        getMenuParent: getMenuParent,
    };
}
$(document).ready(function () {
    menu.Init();
});