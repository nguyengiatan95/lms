﻿var departmentList = new function () {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var data = {
                    GeneralSearch: $('#txt_search_department').val().trim(),
                    Status: parseInt($('#sl_search_status').val().trim()),
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxDone('GET', baseCommon.Endpoint.GetDepartment, (data), function (res) {
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    if (res.Result == 1) {
                        options.success(res);
                    }
                }, "Thao tác thất bại, vui lòng thử lại sau!");
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function (objQuery) {
        $("#dv_result_department").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            selectable: "row",
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 1) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {

                    field: 'DepartmentName',
                    title: 'Phòng ban',
                    template: function (row) {
                        // callback function support for column rendering
                        return '<a class="kt-font-primary kt-font-boldest kt-link" style="cursor:pointer" title="Sửa">' + row.DepartmentName + '</a>';
                    },
                },
                {
                    field: 'ParentName',
                    title: 'Phòng ban gốc',
                    textAlign: 'left',
                },
                {
                    field: 'CreateDate',
                    title: 'Ngày tạo',
                    textAlign: 'center',
                    template: function (row) {
                        // callback function support for column rendering
                        return formattedDate(row.CreateDate);
                    },
                },
                {
                    field: 'Status',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    // callback function support for column rendering
                    template: function (row) {
                        if (row.Status == 1) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Hoạt Động</span>';
                        } else if (row.Status == 0) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Khóa</span>';
                        }
                    },
                }
                ,
                {
                    field: 'Action',
                    title: 'Hành động',
                    sortable: false,
                    width: 160,
                    template: function (row, index, datatable) {
                        var html = '';
                        return '\
                          <a class="btn btn-primary btn-icon btn-sm" title="Sửa" data-toggle="modal" data-target="#modal_edit_department" onclick="departmentList.ShowModal('+ row.DepartmentID + ',\'' + row.DepartmentName + '\',' + row.Status + ',' + row.ParentID + ',' + row.AppID + ')">\
                            <i class="fa fa-edit" ></i>\
                            </a>\
					';
                        return html;
                    }
                }
            ]
        });
    };
    var GridRefresh = function () {
        baseCommon.ButtonSubmit(false, '#btnGetData');
        var grid = $("#dv_result_department").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };
    var Init = function () {
        GridRefresh();
        $("#sl_search_status").on('change', function (e) {
            GridRefresh();
        });
        $("#txt_search_department").on('keypress', function (e) {
            GridRefresh();
        });
        $("#btnGetData").on('click', function (e) {
            GridRefresh();
        });
        submitFormSave();
    };
    var ShowModal = function (id = 0, name = '', status = 0, parentID = 0, appID = 0) {
        var form = $('#btn_save_department').closest('form');
        form.validate().destroy();
        $("#hdd_edit_departmentID").val(id);
        $('#txt_edit_deparment').val(name);
        $('#ParentID').val(parentID).change();
        $('#AppID').val(appID).change();

        if (status == 1) {
            $('#kt_switch_2').prop('checked', true).trigger('change');
        } else {
            $('#kt_switch_2').prop('checked', false).trigger('change');
        }
        if (id == 0)//form tạo mới
        {
            $('#header_edit_department').text('Thêm mới');
            $('#btn_save_department').html('<i class="fa fa-save"></i>Thêm mới');
        }
        else {
            $('#header_edit_department').text('Cập nhật');
            $('#btn_save_department').html('<i class="fa fa-save"></i>Cập nhật');
        }
        $('#modal_edit_department').modal('toggle');
    }
    var submitFormSave = function () {
        $('#btn_save_department').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    DepartmentName: {
                        required: true,
                    },
                    AppID: {
                        required: true,
                    }
                },
                messages: {
                    DepartmentName: {
                        required: "Vui lòng nhập phòng ban",
                    }, AppID: {
                        required: "Vui lòng chọn phân hệ",
                    }
                }
            });

            if (!form.valid()) {
                return;
            }
            debugger;
            var IsActive = $('#kt_switch_2').prop('checked') ? 1 : 0;
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: '/Department/AddOrUpDate',
                method: 'POST',
                data: {
                    Status: IsActive
                },
                success: function (response, status, xhr, $form) {
                    console.log(response)
                    if (response.result == 1) {
                        showMesage("success", response.message);
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        debugger;
                        GridRefresh();
                        //$('#hdd_edit_permissionID').val(response.data);
                        //$('#header_edit_permission').text('Cập nhật Link API');
                        //$('#btn_save_permission').html('<i class="fa fa-save"></i>Cập nhật');
                        //$('#sl_search_status').change();
                    } else {
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        showMesage("danger", response.message);
                    }
                }
            });
        });
    }
    return {
        Init: Init,
        ShowModal: ShowModal,
        submitFormSave: submitFormSave
    };
}

