﻿var datatable;
var _loanId; 
var recordGridLoan = 0;
var btn;
var loan = new function () {  
    var SearchCustomer = function () {
        Clear_Info();
        var code = $("#txt_search").val().trim(); 
        if (code != null && code != "") {
            code = code.trim();
        } else {
            baseCommon.ShowErrorNotLoad("Bạn chưa nhập thông tin tìm kiếm!");
            ButtonSubmit(true, '#btnGetData');
            return;
        }
        var data = {
            "FromDate": "", "ToDate": "",
            "LoanCode": code,
            "LoanStatus": 1, "ShopID": -111, "LenderID": -111,
            "ProductID": -111,
            "PageIndex": 1,
            "PageSize": 1
        };
        baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetLstLoanInfoByCondition, JSON.stringify(data), function (res) {
            if (res.Result == 1) {
                if (res.Data != null && res.Data.Data != null && res.Data.Data.length > 0) {
                    baseCommon.AjaxSuccess('GET',baseCommon.Endpoint.GetLoanByID + res.Data.Data[0].LoanID,'', function (respone) {
                        if (respone.Result == 1) {
                            $('#CustomerName').val(res.Data.Data[0].CustomerName);
                            $("#hdd_loanID").val(res.Data.Data[0].LoanID);
                            $("#UnitTimeName").html(res.Data.Data[0].UnitTimeName);
                            $('#LoanTime').val(respone.Data.LoanTime);
                            $('#RateType').val(respone.Data.RateType).change();
                            $('#RateInterest').val(respone.Data.RateInterest);
                            $('#RateConsultant').val(respone.Data.RateConsultant);
                            $('#RateService').val(respone.Data.RateService);
                            var date = moment(respone.Data.FromDate);
                            var createDate = date.format("DD/MM/YYYY");
                            $('#StrFromDate').val(createDate);
                        } 
                    }, "Chưa lấy được thông tin đơn vay, vui lòng thử lại!"); 
                } else {
                    baseCommon.ShowErrorNotLoad("Không tìm thấy khách hàng!");
                }
            }

            ButtonSubmit(true, '#btnGetData');
        }, function (e) {
            if (e.status == 401) {
                window.open(LoginUrl, "_self");
            }
                baseCommon.ShowErrorNotLoad("Thao tác thất bại, vui lòng thử lại sau!"); 
                ButtonSubmit(true, '#btnGetData');
        }); 
    }; 
    var Init = function () {
        Clear_Info();
        $('#btnGetData').on('click', function () {
            ButtonSubmit(false,'#btnGetData');
            SearchCustomer(); 
        })
    };  
    var Save = function () { 
        Save_Visible(false);
        var LoanID = $('#hdd_loanID').val();
        var LoanTime = $('#LoanTime').val();
        var RateType =$('#RateType').val();
        var RateInterest = $('#RateInterest').val();
        var RateConsultant = $('#RateConsultant').val();
        var RateService = $('#RateService').val();
        var StrFromDate = $('#StrFromDate').val();
        var CreateBy = baseCommon.GetUser().UserID;
        if (typeof CreateBy == "undefined" || CreateBy == null || CreateBy == 0) {
            baseCommon.ShowErrorNotLoad("Cập nhật không thành công vui lòng thử lại sau!");
            Save_Visible(true);
            return;
        } 
        if (typeof LoanTime == "undefined" || LoanTime == null || LoanTime == 0 || LoanTime == '') {
            baseCommon.ShowErrorNotLoad("Bạn chưa nhập thời gian vay!");
            Save_Visible(true);
            return;
        }
        if (typeof LoanID == "undefined" || LoanID == null || LoanID == 0 || LoanID  == '') {
            baseCommon.ShowErrorNotLoad("Thông tin đơn vay không hợp lệ, vui lòng thử lại!");
            Save_Visible(true);
            return;
        }
        if (typeof RateType == "undefined" || RateType == null || RateType == 0) {
            baseCommon.ShowErrorNotLoad("Bạn chưa chọn hình thức vay!");
            Save_Visible(true);
            return;
        }
        if (typeof RateInterest == "undefined" || RateInterest == null || RateInterest == '') {
            baseCommon.ShowErrorNotLoad("Bạn chưa nhập Rate Interest!");
            Save_Visible(true);
            return;
        }
        if (typeof RateConsultant == "undefined" || RateConsultant == null ||  RateConsultant == '') {
            baseCommon.ShowErrorNotLoad("Bạn chưa nhập tiền tư vấn!");
            Save_Visible(true);
            return;
        }
        if (typeof RateService == "undefined" || RateService == null ||  RateService == '') {
            baseCommon.ShowErrorNotLoad("Bạn chưa nhập phí dịch vụ!");
            Save_Visible(true);
            return;
        }
        if (typeof StrFromDate == "undefined" || StrFromDate == null || StrFromDate == "") {
            baseCommon.ShowErrorNotLoad("Bạn chưa nhập ngày giải ngân!");
            Save_Visible(true);
            return;
        }
        var model = {
            LoanID: parseFloat(LoanID),
            LoanTime: parseInt(LoanTime),
            RateType: parseInt(RateType),
            RateInterest: parseInt(RateInterest),
            RateConsultant: parseInt(RateConsultant),
            RateService: parseInt(RateService),
            StrFromDate: StrFromDate.replaceAll('/','-'),
            CreateBy: CreateBy, 
        };
        baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.UpdateInforPaymentLoan, JSON.stringify(model), function (respone) {
            if (respone.Result > 0) {
                baseCommon.ShowSuccess(respone.Message);
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }  
            Save_Visible(true);
        }, "Dữ liệu nhập vào không hợp lệ, bạn vui lòng kiểm tra lại!" ,
            "loan.Save_Visible(true);  baseCommon.ShowErrorNotLoad(\"Dữ liệu nhập vào không hợp lệ, bạn vui lòng kiểm tra lại!\");"); 
    }
    var Save_Visible = function (isVisible) {
        if (!isVisible) {
            $("#btn_save").addClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
            $("#btn_save").prop("disabled", true);
        } else {
            $("#btn_save").removeClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
            $("#btn_save").prop("disabled", false);
        }
    } 
    var ButtonSubmit = function (isVisible,id) {
        if (!isVisible) {
            $(id).addClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
            $(id).prop("disabled", true);
        } else {
            $(id).removeClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
            $(id).prop("disabled", false);
        }
    }
    var Clear_Info = function () {
        $('#CustomerName').val('');
        $("#hdd_loanID").val(0);
        $("#UnitTimeName").html('');
        $('#LoanTime').val(0);
        $('#RateType').val(0).change();
        $('#RateInterest').val(0);
        $('#StrFromDate').val('');
        $('#RateConsultant').val(0);
        $('#RateService').val(0);
    }
    return {
        Init: Init, 
        SearchCustomer: SearchCustomer,
        Save: Save,
        Save_Visible: Save_Visible
    };
}
$(document).ready(function () {
    loan.Init();
});