﻿var meta = function () {
    var loadBankSelect = function (className) {
        $('.' + className).html('');
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetAllBank, "", function (respone) {
            if (respone.Result == 1) {
                baseCommon.DataSource("." + className, "Tất cả", "Value", "Text", 0, respone.Data);
            }
        }, "Chưa lấy được danh sách ngân hàng!");
    };
    var loadUserByGroup = function (className, groupID) {
        $('.' + className).html('');
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.UserByGroup + groupID, '', function (respone) {
            if (respone.Result == 1) {
                baseCommon.DataSource("." + className, "Chọn nhân viên", "Value", "Text", 0, respone.Data);
            }
        }, "Chưa lấy được danh sách nhân viên!");
    };
    var loadAffLender = function (className) {
        $('.' + className).html('');
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.AffLender, '', function (respone) {
            if (respone.Result == 1) {
                baseCommon.DataSource("." + className, "Chọn người giới thiệu", "Value", "Text", 0, respone.Data);
            }
        }, "Chưa lấy được danh sách aff!");
    };
    var loadBankCard = function (className) {
        $('.' + className).html('');
        baseCommon.AjaxDone("GET", baseCommon.Endpoint.GetBankCard + '-1&Status=1', '', function (respone) {
            console.log(respone);
            if (respone.Result == 1) {
                baseCommon.DataSource("." + className, "Chọn thẻ ngân hàng", "Value", "Text", 0, respone.Data);
            }
        }, "Chưa lấy được danh sách bankcard!");
    };
    var loadDepartmentParentSelect = function (className) {
        $('.' + className).html('');
        baseCommon.AjaxDone("GET", `${baseCommon.Endpoint.GetDepartment}?Status=1&PageInde=1&PageSize=100000`, '', function (respone) {
            if (respone.Result == 1) {
                respone.Data = jQuery.grep(respone.Data, function (a) {
                    return a.ParentID == 0;
                });
                $('.' + className).append('<option value="0">Phòng ban gốc</option>');
                for (var i = 0; i < respone.Data.length; i++) {
                    $('.' + className).append('<option value="' + respone.Data[i].DepartmentID + '">' + respone.Data[i].DepartmentName + ' </option>');
                }
            }
        }, "Chưa lấy được danh sách !");
    };

    var loadFileForm = function (className, typeFile) {
        $('.' + className).html('');
        var request = {
            TypeFile: typeFile
        }
        baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetLstTypeFileForm, JSON.stringify(request), function (respone) {
            if (respone.Result == 1) {
                baseCommon.DataSource("." + className, "Tất cả", "Value", "Text", 0, respone.Data);
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        }, "Chưa lấy được danh sách FileForm !");
    }

    var loadBookDebtSelect = function (className, isCreate = 0) {
        $('.' + className).html('');
        var request = {
            KeySearch: '',
            Status: 1
        }
        baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.ListBookDebt, JSON.stringify(request), function (respone) {
            if (respone.Result == 1) {
                if (isCreate == 0) {
                    baseCommon.DataSource("." + className, "Tất cả", "BookDebtID", "BookDebtName", 0, respone.Data);
                }
                else {
                    baseCommon.DataSource("." + className, "Chọn giá trị", "BookDebtID", "BookDebtName", 0, respone.Data);
                }
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        }, "Chưa lấy được danh sách");
    }

    var loadapprovalLevelBookDebtSelect = function (className, isCreate = 0, isUserSetting = 0) {
        $('.' + className).html('');
        var request = {
            KeySearch: '',
            Status: 1
        }
        baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetListApprovalLevelBookDebt, JSON.stringify(request), function (respone) {
            if (respone.Result == 1) {
                if (isUserSetting != 0) {
                    if (isCreate == 0) {
                        $('.' + className).append('<option value="-111">Tất cả</option>');
                        baseCommon.DataSource("." + className, "Tạo tờ trình", "ApprovalLevelBookDebtID", "ApprovalLevelBookDebtName", 0, respone.Data, 0, 0);
                    } else {
                        baseCommon.DataSource("." + className, "Chọn giá trị", "ApprovalLevelBookDebtID", "ApprovalLevelBookDebtName", 0, respone.Data, 0, 0);
                    }
                } else {
                    if (isCreate == 0) {
                        baseCommon.DataSource("." + className, "Tất cả", "ApprovalLevelBookDebtID", "ApprovalLevelBookDebtName", 0, respone.Data);
                    } else {
                        baseCommon.DataSource("." + className, "Chọn giá trị", "ApprovalLevelBookDebtID", "ApprovalLevelBookDebtName", 0, respone.Data);
                    }
                }
            } else {
                baseCommon.ShowErrorNotLoad(respone.Message);
            }
        }, "Chưa lấy được danh sách");
    }




    var loadSelectInputLender = function (className) {
        $("." + className).select2({
            placeholder: "Chọn lender",
            allowClear: true,
            ajax: {
                url: baseCommon.Endpoint.GetLenderSearch,
                type: 'get',
                headers: {
                    'Authorization': baseCommon.GetToken()
                },
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        GeneralSearch: params.term, // search term
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    if (data != null && typeof data != "undefined" && data.Result == 1) {
                        return {
                            results: repoConvert(data.Data),
                            pagination: {
                                more: (params.page * 30) < data.Total
                            }
                        };
                    }
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 5,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
        function formatRepo(repo) {

            if (repo.loading) return repo.text;
            var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__title'>" + repo.text + "</div>";
            return markup;
        }
        function formatRepoSelection(repo) {

            return repo.text;
        }
        function repoConvert(data) {

            var newObject = [];
            if (data != null && data != "") {
                $.each(data, function (key, value) {
                    var newRepo = { id: value.ID, text: value.FullName };
                    newObject.push(newRepo);
                });
            }
            return newObject;
        }

    }
    return {
        loadBankSelect: loadBankSelect,
        loadUserByGroup: loadUserByGroup,
        loadAffLender: loadAffLender,
        loadBankCard: loadBankCard,
        loadSelectInputLender: loadSelectInputLender,
        loadDepartmentParentSelect: loadDepartmentParentSelect,
        loadFileForm: loadFileForm,
        loadBookDebtSelect: loadBookDebtSelect,
        loadapprovalLevelBookDebtSelect: loadapprovalLevelBookDebtSelect
    };
}();
