﻿var recordGrid = 0;
var logCallAg = new function () {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var linkUrl = $('#txt_search_linkUrl').val();
                var dataPost = $('#txt_search_dataPost').val();
                var datetime = $('#datetimeSearch').val();
                var status = $('#sl_search_status').val();

                var data = {
                    LinkUrl: linkUrl,
                    DataPost: dataPost,
                    StrDate: datetime,
                    Status: parseInt(status),
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxDone('POST', baseCommon.Endpoint.GetLogCallLMS, JSON.stringify(data), function (res) {
                    if (res.Result == 1) {
                        options.success(res);
                    }
                }, "Thao tác thất bại, vui lòng thử lại sau!");
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var UpdateStatusFailToWaitingLogCallLMS = function (LogCallLMSID) {
        swal.fire({
            html: "Bạn có chắc chắn muốn Đổi trạng thái từ <b>Fail</b> về <b>Chờ Call</b> không?",
            type: "warning",
            showCancelButton: !0,
            cancelButtonText: "Hủy",
            confirmButtonText: "Đồng ý"
        }).then(function (result) {
            if (result.value) {
                var modelUpdate = {
                    "LogCallLMSID": parseInt(LogCallLMSID),
                };
                baseCommon.AjaxDone("POST", baseCommon.Endpoint.UpdateStatusFailToWaitingLogCallLMS, JSON.stringify(modelUpdate), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        GridRefresh()
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                }, "Mở khóa không thành công vui lòng thử lại!");
            }
        });
    };
    var InitGrid = function (objQuery) {
        $("#grid").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            selectable: "row",
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 2 && i != 4 && i != 5 && i != 6) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'LinkUrl',
                    title: 'LinkUrl',
                    template: function (row) {
                        return '<span title="' + row.LinkUrl + '">' + row.LinkUrl + '</span>';
                    }
                },
                {
                    field: 'DataPost',
                    width: 500,
                    title: 'DataPost',
                },
                {
                    field: 'Method',
                    title: 'Method',
                    template: function (row) {
                        if (row.Method == 1) {
                            return '<span>GET</span>';
                        }
                        if (row.Method == 2) {
                            return '<span">POST</span>';
                        }
                    }
                },
                {
                    field: 'ResultResponseApi',
                    title: 'ResultResponseApi',
                    width: 350
                },
                {
                    title: 'Timeline',
                    width: 300,
                    attributes: { style: "text-align:center;" },
                    headerAttributes: { style: "text-align: center" },
                    template: function (row) {
                        var date = moment(row.CreateDate);
                        var date2 = moment(row.ModifyDate);
                        return '<span> Ngày tạo : ' + date.format("DD/MM/YYYY HH:mm:ss") + '<br>\
                        <span>Ngày cập nhật : ' + date2.format("DD/MM/YYYY HH:mm:ss") + '</span>\
                        </span>';
                    }
                },
                {
                    field: 'Status',
                    title: 'Trạng thái',
                    width: 150,
                    attributes: { style: "text-align:center;" },
                    template: function (row) {
                        if (row.Status == 0) {
                            return '<span class="kt-badge kt-badge--info kt-badge--inline kt-badge--pill">Chờ Call</span>';
                        }
                        if (row.Status == 1) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Success</span>';
                        }
                        if (row.Status == 2) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Call Fail</span>';
                        }
                        if (row.Status == 3) {
                            return '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">Đã block</span>';
                        }
                    }
                },
                {
                    title: "Hành động",
                    selectable: false,
                    template: function (row) {
                        var html = '';
                        if (row.Status == 2) {
                            html = '<button class="btn btn-outline-success btn-icon" title="Đổi trạng thái" onclick="logCallAg.UpdateStatusFailToWaitingLogCallLMS(' + row.LogCallLMSID + ')">\
                                            <i onclick="javascript:;" class="fa fa-reply"></i></button>';
                        }
                        return html;
                    }
                }
            ]
        });
    };
    var GridRefresh = function () {
        var grid = $("#grid").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid()
        }
    };
    var Init = function () {
        $('#btnGetData').on('click', function () {
            GridRefresh();
        })
        $("#sl_search_status,#datetimeSearch,#txt_search_dataPost,#txt_search_linkUrl").on('change', function (e) {
            GridRefresh();
        });
        GridRefresh();
    }
    return {
        Init: Init,
        GridRefresh: GridRefresh,
        UpdateStatusFailToWaitingLogCallLMS: UpdateStatusFailToWaitingLogCallLMS,
    };
}
$(document).ready(function () {
    logCallAg.Init();
});