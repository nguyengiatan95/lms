﻿var recordGrid = 0;
var logLMSCallAg = new function () {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var jsonRq = $('#txt_search_jsonRq').val();
                var path = $('#txt_search_path').val();
                var datetime = $('#datetimeSearch').val();
                var status = $('#sl_search_status').val();

                var data = {
                    JsonRequest: jsonRq,
                    Path: path,
                    StrDate: datetime,
                    Status: parseInt(status),
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxDone('POST', baseCommon.Endpoint.GetQueueCallAPI, JSON.stringify(data), function (res) {
                    if (res.Result == 1) {
                        options.success(res);
                    }
                }, "Thao tác thất bại, vui lòng thử lại sau!");
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var UpdateStatusFailToWaitingGetQueueCallAPI = function (QueueCallAPIID) {
        swal.fire({
            html: "Bạn có chắc chắn muốn Đổi trạng thái từ <b>Fail</b> về <b>Chờ Call</b> không?",
            type: "warning",
            showCancelButton: !0,
            cancelButtonText: "Hủy",
            confirmButtonText: "Đồng ý"
        }).then(function (result) {
            if (result.value) {
                var modelUpdate = {
                    "QueueCallAPIID": parseInt(QueueCallAPIID),
                };
                baseCommon.AjaxDone("POST", baseCommon.Endpoint.UpdateStatusFailToWaitingGetQueueCallAPI, JSON.stringify(modelUpdate), function (respone) {
                    if (respone.Result == 1) {
                        baseCommon.ShowSuccess(respone.Message);
                        GridRefresh()
                    } else {
                        baseCommon.ShowErrorNotLoad(respone.Message);
                    }
                }, "Mở khóa không thành công vui lòng thử lại!");
            }
        });
    };
    var InitGrid = function (objQuery) {
        $("#grid").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            selectable: "row",
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 1 && i != 2 && i != 3 && i != 4 && i != 7 && i != 8) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'Domain',
                    title: 'Domain',
                    width: 250
                },
                {
                    field: 'JsonRequest',
                    width: 250,
                    title: 'JsonRequest',
                },
                {
                    field: 'JsonResponse',
                    width: 500,
                    title: 'JsonResponse',
                },

                {
                    field: 'Method',
                    title: 'Method',
                    width: 150,
                },
                {
                    field: 'Path',
                    title: 'Path',
                    width: 300,
                },
                {
                    field: 'Description',
                    title: 'Nội dung',
                    width: 300,
                    template: function (row) {
                        return `${row.Description}`;
                    }
                },
                {
                    title: 'Timeline',
                    width: 300,
                    attributes: { style: "text-align:center;" },
                    headerAttributes: { style: "text-align: center" },
                    template: function (row) {
                        var date = moment(row.CreateDate).format("DD/MM/YYYY HH:mm:ss");
                        var date2 = moment(row.LastModifyDate).format("DD/MM/YYYY HH:mm:ss");
                        if (date2 == 'Invalid date') {
                            date2 = ''
                        }
                        return '<span> Ngày tạo : ' + date + '<br>\
                        <span>Ngày cập nhật : ' + date2 + '</span>\
                        </span>';
                    }
                },
                {
                    field: 'Status',
                    title: 'Trạng thái',
                    width: 150,
                    attributes: { style: "text-align:center;" },
                    template: function (row) {
                        if (row.Status == 0) {
                            return '<span class="kt-badge kt-badge--info kt-badge--inline kt-badge--pill">Chờ Call</span>';
                        }
                        if (row.Status == 1) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Success</span>';
                        }
                        if (row.Status == 2) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Call Fail</span>';
                        }
                        if (row.Status == 3) {
                            return '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">Đã block</span>';
                        }
                    }
                },
                {
                    title: "Hành động",
                    selectable: false,
                    width: 150,
                    template: function (row) {
                        var html = '';
                        if (row.Status == 2) {
                            html = '<button class="btn btn-outline-success btn-icon" title="Đổi trạng thái" onclick="logLMSCallAg.UpdateStatusFailToWaitingGetQueueCallAPI(' + row.QueueCallAPIID + ')">\
                                            <i onclick="javascript:;" class="fa fa-reply"></i></button>';
                        }
                        return html;
                    }
                }
            ]
        });
    };
    var GridRefresh = function () {
        var grid = $("#grid").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid()
        }
    };
    var Init = function () {
        $('#btnGetData').on('click', function () {
            GridRefresh();
        })
        $("#sl_search_status,#datetimeSearch,#txt_search_path,#txt_search_jsonRq").on('change', function (e) {
            GridRefresh();
        });
        GridRefresh();
    }
    return {
        Init: Init,
        GridRefresh: GridRefresh,
        UpdateStatusFailToWaitingGetQueueCallAPI: UpdateStatusFailToWaitingGetQueueCallAPI,
    };
}
$(document).ready(function () {
    logLMSCallAg.Init();
});