﻿
var CheckAll = 0, pageSize = baseCommon.Option.AllPage, checkedIds = [], dataSourceGroupPermission, dataGroupPermission;
var addOrEdit = function () {  
    var loadInfo = function (groupId = 0, groupName = '', status = 1) {
        var form = $('#btn_save').closest('form');
        form.validate().destroy();
        $('#txt_edit').val(groupName); 
        $('#hdd_Id').val(groupId);
        if (status == 1) {
            $('#kt_switch_2').prop('checked', true).trigger('change');
        } else {
            $('#kt_switch_2').prop('checked', false).trigger('change');
        }
        if (groupId == 0)//form tạo mới
        {
            $('#headerId').text('Thêm mới');
            $('#btn_save').html('<i class="fa fa-save"></i>Thêm mới');
        }
        else {
            $('#headerId').text('Cập nhật');
            $('#btn_save').html('<i class="fa fa-save"></i>Cập nhật');
        }
    };
    var sumitFormSave = function () {
        $('#btn_save').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    GroupName: {
                        required: true,
                    }, 
                },
                messages: {
                    GroupName: {
                        required: "Vui lòng nhập tên Group",
                    } 
                }
            });

            if (!form.valid()) {
                return;
            }
            var status = $('#kt_switch_2').prop('checked') ? 1 : 0;
            var id = $('#hdd_Id').val(); 
            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.ajaxSubmit({
                url: '/Group/AddOrUpDate',
                method: 'POST',
                data: {
                    GroupID: id, Status: status
                },
                success: function (response, status, xhr, $form) { 
                    if (response.result == 1) {
                        showMesage("success", id == 0 ? "Tạo mới thành công" : "Cập nhật thành công");
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        $('#hdd_Id').val(response.data.groupID);
                        $('#headerId').text('Cập nhật');
                        $('#btn_save').html('<i class="fa fa-save"></i>Cập nhật');
                        $('#sl_search_status').change();
                        $('#modal_add_edit').modal('hide')
                    } else {
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        showMesage("danger", response.message);
                    }
                }
            });
        });
    }
     
    //modal Phân quyền api cho group
    var LoadBoxGroupPermission = function (groupId, groupName) {
        $('#GroupID').val(groupId); 
        $('#lblGroupName').html(groupName); 
        $('#IsHasPermission').on('change', function () {
            GridRefresh();
        });
        $("#GeneralSearch").on('keypress', function (e) {
            if (e.which === 13) {
                GridRefresh();
            }
        });
        setTimeout(function () { 
            GridRefresh();
        }, 500)
    } 
   dataSourceGroupPermission = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                checkedIds = [];
                var GroupID = $('#GroupID').val();
                var GeneralSearch = $('#GeneralSearch').val();
                var IsHasPermission = $('#IsHasPermission').val(); 
                var pageIndex = options.data.page;
                var data = {
                    GroupID: parseFloat(GroupID),
                    GeneralSearch: GeneralSearch,
                    IsHasPermission: parseFloat(IsHasPermission),
                    PageIndex: pageIndex,
                    PageSize: baseCommon.Option.AllPage //options.data.pageSize
                }; 
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GroupPermissionGetByGroupID, JSON.stringify(data), function (res) { 
                    $("#Seaching").hide(); 
                    if (res.Result != 1) {
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                    dataGroupPermission = res.Data;
                    options.success(res);
                } );
            }
        },
        serverPaging: false,
        schema: {
            model: {
                id: "PermissionID"
            },
            total: function (response) {
                var total = 0; 
                try {
                    if (response != null && response.Data != null && response.Data.LstPermission != null && response.Data.LstPermission.length > 0) { LstHavePermissionID = 
                        total = response.Data.LstPermission[0].TotalCount; 
                    }
                } catch (e) {
                    total = 0;
                }
                pageSize = total;
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response != null && response.Data != null && response.Data.LstPermission != null && response.Data.LstPermission.length > 0) {  
                    return response.Data.LstPermission; // total is returned in the "total" field of the response
                }   
                return [];
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });   
    var InitGridGroupPermission = function () {
        $("#groupPermission_grid").kendoGrid({ 
            dataSource: dataSourceGroupPermission,
            pageable: {
                pageSizes: [20, 30, 50, 100, 'All' ]
            },
            resizable: true,
            persistSelection: true,  
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * pageSize;//this.dataSource.pageSize();
            },
            dataBound: function (e) { 
                for (var i = 0; i < this.columns.length; i++) {
                    if (i > 1) {
                        this.autoFitColumn(i);
                    }
                }    
                var grid = this;

                var rows = grid.items(); 
                $(rows).each(function (e) {
                    var row = this;
                    var dataItem = grid.dataItem(row); 
                    if (dataItem.HasPermission) {
                        grid.select(row);
                    }
                });  
                
            },
            columns: [ 
                { 
                    selectable: true,
                    width: 80 
                }, 
                {
                    title: "STT",
                    template: "#= ++recordGrid #",
                    width: 80
                },  
                { 
                    field: 'LinkApi',
                    title: 'Link Api'
                },{
                    field: 'DisplayText',
                    title: 'Tên hiển thị'
                },
                {
                    field: 'HasPermission',
                    title: 'Trạng thái', 
                    template: function (row) {
                        if (row.HasPermission) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Đã phân quyền</span>';
                        }else {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Chưa phân quyền</span>';
                        }
                    }
                }//,
                //{
                //    headerTemplate: '<label class="kt-checkbox kt-checkbox--tick kt-checkbox--danger"><input type="checkbox" id="CheckAll" onclick="addOrEdit.CheckAllOnchange()" /><span style="margin-top:-6px"></span></label>',
                //    template: function (row) { 
                //        if (!row.HasPermission) {
                //            return ' <label class="kt-checkbox kt-checkbox--brand"><input class="chkAPI" id="cb_' + row.PermissionID + '" value="' + row.PermissionID + '" name="chkAPI" type="checkbox" onclick="addOrEdit.addRemovePermissionToArray(1,' + row.PermissionID + ')"><span style="margin-top:-6px"></span> </label>';
                //        }
                //        else {
                //            return ' <label class="kt-checkbox kt-checkbox--brand"><input checked class="chkAPI" id="cb_' + row.PermissionID + '" value="' + row.PermissionID + '" name="chkAPI" type="checkbox" onclick="addOrEdit.addRemovePermissionToArray(0,' + row.PermissionID + ')"><span style="margin-top:-6px"></span> </label>';
                //        }
                //    }
                //}
            ]
        });  
        //InitSelectCheckBox();
    };
    var GridRefresh = function () { 
        $("#Seaching").show(); 
        var grid = $("#groupPermission_grid").data("kendoGrid");
        if (grid != null) { 
            grid.dataSource.read().then(function () {
                grid.dataSource.page(1);
            }); 
        } else {
            InitGridGroupPermission();
        } 
    };   
    var SavePermissionsToGroup = function () {  
        try {   
            var grid = $("#groupPermission_grid").data("kendoGrid"); 

            var selectedId = dataGroupPermission.LstHavePermissionID;  
            var views = grid.dataSource.view();
            var items = grid.items()
            
            $.each(items, function (index, item) {
                var indx = $.inArray(views[index].PermissionID, selectedId);
                var indxlistChecked = item.firstElementChild.firstElementChild.checked;
                if (indxlistChecked) { // nếu có chọn
                    if (indx < 0) { // nếu trong list phân quyền chưa có thì thêm vào, có rồi thì thôi
                        selectedId.push(views[index].PermissionID);
                    }
                } else { // nếu ko chọn
                    if (indx >= 0) { // nếu trong list phân quyền có thì xóa đi, chưa có thì thôi
                        selectedId.splice(indx, 1);
                    }
                }
            }); 

            var GroupID = $('#GroupID').val();
            if ((selectedId == null || typeof selectedId == "undefined" || selectedId == "" || selectedId.length == 0)) {
                selectedId = [];
            }
            if (GroupID == null || typeof GroupID == "undefined" || GroupID == "" || GroupID == 0) {
                baseCommon.ShowErrorNotLoad("Thao tác không thành công, vui lòng thử lại!");
                return;
            } 
            var data = {
                CheckAll: 0,
                GroupID: parseFloat(GroupID),
                LstPermissionID: selectedId
            };
            baseCommon.ButtonSubmit(false, '#btnGetData');
            baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.CreateGroupPermission, JSON.stringify(data), function (res) {
                baseCommon.ButtonSubmit(true, '#btnGetData');
                GridRefresh();  
                if (res.Result == 1) {
                    baseCommon.ShowSuccess("Thành công!");
                } else {
                    baseCommon.ShowErrorNotLoad(res.Message);
                }
            }); 
        } catch (e) {
            baseCommon.ShowErrorNotLoad(e); 
        }
    };  
    ///////////////////////// MENU  
    var LoadBoxGroupMenu = function (groupId, groupName) {
        $('#AppID').on('change', function () {
            getPermissionStaff();
        });
        loadTrees('',0,0)
        $('#GroupID').val(groupId);
        $('#lblGroupName_Menu').html(groupName); 
        baseCommon.DataSource("#AppID", "Chọn phân hệ", "Key", "Value", 0, menu_AppIDs,0);
        setTimeout(function () { getPermissionStaff(); }, 500)
    } 
    var getPermissionStaff = function () { 
        var GroupID = $('#GroupID').val();
        var AppID = $('#AppID').val();
        if (AppID == null || AppID == "" || AppID == 0) {
            return;
        }
        if (GroupID != "") { 
            if (AppID == null || typeof AppID == "undefined" || AppID == "") {
                AppID = 0;
            }
            var data = { GroupID: parseInt(GroupID), AppID: parseInt(AppID) }
            baseCommon.AjaxSuccess('GET', baseCommon.Endpoint.GetMenuByGroupID, data, function (res) {
                $("#Seaching").hide();
                if (res.Result == 1) {
                    loadTrees(res.Data, GroupID, AppID);
                } else { 
                    baseCommon.ShowErrorNotLoad(res.Message);
                }
            });
        }
    };
    var loadTrees = function (jsondata, GroupID, appId) { 
        var dataJson = JSON.stringify(jsondata).toLowerCase();
        jsondata = JSON.parse(dataJson);
        try {
            $.each(jsondata, function (index, value) {
                value.text = value.text.toUpperCase();
                value.state.opened = true;
                try {
                    $.each(value.children, function (index, value2) {
                        value2.text = value2.text.toUpperCase();
                    });
                } catch (e) {

                } 
            });
        } catch (e) {

        } 
        $("#kt_tree_permission").jstree('destroy');
        $('#kt_tree_permission').on('changed.jstree', function (e, data) {
            if (data.action == "select_node" || data.action == "deselect_node") { 
                var model = {
                    LstMenuID: data.selected.map(Number), GroupID: parseInt(GroupID), AppID: parseInt(appId)
                };
                baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.CreateMenuGroup, JSON.stringify(model), function (res) { 
                    console.log(data);
                    if (res.Result == 1) { 
                        baseCommon.ShowSuccess(res.Message);
                    } else { 
                        baseCommon.ShowErrorNotLoad(res.Message);
                    }
                });
            }
        }).jstree({
            'plugins': ["wholerow", "checkbox", "types"],
            'core': {
                "check_callback": true,
                "themes": {
                    "responsive": true
                },
                'data': jsondata
            },
            "types": {
                "default": {
                    "icon": "fa fa-folder kt--font-success"
                },
                "file": {
                    "icon": "fa fa-file  kt--font-warning"
                }
            },
        });
    };  
    var CheckAllOnchange = function () {
        var chk = $('#CheckAll').is(":checked");
        if (chk) {
            CheckAll = 1;
            $(".chkAPI").prop('checked', true);
        } else {
            CheckAll = 2;
            $(".chkAPI").prop('checked', false);
        } 
    }  
    return {
        CheckAllOnchange: CheckAllOnchange, 
        SavePermissionsToGroup: SavePermissionsToGroup,
        LoadBoxGroupMenu: LoadBoxGroupMenu,
        loadInfo: loadInfo,
        sumitFormSave: sumitFormSave,
        LoadBoxGroupPermission: LoadBoxGroupPermission 
    }
}()

