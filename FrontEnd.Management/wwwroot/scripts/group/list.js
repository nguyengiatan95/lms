﻿var list = function () {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var data = {
                    GeneralSearch: $('#txt_search').val().trim(),
                    Status: parseInt($('#sl_search_status').val().trim()),
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxDone('GET', baseCommon.Endpoint.GetGroup, (data), function (res) {
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    if (res.Result == 1) {
                        options.success(res);
                    }
                }, "Thao tác thất bại, vui lòng thử lại sau!");
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function (objQuery) {
        $("#dv_result_list").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            selectable: "row",
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 1) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'GroupName',
                    title: 'Tên group',
                    textAlign: 'center',
                    width: 350
                },
                {
                    field: 'Status',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    template: function (row) {
                        if (row.Status == 1) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Hoạt Động</span>';
                        } else if (row.Status == 0) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Khóa</span>';
                        }
                    },
                },
                {
                    field: 'action',
                    title: 'Chức năng',
                    textAlign: 'center',
                    sortable: false,
                    template: function (row) {
                        return '\
                          <a class="btn btn-primary btn-icon btn-sm" title="Sửa" data-toggle="modal" data-target="#modal_add_edit" onclick="addOrEdit.loadInfo('+ row.GroupID + ',\'' + row.GroupName + '\',' + row.Status + ')">\
                            <i class="fa fa-edit" ></i>\
                            </a>\
                          <a class="btn btn-primary btn-icon btn-sm" title="Phân quyền API" data-toggle="modal" data-target="#modal_groupPermission" onclick="addOrEdit.LoadBoxGroupPermission('+ row.GroupID + ',\'' + row.GroupName + '\')">\
                            <i class="fa fa-code-branch" ></i>\
                            </a>\
                          <a class="btn btn-primary btn-icon btn-sm" title="Phân quyền Menu" data-toggle="modal" data-target="#modal_groupMenu" onclick="addOrEdit.LoadBoxGroupMenu('+ row.GroupID + ',\'' + row.GroupName + '\')">\
                            <i class="fa fa-indent" ></i>\
                            </a>\
					';
                    }
                }
            ]
        });
    };
    var GridRefresh = function () {
        baseCommon.ButtonSubmit(false, '#btnGetData');
        var grid = $("#dv_result_list").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };
    var Init = function () {
        GridRefresh();
        $("#sl_search_status").on('change', function (e) {
            GridRefresh();
        });
        $("#txt_search").on('keypress', function (e) {
            GridRefresh();
        });
        $("#btnGetData").on('click', function (e) {
            GridRefresh();
        });
    }
    return {
        GridRefresh: GridRefresh,
        Init: Init,
    }
}();
$(document).ready(function () {
    list.Init();
});