﻿var summaryCompare = function () {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                var data = {
                    StrSearchDate: $('#datetimeSearch').val().trim(),
                    type: parseInt($('#sl_search_MainType').val().trim()),
                    PageIndex: options.data.page,
                    PageSize: options.data.pageSize
                };
                baseCommon.AjaxDone('GET', baseCommon.Endpoint.GetSummaryCompare, (data), function (res) {
                    baseCommon.ButtonSubmit(true, '#btnGetData');
                    if (res.Result == 1) {
                        options.success(res);
                    }
                }, "Thao tác thất bại, vui lòng thử lại sau!");
            }
        },
        serverPaging: true,
        schema: {
            total: function (response) {
                var total = response.Total;
                if (total == 0 && response.Data != null && response.Data.length > 0) {
                    total = response.Data.length;
                }
                return total; // total is returned in the "total" field of the response
            },
            data: function (response) {
                if (response.Data == null) {
                    var fruits = [];
                    return fruits;
                }
                return response.Data; // total is returned in the "total" field of the response
            },
        },
        batch: true,
        pageSize: baseCommon.Option.Pagesize
    });
    var InitGrid = function (objQuery) {
        $("#dv_result_list").kendoGrid({
            dataSource: dataSource,
            pageable: {
                pageSizes: [20, 30, 50, 100],
                messages: {
                    itemsPerPage: "",
                    display: "{0}-{1} của {2} bản ghi",
                    empty: "Không có dữ liệu"
                }
            },
            resizable: true,
            selectable: "row",
            dataBinding: function () {
                recordGrid = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    if (i != 2 && i != 3 && i != 4) {
                        this.autoFitColumn(i);
                    }
                }
            },
            columns: [
                {
                    title: "STT",
                    template: "#= ++recordGrid #"
                },
                {
                    field: 'MainType',
                    title: 'Loại',
                    textAlign: 'left',
                    template: function (row, index, datatable) {
                        return loađMainType(row.MainType);
                    }
                },
                {
                    field: 'DetailNote',
                    title: 'Chỉ số',
                    textAlign: 'left',
                    width: 350
                },
                {
                    field: 'TotalAG',
                    title: 'Số AG',
                    width: 250,
                    attributes: { style: "text-align:right;" },
                    headerAttributes: { style: "text-align: right" },
                    template: function (row, index, datatable) {
                        return html_money(row.TotalAG);
                    }
                },
                {
                    field: 'TotalLMS',
                    title: 'Số LMS',
                    width: 250,
                    attributes: { style: "text-align:right;" },
                    headerAttributes: { style: "text-align: right" },
                    template: function (row, index, datatable) {
                        return html_money(row.TotalLMS);
                    }
                },
                {
                    field: 'Check',
                    title: 'Check',
                    textAlign: 'center',
                    sort: false,
                    attributes: { style: "text-align:center;" },
                    headerAttributes: { style: "text-align: center" },
                    template: function (row, index, datatable) {
                        if (row.TotalLMS != row.TotalAG) {
                            return '<i class="fa fa-window-close kt-font-danger" style="font-size: 1.5rem;" title="Số liệu không khớp"></i>';
                        } else {
                            return '<i class="fa fa-check-square kt-font-success" style="font-size: 1.5rem;" title="Số liệu không khớp"></i>';
                        }
                    }
                },
            ]
        });
    };
    var GridRefresh = function () {
        baseCommon.ButtonSubmit(false, '#btnGetData');
        var grid = $("#dv_result_list").data("kendoGrid");
        if (grid != null) {
            grid.dataSource.page(1);
        } else {
            InitGrid();
        }
    };
    var Init = function () {
        GridRefresh();
        $("#datetimeSearch").on('change', function (e) {
            GridRefresh();
        });
        $("#sl_search_MainType").on('keypress', function (e) {
            GridRefresh();
        });
        $("#btnGetData").on('click', function (e) {
            GridRefresh();
        });
    };
    var loađMainType = function (mainType) {
        switch (mainType) {
            case 1: {
                return '<span class="kt-font-info">Giải ngân</span>';
            }
            case 2: {
                return '<span class="kt-font-success">Lender</span>';
            }
            case 3: {
                return '<span class="kt-font-blue">Loan</span>';
            }
            case 4: {
                return "Hạch Toán";
            }
            case 5: {
                return "Phiếu Thu / Chi";
            }
            case 6: {
                return "Khách hàng";
            }
            case 7: {
                return "Comment";
            }
            case 8: {
                return "Dự thu";
            }
            case 9: {
                return "Hold tiền KH";
            }
            case 10: {
                return "SMS";
            }
            default:
        }
    };
    return {
        GridRefresh: GridRefresh,
        Init: Init,
    }
}();