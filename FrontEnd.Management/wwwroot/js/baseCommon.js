﻿var isClearSession = false, isRefreshToken = false;
var baseCommon = new function () {
    var Option = {
        All: -111,
        Default: -1,
        Pagesize: 10,
        PagesizeExcel: 100000,
        AllPage: 100000
    }, AppId = {
        "Admin": 1,
        "Accountant": 2,
        "Lender": 3,
        "THN": 4
    };
    var Pagesize = 20
    var BaseUrl = {}, Endpoint = {}, UserLoginModel = {}, isCalledRefreshToken = "", LoginUrl_ = LoginUrl;
    var SetBase = function () {
        BaseUrl = {
            "Admin": ApiUrl + "admin/",
            "Loan": ApiUrl + "loan/",
            "Authen": ApiUrl + "authen/",
            "Invoice": ApiUrl + "invoice/",
            "Lender": ApiUrl + "lender/",
            "Customer": ApiUrl + "customer/",
            "Transaction": ApiUrl + "transaction/",
            "Thn": ApiUrl + "thn/"
        };
        Endpoint = {
            "GetLoanByID": BaseUrl["Loan"] + "api/Loan/getLoanByID/",
            "GetLstLoanInfoByCondition": BaseUrl["Loan"] + "api/Loan/GetLstLoanInfoByCondition",
            "GetDepartment": BaseUrl["Admin"] + "api/Department/GetDepartment",
            "UpdateInforPaymentLoan": BaseUrl["Loan"] + "api/Loan/UpdateInforPaymentLoan",
            "CreatePermission": BaseUrl["Admin"] + "api/Permission/CreatePermission",
            "UpdatePermission": BaseUrl["Admin"] + "api/Permission/UpdatePermission",
            "GetByConditionsMenu": BaseUrl["Admin"] + "api/Menu/GetByConditions",
            "CreateUpdateMenu": BaseUrl["Admin"] + "api/Menu/CreateUpdateMenu",
            "GetMenuParent": BaseUrl["Admin"] + "api/Menu/GetMenuParent",
            "GroupPermissionGetByGroupID": BaseUrl["Admin"] + "api/GroupPermission/GetByGroupID",
            "GetMenuByGroupID": BaseUrl["Admin"] + "api/Menu/GetMenuByGroupID",
            "CreateMenuGroup": BaseUrl["Admin"] + "api/Menu/CreateMenuGroup",
            "CreateGroupPermission": BaseUrl["Admin"] + "api/GroupPermission/CreatePermission",
            "GetLogCallLMS": BaseUrl["Admin"] + "api/LogCallLMS/GetLogCallLMS",
            "UpdateStatusFailToWaitingLogCallLMS": BaseUrl["Admin"] + "api/LogCallLMS/UpdateStatusFailToWaitingLogCallLMS",
            "GetQueueCallAPI": BaseUrl["Admin"] + "api/QueueCallAPI/GetQueueCallAPI",
            "UpdateStatusFailToWaitingGetQueueCallAPI": BaseUrl["Admin"] + "api/QueueCallAPI/UpdateStatusFailToWaitingGetQueueCallAPI",
            "GetRefreshToken": "/Home/GetRefreshToken",
            "GetUser": BaseUrl["Admin"] + "api/User/GetUser",
            "GetGroup": BaseUrl["Admin"] + "api/Group/GetGroup",
            "GetByConditionsPermission": BaseUrl["Admin"] + "api/Permission/GetByConditions",
            "GetDepartment": BaseUrl["Admin"] + "api/Department/GetDepartment",
            "GetSummaryCompare": BaseUrl["Admin"] + "api/LogCompare/GetSummaryCompare",
            "CreateUserFromAppValid": BaseUrl["Admin"] + "api/User/CreateUserFromAppValid",
            "GetUserByID": BaseUrl["Admin"] + "api/User/GetUserByID",
            "UpdateUserFromApp": BaseUrl["Admin"] + "api/User/UpdateUserFromApp",
            "ListBookDebt": BaseUrl["Thn"] + "api/BookDebt/ListBookDebt",
            "CreateOfUpdateBookDebt": BaseUrl["Thn"] + "api/BookDebt/CreateOfUpdateBookDebt",
            "CreateOfUpdateApprovalLevelBookDebt": BaseUrl["Thn"] + "api/ApprovalLevelBookDebt/CreateOfUpdateApprovalLevelBookDebt",
            //"GetListApprovalLevelBookDebt": BaseUrl["Thn"] + "api/ApprovalLevelBookDebt/GetListApprovalLevelBookDebt",
            "UpdateApprovalLevelBookDebt": BaseUrl["Thn"] + "api/ApprovalLevelBookDebt/UpdateApprovalLevelBookDebt",
            "GetListTreeViewApprovalLevelBookDebt": BaseUrl["Thn"] + "api/ApprovalLevelBookDebt/GetListTreeViewApprovalLevelBookDebt",
            "GetSettingByConditions": BaseUrl["Thn"] + "api/MGLP/GetSettingByConditions",
            "GetListApprovalLevelBookDebt": BaseUrl["Thn"] + "api/ApprovalLevelBookDebt/GetListApprovalLevelBookDebt",
            "CreateSettingInterestReduction": BaseUrl["Thn"] + "api/MGLP/CreateSettingInterestReduction",
            "GetSettingByBookDebtID": BaseUrl["Thn"] + "api/MGLP/GetSettingByBookDebtID",
            "CreateSettingTotalMoney": BaseUrl["Thn"] + "api/MGLP/CreateSettingTotalMoney",
            "GetSettingMoneyByApprovalLevelBookDebtID": BaseUrl["Thn"] + "api/MGLP/GetSettingMoneyByApprovalLevelBookDebtID",
            "GetUserApprovalLevelConditions": BaseUrl["Thn"] + "api/UserApprovalLevel/GetUserApprovalLevelConditions",
            "CreateOrUpdateUserAppovalLevel": BaseUrl["Thn"] + "api/UserApprovalLevel/CreateOrUpdateUserAppovalLevel",
            "CreateRequestLoanExemption": BaseUrl["Thn"] + "api/loan/CreateRequestLoanExemption",
            "Getlstloanexemption": BaseUrl["Thn"] + "api/loan/getlstloanexemption",
            "UpdateStatusLoanExemption": BaseUrl["Thn"] + "api/loan/UpdateStatusLoanExemption",
            "Uploadfileloanexemption": BaseUrl["Thn"] + "api/loan/uploadfileloanexemption",
            "GetDetailLoanExemptionByID": BaseUrl["Thn"] + "api/loan/GetDetailLoanExemptionByID/",
            "GetHistoryLoanExemptionByLoanExemptionID": BaseUrl["Thn"] + "api/loan/GetHistoryLoanExemptionByLoanExemptionID/",

        };
    }
    var Redirect = function (appId) {
        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                var user = JSON.parse(userModel), endpoint = "/";
                var path = "/redirect?token=" + user.Token + "&TimeExpired=" + user.TimeExpiredString;
                switch (appId) {
                    case AppId.Accountant:
                        endpoint = user.UserLoginModel.AccountantUrl + path;
                        break;
                    case AppId.Lender:
                        endpoint = user.UserLoginModel.LenderUrl + path;
                        break;
                    case AppId.THN:
                        endpoint = user.UserLoginModel.THNUrl + path;
                        break;
                    default:
                }
                window.open(endpoint, "_self");
            }
        } catch (err) {
        }
    }
    var LoginPage = function () {
        var referrer = window.location.href;
        referrer = referrer.replace(':', '>').replace(/\//gi, '<').replace('&', '*');
        $.removeCookie(STATIC_USERMODEL, { path: '/' });
        window.location = LoginUrl + referrer;
    }
    function diff_minutes(dt2, dt1) {

        var diff = (dt2.getTime() - dt1.getTime()) / 1000;
        diff /= 60;
        return Math.round(diff);

    }
    var RefreshToken = function () {
        //ngủ cho tới khi gần hết time Token
        //mở tab mới gọi chức năng check token
        // check token nếu còn dài hạn trả lại ngay = cách gọi lại tất cả các trang 
        // nếu hết hạn thì refresh token và gọi lại tất cả các trang
        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                var user = JSON.parse(userModel);
                var remainTime = diff_minutes(new Date(user.TimeExpired), new Date());
                var waittime = 5;
                if (remainTime < waittime) {
                    //gọi RefreshToken
                    setTimeout(() => { RefreshToken(); }, 10000);
                    if (isCalledRefreshToken == "") {
                        isCalledRefreshToken = "called";
                        baseCommon.AjaxSuccess('POST', baseCommon.Endpoint.GetRefreshToken, "", function (respone) {
                            if (respone.status == 1) {
                                access_token = respone.data.token;
                                user.Token = respone.data.token;
                                user.TimeExpired = respone.data.timeExpired;
                                user.TimeExpiredString = respone.data.timeExpiredString;
                                $.cookie(STATIC_USERMODEL, JSON.stringify(user), { expires: 30, path: '/', domain: user.DomainCookie, secure: true });
                            } else {
                                ShowErrorNotLoad("Phiên làm việc sắp hết hạn, vui lòng F5 thử lại!");
                            }
                            isCalledRefreshToken = "";
                        }, "Phiên làm việc sắp hết hạn, vui lòng F5 thử lại!");
                    }
                } else {
                    var timeOut = ((remainTime - waittime) + 1) * 60 * 1000;
                    setTimeout(() => {
                        isCalledRefreshToken = "";
                        RefreshToken();
                    }, timeOut);
                }
            } else {
                setTimeout(() => { RefreshToken(); }, 2000);
            }
        } catch (err) {
            setTimeout(() => { RefreshToken(); }, 2000);
        }
    }
    var Init = function () {
        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                var userModel = $.cookie(STATIC_USERMODEL);
                if (userModel != null && userModel != "") {
                    var user = JSON.parse(userModel);
                    UserLoginModel = user.UserLoginModel;
                    RefreshToken();
                    AdminUrl = user.AdminUrl;
                    SetBase();
                    var fullShortName = (user.FullName).split(" ");
                    var shortName = fullShortName[(fullShortName.length - 1)];
                    if (fullShortName.length >= 2) {
                        shortName = fullShortName[(fullShortName.length - 2)] + " " + shortName;
                    }
                    $('#_UserFullNameId').html(shortName);
                    $('#_UserFullNameShortId').html(user.UserName.substring(0, 1).toUpperCase());
                    $('#_UserFullNameShort2Id').html(user.UserName.substring(0, 1).toUpperCase());
                } else {
                    return null;
                }
            } else {
                LoginPage();
            }
        } catch (err) {
            LoginPage();
        }
    }

    let InitUser = new Promise(function (myResolve, myReject) {
        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                var userModel = $.cookie(STATIC_USERMODEL);
                if (userModel != null && userModel != "") {
                    var user = JSON.parse(userModel);
                    access_token = user.Token;
                    AdminUrl = user.AdminUrl;
                    USER_ID = user.UserID;
                    SetBase();
                    myResolve(user);
                } else {
                    myReject("Error");
                    LoginPage();
                }
            } else {
                LoginPage();
            }
        } catch (e) {
            LoginPage();
        }
    });
    var AjaxDone = function (method, endpoint, data, done, error, errFuntion) {
        return $.ajax({
            type: method,
            headers: {
                'Authorization': access_token
            },
            url: endpoint,
            data: data,
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).done(done).fail(function (e) {
            if (e.status == 401) {
                LoginPage();
            } else if (e.status == 403) {
                var params = (new URL(endpoint)).pathname;
                baseCommon.ShowErrorNotLoad("Vui lòng liên hệ quản lý để được phân quyền chức năng!<br>" + params);
            } else if (errFuntion == null || errFuntion == "") {
                eval(errFuntion);
            } else {
                if (error == null || error == "") {
                    error = e;
                }
                baseCommon.ShowErrorNotLoad(error);
            }
        });
    };
    var AjaxSuccess = function (method, endpoint, data, success, error) {
        return $.ajax({
            type: method,
            headers: {
                'Authorization': access_token
            },
            url: endpoint,
            data: data,
            contentType: "application/json; charset=utf-8",
            success: success,
            error: function (e) {
                if (e.status == 401) {
                    LoginPage();
                } else if (e.status == 403) {
                    var params = (new URL(endpoint)).pathname;
                    baseCommon.ShowErrorNotLoad("Vui lòng liên hệ quản lý để được phân quyền chức năng!<br>" + params);
                } else {
                    if (error == null || typeof error == "undefined" || error == "") {
                        error = e.responseText;
                    }
                    baseCommon.ShowErrorNotLoad(error);
                }
            }
        });
    };
    var GetUser = function () {
        try {
            var userModel = $.cookie(STATIC_USERMODEL);
            if (userModel != null && userModel != "") {
                var user = JSON.parse(userModel);
                return user;
            } else {
                window.open(LoginUrl, "_self");
                return null;
            }
        } catch (e) {
            window.open(LoginUrl, "_self");
        }
    };
    var HomePage = function () {
        $.ajax({
            type: "GET",
            url: "/Home/GetTokenForHomePage",
            processData: false,
            contentType: false,
        }).done(function (respone) {
            var homeUrl = respone.url + "/redirect?token=" + respone.data;
            window.location = homeUrl;
        });
    }
    var SelectDataSource = function (url, element, placeholder, value, name, selectedId, data) {
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            processData: false,
            contentType: false,
        }).done(function (respone) {
            $(element).html('');
            $(element).append('<option value="">' + placeholder + '</option>');
            $.each(respone.data, function (key, item) {
                var selected = "";
                if (Array.isArray(selectedId)) {
                    if (selectedId.find((itemSelected) => itemSelected[value] === item[value])) {
                        selected = "selected";
                    }
                } else {
                    if (item[value] == selectedId) {
                        selected = "selected";
                    }
                }
                $(element).append('<option ' + selected + ' value="' + item[value] + '">' + item[name] + '</option>');
            });
        });
    };
    var GetDataSource = function (url, data, done) {
        return $.ajax({
            type: "POST",
            url: url,
            data: data,
            processData: false,
            contentType: false,
        }).done(done);
    };
    var DataSource = function (element, placeholder, value, name, selectedId, data, valueDefaul = -111) {
        try {
            $(element).html('');
            $(element).append('<option value="' + valueDefaul + '">' + placeholder + ' </option>');
            $.each(data, function (key, item) {
                var selected = "";
                if (Array.isArray(selectedId)) {
                    if (selectedId.find((itemSelected) => itemSelected[value] === item[value])) {
                        selected = "selected";
                    }
                } else {
                    if (item[value] == selectedId) {
                        selected = "selected";
                    }
                }
                $(element).append('<option ' + selected + ' value="' + item[value] + '">' + item[name] + '</option>');
            });
        } catch (err) {

        }
    };
    var ShowErrorNotLoad = function (message) {
        toastr.error(message);
    }
    function FormatDate(dateFormat, format) {
        var date = moment(dateFormat);
        return date.format(format);// DD/MM/YYYY HH:mm
    }
    var ShowSuccess = function (message) {
        toastr.success(message);
    };
    var ButtonSubmit = function (isVisible, id) {
        if (!isVisible) {
            $(id).addClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
            $(id).prop("disabled", true);
        } else {
            $(id).removeClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
            $(id).prop("disabled", false);
        }
    }
    return {
        ButtonSubmit: ButtonSubmit,
        Init: Init,
        Endpoint: Endpoint,
        SelectDataSource: SelectDataSource,
        DataSource: DataSource,
        GetDataSource: GetDataSource,
        AjaxDone: AjaxDone,
        AjaxSuccess: AjaxSuccess,
        HomePage: HomePage,
        Pagesize: Pagesize,
        GetUser: GetUser,
        ShowErrorNotLoad: ShowErrorNotLoad,
        FormatDate: FormatDate,
        LoginUrl: LoginUrl_,
        ShowSuccess: ShowSuccess,
        Option: Option,
        AppId: AppId,
        Redirect: Redirect,
        RefreshToken: RefreshToken
    }
}
$(document).ready(function () {
    baseCommon.Init();
});