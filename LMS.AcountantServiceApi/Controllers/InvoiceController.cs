﻿using AutoMapper;
using LMS.Common.Constants;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Steeltoe.Common.Discovery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AcountantServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceController : Controller
    {
        private readonly IMediator bus;
        DiscoveryHttpClientHandler _handler;
        private readonly IMapper _mapper;
        public InvoiceController(IMediator bus, IDiscoveryClient client, IMapper mapper)
        {
            this.bus = bus;
            _handler = new DiscoveryHttpClientHandler(client);
            _mapper = mapper;
        }
        [HttpPost]
        [Route("CreateInvoice")]
        public async Task<ResponseActionResult> CreateInvoice(Domain.Models.CreateInvoiceCommandModel cmd)
        {
            var result = await bus.Send(new Commands.CreateInvoiceCommand
            {
                InvoiceModel = cmd
            });
            return result;
        }
    }
}
