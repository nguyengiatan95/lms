﻿using FluentValidation;
using LMS.AcountantServiceApi.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AcountantServiceApi.Validators
{ 
    public class InvoiceModelValidator : AbstractValidator<CreateInvoiceCommandModel>
    {
        public InvoiceModelValidator()
        {
            RuleFor(x => x.InvoiceDate).NotNull()
                .WithMessage("Ngày phát sinh giao dịch không hợp lệ");
            RuleFor(x => x.Amount).LessThan(1).WithMessage("Số tiền phát sinh không hợp lệ");

        }
    }
}
