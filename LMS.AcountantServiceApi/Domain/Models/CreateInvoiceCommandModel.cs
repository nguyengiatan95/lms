﻿using LMS.AcountantServiceApi.Domain.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AcountantServiceApi.Domain.Models
{ 
    public class CreateInvoiceCommandModel
    {
        [Required]
        public long InvoiceID { get; set; }

        [Required]
        public int? InvoiceType { get; set; }

        public int? Status { get; set; }

        [Required]
        public long? Amount { get; set; }

        [Required]
        public string Description { get; set; }

        public long? FromBankCardID { get; set; }

        public long? ToBankCardID { get; set; }

        public long? LoanID { get; set; }

        public long? FromShopID { get; set; }

        public long? ToShopID { get; set; }

        [Required]
        public DateTime? InvoiceDate { get; set; }

        public long? CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public long? ModifyBy { get; set; }

        public DateTime? ModifyDate { get; set; }

        public int? CreditAccount { get; set; }

        public int? DebitAccount { get; set; }
        public long? CustomerID { get; set; }

        public List<TblInvoiceDetail> InvoiceDetail { get; set; }
    }
    public class InvoiceCustomerCommandModel
    {
        [Required]
        public long InvoiceID { get; set; }

        [Required]
        public int? InvoiceType { get; set; }

        public int? Status { get; set; }
          
        [Required]
        public string Description { get; set; }
          
        [Required]
        public long? ToBankCardID { get; set; }

        [Required]
        public long? LoanID { get; set; }

        [Required]
        public long? FromShopID { get; set; }
         
        [Required]
        public DateTime? InvoiceDate { get; set; }

        [Required]
        public long? CreateBy { get; set; }
          
        [Required]
        public long? CustomerID { get; set; }

        [Required]
        public List<InvoiceDetailModel> InvoiceDetail { get; set; }
    }
    public class InvoiceDetailModel
    {
        [Required]
        public long? Amount { get; set; }

        [Required]
        public string Description { get; set; }
         
    } 
}
