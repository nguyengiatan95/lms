﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AcountantServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblInvoice")]
    public class TblInvoice
    {
        [Dapper.Contrib.Extensions.Key]
        public long InvoiceID { get; set; }

        public int? InvoiceType { get; set; }

        public int? Status { get; set; }

        public long? Amount { get; set; }

        public string Description { get; set; }

        public long? FromBankCardID { get; set; }

        public long? ToBankCardID { get; set; }

        public long? LoanID { get; set; }

        public long? FromShopID { get; set; }

        public long? ToShopID { get; set; }

        public DateTime? InvoiceDate { get; set; }

        public long? CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public long? ModifyBy { get; set; }

        public DateTime? ModifyDate { get; set; }

        public int? CreditAccount { get; set; }

        public int? DebitAccount { get; set; }
        public long? CustomerID { get; set; }
    }
}
