﻿using LMS.AcountantServiceApi.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.AcountantServiceApi.RestClients
{
    public interface ICustomerService
    {
        Task<CustomerModel> GetCustomerByID(long ID);
    }
    public class CustomerService : ICustomerService
    {
        public const string HostService = "http://lms_customer_service_api";
        public const string Action_GetCustomerByID = "/api/Customer/GetCustomerByID";

        LMS.Common.Helper.IApiHelper _apiHelper;
        public CustomerService(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }

        public async Task<CustomerModel> GetCustomerByID(long ID)
        {
            var url = new Uri($"{LMS.Common.Constants.ServiceHostInternal.PaymentService}{Action_GetCustomerByID}/{ID}");
            return await _apiHelper.ExecuteAsync<CustomerModel>(url: url.ToString());
        }
    }
}
