﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.AcountantServiceApi.Queries
{ 
    public class GetCustomerQueryHandler : IRequestHandler<GetCustomerQuery, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        ILogger<GetCustomerQueryHandler> _logger;
        public GetCustomerQueryHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab, ILogger<GetCustomerQueryHandler> logger)
        {
            _customerTab = customerTab;
            _logger = logger;
        }
        public async Task<ResponseActionResult> Handle(GetCustomerQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult()
                {
                    Result = (int)LMS.Common.Constants.ResponseAction.Error 
                };
                try
                {
                    var data = _customerTab.WhereClause(x => x.CustomerID == request.CustomerID).Query().First();
                    response.Result = (int)LMS.Common.Constants.ResponseAction.Success;
                    response.Data = data;
                }
                catch (Exception ex)
                {

                } 
                return response;
            });
        }
    }
}
