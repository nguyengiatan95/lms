﻿using LMS.AcountantServiceApi.Domain.Models;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading; 
using System.Threading.Tasks;

namespace LMS.AcountantServiceApi.Commands
{ 
    public class CreateInvoiceCommand : IRequest<ResponseActionResult>
    {
        public CreateInvoiceCommandModel InvoiceModel { get; set; }
    }
    public class CreateInvoiceCommandHandler : IRequestHandler<CreateInvoiceCommand, ResponseActionResult>
    {
        Common.DAL.ITableHelper<Domain.Tables.TblInvoice> _invoiceTab;
        Common.DAL.ITableHelper<Domain.Tables.TblInvoiceDetail> _invoiceDetailTab;
        ILogger<CreateInvoiceCommandHandler> _logger; 
        RestClients.ICustomerService _customerService;
        public CreateInvoiceCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblInvoice> invoiceTab,
            Common.DAL.ITableHelper<Domain.Tables.TblInvoiceDetail> invoiceDetailTab,
            ILogger<CreateInvoiceCommandHandler> logger, RestClients.ICustomerService customerService)
        {
            _invoiceTab = invoiceTab;
            _invoiceDetailTab = invoiceDetailTab;
            _logger = logger;
            _customerService = customerService;
        }
        public async Task<ResponseActionResult> Handle(CreateInvoiceCommand request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult { Result = (int)ResponseAction.Error };
            
            var invoice = request.InvoiceModel;
            // 1- valid data invoice
            // 2- valid data invoiceDetail
            // 3- insert invoice
            // 4- insert invoiceDetail
            try
            {
                #region Phiếu thu Khách hàng
                #region 1- valid data invoice

                #endregion
                #region 2- valid data invoiceDetail
                #endregion
                #region 3- insert invoice, 4- insert invoiceDetail
                #endregion
                #endregion

                if (request == null || request.InvoiceModel == null|| request.InvoiceModel.InvoiceType == null || request.InvoiceModel.InvoiceDetail == null || request.InvoiceModel.InvoiceDetail.Count == 0)
                { 
                    response.Message = MessageConstant.NoData;
                }
                request.InvoiceModel.CreateDate = DateTime.Now;
                request.InvoiceModel.Amount = request.InvoiceModel.InvoiceDetail.Sum(x=>x.Amount);
                #region 1- valid data invoice 
                if (request.InvoiceModel.InvoiceType == (int)LMS.Common.Constants.InvoiceType.KhachHangPhieuThu)
                {
                    #region Khách hàng _ Phiếu nạp tiền (phiếu thu)
                    var model = request.InvoiceModel;
                    #region 1- valid data invoice 
                    if (model.LoanID == null || model.LoanID == 0)
                    {
                        response.Message = MessageConstant.RequireLoanID;
                        return response;
                    }
                    if (model.CustomerID == null || model.CustomerID == 0)
                    {
                        response.Message = MessageConstant.RequireCustomerID;
                        return response;
                    }
                    if (model.FromShopID == null || model.FromShopID == 0)
                    {// cửa hàng thu tiền 
                        response.Message = MessageConstant.RequireShopID;
                        return response;
                    }
                    if (model.ToShopID == null || model.ToShopID == 0)
                    {// khách ở cửa hàng
                        response.Message = MessageConstant.RequireShopID;
                        return response;
                    }
                    if (model.FromBankCardID == null || model.FromBankCardID == 0)
                    {
                        response.Message = MessageConstant.RequireBankCardID;
                        return response;
                    }
                    #region kiểm tra thông tin customer & LoanId trong db
                    // valid khách hàng trong db
                    //var customer = await _customerService.GetCustomerByID(model.CustomerID.Value);
                    //if (customer == null || customer.CustomerID == 0)
                    //{
                    //    response.Message = MessageConstant.RequireCustomerID;
                    //}
                    // valid hợp đồng trong db 
                    #endregion
                    #endregion
                    #endregion
                }
                else if (request.InvoiceModel.InvoiceType == (int)LMS.Common.Constants.InvoiceType.KhachHangPhieuThuTreo)
                {
                    #region Khách hàng _ Phiếu Treo  
                    var model = request.InvoiceModel;
                    #region 1- valid data invoice 
                     
                    if (model.FromShopID == null || model.FromShopID == 0)
                    {// cửa hàng thu tiền 
                        response.Message = MessageConstant.RequireShopID;
                        return response;
                    } 
                    if (model.FromBankCardID == null || model.FromBankCardID == 0)
                    {
                        response.Message = MessageConstant.RequireBankCardID;
                        return response;
                    } 
                    #endregion
                    #endregion
                }
                else if (request.InvoiceModel.InvoiceType == (int)LMS.Common.Constants.InvoiceType.KhachHangPhieuThuHo)
                {
                    #region Khách hàng _ Phiếu thu hộ 
                    var model = request.InvoiceModel;
                    #region 1- valid data invoice 
                    if (model.LoanID == null || model.LoanID == 0)
                    {
                        response.Message = MessageConstant.RequireLoanID;
                        return response;
                    }
                    if (model.CustomerID == null || model.CustomerID == 0)
                    {
                        response.Message = MessageConstant.RequireCustomerID;
                        return response;
                    }
                    if (model.FromShopID == null || model.FromShopID == 0)
                    {// cửa hàng thu tiền 
                        response.Message = MessageConstant.RequireShopID;
                        return response;
                    }
                    if (model.ToShopID == null || model.ToShopID == 0)
                    {// khách ở cửa hàng
                        response.Message = MessageConstant.RequireShopID;
                        return response;
                    }
                    if (model.FromBankCardID == null || model.FromBankCardID == 0)
                    {
                        response.Message = MessageConstant.RequireBankCardID;
                        return response;
                    }
                    #region kiểm tra thông tin customer & LoanId trong db
                    // valid khách hàng trong db
                    //var customer = await _customerService.GetCustomerByID(model.CustomerID.Value);
                    //if (customer == null || customer.CustomerID == 0)
                    //{
                    //    response.Message = MessageConstant.RequireCustomerID;
                    //}
                    // valid hợp đồng trong db 
                    #endregion

                    #endregion
                    #endregion
                }
                else if (request.InvoiceModel.InvoiceType == (int)LMS.Common.Constants.InvoiceType.KhachHangPhieuChiTienThua)
                {

                }
                else if (request.InvoiceModel.InvoiceType == (int)LMS.Common.Constants.InvoiceType.KhachHangPhieuChiTienThua)
                {

                }

                #endregion

                #region 2- valid data invoiceDetail
                if (request.InvoiceModel.InvoiceDetail.Any(x => x.Amount == 0))
                {
                    response.Message = MessageConstant.RequireAmount;
                    return response;
                }
                #endregion

                #region 3- insert invoice, 4- insert invoiceDetail
                var invoiceId = _invoiceTab.Insert(new Domain.Tables.TblInvoice
                {
                    CreateBy = invoice.CreateBy,
                    CreateDate = DateTime.Now,
                    InvoiceType = invoice.InvoiceType,
                    Status = invoice.Status,
                    Amount = invoice.Amount,
                    Description = invoice.Description
                });
                if(invoiceId > 0)
                {
                    invoice.InvoiceID = invoiceId;

                    #region Invoice detail
                    request.InvoiceModel.InvoiceDetail.ForEach(c => c.InvoiceID = invoiceId);
                    _invoiceDetailTab.InsertBulk(request.InvoiceModel.InvoiceDetail);
                    #endregion

                    if (request.InvoiceModel.InvoiceType == (int)LMS.Common.Constants.InvoiceType.KhachHangPhieuThu ||
                        request.InvoiceModel.InvoiceType == (int)LMS.Common.Constants.InvoiceType.KhachHangPhieuThuHo
                        )
                    {
                        #region Khách hàng _ cộng tiền vào số dư (số dư của khách hàng tăng)

                        #endregion
                    }
                    else if (request.InvoiceModel.InvoiceType == (int)LMS.Common.Constants.InvoiceType.KhachHangPhieuChiTienThua)
                    {
                        #region Khách hàng _ trừ tiền trong ví (số dư của khách hàng giảm)

                        #endregion
                    }
                    else if (request.InvoiceModel.InvoiceType == (int)LMS.Common.Constants.InvoiceType.LenderPhieuThuNapQuy)
                    {
                        #region Lender _ Phiếu thu hộ  

                        #endregion   
                    }
                    else if (request.InvoiceModel.InvoiceType == (int)LMS.Common.Constants.InvoiceType.KhachHangPhieuChiTienThua)
                    {

                    }
                    else if (request.InvoiceModel.InvoiceType == (int)LMS.Common.Constants.InvoiceType.KhachHangPhieuChiTienThua)
                    {

                    }

                    response.Result = (int)LMS.Common.Constants.ResponseAction.Success;
                    response.Data = request;
                    response.Message = LMS.Common.Constants.MessageConstant.Success;
                }    
                
                #endregion 
                 
            }
            catch (Exception ex)
            {
                response.Result = (int)LMS.Common.Constants.ResponseAction.Error;
                response.Data = ex.Message;
                response.Message = LMS.Common.Constants.MessageConstant.ErrorInternal; 
            } 
            return response;
        }
    }
}
