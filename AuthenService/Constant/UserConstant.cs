﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenServiceApi.Constant
{
    public class UserConstant
    {
        public const string Admin = "Admin";
        public const string HeaderPlatFormRequest = "X-Served-By";
        public const string HeaderPlatFormWeb = "web";
        public const string HeaderPlatFormApp = "app";
    }
    public enum AuthenAppID
    {
        Ag = 1,
        App = 2,
        Los = 3,
        ServerApp = 4,
        CallCenter = 5
    }
}
