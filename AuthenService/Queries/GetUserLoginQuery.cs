﻿using AuthenServiceApi.Services;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AuthenServiceApi.Queries
{
    public class GetUserLoginQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        /// <summary>
        /// web đang hash md5 trước khi gọi lên
        /// app thì ko md5
        /// </summary>
        public int TypeHash { get; set; }
    }
    public class GetUserLoginQueryHandler : IRequestHandler<GetUserLoginQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly UserAuthenManager _userAuthenManager;
        public GetUserLoginQueryHandler(UserAuthenManager userAuthenManager)
        {
            _userAuthenManager = userAuthenManager;
        }
        public async Task<ResponseActionResult> Handle(GetUserLoginQuery request, CancellationToken cancellationToken)
        {
            return await _userAuthenManager.Login(request.UserName, request.Password, request.TypeHash);
        }
    }
}
