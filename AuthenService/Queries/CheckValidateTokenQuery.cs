﻿using AuthenServiceApi.Domain.Models;
using AuthenServiceApi.Services;
using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AuthenServiceApi.Queries
{
    public class CheckValidateTokenQuery : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string Token { get; set; }
        public string UrlRequest { get; set; }
        public string Platform { get; set; }
    }
    public class CheckValidateTokenQueryHandler : IRequestHandler<CheckValidateTokenQuery, LMS.Common.Constants.ResponseActionResult>
    {
        readonly UserAuthenManager _userAuthenManager;
        //readonly ILogger<CheckValidateTokenQueryHandler> _logger;
        readonly PermissionAppSystemSetting _permissionAppSystemSetting;
        readonly PermissionLOSSetting _permissionLosSetting;
        readonly PermissionServerAppSetting _permissionServerAppSetting;
        readonly PermissionAGSetting _permissionAGSetting;
        readonly PermissionSmartDialerSetting _permissionSmartDialerSetting;
        public CheckValidateTokenQueryHandler(UserAuthenManager userAuthenManager,
            PermissionAppSystemSetting permissionAppSystemSetting,
            PermissionLOSSetting permissionLosSetting,
            PermissionServerAppSetting permissionServerAppSetting,
            PermissionAGSetting permissionAGSetting,
            PermissionSmartDialerSetting permissionSmartDialerSetting
            //ILogger<CheckValidateTokenQueryHandler> logger
            )
        {
            _userAuthenManager = userAuthenManager;
            _permissionAppSystemSetting = permissionAppSystemSetting;
            _permissionServerAppSetting = permissionServerAppSetting;
            _permissionLosSetting = permissionLosSetting;
            _permissionAGSetting = permissionAGSetting;
            _permissionSmartDialerSetting = permissionSmartDialerSetting;
            //_logger = logger;
        }
        public async Task<ResponseActionResult> Handle(CheckValidateTokenQuery request, CancellationToken cancellationToken)
        {
            PermisionBaseSetting permision = null;
            switch (request.Platform)
            {
                case Constant.UserConstant.HeaderPlatFormApp:
                    if (request.Token == _permissionAppSystemSetting.Token)
                    {
                        permision = _permissionAppSystemSetting;
                    }
                    else if (request.Token == _permissionServerAppSetting.Token)
                    {
                        permision = _permissionServerAppSetting;
                    }
                    return await _userAuthenManager.CheckPermissionApp(request.Token, request.UrlRequest, permision);
                default:
                    if (request.Token == _permissionAGSetting.Token)
                    {
                        permision = _permissionAGSetting;
                    }
                    else if (request.Token == _permissionLosSetting.Token)
                    {
                        permision = _permissionLosSetting;
                    }
                    else if (request.Token == _permissionSmartDialerSetting.Token)
                    {
                        permision = _permissionSmartDialerSetting;
                    }
                    return await _userAuthenManager.CheckPermission(request.Token, request.UrlRequest, permision);
            }
        }
    }
}
