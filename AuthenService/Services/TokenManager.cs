﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AuthenServiceApi.Services
{
    public class TokenManager
    {
        public static string SecretKey = "XCAP05H6LoKvbRRa/QkqLNMI7cOHguaRyHzyg7n5qEkGjQmtBhz4SzYh4Fqwjyi3KJHlSXKPwVu2+bXr6CtpgQ==";
        public const string TokenAgRequest = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiMSIsIklkZW50aXR5X1VzZXJJRCI6IjEiLCJuYmYiOjE2MTc4NjQ4MDQsImV4cCI6MzMxNzQ3NzM2MDQsImlhdCI6MTYxNzg2NDgwNH0.fw6U7LUXZR-nBbYgVeaxao6HBBk456PyFbwT0lSBiRg";
        public const string TokenAppSystem = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiMiIsIklkZW50aXR5X1VzZXJJRCI6IjIiLCJuYmYiOjE2MzUzOTM4OTksImV4cCI6MTczMDAwMTg5OSwiaWF0IjoxNjM1MzkzODk5fQ._8uq9KRVW7A2u-40lsl77MaeP7_Kl0dgaSSDh3Lb5vg";
        public const string TokenLOS = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiMiIsIklkZW50aXR5X1VzZXJJRCI6IjIiLCJuYmYiOjE2MzcwNTM2MzksImV4cCI6MTczMTY2MTYzOSwiaWF0IjoxNjM3MDUzNjM5fQ.8WfKdetnTk5kxAbhu4wX-pC8F7IPlygT0B67hRjIJvc";
        public static int TimeExpired = 0;
        public static string GenerateToken(long userID)
        {
            byte[] key = Convert.FromBase64String(SecretKey);
            SymmetricSecurityKey securityKey = new SymmetricSecurityKey(key);
            SecurityTokenDescriptor descriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.Name, $"{userID}"),
                    new Claim(LMS.Common.Constants.ClaimTypesInternal.IdentityUserID, $"{userID}")
                }),
                Expires = DateTime.Now.AddMinutes(TimeExpired),
                SigningCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature)
            };

            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            JwtSecurityToken token = handler.CreateJwtSecurityToken(descriptor);
            return handler.WriteToken(token);
        }
        public static ClaimsPrincipal GetPrincipal(string token)
        {
            try
            {
                JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
                JwtSecurityToken jwtToken = (JwtSecurityToken)tokenHandler.ReadToken(token);
                if (jwtToken == null)
                    return null;
                byte[] key = Convert.FromBase64String(SecretKey);
                TokenValidationParameters parameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(key)
                };
                SecurityToken securityToken;
                ClaimsPrincipal principal = tokenHandler.ValidateToken(token, parameters, out securityToken);
                return principal;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public static string ValidateToken(string token)
        {
            ClaimsPrincipal principal = GetPrincipal(token);
            if (principal == null)
                return null;
            ClaimsIdentity identity = null;
            try
            {
                identity = (ClaimsIdentity)principal.Identity;
            }
            catch (NullReferenceException)
            {
                return null;
            }
            Claim usernameClaim = identity.FindFirst(ClaimTypes.Name);
            return usernameClaim.Value;
        }
    }
}
