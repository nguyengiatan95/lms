﻿using AuthenServiceApi.Constant;
using AuthenServiceApi.Domain.Models;
using LMS.Common.Constants;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AuthenServiceApi.Services
{
    public class UserAuthenManager
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> _userTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblGroupPermission> _groupPermissionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblPermission> _permissionTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserGroup> _userGroupTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblToken> _tokenTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCodeUserLogin> _codeUserLoginTab;
        ILogger<UserAuthenManager> _logger;
        LMS.Common.Helper.Utils _common;
        public UserAuthenManager(LMS.Common.DAL.ITableHelper<Domain.Tables.TblUser> userTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblGroupPermission> groupPermissionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblPermission> permissionTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblUserGroup> userGroupTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblToken> tokenTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCodeUserLogin> codeUserLoginTab,
            ILogger<UserAuthenManager> logger)
        {
            _userTab = userTab;
            _groupPermissionTab = groupPermissionTab;
            _permissionTab = permissionTab;
            _userGroupTab = userGroupTab;
            _tokenTab = tokenTab;
            _logger = logger;
            _common = new LMS.Common.Helper.Utils();
            _codeUserLoginTab = codeUserLoginTab;

        }
        public async Task<LMS.Common.Constants.ResponseActionResult> Login(string username, string password, int typeHash = 0)
        {
            var response = new LMS.Common.Constants.ResponseActionResult();
            response.Message = MessageConstant.UserWrongPass;
            var currentDate = DateTime.Now;
            try
            {
                var result = (await _userTab.SetGetTop(1).WhereClause(x => x.UserName == username && x.Status == (int)StatusCommon.Active).QueryAsync()).FirstOrDefault();
                if (result != null)
                {
                    switch (typeHash)
                    {
                        case 1: // app ko md5
                            password = _common.HashMD5(password);
                            break;
                        default:
                            break;
                    }
                    if (result.Password != password)
                    {
                        return response;
                    }

                    response.SetSucces();
                    string token = TokenManager.GenerateToken(result.UserID);
                    var codeGUI = $"{DateTime.Now.Ticks}_{result.UserID}_{result.UserName}";
                    var codeUserLoginFirst = (await _codeUserLoginTab.SetGetTop(1).WhereClause(x => x.UserID == result.UserID && x.CreateDate > currentDate.Date && x.CreateDate < currentDate).QueryAsync()).FirstOrDefault();
                    if (codeUserLoginFirst == null)
                    {
                        _ = _codeUserLoginTab.InsertAsync(new Domain.Tables.TblCodeUserLogin
                        {
                            CodeGUI = codeGUI,
                            UserID = result.UserID,
                            CountLogin = 0,
                            CreateDate = DateTime.Now,
                            ModifyDate = DateTime.Now
                        });
                    }
                    else
                    {
                        codeGUI = codeUserLoginFirst.CodeGUI;
                        codeUserLoginFirst.CountLogin += 1;
                        codeUserLoginFirst.ModifyDate = DateTime.Now;
                        _ = _codeUserLoginTab.UpdateAsync(codeUserLoginFirst);
                    }
                    response.Data = new { Token = token, TimeExpired = DateTime.Now.AddMinutes(TokenManager.TimeExpired), UserID = result.UserID, FullName = result.FullName, UserName = result.UserName };
                    _ = _tokenTab.InsertAsync(new Domain.Tables.TblToken
                    {
                        UserID = result.UserID,
                        Value = token,
                        CreateDate = DateTime.Now
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Login|username={username}|password={password}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<LMS.Common.Constants.ResponseActionResult> CheckPermission(string token, string urlRequest, PermisionBaseSetting permissionApp)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            try
            {
                response.Message = MessageConstant.NotPermission;
                // check request AG
                if (string.IsNullOrEmpty(token))
                {
                    return response;
                }
                if (permissionApp != null)
                {
                    if (!HasPermission(urlRequest, permissionApp))
                    {
                        return response;
                    }
                    response.SetSucces();
                    response.Data = new { UserID = permissionApp.UserID, UserName = permissionApp.UserName, UrlRequest = urlRequest };
                    return response;

                }
                //if (token == TokenManager.TokenAgRequest)
                //{
                //    response.SetSucces();
                //    response.Data = new { UserID = TimaSettingConstant.UserIDAdmin, UserName = TimaSettingConstant.UserNameAG, UrlRequest = urlRequest };
                //    return response;
                //}
                //if (token == TokenManager.TokenLOS)
                //{
                //    if (!HasPermission(urlRequest, permissionApp))
                //    {
                //        return response;
                //    }
                //    response.SetSucces();
                //    response.Data = new { UserID = TimaSettingConstant.UserIDLOS, UserName = TimaSettingConstant.UserNameLOS, UrlRequest = urlRequest };
                //    return response;
                //}
                var userIDToken = TokenManager.ValidateToken(token);
                if (string.IsNullOrEmpty(userIDToken) || !Int32.TryParse(userIDToken, out var userID))
                {
                    _ = DeleteOldToken(token);
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                // đã logout
                var tokenInfo = (await _tokenTab.SetGetTop(1).WhereClause(x => x.Value == token).QueryAsync()).FirstOrDefault();
                if (tokenInfo == null)
                {
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                var objUser = await _userTab.GetByIDAsync(userID);
                if (objUser == null)
                {
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                response.SetSucces();
                response.Data = new { UserID = 0 }; // không có quyền truy câp
                if (objUser.UserName.ToLower() == UserConstant.Admin.ToLower()) // user admin
                {
                    response.Data = new { UserID = TimaSettingConstant.UserIDAdmin, UserName = TimaSettingConstant.UserNameAdmin, UrlRequest = urlRequest };
                    return response;
                }
                // với các api get thông theo id
                // remove id đó ra làm thành link api
                // /loan/api/loan/GetLoanCreditDetailLosByLoanID/52773 -> /loan/api/loan/GetLoanCreditDetailLosByLoanID
                var urlCheckPermission = Regex.Replace(urlRequest, @"/(\d)+", "");
                var permission = (await _permissionTab.SetGetTop(1).WhereClause(x => x.LinkApi == urlCheckPermission).QueryAsync()).FirstOrDefault();
                if (permission == null)
                {
                    response.Message = MessageConstant.ErrorNotExist;
                    return response;
                }
                var lstUserGroup = (await _userGroupTab.WhereClause(x => x.UserID == objUser.UserID).QueryAsync()).ToList();
                if (lstUserGroup != null && lstUserGroup.Count > 0)
                {
                    var result = (await _groupPermissionTab.SetGetTop(1).WhereClause(x => lstUserGroup.Select(y => y.GroupID).Contains(x.GroupID) && x.PermissionID == permission.PermissionID).QueryAsync()).FirstOrDefault();
                    if (result != null)
                    {
                        response.SetSucces();
                        response.Data = new { UserID = objUser.UserID, UserName = objUser.UserName, UrlRequest = urlRequest };
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"CheckPermission|token={token}|urlRequest={urlRequest}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<LMS.Common.Constants.ResponseActionResult> LoginOut(string token)
        {
            var response = new LMS.Common.Constants.ResponseActionResult();
            try
            {
                await DeleteOldToken(token);
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"LoginOut|token={token}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        public async Task<LMS.Common.Constants.ResponseActionResult> RefeshToken(string token)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                var userIDToken = TokenManager.ValidateToken(token);
                if (!Int32.TryParse(userIDToken, out var userID))
                {
                    _ = DeleteOldToken(token);
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                // đã logout
                var tokenInfo = (await _tokenTab.WhereClause(x => x.Value == token).QueryAsync()).FirstOrDefault();
                if (tokenInfo == null)
                {
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                var objUser = await _userTab.GetByIDAsync(userID);
                if (objUser == null)
                {
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                token = TokenManager.GenerateToken(objUser.UserID);
                response.Data = new { Token = token, TimeExpired = DateTime.Now.AddMinutes(TokenManager.TimeExpired) };
                _ = _tokenTab.InsertAsync(new Domain.Tables.TblToken
                {
                    UserID = objUser.UserID,
                    Value = token,
                    CreateDate = DateTime.Now
                });
                response.SetSucces();
            }
            catch (Exception ex)
            {
                _logger.LogError($"RefeshToken|token={token}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
        private async Task DeleteOldToken(string token)
        {
            try
            {
                var tokenInfo = (await _tokenTab.WhereClause(x => x.Value == token).QueryAsync()).FirstOrDefault();
                if (tokenInfo != null && tokenInfo.TokenID > 0)
                {
                    _ = _tokenTab.DeleteAsync(tokenInfo);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"DeleteOldToken|token={token}|ex={ex.Message}-{ex.StackTrace}");
            }
        }

        public async Task<LMS.Common.Constants.ResponseActionResult> CheckPermissionApp(string token, string urlRequest, PermisionBaseSetting permissionApp)
        {
            LMS.Common.Constants.ResponseActionResult response = new LMS.Common.Constants.ResponseActionResult();
            try
            {
                if (permissionApp != null)
                {
                    if (!HasPermission(urlRequest, permissionApp))
                    {
                        return response;
                    }
                    response.SetSucces();
                    response.Data = new { UserID = permissionApp.UserID, UserName = permissionApp.UserName, UrlRequest = urlRequest };
                    return response;
                }
                var userIDToken = TokenManager.ValidateToken(token);
                if (string.IsNullOrEmpty(userIDToken) || !Int32.TryParse(userIDToken, out var userID))
                {
                    _ = DeleteOldToken(token);
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                // đã logout
                var tokenInfo = (await _tokenTab.WhereClause(x => x.Value == token).QueryAsync()).FirstOrDefault();
                if (tokenInfo == null)
                {
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                var objUser = await _userTab.GetByIDAsync(userID);
                if (objUser == null)
                {
                    response.Message = MessageConstant.UserNotExist;
                    return response;
                }
                response.SetSucces();
                response.Data = new { UserID = objUser.UserID, UserName = TimaSettingConstant.UserNameAdmin, UrlRequest = urlRequest };
            }
            catch (Exception ex)
            {
                _logger.LogError($"CheckPermission|token={token}|urlRequest={urlRequest}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }

        private bool HasPermission(string urlRequest, PermisionBaseSetting permissionApp)
        {
            var urlformat = Regex.Replace(urlRequest, @"/(\d)+", "");
            urlformat = urlformat.ToLower();
            if (urlformat.StartsWith("/"))
            {
                urlformat = urlformat.Substring(1);
            }
            if (urlformat.EndsWith("/"))
            {
                urlformat = urlformat.Substring(0, urlformat.Length - 1);
            }
            var lstApi = permissionApp.GetLstApi();
            if (lstApi.Count == 0)
            {
                return true;
            }
            else if (!lstApi.Contains(urlformat))
            {
                return false;
            }
            return true;
        }
    }
}
