using AuthenServiceApi.Domain.Models;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Steeltoe.Discovery.Client;
using Steeltoe.Discovery.Eureka;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace AuthenService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<KestrelServerOptions>(Configuration.GetSection("Kestrel"));
            services.AddDiscoveryClient(Configuration);
            services.AddServiceDiscovery(options => options.UseEureka());
            services.AddHttpContextAccessor();
            services.AddControllers().AddJsonOptions(opts => opts.JsonSerializerOptions.PropertyNamingPolicy = null);
            LMS.Common.Configuration.ConnectStringSetting.DefaultConnectString = Configuration["ConnectStringSetting:DefaultConnectString"];
            AuthenServiceApi.Services.TokenManager.TimeExpired = Convert.ToInt32(Configuration["TokenSetting:TimeExpired"]);
            AuthenServiceApi.Services.TokenManager.SecretKey = Configuration["TokenSetting:SecretKey"];
            services.AddTransient(typeof(LMS.Common.DAL.ITableHelper<>), typeof(LMS.Common.DAL.TableHelper<>));
            services.AddScoped<AuthenServiceApi.Services.UserAuthenManager>();
            services.AddMediatR(Assembly.GetExecutingAssembly());

            var permissionAppSystemSetting = new PermissionAppSystemSetting();
            Configuration.GetSection(nameof(PermissionAppSystemSetting)).Bind(permissionAppSystemSetting);
            services.AddSingleton(permissionAppSystemSetting);

            var permissionLOSSetting = new PermissionLOSSetting();
            Configuration.GetSection(nameof(PermissionLOSSetting)).Bind(permissionLOSSetting);
            services.AddSingleton(permissionLOSSetting);

            var permissionServerToServerSetting = new PermissionServerAppSetting();
            Configuration.GetSection(nameof(PermissionServerAppSetting)).Bind(permissionServerToServerSetting);
            services.AddSingleton(permissionServerToServerSetting);

            var permissionAGSetting = new PermissionAGSetting();
            Configuration.GetSection(nameof(PermissionAGSetting)).Bind(permissionAGSetting);
            services.AddSingleton(permissionAGSetting);

            var permissionSmartDialerSetting = new PermissionSmartDialerSetting();
            Configuration.GetSection(nameof(PermissionSmartDialerSetting)).Bind(permissionSmartDialerSetting);
            services.AddSingleton(permissionSmartDialerSetting);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();
            app.UseDiscoveryClient();
            //Add our new middleware to the pipeline
            app.UseMiddleware<LMS.Common.Helper.RequestResponseLoggingMiddleware>();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                //endpoints.MapControllerRoute("login", "{controller=Authen}/{action=login}");
                //endpoints.MapControllerRoute(name: "login", pattern: "login", defaults: new { controller = "Authen", action = "login" });
                ////endpoints.MapControllerRoute("validate", "{controller=Authen}/{action=validate}");
                //endpoints.MapControllerRoute(name: "validate", pattern: "/validate", defaults: new { controller = "Authen", action = "validate" });
            });
        }
    }
}
