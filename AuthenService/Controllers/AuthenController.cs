﻿using AuthenServiceApi.Services;
using LMS.Common.Constants;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace AuthenServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenController : ControllerBase
    {
        private readonly IMediator bus;
        readonly IHttpContextAccessor _httpContextAccessor;
        readonly ILogger<AuthenController> _logger;
        public AuthenController(IMediator bus,
            IHttpContextAccessor httpContextAccessor,
            ILogger<AuthenController> logger)
        {
            this.bus = bus;
            _httpContextAccessor = httpContextAccessor;
            _logger = logger;
        }
        private string GetPlatformRequest()
        {
            try
            {
                var lstHeader = _httpContextAccessor.HttpContext?.Request?.Headers;
                if (lstHeader != null && lstHeader.Any())
                {
                    var headerCheck = Constant.UserConstant.HeaderPlatFormRequest.ToLowerInvariant();
                    foreach (var item in lstHeader)
                    {
                        if (item.Key.ToLowerInvariant() == headerCheck)
                        {
                            return item.Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"AuthenController_GetPlatformRequest|ex={ex.Message}-{ex.StackTrace}");
            }
            return Constant.UserConstant.HeaderPlatFormWeb;
        }

        [HttpPost]
        [Route("Login")]
        public async Task<LMS.Common.Constants.ResponseActionResult> Login(Queries.GetUserLoginQuery request)
        {
            return await bus.Send(request);
        }
        [HttpGet]
        [Route("Validate")]
        public async Task<LMS.Common.Constants.ResponseActionResult> Validate(string token, string urlRequest)
        {
            return await bus.Send(new Queries.CheckValidateTokenQuery { Token = token, UrlRequest = urlRequest, Platform = GetPlatformRequest() });
        }

        [HttpPost]
        [Route("LogOut")]
        public async Task<LMS.Common.Constants.ResponseActionResult> LogOut(Commands.LogOutCommand request)
        {
            return await bus.Send(request);
        }

        [HttpPost]
        [Route("RefeshToken")]
        public async Task<LMS.Common.Constants.ResponseActionResult> RefeshToken(Commands.RefeshTokenCommand request)
        {
            return await bus.Send(request);
        }
    }
}
