﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenServiceApi.Domain.Models
{
    public class PermisionBaseSetting
    {
        /// <summary>
        /// các api cách nhau = dấu |
        /// </summary>
        public string LstApi { get; set; }
        public List<string> GetLstApi()
        {
            if (string.IsNullOrEmpty(LstApi))
            {
                return new List<string>();
            }
            return LstApi.Split('|').ToList();
        }
        public string Token { get; set; }
        public long UserID { get; set; }
        public string UserName { get; set; }
        public int AppID { get; set; }
    }
    public class PermissionAppSystemSetting : PermisionBaseSetting
    {
    }
    public class PermissionLOSSetting : PermisionBaseSetting
    {
    }
    public class PermissionServerAppSetting : PermisionBaseSetting
    {
    }
    public class PermissionAGSetting : PermisionBaseSetting
    {
    }
    public class PermissionSmartDialerSetting : PermisionBaseSetting
    {
    }
}
