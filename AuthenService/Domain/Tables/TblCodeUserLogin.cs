﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblCodeUserLogin")]
    public class TblCodeUserLogin
    {
        [Dapper.Contrib.Extensions.Key]
        public long CodeUserLoginID { get; set; }

        public long UserID { get; set; }

        public string CodeGUI { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }

        public int CountLogin { get; set; }
    }
}
