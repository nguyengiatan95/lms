﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenServiceApi.Domain.Tables
{
    [Table("TblGroupPermission")]
    public class TblGroupPermission
    {
        [Key]
        public long GroupPermissionID { get; set; }

        public long GroupID { get; set; }

        public long PermissionID { get; set; }
    }
}
