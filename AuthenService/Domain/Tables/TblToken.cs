﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblToken")]
    public class TblToken
    {
        [Dapper.Contrib.Extensions.Key]
        public long TokenID { get; set; }

        public long UserID { get; set; }

        public string Value { get; set; }

        public DateTime CreateDate { get; set; }
    }
}
