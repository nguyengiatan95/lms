﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblUserGroup")]
    public class TblUserGroup
    {
        [Dapper.Contrib.Extensions.Key]
        public long UserGroupID { get; set; }

        public long? UserID { get; set; }

        public long? GroupID { get; set; }

    }
}
