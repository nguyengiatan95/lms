﻿using AuthenServiceApi.Services;
using LMS.Common.Constants;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AuthenServiceApi.Commands
{
    public class RefeshTokenCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string Token { get; set; }
    }
    public class RefeshTokenCommandHandler : IRequestHandler<RefeshTokenCommand, LMS.Common.Constants.ResponseActionResult>
    {
        UserAuthenManager _userAuthenManager;
        public RefeshTokenCommandHandler(UserAuthenManager userAuthenManager)
        {
            _userAuthenManager = userAuthenManager;
        }
        public async Task<ResponseActionResult> Handle(RefeshTokenCommand request, CancellationToken cancellationToken)
        {
            return await _userAuthenManager.RefeshToken(request.Token);
        }
    }
}
