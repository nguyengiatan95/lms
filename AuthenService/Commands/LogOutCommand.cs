﻿using AuthenServiceApi.Services;
using LMS.Common.Constants;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AuthenServiceApi.Commands
{
    public class LogOutCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        public string Token { get; set; }
    }
    public class LogOutCommandHandler : IRequestHandler<LogOutCommand, LMS.Common.Constants.ResponseActionResult>
    {
        UserAuthenManager _userAuthenManager;
        public LogOutCommandHandler(UserAuthenManager userAuthenManager)
        {
            _userAuthenManager = userAuthenManager;
        }
        public async Task<ResponseActionResult> Handle(LogOutCommand request, CancellationToken cancellationToken)
        {
            return await _userAuthenManager.LoginOut(request.Token);
        }
    }
}
