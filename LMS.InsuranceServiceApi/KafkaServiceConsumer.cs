﻿using LMS.Kafka.Interfaces;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi
{
    public class KafkaServiceConsumer : BackgroundService
    {
        IKafkaConsumer<string, LMS.Kafka.Messages.Loan.LoanInsertSuccess> _loanCustomerConsumer;

        ILogger<KafkaServiceConsumer> _logger;
        public KafkaServiceConsumer(IKafkaConsumer<string, LMS.Kafka.Messages.Loan.LoanInsertSuccess> LoanCustomerConsumer,
            ILogger<KafkaServiceConsumer> logger)
        {
            _loanCustomerConsumer = LoanCustomerConsumer;
            _logger = logger;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                var t1 = _loanCustomerConsumer.Consume(Kafka.Constants.KafkaTopics.LoanCreateSuccess, stoppingToken);
                await Task.WhenAll(t1);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{(int)HttpStatusCode.InternalServerError} ConsumeFailedOnTopic - {Kafka.Constants.KafkaTopics.LoanCreateSuccess}, {ex}");
            }
        }

        public override void Dispose()
        {
            _loanCustomerConsumer.Close();
            _loanCustomerConsumer.Dispose();

            base.Dispose();
        }
    }
}
