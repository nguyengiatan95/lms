﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.Helper
{
    public class CommonInsurance
    {
        public decimal InterestFeesCustomer(long money)
        {
            return Convert.ToDecimal(Math.Round(money * LMS.InsuranceServiceApi.Constants.InsuranceConstants.InterestFeesCustomer, 0));
        }
        public decimal InterestNotVatLender(long money)
        {
            return Convert.ToDecimal(Math.Round(money * LMS.InsuranceServiceApi.Constants.InsuranceConstants.InterestNotVatLender, 0));
        }
        public long InterestVatLender(decimal interestNotVat)
        {
            return Convert.ToInt64(Math.Round(interestNotVat * LMS.InsuranceServiceApi.Constants.InsuranceConstants.InterestVatLender, 0));
        }
        public decimal InterestMaterial(decimal interestNotVat)
        {
            return Convert.ToInt64(Math.Round(interestNotVat * LMS.InsuranceServiceApi.Constants.InsuranceConstants.InterestMaterial, 0));
        }

        public string ToMd5(string s)
        {
            //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] originalBytes = Encoding.Default.GetBytes(s);
            byte[] encodedBytes = md5.ComputeHash(originalBytes);

            //Convert encoded bytes back to a 'readable' string
            return BitConverter.ToString(encodedBytes).ToLower().Replace("-", "");
        }
        public string SoHDTinDung(long IdOfPartenter, string ContactCode)
        {
            return "HD-" + IdOfPartenter + "-" + ContactCode;
        }
    }
}
