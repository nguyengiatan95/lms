﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.InsuranceService;
using LMS.InsuranceServiceApi.Constants;
using LMS.InsuranceServiceApi.Domain.Models;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.Queries
{
    public class ByInsurance : IRequest<Common.Constants.ResponseActionResult>
    {
        //public Domain.Models.PaymentScheduleReq PaymentSchedule { get; set; }
    }
    public class ByInsuranceHandler : IRequestHandler<ByInsurance, Common.Constants.ResponseActionResult>
    {
        ILogger<ByInsuranceHandler> _logger;
        RestClients.IPaymentScheduleService _paymentScheduleByCondition;
        RestClients.IInsurancePremiumsOfLenderService _insurancePremiumsOfLenderService;
        RestClients.IInsurancePremiumsOfCustomersService _insurancePremiumsOfCustomersService;
        RestClients.IInsurenceService _insurenceService;
        RestClients.ILoanService _loanService;
        RestClients.IPaymentScheduleService _paymentScheduleService;
        RestClients.ILOSService _lOSService;
        Common.Helper.Utils _common;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInsurance> _insuranceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> _settingKeyTab;
        Helper.CommonInsurance _commonInsurance;
        [Obsolete]
        IHostingEnvironment _hostingEnvironment;

        [Obsolete]
        public ByInsuranceHandler(ILogger<ByInsuranceHandler> logger,
            RestClients.IPaymentScheduleService paymentScheduleByCondition,
            RestClients.IInsurancePremiumsOfLenderService insurancePremiumsOfLenderService,
            RestClients.IInsurancePremiumsOfCustomersService insurancePremiumsOfCustomersService,
            RestClients.IInsurenceService insurenceService,
            RestClients.ILoanService loanService,
            RestClients.IPaymentScheduleService paymentScheduleService,
            RestClients.ILOSService lOSService,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInsurance> insuranceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> settingKeyTab,
            IHostingEnvironment hostingEnvironment)
        {
            _logger = logger;
            _paymentScheduleByCondition = paymentScheduleByCondition;
            _insurancePremiumsOfLenderService = insurancePremiumsOfLenderService;
            _insurancePremiumsOfCustomersService = insurancePremiumsOfCustomersService;
            _insurenceService = insurenceService;
            _loanService = loanService;
            _paymentScheduleService = paymentScheduleService;
            _lOSService = lOSService;
            _common = new Common.Helper.Utils();
            _insuranceTab = insuranceTab;
            _customerTab = customerTab;
            _loanTab = loanTab;
            _lenderTab = lenderTab;
            _hostingEnvironment = hostingEnvironment;
            _settingKeyTab = settingKeyTab;
            _commonInsurance = new Helper.CommonInsurance();
        }
        public async Task<ResponseActionResult> Handle(ByInsurance request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    var keySettingInfo = _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.Insurance_ByInsurance).Query().FirstOrDefault();
                    if (keySettingInfo == null || keySettingInfo.Status == 0)
                    {
                        response.Message = MessageConstant.SettingKey_Insurance_ByInsurance;
                        return response;
                    }

                    var lstInsurance = _insuranceTab.SetGetTop(50).WhereClause(x => x.Status == (int)LMS.Common.Constants.Insurance_Status.WaitingBuyInsurance && x.Repeat < BaoMinhConsants.Repeat ||
                    (x.Status == (int)LMS.Common.Constants.Insurance_Status.SuccessBuyInsurance && x.Path == null)).Query().ToList();
                    if (lstInsurance != null && lstInsurance.Count > 0)
                    {
                        var lstLoanID = new List<long>();
                        var lstCustomerID = new List<long>();
                        var lstLenderID = new List<long>();

                        foreach (var item in lstInsurance)
                        {
                            lstLoanID.Add(item.LoanID);

                            if (item.TypeInsurance == (int)LMS.Common.Constants.Insurance_TypeInsurance.Customer || item.TypeInsurance == (int)LMS.Common.Constants.Insurance_TypeInsurance.Material)
                                lstCustomerID.Add(item.CustomerID);
                            else if (item.TypeInsurance == (int)LMS.Common.Constants.Insurance_TypeInsurance.Lender)
                                lstLenderID.Add(item.CustomerID);

                        }
                        var dictLoanInfos = _loanTab.WhereClause(x => lstLoanID.Contains(x.LoanID)).Query().ToDictionary(x => x.LoanID, x => x);
                        var dictCustomerInfos = _customerTab.WhereClause(x => lstCustomerID.Contains(x.CustomerID)).Query().ToDictionary(x => x.CustomerID, x => x);
                        var dictLenderInfos = _lenderTab.WhereClause(x => lstLenderID.Contains(x.LenderID)).Query().ToDictionary(x => x.LenderID, x => x);
                        foreach (var item in lstInsurance)
                        {
                            var objLoan = dictLoanInfos.GetValueOrDefault(item.LoanID);
                            var objCustomer = dictCustomerInfos.GetValueOrDefault(item.CustomerID);
                            var objLender = dictLenderInfos.GetValueOrDefault(item.CustomerID);
                            if (item.Status == (int)LMS.Common.Constants.Insurance_Status.WaitingBuyInsurance) // mua bảo hiểm
                            {
                                #region mua bảo hiểm cho khách hàng or lender
                                if (item.TypeInsurance == (int)LMS.Common.Constants.Insurance_TypeInsurance.Customer)
                                {
                                    if (objCustomer == null || objLoan == null)
                                    {
                                        UpdateRepeat(item);
                                        continue;
                                    }
                                    var createObectBorrower = CreateObectBorrower(objCustomer, objLoan);
                                    if (createObectBorrower == null)
                                    {
                                        UpdateRepeat(item);
                                        continue;
                                    }
                                    var objCreateContact = new CreateBorrowerContract();
                                    objCreateContact.LoanID = item.LoanID;
                                    objCreateContact.CustomerID = item.CustomerID;
                                    objCreateContact.CreateContract = createObectBorrower;
                                    var createBorrwer = _insurenceService.CreateBorrowerContract(objCreateContact).Result;
                                    if (createBorrwer != null && createBorrwer.Meta != null && createBorrwer.Meta.ErrCode == (int)ResponseCode_Insurance.Success && createBorrwer.Data != null && !string.IsNullOrEmpty(createBorrwer.Data.Path))
                                    {
                                        item.PartnerCode = (int)Insurance_PartnerCode.BaoMinh;
                                        item.InsuranceMoney = objCreateContact.CreateContract.InsuranceMoney;
                                        item.InsuranceFee = objCreateContact.CreateContract.CustomerInsuranceFee;
                                        item.Status = (int)Insurance_Status.SuccessBuyInsurance;
                                        item.DateOfPurchase = DateTime.Now;
                                        item.Path = createBorrwer.Data.Path;
                                        _insuranceTab.Update(item);
                                        _loanService.UpdateLinkInsurance(item.LoanID, (int)Insurance_TypeInsurance.Customer, createBorrwer.Data.Path);
                                    }
                                    else
                                    {
                                        if (createBorrwer != null && createBorrwer.Meta != null && createBorrwer.Meta.ErrCode == (int)ResponseCode_Insurance.Exits)
                                        {
                                            var contactcode = _common.GenContractCodeBaoMinh((long)objLoan.LoanCreditIDOfPartner, objLoan.ContactCode);
                                            var linkCustomer = _insurenceService.GetLink(contactcode, (int)LMS.Common.Constants.Insurance_TypeInsurance.Customer, item.CustomerID).Result;
                                            if (linkCustomer != null && linkCustomer.Meta != null && linkCustomer.Meta.ErrCode == (int)ResponseCode_Insurance.Success && !string.IsNullOrEmpty(linkCustomer.Data))
                                            {
                                                item.PartnerCode = (int)Insurance_PartnerCode.BaoMinh;
                                                item.Path = linkCustomer.Data;
                                                item.Status = (int)Insurance_Status.SuccessBuyInsurance;
                                                _insuranceTab.Update(item);
                                                _loanService.UpdateLinkInsurance(item.LoanID, (int)Insurance_TypeInsurance.Customer, linkCustomer.Data);
                                            }
                                        }
                                    }

                                }
                                if (item.TypeInsurance == (int)LMS.Common.Constants.Insurance_TypeInsurance.Lender)
                                {
                                    if (objLender == null || objLoan == null)
                                    {
                                        UpdateRepeat(item);
                                        continue;
                                    }
                                    var createObectLender = CreateObectLender(objLoan, objLender);
                                    if (createObectLender == null)
                                    {
                                        UpdateRepeat(item);
                                        continue;
                                    }
                                    var objCreateContact = new CreateLenderContract();
                                    objCreateContact.LoanID = item.LoanID;
                                    objCreateContact.LenderID = item.CustomerID;
                                    objCreateContact.CreateContract = createObectLender;
                                    var createLender = _insurenceService.CreateLenderContract(objCreateContact).Result;
                                    if (createLender != null && createLender.Meta != null && createLender.Meta.ErrCode == (int)ResponseCode_Insurance.Success && createLender.Data != null && !string.IsNullOrEmpty(createLender.Data.Path))
                                    {
                                        item.PartnerCode = (int)Insurance_PartnerCode.BaoMinh;
                                        item.InsuranceMoney = objCreateContact.CreateContract.InsuranceMoney;
                                        item.InsuranceFee = objCreateContact.CreateContract.LenderInsuranceFee;
                                        item.Status = (int)Insurance_Status.SuccessBuyInsurance;
                                        item.DateOfPurchase = DateTime.Now;
                                        item.Path = createLender.Data.Path;
                                        _insuranceTab.Update(item);
                                        _loanService.UpdateLinkInsurance(item.LoanID, (int)Insurance_TypeInsurance.Lender, createLender.Data.Path);
                                    }
                                    else
                                    {
                                        if (createLender != null && createLender.Meta != null && createLender.Meta.ErrCode == (int)ResponseCode_Insurance.Exits)
                                        {
                                            var contactcode = _common.GenContractCodeBaoMinh((long)objLoan.LoanCreditIDOfPartner, objLoan.ContactCode);
                                            var linkLender = _insurenceService.GetLink(contactcode, (int)LMS.Common.Constants.Insurance_TypeInsurance.Lender, item.CustomerID).Result;
                                            if (linkLender != null && linkLender.Meta != null && linkLender.Meta.ErrCode == (int)ResponseCode_Insurance.Success && !string.IsNullOrEmpty(linkLender.Data))
                                            {
                                                item.PartnerCode = (int)Insurance_PartnerCode.BaoMinh;
                                                item.Path = linkLender.Data;
                                                item.Status = (int)Insurance_Status.SuccessBuyInsurance;
                                                _insuranceTab.Update(item);
                                                _loanService.UpdateLinkInsurance(item.LoanID, (int)Insurance_TypeInsurance.Lender, linkLender.Data);
                                            }
                                        }
                                    }

                                }
                                if (item.TypeInsurance == (int)LMS.Common.Constants.Insurance_TypeInsurance.Material)
                                {
                                    _ = BuyInsuranceMaterial(item).Result;
                                }
                                #endregion
                            }
                            else // lấy lại link bh
                            {
                                if (item.TypeInsurance == (int)LMS.Common.Constants.Insurance_TypeInsurance.Customer)
                                {
                                    var contactcode = _common.GenContractCodeBaoMinh((long)objLoan.LoanCreditIDOfPartner, objLoan.ContactCode);
                                    var linkCustomer = _insurenceService.GetLink(contactcode, (int)LMS.Common.Constants.Insurance_TypeInsurance.Customer, item.CustomerID).Result;
                                    if (linkCustomer != null && linkCustomer.Meta != null && linkCustomer.Meta.ErrCode == (int)ResponseCode_Insurance.Success && !string.IsNullOrEmpty(linkCustomer.Data))
                                    {
                                        item.PartnerCode = (int)Insurance_PartnerCode.BaoMinh;
                                        item.Path = linkCustomer.Data;
                                        item.Status = (int)Insurance_Status.SuccessBuyInsurance;
                                        _insuranceTab.Update(item);
                                        _loanService.UpdateLinkInsurance(item.LoanID, (int)Insurance_TypeInsurance.Customer, linkCustomer.Data);
                                    }

                                }
                                if (item.TypeInsurance == (int)LMS.Common.Constants.Insurance_TypeInsurance.Lender)
                                {
                                    var contactcode = _common.GenContractCodeBaoMinh((long)objLoan.LoanCreditIDOfPartner, objLoan.ContactCode);
                                    var linkLender = _insurenceService.GetLink(contactcode, (int)LMS.Common.Constants.Insurance_TypeInsurance.Lender, item.CustomerID).Result;
                                    if (linkLender != null && linkLender.Meta != null && linkLender.Meta.ErrCode == (int)ResponseCode_Insurance.Success && !string.IsNullOrEmpty(linkLender.Data))
                                    {
                                        item.PartnerCode = (int)Insurance_PartnerCode.BaoMinh;
                                        item.Path = linkLender.Data;
                                        item.Status = (int)Insurance_Status.SuccessBuyInsurance;
                                        _insuranceTab.Update(item);
                                        _loanService.UpdateLinkInsurance(item.LoanID, (int)Insurance_TypeInsurance.Lender, linkLender.Data);
                                    }
                                }
                            }
                            UpdateRepeat(item);
                        }
                    }
                    response.SetSucces();
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"ByInsuranceHandler|request={_common.ConvertObjectToJSonV2<ByInsurance>(request)}|ex={ex.Message}--{ex.StackTrace}");
                    return response;
                }
            });
        }

        private bool UpdateRepeat(Domain.Tables.TblInsurance entity)
        {
            entity.Repeat = entity.Repeat + 1;
            if (entity.Repeat == BaoMinhConsants.Repeat)
            {
                _logger.LogError($"{MessageConstant.ByInsuranceError}|request={_common.ConvertObjectToJSonV2(entity)}");
            }
            _insuranceTab.Update(entity);
            return true;
        }

        #region map CreateObectBorrower
        private InputCreateBorrowerContract CreateObectBorrower(Domain.Tables.TblCustomer objCustomer, Domain.Tables.TblLoan objLoan)
        {
            try
            {
                if (objCustomer.BirthDay == null || objCustomer.Gender == null)
                {
                    _logger.LogError($"CreateObectBorrower_CreateObectBorrower|CustomerID:{objCustomer.CustomerID}-null : BirthDay ,Gender");
                    return null;
                }
                var insuranceCustomer = _insurancePremiumsOfCustomersService.InsurancePremiumsByLoan(objLoan.LoanID).Result;
                if (insuranceCustomer == null)
                {
                    _logger.LogError($"CreateObectBorrower_InsurancePremiumsByLoan|LoanID={objLoan.LoanID}");
                    return null;
                }
                var insuranceLender = _insurancePremiumsOfLenderService.InsurancePremiumsByLoanId(objLoan.LoanID).Result;
                if (insuranceLender == null)
                {
                    _logger.LogError($"CreateObectBorrower_InsurancePremiumsByLoanId|LoanID={objLoan.LoanID}");
                    return null;
                }
                var result = new InputCreateBorrowerContract();
                result.ContractCode = string.Empty;
                result.PartnerCode = LMS.Entites.Dtos.InsuranceService.BaoMinhConsants.PartnerCode;
                result.Borrower = new BorrowerItem()
                {
                    Type = (int)TypeCustomer.Personal,//1: cá nhân 2: doanh nghiệp
                    FullName = objCustomer.FullName,
                    IdNumber = objCustomer.NumberCard,
                    IdNumberDate = string.Empty,
                    IdNumberPlace = string.Empty,
                    Address = objCustomer.Address,
                    Dob = objCustomer.BirthDay == null ? string.Empty : objCustomer.BirthDay.Value.ToString("dd/MM/yyyy"),
                    Gender = objCustomer.Gender.Value,
                    Phone = objCustomer.Phone ?? string.Empty,
                    Email = objCustomer.Email ?? string.Empty,
                    TaxCode = string.Empty
                };
                var rate = _common.ConvertVNDToRate((long)objLoan.RateInterest);
                result.Rate = Math.Round(rate, 0);
                result.TotalMoney = objLoan.TotalMoneyDisbursement;
                var totalDays = (int)(objLoan.ToDate - objLoan.FromDate).TotalDays + 1;
                result.InsuranceMoney = insuranceCustomer.InsuranceMoney;
                result.LenderInsuranceFee = (long)insuranceLender.InterestVat;
                result.CustomerInsuranceFee = (long)insuranceCustomer.InterestFees;

                result.LoanTime = totalDays;// entity.LoanTime;
                result.ProductName = objLoan.ProductName;

                result.BorrowerContractCode = _common.GenContractCodeBaoMinh((long)objLoan.LoanCreditIDOfPartner, objLoan.ContactCode);
                result.CustomerCode = string.Format("{0}", objCustomer.CustomerID);
                result.FromDate = objLoan.FromDate.ToString("dd/MM/yyyy");
                result.ToDate = objLoan.ToDate.ToString("dd/MM/yyyy");

                var paramsign = string.Format("{0}{1}{2}{3}{4}", objCustomer.FullName, objCustomer.NumberCard ?? string.Empty, objLoan.TotalMoneyDisbursement, objCustomer.Phone, BaoMinhConsants.SecretKey);
                var projectRootPath = _hostingEnvironment.ContentRootPath;
                var path = Path.Combine(projectRootPath, "Keypress", "private.pem");
                var Signature = Encrypt.SignatureSHA256(paramsign, path);
                result.Signature = Encrypt.SignatureSHA256(paramsign, path);
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ByInsuranceHandler_CreateObectBorrower|request=TblCustomer:{_common.ConvertObjectToJSonV2<Domain.Tables.TblCustomer>(objCustomer)} TblLoan:{_common.ConvertObjectToJSonV2<Domain.Tables.TblLoan>(objLoan)}|ex={ex.Message}|StackTrace={ex.StackTrace}");
            }
            return null;
        }
        #endregion
        #region map CreateObectLender
        private InputCreateLenderContract CreateObectLender(Domain.Tables.TblLoan objLoan, Domain.Tables.TblLender objLender)
        {
            try
            {
                var objCustomer = _customerTab.WhereClause(x => x.CustomerID == objLoan.CustomerID).Query().FirstOrDefault();
                if (objCustomer == null)
                {
                    _logger.LogError($"CreateObectLender_CustomerNull|CustomerID:{objCustomer.CustomerID}-null");
                    return null;
                }
                var insuranceCustomer = _insurancePremiumsOfCustomersService.InsurancePremiumsByLoan(objLoan.LoanID).Result;
                if (insuranceCustomer == null)
                {
                    _logger.LogError($"CreateObectLender_insuranceCustomer|LoanID:{objLoan.LoanID}-null");
                    return null;
                }
                var insuranceLender = _insurancePremiumsOfLenderService.InsurancePremiumsByLoanId(objLoan.LoanID).Result;
                if (insuranceLender == null)
                {
                    _logger.LogError($"CreateObectLender_insuranceLender|LoanID:{objLoan.LoanID}-null");
                    return null;
                }
                var result = new InputCreateLenderContract(); //var insuranceService = new Entity.Objects.APIVBI.BaseVbi();
                result.ContractCode = string.Empty;
                result.PartnerCode = LMS.Entites.Dtos.InsuranceService.BaoMinhConsants.PartnerCode;
                result.Lender = new LenderItem()
                {
                    Type = (int)TypeCustomer.Personal,//1: cá nhân 2: doanh nghiệp
                    FullName = objLender.FullName,
                    IdNumber = objLender.NumberCard ?? string.Empty,
                    IdNumberDate = string.Empty,
                    IdNumberPlace = string.Empty,
                    Address = objLender.Address ?? string.Empty,
                    Dob = objLender.BirthDay != null ? objLender.BirthDay.Value.ToString("dd/MM/yyyy") : string.Empty,
                    Gender = (int)objLender.Gender,
                    Phone = objLender.Phone ?? string.Empty,
                    Email = string.Empty,
                    TaxCode = objLender.TaxCode ?? string.Empty
                };
                result.Borrower = new BorrowerItem()
                {
                    Type = (int)TypeCustomer.Personal,//1: cá nhân 2: doanh nghiệp
                    FullName = objCustomer.FullName,
                    IdNumber = objCustomer.NumberCard,
                    IdNumberDate = string.Empty,
                    IdNumberPlace = string.Empty,
                    Address = objCustomer.Address,
                    Dob = objCustomer.BirthDay != null ? objCustomer.BirthDay.Value.ToString("dd/MM/yyyy") : string.Empty,
                    Gender = objCustomer.Gender.Value,
                    Phone = objCustomer.Phone ?? string.Empty,
                    Email = objCustomer.Email ?? string.Empty,
                    TaxCode = string.Empty
                };
                var rate = _common.ConvertVNDToRate((long)objLoan.RateInterest);
                result.Rate = Math.Round(rate, 0);
                result.TotalMoney = objLoan.TotalMoneyDisbursement;
                var totalDays = (int)(objLoan.ToDate - objLoan.FromDate).TotalDays + 1;
                result.InsuranceMoney = insuranceLender.InsuranceMoney;
                result.LenderInsuranceFee = (long)insuranceLender.InterestVat;
                result.CustomerInsuranceFee = insuranceCustomer.InterestFees;

                result.LoanTime = totalDays;// entity.LoanTime;
                result.ProductName = objLoan.ProductName;

                result.LenderContractCode = _common.GenContractCodeBaoMinh((long)objLoan.LoanCreditIDOfPartner, objLoan.ContactCode);
                result.CustomerCode = string.Format("{0}", objCustomer.CustomerID);
                result.FromDate = objLoan.FromDate.ToString("dd/MM/yyyy");
                result.ToDate = objLoan.ToDate.ToString("dd/MM/yyyy");
                var paramsign = string.Format("{0}{1}{2}{3}{4}", objLender.FullName, objLender.NumberCard ?? string.Empty, objLoan.TotalMoneyDisbursement, objLender.Phone, BaoMinhConsants.SecretKey);
                var projectRootPath = _hostingEnvironment.ContentRootPath;
                var path = Path.Combine(projectRootPath, "Keypress", "private.pem");
                result.Signature = Encrypt.SignatureSHA256(paramsign, path);
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ByInsuranceHandler_CreateObectLender|LenderID={objLender.LenderID}|LoanID={objLoan.LoanID}|ex={ex.Message}--{ex.StackTrace}");
                return null;
            }
        }
        #endregion

        #region mua bao hiem vat chat
        private async Task<bool> BuyInsuranceMaterial(Domain.Tables.TblInsurance entity) // mua bảo hiểm vật chất
        {
            try
            {
                var objLoan = _loanTab.WhereClause(x => x.LoanID == entity.LoanID).Query().FirstOrDefault();

                var insurance = await _insurancePremiumsOfCustomersService.InsurancePremiumsByLoan(entity.LoanID);
                if (insurance == null)
                {
                    _logger.LogError($"BuyInsuranceMaterial_InsurancePremiumsByLoan|LoanCreditIDOfPartner={entity.LoanID}");
                    return false;
                }
                var objLoanLosDetail = await _lOSService.GetLoanCreditDetail((long)objLoan.LoanCreditIDOfPartner);
                if (objLoanLosDetail == null || objLoanLosDetail.LoanBriefProperty == null)
                {
                    _logger.LogError($"BuyInsuranceMaterial_GetLoanCreditDetailLos|LoanCreditIDOfPartner={objLoan.LoanCreditIDOfPartner}");
                    return false;
                }
                var createInsuranceMaterial = new CreateInsuranceMaterial
                {
                    CongTyBaoHiem = String.Empty,
                    DaiLyBaoHiem = BaoMinhConsants.DaiLyBaoHiem,
                    BenMuaBaoHiem = objLoanLosDetail.FullName,
                    SoDienThoai = String.Empty,
                    SoCMND = objLoanLosDetail.NationalCard,
                    DiaChiThuongTru = objLoanLosDetail.AddressNationalCard,
                    BienKiemSoat = objLoanLosDetail.LoanBriefProperty.PlateNumber,
                    SoKhung = objLoanLosDetail.LoanBriefProperty.Chassis,
                    SoMay = objLoanLosDetail.LoanBriefProperty.Engine,
                    LoaiXe = string.Empty,
                    NhanHieu = objLoanLosDetail.LoanBriefProperty.Brand + " - " + objLoanLosDetail.LoanBriefProperty.Product,
                    NamSanXuat = objLoanLosDetail.LoanBriefProperty.YearMade,
                    SoGiayDangKy = String.Empty,
                    NgayCap = objLoan.FromDate.ToString(BaoMinhConsants.DataDDMMYYYY),
                    CoQuanCap = String.Empty,
                    SoHDTinDung = _commonInsurance.SoHDTinDung((long)objLoan.LoanCreditIDOfPartner, objLoan.ContactCode),
                    NgayKyHDTinDung = objLoan.FromDate.ToString(BaoMinhConsants.DataDDMMYYYY),
                    SoHDBaoHiem = String.Empty,
                    GiaTriThamDinh = Convert.ToInt64(objLoanLosDetail.LoanAmountExpertise),
                    SoTienBaoHiem = insurance.InsuranceMoney,
                    PhiBaoHiemThanhToan = insurance.InterestFeesMaterial,
                    Dob = objLoanLosDetail.Dob.ToString(BaoMinhConsants.DataDDMMYYYY),
                    ThoiHanBaoHiemBatDau = objLoan.FromDate.ToString(BaoMinhConsants.DataDDMMYYYYHHMMSS),
                    ThoiHanBaoHiemketThuc = objLoan.ToDate.ToString(BaoMinhConsants.DataDDMMYYYYHHMMSS),
                    NgayCapDon = objLoan.FromDate.ToString(BaoMinhConsants.DataDDMMYYYY),
                    NguoiDuocBaoHiem = objLoanLosDetail.FullName,
                    LoaiSanPham = BaoMinhConsants.LoaiSanPham,
                    PartnerCode = BaoMinhConsants.PartnerCode2
                };
                createInsuranceMaterial.Signature = _commonInsurance.ToMd5(createInsuranceMaterial.SoKhung +
                                                createInsuranceMaterial.SoGiayDangKy +
                                                createInsuranceMaterial.SoHDTinDung +
                                                BaoMinhConsants.PartnerSecretKey2);

                var createInsuranceMaterialReq = new CreateInsuranceMaterialReq
                {
                    CreateInsuranceMaterial = createInsuranceMaterial,
                    LoanID = entity.LoanID,
                    CustomerID = entity.CustomerID
                };
                if (createInsuranceMaterial.NamSanXuat <= 0 || createInsuranceMaterial.GiaTriThamDinh <= 0 ||
                    createInsuranceMaterial.SoTienBaoHiem <= 0 || createInsuranceMaterial.PhiBaoHiemThanhToan <= 0)
                {
                    _logger.LogError($"BuyInsuranceMaterial_CreateInsuranceMaterial_NoPass|NamSanXuat={createInsuranceMaterial.NamSanXuat}|GiaTriThamDinh={createInsuranceMaterial.GiaTriThamDinh}|SoTienBaoHiem={createInsuranceMaterial.SoTienBaoHiem}|PhiBaoHiemThanhToan={createInsuranceMaterial.PhiBaoHiemThanhToan}");
                    return false;
                }
                var createMaterial = await _insurenceService.CreateInsuranceMaterial(createInsuranceMaterialReq);
                if (createMaterial != null && createMaterial.Meta != null && createMaterial.Meta.ErrCode == (int)ResponseCode_Insurance.Success && createMaterial.Data != null && !string.IsNullOrEmpty(createMaterial.Data.Path))
                {
                    entity.PartnerCode = (int)Insurance_PartnerCode.BaoMinh;
                    entity.InsuranceMoney = insurance.InsuranceMoney;
                    entity.InsuranceFee = insurance.InterestFeesMaterial;
                    entity.Status = (int)Insurance_Status.SuccessBuyInsurance;
                    entity.DateOfPurchase = DateTime.Now;
                    entity.Path = createMaterial.Data.Path;
                    entity.MaterialCode = createMaterial.Data.Code;
                    _insuranceTab.Update(entity);
                    _ = await _loanService.UpdateLinkInsurance(entity.LoanID, (int)Insurance_TypeInsurance.Material, createMaterial.Data.Path);

                    return true;
                }
                // cập nhật lại số lần call
                UpdateRepeat(entity);
            }
            catch (Exception ex)
            {
                _logger.LogError($"ByInsuranceHandler_BuyInsuranceMaterial|request={_common.ConvertObjectToJSonV2(entity)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return false;
        }
        #endregion
    }
}
