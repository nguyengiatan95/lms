﻿using LMS.Common.Constants;
using LMS.Common.Helper;
using LMS.InsuranceServiceApi.Constants;
using LMS.InsuranceServiceApi.Domain.Models;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.Queries
{
    public class ReportInsurance : IRequest<Common.Constants.ResponseActionResult>
    {
        public Domain.Models.ReportInsuranceModel ReportInsuranceInfor { get; set; }
    }
    public class ReportInsuranceHandler : IRequestHandler<ReportInsurance, Common.Constants.ResponseActionResult>
    {
        ILogger<ReportInsuranceHandler> _logger;
        Common.Helper.Utils _common;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInsurance> _insuranceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> _customerTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> _lenderTab;
        public ReportInsuranceHandler(ILogger<ReportInsuranceHandler> logger,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInsurance> insuranceTab,
                    LMS.Common.DAL.ITableHelper<Domain.Tables.TblCustomer> customerTab,
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab)
        {
            _logger = logger;
            _common = new Common.Helper.Utils();
            _insuranceTab = insuranceTab;
            _customerTab = customerTab;
            _loanTab = loanTab;
            _lenderTab = lenderTab;
        }
        public async Task<ResponseActionResult> Handle(ReportInsurance request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {

                    var result = new List<ReportInsuranceRespone>();
                    int totalCount = 0;
                    var fromDate = DateTime.ParseExact(request.ReportInsuranceInfor.Fromdate, _common.DateTimeDDMMYYYY, CultureInfo.InvariantCulture);
                    var toDate = DateTime.ParseExact(request.ReportInsuranceInfor.Todate, _common.DateTimeDDMMYYYY, CultureInfo.InvariantCulture).AddDays(1);

                    if ((toDate.Date.Subtract(fromDate)).Days > InsuranceConstants.MaxDateReport)
                    {
                        response.Message = MessageConstant.ReportInsuranceMaxDate;
                        return response;
                    }
                    if (request.ReportInsuranceInfor.LenderID != (int)StatusCommon.Default)
                    {
                        _insuranceTab.WhereClause(x => x.CustomerID == request.ReportInsuranceInfor.LenderID);
                    }
                    var lstInsurance = _insuranceTab.WhereClause(x => x.Status == (int)Insurance_Status.SuccessBuyInsurance && (x.DateOfPurchase >= fromDate && x.DateOfPurchase < toDate)).Query();
                    response.SetSucces();
                    if (lstInsurance == null)
                    {
                        return response;
                    }
                    var lstCustomerID = new List<long>();
                    var lstLenderID = new List<long>();
                    var lstLoanID = new List<long>();
                    var dicLoan = new Dictionary<long, List<Domain.Tables.TblInsurance>>();
                    foreach (var item in lstInsurance)
                    {
                        if (!dicLoan.ContainsKey(item.LoanID))
                            dicLoan.Add(item.LoanID, new List<Domain.Tables.TblInsurance> { item });
                        else
                            dicLoan[item.LoanID].Add(item);
                    }
                    totalCount = dicLoan.Keys.Count;
                    if (totalCount > 0)
                    {
                        dicLoan = dicLoan.OrderByDescending(x => x.Key).Skip((request.ReportInsuranceInfor.PageIndex - 1) * request.ReportInsuranceInfor.PageSize).Take(request.ReportInsuranceInfor.PageSize).ToDictionary(x => x.Key, x => x.Value);
                        foreach (var item in dicLoan)
                        {
                            lstLoanID.Add(item.Key);
                            foreach (var k in item.Value)
                            {
                                if (k.TypeInsurance == (int)LMS.Common.Constants.Insurance_TypeInsurance.Customer || k.TypeInsurance == (int)LMS.Common.Constants.Insurance_TypeInsurance.Material)
                                    lstCustomerID.Add(k.CustomerID);
                                else if (k.TypeInsurance == (int)LMS.Common.Constants.Insurance_TypeInsurance.Lender)
                                    lstLenderID.Add(k.CustomerID);
                            }
                        }
                        var dictCustomerInfos = _customerTab.WhereClause(x => lstCustomerID.Contains(x.CustomerID)).Query().ToDictionary(x => x.CustomerID, x => x);
                        var dictLoanInfos = _loanTab.WhereClause(x => lstLoanID.Contains(x.LoanID)).Query().ToDictionary(x => x.LoanID, x => x);
                        var dictLenderInfos = _lenderTab.WhereClause(x => lstLenderID.Contains(x.LenderID)).Query().ToDictionary(x => x.LenderID, x => x);

                        foreach (var item in dicLoan)
                        {
                            var objLoan = dictLoanInfos.GetValueOrDefault(item.Key);
                            if (objLoan == null)
                                continue;
                            var objInsuranceCustomer = item.Value.FirstOrDefault(x => x.TypeInsurance == (int)Insurance_TypeInsurance.Customer);
                            var objInsuranceLender = item.Value.FirstOrDefault(x => x.TypeInsurance == (int)Insurance_TypeInsurance.Lender);
                            var objInsuranceMaterial = item.Value.FirstOrDefault(x => x.TypeInsurance == (int)Insurance_TypeInsurance.Material);

                            var objCustomer = new Domain.Tables.TblCustomer();
                            var objLender = new Domain.Tables.TblLender();

                            if (objInsuranceCustomer != null)
                                objCustomer = dictCustomerInfos.GetValueOrDefault(objInsuranceCustomer.CustomerID);
                            if (objInsuranceLender != null)
                                objLender = dictLenderInfos.GetValueOrDefault(objInsuranceLender.CustomerID);
                            if (objInsuranceMaterial != null)
                                objCustomer = dictCustomerInfos.GetValueOrDefault(objInsuranceMaterial.CustomerID);

                            var partnerCode = item.Value.FirstOrDefault(x => x.PartnerCode != null);
                            result.Add(new ReportInsuranceRespone
                            {
                                CustomerID = objInsuranceCustomer == null ? 0 : objInsuranceCustomer.CustomerID,
                                LenderID = objInsuranceLender == null ? 0 : objInsuranceLender.CustomerID,
                                LoanID = item.Key,
                                LoanCredit = "HD-" + objLoan.LoanCreditIDOfPartner,
                                Code = objLoan.ContactCode,
                                DisbursementDate = objLoan.FromDate,
                                CustomerName = objCustomer == null ? "" : objCustomer.FullName,
                                LenderName = objLender == null ? "" : objLender.FullName,
                                TotalMoneyDisbursement = objLoan.TotalMoneyDisbursement,
                                InsuranceMoney = item.Value.First().InsuranceMoney,
                                MoneyInsuranceLender = objInsuranceLender == null ? 0 : objInsuranceLender.InsuranceFee,
                                MoneyInsuranceCustomer = objInsuranceCustomer == null ? 0 : objInsuranceCustomer.InsuranceFee,
                                MoneyInsuranceMaterial = objInsuranceMaterial == null ? 0 : objInsuranceMaterial.InsuranceFee,
                                //Status = ((Insurance_Status)status).GetDescription(),
                                LinkCustomer = objInsuranceCustomer == null ? "" : objInsuranceCustomer.Path,
                                LinkInsuranceMaterial = objInsuranceMaterial == null ? "" : objInsuranceMaterial.Path,
                                LinkLender = objInsuranceLender == null ? "" : objInsuranceLender.Path,
                                PartnerCode = partnerCode == null ? "" : ((Insurance_PartnerCode)partnerCode.PartnerCode).GetDescription(),
                                DateOfPurchaseCustomer = objInsuranceCustomer == null ? DateTime.MinValue : objInsuranceCustomer.DateOfPurchase,
                                DateOfPurchaseLender = objInsuranceLender == null ? DateTime.MinValue : objInsuranceLender.DateOfPurchase,
                                DateOfPurchaseMaterial = objInsuranceMaterial == null ? DateTime.MinValue : objInsuranceMaterial.DateOfPurchase,
                                MaterialCode = objInsuranceMaterial == null ? "" : objInsuranceMaterial.MaterialCode
                            });
                        }
                        result = result.OrderByDescending(x => x.DisbursementDate).Skip((request.ReportInsuranceInfor.PageIndex - 1) * request.ReportInsuranceInfor.PageSize).Take(request.ReportInsuranceInfor.PageSize).ToList();
                    }
                    response.Total = totalCount;
                    response.Data = result;
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"ReportInsuranceHandler|request={_common.ConvertObjectToJSonV2<ReportInsurance>(request)}|ex={ex.Message}");
                    return response;
                }
            });
        }
    }
}
