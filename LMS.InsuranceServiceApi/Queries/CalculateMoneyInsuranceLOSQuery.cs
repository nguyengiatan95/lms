﻿using LMS.Common.Constants;
using LMS.InsuranceServiceApi.Domain.Models;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.Queries
{
    public class CalculateMoneyInsuranceLOSQuery : IRequest<Common.Constants.ResponseActionResult>
    {
        public PaymentScheduleReq PaymentSchedule { get; set; }

    }
    public class CalculateMoneyInsuranceLOSQueryHandler : IRequestHandler<CalculateMoneyInsuranceLOSQuery, Common.Constants.ResponseActionResult>
    {
        readonly ILogger<CalculateMoneyInsuranceLOSQueryHandler> _logger;

        readonly RestClients.IPaymentScheduleService _paymentScheduleService;
        readonly Common.Helper.Utils _common;
        readonly Helper.CommonInsurance _commonInsurance;
        public CalculateMoneyInsuranceLOSQueryHandler(ILogger<CalculateMoneyInsuranceLOSQueryHandler> logger,
            RestClients.IPaymentScheduleService paymentScheduleService,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLender> lenderTab)
        {
            _logger = logger;
            _paymentScheduleService = paymentScheduleService;
            _common = new Common.Helper.Utils();
            _commonInsurance = new Helper.CommonInsurance();
        }
        public async Task<ResponseActionResult> Handle(CalculateMoneyInsuranceLOSQuery request, CancellationToken cancellationToken)
        {
            ResponseActionResult response = new ResponseActionResult();
            try
            {
                request.PaymentSchedule.InterestStartDate = DateTime.Now;
                var paymentSchedule = await _paymentScheduleService.PaymentScheduleByCondition(request.PaymentSchedule);
                if (paymentSchedule == null)
                {
                    return response;
                }
                long totalInsuranceMoney = 0;
                foreach (var item in paymentSchedule)
                {
                    totalInsuranceMoney += item.MoneyInterest + item.MoneyOriginal;
                }
                var interestFeesCus = _commonInsurance.InterestFeesCustomer(totalInsuranceMoney);
                var interestFeesMaterial = _commonInsurance.InterestMaterial(totalInsuranceMoney);
                var resultData = new CalculateMoneyInsuranceLOSResponse()
                {
                    FeesInsuranceCustomer = interestFeesCus,
                    FeesInsuranceMaterial = interestFeesMaterial
                };
                response.SetSucces();
                response.Data = resultData;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogWarning($"CalculateMoneyInsuranceLOSQueryHandler|request={_common.ConvertObjectToJSonV2(request)}|ex={ex.Message}-{ex.StackTrace}");
                return response;
            }
        }
    }
}
