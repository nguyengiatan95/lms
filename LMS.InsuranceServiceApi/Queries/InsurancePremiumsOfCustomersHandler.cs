﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.Queries
{
    public class InsurancePremiumsOfCustomers : IRequest<Common.Constants.ResponseActionResult>
    {
        public Domain.Models.PaymentScheduleReq PaymentSchedule { get; set; }

    }
    public class InsurancePremiumsOfCustomersHandler : IRequestHandler<InsurancePremiumsOfCustomers, Common.Constants.ResponseActionResult>
    {
        ILogger<InsurancePremiumsOfCustomersHandler> _logger;
        RestClients.IInsurancePremiumsOfCustomersService _insurancePremiumsOfCustomersService;
        Common.Helper.Utils _common;
        public InsurancePremiumsOfCustomersHandler(ILogger<InsurancePremiumsOfCustomersHandler> logger,
            RestClients.IInsurancePremiumsOfCustomersService insurancePremiumsOfCustomersService)
        {
            _logger = logger;
            _insurancePremiumsOfCustomersService = insurancePremiumsOfCustomersService;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(InsurancePremiumsOfCustomers request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    response.SetSucces();
                    response.Data = _insurancePremiumsOfCustomersService.InsurancePremiums(request.PaymentSchedule).Result;
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"InsurancePremiumsOfCustomersHandler|request={_common.ConvertObjectToJSonV2<InsurancePremiumsOfCustomers>(request)}|ex={ex.Message}");
                    return response;
                }
            });
        }
    }
}
