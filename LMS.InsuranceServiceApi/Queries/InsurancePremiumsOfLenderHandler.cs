﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.Queries
{
    public class InsurancePremiumsOfLender : IRequest<Common.Constants.ResponseActionResult>
    {
        public Domain.Models.PaymentScheduleReq PaymentSchedule { get; set; }
    }
    public class InsurancePremiumsOfLenderHandler : IRequestHandler<InsurancePremiumsOfLender, Common.Constants.ResponseActionResult>
    {
        ILogger<InsurancePremiumsOfLenderHandler> _logger;
        RestClients.IInsurancePremiumsOfLenderService _insurancePremiumsOfLenderService;
        Common.Helper.Utils _common;
        public InsurancePremiumsOfLenderHandler(ILogger<InsurancePremiumsOfLenderHandler> logger,
            RestClients.IInsurancePremiumsOfLenderService insurancePremiumsOfLenderService)
        {
            _logger = logger;
            _insurancePremiumsOfLenderService = insurancePremiumsOfLenderService;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(InsurancePremiumsOfLender request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    response.SetSucces();
                    response.Data = _insurancePremiumsOfLenderService.InsurancePremiums(request.PaymentSchedule).Result;
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"InsurancePremiumsOfLenderHandler|request={_common.ConvertObjectToJSonV2<InsurancePremiumsOfLender>(request)}|ex={ex.Message}");
                    return response;
                }
            });
        }
    }
}
