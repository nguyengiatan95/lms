﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.Domain.Tables
{
    [Dapper.Contrib.Extensions.Table("TblInsurance")]
    public class TblInsurance
    {
        [Dapper.Contrib.Extensions.Key]
        public long InsuranceID { get; set; }

        public long LoanID { get; set; }

        public long CustomerID { get; set; }

        public int? PartnerCode { get; set; }

        public string Path { get; set; }

        public DateTime CreateDate { get; set; }

        public int? TypeInsurance { get; set; }

        public int? Status { get; set; }

        public DateTime? DateOfPurchase { get; set; }

        public long InsuranceMoney { get; set; }

        public long InsuranceFee { get; set; }
        public int Repeat { get; set; }
        public int StatusCallAI { get; set; }
        public int RepeatCallAI { get; set; }

        public string MaterialCode { get; set; }
    }

}
