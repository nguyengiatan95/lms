﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace LMS.InsuranceServiceApi.Domain.Tables
{
    [Table("TblLendingCertificateInformation")]
    public class TblLendingCertificateInformation
    {
        [Key]
        public long LendingCertificateInformationID { get; set; }

        public long? LoanID { get; set; }

        public DateTime? CreateDate { get; set; }

        public string NumberInsurance { get; set; }

        public string CustomerAddress { get; set; }

        public string CustomerContactNumber { get; set; }

        public string CustomerDateOfBirth { get; set; }

        public string CustomerEmail { get; set; }

        public string CustomerFullname { get; set; }

        public string CustomerIdCard { get; set; }

        public string CustomerPhone { get; set; }

        public string FromDate { get; set; }

        public string FromHour { get; set; }

        public string FromMonth { get; set; }

        public string FromYear { get; set; }

        public string ToDate { get; set; }

        public string ToHour { get; set; }

        public string ToMonth { get; set; }

        public string ToYear { get; set; }

        public string InvestorAddress { get; set; }

        public string InvestorContactNumber { get; set; }

        public string InvestorDateOfBirth { get; set; }

        public string InvestorEmail { get; set; }

        public string InvestorFullname { get; set; }

        public string InvestorIdCard { get; set; }

        public string InvestorInvestMoney { get; set; }

        public string InvestorPhone { get; set; }

        public string InvestorProduct { get; set; }

        public string InvestorRate { get; set; }

        public string InvestorTaxCode { get; set; }

        public string MoneyIndemnification { get; set; }

    }
}
