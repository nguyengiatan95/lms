﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.Domain.Models
{
    public class CalculateMoneyInsuranceLOSResponse
    {
        public decimal FeesInsuranceCustomer { get; set; }
        public decimal FeesInsuranceMaterial { get; set; }
    }
}
