﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.Domain.Models
{
    public class PaymentScheduleReq
    {
        [Required]
        public long TotalMoneyDisbursement { get; set; }
        [Required]
        public int LoanTime { get; set; }
        [Required]
        public int LoanFrequency { get; set; }
        [Required]
        public int RateType { get; set; }
        [Required]
        public decimal RateConsultant { get; set; }
        [Required]
        public decimal RateInterest { get; set; }
        //[Required]
        public decimal RateService { get; set; }
        public DateTime InterestStartDate { get; set; }
    }
}
