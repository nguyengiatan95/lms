﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.Domain.Models
{
    public class CreateInsuranceModel 
    {
        public long LoanID { get; set; }
        public long CustomerID { get; set; }
        public long LenderID { get; set; }
    }
}
