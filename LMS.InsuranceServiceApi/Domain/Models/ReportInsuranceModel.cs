﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.Domain.Models
{
    public class ReportInsuranceModel
    {
        public string Fromdate { get; set; }
        public string Todate { get; set; }
        public long LenderID { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public class ReportInsuranceRespone
    {
        public long LoanID { get; set; }
        public string LoanCredit { get; set; } // mã hđ đăng ký
        public string Code { get; set; } // mã hd giải giải ngân
        public long CustomerID { get; set; }
        public string CustomerName { get; set; } // tên kh
        public long LenderID { get; set; }
        public string LenderName { get; set; } // đôi tác cho vay
        public long TotalMoneyDisbursement{ get; set; } // số tiền giải ngân
        public long InsuranceMoney { get; set; } // tiền mua bảo hiểm
        public string TypeInsurance { get; set; }
        public long MoneyInsuranceTima { get; set; }
        public long MoneyInsuranceLender { get; set; }//tiền bảo hiểm đại lý phai đóng
        public long MoneyInsuranceCustomer { get; set; } //tiền bảo hiểm tima thu
        public string Status { get; set; } // trạng thái
        public string LinkCustomer { get; set; }
        public string LinkLender { get; set; }
        public string PartnerCode { get; set; }// loai bảo hiểm
        public DateTime? DateOfPurchaseLender { get; set; }
        public DateTime ? DateOfPurchaseCustomer { get; set; }
        public DateTime? DateOfPurchaseMaterial { get; set; }
        public DateTime? DisbursementDate { get; set; }
        public long MoneyInsuranceMaterial { get; set; }//tiền bảo hiểm vật chất
        public string LinkInsuranceMaterial { get; set; }
        public string MaterialCode { get; set; }
    }
}
