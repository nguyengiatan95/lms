﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.Domain.Models
{
    public class InsurancePremiumsOfCustomersResponse
    {
        public long InsuranceMoney { get; set; }
        public long InterestFees { get; set; }
        public long InterestFeesMaterial { get; set; }
    }
}
