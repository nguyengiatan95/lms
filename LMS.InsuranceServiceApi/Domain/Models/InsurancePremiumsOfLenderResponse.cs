﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.Domain.Models
{
    public class InsurancePremiumsOfLenderResponse
    {
        public long InsuranceMoney { get;set;}
        public long InterestVat { get; set; }
        public long InterestNotVat { get; set; }
    }
}
