﻿using LMS.Common.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.Commands.Insurance
{
    public class CreateInsuranceOfCustomerCommand : IRequest<LMS.Common.Constants.ResponseActionResult>
    {
        [Required]
        public long LoanID { get; set; }
        [Required]
        public long CustomerID { get; set; }
    }
    public class CreateInsuranceOfCustomerCommandHandler : IRequestHandler<CreateInsuranceOfCustomerCommand, LMS.Common.Constants.ResponseActionResult>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInsurance> _insuranceTab;
        ILogger<CreateInsuranceOfCustomerCommandHandler> _logger;
        Common.Helper.Utils _common;
        public CreateInsuranceOfCustomerCommandHandler(LMS.Common.DAL.ITableHelper<Domain.Tables.TblInsurance> insuranceTab, ILogger<CreateInsuranceOfCustomerCommandHandler> logger)
        {
            _insuranceTab = insuranceTab;
            _logger = logger;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(CreateInsuranceOfCustomerCommand request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    var checkInsurance = _insuranceTab.WhereClause(x => x.LoanID == request.LoanID &&
                                                                        x.CustomerID == request.CustomerID &&
                                                                        x.TypeInsurance == (int)LMS.Common.Constants.Insurance_TypeInsurance.Customer).Query().FirstOrDefault();
                    if (checkInsurance != null)
                    {
                        response.Message = MessageConstant.InsuranceExist;
                        return response;
                    }
                    var objInsurance = new Domain.Tables.TblInsurance
                    {
                        LoanID = request.LoanID,
                        CustomerID = request.CustomerID,
                        TypeInsurance = (int)LMS.Common.Constants.Insurance_TypeInsurance.Customer,
                        CreateDate = DateTime.Now,
                        Status = (int)LMS.Common.Constants.Insurance_Status.WaitingBuyInsurance
                    };
                    var insertID = _insuranceTab.Insert(objInsurance);
                    if (insertID > 0)
                    {
                        response.Result = (int)LMS.Common.Constants.ResponseAction.Success;
                        response.Data = insertID;
                        response.Message = MessageConstant.Success;
                    }
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"CreateInsuranceOfCustomerCommandHandler|request={_common.ConvertObjectToJSonV2<CreateInsuranceOfCustomerCommand>(request)}|ex={ex.Message}");
                    return response;
                }
            });
        }
    }
}
