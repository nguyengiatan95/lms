﻿using LMS.Common.Constants;
using LMS.Entites.Dtos.InsuranceService;
using LMS.InsuranceServiceApi.Constants;
using LMS.InsuranceServiceApi.Domain.Models;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace LMS.InsuranceServiceApi.Commands
{
    public class GetInformationInsuranceAICommand : IRequest<Common.Constants.ResponseActionResult>
    {
    }
    public class GetInformationInsuranceAICommandHandler : IRequestHandler<GetInformationInsuranceAICommand, Common.Constants.ResponseActionResult>
    {
        ILogger<GetInformationInsuranceAICommandHandler> _logger;
        RestClients.IInsurenceService _insurenceService;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblInsurance> _insuranceTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLendingCertificateInformation> _lendingCertificateInformationTab;
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> _settingKeyTab;
        Common.Helper.Utils _common;
        public GetInformationInsuranceAICommandHandler(
            ILogger<GetInformationInsuranceAICommandHandler> logger,
            RestClients.IInsurenceService insurenceService,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblInsurance> insuranceTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLendingCertificateInformation> lendingCertificateInformationTab,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblSettingKey> settingKeyTab
            )
        {
            _logger = logger;
            _insurenceService = insurenceService;
            _insuranceTab = insuranceTab;
            _lendingCertificateInformationTab = lendingCertificateInformationTab;
            _settingKeyTab = settingKeyTab;
            _common = new Common.Helper.Utils();
        }
        public async Task<ResponseActionResult> Handle(GetInformationInsuranceAICommand request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                ResponseActionResult response = new ResponseActionResult();
                try
                {
                    var keySettingInfo = _settingKeyTab.WhereClause(x => x.KeyName == LMS.Common.Constants.SettingKey_KeyValue.Insurance_InformationInsuranceAI).Query().FirstOrDefault();
                    if (keySettingInfo == null || keySettingInfo.Status == 0)
                    {
                        response.Message = MessageConstant.SettingKey_Insurance_InformationInsuranceAI;
                        return response;
                    }
                    response.SetSucces();
                    var lstInsurance = _insuranceTab.SetGetTop(50).WhereClause(x => x.TypeInsurance == (int)Insurance_TypeInsurance.Lender &&
                                                x.Status == (int)Insurance_Status.SuccessBuyInsurance &&
                                                x.RepeatCallAI < InsuranceAIConsants.Repeat &&
                                                (x.StatusCallAI != (int)Insurance_StatusCallAI.Success || x.StatusCallAI == null) &&
                                                x.Path != null).Query();
                    if (lstInsurance == null || lstInsurance.Count() <= 0)
                    {
                        return response;
                    }
                    foreach (var item in lstInsurance)
                    {
                        var objInsuranceAI = _insurenceService.GetInformationInsuranceAI(item.Path, (int)TypeCallInsuranceAI.BaoMinh).Result;
                        if (objInsuranceAI != null)
                        {
                            var objMap = MapLendingCertificateInformation(objInsuranceAI, item.LoanID);
                            if (objMap == null)
                                continue;

                            var insertID = _lendingCertificateInformationTab.Insert(objMap);
                            if (insertID > 0)
                            {
                                item.StatusCallAI = (int)Insurance_StatusCallAI.Success;
                                _insuranceTab.Update(item);
                            }
                        }
                        UpdateRepeatCallAI(item);
                    }
                    return response;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"GetInformationInsuranceAICommandHandler|ex={ex.Message}|{ex.StackTrace}");
                }
                return response;
            });
        }
        private bool UpdateRepeatCallAI(Domain.Tables.TblInsurance entity)
        {
            entity.RepeatCallAI = entity.RepeatCallAI + 1;
            if (entity.RepeatCallAI == InsuranceAIConsants.Repeat)
            {
                _logger.LogError($"{MessageConstant.CallAIInsuranceError}|request={_common.ConvertObjectToJSonV2(entity)}");
            }
            _insuranceTab.Update(entity);
            return true;
        }
        private Domain.Tables.TblLendingCertificateInformation MapLendingCertificateInformation(LoanCertificateInformation obj, long LoanID)
        {
            var ObjLendingCertificateInformation = new Domain.Tables.TblLendingCertificateInformation();
            try
            {
                ObjLendingCertificateInformation.LoanID = (int)LoanID;
                ObjLendingCertificateInformation.NumberInsurance = obj.Number;
                ObjLendingCertificateInformation.CustomerAddress = obj.CustomerInfo.Address;
                ObjLendingCertificateInformation.CustomerContactNumber = obj.CustomerInfo.ContactNumber;
                ObjLendingCertificateInformation.CustomerDateOfBirth = obj.CustomerInfo.DateOfBirth;
                ObjLendingCertificateInformation.CustomerEmail = obj.CustomerInfo.Email;
                ObjLendingCertificateInformation.CustomerFullname = obj.CustomerInfo.Fullname;
                ObjLendingCertificateInformation.CustomerIdCard = obj.CustomerInfo.IdCard;
                ObjLendingCertificateInformation.CustomerPhone = obj.CustomerInfo.Phone;
                ObjLendingCertificateInformation.FromDate = _common.ConvertNullToEmpty(obj.InsuranceInfo.FromDate);
                ObjLendingCertificateInformation.FromHour = _common.ConvertNullToEmpty(obj.InsuranceInfo.FromHour);
                ObjLendingCertificateInformation.FromMonth = _common.ConvertNullToEmpty(obj.InsuranceInfo.FromMonth);
                ObjLendingCertificateInformation.FromYear = _common.ConvertNullToEmpty(obj.InsuranceInfo.FromYear);
                ObjLendingCertificateInformation.ToDate = obj.InsuranceInfo.ToDate;
                ObjLendingCertificateInformation.ToHour = _common.ConvertNullToEmpty(obj.InsuranceInfo.ToHour);
                ObjLendingCertificateInformation.ToMonth = _common.ConvertNullToEmpty(obj.InsuranceInfo.ToMonth);
                ObjLendingCertificateInformation.ToYear = _common.ConvertNullToEmpty(obj.InsuranceInfo.ToYear);
                ObjLendingCertificateInformation.MoneyIndemnification = obj.InsuranceInfo.Money;

                ObjLendingCertificateInformation.InvestorAddress = obj.InvestorInfo.Address;
                ObjLendingCertificateInformation.InvestorContactNumber = obj.InvestorInfo.ContactNumber;
                ObjLendingCertificateInformation.InvestorDateOfBirth = obj.InvestorInfo.DateOfBirth;
                ObjLendingCertificateInformation.InvestorEmail = obj.InvestorInfo.Email;
                ObjLendingCertificateInformation.InvestorFullname = obj.InvestorInfo.Fullname;
                ObjLendingCertificateInformation.InvestorIdCard = obj.InvestorInfo.IdCard;
                ObjLendingCertificateInformation.InvestorInvestMoney = obj.InvestorInfo.InvestMoney;
                ObjLendingCertificateInformation.InvestorPhone = obj.InvestorInfo.Phone;
                ObjLendingCertificateInformation.InvestorProduct = obj.InvestorInfo.Product;
                ObjLendingCertificateInformation.InvestorRate = obj.InvestorInfo.InvestMoney;
                ObjLendingCertificateInformation.InvestorTaxCode = obj.InvestorInfo.InvestMoney;
                ObjLendingCertificateInformation.CreateDate = DateTime.Now;
            }
            catch (Exception ex)
            {
                _logger.LogError($"MapLendingCertificateInformation|request={_common.ConvertObjectToJSonV2(obj)}|ex={ex.Message}|{ex.StackTrace}");
            }
            return ObjLendingCertificateInformation;
        }

    }
}
