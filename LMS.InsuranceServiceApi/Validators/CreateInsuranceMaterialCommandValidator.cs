﻿using FluentValidation;
using LMS.Common.Constants;
using LMS.InsuranceServiceApi.Commands.Insurance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace LMS.InsuranceServiceApi.Validators
{
    public class CreateInsuranceMaterialCommandValidator : AbstractValidator<CreateInsuranceMaterialCommand>
    {
        public CreateInsuranceMaterialCommandValidator()
        {
            RuleFor(x => x.LoanID).GreaterThan(0).WithMessage(MessageConstant.LoanIDNull);
            RuleFor(x => x.CustomerID).GreaterThan(0).WithMessage(MessageConstant.CustomerIDNull);
        }
    }
}
