﻿using FluentValidation;
using LMS.InsuranceServiceApi.Commands.Insurance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.Validators
{
    public class CreateInsuranceOfLenderCommandValidator : AbstractValidator<CreateInsuranceOfLenderCommand>
    {
        public CreateInsuranceOfLenderCommandValidator()
        {
            RuleFor(x => x.LoanID).GreaterThan(0).WithMessage("LoanID invalid");
            RuleFor(x => x.LenderID).GreaterThan(0).WithMessage("LenderID invalid");
        }
    }
    
}
