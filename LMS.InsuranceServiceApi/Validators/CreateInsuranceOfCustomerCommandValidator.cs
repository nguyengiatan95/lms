﻿using FluentValidation;
using LMS.InsuranceServiceApi.Commands.Insurance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.Validators
{
    public class CreateInsuranceOfCustomerCommandValidator : AbstractValidator<CreateInsuranceOfCustomerCommand>
    {
        public CreateInsuranceOfCustomerCommandValidator()
        {
            RuleFor(x => x.LoanID).GreaterThan(0).WithMessage("LoanID invalid");
            RuleFor(x => x.CustomerID).GreaterThan(0).WithMessage("CustomerID invalid");
        }
    }
}
