﻿using FluentValidation;
using LMS.InsuranceServiceApi.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.Validators
{
    public class PaymentScheduleQueryValidator : AbstractValidator<PaymentScheduleReq>
    {
        public PaymentScheduleQueryValidator()
        {
            RuleFor(x => x.TotalMoneyDisbursement).GreaterThan(0).WithMessage("TotalMoneyDisbursement invalid");
            RuleFor(x => x.LoanTime).GreaterThan(0).WithMessage("LoanTime invalid");
            RuleFor(x => x.LoanFrequency).GreaterThanOrEqualTo(0).WithMessage("LoanFrequency invalid");
            RuleFor(x => x.RateType).GreaterThanOrEqualTo(0).WithMessage("RateType invalid");
            RuleFor(x => x.RateConsultant).GreaterThanOrEqualTo(0).WithMessage("RateConsultant invalid");
            //RuleFor(x => x.RateService).GreaterThanOrEqualTo(0).WithMessage("RateService invalid");
            RuleFor(x => x.RateInterest).GreaterThanOrEqualTo(0).WithMessage("RateInterest invalid");
        }
    }
}
