﻿using LMS.InsuranceServiceApi.Domain.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.RestClients
{
    public interface IInsurancePremiumsOfLenderService
    {
        Task<InsurancePremiumsOfLenderResponse> InsurancePremiums(PaymentScheduleReq req);
        Task<InsurancePremiumsOfLenderResponse> InsurancePremiumsByLoanId(long LoanID);
    }
    public class InsurancePremiumsOfLenderService : IInsurancePremiumsOfLenderService
    {
        ILogger<InsurancePremiumsOfLenderService> _logger;
        RestClients.IPaymentScheduleService _paymentScheduleService;
        Common.Helper.Utils _common;
        Helper.CommonInsurance _commonInsurance;
        public InsurancePremiumsOfLenderService(ILogger<InsurancePremiumsOfLenderService> logger,
            RestClients.IPaymentScheduleService paymentScheduleService)
        {
            _logger = logger;
            _paymentScheduleService = paymentScheduleService;
            _common = new Common.Helper.Utils();
            _commonInsurance = new Helper.CommonInsurance();
        }
        public async Task<InsurancePremiumsOfLenderResponse> InsurancePremiums(PaymentScheduleReq request)
        {
            try
            {
                var result = new InsurancePremiumsOfLenderResponse();
                request.InterestStartDate = DateTime.Now;
                var paymentSchedule = _paymentScheduleService.PaymentScheduleByCondition(request).Result;
                if (paymentSchedule == null)
                {
                    _logger.LogWarning($"InsurancePremiumsOfLenderService|request={_common.ConvertObjectToJSonV2<PaymentScheduleReq>(request)}");
                    return null;
                }
                long totalInsuranceMoney = 0;
                foreach (var item in paymentSchedule)
                {
                    totalInsuranceMoney += item.MoneyInterest + item.MoneyOriginal;
                }
                var interestNotVat = _commonInsurance.InterestNotVatLender(totalInsuranceMoney); //* 3%
                if (interestNotVat < Constants.InsuranceConstants.Lender_InterestNotVat)
                {
                    interestNotVat = Constants.InsuranceConstants.Lender_InterestNotVat;
                }
                var interestVat = _commonInsurance.InterestVatLender(interestNotVat); // * 10%
                result.InsuranceMoney = totalInsuranceMoney;
                result.InterestNotVat = Convert.ToInt64(interestNotVat);
                result.InterestVat = interestVat;
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError($"InsurancePremiumsOfLenderService|request={_common.ConvertObjectToJSonV2<PaymentScheduleReq>(request)}|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }

        public async Task<InsurancePremiumsOfLenderResponse> InsurancePremiumsByLoanId(long LoanID)
        {
            try
            {
                var result = new InsurancePremiumsOfLenderResponse();
                var paymentSchedule =  _paymentScheduleService.GetLstPaymentByLoanID((int)LoanID).Result;
                if (paymentSchedule == null)
                {
                    _logger.LogWarning($"InsurancePremiumsOfLenderService|LoanID={LoanID}");
                    return null;
                }
                long totalInsuranceMoney = 0;
                foreach (var item in paymentSchedule)
                {
                    totalInsuranceMoney += item.MoneyInterest + item.MoneyOriginal;
                }
                var interestNotVat = _commonInsurance.InterestNotVatLender(totalInsuranceMoney); //* 3%
                if (interestNotVat < Constants.InsuranceConstants.Lender_InterestNotVat)
                {
                    interestNotVat = Constants.InsuranceConstants.Lender_InterestNotVat;
                }
                var interestVat = _commonInsurance.InterestVatLender(interestNotVat); // * 10%
                result.InsuranceMoney = totalInsuranceMoney;
                result.InterestNotVat = Convert.ToInt64(interestNotVat);
                result.InterestVat = interestVat;
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError($"InsurancePremiumsOfLenderService|LoanID={LoanID}|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }
    }
}
