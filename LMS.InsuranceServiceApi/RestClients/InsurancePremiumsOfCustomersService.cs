﻿using LMS.InsuranceServiceApi.Domain.Models;
using LMS.InsuranceServiceApi.Helper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.RestClients
{
    public interface IInsurancePremiumsOfCustomersService
    {
        Task<InsurancePremiumsOfCustomersResponse> InsurancePremiums(PaymentScheduleReq req);
        Task<InsurancePremiumsOfCustomersResponse> InsurancePremiumsByLoan(long LoanID);
    }
    public class InsurancePremiumsOfCustomersService : IInsurancePremiumsOfCustomersService
    {
        ILogger<InsurancePremiumsOfCustomersService> _logger;
        RestClients.IPaymentScheduleService _paymentScheduleService;
        Common.Helper.Utils _common;
        CommonInsurance _commonInsurance;
        public InsurancePremiumsOfCustomersService(ILogger<InsurancePremiumsOfCustomersService> logger,
               RestClients.IPaymentScheduleService paymentScheduleService)
        {
            _logger = logger;
            _paymentScheduleService = paymentScheduleService;
            _common = new Common.Helper.Utils();
            _commonInsurance = new CommonInsurance();
        }
        public async Task<InsurancePremiumsOfCustomersResponse> InsurancePremiums(PaymentScheduleReq req)
        {
            try
            {
                var result = new InsurancePremiumsOfCustomersResponse();
                req.InterestStartDate = DateTime.Now;
                var paymentSchedule = _paymentScheduleService.PaymentScheduleByCondition(req).Result;
                if (paymentSchedule == null)
                {
                    _logger.LogWarning($"InsurancePremiumsOfCustomersService|request={_common.ConvertObjectToJSonV2<PaymentScheduleReq>(req)}");
                    return null;
                }
                long totalInsuranceMoney = 0;
                foreach (var item in paymentSchedule)
                {
                    totalInsuranceMoney += item.MoneyInterest + item.MoneyOriginal;
                }
                var interestFees = _commonInsurance.InterestFeesCustomer(totalInsuranceMoney);
                result.InsuranceMoney = totalInsuranceMoney;
                result.InterestFees = Convert.ToInt64(interestFees);
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError($"InsurancePremiumsOfCustomersService_InsurancePremiums|request={_common.ConvertObjectToJSonV2<PaymentScheduleReq>(req)}|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }
        public async Task<InsurancePremiumsOfCustomersResponse> InsurancePremiumsByLoan(long LoanID)
        {
            try
            {
                var result = new InsurancePremiumsOfCustomersResponse();
                var paymentSchedule = await _paymentScheduleService.GetLstPaymentByLoanID((int)LoanID);
                if (paymentSchedule == null)
                {
                    _logger.LogWarning($"InsurancePremiumsOfCustomersService|LoanID={LoanID}");
                    return null;
                }
                long totalInsuranceMoney = 0;
                foreach (var item in paymentSchedule)
                {
                    totalInsuranceMoney += item.MoneyInterest + item.MoneyOriginal;
                }
                var interestFees = _commonInsurance.InterestFeesCustomer(totalInsuranceMoney);
                var interestFeesMaterial = _commonInsurance.InterestMaterial(totalInsuranceMoney);
                result.InsuranceMoney = totalInsuranceMoney;
                result.InterestFees = Convert.ToInt64(interestFees);
                result.InterestFeesMaterial = Convert.ToInt64(interestFeesMaterial);
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError($"InsurancePremiumsOfCustomersService_InsurancePremiumsByLoan|LoanID={LoanID}|ex={ex.Message}-{ex.StackTrace}");
                return null;
            }
        }
    }
}
