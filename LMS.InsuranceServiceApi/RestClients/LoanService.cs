﻿using LMS.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.RestClients
{
    public interface ILoanService
    {
        Task<ResponseActionResult> UpdateLinkInsurance(long loanID,int typeInsurance,string path);
    }
    public class LoanService : ILoanService
    {
        LMS.Common.Helper.IApiHelper _apiHelper;
        public LoanService(LMS.Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }

        public async Task<ResponseActionResult> UpdateLinkInsurance(long loanID, int typeInsurance, string path)
        {
            var req = new
            {
                TypeInsurance = typeInsurance,
                Path = path,
                LoanID = loanID
            };
            var url = $"{Common.Constants.ServiceHostInternal.LoanService}{Common.Constants.ActionApiInternal.Loan_Action_UpdateLinkInsurance}";
            return await _apiHelper.ExecuteAsync(url: url.ToString(), RestSharp.Method.POST, req);
        }
    }
}
