﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.RestClients
{
    public interface ILOSService
    {
        Task<LMS.Entites.Dtos.LOSServices.ResultLoanWaitingDisbursementDetailLOS> GetLoanCreditDetail(long loanID);
    }
    public class LOSService : ILOSService
    {
        Common.Helper.IApiHelper _apiHelper;
        public LOSService(Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }

        public async Task<LMS.Entites.Dtos.LOSServices.ResultLoanWaitingDisbursementDetailLOS> GetLoanCreditDetail(long loanID)
        {
            var url = $"{LMS.Common.Constants.ServiceHostInternal.OutGateway}{Common.Constants.ActionApiInternal.OutGatewayApi_LOS_GetLoanCreditDetail}/{loanID}";
            return await _apiHelper.ExecuteAsync<LMS.Entites.Dtos.LOSServices.ResultLoanWaitingDisbursementDetailLOS>(url: url.ToString());
        }
    }
}

