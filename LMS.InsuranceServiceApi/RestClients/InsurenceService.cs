﻿using LMS.Entites.Dtos.InsuranceService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.RestClients
{
    public interface IInsurenceService
    {
        Task<OutputCreateBorrowerContract> CreateBorrowerContract(CreateBorrowerContract entity);
        Task<OutputCreateLenderContract> CreateLenderContract(CreateLenderContract entity);
        Task<OutputGetDoc> GetLink(string strContract, int type, long ID);
        Task<LoanCertificateInformation> GetInformationInsuranceAI(string Path, int DocType);

        Task<ResultBuyInsuranceMaterial> CreateInsuranceMaterial(CreateInsuranceMaterialReq entity);
    }
    public class InsurenceService : IInsurenceService
    {
        Common.Helper.IApiHelper _apiHelper;
        public InsurenceService(Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }
        public async Task<OutputCreateBorrowerContract> CreateBorrowerContract(CreateBorrowerContract req)
        {
            var url = $"{Common.Constants.ServiceHostInternal.OutGateway}{Common.Constants.ActionApiInternal.OutGatewayApi_CreateBorrowerContract}";
            return await _apiHelper.ExecuteAsync<OutputCreateBorrowerContract>(url: url.ToString(), RestSharp.Method.POST, req);
        }
        public async Task<OutputCreateLenderContract> CreateLenderContract(CreateLenderContract req)
        {
            var url = $"{Common.Constants.ServiceHostInternal.OutGateway}{Common.Constants.ActionApiInternal.OutGatewayApi_CreateLenderContract}";
            return await _apiHelper.ExecuteAsync<OutputCreateLenderContract>(url: url.ToString(), RestSharp.Method.POST, req);
        }
        public async Task<OutputGetDoc> GetLink(string strContract, int type, long id)
        {
            var url = $"{Common.Constants.ServiceHostInternal.OutGateway}{Common.Constants.ActionApiInternal.OutGatewayApi_GetLink}?contract={strContract}&type={type}&ID={id}";
            return await _apiHelper.ExecuteAsync<OutputGetDoc>(url: url.ToString());
        }
        public async Task<LoanCertificateInformation> GetInformationInsuranceAI(string path, int docType)
        {
            var url = $"{Common.Constants.ServiceHostInternal.OutGateway}{Common.Constants.ActionApiInternal.OutGatewayApi_GetInformationInsuranceAI}?Path={path}&DocType={docType}";
            return await _apiHelper.ExecuteAsync<LoanCertificateInformation>(url: url.ToString());
        }

        public async Task<ResultBuyInsuranceMaterial> CreateInsuranceMaterial(CreateInsuranceMaterialReq req)
        {
            var url = $"{Common.Constants.ServiceHostInternal.OutGateway}{Common.Constants.ActionApiInternal.OutGatewayApi_CreateInsuranceMaterial}";
            return await _apiHelper.ExecuteAsync<ResultBuyInsuranceMaterial>(url: url.ToString(), RestSharp.Method.POST, req);
        }
    }
}
