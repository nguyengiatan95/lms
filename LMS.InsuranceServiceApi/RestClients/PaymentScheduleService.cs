﻿using AutoMapper.Configuration;
using LMS.InsuranceServiceApi.Domain.Models;
using Polly;
using Polly.CircuitBreaker;
using Polly.Retry;
using Steeltoe.Common.Discovery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.RestClients
{
 
    public interface IPaymentScheduleService
    {
        Task<IEnumerable<LMS.Entites.Dtos.PaymentServices.PaymentScheduleModel>> PaymentScheduleByCondition(PaymentScheduleReq entity);
        Task<IEnumerable<LMS.Entites.Dtos.PaymentServices.PaymentScheduleModel>> GetLstPaymentByLoanID(int loanID);
    }
    public class PaymentScheduleService : IPaymentScheduleService
    {
        Common.Helper.IApiHelper _apiHelper;
        public PaymentScheduleService(Common.Helper.IApiHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }
        public async Task<IEnumerable<LMS.Entites.Dtos.PaymentServices.PaymentScheduleModel>> GetLstPaymentByLoanID(int loanID)
        {
            var url = $"{Common.Constants.ServiceHostInternal.PaymentService}{Common.Constants.ActionApiInternal.PaymentSchedule_GetLstPaymentByLoanID}/{loanID}";
            return await _apiHelper.ExecuteAsync<IEnumerable<LMS.Entites.Dtos.PaymentServices.PaymentScheduleModel>>(url: url.ToString());
        }
        public async Task<IEnumerable<LMS.Entites.Dtos.PaymentServices.PaymentScheduleModel>> PaymentScheduleByCondition(PaymentScheduleReq req)
        {
            var url = new Uri($"{Common.Constants.ServiceHostInternal.PaymentService}{Common.Constants.ActionApiInternal.PaymentSchedule_GenPaymentScheduleByCondition}");
            return await _apiHelper.ExecuteAsync<IEnumerable<LMS.Entites.Dtos.PaymentServices.PaymentScheduleModel>>(url: url.ToString(), RestSharp.Method.POST, req);
        }
    }
}
