﻿using LMS.InsuranceServiceApi.RestClients;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.RestClients
{
    public static class RestClientsInstaller
    {
        public static IServiceCollection AddRestClientsService(this IServiceCollection services)
        {
            services.AddTransient(typeof(IPaymentScheduleService), typeof(PaymentScheduleService));
            services.AddTransient(typeof(IInsurancePremiumsOfCustomersService), typeof(InsurancePremiumsOfCustomersService));
            services.AddTransient(typeof(IInsurancePremiumsOfLenderService), typeof(InsurancePremiumsOfLenderService));
            services.AddTransient(typeof(IInsurenceService), typeof(InsurenceService));
            services.AddTransient(typeof(ILoanService), typeof(LoanService));
            services.AddTransient(typeof(ILOSService), typeof(LOSService));
            return services;
        }
    }
}
