﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LMS.Common.Constants;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Steeltoe.Common.Discovery;

namespace LMS.InsuranceServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InsuranceController : ControllerBase
    {
        private readonly IMediator _bus;
        public InsuranceController(IMediator bus)
        {
            _bus = bus;
        }

        [HttpPost]
        [Route("InsurancePremiumsOfCustomers")]
        public async Task<ResponseActionResult> InsurancePremiumsOfCustomers(Domain.Models.PaymentScheduleReq req)
        {
            var result = await _bus.Send(new Queries.InsurancePremiumsOfCustomers
            {
                PaymentSchedule = req
            });
            return result;
        }
        [HttpPost]
        [Route("InsurancePremiumsOfLender")]
        public async Task<ResponseActionResult> InsurancePremiumsOfLender(Domain.Models.PaymentScheduleReq req)
        {
            var result = await _bus.Send(new Queries.InsurancePremiumsOfLender
            {
                PaymentSchedule = req
            });
            return result;
        }

        [HttpPost]
        [Route("CreateInsuranceOfLender")]
        public async Task<ResponseActionResult> CreateInsuranceOfLender(Commands.Insurance.CreateInsuranceOfLenderCommand req)
        {
            return await _bus.Send(req);
        }
        [HttpPost]
        [Route("CreateInsuranceOfCustomer")]
        public async Task<ResponseActionResult> CreateInsuranceOfCustomer(Commands.Insurance.CreateInsuranceOfCustomerCommand req)
        {
            return await _bus.Send(req);
        }
        [HttpPost]
        [Route("CreateInsuranceMaterial")]
        public async Task<ResponseActionResult> CreateInsuranceMaterial(Commands.Insurance.CreateInsuranceMaterialCommand req)
        {
            return await _bus.Send(req);
        }
        [HttpGet]
        [Route("ByInsurance")]
        public async Task<ResponseActionResult> ByInsurance()
        {
            var result = await _bus.Send(new Queries.ByInsurance());
            return result;
        }
        [HttpPost]
        [Route("ReportInsurance")]
        public async Task<ResponseActionResult> ReportInsurance(Domain.Models.ReportInsuranceModel req)
        {
            var result = await _bus.Send(new Queries.ReportInsurance
            {
                ReportInsuranceInfor = req
            });
            return result;
        }
        [HttpGet]
        [Route("InformationInsuranceAI")]
        public async Task<ResponseActionResult> InformationInsuranceAI()
        {
            var result = await _bus.Send(new Commands.GetInformationInsuranceAICommand());
            return result;
        }

        [HttpPost]
        [Route("CalculateMoneyInsuranceLOS")]
        public async Task<ResponseActionResult> CalculateMoneyInsuranceLOS(Domain.Models.PaymentScheduleReq req)
        {
            var result = await _bus.Send(new Queries.CalculateMoneyInsuranceLOSQuery
            {
                PaymentSchedule = req
            });
            return result;
        }
    }
}