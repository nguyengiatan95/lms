﻿using LMS.Common.Constants;
using LMS.Kafka.Interfaces;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.KafkaEventHandlers
{
    public class CreateInsuranceByLoanIDHandler : IKafkaHandler<string, LMS.Kafka.Messages.Loan.LoanInsertSuccess>
    {
        LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> _loanTab;
        private readonly IMediator _bus;
        LMS.Common.Helper.Utils _common = new Common.Helper.Utils();
        RestClients.ILOSService _lOSService;
        ILogger<CreateInsuranceByLoanIDHandler> _logger;
        public CreateInsuranceByLoanIDHandler(IMediator bus,
            LMS.Common.DAL.ITableHelper<Domain.Tables.TblLoan> loanTab,
            RestClients.ILOSService lOSService,
            ILogger<CreateInsuranceByLoanIDHandler> logger)
        {
            _bus = bus;
            _loanTab = loanTab;
            _logger = logger;
            _lOSService = lOSService;
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> HandleAsync(string key, LMS.Kafka.Messages.Loan.LoanInsertSuccess value)
        {
            LMS.Common.Constants.ResponseActionResult response = new ResponseActionResult();
            try
            {
                var loanInfos = await _loanTab.WhereClause(x => x.LoanID == value.LoanID).QueryAsync();
                if (loanInfos == null || !loanInfos.Any())
                {
                    _logger.LogError($"GenPaymentScheduleByLoanIDHandler|value={_common.ConvertObjectToJSonV2(value)}|Not_found_loan");
                    return response;
                }
                var loanInfo = loanInfos.First();
                LMS.Entites.Dtos.LOSServices.ResultLoanWaitingDisbursementDetailLOS loanDetailLos = await _lOSService.GetLoanCreditDetail((long)loanInfo.LoanCreditIDOfPartner);

                // mua bảo hiểm
                if (loanDetailLos.BuyInsurenceCustomer)
                {
                    //_ = _insuranceService.CreateInsuranceOfCustomer(loanInfo.LoanID, loanInfo.CustomerID.Value);
                }
                if (loanDetailLos.BuyInsuranceProperty)
                {
                    //_ = _insuranceService.CreateInsuranceMaterial(loanInfo.LoanID, loanInfo.CustomerID.Value);
                }
                if (loanDetailLos.BuyInsuranceLender)
                {
                    //_ = _insuranceService.CreateInsuranceOfLender(loanInfo.LoanID, loanInfo.LenderID);
                }

                //_ = await _bus.Send(request);

            }
            catch (Exception ex)
            {
                _logger.LogError($"CreateInsuranceByLoanIDHandler|value={_common.ConvertObjectToJSonV2(value)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return response;
        }
    }
}
