﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.Constants
{
    public class InsuranceConstants
    {
        public const long Lender_InterestNotVat = 100000;
        public const decimal InterestFeesCustomer = 0.05M;
        public const decimal InterestNotVatLender = 0.03M;
        public const decimal InterestVatLender = 0.1M;
        public const decimal InterestMaterial = 0.03M;

        public const int MaxDateReport = 31;
    }
}
