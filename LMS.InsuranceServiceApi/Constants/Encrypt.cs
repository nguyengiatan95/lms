﻿using Microsoft.AspNetCore.Hosting;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace LMS.InsuranceServiceApi.Constants
{
    public static class Encrypt
    {
        public static string SignatureSHA256(string message, string privateKeyPath)
        {
            //string privateKeyPath = Path.Combine(,"Keypress", "private.pem");
            // string privateKeyPath = "private.pem";
            var privateRsa = RsaProviderFromPrivateKeyInPemFileX509(privateKeyPath);
            var signedData = privateRsa.SignData(Encoding.UTF8.GetBytes(message), CryptoConfig.MapNameToOID("SHA256"));
            return Convert.ToBase64String(signedData);
        }

        public static RSACryptoServiceProvider RsaProviderFromPrivateKeyInPemFileX509(string privateKeyPath)
        {
                using (TextReader privateKeyTextReader = new StringReader(File.ReadAllText(privateKeyPath)))
                {
                    PemReader pr = new PemReader(privateKeyTextReader);
                    RsaPrivateCrtKeyParameters keyPair = (RsaPrivateCrtKeyParameters)pr.ReadObject();
                    RSAParameters rsaParams = DotNetUtilities.ToRSAParameters(keyPair);

                    RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
                    csp.ImportParameters(rsaParams);
                    return csp;
                }
        }
    }
}
