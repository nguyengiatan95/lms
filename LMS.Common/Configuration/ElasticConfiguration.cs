﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Common.Configuration
{
    public class ElasticConfiguration
    {
        public string Urls { get; set; }
        public string IndexFormat { get; set; }
        public int NumberOfShards { get; set; }
        public int NumberOfReplicas { get; set; }
    }
}
