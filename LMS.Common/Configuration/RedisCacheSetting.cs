﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Common.Configuration
{
    public class RedisCacheSetting
    {
        public bool Enabled { get; set; }
        public string ConnectionString { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public bool AbortConnect { get; set; }
        public int ConnectTimeout { get; set; }
    }
}
