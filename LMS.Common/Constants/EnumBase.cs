﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace LMS.Common.Constants
{
    public enum ResponseAction
    {
        Success = 1,
        Error = 0
    }
    public enum StatusCommon
    {
        [Description("Default")]
        Default = -1,
        [Description("Khóa")]
        UnActive = 0,
        [Description("Hoạt động")]
        Active = 1,
        [Description("Tất cả")]
        SearchAll = -111
    }

    public enum PaymentSchedule_ActionRateType
    {
        [Description("1.Tất toán cuối kỳ theo ngày")]
        RateDay = 1, // Lãi ngày

        // RATE KHONG SỬ DỤNG
        RateTotalMoney = 2, // Tổng tiền
        RateMonth = 3, // Theo tháng
        RateWeekPercent = 4, // Theo tuần
        RateWeekVND = 5, // Theo tuần vnđ
        //---
        /// <summary>
        /// Tất toán cuối kỳ
        /// </summary>
        [Description("6.Tất toán cuối kỳ")]
        EndingBalance = 6,

        [Description("7.Dư nợ giảm dần")]
        RateAmortization = 7, // Theo dư nợ giảm dần

        [Description("8.Dư nợ giảm dần")]
        RateAmortizationMonth = 8, // Dư nợ giảm dần hàng tháng

        RateAmortizationNew = 9,

        /// <summary>
        /// Dư nợ giảm dần
        /// </summary>
        [Description("10.Dư nợ giảm dần")]
        ReducingBalance = 10,

        [Description("12.Dư nợ giảm dần TopUp")]
        RateAmortization30DaysTopUp = 12,

        [Description("13.Dư nợ giảm dần")]
        RateAmortizationMonthly = 13,

        [Description("14.Tất toán cuối kỳ")]
        RateDayMonthly = 14
    }

    public enum PaymentSchedule_Status
    {
        [Description("Không dùng")]
        NoUse = 0,

        [Description("Dùng")]
        Use = 1,

        [Description("Xóa")]
        Delete = -1,
        [Description("Đóng hợp đồng")]
        Completed = 2,

    }
    public enum Insurance_TypeInsurance
    {
        [Description("Lender")]
        Lender = 1,
        [Description("Bảo hiểm sức khỏe")]
        Customer = 2,
        [Description("Bảo hiểm vật chất")]
        Material = 3

    }
    public enum ResponseCode_Insurance
    {
        [Description("Hợp đồng bảo hiểm đã tồn tại")]
        Exits = 210,
        [Description("Mua bảo hiểm thành công")]
        Success = 200
    }

    public enum InvoiceType
    {
        [Description("Phiếu treo")]
        Waiting = 0,
        [Description("Phiếu thu")]
        Receipt = 1,
        [Description("Phiếu chi")]
        PaySlip = 2
    }
    // dữ liệu bên AG
    public enum SourceBankDisbursementOfApp
    {
        [Description("Nam Á")]
        NamA = 1,

        [Description("NCB")]
        NCB = 2,

        [Description("OtherBank")]
        OtherBank = 3,

        [Description("Nguồn tiền quỹ của Lender trên hệ thống")]
        EscrowSource = 4,
    }
    public enum Transaction_TypeMoney
    {
        [Description("Gốc")]
        Original = 1,
        [Description("Lãi")]
        Interest = 2,
        [Description("Dịch vụ")]
        Service = 3,
        [Description("Tư vấn")]
        Consultant = 4,
        [Description("Phạt trả chậm")]
        FineLate = 5,
        [Description("Phạt lãi trả chậm")]
        FineInterestLate = 6,
        [Description("Phạt trả trước gốc")]
        FineOriginal = 7,
        [Description("Phí bảo hiểm")]
        InsuranceFee = 8,
        [Description("Nợ")]
        Debt = 9,
    }

    public enum Transaction_Action
    {
        [Description("Cho vay")]
        ChoVay = 1,
        [Description("Sửa HĐ")]
        SuaHd = 2,
        [Description("Đóng lãi")]
        DongLai = 3,
        [Description("Hủy đóng lãi")]
        HuyDongLai = 4,
        [Description("Trả gốc")]
        TraGoc = 5,
        [Description("Vay thêm gốc")]
        VayThemGoc = 6,
        [Description("Hủy trả gốc")]
        HuyTraGoc = 7,
        [Description("Hủy vay thêm gốc")]
        HuyVayThemGoc = 8,
        [Description("Đóng HĐ")]
        DongHd = 9,
        [Description("Nợ lãi")]
        NoLai = 10,
        [Description("Trả nợ")]
        TraNo = 11,
        [Description("Gia hạn")]
        GiaHan = 12,
        [Description("Hủy đóng HĐ")]
        HuyDongHd = 13,
        [Description("Thanh lý")]
        ThanhLy = 14,
        [Description("Hủy cho vay")]
        HuyChoVay = 15,
        [Description("Hủy Nợ Lãi")]
        HuyNoLai = 16,
        [Description("Hủy Trả Nợ")]
        HuyTraNo = 17,
        [Description("Hủy Thanh Lý")]
        HuyThanhLy = 18,
        [Description("Chuyển Nợ Xấu")]
        ChuyenNoXau = 19,
        [Description("Bỏ Nợ Xấu")]
        BoNoXau = 20,
        [Description("Tiền Tư Vấn Thu Trước")]
        TienTuVanThuTruoc = 21,
        [Description("Phí phạt trả trước gốc")]
        PhiPhatTraTruocGoc = 22,
        [Description("Hủy Phí phạt trả trước gốc")]
        HuyPhiPhatTraTruocGoc = 23,
        [Description("Phí Bảo Hiểm")]
        PhiBaoHiem = 24,
        [Description("Ghi nợ phí phạt muộn")]
        GhiNoPhiPhatMuon = 25,
        [Description("Trả nợ phí phạt muộn")]
        TraNoPhiPhatMuon = 26,
        [Description("Hủy nợ phí phạt muộn")]
        DeleteTransactionMoneyFeeLate = 27,
        [Description("Thanh Lý bảo hiểm")]
        ThanhLyBaoHiem = 28,

        [Description("Hủy trả nợ phí phạt muộn")]
        DeleteTransactionPayMoneyFeeLate = 29,

        [Description("Miễn giảm lãi phí")]
        Exemption = 30,
        [Description("Trả tiền lãi")]
        PayPartial_Interest = 31,
        [Description("Trả phí tư vấn")]
        PayPartial_Consultant = 32,
        [Description("Trả phí dịch vụ")]
        PayPartial_Service = 33,
        [Description("Hủy Trả Tiền Lãi")]
        DeletePayPartial_Interest = 34,
        [Description("Hủy phí tư vấn")]
        DeletePayPartial_Consultant = 35,
        [Description("Thu bảo hiểm")]
        ReceiptInsurance = 36
    }

    public enum Loan_Status
    {
        [Description("Đang vay")]
        Lending = 1,

        [Description("Kết thúc - tất toán")]
        Close = 100,

        [Description("Kết thúc - Thanh Lý")]
        CloseLiquidation = 101,

        [Description("Kết thúc - Thanh Lý bảo hiểm")]
        CloseLiquidationLoanInsurance = 102,

        [Description("Kết thúc - Bồi thường bảo hiểm")]
        CloseIndemnifyLoanInsurance = 103, // lenderid k lưu

        [Description("Kết thúc - Đã thu đủ tiền sau khi bồi thường BH")]
        ClosePaidInsurance = 104,

        [Description("Kết thúc - Xin miễn giảm")]
        CloseExemption = 105,

        [Description("Kết thúc - Xin miễn giảm bồi thường bảo hiểm")]
        ClosePaidInsuranceExemption = 106,

        [Description("Xóa")]
        Delete = 0,

        [Description("OtherBank")]
        OtherBank = 3,

        [Description("Nguồn tiền quỹ của Lender trên hệ thống")]
        EscrowSource = 4,

    }

    public enum Loan_StatusSendInsurance
    {
        [Description("Chờ gửi bảo hiểm")]
        NoSend = 0,

        [Description("Đã gửi sang Bảo Hiểm")]
        Send = 1,

        [Description("Đã rút hồ sơ về")]
        Back = -1,

        [Description("Bảo hiểm đã đền bù")]
        ProcessingComplete = 2,
    }

    public enum Insurance_Status
    {
        [Description("Chờ mua bảo hiểm")]
        WaitingBuyInsurance = 1,
        [Description("Mua bảo hiểm thành công")]
        SuccessBuyInsurance = 2,
        [Description("Hợp đồng đã tồn tại")]
        InsuranceExist = 3,
    }
    public enum Insurance_PartnerCode
    {
        [Description("GCollect")]
        GCollect = 0,

        [Description("VBI")]
        VBI = 1,

        [Description("Bảo Minh")]
        BaoMinh = 2
    }

    public enum Insurance_StatusCallAI
    {
        [Description("Success")]
        Success = 1
    }

    public enum TypeCallInsuranceAI
    {
        [Description("Bảo Minh")]
        BaoMinh = 1
    }

    public enum TypeCustomer
    {
        [Description("Cá nhân")]
        Personal = 1,

        [Description("Doanh Nghiệp")]
        Enterprise = 2
    }
    public enum LogInsurance_TypeRequest
    {
        [Description("Tạo mua bảo hiểm")]
        Create = 1,
        [Description("Lấy lại link bảo hiểm")]
        GetDoc = 2
    }
    public enum Invoice_Status
    {
        [Description("Treo chờ xác minh")]
        Pending = 1,
        [Description("Chờ kế toán xác nhận")]
        WaitingConfirm = 2,
        [Description("Kế toán xác nhận")]
        Confirmed = 3,
        [Description("Hủy")]
        Deleted = -11
    }

    public enum GroupInvoiceType
    {
        [Description("Phiếu treo")]
        Waiting = 0,
        [Description("Phiếu thu")]
        Receipt = 1,
        [Description("Phiếu chi")]
        PaySlip = 2,
        [Description("Thu tiền khách hàng")]
        ReceiptCustomer = 1001,
        [Description("Thu tiền treo")]
        ReceiptCustomerConsider = 1002,
        [Description("Thu hộ của cửa hàng")]
        ReceiptOnBehalfShop = 1003,
        [Description("Thu nội bộ")]
        ReceiptInternal = 1004,
        [Description("Lender thêm vốn")]
        ReceiptLenderCapital = 1005,
        [Description("Thu lãi ngân hàng")]
        ReceiptInterestBank = 1006,
        [Description("Thu tiền bảo hiểm lender")]
        ReceiptLenderInsurance = 1007,
        [Description("Thu tiền đối tác")]
        ReceiptPartner = 1008,
        [Description("Thu tiền lender hoàn ứng")]
        ReceiptLenderTopUp = 1009,
        [Description("Thu tiền khác")]
        ReceiptOther = 1010,


        [Description("Giải ngân cho khách")]
        Disbursement = 2001,
        [Description("Chi nội bộ")]
        PaySlipTransferInternal = 2002,
        [Description("Trả tiền thừa cho khách")]
        PaySlipGiveBackExcessCustomer = 2003,

        [Description("Lender rút vốn")]
        PaySlipLenderWithdraw = 2004,

        [Description("Tạm ứng cho lender")]
        PaySlipAdvancePaymentForLender = 2005,
        [Description("Trả phí cho ngân hàng")]
        PaySlipFeeForBank = 2006,
        [Description("Chi tiền khác")]
        PaySlipOther = 2007,

        [Description("Vay thêm gốc")]
        PaySlipDisbursement = 2008,

        [Description("Phí chuyển tiền")]
        PaySlipFeeTransferMoney = 2009,

        [Description("Phí ngân hàng")]
        PaySlipBankFee = 2010,

        [Description("Trả công nợ")]
        PaySlipTheDebt = 2011,

        [Description("chuyển tiền cho cửa hàng lý do khác")]
        TransferMoneyStoreReasonOther = 2012,

        [Description("Đặt cọc cho Lender")]
        LenderDepositPayment = 2013,
    }

    public enum Loan_SourceMoneyDisbursement
    {
        [Description("Nguồn tiền quỹ của Lender trên hệ thống")]
        EscrowSource = 1,
        [Description("Nguồn tiền Bank Nam Á của Lender")]
        BankNamAOfLender = 2,
        [Description("Nguồn tiền Bank Quốc Dân của Lender")]
        BankNCBOfLender = 3,
        [Description("Nguồn tiền ngân hàng khác của Lender")]
        OtherBank = 4,
        [Description("Nguồn tiền VIB của Lender")]
        VIBOfLender = 5
    }
    public enum Loan_SourcecBuyInsurance
    {
        [Description("Không mua BH")]
        NO = 0,
        [Description("Mua BH VBI")]
        VBI = 1,
        [Description("Mua BH BaoMinh")]
        BaoMinh = 2
    }
    public enum Invoice_JobStatus
    {
        [Description("Chưa xử lý")]
        Unprocessed = 0,
        [Description("Đã xử lý")]
        Processed = 1
    }
    public enum Invoice_Payee
    {
        [Description("Bên gửi chịu phí")]
        Sender = 0,
        [Description("Bên nhận chịu phí")]
        Receiver = 1
    }
    public enum Loan_UnitRate
    {
        [Description("Tính theo giá trị")]
        Default = 0,
        [Description("Đơn vị tính là %")]
        Percent = 1,
    }

    public enum SmsAnalytics_Status
    {
        [Description("Chưa phân tích")]
        Pending = 0,
        [Description("Đã phân tích")]
        DaPhanTich = 1,
        [Description("Đã nạp")]
        DaNap = 2,
        [Description("Đã nạp phiếu treo")]
        DaNapPhieuTreo = 3,
        [Description("Đã nạp phiếu thu hộ")]
        DaNapThuHo = 4,
        [Description("Hệ thống nạp tự động")]
        DaNapTuDong = 5,
        [Description("Hệ thống nạp phiếu treo tự động")]
        DaNapPhieuTreoTuDong = 6,
        [Description("Hệ thống nạp phiếu thu hộ tự động")]
        DaNapThuHoTuDong = 7,
        [Description("Hệ thống hủy tin nhắn")]
        DaHuy = -1
    }
    public enum BankCard_Status
    {
        [Description("Khóa")]
        UnActice = 0,
        [Description("Hoạt động")]
        Active = 1

    }
    public enum Invoice_CashBankCardStatus
    {
        [Description("Chưa xử lý")]
        NoProcess = 0,
        [Description("Đã xử lý")]
        Processed = 1

    }
    public enum LogBankCard_JobStatus
    {
        [Description("Chưa xử lý")]
        NoProcess = 0,
        [Description("Đã xử lý")]
        Processed = 1
    }

    public enum Loan_StatusFilterRequest
    {
        [Description("Đang vay")]
        Borrowing = 1,

        [Description("Quá hạn 30 ngày")]
        OverdueLoanExpected30 = 30,

        [Description("Quá hạn 90")]
        OverdueLoanExpected90 = 90,

        [Description("Tất Toán")]
        Closed = 101,

        [Description("Thanh Lý")]
        Liquidation = 102,

        [Description("Thanh Lý bảo hiểm")]
        InsuranceLiquidation = 103,

        [Description("Đã đền bù bảo hiểm")]
        InsurancePayment = 104,
        [Description("Xóa")]
        Delete = -1,
    }

    public enum User_Status
    {
        [Description("Hoạt động")]
        Active = 1,
        [Description("Đã khóa")]
        Locked = 0,
        [Description("Đã xóa")]
        Deleted = -11,
        [Description("Chờ xử lý")]
        Waiting = 3,

    }

    public enum User_UserTypeID
    {
        [Description("App service")]
        APP = 0,
        [Description("Nhân viên công ty")]
        Staff = 1,
        [Description("Nhà đầu tư")]
        Lender = 2,
        [Description("Khách hàng")]
        Customer = 3,
        [Description("CTV")]
        Aff = 4,
    }


    public enum LogCompare_MainType
    {
        [Description("Giải ngân")]
        Disbursement = 1,
        [Description("Lender")]
        Lender = 2,
        [Description("Loan")]
        Loan = 3,
        [Description("Hạch Toán")]
        Payment = 4,
        [Description("Phiếu Thu / Chi")]
        CollectExpense = 5,
        [Description("Khách hàng")]
        Customer = 6,
        [Description("Comment")]
        Comment = 7,
        [Description("Dự thu")]
        PrepareLoan = 8,
        [Description("Hold tiền KH")]
        HoldMoney = 9,
        [Description("SMS")]
        SMS = 10,
        [Description("Trả bớt gốc")]
        LoanExtra = 11
    }
    public enum LogCompare_DetailType
    {
        [Description("Hợp đồng được giải ngân theo ngày")]
        Disbursement_TotalLoan = 101,
        [Description("Khách hàng được giải ngân theo ngày")]
        Disbursement_TotalCustomer = 102,
        [Description("Dư nợ được giải ngân theo ngày")]
        Disbursement_TotalMoney = 103,
        [Description("Tiền chi của phiếu giải ngân")]
        Disbursement_TotalMoneyExpense = 104,
        [Description("Mua bảo hiểm khoản vay")]
        Disbursement_TotalMoneyInsurance = 105,
        [Description("Tiền Lender tạm giữ của Tima")]
        Disbursement_TotalMoneyLenderHold = 106,


        [Description("Lender đăng ký chờ xác thực")]
        Lender_WaitingVerify = 201,
        [Description("Lender đã xác thực")]
        Lender_Verified = 202,
        [Description("Lender đã xác thực đang hoạt động")]
        Lender_VerifiedActive = 203,
        [Description("Số Lender đã xác thực - tạm ngưng")]
        Lender_VerifiedLock = 204,
        [Description("Số Lender đã xác thực - Dừng")]
        Lender_WaitingStop = 205,

        [Description("Số hợp đồng đang vay")]
        Loan_TotalLending = 301,
        [Description("Số hợp đồng đang chờ gửi bảo hiểm")]
        Loan_TotalWaitingInsurance = 302,
        [Description("Số hợp đồng đã gửi bảo hiểm")]
        Loan_TotalSentInsurance = 303,
        [Description("Số hợp đồng đã được bồi thường bảo hiểm")]
        Loan_TotalInsurancePaid = 304,
        [Description("Số hợp đồng quá hạn")]
        Loan_TotalOverTime = 305,
        [Description("Số hợp đồng nợ xấu")]
        Loan_TotalBadDebt = 306,
        [Description("Số hợp đồng kết thúc")]
        Loan_TotalClosed = 307,
        [Description("Trả bớt gốc")]
        Loan_OrginalExtra = 308,

        [Description("Số tiền gốc thu về")]
        Payment_OriginalMoney = 401,

        [Description("Số tiền lãi thu về")]
        Payment_InterestMoney = 402,

        [Description("Số tiền phí tư vấn thu về")]
        Payment_ConsultantMoney = 403,

        [Description("Số tiền phí dịch vụ thu về")]
        Payment_ServiceMoney = 404,

        [Description("Số tiền phí phạt trả chậm thu về")]
        Payment_FineLate = 405,

        [Description("Số tiền phí phạt trả trước gốc thu về")]
        Payment_FirstPayOriginal = 406,

        [Description("Số lượng phiếu")]
        CollectExpense_TotalCount = 501,

        [Description("Số tiền")]
        CollectExpense_TotalMoney = 502,


        [Description("Số lượng chờ kế toán xác nhận")]
        Waiting = 503,
        [Description("Số lượng thu tiền khách hàng")]
        ReceiptCustomer = 504,
        [Description("Số lượng treo")]
        ReceiptCustomerConsider = 505,
        [Description("Số lượng Thu hộ của cửa hàng")]
        ReceiptOnBehalfShop = 506,
        [Description("Số lượng Thu nội bộ")]
        ReceiptInternal = 507,
        [Description("Số lượng Lender thêm vốn")]
        ReceiptLenderCapital = 508,
        [Description("Số lượng Thu lãi ngân hàng")]
        ReceiptInterestBank = 509,
        [Description("Số lượng Thu tiền bảo hiểm lender")]
        ReceiptLenderInsurance = 510,
        [Description("Số lượng Thu tiền đối tác")]
        ReceiptPartner = 511,
        [Description("Số lượng Thu tiền lender hoàn ứng")]
        ReceiptLenderTopUp = 512,
        [Description("Số lượng Thu tiền khác")]
        ReceiptOther = 513,
        [Description("Số lượng Giải ngân cho khách")]
        Disbursement = 514,
        [Description("Số lượng Chuyển nội bộ")]
        PaySlipTransferInternal = 515,
        [Description("Số lượng Trả tiền thừa cho khách")]
        PaySlipGiveBackExcessCustomer = 516,
        [Description("Số lượng Lender rút vốn")]
        PaySlipLenderWithdraw = 517,
        [Description("Số lượng Tạm ứng cho lender")]
        PaySlipAdvancePaymentForLender = 518,
        [Description("Số lượng Trả phí cho ngân hàng")]
        PaySlipFeeForBank = 519,
        [Description("Số lượng Chi tiền khác")]
        PaySlipOther = 520,
        [Description("Số lượng Vay thêm gốc")]
        PaySlipDisbursement = 521,
        [Description("Số lượng Phí chuyển tiền")]
        PaySlipFeeTransferMoney = 522,
        [Description("Số lượng Phí ngân hàng")]
        PaySlipBankFee = 523,
        [Description("Số lượng Trả công nợ")]
        PaySlipTheDebt = 524,
        [Description("Số lượng chuyển tiền cho cửa hàng lý do khác")]
        TransferMoneyStoreReasonOther = 525,
        [Description("Số lượng Đặt cọc cho Lender")]
        DepositCount = 549,
        [Description("Số tiền Đặt cọc cho Lender")]
        DepositMoney = 550,


        [Description("Tiền - Phiếu chờ kế toán xác nhận")]
        MoneyWaiting = 526,
        [Description("Tiền - Thu tiền khách hàng")]
        MoneyReceiptCustomer = 527,
        [Description("Tiền - Thu tiền treo")]
        MoneyReceiptCustomerConsider = 528,
        [Description("Tiền - Thu hộ của cửa hàng")]
        MoneyReceiptOnBehalfShop = 529,
        [Description("Tiền - Thu nội bộ")]
        MoneyReceiptInternal = 530,
        [Description("Tiền - Lender thêm vốn")]
        MoneyReceiptLenderCapital = 531,
        [Description("Tiền - Thu lãi ngân hàng")]
        MoneyReceiptInterestBank = 532,
        [Description("Tiền - Thu tiền bảo hiểm lender")]
        MoneyReceiptLenderInsurance = 533,
        [Description("Tiền - Thu tiền đối tác")]
        MoneyReceiptPartner = 534,
        [Description("Tiền - Thu tiền lender hoàn ứng")]
        MoneyReceiptLenderTopUp = 535,
        [Description("Tiền - Thu tiền khác")]
        MoneyReceiptOther = 536,
        [Description("Tiền - Giải ngân cho khách")]
        MoneyDisbursement = 537,
        [Description("Tiền - Chuyển nội bộ")]
        MoneyPaySlipTransferInternal = 538,
        [Description("Tiền - Trả tiền thừa cho khách")]
        MoneyPaySlipGiveBackExcessCustomer = 539,
        [Description("Tiền - Lender rút vốn")]
        MoneyPaySlipLenderWithdraw = 540,
        [Description("Tiền - Tạm ứng cho lender")]
        MoneyPaySlipAdvancePaymentForLender = 541,
        [Description("Tiền - Trả phí cho ngân hàng")]
        MoneyPaySlipFeeForBank = 542,
        [Description("Tiền - Chi tiền khác")]
        MoneyPaySlipOther = 543,
        [Description("Tiền - Vay thêm gốc")]
        MoneyPaySlipDisbursement = 544,
        [Description("Tiền - Phí chuyển tiền")]
        MoneyPaySlipFeeTransferMoney = 545,
        [Description("Tiền - Phí ngân hàng")]
        MoneyPaySlipBankFee = 546,
        [Description("Tiền - Trả công nợ")]
        MoneyPaySlipTheDebt = 547,
        [Description("Tiền - chuyển tiền cho cửa hàng lý do khác")]
        MoneyTransferMoneyStoreReasonOther = 548,
        [Description("KH - Tiền đang có")]
        Customer_TotalMoney = 600,
        [Description("KH - Số KH có tiền đang có")]
        Customer_CountHaveMoney = 601,
        [Description("KH - Số KH lệch sđt")]
        Customer_ErrorPhone = 602,
        [Description("KH - Danh sách lệch sđt")]
        Customer_ListErrorPhone = 603,
        [Description("KH - Danh sách miss")]
        Customer_ListMiss = 604,



        [Description("Payment - Danh sách miss")]
        Payment_ListMiss = 1500,
        [Description("Payment - LoanID sai isvisible")]
        Payment_LoanErrorIsVisible = 1501,
        [Description("Payment - LoanID sai complete")]
        Payment_LoanErrorIsComplete = 1502,

        [Description("Payment - LoanID tổng kì ")]
        Payment_LoanErrorTotalCount = 1503,

        [Description("Payment - LoanID tổng tiền phải thu ")]
        Payment_LoanErrorTotalNeed = 1504,

        [Description("Payment - LoanID tổng tiền đã trả  ")]
        Payment_LoanErrorTotalPay = 1505,

        [Description("Loan - NextDate")]
        Loan_NextDate = 807,
        [Description("Loan - TotalMoneyCurrent")]
        Loan_TotalMoneyCurrent = 808,
        [Description("Loan - LastDateOfPay")]
        Loan_LastDateOfPay = 809,
        [Description("Loan - FineInterest")]
        Loan_FineInterest = 810,

        [Description("Comment trong ngày")]
        Comment_Today = 1001,

        [Description("Tất cả comment")]
        Comment_ALL = 1002,

        [Description("dự thu trong ngày")]
        Prepare_Today = 1101,

        [Description("Tất cả dự thu")]
        Prepare_ALL = 1102,
        [Description("Tiền dự thu trong ngày")]
        Prepare_Money_Today = 1103,

        [Description("Hold tiền KH trong ngày")]
        HoldMoney_Today = 1201,
        [Description("Tất cả Hold tiền KH")]
        HoldMoney_ALL = 1202,
        [Description("Tiền Hold tiền KH trong ngày")]
        HoldMoney_Money_Today = 1103,
        [Description("Tất cả SMS")]
        SMS_ALL = 1302,
        [Description("SMS trong ngày")]
        SMS_Today = 1303
    }

    public enum Lender_Status
    {
        [Description("Dừng")]
        Stop = 0, // dừng
        [Description("Hoạt động")]
        Active = 1, // hoạt động
        [Description("Tạm Ngưng")]
        Delete = -11
    }

    public enum Lender_IsVerified
    {
        [Description("Đã xác minh")]
        Active = 1, // hoạt động
        [Description("Chưa xác minh")]
        Pause = 0
    }
    public enum Lender_UnitRate
    {
        [Description("Tính theo giá trị")]
        Default = 0,
        [Description("Đơn vị tính là %")]
        Percent = 1,
    }

    public enum Lender_Gender
    {
        [Description("Nam")]
        Men = 0,
        [Description("Nữ")]
        Women = 1,
    }



    public enum PaymentSchedule_IsComplete
    {
        [Description("Chờ thanh toán")]
        Waiting = 0,
        [Description("Đã thanh toán")]
        Paid = 1,
        [Description("Bảo hiểm")]
        ProcessInsurance = 2,
        [Description("Đã thu đủ sau bảo hiểm")]
        PaidInsurance = 3
    }

    public enum PaymentSchedule_IsVisible
    {
        Show = 0,
        Hide = 1
    }
    public enum StateLoanCreditNew // trạng thái khoản vay
    {
        [Description("Hủy")]
        Refuse = 0, // từ chối
        WaittingSupport = 1, // Chờ Support hỗ trợ
        SaleAdminBack = 2, // Chờ Support hỗ trợ
        WaitAutoCancel = 3, //   Support chuyển chờ hủy tự động
        BackSupport = -1, // Trả lại cho Support
        WaittingCoordinatorDirectionalToCounselor = 5, // Chờ support điều phối cho TVV
        WaittingSupportCounselors = 9, // Chờ Tư Vấn Viên xác nhận
        WaittingCounselorsToAgency = 10, // Chờ Tư Vấn Viên đi check đẩy cho Điều Phối Viên (KSNB)
        BackCounselors = -10, // KSNB trả lại cho tư vấn viên
        WaittingCoordinatorDirectionalToAgency = 15, // Chờ TĐ hồ sơ chuyển cho TĐ TĐ

        [Description("Chờ Lender giải ngân")]
        WaittingAgency = 20, // Chờ Đại lý đẩy cho tư vấn viên

        [Description("Lender trả lại")]
        BackCoordinator = -15, // Đại lý trả lại kiểm soát nội bộ

        CounselorsBackCoordinator = -16, // Tư Vấn Viên trả lại cho kiểm soát nội bộ để đẩy cho đại lý khác
        CounselorsBackCoordinatorLevelLow = -5, // Tư Vấn Viên trả lại cho KSNB không lấy được hồ sơ

        [Description("Chờ ký hợp đồng")]
        WaittingCounselorsToAccountant = 25, // chờ tư vấn viên đi ký hợp đồng đẩy cho kế toán

        [Description("Chờ Giải Ngân")]
        AccountantDisbursement = 30, // kế toán chờ giải ngân
        [Description("Tự động giải ngân")]
        AutoDisbursement = 31, // kế toán chờ giải ngân

        [Description("Ký lại hợp đồng")]
        AccountantCancel = -25, // kế toán trả lại

        [Description("Đang vay")]
        AccountantDisbursed = 35, // kế toán đã giải ngân

        Finish = 100,// Kết thúc
        CoordinatorReturnCounselors = -11,// KSNB trả lại TVV khi đang ở trạng thái -15 ( khi DL trả lại KSNB)
        CoordinatorReturnCounselorsBack = -12,// KSNB trả lại luôn TVV  khi đang ở trạng thái -16 (khi TVV trả lại KSNB -16)
        CounselorsBackSuperVisionAreaLevelLow = -50, // Tư Vấn Viên trả lại cho KSNB không lấy được hồ sơ
        CoordinatorBackTeleSale = -1, // KSNB trả lại TeleSale
        SaleAdminBackTeleSale = 2, // SaleAdmin trả lại TeleSale

        [Description("Sales Admin Duyệt")]
        WaittingSalesAdminConfirm = 6,

        [Description("Chờ thực địa đi check")]
        CoordinatorTransferFieldSurvey = 17,// Thẩm định hồ sơ chuyển thẩm định thực địa

        [Description("Chờ TĐHS Duyệt")]
        FieldSurveyTransferCoordinator = 18, // Thẩm định thực địa chuyển cho thẩm định hồ sơ

        [Description("Chờ quản lý khu vực duyệt")]
        WaittingManagerConfirm = 22,         // Chờ quản lý khu vực duyệt

        WaittingBOD = 21, // Chờ Bod Hub

        BodHubReturn = -20, // TGĐ HUB trả lại hồ sơ cho cửa hàng

        [Description("Chờ Kiểm Duyệt HĐ Giải Ngân")]
        WaittingApprovalForSignature = 26,

        [Description("Chờ quản lý của kinh doanh duyệt")]
        WaittingShopHead = 27,

        [Description("Chờ Check CIC")]
        WaittingCheckCic = 8,

        [Description("Chờ đẩy cho Lender")]
        WaittingPushToLender = 19,

        [Description("Check Rule trên Third Party chuyển DRS")]
        WattingCheckRule = -35

    }
    public enum LOS_Enum_DisbursementBy
    {
        Accountant = 1,
        Lender = 2,
        Auto = 3
    }
    public enum ShopId
    {
        [Description("Credit")]
        Tima = 3703
    }
    public enum Product_Type
    {
        [Description("Sản phẩm nhỏ")]
        Small = 1,
        [Description("Sản phẩm lớn")]
        Big = 2,
        [Description("Sản phẩm siêu nhỏ")]
        VerySmall = 3,
    }
    public enum SettingFineForProduct : int
    {
        [Description("Số ngày phạt trả chậm của sp nhỏ")]
        MinDayFineLateSmall = 4,
        [Description("Số ngày phạt trả chậm của sp lớn")]
        MinDayFineLateBig = 1,
        [Description("Số tiền phạt trả chậm của sp nhỏ")]
        MoneyFineLateSmall = 400000,
        [Description("Số ngày phạt trả chậm của sp nhỏ")]
        MoneyFineLateBig = 100000,
        [Description("Số tiền lớn nhất có thể bị phạt trả chậm của sp lớn")]
        MaxMoneyFineLateBig = 1000000,
        [Description("Phí phạt trả trước của sp nhỏ (%)")]
        PercentPrePaymentPenaltySmall = 8,
        [Description("Phí phạt trả trước của sp lớn (%)")]
        PercentPrePaymentPenaltyBig = 5
    }

    public enum HoldCustomerMoney_Status
    {
        [Description("Giữ tiền")]
        Hold = 1,
        [Description("Không giữ")]
        UnHold = 0,
        [Description("Huỷ")]
        Delete = -1
    }
    public enum Lender_IsGcash
    {
        [Description("Gcash")]
        Gcash = 1
    }
    public enum Lender_IsHub
    {
        [Description("Hub")]
        Hub = 1
    }

    public enum DisbursementToLOS_TypeID
    {
        [Description("Tất cả")]
        Default = 1,
        [Description("Chờ đẩy cho lender")]
        WaittingPushToLender = 2,
        [Description("Chờ kế toán giải ngân")]
        AccountantDisbursement = 3,
        [Description("Chờ lender giải ngân")]
        WaittingAgency = 4,
        [Description("Chờ GN tự động")]
        AutoDisbursement = 5,
        [Description("Tất cả")]// tất cả màn kế toán
        AllAccountant = 10
    }
    public enum DisbursementLOS_Status
    {
        [Description("Chờ đẩy cho lender")]
        WaittingPushToLender = 90,
        [Description("Chờ kế toán giải ngân")]
        AccountantDisbursement = 102,
        [Description("Chờ lender giải ngân")]
        WaittingAgency = 103,
        [Description("Giải ngân tự động")]
        AutoDisbursement = 104,
        [Description("Default")]
        Default = -1,
    }

    public enum StateTypeReceivingMoney
    {
        [Description("Tiền mặt")]
        MoneyCash = 1, // tiền mặt
        [Description("Momo")]
        Momo = 2,
        [Description("Chuyển khoản Số tài khoản")]
        NumberAccount = 3, // chuyển khoản Số tài khoản
        [Description("Số thẻ")]
        NumberCard = 4, // Số thẻ
        [Description("Viettel")]
        Viettel = 5, // Viettel
    }
    public enum LoanCreditDisbursementAuto_DisbursementType
    {
        [Description("Kế toán giải ngân")]
        Accountant = 1,
        [Description("Lender giải ngân")]
        Lender = 2
    }
    public enum LoanCreditDisbursementAuto_Status
    {
        [Description("Chờ xử lý")]
        Waiting = 1,
        [Description("Đã xử lý")]
        Processed = 2,
        [Description("Chờ SMS OTP")]
        WaitingOTP = 3,
        [Description("OTP hết hiệu lực")]
        OTPTimeOut = 4,
        [Description("Giải ngân thành công")]
        Success = 5,
        [Description("Giải ngân thất bại")]
        Fail = 6,
        // bước call api internal để xử lý lại
        [Description("Chờ kết quả từ OTP")]
        WaitingResultSendOtp = 7,
        [Description("Chờ kết quả từ xác nhận OTP")]
        WaitingResultComfirmOtp = 8,
        [Description("Chuyển sang kế toán giải ngân")]
        FailByAccountant = 9
    }
    public enum LOS_Enum_LockLoanType
    {
        Open = 1,
        Lock = 2
    }
    public enum LOS_Enum_DisbursementType
    {
        CreateLoan = 1,
        CloseLoan = 2
    }
    public enum ReportVatHub
    {
        [Description("HU00101")]
        HU00101 = 5757,
        [Description("HN-HUB001")]
        HUB001 = 5779,
        [Description("HN-HUB002")]
        HUB002 = 5780,
        [Description("HN-HUB003")]
        HUB003 = 5781,
        [Description("HN-HUB004")]
        HUB004 = 5782
    }
    public enum ReportVatHubWithGCash
    {
        [Description("HN-HUB005")]
        HUB005 = 5790,
        [Description("HN-HUB001")]
        HUB001 = 5779,
        [Description("HN-HUB002")]
        HUB002 = 5780,
        [Description("HN-HUB003")]
        HUB003 = 5781,
        [Description("HN-HUB004")]
        HUB004 = 5782
    }

    public enum TypeVerifyInvoiceConsider
    {
        [Description("Khách hàng")]
        Customer = 0,
        [Description("Lender")]
        Lender = 1
    }
    public enum CustomerBank_PartnerCode
    {
        [Description("VA")]
        VA = 1,
        [Description("MSB")]
        MSB = 2,
        [Description("BIDV")]
        BIDV = 3
    }
    public enum CustomerBank_StatusVA
    {
        [Description("Error")]
        Error = 0,
        [Description("Success")]
        Success = 1
    }
    public enum LogVA_Status
    {
        [Description("Error")]
        Error = 0,
        [Description("Success")]
        Success = 1
    }
    public enum SettingKey_Status
    {
        InActice = 0,
        Active = 1,
        PushToAccountant = 2
    }
    public enum ReportVatLender
    {
        [Description("HN03901")]
        HN03901 = 5773,
        [Description("HN03902")]
        HN03902 = 5774,
        [Description("HN03903")]
        HN03903 = 5775,
        [Description("HN03904")]
        HN03904 = 5776,
        [Description("HN03905")]
        HN03905 = 5786,
        [Description("SG00501")]
        SG00501 = 5784,
        [Description("SG00502")]
        SG00502 = 5785
    }
    public enum ReturnLoanLOS
    {
        [Description("KT trả lại đơn")]
        AccountantReturn = 1,
        [Description("Lender trả lại đơn")]
        LenderReturn = 2,
        [Description("Lender Care lấy lại đơn")]
        LenderCareTakeBack = 3
    }
    public enum BankCard_TypePurpose
    {
        [Description("Dùng Giải Ngân")]
        Disbursement = 1,
        [Description("Thanh Toán Gốc Và Lãi")]
        PaymentPrincipalAndInterest = 2,
        [Description("Cả Hai Việc Trên")]
        BothOfThem = 3,
        [Description("Tự động giải ngân")]
        AutoDisbursement = 4
    }

    public enum Ticket_Type
    {
        [Description("Lender rút vốn")]
        LenderRutVon = 1,
        [Description("Lender tạm ứng")]
        LenderTamUng = 2,
    }

    public enum Ticket_Status
    {
        [Description("Đã xử lý")]
        Active = 1,
        [Description("Chờ xử lý")]
        Waiting = 0,
        [Description("Hủy")]
        Delete = -11,
    }

    public enum Department_ID
    {
        [Description("Phòng tài chính kế toán")]
        AccountTant = 1,
        [Description("Trung tâm thu hồi nợ")]
        THN = 2,
        [Description("Bộ phận Field MN")]
        THN_Field_MN = 55,
        [Description("Bộ phận Field MB")]
        THN_Field_MB = 56,
        [Description("Bộ phận Call")]
        THN_Call = 105,
        [Description("THN MN")]
        THN_MN = 106,
        [Description("THN MB")]
        THN_MB = 66,
    }
    public enum Lender_RegFromApp
    {
        [Description("NA")]
        NA = 1,
        [Description("DT")]
        DT = 0
    }

    public enum Menu_AppID
    {
        [Description("Admin")]
        Admin = 1,
        [Description("Kế toán")]
        Accountant = 2,
        [Description("Nguồn vốn")]
        Lender = 3,
        [Description("Thu hồi nợ")]
        THN = 4
    }
    public enum LogCallApi_ActionCallApi
    {
        [Description("Chuẩn bị tạo giao dịch lấy OTP")]
        PreCreateTransaction = 1,
        [Description("Tạo Giao Dịch, lấy OTP")]
        ExecuteCreateTransaction = 2,

        [Description("Chuẩn bị xác nhận OTP")]
        PreExecuteOtp = 3,
        [Description("Execute OTP, gửi OTP")]
        ConfirmeExecuteOtp = 4,

        [Description("Chuẩn bị hủy giao dịch bank")]
        PreCancelTransaction = 5,
        [Description("Cancel Transaction Bank, Hủy giao dịch bank")]
        ExecuteCancelTransaction = 6,
    }
    public enum LogCallApi_StatusCallApi
    {
        [Description("Bắt đầu gọi API")]
        CallApi = 0,

        [Description("Thành công")]
        Success = 1,

        [Description("Thất bại")]
        Error = -1,
        [Description("Gọi lại")]
        Retry = 2,
        [Description("Chờ kết quả")]
        WaitConfirm = 3,
    }

    public enum SmsDisbursement_Status
    {
        [Description("Chờ phân tích nội dung")]
        Waiting = 1,
        [Description("Không tìm thấy LoanCredit")]
        LoanNotFound = 2,
        [Description("Đã tạo đơn thành công")]
        Success = 3,
        [Description("Lỗi không tạo được đơn")]
        Fail = 4,
        [Description("Lỗi chờ xác nhận ngân hàng")]
        FailWaitingRecheck = 5,
        [Description("Chờ thử lại")]
        Retry = 6,
        [Description("Tin rác")]
        Spam = 7,
        [Description("Không còn hiệu lực")]
        TimeOut = 8,
        [Description("Đã phân tích nội dung")]
        Analyzed = 9,
        Exists = 10
    }
    public enum Debt_TypeID
    {
        [Description("Ghi nợ phí phạt trả chậm")]
        CustomerFineLate = 1,
        [Description("Thanh toán phí phạt trả chậm")]
        CustomerPayFineLate = 2,
        [Description("Ghi nợ tiền nợ")]
        CustomerDebt = 3,
        [Description("Trả nợ tiền nợ")]
        PayCustomerDebt = 4,

        [Description("Hủy ghi nợ phí phạt trả chậm")]
        CancelCustomerFineLate = 101,
        [Description("Hủy thanh toán phí phạt trả chậm")]
        CancelCustomerPayFineLate = 102,
        [Description("Hủy ghi nợ tiền nợ")]
        CancelCustomerDebt = 103,
        [Description("Hủy trả nợ tiền nợ")]
        CancelPayCustomerDebt = 104,

        [Description("Lender giữ tiền bảo hiểm")]
        LenderKeepInsurance = 200
    }
    public enum PreparationCollectionLoan_Status
    {
        [Description("Không")]
        No = 0,
        [Description("Có")]
        Yes = 1
    }
    public enum PreparationCollectionLoan_TypeID
    {
        [Description("Số tiền tự nhâp")]
        SelfEnteredAmount = 0,
        [Description("Số tiền theo kỳ")]
        AmountPeriod = 1,
        [Description("Số tiền tất toán")]
        FinalSettlementAmount = -1,
    }

    public enum PreparationCollectionLoan_PrepareType
    {
        [Description("Khả năng thu cao")]
        HighReceivingCapacity = 1,
        [Description("Có PTP")]
        HavePTP = 2,
        [Description("Thiếu tiền kỳ")]
        LackPayment = 3,
        [Description("Thiếu tiền tất toán")]
        LackCloseMoney = 4,
        [Description("Trình MGL")]
        MGL = 5,
        [Description("Trình CTN")]
        CTN = 6,
        [Description("Thu Giữ Xe")]
        VehicleSeizure = 7
    }

    public enum NumberForecastPeriods
    {
        [Description("Số tiền tự nhập")]
        SelfEnteredAmount = 0,
        [Description("1 kỳ")]
        OnePeriods = 1,
        [Description("2 kỳ")]
        TwoPeriods = 2,
        [Description("3 kỳ")]
        ThreePeriods = 3,
        [Description("Tất toán")]
        All = -1
    }
    public enum LOS_TypeFile
    {
        [Description("Tất cả")]
        All = 0,
        [Description("ảnh")]
        Img = 1,
        [Description("video")]
        Video = 2
    }

    public enum Customer_PayTypeID
    {
        [Description("M")]
        Medium = 0,
        [Description("L")]
        Low = 1,
        [Description("H")]
        Hight = 2
    }
    public enum Reason_Status
    {
        [Description("Hoạt động")]
        Active = 1,
    }
    public enum CommentDebtPrompted_IsDisplay
    {
        [Description("Hoạt động")]
        Active = 1,
    }


    public enum CommentDebtPrompted_ActionPerson
    {
        [Description("Người nhà")]
        Home = 1,

        [Description("Hàng xóm")]
        NeighborHood = 2
    }

    public enum CommentDebtPrompted_ActionAddress
    {
        [Description("Nhà")]
        Home = 1,
        [Description("Công ty")]
        Company = 2
    }


    public enum QueueCallAPI_Status
    {
        [Description("Đã chạy")]
        Done = 1,
        [Description("Chờ chạy")]
        Waiting = 0,
        [Description("Hủy")]
        Cancel = -1,
        [Description("Fail")]
        Fail = 2
    }
    public enum Reason_DepartmentID
    {
        [Description("Bộ phận Field")]
        Field = 55,
        [Description("Bộ phận Call")]
        Call = 105,
        //[Description("Bộ phận Legal")]
        //Legal = 105,
        //[Description("Bộ phận thu giữ xe")]
        //VehicleSeizure = 0
    }
    public enum CalculateMoneyInsuranceLOS
    {
        [Description("Tính phí bảo hiểm")]
        Yes = 0,
        [Description("Không phí bảo hiểm")]
        No = 1
    }

    public enum MapUserCall_TypeCall
    {
        Caresoft = 1,
        Cisco = 2
    }
    public enum QueueCallAPI_Description
    {
        [Description("Thay đổi số điện thoại")]
        ChangePhone = 1,
        [Description("Gửi mail cho nhân viên tạo NĐT")]
        SendMail = 2,
        [Description("Gửi tin nhắn")]
        SendSMS = 3

    }
    public enum LogCallLMS_Status
    {
        [Description("Chờ call")]
        Waiting = 0,
        [Description("Success")]
        Success = 1,
        [Description("Call Fail")]
        Fail = 2,
        [Description("Đã block")]
        Block = 3
    }
    public enum MoneyCloseLoanType
    {
        [Description("Tất toán theo kỳ")]
        Schedule = 1,
        [Description("Tất toán sớm")]
        FineOriginal = 2,
        [Description("Hợp đồng kết thúc")]
        Finished = 3
    }
    public enum Lender_SeflEmployed
    {
        [Description("Tima")]
        Tima = 0,
        [Description("TD CDVN ( SP Nhỏ )")]
        SPNho = 2,
        [Description("TD CDVN ( SP Lớn )")]
        SPLon = 1,
        [Description("Hub")]
        Hub = 3,
        [Description("Sim")]
        Sim = 4,
        [Description("Lender Ngoài")]
        LenderOut = 5,
        [Description("Lender GCash")]
        LenderGCash = 6,
    }
    public enum Lender_ContractType
    {
        [Description("Loại HĐ Gold/4.0 DT")]
        DTNormal = 0,
        [Description("Loại HĐ Gold/4.0 NA")]
        NANormal = 1,
        [Description("HĐ ủy quyền đầu tư DT")]
        DTTima = 2,
        [Description("HĐ ủy quyền đầu tư NA")]
        NAOfTima = 4,
        [Description("Loại HĐ Titan DT")]
        DTTimaNew = 6,
        [Description("Loại HĐ Titan NA")]
        NAOfTimaNew = 7,
        [Description("HĐ đầu tư hiệu quả NA")]
        NATima = 3,
        [Description("Loại HĐ vay Diamond(G-bảo lãnh) DT")]
        GTimaDT = 5,
        [Description("Loại HĐ vay Diamond(G-bảo lãnh) NA")]
        GTimaNA = 8,

    }

    public enum Lender_DepositType
    {

        [Description("Không đặt cọc")]
        NoDeposit = 0,
        [Description("Đặt cọc")]
        Deposit = 1,
    }
    public enum LOS_ImcomeType
    {
        [Description("Tiền mặt")]
        TienMat = 1,
        [Description(" Chuyển khoản")]
        ChuyenKhoan = 5,
    }



    public enum AGResponse_Status
    {
        [Description("Thành công")]
        Success = 1

    }
    public enum VaSettingKey_Status
    {
        [Description("Dừng")]
        Stop = 0,
        [Description("Call mua VA")]
        CallBy = 1,
        [Description("Lấy thông tin từ AG")]
        InforAG = 2
    }
    public enum TimeType_StopPoint
    {
        [Description("Ngày")]
        DayTime = 1,
        [Description("Đêm")]
        Night = 3
    }

    public enum UserDepartment_PositionID
    {
        [Description("Nhân viên")]
        Staff = 1,
        [Description("Trưởng nhóm")]
        TeamLead = 2,
        [Description("Trưởng phòng")]
        Manager = 3,
        [Description("Giám đốc")]
        Director = 4
    }

    public enum PaymentSchedule_TypeDebtRestructuring
    {
        [Description("Khoanh Gốc")]
        Original = 1,
        [Description("Khoanh Phí Tư Vấn Chia đều vào các kỳ")]
        AverageConsultingFees = 2,
        [Description("Khoanh phí Tư Vân chia vào kỳ cuối")]
        ConsultingFeesLastPeriods = 3,
        [Description("Khoanh phí tư vấn chọn ngày trả mới")]
        ConsultingFeesChangeDate = 4,
        [Description("Miễn Giảm Lãi Phí")]
        ExemptionFees = 5,
        [Description("Chia gốc & phí tư vấn vào các kỳ")]
        BothAverageOriginalConsultingFee = 6,
        [Description("Chia gốc & phí tư vấn vào kỳ cuối")]
        BothOriginalConsultingFeeLastPeriods = 7,

    }

    public enum ExcelReport_Status
    {
        [Description("Chờ xử lý")]
        Waiting = 0,
        [Description("Thành công")]
        Success = 1,
        [Description("Có lỗi xảy ra")]
        Recall = 2
    }
    /// <summary>
    /// <= 200 excel, >200 file doc
    /// </summary>
    public enum ExcelReport_TypeReport
    {
        [Description("Đơn vay nhân viên Call")]
        Loan = 1,
        [Description("Lịch sử KH nạp tiền")]
        HistoryCustomerTopup = 2,
        [Description("Lịch sử hạch toán theo khách hàng")] // excel thn ở báo cáo thu tiền
        HistoryTransactionLoanByCustomer = 3,
        [Description("Thu tiền theo nhóm nợ")] // excel thn  Báo cáo thu tiền theo nhóm nợ
        ReportLoanDebtType = 4,
        [Description("Báo cáo dư nợ đầu ngày")] // excel thn báo cáo dư nợ đầu ngày
        ReportLoanDebtDaily = 6,
        [Description("Báo cáo kế hoạch ngày")] //Báo cáo kế hoạch ngày
        ReportDayPlan = 7,
        [Description("Excel lịch sử tương tác")] //Excel lịch sử tương tác
        ExcelHistoryInteraction = 8,
        [Description("MGL")] // biểu mẫu miễn giảm lãi
        ExemptionInterest = 201,
        [Description("Quyết định thu giữ tài sản")] // Quyết định thu giữ taì sản
        PropertySeizure = 202,
        [Description("Tái cấu trúc nợ")] // tái cấu trúc nợ
        DebtRestructuring = 203,
        [Description("Thông báo nợ lần cuối tín chấp")] // thông báo nợ lần cuối tín chấp
        UnsecuredFinalDebtNotice = 204,
        [Description("Thông báo thu giữ tài sản")] // thông báo thu giữ tài sản
        NotifyPropertySeizure = 205,
        [Description("Thông báo tự nguyện bàn giao tài sản")] // thông báo tự nguyện bàn giao tài sản
        NotifyVoluntarilyHandingOverPropertye = 206,

        [Description("DANH SÁCH ĐƠN ĐẶT CỌC")] // thông báo tự nguyện bàn giao tài sản
        Lender_Deposit = 1001,
        [Description("Mẫu BM01")] // thông báo tự nguyện bàn giao tài sản
        Lender_BM01 = 1002
    }

    public enum City_DomainID
    {
        [Description("Vùng chưa xác định")]
        Unknow = 0,
        [Description("Miền Bắc")]
        North = 1,
        [Description("Miền Nam")]
        South = 2,
    }
    public enum CodeTranData
    {
        [Description("KYC 3 địa chỉ khách hàng thường xuyên lui tới")]
        K03 = 3,
        [Description("KYC 3 số điện thoại thường xuyên liên hệ")]
        K06 = 6,
        [Description("Cung cấp SĐT & các thông tin khác từ CMT")]
        K21 = 21,
        [Description("Cung cấp thông tin CMT từ SĐT")]
        K22 = 22
    }
    public enum CustomerExtensionThirdParty_Status
    {
        [Description("Chờ xử lý")]
        Wait = 0,
        [Description("Thành công")]
        Success = 1,
        [Description("Có lỗi xảy ra")]
        Error = 2

    }
    public enum SendSms_Status
    {
        [Description("Chờ xử lý")]
        Wait = 0,
        [Description("Gửi thành công")]
        Success = 1,
        [Description("Gửi thất bại")]
        Error = 2
    }
    public enum AssignmentLoan_Status
    {
        [Description("Không xử lý")]
        NoProcessing = 0,
        [Description("Xử lý")]
        Processing = 1
    }

    public enum AssignmentLoan_Type
    {
        [Description("Xử lý nợ")]
        DebtLoan = 0,
        [Description("Thu xe")]
        MotoSeizure = 1,
        [Description("Thu xe + Xử lý nợ")]
        AllDebtMoto = 2
    }
    public enum PaymentSchedule_Fined
    {
        [Description("Chưa ghi nhận phí phạt")]
        NotYet = 0,
        [Description("Đã ghi nhận phí phạt")]
        Fined = 1,
    }
    public enum Department_Status
    {
        [Description("Khóa")]
        UnActive = 0,
        [Description("Hoạt động")]
        Active = 1
    }
    public enum UserDepartment_Status
    {
        [Description("Khóa")]
        UnActive = 0,
        [Description("Hoạt động")]
        Active = 1
    }

    public enum THN_DPD
    {
        [Description("B0")]
        B0 = 0,
        [Description("B1")]
        B1 = 1,
        [Description("B2")]
        B2 = 2,
        [Description("B3")]
        B3 = 3,
        [Description("B4")]
        B4 = 4,
        [Description("B5")]
        B5 = 5,
        [Description("B6")]
        B6 = 6,
        [Description("B7")]
        B7 = 7,
        [Description("B8")]
        B8 = 8,
        [Description("B9")]
        B9 = 9,
        [Description("B10")]
        B10 = 10,
        [Description("B11")]
        B11 = 11,
        [Description("B12")]
        B12 = 12,
        [Description("B13")]
        B13 = 13,
        [Description("B13+")]
        B14 = 14,

    }

    public enum RequestCloseLoan_Status
    {
        [Description("Chờ xử lý")]
        Waitting = 0,
        [Description("Đã phê duyệt")]
        Approved = 1,
        [Description("Từ chối")]
        Reject = 2,
        [Description("Xử lý thành công")]
        Success = 3,
        [Description("Xử lý lỗi")]
        Fail = 4,
        [Description("Hủy xử lý")]
        Cancel = 5
    }

    public enum PaymentConfiguration_MapMoney
    {
        [Description("MoneyOriginal")]
        MoneyOriginal = 1,
        [Description("MoneyInterest")]
        MoneyInterest = 2,
        [Description("MoneyService")]
        MoneyService = 3,
        [Description("MoneyConsultant")]
        MoneyConsultant = 4,
        [Description("MoneyFineLate")]
        MoneyFineLate = 5,
        [Description("MoneyFineInterestLate")]
        MoneyFineInterestLate = 6,
        [Description("MoneyFineOriginal")]
        MoneyFineOriginal = 7,
    }

    public enum KafkaLogEvent_Status
    {
        [Description("Chờ xử lý")]
        Waitting = 0,
        [Description("Xử lý thành công")]
        Success = 1,
        [Description("Xử lý lỗi")]
        Fail = 2
    }

    public enum LenderUser_Status
    {
        [Description("Default")]
        Default = -1,
        [Description("Khóa")]
        UnActive = 0,
        [Description("Hoạt động")]
        Active = 1,
        [Description("Chờ xử lý")]
        Waiting = 3,
    }

    public enum LenderUser_Verified
    {
        [Description("Chưa xác minh")]
        UnActive = 0,
        [Description("Đã xác minh")]
        Active = 1,
    }

    public enum LogLenderChangeInfo_LogTypeID
    {
        [Description("Chữ kí số")]
        LenderDigitalSignature = 1,
        [Description("Khẩu vị đại lý")]
        LenderSpicesConfig = 2,
        [Description("Thông tin Lender")]
        LenderInfomation = 3,
        [Description("Hợp đồng Lender")]
        LenderContract = 4,
    }

    public enum LenderSpicesConfig_RateType
    {
        [Description("Dư nợ giảm dần")]
        RateAmortization = 1,
        [Description("Tất toán cuối kỳ")]
        EndingBalance = 2,
        [Description("Tất cả")]
        All = 3,
    }


    public enum LenderSpicesConfig_TypeMoney
    {
        [Description("Nhỏ hơn hoặc bằng 5 triệu đồng (X <= 5)")]
        Bellow5 = 1,
        [Description("Lớn hơn 5 triệu và nhỏ hơn hoặc bằng 15 triệu đồng (5 < x <=15)")]
        From5To15 = 2,
        [Description("Trên 15 triệu đồng (x>15)")]
        Over15 = 3,
        [Description("Tất cả")]
        All = 4,
    }
    public enum BookDebt_Status
    {
        [Description("Dừng")]
        UnActive = 0,
        [Description("Hoạt động")]
        Active = 1
    }
    public enum ApprovalLevelBookDebt_Status
    {
        [Description("Dừng")]
        Stop = 0,
        [Description("Hoạt động")]
        Active = 1
    }

    public enum LoanExemption_Status
    {
        [Description("Chờ cập nhật chứng từ")]
        WaitFileUpload = 1,
        [Description("Đã cập nhật chứng từ")]
        HasFileUpload = 2,
        [Description("Chờ duyệt")]
        WaitApprove = 3,
        [Description("Đã duyệt")]
        Approved = 4,
        [Description("Đã tất toán")]
        Closed = 5,
        [Description("Hủy")]
        Cancel = 6,
        [Description("Tất toán lỗi")]
        ClosedFail = 7
    }

    public enum LoanExemption_NextStep
    {
        [Description("Hủy")]
        Cancel = 1,
        [Description("Duyệt")]
        Approved = 2,
        [Description("Đẩy lên cấp trên")]
        Next = 3
    }

    public enum LogLoanExemptionAction_ActionType
    {
        [Description("Tạo đề xuất")]
        CreateRequest = 1,
        [Description("Cập nhật chứng từ")]
        UploadFile = 2,
        [Description("Phê duyệt")]
        Approved = 3,
        [Description("Hủy")]
        Cancel = 4,
        [Description("Chờ tất toán")]
        WaitPayment = 5,
        [Description("Tất toán thành công")]
        PaymentSuccess = 6,
        [Description("Tất toán thất bại")]
        PaymentFail = 7,
        [Description("Thử lại tất toán")]
        RetryPayment = 8,
    }
    public enum PlanHandleLoanDaily_Status
    {
        [Description("Xóa")]
        Delete = -1,
        [Description("Chưa xử lý")]
        NoProcess = 0,
        [Description("Đã xử lý")]
        Processed = 1,
        [Description("Đã chuyển qua người khác")]
        Moved = 2
    }
    public enum PlanHandleLoanDaily_TypeHandle
    {
        [Description("Chưa xử lý")]
        NoProcess = 0,
        [Description("Đơn cắt kỳ")]
        LoanCutPeriod = 1,
        [Description("Comment và check in")]
        CommentAndCheckIn = 2,
        [Description("Comment và call")]
        CommentAndCall = 3
    }

    public enum Aff_Status
    {
        [Description("Xóa")]
        Delete = -1,
        [Description("Đã xác minh")]
        Active = 1,
        [Description("Chờ xác minh")]
        UnActive = 0,
    }

    public enum PlanHandleLoanDaily_StatusCheckIn
    {
        [Description("Chưa xử lý")]
        NoProcess = 0,
        [Description("Đã xử lý")]
        Processed = 1,
        [Description("CheckIn thành công")]
        Success = 2
    }

    public enum DebtRestructuringLoan_DebtRestructuringType
    {
        [Description("Tái cấu trúc nợ")]
        DebtRestructure = 1,
        [Description("Giãn nợ")]
        DebtRelief = 2
    }
    public enum DebtRestructuringLoan_Status
    {
        //[Description("Chờ duyệt")]
        //Waiting = 1,
        [Description("Chờ cấu trúc")]
        Approved = 2,
        [Description("Đã cấu trúc")]
        Handled = 3,
        [Description("Hết hạn cấu trúc")]
        ExpirationDate = 4,
        [Description("Xóa")]
        Deleted = -1
    }
    public enum DebtRestructuringLoan_Source
    {
        [Description("Hệ thống")]
        System = 1,
        [Description("Trình base")]
        Base = 2
    }
    public enum LogActionDebtRestructuringLoan_ActionType
    {
        [Description("Thêm mới")]
        Insert = 0,
        [Description("Cập nhật")]
        Update = 1,
        [Description("Phê duyệt")]
        Approved = 2,
        [Description("Đã thực thi")]
        Executed = 3,
        [Description("Xóa")]
        Deleted = 4
    }
    public enum DebtRestructuringLoan_AppView
    {
        [Description("Không xử lý")]
        No = 0,
        [Description("Được xử lý")]
        Yes = 1,
        [Description("Đã xử lý")]
        Executed = 2
    }
    public enum PlanHandleLoanDaily_AppView
    {
        [Description("Chưa lên kế hoạch")]
        No = 0,
        [Description("đang trong kế hoạch")]
        Yes = 1,
        [Description("Hết hiệu lực")]
        ExpirationDate = 2
    }
    public enum CollaboratorLender_Status
    {
        [Description("Hủy")]
        Cannel = -1,
        [Description("Chờ duyệt")]
        WaitApprove = 0,
        [Description("Đã duyệt")]
        Approved = 1,
    }

    public enum PaymentScheduleDepositMoney_Status

    {
        [Description("Hủy")]
        Cancel = -1,
        [Description("Chờ làm hồ sơ đặt cọc")]
        DefaultWaittingRecordDeposit = 0,

        //[Description("Chờ làm hồ sơ đặt cọc")]
        //WaittingRecordDeposit = 1,

        [Description("Chờ đặt cọc")]
        WaittingDeposit = 2,

        [Description("Đã đặt cọc")]
        Deposited = 3,

        [Description("Chờ hoàn cọc")]
        WaitingReimbursement = 4,

        [Description("Đã hoàn cọc")]
        PaidBack = 5,
    }

    public enum PlanHandleCallDaily_Status
    {
        [Description("Chờ đẩy lên TĐ")]
        Waiting = 0,
        [Description("Đã đẩy lên TĐ")]
        Pushed = 1,
        [Description("Đã quay số")]
        Handled = 2,
        [Description("Quá số lần gọi")]
        OverLimit = 3,
        [Description("Hủy cuộc gọi")]
        Cancel = 4
    }
    public enum PlanHandleCallDaily_CallResult
    {
        [Description("Chưa xử lý")]
        Waiting = 0,
        [Description("Gọi thành công")]
        Success = 1,
        [Description("KH không nghe máy")]
        Fail = 2
    }
    public enum PlanHandleCallDaily_PriorityOrder
    {
        [Description("DPD quá hạn")]
        DPDOverDue = 1,
        [Description("DPD trong hạn")]
        DPDInDue = 2,
        [Description("Số tham chiếu 1")]
        ReferLevle1 = 3,
        [Description("Số tham chiếu 2")]
        ReferLevle2 = 4,
        [Description("Số tham chiếu 3")]
        ReferLevle3 = 5,
        [Description("Số tham chiếu 4")]
        ReferLevle4 = 6,
        [Description("Số tham chiếu 5")]
        ReferLevle5 = 7,
        [Description("Số tham chiếu 6")]
        ReferLevle6 = 8,
        [Description("Số tham chiếu 7")]
        ReferLevle7 = 9
    }
}
