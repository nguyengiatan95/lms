﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Common.Constants
{
    public class ServiceHostInternal
    {
        public const string OutGateway = "http://lms_outgateway_service_api";
        public const string PaymentService = "http://lms_paymentschedule_service_api";
        public const string LoanService = "http://lms_loan_service_api";
        public const string AuthenService = "http://lms_authen_service_api";
        public const string CustomerService = "http://lms_customer_service_api";
        public const string TransactionService = "http://lms_transaction_service_api";
        public const string LenderService = "http://lms_lender_service_api";
        public const string InvoiceService = "http://lms_invoice_service_api";
        public const string InsuranceSerivce = "http://lms_insurance_service_api";
        public const string CollectionServiceApi = "http://collection_service_api";
        public const string ReportService = "http://lms_report_service_api";
        public const string AdminService = "http://lms_admin_service_api";


        //public const string LoanService = "http://localhost:56587";
        //public const string OutGateway = "http://localhost:56613";
        //public const string PaymentService = "http://localhost:56595";
        //public const string CustomerService = "http://localhost:62697";
        //public const string LenderService = "http://localhost:63919";
        //public const string CollectionServiceApi = "http://localhost:57585";
    }

    public class ActionApiInternal
    {
        public const string Customer_CreateInfoBorrower = "/api/Customer/CreateInfoBorrower";
        public const string Customer_GetInfosByNumberCard = "/api/Customer/GetCustomerInfosByNumbercard";
        public const string Customer_GetCustomerInfosByIDs = "/api/Customer/GetCustomerInfosByIDs";
        public const string Customer_UpdateMoneyCustomer = "/api/Customer/UpdateMoneyCustomer";
        public const string CustomerCreateBorrower = "/api/customer/createborrower";
        public const string Customer_ReportExtraMoneyCustomer = "/api/Report/ReportExtraMoneyCustomer";
        public const string Customer_UpdatePayType = "/api/Customer/UpdatePayType";
        public const string Customer_ChangePhone = "/api/Customer/ChangePhone";

        public const string PaymentSchedule_GetLstPaymentByLoanID = "/api/PaymentSchedule/GetLstPaymentByLoanID";
        public const string PaymentSchedule_GenPaymentScheduleByCondition = "/api/PaymentSchedule/GenPaymentScheduleByCondition";
        public const string PaymentSchedule_CreatePaymentScheduleByLoanID = "/api/PaymentSchedule/CreatePaymentScheduleByLoanID";
        public const string PaymentSchedule_PayPaymentByLoanID = "/api/PaymentSchedule/PayPaymentByLoanID";
        public const string PaymentSchedule_PayPartialScheduleInPeriodsByLoanID = "/api/PaymentSchedule/PayPartialScheduleInPeriodsByLoanID";
        public const string PaymentSchedule_CloseLoanFromSchedule = "/api/PaymentSchedule/CloseLoanFromSchedule";
        public const string PaymentSchedule_UpdateStatusPaymentScheduleByLoan = "/api/PaymentSchedule/UpdateStatusPaymentScheduleByLoan";
        public const string PaymentSchedule_CuttingOffPaymentLoanInsuranceByLoanID = "/api/PaymentSchedule/CuttingOffPaymentLoanInsuranceByLoanID";
        public const string PaymentSchedule_ForceCloseLoanByLoan = "/api/PaymentSchedule/ForceCloseLoanByLoan";
        public const string PaymentSchedule_ExtendPaymentScheduleByLoanID = "/api/PaymentSchedule/ExtendPaymentScheduleByLoanID";
        public const string PaymentSchedule_WriteLstMoneyFine = "/api/PaymentSchedule/WriteLstMoneyFine";
        public const string PaymentSchedule_DebtRestructuring = "/api/PaymentSchedule/DebtRestructuring";
        public const string PaymentSchedule_CancelMoneyFineByLoanID = "/api/PaymentSchedule/CancelMoneyFineByLoanID";
        public const string PaymentSchedule_DeleteMoneyFineLateByLoanID = "/api/PaymentSchedule/DeleteMoneyFineLateByLoanID";
        public const string PaymentSchedule_AppGenPaymentSchedule = "/api/PaymentSchedule/AppGenPaymentSchedule";
        public const string PaymentSchedule_GetLstPaymentByTimaLoanID = "/api/PaymentSchedule/GetLstPaymentByTimaLoanID";

        public const string PaymentSchedule_GenDebtRestructuring = "/api/PaymentSchedule/GenDebtRestructuring";


        public const string Lender_GetLenderInfosByIDs = "/api/Lender/GetLenderInfosByIDs";
        public const string Lender_UpdateMoneyLender = "/api/Lender/UpdateMoneyLender";
        public const string Lender_CreateLenderDebt = "/api/Lender/CreateLenderDebt";
        public const string Lender_CreateOrUpdateLenderFromAG = "/api/Lender/CreateOrUpdateLenderFromAG";
        public const string Lender_GetLoanForLender = "/api/Lender/GetLoanForLender";
        public const string Lender_GetDetailMoneyLoan = "/api/Lender/GetDetailMoneyLoan";
        public const string Lender_CreateLenderDigitalSignature = "/api/Lender/CreateLenderDigitalSignature";
        public const string Lender_CreateLenderUser = "/api/Lender/AppCreateLenderUser";
        public const string Lender_RunPaymentScheduleDepositMoney = "/api/Lender/RunPaymentScheduleDepositMoney";


        public const string Loan_Action_GetLoanByID = "/api/loan/GetLoanByID/{0}";
        public const string Loan_Action_UpdateLoanAfterPayment = "/api/loan/UpdateLoanAfterPayment";
        public const string Loan_Action_CheckInOutBankCardDate = "/api/CashBankCardByDate/CheckInOutBankCardDate";
        public const string Loan_Action_UpdateLinkInsurance = "/api/Loan/UpdateLinkInsurance";
        public const string Loan_ProcessMoneyFineLate = "/api/loan/ProcessMoneyFineLate";
        public const string Loan_ProcessAutoPayment = "/api/loan/ProcessAutoPayment";
        public const string Loan_ProcessAutoDisbursementLoanCredit = "/api/loan/ProcessAutoDisbursementLoanCredit";
        public const string Loan_GetMoneyNeedCloseLoanByLoanID = "/api/loan/GetMoneyNeedCloseLoanByLoanID";
        public const string Loan_ProcessCutOffLoanDaily = "/api/loan/ProcessCutOffLoanDaily";
        public const string Loan_ProcessCalculatorMoneyCloseLoanDaily = "/api/loan/ProcessCalculatorMoneyCloseLoanDaily";
        public const string Loan_GetLstPaymentScheduleByLoanID = "/api/Loan/GetLstPaymentScheduleByLoanID";
        public const string Loan_GetTransactionByLoanID = "/api/loan/GetTransactionByLoanID";
        public const string Loan_ProcessSplitTransactionLoan = "/api/loan/ProcessSplitTransactionLoan";
        public const string Loan_GenDebtRestructuring = "/api/Loan/GenDebtRestructuring";


        public const string Transaction_Action_CreateTransactionAfterPayment = "/api/transaction/CreateTransactionAfterPayment";
        public const string Transaction_Action_GetByLoanID = "/api/transaction/GetByLoanID/{0}";

        public const string Invoice_CreateInvoicePaySlipCustomer = "/api/Invoice/CreateInvoicePaySlipCustomer";
        public const string Invoice_CreateInvoiceConsiderCustomer = "/api/Invoice/CreateInvoiceConsiderCustomer";
        public const string Invoice_CreateInvoiceCustomer = "/api/Invoice/CreateInvoiceCustomer";
        public const string Invoice_CreateInvoiceOnBehalfShop = "/api/Invoice/CreateInvoiceOnBehalfShop";
        public const string Invoice_CreateInvoiceLender = "/api/Invoice/CreateInvoiceLender";
        public const string Invoice_CreateInvoiceBankInterest = "/api/Invoice/CreateInvoiceBankInterest";

        public const string Insurance_CreateInsuranceOfCustomer = "/api/Insurance/CreateInsuranceOfCustomer";
        public const string Insurance_CreateInsuranceOfLender = "/api/Insurance/CreateInsuranceOfLender";
        public const string Insurance_InsurancePremiumsOfCustomers = "/api/Insurance/InsurancePremiumsOfCustomers";
        public const string Insurance_CalculateMoneyInsuranceLOS = "/api/Insurance/CalculateMoneyInsuranceLOS";
        public const string Insurance_ByInsurance = "/api/Insurance/ByInsurance";
        public const string Insurance_InformationInsuranceAI = "/api/Insurance/InformationInsuranceAI";
        public const string Insurance_CreateInsuranceMaterial = "/api/Insurance/CreateInsuranceMaterial";

        public const string BankCard_ProcessSummaryTransactionDate = "/api/BankCard/ProcessSummaryTransactionDate";
        public const string BankCard_ProcessSummaryBankCardFromInvoiceID = "/api/BankCard/ProcessSummaryBankCardFromInvoiceID";
        public const string BankCard_ProcessSummaryBankCardByBankCardID = "/api/BankCard/ProcessSummaryBankCardByBankCardID";
        public const string Report_ProcessSummaryStasticsLoanBorrow = "/api/Report/ProcessSummaryStasticsLoanBorrow";

        public const string VA_RegisterVa = "/api/VA/RegisterVa";

        // sms
        public const string Sms_ProcessSmsAnalytics = "/api/sms/ProcessSmsAnalytics";
        public const string Sms_ProcessSmsOtpDisbursement = "/api/sms/ProcessSmsOtpDisbursement";

        //OutGatewayApi
        public const string OutGatewayApi_LOS_GetHistoryComment = "/api/LOS/GetHistoryComment";
        public const string OutGatewayApi_LOS_GetListImages = "/api/LOS/GetListImages";
        public const string OutGatewayApi_LOS_DisbursementWaiting = "/api/LOS/DisbursementWaiting";
        public const string OutGatewayApi_LOS_GetWaitingMoneyDisbursementLenderLOS = "/api/LOS/GetWaitingMoneyDisbursementLenderLOS";
        public const string OutGatewayApi_LOS_ChangePhoneLOS = "/api/LOS/ChangePhoneLOS";
        public const string OutGatewayApi_LOS_GetRelativeFamilyLOS = "/api/LOS/GetRelativeFamilyLOS";
        public const string OutGatewayApi_LOS_CreateRelationshipLOS = "/api/LOS/CreateRelationshipLOS";
        public const string OutGatewayApi_LOS_GetVehicleInforOfLoan = "/api/LOS/GetVehicleInforOfLoan";
        public const string OutGatewayApi_LOS_CreateLenderDigitalSignature = "/api/LOS/CreateLenderDigitalSignature";
        public const string OutGatewayApi_LOS_SaveCommentHistory = "/api/LOS/SaveCommentHistory";
        public const string OutGatewayApi_LOS_GetLoanCreditDetail = "/api/LOS/GetLoanCreditDetail";
        public const string OutGatewayApi_LOS_PushLoanLender = "/api/LOS/PushLoanLender";
        public const string OutGatewayApi_LOS_LockLoan = "/api/LOS/LockLoan";
        public const string OutGatewayApi_LOS_DisbursementLoan = "/api/LOS/DisbursementLoan";
        public const string OutGatewayApi_LOS_ReturnLoan = "/api/LOS/ReturnLoan";
        public const string OutGatewayApi_LOS_ChangeDisbursementByAccountant = "/api/LOS/ChangeDisbursementByAccountant";

        public const string OutGatewayApi_CreateBorrowerContract = "/api/Insurance/CreateBorrowerContract";
        public const string OutGatewayApi_CreateLenderContract = "/api/Insurance/CreateLenderContract";
        public const string OutGatewayApi_GetLink = "/api/Insurance/GetDoc";
        public const string OutGatewayApi_GetInformationInsuranceAI = "/api/Insurance/GetInformationInsuranceAI";

        public const string OutGatewayApi_VA_Register = "/api/VA/Register";

        public const string OutGatewayApi_Payment_VIBCreateTransaction = "/api/Payment/VibCreateTransaction";
        public const string OutGatewayApi_Payment_VIBExecuteOtp = "/api/Payment/VibExecuteOtp";
        public const string OutGatewayApi_Payment_VIBCancelTransaction = "/api/Payment/VibCancelTransaction";
        public const string OutGatewayApi_CreateInsuranceMaterial = "/api/Insurance/CreateInsuranceMaterial";

        public const string OutGatewayApi_AI_TopFriendFacebook = "/api/AI/TopFriendFacebook";
        public const string OutGatewayApi_AI_GetStopPoint = "/api/AI/GetStopPoint";
        public const string OutGatewayApi_AI_ShowHistoryComment = "/api/AI/ShowHistoryComment";

        public const string OutGatewayApi_ProxyTima_K03ProxyTima = "/api/ProxyTima/K03ProxyTima";
        public const string OutGatewayApi_ProxyTima_K06ProxyTima = "/api/ProxyTima/K06ProxyTima";
        public const string OutGatewayApi_ProxyTima_K21ProxyTima = "/api/ProxyTima/K21ProxyTima";
        public const string OutGatewayApi_ProxyTima_K22ProxyTima = "/api/ProxyTima/K22ProxyTima";
        public const string OutGatewayApi_AI_CheckInOut = "/api/Ai/CheckInOut";
        public const string OutGatewayApi_AI_ListCheckInOut = "/api/Ai/ListCheckInOut/";

        public const string OutGatewayApi_QueueCallAPIAG = "/api/QueueCallAPI/QueueCallAPIAG";
        public const string OutGatewayApi_CallApiAG = "/api/QueueCallAPI/CallApiAg";

        public const string OutGatewayApi_ListCheckInOut = "/api/Ai/ListCheckInOut";
        public const string OutGatewayApi_CheckInOut = "/api/Ai/CheckInOut";
        public const string OutGatewayApi_SmartDialer_ImportCampaign = "/api/SmartDialer/ImportCampaign";
        public const string OutGatewayApi_SmartDialer_GetPendingOfCampaign = "/api/SmartDialer/GetPendingOfCampaign";
        public const string OutGatewayApi_SmartDialer_GetCampaign = "/api/SmartDialer/GetCampaign";
        public const string OutGatewayApi_SmartDialer_UpdateResource = "/api/SmartDialer/UpdateResource";
        public const string OutGatewayApi_SmartDialer_UpdateCsq = "/api/SmartDialer/UpdateCsq";


        public const string CollectionAPI_InsertConfirmSuspendedMoney = "/api/QueueCallAPI/InsertConfirmSuspendedMoney";

        public const string CollectionService_Report_ProcessSummaryEmployeeHandleLoan = "/api/Report/ProcessSummaryEmployeeHandleLoan";
        public const string CollectionService_Meta_GetLstCity = "/api/Meta/GetLstCity";
        public const string CollectionService_Meta_GetLstDistrictByCity = "/api/Meta/GetDistrictByCityID";
        public const string CollectionService_Loan_GetLstLoanInfoByCondition = "/api/Loan/GetLstLoanInfoByCondition";
        public const string CollectionService_AppTHN_ListLoanBeDivided = "/api/AppTHN/ListLoanBeDivided";
        public const string CollectionService_Reason_GetReason = "/api/Reason/GetReason";
        public const string CollectionService_Comment_GetLstCommentDebtPromptedByLoanID = "/api/CommentDebtPrompted/GetLstCommentDebtPromptedByLoanID";
        public const string CollectionService_THN_LosHistoryComment = "/api/THN/HistoryComment";
        public const string CollectionService_THN_GetImages = "/api/THN/ListImages";
        public const string CollectionService_PreparationCollectionLoan_CreatePreparationCollectionLoan = "/api/PreparationCollectionLoan/CreatePreparationCollectionLoan";
        public const string CollectionService_Loan_GetLoanCreditDetailLosByLoanID = "/api/Loan/GetLoanCreditDetailLosByLoanID";
        public const string CollectionService_SendSms_GetLstSendSms = "/api/SendSms/GetLstSendSms";
        public const string CollectionService_SendSms_UpdateStatusSendSms = "/api/SendSms/UpdateStatusSendSms";
        public const string CollectionService_Loan_GetMotorcycleSeizure = "/api/Loan/GetMotorcycleSeizure";
        public const string CollectionService_HoldCustomerMoney_CreateHoldCustomerMoney = "/api/HoldCustomerMoney/CreateHoldCustomerMoney";
        public const string CollectionService_HoldCustomerMoney_UpdateHoldCustomerMoney = "/api/HoldCustomerMoney/UpdateHoldCustomerMoney";
        public const string CollectionService_Report_ProcessReportLoanDebtType = "/api/Report/ProcessReportLoanDebtType";
        public const string CollectionService_Meta_GetWardByDistrictID = "/api/Meta/GetWardByDistrictID";
        public const string TrandataCallProxy = "/api/CustomerExtensionThirdParty/TrandataCallProxy";
        public const string CollectionService_Meta_GetPreparationType = "/api/Meta/GetPreparationType";
        public const string CollectionService_MGLP_TranferAgCloseLoanExemption = "/api/MGLP/TranferAgCloseLoanExemption";
        public const string CollectionService_CommentDebtPrompted_CreateCommentDebtPrompted = "/api/CommentDebtPrompted/CreateCommentDebtPrompted";
        public const string CollectionService_PlanHandleLoanDaily_CreatePlanHandleLoanDaily = "/api/PlanHandleLoanDaily/CreatePlanHandleLoanDaily";
        public const string CollectionService_PlanHandleLoanDaily_PlanHandleLoanDaily = "/api/PlanHandleLoanDaily/PlanHandleLoanDaily";
        public const string CollectionService_PlanHandleLoanDaily_PlanHandleLoanDailyDetails = "/api/PlanHandleLoanDaily/PlanHandleLoanDailyDetails";
        public const string CollectionService_PlanHandleLoanDaily_UpdatePlanHandleLoanDaily = "/api/PlanHandleLoanDaily/UpdatePlanHandleLoanDaily";
        public const string CollectionService_PlanHandleLoanDaily_MovePlanHandleLoanDaily = "/api/PlanHandleLoanDaily/MovePlanHandleLoanDaily";
        public const string CollectionService_PlanHandleLoanDaily_CheckInOut = "/api/PlanHandleLoanDaily/CheckInOut";
        public const string CollectionService_PlanHandleLoanDaily_GetResultCheckOutFromAI = "/api/PlanHandleLoanDaily/GetResultCheckOutFromAI";
        public const string CollectionService_PlanHandleLoanDaily_AppPlanDaily = "/api/PlanHandleLoanDaily/AppPlanDaily";
        public const string CollectionService_AppTHN_GetTeamLead = "/api/AppTHN/GetTeamLead";
        public const string CollectionService_user_getlstemployees = "/api/user/getlstemployees";
        public const string CollectionService_AppTHN_SummaryPlanLoanHandleByStaff = "/api/AppTHN/GetSummaryPlanLoanHandleByStaff";
        public const string CollectionService_User_GetPositionUser = "/api/user/GetPositionUser";
        public const string CollectionService_THN_SearchHistoryTransaction = "/api/thn/SearchHistoryTransaction";
        public const string CollectionService_AppTHN_GetSummaryEmployeeForPlanHandleLoanByLstDepartmentID = "/api/AppTHN/GetSummaryEmployeeForPlanHandleLoanByLstDepartmentID";
        public const string CollectionService_CustomerExtensionThirdParty_SearchCustomerExtensionThirdParty = "/api/CustomerExtensionThirdParty/SearchCustomerExtensionThirdParty";
        public const string CollectionService_SmartDialer_PushPhoneDailyToCallCenter = "/api/SmartDialer/PushPhoneDailyToCallCenter";
        public const string CollectionService_SmartDialer_ProcessingThePhoneForDailyCall = "/api/SmartDialer/ProcessingThePhoneForDailyCall";
        public const string CollectionService_MGLP_CancelLoanExemptionByExpired = "/api/MGLP/CancelLoanExemptionByExpired";



        public const string ReportService_Loan_ProcessReportLoanDebtDaily = "/api/loan/ProcessReportLoanDebtDaily";
        public const string AdminService_Department_GetDepartment = "/api/Department/GetDepartment";


    }

    public class ActionApiExternal
    {
        public const string THN_SaveComment = "/LMS/SaveCommmentLMS";
        public const string THN_SaveProvisionsCollection = "/LMS/SaveProvisionsCollection";
        public const string THN_HoldCustomerMoney = "/LMS/HoldCustomerMoney";
        public const string THN_UpdateHoldCustomerMoney = "/LMS/UpdateHoldCustomerMoney";
        public const string THN_SaveConfirmSuspendedMoneyTHN = "/LMS/ConfirmSuspendedMoneyTHN";
        public const string LENDER_LenderDepositMoney = "/LMS/LenderDepositMoney";

    }
}
