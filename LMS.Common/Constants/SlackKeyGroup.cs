﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Common.Constants
{
    public class SlackKeyGroup
    {
        public static string Domain = "SlackAPIDomain";
        public static string GroupAccountant = "SlackAPIUrlMoneyNotiChannelOfKeToan";
        public static string GroupDev = "SlackAPIUrlNotiForDev";
        public static string NguonVon = "SlackAPIUrlNotiForNguonVon";
    }

    public class SlackTitleMessage
    {
        public const string ApiMecash = "Api MeCash";
        public const string AutoDisbursement = "Giải ngân tự động";
        public const string AutoPayment = "Hạch toán tự động";
    }
    public class SlackSMS
    {
        public SlackSMS()
        {
            attachments = new List<SlackAttachments>();
        }
        public string text { set; get; }
        public bool mrkdwn { set; get; }
        public List<SlackAttachments> attachments { set; get; }

    }
    public class SlackAttachments
    {
        // Bank Nane +  Bank Number 
        public string author_name { set; get; }
        // SMS
        public string title { set; get; }
        // Content sms
        public string text { set; get; }
        // Lender Name + Phone + money
        public string pretext { set; get; }
        // màu
        public string color { set; get; }
    }
}
