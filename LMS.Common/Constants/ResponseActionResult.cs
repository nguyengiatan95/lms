﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Common.Constants
{
    public class ResponseActionResult
    {
        public int Result { get; set; }
        public int Total { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public ResponseActionResult()
        {
            Result = (int)LMS.Common.Constants.ResponseAction.Error;
            Message = MessageConstant.ErrorInternal;
        }

        public void SetSucces()
        {
            Result = (int)LMS.Common.Constants.ResponseAction.Success;
            Message = MessageConstant.Success;
        }
    }
    public class ResponseForDataTable
    {
        public int Draw { get; set; }
        public int RecordsFiltered { get; set; }
        public int RecordsTotal { get; set; }
        public object Data { get; set; }
        public ResponseForDataTable()
        {
            Data = new object();
        }
    }

  

}
