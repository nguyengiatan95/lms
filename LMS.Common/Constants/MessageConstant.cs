﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Common.Constants
{
    public class MessageConstant
    {
        public const string ErrorInternal = "Có lỗi xảy ra trong quá trình xử lý.Liên hệ IT hỗ trợ.";
        public const string ErrorFormatDate = "Lỗi định dạng ngày.";
        public const string Success = "Thành công.";
        public const string ParameterInvalid = "Tham số không hợp lệ.";
        public const string InforNotChange = "Thông tin không thay đổi";

        //Permission
        public const string ErrorExist = "Đã tồn tại trong hệ thống";
        public const string ErrorNotExist = "Không tồn tại trong hệ thống";
        public const string NotPermission = "Không có quyền truy cập";

        //Department 
        public const string DepartmentNotExist = "Không tồn tại phòng ban";
        public const string DepartmentNameNull = "Chưa truyền departmentName";
        public const string DepartmentNameExist = "Đã tồn tại  departmentName";

        //Group 
        public const string GroupNotExist = "Không tồn tại group";
        public const string GroupNameNull = "Chưa truyền groupName";
        public const string GroupNameExist = "Đã tồn tại  groupName";

        //User 
        public const string UserWrongPass = "Thông tin tài khoản hoặc mật khẩu không đúng.";
        public const string UserExist = "Đã tồn tại user";
        public const string UserNotExist = "Không tồn tại user";
        public const string UserNullFullName = "Chưa truyền FullName";
        public const string UserNullUserName = "Chưa truyền UserName";
        public const string UserNullPass = "Chưa truyền Password";
        public const string UserNullPermission = "Chưa phân quyền user";
        public const string UserErrorPass = "Mật khẩu cũ sai";



        public const string UserNameExist = "Đã tồn tại  userName";

        // Customer
        public const string CustomerExist = "Đã tồn tại thông tin khách hàng";
        public const string CustomerSyncTotalMoneyDifferent = "Đơn vay đồng bộ có ngày TotalMoney khác nhau";
        public const string CustomerNotExist = "Không tồn tại thông tin khách hàng";


        // INVOICE
        public const string NoData = "Không có dữ liệu thao tác, vui lòng thử lại!";
        public const string RequireLoanID = "Mã hợp đồng không hợp lệ!";
        public const string RequireCustomerID = "Khách hàng không hợp lệ!";
        public const string RequireFromBankCardID = "Thẻ ngân hàng không hợp lệ!";
        public const string RequireBankCardID = "Thẻ ngân hàng không hợp lệ!";
        public const string RequireShopID = "Cửa hàng không hợp lệ!";
        public const string RequireAmount = "Số tiền phát sinh không hợp lệ!";
        public const string RequireDescription = "Nội dung diễn giải không hợp lệ!";
        public const string RequireDate = "Ngày giờ chứng từ không hợp lệ!";
        public const string RequireStatus = "Trạng thái xác nhận không hợp lệ!";
        public const string RequireCreateBy = "người tạo phiếu không hợp lệ!";


        public const string LoanExist = "Đã tồn tại loan";
        public const string LoanCreateCustomerFail = "Tạo thông tin khách hàng không thành công";
        public const string LoanNotFoundLender = "Không tìm thấy thông tin Lender";

        //Loan
        public const string LoanNotExist = "Không tồn tại đơn vay này";
        public const string LoanCantProcess = "Hợp đồng đã xóa + kết thúc không thể thao tác";
        public const string LoanNotFoundLoanCreditPartner = "Không tìm thấy đơn từ đối tác LOS";
        public const string LoanNotExtend = "Đơn không bảo hiểm mới được gia hạn";
        public const string LoanRateTypeNotExtend = "Đơn tất toán cuối kì mới được gia hạn";
        public const string LoanIDNull = "Chưa truyền LoanID";
        public const string PathInsuranceNull = "Chưa truyền link bảo hiểm";
        public const string LoanSyncNextDateDifferent = "Đơn vay đồng bộ có ngày NextDate khác nhau";
        public const string LoanHavePayment = "Hợp đồng đã có giao dịch tiền không thể thao tác";
        public const string LoanTimeNull = "Chưa truyền LoanTime";
        public const string LoanHasIncurredMoney = "Đơn đã phát sinh tiền";
        public const string RateTypeNull = "Chưa truyền RateType";
        public const string RateInterestNull = "Chưa truyền RateInterest";
        public const string RateServiceNull = "Chưa truyền RateService";
        public const string RateConsultantNull = "Chưa truyền RateConsultant";

        public const string LoanCloseError = "Đóng hợp đồng không thành công, vui lòng thử lại sau";
        public const string LoanSentInsurance = "Hợp đồng đã gửi sang bồi thường bảo hiểm";
        public const string LoanInsurancePaid = "Hợp đồng đã bồi thường bảo hiểm";
        public const string LoanErrorInsuranceStatus = "Trạng thái bảo hiểm đang ở trạng thái này rồi";
        public const string LoanUpdateError = "Cập nhật thông tin đơn vay không thành công";
        public const string IsLockOpen = "Đơn vay đang mở không tạo được đơn";
        public const string LoanCreditDisbusementStatusByAccountantNotValid = "Trạng thái đơn vay không hợp lệ";
        public const string LoanCreditIsLock = "Hồ sơ này đang bị khóa, chờ kiểm tra tiền ngân hàng đã đi hay chưa";
        public const string LoanCreditLockNotSuccess = "Khóa đơn không thành công";
        public const string LoanCreditCaculatedInsurranceNotSuccess = "Không tính được phí bảo hiểm cho đơn vay";
        public const string LoanCreditNotYetSentOTP = "Đơn chưa tạo otp";
        public const string LoanCreditNotGetTransionRequestOTPFromAI = "Lỗi chưa lấy được giao dịch từ AI";
        public const string LoanCreditNotSureSentVerifyOTP = "Có lỗi xảy ra khi xác nhận OTP. Mời bạn kiểm tra lại giao dịch trên hệ thống ngân hàng.";
        public const string LoanInvalidDatePayment = "Hợp đồng chưa đến hạn thanh toán tiền";
        public const string NullTypeMoney = "Chưa truyền loại tiền";
        public const string TimeLimit01082021 = "Thời gian tìm kiếm từ ngày 01-08-2021 trở đi";
        public const string TimeFormatError = "Thời gian phải là định dạng: dd/MM/yyyy HH:mm";
        public const string LenderSameCurrent_Error = "LenderID giống lender hiện tại";
        public const string MoveLoanHigherTimeLoan = "Ngày chuyển nợ đang > ngày kết thúc hợp đồng";



        //loandebt
        public const string LoanDebtTotalError = "Số tiền phải > 0";
        public const string LoanDebtPaymentIDError = "Lịch thanh toán không hợp lệ";
        public const string LoanDebtDeleteValid = "Giao dịch này không thể xóa";



        //Transaction
        public const string TransactionInsertSuccess = "Lưu giao dịch thành công";

        //Payment
        public const string PaymentInvalid = "Không tồn tại lịch thanh toán này";
        public const string PaymentCompleted = "Lịch thanh toán này đã hoàn thành";
        public const string PaymentNotEnoughMoney = "Tiền khách hàng không đủ để thanh toán";

        public const string Payment_NotFound = "Không có lịch thanh toán";

        //Invoice
        public const string TotalMoneyNull = "Chưa truyền TotolMoney";
        public const string CustomerIDNull = "Chưa truyền CustomerID";
        public const string BankCardIDNull = "Chưa truyền BankCardID";
        public const string NoteNull = "Chưa truyền Note";
        public const string UserIDCreateNull = "Chưa truyền UserIDCreate";
        public const string CreateDateBadFormat = "Sai định dạng StrTransactionDate";
        public const string NoMoneyForClient = "Chưa cộng được tiền cho khách hàng";
        public const string InvoiceExits = "Phiếu đã tồn tại";
        public const string InvoiceNotExits = "Phiếu không tồn tại";
        public const string NotIdentifiedUserID = "Không xác định được người thao tác";
        public const string NotIdentifiedLoanID = "Không xác định được hợp đồng";
        public const string NotIdentifiedCustomerID = "Không xác định được khách hàng";
        public const string NotIdentifiedBankcardID = "BankcardID không tồn tại ";
        public const string NotIdentifiedLenderID = "Không xác định được lender";
        public const string NoMoneyForLender = "Chưa cộng được tiền cho lender";
        public const string NotIdentifiedShopID = "Không xác định được ShopID";
        public const string NotIdentifiedFromBankCardID = "Không xác định được FromBankCardID";
        public const string NotIdentifiedTotalMoney = "Không xác định được TotalMoney";
        public const string StrTransactionDateNull = "Chưa truyền StrTransactionDate";
        public const string StrCheckCreateDate = "Ngày tạo không quá 1 tháng";
        public const string NoteMaxLength = "Note vượt quá số ký tự";
        public const string InvoiceLoanExist = "Đơn vay đã được giải ngân";
        public const string NotChangeInvoice = "Không được sửa phiếu";
        public const string NotDeleteInvoice = "Không được hủy phiếu";
        public const string NotReturnInvoice = "Không được trả lại phiếu";

        //Menu
        public const string Menu_ExistControllerAction = "Đã tồn tại menu control + action này";

        // Insurance
        public const string InsuranceExist = "Đã tồn tại thông tin baỏ hiểm";
        public const string InsuranceNullLoanCredit = "Chưa truyền LoancreditId";
        public const string InsuranceNullCustomer = "Chưa truyền CustomerId";
        public const string InsuranceNullLender = "Chưa truyền LenderId";
        public const string ByInsuranceError = "Mua bảo hiểm không thành công";
        public const string CallAIInsuranceError = "Lấy thông tin bảo hiểm AI không thành công";
        public const string CalculateMoneyInsuranceError = "Chưa tính được tiền bảo hiểm";
        public const string ReportInsuranceMaxDate = "Khoảng thời gian tìm kiếm không quá 1 tháng";

        // BankCard
        public const string BankCardExist = "Đã tồn tại thông thẻ ngân hàng";
        public const string BankCardNotExist = "Không tồn tại thông thẻ ngân hàng";

        //Bank
        public const string BankNotExist = "Không tồn tại thông tin ngân hàng";

        public const string SettingKey_Invoice_SummaryTransactionDateBankCardEmpty = "Key Invoice_SummaryTransactionDateBankCard chưa gán giá trị bắt đầu";
        public const string SettingKey_Invoice_SummaryTransactionDateBankCardStatus = "Key Invoice_SummaryTransactionDateBankCard chưa bật";
        public const string SettingKey_Invoice_SummaryTransactionDateBankCardFormat = "Key Invoice_SummaryTransactionDateBankCard không đúng định dạng";

        public const string SettingKey_Invoice_SummaryBankCardFromInvoiceIDEmpty = "Key Invoice_SummaryBankCardFromInvoiceID chưa gán giá trị bắt đầu";
        public const string SettingKey_Invoice_SummaryBankCardFromInvoiceIDStatus = "Key Invoice_SummaryBankCardFromInvoiceID chưa bật";
        public const string SettingKey_Invoice_SummaryBankCardFromInvoiceIDFormat = "Key Invoice_SummaryBankCardFromInvoiceID không đúng định dạng";
        public const string SettingKey_Insurance_ByInsurance = "Key Insurance_ByInsurance chưa bật";
        public const string SettingKey_Insurance_InformationInsuranceAI = "Key Insurance_InformationInsuranceAI chưa bật";
        public const string SettingKey_VA_Register = "Key VA_Register chưa bật";
        public const string SettingKey_AG_QueueCallAPI = "Key AG_QueueCallAPI chưa bật";
        public const string SettingKey_CutOffLoan_ProcessDailyStatus = "Key CutOffLoan_ProcessDaily chưa bật";
        public const string SettingKey_PlanCloseLoan_ProcessDailyStatus = "Key PlanCalculatorMoneyCloseLoan_ProcessDaily chưa bật";
        public const string SettingKey_PlanCloseLoan_ProcessDailyFinished = "Key PlanCalculatorMoneyCloseLoan_ProcessDaily đã chạy";
        public const string SettingKey_PlanCloseLoan_ReportLoanDebtType_ProcessEveryHourStatus = "Key ReportLoanDebtType_ProcessEveryHour chưa bật";


        public const string SettingKey_Loan_ProcessFineLateEmpty = "Key Loan_ProcessFineLate chưa gán giá trị bắt đầu";
        public const string SettingKey_Loan_ProcessFineLateStatus = "Key Loan_ProcessFineLate chưa bật";
        public const string SettingKey_Loan_ProcessFineLateFormat = "Key Loan_ProcessFineLate không đúng định dạng";
        public const string SettingKey_Loan_ProcessFineLateStartDate = "Key Loan_ProcessFineLate chưa tới ngày chạy";
        public const string SettingKey_Loan_ProcessFineLateStartTime = "Key Loan_ProcessFineLate chưa tới giờ chạy";

        public const string SettingKey_Loan_ProcessAutoPaymentEmpty = "Key Loan_ProcessAutoPayment chưa gán giá trị bắt đầu";
        public const string SettingKey_Loan_ProcessAutoPaymentStatus = "Key Loan_ProcessAutoPayment chưa bật";
        public const string SettingKey_Loan_ProcessAutoPaymentFormat = "Key Loan_ProcessAutoPayment không đúng định dạng";
        public const string SettingKey_Loan_ProcessAutoPaymentStartTime = "Key Loan_ProcessAutoPayment chưa tới giờ chạy";
        public const string SettingKey_Loan_ProcessAutoDisbursementStatus = "Key Loan_ProcessAutoDisbursement chưa bật";
        public const string SettingKey_Loan_ProcessAutoDisbursementStatusPushAccountant = "Key Loan_ProcessAutoDisbursement chuyển về kế toán giải ngân";
        public const string SettingKey_Loan_ProcessAutoDisbursementProcessing = "Đang xử lý đến ID: {0}";
        public const string SettingKey_Loan_ProcessAutoDisbursementRunning = "Giải ngân tự động đang chạy. Bạn tắt giải ngân trước khi chạy thủ công.";

        public const string SettingKey_Sms_ProcessCreateInvoiceAutoStatus = "Key Sms_ProcessCreateInvoiceAuto chưa bật";
        public const string SettingKey_SplitTransactionLoan_ProcessEveryHour = "Key SplitTransactionLoan_ProcessEveryHour chưa bật";
        public const string SettingKey_ReportLoanDebtDaily_ProcessDailyStatus = "Key ReportLoanDebtDaily_ProcessDaily chưa bật";
        public const string SettingKey_ReportLoanDebtDaily_ProcessDailyFinished = "Key ReportLoanDebtDaily_ProcessDaily đã chạy";

        //Lender
        public const string LenderNotExist = "Không tồn tại thông tin lender";
        public const string LenderExisted = "Đã tồn tại mã lender này";
        public const string Lender_LenderIDError = "LenderID không hợp lệ";
        public const string Lender_DisbursementTypeError = "Loại thời gian search không hợp lệ";
        public const string Lender_StatusTypeError = "Loại trạng thái search không hợp lệ";
        public const string Lender_Error_Update_HaveLoan = "Lender đã có đơn vay, không thể cập nhật";
        public const string CollaboratorLenderApproved = "Công tác viên đã được duyệt. Không thể xóa";



        //report
        public const string MaxDateReport = "Tổng số ngày chọn không quá ";

        public const string ErrorPushLoanLender = "Chưa đẩy được đơn cho Lender";
        public const string ErrormoneyCustomerRecieve = "Số tiền khách hàng nhận không hợp lệ.";
        public const string OpenLoan = "Mở khóa khoản vay";
        public const string LockLoan = "Tạm thời khóa lại";

        //los
        public const string ChangeDisbursementByAccountant = "Chuyển trạng thái kế toán giải ngân";
        public const string RetryMinute = "Vui lòng thử lại sau ít phút";
        public const string NotBankAccountNumber = "Khách hàng không có số tài khoản";
        public const string LenderInvalid = "Lender không hợp lệ";
        public const string LoanWaitingOTP = "Đơn vay đang ở trạng thái chờ OTP";


        //ticket 
        public const string TicketNotStatusWaiting = "Yêu cầu đang không ở trạng thái chờ xử lý";

        //sms
        public const string SMS_ContentDouble = "Nội dung trùng";
        public const string SMS_SearchErrorNote = "Bạn cần chọn ngân hàng hoặc nhập số tiền để search nội dung";


        //Collection
        public const string PreparationDateNull = "Chưa chuyền ngày dự thu";
        public const string InvalidDueDate = "Ngày dự thu không hợp lệ";
        public const string InvalidNumberPaymentPeriods = "Số kỳ chọn lớn hơn số kỳ phải đóng hiện tại";
        public const string NotIdentifiedPayment = "Không tồn tại lịch thanh toán của đơn vay này";
        public const string CustomerHoldOrderMoney = "Khách hàng đã có một lệnh giữ tiền";
        public const string HoldCustomerMoneyIDNotExist = "HoldCustomerMoneyID không tồn tại";
        public const string CoincidesInforBookDebt = "Thông tin book nợ đã bị trùng";
        public const string ApprovalLevelBookDebtNameExist = "Cấp phê duyệt đã tồn tại";
        public const string LoanExemption_UserApprovalLevelBookDebtNotFound = "Thông tin người thao tác chưa được cấu hình.";
        public const string LoanExemption_Exists = "Đã tồn tại đơn MGL và đang trong quá trình chờ duyệt.";
        public const string LoanExemption_NotFoundBookDebt = "Không tìm thấy cấu hình book nợ.";
        public const string LoanExemption_OverExpirationDate = "Đơn MGL này quá ngày hết hiệu lực.";
        public const string NotDeletePlanHandleLoanDaily = "Đã có đơn được thực hiện. Kế hoạch ngày không thể xóa";
        public const string NotDeleteLoanDaily = "Đơn được thực hiện. Không thể xóa";
        public const string PlanHandleLoanNotFoundDetailLoan = "Không tìm thấy đơn vay trong kế hoạch ngày";
        public const string PlanHandleLoanMinTimeCheckOut = "Chưa đủ thời gian để checkout.";
        public const string ReportDayPlanCheckTime = "Khoảng thời gian chọn phải nằm cùng tháng.";
        public const string HoldCustomerMoneyReasonEmpty = "Bạn chưa nhập lý do.";


        public const string TimaPreparationCollectionLoanIDExist = "TimaPreparationCollectionLoanID đã tồn tại";
        public const string TimaCommentIDExist = "TimaCommentID đã tồn tại";
        public const string SendSmsIDNoteExist = "SendSmsID không tồn tại";

        // service call
        public const string UserNotConfigurationServiceCall = "Thông tin người dùng chưa được cấu hình cuộc gọi.";
        public const string NotInforAI = "Chưa lấy được thông tin AI.";

        //ExportExcel
        public const string ExcelReport_WaitingProcess = "Chờ xử lý xong.";
        public const string ExcelReport_Error = "Xuất dữ liệu bị lỗi. Vui lòng thử lại hoặc liên hệ IT hỗ trợ.";
        public const string ExcelReport_TraceIDNotExist = "Không tồn tại dữ liệu này.";
        public const string ExcelReport_TraceIDNotExistForLoanExemption = "Không xác định được dữ liệu MGL.";

        public const string TimaLoanIDNotExist = " TimaLoanID không tồn tại ";
        public const string CodeTranDataNotExist = "CodeTranData không tồn tại ";
        public const string InsertSuccess = " Số bản ghi được insert thành công : ";
        public const string InsertError = " Số bản ghi được insert lỗi : ";

        public const string ReportLoanDebtType_CutOffDataError = " Không có dữ liệu bảng cutoff ";
        public const string ReportLoanDebtType_RunALL = " đã chạy hết dữ liệu ";

        //UserLender
        public const string UserLender_Existed = " Đã tồn tại trong dữ liệu ";
        public const string UserLender_PhoneInvalid = "Số điện thoại không đúng định dạng";
        public const string UserLender_PasswordInvalid = "Mật khẩu phải có ít nhất 1 chữ hoa và 8 kí tự trở lên";
        public const string UserLender_Verify_LostInfomation = " Thông tin không đủ, không thể verify nhanh ";


        // LenderSpicesConfig
        public const string LenderSpicesConfig_NotExisted = " Không tồn tại trong dữ liệu ";
        public const string LenderSpicesConfig_LoanTimes = "Thời gian vay yêu thích không hợp lệ";
        public const string LenderSpicesConfig_RateTypes = "Hình thức vay không hợp lệ";

        //SettingInterestReduction
        public const string SettingInterestReduction_InvalidData = " Dữ liệu đầu vào cấu hình không hợp lệ";
        public const string RequestCloseLoan_CloseDateWithMinDate = "Ngày đóng hợp đồng không quá 3 ngày so với hiện tại.";
        public const string RequestCloseLoan_CloseDateWithTopUpDate = "Ngày đóng hợp đồng không quá ngày KH đóng tiền gần nhất.";
        public const string RequestCloseLoan_CloseDateWithToDay = "Ngày đóng hợp đồng không quá ngày so với hiện tại.";


        //TblMapUserCall
        public const string MapUserCall_InvalidTypeCall = " Loại hình gọi không hợp lệ";

        //TblAff
        public const string Aff_NumberCardInvalid = "CMND/CCCD không hợp lệ";
        public const string Aff_Existed = "Đã tồn tại ctv có CMND/CCCD này";
        public const string Aff_PhoneExisted = "Đã tồn tại tài khoản là số điện thoại này";
        public const string Aff_NotExisted = "Không tồn tại ctv này";
        public const string Aff_NotOfU = "Cộng tác viên này không phải của bạn";
        public const string Aff_ErrorCreateUser = "Tạo tài khoản không thành công";



        public const string DebtRestructuringLoan_NotDelete = "Không thể xóa vì đơn đã được thực thi hoặc không có nguồn từ base request.";

    }

    public class SmsSenderName
    {
        public const string SmsSenderBIDV = "bidv";
        public const string SmsSenderAgriBank = "agribank";
        public const string SmsSenderVietComBank = "vietcombank";
        public const string SmsSenderVIB = "vib";
        public const string SmsSenderACB = "acb";
        public const string SmsSenderVPBank = "vpbank";
        public const string SmsSenderMOMO = "momo";
        public const string SmsSenderGPAY = "gpay";
        public const string SmsSenderGUTINA = "gutina";
        public const string SmsSenderGPay_VA = "gpay_va";
        public const string SmsSenderVA = "va";
        public static List<string> ListSenderBankCustomer = new List<string> {
            SmsSenderBIDV , SmsSenderAgriBank , SmsSenderVietComBank, SmsSenderVIB, SmsSenderACB,
            SmsSenderVPBank, SmsSenderMOMO, SmsSenderGPAY, SmsSenderGUTINA, SmsSenderGPay_VA, SmsSenderVA
        };

    }


    public class SettingKey_KeyValue
    {
        public const string Invoice_SummaryTransactionDateBankCard = "Invoice_SummaryTransactionDateBankCard";
        public const string Invoice_SummaryBankCardFromInvoiceID = "Invoice_SummaryBankCardFromInvoiceID";
        public const string Loan_ProcessFineLate = "Loan_ProcessFineLate";
        public const string Loan_ProcessAutoPayment = "Loan_ProcessAutoPayment";
        public const string Insurance_ByInsurance = "Insurance_ByInsurance";
        public const string Insurance_InformationInsuranceAI = "Insurance_InformationInsuranceAI";
        public const string Sms_ProcessCreateInvoiceAuto = "Sms_ProcessCreateInvoiceAuto";
        public const string VA_Register = "VA_Register";
        public const string Loan_ProcessAutoDisbursement = "Loan_ProcessAutoDisbursement";

        public const string AG_QueueCallAPI = "AG_QueueCallAPI";
        public const string CutOffLoan_ProcessDaily = "CutOffLoan_ProcessDaily";
        public const string PlanCloseLoan_ProcessDaily = "PlanCalculatorMoneyCloseLoan_ProcessDaily";
        public const string ReportEmployeeHandleLoan_ProcessEveryHour = "ReportEmployeeHandleLoan_ProcessEveryHour";
        public const string ReportLoanDebtType_ProcessEveryHour = "ReportLoanDebtType_ProcessEveryHour";

        public const string TranDataCallProxy = "TranDataCallProxy";
        public const string SplitTransactionLoan_ProcessEveryHour = "SplitTransactionLoan_ProcessEveryHour";
        public const string System_SwitchKafkaHandle = "System_SwitchKafkaHandle";
        public const string ReportLoanDebtDaily_ProcessDaily = "ReportLoanDebtDaily_ProcessDaily";
    }

    public class TimaSettingConstant
    {
        public const string UserNameAG = "AG";
        public const int UserIDAdmin = 1;
        public const string UserNameAdmin = "Admin";
        public const int UserIDAppSystem = 2;
        public const string UserNameAppSystem = "AppSystem";
        public const int UserIDLOS = 3;
        public const string UserNameLOS = "LOS";
        public const string UserNameAutoDisbursement = "auto_disbursement";
        public const int ShopIDTima = 3703;
        public const int ShopIDHUB001 = 5779;
        public const int NumberDayInMonth = 30;
        public const string DateTimeDayMonthYear = "dd/MM/yyyy";
        public const int UnitMillionMoney = 1000000;
        public const int UnitThousandMoney = 1000;
        public const int NumberDayInYear = 365;
        public const int MaxItemQuery = 2000;
        public const int MaxLengthPhone = 10;
        public const string PatternNumberCard = @"(\d{9,12})";
        public const string PatternPhone = @"0[3,5,7,8,9]\d{8}";
        public const string PatternContractCode = @"tc-(\d)+";
        public const long MoneyCanChangeDPD = 10000; // tiền được phép chuyển DPD
        public const int NumberRoundAfterPoint = 2; // số làm tròn sau dấu phẩy
        public const decimal RateMoneySharingPeriods = 0.65M;
        public const int MinCustomerTotalMoney = 50000;
        public const string SystemFineLateCustomer = "Hệ thống ghi nhận phí phạt trả chậm {0}";
        public const string HeaderLMSUserID = "Lms-Userid";
        public const string HeaderLMSUserName = "Lms-Username";
        public const string DateTimeDayMonthYearFull = "dd/MM/yyyy HH:mm:ss";
        public const string DateTimeFullTimeNoSecond = "dd/MM/yyyy HH:mm";
        public const string PrefixContractCode = "TC-";
        public const string SystemDisbursementToCustomer = "TIMA CT CHO {0} THEO TT";
        public const int MaxDaySentInsurance = 90;
        public const int MaxDayAgoQuery = -60;
        public const string PatternContractCodeLOSLoanCreditID = @"hđ-(\d)+";
        public const string PatternContractCodeLOS = @"hđ-";
        public const int MaxRetryCallAG = 3;
        public const int MaxTimeWaitGetNext = 30;
        public const string UserNameLenderConfig = "LD_";
        public const string UserNameCTVConfig = "V01";
        public const int CallSucessAPI = 200;
        public const string FormatLengthLenderCode = "00000";
        public const string FormatLengthCTVCode = "000000";
        public const string FormatMoney = "#,##0";
    }
}
