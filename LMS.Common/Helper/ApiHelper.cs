﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Polly;
using Polly.Retry;
using RestSharp;
using Steeltoe.Common.Discovery;
using Steeltoe.Discovery;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Common.Helper
{
    public interface IApiHelper : IDisposable
    {
        Task<T> ExecuteAsync<T>(string url, Method method = Method.GET, object body = null, string token = null, string appName = null);
        Task<LMS.Common.Constants.ResponseActionResult> ExecuteAsync(string url, Method method = Method.GET, object body = null, string token = null, string appName = null);
    }

    // https://medium.com/@therealjordanlee/retry-circuit-breaker-patterns-in-c-with-polly-9aa24c5fe23a
    public class ApiHelper : IApiHelper
    {
        DiscoveryHttpClientHandler _handler;
        //RetryPolicy _retryPolicy;
        ILogger<ApiHelper> _logger;
        LMS.Common.Helper.Utils _common;
        IHttpContextAccessor _httpContextAccessor;
        public ApiHelper(IDiscoveryClient discoveryClient,
            IHttpContextAccessor httpContextAccessor,
            ILogger<ApiHelper> logger)
        {
            _logger = logger;
            _common = new Utils();
            _handler = new DiscoveryHttpClientHandler(discoveryClient);
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<T> ExecuteAsync<T>(string url, Method method = Method.GET, object body = null, string token = null, string appName = null)
        {
            //return await _retryPolicy.ExecuteAsync<T>(async () => await ExecuteDetailAsync<T>(url, method, body, token, appName));
            return await ExecuteDetailAsync<T>(url, method, body, token, appName);
        }
        public async Task<LMS.Common.Constants.ResponseActionResult> ExecuteAsync(string url, Method method = Method.GET, object body = null, string token = null, string appName = null)
        {
            //return await _retryPolicy.ExecuteAsync<LMS.Common.Constants.ResponseActionResult>(async () => await ExecuteBaseAsync(url, method, body, token, appName));
            return await ExecuteBaseAsync(url, method, body, token, appName);
        }

        private async Task<T> ExecuteDetailAsync<T>(string url, Method method = Method.GET, object body = null, string token = null, string appName = null)
        {
            try
            {
                LMS.Common.Constants.ResponseActionResult response = await ExecuteBaseAsync(url, method, body, token, appName);
                if (response != null && response.Result == (int)LMS.Common.Constants.ResponseAction.Success)
                {
                    return _common.ConvertJSonToObjectV2<T>(response.Data.ToString());
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ExecuteDetailAsync_Exception|url={url}|Body={_common.ConvertObjectToJSonV2(body)}|ex={ex.Message}-{ex.StackTrace}");
            }
            return default;
        }
        private async Task<LMS.Common.Constants.ResponseActionResult> ExecuteBaseAsync(string url, Method method = Method.GET, object body = null, string token = null, string appName = null)
        {
            HttpResponseMessage respone = null;
            try
            {
                var httpClient = new HttpClient(_handler, true);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                if (!string.IsNullOrEmpty(token))
                {
                    httpClient.DefaultRequestHeaders.Add("Authorization", token);
                }
                string headerUserID = _httpContextAccessor.HttpContext?.Request?.Headers[Constants.TimaSettingConstant.HeaderLMSUserID] ?? $"{Constants.TimaSettingConstant.UserIDAdmin}";
                string headerUserName = _httpContextAccessor.HttpContext?.Request?.Headers[Constants.TimaSettingConstant.HeaderLMSUserName] ?? $"{Constants.TimaSettingConstant.UserNameAdmin}";

                httpClient.DefaultRequestHeaders.Add(Constants.TimaSettingConstant.HeaderLMSUserID, headerUserID);
                httpClient.DefaultRequestHeaders.Add(Constants.TimaSettingConstant.HeaderLMSUserName, headerUserName);
                switch (method)
                {
                    case Method.GET:
                        respone = await httpClient.GetAsync(url);
                        respone.EnsureSuccessStatusCode();
                        var contentGet = await respone.Content.ReadAsStringAsync();
                        return _common.ConvertJSonToObjectV2<LMS.Common.Constants.ResponseActionResult>(contentGet);
                    case Method.POST:
                    default:
                        var requestContent = new StringContent("");
                        if (body != null)
                        {
                            var company = _common.ConvertObjectToJSonV2(body);
                            requestContent = new StringContent(company, Encoding.UTF8, "application/json");
                        }
                        respone = await httpClient.PostAsync(url, requestContent);
                        respone.EnsureSuccessStatusCode();
                        var contentPost = await respone.Content.ReadAsStringAsync();
                        return _common.ConvertJSonToObjectV2<LMS.Common.Constants.ResponseActionResult>(contentPost);
                }
            }
            catch (WebException e)
            {
                _logger.LogError($"ExecuteBaseAsync_WebException|url={url}|body={_common.ConvertObjectToJSonV2(body)}|statusCode={(int)e.Status}");
                throw e;
            }
            catch (Exception ex)
            {
                string statusCode = "";
                if (respone != null)
                {
                    statusCode = $"{respone.StatusCode}";
                }
                _logger.LogError($"ExecuteBaseAsync|url={url}|body={_common.ConvertObjectToJSonV2(body)}|statusCode={statusCode}|ex={ex.Message}-{ex.StackTrace}");
            }
            finally
            {
                //if (httpClient != null)
                //{
                //    httpClient.Dispose();
                //}
            }
            return default;
        }

        public void Dispose()
        {

        }
    }

}
