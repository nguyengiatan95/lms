﻿using LMS.Common.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Common.Helper
{
    public interface ICalculatorInterestManager
    {
        /// <summary>
        /// trả về tổng tiền lãi tính theo ngày,         
        /// </summary>
        /// <param name="totalMoney"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="ratePerDay">đơn vị số tiền/ngày </param>
        /// <returns></returns>
        long Calculator(long totalMoney, DateTime fromDate, DateTime toDate, decimal ratePerDay);
        /// <summary>
        /// trả về chi tiết số tiền lãi của thành phần
        /// </summary>
        /// <param name="totalMoneyInterest"></param>
        /// <param name="totalRatePerDay"></param>
        /// <param name="rateDetail"></param>
        /// <returns></returns>
        long Calculator(long totalMoneyInterest, decimal totalRatePerDay, decimal rateDetail);
    }
    public class CalculatorInterestManager : ICalculatorInterestManager
    {
        public long Calculator(long totalMoney, DateTime fromDate, DateTime toDate, decimal ratePerDay)
        {
            int totalDay = Convert.ToInt32((toDate - fromDate).Days + 1);
            return Convert.ToInt64(Math.Round(totalMoney * ratePerDay * totalDay / TimaSettingConstant.UnitMillionMoney, 0));
        }
        public long Calculator(long totalMoneyInterest, decimal totalRatePerDay, decimal rateDetail)
        {
            return Convert.ToInt64(Math.Round(totalMoneyInterest * rateDetail / totalRatePerDay, 0));
        }
    }
}
