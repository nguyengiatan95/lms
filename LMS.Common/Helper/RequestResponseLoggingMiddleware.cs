﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Common.Helper
{
    // refer: https://exceptionnotfound.net/using-middleware-to-log-requests-and-responses-in-asp-net-core/
    public class RequestResponseLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        ILogger<RequestResponseLoggingMiddleware> _logger;
        public RequestResponseLoggingMiddleware(RequestDelegate next, ILogger<RequestResponseLoggingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            //First, get the incoming request
            await FormatRequest(context, context.TraceIdentifier);

            //Copy a pointer to the original response body stream
            var originalBodyStream = context.Response.Body;

            //Create a new memory stream...
            using (var responseBody = new MemoryStream())
            {
                //...and use that for the temporary response body
                context.Response.Body = responseBody;

                var watch = System.Diagnostics.Stopwatch.StartNew();
                //Continue down the Middleware pipeline, eventually returning to this class
                await _next(context);
                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                //Format the response from the server
                await FormatResponse(context.Response, context.TraceIdentifier);
                _logger.LogInformation($"Request_Finished|traceIdentifier={context.TraceIdentifier}|path={context.Request.Path}|queryString={context.Request.QueryString}|time={elapsedMs}");

                //TODO: Save log to chosen datastore

                //Copy the contents of the new memory stream (which contains the response) to the original stream, which is then returned to the client.
                await responseBody.CopyToAsync(originalBodyStream);
            }
        }

        private async Task FormatRequest(HttpContext context, string traceIdentifier)
        {
            var body = context.Request.Body;

            //This line allows us to set the reader for the request back at the beginning of its stream.
            //request.EnableRewind();
            //BufferingHelper.EnableRewind(request);
            context.Request.EnableBuffering();

            //We now need to read the request stream.  First, we create a new byte[] with the same length as the request stream...
            var buffer = new byte[Convert.ToInt32(context.Request.ContentLength)];

            //...Then we copy the entire request stream into the new buffer.
            var bodyAsText = "";
            using (StreamReader reader = new StreamReader(context.Request.Body, Encoding.UTF8, true, 1024, true))
            {
                bodyAsText = await reader.ReadToEndAsync();
                //request.Body.Position = 0;
                context.Request.Body.Seek(0, SeekOrigin.Begin);
            }
            StringBuilder header = new StringBuilder();
            foreach (var item in context.Request.Headers)
            {
                header.Append($"Key:{item.Key}|Value:{item.Value}|");
            }
            //await request.Body.ReadAsync(buffer, 0, buffer.Length);

            ////We convert the byte[] into a string using UTF8 encoding...
            //var bodyAsText = Encoding.UTF8.GetString(buffer);

            ////..and finally, assign the read body back to the request body, which is allowed because of EnableRewind()
            //request.Body = body;
            //request.Body.Seek(0, SeekOrigin.Begin);
            //_logger.LogInformation($"Request_Start|Ip={context.Connection.RemoteIpAddress}|host={context.Request.Host}|path={context.Request.Path}|queryString={context.Request.QueryString}|body={bodyAsText}");
            _logger.LogInformation($"Request_Start|traceIdentifier={traceIdentifier}|Header={header}|path={context.Request.Path}|queryString={context.Request.QueryString}|body={bodyAsText}");
            //return $"{request.Scheme} {request.Host}{request.Path} {request.QueryString} {bodyAsText}";
        }

        private async Task FormatResponse(HttpResponse response, string traceIdentifier)
        {
            //We need to read the response stream from the beginning...
            response.Body.Seek(0, SeekOrigin.Begin);

            //...and copy it into a string
            string text = await new StreamReader(response.Body).ReadToEndAsync();

            //We need to reset the reader for the response so that the client can read it.
            response.Body.Seek(0, SeekOrigin.Begin);

            //Return the string for the response, including the status code (e.g. 200, 404, 401, etc.)
            //return $"{response.StatusCode}: {text}";
            _logger.LogInformation($"Request_Response|traceIdentifier={traceIdentifier}|StatusCode={response.StatusCode}|body={text}");
        }
    }
}
