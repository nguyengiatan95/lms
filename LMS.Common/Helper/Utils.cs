﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace LMS.Common.Helper
{
    public class Utils
    {
        public string DateTimeDDMMYYYY = "dd-MM-yyyy";
        public string DateTimeDDMMYYYYHHMMSS = "dd-MM-yyyy HH:mm:ss";
        public string DateTimeYYYYMMDD = "yyyyMMdd";
        public string DateTimeHHDDMMYYYY = "HH:mm dd-MM-yyyy";
        public string DateTimeDDMMYYYYHHMM = "dd/MM/yyyy HH:mm";

        public const string UnitTimeNameDate = "Ngày";
        public const string UnitTimeNameMonth = "Tháng";
        public const string DateTimeDayMonthYear = "dd/MM/yyyy";

        public string ConvertObjectToJSonV2<T>(T obj)
        {
            if (obj == null)
            {
                return "";
            }
            return JsonConvert.SerializeObject(obj, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, });
        }
        public T ConvertJSonToObjectV2<T>(string jsonString)
        {
            if (string.IsNullOrEmpty(jsonString))
            {
                return default;
            }
            return JsonConvert.DeserializeObject<T>(jsonString, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, MissingMemberHandling = MissingMemberHandling.Ignore, ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }
        public C ConvertParentToChild<C, P>(P parent)
        {
            string json = ConvertObjectToJSonV2<P>(parent);
            return ConvertJSonToObjectV2<C>(json);
        }
        public string HashMD5(string InputText)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider MD5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            if (string.IsNullOrEmpty(InputText.Trim()))
                return "";
            byte[] arrInput = null;
            arrInput = System.Text.UnicodeEncoding.UTF8.GetBytes(InputText);
            byte[] arrOutput = null;
            arrOutput = MD5.ComputeHash(arrInput);
            return Convert.ToBase64String(arrOutput);
        }

        public string MD5(string s)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(s));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
        public long CalculatorMoneyInterest(long totalMoney, int numberDay, decimal rate, int daysInYear = 365)
        {
            return Convert.ToInt64(Math.Round(totalMoney * 1.0M / daysInYear * rate / 100 * numberDay, 2));
        }
        public static string FirstCharToUpper(string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("ARGH!");
            return input.First().ToString().ToUpper() + input.Substring(1);
        }

        public long ConvertRateToVND(decimal rate)
        {
            return Convert.ToInt64(Math.Round(rate / 100M / Constants.TimaSettingConstant.NumberDayInYear * Constants.TimaSettingConstant.UnitMillionMoney, 2));
        }

        public decimal ConvertRateInterestToVND(float rate)
        {
            return Convert.ToDecimal(Math.Round(rate * Constants.TimaSettingConstant.UnitThousandMoney, 5));
        }
        public decimal ConvertRateInterestLenderToVND(float rate)
        {
            return Convert.ToDecimal(Math.Round(rate * Constants.TimaSettingConstant.UnitMillionMoney, 3));
        }
        public int ConvertLoanTimeLOSToLMS(int loanTimeLOS)
        {
            return loanTimeLOS * Constants.TimaSettingConstant.NumberDayInMonth;
        }
        public int ConvertFrequencyLOSToLMS(int frequencyLOS)
        {
            return frequencyLOS * Constants.TimaSettingConstant.NumberDayInMonth;
        }
        public decimal ConvertVNDToRate(long money)
        {
            return Convert.ToDecimal(Math.Round(money * 100M / Constants.TimaSettingConstant.UnitMillionMoney * Constants.TimaSettingConstant.NumberDayInYear, 2));
        }
        public string GenLoanContactCode(long loanCreditID, string contactCodeDefault = "0")
        {
            string contactCode = "0";
            if (!string.IsNullOrEmpty(contactCodeDefault))
                contactCode = contactCodeDefault;
            if (loanCreditID.ToString().Length > contactCode.Length)
            {
                return $"{Constants.TimaSettingConstant.PrefixContractCode}{loanCreditID}";
            }
            return $"{Constants.TimaSettingConstant.PrefixContractCode}{contactCode.Substring(0, (contactCode.Length - loanCreditID.ToString().Length))}{loanCreditID}";
        }
        public System.Collections.Hashtable GetPhoneAndIdCard(string content)
        {
            System.Collections.Hashtable ht = new System.Collections.Hashtable { { "Phone", "" }, { "IdCard", "" }, { "Other", "" } };
            try
            {
                if (!string.IsNullOrEmpty(content))
                {
                    // check 
                    var characters = content.Select(c => (char.IsLetterOrDigit(c)) ? c : '.').ToArray();
                    var s = characters.Where(x => char.IsDigit(x) || Convert.ToString(x) == ".").ToArray();
                    var output = new string(s);
                    long number1 = 0;
                    string[] words = output.Split('.').Where(x => x.Length > 0 && long.TryParse(x, out number1)).ToArray();
                    List<string> newList = new List<string>();
                    var cacheNuber = "";
                    var cacheSTT = 1;
                    foreach (string word in words)
                    {
                        if (word.Length > 5)
                        {
                            if (cacheSTT > 1)
                            {
                                newList.Add(cacheNuber);
                                newList.Add(word);
                            }
                            else
                            {
                                newList.Add(word);
                            }
                        }
                        else if (cacheSTT == 1)
                        {
                            cacheNuber = word;
                            cacheSTT++;
                        }
                        else
                        {
                            cacheNuber += word;
                            cacheSTT++;
                        }
                    }
                    foreach (string word in newList)
                    {
                        if (word.Length == 10 || word.Length == 11)
                        {
                            var phone = word;
                            if (word.StartsWith("84"))
                            {
                                phone = "0" + word.Substring(2, word.Length - 2);
                            }
                            ht["Phone"] = phone;
                        }
                        if (word.Length == 12 || word.Length == 9)
                        {
                            ht["IdCard"] = word;
                        }
                        if (word.Length != 12 && word.Length != 10 && word.Length != 9 && word.Length != 11)
                        {
                            ht["Other"] = word;
                        }
                    }
                }
            }
            catch
            {
            }
            return ht;
        }
        public string GetDescription(Enum value)
        {
            try
            {
                var da = (DescriptionAttribute[])(value.GetType().GetField(value.ToString())).GetCustomAttributes(typeof(DescriptionAttribute), false);
                return da.Length > 0 ? da[0].Description : value.ToString();
            }
            catch
            {
                return null;
            }
        }

        public string GenContractCodeBaoMinh(long LoanCreditIDOfPartner, string ContactCode)
        {
            return "HD-" + LoanCreditIDOfPartner + ContactCode;
        }
        public bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }

        public DateTime ConvertStringToDate(string strDate, string format)
        {
            DateTime result;
            try
            {
                IFormatProvider celture = new CultureInfo("fr-FR", true);

                var arrayDate = strDate.Split('-');
                if (arrayDate[0].Length < 2)
                {
                    arrayDate[0] = "0" + arrayDate[0];
                }
                if (arrayDate[1].Length < 2)
                {
                    arrayDate[1] = "0" + arrayDate[1];
                }
                strDate = arrayDate[0] + "-" + arrayDate[1] + "-" + arrayDate[2];
                result = DateTime.ParseExact(strDate, format, celture, DateTimeStyles.NoCurrentDateDefault);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool BeAValidDate(string strdate, string fomat)
        {
            try
            {
                DateTime dt = DateTime.ParseExact(strdate, fomat, CultureInfo.InvariantCulture);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string GetUnitTimeName(int rateType)
        {
            switch ((Constants.PaymentSchedule_ActionRateType)rateType)
            {
                case Constants.PaymentSchedule_ActionRateType.RateAmortization:
                case Constants.PaymentSchedule_ActionRateType.RateDay:
                    return UnitTimeNameDate;
                default:
                    return UnitTimeNameMonth;
            }
        }

        public string ConvertNullToEmpty(string str)
        {
            return str ?? "";
        }

        public string UnicodeToPlain(string strEncode)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = strEncode.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').ToUpper();
        }
        /// <summary>
        /// dùng để gen requestID
        /// </summary>
        /// <returns></returns>
        public string GenTraceIndentifier()
        {
            return $"{Guid.NewGuid()}";
        }

        public string HideNumberPhoneOrCardNumber(string number)
        {
            if (!string.IsNullOrEmpty(number))
            {
                string left = number.Substring(0, 3);
                string right = number.Substring(number.Length - 3, 3);

                return left + "*****" + right;
            }
            return number;

        }

        public long DateTimeToUnixTimesTampMilliseconds(DateTime fromDate)
        {
            long result = 0;
            try
            {
                result = (long)(fromDate.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
            }
            catch { result = 0; }
            return result;
        }

        public string ConvertToUnSign3(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }
        public bool CheckValidInputString<T>(T request, ref string message)
        {
            Type type = typeof(T);
            var properties = type.GetProperties();
            foreach (var item in properties)
            {
                object[] requireds = item.GetCustomAttributes(typeof(RequiredAttribute), false);
                if (requireds != null && requireds.Any())
                {
                    RequiredAttribute required = (RequiredAttribute)requireds[0];
                    if (item.PropertyType.Name.ToLower() == "string" && string.IsNullOrWhiteSpace(item.GetValue(request, null).ToString()))
                    {
                        message = required.ErrorMessage;
                        return false;
                    }
                }
            }
            return true;
        }

        public string GetJsonObjectChange<T>(T oldData, T newData)
        {
            List<PropertiesChange> lstChange = new List<PropertiesChange>();
            Type type = typeof(T);
            var properties = type.GetProperties();
            foreach (var item in properties)
            {
                var itemOld = item.GetValue(oldData, null);
                var itemNew = item.GetValue(newData, null);
                if ((itemOld == null ? "" : itemOld.ToString()) != (itemNew == null ? "" : itemNew.ToString()))
                {
                    lstChange.Add(new PropertiesChange()
                    {
                        Name = item.Name,
                        OldValue = itemOld.ToString(),
                        NewValue = itemNew.ToString(),
                    });
                }
            }
            return ConvertObjectToJSonV2(lstChange);
        }

        public T ConvertOldToNewObject<T>(T oldData) where T : new()
        {
            T newData = new T();
            Type type = typeof(T);
            var properties = type.GetProperties();
            foreach (var item in properties)
            {
                var itemOld = item.GetValue(oldData, null);
                item.SetValue(newData, itemOld);
            }
            return newData;
        }

        public string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        // Generate a random password of a given length (optional)  
        public string RandomPassword()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(RandomString(4, true));
            builder.Append(RandomNumber(10, 99));
            builder.Append(RandomString(2, false));
            return builder.ToString();
        }
        public int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }
        public DateTime ConvertTimeUnixToDatetime(long epoch)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(epoch).ToLocalTime();
        }
        public static bool IsMobileBrowser(string userAgent)
        {
            Regex b = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Regex v = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            if ((b.IsMatch(userAgent) || v.IsMatch(userAgent.Substring(0, 4))))
            {
                return true;
            }

            return false;
        }

        public string RemoveHtmlTag(string html)
        {
            if (string.IsNullOrEmpty(html))
            {
                return "";
            }
            string pattern = "(<([^>]+)>)";
            return Regex.Replace(html, pattern, "");
        }
    }
    public static class LinqExtensions
    {
        private static PropertyInfo GetPropertyInfo(Type objType, string name)
        {
            var properties = objType.GetProperties();
            var matchedProperty = properties.FirstOrDefault(p => p.Name == name);
            if (matchedProperty == null)
                throw new ArgumentException("name");

            return matchedProperty;
        }
        private static LambdaExpression GetOrderExpression(Type objType, PropertyInfo pi)
        {
            var paramExpr = Expression.Parameter(objType);
            var propAccess = Expression.PropertyOrField(paramExpr, pi.Name);
            var expr = Expression.Lambda(propAccess, paramExpr);
            return expr;
        }

        public static IEnumerable<T> OrderBy<T>(this IEnumerable<T> query, string name)
        {
            var propInfo = GetPropertyInfo(typeof(T), name);
            var expr = GetOrderExpression(typeof(T), propInfo);

            var method = typeof(Enumerable).GetMethods().FirstOrDefault(m => m.Name == "OrderBy" && m.GetParameters().Length == 2);
            var genericMethod = method.MakeGenericMethod(typeof(T), propInfo.PropertyType);
            return (IEnumerable<T>)genericMethod.Invoke(null, new object[] { query, expr.Compile() });
        }

        public static IQueryable<T> OrderBy<T>(this IQueryable<T> query, string name)
        {
            var propInfo = GetPropertyInfo(typeof(T), name);
            var expr = GetOrderExpression(typeof(T), propInfo);

            var method = typeof(Queryable).GetMethods().FirstOrDefault(m => m.Name == "OrderBy" && m.GetParameters().Length == 2);
            var genericMethod = method.MakeGenericMethod(typeof(T), propInfo.PropertyType);
            return (IQueryable<T>)genericMethod.Invoke(null, new object[] { query, expr });
        }

        public static IEnumerable<T> OrderByDescending<T>(this IEnumerable<T> query, string name)
        {
            var propInfo = GetPropertyInfo(typeof(T), name);
            var expr = GetOrderExpression(typeof(T), propInfo);

            var method = typeof(Enumerable).GetMethods().FirstOrDefault(m => m.Name == "OrderByDescending" && m.GetParameters().Length == 2);
            var genericMethod = method.MakeGenericMethod(typeof(T), propInfo.PropertyType);
            return (IEnumerable<T>)genericMethod.Invoke(null, new object[] { query, expr.Compile() });
        }

        public static IQueryable<T> OrderByDescending<T>(this IQueryable<T> query, string name)
        {
            var propInfo = GetPropertyInfo(typeof(T), name);
            var expr = GetOrderExpression(typeof(T), propInfo);

            var method = typeof(Queryable).GetMethods().FirstOrDefault(m => m.Name == "OrderByDescending" && m.GetParameters().Length == 2);
            var genericMethod = method.MakeGenericMethod(typeof(T), propInfo.PropertyType);
            return (IQueryable<T>)genericMethod.Invoke(null, new object[] { query, expr });
        }

        public static string GetDescription(this Enum value)
        {
            try
            {
                var da = (DescriptionAttribute[])(value.GetType().GetField(value.ToString())).GetCustomAttributes(typeof(DescriptionAttribute), false);
                return da.Length > 0 ? da[0].Description : value.ToString();
            }
            catch
            {
                return "";
            }
        }
        public static IEnumerable<IEnumerable<TSource>> Batch<TSource>(this IEnumerable<TSource> source, int size)
        {
            TSource[] bucket = null;
            var count = 0;

            foreach (var item in source)
            {
                if (bucket == null)
                    bucket = new TSource[size];

                bucket[count++] = item;
                if (count != size)
                    continue;

                yield return bucket;

                bucket = null;
                count = 0;
            }

            if (bucket != null && count > 0)
                yield return bucket.Take(count).ToArray();
        }

    }
    public class PropertiesChange
    {
        public string Name { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
    }
}
