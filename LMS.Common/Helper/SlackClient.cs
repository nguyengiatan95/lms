﻿using LMS.Common.Constants;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace LMS.Common.Helper
{
    public class SlackClient
    {
        public SlackClient() { }
        public SlackClient(string title)
        {
            Title = title;
        }
        public string Title { get; set; }
        private string GetTitle(string titleReplace)
        {
            if (!string.IsNullOrEmpty(Title))
            {
                return Title;
            }
            return titleReplace;
        }

        [Obsolete]
        private void PostMessage(SlackSMS item, string urlApi)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | (SecurityProtocolType)768 | (SecurityProtocolType)3072;
                var slackApi = SlackKeyGroup.Domain;
                var client = new RestClient(slackApi);
                var request = new RestRequest(urlApi);
                request.Method = Method.POST;
                request.AddHeader("Content-Type", "application/json");
                var body = JsonConvert.SerializeObject(item);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = client.Execute(request);
                var content = response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Obsolete]
        public void SendNotifyToAccountGroup(string mess, string title = SlackTitleMessage.ApiMecash)
        {
            try
            {
                var urlApi = SlackKeyGroup.GroupAccountant;
                var objSms = new SlackSMS();
                objSms.mrkdwn = true;
                objSms.text = String.Empty;
                objSms.attachments = new List<SlackAttachments>();

                SlackAttachments itemAttack = new SlackAttachments()
                {
                    author_name = "Hệ thống",
                    color = "blue",
                    pretext = "",
                    title = GetTitle(title),
                    text = mess
                };
                objSms.attachments.Add(itemAttack);
                PostMessage(objSms, urlApi);
            }
            catch
            {
            }
        }

        [Obsolete]
        public void SendNotifyForDev(string mess, string title = SlackTitleMessage.ApiMecash)
        {
            try
            {
                //SlackAPIUrlNotiForDev
                var urlApi = SlackKeyGroup.GroupDev;
                var objSms = new SlackSMS();
                objSms.mrkdwn = true;
                objSms.text = String.Empty;
                objSms.attachments = new List<SlackAttachments>();

                SlackAttachments itemAttack = new SlackAttachments()
                {
                    author_name = "Hệ thống",
                    color = "blue",
                    pretext = "",
                    title = GetTitle(title),
                    text = mess
                };
                objSms.attachments.Add(itemAttack);
                PostMessage(objSms, urlApi);
            }
            catch
            {
            }
        }

        [Obsolete]
        public void SendNotify(string mess, string urlApi, string title)
        {
            try
            {
                var objSms = new SlackSMS();
                objSms.mrkdwn = true;
                objSms.text = String.Empty;
                objSms.attachments = new List<SlackAttachments>();

                SlackAttachments itemAttack = new SlackAttachments()
                {
                    author_name = "Hệ thống",
                    color = "blue",
                    pretext = "",
                    title = GetTitle(title),
                    text = mess
                };
                objSms.attachments.Add(itemAttack);
                PostMessage(objSms, urlApi);
            }
            catch
            {
            }
        }

    }
}
