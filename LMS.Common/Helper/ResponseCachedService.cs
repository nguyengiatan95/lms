﻿using LMS.Common.Configuration;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Common.Helper
{
    public interface ICacheService
    {
        Task CachedResponseAsync(string cachedKey, object response, TimeSpan timeTimeLive);
        Task<string> GetCachedResponseAsync(string cachedkey);
        Task<T> Get<T>(string key);
        Task<T> Set<T>(string key, T value, TimeSpan timeTimeLive);
        Task<long> GetOrSetKeyUniqueBigID(string key, long value, TimeSpan timeLive);
    }
    public class CacheService : ICacheService
    {
        IDistributedCache _distributedCache;
        public CacheService(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }
        public async Task CachedResponseAsync(string cachedKey, object response, TimeSpan timeTimeLive)
        {
            if (response == null)
            {
                return;
            }
            var serializeResponse = JsonConvert.SerializeObject(response);
            await _distributedCache.SetStringAsync(cachedKey, serializeResponse, new DistributedCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = timeTimeLive
            });
        }

        public async Task<string> GetCachedResponseAsync(string cachedkey)
        {
            var cachedResponse = await _distributedCache.GetStringAsync(cachedkey);
            return string.IsNullOrEmpty(cachedResponse) ? null : cachedResponse;
        }
        public async Task<T> Get<T>(string key)
        {
            var value = await _distributedCache.GetStringAsync(key);

            if (value != null)
            {
                return JsonConvert.DeserializeObject<T>(value);
            }

            return default;
        }

        public async Task<T> Set<T>(string key, T value, TimeSpan timeTimeLive)
        {
            var options = new DistributedCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = timeTimeLive
            };

            await _distributedCache.SetStringAsync(key, JsonConvert.SerializeObject(value), options);

            return value;
        }

        public async Task<long> GetOrSetKeyUniqueBigID(string key, long value, TimeSpan timeLive)
        {
            var bigID = await this.Get<long>(key);
            if (bigID == 0)
            {
                return bigID;
            }
            bigID++;
            await this.Set(key, bigID, timeLive);
            return bigID;
        }
    }

    public static class InstallCacheManager
    {
        public static void AddRedisCacheService(this IServiceCollection services, IConfiguration configuration)
        {
            try
            {
                var redisCacheSettings = new RedisCacheSetting();
                configuration.GetSection(nameof(RedisCacheSetting)).Bind(redisCacheSettings);
                services.AddSingleton(redisCacheSettings);
                if (!redisCacheSettings.Enabled)
                {
                    return;
                }
                services.AddStackExchangeRedisCache(op =>
                {
                    op.Configuration = redisCacheSettings.ConnectionString;
                });
                services.AddSingleton<ICacheService, CacheService>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
