﻿using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Sinks.Elasticsearch;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Common.Helper
{
    public class ElasticSearchConfigureLogging
    {
        public static void ConfigureLogging()
        {
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var appseting = "appsettings.json";
            if (!string.IsNullOrEmpty(environment))
            {
                appseting = $"appsettings.{environment}.json";
            }
            Console.WriteLine($"appsetting: {appseting}");
            
            var configuration = new ConfigurationBuilder()
                .AddJsonFile(appseting, optional: false, reloadOnChange: true)
                //.AddJsonFile($"appsettings.{environment}.json", optional: true)
                .Build();

            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .Enrich.WithMachineName()
                //.WriteTo.Debug()
                .WriteTo.Console()
                //.WriteTo.Elasticsearch(ConfigureElasticSink(configuration, environment))
                //.WriteTo.File("./Logs/log.txt", rollingInterval: RollingInterval.Day, retainedFileCountLimit: null, fileSizeLimitBytes: null, shared: true)
                //.Enrich.WithProperty("Environment", environment)
                //.ReadFrom.Configuration(configuration)
                .CreateLogger();
        }

        private static ElasticsearchSinkOptions ConfigureElasticSink(IConfigurationRoot configuration, string environment)
        {
            var appSettingsSection = configuration.GetSection("ElasticConfiguration").Get<LMS.Common.Configuration.ElasticConfiguration>();
            var lstUriNodes = appSettingsSection.Urls.Split(",");
            List<Uri> nodes = new List<Uri>();
            foreach (var item in lstUriNodes)
            {
                nodes.Add(new Uri(item));
            }
            return new ElasticsearchSinkOptions(nodes)
            {
                AutoRegisterTemplate = true,
                IndexFormat = appSettingsSection.IndexFormat,
                NumberOfShards = appSettingsSection.NumberOfShards,
                NumberOfReplicas = appSettingsSection.NumberOfReplicas
            };
        }
    }
}
