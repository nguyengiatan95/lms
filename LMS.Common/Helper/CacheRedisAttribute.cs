﻿using LMS.Common.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace LMS.Common.Helper
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CacheRedisAttribute : Attribute, IAsyncActionFilter
    {
        int _timeToLiveSecond;
        public CacheRedisAttribute(int timeToLiveSecond)
        {
            _timeToLiveSecond = timeToLiveSecond;
        }
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var cachedSetting = context.HttpContext.RequestServices.GetRequiredService<RedisCacheSetting>();
            if (!cachedSetting.Enabled)
            {
                await next();
                return;
            }
            var cachedService = context.HttpContext.RequestServices.GetRequiredService<ICacheService>();
            var cachedKey = GenerateCachKeyFromRequest(context.HttpContext.Request);
            var cachedResponse = await cachedService.GetCachedResponseAsync(cachedKey);

            if (!string.IsNullOrEmpty(cachedResponse))
            {
                var contentResult = new Microsoft.AspNetCore.Mvc.ContentResult
                {
                    Content = cachedResponse,
                    ContentType = "application/json",
                    StatusCode = StatusCodes.Status200OK
                };
                context.Result = contentResult;
                return;
            }
            var excecutedContext = await next();
            if (excecutedContext.Result is Microsoft.AspNetCore.Mvc.ObjectResult obResult)
            {
                if (obResult.Value is Constants.ResponseActionResult responseAction)
                    if (responseAction.Result == (int)Constants.ResponseAction.Success)
                        await cachedService.CachedResponseAsync(cachedKey, responseAction, TimeSpan.FromSeconds(_timeToLiveSecond));
            }
        }

        private string GenerateCachKeyFromRequest(HttpRequest request)
        {
            var keyBuilder = new StringBuilder();
            keyBuilder.Append($"{request.Path}");
            foreach (var (key, value) in request.Query.OrderBy(x => x.Key))
            {
                keyBuilder.Append($"|{key}-{value}");
            }
            return keyBuilder.ToString();
        }
    }
}
