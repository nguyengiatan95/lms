﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;

namespace LMS.Common.DAL
{
    // refer https://csharpindepth.com/articles/singleton
    public class BulkOperationsHelpers
    {

        private string TempTable = "#TempForBulkInsertUpdate";
        private const string _schema = "dbo";
        private const string _sourceAlias = "Source";
        private const string _targetAlias = "Target";
        private const int _bulkCopyTimeout = 600;
        private const int _bulkCopyBatchSize = 2000;
        private const int _sqlTimeout = 600;

        private static readonly Lazy<BulkOperationsHelpers> lazy = new Lazy<BulkOperationsHelpers>(() => new BulkOperationsHelpers());
        public static BulkOperationsHelpers Instance { get { return lazy.Value; } }
        private BulkOperationsHelpers()
        {
        }

        public void InsertBulk<T>(string connectstring, IEnumerable<T> list, string tableName, string sqlDeleteDataOld = "", bool setOffIdentity = false)
        {
            var _columns = GetAllValueTypeAndStringColumns(typeof(T));
            var _sqlBulkCopyOptions = SqlBulkCopyOptions.Default;
            var _customColumnMappings = new Dictionary<string, string>();
            var _bulkCopyEnableStreaming = false;
            int? _bulkCopyNotifyAfter = null;
            var _disableAllIndexes = false;
            var _disableIndexList = new HashSet<string>();
            var _updateOnList = new List<string>();
            DataTable dt = ToDataTable(list, _columns, _customColumnMappings);
            // Must be after ToDataTable is called. 
            DoColumnMappings(_customColumnMappings, _columns, _updateOnList);
            if (setOffIdentity)
            {
                _sqlBulkCopyOptions = SqlBulkCopyOptions.KeepIdentity;
            }
            using (var conn = new SqlConnection(connectstring))
            {

                conn.Open();

                using (SqlTransaction transaction = conn.BeginTransaction())
                {
                    //Bulk insert into temp table
                    using (SqlBulkCopy bulkcopy = new SqlBulkCopy(conn, _sqlBulkCopyOptions, transaction))
                    {
                        try
                        {
                            bulkcopy.DestinationTableName = GetFullQualifyingTableName(conn.Database, _schema, tableName);
                            MapColumns(bulkcopy, _columns, _customColumnMappings);

                            SetSqlBulkCopySettings(bulkcopy, _bulkCopyEnableStreaming, _bulkCopyBatchSize,
                                _bulkCopyNotifyAfter, _bulkCopyTimeout);

                            SqlCommand command = conn.CreateCommand();

                            command.Connection = conn;
                            command.Transaction = transaction;

                            if (_disableAllIndexes || (_disableIndexList != null && _disableIndexList.Any()))
                            {
                                command.CommandText = GetIndexManagementCmd(IndexOperation.Disable, tableName, _disableIndexList, _disableAllIndexes);
                                command.ExecuteNonQuery();
                            }

                            if (!string.IsNullOrEmpty(sqlDeleteDataOld))
                            {
                                command.CommandText = sqlDeleteDataOld;
                                command.ExecuteNonQuery();
                            }
                            bulkcopy.WriteToServer(dt);

                            if (_disableAllIndexes || (_disableIndexList != null && _disableIndexList.Any()))
                            {
                                command.CommandText = GetIndexManagementCmd(IndexOperation.Rebuild, tableName, _disableIndexList, _disableAllIndexes);
                                command.ExecuteNonQuery();
                            }

                            transaction.Commit();

                            bulkcopy.Close();
                        }

                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();

                        }
                    }
                }
            }
        }
        public void UpdateBulk<T>(string connectstring, IEnumerable<T> list, string tableName, string privateKeyName)
        {
            var _columns = GetAllValueTypeAndStringColumns(typeof(T));
            var _sqlBulkCopyOptions = SqlBulkCopyOptions.Default;
            var _customColumnMappings = new Dictionary<string, string>();
            var _bulkCopyEnableStreaming = false;
            int? _bulkCopyNotifyAfter = null;
            var _disableAllIndexes = false;
            var _disableIndexList = new HashSet<string>();
            var _matchTargetOn = new List<string>() { privateKeyName };
            var _identityColumn = privateKeyName;
            DataTable dt = ToDataTable(list, _columns, _customColumnMappings, _matchTargetOn);

            // set name temp table
            TempTable = $"#TempForBulkInsertUpdate_{tableName}";

            // Must be after ToDataTable is called. 
            DoColumnMappings(_customColumnMappings, _columns, _matchTargetOn);
            using (var conn = new SqlConnection(connectstring))
            {
                conn.Open();
                var dtCols = GetDatabaseSchema(conn, _schema, tableName);

                using (SqlTransaction transaction = conn.BeginTransaction())
                {
                    try
                    {
                        SqlCommand command = conn.CreateCommand();
                        command.Connection = conn;
                        command.Transaction = transaction;
                        command.CommandTimeout = _sqlTimeout;

                        //Creating temp table on database
                        command.CommandText = BuildCreateTempTable(_columns, dtCols);
                        command.ExecuteNonQuery();

                        //Bulk insert into temp table
                        InsertToTmpTable(conn, transaction, dt, _bulkCopyEnableStreaming, _bulkCopyBatchSize,
                            _bulkCopyNotifyAfter, _bulkCopyTimeout, _sqlBulkCopyOptions);

                        // Updating destination table, and dropping temp table
                        if (_disableIndexList != null && _disableIndexList.Any())
                        {
                            command.CommandText = GetIndexManagementCmd(IndexOperation.Disable, tableName, _disableIndexList, _disableAllIndexes);
                            command.ExecuteNonQuery();
                        }

                        string comm = "MERGE INTO " + GetFullQualifyingTableName(conn.Database, _schema, tableName) + " WITH (HOLDLOCK) AS Target " +
                                      $"USING {TempTable} AS Source " +
                                      BuildJoinConditionsForUpdateOrInsert(_matchTargetOn.ToArray(),
                                          _sourceAlias, _targetAlias) +
                                      "WHEN MATCHED THEN " +
                                      BuildUpdateSet(_columns, _sourceAlias, _targetAlias, _identityColumn) +
                                      $"; DROP TABLE {TempTable};";
                        command.CommandText = comm;
                        command.ExecuteNonQuery();

                        if (_disableIndexList != null && _disableIndexList.Any())
                        {
                            command.CommandText = GetIndexManagementCmd(IndexOperation.Rebuild, tableName, _disableIndexList);
                            command.ExecuteNonQuery();
                        }

                        transaction.Commit();
                    }


                    catch (SqlException e)
                    {
                        for (int i = 0; i < e.Errors.Count; i++)
                        {
                            // Error 8102 is identity error. 
                            if (e.Errors[i].Number == 8102)
                            {
                                // Expensive call but neccessary to inform user of an important configuration setup. 
                                throw new IdentityException(e.Errors[i].Message);
                            }
                        }
                        transaction.Rollback();
                        throw;
                    }

                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }

                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        #region internal
        internal DataTable GetDatabaseSchema(SqlConnection conn, string schema, string tableName)
        {
            string[] restrictions = new string[4];
            restrictions[0] = conn.Database;
            restrictions[1] = schema;
            restrictions[2] = tableName;
            var dtCols = conn.GetSchema("Columns", restrictions);

            if (dtCols.Rows.Count == 0 && schema != null) throw new InvalidOperationException("Table name '" + tableName + "\' with schema name \'" + schema + "\' not found. Check your setup and try again.");
            if (dtCols.Rows.Count == 0) throw new InvalidOperationException("Table name \'" + tableName + "\' not found. Check your setup and try again.");
            return dtCols;
        }

        /// <summary>
        /// This is used only for the BulkInsert method at this time.  
        /// </summary>
        /// <param name="bulkCopy"></param>
        /// <param name="columns"></param>
        /// <param name="customColumnMappings"></param>
        internal void MapColumns(SqlBulkCopy bulkCopy, HashSet<string> columns, Dictionary<string, string> customColumnMappings)
        {

            foreach (var column in columns.ToList())
            {
                string mapping;

                if (customColumnMappings.TryGetValue(column, out mapping))
                {
                    bulkCopy.ColumnMappings.Add(mapping, mapping);
                }

                else
                    bulkCopy.ColumnMappings.Add(column, column);
            }

        }
        internal void DoColumnMappings(Dictionary<string, string> columnMappings, HashSet<string> columns, List<string> updateOnList)
        {
            if (columnMappings.Count > 0)
            {
                foreach (var column in columnMappings)
                {
                    if (columns.Contains(column.Key))
                    {
                        columns.Remove(column.Key);
                        columns.Add(column.Value);
                    }

                    for (int i = 0; i < updateOnList.ToArray().Length; i++)
                    {
                        if (updateOnList[i] == column.Key)
                        {
                            updateOnList[i] = column.Value;
                        }
                    }
                }
            }
        }

        internal string GetIndexManagementCmd(string action, string table, HashSet<string> disableIndexList, bool disableAllIndexes = false)
        {
            //AND sys.objects.name = 'Books' AND sys.indexes.name = 'IX_Title'
            StringBuilder sb = new StringBuilder();

            if (disableIndexList != null && disableIndexList.Any())
            {
                foreach (var index in disableIndexList)
                {
                    sb.Append(" AND sys.indexes.name = \'");
                    sb.Append(index);
                    sb.Append("\'");
                }
            }

            string cmd = "DECLARE @sql AS VARCHAR(MAX)=''; " +
                                "SELECT @sql = @sql + " +
                                "'ALTER INDEX ' + sys.indexes.name + ' ON ' + sys.objects.name + ' " + action + ";'" +
                                "FROM sys.indexes JOIN sys.objects ON sys.indexes.object_id = sys.objects.object_id " +
                                "WHERE sys.indexes.type_desc = 'NONCLUSTERED' " +
                                "AND sys.objects.type_desc = 'USER_TABLE'" +
                                " AND sys.objects.name = '" + table + "'" + (sb.Length > 0 ? sb.ToString() : "") + "; EXEC(@sql);";

            return cmd;
        }
        internal string BuildUpdateSet(HashSet<string> columns, string sourceAlias, string targetAlias, string identityColumn)
        {
            StringBuilder command = new StringBuilder();
            List<string> paramsSeparated = new List<string>();

            command.Append("UPDATE SET ");

            foreach (var column in columns.ToList())
            {
                if (identityColumn != null && column != identityColumn || identityColumn == null)
                {
                    if (column != "InternalId")
                        paramsSeparated.Add("[" + targetAlias + "]" + "." + "[" + column + "]" + " = " + "[" + sourceAlias + "]" + "." + "[" + column + "]");
                }
            }

            command.Append(string.Join(", ", paramsSeparated) + " ");

            return command.ToString();
        }
        internal string BuildJoinConditionsForUpdateOrInsert(string[] updateOn, string sourceAlias, string targetAlias)
        {
            StringBuilder command = new StringBuilder();

            command.Append("ON " + "[" + targetAlias + "]" + "." + "[" + updateOn[0] + "]" + " = " + "[" + sourceAlias + "]" + "." + "[" + updateOn[0] + "]" + " ");

            if (updateOn.Length > 1)
            {
                // Start from index 1 to just append "AND" conditions
                for (int i = 1; i < updateOn.Length; i++)
                {
                    command.Append("AND " + "[" + targetAlias + "]" + "." + "[" + updateOn[i] + "]" + " = " + "[" + sourceAlias + "]" + "." + "[" + updateOn[i] + "]" + " ");
                }
            }

            return command.ToString();
        }
        internal string GetFullQualifyingTableName(string databaseName, string schemaName, string tableName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            sb.Append(databaseName);
            sb.Append("].[");
            sb.Append(schemaName);
            sb.Append("].[");
            sb.Append(tableName);
            sb.Append("]");

            return sb.ToString();
        }
        internal HashSet<string> GetAllValueTypeAndStringColumns(Type type)
        {
            HashSet<string> columns = new HashSet<string>();

            //Get all the properties
            PropertyInfo[] props = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            for (int i = 0; i < props.Length; i++)
            {
                var type2 = props[i].GetType();
                if (props[i].PropertyType.IsValueType || props[i].PropertyType == typeof(string))
                {
                    columns.Add(props[i].Name);
                }
            }

            return columns;

        }
        internal DataTable ToDataTable<T>(IEnumerable<T> items, HashSet<string> columns, Dictionary<string, string> columnMappings, List<string> matchOnColumns = null, bool? outputIdentity = null, Dictionary<int, T> outputIdentityDic = null)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            if (matchOnColumns != null)
            {
                columns = CheckForAdditionalColumns(columns, matchOnColumns);
            }

            if (outputIdentity.HasValue && outputIdentity.Value)
            {
                columns.Add("InternalId");
            }

            //Get all the properties
            PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var column in columns.ToList())
            {
                if (columnMappings.ContainsKey(column))
                {
                    dataTable.Columns.Add(columnMappings[column]);
                }

                else
                    dataTable.Columns.Add(column);
            }

            AssignTypes(props, columns, dataTable, outputIdentity);

            int counter = 0;

            foreach (T item in items)
            {

                var values = new List<object>();

                foreach (var column in columns.ToList())
                {
                    if (column == "InternalId")
                    {
                        values.Add(counter);
                        outputIdentityDic.Add(counter, item);
                    }
                    else
                        for (int i = 0; i < props.Length; i++)
                        {
                            if (props[i].Name == column && item != null)
                                values.Add(props[i].GetValue(item, null));
                        }

                }
                counter++;
                dataTable.Rows.Add(values.ToArray());

            }
            return dataTable;
        }
        internal HashSet<string> CheckForAdditionalColumns(HashSet<string> columns, List<string> matchOnColumns)
        {
            foreach (var col in matchOnColumns)
            {
                if (!columns.Contains(col))
                {
                    columns.Add(col);
                }
            }

            return columns;
        }

        private void AssignTypes(PropertyInfo[] props, HashSet<string> columns, DataTable dataTable, bool? outputIdentity = null)
        {
            int count = 0;

            foreach (var column in columns.ToList())
            {
                if (column == "InternalId")
                {
                    dataTable.Columns[count].DataType = typeof(int);
                }
                else
                    for (int i = 0; i < props.Length; i++)
                    {
                        if (props[i].Name == column)
                        {
                            dataTable.Columns[count].DataType = Nullable.GetUnderlyingType(props[i].PropertyType) ??
                                                                props[i].PropertyType;
                        }
                    }
                count++;
            }
        }
        internal string BuildCreateTempTable(HashSet<string> columns, DataTable schema, bool? outputIdentity = null)
        {
            Dictionary<string, string> actualColumns = new Dictionary<string, string>();
            Dictionary<string, string> actualColumnsMaxCharLength = new Dictionary<string, string>();
            Dictionary<string, PrecisionType> actualColumnsPrecision = new Dictionary<string, PrecisionType>();


            foreach (DataRow row in schema.Rows)
            {
                string columnType = row["DATA_TYPE"].ToString();
                string columnName = row["COLUMN_NAME"].ToString();

                actualColumns.Add(row["COLUMN_NAME"].ToString(), row["DATA_TYPE"].ToString());

                if (columnType == "varchar" || columnType == "nvarchar" ||
                    columnType == "char" || columnType == "binary" ||
                    columnType == "varbinary")

                {
                    actualColumnsMaxCharLength.Add(row["COLUMN_NAME"].ToString(),
                        row["CHARACTER_MAXIMUM_LENGTH"].ToString());
                }

                if (columnType == "numeric" || columnType == "decimal")
                {
                    PrecisionType p = new PrecisionType
                    {
                        NumericPrecision = row["NUMERIC_PRECISION"].ToString(),
                        NumericScale = row["NUMERIC_SCALE"].ToString()
                    };
                    actualColumnsPrecision.Add(columnName, p);
                }

            }

            StringBuilder command = new StringBuilder();

            command.Append($"CREATE TABLE {TempTable}(");

            List<string> paramList = new List<string>();

            foreach (var column in columns.ToList())
            {
                if (column == "InternalId")
                    continue;
                string columnType;
                if (actualColumns.TryGetValue(column, out columnType))
                {
                    columnType = GetVariableCharType(column, columnType, actualColumnsMaxCharLength);
                    columnType = GetDecimalPrecisionAndScaleType(column, columnType, actualColumnsPrecision);
                }

                paramList.Add("[" + column + "]" + " " + columnType);
            }

            string paramListConcatenated = string.Join(", ", paramList);

            command.Append(paramListConcatenated);

            if (outputIdentity.HasValue && outputIdentity.Value)
            {
                command.Append(", [InternalId] int");
            }
            command.Append(");");

            return command.ToString();
        }
        private string GetVariableCharType(string column, string columnType, Dictionary<string, string> actualColumnsMaxCharLength)
        {
            if (columnType == "varchar" || columnType == "nvarchar")
            {
                string maxCharLength;
                if (actualColumnsMaxCharLength.TryGetValue(column, out maxCharLength))
                {
                    if (maxCharLength == "-1")
                        maxCharLength = "max";

                    columnType = columnType + "(" + maxCharLength + ")";
                }
            }

            return columnType;
        }
        private string GetDecimalPrecisionAndScaleType(string column, string columnType, Dictionary<string, PrecisionType> actualColumnsPrecision)
        {
            if (columnType == "decimal" || columnType == "numeric")
            {
                PrecisionType p;

                if (actualColumnsPrecision.TryGetValue(column, out p))
                {
                    columnType = columnType + "(" + p.NumericPrecision + ", " + p.NumericScale + ")";
                }
            }

            return columnType;
        }

        internal void InsertToTmpTable(SqlConnection conn, SqlTransaction transaction, DataTable dt, bool bulkCopyEnableStreaming, int? bulkCopyBatchSize, int? bulkCopyNotifyAfter, int bulkCopyTimeout, SqlBulkCopyOptions sqlBulkCopyOptions)
        {
            using (SqlBulkCopy bulkcopy = new SqlBulkCopy(conn, sqlBulkCopyOptions, transaction))
            {
                bulkcopy.DestinationTableName = TempTable;

                SetSqlBulkCopySettings(bulkcopy, bulkCopyEnableStreaming,
                    bulkCopyBatchSize,
                    bulkCopyNotifyAfter, bulkCopyTimeout);

                bulkcopy.WriteToServer(dt);
            }
        }

        /// <summary>
        /// Advanced Settings for SQLBulkCopy class. 
        /// </summary>
        /// <param name="bulkcopy"></param>
        /// <param name="bulkCopyEnableStreaming"></param>
        /// <param name="bulkCopyBatchSize"></param>
        /// <param name="bulkCopyNotifyAfter"></param>
        /// <param name="bulkCopyTimeout"></param>
        internal void SetSqlBulkCopySettings(SqlBulkCopy bulkcopy, bool bulkCopyEnableStreaming, int? bulkCopyBatchSize, int? bulkCopyNotifyAfter, int bulkCopyTimeout)
        {
            bulkcopy.EnableStreaming = bulkCopyEnableStreaming;

            if (bulkCopyBatchSize.HasValue)
            {
                bulkcopy.BatchSize = bulkCopyBatchSize.Value;
            }

            if (bulkCopyNotifyAfter.HasValue)
            {
                bulkcopy.NotifyAfter = bulkCopyNotifyAfter.Value;
            }

            bulkcopy.BulkCopyTimeout = bulkCopyTimeout;
        }
        internal struct PrecisionType
        {
            public string NumericPrecision { get; set; }
            public string NumericScale { get; set; }
        }
        internal static class IndexOperation
        {
            public const string Rebuild = "REBUILD";
            public const string Disable = "DISABLE";
        }
        internal class IdentityException : Exception
        {
            public IdentityException(string message) : base(message + " SQLBulkTools requires the SetIdentityColumn method " +
                                                                "to be configured if an identity column is being used. Please reconfigure your setup and try again.")
            {
            }
        }
        #endregion
    }
}
