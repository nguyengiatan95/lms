﻿using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Common.DAL
{
    public interface ITableHelper<T> where T : class
    {
        TableHelper<T> SetConnectString(string connect);
        TableHelper<T> SetGetTop(int top);
        TableHelper<T> SelectColumns(params Expression<Func<T, object>>[] columnNames);
        TableHelper<T> GetDistinct();
        IEnumerable<T> Query();
        Task<IEnumerable<T>> QueryAsync();
        IEnumerable<TResult> Query<TResult>();
        IEnumerable<T> Query(string strconnect);
        IEnumerable<T> Query(string sql, Dapper.DynamicParameters parameters);
        TableHelper<T> WhereClause(params Expression<Func<T, bool>>[] columnNames);
        TableHelper<T> WhereClauseOR(params Expression<Func<T, bool>>[] columnNames);

        TableHelper<T> JoinOn<TInner>(Expression<Func<T, object>> colA, Expression<Func<TInner, object>> colB, int typeJoin = 0);
        TableHelper<T> SelectColumnsJoinOn<TInner>(params Expression<Func<TInner, object>>[] columnNames);
        TableHelper<T> OrderBy(params Expression<Func<T, object>>[] columnNames);
        TableHelper<T> OrderByDescending(params Expression<Func<T, object>>[] columnNames);
        TableHelper<T> SetMax(params Expression<Func<T, object>>[] columnNames);
        TableHelper<T> SetMin(params Expression<Func<T, object>>[] columnNames);
        TableHelper<T> GroupBy(params Expression<Func<T, object>>[] columnNames);
        long Insert(T entity, bool setOffIdentity = false);
        Task<long> InsertAsync(T entity, bool setOffIdentity = false);
        bool Update(T entity);
        Task<bool> UpdateAsync(T entity);
        void InsertBulk(IEnumerable<T> list, string tableName = "", string sqlDeleteDataOld = "", bool setOffIdentity = false);
        void UpdateBulk(IEnumerable<T> list, string tableName = "", string privateKeyName = "");
        T GetByID(long ID);
        Task<T> GetByIDAsync(long ID);
        bool DeleteList(List<T> entity);
        Task<bool> DeleteListAsync(List<T> entity);
        bool Delete(T entity);
        Task<bool> DeleteAsync(T entity);
        int Execute(string sql);

        bool DeleteWhere(params Expression<Func<T, bool>>[] columnNames);
        Task<bool> DeleteWhereAsync(params Expression<Func<T, bool>>[] columnNames);
        Task<IEnumerable<T>> QueryAsync(string sql, Dapper.DynamicParameters parameters);
        Task<IEnumerable<TResult>> QueryAsync<TResult>();
        TableHelper<T> Clone();
    }

    public class TableHelper<T> : IDisposable, ITableHelper<T> where T : class
    {
        List<string> _columns = new List<string>();
        List<WherePart> _whereParts = new List<WherePart>();
        List<WherePart> _orWhereParts = new List<WherePart>();
        string _sqlSelect = "";
        string _sqlWhere = "";
        string _tableName = "";
        string _privateKeyName = "";
        int _top = 0;
        int _minRowUseBulk = 100;
        string _sqlJoin = "";
        List<string> _orderByColumns = new List<string>();
        bool _isDistinct = false;
        private string _connectString;
        List<string> _lstMaxColumns = new List<string>();
        List<string> _lstMinColumns = new List<string>();
        List<string> _groupByColumns = new List<string>();
        ILogger<TableHelper<T>> _logger;
        public TableHelper()
        {

        }
        public TableHelper<T> Clone()
        {
            return new TableHelper<T>();
        }
        public TableHelper(Microsoft.Extensions.Logging.ILogger<TableHelper<T>> logger)
        {
            _logger = logger;
        }
        public TableHelper<T> SetConnectString(string connect)
        {
            _connectString = connect;
            return this;
        }
        public TableHelper<T> GetDistinct()
        {
            _isDistinct = true;
            return this;
        }
        public TableHelper<T> SetGetTop(int top)
        {
            _top = top;
            return this;
        }
        public TableHelper<T> SelectColumns(params Expression<Func<T, object>>[] columnNames)
        {
            var tableName = (typeof(T).GetCustomAttributes(typeof(TableAttribute), false).FirstOrDefault() as TableAttribute).Name;
            foreach (var item in columnNames)
            {
                _columns.Add(this.GetColumnName(item, tableName));
            }
            return this;
        }
        public IEnumerable<T> Query()
        {
            return ExeQueryAsync<T>().Result;
        }
        public async Task<IEnumerable<T>> QueryAsync()
        {
            return await ExeQueryAsync<T>();
        }
        private async Task<IEnumerable<TResult>> ExeQueryAsync<TResult>()
        {
            string sql = "";
            try
            {
                _sqlSelect = CreateSelectCommand();
                var parameters = new DynamicParameters();
                _sqlWhere = "";
                if (_whereParts.Any())
                {
                    _sqlWhere = $" WHERE ";
                    _sqlWhere += string.Join(" AND ", _whereParts.Select(x => x.Sql));
                    foreach (var item in _whereParts)
                    {
                        foreach (var param in item.Parameters)
                        {
                            parameters.Add(param.Key, param.Value, param.Type);
                        }
                    }
                }
                if (_orWhereParts.Any())
                {
                    if (string.IsNullOrEmpty(_sqlWhere))
                    {
                        _sqlWhere = $" WHERE ";
                    }
                    _sqlWhere += string.Join(" OR ", _orWhereParts.Select(x => x.Sql));
                    foreach (var item in _orWhereParts)
                    {
                        foreach (var param in item.Parameters)
                        {
                            parameters.Add(param.Key, param.Value, param.Type);
                        }
                    }
                }
                sql = $"{_sqlSelect} {_sqlWhere}";
                if (_groupByColumns.Any())
                {
                    sql = $"{sql} GROUP BY {string.Join(",", _groupByColumns)}";
                }
                if (_orderByColumns.Count > 0)
                {
                    sql = $"{sql} ORDER BY {string.Join(",", _orderByColumns)}";
                }
                using (SqlConnection connection = new SqlConnection(GetConnectString()))
                {
                    await connection.OpenAsync();
                    return await connection.QueryAsync<TResult>(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"ExeQueryAsync|sql={sql}|connectString={_connectString.Split(";")[0]}ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
            finally
            {
                Reset();
            }
        }
        //private IEnumerable<TResult> ExeQuery<TResult>()
        //{
        //    string sql = "";
        //    try
        //    {
        //        _sqlSelect = CreateSelectCommand();
        //        var parameters = new DynamicParameters();
        //        _sqlWhere = "";
        //        if (_whereParts.Any())
        //        {
        //            _sqlWhere = $" WHERE ";
        //            _sqlWhere += string.Join(" AND ", _whereParts.Select(x => x.Sql));
        //            foreach (var item in _whereParts)
        //            {
        //                foreach (var param in item.Parameters)
        //                {
        //                    parameters.Add(param.Key, param.Value, param.Type);
        //                }
        //            }
        //        }
        //        if (_orWhereParts.Any())
        //        {
        //            if (string.IsNullOrEmpty(_sqlWhere))
        //            {
        //                _sqlWhere = $" WHERE ";
        //            }
        //            _sqlWhere += string.Join(" OR ", _orWhereParts.Select(x => x.Sql));
        //            foreach (var item in _orWhereParts)
        //            {
        //                foreach (var param in item.Parameters)
        //                {
        //                    parameters.Add(param.Key, param.Value, param.Type);
        //                }
        //            }
        //        }
        //        sql = $"{_sqlSelect} {_sqlWhere}";
        //        if (_groupByColumns.Any())
        //        {
        //            sql = $"{sql} GROUP BY {string.Join(",", _groupByColumns)}";
        //        }
        //        if (_orderByColumns.Count > 0)
        //        {
        //            sql = $"{sql} ORDER BY {string.Join(",", _orderByColumns)}";
        //        }
        //        using (SqlConnection connection = new SqlConnection(GetConnectString()))
        //        {
        //            var result = connection.Query<TResult>(sql, parameters);
        //            return result;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        Reset();
        //    }
        //}

        public IEnumerable<T> Query(string strconnect)
        {
            _connectString = strconnect;
            return ExeQueryAsync<T>().Result;
        }
        public IEnumerable<TResult> Query<TResult>()
        {
            return ExeQueryAsync<TResult>().Result;
        }
        public async Task<IEnumerable<TResult>> QueryAsync<TResult>()
        {
            return await ExeQueryAsync<TResult>();
        }
        public IEnumerable<T> Query(string sql, Dapper.DynamicParameters parameters)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(GetConnectString()))
                {
                    connection.Open();
                    var result = connection.Query<T>(sql, parameters);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Query|sql={sql}|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }
        public async Task<IEnumerable<T>> QueryAsync(string sql, Dapper.DynamicParameters parameters)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(GetConnectString()))
                {
                    await connection.OpenAsync();
                    var result = await connection.QueryAsync<T>(sql, parameters);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Query|sql={sql}|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }
        /// <summary>
        /// TypeJoin: 0: join, 1: left join, 2: right join
        /// </summary>        
        public TableHelper<T> JoinOn<TInner>(Expression<Func<T, object>> colA, Expression<Func<TInner, object>> colB, int typeJoin = 0)
        {
            try
            {
                string joinon = "";
                var tableNameOut = (typeof(T).GetCustomAttributes(typeof(TableAttribute), false).FirstOrDefault() as TableAttribute).Name;
                var tableNameInner = (typeof(TInner).GetCustomAttributes(typeof(TableAttribute), false).FirstOrDefault() as TableAttribute).Name;
                var onBefore = GetColumnName(colA, tableNameOut);
                var onAfter = GetColumnName(colB, tableNameInner);
                joinon = $" JOIN {tableNameInner} (NOLOCK) ON {onBefore} = {onAfter} ";
                switch (typeJoin)
                {
                    case 1:
                        joinon = $" LEFT {joinon}";
                        break;
                    case 2:
                        joinon = $" RIGHT {joinon}";
                        break;
                    default:
                        break;
                }
                if (string.IsNullOrEmpty(_sqlJoin))
                {
                    _sqlJoin = $"{tableNameOut} (NOLOCK) ";
                }
                _sqlJoin += joinon;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return this;
        }
        public TableHelper<T> SelectColumnsJoinOn<TInner>(params Expression<Func<TInner, object>>[] columnNames)
        {
            var tableName = (typeof(TInner).GetCustomAttributes(typeof(TableAttribute), false).FirstOrDefault() as TableAttribute).Name;
            foreach (var item in columnNames)
            {
                _columns.Add(this.GetColumnName(item, tableName));
            }
            return this;
        }
        public TableHelper<T> OrderBy(params Expression<Func<T, object>>[] columnNames)
        {
            var tableName = (typeof(T).GetCustomAttributes(typeof(TableAttribute), false).FirstOrDefault() as TableAttribute).Name;
            foreach (var item in columnNames)
            {
                _orderByColumns.Add($"{this.GetColumnName(item, tableName)} ASC");
            }
            return this;
        }
        public TableHelper<T> OrderByDescending(params Expression<Func<T, object>>[] columnNames)
        {
            var tableName = (typeof(T).GetCustomAttributes(typeof(TableAttribute), false).FirstOrDefault() as TableAttribute).Name;
            foreach (var item in columnNames)
            {
                _orderByColumns.Add($"{this.GetColumnName(item, tableName)} DESC");
            }
            return this;
        }
        public TableHelper<T> WhereClause(params Expression<Func<T, bool>>[] columnNames)
        {
            var indexParams = GetNumberParamSQL();
            foreach (var item in columnNames)
            {
                indexParams++;
                _whereParts.Add(item.ToSql(indexParams));
            }
            return this;
        }
        public TableHelper<T> WhereClauseOR(params Expression<Func<T, bool>>[] columnNames)
        {
            var indexParams = GetNumberParamSQL();
            foreach (var item in columnNames)
            {
                indexParams++;
                _orWhereParts.Add(item.ToSql(indexParams));
            }
            return this;
        }
        public TableHelper<T> WhereClauseJoinOn<TInner>(params Expression<Func<TInner, bool>>[] columnNames)
        {
            var indexParams = GetNumberParamSQL();
            foreach (var item in columnNames)
            {
                indexParams++;
                _whereParts.Add(item.ToSql(indexParams));
            }
            return this;
        }
        private int GetNumberParamSQL()
        {
            return _whereParts.Sum(x => x.Parameters.Count()) + _orWhereParts.Sum(x => x.Parameters.Count());
        }
        public long Insert(T entity, bool setOffIdentity = false)
        {
            return InsertAsync(entity, setOffIdentity).Result;
        }
        public async Task<long> InsertAsync(T entity, bool setOffIdentity = false)
        {
            var sql = "";
            try
            {
                if (setOffIdentity)
                {
                    GetTableNameAndPrivateKey(entity);
                    sql = $"SET IDENTITY_INSERT {_tableName} ON {GenerateInsertQuery()} ";
                    sql += $"SET IDENTITY_INSERT {_tableName} OFF ";
                }
                using (SqlConnection connection = new SqlConnection(GetConnectString()))
                {
                    await connection.OpenAsync();
                    if (setOffIdentity)
                    {
                        return await connection.ExecuteAsync(sql, entity);
                    }
                    else
                    {
                        return await connection.InsertAsync(entity);
                    }
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2601 || ex.Number == 2627)
                {
                    // Violation in one on both...
                    return 0;
                }
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.LogError($"InsertAsync|tableName={_tableName}|sql={sql}|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }
        public bool Update(T entity)
        {
            return UpdateAsync(entity).Result;
        }
        public async Task<bool> UpdateAsync(T entity)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(GetConnectString()))
                {
                    await connection.OpenAsync();
                    return await connection.UpdateAsync(entity);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateAsync|tableName={_tableName}|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }
        private async Task<bool> Update(List<T> entitys)
        {
            try
            {
                if (entitys.Count >= _minRowUseBulk)
                {
                    this.UpdateBulk(entitys);
                    return true;
                }
                using (SqlConnection connection = new SqlConnection(GetConnectString()))
                {
                    await connection.OpenAsync();
                    return await connection.UpdateAsync(entitys);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"UpdateAsync|tableName={_tableName}|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }
        public void InsertBulk(IEnumerable<T> list, string tableName = "", string sqlDeleteDataOld = "", bool setOffIdentity = false)
        {
            if (list != null && list.Any())
            {
                if (string.IsNullOrEmpty(tableName))
                {
                    GetTableNameAndPrivateKey(list.ElementAt(0));
                    tableName = _tableName;
                }
                BulkOperationsHelpers.Instance.InsertBulk<T>(GetConnectString(), list, tableName, sqlDeleteDataOld, setOffIdentity);
            }
        }
        public void UpdateBulk(IEnumerable<T> list, string tableName = "", string privateKeyName = "")
        {
            if (list.Count() < _minRowUseBulk)
            {
                _ = Update(list.ToList());
                return;
            }
            if (string.IsNullOrEmpty(tableName) || string.IsNullOrEmpty(privateKeyName))
            {
                GetTableNameAndPrivateKey(list.ElementAt(0));
                tableName = _tableName;
                privateKeyName = _privateKeyName;
            }
            BulkOperationsHelpers.Instance.UpdateBulk<T>(GetConnectString(), list, tableName, privateKeyName);
        }
        public T GetByID(long ID)
        {
            return GetByIDAsync(ID).Result;
        }
        public async Task<T> GetByIDAsync(long ID)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(GetConnectString()))
                {
                    await connection.OpenAsync();
                    return await connection.GetAsync<T>(ID);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(T entity)
        {
            return DeleteAsync(entity).Result;
        }
        public async Task<bool> DeleteAsync(T entity)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(GetConnectString()))
                {
                    await connection.OpenAsync();
                    return await connection.DeleteAsync(entity);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteList(List<T> entity)
        {
            return DeleteListAsync(entity).Result;
        }
        public async Task<bool> DeleteListAsync(List<T> entity)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(GetConnectString()))
                {
                    await connection.OpenAsync();
                    return await connection.DeleteAsync(entity);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"DeleteListAsync|tableName={_tableName}|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }
        public int Execute(string sql)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(GetConnectString()))
                {
                    connection.Open();
                    return connection.Execute(sql);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Execute|sql={sql}|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
        }


        public TableHelper<T> SetMax(params Expression<Func<T, object>>[] columnNames)
        {
            foreach (var item in columnNames)
            {
                _lstMaxColumns.Add($"{this.GetColumnName(item)}");
            }
            return this;
        }

        public TableHelper<T> GroupBy(params Expression<Func<T, object>>[] columnNames)
        {
            foreach (var item in columnNames)
            {
                _groupByColumns.Add($"{this.GetColumnName(item)}");
            }
            return this;
        }
        public TableHelper<T> SetMin(params Expression<Func<T, object>>[] columnNames)
        {
            foreach (var item in columnNames)
            {
                _lstMinColumns.Add($"{this.GetColumnName(item)}");
            }
            return this;
        }

        public bool DeleteWhere(params Expression<Func<T, bool>>[] columnNames)
        {
            return DeleteWhereAsync(columnNames).Result;
        }
        public async Task<bool> DeleteWhereAsync(params Expression<Func<T, bool>>[] columnNames)
        {
            try
            {
                var indexParams = GetNumberParamSQL();
                foreach (var item in columnNames)
                {
                    indexParams++;
                    _whereParts.Add(item.ToSql(indexParams));
                }
                if (!_whereParts.Any())
                {
                    return false;
                }
                var parameters = new DynamicParameters();
                _sqlWhere = $" WHERE {string.Join(" AND ", _whereParts.Select(x => x.Sql))}";
                foreach (var item in _whereParts)
                {
                    foreach (var param in item.Parameters)
                    {
                        parameters.Add(param.Key, param.Value, param.Type);
                    }
                }
                var _tableName = (typeof(T).GetCustomAttributes(typeof(TableAttribute), false).FirstOrDefault() as TableAttribute).Name;

                string sql = $"DELETE {_tableName} {_sqlWhere} ";

                using (SqlConnection connection = new SqlConnection(GetConnectString()))
                {
                    await connection.OpenAsync();
                    var result = await connection.ExecuteAsync(sql, parameters);
                    if (result > 0)
                        return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                _logger.LogError($"DeleteWhereAsync|tableName={_tableName}|ex={ex.Message}-{ex.StackTrace}");
                throw ex;
            }
            finally
            {
                Reset();
            }
        }

        #region hàm private
        private string GetConnectString()
        {
            //return System.Configuration.ConfigurationManager.AppSettings["ConnectionStringModel"];
            //switch (_connectString)
            //{
            //    case DB_ConnectString.Log:
            //        return _config["Data:DefaultConnection:ConnectionStringLog"];
            //    case DB_ConnectString.Report:
            //        return _config["Data:DefaultConnection:ReportConnectionString"];
            //    case DB_ConnectString.THN:
            //        return _config["Data:DefaultConnection:ConnectionStringTHN"];
            //    default:
            //        return _config["Data:DefaultConnection:ConnectionString"];
            //}
            if (string.IsNullOrEmpty(_connectString))
            {
                _connectString = Configuration.ConnectStringSetting.DefaultConnectString;
            }
            return _connectString;
        }
        private void GetTableNameAndPrivateKey(T _)
        {
            _privateKeyName = typeof(T).GetProperties().Where(x => Attribute.IsDefined(x, typeof(KeyAttribute))).FirstOrDefault().Name;
            _tableName = (typeof(T).GetCustomAttributes(typeof(TableAttribute), false).FirstOrDefault() as TableAttribute).Name;
        }

        private string GetColumnName(Expression method, string tablename = null)
        {
            LambdaExpression lambda = method as LambdaExpression;
            if (lambda == null)
                throw new ArgumentNullException("method");

            MemberExpression memberExpr = null;

            if (lambda.Body.NodeType == ExpressionType.Convert)
            {
                memberExpr =
                    ((UnaryExpression)lambda.Body).Operand as MemberExpression;
            }
            else if (lambda.Body.NodeType == ExpressionType.MemberAccess)
            {
                memberExpr = lambda.Body as MemberExpression;
            }

            if (memberExpr == null)
            {
                throw new ArgumentException("method");
            }
            if (!string.IsNullOrEmpty(tablename))
            {
                return $"{tablename}.{memberExpr.Member.Name}";
            }
            return $"{memberExpr.Member.Name}";
        }
        private string CreateSelectCommand()
        {
            var _tableName = (typeof(T).GetCustomAttributes(typeof(TableAttribute), false).FirstOrDefault() as TableAttribute).Name;
            string selectColumn = $"{_tableName}.*";
            if (_columns.Any())
            {
                selectColumn = string.Join(",", _columns);
            }
            if (_lstMaxColumns.Any())
            {
                foreach (var col in _lstMaxColumns)
                {
                    selectColumn += $", MAX({col}) {col}";
                }
            }
            if (_lstMinColumns.Any())
            {
                foreach (var col in _lstMinColumns)
                {
                    selectColumn += $", MIN({col}) {col}";
                }
            }
            string selectMax = _top > 0 ? $"TOP {_top} {selectColumn}" : selectColumn;
            if (_isDistinct)
            {
                selectMax = $"DISTINCT {selectMax}";
            }
            string command = string.Format("SELECT {0} FROM {1} (NOLOCK) ", selectMax, _tableName);
            if (!string.IsNullOrEmpty(_sqlJoin))
            {
                command = string.Format("SELECT {0} FROM {1} ", selectMax, _sqlJoin);
            }
            return command;
        }
        private List<string> GenerateListOfProperties(IEnumerable<PropertyInfo> listOfProperties)
        {
            return (from prop in listOfProperties
                    let attributes = prop.GetCustomAttributes(typeof(DescriptionAttribute), false)
                    where attributes.Length <= 0 || (attributes[0] as DescriptionAttribute)?.Description != "ignore"
                    select prop.Name).ToList();
        }
        private IEnumerable<PropertyInfo> GetProperties => typeof(T).GetProperties();
        private string GenerateInsertQuery()
        {
            var insertQuery = new StringBuilder($"INSERT INTO {_tableName} ");

            insertQuery.Append("(");
            var properties = GenerateListOfProperties(GetProperties);
            properties.ForEach(prop => { insertQuery.Append($"[{prop}],"); });
            insertQuery
                .Remove(insertQuery.Length - 1, 1)
                .Append(") VALUES (");
            properties.ForEach(prop => { insertQuery.Append($"@{prop},"); });
            insertQuery
                .Remove(insertQuery.Length - 1, 1)
                .Append(")");
            return insertQuery.ToString();
        }
        private void Reset()
        {
            _whereParts.Clear();
            _sqlSelect = "";
            _columns.Clear();
            _sqlJoin = "";
            _orderByColumns.Clear();
            _isDistinct = false;
            _top = 0;
            _lstMaxColumns.Clear();
            _groupByColumns.Clear();
            _lstMinColumns.Clear();
            _orWhereParts.Clear();
        }

        #endregion
    }

    #region helper
    public class Parameter
    {
        public Parameter(string key, object value, DbType? type = null)
        {
            Key = key;
            Value = value;
            Type = type ?? GetValueType(value);
        }

        public string Key { get; }
        public object Value { get; }
        public DbType? Type { get; }

        private static DbType? GetValueType(object value)
        {
            if (value is string)
            {
                return DbType.String;
            }

            return null; // Use SqlMapper DefaultTypes mapping
        }
    }
    public class WherePart
    {
        private WherePart(string sql, params Parameter[] parameters) : this(sql, parameters.ToList())
        {
        }

        private WherePart(string sql, IEnumerable<Parameter> parameters)
        {
            Sql = sql;
            Parameters = new List<Parameter>(parameters);
        }

        public string Sql { get; }
        public bool HasSql => !string.IsNullOrEmpty(Sql);

        public IReadOnlyList<Parameter> Parameters { get; }

        public static WherePart IsSql(string sql)
        {
            return new WherePart(sql);
        }

        public static WherePart IsParameter(int count, object value)
        {
            return new WherePart($"@{count}", new Parameter(count.ToString(), value));
        }

        public static WherePart Between(ref int countStart, object value)
        {
            var parameters = new List<Parameter>();
            var sql = new StringBuilder("(");
            parameters.Add(new Parameter(countStart.ToString(), value));
            sql.Append($"@{countStart},");
            countStart++;
            sql[sql.Length - 1] = ')';
            return new WherePart(sql.ToString(), parameters);
        }

        public static WherePart IsCollection(ref int countStart, IEnumerable values)
        {
            var parameters = new List<Parameter>();
            var sql = new StringBuilder("(");
            foreach (var value in values)
            {
                parameters.Add(new Parameter(countStart.ToString(), value));
                sql.Append($"@{countStart},");
                countStart++;
            }

            if (sql.Length == 1)
            {
                sql.Append("null,");
            }

            sql[sql.Length - 1] = ')';
            return new WherePart(sql.ToString(), parameters);
        }

        public static WherePart Concat(string @operator, WherePart operand)
        {
            return new WherePart($"({@operator} {operand.Sql})", operand.Parameters);
        }

        public static WherePart Concat(WherePart left, string @operator, WherePart right)
        {
            if (right.Sql.Equals("NULL", StringComparison.InvariantCultureIgnoreCase))
            {
                @operator = @operator == "=" ? "IS" : "IS NOT";
            }

            return new WherePart($"({left.Sql} {@operator} {right.Sql})", left.Parameters.Union(right.Parameters));
        }

        public static WherePart Empty => new WherePart(string.Empty);
    }

    public static class WhereBuilder
    {
        private static readonly int MaxItemQueryIn = 2000;
        private static readonly IDictionary<ExpressionType, string> nodeTypeMappings = new Dictionary<ExpressionType, string>
        {
            {ExpressionType.Add, "+"},
            {ExpressionType.And, "AND"},
            {ExpressionType.AndAlso, "AND"},
            {ExpressionType.Divide, "/"},
            {ExpressionType.Equal, "="},
            {ExpressionType.ExclusiveOr, "^"},
            {ExpressionType.GreaterThan, ">"},
            {ExpressionType.GreaterThanOrEqual, ">="},
            {ExpressionType.LessThan, "<"},
            {ExpressionType.LessThanOrEqual, "<="},
            {ExpressionType.Modulo, "%"},
            {ExpressionType.Multiply, "*"},
            {ExpressionType.Negate, "-"},
            {ExpressionType.Not, "NOT"},
            {ExpressionType.NotEqual, "<>"},
            {ExpressionType.Or, "OR"},
            {ExpressionType.OrElse, "OR"},
            {ExpressionType.Subtract, "-"}
        };

        public static WherePart ToSql<T>(this Expression<Func<T, bool>> expression, int indexParamer = 1)
        {
            var i = indexParamer;

            var result = Recurse<T>(ref i, expression.Body, isUnary: true);
            return result;
        }

        private static WherePart Recurse<T>(ref int i, Expression expression, bool isUnary = false,
            string prefix = null, string postfix = null, bool left = true)
        {
            switch (expression)
            {
                case UnaryExpression unary: return UnaryExpressionExtract<T>(ref i, unary);
                case BinaryExpression binary: return BinaryExpressionExtract<T>(ref i, binary);
                case ConstantExpression constant: return ConstantExpressionExtract(ref i, constant, isUnary, prefix, postfix, left);
                case MemberExpression member: return MemberExpressionExtract<T>(ref i, member, isUnary, prefix, postfix, left);
                case MethodCallExpression method: return MethodCallExpressionExtract<T>(ref i, method);
                case InvocationExpression invocation: return InvocationExpressionExtract<T>(ref i, invocation, left);
                default: throw new Exception("Unsupported expression: " + expression.GetType().Name);
            }
        }

        private static WherePart InvocationExpressionExtract<T>(ref int i, InvocationExpression expression, bool left)
        {
            return Recurse<T>(ref i, ((Expression<Func<T, bool>>)expression.Expression).Body, left: left);
        }

        private static WherePart MethodCallExpressionExtract<T>(ref int i, MethodCallExpression expression)
        {
            // LIKE queries:
            if (expression.Method == typeof(string).GetMethod("Contains", new[] { typeof(string) }))
            {
                return WherePart.Concat(Recurse<T>(ref i, expression.Object), "LIKE",
                    Recurse<T>(ref i, expression.Arguments[0], prefix: "%", postfix: "%"));
            }

            if (expression.Method == typeof(string).GetMethod("StartsWith", new[] { typeof(string) }))
            {
                return WherePart.Concat(Recurse<T>(ref i, expression.Object), "LIKE",
                    Recurse<T>(ref i, expression.Arguments[0], postfix: "%"));
            }

            if (expression.Method == typeof(string).GetMethod("EndsWith", new[] { typeof(string) }))
            {
                return WherePart.Concat(Recurse<T>(ref i, expression.Object), "LIKE",
                    Recurse<T>(ref i, expression.Arguments[0], prefix: "%"));
            }

            if (expression.Method == typeof(string).GetMethod("Equals", new[] { typeof(string) }))
            {
                return WherePart.Concat(Recurse<T>(ref i, expression.Object), "=",
                    Recurse<T>(ref i, expression.Arguments[0], left: false));
            }

            // IN queries:
            if (expression.Method.Name == "Contains")
            {
                Expression collection;
                Expression property;
                if (expression.Method.IsDefined(typeof(ExtensionAttribute)) && expression.Arguments.Count == 2)
                {
                    collection = expression.Arguments[0];
                    property = expression.Arguments[1];
                }
                else if (!expression.Method.IsDefined(typeof(ExtensionAttribute)) && expression.Arguments.Count == 1)
                {
                    collection = expression.Object;
                    property = expression.Arguments[0];
                }
                else
                {
                    throw new Exception("Unsupported method call: " + expression.Method.Name);
                }

                var values = (IEnumerable)GetValue(collection);
                int count = 0;
                object min = null;
                object max = null;
                foreach (var value in values)
                {
                    if (min == null || Comparer<object>.Default.Compare(min, value) > 0)
                    {
                        min = value;
                    }
                    if (max == null || Comparer<object>.Default.Compare(max, value) < 0)
                    {
                        max = value;
                    }
                    count++;
                }
                if (count > WhereBuilder.MaxItemQueryIn)
                {
                    var partMin = WherePart.Concat(Recurse<T>(ref i, property), " >=", WherePart.Between(ref i, min));
                    var partMax = WherePart.Concat(Recurse<T>(ref i, property), "<=", WherePart.Between(ref i, max));
                    return WherePart.Concat(partMin, " AND ", partMax);
                }
                return WherePart.Concat(Recurse<T>(ref i, property), "IN", WherePart.IsCollection(ref i, values));
            }

            throw new Exception("Unsupported method call: " + expression.Method.Name);
        }

        private static WherePart MemberExpressionExtract<T>(ref int i, MemberExpression expression, bool isUnary,
            string prefix, string postfix, bool left)
        {
            if (isUnary && expression.Type == typeof(bool))
            {
                return WherePart.Concat(Recurse<T>(ref i, expression), "=", WherePart.IsSql("1"));
            }

            if (expression.Member is PropertyInfo property)
            {
                var tableName = (typeof(T).GetCustomAttributes(typeof(TableAttribute), false).FirstOrDefault() as TableAttribute).Name;
                if (left)
                {
                    //var className = expression.Member.ReflectedType.Name;
                    var className = (expression.Member.ReflectedType.GetCustomAttributes(typeof(TableAttribute), false).FirstOrDefault() as TableAttribute)?.Name;
                    //var className = (typeof(type).GetCustomAttributes(typeof(TableAttribute), false).FirstOrDefault() as TableAttribute).Name;
                    var colName = expression.Member.Name;
                    if (expression.Expression.NodeType == ExpressionType.Parameter)
                    {
                        return WherePart.IsSql($"[{tableName}].[{colName}]");
                    }
                    //if (!string.IsNullOrEmpty(className) && className.ToLower() == tableName.ToLower())
                    //{
                    //    return WherePart.IsSql($"[{tableName}].[{colName}]");
                    //}
                    else
                    {
                        var value = GetValue(expression);
                        if (value is string textValue)
                        {
                            value = prefix + textValue + postfix;
                        }
                        return WherePart.IsParameter(i++, value);
                    }
                }

                if (property.PropertyType == typeof(bool))
                {
                    var colName = expression.Member.Name;
                    //var tableName = (typeof(T).GetCustomAttributes(typeof(TableAttribute), false).FirstOrDefault() as TableAttribute).Name;
                    return WherePart.IsSql($"[{tableName}].[{colName}]=1");
                }
            }

            if (expression.Member is FieldInfo || left == false)
            {
                var value = GetValue(expression);
                if (value is string textValue)
                {
                    value = prefix + textValue + postfix;
                }

                return WherePart.IsParameter(i++, value);
            }

            throw new Exception($"Expression does not refer to a property or field: {expression}");
        }
        private static WherePart ConstantExpressionExtract(ref int i, ConstantExpression expression, bool isUnary,
            string prefix, string postfix, bool left)
        {
            var value = expression.Value;

            switch (value)
            {
                case null:
                    return WherePart.IsSql("NULL");
                case int _:
                    return WherePart.IsSql(value.ToString());
                case string text:
                    value = prefix + text + postfix;
                    break;
            }

            if (!(value is bool) || isUnary) return WherePart.IsParameter(i++, value);

            var result = ((bool)value) ? "1" : "0";
            if (left)
                result = result.Equals("1") ? "1=1" : "0=0";
            return WherePart.IsSql(result);
        }

        private static WherePart BinaryExpressionExtract<T>(ref int i, BinaryExpression expression)
        {
            return WherePart.Concat(Recurse<T>(ref i, expression.Left), NodeTypeToString(expression.NodeType),
                Recurse<T>(ref i, expression.Right, left: false));
        }

        private static WherePart UnaryExpressionExtract<T>(ref int i, UnaryExpression expression)
        {
            return WherePart.Concat(NodeTypeToString(expression.NodeType), Recurse<T>(ref i, expression.Operand, true));
        }
        private static object GetValue(Expression member)
        {
            var objectMember = Expression.Convert(member, typeof(object));
            var getterLambda = Expression.Lambda<Func<object>>(objectMember);
            var getter = getterLambda.Compile();
            return getter();
        }

        private static string NodeTypeToString(ExpressionType nodeType)
        {
            return nodeTypeMappings.TryGetValue(nodeType, out var value)
                ? value
                : string.Empty;
        }

        public static List<T> AsList<T>(this IEnumerable<T> source) =>
            (source == null || source is List<T>) ? (List<T>)source : source.ToList();
    }
    #endregion
}
